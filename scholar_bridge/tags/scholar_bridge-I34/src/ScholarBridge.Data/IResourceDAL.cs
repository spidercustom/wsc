﻿using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
	public interface IResourceDAL : IDAL<Resource>
	{
		Resource FindById(int id);
		Resource Save(Resource article);
	}
}