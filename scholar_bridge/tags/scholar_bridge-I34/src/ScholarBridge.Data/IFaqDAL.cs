﻿using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
	public interface IFaqDAL : IDAL<Faq>
	{
		Faq FindById(int id);
		Faq Save(Faq faq);
	}
}