﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(230)]
    public class ChangeScholarshipAddExpectedFamilyContribution : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        public readonly string[] COLUMNS
           = { "IsFAFSARequired", "IsFAFSAEFCRequired", "IsFamilyMembersInHouseHoldRequired",
                 "IsFamilyDependentsAttendingCollegeRequired" ,"IsFamilyIncomeRequired",
                 "FamilyIncomeFrom","FamilyIncomeTo"};
        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, COLUMNS[0], DbType.Boolean);
            Database.AddColumn(TABLE_NAME, COLUMNS[1], DbType.Boolean);
            Database.AddColumn(TABLE_NAME, COLUMNS[2], DbType.Boolean);
            Database.AddColumn(TABLE_NAME, COLUMNS[3], DbType.Boolean);
            Database.AddColumn(TABLE_NAME, COLUMNS[4], DbType.Boolean);
            Database.AddColumn(TABLE_NAME, COLUMNS[5], DbType.Decimal);
            Database.AddColumn(TABLE_NAME, COLUMNS[6], DbType.Decimal);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMNS[0]);
            Database.RemoveColumn(TABLE_NAME, COLUMNS[1]);
            Database.RemoveColumn(TABLE_NAME, COLUMNS[2]);
            Database.RemoveColumn(TABLE_NAME, COLUMNS[3]);
            Database.RemoveColumn(TABLE_NAME, COLUMNS[4]);
            Database.RemoveColumn(TABLE_NAME, COLUMNS[5]);
            Database.RemoveColumn(TABLE_NAME, COLUMNS[6]);
        }
    }
}