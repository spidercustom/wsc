﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(103)]
    public class ChangeMessageRemoveRelatedRelationshipColumn : Migration
    {
        public const string RELATEDRELATIONSHIP_COLUMNNAME = "RelatedRelationshipId";
        private const string FK_SBMessage_SBRelationship = "FK_SBMessage_RelatedRelationshipId";


        public override void Up()
        {
            Database.RemoveForeignKey(AddMessagesTable.TABLE_NAME, FK_SBMessage_SBRelationship);
            Database.RemoveColumn(AddMessagesTable.TABLE_NAME, RELATEDRELATIONSHIP_COLUMNNAME);
        }

        public override void Down()
        {
            Database.AddColumn(AddMessagesTable.TABLE_NAME,RELATEDRELATIONSHIP_COLUMNNAME,DbType.Int32);
            Database.AddForeignKey(FK_SBMessage_SBRelationship, AddMessagesTable.TABLE_NAME,
                                   RELATEDRELATIONSHIP_COLUMNNAME, AddRelationship.TABLE_NAME, AddRelationship.PRIMARY_KEY_COLUMN);
        }
    }
}
