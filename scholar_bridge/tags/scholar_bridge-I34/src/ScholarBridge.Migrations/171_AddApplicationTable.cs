using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(171)]
    public class AddApplicationTable : AddTableBase
    {
        public const string TABLE_NAME = "SBApplication";
        public const string PRIMARY_KEY_COLUMN = "SBApplicationId";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("SeekerId", DbType.Int32, ColumnProperty.NotNull),
                           new Column("ScholarshipId", DbType.Int32, ColumnProperty.NotNull),
                           new Column("Stage", DbType.String,50, ColumnProperty.NotNull),

                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())"),
                           new Column("AddressStreet1", DbType.String, 50, ColumnProperty.Null),
                           new Column("AddressStreet2", DbType.String, 50, ColumnProperty.Null),
                           new Column("AddressState", DbType.String, 2, ColumnProperty.Null),
                           new Column("AddressPostalCode", DbType.String, 10, ColumnProperty.Null),
                           new Column("PhoneNumber", DbType.String,10, ColumnProperty.Null),
                           new Column("MobilePhoneNumber", DbType.String,10, ColumnProperty.Null),
                           new Column("DateOfBirth", DbType.DateTime, ColumnProperty.Null),
                           new Column("CityIndex", DbType.Int32, ColumnProperty.Null),
                           new Column("CountyIndex", DbType.Int32, ColumnProperty.Null),
                           new Column("Gender", DbType.Int32, ColumnProperty.Null),
                           new Column("EthnicityOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("ReligionOther", DbType.String, 250, ColumnProperty.Null),
                           
                           new Column("PersonalStatement", DbType.String, 1500, ColumnProperty.Null),
                           new Column("MyChallenge", DbType.String, 150, ColumnProperty.Null),
                           new Column("MyGift", DbType.String, 150, ColumnProperty.Null),

                           new Column("SportOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("AcademicAreaOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("CareerOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("CommunityInvolvementCauseOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("HobbyOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("ClubOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("SeekerMatchOrganizationOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("AffiliationTypeOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("WorkTypeOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("WorkHourOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("ServiceTypeOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("ServiceHourOther", DbType.String, 250, ColumnProperty.Null),
                           new Column("Fafsa", DbType.Boolean,  ColumnProperty.Null),
                           new Column("UserDerived", DbType.Boolean,  ColumnProperty.Null),
                           new Column("MinimumSeekerNeed", DbType.Double, ColumnProperty.Null),
                           new Column("MaximumSeekerNeed", DbType.Double, ColumnProperty.Null),
                           
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new[]
                       {
                           CreateForeignKey("SeekerId", "SBSeeker", "SBSeekerId"),
                           CreateForeignKey("ScholarshipId", "SBScholarship", "SBScholarshipId"),
                           CreateForeignKey("CityIndex", "SBCityLUT", "SBCityIndex"),
                           CreateForeignKey("CountyIndex", "SBCountyLUT", "SBCountyIndex"),

                       };
        }
    }
}