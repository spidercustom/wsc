using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(183)]
    public class AddApplicationClubRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationClubRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBClubLUT"; }
        }
    }
}