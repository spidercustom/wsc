﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(139)]
    public class AddActivitiesToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("SportOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("AcademicAreaOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("CareerOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("CommunityInvolvementCauseOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("SeekerHobbyOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("ClubOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("SeekerMatchOrganizationOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("AffiliationTypeOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("WorkTypeOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("WorkHourOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("ServiceTypeOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("ServiceHourOther", DbType.String, 250));

        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "SportOther");
            Database.RemoveColumn(TABLE_NAME, "AcademicAreaOther");
            Database.RemoveColumn(TABLE_NAME, "CareerOther");
            Database.RemoveColumn(TABLE_NAME, "CommunityInvolvementCauseOther");
            Database.RemoveColumn(TABLE_NAME, "SeekerHobbyOther");
            Database.RemoveColumn(TABLE_NAME, "ClubOther");
            Database.RemoveColumn(TABLE_NAME, "SeekerMatchOrganizationOther");
            Database.RemoveColumn(TABLE_NAME, "AffiliationTypeOther");
            Database.RemoveColumn(TABLE_NAME, "WorkTypeOther");
            Database.RemoveColumn(TABLE_NAME, "WorkHourOther");
            Database.RemoveColumn(TABLE_NAME, "ServiceTypeOther");
            Database.RemoveColumn(TABLE_NAME, "ServiceHourOther");
        }
    }
}