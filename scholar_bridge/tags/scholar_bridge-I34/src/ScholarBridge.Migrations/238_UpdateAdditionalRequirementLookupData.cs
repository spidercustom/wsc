﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(238)]
    public class UpdateAdditionalRequirementLookupData : Migration
	{
		private const string SBAdditionalRequirementLUT = "SBAdditionalRequirementLUT";
	    private const String recordId = "5";
		
		public override void Up()
		{
            Database.ExecuteNonQuery("update " + SBAdditionalRequirementLUT +
                                     @" set SBAdditionalRequirement = 'Resume / Work History', Description = 'History of Previous work.'
										where SBAdditionalRequirementIndex = " +recordId );
		}

		public override void Down()
		{
            Database.ExecuteNonQuery("update " + SBAdditionalRequirementLUT +
                                     @" set SBAdditionalRequirement = 'Transcripts - College', Description = 'Previous college transcripts'
										where SBAdditionalRequirementIndex = " + recordId);
			
		}

		
	}
}
