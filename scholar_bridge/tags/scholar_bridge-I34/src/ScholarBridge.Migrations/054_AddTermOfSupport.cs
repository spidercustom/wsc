﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(54)]
    public class AddTermOfSupport : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "TermOfSupport";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }

        public override void AddDefaultData()
        {
            var adminId = HECBStandardDataHelper.GetAdminUserId(Database);
            var columns = new[] {INDICATIVE_NAME, "Description", "LastUpdateBy" };
            Database.Insert(TableName, columns, new[] { "Quarter", "Academic quarter", adminId.ToString() });
            Database.Insert(TableName, columns, new[] { "Semester", "Academic semester", adminId.ToString() });
            Database.Insert(TableName, columns, new[] { "Academic Year", "Academic year", adminId.ToString() });
            Database.Insert(TableName, columns, new[] { "Other", "Other period of time", adminId.ToString() });
        }
    }
}
