﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(88)]
    public class ChangeScholarshipChangeMatchCriteriaAPCreditsEarnedToNullable : Migration
    {
        public override void Up()
        {
            Database.ChangeColumn("SBScholarship", new Column("MatchCriteriaAPCreditsEarned", DbType.Int32, ColumnProperty.Null, null));
        }

        public override void Down()
        {
            // FIXME: This doesn't work
            Database.ChangeColumn("SBScholarship", new Column("MatchCriteriaAPCreditsEarned", DbType.Int32, ColumnProperty.NotNull, 0));
        }
    }
}
