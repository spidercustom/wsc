﻿using System;
using System.Runtime.Serialization;

namespace ScholarBridge.Business.Exceptions
{
    [Serializable]
    public class TooManyScholarshipCopyExistsException : Exception
    {
        public const string DEFAULT_MESSAGE =
            "There are too many copies of same scholarship. Please rename one or more from them and try again.";

        public TooManyScholarshipCopyExistsException()
            : base(DEFAULT_MESSAGE)
        {
        }

        public TooManyScholarshipCopyExistsException(string message) : base(message)
        {
        }

        public TooManyScholarshipCopyExistsException(string message, Exception inner) : base(message, inner)
        {
        }

        protected TooManyScholarshipCopyExistsException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
