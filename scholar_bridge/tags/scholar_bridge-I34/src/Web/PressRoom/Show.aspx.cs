﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Business;
using ScholarBridge.Web.Extensions;
namespace ScholarBridge.Web.PressRoom
{
    public partial class Show : System.Web.UI.Page
    {
        public IArticleService ArticleService { get; set; }
        private int ArticleId
        {
            get
            {
                int articleId;
                if (!Int32.TryParse(Request.Params["id"], out articleId))
                    throw new ArgumentException("Cannot understand value of parameter id");
                return articleId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var article = ArticleService.GetById(ArticleId);
            if (article == null)
            {
                errorMessage.Visible = true;
                return;
            }

            DateControl.Text = article.PostedDate.ToShortDateString();
            var title = article.Title.BuildLinks();
            TitleControl.Text = title.PreserveLineBreaks();
            var body = article.Body.BuildLinks();
            BodyControl.Text = body.PreserveLineBreaks();

        }

       
    }
}
