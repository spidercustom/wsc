﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ShowApplication : UserControl
    {
        public string LinkTo { get; set; }
        public string ScholarshipLinkTo { get; set; }
        public Application ApplicationToView { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                scholarshipName.Text = ApplicationToView.Scholarship.DisplayName;

                ApplicationBasicsShow1.ApplicationToView = ApplicationToView;
                ApplicationAboutMeShow1.ApplicationToView = ApplicationToView;
                ApplicationAcademicInfoShow1.ApplicationToView = ApplicationToView;
                ApplicationActivitiesShow1.ApplicationToView = ApplicationToView;
                ApplicationFinancialNeedShow1.ApplicationToView = ApplicationToView;
                ApplicationAdditionalRequirementsShow1.ApplicationToView = ApplicationToView;
            }
        }
    }
}