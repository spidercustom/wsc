﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalFooter.ascx.cs" Inherits="ScholarBridge.Web.Common.GlobalFooter" %>
<div class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</div>
<div class="GreenFooterLinks"><A href='<%= LinkGenerator.GetFullLink("/AboutUs.aspx") %>'>About Us</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/PressRoom/") %>'>Press Room</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/Terms.aspx") %>'>Terms &amp; Conditions</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/ContactUs.aspx") %>'>Contact Us</A>  |  
                            <A href='<%= ResolveUrl("~/SiteMap.aspx") %>'>Sitemap</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/Resources/") %>'>Resources</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/Help.aspx") %>'>Help</A>  
                            <A id="loginLink" runat="server">| Log In</A></div>

