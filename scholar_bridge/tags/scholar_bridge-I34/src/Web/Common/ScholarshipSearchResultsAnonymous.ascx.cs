﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace ScholarBridge.Web.Common
{
	public partial class ScholarshipSearchResultsAnonymous : UserControl
    {
        public string Criteria
        {
			get 
			{ return (string)ViewState["criteria"]; }
			set
			{
				ViewState["criteria"] = value;
                Bind();
            }
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Bind()
        {
			if (!(Criteria == null))
            {
				XElement scholarshipDoc = XElement.Load(Server.MapPath("~/_ScholarshipData/scholarships.xml"));

            	string criteriaLowerCase = Criteria.ToLower();
            	var query = from s in scholarshipDoc.Descendants("scholarship")
							where s.Element("Name").Value.ToLower().Contains(criteriaLowerCase) ||
								  s.Element("ProviderName").Value.ToLower().Contains(criteriaLowerCase) ||
								  s.Element("IntermediaryName").Value.ToLower().Contains(criteriaLowerCase) ||
								  s.Element("DonorName").Value.ToLower().Contains(criteriaLowerCase)
							select s;

                lstScholarships.DataSource = query.ToList();
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
			pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }
        protected void pager_PreRender(object sender, EventArgs e)
        {
			if (pager.TotalRowCount <= pager.PageSize)
			{
				pager.Visible = false;
			}
        }
        
    }
}