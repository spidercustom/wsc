﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintScholarship.ascx.cs"
    Inherits="ScholarBridge.Web.Common.PrintScholarship" %>
<div class="subsection">
    <br />
    <br />
   <div>         
       <p class="smallText indentsection">Scholarship Information For:</p> 
       <asp:Label CssClass="headerText indentsection" ID="scholarshipName" runat="server" ></asp:Label>  
       <br />
       <br />
         
         <table class="viewonlyTable indentsection">
            <tr>
            <td><label class="sectionHeader">Application Due On:</label></td><td><asp:Label CssClass="sectionHeader" ID="ApplicationDueOn" runat="server" ></asp:Label> </td>
            
            </tr>
           
         </table>
 
  </div> 
   <br />
   <br />
   <table class="viewonlyTable">
            <tr>
            <td><label class="captionMedium">Academic Year:</label></td><td>
            
                <table class="viewonlyTable">
                            <tr>
                                <td><asp:Label CssClass="smallText" ID="AcademicYear" runat="server" ></asp:Label></td><td></td>
                            </tr>
                            
                        </table>
            
            
            </td>
            </tr>
            <tr>
                <td><label class="captionMedium">Mission Statement:</label></td><td>
                <table class="viewonlyTable">
                            <tr>
                                <td><asp:Label CssClass="smallText" ID="MissionStatement" runat="server" ></asp:Label></td><td></td>
                            </tr>
                            
                        </table>
                        </td>
               </tr>
             <tr runat="server" id="DonorRow">
                <td><label class="captionMedium">Donor:</label></td><td><asp:Label CssClass="smallText" ID="Donor" runat="server" ></asp:Label></td>
            </tr>
             <tr>
                <td><label class="captionMedium">Schedule:</label></td>
                <td>
                        <table class="viewonlyTable">
                            <tr>
                                <td><label class="captionMedium">Application Starts On:</label></td><td><asp:Label CssClass="smallText" ID="ScheduleApplicationStart" runat="server" ></asp:Label></td>
                            </tr>
                             <tr>
                                <td><label class="captionMedium">Application Due On:</label></td><td><asp:Label CssClass="smallText" ID="ScheduleApplicationDue" runat="server" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td><label class="captionMedium">Award On:</label></td><td><asp:Label CssClass="smallText" ID="ScheduleApplicationAwardOn" runat="server" ></asp:Label></td>
                            </tr>
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Funding:</label></td>
                <td>
                        <table class="viewonlyTable">
                            <tr>
                                <td><label class="captionMedium">Number of Awards:</label></td><td><asp:Label CssClass="smallText" ID="FundingNumberOfAwards" runat="server" ></asp:Label></td>
                            </tr>
                             <tr>
                                <td><label class="captionMedium">Award Amount:</label></td><td><asp:Label CssClass="smallText" ID="FundingAwardAmount" runat="server" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td><label class="captionMedium">Total Funds Available:</label></td><td><asp:Label CssClass="smallText" ID="FundingTotalFunds" runat="server" ></asp:Label></td>
                            </tr>
                             <tr>
                                <td><label class="captionMedium">Support Period:</label></td><td><asp:Label CssClass="smallText" ID="FundingSupportPeriod" runat="server" ></asp:Label></td>
                            </tr>
                        </table>
                </td>
            </tr>
            <tr>
                <td><label class="captionMedium">Contact Information:</label></td>
                <td>
                        <table class="viewonlyTable">
                            <tr>
                                <td><label class="captionMedium">Name:</label></td><td><asp:Label CssClass="smallText" ID="ContactName" runat="server" ></asp:Label></td>
                            </tr>
                             <tr runat="server" id="WebsiteRow">
                                <td><label class="captionMedium">Website:</label></td><td><asp:Label CssClass="smallText" ID="ContactWebsite" runat="server" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td><label class="captionMedium">Address:</label></td><td><asp:Label CssClass="smallText" ID="ContactAddress" runat="server" ></asp:Label></td>
                            </tr>
                             <tr>
                                <td><label class="captionMedium">Phone:</label></td><td><asp:Label CssClass="smallText" ID="ContactPhone" runat="server" ></asp:Label></td>
                            </tr>
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Applicant Eligibility Guidelines:</label></td>
                <td>
                        <table class="viewonlyTable">
                            <tr>
                                <td><label class="captionMedium">Type of Students Eligible to Apply:</label></td><td class="limitWidth"><asp:Label CssClass="smallText" ID="EligibilityTypeOfStudent" runat="server" ></asp:Label></td>
                            </tr>
                             <tr>
                                <td><label class="captionMedium">Situations the Scholarhsip will Fund:</label></td><td><asp:Label CssClass="smallText" ID="EligibilitySupportedSituation" runat="server" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td><label class="captionMedium">School Types:</label></td><td><asp:Label CssClass="smallText" ID="EligibilitySchoolType" runat="server" ></asp:Label></td>
                            </tr>
                             <tr>
                                <td><label class="captionMedium">Academic Programs:</label></td><td><asp:Label CssClass="smallText" ID="EligibilityAcademicPrograms" runat="server" ></asp:Label></td>
                            </tr>
                            <tr>
                                <td><label class="captionMedium">EnrollmentStatus:</label></td><td><asp:Label CssClass="smallText" ID="EligibilityEnrollmentStatus" runat="server" ></asp:Label></td>
                            </tr>
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Minimum Requirements:</label></td>
                <td>
                        <table class="viewonlyTable" runat="server" id="MinimumTable" >
                             
                            
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Preference Given For:</label></td>
                <td>
                        <table class="viewonlyTable" runat="server" id="PreferenceTable" >
                             
                            
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Financial Need:</label></td>
                <td>
                        <table class="viewonlyTable" runat="server" id="FinancialNeedTable" >
                             
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Additional Scholarship Requirements:</label></td>
                <td>
                        <table class="viewonlyTable" runat="server" id="AddtionalRequiremnetsTable">
                            <tr>
                                <td><label class="smallText">Please include the following with your application:</label></td><td></td>
                            </tr> 
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Scholarship Questions:</label></td>
                <td>
                        <table class="viewonlyTable" runat="server" id="QuestionsTable">
                             <tr>
                                <td><label class="smallText">Please answer the following questions:</label></td><td></td>
                            </tr> 
                        </table>
                </td>
            </tr>
            
            <tr>
                <td><label class="captionMedium">Additional Application Forms:</label></td>
                <td>
                        <table class="viewonlyTable" runat="server" id="FormsTable">
                             
                        </table>
                </td>
            </tr>
            
    </table>
        
 
 
</div>
