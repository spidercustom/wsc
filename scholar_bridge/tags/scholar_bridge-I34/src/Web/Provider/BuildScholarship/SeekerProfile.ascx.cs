﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;
using ScholarBridge.Domain.Extensions;
using System.Web;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class SeekerProfile : WizardStepUserControlBase<Scholarship>
    {
        private const string GPA_VALUE_FORMAT = "0.000";
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IStateDAL StateService { get; set; }
        public const string WashingtonState = "WA";
        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context"); 

            if (!IsPostBack)
            {
                EthnicityControl.DataBind();
                ReligionControl.DataBind();
                ClassRankControl.DataBind();
                BindAttibuteSelectionControls();
                PopulateStates();
                
                PopulateScreen();
            } else
            {
                FirstGenerationControl.Text = AttributeSelectionFirstGenerationControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
                HonorsControl.Text = AttributeSelectionHonorsControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
                SetSectionsEnabledState();
            }
        }

        public override void Activated()
        {
            base.Activated();
            SetSectionsEnabledState();
            PopulateScreen();
        }
        public void BindAttibuteSelectionControls()
        {
             
            AttributeSelectionFirstGenerationControl.AttributeKey = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.FirstGeneration).First().Key;
            AttributeSelectionFirstGenerationControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.FirstGeneration).First().Value;
            AttributeSelectionFirstGenerationControl.Bind();

            AttributeSelectionEthinicityControl.AttributeKey = EnumExtensions.GetKeyValue(
                   SeekerProfileAttribute.Ethnicity).First().Key;
            AttributeSelectionEthinicityControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Ethnicity).First().Value;
            AttributeSelectionEthinicityControl.Bind();


            AttributeSelectionReligionControl.AttributeKey = EnumExtensions.GetKeyValue(
                   SeekerProfileAttribute.Religion).First().Key;
            AttributeSelectionReligionControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Religion).First().Value;
            AttributeSelectionReligionControl.Bind();


            AttributeSelectionGendersControl.AttributeKey = EnumExtensions.GetKeyValue(
                  SeekerProfileAttribute.Gender).First().Key;
            AttributeSelectionGendersControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Gender).First().Value;
            AttributeSelectionGendersControl.Bind();

            AttributeSelectionGPAControl.AttributeKey = EnumExtensions.GetKeyValue(
                  SeekerProfileAttribute.GPA).First().Key;
            AttributeSelectionGPAControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.GPA ).First().Value;
            AttributeSelectionGPAControl.Bind();

            AttributeSelectionSATWritingControl.AttributeKey = EnumExtensions.GetKeyValue(
                 SeekerProfileAttribute.SATWriting).First().Key;
            AttributeSelectionSATWritingControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SATWriting ).First().Value;
            AttributeSelectionSATWritingControl.Bind();

            AttributeSelectionSATCriticalReadingControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.SATCriticalReading).First().Key;
            AttributeSelectionSATCriticalReadingControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SATCriticalReading).First().Value;
            AttributeSelectionSATCriticalReadingControl.Bind();

            AttributeSelectionSATMathematicsControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.SATMathematics).First().Key;
            AttributeSelectionSATMathematicsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SATMathematics).First().Value;
            AttributeSelectionSATMathematicsControl.Bind();

            AttributeSelectionACTEnglishControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.ACTEnglish).First().Key;
            AttributeSelectionACTEnglishControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTEnglish).First().Value;
            AttributeSelectionACTEnglishControl.Bind();

            AttributeSelectionACTMathematicsControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ACTMathematics).First().Key;
            AttributeSelectionACTMathematicsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTMathematics).First().Value;
            AttributeSelectionACTMathematicsControl.Bind();

            AttributeSelectionACTReadingControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ACTReading).First().Key;
            AttributeSelectionACTReadingControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTReading).First().Value;
            AttributeSelectionACTReadingControl.Bind();

            AttributeSelectionACTScienceControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ACTScience).First().Key;
            AttributeSelectionACTScienceControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTScience).First().Value;
            AttributeSelectionACTScienceControl.Bind();

            AttributeSelectionClassRankControl.AttributeKey = EnumExtensions.GetKeyValue(
              SeekerProfileAttribute.ClassRank).First().Key;
            AttributeSelectionClassRankControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ClassRank).First().Value;
            AttributeSelectionClassRankControl.Bind();

            AttributeSelectionHonorsControl.AttributeKey = EnumExtensions.GetKeyValue(
              SeekerProfileAttribute.Honor ).First().Key;
            AttributeSelectionHonorsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Honor ).First().Value;
            AttributeSelectionHonorsControl.Bind();

            AttributeSelectionAPCreditControl.AttributeKey = EnumExtensions.GetKeyValue(
             SeekerProfileAttribute.APCreditsEarned).First().Key;
            AttributeSelectionAPCreditControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.APCreditsEarned).First().Value;
            AttributeSelectionAPCreditControl.Bind();

            AttributeSelectionIBCreditControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.IBCreditsEarned).First().Key;
            AttributeSelectionIBCreditControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.IBCreditsEarned ).First().Value;
            AttributeSelectionIBCreditControl.Bind();

            AttributeSelectionStateControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.State).First().Key;
            AttributeSelectionStateControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.State ).First().Value;
            AttributeSelectionStateControl.Bind();

            AttributeSelectionHighSchoolControl.AttributeKey = EnumExtensions.GetKeyValue(
           SeekerProfileAttribute.HighSchool).First().Key;
            AttributeSelectionHighSchoolControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.HighSchool).First().Value;
            AttributeSelectionHighSchoolControl.Bind();

            AttributeSelectionSchoolDistrictControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.SchoolDistrict).First().Key;
            AttributeSelectionSchoolDistrictControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SchoolDistrict).First().Value;
            AttributeSelectionSchoolDistrictControl.Bind();

            AttributeSelectionCityControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.City).First().Key;
            AttributeSelectionCityControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.City).First().Value;
            AttributeSelectionCityControl.Bind();

            AttributeSelectionCountyControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.County).First().Key;
            AttributeSelectionCountyControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.County).First().Value;
            AttributeSelectionCountyControl.Bind();

            AttributeSelectionAcademicAreasControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.AcademicArea).First().Key;
            AttributeSelectionAcademicAreasControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.AcademicArea).First().Value;
            AttributeSelectionAcademicAreasControl.Bind();

            AttributeSelectionCareersControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.Career).First().Key;
            AttributeSelectionCareersControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Career).First().Value;
            AttributeSelectionCareersControl.Bind();

            AttributeSelectionOrganizationsControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.Organization).First().Key;
            AttributeSelectionOrganizationsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Organization).First().Value;
            AttributeSelectionOrganizationsControl.Bind();

            AttributeSelectionCompanyControl.AttributeKey = EnumExtensions.GetKeyValue(
              SeekerProfileAttribute.Company).First().Key;
            AttributeSelectionCompanyControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Company).First().Value;
            AttributeSelectionCompanyControl.Bind();

            AttributeSelectionHobbiesControl.AttributeKey = EnumExtensions.GetKeyValue(
             SeekerProfileAttribute.SeekerHobby ).First().Key;
            AttributeSelectionHobbiesControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SeekerHobby).First().Value;
            AttributeSelectionHobbiesControl.Bind();

            AttributeSelectionSportsControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.Sport ).First().Key;
            AttributeSelectionSportsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Sport).First().Value;
            AttributeSelectionSportsControl.Bind();

            AttributeSelectionClubsControl.AttributeKey = EnumExtensions.GetKeyValue(
           SeekerProfileAttribute.Club).First().Key;
            AttributeSelectionClubsControl .AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Club).First().Value;
            AttributeSelectionClubsControl.Bind();

            AttributeSelectionWorkTypeControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.WorkType ).First().Key;
            AttributeSelectionWorkTypeControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.WorkType).First().Value;
            AttributeSelectionWorkTypeControl.Bind();

            AttributeSelectionServiceTypeControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ServiceType).First().Key;
            AttributeSelectionServiceTypeControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ServiceType ).First().Value;
            AttributeSelectionServiceTypeControl.Bind();
        }

        public void PopulateSelectionAttributesScreen(SingleAttributeSelectionControl attributeSelectionControl)
        {
             
                   var attribute = (SeekerProfileAttribute)attributeSelectionControl.UsageButtonGroup.AttributeEnumInt;

                   var attributeUsage = ScholarshipInContext.SeekerProfileCriteria.Attributes.Find(attribute);

                   attributeSelectionControl.UsageButtonGroup.Value =
                       null == attributeUsage ?
                       ScholarshipAttributeUsageType.NotUsed :
                       attributeUsage.UsageType;
               
        }

        #region Visibilty of the controls

        public void SetAttributeSelectionState(SingleAttributeSelectionControl attributeSelectionControl)
        {
            SingleAttributeSelectionControl.AttributeUsageButtonGroup buttonGroup =
                BuildButtonGroup(attributeSelectionControl);
            var attribute = (SeekerProfileAttribute) buttonGroup.AttributeEnumInt;
            if (buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
            {
                DisableAttributeSelection(attributeSelectionControl);

            }
        }

        public void DisableAttributeSelection(SingleAttributeSelectionControl attributeSelectionControl)
        {
            // Gets the executing web page
            var page = HttpContext.Current.CurrentHandler as Page;
            var title = "set_attribute_state_" + attributeSelectionControl.ClientID;
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), title))
            {
                var script  = string.Format("disableElementsByClass('{0}')", attributeSelectionControl.ClientID);
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), title, script);
            }
             
        }

        private void SetSectionsEnabledState()
        {
            SeekerProfileCriteria criteria = ScholarshipInContext.SeekerProfileCriteria;
            FirstGenerationControl.Text = AttributeSelectionFirstGenerationControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
            HonorsControl.Text = AttributeSelectionHonorsControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
            SetAttributeSelectionState(AttributeSelectionEthinicityControl);
            SetAttributeSelectionState(AttributeSelectionReligionControl);
            SetAttributeSelectionState(AttributeSelectionGendersControl);
            SetAttributeSelectionState(AttributeSelectionGPAControl);
            SetAttributeSelectionState(AttributeSelectionSATWritingControl);
            SetAttributeSelectionState(AttributeSelectionSATCriticalReadingControl);
            SetAttributeSelectionState(AttributeSelectionSATMathematicsControl);
            SetAttributeSelectionState(AttributeSelectionACTEnglishControl);
            SetAttributeSelectionState(AttributeSelectionACTMathematicsControl);
            SetAttributeSelectionState(AttributeSelectionACTReadingControl);
            SetAttributeSelectionState(AttributeSelectionACTScienceControl);
            SetAttributeSelectionState(AttributeSelectionClassRankControl);
            SetAttributeSelectionState(AttributeSelectionHonorsControl);
            SetAttributeSelectionState(AttributeSelectionAPCreditControl);
            SetAttributeSelectionState(AttributeSelectionIBCreditControl);
            SetAttributeSelectionState(AttributeSelectionStateControl);
            SetAttributeSelectionState(AttributeSelectionHighSchoolControl);
            SetAttributeSelectionState(AttributeSelectionSchoolDistrictControl);
            SetAttributeSelectionState(AttributeSelectionCityControl);
            SetAttributeSelectionState(AttributeSelectionCountyControl);
            SetAttributeSelectionState(AttributeSelectionAcademicAreasControl);
            SetAttributeSelectionState(AttributeSelectionCareersControl);
            SetAttributeSelectionState(AttributeSelectionOrganizationsControl);
            SetAttributeSelectionState(AttributeSelectionCompanyControl);
            SetAttributeSelectionState(AttributeSelectionHobbiesControl);
            SetAttributeSelectionState(AttributeSelectionSportsControl);
            SetAttributeSelectionState(AttributeSelectionClubsControl);
            WorkTypeControl.Text = AttributeSelectionWorkTypeControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
            ServiceTypeControl.Text = AttributeSelectionServiceTypeControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
   
        }

        
        #endregion
        private void PopulateStates()
        {
            StateControl.DataSource = StateService.FindAll();
            StateControl.DataTextField = "Name";
            StateControl.DataValueField = "Abbreviation";
            StateControl.DataBind();
            StateControl.SelectedIndex = 0;
        }

        private void SetWashigtonStateDropownEvent()
        {
            var scriptname = "SetWashigtonStateDropownEvent_script";
            var clientIds = HighSchoolControlDialogButton.SelectButton.ClientID + "," +
                            SchoolDistrictControlDialogButton.SelectButton.ClientID + "," + CityControlDialogButton.SelectButton.ClientID + "," +
                            CountyControlDialogButton.SelectButton.ClientID;

            var notusedradioIds = AttributeSelectionHighSchoolControl.NotUsedRadioButton.ClientID + "," +
                           AttributeSelectionSchoolDistrictControl.NotUsedRadioButton.ClientID + "," + AttributeSelectionCityControl.NotUsedRadioButton.ClientID + "," +
                           AttributeSelectionCountyControl.NotUsedRadioButton.ClientID;
            var minimumradioIds =AttributeSelectionHighSchoolControl.MinimumRadioButton.ClientID + "," +
                          AttributeSelectionSchoolDistrictControl.MinimumRadioButton.ClientID + "," + AttributeSelectionCityControl.MinimumRadioButton.ClientID + "," +
                          AttributeSelectionCountyControl.MinimumRadioButton.ClientID;

            var preferenceradioIds =  AttributeSelectionHighSchoolControl.PreferencedRadioButton.ClientID + "," +
                         AttributeSelectionSchoolDistrictControl.PreferencedRadioButton.ClientID + "," + AttributeSelectionCityControl.PreferencedRadioButton.ClientID + "," +
                         AttributeSelectionCountyControl.PreferencedRadioButton.ClientID;

            var buddyIds = HighSchoolControl.ClientID + "," + SchoolDistrictControl.ClientID + "," +
                           CityControl.ClientID + "," + CountyControl.ClientID;

            var script = "$('#" + StateControl.ClientID + "').change(function(e) { return CheckWashingtonState('" + StateControl.ClientID + "', '" + clientIds + "','" + notusedradioIds + "','" + minimumradioIds + "' ,'" + preferenceradioIds + "','" + buddyIds + "'); });";

            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
        }

        public void SetGeographicsControlState()
        {
            var script = "$('#" + StateControl.ClientID + "').change();";
            var scriptname = "RunSetWashigtonStateDropownEvent_script";
            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
        }

        private void PopulateScreen()
        {

            SetWashigtonStateDropownEvent();      
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            PopulateSelectionAttributesScreen(AttributeSelectionFirstGenerationControl);
            PopulateSelectionAttributesScreen(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionReligionControl);
            PopulateSelectionAttributesScreen(AttributeSelectionGendersControl);
            PopulateSelectionAttributesScreen(AttributeSelectionEthinicityControl);

            PopulateSelectionAttributesScreen(AttributeSelectionGPAControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSATWritingControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSATCriticalReadingControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSATMathematicsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTEnglishControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTMathematicsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTReadingControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTScienceControl);
            PopulateSelectionAttributesScreen(AttributeSelectionClassRankControl);
            PopulateSelectionAttributesScreen(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionAPCreditControl);
            PopulateSelectionAttributesScreen(AttributeSelectionIBCreditControl);
            PopulateSelectionAttributesScreen(AttributeSelectionStateControl);

            PopulateSelectionAttributesScreen(AttributeSelectionHighSchoolControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSchoolDistrictControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCityControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCountyControl);

            PopulateSelectionAttributesScreen(AttributeSelectionAcademicAreasControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCareersControl);
            PopulateSelectionAttributesScreen(AttributeSelectionOrganizationsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCompanyControl);

            PopulateSelectionAttributesScreen(AttributeSelectionHobbiesControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSportsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionClubsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionWorkTypeControl);
            PopulateSelectionAttributesScreen(AttributeSelectionServiceTypeControl);
            
            HighSchoolControlDialogButton.Keys = seekerProfileCriteria.HighSchools.CommaSeparatedIds();
            SchoolDistrictControlDialogButton.Keys = seekerProfileCriteria.SchoolDistricts.CommaSeparatedIds();
            CityControlDialogButton.Keys = seekerProfileCriteria.Cities.CommaSeparatedIds();
            CountyControlDialogButton.Keys = seekerProfileCriteria.Counties.CommaSeparatedIds();
            FirstGenerationControl.Text = seekerProfileCriteria.FirstGeneration ? "Yes" : "";
            HonorsControl.Text = seekerProfileCriteria.Honors ? "Yes" : "";
            EthnicityControl.SelectedValues = seekerProfileCriteria.Ethnicities.Cast<ILookup>().ToList();

            ReligionControl.SelectedValues = seekerProfileCriteria.Religions.Cast<ILookup>().ToList();
            GendersControl.SelectedValues = (int)seekerProfileCriteria.Genders;
            if (seekerProfileCriteria.State != null)
                StateControl.SelectedValue = seekerProfileCriteria.State.Abbreviation;
                 
            
                SetGeographicsControlState();
            

            AcademicAreasControlDialogButton.Keys = seekerProfileCriteria.AcademicAreas.CommaSeparatedIds();
            CareersControlDialogButton.Keys = seekerProfileCriteria.Careers.CommaSeparatedIds();
            OrganizationsControlDialogButton.Keys = seekerProfileCriteria.Organizations.CommaSeparatedIds();
            CompanyControlDialogButton.Keys = seekerProfileCriteria.Companies.CommaSeparatedIds();
            SeekerHobbiesControlDialogButton.Keys = seekerProfileCriteria.Hobbies.CommaSeparatedIds();
            SportsControlDialogButton.Keys = seekerProfileCriteria.Sports.CommaSeparatedIds();
            ClubsControlDialogButton.Keys = seekerProfileCriteria.Clubs.CommaSeparatedIds();
            ClassRankControl.SelectedValues = seekerProfileCriteria.ClassRanks.Cast<ILookup>().ToList();
            PopulateGPAControls(seekerProfileCriteria);

            if (null != seekerProfileCriteria.SATScore)
            {
                SATWritingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Writing);
                SATCriticalReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.CriticalReading);
                SATMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Mathematics);
            }

            if (null != seekerProfileCriteria.ACTScore)
            {
                ACTEnglishControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.English);
                ACTMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Mathematics);
                ACTReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Reading);
                ACTScienceControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Science);
            }
            if (seekerProfileCriteria.APCreditsEarned.HasValue)
                APCreditControl.Amount = seekerProfileCriteria.APCreditsEarned.Value;
            if (seekerProfileCriteria.IBCreditsEarned.HasValue)
                IBCreditControl.Amount = seekerProfileCriteria.IBCreditsEarned.Value;
        }

        private void PopulateGPAControls(SeekerProfileCriteria seekerProfileCriteria)
        {
            /* see comment below marked as WSC-352 */
            if (null != seekerProfileCriteria.GPA)
            {
                GPAGreaterThanControl.Amount = (decimal) seekerProfileCriteria.GPA.Minimum;
                if (null == seekerProfileCriteria.GPA.Maximum)
                    GPALessThanControl.Text = string.Empty;
                else
                    GPALessThanControl.Text = seekerProfileCriteria.GPA.Maximum.Value.ToString(GPA_VALUE_FORMAT);
            }
            else
            {
                GPAGreaterThanControl.Amount = 0m;
                GPALessThanControl.Text = string.Empty;
            }
        }

        
        public override void PopulateObjects()
        {
             
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            PopulateSelectionAttributesObject(AttributeSelectionFirstGenerationControl);
            PopulateSelectionAttributesObject(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesObject(AttributeSelectionEthinicityControl);

            PopulateSelectionAttributesObject(AttributeSelectionReligionControl);
            PopulateSelectionAttributesObject(AttributeSelectionGendersControl);

            PopulateSelectionAttributesObject(AttributeSelectionGPAControl);
            PopulateSelectionAttributesObject(AttributeSelectionSATWritingControl);
            PopulateSelectionAttributesObject(AttributeSelectionSATCriticalReadingControl);
            PopulateSelectionAttributesObject(AttributeSelectionSATMathematicsControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTEnglishControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTMathematicsControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTReadingControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTScienceControl);
            PopulateSelectionAttributesObject(AttributeSelectionClassRankControl);
            PopulateSelectionAttributesObject(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesObject(AttributeSelectionAPCreditControl);
            PopulateSelectionAttributesObject(AttributeSelectionIBCreditControl);
            PopulateSelectionAttributesObject(AttributeSelectionStateControl);

            PopulateSelectionAttributesObject(AttributeSelectionAcademicAreasControl);
            PopulateSelectionAttributesObject(AttributeSelectionCareersControl);
            PopulateSelectionAttributesObject(AttributeSelectionOrganizationsControl);
            PopulateSelectionAttributesObject(AttributeSelectionCompanyControl);

            PopulateSelectionAttributesObject(AttributeSelectionHobbiesControl);
            PopulateSelectionAttributesObject(AttributeSelectionSportsControl);
            PopulateSelectionAttributesObject(AttributeSelectionClubsControl);
            PopulateSelectionAttributesObject(AttributeSelectionWorkTypeControl);
            PopulateSelectionAttributesObject(AttributeSelectionServiceTypeControl);


            if (StateControl.SelectedValue != WashingtonState)
            {
                AttributeSelectionHighSchoolControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
                AttributeSelectionSchoolDistrictControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
                AttributeSelectionCityControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
                AttributeSelectionCountyControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
            }

            PopulateSelectionAttributesObject(AttributeSelectionHighSchoolControl);
            PopulateSelectionAttributesObject(AttributeSelectionSchoolDistrictControl);
            PopulateSelectionAttributesObject(AttributeSelectionCityControl);
            PopulateSelectionAttributesObject(AttributeSelectionCountyControl);
            
            seekerProfileCriteria.FirstGeneration = AttributeSelectionFirstGenerationControl.UsageButtonGroup.Value != ScholarshipAttributeUsageType.NotUsed;
            seekerProfileCriteria.Honors = AttributeSelectionHonorsControl.UsageButtonGroup.Value != ScholarshipAttributeUsageType.NotUsed;
            PopulateList(AttributeSelectionEthinicityControl, EthnicityControl, seekerProfileCriteria.Ethnicities);
            PopulateList(AttributeSelectionReligionControl, ReligionControl, seekerProfileCriteria.Religions);
            seekerProfileCriteria.Genders = GendersContainerControl.Visible
                                              ? (Genders) GendersControl.SelectedValues
                                              : 0;
            var state = StateService.FindByAbbreviation(StateControl.SelectedValue);
            seekerProfileCriteria.State  = state;
            


            if (StateControl.SelectedValue == WashingtonState)
            {
                PopulateList(AttributeSelectionHighSchoolControl, HighSchoolControlDialogButton,
                             seekerProfileCriteria.HighSchools);

                PopulateList(AttributeSelectionSchoolDistrictControl, SchoolDistrictControlDialogButton,
                             seekerProfileCriteria.SchoolDistricts);
                PopulateList(AttributeSelectionCityControl, CityControlDialogButton, seekerProfileCriteria.Cities);
                PopulateList(AttributeSelectionCountyControl, CountyControlDialogButton, seekerProfileCriteria.Counties);
            } else
            {
                seekerProfileCriteria.HighSchools.Clear();
                seekerProfileCriteria.SchoolDistricts.Clear();
                seekerProfileCriteria.Cities.Clear();
                seekerProfileCriteria.Counties.Clear(); 
            }

            PopulateList(AttributeSelectionAcademicAreasControl, AcademicAreasControlDialogButton, seekerProfileCriteria.AcademicAreas);
            PopulateList(AttributeSelectionCareersControl, CareersControlDialogButton, seekerProfileCriteria.Careers);

            PopulateList(AttributeSelectionOrganizationsControl, OrganizationsControlDialogButton, seekerProfileCriteria.Organizations);
            PopulateList(AttributeSelectionCompanyControl, CompanyControlDialogButton, seekerProfileCriteria.Companies);
            PopulateList(AttributeSelectionHobbiesControl, SeekerHobbiesControlDialogButton, seekerProfileCriteria.Hobbies);
            PopulateList(AttributeSelectionSportsControl, SportsControlDialogButton, seekerProfileCriteria.Sports);
            PopulateList(AttributeSelectionClubsControl, ClubsControlDialogButton, seekerProfileCriteria.Clubs);

            PopulateList(AttributeSelectionClassRankControl, ClassRankControl, seekerProfileCriteria.ClassRanks);
            PopulateGPAObjects();
             
                seekerProfileCriteria.SATScore = new SatScore();
                seekerProfileCriteria.SATScore.Writing = SATWritingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.CriticalReading = SATCriticalReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.Mathematics = SATMathematicsControl.CreateIntegerRangeCondition();
             
                seekerProfileCriteria.ACTScore = new ActScore();
                seekerProfileCriteria.ACTScore.English = ACTEnglishControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Mathematics = ACTMathematicsControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Reading = ACTReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Science = ACTScienceControl.CreateIntegerRangeCondition();
             
            if (string.IsNullOrEmpty(APCreditControl.Text))
                seekerProfileCriteria.APCreditsEarned = null;
            else
                seekerProfileCriteria.APCreditsEarned = Convert.ToInt32(APCreditControl.Amount);

            if (string.IsNullOrEmpty(IBCreditControl.Text))
                seekerProfileCriteria.IBCreditsEarned = null;
            else
                seekerProfileCriteria.IBCreditsEarned = Convert.ToInt32(IBCreditControl.Amount);

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage <ScholarshipStages.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStages.NotActivated;
        }


        private void PopulateSelectionAttributesObject(SingleAttributeSelectionControl attributeSelectionControl)
        {
              SingleAttributeSelectionControl.AttributeUsageButtonGroup buttonGroup = BuildButtonGroup(attributeSelectionControl);
                var attribute = (SeekerProfileAttribute)buttonGroup.AttributeEnumInt;
                if (!buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
                {
                    var attributeUsage = new SeekerProfileAttributeUsage
                    {
                        Attribute = attribute,
                        UsageType = buttonGroup.Value
                    };
                    ScholarshipInContext.SeekerProfileCriteria.Attributes.Add(attributeUsage);
                }
                else
                {
                    ScholarshipInContext.SeekerProfileCriteria.Attributes.Remove(attribute);
                }
             
        }
        private void PopulateGPAObjects()
        {
            var minimum = (double)GPAGreaterThanControl.Amount;
            var maximum = GPALessThanControl.Text.CreateNullableDouble();

            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            seekerProfileCriteria.GPA = new RangeCondition<double?>(minimum, maximum);
        }

        private static SingleAttributeSelectionControl.AttributeUsageButtonGroup BuildButtonGroup(SingleAttributeSelectionControl attributeSelectionControl)
        {
            var usageMinimumRB =(RadioButton)attributeSelectionControl.FindControl("Minimum");
            var usagePreferenceRB = (RadioButton)attributeSelectionControl.FindControl("Preference");
            var usageNotUsedRB = (RadioButton)attributeSelectionControl.FindControl("Minimum");
            var attributeControl = (HtmlInputHidden)attributeSelectionControl.FindControl("KeyControl");
            return new SingleAttributeSelectionControl.AttributeUsageButtonGroup(Int32.Parse(attributeControl.Value), usageMinimumRB, usagePreferenceRB, usageNotUsedRB);
        }

        private static void PopulateList<T>(SingleAttributeSelectionControl attributeControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (attributeControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed)
                list.Clear();
            else
                checkboxList.PopulateListFromSelectedValues(list);
        }

        private static void PopulateList<T>(SingleAttributeSelectionControl attributeControl, LookupDialog lookupDialog, IList<T> list)
        {
           if (attributeControl.UsageButtonGroup.Value==ScholarshipAttributeUsageType.NotUsed )
                list.Clear();
            else
 
               lookupDialog.PopulateListFromSelection(list);
            
        }

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool ValidateStep()
        {
             bool result = true;
            string noneSelected = "None Selected";
             var buttonGroup = BuildButtonGroup(AttributeSelectionEthinicityControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (EthnicityControl.SelectedValues.Count == 0)
                 {
                     EthinicityValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionReligionControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (ReligionControl.SelectedValues.Count == 0)
                 {
                     ReligionValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionGendersControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (GendersControl.SelectedValues == 0)
                 {
                     GendersValidator.IsValid = false;
                     result &= false;
                 }
             }
             buttonGroup = BuildButtonGroup(AttributeSelectionHighSchoolControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (HighSchoolControl.Text ==noneSelected)
                 {
                     HighSchoolValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionSchoolDistrictControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (SchoolDistrictControl.Text == noneSelected)
                 {
                     SchoolDistrictValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionCityControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (CityControl.Text == noneSelected)
                 {
                     CityValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionCountyControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (CountyControl.Text == noneSelected)
                 {
                     CountyValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionAcademicAreasControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (AcademicAreasControl.Text == noneSelected)
                 {
                     AcademicAreasValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionCareersControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (CareersControl.Text == noneSelected)
                 {
                     CareersValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionOrganizationsControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (OrganizationsControl.Text == noneSelected)
                 {
                     OrganizationsValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionCompanyControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (CompanyControl.Text == noneSelected)
                 {
                     CompanyValidator.IsValid = false;
                     result &= false;
                 }
             }


             buttonGroup = BuildButtonGroup(AttributeSelectionHobbiesControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (SeekerHobbiesControl.Text == noneSelected)
                 {
                     SeekerHobbiesValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionSportsControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (SportsControl.Text == noneSelected)
                 {
                     SportsValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionClubsControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (ClubsControl.Text == noneSelected)
                 {
                     ClubsValidator.IsValid = false;
                     result &= false;
                 }
             }

             buttonGroup = BuildButtonGroup(AttributeSelectionClassRankControl);
             if (buttonGroup.Value != ScholarshipAttributeUsageType.NotUsed)
             {
                 if (ClassRankControl.SelectedValues.Count==0)
                 {
                     ClassRankValidator.IsValid = false;
                     result &= false;
                 }
             }

            return result;
        }

        #endregion

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int) WizardStepName.MatchCriteria);
        }
    }
}

/* In ref: WSC-352 and .aspx of GPA 
    Per WSC-352, GPA Less than Field is expected to be null and number control that we use do not support blank (null)
    value. So, for that, GPA is not implimented as like other fields. JavaScript code for third party control
    has been modified to support this situation.
*/