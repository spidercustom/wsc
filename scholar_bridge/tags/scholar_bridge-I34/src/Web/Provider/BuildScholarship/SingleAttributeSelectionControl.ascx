﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SingleAttributeSelectionControl.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.SingleAttributeSelectionControl" %>

<div >  

   <table width="225px">
   <thead>
       <tr  id="headerrow" runat="server">
          <th   style="text-align:center;" >Required Minimum</th>
          <th  style="text-align:center;"  >Given Preference</th>
          <th  style="text-align:center;" >Not Used</th>
        </tr>
    </thead>        
    <tbody>
         <tr>
            <td  align="center" style="width:80px;" >
            <asp:RadioButton  ID="Minimum" runat="server"  GroupName="UsageSelector" Width="80px"  CausesValidation="false" />
            </td>
            <td  align="center"  style="width:85px;" >
               <asp:RadioButton ID="Preference" runat="server"   GroupName="UsageSelector"  Width="85px"   CausesValidation="false" />
                <asp:Literal Visible="false" ID="attributeName" runat="server" />
                <input type="hidden" runat="server" id="KeyControl" style="display:none;" />
            </td>
            <td  align="center"  style="width:60px; >
               <asp:RadioButton ID="NotUsed" runat="server"   GroupName="UsageSelector"  Width="60px"  CausesValidation="false" />
               
            </td>
         </tr>      
  </tbody> 
      </table> 
</div>

 

