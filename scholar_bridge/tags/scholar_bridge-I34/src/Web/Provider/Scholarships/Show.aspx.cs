﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Show : Page
    {
        private const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship ScholarshipToView { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params[SCHOLARSHIP_ID], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Business.Exceptions.ProviderNotApprovedException();

            ScholarshipToView = ScholarshipService.GetById(ScholarshipId);
            if (ScholarshipToView != null)
            {
				//if we're in printview mode then we'll let the provider see it even if they don't own it
				if (!Page.IsInPrintView())
					if (!ScholarshipToView.Provider.Id.Equals(provider.Id) )
						throw new InvalidOperationException("Scholarship does not belong to provider in context");

                showScholarship.Scholarship = ScholarshipToView;

                deleteConfirmBtn.Visible = ScholarshipToView.CanEdit();
                linkCopy.NavigationUrl = "~/Provider/BuildScholarship/Default.aspx?copyfrom=" + ScholarshipToView.Id;
                ScholarshipTitleStripeControl.UpdateView(ScholarshipToView);
            }
            sendToFriendBtn.NavigationUrl = ResolveUrl("~/SendToFriend.aspx?popup=true&id={0}".Build(ScholarshipId));
        }
        
        protected void deleteConfirmBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (e.DialogResult)
            {
                ScholarshipToView.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                ScholarshipService.Delete(ScholarshipToView);
                SuccessMessageLabel.SetMessage("Scholarship was deleted.");
                Response.Redirect("~/Provider/Scholarships/Default.aspx");  

            }
        }
        
    }
}
