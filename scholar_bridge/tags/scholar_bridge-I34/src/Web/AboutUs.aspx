﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="ScholarBridge.Web.AboutUs" Title="About Us" %>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />

<h2>About Us</h2>
    <p class="HighlightedTextMain">
        <b>Washington Scholarship Coalition &mdash; Making college possible, one person at a time.</b><br />
        We are a partnership of public and nonprofit agencies coming together to build an online 
        scholarship marketplace and provide a trusted source of scholarships. The 
        Washington Scholarship Coalition exists to create connections and ensure 
        scholarship funds are reaching those in need.</p>
<div class="subsection">
    <h3 style="margin:30px 0px 10px 0px;">Our Goals</h3>
    <ul>
        <li style="margin-top:.75em;">Increase access to scholarships for Washington students, with emphasis around low-income and underserved students</li>
        <li style="margin-top:.75em;">Increase awareness of financial aid &amp; scholarship opportunities</li>
        <li style="margin-top:.75em;">Increase private scholarship funding by providing donors with information on effective, efficient ways to contribute</li>
        <li style="margin-top:.75em;">Improve efficiencies in private scholarship application process</li>
        <li style="margin-top:.75em;">Improve availability of data on private scholarships for scholarship-seeking students</li>
    </ul>  
    
    <h3 style="margin:30px 0px 10px 0px;">Steering Committee</h3>
    <div id="CoalitionPartnersLogoList">
            <a href="http://www.hecb.wa.gov/"><img style="width:200px; margin:0px 30px;" alt="Washington Higher Education Coordinating Board" src="images/PartnerLogos/HECB+LgLogo.jpg" /></a>
            <a href="http://www.collegespark.org/"><img style="width:200px; margin:0px 30px;" alt="College Spark Washington" src="images/PartnerLogos/CS_logo_Tag_color.png" /></a><br /><br /><br />
            <a href="http://www.nela.net/"><img style="width:200px; margin:0px 30px;" alt="Nela" src="images/PartnerLogos/nela_logo.jpg" /></a>
            <a href="http://www.seattlefoundation.org/"><img style="width:200px; margin:0px 30px;" alt="Seattle Foundation" src="images/PartnerLogos/TSF_1line_blue_tag.jpg" /></a>
    </div>

    <h3 style="margin:30px 0px 10px 0px;">Members and Partners</h3>
    <a href="http://www.tacomafoundation.org/"><img alt="The Greater Tacoma Community Foundation" src="images/PartnerLogos/the_greater_tacoma_comm_found.gif" /></a><br />
    <a href="http://www.seattlefoundation.org/"><img alt="Seattle Foundation" src="images/PartnerLogos/independent_colleges_of_washington.gif" /></a>
    <ul>
        <li style="margin-top:.75em;">
            <a class="GreenLink" href="http://www.collegeplan.org/">College Planning Network</a>
        </li>
        <li style="margin-top:.75em;">
            <a class="GreenLink" href="http://www.collegesuccessfoundation.org/">The College Success Foundation</a> 
        </li>
    </ul>

    <h3 style="margin:30px 0px 10px 0px;">Contact the Washington Scholarship Coalition c/o</h3>
    <p>
        Northwest Education Loan Association<br />
        Danette Knudson, Director, External Relations<br />
        (206) 461-5491<br />
        danette.knudson@nela.net
    </p>
</div>
</asp:Content>