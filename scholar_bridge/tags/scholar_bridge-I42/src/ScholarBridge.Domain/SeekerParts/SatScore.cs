namespace ScholarBridge.Domain.SeekerParts
{
    public class SatScore
    {
        public virtual int? Writing { get; set; }
        public virtual int? CriticalReading { get; set; }
        public virtual int? Mathematics { get; set; }
        public virtual int? Commulative { get; set; }
    }
}