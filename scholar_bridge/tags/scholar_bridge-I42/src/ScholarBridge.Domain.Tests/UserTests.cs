using Microsoft.Practices.EnterpriseLibrary.Validation;
using NUnit.Framework;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class UserTests
    {
        [Test]
        public void can_check_for_roles()
        {
            var u = new User();
            u.Roles.Add(new Role {Name = "foo"});
            u.Roles.Add(new Role {Name = "bar"});

            Assert.IsTrue(u.IsInRole("foo"));
            Assert.IsTrue(u.IsInRole("bar"));

            Assert.IsFalse(u.IsInRole("baz"));
        }

        [Test]
        public void can_validate_valid_user()
        {
            var u = CreateValidUser();
        	ValidationResults results = Validation.Validate(u);
            Assert.IsTrue(Validation.Validate(u).IsValid);
        }

		[Test]
		public void can_validate_valid_passwords()
		{
			var u = CreateValidUser();
			var valid = new[] { "abcde123A", "abcD123*", "AAAAb1$DD", "AAAc1DDD", "AAAAc^FF", "aaaa*BBB", "ScholarBridge1", "ScholarBridge*2" };
			foreach (var v in valid)
			{
				u.Password = v;
				Assert.IsTrue(Validation.Validate(u).IsValid, v + " is not valid");
			}
		}

		[Test]
		public void can_validate_valid_userids()
		{
			var u = CreateValidUser();
			Assert.IsTrue(Validation.Validate(u).IsValid, u.Username + " is not valid");
			var valid = new[] { "a@b.com", "\"abc\"@xyz.org", "!#$%&'*+/=?^`{|}~@blah.edu" };
			foreach (var v in valid)
			{
				u.Username = u.Email = v;
				Assert.IsTrue(Validation.Validate(u).IsValid, v + " is not valid");
			}
		}

        [Test]
        public void can_validate_invalid_passwords()
        {
            var u = CreateValidUser();
            var valid = new[] {"abcde123", "abcd", "AAAA1$DD", "AAAcDDDd", "", null};
            foreach (var v in valid)
            {
                u.Password = v;
                Assert.IsFalse(Validation.Validate(u).IsValid, v + " is valid and was not supposed to be");
            }
        }

        public static User CreateValidUser()
        {
            return new User
                       {
                           Question = "123",
                           Answer = "47",
                           Username = "testuser@example.com",
                           Email = "test@example.com",
						   EmailWaitingforVerification = "test@example.com",
                           Password = "abcd1D3*"
                       };
        }
    }
}