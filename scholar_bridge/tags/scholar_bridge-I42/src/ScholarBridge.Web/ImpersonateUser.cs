﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using ScholarBridge.Domain.Auth;
using System.Web.SessionState;

namespace ScholarBridge.Web
{
	/// <summary>
	/// Change the logged in user to another user and add the current user to the Authentication Ticket so that 
	/// the impersonation can be reversed.
	/// </summary>
	public class ImpersonateUser
	{
        public const string IMPERSONATION_COOKIE_NAME = "prevImpNameList";
        public const string IMPERSONATING_USER_SESSION_VARIABLE_NAME = "ImpersonatingUser";

		/// <summary>
		/// Impersonate a given user name.  
		/// Note that the current user is tacked on to the name in the Authentication Ticket with a "!" separator.
		/// </summary>
		/// <param name="currentUserName">currently logged in user (s/b wscadmin@scholarbridge.com</param>
		/// <param name="impersonatedUserName"></param>
		/// <param name="page">reference to the current page</param>
        /// <param name="session">reference to the current session</param>
        /// <param name="redirURL">page to which the impersonated user s/b directed to</param>
        public static void Impersonate(string currentUserName, string impersonatedUserName, Page page, HttpSessionState session, string redirURL)
        {
            AddUserToPreviouslyImpersonatedUsers(impersonatedUserName, page);

            FormsAuthentication.SignOut();

            FormsAuthentication.SetAuthCookie(impersonatedUserName + '!' + currentUserName, false);
            AbandonSessionAndRedirect(redirURL, page, session);
        }

        public static void Impersonate(string currentUserName, string impersonatedUserName, Page page, HttpSessionState session)
        {
            string redirUrl = FormsAuthentication.GetRedirectUrl(impersonatedUserName + '!' + currentUserName, false) +
                              page.Request.PathInfo;
		    Impersonate(currentUserName, impersonatedUserName, page, session, redirUrl);
        }

        private static void AbandonSessionAndRedirect(string path, Page page, HttpSessionState session)
		{
			session.Abandon();
			page.Response.Redirect(path, true);
		}

        public static bool IsImpersonationInProgress(HttpSessionState session)
        {
            return session[IMPERSONATING_USER_SESSION_VARIABLE_NAME] != null;  
        }
		private static void AddUserToPreviouslyImpersonatedUsers(string newUser, Page page)
		{
			HttpCookie currentCookie = page.Request.Cookies[IMPERSONATION_COOKIE_NAME];
			HttpCookie updatedCookie = new HttpCookie(IMPERSONATION_COOKIE_NAME);

			updatedCookie.Expires = DateTime.Now.AddMonths(1);
			updatedCookie.Values.Add(null, newUser);

			if (!(currentCookie == null))
			{
				foreach (string u in currentCookie.Values.GetValues(null))
				{
					if (u != null)
						if (u != newUser)
							updatedCookie.Values.Add(null, u);
					if (updatedCookie.Values.Count > 7)
						break;
				}
			}

			page.Response.Cookies.Add(updatedCookie);
		}

	}
}
