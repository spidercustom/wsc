﻿using System.Web;
using ScholarBridge.Web.Extensions;
using System.Web.UI;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web
{
    /// <summary>
    /// contains helper functions for pop up window functionality
    /// </summary>
    public static class PopupHelper
    {
        private const string CLOSE_SELF_SCRIPT_TEMPLATE = "ClosePopupWindow('{0}');";
        private const string REFRESH_PARENT_SCRIPT_TEMPLATE = "RefreshParentWindow('{0}');";
        private const string OPEN_URL_SCRIPT_TEMPLATE = "window.open('{0}', '', 'width=800,height=600').focus();";
        public static void CloseSelf(bool refreshParent )
        {
            var scriptname = "closeself_refresh_parent";
            var script = CLOSE_SELF_SCRIPT_TEMPLATE.Build(refreshParent);

            var page = HttpContext.Current.CurrentHandler as Page;
  
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
           
        }

        

        public static void RefreshParent()
        {
            RefreshParent("");

        }

        public static void RefreshParent(string newUrl)
        {
            var scriptname = "refresh_parent_window";
            var script = REFRESH_PARENT_SCRIPT_TEMPLATE.Build(newUrl);

            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);

        }


        public static void OpenURL(string url)
        {
            var scriptname = "open_url_window";
            var script = OPEN_URL_SCRIPT_TEMPLATE.Build(url);

            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);

        }

    }
}