using System;
using System.Collections.Generic;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Messaging
{
    public class TemplateParametersService : ITemplateParametersService
    {
        private const string RESET = "reset=true";
        public ILinkGenerator LinkGenerator { get; set; }

        public void ConfirmationLink(User user, bool requireResetPassword, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()},
                               {"Link", BuildConfirmationLinkForEmail(user.Email, requireResetPassword)}
                               ,{"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };

            templateParams.MergeVariablesInto(dict);
        }
        
        public void EmailAddressChangeVerificationLink(User user, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()},
                               {"Link", BuildVerificationLinkForEmail(user.EmailWaitingforVerification)}
                           };

            templateParams.MergeVariablesInto(dict);
        }
        
        public void ListChangeRequest(Organization organization, User user, string listType,string value, string reason,MailTemplateParams templateParams)
        {
            string from=string.Format("{0}({1}) - {2}", (user.Name == null ? "" : user.Name.NameFirstLast),organization.Name,user.Email );
            
            var dict = new Dictionary<string, string>
                           {
                               {"From", from },
                               {"ListType", listType},
                               {"Value",value},
                               {"Reason",reason}
                           };

            templateParams.MergeVariablesInto(dict);
        }
        public void RequestOnlineApplicationApproval(Scholarship scholarship, Organization organization, MailTemplateParams templateParams)
        {
             var dict = new Dictionary<string, string>
                           {
                               {"ScholarshipName", scholarship.Name },
                               {"OrganizationName", organization.Name},
                               {"AdminName",organization.AdminUser.Name.NameFirstLast},
                               {"Email",organization.AdminUser.Email},
                               {"Phone",organization.Phone==null ? "" :organization.Phone.FormattedPhoneNumber},
                               {"OnlineApplicationUrl", scholarship.OnlineApplicationUrl}

                           };
             

            templateParams.MergeVariablesInto(dict);

        }

        public void OrganizationApproved(User user, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()}
                               ,{"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void OrganizationRejected(User user, MailTemplateParams templateParams)
        {
            OrganizationApproved(user, templateParams);
        }

        public void RequestOrganizationApproval(User user, Organization organization, MailTemplateParams templateParams)
        {

			string link = LinkGenerator.GetFullLink("Message/Default.aspx");

            var dict = new Dictionary<string, string>
                           {
                               {"FirstName", user.Name.FirstName},
                               {"MiddleName", user.Name.MiddleName},
                               {"LastName", user.Name.LastName},
                               {"Email", user.Email},
                               {"OrganizationName", organization.Name},
                               {"OrganizationTax", organization.TaxId},
                               {"OrganizationWebsite", organization.Website},
                               {"AddressStreet", (organization.Address == null) ? "" : organization.Address.Street},
                               {"AddressStreet2", (organization.Address == null) ? "" : organization.Address.Street2},
                               {"City", (organization.Address == null) ? "" : organization.Address.City},
                               {"State", (organization.Address == null) ? "" : organization.Address.State.Name},
                               {"PostalCode", (organization.Address == null) ? "" : organization.Address.PostalCode},
                               {"Phone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Fax", (organization.Fax == null) ? "" : organization.Fax.FormattedPhoneNumber},
                               {"OtherPhone", (organization.OtherPhone == null) ? "" : organization.OtherPhone.FormattedPhoneNumber},
                               {"Link", link}
                               ,{"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void ScholarshipApproved(Scholarship scholarship, User user, MailTemplateParams templateParams)
        {
            var org = user.Organizations[0];
            
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", org==null ? "" : org.Name},
                               {"ScholarshipName", scholarship.DisplayName},
                               {"ApproverName", user.Name.NameFirstLast}
                               ,{"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };
            templateParams.MergeVariablesInto(dict);
        }
        public void ScholarshipOnlineApplicationUrlApproved(Scholarship scholarship, User user, MailTemplateParams templateParams)
        {
            var org = user.Organizations[0];

            var dict = new Dictionary<string, string>
                           {
                               {"Name", org==null ? "" : org.Name},
                               {"URL", scholarship.OnlineApplicationUrl},
                               {"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };
            templateParams.MergeVariablesInto(dict);
        }
        public void ScholarshipOnlineApplicationUrlRejected(Scholarship scholarship, User user, MailTemplateParams templateParams)
        {
           
            var org = user.Organizations[0];

            var dict = new Dictionary<string, string>
                           {
                                {"Name", org==null ? "" : org.Name},
                               {"URL", scholarship.OnlineApplicationUrl},
                               {"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void ScholarshipClosed(Scholarship scholarship, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", scholarship.Provider.Name},
                               {"ScholarshipName", scholarship.DisplayName}
                           };
            templateParams.MergeVariablesInto(dict);
        }
        
        public void RelationshipRequestCreated(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                               ,{"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void RelationshipInActivated(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                                ,{"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
            
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public string BuildConfirmationLinkForEmail(string email, bool requiredToSetPassword)
        {
            var linkData = email;
            if (requiredToSetPassword)
            {
                linkData += "&" + RESET;
            }
            var queryString = String.Format("key={0}", LinkGenerator.UrlEncode(CryptoHelper.Encrypt(linkData)));
            return LinkGenerator.GetFullLink("ConfirmEmail.aspx", queryString);
        }
        public string BuildVerificationLinkForEmail(string email)
        {
            var linkData = email;
            
            var queryString = String.Format("key={0}", LinkGenerator.UrlEncode(CryptoHelper.Encrypt(linkData)));
            return LinkGenerator.GetFullLink("VerifyUserEmailChange.aspx", queryString);
        }
        public void RelationshipRequestApproved(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                                ,{"LogoImage", LinkGenerator.GetFullLink("images/LogoTheWashBoard.gif")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void RelationshipRequestRejected(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void AwardScholarship(Application application, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"ScholarshipName", application.Scholarship.DisplayName},
                               {"Link", LinkGenerator.GetFullLink("Seeker/Matches/Show.aspx", "id=" + application.Scholarship.Id)}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void ApplicationSubmisionSuccess(Application application, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"ScholarshipName", application.Scholarship.DisplayName},
                               {"SubmittedDate", application.SubmittedDate.ToString()}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void ApplicationAcknowledgement(Application application, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"ScholarshipName", application.Scholarship.DisplayName},
                               {"OrganizationName", application.Scholarship.Provider.Name},
                               {"NotificationDate", application.Scholarship.ApplicationDueDate.ToString() }
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void OfferScholarship(Application application, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"ScholarshipName", application.Scholarship.DisplayName},
                               {"Link", LinkGenerator.GetFullLink("Seeker/Matches/Show.aspx", "id=" + application.Scholarship.Id)}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void ApplicationDue(Scholarship scholarship, int days, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"ScholarshipName", scholarship.DisplayName},
                               {"InDays", days.ToString()},
                               {"DueDate", scholarship.ApplicationDueDate.Value.ToString("MM/dd/yyyy")},
                               {"Link", LinkGenerator.GetFullLink("Seeker/Scholarships/")}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void ScholarshipSendToFriend(Scholarship scholarship,string userComments, MailTemplateParams templateParams,string domainName,string senderEmail)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"DomainName", domainName},
                               {"SenderEmail", senderEmail},
                              {"UserComments", userComments},
                              {"ScholarshipName", scholarship.Name},
                              {"ProviderName", (scholarship.Provider==null ? "" :  scholarship.Provider.Name)},
                              {"IntermediaryName", (scholarship.Intermediary==null ? "" :  scholarship.Intermediary.Name)},
                              {"DonorName", (scholarship.Donor==null ? "" :  scholarship.Donor.Name)},
                              {"MissionStatement", (scholarship.MissionStatement)},
                              {"LoginLink", LinkGenerator.GetFullLink("Seeker/")}
                           };
            templateParams.MergeVariablesInto(dict);
        }

		public void ContactUs(string emailAddress, User user, string messageText, MailTemplateParams templateParams)
		{
			var dict = new Dictionary<string, string>
			           	{
			           		{"SenderEmail", emailAddress},
			           		{"SenderName", user == null ? "Anonymous User" : user.Name.NameFirstLast},
			           		{"MessageText", messageText}
			           	};
			templateParams.MergeVariablesInto(dict);
		}

        public void RegistrationInterest(string email, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"InterestedEmailAddress", email}
                           };

            templateParams.MergeVariablesInto(dict);
        }

    }
}
