using System.Collections.Generic;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IMatchDAL : IDAL<Match>
    {
        IList<Match> FindAll(Application application);
        IList<Match> FindAll(Seeker seeker);
        IList<Match> FindAll(Seeker seeker, MatchStatus status);
        IList<Match> FindAll(Seeker seeker, MatchStatus[] status);
        IList<Match> FindAll(Scholarship scholarship, MatchStatus[] status);
        IList<Match> FindAllWithApplications(Scholarship scholarship, MatchStatus[] statuses);
		IList<Match> FindAllCurrentMatches(Seeker seeker, bool excludeMatchesWithApplications);
		IList<Match> FindAllCurrentMatches(Seeker seeker);

        Match Find(Seeker seeker, int scholarshipId);

        void UpdateMatches(Seeker seeker);
        void UpdateMatches(Scholarship newScholarship);
    }
}