using NHibernate.Type;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>SeekerType</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class ApplicationPartsApplicationType : EnumStringType
    {
        public ApplicationPartsApplicationType()
            : base(typeof(ScholarBridge.Domain.ApplicationParts.ApplicationType), 255)
        {
        }
    }
}