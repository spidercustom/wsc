﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(55)]
    public class AddScholarshipTermOfSupport : Migration
    {
        public const string TABLE_NAME = "SBScholarshipTermOfSupportRT";

        protected static readonly string[] COLUMNS = new[] { "SBScholarshipId", "SBTermOfSupportIndex" };
        protected static readonly string FK_SCHOLARSHIP = string.Format("FK_{0}_SBScholarship", TABLE_NAME);
        protected static readonly string FK_SUPPORT = string.Format("FK_{0}_SBTermOfSupport", TABLE_NAME);


        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKey),
                              new Column(COLUMNS[1], DbType.Int32, ColumnProperty.PrimaryKey)
                );

            Database.AddForeignKey(FK_SCHOLARSHIP, TABLE_NAME, COLUMNS[0], "SBScholarship",  COLUMNS[0]);
            Database.AddForeignKey(FK_SUPPORT, TABLE_NAME, COLUMNS[1], "SBTermOfSupportLUT", COLUMNS[1]);
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_SCHOLARSHIP);
            Database.RemoveForeignKey(TABLE_NAME, FK_SUPPORT);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}