﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(51)]
    public class SeekerSkill : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "SeekerSkill";
        
        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
