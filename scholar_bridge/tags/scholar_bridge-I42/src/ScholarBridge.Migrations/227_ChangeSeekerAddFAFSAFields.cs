﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(227)]
    public class ChangeSeekerAddExpectedFamilyContribution : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, "HasFAFSACompleted", DbType.Boolean);
            Database.AddColumn(TABLE_NAME, "ExpectedFamilyContribution", DbType.Decimal);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "HasFAFSACompleted");
            Database.RemoveColumn(TABLE_NAME, "ExpectedFamilyContribution");
        }
    }
}