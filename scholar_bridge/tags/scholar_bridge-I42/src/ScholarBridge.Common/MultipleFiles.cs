using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;

namespace ScholarBridge.Common
{
    public class MultipleFiles
    {
        private readonly List<ZipFileEntry> entries = new List<ZipFileEntry>();

        public void AddFile(string zipPath, string filePath)
        {
            entries.Add(new ZipFileEntry(zipPath, filePath));
        }

        public string CreateZip(string zipFile)
        {
            ZipFile zip = null;
            try
            {
                zip = ZipFile.Create(zipFile);
                zip.BeginUpdate();

                foreach (var entry in entries)
                {
                    zip.Add(entry.FileSystemPath, entry.ZipPath);
                }

                zip.CommitUpdate();
            }
            finally
            {
                if (null != zip) zip.Close();
            }

            return zipFile;
        }


        class ZipFileEntry
        {
            public ZipFileEntry(string zipPath, string fsPath)
            {
                ZipPath = zipPath;
                FileSystemPath = fsPath;
            }

            public string ZipPath { get; protected set; }
            public string FileSystemPath { get; protected set; }
        }
    }
}