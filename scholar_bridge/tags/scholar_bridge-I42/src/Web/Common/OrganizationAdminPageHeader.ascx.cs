﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{

    public partial class OrganizationAdminPageHeader : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var linkGenerator = new LinkGenerator();
            supportlink.HRef = linkGenerator.GetFullLink("/contactus.aspx");
        }

    }
}