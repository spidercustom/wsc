﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Intermediary.Admin
{
	public partial class Default : System.Web.UI.Page
	{
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        private LinkGenerator linkGenerator;
        private LinkGenerator LinkGenerator
        {
            get
            {
                if (linkGenerator == null)
                    linkGenerator = new LinkGenerator();
                return linkGenerator;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = UserContext.CurrentIntermediary;
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.IntermediaryAdmin); 
            editOrg.Organization = intermediary;

            relationshiplist.Relationships = RelationshipService.GetByIntermediary(intermediary);
            orgUserList.Users = intermediary.Users;
            adminDetails.UserToView = intermediary.AdminUser;
            adminDetails1.UserToView = intermediary.AdminUser;
            createUserLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", LinkGenerator.GetFullLink("Intermediary/Users/Create.aspx?popup=true"));
            addRequestLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", LinkGenerator.GetFullLink("Intermediary/Relationships/Create.aspx?popup=true"));
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!User.IsInRole(Role.INTERMEDIARY_ADMIN_ROLE))
            {
                createUserLink.Visible = false;
                editOrg.ViewOnlyMode = true;
            }
        }
        protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            IntermediaryService.Update((Domain.Intermediary)org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Intermediary/Admin");
        }
	}
}
