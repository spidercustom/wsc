﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Web;

namespace ScholarBridge.Web.Admin
{
	public partial class ScholarshipSearch : System.Web.UI.UserControl
	{
		public delegate void ScholarshipSaved(Scholarship scholarship);
		public event ScholarshipSaved ScholarshipSavedEvent;

		#region Properties
		public IScholarshipService ScholarshipService { get; set; }
		public IUserContext UserContext { get; set; }

		private int? currentScholarshipId
		{
			get
			{
				if (ViewState["currentScholarshipId"] != null)
					return (int?) ViewState["currentScholarshipId"];
				return new int?();
			}
			set
			{
				ViewState["currentScholarshipId"] = value;
			}
		}

		private Scholarship _currentScholarship;
		private Scholarship CurrentScholarship
		{
			get
			{
				if (_currentScholarship == null)
					_currentScholarship = ScholarshipService.GetById(currentScholarshipId.Value);
				return _currentScholarship;
			}
		}

		private IList<Scholarship> _allScholarships;
		private IList<Scholarship> AllScholarships
		{
			get
			{
				if (_allScholarships == null)
				{
					_allScholarships = ScholarshipService.GetAllScholarships();
				}
				return _allScholarships;
			}
		}
		#endregion

		#region Page Lifecycle Events

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				pnlDeactivateReactivate.Visible = false;
			}
			lblMessage.Text = string.Empty;
		}

		#endregion

		#region DataSource method

		public List<Organization> SelectProvidersForDropDown()
		{
			List<Organization> providerOrgs = new List<Organization>();
			var providers = (from s in AllScholarships
							 orderby s.Provider.Name
							 select s.Provider).Distinct();
			foreach (var provider in providers)
			{
				providerOrgs.Add(provider);
			}
			return providerOrgs;
		}
		public IEnumerable<Organization> SelectIntermediariesForDropDown()
		{
			List<Organization> intermediaryOrgs = new List<Organization>();
			var providers = (from s in AllScholarships
							 where s.Intermediary != null
							 orderby s.Intermediary.Name
							 select s.Intermediary).Distinct();
			foreach (var provider in providers)
			{
				intermediaryOrgs.Add(provider);
			}
			return intermediaryOrgs;
		}
		public IEnumerable<Scholarship> SelectScholarships()
		{
			return GetScholarships();
		}

		#endregion

		#region private methods
		private void ShowSearchPanel()
		{
			pnlSearch.Visible = true;
			pnlDeactivateReactivate.Visible = false;
		}

		private IEnumerable<Scholarship> GetScholarships()
		{
			var scholarships = from s in AllScholarships
							   select s;

			if (ddlProvider.SelectedIndex > 0)
			{
				scholarships = scholarships.Where(s => s.Provider.Id == int.Parse(ddlProvider.SelectedValue));
			}
			if (ddlIntermediary.SelectedIndex > 0)
			{
				scholarships = scholarships.Where(s => s.Intermediary != null && s.Intermediary.Id == int.Parse(ddlIntermediary.SelectedValue));
			}
			if (ddlScholarshipStatus.SelectedIndex > 0)
			{
				switch (ddlScholarshipStatus.SelectedValue)
				{
					case "Activated":
						scholarships =
							scholarships.Where(s => s.Stage == ScholarshipStages.Activated && s.ApplicationDueDate >= DateTime.Now);
						break;
					case "NotActivated":
						scholarships =
							scholarships.Where(s => ScholarshipDAL.GetNonActivatedStages().Contains(s.Stage) && s.Stage != ScholarshipStages.Rejected);
						break;
					case "Closed":
						scholarships =
							scholarships.Where(s => s.Stage == ScholarshipStages.Activated && s.ApplicationDueDate < DateTime.Now);
						break;
					case "Rejected":
						scholarships =
							scholarships.Where(s => s.Stage == ScholarshipStages.Rejected);
						break;
				}
			}
			if (!string.IsNullOrEmpty(txtNameSearch.Text))
			{
				scholarships = scholarships.Where(s => s.Name.Contains(txtNameSearch.Text));
			}
			return scholarships;
		}

		private void ShowReactivatePanel()
		{
			pnlSearch.Visible = false;
			pnlDeactivateReactivate.Visible = true;
			lblDeactivateReactivate.Text = "Reactivate";
			lblScholarshipName.Text = CurrentScholarship.Name;
		}

		private void ShowDeactivatePanel()
		{
			pnlSearch.Visible = false;
			pnlDeactivateReactivate.Visible = true;
			lblDeactivateReactivate.Text = "Deactivate";
			lblScholarshipName.Text = CurrentScholarship.Name;
		}

        private void ImpersonateAndShowScholarship()
        {
            string linkToScholarship = LinkGenerator.GetFullLinkStatic("/Provider/BuildScholarship/Default.aspx",
                                                                       "id=" + CurrentScholarship.Id.ToString());

            ImpersonateUser.Impersonate(UserContext.CurrentUser.Username, CurrentScholarship.Provider.AdminUser.Username, Page, Session, linkToScholarship);
        }


		#endregion

		#region control event handlers

		protected void providerDropDownSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = this;
		}

		protected void intermediaryDropDownSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = this;
		}

		protected void scholarshipDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = !Page.IsPostBack ? null : this;
		}

		protected void btnSearchScholarships_Click(object sender, EventArgs e)
		{
			gvScholarshipList.DataSourceID = scholarshipDataSource.ID;
			gvScholarshipList.DataBind();
		}

		protected void gvScholarshipList_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "deactivate")
			{
				currentScholarshipId = int.Parse(e.CommandArgument.ToString());
				ShowDeactivatePanel();
			}
			if (e.CommandName == "reactivate")
			{
				currentScholarshipId = int.Parse(e.CommandArgument.ToString());
				ShowReactivatePanel();
			}
            if (e.CommandName == "ShowScholarship")
			{
				currentScholarshipId = int.Parse(e.CommandArgument.ToString());
                ImpersonateAndShowScholarship();
			}
            
		}

		protected void btnDeactivateReactivate_Click(object sender, EventArgs e)
		{
			Page.Validate("vgActivation");
			if (!Page.IsValid)
				return;

			CurrentScholarship.Stage = lblDeactivateReactivate.Text == "Deactivate" 
				? ScholarshipStages.Rejected 
				: ScholarshipStages.Activated;
			CurrentScholarship.AppendAdminNotes(UserContext.CurrentUser, txtAdminComment.Text);
			ScholarshipService.Save(CurrentScholarship);
			gvScholarshipList.DataBind();
			ShowSearchPanel();
			lblMessage.Text = "Scholarship <b>" + CurrentScholarship.Name + "</b> was "
			                + (CurrentScholarship.Stage == ScholarshipStages.Rejected
			                  	? "deactivated"
			                  	: "reactivated") 
							+ " successfully.";
			if (null != ScholarshipSavedEvent)
			{
				ScholarshipSavedEvent(CurrentScholarship);
			}

		}

		protected void btnCancelDeactivateReactivate_Click(object sender, EventArgs e)
		{
			ShowSearchPanel();
			lblMessage.Text = "Scholarship status update canceled";
		}
		#endregion


	}
}