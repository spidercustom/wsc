﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class FinancialNeed : WizardStepUserControlBase<Application>
    {
        Domain.Application _applicationInContext;
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        private const string DISABLE_SCRIPT_TEMPLATE = "javascript:$('#{0}').attr('disabled', 'disabled')";
        private const string ENABLE_SCRIPT_TEMPLATE = "javascript:$('#{0}').removeAttr('disabled')";
        Domain.Application applicationInContext
        {
            get
            {
                if (_applicationInContext == null)
                    _applicationInContext = Container.GetDomainObject();
                return _applicationInContext;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (applicationInContext == null)
                throw new InvalidOperationException("There is no application in context");

            if (!Page.IsPostBack)
            {

                TypesOfSupport.DataBind();

                PopulateScreen();
            }
        }
        private void PopulateScreen()
        {
            MyChallengeControl.Text = applicationInContext.MyChallenge;
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());


            PopulateScreenFAFSA();
            if (null != applicationInContext.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(applicationInContext.SupportedSituation.TypesOfSupport, ts => ts.Id.ToString());
            }
        }

        private void PopulateScreenFAFSA()
        {
            FAFSACompletedControl.Checked = applicationInContext.HasFAFSACompleted ?? false;
            FAFSANotCompletedControl.Checked = applicationInContext.HasFAFSACompleted.HasValue && !applicationInContext.HasFAFSACompleted.Value;
            ExpectedFamilyContributionControl.Enabled = FAFSACompletedControl.Checked;
            ExpectedFamilyContributionControl.Amount = applicationInContext.ExpectedFamilyContribution ?? 0;
        }

        public override void PopulateObjects()
        {
            applicationInContext.MyChallenge = MyChallengeControl.Text;
            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            applicationInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            PopulateObjectsFAFSA();


            applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (applicationInContext.Stage < ApplicationStages.NotActivated)
                applicationInContext.Stage = ApplicationStages.NotActivated;
        }
        private void PopulateObjectsFAFSA()
        {
            applicationInContext.HasFAFSACompleted = null;
            applicationInContext.ExpectedFamilyContribution = null;
            if (FAFSACompletedControl.Checked)
            {
                applicationInContext.HasFAFSACompleted = true;
                applicationInContext.ExpectedFamilyContribution = ExpectedFamilyContributionControl.Amount;
            }
            if (FAFSANotCompletedControl.Checked)
                applicationInContext.HasFAFSACompleted = false;
        }
        protected override void OnPreRender(EventArgs e)
        {
            FAFSACompletedControl.Attributes.Add("onclick", ENABLE_SCRIPT_TEMPLATE.Build(ExpectedFamilyContributionControl.ClientID));
            FAFSANotCompletedControl.Attributes.Add("onclick", DISABLE_SCRIPT_TEMPLATE.Build(ExpectedFamilyContributionControl.ClientID));
            base.OnPreRender(e);
        }
        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
       
        #region WizardStepUserControlBase methods

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ApplicationService.Update(applicationInContext);
        }
        #endregion
	}
}