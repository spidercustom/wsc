﻿using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Tests.Wizards
{
    [TestFixture]
    public class PlaceHolderStepTest
    {
        private const int DUMMY_VALUE = 0;

        [Test]
        public void test_default_values()
        {
            var step = new PlaceHolderStep<int>();

            Assert.That(step.ValidateStep(), Is.True);
            Assert.That(step.ChangeNextStepIndex(), Is.False);
            Assert.That(step.GetChangedNextStepIndex(), Is.EqualTo(0));
            Assert.That(step.WasSuspendedFrom(DUMMY_VALUE), Is.False);
            Assert.That(step.CanResume(DUMMY_VALUE), Is.False);
        }
    }
}