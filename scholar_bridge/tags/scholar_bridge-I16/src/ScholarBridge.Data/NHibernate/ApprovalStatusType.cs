using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>ApprovalStatus</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class ApprovalStatusType : EnumStringType
    {
        public ApprovalStatusType()
            : base(typeof(ApprovalStatus), 30)
        {
        }
    }
}