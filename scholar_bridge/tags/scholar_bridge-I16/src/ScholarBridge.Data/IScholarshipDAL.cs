﻿using System;
using System.Collections.Generic;
using Spider.Common.Core.DAL;
using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IScholarshipDAL : IDAL<Scholarship>
    {
        Scholarship FindById(int id);
        Scholarship FindByBusinessKey(Provider provider, string name, int year);
        Scholarship Save(Scholarship scholarship);
        IList<Scholarship> FindByProvider(Provider provider);
        IList<Scholarship> FindByProvider(Provider provider, ScholarshipStages stage);
        IList<Scholarship> FindByProvider(Provider provider, ScholarshipStages[] stages);
        IList<Scholarship> FindByProviderNotActivated(Provider provider);
        IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary);
        IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages stage);
        IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages[] stages);
        IList<Scholarship> FindNotActivatedByOrganizations(Provider provider, Intermediary intermediary);
        IList<Scholarship> FindByIntermediary(Intermediary intermediary);
        IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStages stage);
        IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStages[] stages);
        IList<Scholarship> FindByIntermediaryNotActivated(Intermediary intermediary);
        IList<Scholarship> FindAllClosedSince(DateTime date);
        IList<Scholarship> FindByStage(ScholarshipStages stage);
        IList<Scholarship> FindAllDueOn(DateTime day);

        void Flush();
    }
}    