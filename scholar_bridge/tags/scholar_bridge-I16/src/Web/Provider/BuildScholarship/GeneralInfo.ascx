﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfo.ascx.cs"
    Inherits="ScholarBridge.Web.Provider.BuildScholarship.GeneralInfo" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedIntermediary" Src="~/Common/SelectRelatedIntermediary.ascx" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedProvider" Src="~/Common/SelectRelatedProvider.ascx" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>


<span><span class="requiredAttributeIndicator">*</span> = required</span><br />
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">Complete your profile</p>
        
        <label for="ScholarshipNameControl">Scholarship Name:<span class="requiredAttributeIndicator">*</span></label>
        <asp:TextBox CssClass="longtextarea" ID="ScholarshipNameControl" MaxLength="100" runat="server"></asp:TextBox>
        <br />
        <elv:PropertyProxyValidator ID="ScholarshipNameValidator" Display="Dynamic" runat="server"
            ControlToValidate="ScholarshipNameControl" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        <label for="ScholarshipAcademicYearControl">
            Academic Year:<span class="requiredAttributeIndicator">*</span></label>
        <asp:DropDownList ID="ScholarshipAcademicYearControl" runat="server" />
        <elv:PropertyProxyValidator ID="ScholarshipAcademicYearValidator" runat="server"
            ControlToValidate="ScholarshipAcademicYearControl" PropertyName="AcademicYear"
            SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        <label for="MissionStatementControl">
            Mission Statement:</label>
        <asp:TextBox ID="MissionStatementControl" runat="server" TextMode="MultiLine"></asp:TextBox>
        <elv:PropertyProxyValidator ID="MissionStatementValidator" runat="server" ControlToValidate="MissionStatementControl"
            PropertyName="MissionStatement" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        <label for="ProgramGuidelinesControl">
            Program Guidelines:</label>
        <asp:TextBox ID="ProgramGuidelinesControl" runat="server" TextMode="MultiLine"></asp:TextBox>
        <elv:PropertyProxyValidator ID="ProgramGuidelinesValidator" runat="server" ControlToValidate="ProgramGuidelinesControl"
            PropertyName="ProgramGuidelines" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        
        <hr />
        <P class="form-section-title">Donor Information</P>

        <label for="DonorName">
            Name:</label>
        <asp:TextBox ID="DonorName" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="DonorNameValidator" runat="server" ControlToValidate="DonorName"
            PropertyName="Name" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
        <br />
        <label for="AddressStreet">
            Address Line 1:</label>
        <asp:TextBox ID="AddressStreet" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet"
            PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressStreet2">
            Address Line 2:</label>
        <asp:TextBox ID="AddressStreet2" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2"
            PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressCity">
            City:</label>
        <asp:TextBox ID="AddressCity" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity"
            PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressState">
            State:</label>
        <asp:DropDownList ID="AddressState" runat="server">
        </asp:DropDownList>
        <elv:PropertyProxyValidator ID="AddressStateValidator" runat="server" ControlToValidate="AddressState"
            PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressPostalCode">
            Postal Code:</label>
        <asp:TextBox ID="AddressPostalCode" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode"
            PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="Phone">
            Phone:</label>
        <asp:TextBox ID="Phone" runat="server" CssClass="phone" />
        <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="Phone"
            PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
        <br />
        <label for="EmailAddress">
            Email Address:</label>
        <asp:TextBox ID="EmailAddress" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="EmailAddressValidator" runat="server" ControlToValidate="EmailAddress"
            PropertyName="Email" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
        <asp:RegularExpressionValidator ID="emailLengthValidator" runat="server" ControlToValidate="EmailAddress"
            ValidationExpression="^.{0,50}$" ErrorMessage="Email Address is should be less than 50 characters long." />
        <br />
        <hr />

        <P class="form-section-title">Organizations</P>
        
        <label for="ProviderSelected">Provider:</label>
        <sb:SelectRelatedProvider ID="ProviderSelected" runat="server" />
        <elv:PropertyProxyValidator ID="ProviderSelectedValidator" runat="server" ControlToValidate="ProviderSelected"
            PropertyName="Provider" SourceTypeName="ScholarBridge.Domain.Scholarship" OnValueConvert="providerValidator_OnValueConvert" />
        <br />
        
        <label for="IntermediarySelected">Intermediary:</label>
        <sb:SelectRelatedIntermediary ID="IntermediarySelected" runat="server" />

    </div>        
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">  
        <P class="form-section-title">Schedule</P>
        <label for="calAppStart">Application Start Date</label>
        <sb:CalendarControl ID="calApplicationStartDate" runat="server"></sb:CalendarControl>
        <elv:PropertyProxyValidator ID="ApplicationStartDateValidator" runat="server" ControlToValidate="calApplicationStartDate"
            PropertyName="ApplicationStartDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        
        <label for="calApplicationDueDate">
            Application Due Date</label>
        <sb:CalendarControl ID="calApplicationDueDate" runat="server"></sb:CalendarControl>
        <elv:PropertyProxyValidator ID="ApplicationDueDateValidator" runat="server" ControlToValidate="calApplicationDueDate"
            PropertyName="ApplicationDueDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        
        <label for="calAwardDate">
            Scholarship Notification/Award Date:</label>
        <sb:CalendarControl ID="calAwardDate" runat="server"></sb:CalendarControl>
        <elv:PropertyProxyValidator ID="AwardDateValidator" runat="server" ControlToValidate="calAwardDate"
            PropertyName="AwardDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        
        <P class="form-section-title">Scholarship Funding</P>
        <label for="AnnualSupportAmount">
            Annual Amount of Support:<span class="requiredAttributeIndicator">*</span></label>
        <sandTrap:CurrencyBox ID="AnnualSupportAmount" runat="server" Precision="0" />
        <elv:PropertyProxyValidator ID="AnnualSupportAmountValidator" runat="server" ControlToValidate="AnnualSupportAmount"
            PropertyName="AnnualSupportAmount" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
        <br />
        
        <label for="MinimumNumberOfAwards">
            Minimum Number of Awards:<span class="requiredAttributeIndicator">*</span></label>
        <sandTrap:NumberBox ID="MinimumNumberOfAwards" runat="server" Precision="0" />
        <elv:PropertyProxyValidator ID="MinimumNumberOfAwardsValidator" runat="server" ControlToValidate="MinimumNumberOfAwards"
            PropertyName="MinimumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
        <br />
        
        <label for="MaximumNumberOfAwards">
            Maximum Number of Awards:<span class="requiredAttributeIndicator">*</span></label>
        <sandTrap:NumberBox ID="MaximumNumberOfAwards" runat="server" Precision="0" />
        <elv:PropertyProxyValidator ID="MaximumNumberOfAwardsValidator" runat="server" ControlToValidate="MaximumNumberOfAwards"
            PropertyName="MaximumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
        <br />
        
        <label for="TermsOfSupport">
            Terms of Support:<span class="requiredAttributeIndicator">*</span></label>
        <asp:RadioButtonList ID="TermsOfSupportControl" runat="server" class="control-set" />
        <br />
        
        <label for="MinimumAmount">
            Minimum Scholarship Award Amount:<span class="requiredAttributeIndicator">*</span></label>
        <sandTrap:CurrencyBox ID="MinimumAmount" runat="server" Precision="0" />
        <elv:PropertyProxyValidator ID="MinimumAmountValidator" runat="server" ControlToValidate="MinimumAmount"
            PropertyName="MinimumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
        
        <label for="MaximumAmount">
            Maximum Scholarship Award Amount:<span class="requiredAttributeIndicator">*</span></label>
        <sandTrap:CurrencyBox ID="MaximumAmount" runat="server" Precision="0" />
        <elv:PropertyProxyValidator ID="MaximumAmountValidator" runat="server" ControlToValidate="MaximumAmount"
            PropertyName="MaximumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <br />
    </div>
</div>