﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="ScholarBridge.Web.Profile.ChangePassword" Title="Profile | Change Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div id="CenterContent">
        <div id="HomeContentLeft">
            
        </div>
        <div id="HomeContentRight">
            
        </div>
    <asp:ChangePassword ID="ChangePassword1" runat="server" 
        SuccessPageUrl="~/Profile/Default.aspx" 
        CancelDestinationPageUrl="~/Profile/Default.aspx" 
        onchangedpassword="ChangePassword1_ChangedPassword">
    </asp:ChangePassword>            
</div>    
</asp:Content>
