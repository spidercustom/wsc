﻿//***********************************************************
// DateChange.js
//
// This set of functions is used in conjuction with some serverside logic to
// cause the page to prompt the user if he/she tries to leave the page with un-submitted
// changes left behind.
// 
// 
//***********************************************************
var BP_bOnBeforeUnloadFired = false;

// determine if any fields/checkboxes/radios on the form that have changed 
// returns: bool
function checkForChanges() {

    var oForm = document.forms[BP_strFormName];
    var nElemCnt = oForm.elements.length;
    var nIdx, oElem, nOptIdx;
    var ctlDirty = document.getElementsByName("BP_bIsDirty")[0];
    var bChanged = (ctlDirty.value == "true");
    var nSkipCnt = 1;

    for (nIdx = 0; !bChanged && nIdx < nElemCnt; nIdx++) {
        oElem = oForm.elements[nIdx];

        nSkipCnt = BP_arrSkipList.length;

        // If the control is in the list of ones to ignore, carry on
        for (nOptIdx = 0; nOptIdx < nSkipCnt; nOptIdx++) {
            if (oElem.id == BP_arrSkipList[nOptIdx])
                break;

            nPos = oElem.id.length - BP_arrSkipList[nOptIdx].length;
            if (nPos >= 0)
                if (oElem.id.substr(nPos) == BP_arrSkipList[nOptIdx])
                break;
        }

        if (nOptIdx < nSkipCnt)
            continue;

        // Check for changes based on the control type
        if (oElem.type == "text" || oElem.tagName == "TEXTAREA") {
            if (oElem.value != oElem.defaultValue)
                bChanged = true;
        }
        else
            if (oElem.type == "checkbox" || oElem.type == "radio") {
            if (oElem.checked != oElem.defaultChecked)
                bChanged = true;
        }
        else
            if (oElem.tagName == "SELECT") {
            oOptions = oElem.options;
            nNumOpts = oOptions.length;
            nDefSelIdx = nSelIdx = 0;

            // Search for a change in the default.  If nothing is
            // explicitely marked as the default, element zero is
            // assumed to have been the default.
            for (nOptIdx = 0; nOptIdx < nNumOpts; nOptIdx++) {
                oOpt = oOptions[nOptIdx];

                if (oOpt.defaultSelected)
                    nDefSelIdx = nOptIdx;

                if (oOpt.selected)
                    nSelIdx = nOptIdx;
            }

            if (nDefSelIdx != nSelIdx)
                bChanged = true;
        }
    }
    if (bChanged)
        ctlDirty.value = "true";

    return bChanged;
}

// Get the Id of the object that caused the page unload 
// returns: string
function getSubmittingObjectID() {
    // IE Only:  The event target is most likely the item that caused the
    // request to leave the page.  If it's in the list of controls that can
    // bypass the check, don't prompt.  The control ID must be an exact match
    // or must end with the name (i.e. it's in a DataGrid).
    var strID = "";
    var oElem = document.getElementById("__EVENTTARGET");

    if (oElem == null || typeof (oElem) == "undefined" || oElem.value == "") {
        // Check the active element if there is no event target
        if (typeof (document.activeElement) != "undefined") {
            oElem = document.activeElement;
            strID = oElem.id;
        }
    }
    else
        strID = oElem.value;

    // Some elements may not have an ID but their parent element might
    // so grab that if possible (i.e. AREA elements in a MAP element).
    if (strID == "" && oElem != null && typeof (oElem) != "undefined") {
        // Link buttons in DataGrids don't have IDs but do use __doPostBack().
        // If we see a link with that in its href, assume __doPostBack() is
        // running and skip the check.  The submission will call us again.
        if (oElem.tagName == "A" && oElem.href.indexOf("__doPostBack") != -1)
            return;

        if (typeof (oElem.parentElement) != "undefined")
            strID = oElem.parentElement.id;
    }

    return strID;
}

// check the submitting button or link id against the list of
// controls to exclude from prompting for (usually the page submit buttons).
// returns: bool
function SubmitterIsInBypassList(strSubmitterID) {

    var bBypass = false;
    if (strSubmitterID != "") {
        nSkipCnt = BP_arrBypassList.length;

        for (nIdx = 0; nIdx < nSkipCnt; nIdx++) {
            if (strSubmitterID == BP_arrBypassList[nIdx]) {
                bBypass = true;
                break;
            }
            else {
                nPos = strSubmitterID.length - BP_arrBypassList[nIdx].length;
                if (nPos >= 0) {
                    var subStringToCompare = strSubmitterID.substr(nPos);
                    var bypassID = BP_arrBypassList[nIdx];
                    if (subStringToCompare == bypassID) {
                        bBypass = true;
                        break;
                    }
                }
            }
        }
    }
    return bBypass;
}

// This serves two purposes if post-back is cancelled.  It clears the flag
// that prevents a double prompt and it also clears the event target as it
// doesn't get cleared if you cancel an auto-postback item and then click a
// button for example.
function BP_funClearIfCancelled() {
    var oElem;

    BP_bOnBeforeUnloadFired = false;

    oElem = document.getElementById("__EVENTTARGET");

    if (oElem != null && typeof (oElem) != "undefined")
        oElem.value = "";
}

// Replace the OnSubmit event.  This is so that we can always update the state
// of the Dirty flag even when controls with AutoPostBack cause the submit.
document.forms[BP_strFormName].BP_RealOnSubmit =
    document.forms[BP_strFormName].submit;

function BP_OnSubmit() {
    checkForChanges();

    // It sometimes reports an error if OnBeforeUnload cancels it.  Ignore it.
    try { document.forms[BP_strFormName].BP_RealOnSubmit(); } catch (e) { }
}

// run this function whenever the page is being unloaded (on submit or link out or back button).
function __UnloadPage(e) {

    var bEventPassedIn = (typeof (e) != 'undefined');
    if (!bEventPassedIn && BP_bOnBeforeUnloadFired)
        return;
    var strSubmitterID = getSubmittingObjectID();
    var bBypassPrompt = SubmitterIsInBypassList(strSubmitterID);
    var bDataChanged = checkForChanges();

    if (bDataChanged) {
        if (!bBypassPrompt && !BP_bOnBeforeUnloadFired) {
            BP_bOnBeforeUnloadFired = true;
            window.setTimeout("BP_funClearIfCancelled()", 1000);
            if (e)
                e.returnValue = BP_strDataLossMsg;
            else
                return BP_strDataLossMsg;
        }
    }
}

if (window.onbeforeunload == null) {    // safari
    window.onbeforeunload = function() {
        return __UnloadPage();
    }
}
else if (window.addEventListener) { // ff & Opera
    addEventListener('beforeunload', __UnloadPage, true);
}
else if (window.attachEvent) { // ie 5+
    window.attachEvent('onbeforeunload', __UnloadPage);
}
else {
    window.onbeforeunload = function() { // others ?
        return __UnloadPage();
    }
}

document.forms[BP_strFormName].submit = BP_OnSubmit;

