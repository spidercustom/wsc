﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.PressRoom.Show" Title="Admin" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<sb:PrintView id="PrintViewControl" runat="server" /> 
<p>Press Room</p>
<br />
<br />
<p>Place keeper for WSC provided content. Content could include: Basic facts about the organization, Financial Information, Location of Headquarters, Leadership profiles, Downloadable images, Overview of the organization’s commitment to social responsibility, Media contact information</p>
<br />
<br />
<div>

<asp:Label ID="errorMessage" runat="server" Text="Article not found." Visible="false" CssClass="errorMessage"/>
<asp:Label ID="DateControl" runat="server" ></asp:Label>
<br />
<br />
<pre>
<asp:Label ID="TitleControl" runat="server" Font-Bold="true" ></asp:Label>
</pre>
<br />
<br />
<pre>
<asp:Label ID="BodyControl" runat="server"   CssClass="word_wrap"/>
</pre>
<br /><br />

</div>
<div id="linkarea" class="exclude-in-print">

   <ul class="pageNav">
      <li><asp:HyperLink ID="PressRoomLnk" runat="server" NavigateUrl="~/PressRoom/">Return to Press Room</asp:HyperLink></li>
      <li><asp:HyperLink ID="HomeLnk" runat="server" NavigateUrl="~/">Home</asp:HyperLink></li>
  </ul>
</div>            


</asp:Content>
