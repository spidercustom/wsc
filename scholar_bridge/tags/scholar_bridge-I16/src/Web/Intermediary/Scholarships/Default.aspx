<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Scholarships.Default" Title="Intermediary | Scholarships" %>

<%@ Register TagPrefix="sb" TagName="ScholarshipList" Src="~/Common/ScholarshipList.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgScholarship.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div id="CenterContent">
        <div id="HomeContentLeft">
        </div>
        <div id="HomeContentRight">
        </div>

    
        <p class="FormHeader">View options</p>
        <div class="form-iceland large-labels">
            <label class="label">View Scholarship By Status:</label>
            <asp:CheckBoxList ID="ScholarshipStatusCheckboxes" runat="server"
                RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="False" />
            <br />        
            
            <label class="label">Select Organization:</label>
            <asp:DropDownList ID="cboOrganization" runat="server" />
            <br />
            <label class="label">&nbsp;</label>
            <sbCommon:AnchorButton ID="UpdateViewBtn" runat="server" Text="Update View" onclick="UpdateViewBtn_Click" />
        </div>

    <sb:ScholarshipList id="scholarshipsList" runat="server" 
        ViewActionLink="~/Intermediary/Scholarships/Show.aspx"
        EditActionLink="~/Provider/BuildScholarship"  />            
</div>
</asp:Content>
