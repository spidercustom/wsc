﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Intermediary.Scholarships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = IntermediaryService.FindById(UserContext.CurrentIntermediary.Id);

            var orgs =
                (from i in RelationshipService.GetActiveByOrganization(intermediary)
                 orderby i.Name
                 select new {i.Id, i.Name}).ToList();

            orgs.Insert(0, new {Id = 0, Name = "- ALL -"});

            cboOrganization.DataValueField = "Id";
            cboOrganization.DataTextField = "Name";
            cboOrganization.DataSource = orgs;
            cboOrganization.DataBind();
            ScholarshipStatusCheckboxes.EnumBind(
                ScholarshipStages.NotActivated,
                ScholarshipStages.Activated,
                ScholarshipStages.Awarded);
            ScholarshipStatusCheckboxes.Items.SelectAll();
            FillScholarships();


        }

        private void FillScholarships()
        {
            var intermediary = UserContext.CurrentIntermediary;

            if (cboOrganization.SelectedValue == "0")
            {
                //fetch all
                scholarshipsList.Scholarships = ScholarshipService.GetByIntermediary(intermediary, GetSelectedStages());
            }
            else
            {
                var provider = ProviderService.FindById(int.Parse(cboOrganization.SelectedValue));

                scholarshipsList.Scholarships = ScholarshipService.GetByOrganizations(provider, intermediary, GetSelectedStages());
            }
        }

        private ScholarshipStages[] GetSelectedStages()
        {
            return ScholarshipStatusCheckboxes.Items.SelectedItems(o => (ScholarshipStages)Convert.ToInt32(o)).ToArray();
        }

        protected void UpdateViewBtn_Click(object sender, EventArgs e)
        {
            FillScholarships();
        }
    }
}