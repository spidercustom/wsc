﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Org.Edit" Title="Organization | Edit" %>

<%@ Register TagPrefix="sb" TagName="EditOrganization" Src="~/Common/EditOrganization.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:EditOrganization ID="editOrg" runat="server" OnOrganizationSaved="editOrg_OnOrganizationSaved" OnFormCanceled="editOrg_OnFormCanceled" />
</asp:Content>
