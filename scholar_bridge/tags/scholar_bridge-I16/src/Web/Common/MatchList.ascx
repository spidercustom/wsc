﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>
<asp:ListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    onpagepropertieschanging="matchList_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th></th>
                <th>Scholarship Name</th>
                <th>Required Criteria</th>
                <th>Preferred Criteria</th>
                <th>Application Due Date</th>
                <th># of Awards</th>
                <th>Scholarship Amount</th>
                <th>Action</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:ImageButton id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png"/></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td><asp:Button ID="matchBtn" runat="server" /></td>
        <td><%# Eval("MatchApplicationStatusString")%></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:ImageButton  id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png" /></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td><asp:Button ID="matchBtn" runat="server" /></td>
        <td><%# Eval("MatchApplicationStatusString")%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>&lt;There are no scholarships at this time.&gt;</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="matchList" PageSize="20" onprerender="pager_PreRender" >
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
</div> 