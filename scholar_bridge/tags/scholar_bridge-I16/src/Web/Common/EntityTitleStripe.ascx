﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EntityTitleStripe.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipTitleStripe" %>

<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>

<asp:Panel id="EntityTitleStripeContainer" CssClass="entity-title" runat="server" >
    <asp:PlaceHolder ID="TitleContainer" runat="server">
        <asp:Label runat="server" ID="TitleControl" CssClass="entity-title"></asp:Label>
        <asp:Label runat="server" id="AdditionalInfoControl" CssClass="entity-title-addition-info" ></asp:Label>
    </asp:PlaceHolder>
    <sb:PrintView id="PrintViewControl" runat="server" /> 
</asp:Panel>