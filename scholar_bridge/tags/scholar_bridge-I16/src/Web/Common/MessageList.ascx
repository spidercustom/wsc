﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageList.ascx.cs" Inherits="ScholarBridge.Web.Common.MessageList" %>
<asp:Repeater ID="messageList" runat="server" 
    onitemdatabound="messageList_ItemDataBound">
    <HeaderTemplate>
    <table class="sortableTable" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Subject</th>
                <th>From</th>
                <th>Organization Name</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton ID="linkToMessage" runat="server"   ><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
            <td><asp:Label ID="lblFrom" runat="server"></asp:Label></td>
            <td><asp:Label ID="lblOrg" runat="server"></asp:Label> </td>
            <td><%# DataBinder.Eval(Container.DataItem, "Date", "{0:MM/dd/yyyy}")%></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton ID="linkToMessage" runat="server"   ><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
             <td><asp:Label ID="lblFrom" runat="server"></asp:Label></td>
            <td><asp:Label ID="lblOrg" runat="server"></asp:Label> </td>
             <td><%# DataBinder.Eval(Container.DataItem, "Date", "{0:MM/dd/yyyy}")%></td>
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
        </tbody>
    </table>
    </FooterTemplate>
</asp:Repeater>