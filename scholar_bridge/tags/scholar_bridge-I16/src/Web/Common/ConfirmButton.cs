﻿using System;
using System.ComponentModel;
using System.Web.UI;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ConfirmButton runat=server></{0}:ConfirmButton>")]
    public class ConfirmButton : AnchorButton
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue(true)]
        [Localizable(true)]
        public bool NeedsConfirmation
        {
            get
            {
                var nc = ViewState["NeedsConfirmation"];
                if (null == nc)
                    return true;
                return (bool)nc;
            }

            set
            {
                ViewState["NeedsConfirmation"] = value;
            }
        }

       
        [Category("Appearance")]
        [DefaultValue("")]
        public string ConfirmMessageDivID
        {
            get
            {
                var s = (String)ViewState["ConfirmMessageDivID"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["ConfirmMessageDivID"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptInclude(GetType(), "DEFAULT", ResolveUrl("~/js/ConfirmButton.js"));
            base.OnPreRender(e);
        }

        private const string ON_CLICK_SCRIPT_TEMPLATE = "javascript:ConfirmYesNoDialog('{0}', '{1}');";

        protected override string BuildPostBackScript()
        {
            if (string.IsNullOrEmpty(ConfirmMessageDivID))
                throw new ArgumentException("ConfirmMessageDivID property cannot be left empty");
            
            return NeedsConfirmation ? ON_CLICK_SCRIPT_TEMPLATE.Build(ConfirmMessageDivID, UniqueID) : base.BuildPostBackScript();
        }

        public event EventHandler<ConfirmButtonClickEventArgs> ClickConfirm;

        public override void RaisePostBackEvent(string eventArgument)
        {
            if (NeedsConfirmation)
            {
                if (null != ClickConfirm)
                {
                    bool result ;
                    if (!bool.TryParse(eventArgument, out result))
                        result = true;
                    
                    var args = new ConfirmButtonClickEventArgs { DialogResult = result };
                    ClickConfirm(this, args);
                }
            }
            base.RaisePostBackEvent(eventArgument);
        }
    }

    public class ConfirmButtonClickEventArgs : EventArgs
    {
        public bool DialogResult { get; set; }    
    }
}
