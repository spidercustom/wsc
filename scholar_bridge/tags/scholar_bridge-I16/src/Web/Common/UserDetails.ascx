﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs" Inherits="ScholarBridge.Web.Common.UserDetails" %>

<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>

<asp:Image ID="personImage" runat="server" ImageUrl="~/images/person.png" class="profileImage"/>

<div id="memberInfo">
   <asp:Label ID="nameLbl" runat="server" /><br />
    <asp:Label ID="emailAddressLbl" runat="server" />
     
    <br />
    
    <table>
        <tr>
            <td>
                Phone: 
            </td>
            <td>
                <asp:Literal ID="phoneLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Fax: 
            </td>
            <td>
                <asp:Literal ID="faxLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Other Phone: 
            </td>
            <td>
                <asp:Literal ID="otherPhoneLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Member Since:&nbsp;&nbsp; 
            &nbsp;</td>
            <td>
                <asp:Literal ID="memberSinceLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Last Login: 
            </td>
            <td>
                <asp:Literal ID="lastLoginLbl" runat="server" />
            </td>
        </tr>
    </table>
</div>