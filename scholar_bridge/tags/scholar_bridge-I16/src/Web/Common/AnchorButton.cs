﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:AnchorButton runat=server></{0}:AnchorButton>")]
    public class AnchorButton : WebControl, IPostBackEventHandler
    {
        private const string CLICK_EVENT_KEY = "click";
        public bool CauseValidation { get; set; }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["Text"] = value;

            }
        }

        public string ValidationGroup { get; set; }
        public string AnchorCssClass { get; set; }
        public string OnClientClick { get; set; }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", BuildContainerCssClass());
            var controlwidth = Width.IsEmpty ? 150 : Width.Value;
            writer.WriteAttribute("id", ClientID);
            writer.WriteAttribute("name", UniqueID);
            writer.WriteAttribute("width", controlwidth + "px");
            if (Enabled)
            {
                if (!string.IsNullOrEmpty(OnClientClick))
                    writer.WriteAttribute("onclick", OnClientClick);
                else
                    writer.WriteAttribute("onclick", BuildPostBackScript());
            }
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            renderText(writer);
            writer.WriteLine();

            writer.WriteEndTag("a");
        }


        private string BuildContainerCssClass()
        {
            var result = new StringBuilder();
            
            result.Append(Enabled ? "button" : "disabled-button");
            if (!string.IsNullOrEmpty(CssClass))
                result.AppendFormat(",{0}", CssClass);
            if (!string.IsNullOrEmpty(AnchorCssClass))
                result.AppendFormat(",{0}", AnchorCssClass);

            return result.ToString();
        }

        private void renderText(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", Enabled ? "button-text" : "disabled-button-text");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write(Text);
            writer.WriteEndTag("span");
        }


        protected virtual string BuildPostBackScript()
        {
            var postBackOptions = new PostBackOptions(this, CLICK_EVENT_KEY);
            postBackOptions.ValidationGroup = ValidationGroup;
            postBackOptions.PerformValidation = CauseValidation;

            return Page.ClientScript.GetPostBackEventReference(postBackOptions);
        }

        public event EventHandler Click;

        public virtual void RaisePostBackEvent(string eventArgument)
        {
            if (CLICK_EVENT_KEY.Equals(eventArgument) && null != Click)
                Click(this, EventArgs.Empty);
        }
    }
}
