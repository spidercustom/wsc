﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class GlobalMenu : UserControl
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetLinksForLoggedInUser();
           
        }

        private void SetLinksForLoggedInUser()
        {
            if (HttpContext.Current.User.IsInRole(Role.PROVIDER_ROLE))
            {
                MyHomeLink.NavigateUrl = "~/Provider/";
            }
            else if (HttpContext.Current.User.IsInRole(Role.INTERMEDIARY_ROLE))
            {
                MyHomeLink.NavigateUrl = "~/Intermediary/";
            }
            else
            {
                MyHomeLink.NavigateUrl = "~/Seeker/";
            }
        }
    }
}

