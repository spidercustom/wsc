﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CaptchaControl.ascx.cs"
    Inherits="ScholarBridge.Web.Common.CaptchaControl" %>
<%@ Register Assembly="Hip" Namespace="Msdn.Web.UI.WebControls" TagPrefix="msdn" %>
<div class="captchaControl">
    <label for="txtCaptcha">
        Type the characters shown below</label>
    <asp:TextBox ID="txtCaptcha" runat="server" Width="180px" onkeyup="ToLower(this)"/>
    <br />
    <msdn:HipValidator ID="HipValidator1" runat="server" ControlToValidate="txtCaptcha"
        ErrorMessage="Please enter exact word as shown below" HipChallenge="ImageHipChallenge1" />
    <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCaptcha"
        ErrorMessage="Please enter exact word as shown below" />
    <br />
    <br />
    <msdn:ImageHipChallenge ID="ImageHipChallenge1" runat="server" Height="100px" Width="280px" />
    <br />
    <br />
    <asp:Button ID="trynewButton" runat="server" Text="Try another word." OnClick="trynewButton_Click"
        CausesValidation="false" />
    <br />
    <br />
    <p>
        By entering this code you help us prevent spam and fake registrations. This code
        can be typed completely in lower case.</p>
</div>
