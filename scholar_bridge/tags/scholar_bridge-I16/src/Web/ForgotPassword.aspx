﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="ScholarBridge.Web.ForgotPassword" Title="Forgot Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:PasswordRecovery ID="passwordRecovery" runat="server" 
        UserNameInstructionText="<br /><br />Enter your email address and click submit to reset your password. You will receive an email with your new password. If you forgot which email address you used, enter an email and click the submit button. The system will tell you if that email is not registered and you can try another email address.<br /><br />" 
        UserNameLabelText="Email Address:" 
        GeneralFailureText="An error has occured. Please check and try again."
        UserNameRequiredErrorMessage="Email is required." 
        LabelStyle-HorizontalAlign="Right"
        UserNameFailureText="The entered email address is not in our system. Please check and try again."
        onsendingmail="passwordRecovery_SendingMail">
        <SuccessTemplate>
            <h3>Your password has been sent to you.</h3>
            <asp:HyperLink ID="loginLnk" runat="server" NavigateUrl="~/Login.aspx">Return to Login page</asp:HyperLink>
        </SuccessTemplate>
        <InstructionTextStyle BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" />
            
    </asp:PasswordRecovery>

</asp:Content>
