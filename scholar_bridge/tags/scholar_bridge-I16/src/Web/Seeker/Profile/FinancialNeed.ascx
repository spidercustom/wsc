﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<div>
<h4>What is your financial need?</h4>
<br />
    

    <label for="MyChallengeControl">Describe your financial Challenge:</label>
    <asp:TextBox ID="MyChallengeControl" runat="server" TextMode="MultiLine"></asp:TextBox>
    <elv:PropertyProxyValidator ID="MyChallengeValidator" runat="server" ControlToValidate="MyChallengeControl" PropertyName="MyChallenge" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    <br />
    
    <asp:PlaceHolder ID="FAFSAContainer" runat="server">
        <h5><a href="http://www.fafsa.ed.gov/" target="_blank">FAFSA (Free Application for Federal Student Aid)</a></h5>
        <label>Have you completed the FAFSA?</label> 
        <asp:RadioButton ID="FAFSACompletedControl" Text="Yes" runat="server" GroupName="FAFSAFilled" />
        <asp:RadioButton ID="FAFSANotCompletedControl" Text="No" runat="server" GroupName="FAFSAFilled"/>
        <br />
        <label>What is your FAFSA defined EFC (Expected Family Contribution)?</label> 
        <sandTrap:NumberBox ID="ExpectedFamilyContributionControl" runat="server" Precision="0" Enabled="false"/>
        <br />
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
        <h5>Types of Support</h5>
        <asp:CheckBoxList ID="TypesOfSupport" runat="server" TextAlign="Left"/>
        <br />
    </asp:PlaceHolder>
    <asp:ScriptManager ID="scriptmanager1" runat="server" ></asp:ScriptManager>
</div>