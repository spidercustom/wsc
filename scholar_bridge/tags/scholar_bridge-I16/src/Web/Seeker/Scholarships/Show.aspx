﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Scholarships.Show" Title="Seeker | Scholarships | Show" %>
<%@ Register src="~/Common/ShowScholarship.ascx" tagname="ShowScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">


<asp:LinkButton ID="AddtoMyScholarshipBtn" runat="server"  Text="Add to My Scholarships of Interest" OnClick="AddtoMyScholarshipBt_Click" Style="float: right; padding-top: 24px;"/>
<asp:Label ID="spacer" runat="server" Text="&nbsp;&nbsp;" Style="float: right; padding-top: 24px;"></asp:Label>
<asp:HyperLink ID="sendToFriendBtn" NavigateUrl="mailto:" runat="server"   Text="Send to a friend" Style="float: right; padding-top: 24px;"/>

<sb:ScholarshipTitleStripe id="scholarshipTitleStripeControl" runat="server" />
<sb:ShowScholarship id="showScholarship" runat="server" />
</asp:Content>
