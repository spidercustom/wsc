namespace ScholarBridge.Domain.Lookup
{
    public class Support : LookupBase
    {
        public virtual SupportType SupportType { get; set; }
    }
}