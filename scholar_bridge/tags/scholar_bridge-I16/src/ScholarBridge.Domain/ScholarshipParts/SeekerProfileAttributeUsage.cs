﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    /// <summary>
    /// Place holder type for NHibernate. Do not add anything to this
    /// </summary>
    public class SeekerProfileAttributeUsage : ScholarshipAttributeUsage<SeekerProfileAttribute>
    {
    }
}
