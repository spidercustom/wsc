﻿
using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    public enum AwardStatuses
    {
        None,
        Offered,
        Awarded,
        [DisplayName("Not Awarded")]
        NotAwarded
    }
}
