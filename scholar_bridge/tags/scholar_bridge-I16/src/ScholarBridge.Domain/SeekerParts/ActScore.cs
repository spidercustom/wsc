namespace ScholarBridge.Domain.SeekerParts
{
    public class ActScore
    {
        public virtual int? English { get; set; }
        public virtual int? Mathematics { get; set; }
        public virtual int? Reading { get; set; }
        public virtual int? Science { get; set; }
    }
}