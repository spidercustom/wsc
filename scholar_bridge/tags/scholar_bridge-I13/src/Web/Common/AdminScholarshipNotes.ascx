﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminScholarshipNotes.ascx.cs" Inherits="ScholarBridge.Web.Common.AdminScholarshipNotes" %>

  

<table>
    <tr>
        <td>
            <label for="existingNotesTb">Administrative Notes:</label>
        </td>
        <td>
            <asp:panel ID="existingNotesPanel" runat="server" ScrollBars="Vertical" CssClass="adminNoteDisplay">
                <asp:Literal ID="notesLiteral" runat="server"></asp:Literal>
            </asp:panel>
        </td>
    </tr>
    <tr>
        <td>
            <label for="existingNotesTb">Add Notes:</label>  
        
        </td>
        <td>
            <asp:TextBox ID="newNotesTb" runat="server" TextMode="MultiLine" CssClass="adminNoteEntry" Columns="300" Rows="5"></asp:TextBox><br />
        
        </td>
    </tr>
    <tr>
        <td>
        
        </td>
        <td>
            <asp:Button ID="addNotes" runat="server" Text="Add Note" onclick="addNotes_Click"  />
        
        </td>
    </tr>
</table>
<br />