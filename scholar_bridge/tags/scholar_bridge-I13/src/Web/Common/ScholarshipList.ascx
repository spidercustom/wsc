﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipList.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipList" %>

<asp:ListView ID="lstScholarships" runat="server" 
    OnItemDataBound="lstScholarships_ItemDataBound" 
    onpagepropertieschanging="lstScholarships_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th>Status</th>
                <th>Name</th>
                <th>Academic Year</th>
                <th>Application Start Date</th>
                <th>Application Due Date</th>
                <th>Number of Applicants</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><%# ((ScholarshipStages) DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName() %></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# DataBinder.Eval(Container.DataItem, "DisplayName") %></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
        <td<asp:Label id="applicantCountControl" runat="server" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><%# ((ScholarshipStages) DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName() %></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# DataBinder.Eval(Container.DataItem, "DisplayName") %></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
        <td><asp:Label id="applicantCountControl" runat="server" /></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships, </p>
    <ul class="pageNav">
        <li><asp:HyperLink ID="createScholarshipLnk" runat="server" NavigateUrl="~/Provider/BuildScholarship/">Create a New Scholarship</asp:HyperLink></li>
    </ul>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="lstScholarships" PageSize="20" onprerender="pager_PreRender" >
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField  />
        </Fields>
    </asp:DataPager>
</div> 
