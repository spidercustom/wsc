﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowApplication.ascx.cs" Inherits="ScholarBridge.Web.Common.ShowApplication" %>
<%@ Register src="~/Common/ApplicationBasicsShow.ascx" tagname="ApplicationBasicsShow" tagprefix="sb" %>
<%@ Register src="~/Common/ApplicationAboutMeShow.ascx" tagname="ApplicationAboutMeShow" tagprefix="sb" %>
<%@ Register src="~/Common/ApplicationAcademicInfoShow.ascx" tagname="ApplicationAcademicInfoShow" tagprefix="sb" %>
<%@ Register src="~/Common/ApplicationActivitiesShow.ascx" tagname="ApplicationActivitiesShow" tagprefix="sb" %>
<%@ Register src="~/Common/ApplicationFinancialNeedShow.ascx" tagname="ApplicationFinancialNeedShow" tagprefix="sb" %>
<%@ Register src="~/Common/ApplicationAdditionalRequirementsShow.ascx" tagname="ApplicationAdditionalRequirementsShow" tagprefix="sb" %>

<div class="tabs">
    <ul class="linkarea">
        <li><a href="#basics-tab"><span>Basics</span></a></li>
        <li><a href="#aboutme-tab"><span>About Me</span></a></li>
        <li><a href="#academicinfo-tab"><span>Academic Info</span></a></li>
        <li><a href="#activities-tab"><span>Activities &amp; Interests</span></a></li>
        <li><a href="#financial-tab"><span>Financial Need</span></a></li>
        <li><a href="#requirements-tab"><span>+Requirements</span></a></li>
    </ul>
    
    <h2 class="only-in-print">Application For <asp:Literal ID="scholarshipName" runat="server" /></h2>
        
    <div id="basics-tab">
        <sb:ApplicationBasicsShow ID="ApplicationBasicsShow1" runat="server" />
    </div>
    <div id="aboutme-tab">
        <sb:ApplicationAboutMeShow ID="ApplicationAboutMeShow1" runat="server" />
    </div>
    <div id="academicinfo-tab">
        <sb:ApplicationAcademicInfoShow ID="ApplicationAcademicInfoShow1" runat="server" />
    </div>
    <div id="activities-tab">
        <sb:ApplicationActivitiesShow ID="ApplicationActivitiesShow1" runat="server" />
    </div>
    <div id="financial-tab">
        <sb:ApplicationFinancialNeedShow ID="ApplicationFinancialNeedShow1" runat="server" />
    </div>
    <div id="requirements-tab">
        <sb:ApplicationAdditionalRequirementsShow ID="ApplicationAdditionalRequirementsShow1" runat="server" />
    </div>
</div>