﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class IntermediaryRelationshipList : UserControl
    {
        public IList<Relationship> Relationships { get; set; }
        public string InactivateLinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Bind();
        }

        protected void Bind()
        {
            lstRelationships.DataSource = (from r in Relationships orderby r.Provider.Name select r).ToList();
            lstRelationships.DataBind();
        }

        protected void lstRelationships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) 
                return;


            var relationship = ((Relationship) ((ListViewDataItem) e.Item).DataItem);
            var link = (HyperLink) e.Item.FindControl("inactivateLink");
            if (relationship.Status == RelationshipStatus.Active)
            {
                link.NavigateUrl = InactivateLinkTo + "?id=" + relationship.Id;
                link.Attributes.Add("onclick",
                                    string.Format("return yesnodialog('yesnodialogdiv','{0}');",
                                                  ResolveUrl(link.NavigateUrl)));
            }
            else
            {
                link.Visible = false;
            }

            SetLabel(e, "lblName", relationship.Provider.Name);
            SetLabel(e, "lblStatus", relationship.Status.GetDisplayName());
            SetLabel(e, "lblPhone", relationship.Provider.Phone.Number);

            var admin = relationship.Provider.AdminUser;
            SetLabel(e, "lblAdminName", admin.Name.ToString());
            SetLabel(e, "lblAdminEmail", admin.Email);
        }

        private static void SetLabel(ListViewItemEventArgs e, string controlName, string value)
        {
            var lbl = (Literal)e.Item.FindControl(controlName);
            lbl.Text = value;
        }

        protected void lstRelationships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }

        protected void lstRelationships_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
        }
        protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }
    }
}