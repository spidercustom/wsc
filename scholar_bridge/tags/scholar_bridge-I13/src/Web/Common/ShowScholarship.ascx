﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowScholarship.ascx.cs"
    Inherits="ScholarBridge.Web.Common.ShowScholarship" %>
<%@ Register Src="~/Common/GeneralInfoShow.ascx" TagName="GeneralInfoShow" TagPrefix="sb" %>
<%@ Register Src="~/Common/SeekerInfoShow.ascx" TagName="SeekerInfoShow" TagPrefix="sb" %>
<%@ Register Src="~/Common/FundingInfoShow.ascx" TagName="FundingInfoShow" TagPrefix="sb" %>
<%@ Register Src="~/Common/AdditionalCriteriaShow.ascx" TagName="AdditionalCriteriaShow"
    TagPrefix="sb" %>
<%@ Register Src="~/Common/AdminScholarshipNotes.ascx" TagName="AdminScholarshipNotes"
    TagPrefix="sb" %>
<%@ Register Src="~/Common/ScholarshipApplicants.ascx" TagName="ScholarshipApplicants"
    TagPrefix="sb" %>
<div class="tabs">
    <ul class="linkarea">
        <li><a href="#generalinfo-tab"><span>General Information</span></a></li>
        <li><a href="#seekerprofile-tab"><span>Seeker Profile</span></a></li>
        <li><a href="#fundingprofile-tab"><span>Financial Need</span></a></li>
        <li><a href="#additionalcriteria-tab"><span>+Requirements</span></a></li>
        <asp:LoginView ID="applicantTitleLoginView" runat="server">
            <RoleGroups>
                <asp:RoleGroup Roles="Provider,Intermediary">
                    <ContentTemplate>
                        <li><a href="#applicants-tab"><span id="applicantsLbl" runat="server">Applicants</span></a></li>
                    </ContentTemplate>
                </asp:RoleGroup>
            </RoleGroups>
        </asp:LoginView>
        <asp:LoginView ID="adminNoteTitleLoginView" runat="server">
            <RoleGroups>
                <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
                    <ContentTemplate>
                        <li><a href="#notes-tab"><span>Admin Notes</span></a></li>
                    </ContentTemplate>
                </asp:RoleGroup>
            </RoleGroups>
        </asp:LoginView>
    </ul>
    <h2 class="only-in-print">
        Scholarship Information For
        <asp:Literal ID="scholarshipName" runat="server" /></h2>
    <div id="generalinfo-tab">
        <sb:GeneralInfoShow ID="GeneralInfoShow1" runat="server" />
    </div>
    <div id="seekerprofile-tab">
        <sb:SeekerInfoShow ID="SeekerInfoShow1" runat="server" />
    </div>
    <div id="fundingprofile-tab">
        <sb:FundingInfoShow ID="FundingInfoShow1" runat="server" />
    </div>
    <div id="additionalcriteria-tab">
        <sb:AdditionalCriteriaShow ID="AdditionalCriteriaShow1" runat="server" />
    </div>
    <asp:LoginView ID="applicantTabContentLoginView" runat="server">
        <RoleGroups>
            <asp:RoleGroup Roles="Provider,Intermediary">
                <ContentTemplate>
                    <div id="applicants-tab">
                        <sb:ScholarshipApplicants ID="ScholarshipApplicants1" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
    <asp:LoginView ID="adminNoteTabContentLoginView" runat="server">
        <RoleGroups>
            <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
                <ContentTemplate>
                    <div id="notes-tab">
                        <sb:AdminScholarshipNotes ID="AdminScholarshipNotes1" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
</div>
