﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Show : Page
    {
        private const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship ScholarshipToView { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params[SCHOLARSHIP_ID], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Business.Exceptions.ProviderNotApprovedException();

            ScholarshipToView = ScholarshipService.GetById(ScholarshipId);
            if (ScholarshipToView != null)
            {
                if (!ScholarshipToView.Provider.Id.Equals(provider.Id))
                    throw new InvalidOperationException("Scholarship does not belong to provider in context");

                showScholarship.Scholarship = ScholarshipToView;

                confirmRequestActivationBtn.Visible = (ScholarshipToView.Stage != ScholarshipStages.None &&
                                                ! ScholarshipToView.IsInActivationProcess());
                
                deleteConfirmBtn.Visible = ScholarshipToView.CanEdit();
                linkCopy.NavigateUrl = "~/Provider/BuildScholarship/Default.aspx?copyfrom=" + ScholarshipToView.Id;
                ScholarshipTitleStripeControl.UpdateView(ScholarshipToView);
            }
        }
        
        protected void deleteConfirmBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (e.DialogResult)
            {
                ScholarshipToView.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                ScholarshipService.Delete(ScholarshipToView);
                SuccessMessageLabel.SetMessage("Scholarship was deleted.");
                Response.Redirect("~/Provider/Scholarships/Default.aspx");  

            }
        }

        protected void confirmRequestActivationBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (e.DialogResult)
            {
                ScholarshipToView.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                if (ScholarshipToView.CanSubmitForActivation())
                {
                    ScholarshipService.RequestActivation(ScholarshipToView);
                    SuccessMessageLabel.SetMessage("Scholarship submitted for activation.");
                    Response.Redirect("~/Provider/Scholarships/Default.aspx#pending-activated-tab");
                }
                else
                {
                    ClientSideDialogs.ShowAlert("<p>Scholarship is either already in activation process or is already activated/denied</p>", "Cannot Send Activation Request!");
                }
            }
        }
    }
}
