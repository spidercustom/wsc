﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
  CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Default" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="GeneralInfo.ascx" tagname="GeneralInfo" tagprefix="sb" %>
<%@ Register src="SeekerProfile.ascx" tagname="SeekerProfile" tagprefix="sb" %>
<%@ Register src="FundingProfile.ascx" tagname="FundingProfile" tagprefix="sb" %>
<%@ Register src="MatchCriteriaSelection.ascx" tagname="MatchCriteriaSelection" tagprefix="sb" %>
<%@ Register src="AdditionalCriteria.ascx" tagname="AdditionalCriteria" tagprefix="sb" %>
<%@ Register src="Activate.ascx" tagname="Activate" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
 


<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
 


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
 <sb:PrintView ID="PrintView1" runat="server" />
  <div >    
  
  <sbCommon:ConfirmButton ID="deleteConfirmBtn" ConfirmMessageDivID ="deleteConfirmDiv"  Width="150px" Text="Delete Scholarship"  runat="server" onclick="deleteConfirmBtn_Click" />  
      
  </div> 
   <div id="confirmSaving" title="Confirm saving" style="display:none">
    You have change the data. Would you like to save the changes?
  </div>
  <br />
  <div id="deleteConfirmDiv" title="Confirm delete" style="display:none">
        Deleting the scholarship will remove it from the system. 
        Only scholarships that have yet to be activated can be deleted.
    <br /> <br />Are you sure want to delete?
</div>   
  <p><sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HasPrintView="false" />

  <div class="tabs" id="BuildScholarshipWizardTab">
    <ul>
      <li>
        <a href="#tab">
          <span>General</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <span>Selection Criteria</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <span>Seeker</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <span>Financial Need</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <span>+Requirements</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <span>Activate</span>
        </a>
      </li>
    </ul>
    
    <div id="tab">
      <asp:MultiView ID="BuildScholarshipWizard" runat="server"
        OnActiveViewChanged="BuildScholarshipWizard_ActiveStepChanged">
          <asp:View ID="GeneralInfoStep" runat="server">
            <sb:GeneralInfo ID="generalInfo" runat="server" />
          </asp:View>

          <asp:View ID="MatchCriteriaSelectionStep" runat="server">
            <sb:MatchCriteriaSelection ID="matchCriteriaSelection" runat="server" />
          </asp:View>
          
          <asp:View ID="SeekerProfileStep" runat="server" >
            <div class="controlPanel">
              <input id="SeekerProfileStepListChangeButton" type="button" style="float:right" value="List Change" onclick="javascript:window.open('<%= ResolveUrl("~/provider/scholarships/SubmitListChangeRequest.aspx") %>');" />
            </div><br />
            <sb:SeekerProfile ID="seekerProfile" runat="server" />
          </asp:View>
          
          <asp:View ID="FundingProfileStep" runat="server">
            <div class="controlPanel" >
              <input id="FundingProfileStepListChangeButton" type="button" style="float:right" value="List Change" onclick="javascript:window.open('<%= ResolveUrl("~/provider/scholarships/SubmitListChangeRequest.aspx") %>');" />
            </div><br />
            <sb:FundingProfile ID="fundingProfile" runat="server" />
          </asp:View>
          
          <asp:View ID="CriteriaStep" runat="server" >
            <sb:AdditionalCriteria ID="additionalCriteria" runat="server" />
          </asp:View>
          
          <asp:View ID="ActivateScholarshipStep" runat="server">
            <sb:Activate ID="ActivateStep" runat="server" />
          </asp:View>
      </asp:MultiView>
      
      <sbCommon:jQueryTabIntegrator id="buildScholarshipWizardTabIntegrator" runat="server" MultiViewControlID="BuildScholarshipWizard" 
            TabControlClientID="BuildScholarshipWizardTab"  CausesValidation="true" />
    </div>
  </div>
  <div class="controlPanel">
    <fieldset>
        <sbCommon:SaveConfirmButton ID="previousButton" runat="server" NeedsConfirmation="false" ToolTip="Save & navigate to the prior tab" OnClick="PreviousButton_Click" Text="Previous"/>
        <sbCommon:SaveConfirmButton ID="nextButton" runat="server" NeedsConfirmation="false" ToolTip="Save & navigate to the next tab" OnClick="NextButton_Click" Text="Next" />
        <sbCommon:SaveConfirmButton ID="SaveNoConfirmButton" NeedsConfirmation="false" OnClick="SaveNoConfirmButton_Click" runat="server" Text="Save" />
        <sbCommon:SaveConfirmButton ID="exitButton" runat="server" ToolTip="Exit the scholarship build" ConfirmMessageDivID="confirmSaving" OnClick="ExitButton_Click" Text="Exit" />
        <!-- hack to get the WebForm_DoPostBackWithOptions to be emitted -->
        <span style="visibility:hidden;"><asp:Button ID="saveButton" runat="server" OnClick="SaveButton_Click" ToolTip="Save any work on this tab" Text="Save"/></span>
    </fieldset>
  </div>
</asp:Content>
