﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.FundingProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<h3>Build Scholarship – Financial Need</h3>

<div id="legends">
    <ul>
        <li><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" /> <span>indicates a required criteria for this scholarship</span></li>
        <li><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" /> <span>indicates a preferred, but not required criteria, for this scholarship</span></li>
    </ul>
</div>

<asp:PlaceHolder ID="NeedContainerControl" runat="server">
    <asp:PlaceHolder ID="FAFSAContainer" runat="server">
      <h5>FAFSA <asp:Image ID="NeedUsageTypeIconControl" runat="server" /></h5>
      <label>FAFSA Required?</label> 
      <asp:RadioButton ID="FAFSARequiredControl" Text="Yes" runat="server" GroupName="FAFSARequired" />
      <asp:RadioButton ID="FAFSANotRequiredControl" Text="No" runat="server" GroupName="FAFSARequired"/>
       <br />
       
       <label>FAFSA defined EFC required?</label> 
      <asp:RadioButton ID="FAFSAEFCRequiredControl" Text="Yes" runat="server" GroupName="FAFSAEFCRequired" />
      <asp:RadioButton ID="FAFSAEFCNotRequiredControl" Text="No" runat="server" GroupName="FAFSAEFCRequired"/>
       <br />
     </asp:PlaceHolder> 
          
     <asp:PlaceHolder ID="FinancialInformationContainer" runat="server">
      <h5>Financial Information </h5>
      <label>Number of Family Members in Household required?</label> 
      <asp:RadioButton ID="FamilyMembersInHouseHoldRequiredControl" Text="Yes" runat="server" GroupName="FamilyMembersInHouseHoldRequired" />
      <asp:RadioButton ID="FamilyMembersInHouseHoldNotRequiredControl" Text="No" runat="server" GroupName="FamilyMembersInHouseHoldRequired"/>
       <br />
       
      <label>Number of Family Dependents attending College required?</label> 
      <asp:RadioButton ID="FamilyDependentsAttendingCollegeRequiredControl" Text="Yes" runat="server" GroupName="FamilyDependentsAttendingCollegeRequired" />
      <asp:RadioButton ID="FamilyDependentsAttendingCollegeNotRequiredControl" Text="No" runat="server" GroupName="FamilyDependentsAttendingCollegeRequired"/>
       <br />
       
      <label>Family income required?</label> 
      <asp:RadioButton ID="FamilyIncomeRequiredControl" Text="Yes" runat="server" GroupName="FamilyIncomeRequired" />
      <asp:RadioButton ID="FamilyIncomeNotRequiredControl" Text="No" runat="server" GroupName="FamilyIncomeRequired"/>
       <br />
       <asp:PlaceHolder ID="FamilyIncomeRangeContainer" runat="server">
       </asp:PlaceHolder>
     </asp:PlaceHolder>     
    
</asp:PlaceHolder>

<asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
  <h5>Situations the Scholarship will Fund <asp:Image ID="SupportSituationUsageTypeIconControl" runat="server" /></h5>
  <asp:CheckBoxList ID="TypesOfSupport" runat="server" TextAlign="Left"/>
  <br />
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
  <p>No Scholarship Match Criteria Selected. Please go to the <asp:LinkButton ID="BuildMatchCriteriaLinkControl" runat="server" OnClick="BuildMatchCriteriaLinkControl_Click">Selection Criteria</asp:LinkButton> tab to choose fields to be used for Scholarship selection</p>
</asp:PlaceHolder>
