﻿<%@ Page Language="C#" MasterPageFile="~/Seeker/Seeker.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Default"  Title="Seeker" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<%@ Register src="~/Common/ScholarshipSearchBox.ascx" tagname="ScholarshipSearchBox" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <p>
        This will be the home page for a Seeker. It will contain the dashboard elements
        based on their current status.
    </p>
    <sb:Login ID="loginForm" runat="server" />
    
    <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Seeker/Register.aspx">Register as a Scholarship Seeker</asp:HyperLink>
    <br /><br />
    <P>Looking for a specific Scholarship? Enter the Scholarship name or the name of the Scholarship Provider/Donor and we will display the matching results.</P>
    <sb:ScholarshipSearchBox ID="ScholarshipSearchBox1" runat="server"  />
    
</asp:Content>
