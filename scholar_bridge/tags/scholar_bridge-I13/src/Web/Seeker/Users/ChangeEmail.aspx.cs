﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Seeker.Users
{
    public partial class ChangeEmail : Page
    {
        public IUserContext UserContext { get; set; }
        public int UserId { get { return Int32.Parse(Request["id"]); } }
        public User CurrentUser { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
            CurrentUser = UserContext.CurrentSeeker.User;
            editUserEmail.CurrentUser = CurrentUser;
        }

        protected void editUserEmail_OnUserEmailSaved(User user,string Message)
        {
            SuccessMessageLabel.SetMessage(Message);
            Response.Redirect("~/Seeker/Profile");
        }

        protected void editUserEmail_OnFormCanceled()
        {
            Response.Redirect("~/Seeker/Profile");
        }
    }
}
