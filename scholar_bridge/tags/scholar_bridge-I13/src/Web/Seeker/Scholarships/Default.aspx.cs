﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Scholarships
{
	public partial class Default : SBBasePage
	{
	    private const string SCHOLARSHIP_VIEW_TEMPLATE = "/Seeker/Matches/Show.aspx";
	    public IMatchService MatchService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Seeker currentSeeker;
	    private IList<Match> matches;
	    

	    public IList<Match> Matches
	    {
	        get
	        {
                if (matches == null)
                {
                    if (currentSeeker == null)
                        throw new InvalidOperationException("There is no seeker in context");
                    matches = MatchService.GetSavedMatches(currentSeeker);
                }
	            return matches;
	        }
	    }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
            currentSeeker = UserContext.CurrentSeeker;
            lblProfileActivationMessage.Visible = !UserContext.CurrentSeeker.IsPublished();
            if (!Page.IsPostBack)
            {
                BindMyScholarships();
            }
        }

        private void BindMyScholarships()
        {
            myScholarhipList.DataSource = Matches;
            myScholarhipList.DataBind();
        }

        protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.UnSaveMatch(currentSeeker, scholarshipId);
        }

        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match)((ListViewDataItem)e.Item).DataItem);
                var link = (LinkButton)e.Item.FindControl("linkToScholarship");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(SCHOLARSHIP_VIEW_TEMPLATE) }.ToString()
                            + "?id=" + match.Scholarship.Id + "&print=true";
                link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
           

                var btn = (Button)e.Item.FindControl("DefaultActionButton");
                ApplyMatchAction(btn, match);

                btn = (Button)e.Item.FindControl("RemoveButton");
                btn.CommandArgument = match.Id.ToString();
                if (match.Application != null)
                {
                    if (match.Application.Stage == ApplicationStages.Submitted)
                        btn.Visible = false;
                }
            }
        }


        protected void DefaultActionButton_OnCommand(object sender, CommandEventArgs e)
        {
            var match = GetMatch(e);
            ExecuteDefaultAction(match);
            BindMyScholarships();
        }

	    protected void RemoveButton_OnCommand(object sender, CommandEventArgs e)
        {
            var match = GetMatch(e);
            MatchService.UnSaveMatch(currentSeeker, match.Scholarship.Id);
	        matches = null;
            BindMyScholarships();
        }

        protected void matchList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindMyScholarships();
        }

        private Match GetMatch(CommandEventArgs e)
        {
            var id = Int32.Parse((string)e.CommandArgument);
            var match = Matches.Single(o => o.Id == id);
            if (null == match)
                throw new InvalidOperationException("Cannot find match in cached collection");
            return match;
        }
        protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }
        #region Match Actions

        private static void ApplyMatchAction(Button btn, Match match)
        {
            var matchAction = GetDefaultAction(match);
            btn.Visible = !(matchAction is BlankAction);
            btn.CommandArgument = match.Id.ToString();
            btn.Text = matchAction.Text;
        }

        private static MatchAction GetDefaultAction(Match match)
        {
            switch (match.MatchApplicationStatus)
            {
                case MatchApplicationStatus.Unknown:
                    return BlankAction.Instance;
                case MatchApplicationStatus.NotApplied:
                    return new BuildApplicationAction();
                case MatchApplicationStatus.Appling:
                    return new EditApplicationAction();
                case MatchApplicationStatus.Applied:
                    return new ViewApplicationAction();
                case MatchApplicationStatus.BeingConsidered:
                    return new ViewApplicationAction();
                case MatchApplicationStatus.Closed:
                    return BlankAction.Instance;
                case MatchApplicationStatus.Offerred:
                    return new ViewContactInfoAction();
                case MatchApplicationStatus.Awarded:
                    return new ViewApplicationAction();
                default:
                    throw new NotSupportedException();
            }
        }

        private static void ExecuteDefaultAction(Match match)
        {
            GetDefaultAction(match).Execute(match);
        }

        
        abstract class MatchAction
        {
            public string Text { get; protected set; }
            public Action<Match> Execute { get; protected set; }
        }

        abstract class OpenUrlAction : MatchAction
        {
            protected OpenUrlAction(string text)
            {
                Text = text;
                Execute = o => HttpContext.Current.Response.Redirect(ConstructUrl(o));
            }

            protected abstract string ConstructUrl(Match match);
        }

        class EditApplicationAction : OpenUrlAction
        {
            private const string EDIT_APPLICATION_URL_TEMPLATE = "~/Seeker/BuildApplication/Default.aspx?aid={0}";

            public EditApplicationAction() : base("Edit") {}

            protected override string ConstructUrl(Match match)
            {
                return EDIT_APPLICATION_URL_TEMPLATE.Build(match.Application.Id);
            }
        }

        class BuildApplicationAction : OpenUrlAction
        {
            private const string BUILD_APPLICATION_URL_TEMPLATE = "~/Seeker/BuildApplication/Default.aspx?sid={0}";

            public BuildApplicationAction() : base("Apply") {}

            protected override string ConstructUrl(Match match)
            {
                return BUILD_APPLICATION_URL_TEMPLATE.Build(match.Scholarship.Id);
            }
        }

        class ViewApplicationAction : OpenUrlAction
        {
			private const string VIEW_APPLICATION_URL_TEMPLATE = "~/Seeker/Applications/Show.aspx?aid={0}&print=true";

            public ViewApplicationAction() : base("View") {}

            protected override string ConstructUrl(Match match)
            {
                return VIEW_APPLICATION_URL_TEMPLATE.Build(match.Application.Id);
            }
        }

        class ViewContactInfoAction : OpenUrlAction
        {
            private const string VIEW_CONTACT_INFO_URL_TEMPLATE = "~/Seeker/Scholarships/Show.aspx?id={0}#generalinfo-tab";

            public ViewContactInfoAction() : base("Contact") { }

            protected override string ConstructUrl(Match match)
            {
                return VIEW_CONTACT_INFO_URL_TEMPLATE.Build(match.Scholarship.Id);
            }
        }

        sealed class BlankAction : MatchAction
        {
            public static readonly MatchAction Instance = new BlankAction();
            
            private BlankAction()
            {
                Text = String.Empty;
                Execute = NoActionMethod;
            }

            private static void NoActionMethod(Match match) {}
        }
        #endregion
    }
}