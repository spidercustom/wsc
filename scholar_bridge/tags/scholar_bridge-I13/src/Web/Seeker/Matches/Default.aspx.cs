﻿using System;
using System.Linq;
using System.Collections.Generic;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Matches
{
	public partial class Default : SBBasePage
	{

        public IMatchService MatchService { get; set; }
		public IUserContext UserContext { get; set; }

	    private Domain.Seeker currentSeeker;

		protected void Page_Load(object sender, EventArgs e)
        {
		    UserContext.EnsureSeekerIsInContext();
		    currentSeeker = UserContext.CurrentSeeker;

            PopulatePage();
		}

	    private void PopulatePage()
	    {
            IList<Match> matches = MatchService.GetMatchesForSeeker(currentSeeker);
            IList<Match> qualify = matches.Where(m => m.MinimumCriteriaPercent() == 1).OrderBy(q => q.PreferredCriteriaPercent()).ToList();
            IList<Match> mayQualify = matches.Except(qualify).OrderBy(q => q.MinimumCriteriaPercent()).ToList();

	        qualifyList.Matches = qualify;
            qualifyList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
	        qualifyList.ActionText = "Add";
	        qualifyList.IsActionEnabled = (m => m.MatchStatus != MatchStatus.Saved);
	        qualifyList.ShowActions = UserContext.CurrentSeeker.IsPublished();

	        mayQualifyList.Matches = mayQualify;
            mayQualifyList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
            mayQualifyList.ActionText = "Add";
            mayQualifyList.IsActionEnabled = (m => m.MatchStatus != MatchStatus.Saved) ;
        mayQualifyList.ShowActions = UserContext.CurrentSeeker.IsPublished();
	    }

        protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.SaveMatch(currentSeeker, scholarshipId);
        }
	}
}