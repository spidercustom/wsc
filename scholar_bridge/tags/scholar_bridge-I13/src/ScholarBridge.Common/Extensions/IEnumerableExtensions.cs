﻿using System;
using System.Collections;

namespace ScholarBridge.Common.Extensions
{
    public static class IEnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable list, Action<T> action)
        {
            foreach (var listItem in list)
            {
                action((T)listItem);
            }
        }
    }
}
