﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(76)]
    public class AddSchololarshipServiceTypeRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBScholarship"; }
        }

        protected override string SecondTableName
        {
            get { return "SBServiceTypeLUT"; }
        }
    }
}
