﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;
using System.Data;

namespace ScholarBridge.Migrations
{
    [Migration(90)]
    public class AddRelationshipStage : AddTableBase
    {
        public const string TABLE_NAME = "SBRelationshipStageLUT";
        public const string PRIMARY_KEY_COLUMN = "SBRelationshipStageIndex";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return null;
            
        }


        public override Column[] CreateColumns()
        {
            return new Column[]
            {
                new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKey),
                new Column("Name", DbType.String, 50, ColumnProperty.NotNull), 
            };
        }

        //TODO: check if there is better/easy way to do this
        public override void AddDefaultData()
        {
            InsertAllFieldValue(new string[] { "0", "Pending" });
            InsertAllFieldValue(new string[] { "1", "Active" });
            InsertAllFieldValue(new string[] { "2", "InActive" });
            
        }
    }
}
