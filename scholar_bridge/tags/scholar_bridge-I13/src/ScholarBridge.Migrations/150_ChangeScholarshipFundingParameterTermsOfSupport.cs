﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(150)]
    public class ChangeScholarshipFundingParameterTermsOfSupport : Migration
    {
        public const string TERM_OF_SUPPORT_COUNT = "TermOfSupportUnitCount";
        public const string SCHOLARSHIP_TABLE_NAME = "SBScholarship";
        public const string TERM_OF_SUPPORT_ID = "SBTermOfSupportIndex";
        public const string TERM_OF_SUPPORT_TABLE_NAME = "SBTermOfSupportLUT";
        public static readonly string FK_TERM_OF_SUPPORT = string.Format("FK_{0}_{1}", SCHOLARSHIP_TABLE_NAME, TERM_OF_SUPPORT_ID);
        
        public override void Up()
        {
            Database.AddColumn(SCHOLARSHIP_TABLE_NAME, TERM_OF_SUPPORT_COUNT, DbType.Int16);
            Database.AddColumn(SCHOLARSHIP_TABLE_NAME, TERM_OF_SUPPORT_ID, DbType.Int32);
            Database.AddForeignKey(FK_TERM_OF_SUPPORT, SCHOLARSHIP_TABLE_NAME, TERM_OF_SUPPORT_ID, TERM_OF_SUPPORT_TABLE_NAME, TERM_OF_SUPPORT_ID);
        }

        public override void Down()
        {
            Database.RemoveForeignKey(SCHOLARSHIP_TABLE_NAME, FK_TERM_OF_SUPPORT);
            Database.RemoveColumn(SCHOLARSHIP_TABLE_NAME, TERM_OF_SUPPORT_COUNT);
            Database.RemoveColumn(SCHOLARSHIP_TABLE_NAME, TERM_OF_SUPPORT_ID);
        }
    }
}