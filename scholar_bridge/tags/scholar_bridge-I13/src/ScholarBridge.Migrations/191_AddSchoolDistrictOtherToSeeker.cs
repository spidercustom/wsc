using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(191)]
    public class AddSchoolDistrictOtherToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        private const string COLUMN = "CurrentSchoolDistrictOther";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, COLUMN, DbType.String, 250, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMN);
        }
    }
}