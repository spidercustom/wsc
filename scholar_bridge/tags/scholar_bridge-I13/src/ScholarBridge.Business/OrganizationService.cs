using System;
using System.Collections.Generic;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public abstract class OrganizationService<T> : IOrganizationService<T> where T : Organization
    {
        public IOrganizationDAL<T> OrganizationDAL { get; set; }
        public IRoleDAL RoleDAL { get; set; }

        public IUserService UserService { get; set; }
        public IMessagingService MessagingService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }

        protected abstract void AddRolesToNewUser(User user, bool isAdmin);

        protected abstract MessageType TemplateForApproval { get; }
        protected abstract MessageType TemplateForRejection { get; }

        public T FindById(int id)
        {
            return OrganizationDAL.FindById(id);
        }

        public void SaveNewWithAdminUser(T organization, User user)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }
			if (user.Id < 1)
				UserService.Insert(user);

            AddRolesToNewUser(user, true);
            UserService.Update(user);
            organization.AdminUser = user;
            OrganizationDAL.Insert(organization);

            UserService.SendConfirmationEmail(user, false);
        }

        public void SaveNewUser(T organization, User user)
        {
            SaveNewUser(organization, user, false);
        }

        public void SaveNewUser(T organization, User user, bool requireResetPassword)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            AddRolesToNewUser(user, false);
            user.IsApproved = true;
            organization.Users.Add(user);
            OrganizationDAL.Update(organization);

            UserService.SendConfirmationEmail(user, requireResetPassword);
        }

        public void DeleteUser(T organization, User user)
        {
            UserService.Delete(user);
        }

        public void ReactivateUser(T organization, User user)
        {
            user.IsDeleted = false;
            UserService.Update(user);
        }

        public void Update(T organization)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            OrganizationDAL.Update(organization);
        }

        public IList<T> FindPendingOrganizations()
        {
            return OrganizationDAL.FindAllPending();
        }

        public void Approve(T organization)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }
            organization.ApprovalStatus = ApprovalStatus.Approved;

            if (null != organization.AdminUser)
            {
				organization.AdminUser.IsApproved = true;
				organization.AdminUser.IsDeleted = false;
			}
            OrganizationDAL.Update(organization);

            // Send message to admin user that this organization has been approved
            var templateParams = new MailTemplateParams();
            TemplateParametersService.OrganizationApproved(organization.AdminUser, templateParams);
            var msg = new OrganizationMessage
                          {
                              MessageTemplate = TemplateForApproval,
                              To = new MessageAddress {Organization = organization},
                              LastUpdate = new ActivityStamp(organization.AdminUser)
                          };
            MessagingService.SendMessageFromAdmin(msg, templateParams, true);
        }

        public void Reject(T organization)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }
            organization.ApprovalStatus = ApprovalStatus.Denied;
            OrganizationDAL.Update(organization);

            // Send message to admin user that this organization has been rejected
            var templateParams = new MailTemplateParams();
            TemplateParametersService.OrganizationRejected(organization.AdminUser, templateParams);
            var msg = new OrganizationMessage
                          {
                              MessageTemplate = TemplateForRejection,
                              To = new MessageAddress {Organization = organization},
                              LastUpdate = new ActivityStamp(organization.AdminUser)
                          };
            MessagingService.SendMessageFromAdmin(msg, templateParams, true);
            
			//this will soft delete the user so that same email address can be used
			//again for registration
            DeleteUser(organization, organization.AdminUser);
        }

        public User FindUserInOrg(T organization, int userId)
        {
            return OrganizationDAL.FindUserInOrg(organization, userId);
        }

        public void SubmitListChangeRequest(T organization,User user,string listType, string value, string reason)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "organization can not be null");
            }
            if (null == user)
            {
                throw new ArgumentNullException("user", "user can not be null");
            }

            // Send message to wsc admin to add/change lists
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ListChangeRequest(organization,user,listType,value,reason,templateParams);
            var msg = new OrganizationMessage
            {
                MessageTemplate =  MessageType.ListChangeRequest,
                From = new MessageAddress { Organization = organization,User=user  },
                LastUpdate = new ActivityStamp(user )
            };
            MessagingService.SendMessageToAdmin(msg, templateParams, true);

        }
    }
}