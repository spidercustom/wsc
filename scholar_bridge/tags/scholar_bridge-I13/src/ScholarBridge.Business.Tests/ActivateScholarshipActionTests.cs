using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Actions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ActivateScholarshipActionTests
    {
        private MockRepository mocks;
        private ActivateScholarshipAction activateScholarship;
        private IScholarshipService scholarshipService;
        private IMessageService messageService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            scholarshipService = mocks.StrictMock<IScholarshipService>();
            messageService = mocks.StrictMock<IMessageService>();
            activateScholarship = new ActivateScholarshipAction
                                      {
                                          ScholarshipService = scholarshipService,
                                          MessageService = messageService
                                      };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(scholarshipService);
            mocks.BackToRecord(messageService);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void approve_with_wrong_message_type_throws_exception()
        {
            activateScholarship.Approve(new Message(), null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void reject_with_wrong_message_type_throws_exception()
        {
            activateScholarship.Reject(new Message(), null, null);
            Assert.Fail();
        }

        [Test]
        public void approve_with_scholarship()
        {
            var p = new Provider {AdminUser = new User()};
            var s = new Scholarship {Provider = p};
            var m = new ScholarshipMessage {RelatedScholarship = s};

            Expect.Call(() => scholarshipService.Approve(s, p.AdminUser));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateScholarship.Approve(m, null, p.AdminUser);

            mocks.VerifyAll();
        }

        [Test]
        public void reject_with_scholarship()
        {
            var p = new Provider { AdminUser = new User() };
            var s = new Scholarship { Provider = p };
            var m = new ScholarshipMessage { RelatedScholarship = s };

            Expect.Call(() => scholarshipService.Reject(s, p.AdminUser));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateScholarship.Reject(m, null, p.AdminUser);

            mocks.VerifyAll();
        }
    }
}