using System;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Domain
{
    public class Match : CriteriaCountBase, ISoftDeletable
    {
        public virtual int Id { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual Scholarship Scholarship { get; set; }

        public virtual Application Application { get; set; }

        public virtual MatchStatus MatchStatus { get; set; }
        public virtual bool IsDeleted { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual string ScholarshipName
        {
            get { return null == Scholarship ? null : Scholarship.DisplayName; }
        }

        public virtual MatchApplicationStatus MatchApplicationStatus
        {
            get
            {
                if (Scholarship.Stage==ScholarshipStages.Awarded || Scholarship.Stage==ScholarshipStages.Awarded) 
                    return Domain.MatchApplicationStatus.Closed;
                
                var result = MatchApplicationStatus.Unknown;
                 
                if (Application==null ) 
                    {
                        if (DateTime.Today< Scholarship.ApplicationStartDate) result = Domain.MatchApplicationStatus.NotApplied;

                        if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate,Scholarship.ApplicationDueDate))
                            result = Domain.MatchApplicationStatus.NotApplied;

                        if (DateTime.Today >= Scholarship.ApplicationDueDate) result = Domain.MatchApplicationStatus.Closed;

                        if (Scholarship.AwardPeriodClosed.HasValue) result = Domain.MatchApplicationStatus.Closed;
                    } 

                else if (Application.Stage == ApplicationStages.Submitted)
                    {

                        if (DateTime.Today < Scholarship.ApplicationStartDate) result = Domain.MatchApplicationStatus.Applied;

                        if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate,Scholarship.ApplicationDueDate))
                            result = Domain.MatchApplicationStatus.Applied;

                        if (DateTime.Today >= Scholarship.ApplicationDueDate) result = Domain.MatchApplicationStatus.BeingConsidered;
                        if (Scholarship.AwardPeriodClosed.HasValue) result = Domain.MatchApplicationStatus.BeingConsidered;
                    }

                else  
                {
                    if (DateTime.Today < Scholarship.ApplicationStartDate) result = Domain.MatchApplicationStatus.Appling;
                    
                    if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate,Scholarship.ApplicationDueDate))
                        result = Domain.MatchApplicationStatus.Appling;

                    if (DateTime.Today >= Scholarship.ApplicationDueDate) result = Domain.MatchApplicationStatus.Closed;

                    if (Scholarship.AwardPeriodClosed.HasValue) result = Domain.MatchApplicationStatus.Closed;
                }
                     
                return result; 
            }
        }

        public virtual string MatchApplicationStatusString
        {
            get { return MatchApplicationStatus.GetDisplayName(); }   
        }
    }
}