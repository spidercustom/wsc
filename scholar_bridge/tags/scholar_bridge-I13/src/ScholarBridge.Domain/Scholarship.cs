﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain
{
    public class Scholarship
    {
        private const string NOT_ACTIVATED_STATUS_TEXT = "Not Activated";
        public const int MAX_GPA = 5;

        public Scholarship()
        {
        	InitializeMembers();        
		}    	

		private void InitializeMembers()    	
		{
            FundingParameters = new FundingParameters();
            AdditionalRequirements = new List<AdditionalRequirement>();
    		AdditionalQuestions = new List<ScholarshipQuestion>();
    		FundingProfile = new FundingProfile();
    		Donor = new ScholarshipDonor();
    		SeekerProfileCriteria = new SeekerProfileCriteria();

    		Attachments = new List<Attachment>();
    	}

    	public virtual int Id { get; set; }

        [NotNullValidator]
        [StringLengthValidator(1, 100)]
        public virtual string Name { get; set; }

        public virtual AcademicYear AcademicYear { get; set; }

        [StringLengthValidator(0, 2000)]
        public virtual string MissionStatement { get; set; }

        [NotNullValidator(MessageTemplate="Application Start Date should be a valid date.")]
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? ApplicationStartDate { get; set; }
        
        [NotNullValidator]
        [PropertyComparisonValidator("ApplicationStartDate", ComparisonOperator.GreaterThan, MessageTemplate = "Application Due Date should be greater than Application Start Date")]
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? ApplicationDueDate { get; set; }
        
        [NotNullValidator]
        [PropertyComparisonValidator("ApplicationDueDate", ComparisonOperator.GreaterThan, MessageTemplate = "Award Date should be greater than Application Due Date")]
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? AwardDate { get; set; }

        public virtual DateTime? ActivatedOn { get; set; }

        public virtual DateTime? AwardPeriodClosed { get; set; }

        [StringLengthValidator(0, 2000)]
        public virtual string ProgramGuidelines { get; set; }
        public virtual ScholarshipDonor Donor { get; set; }

        [NotNullValidator]
        public virtual Provider Provider { get; set; }

        public virtual Intermediary Intermediary { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual ScholarshipStages Stage { get; set; }

        [NotNullValidator]
        [MinimumValidator(0)]
        public virtual decimal MinimumAmount { get; set; }

        [NotNullValidator]
        [MinimumValidator(1)]
        [PropertyComparisonValidator("MinimumAmount", ComparisonOperator.GreaterThanEqual, MessageTemplate = "Maximum amount should be greater than or equal to minimum amount")]
        public virtual decimal MaximumAmount { get; set; }
        public virtual FundingParameters FundingParameters { get; set; }

        public virtual string AmountRange
        {
            get { return String.Format("{0:$#,###;($#,###);$0} to {1:$#,###;($#,###);$0}", MinimumAmount, MaximumAmount); }
        }

        public virtual SeekerProfileCriteria SeekerProfileCriteria { get; set; }

        public virtual FundingProfile FundingProfile { get; set; }

        public virtual IList<Attachment> Attachments { get; protected set; }

		public virtual IList<ScholarshipQuestion> AdditionalQuestions { get; protected set; }

		public virtual AdminNotes AdminNotes { get; protected set; }
        public virtual bool? IsFAFSARequired { get; set; }
        public virtual bool? IsFAFSAEFCRequired { get; set; }

        public virtual bool? IsFamilyMembersInHouseHoldRequired { get; set; }

        public virtual bool? IsFamilyDependentsAttendingCollegeRequired { get; set; }

        public virtual bool? IsFamilyIncomeRequired { get; set; }

        public virtual decimal? FamilyIncomeFrom { get; set; }
        public virtual decimal? FamilyIncomeTo { get; set; }





        /// <summary>
        /// Remove a Question from the Scholarship.
        /// </summary>
        /// <remarks>
        /// You really need to use this to add Questions because of the oddities of one-to-many 
        /// relationships when the key on the child table is NOT NULL.
        /// </remarks>
        /// <param name="question"></param>
        public virtual void AddAdditionalQuestion(ScholarshipQuestion question)
        {
            question.Scholarship = this;
            AdditionalQuestions.Add(question);
        }

        public virtual void RemoveAdditionalQuestion(int position)
        {
            AdditionalQuestions.RemoveAt(position);
        }

		public virtual IList<AdditionalRequirement> AdditionalRequirements { get; protected set; }

		public virtual void ResetAdditionalRequirements(IList<AdditionalRequirement> additionalReqs)
        {
            AdditionalRequirements.Clear();
            foreach (var ar in additionalReqs)
            {
                AdditionalRequirements.Add(ar);
            }
        }

        public virtual string DisplayName
        {
            get
            {
                if (null != Donor && null != Donor.Name && ! String.IsNullOrEmpty(Donor.Name.Trim()))
                    return String.Format("{0} - {1}", Name, Donor.Name);
                return Name;
            }
        }


        public virtual bool IsAwardPeriodClosed
        {
            get { return AwardPeriodClosed.HasValue && AwardPeriodClosed.Value <= DateTime.Now; }
        }

        public virtual bool IsBelongToOrganization(Organization organization)
        {
            if (null == organization)
                return false;

            return BelongsToProvider(organization as Provider)
                   || BelongsToIntermediary(organization as Intermediary);
        }

        private bool BelongsToProvider(Provider provider)
        {
            if (null == provider)
                return false;

            return null != Provider && Provider.Id.Equals(provider.Id);
        }

        private bool BelongsToIntermediary(Intermediary intermediary)
        {
            if (null == intermediary)
                return false;

            return null != Intermediary && Intermediary.Id.Equals(intermediary.Id);
        }

        /// <summary>
        /// Append notes to the end of the AdminNotes.
        /// The user passed in will be atrributed.
        /// </summary>
        /// <param name="userAppendingNotes">The user to attribute the notes to.</param>
        /// <param name="notes">The test to add.</param>
        public virtual void AppendAdminNotes(User userAppendingNotes, string notes)
        {
			if (AdminNotes == null)
				AdminNotes = new AdminNotes();
			AdminNotes.AppendAdminNote(userAppendingNotes, notes);
        }

        #region ICloneable Members

        public virtual object Clone(string baseAttachmentPath)
        {
            var result = (Scholarship) MemberwiseClone();
            result.Id = 0;
            result.AcademicYear = AcademicYear.CurrentScholarshipYear;
            result.LastUpdate = null;
            result.Stage = ScholarshipStages.None;
            result.SeekerProfileCriteria = (SeekerProfileCriteria) SeekerProfileCriteria.Clone();
            result.FundingProfile = (FundingProfile)FundingProfile.Clone();

            result.AdditionalRequirements = new List<AdditionalRequirement>(AdditionalRequirements);

            result.AdditionalQuestions = new List<ScholarshipQuestion>(AdditionalQuestions.Count);
			foreach (var question in AdditionalQuestions)
			{
				var clonedQuestion = (ScholarshipQuestion) question.Clone();
				clonedQuestion.Scholarship = result;
				result.AdditionalQuestions.Add(clonedQuestion);
			}

            result.Attachments = new List<Attachment>(Attachments.Count);
            foreach (var attachment in Attachments)
            {
                var clonedAttachment = (Attachment) attachment.Clone(baseAttachmentPath);
                result.Attachments.Add(clonedAttachment);
            }

            return result;
        }

        #endregion

        public virtual string GetStatusText()
        {
            if (Stage.IsIn(ScholarshipStages.None, ScholarshipStages.NotActivated))
                return NOT_ACTIVATED_STATUS_TEXT;

            return Stage.GetDisplayName();
        }

        #region stage based methods
        public virtual bool IsRequestedForActivation()
        { 
            return Stage == ScholarshipStages.RequestedActivation;
        }

        public virtual bool IsRejected()
        {
            return Stage == ScholarshipStages.Rejected; 
        }

        public virtual bool IsActivated()
        {
            return Stage == ScholarshipStages.Activated || Stage == ScholarshipStages.Awarded;
        }

        public virtual bool CanSubmitForActivation()
        {
            return !IsActivated() && !IsRequestedForActivation();
        }

        public virtual bool CanEdit()
        {
            return !IsInActivationProcess();
        }

        public virtual bool IsInActivationProcess()
        {
            return IsRequestedForActivation() || IsActivated();
        }
        #endregion
    }
}
