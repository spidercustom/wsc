﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    public enum MatchApplicationStatus
    {
        Unknown,

        [DisplayName("Not Started")]
        NotApplied,
        
        [DisplayName("Started")]
        Appling,

        [DisplayName("Submitted")]
        Applied,

        [DisplayName("Being considered")]
        BeingConsidered,

        Offerred,
        Closed,
        Awarded
    }
}
