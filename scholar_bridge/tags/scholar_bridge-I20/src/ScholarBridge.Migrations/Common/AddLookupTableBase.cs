﻿using Migrator.Framework;

namespace ScholarBridge.Migrations.Common
{
    public abstract class AddLookupTableBase : AddTableBase
    {
        protected abstract string IndicativeName
        { get; }

        public override string TableName
        {
            get { return LookupTableHelper.CreateTableName(IndicativeName); }
        }

        public override string PrimaryKeyColumn
        {
            get { return LookupTableHelper.CreatePrimaryKeyColumnName(IndicativeName); }
        }

        public override Column[] CreateColumns()
        {
            return LookupTableHelper.MergeNameWithStandardFields(IndicativeName, 20);
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new[]
                       {
                           CreateForeignKey("LastUpdateBy", "SBUser", "SBUserID")
                       };
        }
    }
}
