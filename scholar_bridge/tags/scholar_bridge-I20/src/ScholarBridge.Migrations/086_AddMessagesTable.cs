using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(86)]
    public class AddMessagesTable : AddTableBase
    {
        public const string TABLE_NAME = "SBMessage";
        public const string PRIMARY_KEY_COLUMN = "SBMessageID";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("Subject", DbType.String, 100, ColumnProperty.NotNull),
                           new Column("Content", DbType.String, 8000, ColumnProperty.NotNull),
                           new Column("SentDate", DbType.DateTime, ColumnProperty.NotNull),
                           new Column("IsRead", DbType.Boolean, ColumnProperty.NotNull, "0"),
                           new Column("IsArchived", DbType.Boolean, ColumnProperty.NotNull, "0"),
                           new Column("ToUserId", DbType.Int32, ColumnProperty.Null),
                           new Column("ToRoleId", DbType.Int32, ColumnProperty.Null),
                           new Column("ToOrganizationId", DbType.Int32, ColumnProperty.Null),
                           new Column("FromUserId", DbType.Int32, ColumnProperty.Null),
                           new Column("FromRoleId", DbType.Int32, ColumnProperty.Null),
                           new Column("FromOrganizationId", DbType.Int32, ColumnProperty.Null),
                           new Column("InResponseToMessageId", DbType.Int32, ColumnProperty.Null),
                           new Column("MessageTemplate", DbType.String, 50, ColumnProperty.Null),
                           new Column("RelatedOrganizationId", DbType.Int32, ColumnProperty.Null),
                           new Column("RelatedScholarshipId", DbType.Int32, ColumnProperty.Null),
                           new Column("MessageType", DbType.String, 50, ColumnProperty.NotNull),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())")
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new[]
                       {
                           CreateForeignKey("InResponseToMessageId", TABLE_NAME, PRIMARY_KEY_COLUMN),
                           CreateForeignKey("ToUserId", "SBUser", "SBUserID"),
                           CreateForeignKey("FromUserId", "SBUser", "SBUserID"),
                           CreateForeignKey("ToRoleId", "SBRoleLUT", "SBRoleIndex"),
                           CreateForeignKey("FromRoleId", "SBRoleLUT", "SBRoleIndex"),
                           CreateForeignKey("ToOrganizationId", "SBOrganization", "SBOrganizationID"),
                           CreateForeignKey("FromOrganizationId", "SBOrganization", "SBOrganizationID"),
                           CreateForeignKey("RelatedOrganizationId", "SBOrganization", "SBOrganizationID"),
                           CreateForeignKey("RelatedScholarshipId", "SBScholarship", "SBScholarshipID"),
                           CreateForeignKey("LastUpdateBy", "SBUser", "SBUserID")
                       };
        }
    }
}