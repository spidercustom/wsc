using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(188)]
    public class AddApplicationServiceTypeRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationServiceTypeRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBServiceTypeLUT"; }
        }
    }
}