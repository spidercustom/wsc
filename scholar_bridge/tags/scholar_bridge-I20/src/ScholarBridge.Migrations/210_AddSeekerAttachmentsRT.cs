using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(210)]
    public class AddSeekerAttachmentsRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBSeekerAttachmentRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBAttachment"; }
        }
    }
}