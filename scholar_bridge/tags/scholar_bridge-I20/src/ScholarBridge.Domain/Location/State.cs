﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.Location
{
    [Serializable]
    public class State : ILookup
    {
        [StringLengthValidator(2,2)]
        public virtual string Abbreviation { get; set; }

        [StringLengthValidator(0, 30)]
        public virtual string Name { get; set; }


        #region ILookup Members

        object ILookup.Id
        {
            get
            {
                return Abbreviation;
            }
            set
            {
                Abbreviation = value.ToString();
            }
        }

        int ILookup.GetIdAsInteger()
        {
            throw new NotImplementedException();
        }

        string ILookup.GetIdAsString()
        {
            return Abbreviation;
        }

        string ILookup.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }

        string ILookup.Description
        {
            get
            {
                return Name;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        ActivityStamp ILookup.LastUpdate
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IDeprecatable Members

        bool IDeprecatable.Deprecated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}