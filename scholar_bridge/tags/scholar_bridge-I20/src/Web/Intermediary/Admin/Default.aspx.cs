﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Intermediary.Admin
{
	public partial class Default : System.Web.UI.Page
	{
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = UserContext.CurrentIntermediary;

            editOrg.Organization = intermediary;

            relationshiplist.Relationships = RelationshipService.GetByIntermediary(intermediary);
            orgUserList.Users = intermediary.Users;
            adminDetails.UserToView = intermediary.AdminUser;
            adminDetails1.UserToView = intermediary.AdminUser;
			LinkGenerator linker = new LinkGenerator();
			createUserLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", linker.GetFullLink("Intermediary/Users/Create.aspx?popup=true"));
			addRequestLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", linker.GetFullLink("Intermediary/Relationships/Create.aspx?popup=true"));
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!User.IsInRole(Role.INTERMEDIARY_ADMIN_ROLE))
            {
                createUserLink.Visible = false;
                editOrg.ViewOnlyMode = true;
            }
        }
        protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            IntermediaryService.Update((Domain.Intermediary)org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Intermediary/Admin");
        }
	}
}
