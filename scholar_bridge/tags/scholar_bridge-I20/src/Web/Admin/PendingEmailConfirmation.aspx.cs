﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin
{
    public partial class PendingEmailConfirmation : System.Web.UI.Page
    {
		public IIntermediaryService IntermediaryService { get; set; }
		public IProviderService ProviderService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
			pendingEmailConfirm.Organizations = IntermediaryService.FindPendingOrganizations()
				.Cast<Organization>().ToList();
			IList<Organization> providerOrgs = ProviderService.FindPendingOrganizations()
					.Cast<Organization>().ToList();
			foreach(Organization o in providerOrgs)
				pendingEmailConfirm.Organizations.Add(o);
		}
    }
}