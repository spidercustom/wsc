﻿<%@ Page Title="Provider" Language="C#"  AutoEventWireup="true"
    CodeBehind="Anonymous.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Anonymous" %>
<%@ Register src="~/Common/ScholarshipSearchBox.ascx" tagname="ScholarshipSearchBox" tagprefix="sb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<%@ Register src="~/Common/GlobalFooter.ascx" tagname="GlobalFooter" tagprefix="sb" %>
<%@ Register src="~/Common/ProviderMenu.ascx" tagname="ProviderMenu" tagprefix="sb" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>

<HTML xmlns="http://www.w3.org/1999/xhtml"></HTML>

<TITLE>The WashBoard.org - Seeker's Home</TITLE>
    <META name="FORMAT" content="text/html">
    <META name="CHARSET" content="ISO-8859-1">
    <META name="DOCUMENTLANGUAGECODE" content="en">
    <META name="DOCUMENTCOUNTRYCODE" content="us">
    <META name="DC.LANGUAGE" scheme="rfc1766" content="en-us">
    <META name="COPYRIGHT" content="Copyright (c) 2009 by Washington Scholarship Coalition">
    <META name="SECURITY" content="Public">
    <META name="ROBOTS" content="index,follow">
    <META name="GOOGLEBOT" content="index,follow">
    <META name="Description" content="The WashBoard.org Provider Home ">
    <META name="Keywords" content="">
    <META name="Author" content="The WashBoard.org">
    <!-- Base keywords here-->
    <SCRIPT type="text/javascript" src="https://www.google.com/jsapi"></SCRIPT>
    <SCRIPT type="text/javascript">
        google.load("jquery", "1.3");
    </SCRIPT>
   <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.7.1.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tablesorter.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.pager.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tinysort.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskedinput.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.cookie.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.watermark-2.0.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/superfish.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/site.js") %>"></script>
        <%--<link rel="stylesheet" href="<%= ResolveUrl("~/styles/main.css") %>" type="text/css" />--%>
        <LINK href="<%= ResolveUrl("~/styles/WSCStyles.CSS") %>" rel="stylesheet" type="text/css" media="All"/>
</HEAD>
<BODY>
<form id="default" runat="server">
<!--Page wrapper starts here-->
<DIV id="EntirePageWrapper">


    <DIV id="HeaderWrapper">
          <DIV id="Logo"><img src="<%= ResolveUrl("~/images/LeftShadow_Logo.gif") %>" width="22px" height="137px"><A href="<%= ResolveUrl("~/") %>"><img src="<%= ResolveUrl("~/images/LogoTheWashBoard.gif") %>" width="234px" height="137px"></A></DIV>
          <div id="successMessage"><sb:SuccessMessageLabel ID="SuccessMessageLabel2" runat="server" ></sb:SuccessMessageLabel></div>
          <DIV id="LogoRight"><img src="<%= ResolveUrl("~/images/LogoRight.gif") %>" width="640px" height="137px"><img src="<%= ResolveUrl("~/images/RightShadow_Logo.gif") %>" width="22px" height="137px;"></DIV>
     </DIV>

     <!--Welcome text starts here-->
    <!-- <DIV id="WelcomeTextContainer">
          <DIV class="WelcomeText">Welcome back Brad!!</DIV>

     </DIV>   -->
     <!--Welcome text ends here-->

     <!--topmenu starts here-->
     <DIV Id="MenuContainer">
        <A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/NavSeeker_MyProfileInactive.gif") %>" width="124px" height="36px"></A><img src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px"><A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/NavSeeker_MyMatchesInactive.gif") %>" width="113px" height="36px"></A><img src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px"><A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/NavSeeker_MyApplicationsInactive.gif") %>" width="141px" height="36px"></A><img src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px"><A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/NavSeeker_MySettingsInactive.gif") %>" width="111px" height="36px"></A><img src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px"><A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/NavSeeker_MyMegsInactive.gif") %>" width="126px" height="36px"></A>
     </DIV>
<!--Global nav links start here-->

      <DIV id="GlobaNavLinksContainerSeeker">
          <div class="GlobalNavLinks">
          <sb:ScholarshipSearchBox runat="server" />
          </DIV>

      </DIV>
      
      <!--Branding picture starts here-->

       <DIV><img src="<%= ResolveUrl("~/images/PicTopSeekerHome.gif") %>" width="918px" height="15px"></DIV>
       <DIV><img src="<%= ResolveUrl("~/images/PicBottomSeekerHome.gif") %>" width="918px" height="265px"></DIV>
      <!--Branding picture ends here-->

      <!--This is the outer most wrapper 01 starts here-->
      <DIV id="ContentWrapper01">
           <DIV id="LeftPageShadow"><img src="<%= ResolveUrl("~/images/LeftContentShadow.gif") %>"></DIV>
           <DIV id="RightPageShadow"><img src="<%= ResolveUrl("~/images/RightContentShadow.gif") %>"></DIV>
           <!--This is content wrapper 02 starts here-->
           <DIV id="ContentWrapper02">
           
               <!--Left floated content area starts here-->
                 <DIV id="HomeContentLeft">
                  <Img src="<%= ResolveUrl("~/images/PgTitle_SmarterScholarshipMatches.gif") %>" width="340px" height="54px">
                  <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerUnlogged.gif") %>" width="513px" height="96px">
                 </DIV>
                  <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <DIV id="HomeContentRight">
                     <div  class="LoginContainer">
                         <DIV class="Vertical">
                                <sb:Login ID="loginForm" runat="server" LinkTo="~/Seeker/Register.aspx"  />
                         </DIV>
                     </DIV>
                 </DIV>
                 <!--Right Floated content area ends here-->
                 <BR><BR>

                 <DIV id="DIV1"></DIV>
                



                <div style='clear: both;'></DIV>


                 <!--The Three column starts here-->
                 <DIV id="BoxWrapper">
                      <DIV id="LeftBottomBox">
                           <A href="<%= ResolveUrl("~/Seeker/Register.aspx") %>"><Img src="<%= ResolveUrl("~/images/BottomBox01_StepOne.gif") %>" width="262px" height="51px"></A>
                           <A href="<%= ResolveUrl("~/Seeker/Register.aspx") %>"><Img src="<%= ResolveUrl("~/images/SeekerBottomBox01.gif") %>" width="237px" height="86px"></A>

                           <P style="width:237px">Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                           <p class="IcoBoxArrow"><A href="<%= ResolveUrl("~/Seeker/Register.aspx") %>"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" class="GreenArrow">&nbsp;Register</A></P>

                      </DIV>
                      <DIV id="CenterBottomBox">
                            <A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/BottomBox02_StepTwo.gif") %>" width="262px" height="51px"></A>
                            <A href="#" onclick="loginalert();"><Img src="<%= ResolveUrl("~/images/SeekerBottomBox02.gif") %>" width="237px" height="86px"></A>
                            <P style="width:237px">Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                            <p class="IcoBoxArrow"><A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" class="GreenArrow">&nbsp;Create your profile</A></P>
                      </DIV>
                      <DIV id="RightBottomBox">
                            <A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/BottomBox03_StepThree.gif") %>" width="262px" height="51px"></A>
                            <A href="#" onclick="loginalert();"><Img src="<%= ResolveUrl("~/images/SeekerBottomBox03.gif") %>" width="237px" height="86px"></A>
                            <P style="width:237px">Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                            <p class="IcoBoxArrow"><A href="#" onclick="loginalert();"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" class="GreenArrow">&nbsp;Go to my matches </A></P>
                      </DIV>

                 </DIV> <BR>

                  <DIV id="Clear"></DIV>
                 <HR>


            <DIV id="Footer">
                    <sb:GlobalFooter ID="GlobalFooter1" runat="server" />
                </DIV>

           </DIV>
           <!--This is content wrapper 02 ends here-->




      </DIV>
      <!--This is the outer most wrapper 01 ends here-->


</DIV>
<!--Page wrapper ends here-->
</form>
</BODY>
</HTML>
