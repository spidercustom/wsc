﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Message.Default" Title="Message" %>

<%@ Register TagPrefix="sb" TagName="MessageList" Src="~/Common/MessageList.ascx" %>
<%@ Register TagPrefix="sb" TagName="SentMessageList" Src="~/Common/SentMessageList.ascx" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
        <asp:LoginView ID="loginView2" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Intermediary, Intermediary Admin,Provider, Provider Admin,Admin, WSCAdmin">
                        <ContentTemplate>
                            <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
                            <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOurMessages.gif" width="918px" height="170px" runat="server" /></div>                 
                        </ContentTemplate>
                    </asp:RoleGroup>
                     <asp:RoleGroup Roles="Seeker">
                        <ContentTemplate>
                            <div><asp:Image ID="Image1" ImageUrl="~/images/PictopMyMessages.gif" Width="918px" Height="15px" runat="server" /></div>
                            <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomMyMessages.gif" width="918px" height="170px" runat="server" /></div>                 
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
        </asp:LoginView>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
 
  <asp:LoginView ID="loginView3" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Intermediary, Intermediary Admin,Provider, Provider Admin">
                        <ContentTemplate>
                        <P class="PageTitle"><Img src="<%= ResolveUrl("~/images/PgTitle_OurMsgs.gif") %>" ></p>
                        </ContentTemplate>
                    </asp:RoleGroup>
                     <asp:RoleGroup Roles="Seeker">
                        <ContentTemplate>
                            <!--Left floated content area starts here-->
                            <DIV id="HomeContentLeft">

                              <Img src="<%= ResolveUrl("~/images/PgTitle_MyMessages.gif") %>" width="157px" height="54px">
                              <img src="<%= ResolveUrl("~/images/EmphasisedMyMessagesPage.gif") %>" width="513px" height="96px">
            
                             </DIV>
                              <!--Left floated content area ends here-->

                             <!--Right Floated content area starts here-->
                             <DIV id="HomeContentRight">
                             <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                             </DIV>
                            <BR>

                             <DIV id="Clear"></DIV>                        
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
        </asp:LoginView>
 
 
 
 <P class="HighlightedTextMain"><asp:Label ID="lblPageText" runat="server" /></P>
        <div class="tabs">
            <ul>
                <li><a href="#work-tab"><span>Inbox</span></a></li>
                <asp:LoginView ID="loginView" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
                        <ContentTemplate>
                <li><a href="#denied-tab"><span>Denied</span></a></li>
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
                </asp:LoginView>
                <li><a href="#archived-tab"><span>Archived</span></a></li>
                <li><a href="#sent-tab"><span>Sent</span></a></li>
            </ul>
            <div id="work-tab">
               <h3>Inbox</h3>
                <sb:MessageList id="messageList" runat="server" LinkTo="~/Message/Show.aspx" />
            </div>
            <asp:LoginView ID="loginView1" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
                        <ContentTemplate>            
            <div id="denied-tab">
                <h3>Denied</h3>
                <sb:MessageList id="MessageList2" runat="server" MessageAction="Deny" LinkTo="~/Message/Show.aspx" />
            </div>
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
            </asp:LoginView>
            <div id="archived-tab">
                <h3>Archived</h3>
                <sb:MessageList id="archivedMessageList" runat="server" Archived="true" LinkTo="~/Message/Show.aspx" />
            </div>
            <div id="sent-tab">
                <h3>Sent</h3>
                <sb:SentMessageList id="sentMessageList" runat="server" LinkTo="~/Message/Show.aspx" />
            </div>
        </div>        
</asp:Content>
