﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminScholarshipNotes.ascx.cs" Inherits="ScholarBridge.Web.Common.AdminScholarshipNotes" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
  

<div class="form-iceland">
    <p class="form-section-title">Scholarship Notes</p>

    <label for="existingNotesTb">Administrative Notes:</label>
    <asp:panel ID="existingNotesPanel" runat="server" ScrollBars="Vertical" CssClass="adminNoteDisplay">
        <asp:Literal ID="notesLiteral" runat="server"></asp:Literal>
    </asp:panel>
    <br />
    <label for="existingNotesTb">Add Notes:</label>  
    <asp:TextBox ID="newNotesTb" runat="server" TextMode="MultiLine" CssClass="adminNoteEntry" Columns="300" Rows="5"></asp:TextBox><br />
    <br />
    <sbCommon:AnchorButton ID="addNotes" runat="server" Text="Add Note" onclick="addNotes_Click"  />
</div>
<br />