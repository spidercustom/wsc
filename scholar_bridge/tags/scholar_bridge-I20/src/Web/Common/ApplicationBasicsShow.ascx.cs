﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Seeker.BuildApplication;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationBasicsShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                applicantName.Text = ApplicationToView.ApplicantName.NameFirstLast;
                applicantEmail.Text = ApplicationToView.Email;
                if (ApplicationToView.DateOfBirth.HasValue)
                    dob.Text = ApplicationToView.DateOfBirth.Value.ToString("MM/dd/yyyy");
                if (null != ApplicationToView.Phone)
                    phoneNumber.Text = ApplicationToView.Phone.Number;
                if (null != ApplicationToView.MobilePhone)
                    mobileNumber.Text = ApplicationToView.MobilePhone.Number;
                gender.Text = ApplicationToView.Gender.ToString();

                if (null != ApplicationToView.Address)
                    address.Text = ApplicationToView.Address.ToString().Replace("\r\n", "<br/>\r\n");

                if (null != ApplicationToView.County)
                    county.Text = ApplicationToView.County.Name;

                religions.DataSource = ApplicationToView.Religions;
                religions.DataBind();
                religionsOther.Text = ApplicationToView.ReligionOther;

                heritages.DataSource = ApplicationToView.Ethnicities;
                heritages.DataBind();
                heritagesOther.Text = ApplicationToView.EthnicityOther;

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);
            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            EthnicityRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Ethnicity);
            ReligionRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Religion);
            GendersRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Gender);
        }

        public override int ResumeFrom
        {
            get { return WizardStepName.Basics.GetNumericValue(); }
        }
    }
}