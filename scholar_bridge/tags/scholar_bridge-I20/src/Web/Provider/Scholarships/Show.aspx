﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Show" %>
<%@ Register src="~/Common/ShowScholarship.ascx" tagname="ShowScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
    <sb:PrintView id="PrintViewControl" runat="server" /> 
</asp:Content>


<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HideInPrintView="true" />
<div id="linkarea" class="exclude-in-print">
        <sbCommon:ConfirmButton ID="deleteConfirmBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
        Text="Delete Scholarship" Width="150px" runat="server" OnClickConfirm="deleteConfirmBtn_Click" />
        <br /><br />
        
    <sbCommon:AnchorButton ID="linkCopy" runat="server" Text="Copy this Scholarship to create new" />
</div>

<div id="deleteConfirmDiv" title="Confirm delete" style="display:none">
        Deleting the scholarship will remove it from the system. 
        Only scholarships that have yet to be activated can be deleted. 
    <br /> <br />Are you sure want to delete?
</div>   

<sb:ShowScholarship id="showScholarship" runat="server" 
    LinkTo="~/Provider/BuildScholarship/Default.aspx" 
    ApplicationLinkTo="~/Provider/Scholarships/ShowApplication.aspx"
    LinkToEditProgramGuideLines ="~/Provider/Scholarships/EditProgramGuidelines.aspx"    />

<asp:ScriptManager ID="ConfirmButtoncriptmanager" runat="server" />

</asp:Content>