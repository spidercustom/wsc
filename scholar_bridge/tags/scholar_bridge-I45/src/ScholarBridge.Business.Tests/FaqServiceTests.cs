using System;
using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class FaqServiceTests
    {
        private FaqService faqService;
        private MockRepository mocks;
        private IFaqDAL faqDAL;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            faqDAL = mocks.StrictMock<IFaqDAL>();
            faqService = new FaqService()
            {
                FaqDAL = faqDAL
            };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(faqDAL);
        }

        [Test]
        public void should_get_faq_by_id()
        {
            Expect.Call(faqDAL.FindById(1)).Return(new Faq {Id = 1});

            mocks.ReplayAll();

            Faq f = faqService.GetById(1);
            Assert.IsNotNull(f);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void should_throw_exception_on_save_of_null()
        {
            faqService.Save(null);
            Assert.Fail();
        }

        [Test]
        public void should_save_faq()
        {
            var f = new Faq {Id = 1};
            Expect.Call(faqDAL.Save(f)).Return(f);

            mocks.ReplayAll();

            faqService.Save(f);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void should_throw_exception_on_delete_of_null()
        {
            faqService.Delete(null);
            Assert.Fail();
        }

        [Test]
        public void should_delete_faq()
        {
            var f = new Faq { Id = 1 };
            Expect.Call(() => faqDAL.Delete(f));

            mocks.ReplayAll();

            faqService.Delete(f);
            mocks.VerifyAll();
        }

        [Test]
        public void should_find_all_when_empty()
        {
            Expect.Call(faqDAL.FindAll("Question")).Return(new List<Faq>());

            mocks.ReplayAll();

            IList<Faq> found = faqService.FindAll();
            mocks.VerifyAll();
        }

        [Test]
        public void should_find_all_when_found()
        {
            Expect.Call(faqDAL.FindAll("Question")).Return(new List<Faq> { new Faq { Question = "A" }, new Faq { Question = "B"} });

            mocks.ReplayAll();

            IList<Faq> found = faqService.FindAll();
            Assert.AreEqual(2, found.Count);
            Assert.AreEqual("A", found[0].Question);
            mocks.VerifyAll();
        }

        [Test]
        public void should_find_all_by_type_when_empty()
        {
            Expect.Call(faqDAL.FindAll("Question")).Return(new List<Faq>());

            mocks.ReplayAll();

            IList<Faq> found = faqService.FindAll(FaqType.General);
            mocks.VerifyAll();
        }

        [Test]
        public void should_find_all_by_type_when_found()
        {
            Expect.Call(faqDAL.FindAll("Question")).Return(new List<Faq> { new Faq { Question = "A", FaqType = FaqType.General }, new Faq { Question = "B", FaqType = FaqType.Provider } });

            mocks.ReplayAll();

            IList<Faq> found = faqService.FindAll(FaqType.General);
            Assert.AreEqual(1, found.Count);
            Assert.AreEqual(FaqType.General, found[0].FaqType);
            mocks.VerifyAll();
        }
    }
}