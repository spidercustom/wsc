﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SeekerStatuses
    {
        [DisplayName("Full-time")]
        [DisplayOrder(1)]
        FullTime = 1,
        [DisplayName("Half-time")]
        [DisplayOrder(2)]
        PartTime = 2,
        [DisplayName("Less than half-time")]
        [DisplayOrder(3)]
        LessThanPartTime = 4
    }
}
