﻿namespace ScholarBridge.Domain.Lookup
{
    public class HighSchool : LookupBase
    {
		public virtual string City { get; set; }
		public virtual string State { get; set; }
		public virtual SchoolDistrict District { get; set; }
	}
}
