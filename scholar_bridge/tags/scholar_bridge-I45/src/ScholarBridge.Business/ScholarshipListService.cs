﻿using System;
using System.IO;
using System.Net;
using ScholarBridge.Data;


namespace ScholarBridge.Business
{
	public class ScholarshipListService : IScholarshipListService
	{
    	public const string SCHOLARSHIP_LIST_FILE_NAME = "Scholarships.xml";
		public IScholarshipDAL ScholarshipDAL { get; set; }
		public IFtpService FtpService { get; set; }

		/// <summary>
		/// local physical directory path set from config via DI
		/// </summary>
		public string ScholarshipListDirectory { get; set; }

		public string GetScholarshipList()
		{
			string xml = ScholarshipDAL.GetScholarshipListXml();
			if (string.IsNullOrEmpty(xml))
				xml = "<doc></doc>"; // assure valid xml even if the list is empty.
			return xml;
		}

		public void GenerateAndStoreScholarshipList()
		{
			string scholarshipListXml = GetScholarshipList();
			string scholarshipListFilePath = ScholarshipListDirectory.EndsWith("\\")
												? ScholarshipListDirectory
												: ScholarshipListDirectory + "\\";
			scholarshipListFilePath += SCHOLARSHIP_LIST_FILE_NAME;
			File.WriteAllText(scholarshipListFilePath, scholarshipListXml);
			if (FtpService.FTPEnabled)
				FtpService.PutFileToPublicServer(SCHOLARSHIP_LIST_FILE_NAME, scholarshipListFilePath);
		}
	}
}
