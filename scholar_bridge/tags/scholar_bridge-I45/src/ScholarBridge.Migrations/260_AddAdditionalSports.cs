﻿using System;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(260)]
	public class AddAdditionalSports : Migration
	{
		private string LOOKUP_TABLE = "SBSportLUT";

		private string[] COLUMNS = new []
		                           	{
		                           		"Sport", "Description", "Deprecated", "LastUpdateBy", "LastUpdateDate"
		                           	};

		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			foreach (var value in LOOKUP_VALUES)
			{
				AddLookup(value, adminId);	
			}

		}

		public override void Down()
		{
			foreach (var value in LOOKUP_VALUES)
			{
				try
				{
					Database.Delete(LOOKUP_TABLE, COLUMNS[0], value);
				}
				catch (Exception)
				{
					// do nothing in-case the lookup value is already attached 
				}
			}
		}

		private void AddLookup(string lookupValue, int adminId)
		{
			int hitCount = (int)Database.ExecuteScalar("select count(*) from " + LOOKUP_TABLE + " where " + COLUMNS[0] + " = '" + lookupValue + "'");
			if (hitCount == 0)
				Database.Insert(LOOKUP_TABLE, COLUMNS,
								new[] { lookupValue, lookupValue, "0", adminId.ToString(), DateTime.Now.ToString() }
					);
		}

		private string[] LOOKUP_VALUES = new []
		                                	{
												"Aerobics",
												"Archery",
												"Badminton",
												"Billiards",
												"Bowling",
												"Crew/Rowing",
												"Cycling",
												"Diving",
												"Equestrian",
												"Fencing",
												"Field Hockey",
												"Fishing",
												"Football (American)",
												"Gymnastics",
												"Handball",
												"Ice Hockey",
												"Martial Arts",
												"Mountain Biking",
												"Racquetball",
												"Rodeo",
												"Rugby",
												"Running",
												"Sailing",
												"Sandboarding",
												"Skating",
												"Skiing (Cross Country)",
												"Skiing (Downhill)",
												"Skiing (Water)",
												"Snowboarding",
												"Soccer",
												"Softball",
												"Surfing",
												"Swimming",
												"Table Tennis",
												"Tennis",
												"Track and Field",
												"Ultimate Frisbee",
												"Volleyball",
												"Weightlifting"
		                                	};
	}
}
