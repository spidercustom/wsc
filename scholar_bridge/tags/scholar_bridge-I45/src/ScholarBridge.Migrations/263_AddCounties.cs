﻿using System;
using System.Collections.Generic;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(263)]
	public class AddCounties : Migration
	{
		private string LOOKUP_TABLE = "SBCountyLUT";

		private string[] COLUMNS = new []
		                           	{
		                           		"County", "Description", "Deprecated", "LastUpdateBy", "LastUpdateDate", "StateAbbreviation"
		                           	};

		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			foreach (var value in LOOKUP_VALUES)
			{
				AddLookup(value, adminId);	
			}

		}

		public override void Down()
		{
			foreach (var value in LOOKUP_VALUES)
			{
				try
				{
					Database.Delete(LOOKUP_TABLE, COLUMNS[0], value.Key);
				}
				catch (Exception)
				{
					// do nothing in-case the lookup value is already attached 
				}
			}
		}

		private void AddLookup(KeyValuePair<string, string> lookupValue, int adminId)
		{
			int hitCount = (int)Database.ExecuteScalar("select count(*) from " + LOOKUP_TABLE + " where " + COLUMNS[0] + " = '" + lookupValue.Key + "'");
			if (hitCount == 0)
				Database.Insert(LOOKUP_TABLE, COLUMNS,
								new[] { lookupValue.Key, lookupValue.Value, "0", adminId.ToString(), DateTime.Now.ToString(), "WA" }
					);
		}

		private Dictionary<string, string> LOOKUP_VALUES = new Dictionary<string, string>
		                                	{
												{"Adams", "AD"},
												{"Asotin", "AS"},
												{"Benton", "BE"},
												{"Chelan", "CH"},
												{"Clallam", "CL"},
												{"Clark", "CK"},
												{"Columbia", "CO"},
												{"Cowlitz", "CW"},
												{"Douglas", "DO"},
												{"Ferry", "FE"},
												{"Franklin", "FR"},
												{"Garfield", "GA"},
												{"Grant", "GR"},
												{"Grays Harbor", "GH"},
												{"Island", "IS"},
												{"Jefferson", "JE"},
												{"King", "KG"},
												{"Kitsap", "KI"},
												{"Kittitas", "KT"},
												{"Klickitat", "KL"},
												{"Lewis", "LE"},
												{"Lincoln", "LN"},
												{"Mason", "MA"},
												{"Okanogan", "OK"},
												{"Pacific", "PA"},
												{"Pend Oreille", "PE"},
												{"Pierce", "PC"},
												{"San Juan", "SJ"},
												{"Skagit", "SK"},
												{"Skamania", "SM"},
												{"Snohomish", "SN"},
												{"Spokane", "SP"},
												{"Stevens", "ST"},
												{"Thurston", "TH"},
												{"Wahkiakum", "WK"},
												{"Walla Walla", "WL"},
												{"Whatcom", "WH"},
												{"Whitman", "WT"},
												{"Yakima", "YA"}		                                	
											};
	}
}
