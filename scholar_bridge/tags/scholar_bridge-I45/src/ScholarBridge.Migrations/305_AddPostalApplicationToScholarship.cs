using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(305)]
    public class AddPostalApplicationToScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        private const string COLUMNA = "IsUsePostalApplication";
        

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, COLUMNA, DbType.Boolean, ColumnProperty.Null);
             
             
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMNA);
             
        }
    }
}