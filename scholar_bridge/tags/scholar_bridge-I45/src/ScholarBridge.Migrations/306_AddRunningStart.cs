﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(306)]
	public class AddRunningStart : Migration
	{
		public override void Up()
		{
			Database.AddColumn("SBScholarship", new Column("MatchCriteriaRunningStart", DbType.Boolean, ColumnProperty.Null));
			Database.AddColumn("SBSeeker", new Column("RunningStart", DbType.Boolean, ColumnProperty.Null));
			Database.AddColumn("SBApplication", new Column("RunningStart", DbType.Boolean, ColumnProperty.Null));
			Database.AddColumn("SBSeeker", new Column("RunningStartDetail", DbType.String, 250, ColumnProperty.Null));
			Database.AddColumn("SBApplication", new Column("RunningStartDetail", DbType.String, 250, ColumnProperty.Null));
		}

		public override void Down()
		{
			Database.RemoveColumn("SBScholarship", "MatchCriteriaRunningStart");
			Database.RemoveColumn("SBSeeker", "RunningStart");
			Database.RemoveColumn("SBSeeker", "RunningStartDetail");
			Database.RemoveColumn("SBApplication", "RunningStart");
			Database.RemoveColumn("SBApplication", "RunningStartDetail");
		}
	}
}
