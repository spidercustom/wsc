﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(129)]
    public class AddSeekerCareerRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCareerLUT"; }
        }
    }
}
