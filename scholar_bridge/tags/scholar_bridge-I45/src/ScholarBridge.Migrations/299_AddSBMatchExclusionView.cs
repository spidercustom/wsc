﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(299)]
    public class AddSBMatchExclusionView : Migration
    {
        public override void Up()
        {
        	DropView();
			Database.ExecuteNonQuery(SB_MATCH_EXCLUSION_VIEW);
		}

		public override void Down()
		{
        	DropView();
		}

		private void DropView()
		{
			Database.ExecuteNonQuery("if exists (select * from sys.views where name = 'SBMatchExclusion') DROP VIEW [dbo].[SBMatchExclusion]");
		}

		#region Match Exclusion view

    	public const string SB_MATCH_EXCLUSION_VIEW = @"
CREATE VIEW [dbo].[SBMatchExclusion]
AS
/*
     build a view of seeker/scholarship combinations that should not make it onto the 
     seekers match list based on certain absolutely required criteria (Gender, Ethnicity & High School) when 
     they are specified as Minimums on the scholarship.
*/

select distinct seeker.SBSeekerId, rt.SBScholarshipID, '13HighSchool' as Criterion  
from  SBScholarshipMatchCriteriaAttributeUsageRT mc 
inner join SBScholarship s on mc.SBScholarshipID = s.SBScholarshipID and s.Stage='Activated'
inner join SBScholarshipHighSchoolRT rt on s.SBScholarshipID = rt.SBScholarshipID
inner join SBSeeker seeker on seeker.CurrentHighSchoolIndex  
				not in (select SBHighSchoolIndex 
							from SBScholarshipHighSchoolRT rt2 
							where rt.SBScholarshipID = rt2.SBScholarshipID) 
where  mc.SBMatchCriteriaAttributeIndex = 13 and mc.SBUsageTypeIndex = 2
	and seeker.CurrentHighSchoolIndex is not null

union all
select distinct seeker.SBSeekerId, s.SBScholarshipID, '10Gender' as Criterion  
from SBScholarshipMatchCriteriaAttributeUsageRT mc
inner join SBScholarship s on s.SBScholarshipId=mc.SBScholarshipId and s.Stage='Activated'
inner join SBSeeker seeker on (seeker.Gender & s.MatchCriteriaGenders) = 0 and seeker.Gender > 1
where mc.SBMatchCriteriaAttributeIndex=10 and mc.SBUsageTypeIndex = 2

union all
select distinct sRt.SBSeekerId, s.SBScholarshipID, '09Ethnicity' as criterion
from SBScholarshipMatchCriteriaAttributeUsageRT mc
inner join SBScholarship s on s.SBScholarshipId=mc.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipEthnicityRT rt on s.SBScholarshipId=rt.SBScholarshipId
inner join SBSeekerEthnicityRT sRt on sRt.SBSeekerID not in (select distinct sbseekerid from SBSeekerEthnicityRT srt2
																where srt2.SBSeekerID = srt.SBSeekerID  
																	and srt2.SBEthnicityIndex in 
																		(select SBEthnicityIndex from SBScholarshipEthnicityRT se2
																			where se2.SBScholarshipID = s.SBScholarshipID))   
where mc.SBMatchCriteriaAttributeIndex=9 and mc.SBUsageTypeIndex = 2
";
		#endregion

	}
}