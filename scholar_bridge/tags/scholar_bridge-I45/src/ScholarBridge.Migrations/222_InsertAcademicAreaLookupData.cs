﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(222)]
    public class InsertAcademicAreaLookupData : Migration
	{
		private const string SBAcademicAreaLUT = "SBAcademicAreaLUT";
		public override void Up()
		{

            Database.ExecuteNonQuery("DELETE FROM SBApplicationAcademicAreaRT");
            Database.ExecuteNonQuery("DELETE FROM SBScholarshipAcademicAreaRT");
            Database.ExecuteNonQuery("DELETE FROM SBSeekerAcademicAreaRT");
            Database.ExecuteNonQuery("DELETE FROM " + SBAcademicAreaLUT);
		    Database.ExecuteNonQuery("DBCC CHECKIDENT ("+ SBAcademicAreaLUT+", RESEED, 0)" );

			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			string tableName = SBAcademicAreaLUT;
			string[] columns = GetInsertColumns(tableName);

            Database.Insert(tableName, columns, new[] { "Art", "Art", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Athletics/Sports", "Athletics/Sports", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Broadcasting/Journalism", "Broadcasting/Journalism", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Business", "Business", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Computer Science/Information Technology", "Computer Science/Information Technology", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Education", "Education", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] {"Law", "Law", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns,new[] { "Health", "Health", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Science & Engineering", "Science & Engineering", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Theology", "Theology", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Vocational & Trades", "Vocational & Trades", "0", adminId.ToString(), DateTime.Now.ToString() });
			
		}

		public override void Down()
		{
			Database.ExecuteNonQuery("DELETE FROM " + SBAcademicAreaLUT);
			
		}

		private string[] GetInsertColumns(string tableName)
		{
			Column[] columns = Database.GetColumns(tableName);
			return new string[]
			       	{
			       		columns[1].Name,
						columns[2].Name,
						"Deprecated",
                        "LastUpdateBy",
                        "LastUpdateDate"
					};
		}
	}
}
