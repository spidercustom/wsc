﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(310)]
	public class UpdateBoolViewForEslAndRunningStart : Migration
    {
        public override void Up()
        {
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchBools]");
			Database.ExecuteNonQuery(SB_MATCH_IN_BOOL);
		}

		public override void Down()
		{
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchBools]");
			Database.ExecuteNonQuery(UpdateMatchViewsWithCriteriaIds.SB_MATCH_IN_BOOL); //288

		}

		#region new Bool view
		public const string SB_MATCH_IN_BOOL =
	@"
CREATE VIEW [dbo].[SBMatchBools]
AS

select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '33Honors' as criterion, 
	s.MatchCriteriaHonors as selected,
	seeker.SBSeekerId, seeker.Honors as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.Honors=s.MatchCriteriaHonors
where match.SBMatchCriteriaAttributeIndex=33

union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '06FirstGeneration', 
	s.MatchCriteriaFirstGeneration as selected,
	seeker.SBSeekerId, seeker.FirstGeneration
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.FirstGeneration=s.MatchCriteriaFirstGeneration
where match.SBMatchCriteriaAttributeIndex=6

union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '07EslEll', 
	s.MatchCriteriaFirstGeneration as selected,
	seeker.SBSeekerId, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.IsEslEll=1
where match.SBMatchCriteriaAttributeIndex=7

union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '11RunningStart', 
	s.MatchCriteriaFirstGeneration as selected,
	seeker.SBSeekerId, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.RunningStart=1
where match.SBMatchCriteriaAttributeIndex=11

union all
-- college match situation 1 - provider selects wa schools and seeker selects wa school box or a specific wa school
-- *****************************************************************************************************
-- Note!!! On all college matches we do not look at the SBScholarshipMatchCriteriaAttributeUsageRT table
-- because this if entered by the provider will always be a required minimum
-- *****************************************************************************************************
select sbscholarshipid, AcademicYear, 2 as SBUsageTypeIndex, '05College' as criterion, 1 as selected, SBSeekerID, 1 as seekerValue  
from SBScholarship,
(
	select distinct sbseekerid from SBSeeker 
	where IsCollegeAppliedInWashington = 1
	or exists (select * from SBSeekerCollegeAppliedRT where SBSeekerId = SBSeeker.SBSeekerId)
) as WASeekers 
WHERE SBScholarship.MatchCriteriaCollegeType = 2 and Stage='Activated'

union all
-- college match situation 2 - provider selects Out of State (college type = 8) schools and seeker checks the OOS box
select sbscholarshipid, AcademicYear, 2 as SBUsageTypeIndex, '05College' as criterion, 1 as selected, SBSeekerID, 1 as seekerValue  
from SBScholarship s
left join SBSeeker on IsCollegeAppliedOutOfState = 1
WHERE s.MatchCriteriaCollegeType = 8  and s.Stage='Activated'

union all
select distinct * from
( 
	-- college match situation 3 - provider selects specific colleges and seeker has 1 or more specific matches 
	select distinct s.sbscholarshipid, s.AcademicYear, 2 as SBUsageTypeIndex, 
				'05College' as criterion, 1 as selected, srt.SBSeekerID, 1 as seekerValue  
	from SBScholarship s
	inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
	left join SBSeekerCollegeAppliedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
	where s.MatchCriteriaCollegeType = 4  and s.Stage='Activated'
	
	union all
	-- college match situation 4 - seeker selected college types match implied types from Provider list
	select distinct s.sbscholarshipid, s.AcademicYear, 2 as SBUsageTypeIndex, 
			'05College' as criterion, 1 as selected, SBSeeker.SBSeekerID, 1 as seekerValue  
	from SBScholarship s
	inner join
	(
		select distinct rt.SBScholarshipID, c.SchoolType from SBScholarshipCollegeRT rt 
		inner join SBCollegeLUT c on rt.SBCollegeIndex = c.SBCollegeIndex
		inner join SBScholarship s on s.SBScholarshipID = rt.SBScholarshipID
		where s.MatchCriteriaCollegeType = 4  and s.Stage='Activated'
	) ST on ST.SBScholarshipID = s.SBScholarshipID
	inner join sbseeker on (SBSeeker.SchoolTypes & ST.SchoolType) > 0  
) SeekersMatchingProviderImpliedSchoolTypes

union all
-- Note!  School Type is always a Minimum requirement if entered by the provider
--        so it is not necessary to look at SBScholarshipMatchCriteriaAttributeUsageRT.
select distinct * from 
(
	-- school type match #1:  where any one school type selected by the seeker matches any one selected by the provider
	select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, '18SchoolTypes' as criterion, 
		1 as selected, seeker.SBSeekerID, 1 as seekerValue   
	from SBScholarship s
	left join SBSeeker seeker on (seeker.SchoolTypes & s.MatchCriteriaSchoolTypes) > 0
	where	s.Stage='Activated' and  
			s.MatchCriteriaSchoolTypes between 1 and 30 -- 31 = all school types selected
			
	union all
	-- school type match #2:  where any one school type implied by the colleges selected by the seeker 
	-- matches any one school type selected by the provider
	select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, '18SchoolTypes' as criterion, 
		1 as selected, ST.SBSeekerID, 1 as seekerValue   
	from SBScholarship s
	inner join
	(
		select rt.SBSeekerID, sum(distinct c.SchoolType) as SchoolType from SBSeekerCollegeAppliedRT rt 
		inner join SBCollegeLUT c on rt.SBCollegeIndex = c.SBCollegeIndex
		group by rt.SBSeekerID
	) ST on (ST.SchoolType & s.MatchCriteriaSchoolTypes) > 0
	where s.Stage='Activated' 
	  and s.MatchCriteriaSchoolTypes between 1 and 30 -- 31 = all school types selected

) SchoolTypeMatches

union all
-- check for types of financial support matches
-- Note!  Financial support is always a Minimum requirement so it is not necessary to look at 
--        SBScholarshipMatchCriteriaAttributeUsageRT.
select distinct s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, '102SupportSituation' as criterion,
		1 as selected, sRt.SBSeekerID, 1 as seekerValue   
from SBScholarship s
inner join SBScholarshipSupportRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSupportRT sRt on rt.SBSupportIndex=sRt.SBSupportIndex
where s.Stage ='Activated'

union all
-- Any match with provider ClassRank will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '30ClassRank' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipClassRankRT rt on s.SBScholarshipId = rt.SBScholarshipId
left join SBSeeker sk on rt.SBClassRankIndex = sk.ClassRankIndex
where match.SBMatchCriteriaAttributeIndex=30

union all
-- Any (one or more) match with provider HighSchool will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '13HighSchool' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage = 'Activated'
inner join SBScholarshipHighSchoolRT rt on s.SBScholarshipId = rt.SBScholarshipId
left join SBSeeker seeker on rt.SBHighSchoolIndex = seeker.CurrentHighSchoolIndex
where match.SBMatchCriteriaAttributeIndex = 13

union all
-- Any (one or more) match with provider SchoolDistrict will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '17SchoolDistrict' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSchoolDistrictRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker sk on rt.SBSchoolDistrictIndex=sk.SchoolDistrictIndex
where match.SBMatchCriteriaAttributeIndex=17

union all
-- Any (one or more) match with provider City will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '03City' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.SBCityIndex=seeker.CityIndex
where match.SBMatchCriteriaAttributeIndex=3

union all
-- Any (one or more) match with provider County will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '08County' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCountyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.SBCountyIndex=seeker.CountyIndex
where match.SBMatchCriteriaAttributeIndex=8


union all
-- Any (one or more) match with provider Ethnicity will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '09Ethnicity' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipEthnicityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerEthnicityRT sRt on rt.SBEthnicityIndex=sRt.SBEthnicityIndex
where match.SBMatchCriteriaAttributeIndex=9

union all
-- Any (one or more) match with provider Religion will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '16Religion' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipReligionRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerReligionRT sRt on rt.SBReligionIndex=sRt.SBReligionIndex
where match.SBMatchCriteriaAttributeIndex=16

union all
-- Any (one or more) match with provider AcademicArea will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '00AcademicArea' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAcademicAreaRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAcademicAreaRT sRt on rt.SBAcademicAreaIndex=sRt.SBAcademicAreaIndex
where match.SBMatchCriteriaAttributeIndex=0

union all
-- Any (one or more) match with provider Careers will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Career' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCareerRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCareerRT sRt on rt.SBCareerIndex=sRt.SBCareerIndex
where match.SBMatchCriteriaAttributeIndex=2

union all
-- Any (one or more) match with provider Organizations will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'MatchOrganization' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerMatchOrganizationRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerMatchOrganizationRT sRt on rt.SBSeekerMatchOrganizationIndex=sRt.SBSeekerMatchOrganizationIndex
where match.SBMatchCriteriaAttributeIndex=14

union all
-- Any (one or more) match with provider Company will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '14Company' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCompanyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCompanyRT sRt on rt.SBCompanyIndex = sRt.SBCompanyIndex
where match.SBMatchCriteriaAttributeIndex=14

union all
-- Any (one or more) match with provider SeekerHobby will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '19SeekerHobby' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerHobbyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerHobbyRT sRt on rt.SBSeekerHobbyIndex=sRt.SBSeekerHobbyIndex
where match.SBMatchCriteriaAttributeIndex=19

union all
-- Any (one or more) match with provider Sport will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '25Sport' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSportRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSportRT sRt on rt.SBSportIndex=sRt.SBSportIndex
where match.SBMatchCriteriaAttributeIndex=25

union all
-- Any (one or more) match with provider Club(s) will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '04Club' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipClubRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerClubRT sRt on rt.SBClubIndex=sRt.SBClubIndex
where match.SBMatchCriteriaAttributeIndex=4

union all
-- If the provider has specified either preferred or minimum for student service then we have a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '24SeekerServing' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker sk on sk.IsService = 1
where match.SBMatchCriteriaAttributeIndex=24

union all
-- If the provider has specified either preferred or minimum for student service 
-- then we have a match 
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '28SeekerWorking' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker sk on sk.IsWorking = 1
where match.SBMatchCriteriaAttributeIndex=28

union all
-- If the provider has specified either preferred or minimum for financial need 
-- then we have a match if the seeker fills the financial need challenge statement
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '101FinancialNeed' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipFundingProfileAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker sk on sk.MyChallenge is not null and len(ltrim(rtrim(sk.MyChallenge))) > 0
where match.SBFundingProfileAttributeIndex=1

union all
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '34APCredits' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.APCredits = 1
where match.SBMatchCriteriaAttributeIndex=34

union all
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, '35IBCredits' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.IBCredits = 1
where match.SBMatchCriteriaAttributeIndex=35

";
		#endregion

	}
}