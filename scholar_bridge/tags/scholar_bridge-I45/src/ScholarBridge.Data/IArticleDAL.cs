using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IArticleDAL : IDAL<Article>
    {
        Article FindById(int id);
        Article Save(Article article);
    }
}