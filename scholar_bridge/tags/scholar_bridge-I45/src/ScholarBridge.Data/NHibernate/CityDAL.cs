﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Data.NHibernate
{
    public class CityDAL : LookupDAL<City>
    {
        public StateDAL StateDAL { get; set; }
        public IList<City> FindByState(string stateAbbreviation)
        {
            if (null == StateDAL.SessionFactory)
                StateDAL.SessionFactory = SessionFactory;
            var state = StateDAL.FindByAbbreviation(stateAbbreviation);
            if (null == state)
                throw new ArgumentException("State {0} do not exists in application".Build(stateAbbreviation));
            return CreateCriteria().Add(Restrictions.Eq("State", state)).List<City>();
        }

        public override List<KeyValuePair<string, string>> GetLookupItems(string stateAbbreviation)
        {
            //if user data is null or empty retrieve all 
            if (string.IsNullOrEmpty(stateAbbreviation))
                return base.GetLookupItems(null);

            //if user data is not empty, assume that its state abbriviation and retrieve by state
            var list = FindByState(stateAbbreviation);
            return list.Select(o => new KeyValuePair<string, string>(o.Id.ToString(), o.Name)).ToList();
        }

    	public new void Insert(City city)
    	{
			if (city.State == null)
			{
				city.State = StateDAL.FindByAbbreviation(State.WASHINGTON_STATE_ID);
			}
    		base.Insert(city);
    	}
    }
}
