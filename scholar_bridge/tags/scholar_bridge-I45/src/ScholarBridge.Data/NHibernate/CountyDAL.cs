﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class CountyDAL : LookupDAL<County>
    {
        public StateDAL StateDAL { get; set; }

        public IList<County> FindByState(string stateAbbreviation)
        {
            var state = StateDAL.FindByAbbreviation(stateAbbreviation);
            if (null == state)
                throw new ArgumentException("State '{0}' do not exists in application".Build(stateAbbreviation));
            return CreateCriteria().Add(Restrictions.Eq("State", state)).List<County>();
        }

        public override List<KeyValuePair<string, string>> GetLookupItems(string userData)
        {
            //if user data is null or empty retrieve all 
            if (string.IsNullOrEmpty(userData))
                return base.GetLookupItems(null);

            //if user data is not empty, assume that its state abbriviation and retrieve by state
            var list = FindByState(userData);
            var result = list.Select(o => new KeyValuePair<string, string>(o.Id.ToString(), o.Name)).ToList();

            return result;
        }

		public new void Insert(County county)
		{
			if (county.State == null)
				county.State = StateDAL.FindByAbbreviation(State.WASHINGTON_STATE_ID);
			base.Insert(county);
		}
    }
}
