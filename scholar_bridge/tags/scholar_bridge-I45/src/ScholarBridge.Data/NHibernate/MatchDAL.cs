using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class MatchDAL : AbstractDAL<Match>, IMatchDAL
    {
        private static readonly SimpleExpression NOT_DELETED = Restrictions.Eq("IsDeleted", false);

        public IList<Match> FindAll(Application application)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))

                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus status)
        {
            return FindAll(seeker, new[] {status});
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.In("MatchStatus", status))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }

    	public IList<Match> FindAll(Scholarship scholarship, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.In("MatchStatus", status))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }

		public IList<Match> FindAllCurrentMatches(Seeker seeker, bool excludeMatchesWithApplications)
		{
			var criteria = CreateCriteria()
				.Add(NOT_DELETED)
				.Add(Restrictions.Eq("Seeker", seeker))
				.Add(Restrictions.Eq("AddedViaMatch", true))
				.AddOrder(Order.Desc("Rank"));

			if (excludeMatchesWithApplications)
				criteria.Add(Restrictions.IsNull("Application"));

			return criteria.List<Match>();
		}

        public Match Find(Seeker seeker, int scholarshipId)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship.Id", scholarshipId))
                .UniqueResult<Match>();
        }

		public void UpdateMatches(Seeker seeker)
		{
			IQuery query = Session.CreateSQLQuery("exec dbo.BuildSeekerMatches @SeekerId=:seekerId,@UpdateUserId=:userId");
			query.SetInt32("seekerId", seeker.Id);
			query.SetInt32("userId", seeker.User.Id);
			query.ExecuteUpdate();
		}

		public void UpdateMatches(Scholarship scholarship)
		{
			IQuery query = Session.CreateSQLQuery("exec dbo.BuildScholarshipMatches @ScholarshipId=:scholarshipId,@UpdateUserId=:userId");
			query.SetInt32("scholarshipId", scholarship.Id);
			query.SetInt32("userId", scholarship.LastUpdate.By.Id);
			query.ExecuteUpdate();
		}

		#region Method Overrides
		public new Match Insert(Match match)
		{
			match.ComputeRank();
			return base.Insert(match);
		}

		public new Match Update(Match match)
		{
			match.ComputeRank();
			return base.Update(match);
		}
		#endregion

    }
}
