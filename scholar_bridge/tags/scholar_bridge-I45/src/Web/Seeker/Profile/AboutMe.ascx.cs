﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Brettle.Web.NeatUpload;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class AboutMe : WizardStepUserControlBase<Domain.Seeker>
    {
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }
        public IApplicationService ApplicationService { get; set; }

        private Domain.Seeker seeker;
        Domain.Seeker SeekerInContext
        {
            get
            {
                if (seeker == null)
                    seeker = Container.GetDomainObject();
                if (seeker == null)
                    throw new InvalidOperationException("There is no seeker in context");
                return seeker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
				if (Page is SBBasePage)
				{
					((SBBasePage)Page).BypassPromptIds.AddRange(
						new[]	{
									UploadFile.ID, 
                                    "deleteAttachmentBtn",
                                    "downloadButton"
								});

					((SBBasePage) Page).SkipDataCheckIds.AddRange(
						new[]
							{
								AttachmentComments.ID,
								attachedFiles.ID
							});
				}
			}
		}

		private void BindAttachedFiles()
		{
			attachedFiles.DataSource = SeekerInContext.Attachments;
			attachedFiles.DataBind();
		}

        private void PopulateScreen()
        {
            PersonalStatementControl.Text = SeekerInContext.PersonalStatement;
            MyGiftControl.Text = SeekerInContext.MyGift;
            FiveWordsControl.Text = SeekerInContext.Words;
            FiveSkillsControl.Text = SeekerInContext.Skills;
        	BindAttachedFiles();
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
		    PopulateObjects();
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            SeekerService.Update(SeekerInContext);
		}

        public override void PopulateObjects()
        {
            SeekerInContext.PersonalStatement = PersonalStatementControl.Text;
            SeekerInContext.MyGift = MyGiftControl.Text;

            SeekerInContext.Words = FiveWordsControl.Text;
            SeekerInContext.Skills = FiveSkillsControl.Text;

        	foreach (var row in attachedFiles.Items)
        	{
        		var box = (CheckBox)row.FindControl("includeWithAppsCheck");
				// get the download linkbutton in order to get the attachment id
        		var button = (LinkButton) row.FindControl("downloadButton");
				// set the 'IncludeWithApplications' flag
        		int id = int.Parse(button.CommandArgument);
        		var attachments = from a in SeekerInContext.Attachments
        		                  where a.Id == id
        		                  select a;
				foreach (var a in attachments)
					a.IncludeWithApplications = box.Checked;
        	}
        }
		#endregion

		#region Control event handlers
		protected void UploadFile_Click(object sender, EventArgs e)
		{
			if (Page.IsValid && AttachFile.HasFile)
			{

				//check if a valid filename is being uploaded
				if (String.IsNullOrEmpty(AttachFile.FileName))
				{
					AttachmentCommentsValidator.IsValid = false;
					AttachmentCommentsValidator.Text = "Please choose a valid file to attach and try again.";
					return;
				}

				bool attachmentIsNew = false;
				Attachment attachment = FindOrCreateAttachment();
				if (attachment.Id < 1)
					attachmentIsNew = true;
				try
				{
					AttachFile.MoveTo(attachment.GetFullPath(ConfigHelper.GetAttachmentsDirectory()), MoveToOptions.Overwrite);
					if (attachmentIsNew)
						SeekerInContext.Attachments.Add(attachment);						
					SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
					SeekerService.Update(SeekerInContext);
				}
				catch (Exception)
				{
					AttachmentCommentsValidator.IsValid = false;
					AttachmentCommentsValidator.Text = "Could not upload the file. Please try again.";
					// FIXME: Display error to user
					if (attachmentIsNew)
					{
						attachment.RemoveFile(ConfigHelper.GetAttachmentsDirectory());
						if (SeekerInContext.Attachments.Contains(attachment))
							SeekerInContext.Attachments.RemoveAt(SeekerInContext.Attachments.Count - 1);
					}

				}
				AttachmentComments.Text = null;
				includeCheckBox.Checked = false;
				BindAttachedFiles();
			}
		}

		/// <summary>
		/// if the file has the same name as an already uploaded file then
		/// overlay it, otherwise add new.
		/// </summary>
		/// <returns></returns>
		private Attachment FindOrCreateAttachment()
		{
			string mimeType = AttachFile.ContentType;
			if (String.IsNullOrEmpty(mimeType))
				mimeType = "application/octet-stream";
			Attachment attachment = null;
			var sameNamedAttachment = from a in SeekerInContext.Attachments
									  where a.Name == AttachFile.FileName
									  select a;
			if (sameNamedAttachment.Count() > 0)
			{
				foreach (var sameName in sameNamedAttachment)
				{
					attachment = sameName;
					attachment.Comment = AttachmentComments.Text;
					attachment.Bytes = AttachFile.ContentLength;
					attachment.MimeType = mimeType;
					attachment.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
					attachment.IncludeWithApplications = includeCheckBox.Checked;
					break;
				}
			}
			else
			{
				attachment = new Attachment
			                 	{
			                 		Name = Path.GetFileName(AttachFile.FileName),
			                 		Comment = AttachmentComments.Text,
			                 		Bytes = AttachFile.ContentLength,
			                 		MimeType = mimeType,
			                 		LastUpdate = new ActivityStamp(UserContext.CurrentUser),
			                 		IncludeWithApplications = includeCheckBox.Checked
								};
				attachment.GenerateUniqueName();
			}
			return attachment;
		}

		protected void attachedFiles_OnItemDeleting(object sender, ListViewDeleteEventArgs e)
		{
			if (SeekerInContext.Attachments.Count > e.ItemIndex)
			{
				var attachment = SeekerInContext.Attachments[e.ItemIndex];
				if (null != attachment)
				{
					SeekerInContext.Attachments.RemoveAt(e.ItemIndex);

					SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
					SeekerService.Update(SeekerInContext);
                    if (ApplicationService.CountAllHavingAttachment(attachment)==0)
					    attachment.RemoveFile(ConfigHelper.GetAttachmentsDirectory());
				}
			}
			BindAttachedFiles();
		}

		protected void attachedFiles_ItemCommand(object sender, ListViewCommandEventArgs e)
		{
			if (e.CommandName == "Download")
			{
				int attachmentId = int.Parse(e.CommandArgument.ToString());
				var attachmentList = from a in SeekerInContext.Attachments
				                 where a.Id == attachmentId
				                 select a;

				if  (attachmentList.Count() > 0)
					foreach (var attachment in attachmentList)
					{
						FileHelper.SendFile(Response, 
												attachment.Name, 
												attachment.MimeType, 
												attachment.GetFullPath(ConfigHelper.GetAttachmentsDirectory()));
					}
			}
		}
		#endregion

	}
}