﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.MatchList;

namespace ScholarBridge.Web.Seeker.Matches
{
    public partial class Default : SBBasePage
    {

        public IMatchService MatchService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.SeekerMatches);
            UserContext.EnsureSeekerIsInContext();
            PopulatePage();
        }

        protected override void OnInitComplete(EventArgs e)
        {
            ConfigureLists();
            base.OnInitComplete(e);
        }

        private void PopulatePage()
        {
            BindLists(!IsPostBack);
        }

        private void ConfigureLists()
        {
            var iconHelper = new MatchListIconHelper();
            var actionHelper = new MatchListActionHelper();
            iconHelper.PostIconClick = BindLists;

            savedList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
            iconHelper.SetupListView(savedList.List);
            actionHelper.SetupListView(savedList.List);

            qualifyList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
            iconHelper.SetupListView(qualifyList.List);
            actionHelper.SetupListView(qualifyList.List);

        }

        private void BindLists()
        {
            BindLists(true);
        }

        private void BindLists(bool alsoDataBind)
        {
            var matches = MatchService.GetMatchesForSeekerWithoutApplications(UserContext.CurrentSeeker);
            qualifyList.Matches = matches;
            if (alsoDataBind)
                qualifyList.BindMatches();

            var savedMatches = MatchService.GetSavedButNotAppliedMatches(UserContext.CurrentSeeker);
            savedList.Matches = savedMatches;
            if (alsoDataBind)
                savedList.BindMatches();
        }

        protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.SaveMatch(UserContext.CurrentSeeker, scholarshipId);
        }
    }
}