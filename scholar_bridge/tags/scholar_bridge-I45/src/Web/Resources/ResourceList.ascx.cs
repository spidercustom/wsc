﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Business;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Resources 
{
    public partial class ResourceList : UserControl
    {
		public IResourceService ResourceService { get; set; } 

        private IList<Resource> resources;

		public IList<Resource> Resources
        {
			get { return resources; }
            set
            {
				resources = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            Bind();
        }

        protected void Bind()
        {
            if (!(Resources == null))
            {
				resourceList.DataSource = Resources;
                resourceList.DataBind();
            }
        }

    	protected void resourceList_ItemDataBound(object sender, ListViewItemEventArgs e)
    	{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var resource = ((Resource)((ListViewDataItem)e.Item).DataItem);
				if (resource.URL == null)
				{
					var link = (HyperLink)e.Item.FindControl("resourceUrl");
					link.Visible = false;
				}
				if (resource.AttachedFile == null)
				{
					var linkButton = (LinkButton)e.Item.FindControl("resourceFileLinkButton");
					linkButton.Visible = false;
				}
			}
		}

    	protected void resourceFileLinkButton_OnCommand(object sender, CommandEventArgs e)
    	{
			var id = Int32.Parse((string)e.CommandArgument);
			Resource resource = ResourceService.GetById(id);
			FileHelper.SendFile(Response, resource.AttachedFile.Name, resource.AttachedFile.MimeType, resource.AttachedFile.GetFullPath(ConfigHelper.GetAttachmentsDirectory()));
    	}
    }
}