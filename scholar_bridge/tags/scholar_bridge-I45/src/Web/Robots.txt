﻿Sitemap: http://www.theWashBoard.org/SiteMap.xml

User-agent: *
Disallow: /Admin/
Disallow: /App_Browsers/
Disallow: /App_Code/
Disallow: /App_Data/
Disallow: /bin/
Disallow: /Common/
Disallow: /Emails/
Disallow: /images/
Disallow: /js/
Disallow: /logs/
Disallow: /Message/
Disallow: /NeatUpload/
Disallow: /obj/
Disallow: /Profile/
Disallow: /Properties/
Disallow: /Resources/
Disallow: /styles/
Disallow: /_ScholarshipData/

