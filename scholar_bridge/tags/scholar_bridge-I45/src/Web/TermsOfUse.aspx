﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="TermsOfUse.aspx.cs" Inherits="ScholarBridge.Web.TermsOfUse" Title="Terms of Use" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Terms of Use" />
    <style type="text/css">
        #PageContent p{text-align:justify; font-size:12px; color:black;}
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">

<h2>Terms of Use</h2>

<p>The Washington Scholarship
Coalition is a public/private collaboration of entities including, the
Washington State Higher Education Coordinating Board, the Washington Financial
Aid Association, The Seattle Foundation, Northwest Education Loan Association,
College Spark, College Success Foundation, and College Planning Network
(altogether, the &quot;Companies&quot; or &quot;us,&quot; &quot;We,&quot;
&quot;we,&quot; &quot;Our,&quot; or &quot;our&quot;).  We maintain this product
or site on the Internet (the &quot;Site&quot;) as a service for scholarship
seekers (students, parents and financial aid professionals) and scholarship intermediaries
or providers in the State of Washington.  The Companies require that all the
visitors to our Site adhere to the following rules and regulations.  By
accessing the Site, you indicate your acknowledgment and acceptance of these
Terms of Use (&quot;Terms&quot;).  Please read them carefully.  IF YOU DO NOT
WISH TO BE BOUND BY THESE TERMS, YOU MAY NOT ACCESS OR USE THIS SITE. </p>

<p>The Companies reserve the right
to modify or discontinue, temporarily or permanently, the Site for any reason,
at our sole discretion, with or without notice to you.  The Companies likewise
may change the terms and conditions of the Terms from time to time with or
without notice to you.  You agree to review the Terms periodically to ensure
that you are aware of any modifications.  Your continued access or use of the
Site after the modifications have become effective shall be deemed your
conclusive acceptance of the modified Terms.  You will ensure that your agents,
contractors and employees comply with these Terms. </p>

<p>By using this online service or
Site, you are certifying that you are a scholarship seeker or scholarship intermediary
or provider for the State of Washington or another user with express permission
from us to use this Site and that you have authority to view information on
this Site.  Use of this Site in violation of this certification constitutes a
misrepresentation and will be considered a fraudulent act.  You expressly agree
to the terms of our <asp:HyperLink NavigateUrl="~/PrivacyStatement.aspx" runat="server" ID="privacyStatementLink">Privacy Policy Statement</asp:HyperLink>.</p>

<h3>Terms Applicable to Scholarship Intermediaries and Providers</h3>


<p>You agree that you are offering scholarships with charitable intent
and are one of the following: a tax exempt entity under Section 501(c) of the
Internal Revenue Code, are affiliated with a 501(c) entity, are an accredited
higher education institution in the State of Washington that meets our
criteria, or are an entity that has met our approval criteria otherwise. </p>

<p>You agree that scholarship offerings on the Site may be ranked, sorted
or otherwise presented for scholarship seekers’ benefit and ease.  You agree to
receive, respond to and modify your policies or practices associated with
offering scholarships on the Site based on user feedback.  You agree to fix
your scholarship criteria on the Site by a certain deadline date to be set by
Us.  You agree to participate in evaluative activities to help us improve the
Site and consider ways to improve scholarship philanthropy in general.  You
agree that your scholarship application and award practices comply with all
relevant federal, state and local guidelines, including laws prohibiting
discrimination.</p>

<p>You agree that your scholarships
will benefit either (i) residents of the State of Washington, or (ii) students
attending an accredited Washington higher education institution.  You
acknowledge and agree that only those scholarships meeting these criteria will
be listed as eligible and available on the Site.</p>

<p>You agree to provide a single,
reachable point of contact to Us as your representative with respect to the
Site.  You agree to actively participate and engage with Us regularly regarding
the Site.  You agree to log in to the Site once per year to verify or update
(i) scholarship data, and (ii) organization data.  You agree to log in to the
Site to timely record scholarship awards.  </p>

<h3>Terms Applicable to All Site Participants</h3>

<p>You agree that neither the
Companies nor any of our employees, agents, contractors, third-party content
providers or licensors warrant that the services, the Site or its contents will
be uninterrupted, free of viruses or other harmful code, or error free.  We
make no warranty that any defects in the software used in the Site will be
corrected.  We make no warranties as to the results that may be obtained from
use of the services, the Site or its contents, or as to the timeliness, security,
sequence, adequacy, accuracy, reliability, or completeness of any information obtained
therefrom. </p>

<p>The contents of the Site —
including its text, software, graphics, images, music, sound, logos, button
icons, photographs, video, editorial content, notices, overall appearance,
functionality, and other material — are protected under both United States and
foreign copyright, patent, trademark, service mark, trade dress and other intellectual
property laws.  The contents belong to the Companies or to others as indicated.
 You agree that the Site is the exclusive property of us, or others as
indicated, and you agree that the Site is proprietary and confidential.  The
information and materials contained in the Site may not be copied, displayed, conveyed,
communicated, reverse engineered, distributed, downloaded, licensed, modified,
published, reposted, reproduced, reused, sold, supplemented, transmitted, used
to create a derivative work, or otherwise used for public or commercial
purposes without our express written permission.  You agree to protect the
information and materials accessed through the Site from unauthorized
disclosure and use.  This confidentiality obligation will survive the
termination of your use of the Site.  </p>

<p>Your right to use the Site is
personal to you.  If we give you the authority to assign user id’s and
passwords for the Site, you agree to assign such only to users who have a need
to access the Site.  You agree to ensure that users with id’s and passwords
will not share such with any other persons, and you are responsible for
maintaining the confidentiality of your user id’s and passwords and for any and
all activities that occur under your passwords or accounts.  You agree to
notify us of any unauthorized use or disclosure of your account or any other
breach of security known to you.  You agree to counsel and instruct your
employees and contractors not to disclose to unauthorized persons any
individually identifiable information with respect to any scholarship seeker or
parent to which you or your employees have access as a result of your work with
the records obtained through the Site.  You agree to reasonably instruct and
train your employees on the proper use and care of the Site.  You will be
solely responsible for your actions and the contents of your transmissions
through the Site and your use of any correspondence or other forms provided by
the Site or by us.  You agree not to reproduce, duplicate, copy, sell, resell,
use or exploit for any commercial purposes the Site or use of or access to the
Site or any information or technology obtained from the Site.  </p>

<p>You agree that you will not use
any robot, spider, other automatic device, or manual process to monitor or copy
our Site or the content contained herein without our prior express written
permission.  You agree that you will not use any device, software or routine to
interfere or attempt to interfere with the proper working of the Site or any
transaction being conducted on our Site.   You agree to take feasible steps to
prevent transmission over the Site of any virus or other software routine
designed or likely to permit unauthorized access to the Site or to disable,
erase or otherwise harm any software, hardware or data accessible over the
Site.</p>

<p>You agree to abide by all
applicable local, state, national and international laws and regulations and
all of our rules, policies and procedures in your use of the Site.  Our Site is
available only to individuals who are permitted to use it under applicable law.
 If you do not qualify, please do not use our Site.  You agree not to
impersonate any person or entity or falsely state or otherwise misrepresent
your identity or affiliation with a person or entity.   </p>

<p>You agree that the Companies may
terminate your password(s), account(s) and your use of the Site in our sole
discretion for any reason.  All notices you give to us shall be in writing and
shall be made either via e-mail or conventional mail.  In addition, we may post
notices or links to notices through the Site or via e-mail to inform you of
changes to the Terms, the Site or other matters of importance. </p>

<p>You agree that all access and
use of the Site and its contents is at your own risk.  

<span style="text-transform:uppercase;">By using the Site, you acknowledge that the companies specifically
disclaim any liability (whether based in contract, tort, negligence, strict
liability or otherwise) for any direct, ACTUAL, indirect, incidental, common
law, statutory, regulatory, consequential, compensatory, exemplary, lost
profits, lost savings, punitive or special damages arising out of or in any way
connected with your access to or use of the Site (even if we have been advised
of the possibility of such damages), including, but not limited to, any
liability associated with any viruses that may infect your computer equipment. 
This site, including all content, services, functions, hypertext links, and
other information made available on or accessed through this site, is provided
&quot;as is,&quot; and without warranties of any kind, either express or
implied, including, BUT NOT LIMITED TO, the warranties of non-infringement,
merchantability and fitness for a particular purpose.  SOME JURISDICTIONS DO
NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR LIMITATIONS OF LIABILITY SO
SOME OF THE ABOVE EXCLUSIONS OR LIMITATIONS MAY NOT APPLY TO YOU.</span></p>


<p>You
will reimburse us for all losses, damages, costs and expenses (including court
costs and reasonable legal fees) caused by the negligence or willful misconduct
of you or your employees, contractors or agents including, but not limited to,
failure to maintain confidentiality, the sending of any inappropriate
information over the Internet without encryption or without other appropriate
security protections, or breach of any agreement, representation, warranty or
covenant herein.  YOU WILL BE LIABLE AND YOU AGREE TO INDEMNIFY AND HOLD US HARMLESS,
AND EACH OF OUR AFFILIATES, PARENTS, OWNERS, SUBSIDIARIES, MEMBERS, TRUSTEES,
DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, REPRESENTATIVES, SUCCESSORS AND ASSIGNS
(THE “INDEMNITEES”) FROM AND AGAINST ANY AND ALL CLAIMS OF LIABILITY,
OBLIGATION, LOSS, DAMAGE, DEMAND, COST, EXPENSE, OR DISBURSEMENT (INCLUDING
COURT COSTS AND REASONABLE LEGAL FEES) OF THE INDEMNITEES, ARISING OUT OF OR
RESULTING FROM ANY ACT OR OMISSION RELATED TO YOUR USE OF THE SITE. </p>

<p>On this Site you may find links
that will transfer you to the site of an organization that can provide you with
value-added information and/or functionality.  By linking to these sites, the
Companies do not represent or imply that there is any business relationship
between us and these organizations.  The Companies are not responsible for the
content and performance of these sites or for your transactions with them.  Furthermore,
we strive to keep these links as current and accurate as possible, but we
cannot guarantee and we expressly do not warrant that they point to the
intended third-party site.  Links to and from this Site do not constitute an
endorsement by the Companies. </p>

<p>The Site provides you with the
capability to review, perform and/or complete certain functions related to scholarship
administration.  You represent and warrant that the information you furnish in
connection with your use of the Site is accurate, complete and current to the
best of your knowledge and belief, and meets all requirements of all applicable
laws and regulations.  You agree not to make any adjustment or modification to
the Site without our prior approval.  You agree that we may publish your name
as a user of this Site.  You agree that, by your electronic transmission of
information via the Site, you are making any and all certifications required by
applicable laws, regulations or our requirements, and that such electronic
certifications have the same force and effect as manually signed
certifications.  You further agree to use the Site only for purposes authorized
by us.  <b>SPECIFICALLY, YOU AGREE NOT TO USE THE SITE OR ANY INFORMATION
OBTAINED FROM IT, OR ALLOW A THIRD PARTY TO USE THE SITE, FOR MARKETING
PRODUCTS OR SERVICES TO SCHOLARSHIP SEEKERS, THEIR PARENTS, OR OTHERS.  </b></p>

<p>If you are accessing the Site as
a representative, employee, agent or contractor of a distinct corporate or
other legal entity, you represent and warrant that you have the requisite
authority to accept the Terms on behalf of such entity, and “you” shall be
deemed to include such entity throughout these Terms.  You represent and
warrant that you (i) have the corporate power to accept the Terms and perform
your obligations hereunder, (ii) the acceptance of the Terms by use of the Site
creates a legal, valid and binding agreement between you and us, enforceable
against you in accordance with the terms hereof, and (iii) the acceptance of
the Terms and performance of your obligations hereunder will not violate any of
your constating documents or any agreement to which you are a party or any
applicable law.</p>

<p>You agree to take necessary
steps to adequately document, store and duplicate all of your files and data. 
The Companies will not be responsible for the cost of reconstructing files or
data lost or destroyed.  You agree that, despite our reasonable security
measures, we cannot guarantee that electronic communications over the Internet
will be completely secure.  The Companies' System Requirements are posted on
this Site.  By using this Site, you acknowledge these System Requirements and
agree that the Companies are not responsible for your failure to abide by these
System Requirements.  You are responsible for acquiring, installing and maintaining
any products (such as equipment, software and communication line access) furnished
by third party suppliers that are necessary to access this Site.  We will
provide reasonable assistance according to our then current policies and
procedures.    </p>

<p>These Terms of Use shall be
governed in all respects by the substantive laws of the State of Washington,
without regard to its provisions relating to conflict of laws, and you agree to
submit to the personal and exclusive jurisdiction and venue of the state and
federal courts within King County, Washington.  Our failure to exercise or
enforce any right or provision of the Terms shall not constitute a waiver of
such right or provision.  If any provision of the Terms is found by a court of
competent jurisdiction to be invalid, unlawful, or unenforceable, the parties
nevertheless agree that the court should endeavor to give effect to the
parties' intentions as reflected in the provision, and the other provisions of
the Terms remain in full force and effect.  You agree that, regardless of any
statute, regulation, or law to the contrary, any claim or cause of action
arising out of or related to use of the Site or the Terms must be filed by you within
one (1) year after such claim or cause of action arose or be forever barred.  In
no event shall we be liable for damages or loss caused by third parties not
under our control or by other causes beyond our reasonable control, including,
but not limited to, damages or loss caused by third party telecommunications
service providers.  You agree that these Terms of Use comprise the entire
agreement between you and us and supersede all prior agreements between the
parties regarding the subject matter contained herein.   </p>

</asp:Content>