﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupList.ascx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupList" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %> 
<div id="lookuplist_holder">
    <div id="lookuplist_header">
        <p>Search or select items directly from the Available list. Add them to the Selected list by selecting and clicking Add or double click on the item. You can select multiple items using ctrl+click or click and drag.</p>
    </div>

<div id="lookuplist_left" class="ui-corner-all">
    <h1>Available</h1>
    <div id="lookuplist_search" >
        <asp:TextBox CssClass="lookupsearchbox text ui-widget-content ui-corner-all" runat="server" ID="txtSearch"  ></asp:TextBox>
        <asp:HyperLink  runat="server" NavigateUrl="#" ID="btnSearch"><img  src="/images/lookupdialog/magnifier.png" Alt="Go"  /></asp:HyperLink>
    </div>
    <div id="lookuplist_available_container">
        <asp:Panel  id="lookuplist_available" runat="server"> 
        </asp:Panel>
    </div>
    <asp:Panel  id="pager" runat="server" CssClass="jqpager available-list-pager"></asp:Panel>
</div>
<div id="lookuplist_middle">
    <sbCommon:AnchorButton id="btnAdd" runat="server" text="Add >" CausesValidation="False" />
    <sbCommon:AnchorButton id="btnRemove" runat="server" text="< Remove" CausesValidation="False"  />
    <sbCommon:AnchorButton id="btnRemoveAll" runat="server" text="<< Remove All"   CausesValidation="False"  />
    <sbCommon:AnchorButton id="btnSave" runat="server" text="Save" CausesValidation="False"  />
    <sbCommon:AnchorButton id="btnCancel" runat="server" text="Cancel" CausesValidation="False"  />
    <asp:Image runat="server" ImageUrl="/images/lookupdialog/ajax-loader.gif"  id="ajaxprogress" CssClass="ajaxprogress" AlternateText="Wait..."  />
</div>

<asp:Panel id="lookuplist_right" class="ui-corner-all" runat="server">
    <h1>Selected</h1>
     <asp:Panel  id="lookuplist_selections" runat="server"> </asp:Panel>
     
 </asp:Panel>

<asp:Panel id="lookuplist_otherbox"  CssClass="text ui-widget-content ui-corner-all"  runat="server">
    <h1>Others:</h1>
     <asp:TextBox CssClass="noborder other-textbox" runat="server" ID="txtOther" TextMode="MultiLine" Rows="3" Height="90px"  ></asp:TextBox>
</asp:Panel>
         
    
</div> <%--End of holder--%>

  

