﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CaptchaControl.ascx.cs" Inherits="ScholarBridge.Web.Common.CaptchaControl" %>
<%@ Register Assembly="Hip" Namespace="Msdn.Web.UI.WebControls" TagPrefix="msdn" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sb" %>
<asp:ScriptManager ID="captchascriptmanager" runat="server" />

<asp:UpdatePanel ID="captchupdatepanel" runat="server" >
    <ContentTemplate>
        <p>
            <label for="txtCaptcha">Type in the characters shown in the image below:</label>
            <asp:TextBox ID="txtCaptcha" runat="server" Width="180px" onkeyup="ToLower(this)"/>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCaptcha" ErrorMessage="Please enter exact word as shown below" />
            <msdn:HipValidator ID="HipValidator1" runat="server" ControlToValidate="txtCaptcha" ErrorMessage="Please enter exact word as shown below" HipChallenge="ImageHipChallenge1" />
        </p>
        <div style="margin-bottom:10px;">
            <msdn:ImageHipChallenge ID="ImageHipChallenge1" runat="server" Height="100px" Width="280px" />
        </div>
        <sb:AnchorButton ID="trynewButton" runat="server" Text="Try another word" OnClick="trynewButton_Click" CausesValidation="false" />
    </ContentTemplate>        
</asp:UpdatePanel>        
