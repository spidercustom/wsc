﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterOrganization.ascx.cs" Inherits="ScholarBridge.Web.Common.RegisterOrganization" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register Src="~/Common/CaptchaControl.ascx" tagname="CaptchaControl" tagprefix="sb" %>
<%@ Register src="~/Common/Tip.ascx" tagname="Tip" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<h1><asp:Literal ID="pageTitle" runat="server" Text="Organization Regisration"/></h1>

<asp:MultiView runat="server" ID="CreateUserWizard" ActiveViewIndex="0">
    <asp:View ID="CaptchaView" runat="server">
        <p class="hookline">By completing this word verification, you help us prevent automated registerations.</p>
        <sb:CaptchaControl ID="CaptchaControl1" runat="server" /><br /> 
        <sbCommon:AnchorButton ID="RegisterButton" runat="server" Text="Continue to registration" OnClick="RegisterButton_Click" CausesValidation="true" />
    </asp:View>    
    <asp:View ID="DataEntryView" runat="server">
        <p class="hookline">Please fill out this form to complete your registration. You can also ensure our e-mails don't end up in your spam folder by adding noreply@theWashBoard.org to your safe senders list.</p>

        <h2>Administrator Information</h2>
        <p>As your organization's administrator, you will be responsible for: </p>
        <ul>
            <li>Maintaining your organization's information.</li>
            <li>Adding and removing other users at your organization.</li>
            <li>Establishing relationships in the system with providers or intermediaries.</li>
        </ul>

        <div class="question_list">
            <div>
                <label>First Name:</label>
                <asp:TextBox ID="FirstName" runat="server" MaxLength="40"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="FirstNameRequired" runat="server" ControlToValidate="FirstName" ErrorMessage="First Name is required." ToolTip="First Name must be entered." EnableClientScript="true"></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="firstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" />
            </div>
            <div>
                <label>Last Name:</label>
                <asp:TextBox ID="LastName" runat="server" MaxLength="40"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="LastNameRequired" runat="server" ControlToValidate="LastName" ErrorMessage="Last Name is required." ToolTip="Last Name must be entered."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" />
            </div>
            <div>
                <label>Email Address:</label>
                <asp:TextBox ID="UserName" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="EmailAddressRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Email address is required." ToolTip="Email address must be entered."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" />
                <asp:CustomValidator ID="EmailCustomValidator" runat="server"></asp:CustomValidator>
                <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
            </div>
            <div>
                <label>Confirm Email Address:</label>
                <asp:TextBox ID="ConfirmEmail" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ErrorMessage="Email confirmation is required" ID="confirmEmailRequired" ControlToValidate="ConfirmEmail" runat="server" ></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address."/>
            </div>
            <div>
                <label>Password:</label>
                <asp:TextBox runat="server" ID="Password" MaxLength="20" ToolTip="Your password must be at least 7 characters with at least one special character or number, Example: p@sword" CssClass="tip"></asp:TextBox>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User" />
                <asp:RequiredFieldValidator Display="Dynamic" ID="passwordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required."></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>Confirm Password:</label>
                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."></asp:CompareValidator>
            </div>
        </div>
        
        <h2>Organization Information</h2>
        <div class="question_list">
            <div>
                <label>Name:</label>
                <asp:TextBox ID="Name" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="OrgNameRequired" runat="server" ControlToValidate="Name" ErrorMessage="Organization name is required." ToolTip="Organization name must be selected."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="NameValidator" runat="server" ControlToValidate="Name" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Organization" />
            </div>
            <div>
                <label>Tax Id (EIN):<sb:Tip ID="Tip1" runat="server" content="Your Tax ID (EIN) is used, in part, to verify that you are offering scholarships with charitable intent and are one of the following: a tax exempt entity under Section 501(c) of the Internal Revenue Code, are affiliated with a 501(c) entity, are an accredited higher education institution in the State of Washington that meets our criteria." /></label>
                <asp:TextBox ID="TaxId" runat="server" MaxLength="10" CssClass="ein"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="TaxIDRequired" runat="server" ControlToValidate="TaxId" ErrorMessage="Tax ID is required." ToolTip="Tax ID must be entered."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="TaxIdValidator" runat="server" ControlToValidate="TaxId" PropertyName="TaxId" SourceTypeName="ScholarBridge.Domain.Organization" />
            </div>
            <div>
                <label>Website (optional):</label>
                <asp:TextBox ID="Website" runat="server" MaxLength="128"></asp:TextBox>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="WebsiteValidator" runat="server" ControlToValidate="Website" PropertyName="Website" SourceTypeName="ScholarBridge.Domain.Organization" />
                <asp:customvalidator ID="WebSiteCustomValidator" runat="server"></asp:customvalidator>
            </div>
            <div>
                <label>Address Line 1:</label>
                <asp:TextBox ID="AddressStreet" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="AddressStreetRequired" runat="server" ControlToValidate="AddressStreet" ErrorMessage="Street Address is required." ToolTip="Street address must be entered."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
            </div>
            <div>
                <label>Address Line 2 (optional):</label>
                <asp:TextBox ID="AddressStreet2" runat="server" MaxLength="50"></asp:TextBox>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
            </div>
            <div>
                <label>City:</label>
                <asp:TextBox ID="AddressCity" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="CityRequired" runat="server" ControlToValidate="AddressCity" ErrorMessage="City is required." ToolTip="City must be entered."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
            </div>
            <div>
                <label>State:</label>
                <asp:DropDownList ID="AddressState" runat="server"></asp:DropDownList>
                <asp:RequiredFieldValidator Display="Dynamic" ID="StateRequired" runat="server" ControlToValidate="AddressState" ErrorMessage="State is required." ToolTip="State must be selected."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
            </div>
            <div>
                <label>Postal Code:</label>
                <asp:TextBox ID="AddressPostalCode" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ID="PostalCodeRequired" runat="server" ControlToValidate="AddressPostalCode" ErrorMessage="Postal Code is required." ToolTip="Postal code must be entered."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
            </div>
            <div>
                <label>Phone:</label>
                <asp:TextBox ID="Phone" runat="server" MaxLength="12" CssClass="phone"></asp:TextBox>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
                <asp:RequiredFieldValidator Display="Dynamic" ID="phoneRequired" runat="server" ControlToValidate="Phone" ErrorMessage="Phone number is required." ToolTip="Phone number must be entered."></asp:RequiredFieldValidator>
            </div>
            <div>
                <label>Fax (optional):</label>
                <asp:TextBox ID="Fax" runat="server" MaxLength="12" CssClass="phone"></asp:TextBox>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="FaxValidator" runat="server" ControlToValidate="Fax" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
            </div>
            <div>
                <label>Other Phone (optional):</label>
                <asp:TextBox ID="OtherPhone" runat="server" MaxLength="12" CssClass="phone"></asp:TextBox>
                <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic" ID="OtherPhoneValidator" runat="server" ControlToValidate="OtherPhone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
            </div>
        </div>
        
       <p>By registering as a Provider or Intermediary with theWashBoard.org, you agree that you are offering scholarships with charitable intent and your scholarships will benefit either  residents of the State of Washington, or (ii) students attending an accredited Washington higher education institution.</p>
       <p>To see the full provider and intermediary Terms of Use, please see the Terms Applicable to Scholarship Intermediaries and Providers section of the <A class="GreenLink" href='<%= LinkGenerator.GetFullLink("/Terms.aspx") %>'>Terms of Use</A></p>                        
       <sbCommon:AnchorButton ID="CompleteRegistration" runat="server" Text="Complete registration" OnClick="CompleteRegistration_Click" CausesValidation="true" />
    </asp:View>
    <asp:View ID="CompletionView" runat="server">
        <div id="emailSuccess" runat="server">
            <p class="hookline">Thank you for registering! An email has been sent to the email address you provided with instructions to complete the registration process. Please follow the instructions in the e-mail to enable your account.</p>
        </div>
        <p id="emailFail" runat="server" visible="false">
            <p class="hookline">There was a problem sending a confirmation email so no account was created. Please contact us for assistance.</p>
        </p>
    </asp:View>
</asp:MultiView>
