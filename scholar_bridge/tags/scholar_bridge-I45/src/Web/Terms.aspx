﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Terms.aspx.cs" Inherits="ScholarBridge.Web.Terms" Title="Terms and Conditions" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Terms and Conditions" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Terms &amp; Conditions</h2>
    <p class="hookline">theWashBoard.org is a no-fee, advertising-free, and charitably-motivated community resource for both colleges and residents of Washington State.  We do not share or sell personal information.</p>
    <ul>
        <li><asp:HyperLink ID="privacyStatementLnk" runat="server" NavigateUrl="~/PrivacyStatement.aspx">Privacy Policy Statement</asp:HyperLink></li>
        <li><asp:HyperLink ID="seekerTermsLnk" runat="server" NavigateUrl="~/TermsOfUse.aspx">Terms Of Use</asp:HyperLink></li>
        <li>System Requirements<br />
            <div style="margin-left:20px; margin-top:8px; font-weight:bold;">Supported Internet Browsers</div>
            <ul>
                <li>Internet Explorer 7.0 or higher</li>
                <li>Mozilla Firefox 2.0 or higher</li>
                <li>Safari 3.1 or higher</li>
                <li>Google Chrome 3 or higher</li>
            </ul>
            <div style="margin-left:20px; margin-top:8px; font-weight:bold;">Plug-ins</div>
            <ul>
                <li><a href="http://get.adobe.com/reader/">Adobe Acrobat Reader</a></li>
            </ul>
        </li>
    </ul>
</asp:Content>
