﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Message.Show" Title="Message | Show" %>

<%@ Register TagPrefix="sb" TagName="MessageView" Src="~/Common/MessageView.ascx" %>
<%@ Register TagPrefix="sb" TagName="SentMessageView" Src="~/Common/SentMessageView.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div class="subsection">

    <sb:MessageView id="messageView" runat="server" ListUrl="~/Message/Default.aspx" />
    <sb:SentMessageView id="sentMessageView" runat="server" ListUrl="~/Message/Default.aspx" />
    <sbCommon:AnchorButton ID="CloseBtn" runat="server" Text="Close"  
        onclick="CloseBtn_Click" />
</div>        
</asp:Content>
