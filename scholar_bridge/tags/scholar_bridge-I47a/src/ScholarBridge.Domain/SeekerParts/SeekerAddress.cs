﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Contact;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.Resources;

namespace ScholarBridge.Domain.SeekerParts
{
	/// <summary>
	/// The seeker address handles both in-state (WA) and out-of-state seekers, including County.
	/// For in-state, the city & county are lookups while for out-of-state, the city & county are simple strings.
	/// </summary>
    public class SeekerAddress : AddressBase
    {
		public virtual City CityInState { get; set; }

		[StringLengthValidator(0, 50, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string CityOutOfState { get; set; }

		public virtual County CountyInState { get; set; }

		[StringLengthValidator(0, 50, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string CountyOutOfState { get; set; }

		[NotNullValidator(MessageTemplate = "City must be specified", Ruleset = ADDRESS_REQUIRED)]
        public override string GetCity
        {
			get
			{
				return State == null
					? CityInState == null ? (string.IsNullOrEmpty(CityOutOfState) ? null : CityOutOfState) : CityInState.Name
					: State.Abbreviation == Location.State.WASHINGTON_STATE_ID
				       	? CityInState == null ? null : CityInState.Name
				       	: string.IsNullOrEmpty(CityOutOfState) ? null : CityOutOfState;
			}
        }

		[NotNullValidator(MessageTemplate = "County must be specified", Ruleset = ADDRESS_REQUIRED)]
		public string GetCounty
		{
			get
			{
				return State == null
					? CountyInState == null ? (string.IsNullOrEmpty(CountyOutOfState) ? null : CountyOutOfState) : CountyInState.Name
					: State.Abbreviation == Location.State.WASHINGTON_STATE_ID
						? CountyInState == null ? null : CountyInState.Name
						: string.IsNullOrEmpty(CountyOutOfState) ? null : CountyOutOfState;
			}
		}
         
        public override ValidationResults ValidateAsRequired()
        {
            var validator = ValidationFactory.CreateValidator<SeekerAddress>(ADDRESS_REQUIRED);
            return validator.Validate(this);
        }
    }
}