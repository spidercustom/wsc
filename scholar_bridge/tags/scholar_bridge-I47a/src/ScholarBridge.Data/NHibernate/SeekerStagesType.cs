using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>SeekerStages</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class SeekerStagesType : EnumStringType
    {
        public SeekerStagesType()
            : base(typeof(SeekerStage), 50)
        {
        }
    }
}