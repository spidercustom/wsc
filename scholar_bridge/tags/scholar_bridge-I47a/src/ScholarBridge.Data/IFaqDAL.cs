﻿using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
	public interface IFaqDAL : IDAL<Faq>
	{
		Faq FindById(int id);
		Faq Save(Faq faq);
	}
}