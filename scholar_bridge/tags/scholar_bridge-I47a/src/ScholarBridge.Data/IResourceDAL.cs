﻿using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
	public interface IResourceDAL : IDAL<Resource>
	{
		Resource FindById(int id);
		Resource Save(Resource article);
	}
}