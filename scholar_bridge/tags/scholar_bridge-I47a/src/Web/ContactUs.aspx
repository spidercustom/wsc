﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="ScholarBridge.Web.ContactUs" Title="Contact Us" %>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Contact Page" />
    <style type="text/css">
        #PageContent ul > li {margin-top:8px;}
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Contact Us</h2>
    <asp:panel ID="contactUsSelectionPanel" runat="server">
    <p class="hookline">The Washington Scholarship Coalition is committed to making theWashBoard.org your resource for scholarships.  We want to hear from you!</p>
    
    <h3>E-mail</h3>
    <ul>
        <li>
            <asp:linkbutton ID="supportMessageLink" runat="server" ToolTip="Contact our support team for help using theWashBoard."
                onclick="supportMessageLink_Click">Website Support</asp:linkbutton>
        </li>
        <li>
            <asp:linkbutton ID="problemMessageLink"  runat="server" ToolTip="Click here to alert the theWashBoard.org administrator."
                onclick="problemMessageLink_Click">Report a Problem</asp:linkbutton>
        </li>
        <li>
            <asp:linkbutton ID="suggestionMessageLink" runat="server" tooltip="Send your ideas to theWashBoard development team."
                onclick="suggestionMessageLink_Click">Suggestions</asp:linkbutton>
        </li>
    </ul>

    <h3>Phone</h3>
    You can reach us at 888-535-0747, option #8. Our support hours are 8:00 am – 5:00 pm PST Monday thru Friday.
   
    </asp:panel>

    <asp:panel runat="server" ID="contactUsMessagePanel">
        <uc1:ContactUs ID="contactUsControl" OnFormCanceled="contactUsControl_Canceled" OnFormSent="contactUsControl_Sent" runat="server" />
    </asp:panel>

</asp:Content>
