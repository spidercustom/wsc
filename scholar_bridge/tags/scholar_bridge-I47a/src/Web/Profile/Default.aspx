﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Profile.Default" Title="Profile" %>

<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="EditUserName" Src="~/Common/EditUserName.ascx" %>
<%@ Register TagPrefix="sb" TagName="EditOrganizationUserEmail" Src="~/Common/EditOrganizationUserEmail.ascx" %>
<%@ Register Src="~/Common/SeekerProfileProgress.ascx" TagName="SeekerProfileProgress" TagPrefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <link href="<%= ResolveUrl("~/styles/form.css") %>" rel="stylesheet" type="text/css" media="All"/> 

    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            $("#pnlchangepassword").keypress(function(e) {
                if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                    $("#pnlchangepassword > a[id$='_ChangePasswordPushButton']").click();
                    return true;
                }
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <asp:LoginView ID="loginView2" runat="server">
        <RoleGroups>
            <asp:RoleGroup Roles="Seeker, Influencer">
                <ContentTemplate>
                    <div id="ProfileCompleteness">
                        <sb:SeekerProfileProgress ID="SeekerProfileProgress1" runat="server" ShowGotoProfile="false"/>
                    </div>
                    <h1><img alt="My Settings" src="<%= ResolveUrl("~/images/titles/My-Settings.gif") %>" width="190px" height="22px"/></h1>
                    <p class="hookline">
                        This page allows you to change settings related to how you access theWashBoard.org. 
                        Visit your <a href="<%= ResolveUrl("~/Seeker/Profile") %>">profile</a>
                        if you would like to update your personal information instead.
                    </p>
                </ContentTemplate>
            </asp:RoleGroup>
            <asp:RoleGroup Roles="Intermediary, Intermediary Admin, Provider, Provider Admin">
                <ContentTemplate>
                    <h1><img alt="My Settings" src="<%= ResolveUrl("~/images/titles/My-Settings.gif") %>" width="190px" height="22px"/></h1>
                     <p class="hookline">This page allows you to change settings related to how you access theWashBoard.org. </p>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
    <div class="clear"></div>
    <div id="UserInfo" runat="server" style="margin-bottom:15px; font-size:12px;"></div>
    <div class="two-column with_divider">
        <div>
            <h2>Change Password</h2>
            <asp:ChangePassword ID="ChangePassword1" 
                                runat="server" 
                                OnChangePasswordError="ChangePassword1_Error" 
                                SuccessPageUrl="~/Profile/Default.aspx" 
                                OnChangedPassword="ChangePassword1_ChangedPassword" 
                                BorderPadding="4" 
                                ChangePasswordButtonType="Image" 
                                ChangePasswordButtonImageUrl="~/images/btn_save.gif" 
                                CancelButtonStyle-CssClass="hidden" 
                                OnChangingPassword="ChangePassword1_OnChangingPassword">
                <ChangePasswordTemplate>
                    <div class="question_list">
                        <div>
                            <label>Current Password</label>
                            <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CurrentPassowordCustomValidator" runat="server" ControlToValidate="CurrentPassword"   ValidationGroup="ChangePassword1"  />
                        </div>
                        <div>
                            <label>New Password</label>
                            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" ToolTip="Your password must be at least 7 characters with at least one special character or number, Example: p@sword" CssClass="tip"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="NewPasswordRequired" 
                                                        runat="server" 
                                                        ControlToValidate="NewPassword"
                                                        ErrorMessage="New Password is required." 
                                                        ToolTip="New Password is required."
                                                        ValidationGroup="ChangePassword1">*Required</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="NewPassowrdCustomValidator" runat="server" ControlToValidate="NewPassword" />
                        </div>
                        <div>
                            <label>Confirm Password</label>
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                ValidationGroup="ChangePassword1">*Required</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                ValidationGroup="ChangePassword1"></asp:CompareValidator>
                        </div>
                        <div>
                            <label></label>
                            <sbCommon:AnchorButton ID="ChangePasswordPushButton" runat="server" Text="Change Password" CommandName="ChangePassword" ValidationGroup="ChangePassword1" />
                        </div>
                        <div>
                            <asp:Label ID="FailureText" Style="color: Red;" runat="server" EnableViewState="False"></asp:Label>
                        </div>
                    </div>
                </ChangePasswordTemplate>
            </asp:ChangePassword>
            <div id="ChangePasswordValidationErrorPopup" title="Change Password Failed!" style="display: none">
                <asp:Image ID="ValidationErrorPopupimage" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/cooltipicon_errorbig.png" />
                <asp:Label ID="ChangePasswordValidationErrorPopupLabel" runat="server" Text="Password must be at least 7 characters with at least one special character or number." />
            </div>
        </div>
        <div>
            <sb:EditOrganizationUserEmail runat="server" ID="editUserEmail" />
        </div>        
    </div>
</asp:Content>
