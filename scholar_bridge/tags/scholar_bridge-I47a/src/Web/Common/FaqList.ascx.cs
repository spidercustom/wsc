﻿using System;
using System.Collections.Generic;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
	public partial class FaqList : System.Web.UI.UserControl
	{
		public IFaqService FaqService { get; set; }

		public FaqType FaqType
		{
			get
			{
				if (ViewState["FaqType"] == null)
				{
					return FaqType.General;
				}
				return (FaqType) ViewState["FaqType"];
			}
			set
			{
				ViewState["FaqType"] = value;
			}
		}
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				IList<Faq> faqList = FaqService.FindAll(FaqType);
				faqRepeater.DataSource = faqList;
				faqRepeater.DataBind();
			}
		}
	}
}