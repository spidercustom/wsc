﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationFinancialNeedShow.ascx.cs" Inherits="ScholarBridge.Web.Common.ApplicationFinancialNeedShow" %>

<h3>Financial Need</h3>
<br />
<div>
<div class="viewLabel">Types of Support:</div> 
<div class="viewValue">
<asp:Repeater ID="typesOfSupport" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
</div>
<br />