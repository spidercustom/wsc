﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Seeker.BuildApplication;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationBasicsShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                applicantName.Text = ApplicationToView.Seeker.Name.NameFirstLast;
                applicantEmail.Text = ApplicationToView.Seeker.User.Email;
                if (ApplicationToView.DateOfBirth.HasValue)
                    dob.Text = ApplicationToView.DateOfBirth.Value.ToString("MM/dd/yyyy");
                if (null != ApplicationToView.Seeker.Phone)
                    phoneNumber.Text = ApplicationToView.Seeker.Phone.Number;
                if (null != ApplicationToView.Seeker.MobilePhone)
                    mobileNumber.Text = ApplicationToView.Seeker.MobilePhone.Number;
                gender.Text = ApplicationToView.Gender.ToString();

                if (null != ApplicationToView.Seeker.Address)
                    address.Text = ApplicationToView.Seeker.Address.ToString().Replace("\r\n", "<br/>\r\n");

                if (!string.IsNullOrEmpty(ApplicationToView.Seeker.Address.GetCounty))
                    county.Text = ApplicationToView.Seeker.Address.GetCounty;

                religions.DataSource = ApplicationToView.Religions;
                religions.DataBind();
                religionsOther.Text = ApplicationToView.ReligionOther;

                heritages.DataSource = ApplicationToView.Ethnicities;
                heritages.DataBind();
                heritagesOther.Text = ApplicationToView.EthnicityOther;

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);
            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            EthnicityRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Ethnicity);
            ReligionRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Religion);
            GendersRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Gender);
        }

        public override int ResumeFrom
        {
            get { return WizardStepName.Basics.GetNumericValue(); }
        }
    }
}