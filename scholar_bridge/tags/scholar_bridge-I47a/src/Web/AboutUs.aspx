﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="ScholarBridge.Web.AboutUs" Title="About Us" %>
<%@ Register src="~/PressRoom/ArticleList.ascx" tagname="ArticleList" tagprefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="About the theWashBoard.org" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h2>About Us</h2>
    <p class="hookline">
        <b>Washington Scholarship Coalition &mdash; Making college possible, one person at a time.</b><br />
        We are a partnership of public and nonprofit agencies coming together to build an online 
        scholarship marketplace and provide a trusted source of scholarships. The 
        Washington Scholarship Coalition exists to create connections and ensure 
        scholarship funds are reaching those in need.
    </p>

    <h3 style="margin:30px 0px 10px 0px;">Our Goals</h3>
    <ul>
        <li style="margin-top:.75em;">Increase access to scholarships for Washington students, with emphasis around low-income and underserved students</li>
        <li style="margin-top:.75em;">Increase awareness of financial aid &amp; scholarship opportunities</li>
        <li style="margin-top:.75em;">Increase private scholarship funding by providing donors with information on effective, efficient ways to contribute</li>
        <li style="margin-top:.75em;">Improve efficiencies in private scholarship application process</li>
        <li style="margin-top:.75em;">Improve availability of data on private scholarships for scholarship-seeking students</li>
    </ul>  
    
    <h3 style="margin:30px 0px 10px 0px;">Steering Committee</h3>
    <div id="CoalitionPartnersLogoList">
            <a href="http://www.hecb.wa.gov/"><img style="width:200px; margin:0px 30px;" alt="Washington Higher Education Coordinating Board" src="images/PartnerLogos/HECB+LgLogo.jpg" /></a>
            <a href="http://www.collegespark.org/"><img style="width:200px; margin:0px 30px;" alt="College Spark Washington" src="images/PartnerLogos/CS_logo_Tag_color.png" /></a><br /><br /><br />
            <a href="http://www.nela.net/"><img style="width:200px; margin:0px 30px;" alt="Nela" src="images/PartnerLogos/nela_logo.jpg" /></a>
            <a href="http://www.seattlefoundation.org/"><img style="width:200px; margin:0px 30px;" alt="Seattle Foundation" src="images/PartnerLogos/TSF_1line_blue_tag.jpg" /></a>
    </div>

    <h3 style="margin:30px 0px 10px 0px;">Members and Partners</h3>
    <a href="http://www.tacomafoundation.org/"><img alt="The Greater Tacoma Community Foundation" src="images/PartnerLogos/the_greater_tacoma_comm_found.gif" /></a><br />
    <a href="http://www.icwashington.org/"><img alt="Independent Colleges of Washington" src="images/PartnerLogos/independent_colleges_of_washington.gif" /></a><br />
    <a href="http://www.collegesuccessfoundation.org/"><img style="margin:5px 0px;" alt="The College Success Foundation" src="images/PartnerLogos/college_success_foundation.gif" /></a><br />
    <a href="http://www.collegeplan.org/"><img alt="College Planning Network" src="images/PartnerLogos/college_planning_network.gif" /></a>

    <h3 style="margin:30px 0px 10px 0px;">Contact the Washington Scholarship Coalition c/o</h3>
    <p>
        Northwest Education Loan Association<br />
        Danette Knudson, Director, External Relations<br />
        (206) 461-5491<br />
        danette.knudson@nela.net
    </p>
</asp:Content>