﻿<%@ Page Language="C#" MasterPageFile="~/PrintView.master" AutoEventWireup="true" CodeBehind="PrintScholarship.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Scholarships.PrintScholarship" Title="Intermediary| Scholarships | Show" %>
<%@ Register src="~/Common/PrintScholarship.ascx" tagname="PrintScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PrintViewHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
    <div id="PreviewOptions" class="non-printable">
        <a class="button" onclick="window.print();"><span>Print This Page</span></a>
        <sbCommon:AnchorButton ID="EmailtoOthers" NavigateUrl="mailto:"  runat="server" Text="Email to Others" />
    </div>


<div class="subsection">
<sb:ScholarshipTitleStripe id="scholarshipTitleStripeControl" runat="server" HideInPrintView="true" />
    <sb:PrintScholarship id="showScholarship" runat="server" />
</div>
</asp:Content>
