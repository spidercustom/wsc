﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.FundingProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
 <style type="text/css">
     .large-labels > label 
    {
        width: 460px;
    }

</style>
<span><span class="requiredAttributeIndicator">*</span> = required</span><br />            
<h3>Build Scholarship – Financial Need</h3>
<p>Indicate with these fields if the applicant must demonstrate financial need and what information they may be asked to provide.</p>
<div class="form-iceland-container">
    <div class="form-iceland large-labels">
        <asp:PlaceHolder ID="FAFSAContainer" runat="server">
            <p class="form-section-title">Financial Information </p>
            <label>Applicant must demonstrate Financial Need?<span class="requiredAttributeIndicator">*</span></label> 
            <div class="control-set">
                <asp:radiobuttonlist ID="ApplicantNeedRequiredRadioList" runat="server" RepeatDirection="Horizontal" >
                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                    <asp:ListItem Text="No" Value="false"></asp:ListItem>
                </asp:radiobuttonlist>
            </div>
            <asp:requiredfieldvalidator ID="ApplicantNeedRadioRequired" runat="server" ControlToValidate="ApplicantNeedRequiredRadioList" ErrorMessage="Please specify whether financial need must be demonstrated in order to receive this scholarship."></asp:requiredfieldvalidator>
             <sb:CoolTipInfo ID="CoolTipInfo1" Content="Use to indicate if applicant must demonstrate Financial Need. Applicants will be required to answer the question 'What is your financial need? Describe your challenge?'" runat="server" />
       
            <br />
            <label>Applicant required to complete FAFSA?<span class="requiredAttributeIndicator">*</span></label> 
            <div class="control-set">
                <asp:radiobuttonlist ID="FAFSARequiredRadioList" runat="server" RepeatDirection="Horizontal" >
                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                    <asp:ListItem Text="No" Value="false"></asp:ListItem>
                </asp:radiobuttonlist>
            </div>
            <asp:requiredfieldvalidator ID="FAFSARadioRequired" runat="server" ControlToValidate="FAFSARequiredRadioList" ErrorMessage="Please indicate whether filling out a FAFSA is required."></asp:requiredfieldvalidator>
             <sb:CoolTipInfo ID="CoolTipInfo2" Content="Use to indicate if applicant must complete a FAFSA. For more info on FAFSA, see the link on the Resources page." runat="server" />
   
            <br />
      
            <label>Applicant required to provide FAFSA defined EFC (expected Family Contribution)?<span class="requiredAttributeIndicator">*</span></label> 
            <div class="control-set">
                <asp:radiobuttonlist ID="FAFSAEFCRequiredRadioList" runat="server" RepeatDirection="Horizontal" >
                    <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                    <asp:ListItem Text="No" Value="false"></asp:ListItem>
                </asp:radiobuttonlist>
            </div>
            <asp:requiredfieldvalidator ID="FAFSAEFCRadioRequired" runat="server" ControlToValidate="FAFSAEFCRequiredRadioList" ErrorMessage="Please specify whether the FAFSA EFC is required."></asp:requiredfieldvalidator>
               <sb:CoolTipInfo ID="CoolTipInfo3" Content="Use to indicate if applicant must provide their FAFSA defined EFC. For more info on FAFSA, see the link on the Resources page." runat="server" />
       
            <br />
        </asp:PlaceHolder> 
        
        <p>If you have additional questions, worksheets, or financial information forms that the applicant must complete, you can indicate these on the next tab, +Requirements.</p>
        
       </div>
       </div>
