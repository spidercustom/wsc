﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutMe.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.AboutMe" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register TagPrefix="sb" TagName="Tip" Src="~/Common/Tip.ascx" %>  

<h2>About me</h2>
<p>This is your chance to let Scholarship providers know who you are.  Add a personal statement, share your talents and gifts, and attach a writing sample or pictures. Providers want to know something about the person who is going to receive their scholarships. </p>
<div class="question_list">
    <div class="full">
        <label>
            <span class="tip" title="In 1,500 characters or less, say something about yourself, what you hope to achieve by going to college, what it would mean to you to receive a scholarship.">
                Personal Statement:
            </span>
        </label>
        <asp:TextBox ID="PersonalStatementControl" runat="server" TextMode="MultiLine" Rows="12"></asp:TextBox>
        <elv:PropertyProxyValidator ID="PersonalStatementValidator" runat="server" ControlToValidate="PersonalStatementControl" PropertyName="PersonalStatement" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <div class="full">
        <label>
            <span class="tip" title="Are you creative? a leader? This is a great way to briefly let scholarship providers know something about you. This is not used in the Scholarship match but will be included on your application.">
                5 Words That Describe Me:
            </span>
        </label>
        <asp:TextBox ID="FiveWordsControl" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="FiveWordsValidator" runat="server" ControlToValidate="FiveWordsControl" PropertyName="Words" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <div class="full">
        <label>
            <span class="tip" title="Are you a problem solver? imaginative? a great communicator? This is a great way to briefly let scholarship providers know your skills. This is not used in the Scholarship match but will be included on your application.">
                5 Skills I Have
            </span>
        </label>
        <asp:TextBox ID="FiveSkillsControl" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="FiveSkillsValidator" runat="server" ControlToValidate="FiveSkillsControl" PropertyName="Skills" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <div class="full">
        <label>My Talents & Gifts</label>
        <asp:TextBox ID="MyGiftControl" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
        <elv:PropertyProxyValidator ID="MyGiftValidator" runat="server" ControlToValidate="MyGiftControl" PropertyName="MyGift" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
</div>
