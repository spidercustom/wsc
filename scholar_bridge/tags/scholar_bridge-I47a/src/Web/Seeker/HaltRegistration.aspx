﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HaltRegistration.aspx.cs" Inherits="ScholarBridge.Web.Seeker.HaltRegistration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<%@ Register Src="~/Common/GlobalFooter.ascx" TagName="GlobalFooter" TagPrefix="sb" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>theWashBoard.org</title>
    <meta name="FORMAT" content="text/html" />
    <meta name="CHARSET" content="ISO-8859-1" />
    <meta name="DOCUMENTLANGUAGECODE" content="en" />
    <meta name="DOCUMENTCOUNTRYCODE" content="us" />
    <meta name="DC.LANGUAGE" scheme="rfc1766" content="en-us" />
    <meta name="COPYRIGHT" content="Copyright (c) 2009 by Washington Scholarship Coalition" />
    <meta name="SECURITY" content="Public" />
    <meta name="ROBOTS" content="index,follow" />
    <meta name="GOOGLEBOT" content="index,follow" />
    <meta name="Description" content="theWashBoard.org Provider Home " />
    <meta name="Keywords" content="" />
    <meta name="Author" content="theWashBoard.org" />
    <!-- Base keywords here-->

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
        google.load("jquery", "1.3");
    </script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.7.1.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tablesorter.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.pager.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tinysort.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskedinput.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.cookie.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.watermark-2.0.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/superfish.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/site.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/cooltip.js") %>"></script>

    <link href="<%= ResolveUrl("~/styles/WSCStyles.CSS") %>" rel="stylesheet" type="text/css" media="All" />
    
        <LINK href="<%= ResolveUrl("~/styles/global.css") %>" rel="stylesheet" type="text/css" media="All"/> 
</head>
<body>
    <form id="aspnetForm" runat="server">
    <!--Page wrapper starts here-->
    <div id="EntirePageWrapper">
        <div id="HeaderWrapper">
            <img alt="" src="<%= ResolveUrl("~/images/LogoMainHomepage.gif") %>" width="919px"
                height="151px" border="0" usemap="#Map" /><map id="map" name="Map"><area shape="rect"
                    coords="37,9,236,133" href="<%= ResolveUrl("~/") %>" alt="theWashBoard.org" /></map><img
                        alt="" src="<%= ResolveUrl("~/images/PicBottomOverAllHomepage.gif") %>" width="919px"
                        height="497px" />
        </div>
        <div class="send-email-container">
            <h1 id="sendEmailContainerTitle" runat="server" class="title"> - assigned based on config entry - </h1>
            <asp:Panel ID="AcceptEmailAddress" runat="server">
                <label for="emailTextBox">Enter your e-mail address to be notified when we launch</label>
                <br />
                <asp:TextBox id="emailTextBox" runat="server" CssClass="email-box"></asp:TextBox>
                <asp:ImageButton ID="submitButton" ImageUrl="~/images/submit_button.png" runat="server" CssClass="submit-button" OnClick="RegisterInterest" />
                <span class="errors-container">
                    <asp:RequiredFieldValidator ID="emailRequired" ControlToValidate="emailTextBox" Display="Dynamic" runat="server" ErrorMessage="Email address is required" ToolTip="Please enter your email address"/>
                    <elv:PropertyProxyValidator ID="emailValidator" runat="server" ControlToValidate="emailTextBox" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
                </span>
            </asp:Panel>
            <p id="ConfirmMessage" class="confirmation-message" runat="server" visible="false">
                Thank you for showing your interest. Your email address has been registered and we will notify you via email once registration is open.            
            </p>
        </div>
        <!--Login Text ends here-->
        <div style='clear: both;'>
        </div>
        <!--Branding picture starts here-->
        <div id="ContentWrapper01">
            <div id="LeftPageShadow">
                <img alt="" src="<%= ResolveUrl("~/images/LeftContentShadow.gif") %>" /></div>
            <div id="RightPageShadow">
                <img alt="" src="<%= ResolveUrl("~/images/RightContentShadow.gif") %>" /></div>
            <!--This is content wrapper 02 starts here-->
            <div id="ContentWrapper02">
                <div style='clear: both;'>
                </div>
                <!--  <HR> -->
                <div style='clear: both;'>
                </div>
                <!--The Three column starts here-->
                <div id="BoxWrapper">
                    <div id="LeftBottomBox">

                        <img alt="" src="<%= ResolveUrl("~/images/HomepageBottomBox01_ForSeekers.gif") %>"
                            width="262px" height="41px" />
                        <img alt="" src="<%= ResolveUrl("~/images/OverallHomepageBottomBox01.gif") %>" width="237px"
                            height="86px" />

                        <p style="width: 237px">
                            Create a profile and let us do the rest. We will match you with scholarships you
                            are most likely to qualify for and applying online is easy.
                            <br />
                            <br />
                        </p>
                        <p class="IcoBoxArrow">
                           <img alt="" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px" />&nbsp;<span id="seekerRegistrationLinkTitle" runat="server"  style="font-size:14px;" > - getting assigned based on config entry - </span></p>
                    </div>
                    <div id="CenterBottomBox">

                        <a href="<%= ResolveUrl("~/Intermediary/Anonymous.aspx") %>">
                            <img alt="" src="<%= ResolveUrl("~/images/HomepageBottomBox02_ForIntermediaries.gif") %>"
                                width="262px" height="41px" />
                            <img alt="" src="<%= ResolveUrl("~/images/OverallHomepageBottomBox02.gif") %>" width="237px"
                                height="86px" />
                        </a>

                        <p style="width: 237px">
                            Administer scholarships for providers. We give you the tools to connect students
                            with scholarships to ensure much needed funds are awarded.</p>
                        <p class="IcoBoxArrow">

                            <a href="<%= ResolveUrl("~/Intermediary/RegisterIntermediary.aspx") %>" style="font-size:14px;" >
                                <img alt="" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px" />&nbsp;Intermediary
                                Registration</a></p>
                    </div>
                    <div id="RightBottomBox">

                        <a href="<%= ResolveUrl("~/Provider/Anonymous.aspx") %>">
                            <img alt="" src="<%= ResolveUrl("~/images/HomepageBottomBox03_ForProviders.gif") %>"
                                width="262px" height="41px" />
                            <img alt="" src="<%= ResolveUrl("~/images/OverallHomepageBottomBox03.gif") %>" width="237px"
                                height="86px" />
                        </a>

                        <p style="width: 237px">
                            Post your scholarship to reach a larger group of students. We make reviewing and
                            evaluating applications easy. Find the next recipients of your scholarship.</p>
                        <p class="IcoBoxArrow">

                            <a href="<%= ResolveUrl("~/Provider/RegisterProvider.aspx") %>" style="font-size:14px;" >
                                <img alt="" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px" />&nbsp;Provider
                                Registration</a></p>
                    </div>
                </div>
                <br />
                <div style='clear: both;'>
                </div>
                <hr />
                <div class="initiative-documents-list">
                    <asp:HyperLink id="twoPageSummary" runat="server" NavigateUrl="~/theWashBoard_Summary.pdf" Text="Click here to view a 2-page document outlining the initiative."/>
                    <br />
                    <span class="reader-links">
                        You will need Acrobat Reader to view the document. You can download it for free <a href="http://get.adobe.com/reader/">here</a>.
                    </span>
                </div>
                <hr />
                <div id="Footer">
                    <sb:GlobalFooter ID="GlobalFooter1" runat="server" />
                </div>
            </div>
            <!--This is content wrapper 02 ends here-->
        </div>
        <!--This is the outer most wrapper 01 ends here-->
    </div>
    <!--Page wrapper ends here-->
    </form>
</body>
</html>
