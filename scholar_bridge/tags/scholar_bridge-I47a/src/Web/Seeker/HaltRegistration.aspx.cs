﻿using System;
using System.Configuration;
using System.Web;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Seeker
{
    public partial class HaltRegistration : System.Web.UI.Page
    {
        public IUserService UserService { get; set; }
        public static string HaltMessage
        {
            get
            {
                var reader = new AppSettingsReader();
                return reader.GetValue("DisableSeekerRegistrationWithMessage", typeof(string)).ToString();
            }
        }

        public static bool CheckRegistrationHalted()
        {
            bool isHalted = !String.IsNullOrEmpty(HaltMessage); 
            if (isHalted)
                HttpContext.Current.Response.Redirect("~/Seeker/HaltRegistration.aspx");

            return isHalted;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(HaltMessage))
            {
                sendEmailContainerTitle.InnerText = HaltMessage;
                seekerRegistrationLinkTitle.InnerText = HaltMessage;
            }
            else
            {
                Response.Redirect("~/Seeker/Anonymous.aspx");
            }
        }

        protected void RegisterInterest(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                UserService.SendSeekerRegistrationInterestToAdmin(emailTextBox.Text);
                AcceptEmailAddress.Visible = false;
                ConfirmMessage.Visible = true;
            }
        }
    }
}
