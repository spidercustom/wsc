﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Seeker.Applications
{
    public partial class OnlineApplication : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IApplicationService ApplicationService { get; set; }

        private const string APPLICATIONS_PAGEURL = "/Seeker/Scholarships";
        
        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["sid"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter aid");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            var scholarship = ScholarshipService.GetById(ScholarshipId);

            var application = ApplicationService.FindBySeekerandScholarship(UserContext.CurrentSeeker, scholarship);
            if (application == null)
            {
                //Create New one
                application = new Application(ApplicationType.External)
                                  {
                                      //intialize default values here
                                      Seeker = UserContext.CurrentSeeker,
                                      Scholarship = scholarship,
                                  };
                application.CopyFromSeekerandScholarship();
                application.Submit();

                application.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                ApplicationService.SaveNew(application);
            }

            PopupHelper.OpenURLFullWindow(scholarship.OnlineApplicationUrl);
            RedirectSelf(LinkGenerator.GetFullLinkStatic(APPLICATIONS_PAGEURL));
        }

        public static void RedirectSelf(string url)
        {
           
            var scriptname = "redirectself_script";
            var script = "window.location='{0}'".Build( url);
            var page = HttpContext.Current.CurrentHandler as Page;
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);

        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect(APPLICATIONS_PAGEURL);
        }
    }
}
