﻿<%@ Page Language="C#" MasterPageFile="~/PrintView.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Applications.Show" Title="Application | Show" %>
<%@ Register src="~/Common/PrintApplication.ascx" tagname="PrintApplication" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
   <div id="PreviewOptions" class="non-printable">
        <a class="button" onclick="window.print();"><span>Print This Page</span></a>
        <sbCommon:AnchorButton ID="sendToFriendBtn" NavigateUrl="mailto:"  runat="server" Text="Send to a friend" />
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HideInPrintView="true" />
    <sb:PrintApplication id="printApplication" runat="server" />
</asp:Content>