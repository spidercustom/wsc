using System;
using NUnit.Framework;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class AcademicYearTests
    {

        [Test]
        public void to_string_creates_proper_format()
        {
            var ay = new AcademicYear(2009);
            Assert.AreEqual("2008 - 2009", ay.ToString());
        }

        [Test]
        public void year_for_date_handles_sept_dec()
        {
            var ay = AcademicYear.YearForDate(new DateTime(2009, 10, 1));
            Assert.AreEqual(2010, ay.Year);
        }

        [Test]
        public void year_for_date_handles_jan_aug()
        {
            var ay = AcademicYear.YearForDate(new DateTime(2009, 3, 1));
            Assert.AreEqual(2009, ay.Year);
        }

        [Test]
        public void scholarship_year_for_date_handles_sept_dec()
        {
            var ay = AcademicYear.ScholarshipYearForDate(new DateTime(2009, 10, 1));
            Assert.AreEqual(2011, ay.Year);
        }

        [Test]
        public void scholarship_year_for_date_handles_jan_aug()
        {
            var ay = AcademicYear.ScholarshipYearForDate(new DateTime(2009, 3, 1));
            Assert.AreEqual(2010, ay.Year);
        }
    }
}