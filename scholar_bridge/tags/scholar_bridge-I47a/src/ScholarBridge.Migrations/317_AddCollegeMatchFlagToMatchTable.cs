using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(317)]
	public class AddCollegeMatchFlagToMatchTable : Migration
    {
		private const string TABLE_NAME = "SBMatch";
		private const string COLUMN_NAME = "CollegeMatchFlag";

		public override void Up()
		{
			Database.AddColumn(TABLE_NAME, COLUMN_NAME, DbType.Boolean);
        }

		public override void Down()
		{
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
		}

    }
}