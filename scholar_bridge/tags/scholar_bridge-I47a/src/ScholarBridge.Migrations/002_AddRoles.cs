﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(2)]
    public class AddRoles : Migration
    {
        public const string TABLE_NAME = "SB_Role";
        protected static readonly string FK_LAST_UPDATED_BY = string.Format("FK_{0}_LastUpdatedBy", TABLE_NAME);
        protected static readonly string[] COLUMNS = new[] {"Id", "Name", "LastUpdatedBy", "LastUpdateDate"};

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                              new Column(COLUMNS[1], DbType.String, 50, ColumnProperty.NotNull),
                              new Column(COLUMNS[2], DbType.Int32, ColumnProperty.NotNull),
                              new Column(COLUMNS[3], DbType.DateTime, ColumnProperty.Null, "(getdate())")
                );

            Database.AddForeignKey(FK_LAST_UPDATED_BY, TABLE_NAME, COLUMNS[2], "SB_User", "Id");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_LAST_UPDATED_BY);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}