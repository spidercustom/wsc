﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(82)]
    public class AddNewScholarshipStages : Migration
    {
        public const string TABLE_NAME = "SBScholarshipStageLUT";
        public const string PRIMARY_KEY_COLUMN = "SBScholarshipStageIndex";

        public override void Up()
        {
            var columns = new[] {PRIMARY_KEY_COLUMN, "SBScholarshipStage"};
            Database.Insert(TABLE_NAME, columns, new[] { "7", "Activated Scholarship" });
            Database.Insert(TABLE_NAME, columns, new[] { "8", "Awarded Scholarship" });
        }

        public override void Down()
        {
            Database.Delete(TABLE_NAME, PRIMARY_KEY_COLUMN, "7");
            Database.Delete(TABLE_NAME, PRIMARY_KEY_COLUMN, "8");
        }
    }
}
