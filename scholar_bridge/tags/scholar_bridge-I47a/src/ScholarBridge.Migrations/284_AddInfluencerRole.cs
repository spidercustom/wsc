﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(284)]
	public class AddInfluencerRole : Migration
    {
		private const string TABLE_NAME = "SBRoleLUT";
		private readonly string[] COLUMNS = new[] { "SBRole", "LastUpdateBy" };
		private const string ROLE_NAME = "Influencer";

		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
			Database.Insert(TABLE_NAME, COLUMNS, new[] { ROLE_NAME, adminId.ToString() });
		}

		public override void Down()
		{
			Database.Delete(TABLE_NAME, "SBRole", ROLE_NAME);
		}

    }
}