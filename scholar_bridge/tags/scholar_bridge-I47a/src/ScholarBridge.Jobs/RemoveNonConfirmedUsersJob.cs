using System;
using Common.Logging;
using Quartz;
using ScholarBridge.Data;

namespace ScholarBridge.Jobs
{
    public class RemoveNonConfirmedUsersJob : NHibernateBaseJob
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RemoveNonConfirmedUsersJob));

        public int NonConfirmedUserDays { get; set; }
        private ICleanupDAL cleanupDal;

        // Set only so it doesn't get serialized
        public ICleanupDAL CleanupDAL { set { cleanupDal = value; } }

        protected override void ExecuteTransactional(JobExecutionContext context)
        {
            if (NonConfirmedUserDays <= 0)
            {
                log.Info("Using default NonConfirmedUserDays value.");
                NonConfirmedUserDays = 5;
            }

            log.Info(String.Format("Removing non-confirmed Users that are more than '{0}' days old", NonConfirmedUserDays));
            cleanupDal.RemoveNonConfirmedUsersFromDatabase(NonConfirmedUserDays);
        }
    }
}