using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IApplicationService
    {
        Application GetById(int id);

        void SaveNew(Application application);
        void Update(Application application);
        void Delete(Application application);

        void SubmitApplication(Application application);
        int CountBySeeker(Seeker seeker);
        IList<Application> FindAllSubmitted(Scholarship scholarship);
        IList<Application> FindAllSubmitted(int startIndex, int rowCount, string sortExpression, Scholarship scholarship);
        IList<Application> FindAllSubmitted(int startIndex, int rowCount, string sortExpression, Scholarship scholarship, string lastNameSearch);
        IList<Application> FindAllFinalists(Scholarship scholarship);
        int CountAllSubmitted(Scholarship scholarship, string lastNameSearch);
        int CountAllSubmitted(Scholarship scholarship);
        void Finalist(Application application);
        void NotFinalist(Application application);

		Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship);
        IList<Application> FindBySeeker(int startIndex, int rowCount, string sortExpression, Seeker seeker);
        int CountAllHavingAttachment(Attachment attachment);
        int CountAllSubmittedBySeeker(Seeker seeker);
		int CountAllSubmitted();
		int CountAllSavedButNotSubmitted();

        void OfferScholarship(Application application, User awarder);
        void AwardScholarship(Application application, User awarder);
        void NotAwardScholarship(Application application, User awarder);
    }
}