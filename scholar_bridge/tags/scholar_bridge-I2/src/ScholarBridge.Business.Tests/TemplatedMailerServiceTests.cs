﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;
using ScholarBridge.Domain;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class TemplatedMailerServiceTests
    {

        private readonly TemplatedMailerService mailerService = new TemplatedMailerService();

        [Test]
        public void ValidatesGoodMessage()
        {
            var expected = "Foo Bar Baz";
            var body = new StringBuilder("##FOO## ##BAR## ##BAZ##");
            var vars = new Dictionary<string, string>
                           {
                               {"FOO", "Foo"},
                               {"BAR", "Bar"},
                               {"BAZ", "Baz"}
                           };
            Assert.AreEqual(expected, mailerService.ReplaceTemplateVariables(body, vars));
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ThrowsExceptionIfTemplateCanNotBeFound()
        {
            mailerService.SendMail(MailTemplate.ForgotPassword, "X:\\No\\Such\\Dir", null, "test@example.com", "test@example.com");
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ThrowsExceptionIfTemplateCanNotBeFound2()
        {
            var templateParams = new MailTemplateParams
                                     {
                                         TemplateDirectory = "X:\\No\\Such\\Dir",
                                         To = "test@example.com",
                                         From = "test@example.com"
                                     };
            mailerService.SendMail(MailTemplate.ForgotPassword, null, templateParams);
            Assert.Fail();
        }

    }
}