using System;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using System.Collections.Generic;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ProviderServiceTests
    {
        private MockRepository mocks;
        private IProviderDAL providerDAL;
        private IRoleDAL roleDAL;
        private IUserDAL userDAL;
        private ProviderService providerService;
        private ITemplatedMailerService mailerService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            providerDAL = mocks.StrictMock<IProviderDAL>();
            roleDAL = mocks.StrictMock<IRoleDAL>();
            userDAL = mocks.StrictMock<IUserDAL>();
            mailerService = mocks.StrictMock<ITemplatedMailerService>();
            providerService = new ProviderService { OrganizationDAL = providerDAL, RoleDAL = roleDAL, UserDAL = userDAL, MailerService=mailerService  };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(providerDAL);
            mocks.BackToRecord(roleDAL);
            mocks.BackToRecord(userDAL);
            mocks.BackToRecord(mailerService);
        }

        [Test]
        public void SaveNewProviderSetsUpProperRolesAndAdminUser()
        {
            // TODO: As more things are added, mock them out as well
            var p = new Provider {Name = "TestProvider"};
            var u = new User {Username = "NewUser"};
            Expect.Call(providerDAL.Insert(p)).Return(p);
            Expect.Call(roleDAL.FindByName(Role.PROVIDER_ROLE)).Return(new Role {Id = 3, Name = Role.PROVIDER_ROLE});
            Expect.Call(userDAL.Update(u)).Return(u);
            mocks.ReplayAll();

            providerService.SaveNewWithAdminUser(p, u);
            mocks.VerifyAll();

            Assert.AreEqual(u, p.AdminUser);
            CollectionAssert.IsNotEmpty(u.Roles);
            CollectionAssert.AllItemsAreNotNull(u.Roles);
        }

        [Test]
        public void save_new_user_sets_up_roles_and_adds_user()
        {
            // TODO: As more things are added, mock them out as well
            var provider = new Provider { Name = "Test Provider" };
            var user = new User { Username = "NewUser" };
            Expect.Call(providerDAL.Update(provider)).Return(provider);
            Expect.Call(roleDAL.FindByName(Role.PROVIDER_ROLE)).Return(new Role { Id = 3, Name = Role.PROVIDER_ROLE });
            mocks.ReplayAll();

            providerService.SaveNewUser(provider, user);
            mocks.VerifyAll();

            CollectionAssert.IsNotEmpty(provider.ActiveUsers);
            CollectionAssert.IsNotEmpty(user.Roles);
            CollectionAssert.AllItemsAreNotNull(user.Roles);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_new_user_throws_exception_if_org_null()
        {
            var user = new User { Username = "NewUser" };
            providerService.SaveNewUser(null, user);
            Assert.Fail();
        }

        [Test]
        public void delete_user_passes_to_user_dal()
        {
            var provider = new Provider { Name = "Test Provider" };
            var user = new User { Id = 1, Username = "NewUser" };
            provider.Users.Add(user);
            Expect.Call(() => userDAL.Delete(user));
            mocks.ReplayAll();

            providerService.DeleteUser(provider, user);
            mocks.VerifyAll();
        }

        [Test]
        public void reactivate_user_passes_to_user_dal()
        {
            var provider = new Provider { Name = "Test Provider" };
            var user = new User { Id = 1, Username = "NewUser" };
            provider.Users.Add(user);
            Expect.Call(userDAL.Update(user)).Return(user);
            mocks.ReplayAll();

            providerService.ReactivateUser(provider, user);
            Assert.That(user.IsDeleted, Is.False);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveNullThrowsException()
        {
            providerService.SaveNewWithAdminUser(null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateNullThrowsException()
        {
            providerService.Update(null);
            Assert.Fail();
        }

        [Test]
        public void ApproveProvider()
        {
            // TODO: As more things are added, mock them out as well
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var templateParams = new MailTemplateParams { From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp" };

            Expect.Call(providerDAL.Update(p)).Return(p);

            mailerService.SendMail(Arg<MailTemplate>.Is.Equal(MailTemplate.ProviderApproval),
                 Arg<Dictionary<string, string>>.Is.Anything,
                 Arg<MailTemplateParams>.Is.Equal(templateParams));
            
            mocks.ReplayAll();

            providerService.Approve(p, templateParams);

            Assert.AreEqual(ApprovalStatus.Approved, p.ApprovalStatus);
            Assert.IsTrue(p.AdminUser.IsApproved);
            mocks.VerifyAll();
        }

        [Test]
        public void RejectProvider()
        {
            // TODO: As more things are added, mock them out as well
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var templateParams = new MailTemplateParams { From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp" };

            Expect.Call(providerDAL.Update(p)).Return(p);
            mailerService.SendMail(Arg<MailTemplate>.Is.Equal(MailTemplate.ProviderRejection),
                Arg<Dictionary<string, string>>.Is.Anything,
                Arg<MailTemplateParams>.Is.Equal(templateParams));
            mocks.ReplayAll();

            providerService.Reject(p,templateParams );

            Assert.AreEqual(ApprovalStatus.Rejected, p.ApprovalStatus);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApprovalRequiresNonNullProvider()
        {
            providerService.Approve(null, new MailTemplateParams());
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RejectRequiresNonNullProvider()
        {
            providerService.Reject(null, new MailTemplateParams());
            Assert.Fail();
        }
    }
}