﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        private UserService userService;
        private IUserDAL userDAL;
        private IProviderDAL providerDAL;
        private ITemplatedMailerService mailerService;
        private MockRepository mocks;
        
        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            userDAL = mocks.StrictMock<IUserDAL>();
            providerDAL = mocks.StrictMock<IProviderDAL>();
            mailerService = mocks.StrictMock<ITemplatedMailerService>();
            userService = new UserService  { UserDAL = userDAL, ProviderDAL = providerDAL, MailerService = mailerService };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(userDAL);
            mocks.BackToRecord(providerDAL);
            mocks.BackToRecord(mailerService);
        }

        [Test]
        public void activate_provider_user()
        {
            var user = new User() { Id = 5 };
            var provider = new Provider {Name = "Provider"};
            var templateParams = new MailTemplateParams {From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp"};

            Expect.Call(userDAL.Update(user)).Return(user);
            Expect.Call(providerDAL.FindByUser(user)).Return(provider);
            mailerService.SendMail(Arg<MailTemplate>.Is.Equal(MailTemplate.ProviderApprovalEmailToHECBAdmin),
                Arg<Dictionary<string, string>>.Is.Anything,
                Arg<MailTemplateParams>.Is.Equal(templateParams));

            mocks.ReplayAll();

            userService.ActivateProviderUser(user, templateParams);
            Assert.IsTrue(user.IsActive);
            
            mocks.VerifyAll();
        }
        
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void activate_user_throws_exception_if_null_user_passed()
        {
            var templateParams = new MailTemplateParams { From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp" };
            userService.ActivateProviderUser(null, templateParams);
            Assert.Fail();
        }

        [Test]
        public void ResetUsername()
        {
            var user = new User() { Id = 5, IsActive = true};
            var templateParams = new MailTemplateParams { From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp" };

            Expect.Call(userDAL.Update(user)).Return(user);
            mailerService.SendMail(Arg<MailTemplate>.Is.Equal(MailTemplate.ConfirmationLink),
                Arg<Dictionary<string, string>>.Is.Anything,
                Arg<MailTemplateParams>.Is.Equal(templateParams));
            mocks.ReplayAll();

            userService.ResetUsername(user, "foo@bar.com", templateParams);
            Assert.IsFalse(user.IsActive);
            Assert.AreEqual(user.Username, user.Email);
            Assert.AreEqual("foo@bar.com", user.Email);

            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void resetusername_user_throws_exception_if_null_user_passed()
        {
            var templateParams = new MailTemplateParams { From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp" };
            userService.ResetUsername(null, "foo@bar.com", templateParams);
            Assert.Fail();
        }
    }
}
