using System;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using System.Collections.Generic;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class IntermediaryServiceTests
    {
        private MockRepository mocks;
        private IIntermediaryDAL intermediaryDAL;
        private IRoleDAL roleDAL;
        private IUserDAL userDAL;
        private IntermediaryService intermediaryService;
        private ITemplatedMailerService mailerService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            intermediaryDAL = mocks.StrictMock<IIntermediaryDAL>();
            roleDAL = mocks.StrictMock<IRoleDAL>();
            userDAL = mocks.StrictMock<IUserDAL>();
            mailerService = mocks.StrictMock<ITemplatedMailerService>();
            intermediaryService = new IntermediaryService { OrganizationDAL = intermediaryDAL, RoleDAL = roleDAL, UserDAL = userDAL, MailerService = mailerService };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(intermediaryDAL);
            mocks.BackToRecord(roleDAL);
            mocks.BackToRecord(userDAL);
            mocks.BackToRecord(mailerService);
        }

        [Test]
        public void SaveNewProviderSetsUpProperRolesAndAdminUser()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary {Name = "Test Intermediary"};
            var user = new User {Username = "NewUser"};
            Expect.Call(intermediaryDAL.Insert(intermediary)).Return(intermediary);
            Expect.Call(roleDAL.FindByName(Role.INTERMEDIARY_ROLE)).Return(new Role { Id = 3, Name = Role.INTERMEDIARY_ROLE });
            Expect.Call(userDAL.Update(user)).Return(user);
            mocks.ReplayAll();

            intermediaryService.SaveNewWithAdminUser(intermediary, user);
            mocks.VerifyAll();

            Assert.AreEqual(user, intermediary.AdminUser);
            CollectionAssert.IsNotEmpty(user.Roles);
            CollectionAssert.AllItemsAreNotNull(user.Roles);
        }

        [Test]
        public void save_new_user_sets_up_roles_and_adds_user()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary { Name = "Test Intermediary" };
            var user = new User { Username = "NewUser" };
            Expect.Call(intermediaryDAL.Update(intermediary)).Return(intermediary);
            Expect.Call(roleDAL.FindByName(Role.INTERMEDIARY_ROLE)).Return(new Role { Id = 3, Name = Role.INTERMEDIARY_ROLE });
            mocks.ReplayAll();

            intermediaryService.SaveNewUser(intermediary, user);
            mocks.VerifyAll();

            CollectionAssert.IsNotEmpty(intermediary.ActiveUsers);
            CollectionAssert.IsNotEmpty(user.Roles);
            CollectionAssert.AllItemsAreNotNull(user.Roles);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_new_user_throws_exception_if_org_null()
        {
            var user = new User { Username = "NewUser" };
            intermediaryService.SaveNewUser(null, user);
            Assert.Fail();
        }

        [Test]
        public void delete_user_passes_to_user_dal()
        {
            var intermediary = new Intermediary { Name = "Test Intermediary" };
            var user = new User { Id = 1, Username = "NewUser" };
            intermediary.Users.Add(user);
            Expect.Call(() => userDAL.Delete(user));
            mocks.ReplayAll();

            intermediaryService.DeleteUser(intermediary, user);
            mocks.VerifyAll();
        }

        [Test]
        public void reactivate_user_passes_to_user_dal()
        {
            var intermediary = new Intermediary { Name = "Test Intermediary" };
            var user = new User { Id = 1, Username = "NewUser" };
            intermediary.Users.Add(user);
            Expect.Call(userDAL.Update(user)).Return(user);
            mocks.ReplayAll();

            intermediaryService.ReactivateUser(intermediary, user);
            Assert.That(user.IsDeleted, Is.False);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveNullThrowsException()
        {
            intermediaryService.SaveNewWithAdminUser(null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateNullThrowsException()
        {
            intermediaryService.Update(null);
            Assert.Fail();
        }

        [Test]
        public void approve_sets_proper_statuses()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary { Name = "Test Intermediary", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var templateParams = new MailTemplateParams { From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp" };

            Expect.Call(intermediaryDAL.Update(intermediary)).Return(intermediary);

            mailerService.SendMail(Arg<MailTemplate>.Is.Equal(MailTemplate.IntermediaryApproval),
                 Arg<Dictionary<string, string>>.Is.Anything,
                 Arg<MailTemplateParams>.Is.Equal(templateParams));
            
            mocks.ReplayAll();

            intermediaryService.Approve(intermediary, templateParams);

            Assert.AreEqual(ApprovalStatus.Approved, intermediary.ApprovalStatus);
            Assert.IsTrue(intermediary.AdminUser.IsApproved);
            mocks.VerifyAll();
        }

        [Test]
        public void reject_sets_proper_statuses()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary { Name = "Test Intermediary", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var templateParams = new MailTemplateParams { From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp" };

            Expect.Call(intermediaryDAL.Update(intermediary)).Return(intermediary);
            mailerService.SendMail(Arg<MailTemplate>.Is.Equal(MailTemplate.IntermediaryRejection),
                Arg<Dictionary<string, string>>.Is.Anything,
                Arg<MailTemplateParams>.Is.Equal(templateParams));
            mocks.ReplayAll();

            intermediaryService.Reject(intermediary, templateParams);

            Assert.AreEqual(ApprovalStatus.Rejected, intermediary.ApprovalStatus);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApprovalRequiresNonNullProvider()
        {
            intermediaryService.Approve(null, new MailTemplateParams());
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RejectRequiresNonNullProvider()
        {
            intermediaryService.Reject(null, new MailTemplateParams());
            Assert.Fail();
        }
    }
}