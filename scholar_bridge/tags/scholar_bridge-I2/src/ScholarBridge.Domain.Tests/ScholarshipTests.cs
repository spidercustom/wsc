using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using NUnit.Framework;
using ScholarBridge.Common.DayOfYear;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class ScholarshipTests
    {
        [Test]
        public void scholarship_min_value_must_be_greater_than_zero()
        {
            var s = CreateValidScholarship();
            s.MinimumAmount = -1;
            var results = Validation.Validate(s);

            Assert.IsFalse(results.IsValid);
            Assert.AreEqual(1, results.Count);

            s.MinimumAmount = 0;
            var results2 = Validation.Validate(s);
            Assert.IsTrue(results2.IsValid);

        }

        [Test]
        public void scholarship_max_value_must_be_greater_than_one()
        {
            var s = CreateValidScholarship();
            s.MaximumAmount = 0;
            var results = Validation.Validate(s);

            Assert.IsFalse(results.IsValid);
            Assert.AreEqual(1, results.Count);

            s.MaximumAmount = 1;
            var results2 = Validation.Validate(s);
            Assert.IsTrue(results2.IsValid);
        }

        [Test]
        public void verify_clone_implimentation_exists()
        {
            var s = CreateValidScholarship();
            var cloneable = (ICloneable) s;
            var cloned = (Scholarship) cloneable.Clone();

            Assert.AreEqual(0, cloned.Id);
            Assert.AreEqual(0, cloned.Schedule.Id);
            
        }

        private Scholarship CreateValidScholarship()
        {
            var s = new Scholarship
                        {
                            Name = "Foo",
                            MissionStatement = "Accomplish things",
                            Schedule = new ScholarshipScheduleByMonthDay
                                           {
                                               StartFrom = new ByMonthDay(),
                                               AwardOn = new ByMonthDay(),
                                               DueOn = new ByMonthDay()
                                           },
                            Provider = new Provider { Name = "Foo Provider", ApprovalStatus = ApprovalStatus.Approved },
                            MinimumAmount = 0,
                            MaximumAmount = 100
                        };

            return s;
        }
    }
}