﻿using System.Collections.Generic;
using Spider.Common.Core.DAL;
using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IScholarshipDAL : IDAL<Scholarship>
    {
        Scholarship FindById(int id);
        IList<Scholarship> FindByProvider(Provider provider);
        IList<Scholarship> FindByStageAndProvider(ScholarshipStage scholarshipStage, Provider provider);
        Scholarship FindByNameAndProvider(string name, Provider provider);
        Scholarship Save(Scholarship scholarship);
    }
}    