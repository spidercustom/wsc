﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ISportDAL : ILookupDAL<Sport>
    {
    }
}
