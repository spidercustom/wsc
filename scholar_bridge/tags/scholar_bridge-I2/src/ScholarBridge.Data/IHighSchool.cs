﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IHighSchoolDAL : ILookupDAL<HighSchool>
    {
    }
}
