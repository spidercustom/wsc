﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ICareerDAL : ILookupDAL<Career>
    {
    }
}
