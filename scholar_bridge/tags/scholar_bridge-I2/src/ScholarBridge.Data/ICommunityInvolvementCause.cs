﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ICommunityInvolvementCauseDAL : ILookupDAL<CommunityInvolvementCause>
    {
    }
}
