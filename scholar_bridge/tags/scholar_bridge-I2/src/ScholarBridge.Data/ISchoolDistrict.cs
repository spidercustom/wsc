﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ISchoolDistrictDAL : ILookupDAL<SchoolDistrict>
    {
    }
}
