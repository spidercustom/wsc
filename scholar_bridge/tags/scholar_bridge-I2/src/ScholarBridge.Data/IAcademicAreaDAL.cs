﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IAcademicAreaDAL : ILookupDAL<AcademicArea>
    {
    }
}
