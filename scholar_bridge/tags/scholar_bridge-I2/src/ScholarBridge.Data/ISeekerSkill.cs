﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ISeekerSkillDAL : ILookupDAL<SeekerSkill>
    {
    }
}
