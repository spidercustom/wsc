﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ISeekerVerbalizingWordDAL : ILookupDAL<SeekerVerbalizingWord>
    {
    }
}
