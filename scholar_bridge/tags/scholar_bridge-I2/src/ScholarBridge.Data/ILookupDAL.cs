﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ILookupDAL 
    {
        IList<ILookup> FindAll();
        ILookup FindById(int id);
        List<KeyValuePair<string,string>> GetLookupItems();
    }
}
