﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface INeedGapDAL : IGenericLookupDAL<NeedGap>
    {
    }
}