﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ICountyDAL : ILookupDAL<County>
    {
    }
}
