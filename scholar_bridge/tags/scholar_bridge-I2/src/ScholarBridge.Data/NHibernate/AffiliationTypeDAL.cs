﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class AffiliationTypeDAL : LookupDAL<AffiliationType>, IAffiliationTypeDAL
    {
    }
}
