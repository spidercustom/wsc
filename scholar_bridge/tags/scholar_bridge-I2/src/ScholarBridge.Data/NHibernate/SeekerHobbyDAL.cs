﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class SeekerHobbyDAL : LookupDAL<SeekerHobby>, ISeekerHobbyDAL
    {
    }
}
