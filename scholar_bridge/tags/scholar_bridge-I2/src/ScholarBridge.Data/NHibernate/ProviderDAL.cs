using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class ProviderDAL : AbstractDAL<Provider>, IProviderDAL
    {
        public IList<Provider> FindAllPending()
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("ApprovalStatus", ApprovalStatus.PendingApproval))
                .List<Provider>();
        }

        public Provider FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public Provider FindByUser(User user)
        {
            return CreateCriteria()
                .CreateCriteria("Users")
                .Add(Restrictions.Eq("Id", user.Id)).UniqueResult<Provider>();
        }

        public User FindUserInOrg(Provider organization, int userId)
        {
            // XXX: Couldn't figure out how to do this with Criteria
            var query = Session.CreateQuery(@"select user from Provider as provider
inner join provider.Users as user
where provider.Id=:providerId AND user.id=:userId");

            query.SetInt32("providerId", organization.Id);
            query.SetInt32("userId", userId);
            return query.UniqueResult<User>();
        }
    }
}