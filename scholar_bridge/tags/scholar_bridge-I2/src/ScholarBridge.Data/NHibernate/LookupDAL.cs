﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class LookupDAL<T> : AbstractDAL<T>, ILookupDAL, IGenericLookupDAL<T> where T : ILookup
    {
        private static readonly SimpleExpression NOT_DEPRECATED = Restrictions.Eq("Deprecated", false);

        public T FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public IList<T> FindAll()
        {
            return CreateCriteria()
                .Add(NOT_DEPRECATED)
                .AddOrder(Order.Asc("Name"))
                .List<T>();
        }

        public IList<T> FindAll(IList<int> ids)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Id", ids.ToArray()))
                .List<T>();
        }

        IList<ILookup> ILookupDAL.FindAll()
        {
            var list = FindAll("Name");
            var result = new List<ILookup>(list.Cast<ILookup>());
            return result;
        }

        ILookup ILookupDAL.FindById(int id)
        {
            return FindById(id);
        }

        List<KeyValuePair<string, string>> ILookupDAL.GetLookupItems()
        {
            var result = new  List<KeyValuePair<string,string>>();
            var list = FindAll();
            foreach (var listItem in list)
            {
                 

                result.Add(new KeyValuePair<string, string>(listItem.Id.ToString(),listItem.Name));
            }

            return result;
        }


    }
}
