﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class ServiceTypeDAL : LookupDAL<ServiceType>, IServiceTypeDAL
    {
    }
}
