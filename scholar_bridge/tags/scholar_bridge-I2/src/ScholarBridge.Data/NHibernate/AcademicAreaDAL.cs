﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class AcademicAreaDAL : LookupDAL<AcademicArea>, IAcademicAreaDAL
    {
    }
}
