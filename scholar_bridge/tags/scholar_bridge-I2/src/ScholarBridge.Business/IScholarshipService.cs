﻿using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface IScholarshipService
    {
        Scholarship GetById(int id);
        IList<Scholarship> GetByProvider(Provider provider);
          
        Scholarship Save(Scholarship scholarship);
        bool ScholarshipNameExists(string name, Provider provider);
        Scholarship FindByNameAndProvider(string name, Provider provider);

        ScholarshipStage GetStageById(ScholarshipStages id);
        IList<ScholarshipStage> GetAllStages();
        IList<Scholarship> GetByStageAndProvider(ScholarshipStage scholarshipStage, Provider provider);
        IList<Scholarship> GetNotActivatedScholarshipsByProvider(Provider provider);
    }
}
