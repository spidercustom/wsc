using System.Net.Mail;

namespace ScholarBridge.Business
{
    public interface IMailerService
    {
        /// <summary>
        /// Sends an email.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        void SendMail(string from, string to, string subject, string body);

        /// <summary>
        /// Sends an email.
        /// </summary>
        /// <param name="message">The message to send</param>
        void SendMail(MailMessage message);

    }
}