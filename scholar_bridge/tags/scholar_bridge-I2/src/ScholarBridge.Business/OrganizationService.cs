using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public abstract class OrganizationService<T> : IOrganizationService<T> where T : Organization
    {
        public IOrganizationDAL<T> OrganizationDAL { get; set; }
        public IUserDAL UserDAL { get; set; }
        public IRoleDAL RoleDAL { get; set; }
        public ITemplatedMailerService MailerService { get; set; }

        protected abstract void AddRolesToNewUser(User user);
        protected abstract MailTemplate TemplateForApproval { get; }
        protected abstract MailTemplate TemplateForRejection { get; }

        public T Get(int id)
        {
            return OrganizationDAL.FindById(id);
        }

        public void SaveNewWithAdminUser(T organization, User user)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            AddRolesToNewUser(user);
            UserDAL.Update(user);
            organization.AdminUser = user;
            OrganizationDAL.Insert(organization);
        }

        public void SaveNewUser(T organization, User user, MailTemplateParams templateParams)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            AddRolesToNewUser(user);
            user.IsApproved = true;
            organization.Users.Add(user);
            OrganizationDAL.Update(organization);

            if (null != templateParams)
            {
                MailerService.SendMail(MailTemplate.ConfirmationLink, templateParams.MergeVariables, templateParams);
            }
        }

        public void SaveNewUser(T organization, User user)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            SaveNewUser(organization, user, null);
        }

        public void DeleteUser(T organization, User user)
        {
            UserDAL.Delete(user);
        }

        public void ReactivateUser(T organization, User user)
        {
            user.IsDeleted = false;
            UserDAL.Update(user);
        }

        public void Update(T organization)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            OrganizationDAL.Update(organization);
        }

        public IList<T> FindPendingOrganizations()
        {
            return OrganizationDAL.FindAllPending();
        }

        public void Approve(T organization, MailTemplateParams templateParams)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }
            organization.ApprovalStatus = ApprovalStatus.Approved;

            if (null != organization.AdminUser)
            {
                organization.AdminUser.IsApproved = true;
            }
            OrganizationDAL.Update(organization);

            // Send email to admin user that this organization has been approved
            MailerService.SendMail(TemplateForApproval, BuildMailReplacements(organization.AdminUser, templateParams), templateParams);
        }

        public void Reject(T organization, MailTemplateParams templateParams)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }
            organization.ApprovalStatus = ApprovalStatus.Rejected;
            OrganizationDAL.Update(organization);

            // Send email to admin user that this organization has been rejected
            MailerService.SendMail(TemplateForRejection, BuildMailReplacements(organization.AdminUser, templateParams), templateParams);
        }

        public User FindUserInOrg(T organization, int userId)
        {
            return OrganizationDAL.FindUserInOrg(organization, userId);
        }

        public Dictionary<string, string> BuildMailReplacements(User user, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()}
                           };

            return templateParams.MergeVariablesInto(dict);
        }
    }
}