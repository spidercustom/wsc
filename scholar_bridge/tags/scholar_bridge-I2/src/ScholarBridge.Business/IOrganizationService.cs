using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IOrganizationService<T> where T : Organization
    {
        T Get(int id);

        void SaveNewWithAdminUser(T organization, User user);

        void SaveNewUser(T organization, User user, MailTemplateParams templateParams);
        void SaveNewUser(T organization, User user);
        void DeleteUser(T organization, User user);
        void ReactivateUser(T organization, User user);

        void Update(T organization);

        IList<T> FindPendingOrganizations();

        void Approve(T organization, MailTemplateParams templateParams);
        void Reject(T organization, MailTemplateParams templateParams);

        User FindUserInOrg(T organization, int userId);
    }
}