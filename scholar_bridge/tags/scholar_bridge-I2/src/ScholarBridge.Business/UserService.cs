﻿using System;
using System.Linq;
using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public class UserService : IUserService 
    {
        public IUserDAL UserDAL { get; set; }
        public IProviderDAL ProviderDAL { get; set; }
        public IIntermediaryDAL IntermediaryDAL { get; set; }
        public ITemplatedMailerService MailerService { get; set; }

        public void ActivateUser(User user)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            if (!user.IsActive)
            {
                user.IsActive = true;
                UserDAL.Update(user);
            }
        }

        public void ActivateProviderUser(User user, MailTemplateParams templateParams)
        {
            ActivateUser(user);

            if (!user.IsApproved)
            {
                MailerService.SendMail(MailTemplate.ProviderApprovalEmailToHECBAdmin,
                                       BuildProviderActivationMailReplacements(user, templateParams), 
                                       templateParams);
            }
        }

        public void ActivateIntermediaryUser(User user, MailTemplateParams templateParams)
        {
            ActivateUser(user);

            if (!user.IsApproved)
            {
                MailerService.SendMail(MailTemplate.IntermediaryApprovalEmailToHECBAdmin,
                                       BuildIntermediaryActivationMailReplacements(user, templateParams),
                                       templateParams);
            }
        }

        public void ResetUsername(User user, string newEmail, MailTemplateParams templateParams)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            user.Email = newEmail;
            user.Username = newEmail;
            user.IsActive = false;
            UserDAL.Update(user);

            templateParams.MergeVariables.Add("Name", user.Name.NameFirstLast);
            MailerService.SendMail(MailTemplate.ConfirmationLink, templateParams.MergeVariables, templateParams);
        }

        public Dictionary<string, string> BuildProviderActivationMailReplacements(User user, MailTemplateParams templateParams)
        {
            Provider provider = ProviderDAL.FindByUser(user);
            return BuildActivationMailReplacements(user, provider, templateParams);
        }

        public Dictionary<string, string> BuildIntermediaryActivationMailReplacements(User user, MailTemplateParams templateParams)
        {
            Intermediary intermediary = IntermediaryDAL.FindByUser(user);
            return BuildActivationMailReplacements(user, intermediary, templateParams);
        }

        public Dictionary<string,string> BuildActivationMailReplacements(User user, Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"FirstName", user.Name.FirstName},
                               {"MiddleName", user.Name.MiddleName},
                               {"LastName", user.Name.LastName},
                               {"Email", user.Email},
                               {"OrganizationName", organization.Name},
                               {"OrganizationTax", organization.TaxId},
                               {"OrganizationWebsite", organization.Website},
                               {"AddressStreet", (organization.Address == null) ? "" : organization.Address.Street},
                               {"AddressStreet2", (organization.Address == null) ? "" : organization.Address.Street2},
                               {"City", (organization.Address == null) ? "" : organization.Address.City},
                               {"State", (organization.Address == null) ? "" : organization.Address.State.Name},
                               {"PostalCode", (organization.Address == null) ? "" : organization.Address.PostalCode},
                               {"Phone", (organization.Phone == null) ? "" : organization.Phone.Number},
                               {"Fax", (organization.Fax == null) ? "" : organization.Fax.Number},
                               {"OtherPhone", (organization.OtherPhone == null) ? "" : organization.OtherPhone.Number}
                           };

            return templateParams.MergeVariablesInto(dict);
        }
    }
}
