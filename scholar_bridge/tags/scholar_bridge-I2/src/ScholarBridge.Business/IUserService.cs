﻿using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IUserService
    {
        void ActivateUser(User user);

        void ActivateProviderUser(User user, MailTemplateParams templateParams);

        void ActivateIntermediaryUser(User user, MailTemplateParams templateParams);

        void ResetUsername(User user, string newEmail, MailTemplateParams templateParams);
    }
}
