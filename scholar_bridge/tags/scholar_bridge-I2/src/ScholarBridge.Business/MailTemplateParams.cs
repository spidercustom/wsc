using System.Collections.Generic;

namespace ScholarBridge.Business
{
    public class MailTemplateParams
    {
        public MailTemplateParams()
        {
            MergeVariables = new Dictionary<string, string>();
        }

        public string TemplateDirectory { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public Dictionary<string, string> MergeVariables { get; protected set; }

        public Dictionary<string, string> MergeVariablesInto(Dictionary<string,string> orig)
        {
            foreach (var key in MergeVariables.Keys)
            {
                orig[key] = MergeVariables[key];
            }
            return orig;
        }
    }
}