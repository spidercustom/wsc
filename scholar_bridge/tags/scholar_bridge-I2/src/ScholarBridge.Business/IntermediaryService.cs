using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public class IntermediaryService : OrganizationService<Intermediary>, IIntermediaryService
    {
        protected override void AddRolesToNewUser(User user)
        {
            user.Roles.Add(RoleDAL.FindByName(Role.INTERMEDIARY_ROLE));
        }

        protected override MailTemplate TemplateForApproval
        {
            get { return MailTemplate.IntermediaryApproval; }
        }

        protected override MailTemplate TemplateForRejection
        {
            get { return MailTemplate.IntermediaryRejection; }
        }
    }
}