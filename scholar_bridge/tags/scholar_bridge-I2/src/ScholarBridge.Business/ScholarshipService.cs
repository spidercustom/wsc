﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public class ScholarshipService : IScholarshipService
    {
        public IScholarshipDAL ScholarshipDAL { get; set; }
        public IScholarshipStageDAL ScholarshipStageDAL { get; set; }

        #region IScholarshipService Members

        public Scholarship GetById(int id)
        {
            return ScholarshipDAL.FindById(id);
        }

        public IList<Scholarship> GetByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Exceptions.ProviderNotApprovedException();
            return ScholarshipDAL.FindByProvider(provider);
        }

        public Scholarship Save(Scholarship scholarship)
        {
            if (scholarship == null)
                throw new ArgumentNullException("scholarship");
            if (scholarship.Provider == null)
                throw new ArgumentNullException("scholarship.Provider");
            if (scholarship.Provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Exceptions.ProviderNotApprovedException();

            return ScholarshipDAL.Save(scholarship);
        }

        public Scholarship FindByNameAndProvider(string name, Provider provider)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");
            if (provider == null)
                throw new ArgumentNullException("provider");

            return ScholarshipDAL.FindByNameAndProvider(name, provider);
        }

        public bool ScholarshipNameExists(string name, Provider provider)
        {
            return null != FindByNameAndProvider(name, provider);
        }

        public ScholarshipStage GetStageById(ScholarshipStages id)
        {
            return ScholarshipStageDAL.FindById(id);
        }

        public IList<ScholarshipStage> GetAllStages()
        {
            return ScholarshipStageDAL.GetAll();
        }
        public IList<Scholarship> GetByStageAndProvider(ScholarshipStage scholarshipStage, Provider provider)
        {
            if (scholarshipStage == null)
                throw new ArgumentNullException("scholarshipStage");

            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Exceptions.ProviderNotApprovedException();
            
            
            
            return ScholarshipDAL.FindByStageAndProvider(scholarshipStage, provider);
        }
        public IList<Scholarship> GetNotActivatedScholarshipsByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Exceptions.ProviderNotApprovedException();

            var scholarships  = ScholarshipDAL.FindByProvider(provider);


            return scholarships.Where(s => (s.Stage.Id  < ScholarshipStages.Activated) ).ToList().AsReadOnly();
        }
        #endregion
    }
}
