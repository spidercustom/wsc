using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public class ProviderService : OrganizationService<Provider>, IProviderService
    {
        protected override void AddRolesToNewUser(User user)
        {
            user.Roles.Add(RoleDAL.FindByName(Role.PROVIDER_ROLE));
        }

        protected override MailTemplate TemplateForApproval
        {
            get { return MailTemplate.ProviderApproval; }
        }

        protected override MailTemplate TemplateForRejection
        {
            get { return MailTemplate.ProviderRejection; }
        }
    }
}