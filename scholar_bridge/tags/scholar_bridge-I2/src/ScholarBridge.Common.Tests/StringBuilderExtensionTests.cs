using System.Text;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using Spider.Common.Extensions.System.Text;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class StringBuilderExtensionTests
    {
        [Test]
        public void ends_with_finds_character()
        {
            var sb = new StringBuilder("foo;");
            Assert.That(sb.EndsWith(';'), Is.True);
            Assert.That(sb.EndsWith(' '), Is.False);
            Assert.That(sb.EndsWith('f'), Is.False);
        }
    }
}