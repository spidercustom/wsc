﻿using System;
using NUnit.Framework;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class ByWeekDayTests
    {
        [Test]
        public void can_do_to_string()
        {
            var byWeekDay = new ByWeekDay();
            byWeekDay.Month = MonthOfYear.January;
            byWeekDay.WeekDayOccurrence = WeekDayOccurrencesInMonth.First;
            byWeekDay.DayOfWeek = DayOfWeek.Monday;

            byWeekDay.ToString();
        }
    }
}