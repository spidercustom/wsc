﻿using NUnit.Framework;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class EnumExtensionsTests
    {
        public enum test_enum_1
        {
            element1, element2
        }

        public enum test_enum_2
        {
            element1, 
            [DisplayName("Element2")]
            element2
        }

        public enum test_enum_3
        {
            [DisplayName("Element1")]
            element1,
            [DisplayName("Element2")]
            element2
        }

        [Test] 
        public void get_dispaynames_for_enum_not_having_display_name_attribute()
        {
            var displayNames = EnumExtensions.GetDisplayNames(typeof (test_enum_1));
            Assert.AreEqual(2, displayNames.Length);
            Assert.AreEqual(test_enum_1.element1.ToString(), displayNames[0]);
            Assert.AreEqual(test_enum_1.element2.ToString(), displayNames[1]);
        }

        [Test] 
        public void get_dispaynames_for_enum_having_some_display_name_attribute()
        {
            var displayNames = EnumExtensions.GetDisplayNames(typeof(test_enum_2));
            Assert.AreEqual(2, displayNames.Length);
            Assert.AreEqual(test_enum_2.element1.ToString(), displayNames[0]);
            Assert.AreEqual("Element2", displayNames[1]);
        }


        [Test]
        public void get_dispaynames_for_enum_having_display_name_attribute()
        {
            var displayNames = EnumExtensions.GetDisplayNames(typeof(test_enum_3));
            Assert.AreEqual(2, displayNames.Length);
            Assert.AreEqual("Element1", displayNames[0]);
            Assert.AreEqual("Element2", displayNames[1]);
        }

        [Test]
        public void get_display_name_from_enum_object_without_display_name_attribute()
        {
            test_enum_1 enumObject = test_enum_1.element1;
            Assert.AreEqual("element1", enumObject.GetDisplayName());
        }

        [Test]
        public void get_display_name_from_enum_object_with_display_name_attribute()
        {
            test_enum_2 enumObject = test_enum_2.element2;
            Assert.AreEqual("Element2", enumObject.GetDisplayName());
        }

        public enum IsInTestEnum
        {
            Element1,
            Element2,
            Element3
        }

        [Test]
        public void test_is_in_return_true()
        {
            var find = IsInTestEnum.Element3;
            Assert.IsTrue(find.IsIn(IsInTestEnum.Element1, IsInTestEnum.Element3));
        }
    
        [Test]
        public void test_is_in_return_false()
        {
            var find = IsInTestEnum.Element3;
            Assert.IsFalse(find.IsIn(IsInTestEnum.Element1, IsInTestEnum.Element2));
        }
    }
}
