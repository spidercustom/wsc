﻿<%@ Page Title="Confirm Email" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ConfirmEmail.aspx.cs" Inherits="ScholarBridge.Web.ConfirmEmail" %>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
    
    <asp:Panel ID="setPassword" runat="server" Visible="false">
        <label for="Password">Password:</label>
        <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
        <elv:PropertyProxyValidator ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
        <br />
        <label for="ConfirmPassword">Confirm Password:</label>
        <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
            ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."
            ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
        <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
            ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
            ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
        <br />
        
        <asp:Button ID="saveButton" runat="server" Text="Set Password" onclick="saveButton_Click" />
    
    </asp:Panel>
    
</asp:Content>
