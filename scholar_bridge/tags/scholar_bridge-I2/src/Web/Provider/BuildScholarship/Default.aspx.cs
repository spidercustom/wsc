﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Default : Spring.Web.UI.Page, IWizardStepsContainer<Scholarship>
    {
        public const string SCHOLARSHIP_ID = "sid";
        public const string RESUME_FROM = "resumefrom";
        public const string COPY_FROM = "copyfrom";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            if (provider.ApprovalStatus != Domain.ApprovalStatus.Approved)
                throw new Business.Exceptions.ProviderNotApprovedException();

            foreach (var step in Steps)
            {
                step.Container = this;
            }

            if (!IsPostBack)
            {
                bool copyFromExists = Array.Exists(Request.Params.AllKeys, key => COPY_FROM.Equals(key));
                bool resumeFromExists = Array.Exists(Request.Params.AllKeys, key => RESUME_FROM.Equals(key));
                var scholarshipToEdit = GetCurrentScholarship();
                
                if (copyFromExists)
                {
                    BeginCopyFrom();
                }
                else if (scholarshipToEdit != null)
                {
                    if (!scholarshipToEdit.Provider.Id.Equals(UserContext.CurrentProvider.Id))
                        throw new InvalidOperationException("Scholarship do not belong to provider in context");
                    
                    if (resumeFromExists)
                    {
                        ResumeFromStep();
                    }
                    else
                    {
                        ResumeWizard();
                    }
                }
            }
        }

        protected void SaveAndExitButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ValidateStep())
            {
                Save();
                Response.Redirect("../");
            }
        }

        public Scholarship GetCurrentScholarship()
        {
            string scholarshipIdString = CurrentScholarshipIdString;

            if (string.IsNullOrEmpty(scholarshipIdString))
                return null;

            int scholarshipId;
            if (!Int32.TryParse(scholarshipIdString, out scholarshipId))
                throw new ArgumentException("Cannot understand value of parameter scholarship");
            
            Scholarship result = ScholarshipService.GetById(scholarshipId);
            return result;
        }

        private string CurrentScholarshipIdString
        {
            get
            {
                string idString = Request.Params[SCHOLARSHIP_ID];//retrive from parameters

                if (string.IsNullOrEmpty(idString)) //if not in parameters
                {
                    idString = 
                        ViewState[SCHOLARSHIP_ID] == null ? 
                        null : 
                        ViewState[SCHOLARSHIP_ID].ToString(); //retrive from view state
                }
                return idString;
            }
            set
            {
                ViewState[SCHOLARSHIP_ID] = value;
            }
        }

        protected void BuildScholarshipWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Page.IsValid && ValidateStep())
            {
                Save();
            }
            else if (e.NextStepIndex > e.CurrentStepIndex)
            {
                e.Cancel = true;
            }
        }

        private bool ValidateStep()
        {
            if (BuildScholarshipWizard.ActiveStep != null)
            {
                var wizardStepControl = WizardStepControlQueries.FindFirstWizardStepControl<Scholarship>(BuildScholarshipWizard.ActiveStep.Controls);
                if (wizardStepControl != null)
                {
                    return wizardStepControl.ValidateStep();
                }
            }

            return true;
        }

        private void Save()
        {
            if (Page.IsValid && ValidateStep())
            {
                if (BuildScholarshipWizard.ActiveStep != null)
                {
                    var wizardStepControl = WizardStepControlQueries.FindFirstWizardStepControl<Scholarship>(BuildScholarshipWizard.ActiveStep.Controls);
                    if (wizardStepControl != null)
                    {
                        wizardStepControl.Save();
                        if (wizardStepControl.ChangeNextStepIndex())
                            BuildScholarshipWizard.ActiveStepIndex = wizardStepControl.GetChangedNextStepIndex();
                    }
                }
                CurrentScholarshipIdString = GetDomainObject().Id.ToString();
            }
        }

        private void BeginCopyFrom()
        {
            string copyFromIdString = Request.Params[COPY_FROM];
            int copyFromId;
            if (!Int32.TryParse(copyFromIdString, out copyFromId))
                throw new ArgumentException("Cannot understand value of parameter scholarship");
            Scholarship copyFrom = ScholarshipService.GetById(copyFromId);
            scholarshipInContext = (Scholarship) ((ICloneable)copyFrom).Clone();
            BuildScholarshipWizard.ActiveStepIndex = 1;
        }

        #region IWizardStepsContainer<Scholarship> Members
        //TODO: Can be moved to WizardStepsContainerPageBase
        public void ResumeWizard()
        {
            for (var stepIndex = 0; stepIndex < Steps.Length; stepIndex++)
            {
                var stepControl = Steps[stepIndex];
                if (stepControl.WasSuspendedFrom(GetDomainObject()))
                {
                    BuildScholarshipWizard.ActiveStepIndex = stepControl.StepIndex;
                    break;
                }
            }
        }
        //TODO: Can be moved to WizardStepsContainerPageBase
        private void ResumeFromStep()
        {
            var resumeFromPageIndexString = Request.Params[RESUME_FROM];
            var resumeFromPageIndex = Int32.Parse(resumeFromPageIndexString);
            
            var scholarship = GetCurrentScholarship();
            var resumeFromIndex = Math.Min(resumeFromPageIndex, Steps.Length-1);
            for (; resumeFromIndex >= 0; resumeFromIndex--)
            {
                var stepControl = Steps[resumeFromIndex];
                if (stepControl.CanResume(scholarship))
                {
                    BuildScholarshipWizard.ActiveStepIndex = stepControl.StepIndex;
                    return;
                }
            }
            BuildScholarshipWizard.ActiveStepIndex = 0;
        }

        //TODO: Can be moved to WizardStepsContainerPageBase
        public IWizardStepControl<Scholarship>[] Steps
        {
            get
            {
                IWizardStepControl<Scholarship>[] stepControls = WizardStepControlQueries.FindWizardStepControls<Scholarship>(BuildScholarshipWizard.WizardSteps);
                return stepControls;
            }
        }
        //TODO: Can be moved to WizardStepsContainerPageBase
        Scholarship scholarshipInContext;
        public Scholarship GetDomainObject()
        {
            if (scholarshipInContext == null)
            {
                scholarshipInContext = GetCurrentScholarship();
                if (scholarshipInContext == null)
                {
                    scholarshipInContext = new Scholarship
                                               {
                                                   Provider = UserContext.CurrentProvider
                                               };
                }
            }
            return scholarshipInContext;
        }

        #endregion
    }
}
