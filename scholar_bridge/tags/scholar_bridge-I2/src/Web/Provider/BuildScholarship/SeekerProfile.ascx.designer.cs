﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScholarBridge.Web.Provider.BuildScholarship {
    
    
    public partial class SeekerProfile {
        
        /// <summary>
        /// scholarshipNameLbl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label scholarshipNameLbl;
        
        /// <summary>
        /// FiveWordsApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl FiveWordsApplicabilityControl;
        
        /// <summary>
        /// FiveWordsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FiveWordsControl;
        
        /// <summary>
        /// FiveWordsControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog FiveWordsControlDialogButton;
        
        /// <summary>
        /// FiveSkillsApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl FiveSkillsApplicabilityControl;
        
        /// <summary>
        /// FiveSkillsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FiveSkillsControl;
        
        /// <summary>
        /// FiveSkillsControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog FiveSkillsControlDialogButton;
        
        /// <summary>
        /// StudentGroupControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList StudentGroupControl;
        
        /// <summary>
        /// SchoolTypeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList SchoolTypeControl;
        
        /// <summary>
        /// AcademicProgramControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList AcademicProgramControl;
        
        /// <summary>
        /// SeekerStatusControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList SeekerStatusControl;
        
        /// <summary>
        /// ProgramLengthControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList ProgramLengthControl;
        
        /// <summary>
        /// CollegesApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl CollegesApplicabilityControl;
        
        /// <summary>
        /// CollegesControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox CollegesControl;
        
        /// <summary>
        /// CollegesControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog CollegesControlDialogButton;
        
        /// <summary>
        /// FirstGenerationControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox FirstGenerationControl;
        
        /// <summary>
        /// EthinicityApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl EthinicityApplicabilityControl;
        
        /// <summary>
        /// EthnicityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.LookupItemCheckboxList EthnicityControl;
        
        /// <summary>
        /// ReligionApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl ReligionApplicabilityControl;
        
        /// <summary>
        /// ReligionControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.LookupItemCheckboxList ReligionControl;
        
        /// <summary>
        /// GendersApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl GendersApplicabilityControl;
        
        /// <summary>
        /// GendersControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList GendersControl;
        
        /// <summary>
        /// StatesApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl StatesApplicabilityControl;
        
        /// <summary>
        /// StateControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox StateControl;
        
        /// <summary>
        /// StateControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog StateControlDialogButton;
        
        /// <summary>
        /// CountyApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl CountyApplicabilityControl;
        
        /// <summary>
        /// CountyControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox CountyControl;
        
        /// <summary>
        /// CountyControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog CountyControlDialogButton;
        
        /// <summary>
        /// CityApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl CityApplicabilityControl;
        
        /// <summary>
        /// CityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox CityControl;
        
        /// <summary>
        /// CityControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog CityControlDialogButton;
        
        /// <summary>
        /// SchoolDistrictApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl SchoolDistrictApplicabilityControl;
        
        /// <summary>
        /// SchoolDistrictControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SchoolDistrictControl;
        
        /// <summary>
        /// SchoolDistrictControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog SchoolDistrictControlDialogButton;
        
        /// <summary>
        /// HighSchoolApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl HighSchoolApplicabilityControl;
        
        /// <summary>
        /// HighSchoolControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox HighSchoolControl;
        
        /// <summary>
        /// HighSchoolControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog HighSchoolControlDialogButton;
        
        /// <summary>
        /// AcademicAreasApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl AcademicAreasApplicabilityControl;
        
        /// <summary>
        /// AcademicAreasControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox AcademicAreasControl;
        
        /// <summary>
        /// AcademicAreasControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog AcademicAreasControlDialogButton;
        
        /// <summary>
        /// CareersApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl CareersApplicabilityControl;
        
        /// <summary>
        /// CareersControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox CareersControl;
        
        /// <summary>
        /// CareersControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog CareersControlDialogButton;
        
        /// <summary>
        /// CommunitiyInvolvementCausesApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl CommunitiyInvolvementCausesApplicabilityControl;
        
        /// <summary>
        /// TextBox3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox3;
        
        /// <summary>
        /// CommunityInvolvementCausesControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog CommunityInvolvementCausesControlDialogButton;
        
        /// <summary>
        /// OrganizationsApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl OrganizationsApplicabilityControl;
        
        /// <summary>
        /// OrganizationsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox OrganizationsControl;
        
        /// <summary>
        /// OrganizationsControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog OrganizationsControlDialogButton;
        
        /// <summary>
        /// AffiliationTypesApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl AffiliationTypesApplicabilityControl;
        
        /// <summary>
        /// AffiliationTypesControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox AffiliationTypesControl;
        
        /// <summary>
        /// AffiliationTypesControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog AffiliationTypesControlDialogButton;
        
        /// <summary>
        /// HobbiesApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl HobbiesApplicabilityControl;
        
        /// <summary>
        /// SeekerHobbiesControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SeekerHobbiesControl;
        
        /// <summary>
        /// SeekerHobbiesControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog SeekerHobbiesControlDialogButton;
        
        /// <summary>
        /// SportsApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl SportsApplicabilityControl;
        
        /// <summary>
        /// SportsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SportsControl;
        
        /// <summary>
        /// SportsControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog SportsControlDialogButton;
        
        /// <summary>
        /// ClubsApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl ClubsApplicabilityControl;
        
        /// <summary>
        /// ClubsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ClubsControl;
        
        /// <summary>
        /// ClubsControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog ClubsControlDialogButton;
        
        /// <summary>
        /// WorkTypeApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl WorkTypeApplicabilityControl;
        
        /// <summary>
        /// WorkTypeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox WorkTypeControl;
        
        /// <summary>
        /// WorkTypeControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog WorkTypeControlDialogButton;
        
        /// <summary>
        /// WorkHoursApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl WorkHoursApplicabilityControl;
        
        /// <summary>
        /// WorkHoursControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.LookupItemCheckboxList WorkHoursControl;
        
        /// <summary>
        /// ServiceTypeApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl ServiceTypeApplicabilityControl;
        
        /// <summary>
        /// ServiceTypeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ServiceTypeControl;
        
        /// <summary>
        /// ServiceTypeControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog ServiceTypeControlDialogButton;
        
        /// <summary>
        /// ServiceHoursApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl ServiceHoursApplicabilityControl;
        
        /// <summary>
        /// ServiceHoursControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.LookupItemCheckboxList ServiceHoursControl;
        
        /// <summary>
        /// GAPControlApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl GAPControlApplicabilityControl;
        
        /// <summary>
        /// GPAControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl GPAControl;
        
        /// <summary>
        /// ClassRankApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl ClassRankApplicabilityControl;
        
        /// <summary>
        /// ClassRankControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl ClassRankControl;
        
        /// <summary>
        /// SATApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl SATApplicabilityControl;
        
        /// <summary>
        /// SATWritingControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl SATWritingControl;
        
        /// <summary>
        /// SATCriticalReadingControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl SATCriticalReadingControl;
        
        /// <summary>
        /// SATMathematicsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl SATMathematicsControl;
        
        /// <summary>
        /// ACTApplicabilityControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ApplicabilityControl ACTApplicabilityControl;
        
        /// <summary>
        /// ACTEnglishControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl ACTEnglishControl;
        
        /// <summary>
        /// ACTMathematicsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl ACTMathematicsControl;
        
        /// <summary>
        /// ACTReadingControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl ACTReadingControl;
        
        /// <summary>
        /// ScienceControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.NumberRangeConditionControl ScienceControl;
        
        /// <summary>
        /// HonorsControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox HonorsControl;
        
        /// <summary>
        /// APCreditControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox APCreditControl;
    }
}
