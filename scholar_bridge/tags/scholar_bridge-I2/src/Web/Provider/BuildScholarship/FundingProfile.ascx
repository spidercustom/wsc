﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.FundingProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<h3>Build Scholarship – Funding Profile</h3>
<h4><asp:Label ID="scholarshipNameLbl" runat="server" /></h4>

<h5>Need</h5>
<asp:CheckBox ID="Fafsa" runat="server" Text="FAFSA Definition of Need" TextAlign="Left"/>
<asp:CheckBox ID="UserDerived" runat="server" Text="User Derived Definition of Need" TextAlign="Left"/>
<br />
<label for="MinimumSeekerNeed">Minimum Need:</label>
<asp:TextBox ID="MinimumSeekerNeed" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="MinimumSeekerNeedValidator" runat="server" ControlToValidate="MinimumSeekerNeed" PropertyName="MinimumSeekerNeed" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
<br />
<label for="MaximumSeekerNeed">Maximum Need:</label>
<asp:TextBox ID="MaximumSeekerNeed" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="MaximumSeekerNeedValidator" runat="server" ControlToValidate="MaximumSeekerNeed" PropertyName="MaximumSeekerNeed" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
<br />
<label for="NeedGaps">Need Gap Threshold:</label>
<asp:CheckBoxList ID="NeedGaps" runat="server" TextAlign="Left"/>
<br />

<h5>Situations the Scholarship will Fund</h5>
<asp:CheckBox ID="Emergency" runat="server" Text="Emergency funding" />
<asp:CheckBox ID="Traditional" runat="server" Text="Traditional support" />
<br />
<label for="TypesOfSupport">Types of Support:</label>
<asp:CheckBoxList ID="TypesOfSupport" runat="server" TextAlign="Left"/>
<br />

<h5>Additional Selection Processes Needed</h5>
<label for="AdditionalRequirements">Types of Support:</label>
<asp:CheckBoxList ID="AdditionalRequirements" runat="server" TextAlign="Left"/>
<br />

<h5>Funding Parameters</h5>
<label for="AnnualSupportAmount">Annual Amount of Support:</label>
<asp:TextBox ID="AnnualSupportAmount" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="AnnualSupportAmountValidator" runat="server" ControlToValidate="AnnualSupportAmount" PropertyName="AnnualSupportAmount" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters"/>
<br />
<label for="MinimumNumberOfAwards">Minimum Number of Awards:</label>
<asp:TextBox ID="MinimumNumberOfAwards" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="MinimumNumberOfAwardsValidator" runat="server" ControlToValidate="MinimumNumberOfAwards" PropertyName="MinimumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters"/>
<br />
<label for="MaximumNumberOfAwards">Maximum Number of Awards:</label>
<asp:TextBox ID="MaximumNumberOfAwards" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="MaximumNumberOfAwardsValidator" runat="server" ControlToValidate="MaximumNumberOfAwards" PropertyName="MaximumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters"/>
<br />
<label for="TermsOfSupport">Terms of Support:</label>
<asp:CheckBoxList ID="TermsOfSupport" runat="server" TextAlign="Left"/>
<br />