﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Default" %>
<%@ Register src="GeneralInfo.ascx" tagname="GeneralInfo" tagprefix="sb" %>
<%@ Register src="SeekerProfile.ascx" tagname="SeekerProfile" tagprefix="sb" %>
<%@ Register src="FundingProfile.ascx" tagname="FundingProfile" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/js/jquery.flexigrid.js" type="text/javascript"></script>
    <script src="/js/lookupdialog.js" type="text/javascript"></script>
    
    <link href="/styles/flexigrid.css" rel="stylesheet" type="text/css" />
    <link href="/styles/lookupdialog.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
  Welcome <asp:Label ID="userNameLbl" runat="server" /> for <asp:Label ID="providerNameLbl" runat="server" />

  <asp:Wizard ID="BuildScholarshipWizard" runat="server" ActiveStepIndex="0" 
    onnextbuttonclick="BuildScholarshipWizard_NextButtonClick">
    <WizardSteps>
    
      <asp:WizardStep ID="IntroductionStep" runat="server" Title="Introductory Information">
        <h3>Build Scholarship – Introductory Information</h3>
        <p>
          You are about to start defining a scholarship.
        </p>
        <p>
          This step by step entry screens will guild you to creating scholarship, defining match
          for the scholarship and activating scholarship. At any point of this steps you may
          choose to save and exit from the process. And resume later from whereever you had left it.
          In some screens you will see some attributes marked as red asterisks. These attributes
          are required to be filled in before moving ahead.
        </p>
      </asp:WizardStep>

      <asp:WizardStep ID="GeneralInfoStep" runat="server" Title="General Information">
        <sb:GeneralInfo ID="generalInfo" runat="server" />
      </asp:WizardStep>
      
      <asp:WizardStep ID="SeekerProfileStep" runat="server" Title="Seeker Profile">
        <sb:SeekerProfile ID="seekerProfile" runat="server" />
      </asp:WizardStep>
      
      <asp:WizardStep ID="FundingProfileStep" runat="server" Title="Funding Profile">
        <sb:FundingProfile ID="fundingProfile" runat="server" />
      </asp:WizardStep>
      
      <asp:WizardStep ID="PreviewCandidatePopulationStep" runat="server" Title="Preview Candidate Population">
      </asp:WizardStep>
      
      <asp:WizardStep ID="BuildScholarshipMatchLogicStep" runat="server" Title="Build Scholarship Match Logic">
      </asp:WizardStep>
      
      <asp:WizardStep ID="BuildScholarshipRenewalCriteriaStep" runat="server" Title="Build a Scholarship Renewal Criteria">
      </asp:WizardStep>
      
      <asp:WizardStep ID="ActivateScholarshipStep" runat="server" Title="Activate Scholarship">
      </asp:WizardStep>
      
    </WizardSteps>
    
    <StepNavigationTemplate>
      <asp:Button ID="StepPreviousButton" runat="server" CausesValidation="False" 
        CommandName="MovePrevious" Text="Previous" />
      <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" CausesValidation="True"
        Text="Next" />
      <asp:Button ID="SaveAndExitButton" runat="server" CausesValidation="True"
        CommandName="SaveAndExit" Text="Save and Exit"  OnClick="SaveAndExitButton_Click"/>
    </StepNavigationTemplate>
    
    <FinishNavigationTemplate>
      <asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" 
        CommandName="MovePrevious" Text="Previous" />
      <asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" 
        Text="Finish" />
    </FinishNavigationTemplate>
  </asp:Wizard>
</asp:Content>
