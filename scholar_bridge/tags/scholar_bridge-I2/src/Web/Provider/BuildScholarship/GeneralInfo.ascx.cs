﻿using System;
using System.IO;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;
using System.Globalization;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class GeneralInfo : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IStateDAL StateService { get; set; }
        Scholarship scholarshipInContext;

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();

            scholarshipInContext = Container.GetDomainObject();
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!Page.IsPostBack)
            {
                AddressState.DataSource = StateService.FindAll();
                AddressState.DataTextField = "Name";
                AddressState.DataValueField = "Abbreviation";
                AddressState.DataBind();
                AddressState.Items.Insert(0, new ListItem("- Select One -", ""));
            }


            if (!IsPostBack)
                PopulateScreen();


        }

        private void PopulateObjects()
        {
            scholarshipInContext.Name = ScholarshipNameControl.Text;
            scholarshipInContext.MissionStatement = MissionStatementControl.Text;
            PopulateScheduleObject();
            PopulateAmountObject();
            PopulateDonorObject();

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage == null || scholarshipInContext.Stage.Id < ScholarshipStages.GeneralInformation)
                scholarshipInContext.Stage = ScholarshipService.GetStageById(ScholarshipStages.GeneralInformation);
        }

        private void PopulateAmountObject()
        {
            decimal amounts;
            if (!string.IsNullOrEmpty(MaximumAmount.Text))
            {
                if (Decimal.TryParse(MaximumAmount.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    scholarshipInContext.MaximumAmount = amounts;
                }
                else
                {
                    MaximumAmountValidator.IsValid = false;
                    MaximumAmountValidator.Text = "Invalid currency value";

                }
            }

            if (!string.IsNullOrEmpty(MaximumAmount.Text))
            {
                if (Decimal.TryParse(MinimumAmount.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    scholarshipInContext.MinimumAmount = amounts;
                }
                else
                {
                    MinimumAmountValidator.IsValid = false;
                    MinimumAmountValidator.Text = "Invalid currency value";
                }
            }
        }

        private void PopulateDonorObject()
        {
            scholarshipInContext.Donor.Name.FirstName = FirstName.Text;
            scholarshipInContext.Donor.Name.MiddleName = MiddleName.Text;
            scholarshipInContext.Donor.Name.LastName = LastName.Text;

            scholarshipInContext.Donor.Address.Street = AddressStreet.Text;
            scholarshipInContext.Donor.Address.Street2 = AddressStreet2.Text;
            scholarshipInContext.Donor.Address.City = AddressCity.Text;
            scholarshipInContext.Donor.Address.PostalCode = AddressPostalCode.Text;
            var state = StateService.FindByAbbreviation(AddressState.SelectedValue);
            scholarshipInContext.Donor.Address.State = state;

            scholarshipInContext.Donor.Phone = new PhoneNumber(Phone.Text);
            scholarshipInContext.Donor.Email = EmailAddress.Text;
        }



        private void PopulateScheduleObject()
        {
            //Once we findout better way to put start, due and award to base class
            // this will become simpler
            if (Schedule.ActiveViewIndex == BY_WEEK_PAGE_INDEX)
            {
                ScholarshipScheduleByWeekDay schedule;
                if (null == scholarshipInContext.Schedule ||
                    !(scholarshipInContext.Schedule is ScholarshipScheduleByWeekDay))
                    schedule = new ScholarshipScheduleByWeekDay();
                else
                    schedule = (ScholarshipScheduleByWeekDay) scholarshipInContext.Schedule;
                StartFromByWeekControl.WeekDay.CopyValuesTo(schedule.StartFrom);
                DueOnByWeekControl.WeekDay.CopyValuesTo(schedule.DueOn);
                AwardOnByWeekControl.WeekDay.CopyValuesTo(schedule.AwardOn);
                scholarshipInContext.Schedule = schedule;
            }
            else if (Schedule.ActiveViewIndex == BY_MONTH_PAGE_INDEX)
            {
                ScholarshipScheduleByMonthDay schedule;
                if (null == scholarshipInContext.Schedule ||
                    !(scholarshipInContext.Schedule is ScholarshipScheduleByMonthDay))
                    schedule = new ScholarshipScheduleByMonthDay();
                else
                    schedule = (ScholarshipScheduleByMonthDay)scholarshipInContext.Schedule;
                StartFromByMonthControl.WeekMonth.CopyValuesTo(schedule.StartFrom);
                DueOnByMonthControl.WeekMonth.CopyValuesTo(schedule.DueOn);
                AwardOnByMonthControl.WeekMonth.CopyValuesTo(schedule.AwardOn);
                scholarshipInContext.Schedule = schedule;
            }
        }


        private const string AMOUNT_FORMAT = "###,###,##0";
        private void PopulateScreen()
        {
            ScholarshipNameControl.Text = scholarshipInContext.Name;
            MissionStatementControl.Text = scholarshipInContext.MissionStatement;
            PopulateScheduleScreen();
            MaximumAmount.Text = scholarshipInContext.MaximumAmount.ToString(AMOUNT_FORMAT);
            MinimumAmount.Text = scholarshipInContext.MinimumAmount.ToString(AMOUNT_FORMAT);
            PopulateDonorScreen();
        }

        private void PopulateDonorScreen()
        {
            FirstName.Text = scholarshipInContext.Donor.Name.FirstName;
            MiddleName.Text = scholarshipInContext.Donor.Name.MiddleName;
            LastName.Text = scholarshipInContext.Donor.Name.LastName;

            AddressStreet.Text = scholarshipInContext.Donor.Address.Street;
            AddressStreet2.Text = scholarshipInContext.Donor.Address.Street2;
            AddressCity.Text = scholarshipInContext.Donor.Address.City;
            AddressPostalCode.Text = scholarshipInContext.Donor.Address.PostalCode;
            if (null != scholarshipInContext.Donor.Address.State)
                AddressState.SelectedValue = scholarshipInContext.Donor.Address.State.Abbreviation;
            if (null != scholarshipInContext.Donor.Phone)
                Phone.Text = scholarshipInContext.Donor.Phone.Number;
            EmailAddress.Text = scholarshipInContext.Donor.Email;
        }

        private void PopulateScheduleScreen()
        {
            if (scholarshipInContext.Schedule is ScholarshipScheduleByWeekDay)
            {
                var schedule = scholarshipInContext.Schedule as ScholarshipScheduleByWeekDay;
                if (schedule == null)
                    throw new InvalidDataException("Cannot cast schedule unexpectedly");
                Schedule.ActiveViewIndex = BY_WEEK_PAGE_INDEX;
                StartFromByWeekControl.WeekDay = schedule.StartFrom;
                DueOnByWeekControl.WeekDay = schedule.DueOn;
                AwardOnByWeekControl.WeekDay = schedule.AwardOn;
            }
            else if (scholarshipInContext.Schedule is ScholarshipScheduleByMonthDay)
            {
                var schedule = scholarshipInContext.Schedule as ScholarshipScheduleByMonthDay;
                if (schedule == null)
                    throw new InvalidDataException("Cannot cast schedule unexpectedly");
                Schedule.ActiveViewIndex = BY_MONTH_PAGE_INDEX;
                StartFromByMonthControl.WeekMonth = schedule.StartFrom;
                DueOnByMonthControl.WeekMonth = schedule.DueOn;
                AwardOnByMonthControl.WeekMonth = schedule.AwardOn;
            }
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage.Id == ScholarshipStages.GeneralInformation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage.Id >= ScholarshipStages.GeneralInformation;
        }

        public override int StepIndex
        {
            get { return 1; }
        }

        public override bool ValidateStep()
        {
            bool result = true;

            var foundScholarship = ScholarshipService.FindByNameAndProvider(ScholarshipNameControl.Text, scholarshipInContext.Provider);
            if (null != foundScholarship && foundScholarship.Id != scholarshipInContext.Id)
            {
                result &= false;
                ScholarshipNameValidator.IsValid = false;
                ScholarshipNameValidator.Text = "Scholarship with this name already exists";
            }
            return result;
        }

        private const int BY_WEEK_PAGE_INDEX = 0;
        private const int BY_MONTH_PAGE_INDEX = 1;

        protected void ScheduleByWeekButton_Click(object sender, EventArgs e)
        {
            Schedule.ActiveViewIndex = BY_WEEK_PAGE_INDEX;
        }

        protected void ScheduleByMonthButton_Click(object sender, EventArgs e)
        {
            Schedule.ActiveViewIndex = BY_MONTH_PAGE_INDEX;
        }

    }
}