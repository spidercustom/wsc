﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.SeekerProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>
<%@ Register src="~/Common/NumberRangeConditionControl.ascx" tagname="NumberRangeCondition" tagprefix="sb" %>
<%@ Register Assembly="Web"
             Namespace="ScholarBridge.Web.Common"
             TagPrefix="sbCommon" %>

<h3>Build Scholarship – Seeker Profile</h3>
<h4><asp:Label ID="scholarshipNameLbl" runat="server" /></h4>

<h5>Seeker's Personality</h5>
<p class="additionInfoText">
  Translate the scholarship Mission into specific criteria that describe the Seeker and can be used to perform a "strict" match.
</p>

<label for="FiveWordsControl">5 Words:</label>
<sbCommon:ApplicabilityControl ID="FiveWordsApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="FiveWordsControl" TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="FiveWordsControlDialogButton" runat="server" BuddyControl="FiveWordsControl" ItemSource="SeekerVerbalizingWordDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="FiveSkillsControl">5 Skills:</label>
<sbCommon:ApplicabilityControl ID="FiveSkillsApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="FiveSkillsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="FiveSkillsControlDialogButton" runat="server" BuddyControl="FiveSkillsControl" ItemSource="SeekerSkillDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<h5>Seeker Type</h5>
<label for="StudentGroupControl">Student groups:</label>
<sb:FlagEnumCheckBoxList id="StudentGroupControl" runat="server"></sb:FlagEnumCheckBoxList>
<br />

<label for="SchoolTypeControl">School types:</label>
<sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server"></sb:FlagEnumCheckBoxList>
<br />

<label for="AcademicProgramControl">Academic programs:</label>
<sb:FlagEnumCheckBoxList id="AcademicProgramControl" runat="server"></sb:FlagEnumCheckBoxList>
<br />

<label for="SeekerStatusControl">Seeker statuses:</label>
<sb:FlagEnumCheckBoxList id="SeekerStatusControl" runat="server"></sb:FlagEnumCheckBoxList>
<br />

<label for="ProgramLengthControl">Length of program:</label>
<sb:FlagEnumCheckBoxList id="ProgramLengthControl" runat="server"></sb:FlagEnumCheckBoxList>
<br />

<label for="CollegesControl">Colleges:</label>
<sbCommon:ApplicabilityControl ID="CollegesApplicabilityControl" runat="server" Text="All">
  <asp:TextBox ID="CollegesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="CollegesControlDialogButton" runat="server" BuddyControl="CollegesControl" ItemSource="CollegeDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="FirstGenerationControl">First Generation:</label>
<asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
<br />

<h5>Seeker Demographics</h5>

<label for="EthnicityControl">Ethnicity:</label>
<sbCommon:ApplicabilityControl ID="EthinicityApplicabilityControl" runat="server" Text="N/A">
  <sb:LookupItemCheckboxList id="EthnicityControl" runat="server"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="ReligionControl">Religion:</label>
<sbCommon:ApplicabilityControl ID="ReligionApplicabilityControl" runat="server" Text="N/A">
  <sb:LookupItemCheckboxList id="ReligionControl" runat="server"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="SeekerStatusControl">Genders:</label>
<sbCommon:ApplicabilityControl ID="GendersApplicabilityControl" runat="server" Text="N/A">
  <sb:FlagEnumCheckBoxList id="GendersControl" runat="server"></sb:FlagEnumCheckBoxList>
</sbCommon:ApplicabilityControl>
<br />

<label for="StateControl">State:</label>
<sbCommon:ApplicabilityControl ID="StatesApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="StateControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="StateControlDialogButton" runat="server" BuddyControl="StateControl" ItemSource="SeekerVerbalizingWordDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="CountyControl">County:</label>
<sbCommon:ApplicabilityControl ID="CountyApplicabilityControl" runat="server" Text="All">
  <asp:TextBox ID="CountyControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="CountyControlDialogButton" runat="server" BuddyControl="CountyControl" ItemSource="CountyDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="CityControl">City:</label>
<sbCommon:ApplicabilityControl ID="CityApplicabilityControl" runat="server" Text="All">
  <asp:TextBox ID="CityControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="CityControlDialogButton" runat="server" BuddyControl="CityControl" ItemSource="CityDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="SchoolDistrictControl">School District:</label>
<sbCommon:ApplicabilityControl ID="SchoolDistrictApplicabilityControl" runat="server" Text="All">
  <asp:TextBox ID="SchoolDistrictControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="SchoolDistrictControlDialogButton" runat="server" BuddyControl="SchoolDistrictControl" ItemSource="SchoolDistrictDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="HighSchoolControl">High School:</label>
<sbCommon:ApplicabilityControl ID="HighSchoolApplicabilityControl" runat="server" Text="All">
  <asp:TextBox ID="HighSchoolControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl" ItemSource="HighSchoolDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<h5>Seeker Interests</h5>

<label for="AcademicAreasControl">Academic areas:</label>
<sbCommon:ApplicabilityControl ID="AcademicAreasApplicabilityControl" runat="server" Text="All">
  <asp:TextBox ID="AcademicAreasControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="AcademicAreasControlDialogButton" runat="server" BuddyControl="AcademicAreasControl" ItemSource="AcademicAreaDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="CareersControl">Careers:</label>
<sbCommon:ApplicabilityControl ID="CareersApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="CareersControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="CareersControlDialogButton" runat="server" BuddyControl="CareersControl" ItemSource="CareerDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="CommunityInvolvementCausesControl">Causes/Community Involvement:</label>
<sbCommon:ApplicabilityControl ID="CommunitiyInvolvementCausesApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="TextBox3" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="CommunityInvolvementCausesControlDialogButton" runat="server" BuddyControl="CommunityInvolvementCausesControl" ItemSource="CommunityInvolvementCauseDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="OrganizationsControl">Organizations:</label>
<sbCommon:ApplicabilityControl ID="OrganizationsApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="OrganizationsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="OrganizationsControlDialogButton" runat="server" BuddyControl="OrganizationsControl" ItemSource="SeekerMatchOrganizationDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="AffiliationTypesControl">Affiliation types:</label>
<sbCommon:ApplicabilityControl ID="AffiliationTypesApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="AffiliationTypesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="AffiliationTypesControlDialogButton" runat="server" BuddyControl="AffiliationTypesControl" ItemSource="AffiliationTypeDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<h5>Seeker Activities</h5>

<label for="SeekerHobbiesControl">Hobbies:</label>
<sbCommon:ApplicabilityControl ID="HobbiesApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="SeekerHobbiesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="SeekerHobbiesControlDialogButton" runat="server" BuddyControl="SeekerHobbiesControl" ItemSource="SeekerHobbyDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="SportsControl">Sports:</label>
<sbCommon:ApplicabilityControl ID="SportsApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="SportsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="SportsControlDialogButton" runat="server" BuddyControl="SportsControl" ItemSource="SportDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="ClubsControl">Clubs:</label>
<sbCommon:ApplicabilityControl ID="ClubsApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="ClubsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="ClubsControlDialogButton" runat="server" BuddyControl="ClubsControl" ItemSource="ClubDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="WorkTypeControl">Work Type:</label>
<sbCommon:ApplicabilityControl ID="WorkTypeApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="WorkTypeControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="WorkTypeControlDialogButton" runat="server" BuddyControl="WorkTypeControl" ItemSource="WorkTypeDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="WorkHoursControl">Work Hours:</label>
<sbCommon:ApplicabilityControl ID="WorkHoursApplicabilityControl" runat="server" Text="N/A">
  <sb:LookupItemCheckboxList id="WorkHoursControl" runat="server"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="ServiceTypeControl">Service Type:</label>
<sbCommon:ApplicabilityControl ID="ServiceTypeApplicabilityControl" runat="server" Text="N/A">
  <asp:TextBox ID="ServiceTypeControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
  <sb:LookupDialog ID="ServiceTypeControlDialogButton" runat="server" BuddyControl="ServiceTypeControl" ItemSource="ServiceTypeDAL" Title="Color Selection Dialog"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="ServiceHoursControl">Service hours:</label>
<sbCommon:ApplicabilityControl ID="ServiceHoursApplicabilityControl" runat="server" Text="N/A">
  <sb:LookupItemCheckboxList id="ServiceHoursControl" runat="server"/>
</sbCommon:ApplicabilityControl>
<br />

<h5>Seeker Performance</h5>

<label for="GAPControl">GPA:</label>
<sbCommon:ApplicabilityControl ID="GAPControlApplicabilityControl" runat="server" AddBRAfterCheckBox="true"  Text="N/A">
  <sb:NumberRangeCondition id="GPAControl" runat="server"/>
</sbCommon:ApplicabilityControl>
<br />

<label for="ClassRankControl">Class rank:</label>
<sbCommon:ApplicabilityControl ID="ClassRankApplicabilityControl" AddBRAfterCheckBox="true" runat="server" Text="N/A">
  <sb:NumberRangeCondition id="ClassRankControl" runat="server"/>
</sbCommon:ApplicabilityControl>
<br />

<label>SAT Score:</label>
<sbCommon:ApplicabilityControl ID="SATApplicabilityControl" AddBRAfterCheckBox="true" runat="server" Text="N/A">
  <label>Writing:</label> 
  <sb:NumberRangeCondition id="SATWritingControl" runat="server"/>
  <br />

  <label>Critical Reading:</label> 
  <sb:NumberRangeCondition id="SATCriticalReadingControl" runat="server"/>
  <br />
  
  <label>Mathematics:</label> 
  <sb:NumberRangeCondition id="SATMathematicsControl" runat="server"/>
  <br />
    
</sbCommon:ApplicabilityControl>
<br />

<label>ACT:</label>
<sbCommon:ApplicabilityControl ID="ACTApplicabilityControl" AddBRAfterCheckBox="true" runat="server" Text="N/A">
  <label>English:</label> 
  <sb:NumberRangeCondition id="ACTEnglishControl" runat="server"/>
  <br />
  
  <label>Mathematics:</label> 
  <sb:NumberRangeCondition id="ACTMathematicsControl" runat="server"/>
  <br />

  <label>Reading:</label> 
  <sb:NumberRangeCondition id="ACTReadingControl" runat="server"/>
  <br />

  <label>Science:</label> 
  <sb:NumberRangeCondition id="ScienceControl" runat="server"/>
  <br />
    
</sbCommon:ApplicabilityControl>
<br />

<label for="HonorsControl">Honors:</label>
<asp:CheckBox ID="HonorsControl" Text="" runat="server" />
<br />

<label for="APCreditControl">AP Credits Earned:</label>
<asp:TextBox ID="APCreditControl" runat="server" />
<br />
