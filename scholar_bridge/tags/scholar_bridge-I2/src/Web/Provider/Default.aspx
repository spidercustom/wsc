﻿<%@ Page Title="Provider" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
  <p>
    Place holder: landing page for provider
  </p>
  
  <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Provider/RegisterProvider.aspx">Register as a Scholarship Provider</asp:HyperLink>
  
</asp:Content>
