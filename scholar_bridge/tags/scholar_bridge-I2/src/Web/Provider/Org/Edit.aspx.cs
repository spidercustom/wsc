﻿using System;
using System.Web.UI;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Org
{
    public partial class Edit : Page
    {
        public IProviderDAL ProviderService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            editOrg.Organization = UserContext.CurrentIntermediary;
        }

        protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.Update((Domain.Provider)org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Provider/");
        }

        protected void editOrg_OnFormCanceled()
        {
            Response.Redirect("~/Provider/");
        }
    }
}
