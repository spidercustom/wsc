﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Provider.Users
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;

            orgUserList.Users = provider.ActiveUsers;
            orgInactiveUserList.Users = provider.DeletedUsers;
        }
    }
}