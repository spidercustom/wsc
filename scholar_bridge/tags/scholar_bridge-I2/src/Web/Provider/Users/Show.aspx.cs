﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using Spring.Web.UI;

namespace ScholarBridge.Web.Provider.Users
{
    public partial class Show : Page
    {
        public IUserContext UserContext { get; set; }
        public IProviderService ProviderService { get; set; }

        public int UserId { get { return Int32.Parse(Request["id"]); } }
        public User CurrentUser { get; set;}
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            CurrentUser = ProviderService.FindUserInOrg(UserContext.CurrentProvider, UserId);
            userDetails.CurrentUser = CurrentUser;

            ToggleButtons();
        }

        private void ToggleButtons()
        {
            deleteBtn.Visible = ! CurrentUser.IsDeleted;
            reactivateBtn.Visible = CurrentUser.IsDeleted;

            // Don't allow someone to delete themselves
            if (UserContext.CurrentUser.Id == UserId)
            {
                deleteBtn.Visible = false;
            }
        }

        protected void deleteBtn_Click(object sender, EventArgs e)
        {
            CurrentUser.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.DeleteUser(UserContext.CurrentProvider, CurrentUser);
            SuccessMessageLabel.SetMessage("User was deleted.");
            Response.Redirect("~/Provider/Users/");
        }

        protected void reactivateBtn_Click(object sender, EventArgs e)
        {
            CurrentUser.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.ReactivateUser(UserContext.CurrentProvider, CurrentUser);
            SuccessMessageLabel.SetMessage("User was reactivated.");
            Response.Redirect("~/Provider/Users/");
        }
    }
}
