﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Provider.Users
{
    public partial class Create : Page
    {
        private readonly EmailConfirmation emailConfirmation = new EmailConfirmation();

        public IUserContext UserContext { get; set; }
        public IProviderService ProviderService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
        }

        protected void userForm_OnUserSaved(User user)
        {
            var provider = UserContext.CurrentProvider;
            user.LastUpdate = new ActivityStamp(UserContext.CurrentUser);

            var mailParams = GetConfirmationMailTemplateParams(user);
            ProviderService.SaveNewUser(provider, user, mailParams);

            SuccessMessageLabel.SetMessage("User was created.");
            Response.Redirect("~/Provider/Users/");
        }

        protected void userForm_OnFormCanceled()
        {
            Response.Redirect("~/Provider/Users/");
        }


        private MailTemplateParams GetConfirmationMailTemplateParams(User user)
        {
            var mailParams = ConfigHelper.GetMailParams(user.Email);
            mailParams.MergeVariables.Add("Name", user.Name.ToString());
            mailParams.MergeVariables.Add("Link", emailConfirmation.BuildConfirmationLinkForEmail(user.Email, true));
            return mailParams;
        }
    }
}
