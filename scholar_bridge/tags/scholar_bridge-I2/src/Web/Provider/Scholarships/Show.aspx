﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Show" %>
<%@ Register src="GeneralInfoShow.ascx" tagname="GeneralInfoShow" tagprefix="uc1" %>
<%@ Register src="SeekerInfoShow.ascx" tagname="SeekerInfoShow" tagprefix="uc2" %>
<%@ Register src="FundingInfoShow.ascx" tagname="FundingInfoShow" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <%--  Welcome <asp:Label ID="userNameLbl" runat="server" /> for <asp:Label ID="providerNameLbl" runat="server" />--%>
<div class="print">&nbsp;</div>

<h4>View Scholarship : <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label></h4>
<div id="linkarea">
<p>[<asp:HyperLink ID="linkCopy" runat="server">Copy</asp:HyperLink>]</p> 
</div>

<div class="tabs">
     
    <ul class="linkarea">
        <li><a  href="#generalinfo-tab"><span>General Information</span></a></li>
        <li><a href="#seekerprofile-tab"><span>Seeker Profile</span></a></li>
        <li><a href="#fundingprofile-tab"><span>Funding Profile</span></a></li>
    </ul>
     
    <div id="generalinfo-tab">
        <uc1:GeneralInfoShow ID="GeneralInfoShow1" runat="server" LinkTo="~/Provider/BuildScholarship/Default.aspx" />
    </div>
    <div id="seekerprofile-tab">
        <uc2:SeekerInfoShow ID="SeekerInfoShow1" runat="server" LinkTo="~/Provider/BuildScholarship/Default.aspx" />
    </div>
    <div id="fundingprofile-tab">
        <uc3:FundingInfoShow ID="FundingInfoShow1" runat="server" LinkTo="~/Provider/BuildScholarship/Default.aspx" />
    </div>
</div>
</asp:Content>