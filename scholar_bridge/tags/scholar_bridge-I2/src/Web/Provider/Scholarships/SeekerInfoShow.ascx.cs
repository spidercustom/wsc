﻿using System;
using System.Linq;
using ScholarBridge.Common;
using ScholarBridge.Domain;
namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class SeekerInfoShow : System.Web.UI.UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //setup links
            string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                        (int)ScholarshipStages.SeekerProfile);
            linkBottom.NavigateUrl = navurl;
            linkTop.NavigateUrl = navurl;

            
            //seeker personality
            var seeker = ScholarshipToView.SeekerMatchCriteria;
            
            if (seeker.Words.Count>0)
                lblFiveWords.Text = string.Join(", ", (seeker.Words.Select(word => new { word.Name }.Name).ToArray()));
            if (seeker.Skills.Count > 0)
                lblFiveWords.Text = string.Join(", ", (seeker.Skills.Select(skill=> new { skill.Name }.Name).ToArray()));

            lblStudentGroups.Text = seeker.StudentGroups.GetDisplayName();
            lblSchoolTypes.Text  = seeker.SchoolTypes.GetDisplayName();
            lblAcademicPrograms.Text  = seeker.AcademicPrograms.GetDisplayName();
            lblSeekerStatuses.Text = seeker.SeekerStatuses.GetDisplayName();
            lblLengthOfProgram.Text = seeker.ProgramLengths.GetDisplayName();

            if (seeker.Colleges.Count > 0)
                lblColleges.Text = string.Join(", ", (seeker.Colleges.Select(col=> new { col.Name }.Name).ToArray()));
            lblFirstGeneration.Text = seeker.FirstGeneration.ToString();

            //seeker demographics
            if (seeker.Ethnicities.Count > 0)
                lblEthinicity.Text = string.Join(", ", (seeker.Ethnicities.Select(eth=> new { eth.Name }.Name).ToArray()));
             
             if (seeker.Religions.Count > 0)
                lblReligion.Text = string.Join(", ", (seeker.Religions.Select(rel=> new { rel.Name }.Name).ToArray()));
            
            lblGender.Text = seeker.Genders.GetDisplayName();
            
            if (seeker.Counties.Count > 0)
                lblCounty.Text = string.Join(", ", (seeker.Counties.Select(county => new { county.Name }.Name).ToArray()));
           
            if (seeker.Cities.Count > 0)
                lblCity.Text = string.Join(", ", (seeker.Cities.Select(city => new { city.Name }.Name).ToArray()));

            if (seeker.SchoolDistricts.Count > 0)
                lblSchoolDistrict.Text = string.Join(", ", (seeker.SchoolDistricts.Select(dist=> new {dist.Name }.Name).ToArray()));
            
            if (seeker.HighSchools.Count > 0)
                lblHighSchool.Text = string.Join(", ", (seeker.HighSchools.Select(hs => new { hs.Name }.Name).ToArray()));
            
            //seeker interests

            if (seeker.AcademicAreas.Count > 0)
                lblAcademicAreas.Text = string.Join(", ", (seeker.AcademicAreas.Select(area => new {area.Name }.Name).ToArray()));

            if (seeker.Careers.Count > 0)
                lblCareers.Text = string.Join(", ", (seeker.Careers.Select(career => new { career.Name }.Name).ToArray()));

            if (seeker.CommunityInvolvementCauses.Count > 0)
                lblCauses.Text = string.Join(", ", (seeker.CommunityInvolvementCauses.Select(cause => new { cause.Name }.Name).ToArray()));


            if (seeker.Organizations.Count > 0)
                lblOrganizations.Text = string.Join(", ", (seeker.Organizations.Select(org => new { org.Name }.Name).ToArray()));

            if (seeker.AffiliationTypes.Count > 0)
                lblAffiliationTypes.Text = string.Join(", ", (seeker.AffiliationTypes.Select(aff => new { aff.Name }.Name).ToArray()));

            //seeker activities


            if (seeker.Hobbies.Count > 0)
                lblHobbies.Text = string.Join(", ", (seeker.Hobbies.Select(hobby => new { hobby.Name }.Name).ToArray()));

            if (seeker.Sports.Count > 0)
                lblSports.Text = string.Join(", ", (seeker.Sports.Select(sport => new { sport.Name }.Name).ToArray()));

            if (seeker.Clubs.Count > 0)
                lblClubs.Text = string.Join(", ", (seeker.Clubs.Select(club => new { club.Name }.Name).ToArray()));
            
            if (seeker.WorkTypes.Count > 0)
                lblWorkType.Text = string.Join(", ", (seeker.WorkTypes.Select(work=> new {work.Name }.Name).ToArray()));

            if (seeker.ServiceTypes.Count > 0)
                lblServiceType.Text = string.Join(", ", (seeker.ServiceTypes.Select(service=> new { service.Name }.Name).ToArray()));

            if (seeker.ServiceHours.Count > 0)
                lblServiceHours.Text = string.Join(", ", (seeker.ServiceHours.Select(hours => new { hours.Name }.Name).ToArray()));
            //Seeker performance
            if ( seeker.GPA !=null)
                lblGPA.Text = string.Format("{0} - {1}", seeker.GPA.Minimum, seeker.GPA.Maximum);
        
            if (seeker.SATScore  != null)
            {
                string satscore = string.Format("Critical Reading : {0} - {1}", seeker.SATScore.CriticalReading.Minimum , 
                    seeker.SATScore.CriticalReading.Maximum );
                satscore += string.Format("Writing : {0} - {1}", seeker.SATScore.Writing.Minimum,
                    seeker.SATScore.Writing.Maximum);
                satscore += string.Format("Mathematics : {0} - {1}", seeker.SATScore.Mathematics.Minimum,
                    seeker.SATScore.Mathematics.Maximum);
                lblSATScore.Text = satscore;
            }

            if (seeker.ACTScore != null)
            {
                string actscore = string.Format("Reading : {0} - {1}", seeker.ACTScore.Reading.Minimum,
                    seeker.ACTScore.Reading);
               
                actscore += string.Format("Mathematics : {0} - {1}", seeker.ACTScore.Mathematics.Minimum,
                    seeker.ACTScore.Mathematics.Maximum);
                actscore += string.Format("English: {0} - {1}", seeker.ACTScore.English.Minimum,
                                    seeker.ACTScore.English.Maximum);
                actscore += string.Format("Science: {0} - {1}", seeker.ACTScore.Science.Minimum,
                                    seeker.ACTScore.Science.Maximum);
                lblACTScore.Text = actscore;
            }

            if (seeker.ClassRank != null)
                lblClassRank.Text = string.Format("{0} - {1}", seeker.ClassRank.Minimum, seeker.ClassRank.Maximum);

                
            lblAPCredits.Text = seeker.APCreditsEarned.ToString();
            lblHonors.Text = seeker.Honors.ToString();
            
            
        }

    }
    }
