﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Wizards;
using Spring.Context;
using Spring.Context.Support;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Show : System.Web.UI.Page
    {
        private const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        private Domain.Scholarship _ScholarshipToView;
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            if (provider.ApprovalStatus != ScholarBridge.Domain.ApprovalStatus.Approved)
                throw new Business.Exceptions.ProviderNotApprovedException();
            ScholarshipToView = GetScholarshipToView();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
           
            
            if (!IsPostBack)
            {
               
                if (ScholarshipToView != null)
                {
                    if (!ScholarshipToView.Provider.Id.Equals(UserContext.CurrentProvider.Id))
                        throw new InvalidOperationException("Scholarship do not belong to provider in context");

                    GeneralInfoShow1.ScholarshipToView = ScholarshipToView;
                    SeekerInfoShow1.ScholarshipToView = ScholarshipToView;
                    FundingInfoShow1.ScholarshipToView = ScholarshipToView;
                    linkCopy.NavigateUrl = "~/Provider/BuildScholarship/Default.aspx?copyfrom=" +
                                           ScholarshipToView.Id.ToString();
                    lblName.Text = ScholarshipToView.Donor == null ? ScholarshipToView.Name : string.Format("{0} ({1})", ScholarshipToView.Name, ScholarshipToView.Donor.Name.ToString());
                }
            }
        }


       
        public Domain.Scholarship GetScholarshipToView()
        {
            string scholarshipIdString = ScholarshipIdString;

            if (string.IsNullOrEmpty(scholarshipIdString)) //if not found so far return null
                return null;

            int scholarshipId = -1;
            if (!Int32.TryParse(scholarshipIdString, out scholarshipId))
                throw new ArgumentException("Cannot understand value of parameter scholarship");
            Domain.Scholarship result = ScholarshipService.GetById(scholarshipId);
            return result;
        }

        private string ScholarshipIdString
        {
            get
            {
                string scholarshipIdString = string.Empty;
                if (!string.IsNullOrEmpty(Request.Params[SCHOLARSHIP_ID])) //retrive from parameters
                    scholarshipIdString = Request.Params[SCHOLARSHIP_ID];
                return scholarshipIdString;
            }
          
        }

        public Scholarship ScholarshipToView
        {
            get { return _ScholarshipToView; }
            set { _ScholarshipToView = value; }
        }

       
       
    }
}
