﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class FundingInfoShow : System.Web.UI.UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //setup links
            string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                        (int)ScholarshipStages.FundingProfile);
            linkBottom.NavigateUrl = navurl;
            linkTop.NavigateUrl = navurl;


            //view funding info
            var need = ScholarshipToView.Need;

            var needlist = new List<string>();
            if (need.Fafsa)
                needlist.Add("FAFSA");

            if (need.UserDerived)
                needlist.Add("User Derived");

            lblNeed.Text = string.Join(", ", needlist.ToArray());



            lblMinMaxNeed.Text = string.Format("{0} - {1}", need.MaximumSeekerNeed.ToString("c"),
                                    need.MaximumSeekerNeed.ToString("c"));

            if (need.NeedGaps.Count > 0)
                lblNeedGAP.Text = string.Join(", ", (need.NeedGaps.Select(gap => new { gap.Name }.Name).ToArray()));
        }
    }
}