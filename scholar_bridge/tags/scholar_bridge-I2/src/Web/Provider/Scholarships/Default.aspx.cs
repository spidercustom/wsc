﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            var activatedStage = ScholarshipService.GetStageById(ScholarshipStages.Activated);
            var awardedStage = ScholarshipService.GetStageById(ScholarshipStages.Awarded);
            scholarshipNotActivatedList.Scholarships = ScholarshipService.GetNotActivatedScholarshipsByProvider(provider);
            scholarshipActivatedList.Scholarships = ScholarshipService.GetByStageAndProvider(activatedStage, provider);
            scholarshipAwardedList.Scholarships = ScholarshipService.GetByStageAndProvider(awardedStage, provider);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        
    }
}