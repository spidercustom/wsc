﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Default" Title="Provider | Scholarships" %>

<%@ Register TagPrefix="sb" TagName="ProviderScholarshipList" Src="~/Common/ProviderScholarshipList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div class="tabs">
    <ul>
        <li><a href="#notactivated-scholarships-tab"><span>Not Activated</span></a></li>
        <li><a href="#activated-scholarship-tab"><span>Activated</span></a></li>
        <li><a href="#awarded-scholarship-tab"><span>Awarded</span></a></li>
    </ul>
    <div id="notactivated-scholarships-tab">
       <h3>Not Activated Scholarships</h3>
        <sb:ProviderScholarshipList id="scholarshipNotActivatedList" runat="server" 
            LinkTo="~/Provider/Scholarships/Show.aspx" />
    </div>
    <div id="activated-scholarship-tab">
        <h3>Activated Scholarships</h3>
        <sb:ProviderScholarshipList id="scholarshipActivatedList" runat="server" 
            LinkTo="~/Provider/Scholarships/Show.aspx" />
    </div>
    <div id="awarded-scholarship-tab">
        <h3>Awarded Scholarships</h3>
        <sb:ProviderScholarshipList id="scholarshipAwardedList" runat="server" 
            LinkTo="~/Provider/Scholarships/Show.aspx" />
    </div>
</div>


    
</asp:Content>
