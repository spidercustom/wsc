﻿using System;
using System.Text;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class GeneralInfoShow : System.Web.UI.UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //setup links
            string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                         (int)ScholarshipStages.GeneralInformation );
            linkBottom.NavigateUrl = navurl;
            linkTop.NavigateUrl = navurl;


            
            lblName.Text = ScholarshipToView.Name;

            lblMission.Text = ScholarshipToView.MissionStatement;
            lblAppStart.Text = ScholarshipToView.Schedule.StartFromDisplayString;
            lblAppDue.Text = ScholarshipToView.Schedule.DueOnDisplayString;
            lblAwardOn.Text = ScholarshipToView.Schedule.AwardOnDisplayString;

            //var schedule=Domain.ScholarshipParts.
            lblAwardAmount.Text = string.Format("{0} - {1}", ScholarshipToView.MinimumAmount.ToString("c" ),
                                                ScholarshipToView.MaximumAmount.ToString("c"));
            //view donor info
            lblDonor.Text = GetDonorDisplayString(ScholarshipToView.Donor);

        }

        private string GetDonorDisplayString(ScholarshipDonor donor)
        {
             
            var sb = new StringBuilder();
            
            sb.Append(string.Format("{0}, {1}", donor.Name, donor.Address));
            
            if (null != donor.Phone && !string.IsNullOrEmpty(donor.Phone.Number))
                sb.Append(string.Format(" Phone : {0}", donor.Phone.Number));

            if (!string.IsNullOrEmpty(donor.Email))
                sb.Append(string.Format(" Email : {0}", donor.Email));
            
            return sb.ToString();
        }

        
    }
}