﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Intermediary.Users
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = UserContext.CurrentIntermediary;

            orgUserList.Users = intermediary.ActiveUsers;
            orgInactiveUserList.Users = intermediary.DeletedUsers;
        }
    }
}