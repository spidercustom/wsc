﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Intermediary.Users
{
    public partial class Create : Page
    {
        private readonly EmailConfirmation emailConfirmation = new EmailConfirmation();

        public IUserContext UserContext { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
        }

        protected void userForm_OnUserSaved(User user)
        {
            user.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var intermediary = UserContext.CurrentIntermediary;

            var mailParams = GetConfirmationMailTemplateParams(user);
            IntermediaryService.SaveNewUser(intermediary, user, mailParams);

            SuccessMessageLabel.SetMessage("User was created.");
            Response.Redirect("~/Intermediary/Users/");
        }

        protected void userForm_OnFormCanceled()
        {
            Response.Redirect("~/Intermediary/Users/");
        }

        private MailTemplateParams GetConfirmationMailTemplateParams(User user)
        {
            var mailParams = ConfigHelper.GetMailParams(user.Email);
            mailParams.MergeVariables.Add("Name", user.Name.ToString());
            mailParams.MergeVariables.Add("Link", emailConfirmation.BuildConfirmationLinkForEmail(user.Email, true));
            return mailParams;
        }
    }
}
