﻿using System;
using System.Web.UI;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Intermediary.Org
{
    public partial class Edit : Page
    {
        public IIntermediaryDAL IntermediaryService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            editOrg.Organization = UserContext.CurrentIntermediary;
        }

        protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            IntermediaryService.Update((Domain.Intermediary) org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Intermediary/");
        }

        protected void editOrg_OnFormCanceled()
        {
            Response.Redirect("~/Intermediary/");
        }
    }
}
