﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Profile
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            userDetails.CurrentUser = UserContext.CurrentUser;
        }
    }
}