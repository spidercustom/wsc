﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Profile
{
    public partial class ChangeEmail : Page
    {

        public EmailConfirmation emailConfirmation = new EmailConfirmation();

        public IUserContext UserContext { get; set; }
        public IUserService UserService { get; set; }

        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            CurrentUser = UserContext.CurrentUser;
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var mailParams = ConfigHelper.GetMailParams(Email.Text);
                mailParams.MergeVariables.Add("Link", emailConfirmation.BuildConfirmationLinkForEmail(Email.Text));
                CurrentUser.LastUpdate = new ActivityStamp(CurrentUser);
                UserService.ResetUsername(CurrentUser, Email.Text, mailParams);

                SuccessMessageLabel.SetMessage("Your Email Address has been changed.");
                Response.Redirect("~/Profile/");
            }
        }

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Profile/");
        }
    }
}
