﻿using System;
using System.Web.Security;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web
{
    public partial class ConfirmEmail : Page
    {
        private readonly EmailConfirmation emailConfirmation = new EmailConfirmation();

        public IUserService UserService { get; set; }
        public IUserDAL UserDAL { get; set; }

        public string Key { get { return Request.QueryString["key"]; } }

        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Key) || !IsValidKey(Key))
            {
                SetError();
                return;
            }

            bool needsResetPassword;
            var userName = emailConfirmation.GetEmailAddress(Key, out needsResetPassword);
            CurrentUser = UserDAL.FindByUsername(userName);
            if (null == CurrentUser)
            {
                SetError();
                return;
            }

            if (!Page.IsPostBack)
            {
                if (needsResetPassword)
                {
                    lblStatus.Text = "Please create a password to use to log in.";
                    setPassword.Visible = true;
                    return;
                }

                ConfirmUser(CurrentUser);
            }
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                setPassword.Visible = false;

                CurrentUser.Password = emailConfirmation.HashPassword(Password.Text, CurrentUser.PasswordSalt);
                CurrentUser.PasswordFormat = (int) MembershipPasswordFormat.Hashed;
                ConfirmUser(CurrentUser);
            }
        }

        private void ConfirmUser(User user)
        {
            // XXX: If this gets any more complicated we should pull this into a helper class.
            user.LastUpdate = new ActivityStamp(user);
            if (user.IsInRole(Role.PROVIDER_ROLE) && ! user.IsApproved)
            {
                ConfirmProvider(user);
            }
            else if (user.IsInRole(Role.INTERMEDIARY_ROLE) && !user.IsApproved)
            {
                ConfirmIntermediary(user);
            }
            else
            {
                ConfirmNormalUser(user);
            }
        }

        private void ConfirmNormalUser(User u)
        {
            UserService.ActivateUser(u);
            lblStatus.Text = "Your email has been validated.  You may now login.";
        }

        private void ConfirmProvider(User u)
        {
            var mailParams = ConfigHelper.GetMailParams(ConfigHelper.GetHECBAdminEmail());
            mailParams.MergeVariables.Add("Link", BuildLinkToProviderApproval());
            UserService.ActivateProviderUser(u, mailParams);

            lblStatus.Text = "Thank you for your request. Your email has been validated.  You will receive an email once you are setup in the system.  The validation process may take up to 1 week.";
        }

        private void ConfirmIntermediary(User u)
        {
            var mailParams = ConfigHelper.GetMailParams(ConfigHelper.GetHECBAdminEmail());
            mailParams.MergeVariables.Add("Link", BuildLinkToIntermediaryApproval());
            UserService.ActivateIntermediaryUser(u, mailParams);

            lblStatus.Text = "Thank you for your request. Your email has been validated.  You will receive an email once you are setup in the system.  The validation process may take up to 1 week.";
        }

        private static bool IsValidKey(string key)
        {
            return key.StartsWith("enc");
        }

        public void SetError()
        {
            lblStatus.Text = "The link you have followed is not valid.";
        }

        public static string BuildLinkToProviderApproval()
        {
			return ConfigHelper.GetPageURLViaProxy("Admin/PendingProviders.aspx");
		}

        public static string BuildLinkToIntermediaryApproval()
        {
			return ConfigHelper.GetPageURLViaProxy("Admin/PendingIntermediaries.aspx");
        }
    }
}