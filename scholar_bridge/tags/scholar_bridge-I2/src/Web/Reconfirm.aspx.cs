﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web
{
    public partial class Reconfirm : System.Web.UI.Page
    {
        public EmailConfirmation emailConfirmation = new EmailConfirmation();

        public IUserDAL UserService { get; set; }
        public ITemplatedMailerService MailerService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void StepNextButton_ButtonClick(object sender, WizardNavigationEventArgs e)
        {
            User user = UserService.FindByEmail(EmailAddress.Text);
            if (null == user)
            {
                SetError("No user found with that email address.", e);
                return;
            }

            if (user.IsActive)
            {
                SetError("Your account is already active.", e);
                return;
            }
            
            doneMessage.Text = String.Format("Confirmation email sent to '{0}'.", EmailAddress.Text);
            emailConfirmation.SendConfirmationEmail(MailerService, user);
        }

        private void SetError(string errorMessage, WizardNavigationEventArgs e)
        {
            errorLbl.Text = errorMessage;
            errorLbl.Visible = true;
            e.Cancel = true;
        }
    }
}
