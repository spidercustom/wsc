﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Admin.Default" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<ul class="pageNav">
    <li><asp:HyperLink ID="pendingProvidersLnk" runat="server" NavigateUrl="~/Admin/PendingProviders.aspx">Pending Providers</asp:HyperLink></li>
    <li><asp:HyperLink ID="pendingIntermediariesLnk" runat="server" NavigateUrl="~/Admin/PendingIntermediaries.aspx">Pending Intermediaries</asp:HyperLink></li>
</ul>

</asp:Content>
