﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Admin
{
    public partial class ApproveProvider : System.Web.UI.Page
    {
        public IProviderService ProviderService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Provider currentProvider;

        public int CurrentProviderId
        {
            get { return Int32.Parse(Request["id"]); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            currentProvider = ProviderService.Get(CurrentProviderId);
            showOrg.Organization = currentProvider;
        }

        protected void approveButton_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var mailParams = ConfigHelper.GetMailParams(currentProvider.AdminUser.Email);
			mailParams.MergeVariables.Add("Link", ConfigHelper.GetPageURLViaProxy("login.aspx"));

            ProviderService.Approve(currentProvider, mailParams);
            ReturnToList();
        }

        protected void rejectButton_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var mailParams = ConfigHelper.GetMailParams(currentProvider.AdminUser.Email);
            ProviderService.Reject(currentProvider, mailParams);
            ReturnToList();
        }

        protected void addNotes_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            currentProvider.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            ProviderService.Update(currentProvider);
            newNotesTb.Text = null;
            existingNotesTb.Text = currentProvider.AdminNotes;
        }

        private void ReturnToList()
        {
            Response.Redirect("~/Admin/PendingProviders.aspx");
        }
    }
}
