﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ApproveIntermediary.aspx.cs" Inherits="ScholarBridge.Web.Admin.ApproveIntermediary" Title="Admin | Approve Intermediary" %>

<%@ Register TagPrefix="sb" TagName="ShowOrg" Src="~/Admin/ShowOrg.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Approve/Reject Pending Intermediary</h3>

    <sb:ShowOrg id="showOrg" runat="server" />

    <label for="existingNotesTb">Administrative Notes:</label>  
    <asp:TextBox ID="existingNotesTb" runat="server" Enabled="false" TextMode="MultiLine" Columns="300" Rows="10"></asp:TextBox><br />
    <label for="existingNotesTb">Add Notes:</label>  
    <asp:TextBox ID="newNotesTb" runat="server" TextMode="MultiLine" Columns="300" Rows="5"></asp:TextBox><br />
    <asp:Button ID="addNotes" runat="server" Text="Add Note" onclick="addNotes_Click"  />
    <br />

    <asp:Button ID="approveButton" runat="server" Text="Approve" onclick="approveButton_Click" />
    <asp:Button ID="rejectButton" runat="server" Text="Deny" onclick="rejectButton_Click" />
</asp:Content>
