﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Admin
{
    public partial class ApproveIntermediary : System.Web.UI.Page
    {
        public IIntermediaryService IntermediaryService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Intermediary currentIntermediary;

        public int CurrentProviderId
        {
            get { return Int32.Parse(Request["id"]); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            currentIntermediary = IntermediaryService.Get(CurrentProviderId);
            showOrg.Organization = currentIntermediary;
        }

        protected void approveButton_Click(object sender, EventArgs e)
        {
            currentIntermediary.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var mailParams = ConfigHelper.GetMailParams(currentIntermediary.AdminUser.Email  );
            IntermediaryService.Approve(currentIntermediary, mailParams);
            ReturnToList();
        }

        protected void rejectButton_Click(object sender, EventArgs e)
        {
            currentIntermediary.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var mailParams = ConfigHelper.GetMailParams(currentIntermediary.AdminUser.Email);
            IntermediaryService.Reject(currentIntermediary, mailParams);
            ReturnToList();
        }

        protected void addNotes_Click(object sender, EventArgs e)
        {
            currentIntermediary.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            currentIntermediary.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            IntermediaryService.Update(currentIntermediary);
            newNotesTb.Text = null;
            existingNotesTb.Text = currentIntermediary.AdminNotes;
        }

        private void ReturnToList()
        {
            Response.Redirect("~/Admin/PendingIntermediaries.aspx");
        }
    }
}
