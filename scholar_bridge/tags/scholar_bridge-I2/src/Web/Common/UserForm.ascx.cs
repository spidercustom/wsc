﻿using System;
using System.Web.Security;
using System.Web.UI;
using Common.Logging;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public delegate void UserSaved(User user);
    public delegate void FormCanceled();

    public partial class UserForm : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserForm));

        public IUserDAL UserService { get; set; }

        public event UserSaved UserSaved;
        public event FormCanceled FormCanceled;

        protected void Page_Load(object sender, EventArgs e)
        {
 
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    // email for both username and email
                    Membership.CreateUser(UserName.Text, Membership.GeneratePassword(8, 3), UserName.Text);

                    var currentUser = UserService.FindByUsername(UserName.Text);
                    currentUser.Username = UserName.Text;
                    currentUser.Name.FirstName = FirstName.Text;
                    currentUser.Name.MiddleName = MiddleName.Text;
                    currentUser.Name.LastName = LastName.Text;

                    if (null != UserSaved)
                    {
                        UserSaved(currentUser);
                    }
                } 
                catch (MembershipCreateUserException mcex)
                {
                    log.Warn("Failed to create user.", mcex);
                    SetError(mcex.Message, mcex.StatusCode);
                }
            }
        }

        private void SetError(string message, MembershipCreateStatus code)
        {
            errorMessage.Visible = true;
            if (code == MembershipCreateStatus.DuplicateEmail 
                || code == MembershipCreateStatus.DuplicateUserName)
            {
                errorMessage.Text = "That email address is already in use.";
            }
            else
            {
                errorMessage.Text = "Could not create the user. " + message; 
            }
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}