﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class UserDetails : UserControl
    {
        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (null == CurrentUser)
                {
                    errorMessage.Visible = true;
                    return;
                }

                nameLbl.Text = CurrentUser.Name.NameFirstLast;
                emailAddressLbl.Text = CurrentUser.Email;
                SetNullableDate(memberSinceLbl, CurrentUser.CreationDate);
                SetNullableDate(lastLoginLbl, CurrentUser.LastLoginDate);
            }
        }

        private static void SetNullableDate(ITextControl lbl, DateTime? date)
        {
            if (date.HasValue)
            {
                lbl.Text = date.Value.ToString("M/dd/yyyy HH:mm");
            }
        }
    }
}