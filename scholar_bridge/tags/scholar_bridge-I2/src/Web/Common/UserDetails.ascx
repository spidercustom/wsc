﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs" Inherits="ScholarBridge.Web.Common.UserDetails" %>

<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>

<asp:Image ID="personImage" runat="server" ImageUrl="~/images/person.png" class="profileImage"/>

<div id="memberInfo">
    <asp:Label ID="nameLbl" runat="server" /><br />
    <asp:Label ID="emailAddressLbl" runat="server" />
    <p>
    <small>Member Since: <asp:Label ID="memberSinceLbl" runat="server" /></small><br />
    <small>Last Login: <asp:Label ID="lastLoginLbl" runat="server" /></small>
    </p>
</div>