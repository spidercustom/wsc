﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupList.ascx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupList" %>
<link href="/styles/lookuplist.css" rel="stylesheet" type="text/css" />
     
<div id="wrapper">
    <h1>Select an item by click or multiple items by ctrl+click and clicking Add</h1>    
    <div id="gridcontainer"> 
        <asp:Table ID="flex" runat="server" style="display:none" ></asp:Table>
    </div>
    
    <div id="addbuttoncontainer">
        <asp:button id="btnAdd" runat="server" text="Add Selections"/>
        <asp:TextBox ID="txtSelection"  runat="server" Height="100px" Width="600px" TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
    </div>
</div>


