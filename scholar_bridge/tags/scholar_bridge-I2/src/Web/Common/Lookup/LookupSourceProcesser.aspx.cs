﻿using System;
using ScholarBridge.Data;
using Spring.Context.Support;

namespace ScholarBridge.Web.Common.Lookup
{
    public partial class LookupSourceProcesser : System.Web.UI.Page
    {
        private const string LOOKUPSOURCE = "LookupSource";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request[LOOKUPSOURCE] == null)
                throw new ArgumentNullException("query string LookupSource is null");

            //read page values passed from grid
            int PageNo = int.Parse(Request.Form["page"]);
            int NumberofRows = int.Parse(Request.Form["rp"]);
            string SortOn = Request.Form["sortname"];
            string SortOrder = Request.Form["sortorder"];
            string SearchOn = Request.Form["qtype"];
            string SearchValue = Request.Form["query"];

            if (PageNo == 0)
                PageNo = 1;
            if (NumberofRows == 0)
                NumberofRows = 15;

            //generate xml 
            var source = BuildLookupDialogItemSource(Request[LOOKUPSOURCE]);


            var helper = new LookupDialogHelper(source, PageNo, NumberofRows, SortOn, SortOrder, SearchOn,
                                                SearchValue);

            string xmldata = helper.GetXml();

            //write response 
            Response.ClearHeaders();
            Response.AppendHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
            Response.AppendHeader("Last-Modified",
                                  DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
            Response.AppendHeader("Cache-Control", "no-cache, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Content-type", "text/xml");
            Response.Write(xmldata);
            Response.End();
        }

        private ILookupDAL BuildLookupDialogItemSource(string springKey)
        {
            var context = ContextRegistry.GetContext();
            var retrivedObject = context.GetObject(springKey);
            if (null == retrivedObject)
                throw new ArgumentException(string.Format("Cannot retrieve object by key {0}", springKey));
            var lookupService = retrivedObject as ILookupDAL;
            if (null == lookupService)
                throw new ArgumentException(string.Format("Cannot cast object retrieved by key {0} to ILookupDAL", springKey));

            return lookupService;
        }
    }
}