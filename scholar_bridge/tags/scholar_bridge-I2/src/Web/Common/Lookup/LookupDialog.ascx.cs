﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using JSHelper = ScholarBridge.Web.Common.Lookup.LookupDialogJSHelper;
namespace ScholarBridge.Web.Common.Lookup 
{
    public partial class LookupDialog : UserControl
    {
        [Description("Full Name of Type Implementing ILookupDialogItemSource")]
        public string ItemSource { get; set; }
        public string Title { get; set; }
        public string BuddyControl { get; set; }
        public string DataPageUrl { get; set; }

        public string Keys
        {
            get { return hiddenids.Value; }
            set { hiddenids.Value = value; }
        }

       
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //validate required properties
            if (string.IsNullOrEmpty(BuddyControl ))
                throw new NullReferenceException("Propery Buddy Control must have a valid value");
            if (string.IsNullOrEmpty(ItemSource))
                throw new NullReferenceException("Propery ItemSource must have a valid value");

            DataPageUrl = this.TemplateSourceDirectory + "/LookupSourceProcesser.aspx";

            CreateDynamicControls();
        }

        private void CreateDynamicControls()
        {
            
            var div = CreateDivforDialog();
            var uc = CreateLookupListControl(); 

            div.Controls.Add(uc);
            this.Controls.Add(div);
            //dialogHolder.Controls.Add(div);

            var dialogId = div.ClientID;
            var gridId = uc.FindControl("flex").ClientID;
            var dataUrl = DataPageUrl + "?LookupSource=" + ItemSource ;
            var resultsId = this.ClientID.Replace(this.ID, BuddyControl); //txtResults.ClientID;
            var selectionId = uc.FindControl("txtSelection").ClientID ;
             
            btnLookup.Attributes.Add("onclick", JSHelper.BuildShowDialogJS(dialogId, gridId, dataUrl));
            
            //add documentreadyscript
            Page.ClientScript.RegisterClientScriptBlock(
                typeof(Page),string.Format( "JS_{0}",this.ClientID),
              JSHelper.BuildCreateDialogJS(dialogId, resultsId, selectionId));
        }

        private HtmlGenericControl CreateDivforDialog()
        {
            var div = new HtmlGenericControl("DIV") { ID = "dialog" };
            div.Attributes.Add("class", "dialog");
            div.Attributes.Add("title", Title);
            return div;
        }

        private LookupList CreateLookupListControl()
        {
            var uc=(LookupList)LoadControl(this.TemplateSourceDirectory + "/LookupList.ascx");
            uc.ID = "lookuplist";
            uc.Attributes.Add("runat","server");
            uc.HiddenIds=FindControl("hiddenids").ClientID;
            return uc;
        }
    }
}
