﻿using System.Text;

namespace ScholarBridge.Web.Common.Lookup
{
    public static class LookupDialogJSHelper
    {
        public static string BuildCreateDialogJS(string dialogId, string resultsId, string selectionId)
        {
            var sb = new StringBuilder();
            sb.Append("<script language='javascript'> $(function() {");
            sb.AppendFormat("createDialog('#{0}','#{1}','#{2}');",  dialogId , resultsId, selectionId);
            sb.Append("}); </script>");
            return sb.ToString();
        }

        public static string BuildShowDialogJS(string dialogId, string gridId, string dataUrl)
        {
            return string.Format("JavaScript: return showDialog('#{0}','#{1}','{2}');", dialogId, gridId, dataUrl);
        }
        public static string BuildGetSelectionsJS(string selectionId,string gridid,string hiddenid)
        {
            return string.Format("getSelections('#{0}','#{1}','#{2}');", selectionId, gridid,hiddenid);
        }
    }
}
