﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Data;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Web.Common.Lookup
{
    public class ExampleColorsLookupItemSource : ILookupDAL
    {
        public IList<ILookup> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public ILookup FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public  List<string> GetLookupItems()
         {
             var colorlist = Enum.GetNames(typeof(System.Drawing.KnownColor));
             return  colorlist.ToList<string>();
         }

        #region ILookupDAL Members


        List<KeyValuePair<string, string>> ILookupDAL.GetLookupItems()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
