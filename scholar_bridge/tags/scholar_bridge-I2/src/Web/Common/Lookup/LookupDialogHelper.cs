﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ScholarBridge.Data;

namespace ScholarBridge.Web.Common.Lookup
{
    public class LookupDialogHelper
    {
        public int PageNo { get; set; }
        public int NumberofRows { get; set; }
        public string SortOn { get; set; }
        public string SortOrder { get; set; }
        public string SearchOn { get; set; }
        public string SearchValue { get; set; }
        public ILookupDAL LookupSource { get; set; }
        public LookupDialogHelper(ILookupDAL lookupSource, int pageNo, int numberofRows, string sortOn, string sortOrder, string searchOn, string searchValue)
        {
            PageNo = pageNo;
            NumberofRows = numberofRows;
            SortOn = sortOn;
            SortOrder = sortOrder;
            SearchOn = searchOn;
            SearchValue = searchValue;
            LookupSource = lookupSource;

        }

        

        public virtual string GetXml()
        {
            var list = LookupSource.GetLookupItems();
            if (list == null) throw new ArgumentNullException("list");

            //see if need to filter
            if (!string.IsNullOrEmpty(SearchValue))
                 list = FilterItems( list);


            list = SortOrder == "desc" ? list.OrderByDescending(item => item.Value).ToList() : list.OrderBy(item => item.Value).ToList();
            

            var totalRecords = list.Count;

            var start = (PageNo - 1) * NumberofRows;
            var end = NumberofRows;
            if ((start + NumberofRows) > totalRecords)
            {
                end = totalRecords - start;

            }
            list = list.GetRange(start, end);

            // Generating XML Data
            var xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("rows",
                        new XElement("page", PageNo.ToString()),
                        new XElement("total", totalRecords),
                        list.Select(row => new XElement("row", new XAttribute("id", row.Key.ToString()),
                                                          new XElement("cell", row.Value.ToString())
                                                         )
                                    )
                        )
            );
            return xmlDoc.ToString();
        }

        protected virtual List<KeyValuePair<string, string>> FilterItems(List<KeyValuePair<string, string>> list)
        {
            var qry = from i in list where i.Value.ToUpper().StartsWith(SearchValue.ToUpper()) select i;
            return qry.ToList();
        }
    }
}
