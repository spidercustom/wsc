﻿using System;
using Spring.Web.UI;
using JSHelper = ScholarBridge.Web.Common.Lookup.LookupDialogJSHelper;
namespace ScholarBridge.Web.Common.Lookup
{
    public partial class LookupList : UserControl
    {
        public string HiddenIds { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            var selectionId = txtSelection.ClientID;
            var gridId =  FindControl("flex").ClientID;

            btnAdd.Attributes.Add("onclick", JSHelper.BuildGetSelectionsJS(selectionId, gridId, HiddenIds));
        }
    }
}
