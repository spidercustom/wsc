﻿using System;
using System.Web.UI;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class EditUserName : UserControl
    {
        public event UserSaved UserSaved;
        public event FormCanceled FormCanceled;

        public IUserDAL UserService { get; set; }
        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (null == CurrentUser)
                {
                    errorMessage.Visible = true;
                    return;
                }

                FirstName.Text = CurrentUser.Name.FirstName;
                MiddleName.Text = CurrentUser.Name.MiddleName;
                LastName.Text = CurrentUser.Name.LastName;
            }
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            CurrentUser.Name.FirstName = FirstName.Text;
            CurrentUser.Name.MiddleName = MiddleName.Text;
            CurrentUser.Name.LastName = LastName.Text;
            UserService.Update(CurrentUser);

            if (null != UserSaved)
            {
                UserSaved(CurrentUser);
            }
        }

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}