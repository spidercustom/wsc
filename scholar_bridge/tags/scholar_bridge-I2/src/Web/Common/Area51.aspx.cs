﻿using System;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Web.Common
{
    public partial class Area51 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                flagEnum1.DatasourceEnumTypeName = typeof(StudentGroups).AssemblyQualifiedName;
                flagEnum1.DataBind();
                flagEnum1.SelectAll();
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            textBox.Text = flagEnum1.SelectedValues.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            flagEnum1.SelectedValues = Int32.Parse(textBox.Text);
        }
    }
}
