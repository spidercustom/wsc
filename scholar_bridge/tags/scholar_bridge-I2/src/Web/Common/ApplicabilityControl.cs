﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ApplicableControl runat=server></{0}:ApplicableControl>")]
    [Designer("System.Web.UI.Design.WebControls.PanelContainerDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [ParseChildren(false), PersistChildren(true)]
    public class ApplicabilityControl : Panel, INamingContainer
    {
        private const string SET_DEFAULT_CHECK_STATE =
            "$(document).ready(function() {{ EnableDisableControls('{0}', '{1}', {2}); }});";

        public ApplicabilityControl()
        {
            DefaultCheckState = true;
        }

        private CheckBox controllerCheckBox;
        protected CheckBox ControllerCheckBox
        {
            get
            {
                if (controllerCheckBox == null)
                {
                    controllerCheckBox = new CheckBox
                                          {
                                              ID = "controllerCheckBox",
                                              Checked = DefaultCheckState,
                                          };
                    controllerCheckBox.Text = Text;
                }
                return controllerCheckBox;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            ControllerCheckBox.Attributes.Add(
                "onclick",
                string.Format("javascript:EnableDisableControls('{0}', '{1}', {2})", ClientID, ControllerCheckBox.ClientID, EnableOnCheck.ToString().ToLower()));
            base.Render(writer);
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("N/A")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        public bool DefaultCheckState { get; set; }
        public bool AddBRAfterCheckBox { get; set; }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            Controls.Add(ControllerCheckBox);
            if (null != Page)
            {
                Page.ClientScript.RegisterClientScriptBlock(
                    GetType(), 
                    ClientID,
                    string.Format(SET_DEFAULT_CHECK_STATE, ClientID, ControllerCheckBox.ClientID, EnableOnCheck.ToString().ToLower()), true);
            }
            if (AddBRAfterCheckBox)
            {
                var newLineLiteral = new LiteralControl("<br/>");
                Controls.AddAt(1, newLineLiteral);
            }
        }

        public bool EnableOnCheck { get; set; }
    }
}
