﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserForm.ascx.cs" Inherits="ScholarBridge.Web.Common.UserForm" %>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<asp:Label ID="errorMessage" runat="server" Visible="false" CssClass="errorMessage"/>

<label for="FirstName">First Name:</label>
<asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" />
<br />
<label for="MiddleName">Middle Name:</label>
<asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName" />
<br />
<label for="LastName">Last Name:</label>
<asp:TextBox ID="LastName" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" />
<br />
<label for="UserName">Email Address:</label>
<asp:TextBox ID="UserName" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" />
<asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
<br />
<label for="ConfirmEmail">Confirm Email Address:</label>
<asp:TextBox ID="ConfirmEmail" runat="server"></asp:TextBox>
<asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" 
    ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address." />
<br />


<asp:Button ID="saveBtn" runat="server" Text="Save" onclick="saveBtn_Click" /> 
<asp:Button ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelBtn_Click" />