﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NumberRangeConditionControl.ascx.cs" Inherits="ScholarBridge.Web.Common.NumberRangeConditionControl" %>
<label for="GreaterThanControl">Greater than:</label>
<asp:TextBox ID="GreaterThanControl" runat="server"/>
<asp:CustomValidator ID="GreaterThanControlValidator" runat="server" 
  ErrorMessage="" Text="" 
  onservervalidate="GreaterThanControlValidator_ServerValidate" ></asp:CustomValidator>
<br />

<label for="LessThanControl">Less than:</label>
<asp:TextBox ID="LessThanControl" runat="server"/>
<asp:CustomValidator ID="LessThanControlValidator" runat="server" 
  ErrorMessage="" Text="" 
  onservervalidate="LessThanControlValidator_ServerValidate" ></asp:CustomValidator>
<br />
