﻿using System;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Web.Common
{
    public partial class DayByWeek : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private ByWeekDay weekDay;
        public ByWeekDay WeekDay
        {
            get
            {
                if (null == weekDay)
                    weekDay = new ByWeekDay();

                weekDay.WeekDayOccurrence = (WeekDayOccurrencesInMonth) WeekDayOccurrenceControl.SelectedIndex;
                weekDay.DayOfWeek = (DayOfWeek)DayOfWeekControl.SelectedIndex;
                weekDay.Month = (MonthOfYear)MonthControl.SelectedIndex;

                return weekDay;
            }
            set
            {
                weekDay = value;

                WeekDayOccurrenceControl.SelectedIndex = (int)weekDay.WeekDayOccurrence;
                DayOfWeekControl.SelectedIndex = (int) weekDay.DayOfWeek;
                MonthControl.SelectedIndex = (int) weekDay.Month;
            }
        }
    }
}