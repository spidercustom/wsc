﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ScholarBridge.Web.Login" Title="Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:Login ID="Login1" runat="server" UserNameLabelText="Email Address" PasswordRecoveryUrl="~/ForgotPassword.aspx" PasswordRecoveryText="Forgot Your Password?">
    </asp:Login>
    <asp:HyperLink ID="reconfirmLnk" runat="server" NavigateUrl="~/Reconfirm.aspx">Resend Confirmation Email?</asp:HyperLink>
</asp:Content>
