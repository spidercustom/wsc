﻿function createDialog(dialogdiv, inputid,outputid) {
    // Dialog
    //alert('in create dialog');
    var xwidth = $(dialogdiv).width();
    var xheight = $(dialogdiv).height();
    $(dialogdiv).dialog({
        autoOpen: false,
        width: xwidth,
        height: xheight,
        modal: true,
        resizable: false,
        buttons: {
            "Ok": function() {
                $(this).dialog("close"); 
                $(inputid).val($(outputid).val());
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });
   
}

function showDialog(dialogdiv, gridid, dataurl) {
   createGrid(gridid, dataurl);
   $(dialogdiv).dialog('open');
   return false;
}

//Grid functions
function createGrid(gridid, dataurl) {
   
   $(gridid).flexigrid
       ({
            url: dataurl,
            dataType: 'xml',
            colModel: [ { display: 'Name', name: 'name', width: 500, sortable: true, align: 'left' } ],
            searchitems: [ { display: 'Name', name: 'name', isdefault: true } ],
            sortname: "name",
            sortorder: "asc",
            usepager: true,
            //title: 'Known Colors',
            useRp: false,
            rp: 15,
            showTableToggleBtn: true 
        });
}
   

function getSelections(selectionId, gridid,hiddenids) {
    
    var ids = $(document).find(hiddenids).val();
    var names = $(document).find(selectionId).val();
    var items = $('.trSelected');
    var itemlist = '';

    for (i = 0; i < items.length; i++) {
        var s = items[i].id.substr(3)+", ";
        if (ids.indexOf(s)==-1) {

            
               //get ids
               if (ids.length == 0)
                   ids = items[i].id.substr(3);
               else
                   ids = ids + ", " + items[i].id.substr(3);
               
               //get names
               if (names.length == 0)
                   names = items[i].cells[0].textContent;
               else
                   names = names + ", " + items[i].cells[0].textContent;
           }

       }
       
       $(document).find(hiddenids).val(ids);
    $(document).find(selectionId).val(names);
    //deselect all selected items
    $('.bDiv tbody tr').removeClass('trSelected');
} 