﻿$(document).ready(function() {
    // convert print divs to clickable things that cause a print to occur
    $(".print").mouseup(function() {
        window.print();
    }).hover(
        function() { $(this).attr("style", "cursor: pointer;"); },
        function() { $(this).attr("style", "cursor: default;"); }
    );

    // close a parent element
    $(".closeBox").click(function(event) {
        $(this).parent().fadeOut();
    }).hover(
        function() { $(this).attr("style", "cursor: pointer;"); },
        function() { $(this).attr("style", "cursor: default;"); }
    );
    
    // make table class="sortableTable" sortable
    $(".sortableTable").tablesorter(); 
    
    // make div class="tabs" int tabs
    $(".tabs").tabs();
});

function SelectAllCheckBoxIn(parentId) {
    CheckUncheckCheckBoxesIn(parentId, true);
}

function DeselectAllCheckBoxIn(parentId) {
    CheckUncheckCheckBoxesIn(parentId, false);
}

function CheckUncheckCheckBoxesIn(parentId, checkState) {
    $("#" + parentId + " > *").find("input:checkbox").each(function() {
        this.checked = checkState;
    });
}

function EnableDisableControls(parentId, controllerCheckBoxId, enableOnCheck) {
    var disableElements = enableOnCheck;
    if ($('#' + controllerCheckBoxId).is(':checked'))
        disableElements = !enableOnCheck;
    $("#" + parentId + " :input").filter(function(index) {
        return $(this).attr("id") != controllerCheckBoxId;
    }).attr('disabled', disableElements);
}
