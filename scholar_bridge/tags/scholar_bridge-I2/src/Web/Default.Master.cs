﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web
{
    public partial class DefaultMasterPage : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "ScholarBridge | " + Page.Header.Title;
        }
    }
}
