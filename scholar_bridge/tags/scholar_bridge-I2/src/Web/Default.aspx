﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" 
Inherits="ScholarBridge.Web.DefaultPage" Title="Welcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<h2>Welcome to ScholarBridge</h2>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


<asp:HyperLink ID="providerLnk" runat="server" NavigateUrl="~/Provider/Default.aspx">Scholarship Providers</asp:HyperLink>
<br />
<asp:HyperLink ID="intermediaryLnk" runat="server" NavigateUrl="~/Intermediary/Default.aspx">Scholarship Intermediaries</asp:HyperLink>

</asp:Content>