﻿using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Net.Configuration;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Config
{
    /// <summary>
    /// Contains common methods for reading sections from Web.Config
    /// </summary>
    public static class ConfigHelper
    {
		/// <summary>
		/// Gets the url of the Proxy website - needed when running behind the DIS Fortress Firewall
		/// </summary>
		public static string ProxyURL
		{
			get { return GetAppSettingValue("ProxyURL"); }
		}

        /// <summary>
        /// Gets the HECB admin email from config file.
        /// </summary>
        /// <returns></returns>
        public static string GetHECBAdminEmail()
        {
            return GetAppSettingValue("HECBAdminEmail");
        }

        /// <summary>
        /// Gets the HECB admin email from config file.
        /// </summary>
        /// <returns></returns>
        public static string GetEmailTemplatesDirectory()
        {
            return GetAppSettingValue("EmailsDirectory");
        }

        /// <summary>
        /// Gets the SMTP section from web.config file.
        /// </summary>
        /// <returns></returns>
        public static SmtpSection GetSmtpSection()
        {
            var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            var section = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
            return section.Smtp;
        }

        /// <summary>
        /// Gets the app setting value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string GetAppSettingValue(string key)
        {
            var reader = new AppSettingsReader();
            return reader.GetValue(key, typeof(string)).ToString();
        }

        public static MailTemplateParams GetMailParams(string to)
        {
            return new MailTemplateParams
                       {
                           TemplateDirectory = GetEmailTemplatesDirectory(),
                           From = GetSmtpSection().From,
                           To = to
                       };
        }

		/// <summary>
		/// Generates a correct url for inclusion in emails or other non-web documents (to a specific web page)
		/// </summary>
		/// <param name="relativePathToPage">relative path to a specific web page</param>
		/// <returns></returns>
		public static string GetPageURLViaProxy(string relativePathToPage)
		{
		    return GetPageURLViaProxy(relativePathToPage, null);
		}

        /// <summary>
		/// Generates a correct url for inclusion in emails or other non-web documents (to a specific web page)
		/// </summary>
		/// <param name="relativePathToPage">relative path to a specific web page</param>
		/// <param name="queryString">query string values</param>
		/// <returns></returns>
		public static string GetPageURLViaProxy(string relativePathToPage, string queryString)
		{
            if (! ShouldUseProxyURL())
				return BuildURLFromContext(relativePathToPage, queryString);

            return BuildUrl(ProxyURL.Trim(), relativePathToPage, queryString);
		}

		private static string BuildURLFromContext(string path, string queryString)
		{
		    string fullPath = BuildRelativePath(HttpContext.Current.Request.ApplicationPath, path);
		    return BuildUrl(HttpContext.Current.Request.Url.ToString(), fullPath, queryString);
		}

        private static string BuildRelativePath(string applicationPath, string path)
        {
            if (! applicationPath.EndsWith("/") && ! path.StartsWith("/"))
                applicationPath += "/";
            return String.Format("{0}{1}", applicationPath, path);
        }

        private static string BuildUrl(string baseUrl, string relativePath, string queryString)
        {
            var builder = new UriBuilder(baseUrl) { Query = queryString, Path = relativePath };
            return builder.Uri.AbsoluteUri;
        }

        private static bool ShouldUseProxyURL()
        {
            return ProxyURL != null && ProxyURL.Trim() != string.Empty;
        }
    }
}
