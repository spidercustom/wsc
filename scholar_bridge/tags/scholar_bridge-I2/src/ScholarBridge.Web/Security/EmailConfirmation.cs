using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Security
{
    public class EmailConfirmation
    {
        private const string RESET = "reset=true";

        public void SendConfirmationEmail(ITemplatedMailerService mailerService, User user)
        {
            SendConfirmationEmail(mailerService, user, false);
        }

        public void SendConfirmationEmail(ITemplatedMailerService mailerService, User user, bool requiredToSetPassword)
        {
            string link = BuildConfirmationLinkForEmail(user.Email, requiredToSetPassword);
            var dict = new Dictionary<string, string> { { "Name", user.Name.ToString() }, { "Link", link } };
            mailerService.SendMail(MailTemplate.ConfirmationLink, dict, ConfigHelper.GetMailParams(user.Email));
        }

        public string BuildConfirmationLinkForEmail(string email)
        {
            return BuildConfirmationLinkForEmail(email, false);
        }

        public string BuildConfirmationLinkForEmail(string email, bool requiredToSetPassword)
        {
            var linkData = email;
            if (requiredToSetPassword)
            {
                linkData += "&" + RESET;
            }
            var queryString = String.Format("key={0}", HttpUtility.UrlEncode(CryptoHelper.Encrypt(linkData)));
        	return ConfigHelper.GetPageURLViaProxy("ConfirmEmail.aspx", queryString);
        }

        public string GetEmailAddress(string encryptedParam, out bool resetPassword)
        {
            var decrypted = CryptoHelper.Decrypt(encryptedParam);
            if (decrypted.Contains("&"))
            {
                var parts = decrypted.Split(new[] {"&"}, StringSplitOptions.RemoveEmptyEntries);
                resetPassword = 2 == parts.Length && RESET == parts[1];
                return parts[0];
            }

            resetPassword = false;
            return decrypted;
        }

        public string HashPassword(string password, string salt)
        {
            var hash = new HMACSHA1 { Key = HexToByte(salt) };
            return Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
        }

        private byte[] HexToByte(string hexString)
        {
            var bytes = new byte[hexString.Length / 2 + 1];
            for (var i = 0; i <= hexString.Length / 2 - 1; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }
            return bytes;
        }
    }
}