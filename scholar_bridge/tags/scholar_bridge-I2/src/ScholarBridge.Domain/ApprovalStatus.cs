namespace ScholarBridge.Domain
{
    public enum ApprovalStatus
    {
        None,
        PendingApproval,
        Approved,
        Rejected
    }

    /// <summary>
    /// Useful methods for dealing with <c>ApprovalStatus</c>. Really a workaround for
    /// enums being simple integer values in .NET.
    /// </summary>
    public static class ApprovalStatusExtension
    {
        /// <summary>
        /// Create a simple English name for the Provider Status.
        /// </summary>
        /// <param name="status">The ApprovalStatus that you want the display name for.</param>
        /// <returns>A human readable version of the ApprovalStatus.</returns>
        public static string ToDisplayName(this ApprovalStatus status)
        {
            switch (status)
            {
                case ApprovalStatus.PendingApproval:
                    return "Pending Approval";
                case ApprovalStatus.Approved:
                    return "Approved";
                case ApprovalStatus.Rejected:
                    return "Rejected";
                default:
                    return "None";
            }
        }
    }
}