﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain.Auth
{
    public class User : ISoftDeletable
    {
        private PersonName name = new PersonName();

        public User()
        {
            Roles = new List<Role>();
        }

        public virtual IList<Role> Roles { get; protected set; }

//        public virtual IList<Privilege> Privileges { get; set; }

        public virtual int Id { get; set; }

        [NotNullValidator]
        [StringLengthValidator(1, 255)]
        public virtual String Question { get; set; }

        [StringLengthValidator(1, 255)]
        public virtual string Answer { get; set; }

        [PasswordValidator(Regex = @"(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", MessageTemplate = "Password must contain at least 1 upper case character and either a number or special character.")]
        [StringLengthValidator(8, 20)]
        public virtual string Password { get; set; }

        [UniqueProperty]
        [StringLengthValidator(6, RangeBoundaryType.Inclusive, 50, RangeBoundaryType.Inclusive)]
        [NotNullValidator]
        public virtual string Username { get { return Email; } set { Email = value; } }

        [NotNullValidator]
        [EmailValidatorAttribute]
        public virtual string Email { get; set; }

        public virtual PersonName Name { get { return name; } set{ name = value;} }
        
        public virtual string Comments {get; set;}
        public virtual int PasswordFormat {get; set;}
        public virtual string PasswordSalt {get; set;}

        public virtual bool  IsApproved {get; set;}
        public virtual bool  IsLockedOut {get; set;}
        public virtual bool  IsActive {get; set;}
        public virtual bool  IsDeleted {get; set;}
        public virtual bool  IsPasswordReset {get; set;}
        public virtual bool  IsOnline {get; set;}

        public virtual DateTime?  CreationDate {get; set;}
        public virtual DateTime?  LastActivityDate {get; set;}
        public virtual DateTime?  LastLoginDate {get; set;}
        public virtual DateTime?  LastLockedOutDate {get; set;}
        public virtual DateTime?  LastPasswordChangeDate {get; set;}
        
        public virtual DateTime?  FailedPasswordAttemptWindowStart {get; set;}
        public virtual DateTime?  FailedPasswordAnswerAttemptWindowStart {get; set;}
        public virtual int  FailedPasswordAttemptCount {get; set;}
        public virtual int  FailedPasswordAnswerAttemptCount {get; set;}

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual bool IsInRole(string roleName)
        {
            return Roles.Any(r => r.Name == roleName);
        }
    }
}