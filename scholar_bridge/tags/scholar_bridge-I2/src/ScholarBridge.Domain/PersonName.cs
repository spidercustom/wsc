﻿using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain
{
    public class PersonName
    {

        [StringLengthValidator(1, 40)]
        public virtual string FirstName { get; set; }

        [StringLengthValidator(1, 40)]
        public virtual string LastName { get; set; }

        [StringLengthValidator(0, 40)]
        public virtual string MiddleName { get; set; }

        public string NameFirstLast
        {
            get
            {
                var result = new StringBuilder();
                result.Append(FirstName);
                result.Append(' ');
                if (!string.IsNullOrEmpty(MiddleName))
                {
                    result.Append(MiddleName);
                    result.Append(' ');
                }
                result.Append(LastName);
                return result.ToString();
            }
        }

        public string NameLastFirst
        {
            get
            {
                var result = new StringBuilder();
                result.Append(LastName);
                result.Append(", ");
                result.Append(FirstName);
                if (!string.IsNullOrEmpty(MiddleName))
                {
                    result.Append(' ');
                    result.Append(MiddleName);
                }
                return result.ToString();
            }
        }

        public override string ToString()
        {
            return NameFirstLast;
        }
    }
}
