﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum ProgramLengths
    {
        [DisplayName("2 years")]
        TwoYears = 1,
        [DisplayName("4 years")]
        FourYears = 2
    }
}
