﻿using System;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum Genders
    {
        Male = 1,
        Female = 2
    }
}
