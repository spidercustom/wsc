﻿namespace ScholarBridge.Domain.Lookup
{
    public enum Gender
    {
        Male,
        Female
    }
}
