﻿namespace ScholarBridge.Domain.Lookup
{
    public interface ILookup : IDeprecatable
    {
        int Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        ActivityStamp LastUpdate { get; set; }
    }
}
