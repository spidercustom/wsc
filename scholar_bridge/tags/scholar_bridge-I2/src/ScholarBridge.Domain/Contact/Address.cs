using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Location;

namespace ScholarBridge.Domain.Contact
{
    public class Address
    {
        [StringLengthValidator(0, 50, MessageTemplate = "Street must be less than 50 characters.")]
        public virtual string Street { get; set; }

        [StringLengthValidator(0, 50, MessageTemplate = "Street2 must be less than 50 characters.")]
        public virtual string Street2 { get; set; }

        [StringLengthValidator(0, 50, MessageTemplate = "City must be less than 50 characters.")]
        public virtual string City { get; set; }

        [StringLengthValidator(0, 10, MessageTemplate = "Postal Code must be less than 10 characters.")]
        public virtual string PostalCode { get; set; }

        public virtual State State { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (null != Street)
            {
                sb.AppendLine(Street);
            }

            if (null != Street2)
            {
                sb.AppendLine(Street2);
            }

            if (null != City)
            {
                sb.Append(City);
                if (null != State)
                {
                    sb.Append(", ");
                }
            }

            if (null != State)
            {
                sb.Append(State.Abbreviation);
            }

            if (null != PostalCode)
            {
                sb.Append(" ").Append(PostalCode);
            }

            return sb.ToString();
        }
    }
}