using Spring.Testing.NUnit;

namespace ScholarBridge.Data.Tests
{
    public abstract class TestBase : AbstractTransactionalDbProviderSpringContextTests
    {
        protected override string[] ConfigLocations
        {
            get
            {
                return new string[] 
                    { 
                        "assembly://ScholarBridge.Data/ScholarBridge.Data.NHibernate/DAL.xml"
                    };
            }
        }
    }
}