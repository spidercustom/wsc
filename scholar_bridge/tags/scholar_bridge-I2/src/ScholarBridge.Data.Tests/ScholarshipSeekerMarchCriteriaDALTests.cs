﻿using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipSeekerMarchCriteriaDALTests : TestBase
    {
        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }
        public ScholarshipStageDAL ScholarshipStageDAL { get; set; }

        private User user;
        private Provider provider;
        private ScholarshipStage stage;
        private Scholarship scholarship;
        public override void SetUp()
        {
            base.SetUp();
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            stage = ScholarshipStageDAL.FindById(ScholarshipStages.GeneralInformation);

            scholarship = ScholarshipDALTest.CreateTestObject(user, provider, stage);
        }

        [Test]
        public void test_enums_gets_save_and_load()
        {
            const StudentGroups studentGroups = StudentGroups.AdultFirstTime | StudentGroups.HighSchoolSenior;
            const SchoolTypes schoolTypes = SchoolTypes.PublicUniversity | SchoolTypes.CommunityCollege;
            const AcademicPrograms academicPrograms = AcademicPrograms.AdvancedDegree | AcademicPrograms.Undergraduate;
            const SeekerStatuses seekerStatuses = SeekerStatuses.PartTime | SeekerStatuses.FullTime;
            const ProgramLengths programLengths = ProgramLengths.FourYears | ProgramLengths.TwoYears;
            const Genders selectedGenders = Genders.Male | Genders.Female;

            scholarship.SeekerMatchCriteria.StudentGroups = studentGroups;
            scholarship.SeekerMatchCriteria.SchoolTypes = schoolTypes;
            scholarship.SeekerMatchCriteria.AcademicPrograms = academicPrograms;
            scholarship.SeekerMatchCriteria.SeekerStatuses = seekerStatuses;
            scholarship.SeekerMatchCriteria.ProgramLengths = programLengths;
            scholarship.SeekerMatchCriteria.Genders = selectedGenders;

            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            
            Assert.AreEqual(selectedGenders, scholarship.SeekerMatchCriteria.Genders);
            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.AreEqual(studentGroups, scholarship.SeekerMatchCriteria.StudentGroups);
            Assert.AreEqual(schoolTypes,scholarship.SeekerMatchCriteria.SchoolTypes);
            Assert.AreEqual(academicPrograms,scholarship.SeekerMatchCriteria.AcademicPrograms);
            Assert.AreEqual(seekerStatuses,scholarship.SeekerMatchCriteria.SeekerStatuses);
            Assert.AreEqual(programLengths,scholarship.SeekerMatchCriteria.ProgramLengths);
            Assert.AreEqual(selectedGenders, scholarship.SeekerMatchCriteria.Genders);
        }

        [Test]
        public void test_words_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<SeekerVerbalizingWord>("word-1");

            scholarship.SeekerMatchCriteria.Words.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.Words);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.Words.Count);
        }
        [Test]
        public void test_SeekerSkill_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<SeekerSkill>("SeekerSkill");

            scholarship.SeekerMatchCriteria.Skills.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.Skills);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.Skills.Count);
        }
        [Test]
        public void test_College_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<College>("College");

            scholarship.SeekerMatchCriteria.Colleges.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.Colleges);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.Colleges.Count);
        }
        [Test]
        public void test_Ethnicity_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<Ethnicity>("Ethnicity");

            scholarship.SeekerMatchCriteria.Ethnicities.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.Ethnicities);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.Ethnicities.Count);
        }

        [Test]
        public void test_Religion_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<Religion>("Religion");

            scholarship.SeekerMatchCriteria.Religions.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.Religions);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.Religions.Count);
        }

        [Test]
        public void test_County_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<County>("County");

            scholarship.SeekerMatchCriteria.Counties.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.Counties);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.Counties.Count);
        }

        [Test]
        public void test_City_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<City>("City");

            scholarship.SeekerMatchCriteria.Cities.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.Cities);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.Cities.Count);
        }

        [Test]
        public void test_SchoolDistrict_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<SchoolDistrict>("SchoolDistrict");

            scholarship.SeekerMatchCriteria.SchoolDistricts.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.SchoolDistricts);
            Assert.AreEqual(1, retrivedScholarship.SeekerMatchCriteria.SchoolDistricts.Count);
        }


        private T CreateLookupObjectOfType<T>(string name) where T : ILookup
        {
            var word = LookupTestBase<LookupDAL<T>, T>.CreateLookupObject(name, user);
            var dal = LookupTestBase<LookupDAL<T>, T>.RetrieveDAL();
            dal.Insert(word);

            return word;
        }

        [Test]
        public void test_organization_gets_saved_and_load()
        {
            var organization = LookupTestBase<LookupDAL<SeekerMatchOrganization>, SeekerMatchOrganization>.CreateLookupObject("org-1", user);
            var dal = LookupTestBase<LookupDAL<SeekerMatchOrganization>, SeekerMatchOrganization>.RetrieveDAL();
            dal.Insert(organization);

            scholarship.SeekerMatchCriteria.Organizations.Add(organization);

            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.IsNotNull(scholarship.SeekerMatchCriteria.Organizations);
            Assert.AreEqual(1, scholarship.SeekerMatchCriteria.Organizations.Count);
        }


        [Test]
        public void test_gpa_is_null_when_not_set()
        {
            Assert.IsNull(scholarship.SeekerMatchCriteria.GPA);
            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNull(retrivedScholarship.SeekerMatchCriteria.GPA);
        }

        [Test]
        public void test_gpa_is_not_null_when_set()
        {
            scholarship.SeekerMatchCriteria.GPA  = new RangeCondition<int> {Minimum = 10, Maximum = 100};
            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.GPA);
            AssertRangeEqual(scholarship.SeekerMatchCriteria.GPA, retrivedScholarship.SeekerMatchCriteria.GPA);
        }

        public static void AssertRangeEqual(RangeCondition<int> expected, RangeCondition<int> actual)
        {
            Assert.AreEqual(expected.Minimum, actual.Minimum);
            Assert.AreEqual(expected.Maximum, actual.Maximum);
        }

        [Test]
        public void test_sat_is_null_when_not_set()
        {
            Assert.IsNull(scholarship.SeekerMatchCriteria.SATScore);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNull(retrivedScholarship.SeekerMatchCriteria.SATScore);
        }

        [Test]
        public void test_sat_is_retrived_when_set()
        {
            Assert.IsNull(scholarship.SeekerMatchCriteria.SATScore);
            scholarship.SeekerMatchCriteria.SATScore = new SatScore();
            Assert.IsNotNull(scholarship.SeekerMatchCriteria.SATScore.Writing);
            Assert.IsNotNull(scholarship.SeekerMatchCriteria.SATScore.Mathematics);
            Assert.IsNotNull(scholarship.SeekerMatchCriteria.SATScore.CriticalReading);
            scholarship.SeekerMatchCriteria.SATScore.Writing.Minimum = 1;
            scholarship.SeekerMatchCriteria.SATScore.Writing.Maximum = 2;
            scholarship.SeekerMatchCriteria.SATScore.Mathematics.Minimum = 3;
            scholarship.SeekerMatchCriteria.SATScore.Mathematics.Maximum = 4;
            scholarship.SeekerMatchCriteria.SATScore.CriticalReading.Minimum = 5;
            scholarship.SeekerMatchCriteria.SATScore.CriticalReading.Maximum = 6;

            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.SATScore);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.SATScore.Writing);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.SATScore.Mathematics);
            Assert.IsNotNull(retrivedScholarship.SeekerMatchCriteria.SATScore.CriticalReading);
            Assert.AreEqual(1, scholarship.SeekerMatchCriteria.SATScore.Writing.Minimum);
            Assert.AreEqual(2, scholarship.SeekerMatchCriteria.SATScore.Writing.Maximum);
            Assert.AreEqual(3, scholarship.SeekerMatchCriteria.SATScore.Mathematics.Minimum);
            Assert.AreEqual(4, scholarship.SeekerMatchCriteria.SATScore.Mathematics.Maximum);
            Assert.AreEqual(5, scholarship.SeekerMatchCriteria.SATScore.CriticalReading.Minimum);
            Assert.AreEqual(6, scholarship.SeekerMatchCriteria.SATScore.CriticalReading.Maximum);
        }

    }
}
