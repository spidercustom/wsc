using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class ScholarshipAssociationsTests : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }
        public ScholarshipStageDAL ScholarshipStageDAL { get; set; }

        public AdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
        public SupportDAL SupportDAL { get; set; }
        public NeedGapDAL NeedGapDAL { get; set; }
        public TermOfSupportDAL TermOfSupportDAL { get; set; }

        [Test]
        public void can_associate_additional_reqs_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void can_associate_need_gaps_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.Need.ResetNeedGaps(NeedGapDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void can_associate_support_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void can_associate_terms_of_support_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.FundingParameters.ResetTermsOfSupport(TermOfSupportDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        private Scholarship BuildScholarship()
        {
            var u = UserDAL.FindByUsername("admin");
            var p = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "test", u);
            var s = InsertScholarship(ScholarshipDAL, ScholarshipStageDAL, p, u);

            Assert.IsNotNull(s);
            return s;
        }

        public static Scholarship InsertScholarship(ScholarshipDAL scholarshipDal, ScholarshipStageDAL stageDal, Provider provider, User user)
        {
            var s = new Scholarship
                                     {
                                         Name = "Test Scholarship 1",
                                         MissionStatement = @"Long big text to describe mission",
                                         Schedule = ScholarshipDALTest.CreateScheduleByMonthDay(),
                                         Provider = provider,
                                         LastUpdate = new ActivityStamp(user),
                                         Stage = stageDal.FindById(ScholarshipStages.GeneralInformation)
                                     };

            return scholarshipDal.Save(s);
        }
    }
}