﻿using System;
using System.Text;
using Migrator.Framework;
using System.Collections.Generic;

namespace ScholarBridge.Migrations.Common
{
    public enum SufixType
    {
        Lookup,
        Relation
    }

    public abstract class AddTableBase : Migration
    {

        static AddTableBase()
        {
            KnownSuffixes = new Dictionary<SufixType, string>();
            KnownSuffixes.Add(SufixType.Lookup, "LUT");
            KnownSuffixes.Add(SufixType.Relation, "RT");
        }

        public static bool IsLookupTable(string tableName)
        {
            return tableName.EndsWith(KnownSuffixes[SufixType.Lookup]);
        }

        public static Dictionary<SufixType, string> KnownSuffixes { get; private set; }
        public static string GetPlainTableName(string tableName)
        {
            var result = tableName;
            if (tableName.StartsWith(TABLE_PREFIX))
                result = tableName.Substring(TABLE_PREFIX.Length);
            return GetNonSuffixedTableName(result);
        }

        public static string GetNonSuffixedTableName(string result)
        {
            foreach (var suffix in KnownSuffixes.Values)
            {
                if (result.EndsWith(suffix))
                {
                    result = result.Substring(0, result.Length - suffix.Length);
                    //table can have only one suffix
                    break;
                }
            }
            return result;
        }

        public static string PrepareTablePrimaryKeyColumnName(string tableName)
        {
            var result = new StringBuilder();
            result.Append(GetNonSuffixedTableName(tableName));
            if (IsLookupTable(tableName))
                result.Append("Index");
            else
                result.Append("ID");
            return result.ToString();
        }

        public const string TABLE_PREFIX = "SB";
        public abstract string TableName { get ;}
        public abstract string PrimaryKeyColumn { get; }
        public abstract Column[] CreateColumns();
        public abstract ForeignKey[] CreateForeignKeys();

        private Column[] columns;
        public virtual Column[] Columns
        {
            get
            {
                if (columns == null)
                    columns = CreateColumns();
                return columns;
            }
        }

        ForeignKey[] foreignKeys;
        public virtual ForeignKey[] ForeignKeys
        {
            get
            {
                if (foreignKeys == null)
                    foreignKeys = CreateForeignKeys();
                return foreignKeys;
            }
        }

        public ForeignKey CreateForeignKey(string name, string foreignColumn, AddTableBase primaryTable)
        {
            return new ForeignKey(name, this, foreignColumn, primaryTable);
        }

        public ForeignKey CreateForeignKey(string foreignColumn, AddTableBase primaryTable)
        {
            return new ForeignKey(
                string.Format("FK_{0}_{1}", TableName, foreignColumn),
                this, foreignColumn, primaryTable);
        }

        public ForeignKey CreateForeignKey(string foreignColumn, string primaryTable, string primaryColumn)
        {
            return new ForeignKey(
                string.Format("FK_{0}_{1}", TableName, foreignColumn),
                this, foreignColumn, primaryTable, primaryColumn);
        }

        public virtual void AddTable()
        {
            Database.AddTable(
                TableName, Columns);
        }

        public virtual void RemoveTable()
        {
            Database.RemoveTable(TableName);
        }

        public virtual void AddForeignKeys()
        {
            if (ForeignKeys == null)
                return;
            for (int index = 0; index < ForeignKeys.Length; index++)
            {
                ForeignKey key = ForeignKeys[index];
                Database.AddForeignKey(key.Name, key.ForeignTable, key.ForeignColumn, key.PrimaryTable, key.PrimaryColumn);
            }
        }

        public virtual void RemoveForeignKeys()
        {
            if (ForeignKeys == null)
                return;
            for (int index = ForeignKeys.Length - 1; index > -1; index--)
            {
                ForeignKey key = ForeignKeys[index];
                Database.RemoveForeignKey(key.ForeignTable, key.Name);
            }
        }

        public override void Up()
        {
            AddTable();
            AddForeignKeys();
            AddDefaultData();
        }

        public virtual void AddDefaultData()
        {
        }

        public override void Down()
        {
            RemoveForeignKeys();
            RemoveTable();
        }

        protected void InsertAllFieldValue(string[] values)
        {
            if (values.Length != Columns.Length)
                throw new ArgumentException("field and value count do not match");

            string[] columnNames = PrepareColumnNameArray();
            Database.Insert(TableName, columnNames, values);
        }

        private string[] cachedColumnNames;
        private string[] PrepareColumnNameArray()
        {
            if (cachedColumnNames != null)
                return cachedColumnNames;
            var columnNames = new List<string>();
            foreach (var column in Columns)
            {
                columnNames.Add(column.Name);
            }
            cachedColumnNames = columnNames.ToArray();
            return cachedColumnNames;
        }

    }
}
