using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public class ArticleService : IArticleService
    {
        public IArticleDAL ArticleDAL { get; set; }

        public Article GetById(int id)
        {
            return ArticleDAL.FindById(id);
        }


        public Article Save(Article article)
        {
            if (article == null)
                throw new ArgumentNullException("article");

            return ArticleDAL.Save(article);
        }



        public IList<Article> FindAll()
        {
            return (from a in ArticleDAL.FindAll("PostedDate") orderby a.PostedDate descending select a).ToList();
        }


        public void Delete(Article article)
        {
            if (null == article)
                throw new ArgumentNullException("article");


            ArticleDAL.Delete(article);
        }
    }
}