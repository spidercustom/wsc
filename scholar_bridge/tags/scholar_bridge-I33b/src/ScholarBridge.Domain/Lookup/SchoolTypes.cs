﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SchoolTypes
    {
        [DisplayName("Private 4-year University or College")]
        PrivateUniversityCollege = 1,
        [DisplayName("Private Vocational / Technical / Career College")]
        PrivateVocationalOrTechnical = 2,
        [DisplayName("Public 4-year University or College")]
        PublicUniversityOrCollege = 4,
        [DisplayName("Public 2-year Community / Technical College")]
        PublicCommunityORTechnicalCollege = 8,
        [DisplayName("Seminary")]
        Seminary = 10
    }
}
