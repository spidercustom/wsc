using System.Web;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    abstract class OpenUrlAction : MatchAction
    {
        protected OpenUrlAction(string text)
        {
            Text = text;
        }
        public override void  Execute(Match match)
        {
            HttpContext.Current.Response.Redirect(ConstructUrl(match));
        }

        protected abstract string ConstructUrl(Match match);
    }
}