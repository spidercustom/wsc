using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Adapters
{
    public class MenuAdapter : System.Web.UI.WebControls.Adapters.MenuAdapter
    {
        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.Indent++;
            BuildItems(Control.Items, true, writer);
            writer.Indent--;
            writer.WriteLine();
        }

        private void BuildItems(MenuItemCollection items, bool isRoot, HtmlTextWriter writer)
        {
            if (items.Count > 0)
            {
                writer.WriteLine();

                writer.WriteBeginTag("ul");
                if (isRoot)
                {
                    writer.WriteAttribute("class", "menu");
                }
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.Indent++;

                foreach (MenuItem item in items)
                {
                    BuildItem(item, writer);
                }

                writer.Indent--;
                writer.WriteLine();
                writer.WriteEndTag("ul");
            }
        }

        private void BuildItem(MenuItem item, HtmlTextWriter writer)
        {
            var menu = Control as Menu;
            if ((menu != null) && (item != null) && (writer != null))
            {
                writer.WriteLine();
                writer.WriteBeginTag("li");
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.Indent++;

                if (item.NavigateUrl.Length > 0)
                {
                    writer.WriteBeginTag("a");
                    writer.WriteAttribute("href", Page.ResolveUrl(item.NavigateUrl));
                    if (item.Target.Length > 0)
                    {
                        writer.WriteAttribute("target", item.Target);
                    }
                    if (item.ToolTip.Length > 0)
                    {
                        writer.WriteAttribute("title", item.ToolTip);
                    }
                    else if (menu.ToolTip.Length > 0)
                    {
                        writer.WriteAttribute("title", menu.ToolTip);
                    }
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Indent++;
                }
                else
                {
                    writer.WriteBeginTag("span");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Indent++;
                }

                if (item.ImageUrl.Length > 0)
                {
                    writer.WriteBeginTag("img");
                    writer.WriteAttribute("src", Page.ResolveUrl(item.ImageUrl));
                    writer.WriteAttribute("alt", item.ToolTip.Length > 0 ? item.ToolTip : (menu.ToolTip.Length > 0 ? menu.ToolTip : item.Text));
                    writer.Write(HtmlTextWriter.SelfClosingTagEnd);
                }

                writer.Write(item.Text);

                if (item.NavigateUrl.Length > 0)
                {
                    writer.WriteEndTag("a");
                }
                else
                {
                    writer.WriteEndTag("span");
                }
                writer.Indent--;

                if ((item.ChildItems != null) && (item.ChildItems.Count > 0))
                {
                    BuildItems(item.ChildItems, false, writer);
                }

                writer.Indent--;
                writer.WriteEndTag("li");
            }
        }
    }
}