﻿
$.lDialog = {
 PagerID:'',
 TotalPages: 2,
 CurrentPage:1,
 CurrentFilter:'',
 DataUrl: '',
 HiddenUserDataID: '',
 ExludeItems: '',
 HiddenID: '',
 SearchButtonID: '',
 SearchBoxID: '',
 SelectionsID:'',
 AvailablesID: '',
 AjaxProgressID: '',
 SelectionLimit:0
 }

 function GetParams() {
    setExlcudeItems();
    var result = "&userdata="+$($.lDialog.HiddenUserDataID).val() +"&exclude=" + $.lDialog.ExludeItems + "&page=" + $.lDialog.CurrentPage + "&rp=60&sorton=name&sortorder=asc&qtype=name&query="+$.lDialog.CurrentFilter;
    return result;
    
}
function CreatePager() {
   
    $($.lDialog.PagerID).pager({ pagenumber: $.lDialog.CurrentPage, pagecount: $.lDialog.TotalPages, buttonClickCallback: PageClick });
}
function createDialog(dialogdiv, buddycontrolid, hiddenID,othercontrolid,rightdivid,otherboxdivid,othertextboxid,otherlabelid) {
    $.lDialog.HiddenD = hiddenID;
    if (othercontrolid=='#') {
        $(rightdivid).addClass('lookuplist_right');
        $(otherboxdivid).addClass('lookuplist_otherbox_hidden');
    }
    else 
        {
        $(rightdivid).addClass('lookuplist_right_withotherbox');
        $(otherboxdivid).addClass('lookuplist_otherbox_visible');
        $(othertextboxid).val($(othercontrolid).val());
        $(othercontrolid).attr("ReadOnly", "true");
         
         HideOtherControl(othercontrolid, othertextboxid, otherlabelid);
    }
    
    $(dialogdiv).dialog({
        autoOpen: false,
        width: 700,
        height: 580,
        modal: true,
        resizable: false/*,
        buttons: {
            "OK": function() {

                if ($.lDialog.SelectionLimit > 0) {

                    if ($.lDialog.SelectionLimit < GetSelectedItemCount()) {
                        alertdialog("You need to select " + $.lDialog.SelectionLimit + " items only.", "Hi!");
                        return false;
                    }
                }
                UpdateBuddyControl(buddycontrolid);
                if (othercontrolid !=='#') {
                    UpdateOtherControl(othercontrolid, othertextboxid, otherlabelid);
                }

                CleanUp();
                $(this).dialog("close");
            },
            "CANCEL": function() {
                CleanUp();
                $(this).dialog("close");
            }
        }*/
    });
   
}

function saveDialog(dialogdiv, buddycontrolid, hiddenID, othercontrolid, othertextboxid, otherlabelid) {
    
    if ($.lDialog.SelectionLimit > 0) {

        if ($.lDialog.SelectionLimit < GetSelectedItemCount()) {
            alertdialog("You need to select " + $.lDialog.SelectionLimit + " items only.", "Hi!");
            return false;
        }
    }
    UpdateBuddyControl(buddycontrolid);
    if (othercontrolid !== '#') {
        UpdateOtherControl(othercontrolid, othertextboxid, otherlabelid);
    }

    CleanUp();
    $(dialogdiv).dialog("close");
}

function cancelDialog(dialogdiv) {
    CleanUp();
    $(dialogdiv).dialog("close");
}


function showDialog(dialogdiv, div_available, div_selection, div_pager, dataurl, hiddenID, searchbuttonid, searchboxid, ajaxprogress, selectionLimit, hiddenUserDataID) {
    //create list of items
    $.lDialog.DataUrl = dataurl;
    $.lDialog.PagerID = div_pager;
    $.lDialog.HiddenID = hiddenID;
    $.lDialog.HiddenUserDataID = hiddenUserDataID;
    $.lDialog.SearchButtonID = searchbuttonid;
    
    $.lDialog.SearchBoxID= searchboxid;
    $.lDialog.SelectionsID = div_selection+'_liselections'; 
    $.lDialog.AvailablesID =  div_available+'_available';
    $.lDialog.AjaxProgressID = ajaxprogress;
    $.lDialog.SelectionLimit = selectionLimit;
    $($.lDialog.AjaxProgressID).show();
    
    if ($($.lDialog.AvailablesID).length == 0) { $(div_available).append('<ol id="' + $.lDialog.AvailablesID.replace("#", "") + '" class="available"></ol>'); }
    if ($($.lDialog.SelectionsID).length == 0) { $(div_selection).append('<ol id="' + $.lDialog.SelectionsID.replace("#", "") + '" class="selections"></ol>'); }

    
    $($.lDialog.SearchButtonID).click(function() {

    $.lDialog.CurrentFilter = $($.lDialog.SearchBoxID).val();
        
        getAjaxData();
        return false;
    });

    $($.lDialog.SearchBoxID).keyup(function(e) {
        if (e.which == 13) {
            $.lDialog.CurrentFilter = $($.lDialog.SearchBoxID).val();
             
            getAjaxData();
        }
        return false;
    });

    $($.lDialog.AvailablesID).dblclick(function() {
        addToSelections('');
    });

    $($.lDialog.SelectionsID).dblclick(function() {
        removeFromSelections('false');
    });
    getAjaxDataForSpecificItems($($.lDialog.HiddenID).val());
    $($.lDialog.SelectionsID).selectable();
    
    $($.lDialog.AvailablesID).selectable();
    
    getAjaxData();
    
    $(dialogdiv).dialog('open');
   return false;
}

function CleanUp() {
    $.lDialog.CurrentPage=1;
    $.lDialog.CurrentFilter = '';
    $($.lDialog.SearchBoxID).val('');
    return false;
}
 
function UpdateBuddyControl(buddycontrolid) {

    var names = '';
    
    var ids = '';
    var idcount = 0;
    $($.lDialog.SelectionsID + " li").each(function() {
        //var index = $($.lDialog.SelectionsID + " li").index(this);
        //var item = $($.lDialog.SelectionsID + " li")[index];
        idcount++;
        var id = $(this).attr('id');
        var name = $(this).text();

        if (ids == '')
            ids = id;
        else
            ids = ids + ", " + id;

        if (names == '')
            names = name;
        else
            names = names + ", " + name;
    });
    if (names.length == 0) {
        names = "None Selected";
    }
        
    $(buddycontrolid).val(names);
    
    $($.lDialog.HiddenID).val(ids);
}

function UpdateOtherControl(othercontrolid, othertextboxid, otherlabelid) {

    if ($(othercontrolid).val() !== $(othertextboxid).val()) {
        alertdialog('Other values will be shared with Scholarship providers but are not used in the logic to display scholarship matches.','Other Values');
    }
    $(othercontrolid).val($(othertextboxid).val());
    HideOtherControl(othercontrolid, othertextboxid, otherlabelid);
}

function HideOtherControl(othercontrolid, othertextboxid, otherlabelid) {
    
    if ($(othercontrolid).val() == '') {
        $(othercontrolid).hide();
        if (otherlabelid !== '#')
            { $(otherlabelid).hide(); }
            
    } else {
        $(othercontrolid).show();
        if (otherlabelid !== '#')
        { $(otherlabelid).show(); }
    }
    //alert('out  hide');
}

function GetSelectedItemCount() {
    
    var idcount = 0;
    $($.lDialog.SelectionsID + " li").each(function() {
        idcount++;
    });
    return idcount;
}

PageClick = function(pageclickednumber) {
            $.lDialog.CurrentPage = pageclickednumber;
            getAjaxData();
}

function getAjaxData() {
   
    var params = $.lDialog.DataUrl + GetParams();
    $($.lDialog.AjaxProgressID).show();
    $.ajax({
    type: "GET", //POST
    url: params,
        data: "", // Set Method Params
        beforeSend: function(xhr) {
        xhr.setRequestHeader("Content-type", "application/xml; charset=utf-8");
            xhr.setRequestHeader("Content-length", params.length);
        },
        contentType: "application/xml; charset=utf-8",
        dataType: "xml", // Set return Data Type
        success: function(msg, status) {
            CreateAvailableItems(msg);
            $($.lDialog.AjaxProgressID).hide();
        },
        error: function(xhr, msg, e) {
        $($.lDialog.AjaxProgressID).hide();
            alertdialog('Error: ' + msg,'oops!'); //Error Callback
        }
    });
}

function getAjaxDataForSpecificItems(ids) {
    if (ids == '')
        return false;
    $($.lDialog.AjaxProgressID).show();
    var params = $.lDialog.DataUrl + GetParams()+"&specific="+ids;
    $.ajax({
        type: "GET", //POST
        url: params,
        data: "", // Set Method Params
        async: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Content-type", "application/xml; charset=utf-8");
            xhr.setRequestHeader("Content-length", params.length);
        },
        contentType: "application/xml; charset=utf-8",
        dataType: "xml", // Set return Data Type
        success: function(msg, status) {
            CreateSelectionsItems(msg);
            $($.lDialog.AjaxProgressID).hide();
        },
        error: function(xhr, msg, e) {
        alertdialog('Error: ' + msg,'oops!'); //Error Callback
        $($.lDialog.AjaxProgressID).hide();
        }
    });
}


function CreateAvailableItems(xml) {
    var jdata = $(xml);
    var jrows = jdata.find("row");
    var total = jdata.find("totalpages").text();
    $.lDialog.TotalPages = total;
    $($.lDialog.AvailablesID+" li").remove();
    jrows.each(function() {
        var jrow = $(this);
        
        var name = jrow.children().text();
        $($.lDialog.AvailablesID).append("<li class='ui-widget-content ui-corner-all' id='" + jrow.attr("id") + "'>" + name + "</li>");

    });


    CreatePager();
   
   }

   function CreateSelectionsItems(xml) {
       var jdata = $(xml);
       var jrows = jdata.find("row");
       $($.lDialog.SelectionsID + " li").remove();
       jrows.each(function() {
           var jrow = $(this);
           var name = jrow.children().text();
           $($.lDialog.SelectionsID).append("<li class='ui-widget-content ui-corner-all' id='" + jrow.attr("id") + "'>" + name + "</li>");
       });
   }

function addToSelections() {
    var needRefresh = false;
    $($.lDialog.AvailablesID + " .ui-selected").each(function() {
    var index = $($.lDialog.AvailablesID + " li").index(this);
    var item = $($.lDialog.AvailablesID + " li")[index];
        $($.lDialog.SelectionsID).prepend(item);
        $(item).removeClass('ui-selected');
        needRefresh = true;
    });
    $("ol"+$.lDialog.SelectionsID+">li").tsort();
    if (needRefresh)
        getAjaxData();

    
}

function removeFromSelections(withall) {
    var needRefresh = false;
    var selectfrom = $.lDialog.SelectionsID + " .ui-selected";
    if (withall=='True')
        selectfrom = $.lDialog.SelectionsID + " li";
   
    
    $(selectfrom).each(function() {
    var index = $($.lDialog.SelectionsID + " li").index(this);
    var item = $($.lDialog.SelectionsID + " li")[index];
        $(item).remove();
        needRefresh = true;
    });
if (needRefresh)
   getAjaxData();
}

function setExlcudeItems() {
   
    $.lDialog.ExludeItems='';
    $($.lDialog.SelectionsID + " li").each(function() {
        $.lDialog.ExludeItems = $.lDialog.ExludeItems + "," + $(this).attr('id');
    });
    
}
