﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ApplicableControl runat=server></{0}:ApplicableControl>")]
    [Designer("System.Web.UI.Design.WebControls.PanelContainerDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [ParseChildren(false), PersistChildren(true)]
    public class ApplicabilityControl : Panel, INamingContainer
    {
        private const string SET_DEFAULT_CHECK_STATE =
            "$(window).load(function() {{ EnableDisableControls('{0}', '{1}', {2}); }});";

        public ApplicabilityControl()
        {
            DefaultCheckState = true;
        }

        private CheckBox controllerCheckBox;
        protected CheckBox ControllerCheckBox
        {
            get
            {
                if (controllerCheckBox == null)
                {
                    controllerCheckBox = new CheckBox
                                             {
                                                 ID = "controllerCheckBox",
                                                 Checked = DefaultCheckState,
                                                 Text = Text,
                                             };
                }
                return controllerCheckBox;
            }
        }

        public bool IsChecked
        {
            get
            {
                return ControllerCheckBox.Checked;
            }
            set
            {
                ControllerCheckBox.Checked = value;
                ResetVisibility();
            }
        }


        protected override void Render(HtmlTextWriter writer)
        {

            ControllerCheckBox.Attributes.Add(
                "onclick",
                string.Format("javascript:EnableDisableControls('{0}', '{1}', {2})", ClientID, ControllerCheckBox.ClientID, ApplicableOnUncheck.ToString().ToLower()));
            base.Render(writer);
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("N/A")]
        public string Text
        {
            get
            {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        [Bindable(true)]
        [DefaultValue(true)]
        public bool DefaultCheckState { get; set; }

        private bool applicableOnUncheck;
        [Bindable(true)]
        [DefaultValue(false)]
        public bool ApplicableOnUncheck
        {
            get { return applicableOnUncheck; }
            set
            {
                applicableOnUncheck = value;
                ResetVisibility();
            }
        }

        [Bindable(true)]
        [DefaultValue(false)]
        public bool AddBRAfterCheckBox { get; set; }

        private ApplicabilityEffect applicabilityEffect;
        [Bindable(true)]
        [DefaultValue(ApplicabilityEffect.EnableOrDisable)]
        public ApplicabilityEffect ApplicabilityEffect
        {
            get { return applicabilityEffect; }
            set
            {
                applicabilityEffect = value;
                ResetVisibility();
            }
        }

        private void ResetVisibility()
        {
            if (ApplicabilityEffect.VisibleOrInvisible == ApplicabilityEffect)
            {
                Visible = ApplicableOnUncheck ? IsChecked : !IsChecked;
            }
            else
            {
                Visible = true;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ResetVisibility();
            Controls.AddAt(0, ControllerCheckBox);
            
            if (null != Page)
            {
                Page.ClientScript.RegisterClientScriptBlock(
                    GetType(),
                    ClientID,
                    string.Format(SET_DEFAULT_CHECK_STATE, ClientID, ControllerCheckBox.ClientID, ApplicableOnUncheck.ToString().ToLower()), true);
            }
            
            if (AddBRAfterCheckBox)
            {
                var newLineLiteral = new LiteralControl("<br/>");
                Controls.AddAt(1, newLineLiteral);
            }
        }
    }

    public enum ApplicabilityEffect
    {
        VisibleOrInvisible,
        EnableOrDisable
    }
}
