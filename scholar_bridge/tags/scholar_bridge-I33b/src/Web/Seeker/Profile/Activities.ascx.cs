﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain.Lookup;
namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Activities : WizardStepUserControlBase<Domain.Seeker>
    {
        Domain.Seeker _seekerInContext;
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Seeker SeekerInContext
        {
            get
            {
                if (_seekerInContext == null)
                    _seekerInContext = Container.GetDomainObject();
                return _seekerInContext;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context");

            if (!Page.IsPostBack)
            {
                
                WorkHoursControl.DataBind();
                ServiceHoursControl.DataBind();
                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {

            AcademicAreasControlDialogButton.Keys = SeekerInContext.AcademicAreas.CommaSeparatedIds();
            AcademicAreasOtherControl.Text = SeekerInContext.AcademicAreaOther;
            
            CareersControlDialogButton.Keys = SeekerInContext.Careers.CommaSeparatedIds();
            CareersOtherControl.Text = SeekerInContext.CareerOther;
            
            CommunityServiceControlDialogButton.Keys = SeekerInContext.CommunityServices.CommaSeparatedIds();
            CommunityServiceOtherControl.Text = SeekerInContext.CommunityServiceOther;

            OrganizationsControlDialogButton.Keys = SeekerInContext.MatchOrganizations.CommaSeparatedIds();
            OrganizationsOtherControl.Text = SeekerInContext.MatchOrganizationOther;

            AffiliationTypesControlDialogButton.Keys = SeekerInContext.AffiliationTypes.CommaSeparatedIds();
            AffiliationTypesOtherControl.Text = SeekerInContext.AffiliationTypeOther;

            SeekerHobbiesControlDialogButton.Keys = SeekerInContext.Hobbies.CommaSeparatedIds();
            SeekerHobbiesOtherControl.Text = SeekerInContext.HobbyOther;

            SportsControlDialogButton.Keys = SeekerInContext.Sports.CommaSeparatedIds();
            SportsOtherControl.Text = SeekerInContext.SportOther;

            ClubsControlDialogButton.Keys = SeekerInContext.Clubs.CommaSeparatedIds();
            ClubsOtherControl.Text = SeekerInContext.ClubOther;

            WorkTypeControlDialogButton.Keys = SeekerInContext.WorkTypes.CommaSeparatedIds();
            
            WorkHoursControl.SelectedValues = SeekerInContext.WorkHours.Cast<ILookup>().ToList();

            ServiceTypeControlDialogButton.Keys = SeekerInContext.ServiceTypes.CommaSeparatedIds();

            ServiceHoursControl.SelectedValues = SeekerInContext.ServiceHours.Cast<ILookup>().ToList();
        }

        public override void PopulateObjects()
        {
            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, SeekerInContext.AcademicAreas);
            SeekerInContext.AcademicAreaOther = AcademicAreasOtherControl.Text;

            PopulateList(CareersContainerControl, CareersControlDialogButton, SeekerInContext.Careers);
            SeekerInContext.CareerOther = CareersOtherControl.Text;

            PopulateList(CommunityServiceContainerControl, CommunityServiceControlDialogButton, SeekerInContext.CommunityServices);
            SeekerInContext.CommunityServiceOther = CommunityServiceOtherControl.Text;

            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, SeekerInContext.MatchOrganizations);
            SeekerInContext.MatchOrganizationOther = OrganizationsOtherControl.Text;

            PopulateList(AffiliationTypesContainerControl, AffiliationTypesControlDialogButton, SeekerInContext.AffiliationTypes);
            SeekerInContext.AffiliationTypeOther = AffiliationTypesOtherControl.Text;

            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, SeekerInContext.Hobbies);
            SeekerInContext.HobbyOther = SeekerHobbiesOtherControl.Text;

            PopulateList(SportsContainerControl, SportsControlDialogButton, SeekerInContext.Sports);
            SeekerInContext.SportOther = SportsOtherControl.Text;

            PopulateList(ClubsContainerControl, ClubsControlDialogButton, SeekerInContext.Clubs);
            SeekerInContext.ClubOther = ClubsOtherControl.Text;

            PopulateList(WorkTypeContainerControl, WorkTypeControlDialogButton, SeekerInContext.WorkTypes);
            
            PopulateList(WorkHoursContainerControl, WorkHoursControl, SeekerInContext.WorkHours);
            
            PopulateList(ServiceTypeContainerControl, ServiceTypeControlDialogButton, SeekerInContext.ServiceTypes);
            PopulateList(ServiceHoursContainerControl, ServiceHoursControl, SeekerInContext.ServiceHours);
            
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (SeekerInContext.Stage < SeekerStages.NotActivated)
                SeekerInContext.Stage = SeekerStages.NotActivated;
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        private static bool Validate(PlaceHolder applicabilityControl, LookupDialog dialogButton)
        {
            return true;
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
            PopulateObjects();
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            SeekerService.Update(SeekerInContext);
		}
		#endregion
	}
}