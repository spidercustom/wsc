﻿using System;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Seeker
{
    public partial class Default : System.Web.UI.Page
    {
        public UserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            MainMenuHelper.ResetActiveMenu(this);
            if (!Page.User.Identity.IsAuthenticated)
                Response.Redirect("~/seeker/anonymous.aspx");
            UserContext.EnsureSeekerIsInContext();
        }
    }
}