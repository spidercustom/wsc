﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class FinancialNeed : WizardStepUserControlBase<Application>
    {
        Domain.Application _applicationInContext;
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        Domain.Application applicationInContext
        {
            get
            {
                if (_applicationInContext == null)
                    _applicationInContext = Container.GetDomainObject();
                return _applicationInContext;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (applicationInContext == null)
                throw new InvalidOperationException("There is no application in context");

            if (!Page.IsPostBack)
            {

                TypesOfSupport.DataBind();

                PopulateScreen();
            }
        }
        private void PopulateScreen()
        {
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());
            

            if (null != applicationInContext.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(applicationInContext.SupportedSituation.TypesOfSupport, ts => ts.Id.ToString());
            }
        }

        public override void PopulateObjects()
        {
            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            applicationInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (applicationInContext.Stage < ApplicationStages.NotActivated)
                applicationInContext.Stage = ApplicationStages.NotActivated;
        }


        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
       
        #region WizardStepUserControlBase methods

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            ApplicationService.Update(applicationInContext);
        }
        #endregion
	}
}