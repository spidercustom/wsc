﻿<%@ Page Language="C#" MasterPageFile="~/PrintView.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Applications.Show" Title="Application | Show" %>
<%@ Register src="~/Common/ShowApplication.ascx" tagname="ShowApplication" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
    <sb:PrintView id="PrintViewControl" runat="server" /> 
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
 <%--<div runat="Server" visible="<% #!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this) %>" ><asp:Image   ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div runat="Server" visible="<% #!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this) %>"><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
--%></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HideInPrintView="true" />

<sb:ShowApplication id="showApplication" runat="server" 
    LinkTo="~/Seeker/BuildApplication/Default.aspx" 
    ScholarshipLinkTo="~/Seeker/Scholarships/Show.aspx"/>
</asp:Content>