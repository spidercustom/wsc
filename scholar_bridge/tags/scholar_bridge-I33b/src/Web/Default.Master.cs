﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web
{
    public partial class DefaultMasterPage : MasterPage
    {
		public string ImpersonatingUser
		{
			get
			{
				return Session["ImpersonatingUser"] as string;
			}
			set
			{
				Session["ImpersonatingUser"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "theWashBoard.org | " + Page.Header.Title;

            PrintViewHeadContainer.Visible = Page.IsInPrintView();
            ScreenViewHeadContainer.Visible = !PrintViewHeadContainer.Visible;
            PopupViewHeadContainer.Visible = Page.IsInPopupView();
            PrintViewPageHeaderPlaceHolder.Visible = Page.IsInPrintView();
            ScreenViewPlaceHolder.Visible = !Page.IsInPopupView();
            PopupViewLogoPlaceHolder.Visible = Page.IsInPopupView();
			ImpersonationPlaceholder.Visible = ImpersonatingUser != null;
			if (!Page.IsPostBack)
			{
				string[] names = Page.User.Identity.Name.Split('!');
				if (names.Length > 1)
				{
					ImpersonatingUser = names[1];
					FormsAuthentication.SetAuthCookie(names[0], false);

				}
			}
        }

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (ImpersonatingUser != null)
					ImpersonationPlaceholder.Visible = true;
			}
		}

    	protected void stopImpersonationButton_Command(object sender, CommandEventArgs e)
		{
			FormsAuthentication.SignOut();

			string redirUrl = FormsAuthentication.GetRedirectUrl(ImpersonatingUser, false) +
							  Page.Request.PathInfo;
			if (Page.Request.QueryString.HasKeys())
			{
				redirUrl += "?" + Page.Request.QueryString;
			}
			FormsAuthentication.SetAuthCookie(ImpersonatingUser, false);
			AbandonSessionAndRedirect(redirUrl);
		}

		public void AbandonSessionAndRedirect(string path)
		{
			Session.Abandon();
			Response.Redirect(path, true);
		}

	}
}
