﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Admin
{
    public partial class Default : Page
	{
		#region Properties
		public IProviderService ProviderService {get;set;}
		public IIntermediaryService IntermediaryService { get; set; }
		public IApplicationService ApplicationService { get; set; }
		public ISeekerService SeekerService { get; set; }
		public IScholarshipService ScholarshipService { get; set; }


		private string providerSortColumn
		{
			get
			{
				if (ViewState["providerSortColumn"] == null)
				{
					ViewState["providerSortColumn"] = "Name";
				}
				return (string)ViewState["providerSortColumn"];
			}
			set
			{
				ViewState["ApprovedProviders"] = value;
			}

		}

		private string intermediarySortColumn
		{
			get
			{
				if (ViewState["intermediarySortColumn"] == null)
				{
					ViewState["intermediarySortColumn"] = "Name";
				}
				return (string)ViewState["intermediarySortColumn"];
			}
			set
			{
				ViewState["ApprovedProviders"] = value;
			}

		}

		private IList<Organization> ApprovedProviders
		{
			get
			{
				if (Session["ApprovedProviders"] == null)
				{
					var providers = ProviderService.FindActiveOrganizations();
					IEnumerable<Organization> providerEnum = from p in providers select p as Organization;
					Session["ApprovedProviders"] = providerEnum.ToList();
				}
				return (IList<Organization>)Session["ApprovedProviders"];
			}
			set
			{
				Session["ApprovedProviders"] = value;
			}

		}

		private IList<Organization> ApprovedIntermediaries
		{
			get
			{
				if (Session["ApprovedIntermediaries"] == null)
				{
					var intermediaries = IntermediaryService.FindActiveOrganizations();
					IEnumerable<Organization> intermediaryEnum = from p in intermediaries select p as Organization;
					Session["ApprovedIntermediaries"] = intermediaryEnum.ToList();
				}
				return (IList<Organization>)Session["ApprovedIntermediaries"];
			}
			set
			{
				Session["ApprovedIntermediaries"] = value;
			}

		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
        {
			if (!Page.IsPostBack)
			{
				InitializeObjects();
				LoadStatistics();
			}
		}

		#region Private Methods

		private void LoadStatistics()
    	{
			lblActiveSessionCount.Text = Application[Global.ACTIVE_USER_COUNT_APP_KEY].ToString();
    		providerCountLabel.Text = ApprovedProviders.Count().ToString();
    		intermediaryCountLabel.Text = ApprovedIntermediaries.Count().ToString();
			registeredSeekersLabel.Text = SeekerService.CountActivatedSeekers().ToString();
			activeProfilesLabel.Text = SeekerService.CountSeekersWithActiveProfiles().ToString();
			appsStartedLabel.Text = ApplicationService.CountAllSavedButNotSubmitted().ToString();
			appsSubmittedLabel.Text = ApplicationService.CountAllSubmitted().ToString();

			lblActivatedScholarshipCount.Text = ScholarshipService.CountAllActivated().ToString();
			lblNotActivatedScholarshipCount.Text = ScholarshipService.CountAllNotActivated().ToString();
			lblClosedScholarshipCount.Text = ScholarshipService.CountAllClosed().ToString();

			providersHidden.Value = "true";
    		intermediariesHidden.Value = "true";
    	}

    	private void InitializeObjects()
		{
			ApprovedProviders = null;
			ApprovedIntermediaries = null;
		}
		protected virtual void SetResponseHeadersForExcel(GridView gridView)
		{
			Response.Clear();

			Response.AddHeader("content-disposition", "attachment; filename=FileName.xls");

			Response.Charset = "";

			gridView.AllowPaging = false;
			gridView.DataBind();

			// If you want the option to open the Excel file without saving then
			// comment out the line below
			// Response.Cache.SetCacheability(HttpCacheability.NoCache);

			Response.ContentType = "application/vnd.xls";

			System.IO.StringWriter stringWrite = new System.IO.StringWriter();

			HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

			gridView.RenderControl(htmlWrite);

			Response.Write(stringWrite.ToString());

			Response.End();
			gridView.AllowPaging = true;
		}

		private void ImpersonateUser(string userName)
		{
			Web.ImpersonateUser.Impersonate(User.Identity.Name, userName, Page, Session);
		}

		#endregion

		#region datasource methods

		public IList<Organization> SelectProviders(string sortColumnName)
		{
			if (sortColumnName != providerSortColumn)
			{
				ApprovedProviders = sortOrganizations(ApprovedProviders, sortColumnName);
				providerSortColumn = sortColumnName;
			}
			return ApprovedProviders; 
		}

		public IEnumerable<Organization> SelectIntermediaries(string sortColumnName)
		{
			if (sortColumnName != intermediarySortColumn)
			{
				ApprovedIntermediaries = sortOrganizations(ApprovedIntermediaries, sortColumnName);
				intermediarySortColumn = sortColumnName;
			}
			return ApprovedIntermediaries;
		}

		public IEnumerable<Organization> SelectProvidersForDropDown()
		{
			return SelectProviders("Name");
		}

		public IEnumerable<Organization> SelectIntermediariesForDropDown()
		{
			return SelectIntermediaries("Name");
		}

		private static IList<Organization> sortOrganizations(IList<Organization> orgs, string sortColumn)
		{
			string[] sortWords = sortColumn.Split(' ');

			bool ascending = true;
			string sortColumnName = string.Empty;

			if (sortWords.Length > 0)
			{
				sortColumnName = sortWords[0];
				if (sortWords.Length > 1)
					ascending = false;
			}

			if (sortColumnName == string.Empty)
				sortColumnName = "Name";

			switch (sortColumnName)
			{
				case "Name":
					return orgs.OrderBy(o => o.Name, ascending).ToList();
				case "Website":
					return orgs.OrderBy(o => o.Website, ascending).ToList();
				case "TaxId":
					return orgs.OrderBy(o => o.TaxId, ascending).ToList();
				case "AdminUser":
					return orgs.OrderBy(o => o.AdminUser.Name, ascending).ToList();
				case "ApprovedStatusDate":
					return orgs.OrderBy(o => o.ApprovalStatusDate, ascending).ToList();
			}
			return orgs;
		}

		#endregion

		#region Render to Excel overrides

		/// <summary>
		/// this empty override prevents asp from killing the GridView render to excel
		/// </summary>
		/// <param name="control"></param>
		public override void VerifyRenderingInServerForm(Control control)
		{
			// Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
		}

		private bool enableEventValidation = true;
		public override bool EnableEventValidation
		{
			get
			{
				return enableEventValidation;
			}
			set
			{
				enableEventValidation = value;
			}
		}
		#endregion

		#region event handlers

		protected void providerDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
    		e.ObjectInstance = this;
		}

    	protected void intermediaryDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
			e.ObjectInstance = this;
		}

		protected void saveProvidersToExcel_Click(object sender, EventArgs e)
		{
			enableEventValidation = false;
			providerGrid.Columns[providerGrid.Columns.Count - 1].Visible = false;  // hide the impersonate column
			SetResponseHeadersForExcel(providerGrid);
		}

    	protected void saveIntermediariesToExcel_Click(object sender, EventArgs e)
    	{
			enableEventValidation = false;
    		intermediaryGrid.Columns[intermediaryGrid.Columns.Count - 1].Visible = false;  // hide the impersonate column
			SetResponseHeadersForExcel(intermediaryGrid);
    	}


		protected void providerGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "Impersonate")
				ImpersonateUser(e.CommandArgument.ToString());
		}


    	protected void btnSearchScholarships_Click(object sender, EventArgs e)
    	{
    	}
		#endregion

		protected void lbScholarshipSearchShowHide_Click(object sender, EventArgs e)
		{
			if (pnlScholarshipSearch.Visible)
			{
				pnlScholarshipSearch.Visible = false;
				lbScholarshipSearchShowHide.Text = "Show Scholarship Search";
			}
			else
			{
				pnlScholarshipSearch.Visible = true;
				lbScholarshipSearchShowHide.Text = "Hide Scholarship Search";
			}
		}

	}
}
