﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Relationships.Create" Title="Intermediary | Relationships | Create " %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />
<h3>Add Relationship Request</h3>
<br />
<h3>Place keeper for WSC provided text explaining the ScholarBridge process for establishing a relationship including contacting the organization before submitting this request</h3>    
<div class="form-iceland-container">
    <div class="form-iceland">

<label for="orgList" >Select Provider</label>
<asp:DropDownList ID="orgList" runat="server"></asp:DropDownList>
<asp:RequiredFieldValidator runat="server" ID="validateorganization" ControlToValidate="orgList" />
<br />
<br />
<sbCommon:AnchorButton ID="saveBtn" runat="server" Text="Submit Request" 
        onclick="saveBtn_Click"  /> 
<sbCommon:AnchorButton ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" 
        onclick="cancelBtn_Click" />
</div>
</div>        
</asp:Content>
