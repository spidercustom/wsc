﻿using System;
using System.Collections.Generic;
using NHibernate;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class ScholarshipDAL : AbstractDAL<Scholarship>, IScholarshipDAL
    {
       
        #region IScholarshipDAL Members
        public Scholarship FindById(int id)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Scholarship>();
        }

        public Scholarship FindByBusinessKey(Provider provider, string name, int year)
        {
            return CreateCriteria()
               .Add(Restrictions.Eq("Name", name))
               .Add(Restrictions.Eq("Provider", provider))
               .Add(Restrictions.Eq("AcademicYear.Year", year))
               .UniqueResult<Scholarship>();
        }
        

        public Scholarship Save(Scholarship scholarship)
        {
            return scholarship.Id < 1 ?
                Insert(scholarship) :
                Update(scholarship);
        }

        public IList<Scholarship> FindByProvider(Provider provider)
        {
            return CreateCriteria().Add(Restrictions.Eq("Provider", provider)).List<Scholarship>(); 
        }

        public IList<Scholarship> FindByProvider(Provider provider, ScholarshipStages stage)
        {
            return FindByProvider(provider, new[] { stage });
        }

        public IList<Scholarship> FindByProviderNotActivated(Provider provider)
        {
            return FindByProvider(provider, GetNonActivatedStages());
        }

        public IList<Scholarship> FindByProvider(Provider provider, ScholarshipStages[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Provider", provider))
                .AddOrder(Order.Asc("Stage"))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .AddOrder(Order.Asc("Stage"))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages stage)
        {
            return FindByOrganizations(provider, intermediary, new[] { stage });
        }

        public IList<Scholarship> FindNotActivatedByOrganizations(Provider provider, Intermediary intermediary)
        {
            return FindByOrganizations(provider, intermediary, GetNonActivatedStages());
        }

        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>(); 
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStages stage)
        {
            return FindByIntermediary(intermediary, new[] {stage});
        }


        public IList<Scholarship> FindByIntermediaryNotActivated(Intermediary intermediary)
        {
            return FindByIntermediary(intermediary, GetNonActivatedStages());
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStages[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindAllClosedSince(DateTime date)
        {
            return CreateCriteria()
                .Add(Restrictions.IsNotNull("AwardPeriodClosed"))
                .Add(Restrictions.Between("AwardPeriodClosed", date, DateTime.Now))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindAllDueOn(DateTime date)
        {
            return CreateCriteria()
                .Add(Restrictions.IsNotNull("ApplicationDueDate"))
                .Add(Restrictions.Between("ApplicationDueDate", date, date.AddDays(1).AddSeconds(-1)))
                .List<Scholarship>();
        }

    	public IList<Scholarship> GetAllScholarships()
    	{
    		return CreateCriteria().List<Scholarship>();
    	}


    	public IList<Scholarship> FindByStage(ScholarshipStages stage)
        {
            return CreateCriteria()
                 .Add(Restrictions.Eq( "Stage", stage))
                 .List<Scholarship>();
        }

        #endregion
        public static ScholarshipStages[] GetNonActivatedStages()
        {
            return new[]
                       {
                           ScholarshipStages.None,
                           ScholarshipStages.NotActivated,
                           ScholarshipStages.Rejected
                       };
        }

        public void Flush()
        {
            var cur = Session.FlushMode;
            try
            {
                Session.FlushMode = FlushMode.Never;
                Session.Flush();
            }
            finally
            {
                Session.FlushMode = cur;   
            }
        }

    	public string GetScholarshipListXml()
    	{
    		const string sql = @"
select
(
	select 
			'/' + dbo.ScholarshipUrlEncode(p.Name) + '/' + CAST(s.AcademicYear - 1 as CHAR(4)) + '-' + cast(s.AcademicYear as CHAR(4)) + '/' + dbo.ScholarshipUrlEncode(s.Name) as UrlKey,	
			s.Name, cast(s.AcademicYear - 1 as CHAR(4)) + '-' + CAST(s.AcademicYear as CHAR(4)) as AcademicYear, 
			s.ApplicationStartDate, 
			s.ApplicationDueDate, 
			s.MaximumAmount,
			p.Name as ProviderName, 
			isnull(i.Name, '') as IntermediaryName,
			isnull(s.DonorName, '') as DonorName,
			s.MissionStatement,
			s.SBScholarshipID
	from SBScholarship s
	inner join SBOrganization p on p.SBOrganizationID = s.SBProviderID
	left join SBOrganization i on i.SBOrganizationID = s.SBIntermediaryID
	where Stage = 'Activated' and AwardDate >= GETDATE()
	FOR XML PATH('scholarship'), ROOT('doc')
) as ScholarshipList 
";
			ISQLQuery query = Session.CreateSQLQuery(sql).AddScalar("ScholarshipList", NHibernateUtil.String);
			//Double [] amount = (Double []) query.uniqueResult(); 
    		string xmlout = (string) query.UniqueResult();
    		return  xmlout;
    	}

    	public int CountAllActivatedScholarships()
    	{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ScholarshipStages.Activated))
				.Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

    	public int CountAllNotActivatedScholarships()
    	{
			return CreateCriteria()
				.Add(Restrictions.In("Stage", GetNonActivatedStages()))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

    	public int CountAllClosedScholarships()
    	{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ScholarshipStages.Activated))
				.Add(Restrictions.Lt("ApplicationDueDate", DateTime.Now))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
    	}
    }
}
