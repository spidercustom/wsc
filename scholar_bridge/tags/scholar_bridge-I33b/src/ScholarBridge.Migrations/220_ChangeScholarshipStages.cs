﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(220)]
    public class ChangeScholarshipStages : Migration
    {
        public override void Up()
        {
            UpdateToNotActivate("GeneralInformation");
            UpdateToNotActivate("MatchCriteriaSelection");
            UpdateToNotActivate("SeekerProfile");
            UpdateToNotActivate("FundingProfile");
            UpdateToNotActivate("AdditionalCriteria");
        }

        private void UpdateToNotActivate(string fromStage)
        {
            UpdateStage(fromStage, "NotActivated");
        }

        private void UpdateStage(string fromStage, string toStage)
        {
            Database.Update("SBScholarship", new[] { "Stage" }, new[] { toStage }, string.Format("[Stage] = '{0}'", fromStage));
        }

        public override void Down()
        {
            UpdateStage("NotActivated", "GeneralInformation");
        }
    }
}