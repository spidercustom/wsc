﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(245)]
	public class AddAnonUser : Migration
	{
		public override void Up()
		{
			Database.Insert("SBUser",
				new[] { "Username", "Password", "Email", "PasswordFormat", "IsActive", "IsApproved", "IsDeleted", "FirstName", "MiddleName", "LastName" },
				new[] { "anonymous@scholarbridge.com", "sb@dmin123", "anonymous@scholarbridge.com", "2", "true", "true", "false", "Anonymous", string.Empty, "User" });
		}

		public override void Down()
		{
			Database.Delete("SBUser", "UserName", "anonymous@scholarbridge.com");
		}
	}
}