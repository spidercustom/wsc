﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(244)]
    public class RemoveClassRankRangeFromScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        public readonly string[]  COLUMNS
            = { "MatchCriteriaClassRankGreaterThan", "MatchCriteriaClassRankLessThan"};

        public override void Up()
        {
            foreach (var col in COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }

           

             
        }

        public override void Down()
        {
            Database.AddColumn(TABLE_NAME, COLUMNS[0], DbType.Int32 , ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, COLUMNS[1], DbType.Int32 , ColumnProperty.Null);
        }
    }
}