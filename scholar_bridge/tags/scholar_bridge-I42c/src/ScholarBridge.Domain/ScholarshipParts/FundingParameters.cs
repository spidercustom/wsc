using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingParameters
    {
        [RangeValidator(typeof(decimal),"1", RangeBoundaryType.Inclusive, "0", RangeBoundaryType.Ignore, MessageTemplate = "Should be at least one dollar")]
        public virtual decimal AnnualSupportAmount { get; set; }

        [RangeValidator(1, RangeBoundaryType.Inclusive, 0, RangeBoundaryType.Ignore, MessageTemplate = "Should be at least one")]
        public virtual int MinimumNumberOfAwards { get; set; }

        [PropertyComparisonValidator("MinimumNumberOfAwards", ComparisonOperator.GreaterThanEqual, MessageTemplate = "Maximum Number of Awards must be greater than or equal to Minimum Number of Awards.")]
        public virtual int MaximumNumberOfAwards { get; set; }

        public virtual TermOfSupport TermOfSupport { get; set; }
    }
}