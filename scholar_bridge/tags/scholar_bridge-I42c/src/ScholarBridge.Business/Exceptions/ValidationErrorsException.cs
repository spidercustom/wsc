﻿
using System;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation;

namespace ScholarBridge.Business.Exceptions
{
    [Serializable]
    public class ValidationErrorsException : Exception
    {
        public ValidationResults Errors { get; private set; }

        public ValidationErrorsException(ValidationResults errors)
        {
            Errors = errors;
        }

        public ValidationErrorsException(string message, ValidationResults errors)
            : base(message)
        {
            Errors = errors;
        }

        public ValidationErrorsException(string message, ValidationResults errors, Exception inner) : base(message, inner)
        {
            Errors = errors;
        }

        protected ValidationErrorsException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
