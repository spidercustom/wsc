using System;

namespace ScholarBridge.Business.Exceptions
{
    /// <summary>
    /// Custom Exception for Invalid Email Message
    /// </summary>
    public class InvalidEmailMessageException : Exception
    {
        public InvalidEmailMessageException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}