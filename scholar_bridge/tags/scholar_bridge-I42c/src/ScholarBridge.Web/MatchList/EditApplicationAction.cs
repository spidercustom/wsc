using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    class EditApplicationAction : OpenUrlAction
    {
        private const string EDIT_APPLICATION_URL_TEMPLATE = "~/Seeker/BuildApplication/Default.aspx?aid={0}";

        public EditApplicationAction() : base("Edit") { }

        protected override string ConstructUrl(Match match)
        {
            return EDIT_APPLICATION_URL_TEMPLATE.Build(match.Application.Id);
        }

		public override string GetActionUrl(Match match)
		{
			return ConstructUrl(match);
		}

    }
}