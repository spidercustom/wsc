using System.Web;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    abstract class OpenUrlAction : MatchAction
    {

        protected OpenUrlAction(string text)
        {
            Text = text;
        }
        public bool  IsShowInPopup{get;set;}
        public override void  Execute(Match match)
        {
			if (!IsShowInPopup)
			{
				var url = ConstructUrl(match);
				HttpContext.Current.Response.Redirect(url);
			}
        }

        protected abstract string ConstructUrl(Match match);
    }
}