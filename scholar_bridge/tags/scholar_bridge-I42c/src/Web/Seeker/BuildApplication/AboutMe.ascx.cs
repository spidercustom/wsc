﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
    public partial class AboutMe : WizardStepUserControlBase<Application>
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        Application application;
        Application ApplicationInContext
        {
            get
            {
                if (application == null)
                    application = Container.GetDomainObject();
                if (application == null)
                    throw new InvalidOperationException("There is no application in context");
                return application;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ApplicationInContext == null)
                throw new InvalidOperationException("There is no application in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
		}

        private void PopulateScreen()
        {
            PersonalStatementControl.Text = ApplicationInContext.PersonalStatement;
            MyGiftControl.Text = ApplicationInContext.MyGift;
            FiveWordsControl.Text = ApplicationInContext.Words;
            FiveSkillsControl.Text = ApplicationInContext.Skills;
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
		    PopulateObjects();
            ApplicationService.Update(ApplicationInContext);
		}

        public override void PopulateObjects()
        {
            ApplicationInContext.PersonalStatement = PersonalStatementControl.Text;
            ApplicationInContext.MyGift = MyGiftControl.Text;
            ApplicationInContext.Words = FiveWordsControl.Text;
            ApplicationInContext.Skills = FiveSkillsControl.Text;

            ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
        }

		#endregion

	}
}