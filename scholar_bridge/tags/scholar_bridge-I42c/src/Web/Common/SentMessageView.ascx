﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SentMessageView.ascx.cs" Inherits="ScholarBridge.Web.Common.SentMessageView" %>

<div class="message">
   <strong>From:</strong> <asp:Literal ID="fromLbl" runat="server" /><br /><br />
    <strong>Date:</strong> <asp:Literal ID="dateLbl" runat="server" /><br /><br />
    <strong>To: </strong><asp:Literal ID="toLbl" runat="server" /><br /><br />
    <h3>Subject: <asp:Literal ID="subjectLbl" runat="server"/></h2>
    <pre>
    <asp:Literal ID="contentLbl" runat="server"/>
    </pre>
</div>
