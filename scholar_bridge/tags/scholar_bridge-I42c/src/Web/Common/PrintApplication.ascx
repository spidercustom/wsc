﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintApplication.ascx.cs"
    Inherits="ScholarBridge.Web.Common.PrintApplication" %>
    <%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>
<div class="subsection-big">
    <br />
    <br />
   <div>         
       <p class="smallText indentsection">Application For:</p> 
       <p class="headerText indentsection"> <asp:Literal  ID="scholarshipName"  runat="server" /> </p>  
       <br />
  </div> 
  
   <br />
   <br />
   <label class="sectionHeader">Personal Information:</label>
   <br />
    <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Name:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoName" runat="server" ></asp:Label></td>
        </tr>
         <tr runat="server" id="Tr1">
            <td><label class="captionMedium">Email:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoEmail" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Address:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoAddress" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">County:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoCounty" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">Phone:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoPhone" runat="server" ></asp:Label></td>
        </tr>
          <tr>
            <td><label class="captionMedium">Mobile:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoMobile" runat="server" ></asp:Label></td>
        </tr>
          <tr>
            <td><label class="captionMedium">Birthday:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoBirthDay" runat="server" ></asp:Label></td>
        </tr>
          <tr>
            <td><label class="captionMedium">Gender:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoGender" runat="server" ></asp:Label></td>
        </tr>
          <tr>
            <td><label class="captionMedium">Personal Statement:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoStatement" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">My Gift:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoMyGift" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">5 Words That Describe Me:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoFiveWords" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">5 Skills I Have:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoFiveSkills" runat="server" ></asp:Label></td>
        </tr>
        
    </table>
    <table class="viewonlyTable" runat="server" id="ReligionTable" >
         
        
    </table>
    <table class="viewonlyTable" runat="server" id="HeritageTable" >
         
        
    </table>
<br />    
  <br /> 
<label class="sectionHeader">Academic Information:</label>
<br />
<br /> 
        <table class="viewonlyTable" runat="server" id="AcademicInfoHighSchoolTable" >
        </table>
        
         <table class="viewonlyTable" runat="server" id="AcademicInfoCollegeTable" >
        </table>
        <table class="viewonlyTable" runat="server" id="InterestsTable" >
            <tr> <td><label class="captionMedium">What are your academic interests?</label></td></tr>                
        </table>
<br />    
  
<label class="sectionHeader">Academic Performance:</label>
<br />
<br />        
        <table class="viewonlyTable" runat="server" id="AcademicPeformanceTable" >
             
            
        </table>        
<br />       

<br />    
  
<label class="sectionHeader">Interests and Activities:</label>
<br />
<br />     
 <table class="viewonlyTable" runat="server" id="OrganizationsTable" >
     <tr>
        <td><label class="captionMedium">Are you or any family members affiliated with specific Organizations Or Companies?</label></td>
        <td>
                
        </td>
    </tr>   
                     
 </table>  
 <table class="viewonlyTable" runat="server" id="GroupsInvolvedTable" >
       <tr>
                <td><label class="captionMedium">What other groups or interests are you involved in?</label></td>
                <td>
                       
                </td>
            </tr>                      
 </table> 
 <table class="viewonlyTable" runat="server" id="ServicesTable" >
     <tr>
    <td><label class="captionMedium">Are you working? Volunteering or Community Service?</label></td>
    <td>
            
    </td>
    </tr>                            
</table> 

<br />    
  
<label class="sectionHeader">What is your financial need?:</label>
<br />
<br />    
<table class="viewonlyTable" runat="server" id="FinancialNeedTable" >
                       
 </table>
 <table class="viewonlyTable" runat="server" id="FafsaTable" >
 <tr>
    <td><label class="captionMedium">FAFSA (Free Application for Federal Student Aid):</label></td>
    <td>
                        
                         
    </td>   
    </tr>                                   
</table>   


<br />    
  
<label class="sectionHeader">Additional Requirements:</label>
<br />
<br />   
 <table class="viewonlyTable" runat="server" id="AddtionalRequirementsTable">
    <tr>
    <td><label class="captionMedium">Additional Requirements with Scholarship Application:</label></td>
    <td>      </td>
    </tr>
</table>         
<table class="viewonlyTable" runat="server" id="QuestionsTable">
    <tr>
    <td><label class="captionMedium">Please answer following questions:</label></td>
    <td>
            
    </td>
    </tr>                             
</table>  

<table class="viewonlyTable" runat="server" id="FormsTable">
    <tr>
    <td><label class="captionMedium">Application Attachments:</label></td>
    <td>
            
    </td>
    </tr>      
                
    <tr>
    <td><sb:FileList id="applicationAttachments" runat="server" />
    </td>
    </tr>
     
</table>
  <br />
        
 <p>TheWashBoard.org is a free web-based tool for Washington students seeking college scholarships. We make scholarship searching simple.</p> 
 <p>Create a profile and review your scholarship matches. Register Today at <a class="GreenLink" href=http://www.theWashBoard.org>theWashBoard.org</a></p>
 <br />
 <p class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</p>
</div>
