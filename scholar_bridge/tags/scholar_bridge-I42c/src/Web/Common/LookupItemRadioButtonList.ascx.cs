﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Data;
using ScholarBridge.Domain.Lookup;
using Spring.Context.Support;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class LookupItemRadioButtonList : System.Web.UI.UserControl
    {
        public LookupItemRadioButtonList()
        {
            RadioButtonListWidth = new Unit(450, UnitType.Pixel);
        }
        public bool MultiColumn { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (MultiColumn)
                RadioButtonList.RepeatColumns = 2;
        }
       
        public void DisableControls()
        {
            RadioButtonList.Enabled = false;
        }
        public void EnableControls()
        {
            RadioButtonList.Enabled = true;
        }

         


        private ILookupDAL lookupService;
        private string lookupServiceSpringContainerKey;
        public Unit RadioButtonListWidth { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            RadioButtonList.Width = RadioButtonListWidth;
            base.OnPreRender(e);
        }

        public string LookupServiceSpringContainerKey
        {
            get { return lookupServiceSpringContainerKey; }
            set
            {
                lookupServiceSpringContainerKey = value;
                var context = ContextRegistry.GetContext();
                var retrivedObject = context.GetObject(value);
                if (null == retrivedObject)
                    throw new ArgumentException(string.Format("Cannot retrieve object by key {0}", value));
                lookupService = retrivedObject as ILookupDAL;
                if (null == lookupService)
                    throw new ArgumentException(string.Format("Cannot cast object retrieved by key {0} to ILookupDAL", value)); 
                DataBind();
            }
        }


        public override void DataBind()
        {
            if (null != lookupService)
            {
                RadioButtonList.DataSource = lookupService.FindAll();
                RadioButtonList.DataTextField = "Name";
                RadioButtonList.DataValueField = "Id";
            }

            base.DataBind();
			
        }

        [DefaultValue(false)]
        public bool SelectNoneAllowed { get; set; }

        [Browsable(false)]
        public  ILookup  SelectedValue 
        {
            get
            {
                   if( string.IsNullOrEmpty(RadioButtonList.SelectedValue))
                       return null;
                    int selectedItemId = Int32.Parse(RadioButtonList.SelectedValue);
                    var retrivedObject = lookupService.FindById(selectedItemId);
                    return retrivedObject;
            }
            set
            {
                if (value == null)
                    RadioButtonList.SelectedIndex = 0;
                else
                    RadioButtonList.SelectedValue = value.Id.ToString();
            }
        }

         
       

       
       
    }
}