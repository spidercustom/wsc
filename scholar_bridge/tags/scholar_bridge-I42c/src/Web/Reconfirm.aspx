﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Reconfirm.aspx.cs" Inherits="ScholarBridge.Web.Reconfirm" Title="Resend Confirmation Email" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<asp:Wizard ID="reconfirmWizard" runat="server" 
        OnNextButtonClick="StepNextButton_ButtonClick" DisplaySideBar="False">
    <WizardSteps>
        <asp:WizardStep ID="getUserInfo" runat="server" Title="Resend Confirmation Email">
            <asp:Label id="errorLbl" runat="server" Visible="false" />
            <br />
            <label for="EmailAddress">Email Address:</label>
            &nbsp;<asp:TextBox ID="EmailAddress" runat="server" />
            <br />
            <asp:RequiredFieldValidator ID="emailAddressReqValid" ControlToValidate="EmailAddress" runat="server" ErrorMessage="Email Address is required to continue." />
            <br />
        </asp:WizardStep>
        <asp:WizardStep ID="complete" runat="server" Title="Confirmation Email Sent">
            <h3><asp:Label id="doneMessage" runat="server"/></h3>
            <asp:HyperLink ID="loginLnk" runat="server" NavigateUrl="~/login.aspx">Return to Login page</asp:HyperLink>
        </asp:WizardStep>
    </WizardSteps>
    <StepNavigationTemplate>
      <sbCommon:AnchorButton ID="StepNextButton" runat="server" CommandName="MoveNext" CausesValidation="True" Text="Send" />
    </StepNavigationTemplate>
    <FinishNavigationTemplate></FinishNavigationTemplate>
</asp:Wizard>
</asp:Content>
