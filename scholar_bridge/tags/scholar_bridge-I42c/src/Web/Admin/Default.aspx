<%@ Page ValidateRequest="false" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Admin.Default" Title="Admin" %>
<%@ Import Namespace="ScholarBridge.Web"%>
<%@ Register Src="~/Common/ImpersonateUser.ascx" TagName="ImpersonateUser" TagPrefix="sb" %>
<%@ Register src="ScholarshipSearch.ascx" tagname="ScholarshipSearch" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {

            var providersHidden = '#' + '<%= providersHidden.ClientID %>';
            var intermediariesHidden = '#' + '<%= intermediariesHidden.ClientID %>';

            if ($(providersHidden).val() == 'true') {
                $("#providersDiv").hide();
            }
            //toggle the display of the Providers
            $("#showHideProvider").click(function() {
                $("#providersDiv").slideToggle(200);
                if ($(providersHidden).val() == 'false')
                    $(providersHidden).val('true');
                else
                    $(providersHidden).val('false');
            }
            );
            
            if ($(intermediariesHidden).val() == 'true') {
                $("#intermediaryDiv").hide();
            }
            //toggle the display of the Intermediary List
            $("#showHideIntermediaries").click(function() {
                $("#intermediaryDiv").slideToggle(200);
                if ($(intermediariesHidden).val() == 'true')
                    $(intermediariesHidden).val('false');
                else
                    $(intermediariesHidden).val('true');
            });
        });
    </script>
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgScholarship.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<h3>My Home</h3>
<fieldset>
    <legend><b>Providers & Intermediaries</b></legend>

    <br />
    <table>
        <tr>
            <td><label>Providers (# of active Orgs):&nbsp;&nbsp;&nbsp;</label></td>
            <td><asp:Label ID="providerCountLabel" runat="server"></asp:Label></td>
            <td>&nbsp;&nbsp;&nbsp;<a href="#" class="GreenText" id="showHideProvider">[show/hide]<asp:hiddenfield ID="providersHidden" runat="server" /></a></td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="providersDiv">
                    <br />
                    <asp:gridview ID="providerGrid" DataSourceID="providerDataSource" Width="100%"
                            runat="server" 
                            AutoGenerateColumns="False"
                            AllowSorting="true"
                            AllowPaging="true"  PageSize="20" 
                        onrowcommand="providerGrid_RowCommand">
                       <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                            <asp:TemplateField HeaderText="Web Site" SortExpression="Website">
                                <ItemTemplate>
                                    <a href='<%# ((Eval("Website") != null && ((string)Eval("Website")).Length > 3)) ? ((string)Eval("Website")).Substring(0,4).ToLower() == "http" ? Eval("Website") : string.Concat("http://", (string)Eval("Website")) : Eval("Website") %>'><%# Eval("Website") %></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tax ID" SortExpression="TaxId">
                                <ItemTemplate>
                                    <asp:label runat="server" ID="taxidLabel" text='<%# string.Concat(((string)Eval("TaxId")).Substring(0, 2), "-", ((string)Eval("TaxId")).Substring(2)) %>'></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Admin" SortExpression="AdminUser">
                                <ItemTemplate>
                                    <a href='mailto:<%# Eval("AdminUser.UserName") %>'><asp:label runat="server" ToolTip='<%# Eval("AdminUser.UserName") %>' ID="adminUserLabel" text='<%# Eval("AdminUser.Name") %>'></asp:label></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Approved" SortExpression="ApprovedStatusDate">
                                <ItemTemplate>
                                    <asp:label runat="server" ID="statusDateLabel" text='<%# ((DateTime)Eval("ApprovalStatusDate")).ToShortDateString() %>'></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="impersonateButton" CausesValidation="false" runat="server" CommandArgument='<%# Eval("AdminUser.UserName") %>' Text="Impersonate" CommandName="Impersonate"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:gridview>  
                    <asp:linkbutton ID="saveProvidersToExcel" runat="server" Text="Save to Excel" 
                        onclick="saveProvidersToExcel_Click"></asp:linkbutton>  
                </div>
                <br />
            </td>
        </tr>
        <tr>
            <td>Intermediaries (# of active Orgs):&nbsp;&nbsp;&nbsp;</td>
            <td><asp:Label ID="intermediaryCountLabel" runat="server"></asp:Label></td>
            <td>&nbsp;&nbsp;&nbsp;<a href="#" class="GreenText" id="showHideIntermediaries">[show/hide]</a><asp:hiddenfield ID="intermediariesHidden" runat="server" /></td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="intermediaryDiv">
                    <br />
                    <asp:gridview ID="intermediaryGrid" runat="server" 
                        DataSourceID="intermediaryDataSource" AutoGenerateColumns="False" 
                        AllowSorting="true" 
                        AllowPaging="true"  PageSize="20"
                        onrowcommand="providerGrid_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" 
                                SortExpression="Name" />
                            <asp:TemplateField HeaderText="Web Site" SortExpression="Website">
                                <ItemTemplate>
                                    <a href='<%# ((Eval("Website") != null && ((string)Eval("Website")).Length > 3)) ? ((string)Eval("Website")).Substring(0,4).ToLower() == "http" ? Eval("Website") : string.Concat("http://", (string)Eval("Website")) : Eval("Website") %>'><%# Eval("Website") %></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tax ID" SortExpression="TaxId">
                                <ItemTemplate>
                                    <asp:label runat="server" ID="taxIdLabel2" text='<%# string.Concat(((string)Eval("TaxId")).Substring(0, 2), "-", ((string)Eval("TaxId")).Substring(2)) %>'></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Admin" SortExpression="AdminUser">
                                <ItemTemplate>
                                    <a href='mailto:<%# Eval("AdminUser.UserName") %>'><asp:label runat="server" ID="adminUserLabel2" ToolTip='<%# Eval("AdminUser.UserName") %>' text='<%# Eval("AdminUser.Name") %>'></asp:label></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Approved" SortExpression="ApprovedStatusDate">
                                <ItemTemplate>
                                    <asp:label runat="server" ID="statusDateLabel2" text='<%# ((DateTime)Eval("ApprovalStatusDate")).ToShortDateString() %>'></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="impersonateButton" runat="server" CausesValidation="false" CommandArgument='<%# Eval("AdminUser.UserName") %>' Text="Impersonate" CommandName="Impersonate"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:gridview>    
                    <asp:linkbutton ID="saveIntermediariesToExcel" runat="server" Text="Save to Excel" 
                        onclick="saveIntermediariesToExcel_Click"></asp:linkbutton>  
                </div>
            </td>
        </tr>
    </table>
</fieldset>
<br /><br />
<fieldset>
    <legend><b>Impersonate User</b></legend>
    Count of Active User Sessions:
    <asp:Label ID="lblActiveSessionCount" runat="server"></asp:Label>
    <br />
    <sb:ImpersonateUser ID="impersonateUserControl" runat="server" Visible="true"  />    
    <br />
</fieldset>
<br /><br />
<fieldset>
<legend><b>Scholarships</b></legend>
<br />
<table>
    <tr>
        <td>Activated:&nbsp;</td>
        <td><asp:Label ID="lblActivatedScholarshipCount" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Not Activated:&nbsp;</td>
        <td><asp:Label ID="lblNotActivatedScholarshipCount" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Closed:&nbsp;</td>
        <td><asp:Label ID="lblClosedScholarshipCount" runat="server"></asp:Label>&nbsp;&nbsp;</td>
        <td>Rejected:&nbsp;</td>
        <td><asp:Label ID="lblRejectedScholarshipCount" runat="server"></asp:Label>&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td colspan="8">
            <asp:LinkButton ID="lbScholarshipSearchShowHide" runat="server" 
                CausesValidation="False" onclick="lbScholarshipSearchShowHide_Click">Show Scholarship Search</asp:LinkButton>
        </td>
    </tr>
</table>

<br />

<asp:panel ID="pnlScholarshipSearch" Visible="false"  runat="server">
            <uc1:ScholarshipSearch OnScholarshipSavedEvent="scholarshipSearch1_ScholarshipSavedEvent" ID="scholarshipSearch1" runat="server" />
</asp:panel>
</fieldset>
<br /><br />
<fieldset>
<legend><b>Seekers</b></legend>
<br />
<table>
    <tr>
        <td>Count of Registered Seekers:&nbsp;&nbsp;&nbsp;</td>
        <td><asp:Label ID="registeredSeekersLabel" runat="server"></asp:Label></td>
        <td></td>
    </tr>
    <tr>
        <td>Count of Active Profiles:&nbsp;&nbsp;&nbsp;</td>
        <td><asp:Label ID="activeProfilesLabel" runat="server"></asp:Label></td>
        <td></td>
    </tr>
</table>

</fieldset>

<br /><br />
<fieldset>
<legend><b>Scholarship Applications (open scholarships only)</b></legend>
<br />
<table>
    <tr>
        <td>Count of Apps Started:&nbsp;&nbsp;&nbsp;</td>
        <td><asp:Label ID="appsStartedLabel" runat="server"></asp:Label></td>
        <td></td>
    </tr>
    <tr>
        <td>>Count of Apps Submitted:&nbsp;&nbsp;&nbsp;</td>
        <td><asp:Label ID="appsSubmittedLabel" runat="server"></asp:Label></td>
        <td>
        </td>
    </tr>
</table>
</fieldset>

<br /><br />
<fieldset>
<legend><b>Google Analytics</b></legend><br />
<a href="https://www.google.com/analytics/">Google Analytics - Home</a><br /><br />
<a href="https://www.google.com/analytics/siteopt/splash?hl=en">Google Analytics - Web Site Optimizer</a>
</fieldset>

<asp:ObjectDataSource ID="providerDataSource" 
    runat="server" SelectMethod="SelectProviders"
    TypeName="ScholarBridge.Web.Admin.Default"
    SortParameterName="sortColumnName"
    OnObjectCreating="providerDataSource_ObjectCreating">
</asp:ObjectDataSource>


<asp:ObjectDataSource ID="intermediaryDataSource" 
    runat="server" 
    SelectMethod="SelectIntermediaries"
    SortParameterName="sortColumnName"
    TypeName="ScholarBridge.Web.Admin.Default"
    OnObjectCreating="intermediaryDataSource_ObjectCreating">
</asp:ObjectDataSource>


</asp:Content>
