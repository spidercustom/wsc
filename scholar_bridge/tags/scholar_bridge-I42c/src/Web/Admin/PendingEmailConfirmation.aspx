﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PendingEmailConfirmation.aspx.cs" Inherits="ScholarBridge.Web.Admin.PendingEmailConfirmation" Title="Admin | Pending Email Confirmations" %>

<%@ Register TagPrefix="sb" TagName="PendingOrgs" Src="~/Admin/PendingOrgs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
   <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderGeneralImage.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderGeneralImage.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Organizations Pending Email Confirmation</h3>
    <sb:PendingOrgs id="pendingEmailConfirm" runat="server" LinkTo="~/Admin/DisplayOrganization.aspx" />

</asp:Content>

