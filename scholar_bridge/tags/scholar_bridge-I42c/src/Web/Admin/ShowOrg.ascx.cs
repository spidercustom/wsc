﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Web.Admin
{
    public partial class ShowOrg : System.Web.UI.UserControl
    {
        public Organization Organization { get; set; }

		public IProviderService ProviderService { get; set; }

		public IIntermediaryService IntermediaryService { get; set; }

		public IUserContext UserContext { get; set; }

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (Organization.AdminNotes != null)
				notesLiteral.Text = Organization.AdminNotes.ToStringTableFormat();
		}
		
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (null != Organization)
                {
                    statusLabel.Text = Organization.ApprovalStatus.GetDisplayName();
                    adminConfirmLabel.Text = Organization.AdminUser.IsActive.ToString();
					adminNameLabel.Text = Organization.AdminUser.Name.ToString();
					orgNameLabel.Text = Organization.Name;
					emailLiteral.Text = "<a href='mailto:" + Organization.AdminUser.Email + "'>" + Organization.AdminUser.Email + "</a>";
					taxIdLabel.Text = Organization.TaxId;

					if (!(Organization.Website == null))
					{
						websiteLabel.Text = Organization.Website.ToUpper().StartsWith("HTTP")
													   ? Organization.Website
													   : "http://" + Organization.Website;

						websiteLabel.NavigateUrl = websiteLabel.Text;
					}
					addressLabel.Text = Organization.Address == null ? null : Organization.Address.ToString();
                    phoneLabel.Text = Organization.Phone == null ? null : Organization.Phone.ToString();
                    faxLabel.Text = Organization.Fax == null ? null : Organization.Fax.ToString();
                    otherPhoneLabel.Text = Organization.OtherPhone == null ? null : Organization.OtherPhone.ToString();
                    //                existingNotesTb.Text = Organization.AdminNotes;
                }
            }
        }
		protected void addNotes_Click(object sender, EventArgs e)
		{
			Organization.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
			Organization.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
			if (Organization is Domain.Provider )
				ProviderService.Update((Domain.Provider)Organization);
			else
			{
				IntermediaryService.Update((Domain.Intermediary)Organization);
			}
			newNotesTb.Text = null;
		}


    }
}