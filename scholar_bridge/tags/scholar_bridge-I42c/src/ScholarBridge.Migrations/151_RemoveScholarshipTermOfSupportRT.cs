﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(151)]
    public class RemoveScholarshipTermOfSupportRT : Migration
    {
        private const string TABLE_NAME = "SBScholarshipTermOfSupportRT";
        public override void Up()
        {
            Database.RemoveTable(TABLE_NAME);
        }

        public override void Down()
        {
            var addTableMigration = new AddScholarshipTermOfSupport {Database = Database};
            addTableMigration.Up();
        }
    }
}