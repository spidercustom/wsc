﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(29)]
    public class ChangeScholarshipAddDonor : Migration
    {
        private readonly Column[] DonorColumns = new[]
        {
            new Column("DonorFirstName", DbType.String, 50),
            new Column("DonorMiddleName", DbType.String, 50), 
            new Column("DonorLastName", DbType.String, 50), 
            new Column("DonorAddressStreet1", DbType.String, 50), 
            new Column("DonorAddressStreet2", DbType.String, 50), 
            new Column("DonorAddressCity", DbType.String, 50), 
            new Column("DonorAddressPostalCode", DbType.String, 10), 
            new Column("DonorAddressState", DbType.String, 2), 
            new Column("DonorPhoneNumber", DbType.String, 10),
            new Column("DonorEmail", DbType.String, 50)
        };

        public override void Up()
        {
            Array.ForEach(DonorColumns, 
                column => Database.AddColumn(AddScholarship.TABLE_NAME, column));
        }

        public override void Down()
        {
            Array.ForEach(DonorColumns,
                column => Database.RemoveColumn(AddScholarship.TABLE_NAME, column.Name));
        }
    }
}
