﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(279)]
    public class AddIsServiceToApplication : Migration
    {
        private const string TABLE_NAME = "SBApplication";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("IsService", DbType.Boolean,ColumnProperty.Null));
            Database.AddColumn(TABLE_NAME, new Column("IsWorking", DbType.Boolean, ColumnProperty.Null));
            Database.AddColumn(TABLE_NAME, new Column("WorkHistory", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("ServiceHistory", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("CompanyOther", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("AddressCity", DbType.String, 50));
            Database.AddColumn(TABLE_NAME, new Column("AddressCounty", DbType.String, 50));
            Database.AddColumn(TABLE_NAME, new Column("ACTCommulative", DbType.Int32, ColumnProperty.Null));
            Database.AddColumn(TABLE_NAME, new Column("SATCommulative", DbType.Int32, ColumnProperty.Null));

            Database.RemoveColumn(TABLE_NAME, "APCredits");
            Database.RemoveColumn(TABLE_NAME, "IBCredits");

            Database.AddColumn(TABLE_NAME, new Column("APCredits", DbType.Boolean, 50));
            Database.AddColumn(TABLE_NAME, new Column("IBCredits", DbType.Boolean, 50));

            Database.AddColumn(TABLE_NAME, new Column("HonorsDetail", DbType.String, 50, ColumnProperty.Null));
            Database.AddColumn(TABLE_NAME, new Column("APCreditsDetail", DbType.String, 50, ColumnProperty.Null));
            Database.AddColumn(TABLE_NAME, new Column("IBCreditsDetail", DbType.String, 50, ColumnProperty.Null));

            Database.AddColumn(TABLE_NAME, new Column("TypeOfCollegeStudent", DbType.Int32, ColumnProperty.Null));

            Database.AddColumn(TABLE_NAME, new Column("IsCollegeAppliedInWashington", DbType.Boolean, 50));
            Database.AddColumn(TABLE_NAME, new Column("IsCollegeAppliedOutOfState", DbType.Boolean, 50));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "IsService");
            Database.RemoveColumn(TABLE_NAME, "IsWorking");
            Database.RemoveColumn(TABLE_NAME, "WorkHistory");
            Database.RemoveColumn(TABLE_NAME, "ServiceHistory");
            Database.RemoveColumn(TABLE_NAME, "AddressCity");
            Database.RemoveColumn(TABLE_NAME, "AddressCounty");
            Database.RemoveColumn(TABLE_NAME, "ACTCommulative");
            Database.RemoveColumn(TABLE_NAME, "SATCommulative");

            Database.RemoveColumn(TABLE_NAME, "APCredits");
            Database.RemoveColumn(TABLE_NAME, "IBCredits");
            Database.RemoveColumn(TABLE_NAME, "HonorsDetail");
            Database.RemoveColumn(TABLE_NAME, "APCreditsDetail");
            Database.RemoveColumn(TABLE_NAME, "IBCreditsDetail");
            Database.RemoveColumn(TABLE_NAME, "TypeOfCollegeStudent");
            Database.RemoveColumn(TABLE_NAME, "IsCollegeAppliedInWashington");
            Database.RemoveColumn(TABLE_NAME, "IsCollegeAppliedOutOfState");
        }
    }
}