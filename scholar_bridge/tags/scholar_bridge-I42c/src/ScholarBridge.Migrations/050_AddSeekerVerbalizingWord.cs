﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(50)]
    public class SeekerVerbalizingWord : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "SeekerVerbalizingWord";
        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
