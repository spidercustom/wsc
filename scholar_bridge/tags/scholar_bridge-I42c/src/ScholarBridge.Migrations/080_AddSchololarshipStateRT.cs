﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(80)]
    public class AddSchololarshipStateRT : Migration
    {
        public const string TABLE_NAME = "SBScholarshipStateRT";

        protected static readonly string[] COLUMNS = new[] { "ScholarshipId", "StateAbbreviation" };
        protected static readonly string FK_SCHOLARSHIP = string.Format("FK_{0}_SBScholarship", TABLE_NAME);
        protected static readonly string FK_STATE = string.Format("FK_{0}_SBStateLUT", TABLE_NAME);


        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKey),
                              new Column(COLUMNS[1], DbType.String, 2, ColumnProperty.PrimaryKey)
                );

            Database.AddForeignKey(FK_SCHOLARSHIP, TABLE_NAME, COLUMNS[0], "SBScholarship", "SBScholarshipID");
            Database.AddForeignKey(FK_STATE, TABLE_NAME, COLUMNS[1], "SBStateLUT", "Abbreviation");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_SCHOLARSHIP);
            Database.RemoveForeignKey(TABLE_NAME, FK_STATE);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}
