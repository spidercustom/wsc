﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(124)]
	public class AddSeekerAddressAndGender : Migration
	{
		private const string SEEKER_TABLE_NAME = "SBSeeker";
		private static readonly Column ADDRESS_1_COLUMN =
			new Column("AddressStreet1", DbType.String, 50, ColumnProperty.Null);
		private static readonly Column ADDRESS_2_COLUMN =
			new Column("AddressStreet2", DbType.String, 50, ColumnProperty.Null);
		private static readonly Column ADDRESS_CITY_COLUMN =
			new Column("AddressCity", DbType.String, 50, ColumnProperty.Null);
		private static readonly Column ADDRESS_STATE_COLUMN =
			new Column("AddressState", DbType.String, 2, ColumnProperty.Null);
		private static readonly Column ADDRESS_POSTALCODE_COLUMN =
			new Column("AddressPostalCode", DbType.String, 10, ColumnProperty.Null);
		private static readonly Column GENDER_CODE =
			new Column("Gender", DbType.Int32, ColumnProperty.Null);
		private static readonly Column OTHER_ETHNICITY =
			new Column("EthnicityOther", DbType.String, 250, ColumnProperty.Null);
		private static readonly Column OTHER_RELIGION =
			new Column("ReligionOther", DbType.String, 250, ColumnProperty.Null);

		public override void Up()
		{
			Database.AddColumn(SEEKER_TABLE_NAME, ADDRESS_1_COLUMN);
			Database.AddColumn(SEEKER_TABLE_NAME, ADDRESS_2_COLUMN);
			Database.AddColumn(SEEKER_TABLE_NAME, ADDRESS_CITY_COLUMN);
			Database.AddColumn(SEEKER_TABLE_NAME, ADDRESS_STATE_COLUMN);
			Database.AddColumn(SEEKER_TABLE_NAME, ADDRESS_POSTALCODE_COLUMN);
			Database.AddColumn(SEEKER_TABLE_NAME, GENDER_CODE);
			Database.AddColumn(SEEKER_TABLE_NAME, OTHER_ETHNICITY);
			Database.AddColumn(SEEKER_TABLE_NAME, OTHER_RELIGION);
		}

		public override void Down()
		{
			Database.RemoveColumn(SEEKER_TABLE_NAME, ADDRESS_1_COLUMN.Name);
			Database.RemoveColumn(SEEKER_TABLE_NAME, ADDRESS_2_COLUMN.Name);
			Database.RemoveColumn(SEEKER_TABLE_NAME, ADDRESS_CITY_COLUMN.Name);
			Database.RemoveColumn(SEEKER_TABLE_NAME, ADDRESS_STATE_COLUMN.Name);
			Database.RemoveColumn(SEEKER_TABLE_NAME, ADDRESS_POSTALCODE_COLUMN.Name);
			Database.RemoveColumn(SEEKER_TABLE_NAME, GENDER_CODE.Name);
			Database.RemoveColumn(SEEKER_TABLE_NAME, OTHER_ETHNICITY.Name);
			Database.RemoveColumn(SEEKER_TABLE_NAME, OTHER_RELIGION.Name);
		}
	}
}