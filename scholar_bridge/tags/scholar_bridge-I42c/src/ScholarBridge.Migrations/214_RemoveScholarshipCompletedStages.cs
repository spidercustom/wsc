﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(214)]
    public class RemoveScholarshipCompletedStages : Migration
    {

        public override void Up()
        {
            var recreateTableMigration = new ScholarshipStageCompletionState { Database = Database };
            recreateTableMigration.Down();
        }

        public override void Down()
        {
            var recreateTableMigration = new ScholarshipStageCompletionState {Database = Database};
            recreateTableMigration.Up();
        }
    }
}