﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(217)]
    public class ChangeSeekerAddProfileActivatedOn : Migration
    {
        public override void Up()
        {
            Database.AddColumn(AddSeekerTable.TABLE_NAME, new Column("ProfileActivatedOn", DbType.DateTime));
            Database.Update(AddSeekerTable.TABLE_NAME, new[] {"ProfileActivatedOn"},
                            new[] { DateTime.Now.ToString("s") },
                            "Stage = 'Published'");
        }

        public override void Down()
        {
            Database.RemoveColumn(AddSeekerTable.TABLE_NAME, "ProfileActivatedOn");
        }
    }
}