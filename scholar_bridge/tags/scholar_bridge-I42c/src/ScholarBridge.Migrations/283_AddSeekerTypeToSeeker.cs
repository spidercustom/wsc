﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(283)]
	public class AddSeekerTypeToSeeker : Migration
    {
		private const string TABLE_NAME = "SBSeeker";
		private const string COLUMN_NAME = "SeekerType";

        public override void Up()
        {
			Database.AddColumn(TABLE_NAME, COLUMN_NAME, DbType.String);
        	SetSeekerTypeToStudent();
			Column typeColumn = Database.GetColumnByName(TABLE_NAME, COLUMN_NAME);
        	typeColumn.ColumnProperty = ColumnProperty.NotNull;
        	typeColumn.Type = DbType.String;
			Database.ChangeColumn(TABLE_NAME, typeColumn);
        }

        public override void Down()
        {
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
        }

		/// <summary>
		/// set all seekers to 'Student' type for now.
		/// </summary>
		private void SetSeekerTypeToStudent()
		{
			Database.ExecuteNonQuery(@"
update SBSeeker
set SeekerType = 'Student'
");
		}

    }
}