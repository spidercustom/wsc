using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(177)]
    public class AddApplicationReligionRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationReligionRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBReligionLUT"; }
        }
    }
}