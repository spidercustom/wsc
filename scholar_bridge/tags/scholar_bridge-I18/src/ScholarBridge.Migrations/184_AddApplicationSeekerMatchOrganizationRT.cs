using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(184)]
    public class AddApplicationSeekerMatchOrganizationRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationSeekerMatchOrganizationRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSeekerMatchOrganizationLUT"; }
        }
    }
}