﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(49)]
    public class AddWorkHour : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "WorkHour";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
