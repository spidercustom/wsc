using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(176)]
    public class AddApplicationEthnicityRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationEthnicityRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBEthnicityLUT"; }
        }
    }
}