using NHibernate.Type;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>MessageType</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class MessageTypeType : EnumStringType
    {
        public MessageTypeType()
            : base(typeof(MessageType), 50)
        {
        }
    }
}