﻿using System;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class ActivateRelationshipAction : IAction
    {
        public IRelationshipService RelationshipService { get; set; }
        public IMessageService MessageService { get; set; }

        public bool SupportsApprove { get { return true; } }
        public bool SupportsReject { get { return true; } }
        public string LastStatus { get; set; }
        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
            if (! (message is RelationshipMessage))
                throw new ArgumentException("Message must be a RelationshipMessage");

            var relationship = RelationshipService.GetByOrganizations(message.From.Organization, message.To.Organization);

			// if the relationship has been denied then we need to re-add the relationship
			if (relationship == null)
			{
				RelationshipMessage relationshipMessage = (RelationshipMessage)message;
				if (relationshipMessage.ActionTaken == MessageAction.Deny)
				{
					relationship = new Relationship();
					relationship.RequestedOn = relationshipMessage.Date;
					if (message.From.Organization is Intermediary)
					{
						relationship.Intermediary = (Intermediary)message.From.Organization;
						relationship.Provider = (Provider)message.To.Organization;
						relationship.Requester = RelationshipRequester.Intermediary;
					}
					else
					{
						relationship.Intermediary = (Intermediary)message.To.Organization;
						relationship.Provider = (Provider)message.From.Organization;
						relationship.Requester = RelationshipRequester.Provider;
					}
				}
				else
					throw new NullReferenceException("RelatedRelationship can not be null");
			}


            relationship.LastUpdate = new ActivityStamp(approver);
            RelationshipService.Approve(relationship);

            message.ActionTaken = MessageAction.Approve;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Relationship Approved";
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
            if (!(message is RelationshipMessage))
                throw new ArgumentException("Message must be a RelationshipMessage");

            var relationship = RelationshipService.GetByOrganizations(message.From.Organization, message.To.Organization);
            if (relationship == null)
                throw new NullReferenceException("RelatedRelationship can not be null");

            relationship.LastUpdate = new ActivityStamp(approver);
            RelationshipService.Reject(relationship);

            message.ActionTaken = MessageAction.Deny;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Relationship Denied";
        }
    }
}
