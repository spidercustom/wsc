﻿using System;
using System.Collections.Specialized;
using System.Web.Security;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Seeker
{
    public partial class Register : Page
    {
        public IUserService UserService { get; set; }
        public ISeekerService SeekerService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

    	protected void RegisterButton_Click(object sender, EventArgs e)
    	{
			Page.Validate("CreateUserWizard1");
			if (!Page.IsValid)
			{
                CaptchaControl1.ClearGuess();
                return;
			}

			var provider = new SpiderMembershipProvider();
			provider.Initialize(string.Empty, new NameValueCollection());
			User user = provider.CreateUserInstance
				(
					UserName.Text, Password.Text, UserName.Text, null, null, false
				);

			if (user == null)
			{
				switch (provider.CreateUserStatus)
				{
					case MembershipCreateStatus.InvalidEmail:
						UserNameValidator.Text = "Email Address is Invalid";
						UserNameValidator.IsValid = false;
						return;
					case MembershipCreateStatus.DuplicateEmail:
                    case MembershipCreateStatus.DuplicateUserName:
						UserNameValidator.Text = "Email Address is already in use by another user.  Please choose another.";
						UserNameValidator.IsValid = false;
						return;
					default:
						throw new ApplicationException("Unhandled User Creation status event: " + provider.CreateUserStatus);
				}
			}
			user.Name.FirstName = FirstName.Text;
			user.Name.MiddleName = MiddleName.Text;
			user.Name.LastName = LastName.Text;

			var seeker = new Domain.Seeker {User = user};

    		SeekerService.SaveNew(seeker);
			CreateUserWizard.ActiveViewIndex = 1; // CompletionView
		}
    }
}
