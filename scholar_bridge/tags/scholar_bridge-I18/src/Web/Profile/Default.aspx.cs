﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Profile
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            editUserName.CurrentUser = UserContext.CurrentUser;
             editUserEmail.CurrentUser = UserContext.CurrentUser;
        }


        protected void editUserName_OnUserSaved(User user)
        {
            SuccessMessageLabel.SetMessage("Your name has been changed.");
            Response.Redirect("~/Profile/");
        }

        protected void ChangePassword1_ChangedPassword(object sender, EventArgs e)
        {
            SuccessMessageLabel.SetMessage("Your password has been changed.");
        }
    }
}