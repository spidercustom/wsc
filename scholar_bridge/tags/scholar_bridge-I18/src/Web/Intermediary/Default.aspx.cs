﻿using System;

namespace ScholarBridge.Web.Intermediary
{
    public partial class Default : System.Web.UI.Page
    {
        public IUserContext UserContext { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!Page.User.Identity.IsAuthenticated)
                Response.Redirect("~/intermediary/anonymous.aspx");
            UserContext.EnsureIntermediaryIsInContext();
		}
    }
}
