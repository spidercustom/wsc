﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Users.Show" Title="Intermediary | Users | Show" %>
<%@ Register TagPrefix="sb" TagName="UserDetails" Src="~/Common/UserDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br /><br /><br />
<h3>User Information</h3>
    <sb:UserDetails ID="userDetails" runat="server" />
    <asp:LinkButton ID="editLinkButton" runat="server" onclick="editLinkButton_Click">Edit User</asp:LinkButton>

    <asp:Button ID="deleteBtn" runat="server" Text="Deactivate User" onclick="deleteBtn_Click" />
    <asp:Button ID="reactivateBtn" runat="server" Text="Reactivate User" onclick="reactivateBtn_Click" />
</asp:Content>

