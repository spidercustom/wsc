﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="ScholarBridge.Web.Provider.Relationships.Create" Title="Provider | Relationships | Create " %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />
<h3>Add Relationship Request</h3>
<br />
<p>Authorize Intermediary to Build New Scholarships?</p>
<br />
<div class="form-iceland-container">
    <div class="form-iceland">
<label for="orgList" >Select Intermediary</label>
<asp:DropDownList ID="orgList" runat="server"></asp:DropDownList>
<asp:RequiredFieldValidator runat="server" ID="validateorganization" ControlToValidate="orgList" />
<br />
<br />
<asp:Button ID="saveBtn" runat="server" Text="Yes"  onclick="saveBtn_Click" UseSubmitBehavior="False"  /> 
<asp:Button ID="cancelBtn" runat="server" Text="No" CausesValidation="false" onclick="cancelBtn_Click" />
</div> 
</div> 
</asp:Content>
