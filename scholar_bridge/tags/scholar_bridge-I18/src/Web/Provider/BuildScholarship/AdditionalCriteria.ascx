﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteria.ascx.cs"
    Inherits="ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<h3>Build Scholarship – Criteria</h3>
<p>
    Please indicate here what additional materials and information the applicant needs
    to provide. Each question can be 200 characters maximum.</p>

<div class="form-iceland-container">
    <div class="form-iceland">
        <p class="form-section-title">Additional Requirements with Scholarship Application</p>
        <div class="control-set">
            <asp:CheckBoxList ID="AdditionalRequirements" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" />
        </div>
        <br />
        <hr />

        <p class="form-section-title">Scholarship Specific Questions</p>
        <table width="90%" class="control-set">
            <tr>
                <td>
                    <asp:GridView ID="questionsGrid" runat="server" ShowFooter="true" AutoGenerateColumns="False"
                        DataSourceID="QuestionSource">
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <th colspan="2">
                                        
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine"
                                            ID="questionEmptyAddTextBox" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="addEmptyQuestionButton" runat="server" Text="Add" OnClick="addEmptyQuestionButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <EditItemTemplate>
                                    <asp:Label ID="displayOrderEditLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="questionOrderHidden" Value='<%# Bind("DisplayOrder") %>' />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="displayOrderLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>' /><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Questions">
                                <EditItemTemplate>
                                    <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine"
                                        ID="questionTextBox" runat="server" Text='<%# Bind("QuestionText") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="questionTextLabel" runat="server" Text='<%# Bind("QuestionText") %>' /><br />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <br />
                                    <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine"
                                        ID="questionAddTextBox" runat="server"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="updateQuestionButton" runat="server" CausesValidation="True"
                                        CommandName="Update" Text="Update" CommandArgument='<%# Bind("DisplayOrder") %>'></asp:LinkButton>
                                    <asp:LinkButton ID="cancelQuestionUpdateButton" runat="server" CausesValidation="False"
                                        CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="editQuestionButton" runat="server" CausesValidation="False" CommandName="Edit"
                                        Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="addQuestionButton" runat="server" Text="Add" OnClick="addQuestionButton_Click" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="QuestionSource" runat="server" SelectMethod="Select" UpdateMethod="Update"
                        TypeName="ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria+QuestionDataProvider"
                        OnObjectCreating="QuestionSource_ObjectCreating"></asp:ObjectDataSource>
                </td>
            </tr>
        </table>
        
        <br />
        <hr />

        <script type="text/javascript">
            $(document).ready(function() {

                var inlineProgressBar = NeatUploadPB.prototype.Bars['<%= AttachFileProgressBar.ClientID %>'];
                var origDisplay = inlineProgressBar.Display;
                inlineProgressBar.Display = function() {
                    var elem = document.getElementById(this.ClientID);
                    elem.parentNode.style.display = "block";
                    origDisplay.call(this);
                }
                inlineProgressBar.EvalOnClose = "NeatUploadMainWindow.document.getElementById('" + inlineProgressBar.ClientID + "').parentNode.style.display = \"none\";";
            });
        </script>

        <p class="form-section-title">Upload scholarship application forms</p>
        
        <p>These forms will be available to scholarship seekers to download.</p>
        <div style="display: none;">
            <Upload:ProgressBar ID="AttachFileProgressBar" runat="server" Inline="true" />
        </div>
        <label for="AttachFile">Select File:</label>
        <Upload:InputFile ID="AttachFile" runat="server" />
        <br />
        <label for="AttachmentComments">Instructions for applicant:</label>
        <asp:TextBox ID="AttachmentComments" runat="server" />
        <elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments"
            PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment" />
        <span class="noteBene">Please enter instructions for applicant. This will accompany the form.</span>
        <br />
        <asp:Button ID="UploadFile" runat="server" Text="Attach File" OnClick="UploadFile_Click" />
        <br />
        <asp:Label ID="fileTooBigLbl" runat="server" Visible="false" CssClass="error" Text="The file you tried to upload is too large. It should be less than 10MB." />
        <br />

        <asp:ListView ID="attachedFiles" runat="server" OnItemDeleting="attachedFiles_OnItemDeleting">
            <LayoutTemplate>
                <table class="sortableTable">
                    <thead>
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                File
                            </th>
                            <th>
                                Size
                            </th>
                            <th>
                                Type
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton ID="deleteAttachmentBtn" runat="server" Text="Delete" CommandName="Delete"
                            CommandArgument='<%# Eval("Id")%>' />
                    </td>
                    <td>
                        <%# Eval("Name") %>
                    </td>
                    <td>
                        <%# Eval("DisplaySize") %>
                    </td>
                    <td>
                        <%# Eval("MimeType") %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</div>