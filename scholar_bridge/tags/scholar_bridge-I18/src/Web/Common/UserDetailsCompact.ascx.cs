﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class UserDetailsCompact : UserControl
    {
        public User UserToView { get; set; }
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (null == UserToView)
                {
                    errorMessage.Visible = true;
                    return;
                }

                nameLbl.Text = UserToView.Name == null ? UserToView.Username : UserToView.Name.NameFirstLast;
                emailAddressLbl.Text = UserToView.Email;

                phoneLbl.Text = GetPhone(UserToView.Phone);
                faxLbl.Text = GetPhone(UserToView.Fax);
                otherPhoneLbl.Text = GetPhone(UserToView.OtherPhone);

                SetNullableDate(memberSinceLbl, UserToView.CreationDate);
                SetNullableDate(lastLoginLbl, UserToView.LastLoginDate);
            }
        }

		private static string GetPhone(Domain.Contact.PhoneNumber phoneNumber)
		{
			return phoneNumber == null ? string.Empty : phoneNumber.FormattedPhoneNumber;
		}

        private static void SetNullableDate(ITextControl lbl, DateTime? date)
        {
            if (date.HasValue)
            {
                lbl.Text = date.Value.ToString("M/dd/yyyy HH:mm");
            }
        }
    }
}