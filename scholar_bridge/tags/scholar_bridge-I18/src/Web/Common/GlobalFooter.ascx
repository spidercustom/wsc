﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalFooter.ascx.cs" Inherits="ScholarBridge.Web.Common.GlobalFooter" %>
<DIV class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</DIV>
<DIV class="GreenFooterLinks"><A href="">About Us</A>  |  <A href="<%= ResolveUrl("~/PressRoom/") %>">Press Room</A>  |  <A href="<%= ResolveUrl("~/Terms.aspx") %>">Terms and Conditions</A>  |  <A href="#">Contact Us</A>  |  <A href="#">Sitemap</A>  |  <A href="#">Resources</A>  |  <A href="#">Help</A></DIV>

