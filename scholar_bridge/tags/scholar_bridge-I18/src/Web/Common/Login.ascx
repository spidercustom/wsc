﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ScholarBridge.Web.Common.Login" %>
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login   ID="Login1" runat="server" onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError"  FailureText="(!) Invalid email or password. Please try again."   >
            <%--UserNameLabelText=""  
            PasswordRecoveryUrl="~/ForgotPassword.aspx"  
            LoginButtonImageUrl="~/images/Btn_Login.gif"
            PasswordLabelText=""
            RememberMeText="Remember my username on this computer?" 
            TitleTextStyle-CssClass="loginTitleText" 
            onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError" 
            LoginButtonType="Image" 
            DisplayRememberMe="False">--%>
           
            
     <LayoutTemplate >
          <table cellpadding="0" cellspacing="0" border="0">
                 <tr>
                     <td colspan="3" class="LoginHeaderText">Returning User? Log in here.</td>
                 </tr>
                 <tr>
                     <td><asp:Textbox id="UserName" text="Email Address" runat="server" />&nbsp;&nbsp;</td>
                     <td><asp:TextBox TextMode="Password"  id="Password" text="password" runat="server" /></td>
                     <td>&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="Login"  CommandName="Login" runat="server" ImageUrl ="~/images/Btn_OverAllHomepageLogin.gif" width="91px" height="33px" ImageAlign="AbsMiddle"/></td>
                 </tr>
                  <tr>
                  <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr> 
                 <tr>
               
                     <td colspan="3" class="LoginErrorMessage"> <asp:Literal ID="FailureText"  runat="Server"  /></td>
                 </tr>
          </table>
     </LayoutTemplate>
     
     </asp:Login>
    </AnonymousTemplate>    
</asp:LoginView>