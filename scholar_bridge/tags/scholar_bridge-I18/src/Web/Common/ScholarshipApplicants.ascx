﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipApplicants.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipApplicants" %>

<%@ Register src="~/Common/CalendarControl.ascx" tagname="CalendarControl" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<div class="form-iceland-container">
    <div class="search exclude-in-print form-iceland">
        <p class="form-section-title">Search</p>
        
        <label for="<%= searchApplicantsTxt.ClientID %>">Search by Applicant Last Name</label>
        <asp:TextBox ID="searchApplicantsTxt" runat="server" title="Last Name"/> 
        <sbCommon:AnchorButton ID="searchApplicants" runat="server" Text="Search" 
            onclick="searchApplicants_Click" /> <sbCommon:AnchorButton ID="clearSearchBtn" 
            runat="server" Text="Clear Search" onclick="clearSearchBtn_Click" />
        <br />
        <hr />
        <sbCommon:AnchorButton ID="printWindow" runat="server" Text="Print" />
        <br />
        <hr />
    </div>


    <br />
    <asp:HiddenField ID="hiddenfinalist" runat="server" />
    <asp:HiddenField ID="hiddenawardstatus" runat="server" />
    

    <div>
        <p class="form-section-title">Award Status</p>
        <p>
            Scholarship applications submitted. Sort by any field. Check finalists as you evaluate applicants. When ready, update the award status:
        </p>
        <dl>
            <dt>Offered</dt>
            <dd>Indicates Scholarship offered to the applicant – generates a message to the applicants in box.</dd>
            <dt>Awarded</dt>
            <dd>Indicates Scholarship accepted by applicant (off line) – generates a message to applicants in box.</dd>
            <dt>Not Awarded</dt>
            <dd>Indicates Scholarship was offered to an applicant but not awarded to that applicant.</dd> 
        </dl>
    </div>

    <br />

    <div id="closePeriod" title="Confirm delete" style="display:none">
        No applicants have been updated to an award status of Awarded. <br />
        Are you sure you want to indicate the award period is closed?
    </div>   
    <br /><hr />

    <div class="form-iceland">
        <p class="form-section-title">Award Period Close Date</p>
        <p>Update this date when all awards for the current award period have been awarded. This will close the scholarship and send a message to applicants that the award period is closed.</p>
        <br />
        <label for="<%= awardPeriodCloseDate.ClientID %>">Award Period Close Date</label>
        <sb:CalendarControl ID="awardPeriodCloseDate" runat="server" /> 
        <br />
        
        <sbCommon:ConfirmButton ID="saveAwardPeriodDate" runat="server" Text="Save" OnClickConfirm="saveAwardPeriodDate_Click" Width="60px"   ConfirmMessageDivID="closePeriod"/>
        <br /><hr />
    </div>

    <br />

    <asp:ListView ID="lstApplicants" runat="server" onitemdatabound="lstApplicants_ItemDataBound">
        <LayoutTemplate>
        <table class="sortableTable">
                <thead>
                <tr>
                    <th>Finalist</th>
                    <th>Award Status</th>
                    <th>Applicant Name</th>
                    <th>Date Submitted</th>
                    <th>Required Criteria</th>
                    <th>Preferred Criteria</th>
                    <th>Attachments</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        </LayoutTemplate>
              
        <ItemTemplate>
        <tr class="row">
            <td><asp:CheckBox ID="appFinalistCheckBox" runat="server" AutoPostBack="true"/></td>
            <td><asp:DropDownList ID="awardStatusDdl" runat="server" AutoPostBack="true" /></td>
            <td><asp:HyperLink id="linkToApplication" runat="server"><%# Eval("ApplicantName.NameLastFirst")%></asp:HyperLink></td>
            <td><%# Eval("SubmittedDate", "{0:MM/dd/yyyy}")%></td>
            <td><%# Eval("MinumumCriteriaString")%></td>
            <td><%# Eval("PreferredCriteriaString")%></td>
            <td><asp:Button ID="attachmentsBtn" runat="server" /></td>
        </tr>
        </ItemTemplate>
        
        <EmptyDataTemplate>
            <p><asp:label ID="emptySearchLabel" runat="server"><%= EmptyMessage %></asp:label></p>
        </EmptyDataTemplate>
    </asp:ListView>

    <sbCommon:AnchorButton ID="downloadAllBtn" runat="server" Text="Download All Attachments" OnClick="downloadAllBtn_OnClick" CssClass="exclude-in-print"/>
    <sbCommon:AnchorButton ID="downloadAllFinalistBtn" runat="server" Text="Download All Finalist Attachments" OnClick="downloadAllFinalistBtn_OnClick"  CssClass="exclude-in-print"/>

    <div class="pager">
        <asp:DataPager runat="server" ID="pager"  PagedControlID="lstApplicants" PageSize="25" onprerender="pager_PreRender">
            <Fields>
                <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
                <asp:NextPreviousPagerField />
            </Fields>
        </asp:DataPager>
    </div> 

</div>