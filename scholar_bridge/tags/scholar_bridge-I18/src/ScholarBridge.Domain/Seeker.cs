using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;
using SupportedSituation=ScholarBridge.Domain.ScholarshipParts.SupportedSituation;

namespace ScholarBridge.Domain
{
    public class Seeker
    {
        public const string PROFILE_ACTIVATION_RULESET = "Profile-Activation";
        
        public Seeker()
        {
        	InitializeMembers();
        }

		private void InitializeMembers()
		{
			Ethnicities = new List<Ethnicity>();
			Religions = new List<Religion>();
            Sports = new  List<Sport>();

            AcademicAreas = new  List<AcademicArea>();
            Careers =new List<Career>();
            CommunityServices =new List<CommunityService>();
            Hobbies =new List<SeekerHobby>();
            Clubs = new List<Club>();
            MatchOrganizations =new List<SeekerMatchOrganization>();
            AffiliationTypes =new List<AffiliationType>();
            WorkTypes =new List<WorkType>();
            WorkHours =new List<WorkHour>();
            ServiceTypes =new List<ServiceType>();
            ServiceHours =new List<ServiceHour>();
			Address = new SeekerAddress();            
            SupportedSituation=new SupportedSituation();
			Attachments = new List<Attachment>();
		}

        public virtual int Id { get; set; }
		public virtual User User { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual PhoneNumber MobilePhone { get; set; }
        public virtual DateTime? DateOfBirth { get; set; }
        
        [StringLengthValidator(0, 100)]
		public virtual County County
		{
			get; set;
		}

		public virtual DateTime? ProfileActivatedOn { get; set; }
		public virtual DateTime? ProfileLastUpdateDate { get; set; }
		private SeekerAddress _address;

        public virtual SeekerAddress Address 
		{ 
			get
			{
				if (_address == null)
                    _address = new SeekerAddress();
				return _address;
			}
			 set
			 {
			 	_address = value;
			 }
		}

        public virtual PersonName Name { get { return null == User ? null : User.Name; } }

        public virtual bool FirstGeneration { get; set; }

        public virtual Genders Gender { get; set; }

        public virtual SeekerStages Stage { get; set; }

        public virtual CurrentSchool CurrentSchool { get; set; }

        [StringLengthValidator(0, 1500)]
        public virtual string PersonalStatement { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyChallenge { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyGift { get; set; }

        public virtual SeekerAcademics SeekerAcademics { get; set; }

        [StringLengthValidator(0, 100)]
        public virtual string Words { get; set; }
        [StringLengthValidator(0, 100)]
        public virtual string Skills { get; set; }

		public virtual IList<Ethnicity> Ethnicities { get; set; }
        [StringLengthValidator(0, 250)]
		public virtual string EthnicityOther { get; set; }

        public virtual IList<Religion> Religions { get; set; }
        [StringLengthValidator(0, 250)]
		public virtual string ReligionOther { get; set; }
        
        public virtual IList<Sport> Sports { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string SportOther { get; set; }

        public virtual IList<AcademicArea> AcademicAreas { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string AcademicAreaOther { get; set; }

        public virtual IList<Career> Careers { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CareerOther { get; set; }

        public virtual IList<CommunityService> CommunityServices { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CommunityServiceOther { get; set; }

        public virtual IList<SeekerHobby> Hobbies { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string HobbyOther { get; set; }

        public virtual IList<Club> Clubs { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ClubOther { get; set; }

        public virtual IList<SeekerMatchOrganization> MatchOrganizations { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string MatchOrganizationOther { get; set; }

        public virtual IList<AffiliationType> AffiliationTypes { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string AffiliationTypeOther { get; set; }

        public virtual IList<WorkType> WorkTypes { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string WorkTypeOther { get; set; }

        public virtual IList<WorkHour> WorkHours { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string WorkHourOther { get; set; }

        public virtual IList<ServiceType> ServiceTypes { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ServiceTypeOther { get; set; }

        public virtual IList<ServiceHour> ServiceHours { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ServiceHourOther { get; set; }

        public virtual SupportedSituation SupportedSituation { get; set; }

		public virtual IList<Attachment> Attachments { get; protected set; }

        public virtual DateTime? LastMatch { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual bool? HasFAFSACompleted { get; set; }
        public virtual decimal? ExpectedFamilyContribution { get; set; }
        
        public virtual ValidationResults ValidateActivation()
        {
            var results = Validate(PROFILE_ACTIVATION_RULESET);

            var currentSchool = CurrentSchool ?? new CurrentSchool();
            results.AddAllResults(currentSchool.ValidateActivation());

            var seekerAcademics = SeekerAcademics ?? new SeekerAcademics();
            results.AddAllResults(seekerAcademics.ValidateActivation());

            var name = Name ?? new PersonName();
            results.AddAllResults(name.ValidateAsRequired());

            var address = Address ?? new SeekerAddress();
            results.AddAllResults(address.ValidateAsRequired());

            var supportedSituation = SupportedSituation ?? new SupportedSituation();
            results.AddAllResults(supportedSituation.ValidateAsRequired());
            
            return results;
        }

        private ValidationResults Validate(string ruleSet)
        {
            var validator = ValidationFactory.CreateValidator<Seeker>(ruleSet);
            return validator.Validate(this);
        }

        public virtual int GetPercentComplete()
        {
           //Calculate Percentage based on seeker object fields
          var completedCount = 0;
          var allCount = 0;
        
            
            allCount++; 
            if (!String.IsNullOrEmpty( AcademicAreaOther ) || AcademicAreas.Count>0)
                completedCount++;

            if (Address != null)
            {
                allCount++;
                if (!String.IsNullOrEmpty(Address.GetCity))
                    completedCount++;

                allCount++;
                if (!String.IsNullOrEmpty(Address.Street))
                    completedCount++;

                allCount++;
                if (!(Address.State==null ))
                    completedCount++;

                allCount++;
                if (!(Address.PostalCode == null))
                    completedCount++;

            }

            allCount++;
            if (!String.IsNullOrEmpty(AffiliationTypeOther ) || AffiliationTypes.Count >0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(CareerOther) || Careers.Count > 0)
                completedCount++;


            allCount++;
            if (!String.IsNullOrEmpty(ClubOther) || Clubs.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(CommunityServiceOther) || CommunityServices.Count > 0)
                completedCount++;


            allCount++;
            if (!(County==null ))
                completedCount++;

            allCount++;
            if (!(CurrentSchool == null))
                completedCount++;

            allCount++;
            if (!(DateOfBirth == null))
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(EthnicityOther ) || Ethnicities.Count > 0)
                completedCount++;

            allCount++;
            if (!(FirstGeneration == null))
                completedCount++;

            allCount++;
            if (!(Gender == null))
                completedCount++;


            allCount++;
            if (!String.IsNullOrEmpty(HobbyOther ) || Hobbies.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(MatchOrganizationOther) || MatchOrganizations.Count > 0)
                completedCount++;


            allCount++;
            if (!(MobilePhone==null))
                completedCount++;
            
            allCount++;
            if (!String.IsNullOrEmpty(MyChallenge))
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(MyGift ))
                completedCount++;
            if (!(Name==null ))
            {
                allCount++;
                if (!String.IsNullOrEmpty(Name.NameFirstLast))
                    completedCount++;
            }

            allCount++;
            if (!String.IsNullOrEmpty(PersonalStatement))
                completedCount++;

            if (!(Phone == null))
            {
                allCount++;
                if (!String.IsNullOrEmpty(Phone.Number))
                    completedCount++;

            }

            allCount++;
            if (!String.IsNullOrEmpty(ReligionOther) || Religions.Count > 0)
                completedCount++;



            if (!(SeekerAcademics== null))
            {

                if (!(SeekerAcademics.ACTScore == null))
                {
                    allCount++;
                    if (!(SeekerAcademics.ACTScore.English==null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.ACTScore.Mathematics == null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.ACTScore.Reading == null))
                        completedCount++;
                    allCount++;
                    if (!(SeekerAcademics.ACTScore.Science == null))
                        completedCount++;
                }

                if (!(SeekerAcademics.SATScore == null))
                {
                    allCount++;
                    if (!(SeekerAcademics.SATScore.CriticalReading == null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.SATScore.Mathematics  == null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.SATScore.Writing  == null))
                        completedCount++;
                }

                allCount++;
                if (!(SeekerAcademics.APCredits == null))
                    completedCount++;

                allCount++;
                if (!(SeekerAcademics.ClassRank == null))
                    completedCount++;


                allCount++;
                if (!String.IsNullOrEmpty(SeekerAcademics.CollegesAcceptedOther) || SeekerAcademics.CollegesAccepted.Count > 0)
                    completedCount++;


                allCount++;
                if (!String.IsNullOrEmpty(SeekerAcademics.CollegesAppliedOther) || SeekerAcademics.CollegesApplied.Count > 0)
                    completedCount++;

                allCount++;
                if (!(SeekerAcademics.GPA == null))
                    completedCount++;
                allCount++;
                if (!(SeekerAcademics.IBCredits == null))
                    completedCount++;
              
            }

            allCount++;
            if (!String.IsNullOrEmpty(ServiceHourOther) || ServiceHours.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(ServiceTypeOther) || ServiceTypes.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(Words))
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(WorkHourOther) || WorkHours.Count> 0)
                completedCount++;


            allCount++;
            if (!String.IsNullOrEmpty(WorkTypeOther) || WorkTypes.Count > 0)
                completedCount++;

            double percent = completedCount * 100;
            percent = percent/allCount;
            return Convert.ToInt32(percent);
        }

        public virtual bool IsPublished()
        {
            return Stage == SeekerStages.Published;
        }
    }
}