using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    class ViewApplicationAction : OpenUrlAction
    {
        private const string VIEW_APPLICATION_URL_TEMPLATE = "~/Seeker/Applications/Show.aspx?aid={0}&print=true";

        public ViewApplicationAction() : base("View") { }

        protected override string ConstructUrl(Match match)
        {
            return VIEW_APPLICATION_URL_TEMPLATE.Build(match.Application.Id);
        }
    }
}