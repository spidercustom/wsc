﻿using System;
using System.Diagnostics;
using ScholarBridge.Business;


namespace ScholarBridge.Web
{
	public partial class ScholarshipList: System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Header.Title = "theWashBoard.org - Complete Scholarship List";
			scholarshipXmlDataSource.DataFile = string.Format("~/_ScholarshipData/{0}", ScholarshipListService.SCHOLARSHIP_LIST_FILE_NAME );
			if (!Page.IsPostBack)
			{

			}
		}
	}
}
