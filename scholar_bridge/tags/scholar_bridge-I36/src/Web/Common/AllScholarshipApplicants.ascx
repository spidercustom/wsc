﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AllScholarshipApplicants.ascx.cs" Inherits="ScholarBridge.Web.Common.AllScholarshipApplicants" %>
<asp:ListView ID="lstApplicants" runat="server">
    <LayoutTemplate>
    <table class="viewonlyTable">
            <thead>
            <tr>
                <th>Applicant Name</th>
                <th>Date Submitted</th>
                <th>Required Criteria</th>
                <th>Preferred Criteria</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><%# Eval("ApplicantName.NameLastFirst")%></td>
        <td><%# Eval("SubmittedDate", "{0:MM/dd/yyyy}")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><%# Eval("ApplicantName.NameLastFirst")%></td>
        <td><%# Eval("SubmittedDate", "{0:MM/dd/yyyy}")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
        <p><asp:label ID="emptySearchLabel" runat="server"><%= EmptyMessage %></asp:label></p>
    </EmptyDataTemplate>
</asp:ListView>