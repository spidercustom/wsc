﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
             
<div class="form-iceland-container">
    <div class="form-iceland">
        <p class="form-section-title">What is your financial need?</p>
        <label for="MyChallengeControl">Describe your financial Challenge:</label>
        <asp:TextBox ID="MyChallengeControl" runat="server" TextMode="MultiLine"  Rows="4" style="width:520px !important;"></asp:TextBox>
        <sb:CoolTipInfo 
            Content="describe your financial need in your own words. Let providers know the challenges you are facing in paying for college. Examples would include employment status, size of family, and other financial obligations." 
            runat="server" />
        <elv:PropertyProxyValidator ID="MyChallengeValidator" runat="server" ControlToValidate="MyChallengeControl" PropertyName="MyChallenge" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <br />
    <hr />
    <br />
    <div class="form-iceland two-columns">     
        <asp:PlaceHolder ID="FAFSAContainer" runat="server">
            <p class="form-section-title"><a href="http://www.fafsa.ed.gov/" class="GreenLink" target="_blank">FAFSA</a> (Free Application for Federal Student Aid)</p>
            <label>Have you completed the <a class="BlueLink"  href="http://www.fafsa.ed.gov">FAFSA</a>?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FAFSACompletedControl" Text="Yes" runat="server" GroupName="FAFSAFilled" />
                <asp:RadioButton ID="FAFSANotCompletedControl" Text="No" runat="server" GroupName="FAFSAFilled"/>
            </div>
            <sb:CoolTipInfo 
                ID="CoolTipInfo1" 
                Content="The FAFSA isn’t just used by schools to determine your eligibility for grants, student loans, and other federal aid. Many scholarship providers will also use the FAFSA to evaluate your need for scholarship aid. While not all scholarships require you to complete the FAFSA, we encourage all students seeking aid to apply. " 
                runat="server" />
           <br />
            <label>What is your FAFSA defined EFC (Expected Family Contribution)?</label> 
            <sandTrap:NumberBox ID="ExpectedFamilyContributionControl" runat="server" Precision="0" Enabled="false"/>
            <br />
        </asp:PlaceHolder>
    </div>
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">
        <asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
            &nbsp;<br />
            <label>Types of Support</label>
            <table>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="TypesOfSupport" runat="server" CssClass="control-set"/>
                    </td>
                    <td valign="top">
                        <sb:CoolTipInfo 
                            ID="CoolTipInfo3" 
                            Content="Some scholarships specify what the funds can be used for. Check all that apply for your situation to ensure you get matched with scholarships that will fund your needs." 
                            runat="server" />
                    </td>
                </tr>
            </table>
        </asp:PlaceHolder>
        
  </div>
</div>
