﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Seeker.Applications
{
    public partial class Show : Page
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        public Application ApplicationToView { get; set; }

        private int ApplicationId
        {
            get
            {
                int applicationId;
                if (!Int32.TryParse(Request.Params["aid"], out applicationId))
                    throw new ArgumentException("Cannot understand value of parameter aid");
                return applicationId;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsInPrintView())
                Response.Redirect("~/Seeker/Applications/show.aspx?aid=" + ApplicationId.ToString() + "&print=true");

            UserContext.EnsureSeekerIsInContext();
            var seeker = UserContext.CurrentSeeker;
            ApplicationToView = ApplicationService.GetById(ApplicationId);
            if (ApplicationToView != null)
            {
                if (!ApplicationToView.Seeker.Id.Equals(seeker.Id))
                    throw new InvalidOperationException("Application does not belong to seeker in context");

                ScholarshipTitleStripeControl.UpdateView(ApplicationToView.Scholarship);
                showApplication.ApplicationToView = ApplicationToView;
            }
        }
    }
}
