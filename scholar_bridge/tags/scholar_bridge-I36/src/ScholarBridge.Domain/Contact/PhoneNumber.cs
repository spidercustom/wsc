﻿using System;
using System.Text.RegularExpressions;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain.Contact
{
    public class PhoneNumber
    {
        private string number;
        private readonly Regex regex = new Regex("[^0-9]");

        public PhoneNumber()
        {
        }

        public PhoneNumber(string number)
        {
            Number = number;
        }

        [ValidatorComposition(CompositionType.Or, MessageTemplate = "The phone number '{0}' is invalid.")]
        [StringLengthValidator(0, 0)]
        [RegexValidator(@"^[1]?[-\s.]?\(?\d{3}\)?[-\s.]?\d{3}[-\s.]?\d{4}$")]
        public virtual string Number
        {
            get { return number; }
            set
            {
                if (string.IsNullOrEmpty(value)) return;

                // strip out everything but the numbers
                number = regex.Replace(value, "");

                // trim the beginning 1
                //if (number.StartsWith("1"))
                //{
                //    number = number.Substring(1);
                //}
            }
        }

        public virtual string FormattedPhoneNumber
        {
            get
            {
                if (null == number || number.Length != 10)
                    return number;

                return String.IsNullOrEmpty(number)
                           ? null
                           : string.Format("{0}-{1}-{2}",
                                           Number.Substring(0, 3),
                                           Number.Substring(3, 3),
                                           Number.Substring(6));
            }
        }

        public override string ToString()
        {
            return FormattedPhoneNumber;
        }
    }
}