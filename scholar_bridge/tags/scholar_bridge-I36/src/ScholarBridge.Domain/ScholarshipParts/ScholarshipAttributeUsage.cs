namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipAttributeUsage<T>
    {
        public virtual T Attribute { get; set; }
        public virtual ScholarshipAttributeUsageType UsageType { get; set; }
    }
}