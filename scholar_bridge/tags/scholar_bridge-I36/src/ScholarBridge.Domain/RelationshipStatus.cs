﻿
namespace ScholarBridge.Domain
{
    public enum RelationshipStatus
    {   
        Pending,
        Active,
        InActive
    }
    
}
