﻿using System.Collections.Generic;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IRelationshipDAL : IDAL<Relationship>
    {
        Relationship FindById(int id);

        IList<Relationship> FindByProvider(Provider provider);
        IList<Relationship> FindByIntermediary(Intermediary intermediary);

        IList<Intermediary> FindNotRelatedByProvider(Provider provider);
        IList<Provider> FindNotRelatedByIntermediary(Intermediary intermediary);

        Relationship Save(Relationship relationship);
        Relationship FindByProviderandIntermediary(Provider provider, Intermediary intermediary);
    }
}
