﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(254)]
    public class ExpandScholarshipQuestionField : Migration
    {
		private const string TABLE = "SBScholarshipQuestion";
		private const string COL_NAME = "QuestionText";

        public override void Up()
        {
            Column nameColumn = Database.GetColumnByName(TABLE, COL_NAME);

        	nameColumn.Size = 500;
			Database.ChangeColumn(TABLE, nameColumn);
        }

        public override void Down()
        {
        	Database.ExecuteNonQuery("update " + TABLE + " set " + COL_NAME + " = Substring(" + COL_NAME + ", 1, 200)");
            Column nameColumn = Database.GetColumnByName(TABLE, COL_NAME);

        	nameColumn.Size = 200;
			Database.ChangeColumn(TABLE, nameColumn);
        }
    }
}
