using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(173)]
    public class AddApplicationAppliedCollegesRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationCollegeAppliedRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCollegeLUT"; }
        }
    }
}