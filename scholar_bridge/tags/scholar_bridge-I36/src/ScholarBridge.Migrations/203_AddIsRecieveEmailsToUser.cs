using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(203)]
    public class AddRecieveEmailNotificationsToUser : Migration
    {
        private const string TABLE_NAME = "SBUser";

        private const string RECIEVE_EMAILS = "IsRecieveEmails";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, RECIEVE_EMAILS, DbType.Boolean, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, RECIEVE_EMAILS);
        }
    }
}