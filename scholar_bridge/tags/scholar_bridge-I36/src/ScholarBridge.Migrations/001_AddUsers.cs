﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(1)]
    public class AddUsers : Migration
    {
        public const string TABLE_NAME = "SB_User";
        public const string PRIMARY_KEY_COLUMN = "Id";
        protected static readonly string FK_LAST_UPDATED_BY = string.Format("FK_{0}_LastUpdatedBy", TABLE_NAME);

        protected static readonly string[] COLUMNS = new[] { PRIMARY_KEY_COLUMN, "Username", "Password", "Email", "Question", "Answer",
                                "IsApproved","IsLockedOut","IsActive","IsDeleted","IsPasswordReset","IsOnLine",
                                "CreationDate","LastActivityDate","LastLoginDate","LastLockedOutDate","LastPasswordChangeDate",
                                "PasswordFormat","PasswordSalt","Comments",
                                "FailedPasswordAttemptCount","FailedPasswordAttemptWindowStart",
                                "FailedPasswordAnswerAttemptCount","FailedPasswordAnswerAttemptWindowStart",
                                "LastUpdatedBy", "LastUpdateDate"
                                };

        public override void Up()
        {
            int columnCounter = 0;
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[columnCounter++], DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity), /*Id*/
                              new Column(COLUMNS[columnCounter++], DbType.String, 255, ColumnProperty.NotNull), /*Username*/
                              new Column(COLUMNS[columnCounter++], DbType.String, 50, ColumnProperty.NotNull),     /*Password*/
                              new Column(COLUMNS[columnCounter++], DbType.String, 50, ColumnProperty.NotNull),     /*Email*/
                              new Column(COLUMNS[columnCounter++], DbType.String, 255, ColumnProperty.NotNull),    /*Question*/
                              new Column(COLUMNS[columnCounter++], DbType.String, 255, ColumnProperty.NotNull),    /*Answer*/
                              
                              new Column(COLUMNS[columnCounter++], DbType.Boolean , ColumnProperty.Null), /*IsApproved*/
                              new Column(COLUMNS[columnCounter++], DbType.Boolean,  ColumnProperty.Null), /*IsLockedOut*/ 
                              new Column(COLUMNS[columnCounter++], DbType.Boolean,  ColumnProperty.Null), /*IsActive*/ 
                              new Column(COLUMNS[columnCounter++], DbType.Boolean,  ColumnProperty.Null), /*IsDeleted*/ 
                              new Column(COLUMNS[columnCounter++], DbType.Boolean,  ColumnProperty.Null), /*IsPasswordReset*/
                              new Column(COLUMNS[columnCounter++], DbType.Boolean, ColumnProperty.Null), /*IsOnLine*/ 

                              new Column(COLUMNS[columnCounter++], DbType.DateTime ,  ColumnProperty.Null), /*CreationDate*/ 
                              new Column(COLUMNS[columnCounter++], DbType.DateTime,  ColumnProperty.Null), /*LastActivityDate*/ 
                              new Column(COLUMNS[columnCounter++], DbType.DateTime,  ColumnProperty.Null), /*LastLoginDate*/ 
                              new Column(COLUMNS[columnCounter++], DbType.DateTime,  ColumnProperty.Null), /*LastLockedOutDate*/ 
                              new Column(COLUMNS[columnCounter++], DbType.DateTime,  ColumnProperty.Null), /*LastPasswordChangeDate*/
                              
                              new Column(COLUMNS[columnCounter++], DbType.Int32,  ColumnProperty.Null), /*PasswordFormat*/
                              new Column(COLUMNS[columnCounter++], DbType.String, 255, ColumnProperty.Null), /*PasswordSalt*/
                              new Column(COLUMNS[columnCounter++], DbType.String, 500,  ColumnProperty.Null), /*Comments*/

                              new Column(COLUMNS[columnCounter++], DbType.Int32,  ColumnProperty.Null), /*FailedPasswordAttemptCount*/
                              new Column(COLUMNS[columnCounter++], DbType.DateTime,  ColumnProperty.Null), /*FailedPasswordAttemptWindowStart*/
                              new Column(COLUMNS[columnCounter++], DbType.Int32, ColumnProperty.Null), /*FailedPasswordAnswerAttemptCount*/
                              new Column(COLUMNS[columnCounter++], DbType.DateTime,  ColumnProperty.Null), /*FailedPasswordAnswerAttemptWindowStart*/

                              new Column(COLUMNS[columnCounter++], DbType.Int32, ColumnProperty.Null), /*LastUpdatedBy*/
                              new Column(COLUMNS[columnCounter++], DbType.DateTime, ColumnProperty.Null, "(getdate())") /*LastUpdateDate*/
                              
                );

            Database.AddForeignKey(FK_LAST_UPDATED_BY, TABLE_NAME, "LastUpdatedBy", "SB_User", "Id");
            
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_LAST_UPDATED_BY);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}