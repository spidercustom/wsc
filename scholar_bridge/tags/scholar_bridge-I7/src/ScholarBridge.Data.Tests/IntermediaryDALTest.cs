using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class IntermediaryDALTest : TestBase
    {
        public IntermediaryDAL IntermediaryDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }

        [Test]
        public void CanCreateProvider()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual("TestProvider", provider.Name);
            Assert.IsNotNull(provider.Address);
            Assert.IsNotNull(provider.Address.State);
            Assert.AreEqual("WI", provider.Address.State.Abbreviation);
            Assert.IsNotNull(provider.Phone);
        }

        [Test]
        public void CanGetProviderById()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            var foundProvider = IntermediaryDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            Assert.AreEqual(provider.Name, foundProvider.Name);
            Assert.AreEqual(provider.TaxId, foundProvider.TaxId);
            Assert.AreEqual(provider.ApprovalStatus, foundProvider.ApprovalStatus);
        }

        [Test]
        public void ProviderStartsOutAsPendingApproval()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual(ApprovalStatus.PendingApproval, provider.ApprovalStatus);

            var foundProvider = IntermediaryDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            Assert.AreEqual(ApprovalStatus.PendingApproval, foundProvider.ApprovalStatus);
        }

        [Test]
        public void CanFindAllProvidersPendingApproval()
        {

            var initialCount = IntermediaryDAL.FindAllPending().Count;

            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider1 = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);
            var provider2 = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual(initialCount + 2, IntermediaryDAL.FindAllPending().Count);

            provider1.ApprovalStatus = ApprovalStatus.Approved;
            IntermediaryDAL.Update(provider1);

            Assert.AreEqual(initialCount + 1, IntermediaryDAL.FindAllPending().Count);

            provider2.ApprovalStatus = ApprovalStatus.Rejected;
            IntermediaryDAL.Update(provider2);

            Assert.AreEqual(initialCount, IntermediaryDAL.FindAllPending().Count);

        }

        [Test]
        public void CanAddAdminUserToProvider()
        {
            Intermediary intermediary = AddIntermediaryWithAdminUser();

            var foundProvider = IntermediaryDAL.FindById(intermediary.Id);
            Assert.IsNotNull(foundProvider);
            Assert.IsNotNull(foundProvider.AdminUser);
            Assert.IsNotNull(foundProvider.ActiveUsers);
            Assert.AreEqual(1, foundProvider.ActiveUsers.Count);
        }

        [Test]
        public void CanGetProviderByUser()
        {
            Intermediary intermediary = AddIntermediaryWithAdminUser();

            var foundProvider = IntermediaryDAL.FindByUser(intermediary.AdminUser);
            Assert.IsNotNull(foundProvider);
            Assert.AreEqual(intermediary.Id, foundProvider.Id);
        }

        [Test]
        public void can_get_user_by_provider_and_id()
        {
            var intermediary = AddIntermediaryWithAdminUser();

            var foundUser = IntermediaryDAL.FindUserInOrg(intermediary, intermediary.AdminUser.Id);
            Assert.IsNotNull(foundUser);
            Assert.AreEqual(intermediary.AdminUser.Id, foundUser.Id);
        }

        [Test]
        public void can_not_get_user_by_other_provider_and_id()
        {
            var intermediary = AddIntermediaryWithAdminUser();
            var user2 = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary2 = InsertIntermediary(IntermediaryDAL, StateDAL, "New Provider", user2);

            var foundUser = IntermediaryDAL.FindUserInOrg(intermediary2, intermediary.AdminUser.Id);
            Assert.IsNull(foundUser);
        }

        [Test]
        public void delete_user_shows_up_in_deleted_users_collection()
        {
            var intermediary = AddIntermediaryWithAdminUser();
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            intermediary.Users.Add(user);
            IntermediaryDAL.Update(intermediary);

            UserDAL.Delete(user);

            var foundIntermediary = IntermediaryDAL.FindById(intermediary.Id);
            Assert.IsNotNull(foundIntermediary);

            Assert.AreEqual(1, intermediary.ActiveUsers.Count); // Admin user is still in here
            Assert.AreEqual(1, intermediary.DeletedUsers.Count);
        }

        [Test]
        public void CanAddProviderAdminNotes()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);
            
            provider.AppendAdminNotes(user, "Testing the note.");
            IntermediaryDAL.Update(provider);

            provider.AppendAdminNotes(user, "Testing the note again.");
            IntermediaryDAL.Update(provider);

            var foundProvider = IntermediaryDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            StringAssert.Contains("Testing the note.", foundProvider.AdminNotes.ToString());
            StringAssert.Contains("Testing the note again.", foundProvider.AdminNotes.ToString());
        }

        private Intermediary AddIntermediaryWithAdminUser()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);
            intermediary.AdminUser = user;

            IntermediaryDAL.Update(intermediary);
            return intermediary;
        }

        public static Intermediary InsertIntermediary(IIntermediaryDAL intermediaryDAL, StateDAL stateDAL, string name, User user)
        {
            var wi = stateDAL.FindByAbbreviation("WI");
            var intermediary = new Intermediary()
                               {
                                   Name = name,
                                   TaxId = "123-45678",
                                   LastUpdate = new ActivityStamp(user),
                                   Address = new Address
                                                 {
                                                     Street = "123 Foo St.",
                                                     City = "Milwaukee",
                                                     PostalCode = "53212",
                                                     State = wi
                                                 },
                                   Phone = new PhoneNumber("414 431-5593")
                               };

            intermediary = intermediaryDAL.Insert(intermediary);

            Assert.IsNotNull(intermediary);
            Assert.AreNotEqual(0, intermediary.Id);

            return intermediary;
        }
    }
}