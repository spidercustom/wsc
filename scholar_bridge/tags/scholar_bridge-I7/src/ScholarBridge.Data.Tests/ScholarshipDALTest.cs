﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipDALTest : TestBase
    {

        public ProviderDAL ProviderDAL { get; set; }
        public IntermediaryDAL IntermediaryDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        private User user;
        private Provider provider;
        private Intermediary intermediary;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            intermediary = IntermediaryDALTest.InsertIntermediary(IntermediaryDAL, StateDAL, "Test Intermediary", user);
        }

        [Test]
        public void can_create_scholarship()
        {
            Scholarship scholarship = CreateTestObject();
            var newScholarship = InsertScholarship(scholarship);
            Assert.IsNotNull(newScholarship);
            Assert.AreNotEqual(0, newScholarship.Id);
        }

        [Test]
        public void clone_save_test()
        {
            Scholarship firstScholarship = CreateTestObject();
            var firstScholarshipRetrieved = InsertScholarship(firstScholarship);
            var clonedScholarship = (Scholarship)firstScholarshipRetrieved.Clone("c:/temp");
            clonedScholarship.LastUpdate = new ActivityStamp(user);
            clonedScholarship.Stage = ScholarshipStages.GeneralInformation;
            var clonedScholarshipRetrieved = InsertScholarship(clonedScholarship);

            AssertScholarshipsAreSmilar(firstScholarshipRetrieved, clonedScholarshipRetrieved);
        }

        public static void AssertScholarshipsAreSmilar(Scholarship expected, Scholarship actual)
        {
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.MissionStatement, actual.MissionStatement);
            Assert.AreEqual(expected.ProgramGuidelines, actual.ProgramGuidelines);
            Assert.AreEqual(expected.MinimumAmount, actual.MinimumAmount);
            Assert.AreEqual(expected.MaximumAmount, actual.MaximumAmount);
            Assert.AreEqual(AcademicYear.CurrentScholarshipYear, actual.AcademicYear);
        }

        [Test]
        public void can_update_scholarship()
        {
            Scholarship scholarship = CreateTestObject();
            var newScholarship = InsertScholarship(scholarship);
            Assert.IsNotNull(newScholarship);
            Assert.AreNotEqual(0, newScholarship.Id);

            newScholarship.FundingProfile.FundingParameters = new FundingParameters
                                                   {
                                                       AnnualSupportAmount = 100,
                                                       MinimumNumberOfAwards = 1,
                                                       MaximumNumberOfAwards = 5
                                                   };
            ScholarshipDAL.Save(newScholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);

            Assert.AreEqual(100, (double)foundScholarship.FundingProfile.FundingParameters.AnnualSupportAmount, 0.001);
            Assert.AreEqual(1, foundScholarship.FundingProfile.FundingParameters.MinimumNumberOfAwards);
            Assert.AreEqual(5, foundScholarship.FundingProfile.FundingParameters.MaximumNumberOfAwards);
        }

        [Test]
        public void can_create_scholarship_with_definition_of_need()
        {
            Scholarship scholarship = CreateTestObject();
            scholarship.FundingProfile.Need = new DefinitionOfNeed
                                   {
                                       Fafsa = true,
                                       UserDerived = true,
                                       MinimumSeekerNeed = 100,
                                       MaximumSeekerNeed = 200
                                   };

            InsertScholarship(scholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);
            Assert.IsTrue(foundScholarship.FundingProfile.Need.Fafsa);
            Assert.IsTrue(foundScholarship.FundingProfile.Need.UserDerived);

            Assert.AreEqual(100, (double)foundScholarship.FundingProfile.Need.MinimumSeekerNeed, 0.001);
            Assert.AreEqual(200, (double)foundScholarship.FundingProfile.Need.MaximumSeekerNeed, 0.001);
        }

        [Test]
        public void can_create_scholarship_with_fundingparameters()
        {
            Scholarship scholarship = CreateTestObject();
            scholarship.FundingProfile.FundingParameters = new FundingParameters
                    {
                        AnnualSupportAmount = 100,
                        MinimumNumberOfAwards = 1,
                        MaximumNumberOfAwards = 5
                    };

            InsertScholarship(scholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);

            Assert.AreEqual(100, (double)foundScholarship.FundingProfile.FundingParameters.AnnualSupportAmount, 0.001);
            Assert.AreEqual(1, foundScholarship.FundingProfile.FundingParameters.MinimumNumberOfAwards);
            Assert.AreEqual(5, foundScholarship.FundingProfile.FundingParameters.MaximumNumberOfAwards);
        }

        [Test]
        public void can_create_scholarship_with_supportedsituation()
        {
            Scholarship scholarship = CreateTestObject();
            scholarship.FundingProfile.SupportedSituation = new SupportedSituation();

            InsertScholarship(scholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);
            Assert.IsNotNull(foundScholarship.FundingProfile.SupportedSituation);

        }

        [Test]
        public void find_by_name_and_provider()
        {
            Scholarship newScholarship = CreateTestObject();
            InsertScholarship(newScholarship);

            Scholarship scholarship = ScholarshipDAL.FindByBusinessKey(newScholarship.Provider, newScholarship.Name, newScholarship.AcademicYear.Year);
            Assert.IsNotNull(scholarship);

            scholarship = ScholarshipDAL.FindByBusinessKey(newScholarship.Provider, "not-existing-name-of-scholarship-in-provider", newScholarship.AcademicYear.Year);
            Assert.IsNull(scholarship);
        }

        [Test]
        public void find_by_stage_and_provider()
        {
            Scholarship newScholarship = CreateTestObject();
            newScholarship.Stage = ScholarshipStages.GeneralInformation;

            InsertScholarship(newScholarship);

            Scholarship scholarship = ScholarshipDAL.FindByProvider(newScholarship.Provider, newScholarship.Stage)[0];
            Assert.IsNotNull(scholarship);
            var scholarships = ScholarshipDAL.FindByProvider(newScholarship.Provider, ScholarshipStages.BuildScholarshipMatchLogic);
            Assert.IsTrue(scholarships.Count == 0);
        }

        [Test]
        public void find_by_stage_and_intermediary()
        {
            Scholarship newScholarship = CreateTestObject();
            newScholarship.Stage = ScholarshipStages.GeneralInformation;

            InsertScholarship(newScholarship);

            Scholarship scholarship = ScholarshipDAL.FindByIntermediary(newScholarship.Intermediary, newScholarship.Stage)[0];
            Assert.IsNotNull(scholarship);
            var scholarships = ScholarshipDAL.FindByProvider(newScholarship.Provider, ScholarshipStages.BuildScholarshipMatchLogic);
            Assert.IsTrue(scholarships.Count == 0);
        }

        [Test]
        public void can_find_by_provider()
        {
            Scholarship newScholarship = InsertScholarship(CreateTestObject());
            IList<Scholarship> scholarships = ScholarshipDAL.FindByProvider(provider);
            Assert.AreEqual(1, scholarships.Count);
            AssertScholarshipAreSame(newScholarship, scholarships[0]);
        }

        [Test]
        public void can_find_by_intermediary()
        {
            Scholarship newScholarship = InsertScholarship(CreateTestObject());
            IList<Scholarship> scholarships = ScholarshipDAL.FindByIntermediary(intermediary);
            Assert.AreEqual(1, scholarships.Count);
            AssertScholarshipAreSame(newScholarship, scholarships[0]);
        }

        [Test]
        public void can_find_by_id()
        {
            Scholarship newScholarship = InsertScholarship(CreateTestObject());
            Scholarship retrivedScholarship = ScholarshipDAL.FindById(newScholarship.Id);
            AssertScholarshipAreSame(newScholarship, retrivedScholarship);
        }

        [Test]
        public void test_scholarship_completed_test()
        {
            Scholarship scholarship = CreateTestObject();
            Assert.IsNotNull(scholarship.CompletedStages);
            Assert.AreEqual(0, scholarship.CompletedStages.Count);

            scholarship.CompletedStages.Add(ScholarshipStages.GeneralInformation);
            scholarship = InsertScholarship(scholarship);
            var retrievedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(retrievedScholarship.CompletedStages);
            Assert.AreEqual(1, retrievedScholarship.CompletedStages.Count);
            Assert.AreEqual(ScholarshipStages.GeneralInformation, retrievedScholarship.CompletedStages[0]);
        }

        [Test]
        public void can_get_all_closed_since_a_date()
        {
            var since = DateTime.Now.AddDays(-1);
            var initialCount = ScholarshipDAL.FindAllClosedSince(since).Count;

            var s1 = InsertScholarship(CreateTestObject());
            InsertScholarship(CreateTestObject());

            s1.AwardPeriodClosed = DateTime.Now.AddHours(-8);
            ScholarshipDAL.Update(s1);

            var closed = ScholarshipDAL.FindAllClosedSince(since);

            Assert.IsNotNull(closed);
            Assert.AreEqual(initialCount + 1, closed.Count);
        }

        [Test]
        public void can_find_by_organizations()
        {
            Scholarship newScholarship = CreateTestObject();
            newScholarship.Stage = ScholarshipStages.GeneralInformation;

            InsertScholarship(newScholarship);

            Scholarship scholarship = ScholarshipDAL.FindByOrganizations(newScholarship.Provider, newScholarship.Intermediary, newScholarship.Stage)[0];
            Assert.IsNotNull(scholarship);
            var scholarships = ScholarshipDAL.FindByOrganizations(newScholarship.Provider, newScholarship.Intermediary, ScholarshipStages.BuildScholarshipMatchLogic);
            Assert.IsTrue(scholarships.Count == 0);
        }

        [Test]
        public void can_find_all_due_today()
        {
            var initialCount = ScholarshipDAL.FindAllDueOn(DateTime.Today).Count;

            Scholarship newScholarship = CreateTestObject();
            newScholarship.ApplicationDueDate = DateTime.Now;
            InsertScholarship(newScholarship);

            var found = ScholarshipDAL.FindAllDueOn(DateTime.Today);
            Assert.AreEqual(initialCount + 1, found.Count);
        }

        public void AssertScholarshipAreSame(Scholarship expected, Scholarship actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);
        }

        public Scholarship CreateTestObject()
        {
            var stage = ScholarshipStages.GeneralInformation;
            return CreateTestObject(user, provider, intermediary, stage);
        }

        public static Scholarship CreateTestObject(User user, Provider provider, ScholarshipStages stage)
        {
            return CreateTestObject(user, provider, null, stage);
        }

        public static Scholarship CreateTestObject(User user, Provider provider, Intermediary intermediary, ScholarshipStages stage)
        {
            var result = new Scholarship
                             {
                                 Name = "Test Scholarship 1",
                                 AcademicYear = new AcademicYear(2009),
                                 MissionStatement = @"Long big text to describe mission",
                                 ProgramGuidelines = @"Long big text to describe donor wishes for scholarship",
                                 Provider = provider,
                                 Intermediary = intermediary,
                                 ApplicationStartDate = DateTime.Today,
                                 ApplicationDueDate = DateTime.Today.AddMonths(1),
                                 AwardDate = DateTime.Today.AddMonths(2),
                                 LastUpdate = new ActivityStamp(user),
                                 Stage = stage
                             };
            return result;
        }


        public Scholarship InsertScholarship(Scholarship scholarship)
        {
            return ScholarshipDAL.Save(scholarship);
        }
    }
}
