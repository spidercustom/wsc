﻿using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class RelationshipDALTest : TestBase
    {
        public RelationshipDAL RelationshipDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public IntermediaryDAL IntermediaryDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        private User user;
        private Provider provider;
        private Intermediary intermediary;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            intermediary = IntermediaryDALTest.InsertIntermediary(IntermediaryDAL, StateDAL, "TestIntermediary", user);
        }

        [Test]
        public void find_by_provider()
        {
            Relationship newRelationship = CreateTestObject();
            InsertRelationship(newRelationship);

            Relationship relationship = RelationshipDAL.FindByProvider(newRelationship.Provider)[0];
            Assert.IsNotNull(relationship);

            var relationshipList = RelationshipDAL.FindByProvider(new Provider(){Id=12,Name="Nonexistingprovider"});
            Assert.IsTrue(relationshipList.Count==0);
        }

        [Test]
        public void find_by_intermediary()
        {
            Relationship newRelationship = CreateTestObject();
            InsertRelationship(newRelationship);

            Relationship relationship = RelationshipDAL.FindByIntermediary(newRelationship.Intermediary)[0];
            Assert.IsNotNull(relationship);

            var relationshipList = RelationshipDAL.FindByIntermediary(new Intermediary() { Id = 12, Name = "NonexistingIntermediary" });
            Assert.IsTrue(relationshipList.Count == 0);
        }

        [Test]
        public void find_by_provider_and_intermediary()
        {
            var newRelationship = CreateTestObject();
            InsertRelationship(newRelationship);

            var relationship = RelationshipDAL.FindByProviderandIntermediary(newRelationship.Provider,newRelationship.Intermediary);
            Assert.IsNotNull(relationship);

            var relationshipnonexistent = RelationshipDAL.FindByProviderandIntermediary(newRelationship.Provider, new Intermediary() { Id = 12, Name = "NonexistingIntermediary" });
            Assert.IsNull(relationshipnonexistent);
        }


        [Test]
        public void Can_find_by_id()
        {
            Relationship newRelationship = CreateTestObject();
            InsertRelationship(newRelationship);
            Relationship retrivedRelationship = RelationshipDAL.FindById(newRelationship.Id);
            Assert.AreEqual(newRelationship.Id, retrivedRelationship.Id);
        }

        [Test]
        public void Can_create_Relationship()
        {
            Relationship relationship = CreateTestObject();
            var newRelationship = InsertRelationship(relationship);
            Assert.IsNotNull(newRelationship);
            Assert.AreNotEqual(0, newRelationship.Id);
        }

        [Test]
        public void Can_update_Relationship()
        {
            Relationship relationship = CreateTestObject();
            var newRelationship = InsertRelationship(relationship);
            Assert.IsNotNull(newRelationship);
            Assert.AreNotEqual(0, newRelationship.Id);

            newRelationship.Status = RelationshipStatus.InActive;
            
            RelationshipDAL.Save(newRelationship);

            var foundRelationship = RelationshipDAL.FindById(relationship.Id);

            Assert.IsNotNull(foundRelationship);

            Assert.IsTrue(foundRelationship.Status==RelationshipStatus.InActive);
        }

        public Relationship CreateTestObject()
        {
            return CreateTestObject(user, provider, intermediary, RelationshipStatus.Pending);
        }

        public static Relationship CreateTestObject(User user, Provider provider,Intermediary intermediary, RelationshipStatus status)
        {
            var result = new Relationship
            {
               
                Provider = provider,
                Intermediary = intermediary,
                Requester=RelationshipRequester.Intermediary,
                LastUpdate = new ActivityStamp(user),
                Status= status,
                RequestedOn = DateTime.Today
                 
            };
            return result;
        }

        public Relationship InsertRelationship(Relationship relationship)
        {
            return RelationshipDAL.Save(relationship);
        }

    }
}
