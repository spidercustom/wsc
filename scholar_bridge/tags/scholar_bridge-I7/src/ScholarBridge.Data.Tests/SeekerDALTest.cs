using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class SeekerDALTest : TestBase
    {
        public SeekerDAL SeekerDAL { get; set; }
        public UserDAL UserDAL { get; set; }
		public LookupDAL<SeekerSkill> SeekerSkillDAL { get; set; }
		public LookupDAL<Ethnicity> EthnicityDAL { get; set; }
		public LookupDAL<Religion> ReligionDAL { get; set; }
        public LookupDAL<AcademicArea> AcademicAreaDAL { get; set; }
        public LookupDAL<Career> CareerDAL { get; set; }
        public LookupDAL<CommunityService> CommunityServiceDAL { get; set; }
        public LookupDAL<SeekerMatchOrganization> SeekerMatchOrganizationDAL { get; set; }
        public LookupDAL<AffiliationType> AffiliationTypeDAL { get; set; }
        public LookupDAL<SeekerHobby>  SeekerHobbyDAL { get; set; }
        public LookupDAL<Sport> SportDAL { get; set; }
        public LookupDAL<Club> ClubDAL { get; set; }
        public LookupDAL<WorkType> WorkTypeDAL { get; set; }
        public LookupDAL<ServiceType> ServiceTypeDAL { get; set; }
        public LookupDAL<WorkHour> WorkHourDAL { get; set; }
        public LookupDAL<ServiceHour> ServiceHourDAL { get; set; }
        public LookupDAL<NeedGap> NeedGapDAL { get; set; }
        public LookupDAL<SchoolDistrict> SchoolDistrictDAL { get; set; }
		public StateDAL StateDAL { get; set; }

        private User user;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
        }

        [Test]
        public void can_create_seeker()
        {
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);

            Assert.IsNotNull(seeker.User);
            Assert.AreEqual("TestFirst TestLast", seeker.Name.ToString());
        }

        [Test]
        public void can_find_seeker_by_user()
        {
			InsertSeeker(SeekerDAL, user, StateDAL);

            var seeker2 = SeekerDAL.FindByUser(user);
            Assert.IsNotNull(seeker2);
		}

		#region Seeker Skills tests
		[Test]
		public void can_associate_seeker_with_skills()
		{
			Seeker seeker = GetSeekerWithSkills();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Skills);
			Assert.AreEqual(2, found.Skills.Count);
		}

		[Test]
		public void can_remove_skills_from_seeker()
		{
			Seeker seeker = GetSeekerWithSkills();
			Assert.AreEqual(2, seeker.Skills.Count);

			seeker.Skills.RemoveAt(0);
			SeekerDAL.Update(seeker);

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Skills);
			Assert.AreEqual(1, found.Skills.Count);
		}
		#endregion

		#region Seeker ethnicity tests
		[Test]
		public void can_associate_seeker_with_ethnicities()
		{
			Seeker seeker = GetSeekerWithEthnicities();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Ethnicities);
			Assert.AreEqual(2, found.Ethnicities.Count);
		}

		[Test]
		public void can_remove_ethnicities_from_seeker()
		{
			Seeker seeker = GetSeekerWithEthnicities();
			Assert.AreEqual(2, seeker.Ethnicities.Count);

			seeker.Ethnicities.RemoveAt(0);
			SeekerDAL.Update(seeker);

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Ethnicities);
			Assert.AreEqual(1, found.Ethnicities.Count);
		}
		#endregion

		#region Seeker religion tests
		[Test]
		public void can_associate_seeker_with_religions()
		{
			Seeker seeker = GetSeekerWithReligions();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Religions);
			Assert.AreEqual(2, found.Religions.Count);
		}

		[Test]
		public void can_remove_Religions_from_seeker()
		{
			Seeker seeker = GetSeekerWithReligions();
			Assert.AreEqual(2, seeker.Religions.Count);

			seeker.Religions.RemoveAt(0);
			SeekerDAL.Update(seeker);

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Religions);
			Assert.AreEqual(1, found.Religions.Count);
		}
		#endregion
        #region Seeker AcademicAreas tests
        [Test]
        public void can_associate_seeker_with_AcademicAreas()
        {
            Seeker seeker = GetSeekerWithAcademicAreas();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AcademicAreas);
            Assert.AreEqual(2, found.AcademicAreas.Count);
        }

        [Test]
        public void can_remove_AcademicAreas_from_seeker()
        {
            Seeker seeker = GetSeekerWithAcademicAreas();
            Assert.AreEqual(2, seeker.AcademicAreas.Count);

            seeker.AcademicAreas.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AcademicAreas);
            Assert.AreEqual(1, found.AcademicAreas.Count);
        }
        #endregion
        #region Seeker Careers tests
        [Test]
        public void can_associate_seeker_with_Careers()
        {
            Seeker seeker = GetSeekerWithCareers();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Careers);
            Assert.AreEqual(2, found.Careers.Count);
        }

        [Test]
        public void can_remove_Careers_from_seeker()
        {
            Seeker seeker = GetSeekerWithCareers();
            Assert.AreEqual(2, seeker.Careers.Count);

            seeker.Careers.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Careers);
            Assert.AreEqual(1, found.Careers.Count);
        }
        #endregion
        #region Seeker CommunityServices tests
        [Test]
        public void can_associate_seeker_with_CommunityServices()
        {
            Seeker seeker = GetSeekerWithCommunityServices();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.CommunityServices);
            Assert.AreEqual(2, found.CommunityServices.Count);
        }

        [Test]
        public void can_remove_CommunityServices_from_seeker()
        {
            Seeker seeker = GetSeekerWithCommunityServices ();
            Assert.AreEqual(2, seeker.CommunityServices.Count);

            seeker.CommunityServices.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.CommunityServices);
            Assert.AreEqual(1, found.CommunityServices.Count);
        }
        #endregion
        #region Seeker SeekerMatchOrganizations tests
        [Test]
        public void can_associate_seeker_with_SeekerMatchOrganizations()
        {
            Seeker seeker = GetSeekerWithSeekerMatchOrganizations();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.MatchOrganizations);
            Assert.AreEqual(2, found.MatchOrganizations.Count);
        }

        [Test]
        public void can_remove_SeekerMatchOrganizations_from_seeker()
        {
            Seeker seeker = GetSeekerWithSeekerMatchOrganizations();
            Assert.AreEqual(2, seeker.MatchOrganizations.Count);

            seeker.MatchOrganizations.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.MatchOrganizations);
            Assert.AreEqual(1, found.MatchOrganizations.Count);
        }
        #endregion
        #region Seeker AffiliationTypes tests
        [Test]
        public void can_associate_seeker_with_AffiliationTypes()
        {
            Seeker seeker = GetSeekerWithAffiliationTypes();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AffiliationTypes);
            Assert.AreEqual(2, found.AffiliationTypes.Count);
        }

        [Test]
        public void can_remove_AffiliationTypes_from_seeker()
        {
            Seeker seeker = GetSeekerWithAffiliationTypes();
            Assert.AreEqual(2, seeker.AffiliationTypes.Count);

            seeker.AffiliationTypes.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AffiliationTypes);
            Assert.AreEqual(1, found.AffiliationTypes.Count);
        }
        #endregion
        #region Seeker Hobbies tests
        [Test]
        public void can_associate_seeker_with_Hobbies()
        {
            Seeker seeker = GetSeekerWithHobbies();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Hobbies);
            Assert.AreEqual(2, found.Hobbies.Count);
        }

        [Test]
        public void can_remove_Hobbies_from_seeker()
        {
            Seeker seeker = GetSeekerWithHobbies();
            Assert.AreEqual(2, seeker.Hobbies.Count);

            seeker.Hobbies.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Hobbies);
            Assert.AreEqual(1, found.Hobbies.Count);
        }


        #endregion

        #region Seeker Sports tests
        [Test]
        public void can_associate_seeker_with_Sports()
        {
            Seeker seeker = GetSeekerWithSports();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Sports);
            Assert.AreEqual(2, found.Sports.Count);
        }

        [Test]
        public void can_remove_Sports_from_seeker()
        {
            Seeker seeker = GetSeekerWithSports();
            Assert.AreEqual(2, seeker.Sports.Count);

            seeker.Sports.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Sports);
            Assert.AreEqual(1, found.Sports.Count);
        }
        #endregion

        #region Seeker Clubs tests
        [Test]
        public void can_associate_seeker_with_Clubs()
        {
            Seeker seeker = GetSeekerWithClubs();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Clubs);
            Assert.AreEqual(2, found.Clubs.Count);
        }

        [Test]
        public void can_remove_Clubs_from_seeker()
        {
            Seeker seeker = GetSeekerWithClubs();
            Assert.AreEqual(2, seeker.Clubs.Count);

            seeker.Clubs.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Clubs);
            Assert.AreEqual(1, found.Clubs.Count);
        }
        #endregion

        #region Seeker WorkTypes tests
        [Test]
        public void can_associate_seeker_with_WorkTypes()
        {
            Seeker seeker = GetSeekerWithWorkTypes();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.WorkTypes);
            Assert.AreEqual(2, found.WorkTypes.Count);
        }

        [Test]
        public void can_remove_WorkTypes_from_seeker()
        {
            Seeker seeker = GetSeekerWithWorkTypes();
            Assert.AreEqual(2, seeker.WorkTypes.Count);

            seeker.WorkTypes.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.WorkTypes);
            Assert.AreEqual(1, found.WorkTypes.Count);
        }
        #endregion

        #region Seeker ServiceTypes tests
        [Test]
        public void can_associate_seeker_with_ServiceTypes()
        {
            Seeker seeker = GetSeekerWithServiceTypes();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.ServiceTypes);
            Assert.AreEqual(2, found.ServiceTypes.Count);
        }

        [Test]
        public void can_remove_ServiceTypes_from_seeker()
        {
            Seeker seeker = GetSeekerWithServiceTypes();
            Assert.AreEqual(2, seeker.ServiceTypes.Count);

            seeker.ServiceTypes.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.ServiceTypes);
            Assert.AreEqual(1, found.ServiceTypes.Count);
        }
        #endregion

        #region Seeker Attachments tests
		[Test]
		public void can_associate_seeker_with_Attachments()
		{
			Seeker seeker = GetSeekerWithAttachments();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.WorkHours);
			Assert.AreEqual(2, found.Attachments.Count);
		}

		[Test]
        public void can_remove_Attachments_from_seeker()
        {
            Seeker seeker = GetSeekerWithAttachments();
            Assert.AreEqual(2, seeker.Attachments.Count);

            seeker.Attachments.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Attachments);
            Assert.AreEqual(1, found.Attachments.Count);
        }
        #endregion

		#region Seeker WorkHours tests
		[Test]
		public void can_associate_seeker_with_WorkHours()
		{
			Seeker seeker = GetSeekerWithWorkHours();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.WorkHours);
			Assert.AreEqual(2, found.WorkHours.Count);
		}

		[Test]
        public void can_remove_WorkHours_from_seeker()
        {
            Seeker seeker = GetSeekerWithWorkHours();
            Assert.AreEqual(2, seeker.WorkHours.Count);

            seeker.WorkHours.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.WorkHours);
            Assert.AreEqual(1, found.WorkHours.Count);
        }
        #endregion

        #region Seeker ServiceHours tests
        [Test]
        public void can_associate_seeker_with_ServiceHours()
        {
            Seeker seeker = GetSeekerWithServiceHours();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.ServiceHours);
            Assert.AreEqual(2, found.ServiceHours.Count);
        }

        [Test]
        public void can_remove_ServiceHours_from_seeker()
        {
            Seeker seeker = GetSeekerWithServiceHours();
            Assert.AreEqual(2, seeker.ServiceHours.Count);

            seeker.ServiceHours.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.ServiceHours);
            Assert.AreEqual(1, found.ServiceHours.Count);
        }
        #endregion

        #region Seeker NeedGaps tests
        [Test]
        public void can_associate_seeker_with_NeedGaps()
        {
            Seeker seeker = GetSeekerWithNeedGaps();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Need.NeedGaps);
            Assert.AreEqual(2, found.Need.NeedGaps.Count);
        }

        [Test]
        public void can_remove_NeedGaps_from_seeker()
        {
            Seeker seeker = GetSeekerWithNeedGaps();
            Assert.AreEqual(2, seeker.Need.NeedGaps.Count);

            seeker.Need.NeedGaps.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Need.NeedGaps);
            Assert.AreEqual(1, found.Need.NeedGaps.Count);
        }
        #endregion

        
		private Seeker GetSeekerWithSkills()
		{
			var s1 = SeekerSkillDAL.Insert(new SeekerSkill { Name = "Fancy Walking", LastUpdate = new ActivityStamp(user) });
			var s2 = SeekerSkillDAL.Insert(new SeekerSkill { Name = "Fancy Talking", LastUpdate = new ActivityStamp(user) });
			var s3 = SeekerSkillDAL.Insert(new SeekerSkill { Name = "Googling", LastUpdate = new ActivityStamp(user) });

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.Skills.Add(s1);
			seeker.Skills.Add(s3);

			SeekerDAL.Update(seeker);
			return seeker;
		}

		private Seeker GetSeekerWithEthnicities()
		{
			var s1 = EthnicityDAL.Insert(new Ethnicity { Name = "Martian", LastUpdate = new ActivityStamp(user) });
			var s2 = EthnicityDAL.Insert(new Ethnicity { Name = "Lunar", LastUpdate = new ActivityStamp(user) });
			var s3 = EthnicityDAL.Insert(new Ethnicity { Name = "Asteroidian", LastUpdate = new ActivityStamp(user) });

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.Ethnicities.Add(s1);
			seeker.Ethnicities.Add(s3);

			SeekerDAL.Update(seeker);
			return seeker;
		}

		private Seeker GetSeekerWithReligions()
		{
			var s1 = ReligionDAL.Insert(new Religion { Name = "Astrology", LastUpdate = new ActivityStamp(user) });
			var s2 = ReligionDAL.Insert(new Religion { Name = "Darwinism", LastUpdate = new ActivityStamp(user) });
			var s3 = ReligionDAL.Insert(new Religion { Name = "Athiesm", LastUpdate = new ActivityStamp(user) });

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.Religions.Add(s1);
			seeker.Religions.Add(s3);

			SeekerDAL.Update(seeker);
			return seeker;
		}
        private Seeker GetSeekerWithAcademicAreas()
        {
            var s1 = AcademicAreaDAL.Insert(new AcademicArea { Name = "Test Academic1", LastUpdate = new ActivityStamp(user) });
            var s2 = AcademicAreaDAL.Insert(new AcademicArea { Name = "Test Academic2", LastUpdate = new ActivityStamp(user) });
         
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.AcademicAreas.Add(s1);
            seeker.AcademicAreas.Add(s2);
           
            SeekerDAL.Update(seeker);
            return seeker;
        }
        private Seeker GetSeekerWithCareers()
        {
            var s1 = CareerDAL.Insert(new Career { Name = "Test Career1", LastUpdate = new ActivityStamp(user) });
            var s2 = CareerDAL.Insert(new Career { Name = "Test Career2", LastUpdate = new ActivityStamp(user) });
          
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Careers.Add(s1);
            seeker.Careers.Add(s2);
         
            SeekerDAL.Update(seeker);
            return seeker;
        }
        private Seeker GetSeekerWithCommunityServices()
        {
            var s1 =  CommunityServiceDAL.Insert(new  CommunityService { Name = "Test  CommunityService1", LastUpdate = new ActivityStamp(user) });
            var s2 =  CommunityServiceDAL.Insert(new  CommunityService { Name = "Test  CommunityService2", LastUpdate = new ActivityStamp(user) });
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker. CommunityServices.Add(s1);
            seeker. CommunityServices.Add(s2);
          
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithSeekerMatchOrganizations()
        {
            var s1 = SeekerMatchOrganizationDAL.Insert(new SeekerMatchOrganization { Name = "Test SeekerMatchOrganization1", LastUpdate = new ActivityStamp(user) });
            var s2 = SeekerMatchOrganizationDAL.Insert(new SeekerMatchOrganization { Name = "Test SeekerMatchOrganization2", LastUpdate = new ActivityStamp(user) });
         
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.MatchOrganizations.Add(s1);
            seeker.MatchOrganizations.Add(s2);
         
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithAffiliationTypes()
        {
            var s1 = AffiliationTypeDAL.Insert(new AffiliationType { Name = "Test AffiliationType1", LastUpdate = new ActivityStamp(user) });
            var s2 = AffiliationTypeDAL.Insert(new AffiliationType { Name = "Test AffiliationType2", LastUpdate = new ActivityStamp(user) });
      
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.AffiliationTypes.Add(s1);
            seeker.AffiliationTypes.Add(s2);
        
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithHobbies()
        {
            var s1 = SeekerHobbyDAL.Insert(new SeekerHobby { Name = "Test SeekerHobby1", LastUpdate = new ActivityStamp(user) });
            var s2 = SeekerHobbyDAL.Insert(new SeekerHobby { Name = "Test SeekerHobby2", LastUpdate = new ActivityStamp(user) });
       
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Hobbies.Add(s1);
            seeker.Hobbies.Add(s2);
         
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithSports()
        {
            var s1 = SportDAL.Insert(new Sport { Name = "Test Sport1", LastUpdate = new ActivityStamp(user) });
            var s2 = SportDAL.Insert(new Sport { Name = "Test Sport2", LastUpdate = new ActivityStamp(user) });
       
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Sports.Add(s1);
            seeker.Sports.Add(s2);
       
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithClubs()
        {
            var s1 = ClubDAL.Insert(new Club { Name = "Test Club1", LastUpdate = new ActivityStamp(user) });
            var s2 = ClubDAL.Insert(new Club { Name = "Test Club2", LastUpdate = new ActivityStamp(user) });
       
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Clubs.Add(s1);
            seeker.Clubs.Add(s2);
        
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithWorkTypes()
        {
            var s1 = WorkTypeDAL.Insert(new WorkType { Name = "Test WorkType1", LastUpdate = new ActivityStamp(user) });
            var s2 = WorkTypeDAL.Insert(new WorkType { Name = "Test WorkType2", LastUpdate = new ActivityStamp(user) });
    
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.WorkTypes.Add(s1);
            seeker.WorkTypes.Add(s2);
      
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithServiceTypes()
        {
            var s1 = ServiceTypeDAL.Insert(new ServiceType { Name = "Test ServiceType1", LastUpdate = new ActivityStamp(user) });
            var s2 = ServiceTypeDAL.Insert(new ServiceType { Name = "Test ServiceType2", LastUpdate = new ActivityStamp(user) });
    
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.ServiceTypes.Add(s1);
            seeker.ServiceTypes.Add(s2);
    
            SeekerDAL.Update(seeker);
            return seeker;
        }

		private Seeker GetSeekerWithWorkHours()
		{
			var s1 = WorkHourDAL.Insert(new WorkHour { Name = "Test WorkHour1", LastUpdate = new ActivityStamp(user) });
			var s2 = WorkHourDAL.Insert(new WorkHour { Name = "Test WorkHour2", LastUpdate = new ActivityStamp(user) });

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.WorkHours.Add(s1);
			seeker.WorkHours.Add(s2);

			SeekerDAL.Update(seeker);
			return seeker;
		}

		private Seeker GetSeekerWithAttachments()
		{
			var a1 = new Attachment()
			{
				Bytes = 100,
				Comment = "test file a1",
				LastUpdate = new ActivityStamp(user),
				MimeType = "Text/Plain",
				Name = "tempfilea1.txt",
				UniqueName = Guid.NewGuid().ToString()
			};
			var a2 = new Attachment()
			{
				Bytes = 100,
				Comment = "test file a2",
				LastUpdate = new ActivityStamp(user),
				MimeType = "Text/Plain",
				Name = "tempfilea2.txt",
				UniqueName = Guid.NewGuid().ToString()
			};

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.Attachments.Add(a1);
			seeker.Attachments.Add(a2);

			SeekerDAL.Update(seeker);
			return seeker;
		}

		private Seeker GetSeekerWithServiceHours()
        {
            var s1 = ServiceHourDAL.Insert(new ServiceHour { Name = "Test ServiceHour1", LastUpdate = new ActivityStamp(user) });
            var s2 = ServiceHourDAL.Insert(new ServiceHour { Name = "Test ServiceHour2", LastUpdate = new ActivityStamp(user) });
   
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.ServiceHours.Add(s1);
            seeker.ServiceHours.Add(s2);
    
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithNeedGaps()
        {
            var s1 = NeedGapDAL.Insert(new NeedGap { Name = "Test NeedGap1",Deprecated=false,MaxPercent = 2,MinPercent = 1,Description ="desc" ,LastUpdate = new ActivityStamp(user) });
            var s2 = NeedGapDAL.Insert(new NeedGap { Name = "Test NeedGap2", Deprecated = false, MaxPercent = 2, MinPercent = 1, Description = "desc", LastUpdate = new ActivityStamp(user) });
  
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Need=new DefinitionOfNeed();
            seeker.Need.NeedGaps.Add(s1);
            seeker.Need.NeedGaps.Add(s2);
   
            SeekerDAL.Update(seeker);
            return seeker;
        }

       
		public static Seeker InsertSeeker(ISeekerDAL seekerDAL, User user, StateDAL stateDAL)
        {
            var wi = stateDAL.FindByAbbreviation("WI");
		    var seeker = new Seeker
		                     {
		                         User = user,
		                         PersonalStatement = "I like to do stuff",
		                         MyGift = "I read minds",
		                         Gender = Genders.Male,
		                         EthnicityOther = "Plutarian",
		                         ReligionOther = "Anarchy",
		                         AcademicAreaOther = "AcademicOther",
		                         CareerOther = "Test CareerOther",
		                         CommunityServiceOther = "Test CommunityOther",
		                         MatchOrganizationOther = "Test MatchOrganization",
		                         AffiliationTypeOther = "Test AffliiationOther",
		                         HobbyOther = "Test HobbiersOther",
		                         SportOther = "Test SportsOther",
		                         ClubOther = "Test ClubOther",
		                         WorkTypeOther = "Test WorkTypeOther",
		                         ServiceTypeOther = "Test ServiceTypeOther",
		                         WorkHourOther = "Test WorkHourOther",
		                         ServiceHourOther = "Test ServiceHourOther",
                                 SeekerAcademics = new SeekerAcademics { GPA = 3 },
		                         Need = new DefinitionOfNeed {Fafsa = true, MaximumSeekerNeed = 2, MinimumSeekerNeed = 1,UserDerived = true},
                                 LastUpdate = new ActivityStamp(user),
								 Address = new SeekerAddress {City = null, PostalCode="99999-9999", State= wi, Street= "Here", Street2 = "There"},
                                 County=null,
                                 DateOfBirth = DateTime.Now.AddYears(-18),
                                 Phone = new PhoneNumber("414-123-4567"),
                                 MobilePhone = new PhoneNumber("414-123-4568")
                               };

            seeker = seekerDAL.Insert(seeker);

            Assert.IsNotNull(seeker);
            Assert.AreNotEqual(0, seeker.Id);

            return seeker;
        }
    }
}