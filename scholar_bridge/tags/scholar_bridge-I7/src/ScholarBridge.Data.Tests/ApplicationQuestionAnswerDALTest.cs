using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ApplicationQuestionAnswerDALTest : TestBase
    {
        public ApplicationQuestionAnswerDAL ApplicationQuestionAnswerDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL  { get; set; }
        public SeekerDAL SeekerDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public ApplicationDAL ApplicationDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }
        private User user;
        private ScholarshipQuestion question;
        private Application application;
        private Seeker seeker;
        private Scholarship scholarship;
        private Provider provider;
        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            seeker = SeekerDALTest.InsertSeeker(SeekerDAL, user, StateDAL);
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            scholarship = ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStages.Activated);
            scholarship.AddAdditionalQuestion(new ScholarshipQuestion(){DisplayOrder=1,QuestionText="testQ1",Scholarship=scholarship,LastUpdate=new ActivityStamp(user) });
            scholarship.AddAdditionalQuestion(new ScholarshipQuestion() { DisplayOrder = 2, QuestionText = "testQ2", Scholarship = scholarship, LastUpdate = new ActivityStamp(user) });
            
            scholarship = ScholarshipDAL.Insert(scholarship);
            question = scholarship.AdditionalQuestions[0];
            application = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            
        }

        [Test]
        public void CanCreateApplicationQuestionAnswer()
        {
            InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL,application,question,user );
        }

        [Test]
        public void CanGetById()
        {
            var qa = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application, question, user);
            var found = ApplicationQuestionAnswerDAL.FindById(qa.Id);

            Assert.IsNotNull(found);
            Assert.AreEqual(qa.AnswerText, found.AnswerText);
        }
        
        [Test]
        public void CanFindByApplicatioAndQuestion()
        {
            var qa = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application, question, user);
            var application2 = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);

            var qa2 = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application2, application2.Scholarship.AdditionalQuestions[1], user);
            
            var found = ApplicationQuestionAnswerDAL.FindByApplicationandQuestion(application, question);

            Assert.IsNotNull(found);
            
            Assert.AreEqual(qa.Application, found.Application);
            Assert.AreEqual(qa.Question, found.Question);

            Assert.AreNotEqual(qa2.Application, found.Application);
            Assert.AreNotEqual(qa2.Question, found.Question);

        }

        [Test]
        public void Can_Not_FindByApplicatioAndQuestion()
        {
            var qa = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application, question, user);
            var application2 = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);

            var qa2 = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application2, application2.Scholarship.AdditionalQuestions[1], user);

            var found = ApplicationQuestionAnswerDAL.FindByApplicationandQuestion(application2, question);

            Assert.IsNull(found);
        }

        [Test]
        public void CanFindByApplication()
        {
            var qa = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application, question, user);
            var application2 = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);

            var qa2 = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application2, application2.Scholarship.AdditionalQuestions[1], user);

            var found = ApplicationQuestionAnswerDAL.FindByApplication(application);
            
           
            Assert.AreEqual(1, found.Count);

            Assert.AreEqual(qa.Application, found[0].Application);
            Assert.AreEqual(qa.Question, found[0].Question);

            Assert.AreNotEqual(qa2.Application, found[0].Application);
            Assert.AreNotEqual(qa2.Question, found[0].Question);

        }

        [Test]
        public void Can_Not_FindByApplication()
        {
            var qa = InsertApplicationQuestionAnswer(ApplicationQuestionAnswerDAL, application, question, user);
            var application2 = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);

          
            var found = ApplicationQuestionAnswerDAL.FindByApplication(application2);

            Assert.AreEqual(0,found.Count );
        }

        public static ApplicationQuestionAnswer InsertApplicationQuestionAnswer(IApplicationQuestionAnswerDAL applicationQuestionAnswerDAL, Application application,ScholarshipQuestion question,User user)
        {
            var qa = new ApplicationQuestionAnswer
                           {
                                AnswerText = "TestAnswer",
                                Question =question,
                                Application=application,
                               LastUpdate = new ActivityStamp(user)
                           };

            var createdQA = applicationQuestionAnswerDAL.Insert(qa);

            Assert.IsNotNull(createdQA);
            Assert.AreNotEqual(0, createdQA.Id);

            return createdQA;
        }
    }
}