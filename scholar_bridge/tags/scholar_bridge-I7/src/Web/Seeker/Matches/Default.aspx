﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Matches.Default"  Title="Seeker | My Matches" %>

<%@ Register src="~/Common/MatchList.ascx" tagname="MatchList" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<p>Placekeeper for WSC provided text</p>
<p>
The following matches are based on your profile and the scholarship requirements. A more complete profile 
will result in better scholarship matches and may show more scholarships that you qualify for.  The Required 
Criteria column determines if you qualify for a scholarship.  The Preferred Criteria column shows the number 
of additional selection criteria that you meet.  Click on the scholarship name to see the criteria.
</p>
<p>
Your scholarships are sorted with the best match shown at the top.  The top group of shows scholarships you 
are qualifiy for based on your current profile.   The second group shows scholarships you may qualify for.  
Look at the details of the scholarship by clicking the scholarship's name.   The scholarships are primarily 
sorted based on the percentage of minimum criteria you match and secondarily sorted based on the percentage of 
preferred criteria you match.
</p>

<h3>Scholarships You Qualify For:</h3>
<sb:MatchList id="qualifyList" runat="server" OnMatchAction="list_OnMatchAction" />

<h3>Scholarships You May Qualify For:</h3>
<sb:MatchList id="mayQualifyList" runat="server" OnMatchAction="list_OnMatchAction" />

</asp:Content>
