﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutMe.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.AboutMe" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>

<label for="PersonalStatementControl">Personal Statement:</label>
<asp:TextBox ID="PersonalStatementControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="PersonalStatementValidator" runat="server" ControlToValidate="PersonalStatementControl" PropertyName="PersonalStatement" SourceTypeName="ScholarBridge.Domain.Seeker"/>
<br />

<label for="MyChallengeControl">My Challenge:</label>
<asp:TextBox ID="MyChallengeControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="MyChallengeValidator" runat="server" ControlToValidate="MyChallengeControl" PropertyName="MyChallenge" SourceTypeName="ScholarBridge.Domain.Seeker"/>
<br />

<label for="MyGiftControl">My Gift:</label>
<asp:TextBox ID="MyGiftControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="MyGiftValidator" runat="server" ControlToValidate="MyGiftControl" PropertyName="MyGift" SourceTypeName="ScholarBridge.Domain.Seeker"/>
<br />

<label id="FiveWordsLabelControl" for="FiveWordsControl">5 Words:</label>
<asp:TextBox ID="FiveWordsControl" TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
<sb:LookupDialog ID="FiveWordsControlDialogButton" runat="server" BuddyControl="FiveWordsControl" ItemSource="SeekerVerbalizingWordDAL" Title="Five Words Describing the Seeker"/>
<br />

<label id="FiveSkillsLabelControl" for="FiveSkillsControl">5 Skills:</label>
<asp:TextBox ID="FiveSkillsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
<sb:LookupDialog ID="FiveSkillsControlDialogButton" runat="server" BuddyControl="FiveSkillsControl" ItemSource="SeekerSkillDAL" Title="Five Seeker Skills"/>
<br />
