﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Profile.Default"  Title="MyProfile" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="~/Seeker/Profile/Basics.ascx" tagname="Basics" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AboutMe.ascx" tagname="AboutMe" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AcademicInformation.ascx" tagname="AcademicInfo" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/Activities.ascx" tagname="Activities" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/FinancialNeed.ascx" tagname="FinancialNeed" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div class="controlPanel">
        <sbCommon:SaveConfirmButton ID="PreviousButton" runat="server" ConfirmMessageDivID="confirmSaving" OnClick="PreviousButton_Click" Text="<<Previous" CausesValidation="false" />
        <sbCommon:SaveConfirmButton ID="NextButton" OnClick="NextButton_Click" ConfirmMessageDivID="confirmSaving" runat="server" Text="Next>>" />
        <sbCommon:SaveConfirmButton ID="SaveButton" runat="server" OnClick="SaveButton_Click" ConfirmMessageDivID="confirmSavingActivatedProfile" Text="Save"/>
        <sbCommon:SaveConfirmButton ID="ActivateButton" OnClick="ActivateButton_Click" runat="server" ConfirmMessageDivID="confirmSaving" Text="Activate Profile"/>
        <sbCommon:SaveConfirmButton ID="ExitButton" OnClick="ExitButton_Click" runat="server" ConfirmMessageDivID="confirmSaving" Text="Exit" />
    </div>
    <div id="confirmSaving" title="Confirm saving" style="display:none">
        You have change the data. Would you like to save the changes?
    </div>
    <div ID="confirmSavingActivatedProfile" title="Confirm saving" style="display:none">
        <asp:Label ID="ConfirmSaveIfActivatedLabel" runat="server" />
    </div>
    <br />

    <div class="tabs" id="BuildSeekerProfileWizardTab">
        <ul class="linkarea">
            <li><a  href="#tab"><span>Basics</span></a></li>
            <li><a  href="#tab"><span>About Me</span></a></li>
            <li><a href="#tab"><span>My Academic Info</span></a></li>
            <li><a href="#tab"><span>My Activities & Interests</span></a></li>
            <li><a href="#tab"><span>My Financial Need</span></a></li>
        </ul>
         
        <div id="tab">
            <asp:MultiView  ID="BuildSeekerProfileWizard" 
                            runat="server"
                            OnActiveViewChanged="BuildSeekerProfileWizard_ActiveStepChanged">
              <asp:View ID="BasicInfoStep" runat="server">
                    <sb:Basics ID="Basics1" runat="server" />
              </asp:View>
              <asp:View ID="AboutMeStep" runat="server">
                    <sb:AboutMe ID="AboutMe1" runat="server" />
              </asp:View>
              <asp:View ID="AcademicStep" runat="server">
                    <sb:AcademicInfo ID="AcademicInfo1" runat="server" />
              </asp:View>
              <asp:View ID="ActivitiesStep" runat="server">
                    <sb:Activities ID="Activities1" runat="server"  />
              </asp:View>
              <asp:View ID="FinancialNeed" runat="server">
                    <sb:FinancialNeed ID="FinancialNeed1" runat="server" />
              </asp:View>
            </asp:MultiView>
        </div>
        <sbCommon:jQueryTabEvents id="BuildSeekerProfileWizardTabEvents" 
                                runat="server" 
                                MultiViewControlID="BuildSeekerProfileWizard"
                                TabControlClientID="BuildSeekerProfileWizardTab" />

    </div>
    
    <div id="ActivationValidationErrors" class="issueList" title="Profile activation" style="display:none">
        <asp:Repeater ID="IssueListControl" runat="server">
            <HeaderTemplate><ul></HeaderTemplate>
            <ItemTemplate><li><%# DataBinder.Eval(Container.DataItem, "Message") %></li></ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
        <br />
        <p>
            All issues specified in above list must to be fixed for profile that is activating or is active.
        </p>
    </div>
</asp:Content>
