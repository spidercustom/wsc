﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutMe.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.AboutMe" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload"
             Assembly="Brettle.Web.NeatUpload" %>

<label for="PersonalStatementControl">Personal Statement:</label>
<asp:TextBox ID="PersonalStatementControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="PersonalStatementValidator" runat="server" ControlToValidate="PersonalStatementControl" PropertyName="PersonalStatement" SourceTypeName="ScholarBridge.Domain.Seeker"/>
<br />

<label for="MyChallengeControl">My Challenge:</label>
<asp:TextBox ID="MyChallengeControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="MyChallengeValidator" runat="server" ControlToValidate="MyChallengeControl" PropertyName="MyChallenge" SourceTypeName="ScholarBridge.Domain.Seeker"/>
<br />

<label for="MyGiftControl">My Gift:</label>
<asp:TextBox ID="MyGiftControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="MyGiftValidator" runat="server" ControlToValidate="MyGiftControl" PropertyName="MyGift" SourceTypeName="ScholarBridge.Domain.Seeker"/>
<br />

<label id="FiveWordsLabelControl" for="FiveWordsControl">5 Words:</label>
<asp:TextBox ID="FiveWordsControl" TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
<sb:LookupDialog ID="FiveWordsControlDialogButton" runat="server" BuddyControl="FiveWordsControl" ItemSource="SeekerVerbalizingWordDAL" Title="Five Words Describing the Seeker"/>
<br />

<label id="FiveSkillsLabelControl" for="FiveSkillsControl">5 Skills:</label>
<asp:TextBox ID="FiveSkillsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
<sb:LookupDialog ID="FiveSkillsControlDialogButton" runat="server" BuddyControl="FiveSkillsControl" ItemSource="SeekerSkillDAL" Title="Five Seeker Skills"/>
<br />

<fieldset>
<legend>Attach documents (Transcripts, Letters of Recommendation, Award Letters, etc.)</legend>
<p>These documents may be attached to any scholarship application that you fill out.</p>

<div style="display: none;">
<Upload:ProgressBar id="AttachFileProgressBar" runat="server" inline="true" />
</div>
<br />
<label for="AttachFile">Select File:</label>
<Upload:InputFile ID="AttachFile" runat="server" />
<br />
<label for="AttachmentComments">Document Description:</label>
<asp:TextBox ID="AttachmentComments" runat="server" />
<elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments" PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment"/>
<asp:Label ID="fileTooBigLbl" runat="server" Visible="false" CssClass="error" Text="The file you tried to upload is too large. It should be less than 10MB." />
<br />
<label for="IncludeCheckBox">*Include with Applications:</label>
<asp:checkbox runat="server" ID="includeCheckBox" />
<br />
<asp:Button ID="UploadFile" runat="server" Text="Attach File" onclick="UploadFile_Click" />
</fieldset>

<asp:ListView ID="attachedFiles" runat="server" 
    OnItemDeleting="attachedFiles_OnItemDeleting" 
    onitemcommand="attachedFiles_ItemCommand">
    <LayoutTemplate>
    <table class="sortableTable">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Document</th>
                <th>Size</th>
                <th>Type</th>
                <th>*Include With Apps</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5">
                *Including files with Applications will attach the file to each scholarship application you create. You will be able to add and remove files from the application before you submit it
            </td>
        </tr>
        </tfoot>
    </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr>
            <td><asp:LinkButton ID="deleteAttachmentBtn" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("Id")%>' /></td>
            <td>
                <asp:LinkButton ID="downloadButton" CommandName="Download" CausesValidation="false" 
                            CommandArgument='<%# Eval("Id") %>' 
                            runat="server" Text='<%# Bind("Name") %>' 
                            ToolTip='<%# Eval("Comment") %>'>
                </asp:LinkButton>            
            </td>
            <td><%# Eval("DisplaySize") %></td>
            <td><%# Eval("MimeType") %></td>
            <td align="center"><asp:checkbox runat="server" ID="includeWithAppsCheck" Checked='<%# Eval("IncludeWithApplications") %>'/></td>
        </tr>
    </ItemTemplate>
   
</asp:ListView>