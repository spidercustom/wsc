﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Scholarships
{
    public partial class SearchResults : SBBasePage
    {

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        private string Criteria
        {
            get
            {

                if (string.IsNullOrEmpty(Request.Params["val"]))
                    throw new ArgumentException("Cannot understand value of parameter criteria");
                return Request.Params["val"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string value = Criteria;
                ScholarshipSearchBox1.Criteria = Criteria;
                var scholarships = ScholarshipService.GetBySearchCriteria(Criteria);
                ScholarshipSearchResults1.Scholarships = scholarships;
            }
        }

        protected void BackBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/Seeker"));
        }
    }
        
}