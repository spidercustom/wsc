﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SearchResults.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Scholarships.SearchResults"  Title="Seeker | Scholarship SearchResults" %>
<%@ Register src="~/Common/ScholarshipSearchResults.ascx" tagname="ScholarshipSearchResults" tagprefix="sb" %>
<%@ Register src="../../Common/ScholarshipSearchBox.ascx" tagname="ScholarshipSearchBox" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<p>Looking for a specific Scholarship? Enter the Scholarship name or the name of the Scholarship Provider/Donor and we will display the matching results.</p>
    
<sb:ScholarshipSearchBox ID="ScholarshipSearchBox1" runat="server" />
<br />
<br />
<asp:Button ID="BackBtn" runat="server" Text="Go Back" onclick="BackBtn_Click" />
<br />

<p>The following scholarships matched your search criteria. To see full details on these scholarships,
as well as get matched to scholarships you may qualify for, create a profile.
To get started creating your profile,  <a href="<%= ResolveUrl("~/Seeker/Register.aspx") %>">click here</a></p>
<br /><br />


<sb:ScholarshipSearchResults ID="ScholarshipSearchResults1" runat="server" LinkTo="~/Seeker/Matches/Create.aspx" />
</asp:Content>
