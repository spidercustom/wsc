﻿function SaveAndContinue(controlId) {
    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlId, 'True', true, '', '', false, true));
}

function CancelSavingAndContinue(controlId) {
    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlId, 'False', true, '', '', false, true));
}

function SavingYesNoCancelDialog(divId, controlId) {
    if (checkForChanges()) {
        $("#" + divId).dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            buttons: {
                "Yes": function() {
                    $(this).dialog("destroy");
                    SaveAndContinue(controlId);
                },
                "No": function() {
                    $(this).dialog("destroy");
                    CancelSavingAndContinue(controlId);
                },
                "Cancel": function() {
                    $(this).dialog("destroy");
                }
            }
        });
    }
    else {
        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlId, 'False', true, '', '', false, true));
    }
}