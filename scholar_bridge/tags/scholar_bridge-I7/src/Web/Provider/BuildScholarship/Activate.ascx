﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activate.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Activate" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<h3>Build Scholarship – Activate</h3>
<p>Activating the Scholarship will make the scholarship visible to applicants and allow the scholarship to be matched with qualified applicants. 
Once you request activation, you will not be able to edit the scholarship criteria. By activating this scholarship, you, the provider 
(or the intermediary on the provider's behalf), indicate your intention to award the scholarship as it has been entered.
</p>
