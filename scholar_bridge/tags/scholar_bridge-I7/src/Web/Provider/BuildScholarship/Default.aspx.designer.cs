﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3074
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScholarBridge.Web.Provider.BuildScholarship {
    
    
    public partial class Default {
        
        /// <summary>
        /// StepCompletedCheckBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox StepCompletedCheckBox;
        
        /// <summary>
        /// PreviousButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.SaveConfirmButton PreviousButton;
        
        /// <summary>
        /// NextButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.SaveConfirmButton NextButton;
        
        /// <summary>
        /// SaveButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SaveButton;
        
        /// <summary>
        /// ExitButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.SaveConfirmButton ExitButton;
        
        /// <summary>
        /// ScholarshipTitleStripeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ScholarshipTitleStripe ScholarshipTitleStripeControl;
        
        /// <summary>
        /// GeneralInformationCompletedControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image GeneralInformationCompletedControl;
        
        /// <summary>
        /// MatchCriteraCompletedControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image MatchCriteraCompletedControl;
        
        /// <summary>
        /// SeekerProfileCompletedControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image SeekerProfileCompletedControl;
        
        /// <summary>
        /// FundingProfileCompletedControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image FundingProfileCompletedControl;
        
        /// <summary>
        /// AdditionalCriteriaCompletedControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image AdditionalCriteriaCompletedControl;
        
        /// <summary>
        /// ActivateCompletedControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ActivateCompletedControl;
        
        /// <summary>
        /// BuildScholarshipWizard control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.MultiView BuildScholarshipWizard;
        
        /// <summary>
        /// GeneralInfoStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View GeneralInfoStep;
        
        /// <summary>
        /// generalInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Provider.BuildScholarship.GeneralInfo generalInfo;
        
        /// <summary>
        /// MatchCriteriaSelectionStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View MatchCriteriaSelectionStep;
        
        /// <summary>
        /// matchCriteriaSelection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Provider.BuildScholarship.MatchCriteriaSelection matchCriteriaSelection;
        
        /// <summary>
        /// SeekerProfileStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View SeekerProfileStep;
        
        /// <summary>
        /// seekerProfile control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Provider.BuildScholarship.SeekerProfile seekerProfile;
        
        /// <summary>
        /// FundingProfileStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View FundingProfileStep;
        
        /// <summary>
        /// fundingProfile control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Provider.BuildScholarship.FundingProfile fundingProfile;
        
        /// <summary>
        /// CriteriaStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View CriteriaStep;
        
        /// <summary>
        /// additionalCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria additionalCriteria;
        
        /// <summary>
        /// ActivateScholarshipStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View ActivateScholarshipStep;
        
        /// <summary>
        /// ActivateStep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Provider.BuildScholarship.Activate ActivateStep;
        
        /// <summary>
        /// BuildScholarshipWizardTabEvents control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.jQueryTabEvents BuildScholarshipWizardTabEvents;
    }
}
