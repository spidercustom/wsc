﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Admin
{
    public partial class PendingOrgs : System.Web.UI.UserControl
    {

        public IList<Organization> Organizations { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            pendingOrgs.DataSource = Organizations;
            pendingOrgs.DataBind();
        }

        protected void pendingOrgs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var link = (HyperLink) e.Item.FindControl("linktoOrg");
                link.NavigateUrl = LinkTo + "?id=" + ((Organization) e.Item.DataItem).Id;
            }
        }
    }
}