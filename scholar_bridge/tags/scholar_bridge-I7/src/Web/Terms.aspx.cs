﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web
{
    public partial class Terms : Page
    {
        public string Referrer
        {
            get { return Request.ServerVariables["HTTP_REFERER"] ?? String.Empty; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                RedirectForLoggedInUser();
            }
            else
            {
                RedirectForAnonymousUser();
            }
        }

        private void RedirectForAnonymousUser()
        {
            if (Referrer.Contains("/Provider/"))
            {
                Response.Redirect("~/Provider/Terms.aspx");
            }
            else if (Referrer.Contains("/Intermediary/"))
            {
                Response.Redirect("~/Intermediary/Terms.aspx");
            }
            else
            {
                Response.Redirect("~/Seeker/Terms.aspx");
            }
        }

        private void RedirectForLoggedInUser()
        {
            if (HttpContext.Current.User.IsInRole(Role.PROVIDER_ROLE))
            {
                Response.Redirect("~/Provider/Terms.aspx");
            }
            else if (HttpContext.Current.User.IsInRole(Role.INTERMEDIARY_ROLE))
            {
                Response.Redirect("~/Intermediary/Terms.aspx");
            }
            else
            {
                Response.Redirect("~/Seeker/Terms.aspx");
            }
        }
    }
}
