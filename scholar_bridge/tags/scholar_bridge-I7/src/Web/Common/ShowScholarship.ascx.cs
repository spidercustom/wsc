﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ShowScholarship : UserControl
    {
        public string LinkTo { get; set; }
        public string ApplicationLinkTo { get; set; }
        public Scholarship Scholarship { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            GeneralInfoShow1.ScholarshipToView = Scholarship;
            GeneralInfoShow1.LinkTo = LinkTo;

            SeekerInfoShow1.ScholarshipToView = Scholarship;
            SeekerInfoShow1.LinkTo = LinkTo;

            FundingInfoShow1.ScholarshipToView = Scholarship;
            FundingInfoShow1.LinkTo = LinkTo;

            AdditionalCriteriaShow1.ScholarshipToView = Scholarship;
            AdditionalCriteriaShow1.LinkTo = LinkTo;

            var notesControl = (AdminScholarshipNotes) loginView1.FindControl("AdminScholarshipNotes1");
            if (null != notesControl)
                notesControl.Scholarship = Scholarship;

            var applicantsControl = (ScholarshipApplicants)loginView1.FindControl("ScholarshipApplicants1");
            if (null != applicantsControl)
            {
                applicantsControl.Scholarship = Scholarship;
                applicantsControl.LinkTo = ApplicationLinkTo;

                if (!Page.IsPostBack)
                {
                    var applicantsLbl = (HtmlGenericControl) loginView.FindControl("applicantsLbl");
                    if (null != applicantsLbl)
                    {
                        var count = applicantsControl.ApplicationCount;
                        applicantsLbl.InnerHtml += String.Format("&nbsp;<span style=\"color: green;\">{0}</span>", count);
                    }
                }
            }
        }
    }
}