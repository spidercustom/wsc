﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectRelatedIntermediary.ascx.cs" Inherits="ScholarBridge.Web.Common.SelectRelatedIntermediary" %>
<asp:DropDownList ID="intermediaryDDL" runat="server" onselectedindexchanged="intermediaryDDL_SelectedIndexChanged"  AutoPostBack="true" />
<span id="hecbMessage" class="noteBene" runat="server" visible="false">Place keeper text for WSC defined message about the HECB acting as the intermediary in ScholarBridge if they select their organization as an intermediary</span>
<span id="intermediaryNotAvailable" class="errorMessage" runat="server" visible="false">Intermediary is no longer valid (since you no longer have a relationship with them). Please select a valid Intermediary from the list.</span>
