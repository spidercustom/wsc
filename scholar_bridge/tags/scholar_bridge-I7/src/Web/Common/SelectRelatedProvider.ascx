﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectRelatedProvider.ascx.cs" Inherits="ScholarBridge.Web.Common.SelectRelatedProvider" %>
<asp:DropDownList ID="providerDDL" runat="server"  />
<span id="providerNotAvailable" class="errorMessage" runat="server" visible="false">Provider is no longer valid (since you no longer have a relationship with them). Please select a valid Provider from the list.</span>
