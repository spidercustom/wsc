﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipList : UserControl
    {
        private IList<Scholarship> _Scholarships;
        public string LinkTo { get; set; }

        public IList<Scholarship> Scholarships
        {
            get { return _Scholarships; }
            set { _Scholarships = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
             
                Bind();
        }

        protected void Bind()
        {
            if (!(Scholarships == null))
            {
                lstScholarships.DataSource = (from s in Scholarships orderby s.Name select s).ToList();
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship) ((ListViewDataItem) e.Item).DataItem);

                var link = (HyperLink) e.Item.FindControl("linktoScholarship");
                link.NavigateUrl = LinkTo + "?id=" + scholarship.Id;
            }
        }

        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }
    }
}