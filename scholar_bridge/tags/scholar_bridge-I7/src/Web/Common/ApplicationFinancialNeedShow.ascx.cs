﻿using System;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationFinancialNeedShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                if (null != ApplicationToView.Need)
                {
                    fafsa.Text = ApplicationToView.Need.Fafsa ? "Yes" : "No";
                    userDerived.Text = ApplicationToView.Need.UserDerived ? "Yes" : "No";

                    minimumNeed.Text = ApplicationToView.Need.MinimumSeekerNeed.ToString("c");
                    maximumNeed.Text = ApplicationToView.Need.MaximumSeekerNeed.ToString("c");

                    needGapThresholds.DataSource = ApplicationToView.Need.NeedGaps;
                    needGapThresholds.DataBind();
                }

                if (null != ApplicationToView.SupportedSituation)
                {
                    typesOfSupport.DataSource = ApplicationToView.SupportedSituation.TypesOfSupport;
                    typesOfSupport.DataBind();
                }
            }
        }

        public override ApplicationStages Stage
        {
            get { return ApplicationStages.FinancialNeed; }
        }
    }
}