﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class SelectRelatedProvider : UserControl
    {
        private Domain.Provider selected;

        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }

        public Domain.Provider SelectedValue
        { 
            get
            {
                var selectedId = -1;
                if (null != selected)
                {
                    selectedId = selected.Id;
                }
                var newId = Int32.Parse(providerDDL.SelectedValue);
                if (newId != selectedId)
                {
                    selected = ProviderService.FindById(newId);
                }
                return selected;
            }

            set { selected = value; } 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            providerNotAvailable.Visible = false;
            if (!IsPostBack)
            {
                IList<Organization> orgs;
                var currentOrg = UserContext.CurrentOrganization;
                if (currentOrg is Domain.Provider)
                {
                    orgs = new List<Organization> { currentOrg };
                }
                else
                {
                    orgs = RelationshipService.GetActiveByOrganization(currentOrg);
                }

                providerDDL.DataSource = orgs;
                providerDDL.DataTextField = "Name";
                providerDDL.DataValueField = "Id";
                if (null != selected)
                {
                    if (!orgs.Any(o => o.Id == selected.Id))
                    {
                        providerNotAvailable.Visible = true;
                    }
                    else
                    {
                        providerDDL.SelectedValue = selected.Id.ToString();
                    }
                }
                providerDDL.DataBind();
            }
        }
    }
}
