using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public abstract class BaseScholarshipShow : UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }

        public abstract ScholarshipStages Stage { get; }

        protected virtual void SetupEditLinks(HtmlGenericControl linkarea1, HtmlGenericControl linkarea2)
        {
            linkarea1.Visible = CanEdit();
            linkarea2.Visible = CanEdit();
            
            string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id, (int)Stage - 1);
            ((HyperLink) linkarea1.FindControl("linkTop")).NavigateUrl = navurl;
            ((HyperLink) linkarea2.FindControl("linkBottom")).NavigateUrl = navurl;
        }

        protected bool CanEdit()
        {
            return !String.IsNullOrEmpty(LinkTo) && ScholarshipToView.CanEdit() &&
                (Roles.IsUserInRole(Role.PROVIDER_ROLE) || Roles.IsUserInRole(Role.INTERMEDIARY_ROLE));
        }
    }
}