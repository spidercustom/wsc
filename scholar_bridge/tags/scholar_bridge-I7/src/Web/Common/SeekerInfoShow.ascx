﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerInfoShow" %>

<h3>Applicant's Profile</h3>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>     

<ul class="vertical-indent-level-1">
    <li><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" /> <span>indicates a required criteria for this scholarship</span></li>
    <li><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" /> <span>indicates a preferred, but not required criteria, for this scholarship</span></li>
</ul>

<asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
    <p class="vertical-indent-level-1">
        No Scholarship Match Criteria Selected. Please go to the "Selection Criteria" tab while editing scholarship, 
        to choose fields to be used for Scholarship selection
    </p>
</asp:PlaceHolder>

<h4 id="ApplicantPersonalityHeader" runat="server">Applicant Personality</h4>
<table class="viewonlyTable" id="ApplicantPersonalityTable" runat="server">
    <tr id="FiveWordsRow" runat="server">
        <th scope="row">5 Words that describe the Applicant: <asp:Image ID="FiveWordsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label  ID="lblFiveWords" runat="server"  /></td>
    </tr>
    <tr id="FiveSkillRow" runat="server">
        <th scope="row">5 Skills of the Applicant: <asp:Image ID="FiveSkillsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblFiveSkills"  runat="server"  /></td>
    </tr>
</table>

<h4 id="ApplicantStudentTypeHeader" runat="server">Applicant Student Type</h4>
<table class="viewonlyTable" id="ApplicantStudentTypeTable" runat="server">
    <tr id="StudentGroupRow" runat="server">
        <th scope="row">Student Groups: <asp:Image ID="StudentGroupUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblStudentGroups"  runat="server"  /></td>
    </tr>
    <tr id="SchoolTypesRow" runat="server">
        <th scope="row">School Types: <asp:Image ID="SchoolTypeUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblSchoolTypes"  runat="server"  /></td>
    </tr>
    <tr id="AcademicProgramsRow" runat="server">
        <th scope="row">Academic Programs: <asp:Image ID="AcademicProgramUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblAcademicPrograms"  runat="server"  /></td>
    </tr>
     <tr id="EnrollmentStatusesRow" runat="server">
        <th scope="row">Enrollment Statuses: <asp:Image ID="SeekerStatusUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblSeekerStatuses"  runat="server"  /></td>
    </tr>
     <tr id="LengthOfProgrammRow" runat="server">
        <th scope="row">Length of Program: <asp:Image ID="ProgramLengthUsageTypeIconControl"  runat="server" /></th>
        <td><asp:Label ID="lblLengthOfProgram"  runat="server"  /></td>
    </tr>
     <tr id="CollegesRow" runat="server">
        <th scope="row">Colleges: <asp:Image ID="CollegesUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblColleges"  runat="server"  /></td>
    </tr>
</table>

<h4 id="ApplicantDemographicsPeronalHeader" runat="server">Applicant Demographics - Personal</h4>
<table class="viewonlyTable" id="ApplicantDemographicsPeronalTable" runat="server">
    <tr id="FirstGenerationRow" runat="server">
        <th scope="row">First Generation: <asp:Image ID="FirstGenerUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblFirstGeneration"  runat="server"  /></td>
    </tr>
    <tr id="EthnicityRow" runat="server">
        <th scope="row">Ethinicity: <asp:Image ID="EthnicityUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblEthinicity"  runat="server"  /></td>
    </tr>
    <tr id="ReligionRow" runat="server">
        <th scope="row">Religion: <asp:Image ID="ReligionUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblReligion"  runat="server"  /></td>
    </tr>
    <tr id="GendersRow" runat="server">
        <th scope="row">Genders: <asp:Image ID="GendersUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblGender"  runat="server"  /></td>
    </tr>
</table>

<h4 id="ApplicantDemographicsGeographicHeader" runat="server">Applicant Demographics - Geographic <asp:Image ID="SeekerDemographicsGeographicUsageTypeIconControl" runat="server" /></h4>
<table class="viewonlyTable" id="ApplicantDemographicsGeographicTable" runat="server">
    <tr id="CountiesRow" runat="server">
        <th scope="row">County:</th>
        <td><asp:Label ID="lblCounty"  runat="server"  /></td>
    </tr>
    <tr id="CitiesRow" runat="server">
        <th scope="row">City:</th>
        <td><asp:Label ID="lblCity"  runat="server"  /></td>
    </tr>
    <tr id="StatesRow" runat="server"> 
        <th scope="row">State: </th>
        <td><asp:Label ID="lblState"  runat="server"  /></td>
    </tr>
    <tr id="SchoolDistrictsRow" runat="server">
        <th scope="row">School District:</th>
        <td><asp:Label ID="lblSchoolDistrict"  runat="server"  /></td>
    </tr>
    <tr id="HighSchoolsRow" runat="server">
        <th scope="row">High School:</th>
        <td><asp:Label ID="lblHighSchool"  runat="server"  /></td>
    </tr>                
</table>

<h4 id="ApplicantInterestsHeader" runat="server">Applicant Interests</h4>
<table class="viewonlyTable" id="ApplicantInterestsTable" runat="server">
    <tr id="AcademicAreasRow" runat="server">
        <th scope="row">Academic Areas: <asp:Image ID="AcademicAreasUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblAcademicAreas"  runat="server"  /></td>
    </tr>
    <tr id="CareersRow" runat="server">
        <th scope="row">Careers: <asp:Image ID="CareersUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblCareers"  runat="server"  /></td>
    </tr>
    <tr id="CommunityServicesRow" runat="server">
        <th scope="row">Community Service/Involvement: <asp:Image ID="CommunityServicesUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblCauses"  runat="server"  /></td>
    </tr>
    <tr  id="OrganizationsRow" runat="server">
        <th scope="row">Organizations: <asp:Image ID="OrganizationsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblOrganizations"  runat="server"  /></td>
    </tr>
    <tr id="AffiliationTypesRow" runat="server">
        <th scope="row">Affiliation types: <asp:Image ID="AffiliationTypesUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblAffiliationTypes"  runat="server"  /></td>
    </tr>                
</table>

<h4 id="ApplicantActivitiesHeader" runat="server">Applicant Activities</h4>
<table class="viewonlyTable" id="ApplicantActivitiesTable" runat="server">
    <tr id="HobbiesRow" runat="server">
        <th scope="row">Hobbies: <asp:Image ID="SeekerHobbiesUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblHobbies"  runat="server"  /></td>
    </tr>
    <tr id="SportsRow" runat="server">
        <th scope="row">Sports: <asp:Image ID="SportsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblSports"  runat="server"  /></td>
    </tr>
    <tr id="ClubsRow" runat="server">
        <th scope="row">Clubs: <asp:Image ID="ClubsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblClubs"  runat="server"  /></td>
    </tr>
    <tr id="WorkTypesRow" runat="server">
        <th scope="row">Work Type: <asp:Image ID="WorkTypeUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblWorkType"  runat="server"  /></td>
    </tr>
    <tr id="WorkHoursRow" runat="server">
        <th scope="row">Work Hours: <asp:Image ID="WorkHoursUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblWorkHours"  runat="server"  /></td>
    </tr>
    <tr id="ServiceTypesRow" runat="server">
        <th scope="row">Service Type: <asp:Image ID="ServiceTypeUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblServiceType"  runat="server"  /></td>
    </tr>
    <tr id="ServiceHoursRow" runat="server">
        <th scope="row">Service Hours: <asp:Image ID="ServiceHoursUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblServiceHours"  runat="server"  /></td>
    </tr>                
</table>

<h4 id="ApplicantStudentPerformanceHeader" runat="server">Applicant Student Performance</h4>
<table class="viewonlyTable" id="ApplicantStudentPerformanceTable" runat="server">
    <tr id="GPARow" runat="server">
        <th scope="row">GPA: <asp:Image ID="GPAUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblGPA"  runat="server"  /></td>
    </tr>
    <tr id="SATScoreRow" runat="server">
        <th scope="row">SAT Score: <asp:Image ID="SATScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblSATScore"  runat="server"  /></td>
    </tr>
    <tr id="ACTScoreRow" runat="server">
        <th scope="row">ACT Score: <asp:Image ID="ACTScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblACTScore"  runat="server"  /></td>
    </tr>
    <tr id="ClassRankRow" runat="server">
        <th scope="row">Class Rank: <asp:Image ID="ClassRankUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblClassRank"  runat="server"  /></td>
    </tr>
    <tr id="APCreditsEarnedRow" runat="server">
        <th scope="row">AP Credits Earned: <asp:Image ID="APCUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblAPCredits"  runat="server"  /></td>
    </tr>
    <tr id="IBCreditsEarnedRow" runat="server">
        <th scope="row">IB Credits Earned: <asp:Image ID="IBCUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblIBCredits"  runat="server"  /></td>
    </tr>
    <tr id="HonorsRow" runat="server">
        <th scope="row">Honors: <asp:Image ID="HonorsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Label ID="lblHonors"  runat="server"  /></td>
    </tr>
</table>


<div id="linkarea2" runat="server" class="linkarea">
 <p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>
