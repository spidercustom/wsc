﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class SeekerInfoShow : BaseScholarshipShow
    {
        private const string REQUIRED = "Required";
        private const string SHOULD_NOT_BE_FIRST_GENERATION = "Should not be first generation";
        private const string HONOR_SHOULD_NOT_BE_AWARDED = "Honor should not be awarded";
        
        public override ScholarshipStages Stage { get { return ScholarshipStages.SeekerProfile; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);

            var seeker = ScholarshipToView.SeekerProfileCriteria;

            MapSeekerPersonality(seeker);
            MapSeekerDemographics(seeker);
            MapSeekerInterests(seeker);
            MapSeekerActivities(seeker);
            MapSeekerPerformace(seeker);

            SetupAttributeUsageTypeIcons(seeker);
            SetupVisibility(seeker);
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            FiveWordsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SeekerVerbalizingWord);
            FiveSkillRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SeekerSkill);
            ApplicantPersonalityTable.Visible = ApplicantPersonalityHeader.Visible = 
                FiveWordsRow.Visible || 
                FiveSkillRow.Visible;

            StudentGroupRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.StudentGroup);
            SchoolTypesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SchoolType);
            AcademicProgramsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.AcademicProgram);
            EnrollmentStatusesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SeekerStatus);
            LengthOfProgrammRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ProgramLength);
            CollegesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.College);
            ApplicantStudentTypeTable.Visible  = ApplicantStudentTypeHeader.Visible =
                StudentGroupRow.Visible ||
                SchoolTypesRow.Visible ||
                AcademicProgramsRow.Visible ||
                EnrollmentStatusesRow.Visible ||
                LengthOfProgrammRow.Visible ||
                CollegesRow.Visible;


            FirstGenerationRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.FirstGeneration);
            EthnicityRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Ethnicity);
            ReligionRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Religion);
            GendersRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Gender);
            ApplicantDemographicsPeronalTable.Visible  = ApplicantDemographicsPeronalHeader.Visible = 
                FirstGenerationRow.Visible ||
                EthnicityRow.Visible ||
                ReligionRow.Visible ||
                GendersRow.Visible;

            StatesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SeekerGeographicsLocation);
            CountiesRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.County;
            CitiesRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.City;
            SchoolDistrictsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.SchoolDistrict;
            HighSchoolsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.HighSchool;
            ApplicantDemographicsGeographicTable.Visible = 
                ApplicantDemographicsGeographicHeader.Visible =
                StatesRow.Visible;
            

            AcademicAreasRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.AcademicArea);
            CareersRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Career);
            CommunityServicesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.CommunityService);
            OrganizationsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.OrganizationAndAffiliationType);
            AffiliationTypesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.OrganizationAndAffiliationType);
            ApplicantInterestsTable.Visible  = ApplicantInterestsHeader.Visible = 
                AcademicAreasRow.Visible ||
                CareersRow.Visible ||
                CommunityServicesRow.Visible ||
                OrganizationsRow.Visible ||
                AffiliationTypesRow.Visible;

            HobbiesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Honor);
            SportsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Sport);
            ClubsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Club);
            WorkTypesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.WorkType);
            WorkHoursRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.WorkHour);
            ServiceTypesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ServiceType);
            ServiceHoursRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ServiceHour);
            ApplicantActivitiesTable.Visible  = ApplicantActivitiesHeader.Visible = 
                HobbiesRow.Visible ||
                SportsRow.Visible ||
                ClubsRow.Visible ||
                WorkTypesRow.Visible ||
                WorkHoursRow.Visible ||
                ServiceTypesRow.Visible ||
                ServiceHoursRow.Visible;

            GPARow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.GPA);
            SATScoreRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SAT);
            ACTScoreRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ACT);
            ClassRankRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ClassRank);
            APCreditsEarnedRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.APCreditsEarned);
            IBCreditsEarnedRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.IBCreditsEarned);
            HonorsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Honor);
            ApplicantStudentPerformanceTable.Visible = ApplicantStudentPerformanceHeader.Visible =
                GPARow.Visible ||
                SATScoreRow.Visible ||
                ACTScoreRow.Visible ||
                ClassRankRow.Visible ||
                APCreditsEarnedRow.Visible ||
                IBCreditsEarnedRow.Visible ||
                HonorsRow.Visible;

            NoSelectionContainerControl.Visible = !(
                ApplicantPersonalityHeader.Visible ||
                ApplicantStudentTypeHeader.Visible ||
                ApplicantDemographicsPeronalHeader.Visible ||
                ApplicantDemographicsGeographicHeader.Visible ||
                ApplicantInterestsHeader.Visible ||
                ApplicantActivitiesHeader.Visible ||
                ApplicantStudentPerformanceHeader.Visible);
        }

        private void MapSeekerPerformace(SeekerProfileCriteria seeker)
        {
            if (seeker.GPA != null)
            {
                if (seeker.GPA.Maximum.HasValue && seeker.GPA.Maximum != Scholarship.MAX_GPA)
                    lblGPA.Text = string.Format("{0} - {1}", seeker.GPA.Minimum, seeker.GPA.Maximum);
                else
                    lblGPA.Text = string.Format("Greater than {0}", seeker.GPA.Minimum);
            }
            if (seeker.SATScore != null)
            {
                string satscore = string.Format("Critical Reading : {0} - {1}",
                                                seeker.SATScore.CriticalReading.Minimum,
                                                seeker.SATScore.CriticalReading.Maximum);
                satscore += string.Format("Writing : {0} - {1}", seeker.SATScore.Writing.Minimum,
                                          seeker.SATScore.Writing.Maximum);
                satscore += string.Format("Mathematics : {0} - {1}", seeker.SATScore.Mathematics.Minimum,
                                          seeker.SATScore.Mathematics.Maximum);
                lblSATScore.Text = satscore;
            }

            if (seeker.ACTScore != null)
            {
                string actscore = string.Format("Reading : {0} - {1}",
                                                seeker.ACTScore.Reading.Minimum,
                                                seeker.ACTScore.Reading.Maximum);

                actscore += string.Format("Mathematics : {0} - {1}",
                                          seeker.ACTScore.Mathematics.Minimum,
                                          seeker.ACTScore.Mathematics.Maximum);
                actscore += string.Format("English: {0} - {1}",
                                          seeker.ACTScore.English.Minimum,
                                          seeker.ACTScore.English.Maximum);
                actscore += string.Format("Science: {0} - {1}",
                                          seeker.ACTScore.Science.Minimum,
                                          seeker.ACTScore.Science.Maximum);
                lblACTScore.Text = actscore;
            }

            if (seeker.ClassRank != null)
                lblClassRank.Text = string.Format("{0} - {1}", seeker.ClassRank.Minimum, seeker.ClassRank.Maximum);


            lblAPCredits.Text = seeker.APCreditsEarned.ToString();
            lblIBCredits.Text = seeker.IBCreditsEarned.ToString();
            lblHonors.Text = seeker.Honors ? REQUIRED : HONOR_SHOULD_NOT_BE_AWARDED;
        }

        private void MapSeekerActivities(SeekerProfileCriteria seeker)
        {
            MapIf(lblHobbies, seeker.Hobbies);
            MapIf(lblSports, seeker.Sports);
            MapIf(lblClubs, seeker.Clubs);
            MapIf(lblServiceHours, seeker.ServiceHours);
        }

        private void MapSeekerInterests(SeekerProfileCriteria seeker)
        {
            MapIf(lblAcademicAreas, seeker.AcademicAreas);
            MapIf(lblCareers, seeker.Careers);
            MapIf(lblCauses, seeker.CommunityServices);
            MapIf(lblOrganizations, seeker.Organizations);
            MapIf(lblAffiliationTypes, seeker.AffiliationTypes);
        }

        private void MapSeekerDemographics(SeekerProfileCriteria seeker)
        {
            MapIf(lblEthinicity, seeker.Ethnicities);
            MapIf(lblReligion, seeker.Religions);
            MapIf(lblCounty, seeker.Counties);
            MapIf(lblCity, seeker.Cities);
            MapIf(lblSchoolDistrict, seeker.SchoolDistricts);
            MapIf(lblHighSchool, seeker.HighSchools);
            MapIf(lblState, seeker.States);

            lblGender.Text = seeker.Genders.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);


        }

        private void SetupAttributeUsageTypeIcons(SeekerProfileCriteria criteria)
        {
            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(MinimumCriteriaIconControl,
                                                                    ScholarshipAttributeUsageType.Minimum);
            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(PreferenceIconCcontrol,
                                                                    ScholarshipAttributeUsageType.Preference);

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FiveWordsUsageTypeIconControl,
                           criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerVerbalizingWord));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FiveSkillsUsageTypeIconControl,
                           criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerSkill));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(StudentGroupUsageTypeIconControl,
                           criteria.GetAttributeUsageType(SeekerProfileAttribute.StudentGroup));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SchoolTypeUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SchoolType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicProgramUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.AcademicProgram));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerStatusUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerStatus));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ProgramLengthUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ProgramLength));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CollegesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.College));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FirstGenerUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.FirstGeneration));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(EthnicityUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Ethnicity));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ReligionUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Religion));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GendersUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Gender));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicAreasUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.AcademicArea));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CareersUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Career));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CommunityServicesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.CommunityService));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(OrganizationsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.OrganizationAndAffiliationType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AffiliationTypesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.OrganizationAndAffiliationType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerHobbiesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerHobby));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SportsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Sport));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClubsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Club));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkTypeUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.WorkType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkHoursUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.WorkHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceTypeUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ServiceType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceHoursUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ServiceHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GPAUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.GPA));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClassRankUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ClassRank));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SATScoreUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SAT));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ACTScoreUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ACT));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(HonorsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Honor));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(APCUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.APCreditsEarned));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(IBCUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.IBCreditsEarned));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerDemographicsGeographicUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerGeographicsLocation));

        }


        private void MapSeekerPersonality(SeekerProfileCriteria seeker)
        {
            MapIf(lblFiveWords, seeker.Words);
            MapIf(lblFiveSkills, seeker.Skills);
            MapIf(lblColleges, seeker.Colleges);

            lblStudentGroups.Text = seeker.StudentGroups.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            lblSchoolTypes.Text = seeker.SchoolTypes.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            lblAcademicPrograms.Text = seeker.AcademicPrograms.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            lblSeekerStatuses.Text = seeker.SeekerStatuses.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            lblLengthOfProgram.Text = seeker.ProgramLengths.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            lblFirstGeneration.Text = seeker.FirstGeneration ? REQUIRED : SHOULD_NOT_BE_FIRST_GENERATION;
        }

        private static void MapIf<T>(ITextControl lbl, IList<T> items) where T : ILookup
        {
            if (null != items && items.Count > 0)
                lbl.Text = items.CommaSeparatedNames();
        }
    }
}