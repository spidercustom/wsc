﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class SelectRelatedIntermediary : UserControl
    {
        private Domain.Intermediary selected;

        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        public Domain.Intermediary SelectedValue { 
            get
            {
                var selectedId = -1;
                if (null != selected)
                {
                    selectedId = selected.Id;
                }
                int newId= Int32.Parse(intermediaryDDL.SelectedValue);
                if (newId != selectedId)
                {
                    selected = IntermediaryService.FindById(newId);
                }
                return selected;
            }

            set { selected = value; } 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            intermediaryNotAvailable.Visible = false;

            if (!IsPostBack)
            {
                IList<Organization> orgs;
                string selectedValue;
                var currentOrg = UserContext.CurrentOrganization;
                if (currentOrg is Domain.Intermediary)
                {
                    orgs = new List<Organization> { currentOrg };
                    selectedValue = null == selected ? null : selected.Id.ToString();
                }
                else
                {
                    orgs = RelationshipService.GetActiveByOrganization(currentOrg);
                    orgs.Add(new Domain.Intermediary { Id = -1, Name = currentOrg.Name });
                    selectedValue = null == selected ? "-1" : selected.Id.ToString();
                }

                intermediaryDDL.DataSource = orgs;
                intermediaryDDL.DataTextField = "Name";
                intermediaryDDL.DataValueField = "Id";

                if (null != selectedValue)
                {
                    // If the current Intermediary has been removed from the relationships, then
                    // we want to show a message to the user and don't select them or else we get an error.
                    if (!orgs.Any(o => o.Id.ToString() == selectedValue))
                    {
                        intermediaryNotAvailable.Visible = true;
                    }
                    else
                    {
                        intermediaryDDL.SelectedValue = selectedValue;
                    }
                }

                intermediaryDDL.DataBind();
            }
            intermediaryDDL_SelectedIndexChanged(sender, e);
        }

        protected void intermediaryDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            hecbMessage.Visible = "-1" == intermediaryDDL.SelectedValue;
        }
    }
}