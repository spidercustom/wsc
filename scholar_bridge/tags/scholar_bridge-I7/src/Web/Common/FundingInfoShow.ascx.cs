﻿using System;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class FundingInfoShow : BaseScholarshipShow
    {
        public const string FAFSA = "FAFSA";
        public const string USER_DERIVED = "User Derived";

        public override ScholarshipStages Stage { get { return ScholarshipStages.FundingProfile; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);

            //view funding info
			if (ScholarshipToView.FundingProfile.HasAttributeUsage(FundingProfileAttribute.Need))
			{
				var need = ScholarshipToView.FundingProfile.Need;

				if (need.Fafsa)
					lblNeed.Text = FAFSA;
				if (need.Fafsa && need.UserDerived)
					lblNeed.Text += ", " + USER_DERIVED;
				else if (need.UserDerived)
					lblNeed.Text = USER_DERIVED;
				lblMinMaxNeed.Text = string.Format("{0} - {1}", 
										need.MinimumSeekerNeed.ToString("c"),
										need.MaximumSeekerNeed.ToString("c"));

				if (need.NeedGaps.Count > 0)
					lblNeedGAP.Text = need.NeedGaps.CommaSeparatedNames();
				
			}
			else
			{
                FundingNeedsContainer.Visible = false;
			}

			if (ScholarshipToView.FundingProfile.HasAttributeUsage(FundingProfileAttribute.SupportedSituation))
			{
				lblTypeOfSupport.Text = string.Empty;
				foreach (var supportType in ScholarshipToView.FundingProfile.SupportedSituation.TypesOfSupport)
				{
					lblTypeOfSupport.Text += supportType.Name + "<br />";
				}
			}
			else
			{
                FundingSituationContainer.Visible = false;
			}
            NoSelectionContainerControl.Visible = !(FundingNeedsContainer.Visible || FundingSituationContainer.Visible);
            SetupAttributeUsageTypeIcons();
        }

        private void SetupAttributeUsageTypeIcons()
        {
            Domain.ScholarshipParts.FundingProfile fundingProfile = ScholarshipToView.FundingProfile;

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(NeedUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.Need));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SupportSituationUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.SupportedSituation));
        }

    }
}