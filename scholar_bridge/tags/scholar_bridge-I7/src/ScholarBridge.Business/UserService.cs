﻿using System;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class UserService : IUserService 
    {
        public IUserDAL UserDAL { get; set; }
        public IProviderDAL ProviderDAL { get; set; }
        public IIntermediaryDAL IntermediaryDAL { get; set; }
        public IMessagingService MessagingService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }

        public User FindByEmail(string email)
        {
            return UserDAL.FindByEmail(email);
        }

		public void Update(User user)
		{
			UserDAL.Update(user);
		}

		public void Insert(User user)
		{
			UserDAL.Insert(user);

			// make sure that the lastUpdate is populated
			if (user.LastUpdate == null)
			{
				user.LastUpdate = new ActivityStamp() { By = user, On = user.CreationDate ?? DateTime.Now };
				UserDAL.Update(user);
			}
		}

		public void Delete(User user)
        {
            UserDAL.Delete(user);

		}

        public void ActivateUser(User user)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            if (!user.IsActive)
            {
                user.IsActive = true;
                Update(user);
            }
        }

        public void ActivateSeekerUser(User user)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            // Seeker doesn't require secondary approval, so approve and activate
            user.IsApproved = true;
            user.IsActive = true;
            Update(user);
        }

        public void ActivateProviderUser(User user)
        {
            ActivateUser(user);

            if (!user.IsApproved)
            {
                Provider provider = ProviderDAL.FindByUser(user);
                SendAdminActivationMessage(user, provider, MessageType.RequestProviderApproval);
            }
        }

        public void ActivateIntermediaryUser(User user)
        {
            ActivateUser(user);

            if (!user.IsApproved)
            {
                Intermediary intermediary = IntermediaryDAL.FindByUser(user);
                SendAdminActivationMessage(user, intermediary, MessageType.RequestIntermediaryApproval);
            }
        }

        public void ResetUsername(User user, string newEmail)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            user.Email = newEmail;
            user.Username = newEmail;
            user.IsActive = false;
            Update(user);

            SendConfirmationEmail(user, false);
        }

        public void UpdateEmailAddress(User user,string newEmail)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            user.EmailWaitingforVerification = newEmail;
            Update(user);

            SendEmailAddressChangeVerificationEmail(user);
        }
        public void ConfrimEmailAddressVerification(User user)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            user.Email = user.EmailWaitingforVerification;
            user.EmailWaitingforVerification = null;
            Update(user);
        }
        public void SendEmailAddressChangeVerificationEmail(User user)
        {
            var templateParams = new MailTemplateParams();
            TemplateParametersService.EmailAddressChangeVerificationLink(user, templateParams);

            MessagingService.SendEmail(user.EmailWaitingforVerification, MessageType.EmailAddressChangeVerificationLink, templateParams, true);

        }

        public void SendConfirmationEmail(User user, bool requiresResetPassword)
        {
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ConfirmationLink(user, requiresResetPassword, templateParams);
            MessagingService.SendEmail(user, MessageType.ConfirmationLink, templateParams, true);
        }

        private void SendAdminActivationMessage(User user, Organization organization, MessageType template)
        {
            var templateParams = new MailTemplateParams();
            TemplateParametersService.RequestOrganizationApproval(user, organization, templateParams);
            var msg = new OrganizationMessage
                          {
                              MessageTemplate = template,
                              From = new MessageAddress {User = user, Organization = organization},
                              LastUpdate = new ActivityStamp(user),
                              RelatedOrg = organization
                          };
            MessagingService.SendMessageToAdmin(msg, templateParams, true);
        }
    }
}
