using System;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class ActivateProviderAction : IAction
    {
        public IProviderService ProviderService { get; set; }
        public IMessageService MessageService { get; set; }

        public bool SupportsApprove { get { return true; } }

        public bool SupportsReject { get { return true; } }

        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
            if (! (message is OrganizationMessage))
                throw new ArgumentException("Message must be an OrganizationMessage");

            var org = ((OrganizationMessage) message).RelatedOrg;
            org.LastUpdate = new ActivityStamp(approver);
            ProviderService.Approve((Provider)org);

            message.ActionTaken = MessageAction.Approve;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
            if (!(message is OrganizationMessage))
                throw new ArgumentException("Message must be an OrganizationMessage");

            var org = ((OrganizationMessage)message).RelatedOrg;
            org.LastUpdate = new ActivityStamp(approver);
            ProviderService.Reject((Provider)org);

            message.ActionTaken = MessageAction.Deny;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
        }
    }
}