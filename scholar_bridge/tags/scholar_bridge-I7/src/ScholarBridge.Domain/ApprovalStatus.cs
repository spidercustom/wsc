using ScholarBridge.Common.Extensions;
using ScholarBridge.Common;
namespace ScholarBridge.Domain
{
    public enum ApprovalStatus
    {
        [DisplayName("None")]
        None,
        [DisplayName("Pending Approval")]
        PendingApproval,
        [DisplayName("Approved")]
        Approved,
        [DisplayName("Rejected")]
        Rejected
    }

    
}