﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain
{
    public class Scholarship
    {
        private const string NOT_ACTIVATED_STATUS_TEXT = "Not Activated";
        public const int MAX_GPA = 5;

        public Scholarship()
        {
        	InitializeMembers();        
		}    	
		private void InitializeMembers()    	
		{    		
			AdditionalRequirements = new List<AdditionalRequirement>();
    		AdditionalQuestions = new List<ScholarshipQuestion>();
    		FundingProfile = new FundingProfile();
    		Donor = new ScholarshipDonor();
    		SeekerProfileCriteria = new SeekerProfileCriteria();

    		CompletedStages = new List<ScholarshipStages>();
    		Attachments = new List<Attachment>();
    	}

    	public virtual int Id { get; set; }

        [NotNullValidator]
        [StringLengthValidator(1, 100)]
        public virtual string Name { get; set; }

        public virtual AcademicYear AcademicYear { get; set; }

        [StringLengthValidator(0, 2000)]
        public virtual string MissionStatement { get; set; }

        [NotNullValidator(MessageTemplate="Application Start Date should be a valid date.")]
        public virtual DateTime? ApplicationStartDate { get; set; }
        
        [NotNullValidator]
        [PropertyComparisonValidator("ApplicationStartDate", ComparisonOperator.GreaterThan, MessageTemplate = "Application Due Date should be greater than Application Start Date")]
        public virtual DateTime? ApplicationDueDate { get; set; }
        
        [NotNullValidator]
        [PropertyComparisonValidator("ApplicationDueDate", ComparisonOperator.GreaterThan, MessageTemplate = "Award Date should be greater than Application Due Date")]
        public virtual DateTime? AwardDate { get; set; }

        public virtual DateTime? ActivatedOn { get; set; }

        public virtual DateTime? AwardPeriodClosed { get; set; }

        public virtual string ProgramGuidelines { get; set; }
        public virtual ScholarshipDonor Donor { get; set; }

        [NotNullValidator]
        public virtual Provider Provider { get; set; }

        public virtual Intermediary Intermediary { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual ScholarshipStages Stage { get; set; }

        [NotNullValidator]
        [MinimumValidator(0)]
        public virtual decimal MinimumAmount { get; set; }

        [NotNullValidator]
        [MinimumValidator(1)]
        [PropertyComparisonValidator("MinimumAmount", ComparisonOperator.GreaterThanEqual, MessageTemplate = "Maximum amount should be greater than or equal to minimum amount")]
        public virtual decimal MaximumAmount { get; set; }

        public virtual string AmountRange
        {
            get { return String.Format("{0:$#,###;($#,###);$0} to {1:$#,###;($#,###);$0}", MinimumAmount, MaximumAmount); }
        }

        public virtual SeekerProfileCriteria SeekerProfileCriteria { get; set; }

        public virtual FundingProfile FundingProfile { get; set; }

        public virtual IList<Attachment> Attachments { get; protected set; }

		public virtual IList<ScholarshipQuestion> AdditionalQuestions { get; protected set; }

        public virtual IList<ScholarshipStages> CompletedStages { get; set; }

		public virtual AdminNotes AdminNotes { get; protected set; }

        /// <summary>
        /// Remove a Question from the Scholarship.
        /// </summary>
        /// <remarks>
        /// You really need to use this to add Questions because of the oddities of one-to-many 
        /// relationships when the key on the child table is NOT NULL.
        /// </remarks>
        /// <param name="question"></param>
        public virtual void AddAdditionalQuestion(ScholarshipQuestion question)
        {
            question.Scholarship = this;
            AdditionalQuestions.Add(question);
        }

        public virtual void RemoveAdditionalQuestion(int position)
        {
            AdditionalQuestions.RemoveAt(position);
        }

		public virtual IList<AdditionalRequirement> AdditionalRequirements { get; protected set; }

		public virtual void ResetAdditionalRequirements(IList<AdditionalRequirement> additionalReqs)
        {
            AdditionalRequirements.Clear();
            foreach (var ar in additionalReqs)
            {
                AdditionalRequirements.Add(ar);
            }
        }

        public virtual string DisplayName
        {
            get
            {
                if (null != Donor && null != Donor.Name && ! String.IsNullOrEmpty(Donor.Name.Trim()))
                    return String.Format("{0} - {1}", Name, Donor.Name);
                return Name;
            }
        }

        public virtual bool CanEdit()
        {
            return ! IsInActivationProcess();
        }

        public virtual bool CanSubmitForActivation()
        {
            return  AreStagesRequiredForActivationCompleted() && (Stage != ScholarshipStages.None && !IsInActivationProcess());
        }

        public virtual bool IsAwardPeriodClosed
        {
            get { return AwardPeriodClosed.HasValue && AwardPeriodClosed.Value <= DateTime.Now; }
        }

        public virtual bool AreStagesRequiredForActivationCompleted()
        {
            return IsStageCompleted(ScholarshipStages.GeneralInformation) &&
                   IsStageCompleted(ScholarshipStages.MatchCriteriaSelection) &&
                   IsStageCompleted(ScholarshipStages.SeekerProfile) &&
                   IsStageCompleted(ScholarshipStages.FundingProfile) &&
                   IsStageCompleted(ScholarshipStages.AdditionalCriteria);
        }

        public virtual bool IsInActivationProcess()
        {
            return Stage == ScholarshipStages.RequestedActivation ||
                   Stage == ScholarshipStages.Activated ||
                   Stage == ScholarshipStages.Awarded;
        }

        public virtual void MarkStageCompleted(ScholarshipStages stage)
        {
            if (!CompletedStages.Contains(stage))
                CompletedStages.Add(stage);
        }

        public virtual void MarkStageIncomplete(ScholarshipStages stage)
        {
            if (CompletedStages.Contains(stage))
                CompletedStages.Remove(stage);
        }

        public virtual void MarkStageCompletion(ScholarshipStages stage, bool isCompleted)
        {
            if (isCompleted)
                MarkStageCompleted(stage);
            else
                MarkStageIncomplete(stage);
        }

        public virtual bool IsStageCompleted(ScholarshipStages stages)
        {
            return CompletedStages.Contains(stages);
        }

        public virtual bool IsBelongToOrganization(Organization organization)
        {
            return Provider.Id.Equals(organization.Id) ||Intermediary.Id.Equals(organization.Id);
        }

        /// <summary>
        /// Append notes to the end of the AdminNotes.
        /// The user passed in will be atrributed.
        /// </summary>
        /// <param name="userAppendingNotes">The user to attribute the notes to.</param>
        /// <param name="notes">The test to add.</param>
        public virtual void AppendAdminNotes(User userAppendingNotes, string notes)
        {
			if (AdminNotes == null)
				AdminNotes = new AdminNotes();
			AdminNotes.AppendAdminNote(userAppendingNotes, notes);
        }

        #region ICloneable Members

        public virtual object Clone(string baseAttachmentPath)
        {
            var result = (Scholarship) MemberwiseClone();
            result.Id = 0;
            result.AcademicYear = AcademicYear.CurrentScholarshipYear;
            result.CompletedStages = new List<ScholarshipStages>();
            result.LastUpdate = null;
            result.Stage = ScholarshipStages.None;
            result.SeekerProfileCriteria = (SeekerProfileCriteria) SeekerProfileCriteria.Clone();
            result.FundingProfile = (FundingProfile)FundingProfile.Clone();

            result.AdditionalRequirements = new List<AdditionalRequirement>(AdditionalRequirements);

            result.AdditionalQuestions = new List<ScholarshipQuestion>(AdditionalQuestions.Count);
			foreach (var question in AdditionalQuestions)
			{
				var clonedQuestion = (ScholarshipQuestion) question.Clone();
				clonedQuestion.Scholarship = result;
				result.AdditionalQuestions.Add(clonedQuestion);
			}

            result.Attachments = new List<Attachment>(Attachments.Count);
            foreach (var attachment in Attachments)
            {
                var clonedAttachment = (Attachment) attachment.Clone(baseAttachmentPath);
                result.Attachments.Add(clonedAttachment);
            }

            return result;
        }

        #endregion

        public virtual string GetStatusText()
        {
            if (Stage.IsIn(ScholarshipStages.None,
                    ScholarshipStages.GeneralInformation,
                    ScholarshipStages.MatchCriteriaSelection,
                    ScholarshipStages.SeekerProfile,
                    ScholarshipStages.FundingProfile,
                    ScholarshipStages.AdditionalCriteria,
                    ScholarshipStages.PreviewCandidatePopulation,
                    ScholarshipStages.BuildScholarshipMatchLogic,
                    ScholarshipStages.BuildScholarshipRenewalCriteria))
                return NOT_ACTIVATED_STATUS_TEXT;

            return Stage.GetDisplayName();
        }
	}
}
