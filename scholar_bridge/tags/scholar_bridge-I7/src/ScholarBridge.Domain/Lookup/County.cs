﻿using ScholarBridge.Domain.Location;

namespace ScholarBridge.Domain.Lookup
{
    public class County : LookupBase
    {
        public virtual State State { get; set; }
    }
}
