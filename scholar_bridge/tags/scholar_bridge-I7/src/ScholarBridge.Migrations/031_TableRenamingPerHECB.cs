﻿using System;
using System.Data;
using System.Collections.Generic;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    /// <summary>
    /// Part 1 of several steps to get to HECB's liking.
    /// 
    /// HECB standard says:
    /// 1) no underscores in the names
    /// 2) Id fields must be in the form TableName + "ID" (e.g. id for SBProvider is SBProviderID)
    /// </summary>
	[Migration(31)]
    public class TableRenamingPerHECB : Migration
    {
        public override void Up()
        {
			// rename all tables
			foreach(var table in Database.GetTables())
			{
				string newName = table;
				if (table.StartsWith("SB_"))
				{
					newName = "SB" + table.Substring(3); 
					Database.RenameTable(table, newName);

				}

				// change the primary key name
				if (Database.ColumnExists(newName, "Id"))
				{
					Database.RenameColumn(newName, "Id", newName + "ID");
				}
			}
        }

        public override void Down()
        {
			// rename all tables back
			foreach (var table in Database.GetTables())
			{
				string newName = table;
				string oldName = table;
				if (table.StartsWith("SB"))
				{
					if (!table.StartsWith("SB_"))
					{
						newName = "SB_" + table.Substring(2);
						Database.RenameTable(table, newName);
					}
				}

				// change the primary keys back
				if (Database.ColumnExists(newName, oldName + "ID"))
				{
					Database.RenameColumn(newName, oldName + "ID", "Id");
				}
			}
		}
    }
}
