﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(110)]
    public class AddYearToScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";
        private const string COLUMN_NAME = "AcademicYear";

        public override void Up()
        {
            var column = new Column(COLUMN_NAME, DbType.Int16, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, column);
            Database.Update(TABLE_NAME, new[] {COLUMN_NAME}, new[] {"2009"});
            
            column.ColumnProperty = ColumnProperty.NotNull;
            Database.ChangeColumn(TABLE_NAME, column);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
        }
    }
}