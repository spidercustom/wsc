using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(192)]
    public class AddApplicationAttachmentsRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationAttachmentRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBAttachment"; }
        }
    }
}