﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(204)]
	public class AdminNotesToXML : Migration
    {
        public override void Up()
        {
			Database.ExecuteNonQuery("update SBOrganization set AdminNotes = null");
			Database.ExecuteNonQuery("update SBScholarship set AdminNotes = null");
        	Database.ExecuteNonQuery("ALTER TABLE SBScholarship ALTER COLUMN AdminNotes nvarchar(max) null ");
		}

        public override void Down()
        {
			Database.ExecuteNonQuery("update SBOrganization set AdminNotes = null");
			Database.ExecuteNonQuery("update SBScholarship set AdminNotes = null");
		}
    }
}