﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(138)]
    public class AddSeekerServiceHourRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBServiceHourLUT"; }
        }
    }
}
