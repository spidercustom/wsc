using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class SeekerServiceTests
    {
        private MockRepository mocks;
        private SeekerService seekerService;
        private ISeekerDAL seekerDal;
        private IRoleDAL roleDal;
        private IUserService userService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            seekerDal = mocks.StrictMock<ISeekerDAL>();
            userService = mocks.StrictMock<IUserService>();
            roleDal = mocks.StrictMock<IRoleDAL>();
            seekerService = new SeekerService
                                {
                                    SeekerDAL = seekerDal,
                                    UserService = userService,
                                    RoleService = roleDal
                                };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(seekerDal);
            mocks.BackToRecord(userService);
            mocks.BackToRecord(roleDal);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_new_throws_exception_if_null_passed()
        {
            seekerService.SaveNew(null);
            Assert.Fail();
        }

        [Test]
        public void save_new_sets_proper_roles()
        {
            var user = new User();
            var seeker = new Seeker
                             {
                                User = user
                             };
            Expect.Call(roleDal.FindByName(Role.SEEKER_ROLE)).Return(new Role {Name = Role.SEEKER_ROLE});
			Expect.Call(() => userService.Insert(seeker.User));
			Expect.Call(() => userService.Update(seeker.User));
			Expect.Call(seekerDal.Insert(seeker)).Return(seeker);
			Expect.Call(() => userService.SendConfirmationEmail(seeker.User, false));
            mocks.ReplayAll();

            seekerService.SaveNew(seeker);

            mocks.VerifyAll();
            Assert.AreEqual(1, seeker.User.Roles.Count);
        }

    	[Test]
    	public void can_retrieve_seeker_from_user()
    	{
            var user = new User();
			var seeker = new Seeker
			{
				User = user
			};

			Expect.Call(seekerDal.FindByUser(user)).Return(seeker);
            mocks.ReplayAll();

			Seeker foundSeeker = seekerService.FindByUser(user);

            mocks.VerifyAll();
            Assert.IsNotNull(foundSeeker);
			Assert.AreEqual(seeker, foundSeeker);
    	}
    }
}