using System;
using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class MatchServiceTests
    {
        private MockRepository mocks;
        private MatchService matchService;
        private IMatchDAL matchDAL;
        private ISeekerDAL seekerDAL;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            matchDAL = mocks.StrictMock<IMatchDAL>();
            seekerDAL = mocks.StrictMock<ISeekerDAL>();
            matchService = new MatchService
                               {
                                   MatchDAL = matchDAL,
                                   SeekerDAL = seekerDAL
                               };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(matchDAL);
            mocks.BackToRecord(seekerDAL);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void apply_for_match_throws_exception_if_null_passed()
        {
            matchService.ApplyForMatch(null, 0, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void change_status_throws_exception_if_null_passed()
        {
            matchService.ChangeStatus(null, 0, MatchStatus.New);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_saved_matches_seeker_throws_exception_if_null_passed()
        {
            Seeker s = null;
            matchService.GetSavedMatches(s);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void update_matches_throws_exception_if_null_passed()
        {
            matchService.UpdateMatches(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_saved_matches_scholarship_throws_exception_if_null_passed()
        {
            Scholarship s = null;
            matchService.GetSavedMatches(s);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_matches_for_seeker_throws_exception_if_null_passed()
        {
            matchService.GetMatchesForSeeker(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_matches_with_applications_throws_exception_if_null_passed()
        {
            matchService.GetSavedMatchesWithApplications(null);
            Assert.Fail();
        }

        [Test]
        public void get_saved_passes_proper_enum()
        {
            var seeker = new Seeker();
            Expect.Call(matchDAL.FindAll(seeker, new[] { MatchStatus.Saved, MatchStatus.Applied })).Return(new List<Match>());
            mocks.ReplayAll();

            matchService.GetSavedMatches(seeker);
            mocks.VerifyAll();
        }

        [Test]
        public void get_matches_doesnt_call_update_if_not_recently_updated()
        {
            var seeker = new Seeker {LastMatch = DateTime.Now, LastUpdate = new ActivityStamp { On = DateTime.Now.AddDays(-10) }};
            Expect.Call(matchDAL.FindAll(seeker)).Return(new List<Match>());
            mocks.ReplayAll();

            matchService.GetMatchesForSeeker(seeker);
            mocks.VerifyAll();
        }

        [Test]
        public void get_matches_calls_update_if_recently_updated()
        {
            var seeker = new Seeker { LastMatch = DateTime.Now.AddDays(-10), LastUpdate = new ActivityStamp { On = DateTime.Now } };
            Expect.Call(() => matchDAL.UpdateMatches(seeker));
            Expect.Call(seekerDAL.Update(seeker)).Return(seeker);
            Expect.Call(matchDAL.FindAll(seeker)).Return(new List<Match>());
            mocks.ReplayAll();

            matchService.GetMatchesForSeeker(seeker);
            mocks.VerifyAll();

            Assert.IsTrue(DateTime.Now.AddDays(-1) < seeker.LastMatch);
        }

        [Test]
        public void get_matches_calls_update_if_never_updated()
        {
            var seeker = new Seeker { LastUpdate = new ActivityStamp { On = DateTime.Now } };
            Expect.Call(() => matchDAL.UpdateMatches(seeker));
            Expect.Call(seekerDAL.Update(seeker)).Return(seeker);
            Expect.Call(matchDAL.FindAll(seeker)).Return(new List<Match>());
            mocks.ReplayAll();

            matchService.GetMatchesForSeeker(seeker);
            mocks.VerifyAll();

            Assert.IsTrue(DateTime.Now.AddDays(-1) < seeker.LastMatch);
        }

        [Test]
        public void save_match_changes_match_status()
        {
            var seeker = new Seeker { LastUpdate = new ActivityStamp { On = DateTime.Now } };
            var match = new Match {Id = 1};
            Expect.Call(matchDAL.Find(seeker, 1)).Return(match);
            Expect.Call(matchDAL.Update(match)).Return(match);
            mocks.ReplayAll();

            matchService.SaveMatch(seeker, 1);
            mocks.VerifyAll();
        }

        [Test]
        public void unsave_match_changes_match_status()
        {
            var seeker = new Seeker { LastUpdate = new ActivityStamp { On = DateTime.Now } };
            var match = new Match { Id = 1 };
            Expect.Call(matchDAL.Find(seeker, 1)).Return(match);
            Expect.Call(matchDAL.Update(match)).Return(match);
            mocks.ReplayAll();

            matchService.UnSaveMatch(seeker, 1);
            mocks.VerifyAll();
        }

        [Test]
        public  void apply_for_match()
        {
            var seeker = new Seeker();
            var match = new Match();
            var scholarshipId = 1;
            var application = new Application();

            Expect.Call(matchDAL.Find(seeker, scholarshipId)).Return(match);
            Expect.Call(matchDAL.Update(match)).Return(match);
            mocks.ReplayAll();

            matchService.ApplyForMatch(seeker, scholarshipId, application);
            Assert.AreEqual(application, match.Application);
            Assert.AreEqual(MatchStatus.Applied, match.MatchStatus);
        }

        [Test]
        public void get_saved_with_applications()
        {
            var scholarship = new Scholarship();
            Expect.Call(matchDAL.FindAllWithApplications(scholarship, new [] {MatchStatus.Saved})).Return(new List<Match>());
            mocks.ReplayAll();

            matchService.GetSavedMatchesWithApplications(scholarship);
            mocks.VerifyAll();
        }
    }
}