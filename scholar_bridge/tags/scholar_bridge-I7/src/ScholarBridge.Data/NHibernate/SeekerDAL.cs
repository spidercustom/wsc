using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class SeekerDAL : AbstractDAL<Seeker>, ISeekerDAL
    {
        public Seeker FindByUser(User user)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("User", user))
                .UniqueResult<Seeker>();
        }
    }
}