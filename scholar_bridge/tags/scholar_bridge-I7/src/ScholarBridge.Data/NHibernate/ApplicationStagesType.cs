using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>ApplicationStages</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class ApplicationStagesType : EnumStringType
    {
        public ApplicationStagesType()
            : base(typeof(ApplicationStages), 50)
        {
        }
    }
}