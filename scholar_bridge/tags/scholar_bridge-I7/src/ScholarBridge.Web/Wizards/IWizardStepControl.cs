﻿
namespace ScholarBridge.Web.Wizards
{
    /// <summary>
    /// Wizard interface is used for communicating with wizard step
    /// by wizard step manager
    /// </summary>
    public interface IWizardStepControl<T>
    {
        bool ValidateStep();
        void PopulateObjects();
        /// <summary>
        /// Saves changes to retrival storage.
        /// </summary>
        void Save();
        bool ChangeNextStepIndex();
        int GetChangedNextStepIndex();
        bool WasSuspendedFrom(T @object);
        bool CanResume(T @object);
        bool IsCompleted { get; set; }
        IWizardStepsContainer<T> Container { get; set; }
        void Activated();
    }
}
