﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(104)]
    public class ScholarshipMatchCriteriaAttributeUsageRT : Migration
    {
        private const string TABLE_NAME = "SBScholarshipMatchCriteriaAttributeUsageRT";
        private const string MATCH_CRITERIA_ATTRIBUTE_INDEX_COLUMN_NAME = "SBMatchCriteriaAttributeIndex";
        private const string ATTRIBUTE_INDEX_COLUMN_NAME = "SBAttributeIndex";

        public override void Up()
        {
            Database.RenameTable(ScholarshipAttributeUsageRT.TABLE_NAME, TABLE_NAME);
            Database.RenameColumn(TABLE_NAME, ATTRIBUTE_INDEX_COLUMN_NAME,
                MATCH_CRITERIA_ATTRIBUTE_INDEX_COLUMN_NAME);
        }

        public override void Down()
        {
            Database.RenameColumn(TABLE_NAME, MATCH_CRITERIA_ATTRIBUTE_INDEX_COLUMN_NAME,
                ATTRIBUTE_INDEX_COLUMN_NAME);
            Database.RenameTable(TABLE_NAME, ScholarshipAttributeUsageRT.TABLE_NAME);
        }

    }
}
