﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(215)]
    public class UpdateAdministratorUserNames : Migration
    {
        private static readonly string[] NAME_COLUMNS = {"FirstName", "MiddleName", "LastName"};
        public override void Up()
        {
            Database.Update("SBUser", NAME_COLUMNS, new[] { "Administrator", string.Empty, string.Empty},
                            "username = 'wscadmin@scholarbridge.com' or username = 'admin'");
        }

        public override void Down()
        {
            Database.Update("SBUser", NAME_COLUMNS, new string[] { null, null, null },
                            "username = 'wscadmin@scholarbridge.com' or username = 'admin'");
        }
    }
}