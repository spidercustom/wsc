using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(159)]
    public class AddMatchViews : Migration
    {
        public override void Up()
        {
            Database.ExecuteNonQuery(SBMatchBools);
            Database.ExecuteNonQuery(SBMatchInEnum);
            Database.ExecuteNonQuery(SBMatchInList);
            Database.ExecuteNonQuery(SBMatchRanges);
            Database.ExecuteNonQuery(SBScholarshipCriteriaCounts);
        }

        public override void Down()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchBools]");
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInEnum]");
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInList]");
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchRanges]");
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBScholarshipCriteriaCounts]");
        }

        private const string SBMatchBools = @"
CREATE VIEW [dbo].[SBMatchBools]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Honors' as criterion, 
	s.MatchCriteriaHonors as selected,
	seeker.SBSeekerId, seeker.Honors as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.Honors=s.MatchCriteriaHonors
where match.SBMatchCriteriaAttributeIndex=33
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'FirstGeneration', 
	s.MatchCriteriaFirstGeneration as selected,
	seeker.SBSeekerId, seeker.FirstGeneration
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.FirstGeneration=s.MatchCriteriaFirstGeneration
where match.SBMatchCriteriaAttributeIndex=6";

        private const string SBMatchInEnum = @"
CREATE VIEW [dbo].[SBMatchInEnum]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'LastAttended' as criterion, 
	s.MatchCriteriaStudentGroups as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.LastAttended & s.MatchCriteriaStudentGroups) = seeker.LastAttended
where match.SBMatchCriteriaAttributeIndex=26
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SchoolTypes', 
	s.MatchCriteriaSchoolTypes as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.SchoolTypes & s.MatchCriteriaSchoolTypes) = seeker.SchoolTypes
where match.SBMatchCriteriaAttributeIndex=18
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AcademicPrograms', 
	s.MatchCriteriaAcademicPrograms as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.AcademicPrograms & s.MatchCriteriaAcademicPrograms) = seeker.AcademicPrograms
where match.SBMatchCriteriaAttributeIndex=1
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerStatuses', 
	s.MatchCriteriaSeekerStatuses as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.SeekerStatuses & s.MatchCriteriaSeekerStatuses) = seeker.SeekerStatuses
where match.SBMatchCriteriaAttributeIndex=21
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ProgramLengths', 
	s.MatchCriteriaProgramLengths as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.ProgramLengths & s.MatchCriteriaGenders) = seeker.ProgramLengths
where match.SBMatchCriteriaAttributeIndex=15
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Gender', 
	s.MatchCriteriaGenders as val, seeker.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.Gender & s.MatchCriteriaGenders) = seeker.Gender
where match.SBMatchCriteriaAttributeIndex=10";

        private const string SBMatchInList =
            @"CREATE VIEW [dbo].[SBMatchInList]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AcademicArea' as criterion, 
	rt.SBAcademicAreaIndex as idx, sRt.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAcademicAreaRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAcademicAreaRT sRt on rt.SBAcademicAreaIndex=sRt.SBAcademicAreaIndex
where match.SBMatchCriteriaAttributeIndex=0
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Career', 
	rt.SBCareerIndex as idx, sRt.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCareerRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCareerRT sRt on rt.SBCareerIndex=sRt.SBCareerIndex
where match.SBMatchCriteriaAttributeIndex=2
union all
--select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'City', 
--	rt.SBCityIndex as idx, sRt.SBSeekerId 
--from SBScholarshipMatchCriteriaAttributeUsageRT match
--inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
--inner join SBScholarshipCityRT rt on s.SBScholarshipId=rt.SBScholarshipId
--left join SBSeekerCityRT sRt on rt.SBCityIndex=sRt.SBCityIndex
--where match.SBMatchCriteriaAttributeIndex=3
--union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Club', 
	rt.SBClubIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipClubRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerClubRT sRt on rt.SBClubIndex=sRt.SBClubIndex
where match.SBMatchCriteriaAttributeIndex=4
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'College', 
	rt.SBCollegeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCollegeAppliedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
where match.SBMatchCriteriaAttributeIndex=5
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'College', 
	rt.SBCollegeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCollegeAcceptedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
where match.SBMatchCriteriaAttributeIndex=5
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'CommunityService', 
	rt.SBCommunityServiceIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCommunityServiceRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCommunityServiceRT sRt on rt.SBCommunityServiceIndex=sRt.SBCommunityServiceIndex
where match.SBMatchCriteriaAttributeIndex=7
--union all
--select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'County', 
--	rt.SBCountyIndex as idx, sRt.SBSeekerId 
--from SBScholarshipMatchCriteriaAttributeUsageRT match
--inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
--inner join SBScholarshipCountyRT rt on s.SBScholarshipId=rt.SBScholarshipId
--left join SBSeekerCountyRT sRt on rt.SBCountyIndex=sRt.SBCountyIndex
--where match.SBMatchCriteriaAttributeIndex=8
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Ethnicity', 
	rt.SBEthnicityIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipEthnicityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerEthnicityRT sRt on rt.SBEthnicityIndex=sRt.SBEthnicityIndex
where match.SBMatchCriteriaAttributeIndex=9
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'State', 
	rt.StateAbbreviation as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipStateRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker sRt on rt.StateAbbreviation=sRt.AddressState
where match.SBMatchCriteriaAttributeIndex=12
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'HighSchool', 
	rt.SBHighSchoolIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipHighSchoolRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker sRt on rt.SBHighSchoolIndex=sRt.CurrentHighSchoolIndex
where match.SBMatchCriteriaAttributeIndex=13
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AffiliationType', 
	rt.SBAffiliationTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAffiliationTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAffiliationTypeRT sRt on rt.SBAffiliationTypeIndex=sRt.SBAffiliationTypeIndex
where match.SBMatchCriteriaAttributeIndex=14
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'MatchOrganization', 
	rt.SBSeekerMatchOrganizationIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerMatchOrganizationRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerMatchOrganizationRT sRt on rt.SBSeekerMatchOrganizationIndex=sRt.SBSeekerMatchOrganizationIndex
where match.SBMatchCriteriaAttributeIndex=14
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Religion', 
	rt.SBReligionIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipReligionRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerReligionRT sRt on rt.SBReligionIndex=sRt.SBReligionIndex
where match.SBMatchCriteriaAttributeIndex=16
--union all
--select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SchoolDistrict', 
--	rt.SBSchoolDistrictIndex as idx, sRt.SBSeekerId 
--from SBScholarshipMatchCriteriaAttributeUsageRT match
--inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
--inner join SBScholarshipSchoolDistrictRT rt on s.SBScholarshipId=rt.SBScholarshipId
--left join SBSeekerSchoolDistrictRT sRt on rt.SBSchoolDistrictIndex=sRt.SBSchoolDistrictIndex
--where match.SBMatchCriteriaAttributeIndex=17
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerHobby', 
	rt.SBSeekerHobbyIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerHobbyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerHobbyRT sRt on rt.SBSeekerHobbyIndex=sRt.SBSeekerHobbyIndex
where match.SBMatchCriteriaAttributeIndex=19
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerSkill', 
	rt.SBSeekerSkillIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerSkillRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerSkillRT sRt on rt.SBSeekerSkillIndex=sRt.SBSeekerSkillIndex
where match.SBMatchCriteriaAttributeIndex=20
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerVerbalizingWord', 
	rt.SBSeekerVerbalizingWordIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerVerbalizingWordRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerVerbalizingWordRT sRt on rt.SBSeekerVerbalizingWordIndex=sRt.SBSeekerVerbalizingWordIndex
where match.SBMatchCriteriaAttributeIndex=22
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ServiceHour', 
	rt.SBServiceHourIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipServiceHourRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerServiceHourRT sRt on rt.SBServiceHourIndex=sRt.SBServiceHourIndex
where match.SBMatchCriteriaAttributeIndex=23
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ServiceType', 
	rt.SBServiceTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipServiceTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerServiceTypeRT sRt on rt.SBServiceTypeIndex=sRt.SBServiceTypeIndex
where match.SBMatchCriteriaAttributeIndex=24
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Sport', 
	rt.SBSportIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSportRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSportRT sRt on rt.SBSportIndex=sRt.SBSportIndex
where match.SBMatchCriteriaAttributeIndex=25
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'WorkHour', 
	rt.SBWorkHourIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipWorkHourRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerWorkHourRT sRt on rt.SBWorkHourIndex=sRt.SBWorkHourIndex
where match.SBMatchCriteriaAttributeIndex=27
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'WorkType',
	rt.SBWorkTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipWorkTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerWorkTypeRT sRt on rt.SBWorkTypeIndex=sRt.SBWorkTypeIndex
where match.SBMatchCriteriaAttributeIndex=28";

        private const string SBMatchRanges =
            @"CREATE VIEW [dbo].[SBMatchRanges]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'GPA' as criterion, 
	s.MatchCriteriaGPAGreaterThan as minimum, s.MatchCriteriaGPALessThan as maximum,
	seeker.SBSeekerId, seeker.GPA as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.GPA between s.MatchCriteriaGPAGreaterThan and  s.MatchCriteriaGPALessThan
where match.SBMatchCriteriaAttributeIndex=29
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex,  'ClassRank', 
	s.MatchCriteriaClassRankGreaterThan, s.MatchCriteriaClassRankLessThan,
	seeker.SBSeekerId, seeker.ClassRank
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ClassRank between s.MatchCriteriaClassRankGreaterThan and  s.MatchCriteriaClassRankLessThan
where match.SBMatchCriteriaAttributeIndex=30
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'APCredits', 
	s.MatchCriteriaAPCreditsEarned, 10000,
	seeker.SBSeekerId, seeker.APCredits
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.APCredits between s.MatchCriteriaAPCreditsEarned and 10000
where match.SBMatchCriteriaAttributeIndex=34
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'IBCredits', 
	s.MatchCriteriaIBCreditsEarned, 10000,
	seeker.SBSeekerId, seeker.IBCredits
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.IBCredits between s.MatchCriteriaIBCreditsEarned and  10000
where match.SBMatchCriteriaAttributeIndex=35
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATWriting', 
	s.MatchCriteriaSATWritingGreaterThan, s.MatchCriteriaSATWritingLessThan,
	seeker.SBSeekerId, seeker.SATWriting
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATWriting between s.MatchCriteriaSATWritingGreaterThan and  s.MatchCriteriaSATWritingLessThan
where match.SBMatchCriteriaAttributeIndex=31
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATCriticalReading', 
	s.MatchCriteriaSATCriticalReadingGreaterThan, s.MatchCriteriaSATCriticalReadingLessThan ,
	seeker.SBSeekerId, seeker.SATCriticalReading
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATCriticalReading between s.MatchCriteriaSATCriticalReadingGreaterThan and  s.MatchCriteriaSATCriticalReadingLessThan
where match.SBMatchCriteriaAttributeIndex=31
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATMathematics', 
	s.MatchCriteriaSATMathematicsGreaterThan, s.MatchCriteriaSATMathematicsLessThan,
	seeker.SBSeekerId, seeker.SATMathematics
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATMathematics between s.MatchCriteriaSATMathematicsGreaterThan and  s.MatchCriteriaSATMathematicsLessThan
where match.SBMatchCriteriaAttributeIndex=31
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTEnglish', 
	s.MatchCriteriaACTScoreEnglishGreaterThan, s.MatchCriteriaACTScoreEnglishLessThan ,
	seeker.SBSeekerId, seeker.ACTEnglish
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTEnglish between s.MatchCriteriaACTScoreEnglishGreaterThan and  s.MatchCriteriaACTScoreEnglishLessThan
where match.SBMatchCriteriaAttributeIndex=32
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTReading', 
	s.MatchCriteriaACTScoreReadingGreaterThan, s.MatchCriteriaACTScoreReadingLessThan,
	seeker.SBSeekerId, seeker.ACTReading
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTReading between s.MatchCriteriaACTScoreReadingGreaterThan and  s.MatchCriteriaACTScoreReadingLessThan
where match.SBMatchCriteriaAttributeIndex=32
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTMathematics', 
	s.MatchCriteriaACTScoreMathematicsGreaterThan, s.MatchCriteriaACTScoreMathematicsLessThan,
	seeker.SBSeekerId, seeker.ACTMathematics
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTMathematics between s.MatchCriteriaACTScoreMathematicsGreaterThan and  s.MatchCriteriaACTScoreMathematicsLessThan
where match.SBMatchCriteriaAttributeIndex=32
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTScience', 
	s.MatchCriteriaACTScoreScienceGreaterThan, s.MatchCriteriaACTScoreScienceLessThan ,
	seeker.SBSeekerId, seeker.ACTScience
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTScience between s.MatchCriteriaACTScoreScienceGreaterThan and  s.MatchCriteriaACTScoreScienceLessThan
where match.SBMatchCriteriaAttributeIndex=32";

        private const string SBScholarshipCriteriaCounts =
            @"CREATE VIEW [dbo].[SBScholarshipCriteriaCounts]
AS
SELECT SBScholarshipID, [1] as prefCrit, [2] as minCrit
FROM
(SELECT SBScholarshipID, SBUsageTypeIndex 
    FROM SBScholarshipMatchCriteriaAttributeUsageRT) AS SourceTable
PIVOT
(
COUNT(SBUsageTypeIndex)
FOR SBUsageTypeIndex IN ([1], [2])
) AS PivotTable";

    }
}