using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(216)]
    public class AddArticleTable : AddTableBase
    {
        public const string TABLE_NAME = "SBArticle";
        public const string PRIMARY_KEY_COLUMN = "SBArticleId";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("PostedDate", DbType.DateTime, ColumnProperty.NotNull), 
                           new Column("Title", DbType.String,250  , ColumnProperty.NotNull),
                           new Column("Body", DbType.String,4000  , ColumnProperty.NotNull),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())"),
                          
                           
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return null;
        }
    }
}