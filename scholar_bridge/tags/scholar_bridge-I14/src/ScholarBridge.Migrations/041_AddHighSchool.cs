﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(41)]
    public class AddHighSchool : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "HighSchool";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
