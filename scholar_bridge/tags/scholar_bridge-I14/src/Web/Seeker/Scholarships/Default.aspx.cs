﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Web.MatchList;

namespace ScholarBridge.Web.Seeker.Scholarships
{
	public partial class Default : SBBasePage
	{
	    private const string SCHOLARSHIP_VIEW_TEMPLATE = "~/Seeker/Matches/Show.aspx";
	    public IMatchService MatchService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Seeker currentSeeker;
	    private IList<Match> matches;
	    private readonly ActionHelper actionHelper = new ActionHelper();

	    public IList<Match> Matches
	    {
	        get
	        {
                if (matches == null)
                {
                    if (currentSeeker == null)
                        throw new InvalidOperationException("There is no seeker in context");
                    matches = MatchService.GetAppliedMatches(currentSeeker);
                }
	            return matches;
	        }
	    }

        protected override void OnInitComplete(EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
            currentSeeker = UserContext.CurrentSeeker;
            BindMyScholarships();
            base.OnInitComplete(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblProfileActivationMessage.Visible = !UserContext.CurrentSeeker.IsPublished();
        }

        private void BindMyScholarships()
        {
            myScholarhipList.DataSource = Matches;
            myScholarhipList.DataBind();
        }

        protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.UnSaveMatch(currentSeeker, scholarshipId);
        }

        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match)((ListViewDataItem)e.Item).DataItem);
                var link = (LinkButton)e.Item.FindControl("linkToScholarship");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(SCHOLARSHIP_VIEW_TEMPLATE) }.ToString()
                            + "?id=" + match.Scholarship.Id + "&print=true";
                link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
           

                var btn = (Button)e.Item.FindControl("DefaultActionButton");
                actionHelper.ActionConfigurator(match, btn);

                var submittedDateLabel = (Label)e.Item.FindControl("SubmittedDateLabel");
                submittedDateLabel.Text = match.Application.SubmittedDate == null
                                              ? "&lt;not submitted&gt;"
                                              : match.Application.SubmittedDate.Value.ToShortDateString();
                actionHelper.ActionConfigurator(match, btn);
            }
        }


        //protected void DefaultActionButton_OnCommand(object sender, CommandEventArgs e)
        //{
        //    var match = GetMatch(e);
        //    ExecuteDefaultAction(match);
        //    BindMyScholarships();
        //}

        protected void matchList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindMyScholarships();
        }

	    protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }
	}
}