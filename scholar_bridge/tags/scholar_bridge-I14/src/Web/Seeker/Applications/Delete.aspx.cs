﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Applications
{
    public partial class Delete : Page
    {
        public IUserContext UserContext { get; set; }
        public IApplicationService ApplicationService { get; set; }
        private const string DEFAULT_PAGEURL = "~/seeker/";
        
        private int ApplicationId
        {
            get
            {
                int applicationId;
                if (!Int32.TryParse(Request.Params["aid"], out applicationId))
                    throw new ArgumentException("Cannot understand value of parameter aid");
                return applicationId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();

            var application = ApplicationService.GetById(ApplicationId);
            if (application == null)
                throw new ArgumentNullException("application");
           
            if (!(application.Seeker==UserContext.CurrentSeeker))
                    throw new InvalidOperationException("Application doesn't belong to seeker in context.");



            ApplicationService.Delete(application);
             SuccessMessageLabel.SetMessage("Application has been deleted.");
            Response.Redirect(DEFAULT_PAGEURL);

        }
    }
}
