﻿<%@ Page Language="C#" MasterPageFile="~/Seeker/Seeker.Master" AutoEventWireup="true" CodeBehind="ChangeEmail.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Users.ChangeEmail" Title="Seeker | Users | Change Email" %>
<%@ Register TagPrefix="sb" TagName="EditUserEmail" Src="~/Common/EditUserEmail.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:EditUserEmail ID="editUserEmail" runat="server" 
         OnUserEmailSaved="editUserEmail_OnUserEmailSaved" 
        OnFormCanceled="editUserEmail_OnFormCanceled" />
</asp:Content>
