﻿<%@ Page Language="C#" MasterPageFile="~/Seeker/Seeker.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Matches.Default"  Title="Seeker | My Matches" %>

<%@ Register src="~/Common/MatchList.ascx" tagname="MatchList" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <ul style="display:inline">
        <li class="left-part">
            <h3>My Scholarships of interest</h3>
            <p>Placekeeper for WSC provided text</p>
        </li>
        <li class="right-part">
            <ul>
                <li>
                    <asp:Image ImageUrl="~/images/matched_scholarship.png" runat="server"/>
                    <span>Matched scholarship not yet saved to <strong>My Scholarships</strong> list</span>
                </li>
                <li>
                    <asp:Image ImageUrl="~/images/saved_scholarship.png" runat="server"/>
                    <span>Scholarship is saved to <strong>My Scholarships</strong> list</span>
                </li>
                <li>
                    <asp:Image ImageUrl="~/images/applied-scholarship.png" runat="server"/>
                    <span>Application is created for scholarship</span>
                </li>
            </ul>
        </li>
    </ul>
    <br />
    
    <sb:MatchList id="savedList" runat="server" OnMatchAction="list_OnMatchAction" PageSize="4" />

    <h3>My Matches</h3>
    <p>Scholarships you qualify for:</p>
    <sb:MatchList id="qualifyList" runat="server" OnMatchAction="list_OnMatchAction" />

</asp:Content>
