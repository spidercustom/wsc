﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.FundingInfoShow" %>

<div class="recordinfo">
<h3>Financial Need</h3>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>     

<ul class="exclude-in-print vertical-indent-level-1">
    <li><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" /> <span>indicates a required criteria for this scholarship</span></li>
    <li><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" /> <span>indicates a preferred, but not required criteria, for this scholarship</span></li>
</ul>

<asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
    <p class="vertical-indent-level-1">
        No Scholarship Match Criteria Selected. Please go to the "Selection Criteria" tab while editing scholarship, 
        to choose fields to be used for Scholarship selection
    </p>
</asp:PlaceHolder>


<asp:PlaceHolder ID="FundingNeedsContainer" runat="server">
    <h4>Financial Need <asp:Image ID="NeedUsageTypeIconControl" runat="server" /></h4>
    <table class="viewonlyTable">
        <tr>
            <th>FAFSA:</th>
            <td><asp:Literal ID="lblFafsa" runat="server"></asp:Literal></td>
        </tr>
    </table>
    <table class="viewonlyTable">
        <tr>
            <th>Financial Information:</th>
            <td><asp:Literal ID="lblFinancialInfo" runat="server"></asp:Literal></td>
        </tr>
    </table>
</asp:PlaceHolder>

<asp:PlaceHolder ID="FundingSituationContainer" runat="server">
    <h4>Situations the Scholarship will Fund <asp:Image ID="SupportSituationUsageTypeIconControl" runat="server" /></h4>
    <table class="viewonlyTable">
        <tr>
            <th>Types of Support:</th>
            <td><asp:Literal ID="lblTypeOfSupport" runat="server"></asp:Literal></td>
        </tr>
    </table>
</asp:PlaceHolder>

</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
