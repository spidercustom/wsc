﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipList : UserControl
    {
        public IApplicationService ApplicationService { get; set; }


        private IList<Scholarship> _Scholarships;
        public string EditActionLink { get; set; }
        public string ViewActionLink { get; set; }

        public IList<Scholarship> Scholarships
        {
            get { return _Scholarships; }
            set { _Scholarships = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Bind();
        }

        protected void Bind()
        {
            if (!(Scholarships == null))
            {
                lstScholarships.DataSource = (from s in Scholarships orderby s.Stage, s.Name select s).ToList();
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship) ((ListViewDataItem) e.Item).DataItem);

                var link = (HyperLink) e.Item.FindControl("linktoScholarship");
                link.NavigateUrl = BuildActionLink(scholarship);

                var applicantCountControl = (Label)e.Item.FindControl("applicantCountControl");
                applicantCountControl.Text = ApplicationService.CountAllSubmitted(scholarship).ToString();
            }
        }

        private string BuildActionLink(Scholarship scholarship)
        {
            if (scholarship.Stage.IsIn(ScholarshipStages.None, ScholarshipStages.NotActivated))
                return EditActionLink + "?id=" + scholarship.Id;

            return ViewActionLink + "?id=" + scholarship.Id;
        }

        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }
        protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }
    }
}