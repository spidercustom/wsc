﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class Login : UserControl
    {
    	private System.Web.UI.WebControls.Login Login1
    	{
    		get
    		{
				return (System.Web.UI.WebControls.Login)loginView.FindControl("Login1");
			}
    	}


        public IUserService UserService { get; set; }

        private System.Web.UI.WebControls.Login LoginControl
        {
            get { return ((System.Web.UI.WebControls.Login) loginView.FindControl("Login1")); }
        }

        private string Username
        {
            get { return LoginControl.UserName; }
        }

        private void SetDestinationUrl(string url)
        {
            LoginControl.DestinationPageUrl = url;
        }

		protected void Page_Init(object sender, EventArgs e)
		{
			// Note this
			Response.Cache.SetNoStore();

		}
		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				if (Request.Cookies["myCookie"] != null)
				{
					HttpCookie cookie = Request.Cookies.Get("myCookie");

					if (Login1 != null)
					{
						Login1.UserName = cookie.Values["username"];

						Login1.RememberMeSet = (!String.IsNullOrEmpty(Login1.UserName));
					}
				}
					
				if (Login1 != null)
				{
					TextBox txtUser = Login1.FindControl("UserName") as TextBox;
					if (txtUser != null)
						Page.SetFocus(txtUser);
					
				}
			}

		}
  
        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
			HttpCookie myCookie = new HttpCookie("myCookie");
			Boolean remember = Login1.RememberMeSet;

			if (remember)
			{
				Int32 persistDays = 30;
				myCookie.Values.Add("username", Login1.UserName);
				myCookie.Expires = DateTime.Now.AddDays(persistDays); //you can add years and months too here
			}
			else
			{
				myCookie.Values.Add("username", string.Empty); // overwrite empty string is safest
				myCookie.Expires = DateTime.Now.AddMinutes(5); //you can add years and months too here
			}

			Response.Cookies.Add(myCookie);
            if (null == Request["ReturnUrl"])
            {
                SetDestinationUrl(DestinationForUser(Username));
            }
        }

        public string DestinationForUser(string username)
        {
            var roles = new List<string>(Roles.GetRolesForUser(username));
            if (roles.Any(r => Role.PROVIDER_ROLE.Equals(r)))
            {
                return "~/Provider/";
            }
            if (roles.Any(r => Role.INTERMEDIARY_ROLE.Equals(r)))
            {
                return "~/Intermediary/";
            }
            if (roles.Any(r => Role.WSC_ADMIN_ROLE.Equals(r)))
            {
                return "~/Message/";
            }
            if (roles.Any(r => Role.SEEKER_ROLE.Equals(r)))
            {
                return "~/Seeker/";
            }

            return "~/";
        }

        protected void Login1_LoginError(object sender, EventArgs e)
        {
            var u = UserService.FindByEmail(Username);
            if (null != u)
            {
                // Just want to check a couple of properties to see why the user can't login
                // Otherwise just use the default error
                if (! u.IsApproved)
                {
                    LoginControl.FailureText = "You have not yet been approved by the administrators of the site. <br/> You will receive an email when you are approved and can login.";
                }
            }
        }
    }
}
