﻿using System;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Common
{
    public partial class SeekerProfileActivation : System.Web.UI.UserControl
    {
        #region consts
        private const string ACTIVATION_SUCCESS_NOTE =
            @"Congratulations, you have successfully activated your profile. Let the scholarship matches roll in!";

        private const string ACTIVATION_MESSAGES_TITLE = @"Profile activation";
        #endregion

        public ISeekerService SeekerService { get; set; }
        public UserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            var seekerProfile = GetSeekerObject(false);
            Visible = seekerProfile != null && !seekerProfile.IsPublished();
            base.OnPreRender(e);
        }

        protected void btnActivateProfile_Click(object sender, EventArgs e)
        {
            var seekerProfile = GetSeekerObject();
            if (seekerProfileActivationValidationErrors.ValidateActivationAndPopulateErrors(seekerProfile))
            {
                SeekerService.Activate(seekerProfile);
                ClientSideDialogs.ShowAlert(ACTIVATION_SUCCESS_NOTE, ACTIVATION_MESSAGES_TITLE);
            }
        }

        private Domain.Seeker GetSeekerObject()
        {
            return GetSeekerObject(true);
        }

        private Domain.Seeker GetSeekerObject(bool tryRetrivingUpdated)
        {
            var profilePage = Page as Seeker.Profile.Default;
            if (!tryRetrivingUpdated || null == profilePage)
                return UserContext.CurrentSeeker;

            return profilePage.GetUpdatedSeeker();
        }

    }
}