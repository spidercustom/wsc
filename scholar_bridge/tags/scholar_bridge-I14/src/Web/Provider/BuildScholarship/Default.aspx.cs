﻿using System;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain;
using ScholarBridge.Business;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Default : SBBasePage, IWizardStepsContainer<Scholarship>
	{
		#region Constants, Members and Properties
		public const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
		#endregion

		#region Event handlers
		protected void Page_Load(object sender, EventArgs e)
        {
			buildScholarshipWizardTabIntegrator.TabChanging += (buildScholarshipWizardTabIntegrator_TabChanging);
            Steps.ForEach(step => step.Container = this);

            if (!IsPostBack)
            {
                CheckForDataChanges = true;

                bool copyFromParameterExists = WizardStepContainerCommon.CopyFromParameterExists();
                bool resumeFromParameterExists = WizardStepContainerCommon.ResumeFromParameterExists();
                var scholarshipToEdit = GetCurrentScholarship();

                BypassPromptIds.AddRange(
                    new[]	{
								previousButton.ID,
								nextButton.ID,                            								
								saveButton.ID,                                
								exitButton.ID,  
								SaveNoConfirmButton.ID,
								buildScholarshipWizardTabIntegrator.ID,
								"intermediaryDDL",                                
								"providerDDL"							
					});

                ActiveStepIndex = WizardStepName.GeneralInformation.GetNumericValue();
                if (copyFromParameterExists)
                {
                    BeginCopyFrom();
                }
                else if (scholarshipToEdit != null)
                {
                    if (!scholarshipToEdit.IsBelongToOrganization(UserContext.CurrentOrganization))
                        throw new InvalidOperationException("Scholarship do not belong to provider in context");

                    if (!scholarshipToEdit.CanEdit())
                    {
                        Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + scholarshipToEdit.Id);
                    }
                    deleteConfirmBtn.Visible = scholarshipToEdit.CanEdit();

                    if (resumeFromParameterExists)
                    {
                        ResumeFromStep();
                    }
                    else
                    {
                        ResumeWizard();
                    }
                }
            }
        }

		protected override void OnPreRender(EventArgs e)
		{
			//because its here, do not need to call this in save and load
			ScholarshipTitleStripeControl.UpdateView(GetDomainObject());
			SwitchEditOrAddInterface();

			if (CurrentScholarshipId > 0)
				PrintView1.CustomUrl = "~/Provider/Scholarships/Show.aspx?id=" + CurrentScholarshipId + "&print=true";
			base.OnPreRender(e);
		}

		/// <summary>
		/// Save the current tab before moving to the next
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void buildScholarshipWizardTabIntegrator_TabChanging(object sender, EventArgs e)
		{
			Save();
		}

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void ExitButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
            {
                Response.Redirect("../Scholarships");
            }
        }

        protected void BuildScholarshipWizard_ActiveStepChanged(object sender, EventArgs e)
        {
			WizardStepContainerCommon.NotifyStepActivated(this);
        }

        protected void PreviousButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (Save())
            {
                GoPrior();
            }
        }

        protected void deleteConfirmBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (e.DialogResult)
            {
                scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                ScholarshipService.Delete(scholarshipInContext);
                SuccessMessageLabel.SetMessage("Scholarship was deleted.");
                Response.Redirect("~/Provider/Scholarships/Default.aspx");
            }
        }

        protected void NextButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (Save())
            {
                GoNext();
            }
        }

        protected void SaveNoConfirmButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            Save();
        }

        #endregion

        #region IWizardStepsContainer<Scholarship> Members
        public void ResumeWizard()
        {
            WizardStepContainerCommon.ResumeWizard(this);
        }

        private void ResumeFromStep()
        {
            WizardStepContainerCommon.ResumeFromStep(this);
        }

        public IWizardStepControl<Scholarship>[] Steps
        {
            get
            {
                var stepControls = BuildScholarshipWizard.Views.FindWizardStepControls<Scholarship>();
                return stepControls;
            }
        }

        Scholarship scholarshipInContext;
        public Scholarship GetDomainObject()
        {
            if (scholarshipInContext == null)
            {
                scholarshipInContext = GetCurrentScholarship() ?? new Scholarship
                                                                      {
                                                                          ApplicationStartDate = DateTime.Today,
                                                                          ApplicationDueDate = DateTime.Today,
                                                                          AwardDate = DateTime.Today
                                                                      } ;
            }
            return scholarshipInContext;
        }

        public void GoPrior()
        {
            WizardStepContainerCommon.GoPrior(this);
        }

        public void GoNext()
        {
            WizardStepContainerCommon.GoNext(this);
        }

        public void Goto(int index)
        {
            if (Save())
                WizardStepContainerCommon.Goto(this, index);
        }

        public int ActiveStepIndex
        {
            get { return BuildScholarshipWizard.ActiveViewIndex; }
            set { BuildScholarshipWizard.ActiveViewIndex = value; }
        }

        #endregion

		#region Private Methods
		private bool CanEdit()
        {
            var scholarship = GetCurrentScholarship();
            return null == scholarship || scholarship.CanEdit();
        }

        private int CurrentScholarshipId
        {
            get
            {
                int scholarshipId = this.GetIntegerPageParameterValue(ViewState, SCHOLARSHIP_ID, -1);
                return scholarshipId;
            }
            set
            {
                ViewState[SCHOLARSHIP_ID] = value;
            }
        }


        private Scholarship GetCurrentScholarship()
        {
            return IsCurrentScholarshipSaved ? ScholarshipService.GetById(CurrentScholarshipId) : null;
        }

        private bool IsCurrentScholarshipSaved
        {
            get { return CurrentScholarshipId > 0; }
        }

        private bool ValidateStep()
        {
            return WizardStepContainerCommon.ValidateStep(this);
        }

        private bool Save()
        {
			if (!Page.IsPostBack)
				return true;

            if (Page.IsValid && ValidateStep() && CanEdit())
            {
                WizardStepContainerCommon.SaveActiveStep(this);
                CurrentScholarshipId = GetDomainObject().Id;
                Dirty = false;

                return true;
            }

            return false;
        }

        private void BeginCopyFrom()
        {
            var copyFromId = WizardStepContainerCommon.GetCopyFromId();
            var copyFrom = ScholarshipService.GetById(copyFromId);

			scholarshipInContext = ScholarshipService.CopyScholarship(copyFrom);
            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(scholarshipInContext);
        	CurrentScholarshipId = scholarshipInContext.Id;
        }

        private void SwitchEditOrAddInterface()
        {
            if (IsCurrentScholarshipSaved)
            {
                buildScholarshipWizardTabIntegrator.DisabledTabIndices = null;
                deleteConfirmBtn.Visible = true;
            }
            else
            {
                buildScholarshipWizardTabIntegrator.DisabledTabIndices = new[] {1, 2, 3, 4, 5};
                deleteConfirmBtn.Visible = false;
            }
		}
		#endregion
	}
}
