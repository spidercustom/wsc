﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Provider.Org
{
    public partial class Edit : Page
    {
        public IProviderService ProviderService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            editOrg.Organization = UserContext.CurrentProvider;
        }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!User.IsInRole(Role.PROVIDER_ADMIN_ROLE))
				Response.Redirect("~/default.aspx");
		}
		protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.Update((Domain.Provider)org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Provider/");
        }

        protected void editOrg_OnFormCanceled()
        {
            Response.Redirect("~/Provider/");
        }
    }
}
