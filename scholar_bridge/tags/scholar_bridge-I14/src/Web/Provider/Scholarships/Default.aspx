<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Default" Title="Provider | Scholarships" %>

<%@ Register TagPrefix="sb" TagName="ScholarshipList" Src="~/Common/ScholarshipList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <fieldset id="ScholarshipFilterContainer" class="list-filter">
        <legend>View options</legend>
    
        <label class="label">View Scholarship By Status:</label>
        <asp:CheckBoxList ID="ScholarshipStatusCheckboxes" runat="server"
            RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="False" />
        <br />        
        
        <label class="label">Select Organization:</label>
        <asp:DropDownList ID="cboOrganization" runat="server" />
        <br />
        
        <asp:Button ID="UpdateViewBtn" runat="server" Text="Update View" onclick="UpdateViewBtn_Click" />
    </fieldset>

    <sb:ScholarshipList id="scholarshipsList" runat="server" 
        ViewActionLink="~/Provider/Scholarships/Show.aspx"
        EditActionLink="~/Provider/BuildScholarship/Default.aspx"  />
</asp:Content>
