﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            var orgs =
                (from i in RelationshipService.GetByProvider(provider)
                 orderby i.Intermediary.Name
                 select new {i.Intermediary.Id, i.Intermediary.Name}).ToList();

            orgs.Insert(0, new {Id = 0, Name = "- ALL -"});

            cboOrganization.DataValueField = "Id";
            cboOrganization.DataTextField = "Name";
            cboOrganization.DataSource = orgs;
            cboOrganization.DataBind();
            ScholarshipStatusCheckboxes.EnumBind(
                ScholarshipStages.NotActivated,
                ScholarshipStages.Activated,
                ScholarshipStages.Awarded);
            ScholarshipStatusCheckboxes.Items.SelectAll();

            FillScholarships();

        }

        private void FillScholarships()
        {
            var provider = UserContext.CurrentProvider;

            if (cboOrganization.SelectedValue == "0")
            {
                scholarshipsList.Scholarships = ScholarshipService.GetByProvider(provider, GetSelectedStages());
            }
            else
            {
                var intermediary = IntermediaryService.FindById(int.Parse(cboOrganization.SelectedValue));
                scholarshipsList.Scholarships = ScholarshipService.GetByOrganizations(provider, intermediary, GetSelectedStages());
            }
        }

        private ScholarshipStages[] GetSelectedStages()
        {
            return ScholarshipStatusCheckboxes.Items.SelectedItems(o => (ScholarshipStages) Convert.ToInt32(o)).ToArray();
        }

        protected void UpdateViewBtn_Click(object sender, EventArgs e)
        {
            FillScholarships();
        }

         
            
         
    }
}