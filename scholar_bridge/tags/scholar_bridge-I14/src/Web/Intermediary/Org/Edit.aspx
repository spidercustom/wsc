﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Org.Edit" Title="Organization | Edit" %>

<%@ Register TagPrefix="sb" TagName="EditOrganization" Src="~/Common/EditOrganization.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:EditOrganization ID="editOrg" runat="server" OnOrganizationSaved="editOrg_OnOrganizationSaved" OnFormCanceled="editOrg_OnFormCanceled" />
</asp:Content>
