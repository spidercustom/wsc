﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Intermediary.Users
{
    public partial class ChangeName : Page
    {
        public IUserContext UserContext { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        public int UserId { get { return Int32.Parse(Request["id"]); } }
        public User CurrentUser { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            CurrentUser = IntermediaryService.FindUserInOrg(UserContext.CurrentIntermediary, UserId);
            editUserName.CurrentUser = CurrentUser;
        }

        protected void editUserName_OnUserSaved(User user)
        {
            SuccessMessageLabel.SetMessage("User's name has been changed.");
            Response.Redirect("~/Intermediary/Users/");
        }

        protected void editUserName_OnFormCanceled()
        {
            Response.Redirect("~/Intermediary/Users/");
        }
    }
}
