﻿using System;
using System.Text.RegularExpressions;
using System.Web.Security;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;
using Spring.Context.Support;
using System.Collections.Specialized;
using System.Configuration;

using System.Web.Configuration;
using System.Web.Hosting;
using System.Configuration.Provider;

using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;


namespace ScholarBridge.Web.Security
{
    public class SpiderMembershipProvider : MembershipProvider
    {
        private const string APPLICATION_NAME = "/";
        private const string PROVIDER_NAME = "SpiderMembershipProvider";

        private string applicationName;
        private bool enablePasswordReset;
        private bool enablePasswordRetrieval;
        private MachineKeySection machineKey;
        private int maxInvalidPasswordAttempts;
        private int minRequiredNonAlphanumericCharacters;
        private int minRequiredPasswordLength;
        private int passwordAttemptWindow;
        private MembershipPasswordFormat passwordFormat;
        private string passwordStrengthRegularExpression;
        private bool requiresQuestionAndAnswer;
        private bool requiresUniqueEmail;

        public SpiderMembershipProvider()
        {
            applicationName = APPLICATION_NAME;
        }

        protected virtual IUserService UserService
        {
            get { return (IUserService)ContextRegistry.GetContext().GetObject("UserService"); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return enablePasswordRetrieval; }
        }

        public override bool EnablePasswordReset
        {
            get { return enablePasswordReset; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return requiresQuestionAndAnswer; }
        }

        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return maxInvalidPasswordAttempts; }
        }

        public override int PasswordAttemptWindow
        {
            get { return passwordAttemptWindow; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return requiresUniqueEmail; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return passwordFormat; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return minRequiredPasswordLength; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return minRequiredNonAlphanumericCharacters; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return passwordStrengthRegularExpression; }
        }
        
        public override void Initialize(string name, NameValueCollection config)
        {
            // Initialize values from Web.config.
            if (null == config)
            {
                throw (new ArgumentNullException("config"));
            }
            if (string.IsNullOrEmpty(name))
            {
                name = "SpiderMembershipProvider";
            }
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Spider Membership Provider");
            }
            // Call the base class implementation.
            base.Initialize(name, config);
            

            // Load configuration data.
            applicationName = GetConfigValue(config["applicationName"], HostingEnvironment.ApplicationVirtualPath);
            requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "False"));
            requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "True"));
            enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "True"));
            enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "True"));
            maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredAlphaNumericCharacters"], "1"));
            passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], string.Empty));

            // Initialize the password format.
            switch (GetConfigValue(config["passwordFormat"], "Hashed"))
            {
                case "Hashed":
                    passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("password format not supported");
            }

            // FindById encryption and decryption key information from the configuration.
            Configuration cfg = WebConfigurationManager.OpenWebConfiguration(HostingEnvironment.ApplicationVirtualPath);
            machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");
            if ("Auto".Equals(machineKey.Decryption))
            {
                // Create our own key if one has not been specified.
                machineKey.DecryptionKey = KeyCreator.CreateKey(24);
                machineKey.ValidationKey = KeyCreator.CreateKey(64);
            }
        }

        internal static string GetConfigValue(string configValue, string defaultValue)
        {
            return (String.IsNullOrEmpty(configValue) ? defaultValue : configValue);
        }

		/// <summary>
		/// keep the status of the user creation on the instance.
		/// </summary>
		public MembershipCreateStatus CreateUserStatus
		{
			get; set;
		}
		/// <summary>
		/// Separate the user creation from the database storage to help with transaction mgmt.
		/// This returns a new unsaved valid user.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <param name="email"></param>
		/// <param name="passwordQuestion"></param>
		/// <param name="passwordAnswer"></param>
		/// <param name="isApproved"></param>
		/// <returns></returns>
		public User CreateUserInstance(string username, string password, string email,
                                                  string passwordQuestion,
                                                  string passwordAnswer, bool isApproved)

		{
			// Raise the ValidatingPassword event in case an event handler has been defined.
			var args = new ValidatePasswordEventArgs(username, password, true);
			OnValidatingPassword(args);
			if (args.Cancel)
			{
				CreateUserStatus = MembershipCreateStatus.InvalidPassword;
				return null;
			}

			// Validate the e-mail address has not already been specified, if required.
			if (RequiresUniqueEmail && !string.IsNullOrEmpty(GetUserNameByEmail(email)))
			{
				CreateUserStatus = MembershipCreateStatus.DuplicateEmail;
				return null;
			}

			// Validate the username has not already been created. GetUserNameByUserName - this is not overriden method of Membership Provider
			if (!string.IsNullOrEmpty(GetUserNameByUserName(username)))
			{
				CreateUserStatus = MembershipCreateStatus.DuplicateUserName;
				return null;
			}

			var user = new User
			{
				Username = username,
				Password = EncodePassword(password, machineKey.ValidationKey),
				PasswordFormat = (int)PasswordFormat,
				PasswordSalt = machineKey.ValidationKey,
				Email = email,
				Question = passwordQuestion,
				Answer = passwordAnswer,
				IsApproved = isApproved,
				CreationDate = DateTime.UtcNow
			};
			return user;
		}

        public override MembershipUser CreateUser(string username, string password, string email,
                                                  string passwordQuestion,
                                                  string passwordAnswer, bool isApproved, object providerUserKey,
                                                  out MembershipCreateStatus status)
        {
        	User user = CreateUserInstance(username, password, email,
        	                   passwordQuestion,
        	                   passwordAnswer, isApproved);

			if (user == null)
			{
				status = CreateUserStatus;
				return null;
			}

            UserService.Insert(user);
        	status = CreateUserStatus = MembershipCreateStatus.Success;
            var result = new MembershipUser(PROVIDER_NAME, username, providerUserKey, email,
                                            passwordQuestion, "", isApproved, false, DateTime.UtcNow, DateTime.UtcNow,
                                            DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow);
            return result;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
                                                             string newPasswordQuestion,
                                                             string newPasswordAnswer)
        {
            // Assume we are unable to perform the operation.
            var result = false;

            // Ensure we are dealing with a valid user.
            if (ValidateUser(username, password))
            {
                // FindById the user from the data store.
                var user = UserService.FindByUsername(username);
                if (null != user)
                {
                    try
                    {
                        // Update the new password question and answer.
                        user.Question = newPasswordQuestion;
                        user.Answer = EncodePassword(newPasswordAnswer, user.PasswordSalt);
                        user.LastActivityDate = DateTime.UtcNow;
                        // Update user record with the new password.
                        UserService.Update(user);
                        // Indicate a successful operation.
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        throw new MembershipPasswordException("Unable to change question and answer.", ex);
                    }
                }
            }

            // Return the result of the operation.
            return result;
        }

        public override string GetPassword(string username, string answer)
        {
            var user = UserService.FindByUsername(username);
            if (RequiresQuestionAndAnswer)
            {
                return user.Answer.Equals(answer) ? user.Password : null;
            }
            
            return user.Password;
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            // Assume we are unable to perform the operation.
            var result = false;
            // Ensure we are dealing with a valid user.
            if (! ValidateUser(username, oldPassword))
            {
                return false;
            }

            if (newPassword.Length < MinRequiredPasswordLength)
            {
                throw new ArgumentException(
                    String.Format("Password is too short, it must be {0} characters or longer.",
                                  MinRequiredPasswordLength), "newPassword");
            }


            int nonAlphaNumericCount = 0;
            for (int i = 0; i < newPassword.Length; i++)
            {
                if (!char.IsLetterOrDigit(newPassword, i))
                {
                    nonAlphaNumericCount++;
                }
            }
            if (nonAlphaNumericCount < MinRequiredNonAlphanumericCharacters)
            {
                throw new ArgumentException(String.Format("New password must have at least {0} non alphanumeric characters.", MinRequiredNonAlphanumericCharacters), "newPassword");
            }
            if ((PasswordStrengthRegularExpression.Length > 0) && !Regex.IsMatch(newPassword, PasswordStrengthRegularExpression))
            {
                throw new ArgumentException("Password does not meet minimum strength requirements", "newPassword");
            }


            // Raise the ValidatingPassword event in case an event handler has been defined.
            ValidatePassword(username, newPassword);

            // FindById the user from the data store.
            var user = UserService.FindByUsername(username);
            if (null != user)
            {
                InternalChangePassword(user, newPassword);
                result = true;
            }

            // Return the result of the operation.
            return result;
        }

        public override string ResetPassword(string username, string answer)
        {
            // Prepare a placeholder for the new passowrd.

            // Ensure password retrievals are allowed.
            if (!EnablePasswordReset)
            {
                throw new MembershipPasswordException("Password reset not enabled.");
            }

            // Determine if a valid answer has been given if question and answer is required.
            if ((null == answer) && RequiresQuestionAndAnswer)
            {
                UpdateFailureCount(username, FailureType.PasswordAnswer);
                throw new MembershipPasswordException("Unable to reset password.");
            }

            // Generate a new random password of the specified length.
            var newPassword = Membership.GeneratePassword(MinRequiredPasswordLength, MinRequiredNonAlphanumericCharacters);

            // Raise the ValidatingPassword event in case an event handler has been defined.
            ValidatePassword(username, newPassword);

            // FindById the user from the data store.
            var user = UserService.FindByUsername(username);
            if (null != user)
            {
                // Determine if the user is locked out of the system.
                if (user.IsLockedOut)
                {
                    throw new MembershipPasswordException("Unable to reset password, user is locked.");
                }

                // Determine if the user is required to answer a password question.
                if (RequiresQuestionAndAnswer && !CheckPassword(answer, user.Answer, user.PasswordSalt))
                {
                    UpdateFailureCount(username, FailureType.PasswordAnswer);
                    throw new MembershipPasswordException("Unable to reset password, password incorrect.");
                }

                // Update user record with the new password.
                InternalChangePassword(user, newPassword);
            }

            // Return the resulting new password.
            return newPassword;
        }

        private void ValidatePassword(string username, string newPassword)
        {
            var args = new ValidatePasswordEventArgs(username, newPassword, true);
            OnValidatingPassword(args);
            if (args.Cancel)
            {
                // Check for a specific error message.
                if (null != args.FailureInformation)
                {
                    throw (args.FailureInformation);
                }
                throw new MembershipPasswordException("Unable to reset password.");
            }
        }

        private void InternalChangePassword(User user, string newPassword)
        {
            try
            {
                user.Password = EncodePassword(newPassword, user.PasswordSalt);
                user.LastPasswordChangeDate = DateTime.UtcNow;
                user.LastActivityDate = DateTime.UtcNow;
                UserService.Update(user);
            }
            catch (Exception ex)
            {
                throw new MembershipPasswordException("Unable to reset password, user is locked.", ex);
            }
        }

        public override void UpdateUser(MembershipUser user)
        {
            // Perform the update in the data store.
            try
            {
                var Objuser = UserService.FindByUsername(user.UserName);
                UserService.Update(FromMembershipUser(Objuser, user));
            }
            catch (Exception ex)
            {
                throw new ProviderException("Unable to update user.", ex);
            }
        }

        public override bool ValidateUser(string username, string password)
        {
            var user = UserService.FindByUsername(username);
            if (null == user)
                return false;

            if (CheckPassword(user, password) && UserCanLogin(user))
            {
                user.LastLoginDate = DateTime.UtcNow;
                UserService.Update(user);
                return true;
            }

            return false;
        }

        private static bool UserCanLogin(User user)
        {
            return user.IsActive && user.IsApproved && !user.IsLockedOut && !user.IsDeleted;
        }

        private bool CheckPassword(User user, string passwordToCheck)
        {
            return EncodePassword(passwordToCheck, user.PasswordSalt) == user.Password;
        }

        public override bool UnlockUser(string userName)
        {
            // Assume we are unable to perform the operation.
            var result = false;

            // Unlock the user in the data store.
            try
            {
                // FindById the user record form the data store.
                var user = UserService.FindByUsername(userName);
                if (null != user)
                {
                    // Perform the update in the data store.
                    user.IsLockedOut = false;
                    user.LastLockedOutDate = DateTime.UtcNow;
                    user.LastActivityDate = DateTime.UtcNow;
                    UserService.Update(user);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw new ProviderException("Unable to unlock user.", ex);
            }

            // Return the result of the operation.
            return result;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            // Assume we were unable to find the user.
            MembershipUser membershipUser = null;

            // Don't accept empty user names.
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            // FindById the user record from the data store.
            try
            {
                var user = UserService.FindByUsername(username);
                if (null != user)
                {
                    membershipUser = ToMembershipUser(PROVIDER_NAME, user);
                }
            }
            catch (Exception ex)
            {
                throw new ProviderException("Unable to get user.", ex);
            }

            // Determine if we need to update the activity information.
            if (userIsOnline && (membershipUser != null))
            {
                // Update the last activity timestamp (LastActivityDate).
                UpdateLastActivityDate(membershipUser.UserName);
            }

            // Return the resulting user.
            return membershipUser;
        }

        private void UpdateLastActivityDate(string username)
        {
            // FindById user record associated to the given user name.
            var user = UserService.FindByUsername(username);
            if (null == user) { return; }

            // Update the activity timestamp (LastActivityDate).
            try
            {
                // Perform the update in the data store.
                user.LastActivityDate = DateTime.UtcNow;
                UserService.Update(user);
            }
            catch (Exception ex)
            {
                throw new ProviderException("Unable to update last activity date.", ex);
            }
        }

        public override string GetUserNameByEmail(string email)
        {
            var user = UserService.FindByEmail(email);
            return user == null ? null : user.Username;
        }

        public string GetUserNameByUserName(string userName)
        {
            var user = UserService.FindByUsername(userName);
            return user == null ? null : user.Username;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            // Assume we are unable to perform the operation.
            var result = false;

            // Delete the corresponding user record from the data store.
            try
            {
                // FindById the user information.
                var user = UserService.FindByUsername(username);
                if (null != user)
                {
                    // Delete the user record.
                    UserService.Delete(user);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw new ProviderException("Unable to delete user.", ex);
            }

            // Return the result of the operation.
            return result;
        }

        public MembershipUserCollection GetAllUsers(string sortProperty)
        {
            // Create a placeholder for all user accounts retrived, if any.
            var users = new MembershipUserCollection();

            // FindById the user record from the data store.
            try
            {
                var foundUsers = UserService.FindAll(sortProperty);
                foreach (var user in foundUsers)
                {
                    users.Add(ToMembershipUser(Name,user));
                }
            }
            catch (Exception ex)
            {
                throw new ProviderException("Unable to get all users.", ex);
            }
           
            // Return the result of the operation.
            return users;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            // Create a placeholder for all user accounts retrived, if any.
            var users = new MembershipUserCollection();
            
            try
            {
                var foundUsers = UserService.FindAll(pageIndex, pageSize);
                foreach (User user in foundUsers)
                {
                    users.Add(ToMembershipUser(Name,user));
                }
            }
            catch (Exception ex)
            {
                throw new ProviderException("Unable to get all users.", ex);
            }
           
            // Prepare return parameters.
            totalRecords = users.Count;

            // Return the result of the operation.
            return users;
        }

        public override int GetNumberOfUsersOnline()
        {
            return UserService.FindCountOfUsersOnline();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
                                                                 out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize,
                                                                  out int totalRecords)
        {
            var users = new MembershipUserCollection();
            try
            {
                IList<User> foundUsers = UserService.FindByEmail(emailToMatch, pageIndex, pageSize);
                foreach (var user in foundUsers)
                {
                    users.Add(ToMembershipUser(Name,user));
                }
            }
           
            catch (Exception ex)
            {
                throw new ProviderException("Unable to get user.", ex);
            }
           

            // Prepare return parameters.
            totalRecords = users.Count;

            // Return the result of the operation.
            return users;
        }

        private string EncodePassword(string password, string validationKey)
        {
            // Assume no encoding is performed.
            string encodedPassword = password;

            // Only perform the encoding if all parameters are passed and valid.
            if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(validationKey))
            {
                // Determine the type of encoding required.
                switch (PasswordFormat)
                {
                    case MembershipPasswordFormat.Clear:
                        // Nothing to do.
                        break;
                    case MembershipPasswordFormat.Encrypted:
                        encodedPassword = Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                        break;
                    case MembershipPasswordFormat.Hashed:
                        // If we are not password a validation key, use the default specified.
                        if (string.IsNullOrEmpty(validationKey))
                        {
                            // The machine key will either come from the Web.config file or it will be automatically generate
                            // during initialization.
                            validationKey = machineKey.ValidationKey;
                        }
                        var hash = new HMACSHA1 {Key = HexToByte(validationKey)};
                        encodedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                        break;
                    default:
                        throw new ProviderException("Unsupported password format.");
                }
            }

            // Return the encoded password.
            return encodedPassword;
        }

        /// <summary>
        /// Decrypts or leaves the password clear based on the <see cref="PasswordFormat"/> property value.
        /// </summary>
        /// <param name="password">password to unencode.</param>
        /// <returns>Unencoded password.</returns>
        private string UnencodePassword(string password)
        {
            // Determine the type of unencoding required.
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    return password;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Can not unencode hashed password.");
            }
            byte[] encodedPassword = Convert.FromBase64String(password);
            byte[] bytes = DecryptPassword(encodedPassword);
            if (null == bytes)
                return null;

            return Encoding.Unicode.GetString(bytes);
        }

        private static byte[] HexToByte(string hexString)
        {
            var bytes = new byte[hexString.Length / 2 + 1];
            for (var i = 0; i <= hexString.Length / 2 - 1; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }
            return bytes;
        }

        /// <summary>
        /// Types of failure that need to be tracked on a per user basis.
        /// </summary>
        private enum FailureType
        {
            Password,
            PasswordAnswer
        }

        private bool CheckPassword(string plain, string possiblyEncoded, string validationKey)
        {
            // Format the password as required for comparison.
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Hashed:
                    plain = EncodePassword(plain, validationKey);
                    break;
                case MembershipPasswordFormat.Encrypted:
                    possiblyEncoded = UnencodePassword(possiblyEncoded);
                    break;
            }

            // Return the result of the comparison.
            return (plain == possiblyEncoded);
        }

        private static MembershipUser ToMembershipUser(string providerName, User user)
        {
            return (new MembershipUser(providerName,
                                       user.Username, user.Username, user.Email, user.Question, user.Comments,
                                       user.IsApproved, user.IsLockedOut,
                                       user.CreationDate.GetValueOrDefault(DateTime.UtcNow), user.LastLoginDate.GetValueOrDefault(DateTime.UtcNow),
                                       user.LastActivityDate.GetValueOrDefault(DateTime.UtcNow), user.LastPasswordChangeDate.GetValueOrDefault(DateTime.UtcNow),
                                       user.LastLockedOutDate.GetValueOrDefault(DateTime.UtcNow)));
        }

        private static User FromMembershipUser(User user, MembershipUser membershipUser)
        {
            user.Id = Convert.ToInt32(membershipUser.ProviderUserKey);
            user.Username = membershipUser.UserName;
            user.Email = membershipUser.Email;
            user.Question = membershipUser.PasswordQuestion;
            user.Comments = membershipUser.Comment;
            user.IsApproved = membershipUser.IsApproved;
            user.IsLockedOut = membershipUser.IsLockedOut;
            user.CreationDate = membershipUser.CreationDate;
            user.LastActivityDate = membershipUser.LastActivityDate;
            user.LastLoginDate = membershipUser.LastLoginDate;
            user.LastPasswordChangeDate = membershipUser.LastPasswordChangedDate;
            user.LastLockedOutDate = membershipUser.LastLockoutDate;

            return user;
        }

        private void UpdateFailureCount(string username, FailureType failureType)
        {
            // FindById user record associated to the given user name.
            var user = UserService.FindByUsername(username);
            if (null != user)
            {
                // Update the failure information for the given user in the data store.
                DateTime windowStart = DateTime.UtcNow;
                int failureCount = 0;
                try
                {
                    // First determine the type of update we need to do and get the relevant details.
                    switch (failureType)
                    {
                        case FailureType.Password:
                            if (user.FailedPasswordAttemptWindowStart.HasValue)
                                windowStart = user.FailedPasswordAttemptWindowStart.Value;
                            failureCount = user.FailedPasswordAttemptCount;
                            break;
                        case FailureType.PasswordAnswer:
                            if (user.FailedPasswordAnswerAttemptWindowStart.HasValue)
                                windowStart = user.FailedPasswordAnswerAttemptWindowStart.Value;
                            failureCount = user.FailedPasswordAnswerAttemptCount;
                            break;
                    }

                    // Then determine if the threashold has been exeeded.
                    DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);
                    if ((0 == failureCount) || DateTime.UtcNow > windowEnd)
                    {
                        // First password failure or outside of window, start new password failure count from 1
                        // and a new window start.
                        switch (failureType)
                        {
                            case FailureType.Password:
                                user.FailedPasswordAttemptWindowStart = DateTime.UtcNow;
                                user.FailedPasswordAttemptCount = 1;
                                break;
                            case FailureType.PasswordAnswer:
                                user.FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow;
                                user.FailedPasswordAnswerAttemptCount = 1;
                                break;
                        }
                    }
                    else
                    {
                        // Track failures.
                        failureCount++;
                        if (failureCount >= MaxInvalidPasswordAttempts)
                        {
                            // Password attempts have exceeded the failure threshold. Lock out the user.
                            user.IsLockedOut = true;
                            user.LastLockedOutDate = DateTime.UtcNow;
                        }
                        else
                        {
                            switch (failureType)
                            {
                                case FailureType.Password:
                                    user.FailedPasswordAttemptCount = failureCount;
                                    break;
                                case FailureType.PasswordAnswer:
                                    user.FailedPasswordAnswerAttemptCount = failureCount;
                                    break;
                            }
                        }
                    }

                    // Persist the changes.
                    UserService.Update(user);
                }
                catch (Exception ex)
                {
                    throw new ProviderException("Unable to update failure count.", ex);
                }
            }
        }
    }
}
