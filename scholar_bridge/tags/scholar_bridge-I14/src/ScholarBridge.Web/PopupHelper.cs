﻿using System.Web;
using ScholarBridge.Web.Extensions;
using System.Web.UI;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web
{
    /// <summary>
    /// contains helper functions for pop up window functionality
    /// </summary>
    public static class PopupHelper
    {
        private const string CLOSE_SELF_SCRIPT_TEMPLATE = "ClosePopupWindow('{0}');";
        public static void CloseSelf(bool refreshParent )
        {
            var scriptname = "closeself_refresh_parent";
            var script = CLOSE_SELF_SCRIPT_TEMPLATE.Build(refreshParent);

            var page = HttpContext.Current.CurrentHandler as Page;
  
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
           
        }
       
    }
}