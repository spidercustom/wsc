﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using Spring.Context.Support;

namespace ScholarBridge.Web.MatchList
{
    public class IconHelper
    {
        //todo:why isn't this getting injected by spring!?
        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }
        //todo:why isn't this getting injected by spring!?
        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        private Action postIconClick;
        public Action PostIconClick
        {
            get { return postIconClick; }
            set { postIconClick = value; }
        }

        public void IconConfigurator(Match match, ImageButton icon)
        {
            switch (match.MatchStatus)
            {
                case MatchStatus.New:
                    icon.ImageUrl = "~/images/matched_scholarship.png";
                    icon.ToolTip = "Click to add into My Scholarships";
                    icon.Click += SaveMatchAction;
                    break;
                case MatchStatus.Saved:
                    icon.ImageUrl = "~/images/saved_scholarship.png";
                    icon.ToolTip = "Click to remove from My Scholarships";
                    icon.Click += UnsaveMatchAction;
                    break;
                case MatchStatus.Applied:
                    icon.ImageUrl = "~/images/applied-scholarship.png";
                    icon.ToolTip = "Application is created for the scholarship. Scholarship cannot be removed from My Scholarship";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SaveMatchAction(object sender, ImageClickEventArgs e)
        {
            var scholarshipId = ExtractId((ImageButton)sender);

            MatchService.SaveMatch(UserContext.CurrentSeeker, scholarshipId);
            if (null != PostIconClick)
                postIconClick();
        }

        private void UnsaveMatchAction(object sender, ImageClickEventArgs e)
        {
            var scholarshipId = ExtractId((ImageButton)sender);
            
            MatchService.UnSaveMatch(UserContext.CurrentSeeker, scholarshipId);
            if (null != PostIconClick)
                postIconClick();
        }

        public static int ExtractId(ImageButton imageButton)
        {
            var id = 0;
            if (!string.IsNullOrEmpty(imageButton.CommandArgument))
                id = Int32.Parse(imageButton.CommandArgument);
            return id;
        }
    }
}