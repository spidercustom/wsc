﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using Spring.Context.Support;

namespace ScholarBridge.Web.MatchList
{
    public class ActionHelper
    {
        //todo:why isn't this getting injected by spring!?
        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }

        //todo:why isn't this getting injected by spring!?
        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        public void ActionConfigurator(Match match, Button btn)
        {
            var matchAction = GetDefaultAction(match);
            btn.Visible = !(matchAction is BlankAction);
            btn.CommandArgument = match.Scholarship.Id.ToString();
            btn.Text = matchAction.Text;
            btn.Click += ActionButtonClick;
        }

        private void ActionButtonClick(object sender, EventArgs e)
        {
            var match = GetMatch(sender);
            ExecuteDefaultAction(match);
        }

        private Match GetMatch(object sender)
        {
            var scholarshipId = ExtractId((Button)sender);
            return MatchService.GetMatch(UserContext.CurrentSeeker, scholarshipId);
        }

        public static int ExtractId(Button button)
        {
            var id = 0;
            if (!string.IsNullOrEmpty(button.CommandArgument))
                id = Int32.Parse(button.CommandArgument);
            return id;
        }


        private void ExecuteDefaultAction(Match match)
        {
            GetDefaultAction(match).Execute(match);
        }

        private MatchAction GetDefaultAction(Match match)
        {
            switch (match.MatchApplicationStatus)
            {
                case MatchApplicationStatus.Unknown:
                    return BlankAction.Instance;
                case MatchApplicationStatus.NotApplied:
                    return new BuildApplicationAction();
                case MatchApplicationStatus.Appling:
                    return new EditApplicationAction();
                case MatchApplicationStatus.Applied:
                    return new ViewApplicationAction();
                case MatchApplicationStatus.BeingConsidered:
                    return new ViewApplicationAction();
                case MatchApplicationStatus.Closed:
                    return BlankAction.Instance;
                case MatchApplicationStatus.Offerred:
                    return new ViewContactInfoAction();
                case MatchApplicationStatus.Awarded:
                    return new ViewApplicationAction();
                default:
                    throw new NotSupportedException();
            }
        }
    }
}