﻿using System.Collections.Generic;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IUserDAL : IDAL<User>
    {
        User FindByEmail(string email);
        User FindByUsername(string email);
        User FindByUsernameAndPassword(string username, string password);

        IList<User> FindAll(int pageIndex, int pageSize);
        IList<User> FindByEmail(string email, int pageIndex, int pageSize);
        IList<User> FindByRoleName(string roleName);
        IList<User> FindByRoleNameAndUsernameWildcard(string roleName, string usernameToMatch);
        IList<User> FindNonConfirmedUsers(int daysOld);
        int FindCountOfUsersOnline();
    }
}