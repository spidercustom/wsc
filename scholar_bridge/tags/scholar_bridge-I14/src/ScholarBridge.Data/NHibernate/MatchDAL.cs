using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class MatchDAL : AbstractDAL<Match>, IMatchDAL
    {
        private static readonly SimpleExpression NOT_DELETED = Restrictions.Eq("IsDeleted", false);

        public IList<Match> FindAll(Application application)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))
                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus status)
        {
            return FindAll(seeker, new[] {status});
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.In("MatchStatus", status))
                .List<Match>();
        }

        public IList<Match> FindAll(Scholarship scholarship, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.In("MatchStatus", status))
                .List<Match>();
        }

        public IList<Match> FindAllWithApplications(Scholarship scholarship, MatchStatus[] statuses)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.IsNotNull("Application"))
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.In("MatchStatus", statuses))
                .SetFetchMode("Application", FetchMode.Join)
                .List<Match>();
        }

        public Match Find(Seeker seeker, Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .UniqueResult<Match>();
        }

        public Match Find(Seeker seeker, int scholarshipId)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship.Id", scholarshipId))
                .UniqueResult<Match>();
        }

        public void RemoveZeroedMatches(Seeker seeker)
        {
            foreach (var match in CreateCriteria()
                .Add(Restrictions.Eq("SeekerMinimumCriteriaCount", 0))
                .Add(Restrictions.Not(Restrictions.Eq("ScholarshipMinimumCriteriaCount", 0)))
                .Add(Restrictions.Not(Restrictions.Eq("MatchStatus", MatchStatus.Saved)))
                .CreateCriteria("Seeker").Add(Restrictions.Eq("Id", seeker.Id)).List<Match>())
            {
                Delete(match);
            }
        }

        public void UpdateMatches(Seeker seeker)
        {
            var sqlQuery = Session.GetNamedQuery("findMatchesForSeeker");
            sqlQuery.SetEntity("seekerId", seeker);
            var results = sqlQuery.List<Object[]>();

            foreach (var r in results)
            {
                var scholarshipId = (int) r[0];
                var scholarship = (Scholarship)Session.Get(typeof(Scholarship), scholarshipId);

                CreateOrUpdateMatch(seeker, scholarship, r);

                RemoveZeroedMatches(seeker);
            }
        }

        public void UpdateMatches(Scholarship newScholarship)
        {
            var sqlQuery = Session.GetNamedQuery("findMatchesForScholarship");
            sqlQuery.SetEntity("scholarshipId", newScholarship);
            var results = sqlQuery.List<Object[]>();

            foreach (var r in results)
            {
                var scholarshipId = (int)r[0];
                var seekerId = (int)r[1];
                var scholarship = (Scholarship)Session.Get(typeof(Scholarship), scholarshipId);
                var seeker = (Seeker)Session.Get(typeof(Seeker), seekerId);

                CreateOrUpdateMatch(seeker, scholarship, r);
            }
        }

        public void CreateOrUpdateMatch(Seeker seeker, Scholarship scholarship, object[] r)
        {
            var seekerPrefCount = (int)r[2];
            var seekerMinCount = (int)r[3];
            var scholPrefCount = (int)r[4];
            var scholMinCount = (int)r[5];

            var match = Find(seeker, scholarship);
            if (null == match)
            {
                match = new Match
                {
                    Seeker = seeker,
                    Scholarship = scholarship
                };
            }

            if (match.IsDeleted)
                return;

            match.SeekerMinimumCriteriaCount = seekerMinCount;
            match.SeekerPreferredCriteriaCount = seekerPrefCount;
            match.ScholarshipMinimumCriteriaCount = scholMinCount;
            match.ScholarshipPreferredCriteriaCount = scholPrefCount;
            match.LastUpdate = new ActivityStamp(seeker.User);
            Session.SaveOrUpdate(match);
        }
    }
}
