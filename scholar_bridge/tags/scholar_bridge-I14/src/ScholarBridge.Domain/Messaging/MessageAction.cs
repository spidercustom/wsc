namespace ScholarBridge.Domain.Messaging
{
    public enum MessageAction
    {
        None,
        Approve,
        Deny
    }
}