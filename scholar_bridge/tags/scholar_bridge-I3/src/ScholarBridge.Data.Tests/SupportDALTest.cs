using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class SupportDALTest : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public SupportDAL SupportDAL { get; set; }

        // private avoids injection issues for spring tests
        private Support Traditional { get; set; }
        private Support Emergency { get; set; }

        protected override void OnSetUpInTransaction()
        {
            var u = UserDALTest.InsertUser(UserDAL, "test@example.com");
            Traditional = SupportDAL.Insert(new Support()
                                                {
                                                    Name = "Traditional",
                                                    Description = "Desc",
                                                    SupportType = SupportType.Traditional,
                                                    LastUpdate = new ActivityStamp(u)
                                                });

            Emergency = SupportDAL.Insert(new Support()
                                              {
                                                  Name = "Emergency",
                                                  Description = "Desc",
                                                  SupportType = SupportType.Emergency,
                                                  LastUpdate = new ActivityStamp(u)
                                              });
        }


        [Test]
        public void CanGetById()
        {
            var s = SupportDAL.FindById(Traditional.GetIdAsInteger());
            Assert.IsNotNull(s);
            Assert.AreEqual(Traditional.Name, s.Name);
            Assert.AreEqual(Traditional.Description, s.Description);
        }

        [Test]
        public void CanGetByAll()
        {
            var supports = SupportDAL.FindAll();
            Assert.IsNotNull(supports);
            CollectionAssert.IsNotEmpty(supports);
            CollectionAssert.AllItemsAreNotNull(supports);
        }

        [Test]
        public void CanGetAllById()
        {
            var supports = SupportDAL.FindAll();
            var ids = new List<int>
                          {
                              supports[0].GetIdAsInteger(),
                              supports[1].GetIdAsInteger(),
                          };

            var found = SupportDAL.FindAll(ids);

            Assert.IsNotNull(found);
            Assert.AreEqual(2, found.Count);
            CollectionAssert.IsNotEmpty(found);
            CollectionAssert.AllItemsAreNotNull(found);
        }

        [Test]
        public void CanGetAllByType()
        {
            var supports = SupportDAL.FindAll(SupportType.Emergency);
            Assert.IsNotNull(supports);
            CollectionAssert.IsNotEmpty(supports);
            CollectionAssert.AllItemsAreNotNull(supports);
            supports.All(s => s.SupportType == SupportType.Emergency);
        }
    }
}