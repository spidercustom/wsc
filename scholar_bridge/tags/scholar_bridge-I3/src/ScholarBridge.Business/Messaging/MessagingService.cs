using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Messaging
{
    public class MessagingService : IMessagingService
    {
        public IMailerService MailerService { get; set; }
        public IMessageService MessageService { get; set; }
        public IRoleDAL RoleService { get; set; }

        public string DefaultFromEmail { get; set; }
        public string AdminEmail { get; set; }

        public void SendMessageToAdmin(Message message, MailTemplateParams templateParams)
        {
            var messageTemplate = MailerService.MailForMessageType(message.MessageTemplate);
            message.Subject = messageTemplate.Subject;
            message.Content = messageTemplate.Body(templateParams.MergeVariables);
            message.To = new MessageAddress {Role = RoleService.FindByName(Role.WSC_ADMIN_ROLE)};

            MessageService.SendMessage(message);
            MailerService.SendMail(messageTemplate, templateParams, templateParams.From ?? DefaultFromEmail, templateParams.To ?? AdminEmail);
        }

        public void SendMessageFromAdmin(Message message, MailTemplateParams templateParams)
        {
            message.From = new MessageAddress { Role = RoleService.FindByName(Role.WSC_ADMIN_ROLE) };
            SendMessage(message, templateParams);
        }

        public void SendMessage(Message message, MailTemplateParams templateParams)
        {
            var messageTemplate = MailerService.MailForMessageType(message.MessageTemplate);
            message.Subject = messageTemplate.Subject;
            message.Content = messageTemplate.Body(templateParams.MergeVariables);

            MessageService.SendMessage(message);
            MailerService.SendMail(messageTemplate, templateParams, templateParams.From ?? DefaultFromEmail, message.To.EmailAddress() ?? AdminEmail);
        }

        public void SendEmail(User toUser, MessageType type, MailTemplateParams templateParams)
        {
            var messageTemplate = MailerService.MailForMessageType(type);
            MailerService.SendMail(messageTemplate, templateParams, templateParams.From ?? DefaultFromEmail, templateParams.To ?? toUser.Email);
        }

        public void DeleteRelatedMessages(Scholarship scholarship)
        {
            MessageService.DeletedRelatedMessages(scholarship);
        }
    }
}