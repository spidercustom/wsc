﻿using System;


namespace ScholarBridge.Business.Exceptions
{
    [global::System.Serializable]
    public class ProviderNotApprovedException : ApplicationException
    {
        public ProviderNotApprovedException()
            : base("Provider is not yet approved")
        { }
        public ProviderNotApprovedException(string message) : base(message) { }
        public ProviderNotApprovedException(string message, Exception inner) : base(message, inner) { }
        protected ProviderNotApprovedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
