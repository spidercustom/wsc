using System.Collections.Generic;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public interface IMessageActionService
    {
        IDictionary<MessageType, IAction> Actions { get; set; }

        void Approve(Message message, MailTemplateParams templateParams, User approver);

        void Reject(Message message, MailTemplateParams templateParams, User approver);

        IAction GetAction(MessageType messageAction);

    }
}