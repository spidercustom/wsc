﻿using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IUserService
    {
        User FindByEmail(string email);

        void Update(User user);

        void Delete(User user);

        void ActivateUser(User user);

        void ActivateProviderUser(User user);

        void ActivateIntermediaryUser(User user);

        void ResetUsername(User user, string newEmail);

        void SendConfirmationEmail(User user, bool requiresResetPassword);

    }
}
