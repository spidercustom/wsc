﻿using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface IRelationshipService
    {
        void Save(Relationship relationship);
        Relationship GetById(int id);
        
        IList<Relationship> GetByProvider(Provider provider);
        IList<Relationship> GetByIntermediary(Intermediary intermediary);

        IList<Organization> GetActiveByOrganization(Organization organization);

        void InActivate(Relationship relationship);
        void CreateRequest(Relationship relationship);
        void Approve(Relationship relationship);
        void Reject(Relationship relationship);
        Relationship GetByOrganizations(Organization OrgA, Organization OrgB);
    }
}
