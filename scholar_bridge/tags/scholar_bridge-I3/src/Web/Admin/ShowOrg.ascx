﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowOrg.ascx.cs" Inherits="ScholarBridge.Web.Admin.ShowOrg" %>
<label for="statusLabel">Status:</label>
<asp:Label ID="statusLabel" runat="server" />
<br />

<label for="statusLabel">Admin User Has Confirmed Email?:</label>
<asp:Label ID="adminConfirmLabel" runat="server" />
<br />

<label for="nameLabel">Name:</label>
<asp:Label ID="nameLabel" runat="server" />
<br />

<label for="taxIdLabel">TaxId (EIN):</label>
<asp:Label ID="taxIdLabel" runat="server" />
<br />

<label for="websiteLabel">Website:</label>
<asp:HyperLink ID="websiteLabel" runat="server"></asp:HyperLink>
<br />

<label for="addressLabel">Address:</label>    
<asp:Label ID="addressLabel" runat="server" />
<br />

<label for="phoneLabel">Phone:</label>  
<asp:Label ID="phoneLabel" runat="server" />
<br />

<label for="faxLabel">Fax:</label>  
<asp:Label ID="faxLabel" runat="server" />
<br />

<label for="otherPhoneLabel">Other Phone:</label>  
<asp:Label ID="otherPhoneLabel" runat="server" />
<br />