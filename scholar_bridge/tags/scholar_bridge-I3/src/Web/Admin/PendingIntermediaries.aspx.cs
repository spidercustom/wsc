﻿using System;
using System.Linq;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin
{
    public partial class PendingIntermediaries : System.Web.UI.Page
    {
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            pendingIntermediaries.Organizations = IntermediaryService.FindPendingOrganizations()
                .Cast<Organization>().ToList();
        }
    }
}