﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ChangeName.aspx.cs" Inherits="ScholarBridge.Web.Profile.ChangeName" Title="Profile | Change Name" %>

<%@ Register TagPrefix="sb" TagName="EditUserName" Src="~/Common/EditUserName.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <sb:EditUserName ID="editUserName" runat="server" 
        OnUserSaved="editUserName_OnUserSaved" 
        OnFormCanceled="editUserName_OnFormCanceled" />
                    
</asp:Content>
