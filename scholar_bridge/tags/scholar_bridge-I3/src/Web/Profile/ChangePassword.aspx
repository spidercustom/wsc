﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="ScholarBridge.Web.Profile.ChangePassword" Title="Profile | Change Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:ChangePassword ID="ChangePassword1" runat="server" 
        SuccessPageUrl="~/Profile/Default.aspx" 
        CancelDestinationPageUrl="~/Profile/Default.aspx" 
        onchangedpassword="ChangePassword1_ChangedPassword">
    </asp:ChangePassword>
</asp:Content>
