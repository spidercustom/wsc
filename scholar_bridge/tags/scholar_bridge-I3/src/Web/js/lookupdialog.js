﻿
$.lDialog = {
 PagerID:'',
 TotalPages: 2,
 CurrentPage:1,
 CurrentFilter:'',
 DataUrl: '',
 ExludeItems: '',
 HiddenXMLID: '',
 SearchButtonID: '',
 SearchBoxID: ''
 
 }

 function GetParams() {
    setExlcudeItems();
    var result = "&exclude=" + $.lDialog.ExludeItems + "&page=" + $.lDialog.CurrentPage + "&rp=11&sorton=name&sortorder=asc&qtype=name&query="+$.lDialog.CurrentFilter;
    return result;
}
function CreatePager() {
   
    $($.lDialog.PagerID).pager({ pagenumber: $.lDialog.CurrentPage, pagecount: $.lDialog.TotalPages, buttonClickCallback: PageClick });
}
function createDialog(dialogdiv, buddycontrolid, hiddenxmlid) {

    $.lDialog.HiddenXMLID = hiddenxmlid;
    var xml = escape($($.lDialog.HiddenXMLID).val());
    $($.lDialog.HiddenXMLID).val(xml)
    
    var xwidth = $(dialogdiv).width();
    var xheight = $(dialogdiv).height();
    $(dialogdiv).dialog({
        autoOpen: false,
        width: xwidth,
        height: xheight,
        modal: true,
        resizable: false,
        buttons: {
            "Ok": function() {

                //$(inputid).val($(outputid).val());
                UpdateBuddyControl(buddycontrolid);
                //CleanUp();
                $(this).dialog("close");
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });
   
}

function showDialog(dialogdiv, div_available, div_selection, div_pager, dataurl, hiddenXMLID, searchbuttonid, searchboxid) {
    //create list of items
    $.lDialog.DataUrl = dataurl;
    $.lDialog.PagerID = div_pager;
    $.lDialog.HiddenXMLID = hiddenXMLID;
    $.lDialog.SearchButtonID= searchbuttonid;
    $.lDialog.SearchBoxID= searchboxid;

    $($.lDialog.SearchButtonID).click(function() {
       
        $.lDialog.CurrentFilter = $($.lDialog.SearchBoxID).val();
        getAjaxData();
        return false;
    });

    $($.lDialog.SearchBoxID).keyup(function(e) {
        if (e.which == 13) {
            $.lDialog.CurrentFilter = $($.lDialog.SearchBoxID).val();
            getAjaxData();
        }
        return false;
    });
    
    
    if ($("#available").length == 0) { $(div_available).append('<ol id="available"></ol>'); }
    if ($("#selections").length == 0) { $(div_selection).append('<ol id="selections"></ol>'); }
    $("#available").dblclick(function() {
        addToSelections('');
    });

    $("#selections").dblclick(function() {
        removeFromSelections('false');
    }); 
    CreateSelectionsItems($(hiddenXMLID).val());
    $("#selections").selectable();
    $("#available").selectable();
        getAjaxData();
    
    $(dialogdiv).dialog('open');
   return false;
}

function CleanUp() {
    $("#selections").remove();
    $("#available").remove();
}
 
 
function UpdateBuddyControl(buddycontrolid) {

    var names = '';
    var ids = '';
    var xml = '<rows>';
    $("#selections li").each(function() {
        var index = $("#selections li").index(this);
        var item = $("#selections li")[index];
        var id = item.value;
        var name = item.textContent;

        if (ids.length == 0)
            ids = id;
        else
            ids = ids + ", " + id;

        if (names.length == 0)
            names = name;
        else
            names = names + ", " + name;

        //create xml element
        xml = xml + GetRowElement(id, name);
        
    });
    xml = xml + '</rows>'
   
    $(document).find(buddycontrolid).val(names);
    xml = escape(xml);
    $($.lDialog.HiddenXMLID).val(xml);
}

PageClick = function(pageclickednumber) {
            $.lDialog.CurrentPage = pageclickednumber;
            getAjaxData();
}

function getAjaxData() {
    var params = $.lDialog.DataUrl + GetParams();
   
    $.ajax({
    type: "GET", //POST
    url: params,
        data: "", // Set Method Params
        beforeSend: function(xhr) {
        xhr.setRequestHeader("Content-type", "application/xml; charset=utf-8");
            xhr.setRequestHeader("Content-length", params.length);
        },
        contentType: "application/xml; charset=utf-8",
        dataType: "xml", // Set return Data Type
        success: function(msg, status) {
            CreateAvailableItems(msg);

        },
        error: function(xhr, msg, e) {
            alert('Error: ' + msg); //Error Callback
        }
    });
}

function CreateAvailableItems(xml) {
    var jdata = $(xml);
    var jrows = jdata.find("row");
    var total = jdata.find("totalpages").text();
    $.lDialog.TotalPages = total;
    $("#available li").remove();
    jrows.each(function() {
        var jrow = $(this);
        
        var name = jrow.children().text();
        $("#available").append("<li class='ui-widget-content' value=" + jrow.attr("id") + ">" + name + "</li>");

    });


    CreatePager();
   
   }

   function CreateSelectionsItems(xml) {
       xml = unescape(xml)
       var jdata = $(xml);
       var jrows = jdata.find("row");
       $("#selections li").remove();
       jrows.each(function() {
           var jrow = $(this);
           var name = jrow.children().text();
           $("#selections").append("<li class='ui-widget-content' value=" + jrow.attr("id") + ">" + name + "</li>");
       });
   }

function addToSelections(hiddenids) {
    var needRefresh = false;
    $("#available .ui-selected").each(function() {
        var index = $("#available li").index(this);
        var item = $("#available li")[index];
        $("#selections").prepend(item);
        $(item).removeClass('ui-selected');
        needRefresh = true;
    });
    $("ol#selections>li").tsort();
    if (needRefresh)
    getAjaxData();
}

function removeFromSelections(withall) {
    var needRefresh = false;
    var selectfrom="#selections .ui-selected";
    if (withall=='True')
        selectfrom ="#selections li";
   
    
    $(selectfrom).each(function() {
        var index = $("#selections li").index(this);
        var item = $("#selections li")[index];
        $(item).remove();
        needRefresh = true;
    });
if (needRefresh)
   getAjaxData();
}

function setExlcudeItems() {
    $.lDialog.ExludeItems='';
    $("#selections li").each(function() {
        var index = $("#selections li").index(this);
        var item = $("#selections li")[index];
        $.lDialog.ExludeItems = $.lDialog.ExludeItems + "," + item.value;
    });
 
}

function GetRowElement(id, name) {
    var xml = '<row id="' + id + '"><cell>' + name + '</cell></row>'
    return xml
}
