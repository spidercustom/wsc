﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Intermediary.Users
{
    public partial class Create : Page
    {
        public IUserContext UserContext { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
        }

        protected void userForm_OnUserSaved(User user)
        {
            user.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var intermediary = UserContext.CurrentIntermediary;

            IntermediaryService.SaveNewUser(intermediary, user, true);

            SuccessMessageLabel.SetMessage("User was created.");
            Response.Redirect("~/Intermediary/Users/");
        }

        protected void userForm_OnFormCanceled()
        {
            Response.Redirect("~/Intermediary/Users/");
        }
    }
}
