﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Users.Show" Title="Intermediary | Users | Show" %>
<%@ Register TagPrefix="sb" TagName="UserDetails" Src="~/Common/UserDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:UserDetails ID="userDetails" runat="server" />
    
    <a href="<%= ResolveUrl(string.Format("~/Intermediary/Users/ChangeName.aspx?id={0}", UserId)) %>">Edit User</a><br />
    <asp:Button ID="deleteBtn" runat="server" Text="Delete User" 
        onclick="deleteBtn_Click" />
    <asp:Button ID="reactivateBtn" runat="server" Text="Reactivate User" 
        onclick="reactivateBtn_Click" />
</asp:Content>

