﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteriaShow.ascx.cs" Inherits="ScholarBridge.Web.Common.AdditionalCriteriaShow" %>

<div id="recordinfo">
<h4>Additional Scholarship Application Criteria</h4>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>

<table class="viewonlyTable">
    <thead><tr><th>Additional Requirements</th></tr></thead>
    <tbody>
        <asp:Repeater ID="AdditionalRequirementsRepeater" runat="server">
            <ItemTemplate>
                <tr>
                    <td><asp:label runat="server" ID="Label1" Text='<%# Bind("Name")%>'></asp:label></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        
    </tbody>
    <tfoot>
        <tr runat="server" id="RequirementsFooterRow">
            <td>No additional requirements listed for this scholarship.</td>
        </tr>
    </tfoot>
</table>

<table class="viewonlyTable">
    <thead><tr><th colspan="2">Scholarship Questions</th></tr></thead>
    <tbody>
    <asp:Repeater ID="AdditionalCriteriaRepeater" runat="server">
        <ItemTemplate>
            <tr>
                <td><asp:label runat="server" ID="order" Text='<%# Bind("DisplayOrder")%>'></asp:label>.</td>
                <td><asp:label runat="server" ID="Label1" Text='<%# Bind("QuestionText")%>'></asp:label></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    </tbody>
</table>


</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
