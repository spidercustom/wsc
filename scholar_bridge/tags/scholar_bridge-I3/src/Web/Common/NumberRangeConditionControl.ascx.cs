﻿using System;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class NumberRangeConditionControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GreaterThanControl.Text = DefaultMinimumValue.HasValue
                                              ? DefaultMinimumValue.Value.ToString()
                                              : 0.ToString();
                LessThanControl.Text = DefaultMaximumValue.HasValue
                                              ? DefaultMaximumValue.Value.ToString()
                                              : 0.ToString();
            }
        }

        public decimal? MinimumAllowedValue { get; set; }
        public decimal? MaximumAllowedValue { get; set; }

        public decimal? DefaultMinimumValue { get; set; }
        public decimal? DefaultMaximumValue { get; set; }

        public string Format { get; set; }

        public decimal? MinimumValue
        {
            get
            {
                if (string.IsNullOrEmpty(GreaterThanControl.Text))
                {
                    return null;
                }
                decimal result;
                return decimal.TryParse(GreaterThanControl.Text, out result) 
                    ? (decimal?) result : null;
            }
            set 
            {
                GreaterThanControl.Text = value.HasValue ? 
                    value.Value.ToString(Format) : string.Empty;
            }
        }

        
        public decimal? MaximumValue
        {
            get
            {
                if (string.IsNullOrEmpty(LessThanControl.Text))
                {
                    return null;
                }
                decimal result;
                return decimal.TryParse(LessThanControl.Text, out result)
                    ? (decimal?)result : null;
            }
            set
            {
                GreaterThanControl.Text = value.HasValue ?
                    value.Value.ToString(Format) : string.Empty;
            }
        }

        protected void GreaterThanControlValidator_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            if (MinimumAllowedValue.HasValue)
            {
                if (null == MinimumValue)
                {
                    args.IsValid = false;
                    GreaterThanControlValidator.Text = "Invalid value or is left empty";
                    return;
                }

                if (MinimumValue < MinimumAllowedValue)
                {
                    args.IsValid = false;
                    GreaterThanControlValidator.Text = string.Format("Minimum allowed value is {0}", MinimumAllowedValue);
                    return;
                }
            }
        }

        protected void LessThanControlValidator_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            if (MaximumAllowedValue.HasValue)
            {
                if (null == MinimumValue)
                {
                    args.IsValid = false;
                    LessThanControlValidator.Text = "Invalid value or is left empty";
                    return;
                }

                if (MinimumValue < MinimumAllowedValue)
                {
                    args.IsValid = false;
                    LessThanControlValidator.Text = string.Format("Maximum allowed value is {0}", MaximumAllowedValue);
                    return;
                }
            }

            if (MaximumValue < MinimumValue)
            {
                args.IsValid = false;
                LessThanControlValidator.Text = string.Format("Should be greater than or equal to {0}", MinimumValue.Value);
                return;
            }
        }

        public RangeCondition<int> CreateIntegerRangeCondition()
        {
            var result = new RangeCondition<int>
                             {
                                 Minimum = (MinimumValue.HasValue ? (int) MinimumValue.Value : 0),
                                 Maximum = (MaximumValue.HasValue ? (int) MaximumValue.Value : 0)
                             };
            return result;
        }

        public void PopulateFromRangeCondition(RangeCondition<int> condition)
        {
            MinimumValue = condition.Minimum;
            MaximumValue = condition.Maximum;
        }

        public RangeCondition<float> CreateFloatRangeCondition()
        {
            var result = new RangeCondition<float>
            {
                Minimum = (MinimumValue.HasValue ? (float)MinimumValue.Value : 0),
                Maximum = (MaximumValue.HasValue ? (float)MaximumValue.Value : 0)
            };
            return result;
        }
    }
}