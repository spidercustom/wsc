﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain.Lookup;
using Spring.Context.Support;
using System.Web.UI.WebControls;
using JSHelper = ScholarBridge.Web.Common.Lookup.LookupDialogJSHelper;

namespace ScholarBridge.Web.Common.Lookup 
{
    public partial class LookupDialog : UserControl
    {
        public const string SEPARATOR_STRING = ", ";

        public string ItemSource { get; set; }
        public string Title { get; set; }
        public string BuddyControl { get; set; }
        public string DataPageUrl { get; set; }

        public string Keys
        {
            get { return hiddenids.Value; }
            set
            {
                hiddenids.Value = value;
                if (!String.IsNullOrEmpty(BuddyControl))
                {
                    SetBuddyControlValues();
                }
            }
        }
        private void SetBuddyControlValues()
        {
            var items = GetSelectedLookupItems();
            var buddy = (TextBox)FindControlRecursive(this.Page, BuddyControl);
            if (buddy != null)
                buddy.Text = string.Join(", ", (from i in items select i.Name).ToArray());
            //also set hiddenidsxml value 
            hiddenidsxml.Value = GetXML(items);
        }

        private string GetXML(IList<ILookup> list)
        {
            // Generating XML Data
            var xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("rows",
                             list.Select(row => new XElement("row", new XAttribute("id", row.Id.ToString()),
                                                             new XElement("cell", row.Name.ToString())
                                                    )
                                 )
                    )
                );


            return xmlDoc.ToString();
            

        }
        

        public string[] GetKeyStrings()
        {
            if (string.IsNullOrEmpty(hiddenids.Value))
                return new string[]{};
            return hiddenids.Value.Split(new[] {SEPARATOR_STRING}, StringSplitOptions.RemoveEmptyEntries);
        }

        public int[] GetKeysAsIntegers()
        {
            string[] keyStrings = GetKeyStrings();
            if (null == keyStrings)
                return new int[] { };
            return Array.ConvertAll<string, int>(keyStrings, Int32.Parse);
        }

        public IList<ILookup> GetSelectedLookupItems()
        {
            int[] keysAsIntegers = GetKeysAsIntegers();
            if (null == keysAsIntegers)
                return new List<ILookup>();
            IList<ILookup> selectedLookupItems = FindAll(keysAsIntegers, ItemSource);
            return selectedLookupItems;
        }

        public void PopulateListFromSelection<T>(IList<T> list)
        {
            list.Clear();
            IEnumerable<T> selectedItems =
                GetSelectedLookupItems().Cast<T>();
            selectedItems.ForEach(o => list.Add(o));
        }

        private static IList<ILookup> FindAll(IEnumerable<int> ids, string dalSpringKey)
        {
            if (null == ids)
                return new List<ILookup>();
            var idsList = ids.ToList();
            if (idsList.Count == 0)
                return new List<ILookup>();
            var dal = ContextRegistry.GetContext().GetObject<ILookupDAL>(dalSpringKey);
            var result = dal.FindAll(idsList);
            return result;
        }
        


        protected void Page_Load(object sender, EventArgs e)
        {
            //validate required properties
            if (string.IsNullOrEmpty(BuddyControl ))
                throw new NullReferenceException("Propery Buddy Control must have a valid value");
            if (string.IsNullOrEmpty(ItemSource))
                throw new NullReferenceException("Propery ItemSource must have a valid value");

            DataPageUrl = this.TemplateSourceDirectory + "/LookupSourceProcesser.aspx";

            CreateDynamicControls();
        }

        private void CreateDynamicControls()
        {
            
            var div = CreateDivforDialog();
            var uc = CreateLookupListControl(); 

            div.Controls.Add(uc);
            this.Controls.Add(div);
            //dialogHolder.Controls.Add(div);

            var dialogId = div.ClientID;
            var availableDivId = uc.FindControl("div_available").ClientID;
            var selectiondivId = uc.FindControl("lookuplist_selections").ClientID;
            var pagerdivId = uc.FindControl("pager").ClientID;
            var dataUrl = DataPageUrl + "?LookupSource=" + ItemSource ;
            var buddyId = this.ClientID.Replace(this.ID, BuddyControl); //txtResults.ClientID;
            var hiddenXMLID = hiddenidsxml.ClientID;
            var searchButtonId = uc.FindControl("btnSearch").ClientID;
            var searchBoxId = uc.FindControl("txtSearch").ClientID;
            btnLookup.Attributes.Add("onclick", JSHelper.BuildShowDialogJS(dialogId, availableDivId, selectiondivId, pagerdivId, dataUrl, hiddenXMLID,searchButtonId,searchBoxId));
            
            //add documentreadyscript
            Page.ClientScript.RegisterClientScriptBlock(
                typeof(Page),string.Format( "JS_{0}",this.ClientID),
              JSHelper.BuildCreateDialogJS(dialogId, buddyId,hiddenXMLID));
        }

        private HtmlGenericControl CreateDivforDialog()
        {
            var div = new HtmlGenericControl("DIV") { ID = "dialog" };
            div.Attributes.Add("class", "dialog");
            div.Attributes.Add("title", Title);
            return div;
        }

        private LookupList CreateLookupListControl()
        {
            var uc=(LookupList)LoadControl(this.TemplateSourceDirectory + "/LookupList.ascx");
            uc.ID = "lookuplist";
            uc.Attributes.Add("runat","server");
            uc.HiddenIds=FindControl("hiddenids").ClientID;
            return uc;
        }

        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }
    }
}
