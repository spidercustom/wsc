﻿using System;
using System.Web.Services;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using Spring.Context.Support;

namespace ScholarBridge.Web.Common.Lookup
{
    public partial class LookupSourceProcesser : System.Web.UI.Page
    {
        private const string LOOKUPSOURCE = "LookupSource";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request[LOOKUPSOURCE] == null)
                throw new ArgumentNullException("query string LookupSource is null");

            //read page values passed from grid
            int PageNo = int.Parse(Request.QueryString["page"]);
            int NumberofRows = int.Parse(Request.QueryString["rp"]);
            string SortOn = Request.QueryString["sortname"];
            string SortOrder = Request.QueryString["sortorder"];
            string SearchOn = Request.QueryString["qtype"];
            string SearchValue = Request.QueryString["query"];
            string ExludeItems = Request.QueryString["exclude"];
            if (ExludeItems.StartsWith(","))
                ExludeItems=ExludeItems.Remove(0, 1);

            if (PageNo == 0)
                PageNo = 1;
            if (NumberofRows == 0)
                NumberofRows = 15;

            //generate xml 
            var source = ContextRegistry.GetContext().GetObject<ILookupDAL>(Request[LOOKUPSOURCE]);


            var helper = new LookupDialogHelper(source, PageNo, NumberofRows, SortOn, SortOrder, SearchOn,
                                                SearchValue,ExludeItems);

            string xmldata = helper.GetXml();

            //write response 
            Response.ClearHeaders();
            Response.AppendHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
            Response.AppendHeader("Last-Modified",
                                  DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
            Response.AppendHeader("Cache-Control", "no-cache, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Content-type", "text/xml");
            Response.Write(xmldata);
            Response.End();
        }
    }
}