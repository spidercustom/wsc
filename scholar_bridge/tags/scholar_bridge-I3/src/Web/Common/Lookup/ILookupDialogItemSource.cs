﻿using System.Collections.Generic;

namespace ScholarBridge.Web.Common.Lookup
{
    public interface ILookupDialogItemSource
    {
       List<string> GetLookupItems();
    }
}
