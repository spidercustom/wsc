﻿using System.Text;

namespace ScholarBridge.Web.Common.Lookup
{
    public static class LookupDialogJSHelper
    {
        public static string BuildCreateDialogJS(string dialogId, string buddyId,string hiddenXMLID)
        {
            var sb = new StringBuilder();
            sb.Append("<script language='javascript'> $(function() {");
            sb.AppendFormat("createDialog('#{0}','#{1}','#{2}');", dialogId, buddyId, hiddenXMLID);
            sb.Append("}); </script>");
            return sb.ToString();
        }

        public static string BuildShowDialogJS(string dialogId, string availableDivId, string selectionDivId, string pagerDivId, string dataUrl, string hiddenXMLID, string searchButtonId, string searchBoxId)
        {
            return string.Format("JavaScript: return showDialog('#{0}','#{1}','#{2}','#{3}','{4}','#{5}','#{6}','#{7}');", dialogId, availableDivId, selectionDivId, pagerDivId, dataUrl, hiddenXMLID, searchButtonId, searchBoxId);
        }
        public static string BuildaddToSelectionsJS(string hiddenid)
        {
            return string.Format("addToSelections('#{0}');", hiddenid);
        }
        public static string BuildremoveFromSelectionsJS(bool withAll)
        {
            return string.Format("removeFromSelections('{0}');",withAll.ToString());
        }
    }
}
