﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupList.ascx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupList" %>
<link href="/styles/lookuplist.css" rel="stylesheet" type="text/css" />
    <div id="lookuplist_header">
        <p>Select single item using click or multiple items either using ctrl+click or mouse drag/lasso.</p>
    </div>

    <div id="lookuplist_left" >
    <h1>Available</h1>
    <div id="lookuplist_search" class="ui-widget">
  
    <asp:TextBox CssClass="text ui-widget-content ui-corner-all" runat="server" ID="txtSearch"  Height="18px" Width="200px"></asp:TextBox>
    <asp:HyperLink  runat="server" NavigateUrl="#" ID="btnSearch"><img src="/images/lookupdialog/magnifier.png" Alt="Go"  /></asp:HyperLink>
     
    </div>
    
        <div id="top" class="listcontainer"> 
            <asp:Panel  id="div_available" runat="server"> 

            </asp:Panel>
        </div>
        <asp:Panel  id="pager" runat="server" CssClass="jqpager"></asp:Panel>
    </div>

    <div id="lookuplist_right">
        <h1>Selections</h1>
             
                <asp:Panel  id="lookuplist_selections" runat="server"> 

                </asp:Panel>
             
 
     </div>
   
    
    <div id="lookuplist_middle">
    <asp:button id="btnAdd" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="Add>" Width="80px" Height="25px"/>
     <asp:button id="btnRemove" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="<Remove" Width="80px" Height="25px"/>
      <asp:button id="btnRemoveAll" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="<<Remove All" Width="100px" Height="25px"/>
    
    </div>

  

