﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Common
{
    public partial class ProviderScholarshipList : UserControl
    {

        public IList<Scholarship> Scholarships  { get; set; }
        IScholarshipService ScholarshipService { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Bind();
        }
        protected void Bind()
           
        {
            lstScholarships.DataSource = (from s in Scholarships orderby s.Name select s ).ToList() ;  
            
            lstScholarships.DataBind();
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs  e)
        {
            if (e.Item.ItemType==ListViewItemType.DataItem)
            {
                var scholarship =((Scholarship ) ((ListViewDataItem ) e.Item ).DataItem );
                
                var link = (HyperLink)e.Item.FindControl("linktoScholarship");
                link.NavigateUrl = LinkTo + "?id=" + scholarship.Id;
                var name = (Label)e.Item.FindControl("lblName");
                name.Text = scholarship.DisplayName;

            }
        }

        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
             pager.SetPageProperties(e.StartRowIndex , e.MaximumRows, false);
             Bind();
        }

      
        
    }
}