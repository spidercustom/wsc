﻿using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Common
{
    public partial class RegisterOrganization : System.Web.UI.UserControl
    {
        public IProviderService ProviderService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        public IUserDAL UserService { get; set; }
        public IStateDAL StateService { get; set; }

        public Type OrganizationType { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var states = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("AddressState");
                states.DataSource = StateService.FindAll();
                states.DataTextField = "Name";
                states.DataValueField = "Abbreviation";
                states.DataBind();
                states.Items.Insert(0, new ListItem("- Select One -", ""));
                //turn on automatic mail sending by setting up from address
                CreateUserWizard1.MailDefinition.From = ConfigHelper.GetSmtpSection().From;
            }
        }

        protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
        {
            var cuw = (CreateUserWizard)sender;
            cuw.Email = cuw.UserName;
        }

        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            User user = GetCurrentUser(sender);
            user.Name.FirstName = GetTextBoxValue(CreateUserWizardStep1, "FirstName");
            user.Name.MiddleName = GetTextBoxValue(CreateUserWizardStep1, "MiddleName");
            user.Name.LastName = GetTextBoxValue(CreateUserWizardStep1, "LastName");

            var newOrganization = (Organization) Activator.CreateInstance(OrganizationType);
            newOrganization.Name = GetTextBoxValue(CreateUserWizardStep1, "Name");
            newOrganization.TaxId = GetTextBoxValue(CreateUserWizardStep1, "TaxId");
            newOrganization.Website = GetTextBoxValue(CreateUserWizardStep1, "Website");
            newOrganization.Address = GetAddress(CreateUserWizardStep1);
            user.Phone = newOrganization.Phone = GetPhone(CreateUserWizardStep1, "Phone");
            user.Fax = newOrganization.Fax = GetPhone(CreateUserWizardStep1, "Fax");
            user.OtherPhone = newOrganization.OtherPhone = GetPhone(CreateUserWizardStep1, "OtherPhone");
            newOrganization.LastUpdate = new ActivityStamp(user);

            // XXX: This is sort of lame.
            if (newOrganization is Domain.Provider)
            {
                ProviderService.SaveNewWithAdminUser((Domain.Provider)newOrganization, user);
            }
            else if (newOrganization is Domain.Intermediary)
            {
                IntermediaryService.SaveNewWithAdminUser((Domain.Intermediary)newOrganization, user);
            }
        }

        private User GetCurrentUser(object sender)
        {
            var cuw = (CreateUserWizard)sender;
            return UserService.FindByUsername(cuw.Email);
        }

        private static PhoneNumber GetPhone(TemplatedWizardStep step2, string name)
        {
            string phone = GetTextBoxValue(step2, name);
            if (String.IsNullOrEmpty(phone))
                return null;

            return new PhoneNumber(phone);
        }

        private Address GetAddress(TemplatedWizardStep step2)
        {
            string street = GetTextBoxValue(step2, "AddressStreet");
            string street2 = GetTextBoxValue(step2, "AddressStreet2");
            string city = GetTextBoxValue(step2, "AddressCity");
            string postalCode = GetTextBoxValue(step2, "AddressPostalCode");
            string state = GetDropDownValue(step2, "AddressState");

            return new Address
            {
                Street = street,
                Street2 = street2,
                City = city,
                PostalCode = postalCode,
                State = StateService.FindByAbbreviation(state)
            };
        }

        private static string GetTextBoxValue(TemplatedWizardStep step, string name)
        {
            return GetTextBoxFromStep(step, name).Text;
        }

        private static string GetDropDownValue(TemplatedWizardStep step, string name)
        {
            return ((DropDownList)step.ContentTemplateContainer.FindControl(name)).SelectedValue;
        }

        private static TextBox GetTextBoxFromStep(TemplatedWizardStep step, string name)
        {
            return (TextBox)step.ContentTemplateContainer.FindControl(name);
        }

        protected void CreateUserWizard1_CreateUserError(object sender, CreateUserErrorEventArgs e)
        {
            switch (e.CreateUserError)
            {
                case MembershipCreateStatus.DuplicateUserName:
                case MembershipCreateStatus.DuplicateEmail:
                    var valid = (PropertyProxyValidator)
                                CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserNameValidator");
                    valid.Text = "EmailAddress is already in use, please choose another one.";
                    valid.IsValid = false;
                    break;
            }
        }

        protected void CreateUserWizard1_SendingMail(object sender, MailMessageEventArgs e)
        {
            // We handled sending it ourselves in the SaveNewWithAdminUser, so cancel
            e.Cancel = true;
        }
    }
}