﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Business.Actions;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Web.Admin;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Common
{
    public partial class MessageView : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }
        public IMessageActionService MessageActionService { get; set; }

        public string ListUrl { get; set; }

        public int MessageId { get { return Int32.Parse(Request["id"]); } }

        public Domain.Messaging.Message CurrentMessage { get; set; }
        public IAction MessageAction { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            CurrentMessage = MessageService.FindMessage(UserContext.CurrentUser, UserContext.CurrentOrganiztion, MessageId);
            if (null != CurrentMessage)
            {
                MessageService.MarkMessageRead(CurrentMessage);
                MapRelatedData();

                MessageAction = MessageActionService.GetAction(CurrentMessage.MessageTemplate);
                if (! Page.IsPostBack)
                {
                    ToggleButtons();

                    subjectLbl.Text = CurrentMessage.Subject;
                    fromLbl.Text = CurrentMessage.From.ToString();
                    dateLbl.Text = CurrentMessage.Date.ToLocalTime().ToShortDateString();
                    contentLbl.Text = CurrentMessage.Content;
                }
            }
        }

        private void MapRelatedData()
        {
            if (CurrentMessage is OrganizationMessage)
            {
                var orgMessage = CurrentMessage as OrganizationMessage;
                if (null != orgMessage && null != orgMessage.RelatedOrg)
                {
                    var control = (ShowOrg)LoadControl("~/Common/ShowOrg.ascx");
                    control.Organization = orgMessage.RelatedOrg;
                    relatedInfo.Controls.Add(control);
                    return;
                }
            }
            
            if (CurrentMessage is ScholarshipMessage)
            {
                var scholarshipMessage = CurrentMessage as ScholarshipMessage;
                if (null != scholarshipMessage && null != scholarshipMessage.RelatedScholarship)
                {
                    var control = (ShowScholarship) LoadControl("~/Common/ShowScholarship.ascx"); 
                    control.Scholarship = scholarshipMessage.RelatedScholarship;
                    relatedInfo.Controls.Add(control);
                    return;
                }
            }
        }

        private void ToggleButtons()
        {
            if (CurrentMessage.IsArchived)
            {
                archiveBtn.Visible = false;
                approveBtn.Visible = false;
                rejectBtn.Visible = false;
            }
            else
            {
                approveBtn.Visible = MessageAction.SupportsApprove;
                rejectBtn.Visible = MessageAction.SupportsReject;
            }
        }

        protected void approveBtn_Click(object sender, EventArgs e)
        {
            MessageAction.Approve(CurrentMessage, ConfigHelper.GetMailParams(null), UserContext.CurrentUser);

            SuccessMessageLabel.SetMessage("Message approved.");
            Response.Redirect(ListUrl);
        }

        protected void rejectBtn_Click(object sender, EventArgs e)
        {
            MessageAction.Reject(CurrentMessage, ConfigHelper.GetMailParams(null), UserContext.CurrentUser);

            SuccessMessageLabel.SetMessage("Message rejected.");
            Response.Redirect(ListUrl);
        }

        protected void archiveBtn_Click(object sender, EventArgs e)
        {
            MessageService.ArchiveMessage(CurrentMessage);

            SuccessMessageLabel.SetMessage("Message archived.");
            Response.Redirect(ListUrl);
        }
    }
}