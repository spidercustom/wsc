﻿<%@ Page Title="Provider" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Default" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <p>
        This will be the home page for a Provider. It will contain the dashboard elements
        based on their organization.
    </p>
    <sb:Login ID="loginForm" runat="server" />
    
    <asp:LoginView ID="loginView" runat="server">
        <AnonymousTemplate>
            <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Provider/RegisterProvider.aspx">Register as a Scholarship Provider</asp:HyperLink>
        </AnonymousTemplate>
    </asp:LoginView>
    
    <asp:HyperLink ID="termsLnk" runat="server" NavigateUrl="~/Provider/Terms.aspx">Provider Terms &amp; Conditions</asp:HyperLink>

</asp:Content>
