﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Relationships
{
    public partial class InActivate : Page
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        private const string DEFAULT_PAGEURL = "~/Provider/Relationships/Default.aspx";
        public int RelationshipId { get { return Int32.Parse(Request["id"]); } }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            //inactivate
            var relationship = RelationshipService.GetById(RelationshipId);
            if (relationship == null)
                throw new ArgumentNullException("relationship");
            relationship.LastUpdate=new ActivityStamp(UserContext.CurrentUser);
            RelationshipService.InActivate(relationship);
            SuccessMessageLabel.SetMessage("Relationship Inactivated.");
            Response.Redirect(DEFAULT_PAGEURL);

        }
    }
}
