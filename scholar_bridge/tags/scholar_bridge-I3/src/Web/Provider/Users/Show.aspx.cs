﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Provider.Users
{
    public partial class Show : Page
    {
        public IUserContext UserContext { get; set; }
        public IProviderService ProviderService { get; set; }

        public int UserId { get { return Int32.Parse(Request["id"]); } }
        public User CurrentUser { get; set;}
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            CurrentUser = ProviderService.FindUserInOrg(UserContext.CurrentProvider, UserId);
            userDetails.CurrentUser = CurrentUser;

            ToggleButtons();
        }

    	private void ToggleButtons()
        {
    		bool userIsAdmin = User.IsInRole(Role.PROVIDER_ADMIN_ROLE);
    		bool userIsViewingSelf = UserContext.CurrentUser.Id == UserId;

			if (!(userIsAdmin || userIsViewingSelf))
			{
				editLinkButton.Visible = false;
			}

			deleteBtn.Visible = !CurrentUser.IsDeleted && userIsAdmin;
            reactivateBtn.Visible = CurrentUser.IsDeleted && userIsAdmin;

            // Don't allow someone to delete themselves
            if (userIsViewingSelf || !(userIsAdmin))
            {
                deleteBtn.Visible = false;
            }
        }

        protected void deleteBtn_Click(object sender, EventArgs e)
        {
            CurrentUser.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.DeleteUser(UserContext.CurrentProvider, CurrentUser);
            SuccessMessageLabel.SetMessage("User was deleted.");
            Response.Redirect("~/Provider/Users/");
        }

        protected void reactivateBtn_Click(object sender, EventArgs e)
        {
            CurrentUser.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.ReactivateUser(UserContext.CurrentProvider, CurrentUser);
            SuccessMessageLabel.SetMessage("User was reactivated.");
            Response.Redirect("~/Provider/Users/");
        }

		protected void editLinkButton_Click(object sender, EventArgs e)
		{
			Response.Redirect(string.Format("~/Provider/Users/ChangeName.aspx?id={0}", UserId));
		}
    }
}
