﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Provider.Users
{
    public partial class Create : Page
    {
        public IUserContext UserContext { get; set; }
        public IProviderService ProviderService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
			if (!User.IsInRole(Role.PROVIDER_ADMIN_ROLE))
				Response.Redirect("~/default.aspx");
        }

        protected void userForm_OnUserSaved(User user)
        {
            var provider = UserContext.CurrentProvider;
            user.LastUpdate = new ActivityStamp(UserContext.CurrentUser);

            ProviderService.SaveNewUser(provider, user, true);

            SuccessMessageLabel.SetMessage("User was created.");
            Response.Redirect("~/Provider/Users/");
        }

        protected void userForm_OnFormCanceled()
        {
            Response.Redirect("~/Provider/Users/");
        }
    }
}
