﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeSelectionControl.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.AttributeSelectionControl" %>
<asp:Repeater ID="AttributesControl" runat="server">
  <HeaderTemplate>
    <table>
      <thead>
        <tr>
          <th>Usage type</th>
          <th>Attribute</th>
        </tr>
      </thead>
      <tbody>
  </HeaderTemplate>
  <ItemTemplate>
    <tr class="row">
      <td>
        <asp:RadioButtonList ID="AttributeUsageControl" runat="server" 
          RepeatLayout="Flow">
          <asp:ListItem Value="1">Preference</asp:ListItem>
          <asp:ListItem Value="2">Minimum</asp:ListItem>
          <asp:ListItem Value="0" Selected="True">Not Used</asp:ListItem>
        </asp:RadioButtonList>
      </td>
      <td>
        <input type="hidden" runat="server" id="KeyControl" value='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />
        <%# DataBinder.Eval(Container.DataItem, "Value") %>
      </td>
    </tr>
  </ItemTemplate>
  <AlternatingItemTemplate>
    <tr class="altrow">
      <td>
        <asp:RadioButtonList ID="AttributeUsageControl" runat="server" 
          RepeatLayout="Flow">
          <asp:ListItem Value="1">Preference</asp:ListItem>
          <asp:ListItem Value="2">Minimum</asp:ListItem>
          <asp:ListItem Value="0" Selected="True">Not Used</asp:ListItem>
        </asp:RadioButtonList>
      </td>
      <td>
        <input type="hidden" runat="server" id="KeyControl" value='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />
        <%# DataBinder.Eval(Container.DataItem, "Value") %>
      </td>
    </tr>
  </AlternatingItemTemplate>
  <FooterTemplate>
      </tbody>
    </table>
  </FooterTemplate>
</asp:Repeater>