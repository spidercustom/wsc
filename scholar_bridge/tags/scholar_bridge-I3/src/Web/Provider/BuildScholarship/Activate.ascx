﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activate.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Activate" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="ByMonthControl" Src="~/Common/DayByMonth.ascx" %>
<%@ Register TagPrefix="sb" TagName="ByWeekControl" Src="~/Common/DayByWeek.ascx" %>

<h3>Build Scholarship – Activate</h3>
<h4><asp:Label ID="scholarshipNameLbl" runat="server" /></h4>

Placehold: Explanation of submit for validation.

