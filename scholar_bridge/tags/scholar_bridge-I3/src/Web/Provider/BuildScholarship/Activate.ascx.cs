﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Activate : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        Scholarship scholarshipInContext;

        protected void Page_Load(object sender, EventArgs e)
        {
            scholarshipInContext = Container.GetDomainObject();
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        private void PopulateScreen()
        {
            if (null != scholarshipInContext.AdditionalQuestions &&
                scholarshipInContext.AdditionalQuestions.Count > 0)
            {
                scholarshipNameLbl.Text = scholarshipInContext.Name;
            }
        }

        public override void Save()
        {
            ScholarshipService.RequestActivation(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.RequestedActivation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.RequestedActivation;
        }

        public override int StepIndex
        {
            get { return 9; }
        }

        public override bool ValidateStep()
        {
            var result = Validation.Validate(scholarshipInContext);
            return result.IsValid;
        }
    }
}
