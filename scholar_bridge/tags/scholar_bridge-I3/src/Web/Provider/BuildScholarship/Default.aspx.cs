﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Default : SBBasePage, IWizardStepsContainer<Scholarship>
    {
        public const string SCHOLARSHIP_ID = "sid";
        public const string RESUME_FROM = "resumefrom";
        public const string COPY_FROM = "copyfrom";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            foreach (var step in Steps)
            {
                step.Container = this;
            }

            if (!IsPostBack)
            {
            	CheckForDataChanges = true;

                bool copyFromParameterExists = Array.Exists(Request.Params.AllKeys, key => COPY_FROM.Equals(key));
                bool resumeFromParameterExists = Array.Exists(Request.Params.AllKeys, key => RESUME_FROM.Equals(key));
                var scholarshipToEdit = GetCurrentScholarship();

				BypassPromptIds.AddRange(
					new[]	{
								"SideBarButton",
								"StartNextButton",
								"StepPreviousButton",                            
								"StepNextButton",                            
								"SaveAndExitButton",                            
								"FinishPreviousButton",                            
								"FinishButton"
							});

				if (copyFromParameterExists)
                {
                    BeginCopyFrom();
                }
                else if (scholarshipToEdit != null)
                {
                    // FIXME: Intermediary won't work
                    if (!scholarshipToEdit.Provider.Id.Equals(UserContext.CurrentProvider.Id))
                        throw new InvalidOperationException("Scholarship do not belong to provider in context");

                    if (! scholarshipToEdit.CanEdit())
                    {
                        Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + scholarshipToEdit.Id);
                    }

                    if (resumeFromParameterExists)
                    {
                        ResumeFromStep();
                    }
                    else
                    {
                        ResumeWizard();
                    }
                }
            }
        }

        protected bool CanEdit()
        {
            var scholarship = GetCurrentScholarship();
            return null == scholarship || scholarship.CanEdit();
        }

        protected void SaveAndExitButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ValidateStep() && CanEdit())
            {
                Save();
                Response.Redirect("../");
            }
        }

        public Scholarship GetCurrentScholarship()
        {
			return CurrentScholarshipId > 0 ? ScholarshipService.GetById(CurrentScholarshipId) : null;
        }

    	private int CurrentScholarshipId
        {
            get
            {
                string idString = Request.Params[SCHOLARSHIP_ID];//retrive from parameters
                if (string.IsNullOrEmpty(idString)) //if not in parameters
                {
                    idString = 
                        ViewState[SCHOLARSHIP_ID] == null ? 
                        "-1" : 
                        ViewState[SCHOLARSHIP_ID].ToString(); //retrive from view state
                }

                int scholarshipId;
                if (!Int32.TryParse(idString, out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");

                return scholarshipId;
            }
            set
            {
                ViewState[SCHOLARSHIP_ID] = value;
            }
        }

        protected void BuildScholarshipWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Page.IsValid && ValidateStep())
            {
                if (CanEdit())
                    Save();
            }
            else if (e.NextStepIndex > e.CurrentStepIndex)
            {
                e.Cancel = true;
            }
        }

        protected void BuildScholarshipWizard_ActiveStepChanged(object sender, EventArgs e)
        {
            if (BuildScholarshipWizard.ActiveStep != null)
            {
                var wizardStepControl = WizardStepControlQueries.FindFirstWizardStepControl<Scholarship>(BuildScholarshipWizard.ActiveStep.Controls);
                if (wizardStepControl != null)
                {
                    wizardStepControl.Activated();
                }
            }
        }

        private bool ValidateStep()
        {
            if (BuildScholarshipWizard.ActiveStep != null)
            {
                var wizardStepControl = WizardStepControlQueries.FindFirstWizardStepControl<Scholarship>(BuildScholarshipWizard.ActiveStep.Controls);
                if (wizardStepControl != null)
                {
                    return wizardStepControl.ValidateStep();
                }
            }

            return true;
        }

        private void Save()
        {
            if (Page.IsValid && ValidateStep())
            {
                if (BuildScholarshipWizard.ActiveStep != null)
                {
                    var wizardStepControl = WizardStepControlQueries.FindFirstWizardStepControl<Scholarship>(BuildScholarshipWizard.ActiveStep.Controls);
                    if (wizardStepControl != null)
                    {
                        wizardStepControl.Save();
                        if (wizardStepControl.ChangeNextStepIndex())
                            BuildScholarshipWizard.ActiveStepIndex = wizardStepControl.GetChangedNextStepIndex();
                    }
                }
                CurrentScholarshipId= GetDomainObject().Id;
            	Dirty = false;
            }
        }

        private void BeginCopyFrom()
        {
            string copyFromIdString = Request.Params[COPY_FROM];
            int copyFromId;
            if (!Int32.TryParse(copyFromIdString, out copyFromId))
                throw new ArgumentException("Cannot understand value of parameter scholarship");
            Scholarship copyFrom = ScholarshipService.GetById(copyFromId);
            scholarshipInContext = (Scholarship) ((ICloneable)copyFrom).Clone();
            BuildScholarshipWizard.ActiveStepIndex = 1;
        }

        #region IWizardStepsContainer<Scholarship> Members
        //TODO: Can be moved to WizardStepsContainerPageBase
        public void ResumeWizard()
        {
            for (var stepIndex = 0; stepIndex < Steps.Length; stepIndex++)
            {
                var stepControl = Steps[stepIndex];
                if (stepControl.WasSuspendedFrom(GetDomainObject()))
                {
                    BuildScholarshipWizard.ActiveStepIndex = stepControl.StepIndex;
                    break;
                }
            }
        }

        //TODO: Can be moved to WizardStepsContainerPageBase
        private void ResumeFromStep()
        {
            var resumeFromPageIndexString = Request.Params[RESUME_FROM];
			var resumeFromPageIndex = Int32.Parse(resumeFromPageIndexString);
            
            var scholarship = GetCurrentScholarship();
            var resumeFromIndex = Math.Min(resumeFromPageIndex, Steps.Length);
            for (; resumeFromIndex >= 0; resumeFromIndex--)
            {
                var stepControl = Steps[resumeFromIndex-1];
                if (stepControl.CanResume(scholarship))
                {
                    BuildScholarshipWizard.ActiveStepIndex = stepControl.StepIndex;
                    return;
                }
            }
            BuildScholarshipWizard.ActiveStepIndex = 0;
        }

        //TODO: Can be moved to WizardStepsContainerPageBase
        public IWizardStepControl<Scholarship>[] Steps
        {
            get
            {
                IWizardStepControl<Scholarship>[] stepControls = WizardStepControlQueries.FindWizardStepControls<Scholarship>(BuildScholarshipWizard.WizardSteps);
                return stepControls;
            }
        }

        //TODO: Can be moved to WizardStepsContainerPageBase
        Scholarship scholarshipInContext;
        public Scholarship GetDomainObject()
        {
            if (scholarshipInContext == null)
            {
                scholarshipInContext = GetCurrentScholarship() ?? new Scholarship();
            }
            return scholarshipInContext;
        }

        public void GoPrior()
        {
            BuildScholarshipWizard.ActiveStepIndex = Math.Max(
                0,
                BuildScholarshipWizard.ActiveStepIndex - 1);
        }

        public void GoNext()
        {
            BuildScholarshipWizard.ActiveStepIndex = Math.Min(
                BuildScholarshipWizard.WizardSteps.Count-1,
                BuildScholarshipWizard.ActiveStepIndex + 1);
        }

        #endregion

		protected void BuildScholarshipWizard_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
		{
			if (e.CurrentStepIndex > 0)
				BuildScholarshipWizard.ActiveStepIndex = e.CurrentStepIndex - 1;

		}
    }
}
