﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class AttributeSelectionControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public Repeater RepeaterControl
        {
            get { return AttributesControl; }
        }

        public void ForEachRepeaterItem(Action<RadioButtonList, string> action)
        {
            foreach (RepeaterItem repeaterItem in RepeaterControl.Items)
            {
                var usageTypeControl = (RadioButtonList) repeaterItem.FindControl("AttributeUsageControl");
                var attributeControl = (HtmlInputHidden) repeaterItem.FindControl("KeyControl");
                var attribute = attributeControl.Value;

                action(usageTypeControl, attribute);
            }
        }

        public void ForEachRepeaterItem(Action<RadioButtonList, Int32> action)
        {
            ForEachRepeaterItem((radioControl, stringValue) => action(radioControl, Int32.Parse(stringValue)) );
        }

    }
}