﻿using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class MatchCriteriaSelection : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        
        Scholarship scholarshipInContext;

        protected void Page_Load(object sender, EventArgs e)
        {
            scholarshipInContext = Container.GetDomainObject();
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
            {
                MatchSpecifications.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(typeof (MatchCriteriaAttribute));
                MatchSpecifications.RepeaterControl.DataBind();

                FundingSpecifications.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(typeof(FundingProfileAttribute));
                FundingSpecifications.RepeaterControl.DataBind();

                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {
            PopulateMatchCriteriaAttributesScreen();
            PopulateFundingAttributesScreen();
        }

        private void PopulateMatchCriteriaAttributesScreen()
        {
            MatchSpecifications.
                ForEachRepeaterItem((RadioButtonList usageTypeControl, int keyValue) =>
                {
                    var attribute = (MatchCriteriaAttribute)keyValue;

                    var attributeUsage = scholarshipInContext.SeekerMatchCriteria.AttributesUsage.
                        FirstOrDefault(o => o.Attribute == attribute);

                    usageTypeControl.SelectedValue = 
                        null == attributeUsage ? 
                        ScholarshipAttributeUsageType.NotUsed.NumericValueString() : 
                        attributeUsage.UsageType.NumericValueString();
                });
        }

        private void PopulateFundingAttributesScreen()
        {
            FundingSpecifications.
                ForEachRepeaterItem((RadioButtonList usageTypeControl, int keyValue) =>
                {
                    var attribute = (FundingProfileAttribute) keyValue;

                    var attributeUsage = scholarshipInContext.FundingProfile.AttributesUsage.
                        FirstOrDefault(o => o.Attribute == attribute);
                    
                    usageTypeControl.SelectedValue = 
                        null == attributeUsage ? 
                        ScholarshipAttributeUsageType.NotUsed.NumericValueString() : 
                        attributeUsage.UsageType.NumericValueString();
                });
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        private void PopulateObjects()
        {
            PopulateMatchCriteriaAttributesObject();
            PopulateFundingAttributesObject();
            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage < ScholarshipStages.MatchCriteriaSelection)
                scholarshipInContext.Stage = ScholarshipStages.MatchCriteriaSelection;
        }

        private void PopulateFundingAttributesObject()
        {
            scholarshipInContext.FundingProfile.AttributesUsage.Clear();
            
            foreach (RepeaterItem repeaterItem in FundingSpecifications.RepeaterControl.Items)
            {
                var usageTypeControl = (RadioButtonList)repeaterItem.FindControl("AttributeUsageControl");
                var usageType = (ScholarshipAttributeUsageType)Int32.Parse(usageTypeControl.SelectedValue);
                if (!usageType.Equals(ScholarshipAttributeUsageType.NotUsed))
                {
                    var attributeControl = (HtmlInputHidden)repeaterItem.FindControl("KeyControl");
                    var attribute = (FundingProfileAttribute) Int32.Parse(attributeControl.Value);

                    var attributeUsage = new FundingProfileAttributeUsage
                                             {
                                                 Attribute = attribute,
                                                 UsageType = usageType
                                             };
                    scholarshipInContext.FundingProfile.AttributesUsage.Add(attributeUsage);
                }
            }
        }

        private void PopulateMatchCriteriaAttributesObject()
        {
            scholarshipInContext.SeekerMatchCriteria.AttributesUsage.Clear();

            foreach (RepeaterItem repeaterItem in MatchSpecifications.RepeaterControl.Items)
            {
                var usageTypeControl = (RadioButtonList)repeaterItem.FindControl("AttributeUsageControl");
                var usageType = (ScholarshipAttributeUsageType)Int32.Parse(usageTypeControl.SelectedValue);
                if (!usageType.Equals(ScholarshipAttributeUsageType.NotUsed))
                {
                    var attributeControl = (HtmlInputHidden)repeaterItem.FindControl("KeyControl");
                    var attribute = (MatchCriteriaAttribute)Int32.Parse(attributeControl.Value);

                    var attributeUsage = new MatchCriteriaAttributeUsage
                    {
                        Attribute = attribute,
                        UsageType = usageType
                    };
                    scholarshipInContext.SeekerMatchCriteria.AttributesUsage.Add(attributeUsage);
                }
            }
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.MatchCriteriaSelection;
        }

        public override int StepIndex
        {
            get { return (int)ScholarshipStages.MatchCriteriaSelection; }
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.MatchCriteriaSelection;
        }
    }
}