﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class AdditionalCriteria : WizardStepUserControlBase<Scholarship>
	{
		#region DI Properties
		public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IAdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
		#endregion

		#region Properties
		private List<Question> Questions
    	{
    		get
    		{
    			if (ViewState["Questions"] == null)
    			{
					List<Question> questions = new List<Question>(scholarshipInContext.AdditionalQuestions.Count);
    				foreach (var question in scholarshipInContext.AdditionalQuestions)
    				{
    					questions.Add(new Question() {DisplayOrder = question.DisplayOrder, QuestionText = question.QuestionText});
    				}
					ViewState["Questions"] = questions;
    			}
				return (List<Question>)ViewState["Questions"];
    		}
    	}

		private Scholarship _scholarshipInContext;
		private Scholarship scholarshipInContext
		{
			get
			{
				if (_scholarshipInContext == null)
					_scholarshipInContext = Container.GetDomainObject();
				return _scholarshipInContext;
			}
		}
		#endregion

		#region Page Events
		protected void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
            {
                PopulateScreen();
				if (Page is IChangeCheckingPage)
				{
					IChangeCheckingPage ccPage = (IChangeCheckingPage)Page;
					ccPage.BypassPromptIds.AddRange(new[] { "updateQuestionButton", "cancelQuestionUpdateButton", "editQuestionButton", "deleteQuestionButton", "addQuestionButton", "addEmptyQuestionButton" });
				}
            }
		}
		#endregion

		#region Private Methods
		private void PopulateScreen()
        {
            if (null != scholarshipInContext.AdditionalQuestions &&
                scholarshipInContext.AdditionalQuestions.Count > 0)
            {
                PopulateCheckBoxes(AdditionalRequirements, AdditionalRequirementDAL.FindAll());
                AdditionalRequirements.Items.SelectItems(scholarshipInContext.AdditionalRequirements, ar => ar.Id.ToString());
                scholarshipNameLbl.Text = scholarshipInContext.Name;
            }
        }

        private void PopulateObjects()
        {
            var selectedAdditionalReqs = AdditionalRequirements.Items.SelectedItems(item => Int32.Parse(item));
            scholarshipInContext.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll(selectedAdditionalReqs));

            int qNum = CreateOrUpdateQuestions();
            RemoveDeletedQuestions(qNum);

            if (scholarshipInContext.Stage < ScholarshipStages.AdditionalCriteria)
                scholarshipInContext.Stage = ScholarshipStages.AdditionalCriteria;

        }

        private int CreateOrUpdateQuestions()
        {
            var user = UserContext.CurrentUser;

            int qNum = 0;
            foreach (var q in Questions)
            {
            	string questionText = q.QuestionText;
                if (! String.IsNullOrEmpty(questionText))
                {
                    ScholarshipQuestion question;
                    qNum++;
                    if (qNum > scholarshipInContext.AdditionalQuestions.Count)
                    {
                        question = new ScholarshipQuestion();
                        scholarshipInContext.AddAdditionalQuestion(question);
                    }
                    else
                    {
                        question = scholarshipInContext.AdditionalQuestions[qNum - 1];
                    }
                    question.QuestionText = questionText;
                    question.DisplayOrder = qNum;
                    question.LastUpdate = new ActivityStamp(user);
                }
            }
            return qNum;
        }

        private void RemoveDeletedQuestions(int qNum)
        {
            if (qNum < scholarshipInContext.AdditionalQuestions.Count)
            {
                for (int del = scholarshipInContext.AdditionalQuestions.Count; del > qNum; del--)
                    scholarshipInContext.AdditionalQuestions.RemoveAt(del - 1);
            }
		}

		private static void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
		}
		#endregion

		#region Wizard Control Override Methods
		public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.AdditionalCriteria;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.AdditionalCriteria;
        }

        public override int StepIndex
        {
            get { return (int)ScholarshipStages.AdditionalCriteria; }
        }

        public override bool ValidateStep()
        {
            return true;
		}
		#endregion

		#region control event handlers
		protected void addQuestionButton_Click(object sender, EventArgs e)
		{
			TextBox questionText = questionsGrid.FooterRow.FindControl("questionAddTextBox") as TextBox;
			if (questionText.Text != null && questionText.Text.Trim() != string.Empty)
			{
				Questions.Add(new Question() { DisplayOrder = Questions.Count + 1, QuestionText = questionText.Text });
				questionsGrid.DataBind();
			}
		}

		protected void addEmptyQuestionButton_Click(object sender, EventArgs e)
		{
			TextBox questionText = questionsGrid.Controls[0].Controls[0].FindControl("questionEmptyAddTextBox") as TextBox;
			if (questionText.Text != null && questionText.Text.Trim() != string.Empty)
			{
				Questions.Add(new Question() { DisplayOrder = Questions.Count + 1, QuestionText = questionText.Text });
				questionsGrid.DataBind();
			}
		}

		protected void questionsDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = this;
		}

		protected void questionsGrid_RowUpdated(object sender, GridViewUpdatedEventArgs e)
		{
			questionsGrid.DataBind();
		}
		#endregion

		#region Question DataSource methods

		public List<Question> SelectQuestions()
		{
			return Questions;
		}
		
		public void UpdateQuestion(int DisplayOrder, string QuestionText)
		{
			int removeIndex = -1;
			bool remove = false;
			foreach (var question in Questions)
			{
				removeIndex++;
				if (question.DisplayOrder == DisplayOrder)
				{
					if (QuestionText == null || QuestionText.Trim() == string.Empty)
						remove = true;
					else
						question.QuestionText = QuestionText;

					break;
				}
			}
			if (remove)
			{
				Questions.RemoveAt(removeIndex);
				int i = 0;
				foreach (var q in Questions)
				{
					q.DisplayOrder = ++i;	
				}
			}
		}
		#endregion

		#region Inner Classes
		[Serializable]
		public class Question
		{
			public int DisplayOrder
			{
				get; set;
			}
			public string QuestionText
			{
				get; set;
			}
		}
		#endregion
	}
}
