﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;
using ScholarBridge.Domain.Extensions;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class SeekerProfile : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

		Scholarship _scholarshipInContext;
    	Scholarship scholarshipInContext
    	{
    		get
    		{
    			if (_scholarshipInContext == null)
					_scholarshipInContext = Container.GetDomainObject();
    			return _scholarshipInContext;
    		}
    	}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context"); 

            if (!IsPostBack)
            {
                EthnicityControl.DataBind();
                ReligionControl.DataBind();
                WorkHoursControl.DataBind();
                ServiceHoursControl.DataBind();

                PopulateScreen();
            }
        }

        #region Visibilty of the controls
        public override void Activated()
        {
            base.Activated();
            SetControlsVisibility();
        }

        private void SetControlsVisibility()
        {
            SeekerPersonalityContainerControl.Visible = true;
            SeekerInterestsControlContainer.Visible = true;
            SeekerTypeContainerControl.Visible = true;
            SeekerDemographicsContainerControl.Visible = true;
            SeekerInterestsControlContainer.Visible = true;
            SeekerActivitiesControlContainer.Visible = true;
            SeekerPerformanceControlContainer.Visible = true;

            SeekerMatchCriteria criteria = scholarshipInContext.SeekerMatchCriteria;
            
            FiveWordsContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.SeekerVerbalizingWord);
            FiveSkillsContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.SeekerSkill);
            StudentGroupContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.StudentGroup);
            SchoolTypeContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.SchoolType);
            AcademicProgramContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.AcademicProgram);
            SeekerStatusContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.SeekerStatus);
            ProgramLengthContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.ProgramLength);
            CollegesContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.College);
            FirstGenerationContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.FirstGeneration);
            EthnicityContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Ethnicity);
            ReligionContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Religion);
            GendersContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Gender);
            StatesContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.State);
            CountyContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.County);
            CityContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.City);
            SchoolDistrictContainerControl.Visible =
                criteria.HasAttributeUsage(MatchCriteriaAttribute.SchoolDistrict);
            HighSchoolContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.HighSchool);
            AcademicAreasContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.AcademicArea);
            CareersContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Career);
            CommunitiyInvolvementCausesContainerControl.Visible =
                criteria.HasAttributeUsage(MatchCriteriaAttribute.CommunityInvolvementCause);
            OrganizationsContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Organization);
            AffiliationTypesContainerControl.Visible =
                criteria.HasAttributeUsage(MatchCriteriaAttribute.AffiliationType);
            HobbiesContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.SeekerHobby);
            SportsContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Sport);
            ClubsContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Club);
            WorkTypeContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.WorkType);
            WorkHoursContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.WorkHour);
            ServiceTypeContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.ServiceType);
            ServiceHoursContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.ServiceHour);
            GPAControlContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.GPA);
            ClassRankContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.ClassRank);
            SATContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.SAT);
            ACTContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.ACT);
            HonorsContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.Honor);
            APCreditContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.APCreditsEarned);
            IBCreditContainerControl.Visible = criteria.HasAttributeUsage(MatchCriteriaAttribute.IBCreditsEarned);
            
            SetDependentVisibility();
        }

        private void SetDependentVisibility()
        {
            SeekerPersonalityContainerControl.Visible = 
                FiveWordsContainerControl.Visible |
                FiveSkillsContainerControl.Visible;

            SeekerTypeContainerControl.Visible =
                StudentGroupControl.Visible |
                SchoolTypeControl.Visible |
                AcademicProgramControl.Visible |
                SeekerStatusControl.Visible |
                ProgramLengthControl.Visible |
                CollegesContainerControl.Visible |
                FirstGenerationControl.Visible;
            

            SeekerDemographicsContainerControl.Visible =
                EthnicityContainerControl.Visible |
                ReligionContainerControl.Visible |
                GendersContainerControl.Visible |
                StatesContainerControl.Visible |
                CountyContainerControl.Visible |
                CityContainerControl.Visible |
                SchoolDistrictContainerControl.Visible |
                HighSchoolContainerControl.Visible;

            SeekerInterestsControlContainer.Visible =
                AcademicAreasContainerControl.Visible |
                CareersContainerControl.Visible |
                CommunitiyInvolvementCausesContainerControl.Visible |
                OrganizationsContainerControl.Visible |
                AffiliationTypesContainerControl.Visible;

            SeekerActivitiesControlContainer.Visible =
                HobbiesContainerControl.Visible |
                SportsContainerControl.Visible |
                ClubsContainerControl.Visible |
                WorkTypeContainerControl.Visible |
                WorkHoursContainerControl.Visible |
                ServiceTypeContainerControl.Visible |
                ServiceHoursContainerControl.Visible;

            SeekerPerformanceControlContainer.Visible =
                GPAControlContainerControl.Visible |
                ClassRankContainerControl.Visible |
                SATContainerControl.Visible |
                ACTContainerControl.Visible |
                HonorsControl.Visible |
                APCreditControl.Visible;


            NoSelectionContainerControl.Visible = !
                (SeekerPersonalityContainerControl.Visible |
                SeekerTypeContainerControl.Visible |
                SeekerDemographicsContainerControl.Visible |
                SeekerInterestsControlContainer.Visible |
                SeekerActivitiesControlContainer.Visible |
                SeekerPerformanceControlContainer.Visible);
        }
        #endregion

        private void PopulateScreen()
        {
            var seekerMatchCriteria = scholarshipInContext.SeekerMatchCriteria;

            FiveWordsControlDialogButton.Keys = seekerMatchCriteria.Words.CommaSeparatedIds();
            FiveSkillsControlDialogButton.Keys =  seekerMatchCriteria.Skills.CommaSeparatedIds();
            StudentGroupControl.SelectedValues = (int)seekerMatchCriteria.StudentGroups;
            SchoolTypeControl.SelectedValues = (int)seekerMatchCriteria.SchoolTypes;
            AcademicProgramControl.SelectedValues= (int)seekerMatchCriteria.AcademicPrograms;
            SeekerStatusControl.SelectedValues = (int)seekerMatchCriteria.SeekerStatuses;
            ProgramLengthControl.SelectedValues =(int)seekerMatchCriteria.ProgramLengths;
            CollegesControlDialogButton.Keys = seekerMatchCriteria.Colleges.CommaSeparatedIds();
            FirstGenerationControl.Checked = seekerMatchCriteria.FirstGeneration;
            EthnicityControl.SelectedValues = seekerMatchCriteria.Ethnicities.Cast<ILookup>().ToList();
            ReligionControl.SelectedValues = seekerMatchCriteria.Religions.Cast<ILookup>().ToList();
            GendersControl.SelectedValues = (int)seekerMatchCriteria.Genders;
            StateControlDialogButton.Keys = seekerMatchCriteria.States.CommaSeparatedIds();
            CountyControlDialogButton.Keys = seekerMatchCriteria.Counties.CommaSeparatedIds();
            CityControlDialogButton.Keys = seekerMatchCriteria.Cities.CommaSeparatedIds();
            SchoolDistrictControlDialogButton.Keys = seekerMatchCriteria.SchoolDistricts.CommaSeparatedIds();
            HighSchoolControlDialogButton.Keys = seekerMatchCriteria.HighSchools.CommaSeparatedIds();
            AcademicAreasControlDialogButton.Keys = seekerMatchCriteria.AcademicAreas.CommaSeparatedIds();
            CareersControlDialogButton.Keys = seekerMatchCriteria.Careers.CommaSeparatedIds();
            CommunityInvolvementCausesControlDialogButton.Keys = seekerMatchCriteria.CommunityInvolvementCauses.CommaSeparatedIds();
            OrganizationsControlDialogButton.Keys = seekerMatchCriteria.Organizations.CommaSeparatedIds();
            AffiliationTypesControlDialogButton.Keys = seekerMatchCriteria.AffiliationTypes.CommaSeparatedIds();
            SeekerHobbiesControlDialogButton.Keys = seekerMatchCriteria.Hobbies.CommaSeparatedIds();
            SportsControlDialogButton.Keys = seekerMatchCriteria.Sports.CommaSeparatedIds();
            ClubsControlDialogButton.Keys = seekerMatchCriteria.Clubs.CommaSeparatedIds();
            WorkTypeControlDialogButton.Keys = seekerMatchCriteria.WorkTypes.CommaSeparatedIds();
            WorkHoursControl.SelectedValues = seekerMatchCriteria.WorkHours.Cast<ILookup>().ToList();
            ServiceTypeControlDialogButton.Keys = seekerMatchCriteria.ServiceTypes.CommaSeparatedIds();
            ServiceHoursControl.SelectedValues = seekerMatchCriteria.ServiceHours.Cast<ILookup>().ToList();

            if (null != seekerMatchCriteria.GPA)
                GPAControl.PopulateFromRangeCondition(seekerMatchCriteria.GPA);

            if (null != seekerMatchCriteria.ClassRank)
                ClassRankControl.PopulateFromRangeCondition(seekerMatchCriteria.ClassRank);

            if (null != seekerMatchCriteria.SATScore)
            {
                SATWritingControl.PopulateFromRangeCondition(seekerMatchCriteria.SATScore.Writing);
                SATCriticalReadingControl.PopulateFromRangeCondition(seekerMatchCriteria.SATScore.CriticalReading);
                SATMathematicsControl.PopulateFromRangeCondition(seekerMatchCriteria.SATScore.Mathematics);
            }

            if (null != seekerMatchCriteria.ACTScore)
            {
                ACTEnglishControl.PopulateFromRangeCondition(seekerMatchCriteria.ACTScore.English);
                ACTMathematicsControl.PopulateFromRangeCondition(seekerMatchCriteria.ACTScore.Mathematics);
                ACTReadingControl.PopulateFromRangeCondition(seekerMatchCriteria.ACTScore.Reading);
                ACTScienceControl.PopulateFromRangeCondition(seekerMatchCriteria.ACTScore.Science);
            }
            HonorsControl.Checked = seekerMatchCriteria.Honors;
            APCreditControl.Text = 
                seekerMatchCriteria.APCreditsEarned.HasValue ?
                seekerMatchCriteria.APCreditsEarned.Value.ToString() :
                string.Empty;
            
            IBCreditControl.Text = 
                seekerMatchCriteria.IBCreditsEarned.HasValue ?
                seekerMatchCriteria.IBCreditsEarned.Value.ToString() :
                string.Empty;
        }

        private void PopulateObjects()
        {
            var seekerMatchCriteria = scholarshipInContext.SeekerMatchCriteria;
            
            PopulateList(FiveWordsContainerControl, FiveWordsControlDialogButton, seekerMatchCriteria.Words);
            PopulateList(FiveSkillsContainerControl, FiveSkillsControlDialogButton, seekerMatchCriteria.Skills);
            seekerMatchCriteria.StudentGroups = (StudentGroups) StudentGroupControl.SelectedValues;
            seekerMatchCriteria.AcademicPrograms =
                (AcademicPrograms) AcademicProgramControl.SelectedValues;
            seekerMatchCriteria.SeekerStatuses = (SeekerStatuses) SeekerStatusControl.SelectedValues;
            seekerMatchCriteria.ProgramLengths =
                (ProgramLengths) ProgramLengthControl.SelectedValues;
            PopulateList(CollegesContainerControl, CollegesControlDialogButton, seekerMatchCriteria.Colleges);
            seekerMatchCriteria.FirstGeneration = FirstGenerationControl.Checked;
            PopulateList(EthnicityContainerControl, EthnicityControl, seekerMatchCriteria.Ethnicities);
            PopulateList(ReligionContainerControl, ReligionControl, seekerMatchCriteria.Religions);
            seekerMatchCriteria.Genders = GendersContainerControl.Visible
                                              ? (Genders) GendersControl.SelectedValues
                                              : 0;
            StateControlDialogButton.PopulateListFromSelection(seekerMatchCriteria.States);
            PopulateList(CountyContainerControl, CountyControlDialogButton, seekerMatchCriteria.Counties);
            PopulateList(CityContainerControl, CityControlDialogButton, seekerMatchCriteria.Cities);
            PopulateList(SchoolDistrictContainerControl, SchoolDistrictControlDialogButton, seekerMatchCriteria.SchoolDistricts);
            PopulateList(HighSchoolContainerControl, HighSchoolControlDialogButton, seekerMatchCriteria.HighSchools);
            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, seekerMatchCriteria.AcademicAreas);
            PopulateList(CareersContainerControl, CareersControlDialogButton, seekerMatchCriteria.Careers);
            PopulateList(CommunitiyInvolvementCausesContainerControl, CommunityInvolvementCausesControlDialogButton, seekerMatchCriteria.CommunityInvolvementCauses);
            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, seekerMatchCriteria.Organizations);
            PopulateList(AffiliationTypesContainerControl, AffiliationTypesControlDialogButton, seekerMatchCriteria.AffiliationTypes);
            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, seekerMatchCriteria.Hobbies);
            PopulateList(SportsContainerControl, SportsControlDialogButton, seekerMatchCriteria.Sports);
            PopulateList(ClubsContainerControl, ClubsControlDialogButton, seekerMatchCriteria.Clubs);
            PopulateList(WorkTypeContainerControl, WorkTypeControlDialogButton, seekerMatchCriteria.WorkTypes);
            PopulateList(WorkHoursContainerControl, WorkHoursControl, seekerMatchCriteria.WorkHours);
            PopulateList(ServiceTypeContainerControl, ServiceTypeControlDialogButton, seekerMatchCriteria.ServiceTypes);
            PopulateList(ServiceHoursContainerControl, ServiceHoursControl, seekerMatchCriteria.ServiceHours);
            
            seekerMatchCriteria.GPA = GPAControl.CreateIntegerRangeCondition();
            seekerMatchCriteria.ClassRank = ClassRankControl.CreateIntegerRangeCondition();

            if (!SATContainerControl.Visible)
            {
                seekerMatchCriteria.SATScore = new SatScore();
                seekerMatchCriteria.SATScore.Writing = SATWritingControl.CreateIntegerRangeCondition();
                seekerMatchCriteria.SATScore.CriticalReading = SATCriticalReadingControl.CreateIntegerRangeCondition();
                seekerMatchCriteria.SATScore.Mathematics = SATMathematicsControl.CreateIntegerRangeCondition();
            }
            else
            {
                seekerMatchCriteria.SATScore = null;
            }

            if (!ACTContainerControl.Visible)
            {
                seekerMatchCriteria.ACTScore = new ActScore();
                seekerMatchCriteria.ACTScore.English = ACTEnglishControl.CreateIntegerRangeCondition();
                seekerMatchCriteria.ACTScore.Mathematics = ACTMathematicsControl.CreateIntegerRangeCondition();
                seekerMatchCriteria.ACTScore.Reading = ACTReadingControl.CreateIntegerRangeCondition();
                seekerMatchCriteria.ACTScore.Science = ACTScienceControl.CreateIntegerRangeCondition();
            }
            else
            {
                seekerMatchCriteria.ACTScore = null;
            }
            seekerMatchCriteria.Honors = HonorsControl.Checked;
            if (string.IsNullOrEmpty(APCreditControl.Text))
                seekerMatchCriteria.APCreditsEarned = null;
            else
                seekerMatchCriteria.APCreditsEarned = Int32.Parse(APCreditControl.Text);

            if (string.IsNullOrEmpty(IBCreditControl.Text))
                seekerMatchCriteria.IBCreditsEarned = null;
            else
                seekerMatchCriteria.IBCreditsEarned = Int32.Parse(IBCreditControl.Text);

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage <ScholarshipStages.SeekerProfile)
                scholarshipInContext.Stage = ScholarshipStages.SeekerProfile;
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.SeekerProfile;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.SeekerProfile;
        }

        public override int StepIndex
        {
            get { return (int)ScholarshipStages.SeekerProfile; }
        }

        public override bool ValidateStep()
        {
            bool result = true;
            result &= Validate(FiveWordsContainerControl, FiveWordsControlDialogButton);

            return result;
        }
        #endregion

        private static bool Validate(PlaceHolder applicabilityControl, LookupDialog dialogButton)
        {
            return true;
        }

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.GoPrior();
        }
    }
}