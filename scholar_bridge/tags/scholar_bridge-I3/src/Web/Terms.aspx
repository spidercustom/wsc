﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Terms.aspx.cs" Inherits="ScholarBridge.Web.Terms" Title="Terms & Conditions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<h2>Terms &amp; Conditions</h2>

<ul>
    <li><asp:HyperLink ID="seekerTermsLnk" runat="server" NavigateUrl="~/Seeker/Terms.aspx">Seeker Terms &amp; Conditions</asp:HyperLink></li>
    <li><asp:HyperLink ID="providerTermsLnk" runat="server" NavigateUrl="~/Provider/Terms.aspx">Provider Terms &amp; Conditions</asp:HyperLink></li>
    <li><asp:HyperLink ID="intermediaryTermsLnk" runat="server" NavigateUrl="~/Intermediary/Terms.aspx">Intermediary Terms &amp; Conditions</asp:HyperLink></li>
</ul>

</asp:Content>
