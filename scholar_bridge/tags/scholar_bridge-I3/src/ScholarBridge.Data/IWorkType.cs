﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IWorkTypeDAL : ILookupDAL<WorkType>
    {
        
    }
}
