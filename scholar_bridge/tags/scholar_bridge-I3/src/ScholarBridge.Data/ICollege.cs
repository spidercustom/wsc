﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ICollegeDAL : ILookupDAL<College>
    {
    }
}
