﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IAffiliationTypeDAL : ILookupDAL<AffiliationType>
    {
    }
}
