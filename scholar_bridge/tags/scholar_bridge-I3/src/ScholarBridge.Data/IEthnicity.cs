﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IEthnicityDAL : ILookupDAL<Ethnicity>
    {
    }
}
