﻿using System.Collections.Generic;
using ScholarBridge.Domain.Location;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IStateDAL : IDAL<State>
    {
        State FindByAbbreviation(string abbreviation);
        IList<State> FindAll();
    }
}