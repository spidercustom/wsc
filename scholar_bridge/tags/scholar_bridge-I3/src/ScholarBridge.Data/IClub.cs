﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IClubDAL : ILookupDAL<Club>
    {
    }
}
