﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class SchoolDistrictDAL : LookupDAL<SchoolDistrict>, ISchoolDistrictDAL
    {
    }
}
