﻿using System;
using System.Collections.Generic;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class ScholarshipDAL : AbstractDAL<Scholarship>, IScholarshipDAL
    {
        #region IScholarshipDAL Members
        public Scholarship FindById(int id)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Scholarship>();
        }

        public Scholarship FindByNameAndProvider(string name, Provider provider)
        {
            return
                CreateCriteria().Add(Restrictions.And(
                    Restrictions.Eq("Name", name),
                    Restrictions.Eq("Provider", provider)
                )).UniqueResult<Scholarship>();
        }

        public IList<Scholarship> FindByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            return CreateCriteria().Add(Restrictions.Eq("Provider", provider)).List<Scholarship>(); 
        }

        public IList<Scholarship> FindByStageAndProvider(ScholarshipStages scholarshipStage,Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            return CreateCriteria().Add(Restrictions.And(
                                            Restrictions.Eq("Stage", scholarshipStage),
                                            Restrictions.Eq("Provider", provider)
                                            )).List<Scholarship>();
        }

        public Scholarship Save(Scholarship scholarship)
        {
            if (scholarship == null)
                throw new ArgumentNullException("scholarship");

            return scholarship.Id < 1 ? 
                Insert(scholarship) : 
                Update(scholarship) ;
        }

        #endregion
    }
}
