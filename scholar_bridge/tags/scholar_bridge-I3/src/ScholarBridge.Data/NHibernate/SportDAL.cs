﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class SportDAL : LookupDAL<Sport>, ISportDAL
    {
    }
}
