﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ITermOfSupportDAL : IGenericLookupDAL<TermOfSupport>
    {
    }
}