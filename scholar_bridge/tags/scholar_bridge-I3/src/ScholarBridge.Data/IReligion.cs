﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IReligionDAL : ILookupDAL<Religion>
    {
    }
}
