﻿
namespace ScholarBridge.Common.DayOfYear
{
    public enum WeekDayOccurrencesInMonth
    {
        [DisplayName("1st")]
        First,
        [DisplayName("2nd")]
        Second,
        [DisplayName("3rd")]
        Third,
        [DisplayName("4th")]
        Fourth,
        Last
    }
}
