﻿
using System;

namespace ScholarBridge.Common.DayOfYear
{
    public abstract class RelativeDayOfYear : ICloneable
    {
        public virtual object Clone()
        {
            return MemberwiseClone();
        }
    }
}
