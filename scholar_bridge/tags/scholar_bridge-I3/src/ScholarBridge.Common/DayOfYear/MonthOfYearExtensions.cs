﻿namespace ScholarBridge.Common.DayOfYear
{
    
    public static class MonthOfYearExtensions
    {
        //Oh, in extension methods how can I have static extension method?
        public static object[] GetMonthsHavingThirtyOneDays()
        {
            return new object[] {
                MonthOfYear.January,
                MonthOfYear.March,
                MonthOfYear.May,
                MonthOfYear.July,
                MonthOfYear.August,
                MonthOfYear.October,
                MonthOfYear.December
            };
        }

        public static object[] GetMonthsHavingThirtyDays()
        {
            return new object[] {
                MonthOfYear.April,
                MonthOfYear.June,
                MonthOfYear.September,
                MonthOfYear.November
            };
        }
    }
}
