﻿using System;

namespace ScholarBridge.Common.DayOfYear
{
    // TODO: Move to common
    public static class WeekDayOccurrenceInMonthExtensions
    {
        public static string ToNumericValueString(this WeekDayOccurrencesInMonth value)
        {
            switch (value)
            {
                case WeekDayOccurrencesInMonth.First:
                    return "1st";
                case WeekDayOccurrencesInMonth.Second:
                    return "2st";
                case WeekDayOccurrencesInMonth.Third:
                    return "3st";
                case WeekDayOccurrencesInMonth.Fourth:
                    return "4st";
                case WeekDayOccurrencesInMonth.Last:
                    return "Last";
                default:
                    throw new NotSupportedException(string.Format("enum value {0} not supported", value.ToString()));
            }
        }
    }
}
