﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain
{
    public class RangeCondition<T>
    {
        public virtual T Minimum { get; set; }
        [PropertyComparisonValidator("Maximum", ComparisonOperator.GreaterThanEqual)]
        public virtual T Maximum { get; set; }
    }
}
