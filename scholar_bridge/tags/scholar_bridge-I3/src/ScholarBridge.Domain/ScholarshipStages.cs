﻿
namespace ScholarBridge.Domain
{
    public enum ScholarshipStages
    {
        None,
        GeneralInformation,
        MatchCriteriaSelection,
        SeekerProfile,
        FundingProfile,
		AdditionalCriteria,
        PreviewCandidatePopulation,
        BuildScholarshipMatchLogic,
        BuildScholarshipRenewalCriteria,
        RequestedActivation,
        Activated,
        Rejected,
        Awarded
    }
}
