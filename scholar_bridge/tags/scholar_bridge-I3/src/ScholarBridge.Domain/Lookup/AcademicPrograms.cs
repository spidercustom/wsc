﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum AcademicPrograms
    {
        [DisplayName("Undergraduate")]
        Undergraduate = 1,
        [DisplayName("Advanced Degree")]
        AdvancedDegree = 2,
        [DisplayName("Continuing Ed/Non-Degree Seeking")]
        ContinuingEdNonDegreeSeeking = 4
    }
}
