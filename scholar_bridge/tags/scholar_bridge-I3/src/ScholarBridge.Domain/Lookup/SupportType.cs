using System;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SupportType
    {
        None,
        Emergency,
        Traditional
    }
}