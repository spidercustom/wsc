﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public enum MatchCriteriaAttribute
    {
        [DisplayName("Academic Area")]
        AcademicArea,
        [DisplayName("Academic Program")]
        AcademicProgram,
        [DisplayName("Affiliation Type")]
        AffiliationType,
        Career,
        City,
        Club,
        College,
        [DisplayName("First Generation")]
        FirstGeneration,
        [DisplayName("Cause/Community Involvement")]
        CommunityInvolvementCause,
        County,
        Ethnicity,
        Gender,
        State,
        HighSchool,
        Organization,
        [DisplayName("Program Length")]
        ProgramLength,
        Religion,
        [DisplayName("School District")]
        SchoolDistrict,
        [DisplayName("School Type")]
        SchoolType,
        [DisplayName("Hobby")]
        SeekerHobby,
        [DisplayName("Skill")]
        SeekerSkill,
        [DisplayName("Seeker Status")]
        SeekerStatus,
        [DisplayName("Words")]
        SeekerVerbalizingWord,
        [DisplayName("Service Hour")]
        ServiceHour,
        [DisplayName("Service Type")]
        ServiceType,
        Sport,
        [DisplayName("Student Group")]
        StudentGroup,
        [DisplayName("Work Hour")]
        WorkHour,
        [DisplayName("Work Type")]
        WorkType,
        GPA,
        [DisplayName("Class Rank")]
        ClassRank,
        [DisplayName("SAT Score")]
        SAT,
        [DisplayName("ACT Score")]
        ACT,
        Honor,
        [DisplayName("AP Credits Earned")]
        APCreditsEarned,
        [DisplayName("IB Credits Earned")]
        IBCreditsEarned
    }
}
