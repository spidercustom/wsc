﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ActScore
    {
        public ActScore()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            English = new RangeCondition<int>();
            Mathematics = new RangeCondition<int>();
            Reading = new RangeCondition<int>();
            Science = new RangeCondition<int>();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual RangeCondition<int> English { get; set; }
        public virtual RangeCondition<int> Mathematics { get; set; }
        public virtual RangeCondition<int> Reading { get; set; }
        public virtual RangeCondition<int> Science { get; set; }
    }
}
