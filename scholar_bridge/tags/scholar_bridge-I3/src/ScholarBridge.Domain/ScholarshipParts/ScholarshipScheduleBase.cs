﻿using System;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public abstract class ScholarshipScheduleBase : ICloneable
    {
        public virtual int Id { get; set; }
        //TODO: Find out better way to move start, due and award here
        public abstract string StartFromDisplayString { get; }
        public abstract string DueOnDisplayString { get; }
        public abstract string AwardOnDisplayString { get; }

        public virtual object Clone()
        {
            var result = (ScholarshipScheduleBase)MemberwiseClone();
            result.Id = 0;
            return result;
        }
    }
}
