﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public class SatScore
    {
        public SatScore()
        {
            Writing = new RangeCondition<int>();
            CriticalReading = new RangeCondition<int>();
            Mathematics = new RangeCondition<int>();
        }

        public virtual RangeCondition<int> Writing { get; set; }
        public virtual RangeCondition<int> CriticalReading { get; set; }
        public virtual RangeCondition<int> Mathematics { get; set; }
    }
}
