using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingParameters
    {
        public FundingParameters()
        {
            TermsOfSupport = new List<TermOfSupport>();
        }

        public virtual decimal AnnualSupportAmount { get; set; }

        public virtual int MinimumNumberOfAwards { get; set; }

        [PropertyComparisonValidator("MinimumNumberOfAwards", ComparisonOperator.GreaterThan)]
        public virtual int MaximumNumberOfAwards { get; set; }

        public virtual IList<TermOfSupport> TermsOfSupport { get; protected set; }

        public virtual void ResetTermsOfSupport(IList<TermOfSupport> terms)
        {
            TermsOfSupport.Clear();
            foreach (var ar in terms)
            {
                TermsOfSupport.Add(ar);
            }
        }
    }
}