﻿
using System.Web;
using System.Web.Security;

namespace ScholarBridge.Web
{
	public class SBXmlSiteMapProvider : XmlSiteMapProvider
	{
		public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
		{
			if (node.Roles.Count == 0)
				return true;

			foreach (string role in node.Roles)
			{

				if (role == "" || role == "*")

					return true;

				if (Roles.IsUserInRole(role))

					return true;

			}

			return false;

		}
	}
}
