﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class MatchCriteriaTest
    {
        private MatchCriteriaAttributeUsage CreateMinimumUsageTypeAttribute(MatchCriteriaAttribute attribute)
        {
            return new MatchCriteriaAttributeUsage
            {
                Attribute = attribute,
                UsageType = ScholarshipAttributeUsageType.Minimum
            };
        }

        [Test]
        public void test_has_attribute()
        {
            var criteria = new SeekerMatchCriteria();
            Assert.IsFalse(criteria.HasAttributeUsage(MatchCriteriaAttribute.AcademicArea));
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(MatchCriteriaAttribute.AcademicArea));

            Assert.IsTrue(criteria.HasAttributeUsage(MatchCriteriaAttribute.AcademicArea));
        }

        public void test_clone_implimentation_exists()
        {
            var criteria = new SeekerMatchCriteria();
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(MatchCriteriaAttribute.AcademicArea));

            var clonedCriteria = (SeekerMatchCriteria)criteria.Clone();
            Assert.AreEqual(criteria.AttributesUsage.Count, clonedCriteria.AttributesUsage.Count);
            Assert.AreEqual(criteria.AttributesUsage[0].Attribute, clonedCriteria.AttributesUsage[0].Attribute);
            Assert.AreEqual(criteria.AttributesUsage[0].UsageType, clonedCriteria.AttributesUsage[0].UsageType);
        }
    }
}
