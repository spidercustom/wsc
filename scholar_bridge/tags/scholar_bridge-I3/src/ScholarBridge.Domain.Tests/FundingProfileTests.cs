﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class FundingProfileTests
    {

        private FundingProfileAttributeUsage CreateMinimumUsageTypeAttribute(FundingProfileAttribute attribute)
        {
            return new FundingProfileAttributeUsage
            {
                Attribute = attribute,
                UsageType = ScholarshipAttributeUsageType.Minimum
            };
        }

        [Test]
        public void test_has_attribute()
        {
            var fundingProfile = new FundingProfile();
            Assert.IsFalse(fundingProfile.HasAttributeUsage(FundingProfileAttribute.FundingParameters));
            fundingProfile.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.FundingParameters));

            Assert.IsTrue(fundingProfile.HasAttributeUsage(FundingProfileAttribute.FundingParameters));
        }

        public void test_clone_implimentation_exists()
        {
            var fundingProfile = new FundingProfile();
            fundingProfile.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.FundingParameters));

            var clonedCriteria = (FundingProfile)fundingProfile.Clone();
            Assert.AreEqual(fundingProfile.AttributesUsage.Count, clonedCriteria.AttributesUsage.Count);
            Assert.AreEqual(fundingProfile.AttributesUsage[0].Attribute, clonedCriteria.AttributesUsage[0].Attribute);
            Assert.AreEqual(fundingProfile.AttributesUsage[0].UsageType, clonedCriteria.AttributesUsage[0].UsageType);
        }

        
    }
}
