using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class ScholarshipAssociationsTests : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        public AdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
        public SupportDAL SupportDAL { get; set; }
        public LookupDAL<TermOfSupport> TermOfSupportDAL { get; set; }

        [Test]
        public void can_associate_additional_reqs_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        

        [Test]
        public void can_associate_support_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.FundingProfile.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void can_set_and_retrieve_term_of_support()
        {
            Scholarship s = BuildScholarship();
            var academicYearSupport = TermOfSupportDAL.FindById(TermOfSupport.ACADEMIC_YEAR_ID);
            Assert.IsNotNull(academicYearSupport, "Could not retrieve term of support 'academic year' for testing");
            
            s.FundingParameters.TermOfSupport = academicYearSupport;

            ScholarshipDAL.Update(s);

            var retrievedScholarship = ScholarshipDAL.FindById(s.Id);
            Assert.IsNotNull(retrievedScholarship.FundingParameters.TermOfSupport);
            Assert.AreEqual(TermOfSupport.ACADEMIC_YEAR_ID, retrievedScholarship.FundingParameters.TermOfSupport.Id);
        }

        private Scholarship BuildScholarship()
        {
        	var uList = UserDAL.FindAll(0, 1);
        	var u = uList[0];

            var p = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "test", u);
            var s = InsertScholarship(ScholarshipDAL, p, u);

            Assert.IsNotNull(s);
            return s;
        }

        public static Scholarship InsertScholarship(ScholarshipDAL scholarshipDal, Provider provider, User user)
        {
            var s = new Scholarship
                                     {
                                         Name = "Test Scholarship 1",
                                         AcademicYear = new AcademicYear(2009),
                                         MissionStatement = @"Long big text to describe mission",
                                         Provider = provider,
                                         ApplicationStartDate = DateTime.Today,
                                         ApplicationDueDate = DateTime.Today.AddMonths(1),
                                         AwardDate = DateTime.Today.AddMonths(2),
                                         LastUpdate = new ActivityStamp(user),
                                         Stage = ScholarshipStage.NotActivated
                                     };

            return scholarshipDal.Save(s);
        }
    }
}