﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace ScholarBridge.Web.Adapters
{
    public class ValidatorAdapter: WebControlAdapter
    {
        protected override void RenderContents(HtmlTextWriter writer)
        {
            var validator  = Control as BaseValidator;
            if ( validator.IsValid==false) 
            {
                var group=validator.ControlToValidate.Length > 0 ? validator.ControlToValidate : validator.ClientID + "_group";
                var errormessage = validator.ErrorMessage.Length > 0 ? validator.ErrorMessage : validator.Text;
                var message = HttpUtility.HtmlEncode(errormessage + "<br/><br/>");
                string s ="<img group=\"" + group + "\" contentdivid=\"\" class=\"coolTipError\" id=\"error_" + Control.ClientID + "\" header=\"This field has following validation error(s)\" content=\"* " +
                    message + "\" src=\"" + Control.Page.ResolveClientUrl("~/Images/cooltipicon_error.png") + "\" />";

                writer.Write(s);

            } 
        }
    }
}
