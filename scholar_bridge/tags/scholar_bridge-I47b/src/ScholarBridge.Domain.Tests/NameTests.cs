using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class NameTests
    {
        
        [Test]
        public void FormatNameFirstLast()
        {
            var name = new PersonName {FirstName = "Geoff", LastName = "Lane"};
            Assert.AreEqual("Geoff Lane", name.NameFirstLast);
        }

        [Test]
        public void FormatNameFirstLastWithMiddle()
        {
            var name = new PersonName { FirstName = "Geoff", LastName = "Lane", MiddleName = "M"};
            Assert.AreEqual("Geoff M Lane", name.NameFirstLast);
        }

        [Test]
        public void FormatNameLastFirst()
        {
            var name = new PersonName { FirstName = "Geoff", LastName = "Lane" };
            Assert.AreEqual("Lane, Geoff", name.NameLastFirst);
        }

        [Test]
        public void FormatNameLastFirstWithMiddle()
        {
            var name = new PersonName { FirstName = "Geoff", LastName = "Lane", MiddleName = "M" };
            Assert.AreEqual("Lane, Geoff M", name.NameLastFirst);
        }

        [Test]
        public void validation_fill_name_as_required()
        {
            var name = new PersonName();
            Assert.That(name.ValidateAsRequired().IsValid, Is.False);
            name.FirstName = "First-name";
            name.MiddleName = string.Empty;
            name.LastName = "Last-name";
            Assert.That(name.ValidateAsRequired().IsValid, Is.True);
        }

        [Test]
        public void validation_unfill_name_as_required()
        {
            var name = new PersonName();
            var result = name.ValidateAsRequired();
            Assert.That(result.IsValid, Is.False);
            Assert.That(result.Count, Is.EqualTo(3));
        }
    }
}