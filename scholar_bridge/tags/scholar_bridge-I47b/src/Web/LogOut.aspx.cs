﻿using System;
 
using System.Web.Security;

namespace ScholarBridge.Web
{
    public partial class LogOut : System.Web.UI.Page
    {
       //created of Jquery Idle Timeout Plugin
        protected void Page_Load(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
         
        }

         
    }
}
