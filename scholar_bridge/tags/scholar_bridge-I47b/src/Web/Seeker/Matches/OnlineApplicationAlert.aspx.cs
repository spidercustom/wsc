﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Seeker.Matches
{
    public partial class OnlineApplicationAlert : Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
        }
    }
}
