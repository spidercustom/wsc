﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Design;

namespace ScholarBridge.Web.Common
{
    /// <summary>
    /// ListView Inherited Custom Control for sorting of column headers.
    /// </summary>
    [
        ToolboxData("<{0}:ListViewSort runat=server></{0}:ListViewSort>"),
        ParseChildren(true),
        PersistChildren(true)
    ]
    public class SortableListView : System.Web.UI.WebControls.ListView
    {
        #region Private Properties

        // ViewState veriables
         private const string _SortExpressionDefault = "_SortExpressionDefault";
        private const string _SortDirectionDefault = "_SortDirectionDefault";
        #endregion

        #region Public Properties
   
        /// <summary>
        /// SortExpressionDefault.
        /// </summary>
        [
        Description("Sort Expression default value."),
        Category("Extensions")
        ]
        public string SortExpressionDefault
        {
            get
            {
                return (this.ViewState[_SortExpressionDefault] == null) ? string.Empty : (string)this.ViewState[_SortExpressionDefault];
            }
            set
            {
                this.ViewState[_SortExpressionDefault] = value;
            }
        }

        /// <summary>
        /// SortDirectionDefault.
        /// </summary>
        [
        Description("Sort Direction default value."),
        Category("Extensions")
        ]
        public SortDirection SortDirectionDefault
        {
            get
            {
                return (this.ViewState[_SortDirectionDefault] == null) ? SortDirection.Ascending : (SortDirection)this.ViewState[_SortDirectionDefault];
            }
            set
            {
                this.ViewState[_SortDirectionDefault] = value;
            }
        }
        #endregion

        #region Protected Event Handlers
        protected override void OnDataBound(EventArgs e)
        {
            if (base.SortExpression.Length == 0 && SortExpressionDefault.Length > 0)
            {
                base.Sort(SortExpressionDefault, SortDirectionDefault);
            }

            var controls = ControlHelper.GetControlsByType(this, typeof(SortableListViewColumnHeader));
            foreach (SortableListViewColumnHeader header in controls)
            {
                  header.ResetSortDirectionIndicator();
                if (header.Key == base.SortExpression)
                    header.SetSortDirectionIndicator(base.SortExpression, base.SortDirection);
            }
            base.OnDataBound(e);
        }
        #endregion
    }
}