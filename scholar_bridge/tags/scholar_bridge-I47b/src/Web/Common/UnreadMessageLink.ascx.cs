﻿using System;
using System.Web.UI;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Common
{
    public partial class UnreadMessageLink : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService{get;set;}
        public int MessageCount { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (UserContext.CurrentUser != null)
            {
                MessageCount = MessageService.CountUnread(UserContext.CurrentUser, UserContext.CurrentOrganization);
                
                lblCount.Text = string.Format("{0} New Message(s)", MessageCount);
            }
            if (MessageCount == 0)
                Visible = false;
        }
    }
}

