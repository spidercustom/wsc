﻿using System;
using ScholarBridge.Domain;


namespace ScholarBridge.Web
{
	public partial class Help : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			generalFaqList.FaqType = FaqType.General;
			seekerFaqList.FaqType = FaqType.Seeker;
			providerFaqList.FaqType = FaqType.Provider;
		}
	}
}
