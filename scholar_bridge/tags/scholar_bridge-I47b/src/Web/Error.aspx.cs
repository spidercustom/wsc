﻿using System;
using System.Web;
using Common.Logging;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web
{
    public partial class Error : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Error));

        protected void Page_Load(object sender, EventArgs e)
        {
            var mainEx = HttpContext.Current.Server.GetLastError();
            if (null != mainEx)
            {
				log.Error(String.Format("UserID: {0} :: BROWSER: {2} :: URL: {1} \n\nUnhandled Exception:\n{3}", User.Identity.Name, Request.Url, Request.Browser.Type, mainEx));

				// check web.config to see if we should show the full error text.
            	string showMessageAppSetting = ConfigHelper.GetAppSettingValue("ShowErrorText");
				if (!string.IsNullOrEmpty(showMessageAppSetting))
				{
					bool showMessage;
					bool ok = Boolean.TryParse(showMessageAppSetting, out showMessage);
					if (ok && showMessage)
					{
						var ex = mainEx.GetBaseException();

						errorMessage.InnerText = ex.Message;
						errorMessage.InnerText += "\n" + ex.StackTrace;
					}
				}
            }
        }
    }
}
