﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticleList.ascx.cs" 
Inherits="ScholarBridge.Web.PressRoom.ArticleList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<asp:ListView ID="lstArticles" runat="server" 
    OnItemDataBound="lstArticles_ItemDataBound" 
    onpagepropertieschanging="lstArticles_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th style="width:80px">Date</th>
                <th>Title</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><%# DataBinder.Eval(Container.DataItem, "PostedDate", "{0:d}")%></td>
        <td><asp:HyperLink id="linktoArticle" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><%# DataBinder.Eval(Container.DataItem, "PostedDate", "{0:d}")%></td>
        <td><asp:HyperLink id="linktoArticle" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are noArticles, </p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="lstArticles" PageSize="10" onprerender="pager_PreRender" >
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
</div> 
