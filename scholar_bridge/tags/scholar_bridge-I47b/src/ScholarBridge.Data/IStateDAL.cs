﻿using System.Collections.Generic;
using ScholarBridge.Domain.Location;

namespace ScholarBridge.Data
{
    public interface IStateDAL : IDAL<State>
    {
        State FindByAbbreviation(string abbreviation);
        IList<State> FindAll();
    }
}