using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.NHibernate
{
    public class ProviderDAL : AbstractDAL<Provider>, IProviderDAL
    {
		public IList<Provider> FindAllPending()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("ApprovalStatus", ApprovalStatus.PendingApproval))
				.List<Provider>();
		}

		/// <summary>
		/// Return all approved providers
		/// </summary>
		/// <returns></returns>
		public IList<Provider> FindAllActive()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("ApprovalStatus", ApprovalStatus.Approved))
				.List<Provider>();
		}

        public Provider FindByUser(User user)
        {
            return CreateCriteria()
                .CreateCriteria("Users")
                .Add(Restrictions.Eq("Id", user.Id))
                .SetCacheable(true)
                .UniqueResult<Provider>();
        }

        public User FindUserInOrg(Provider organization, int userId)
        {
            // XXX: Couldn't figure out how to do this with Criteria
            var query = Session.CreateQuery(@"select user from Provider as provider
inner join provider.Users as user
where provider.Id=:providerId AND user.id=:userId");

            query.SetInt32("providerId", organization.Id);
            query.SetInt32("userId", userId);
            return query.UniqueResult<User>();
        }
    }
}