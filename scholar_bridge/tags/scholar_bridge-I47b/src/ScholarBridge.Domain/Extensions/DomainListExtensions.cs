using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.Extensions
{
    public static class DomainListExtensions
    {
        public static IList<string> ItemNames<T>(this IList<T> items) where T : ILookup
        {
            return items.Select(i => i.Name).ToList();
        }

        public static IList<string> ItemIds<T>(this IList<T> items) where T : ILookup
        {
            return items.Select(i => i.Id.ToString()).ToList();
        }

        public static string CommaSeparatedNames<T>(this IList<T> items) where T : ILookup
        {
            return String.Join(", ", items.ItemNames().ToArray());
        }

        public static string CommaSeparatedIds<T>(this IList<T> items) where T : ILookup
        {
            return String.Join(", ", items.ItemIds().ToArray());
        }
    }
}