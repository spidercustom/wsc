﻿using System;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.ApplicationParts
{
    public class ApplicationQuestionAnswer : ICloneable
    {
        public virtual int Id { get; set; }
        public virtual ScholarshipQuestion Question { get; set; }
        public virtual Application Application { get; set; }
        public virtual string AnswerText { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }
    
        #region ICloneable Members

        object ICloneable.Clone()
        {
            return MemberwiseClone();
        }

        #endregion
    }
}