﻿using System;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum Genders
    {
        // Zero won't work because zero is none and you can't tell between that
        // and another value. 0 + 1 == 1
		Unspecified = 1,
        Male = 2,
        Female = 4,
        Undisclosed = 10
    }
}
