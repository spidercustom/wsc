﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    /// <summary>
    /// MatchApplicationStatus shows the current state of a given Seeker Match
    /// Note! MatchApplicationStatus and ApplicationStatus are closely tied.
    /// </summary>
	public enum MatchApplicationStatus
	{
		Unknown,

		[DisplayName("Not Started")]
		NotApplied,

		[DisplayName("Started")]
		Applying,

		[DisplayName("Submitted")]
		Applied,

		[DisplayName("Being considered")]
		BeingConsidered,

		Offered,
		Closed,
		Awarded
	}
}
