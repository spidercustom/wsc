using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation;

namespace ScholarBridge.Domain.Contact
{
     
    public class Address : AddressBase
    {
		[StringLengthValidator(1, 50, MessageTemplate = "City must be specified and must be less than 50 characters.", Ruleset = ADDRESS_REQUIRED)]
		[StringLengthValidator(0, 50, MessageTemplate = "City must be less than 50 characters.")]
		public virtual string City { get; set; }

        public override string GetCity
        {
            get { return City; }
        }

        public override ValidationResults ValidateAsRequired()
        {
            var validator = ValidationFactory.CreateValidator<Address>(ADDRESS_REQUIRED);
            return validator.Validate(this);
        }
    }
}