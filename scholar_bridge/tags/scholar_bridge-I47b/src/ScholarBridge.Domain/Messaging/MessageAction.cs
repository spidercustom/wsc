namespace ScholarBridge.Domain.Messaging
{
    [System.Serializable]
    public enum MessageAction
    {
        None,
        Approve,
        Deny
    }
}