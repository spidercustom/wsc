﻿using System;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(259)]
	public class AddAdditionalSeekerHobbies : Migration
	{
		private string LOOKUP_TABLE = "SBSeekerHobbyLUT";

		private string[] COLUMNS = new []
		                           	{
		                           		"SeekerHobby", "Description", "Deprecated", "LastUpdateBy", "LastUpdateDate"
		                           	};

		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			foreach (var value in LOOKUP_VALUES)
			{
				AddLookup(value, adminId);	
			}

		}

		public override void Down()
		{
			foreach (var value in LOOKUP_VALUES)
			{
				try
				{
					Database.Delete(LOOKUP_TABLE, COLUMNS[0], value);
				}
				catch (Exception)
				{
					// do nothing in-case the lookup value is already attached 
				}
			}
		}

		private void AddLookup(string lookupValue, int adminId)
		{
			int hitCount = (int)Database.ExecuteScalar("select count(*) from " + LOOKUP_TABLE + " where " + COLUMNS[0] + " = '" + lookupValue + "'");
			if (hitCount == 0)
				Database.Insert(LOOKUP_TABLE, COLUMNS,
								new[] { lookupValue, lookupValue, "0", adminId.ToString(), DateTime.Now.ToString() }
					);
		}

		private string[] LOOKUP_VALUES = new []
		                                	{
												"Animal breeding/Animal Rescue",
												"Astronomy",
												"Beading/Jewelry Making",
												"Bird watching",
												"Carpentry/Furniture Making",
												"Collecting Objects",
												"Computers",
												"Cooking",
												"Crochet/Embroidery/Knitting/Sewing/Quilting",
												"Drawing/Calligraphy",
												"Engraving/Woodcarving",
												"Experiments at home/Kitchen Chemistry",
												"Fitness/Exercise",
												"Literature/Reading",
												"Microscopy",
												"Outdoor activities",
												"Painting/Enamels/Pottery/Sculpture",
												"Performing Arts",
												"Photography",
												"Robotics",
												"Scrapbooking/Collages/Paper Arts",
												"Sports",
												"Stained glass",
												"Travel"
		                                	};
	}
}
