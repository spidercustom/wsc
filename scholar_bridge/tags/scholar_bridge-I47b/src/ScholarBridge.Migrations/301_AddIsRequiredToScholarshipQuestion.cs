﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(301)]
    public class AddIsRequiredToScholarshipQuestion : Migration
    {
        private const string TABLE_NAME = "SBScholarshipQuestion";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("IsRequired", DbType.Boolean,ColumnProperty.NotNull,false));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "IsRequired");
        }
    }
}