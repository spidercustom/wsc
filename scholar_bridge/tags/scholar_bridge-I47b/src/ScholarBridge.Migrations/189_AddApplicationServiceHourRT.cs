using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(189)]
    public class AddApplicationServiceHourRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationServiceHourRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBServiceHourLUT"; }
        }
    }
}