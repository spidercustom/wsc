﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(313)]
    public class AddComputeApplicationStatus : Migration
    {
        public override void Up()
        {
        	DropFunction();
			Database.ExecuteNonQuery(APPLICATION_STATUS_FUNCTION);

			 
		}

		public override void Down()
		{
		 
			DropFunction();
		}

		private void DropFunction()
		{
            Database.ExecuteNonQuery("if exists (select * from sys.objects where name = 'ComputeApplicationStatus') drop function dbo.ComputeApplicationStatus");
		}

		 

		#region Applciation Status Function

        public const string APPLICATION_STATUS_FUNCTION =
            @"
-- =============================================
-- Author:		Bhupindra Singh
-- Create date: 2/16/2010
-- Description:	compute an application status
-- =============================================
create FUNCTION [dbo].[ComputeApplicationStatus]
(
	@ApplicationId int
	
	
)
RETURNS varchar(20)
AS
BEGIN

	declare @ApplicationDueDate datetime,
	@ApplicationStartDate datetime,
	@ScholarshipStage   varchar(20),
	@ApplicationStage   varchar(20),
	@Result varchar(25),
	@ScholarshipAwardPeriodClosed datetime,
	@ApplicationType  varchar(20),
	@ScholarshipId int
	 
	
	SET @Result='UnKnown'
	
	SELECT @ApplicationType = ApplicationType,
	@ScholarshipId =ScholarshipId,
	 @ApplicationStage =Stage
	FROM [dbo].SBApplication
	 
    WHERE
          SBApplicationId=@ApplicationId
	
	IF  (@ApplicationType='External' )
	BEGIN
		SET @Result ='ApplyatProviderSite' 
	END
	ELSE
	BEGIN 
		SELECT @ApplicationDueDate = ApplicationDueDate,
		@ApplicationStartDate=ApplicationStartDate,
		@ScholarshipStage =Stage,
		@ScholarshipAwardPeriodClosed=AwardPeriodClosed 
		
		FROM [dbo].SBScholarship
		WHERE SBScholarshipID=@ScholarshipId
		
		SET @ApplicationDueDate  =DateAdd(day,1, @ApplicationDueDate)
		SET @ApplicationDueDate  =DateAdd(second,-1, @ApplicationDueDate)
		IF (@ScholarshipStage = 'Awarded' )
			SET @Result='Closed'
		ELSE IF (@ApplicationStage = 'Submitted' )
		BEGIN
			 
			 IF (Getdate() < @ApplicationStartDate)
			   SET @Result='Submitted'
			 ELSE IF (Getdate() BETWEEN @ApplicationStartDate AND  @ApplicationDueDate)
				SET @Result='Submitted'
			 ELSE IF (Getdate() >=@ApplicationDueDate)
				SET @Result='BeingConsidered'
			 ELSE IF (IsDate(@ScholarshipAwardPeriodClosed)=1)
				SET @Result='BeingConsidered'
		END
		ELSE 
		BEGIN
			IF (Getdate() < @ApplicationStartDate)
				   SET @Result='Applying'
			ELSE IF (Getdate() BETWEEN @ApplicationStartDate AND  @ApplicationDueDate)
				SET @Result='Applying'
			ELSE IF (Getdate() >=@ApplicationDueDate)
				SET @Result='Closed'
			 ELSE IF (IsDate(@ScholarshipAwardPeriodClosed)=1)
				SET @Result='Closed'
		END
		
		
		
	END
	return @Result ;

END";
		#endregion

		 

	}
}