﻿
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(309)]
	public class DropAffiliationTypeRT : Migration
	{
		private const string OTHER_FIELD_NAME = "AffiliationTypeOther";
		public override void Up()
		{
			Database.RemoveTable("SBApplicationAffiliationTypeRT");
			Database.RemoveTable("SBScholarshipAffiliationTypeRT");
			Database.RemoveTable("SBSeekerAffiliationTypeRT");
			Database.RemoveTable("SBAffiliationTypeLUT");
			Database.RemoveColumn("SBSeeker", OTHER_FIELD_NAME);
			Database.RemoveColumn("SBApplication", OTHER_FIELD_NAME);

		}

		public override void Down()
		{
			Database.ExecuteNonQuery(LUT_BUILD);
			Database.ExecuteNonQuery(SCHOLARSHIP_RT_BUILD);
			Database.ExecuteNonQuery(APPLICATION_RT_BUILD);
			Database.ExecuteNonQuery(SEEKER_RT_BUILD);
			Database.AddColumn("SBSeeker", OTHER_FIELD_NAME, DbType.String, 250, ColumnProperty.Null);
			Database.AddColumn("SBApplication", OTHER_FIELD_NAME, DbType.String, 250, ColumnProperty.Null);
		}

		#region Rebuild LUT

		public const string LUT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBAffiliationTypeLUT')
begin

	CREATE TABLE [dbo].[SBAffiliationTypeLUT](
		[SBAffiliationTypeIndex] [int] IDENTITY(1,1) NOT NULL,
		[AffiliationType] [nvarchar](50) NOT NULL,
		[Description] [nvarchar](250) NULL,
		[Deprecated] [bit] NOT NULL,
		[LastUpdateBy] [int] NOT NULL,
		[LastUpdateDate] [datetime] NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[SBAffiliationTypeIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[SBAffiliationTypeLUT]  WITH CHECK ADD  CONSTRAINT [FK_SBAffiliationTypeLUT_LastUpdateBy] FOREIGN KEY([LastUpdateBy])
	REFERENCES [dbo].[SBUser] ([SBUserID])

	ALTER TABLE [dbo].[SBAffiliationTypeLUT] CHECK CONSTRAINT [FK_SBAffiliationTypeLUT_LastUpdateBy]

	ALTER TABLE [dbo].[SBAffiliationTypeLUT] ADD  DEFAULT ((0)) FOR [Deprecated]

	ALTER TABLE [dbo].[SBAffiliationTypeLUT] ADD  DEFAULT (getdate()) FOR [LastUpdateDate]


end
";

		#endregion

		#region Scolarship Adder
		public const string SCHOLARSHIP_RT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBScholarshipAffiliationTypeRT')
begin

	CREATE TABLE [dbo].[SBScholarshipAffiliationTypeRT](
		[SBScholarshipID] [int] NOT NULL,
		[SBAffiliationTypeIndex] [int] NOT NULL,
	 CONSTRAINT [PK_SBScholarshipAffiliationTypeRT] PRIMARY KEY CLUSTERED 
	(
		[SBScholarshipID] ASC,
		[SBAffiliationTypeIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[SBScholarshipAffiliationTypeRT]  WITH CHECK ADD  CONSTRAINT [FK_SBScholarshipAffiliationTypeRT_SBAffiliationTypeLUT] FOREIGN KEY([SBAffiliationTypeIndex])
	REFERENCES [dbo].[SBAffiliationTypeLUT] ([SBAffiliationTypeIndex])

	ALTER TABLE [dbo].[SBScholarshipAffiliationTypeRT] CHECK CONSTRAINT [FK_SBScholarshipAffiliationTypeRT_SBAffiliationTypeLUT]

	ALTER TABLE [dbo].[SBScholarshipAffiliationTypeRT]  WITH CHECK ADD  CONSTRAINT [FK_SBScholarshipAffiliationTypeRT_SBScholarship] FOREIGN KEY([SBScholarshipID])
	REFERENCES [dbo].[SBScholarship] ([SBScholarshipID])

	ALTER TABLE [dbo].[SBScholarshipAffiliationTypeRT] CHECK CONSTRAINT [FK_SBScholarshipAffiliationTypeRT_SBScholarship]


end
";
		#endregion

		#region Application AdderO
		public const string APPLICATION_RT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBApplicationAffiliationTypeRT')
begin
	CREATE TABLE [dbo].[SBApplicationAffiliationTypeRT](
		[SBApplicationID] [int] NOT NULL,
		[SBAffiliationTypeIndex] [int] NOT NULL,
	 CONSTRAINT [PK_SBApplicationAffiliationTypeRT] PRIMARY KEY CLUSTERED 
	(
		[SBApplicationID] ASC,
		[SBAffiliationTypeIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[SBApplicationAffiliationTypeRT]  WITH CHECK ADD  CONSTRAINT [FK_SBApplicationAffiliationTypeRT_SBAffiliationTypeLUT] FOREIGN KEY([SBAffiliationTypeIndex])
	REFERENCES [dbo].[SBAffiliationTypeLUT] ([SBAffiliationTypeIndex])

	ALTER TABLE [dbo].[SBApplicationAffiliationTypeRT] CHECK CONSTRAINT [FK_SBApplicationAffiliationTypeRT_SBAffiliationTypeLUT]

	ALTER TABLE [dbo].[SBApplicationAffiliationTypeRT]  WITH CHECK ADD  CONSTRAINT [FK_SBApplicationAffiliationTypeRT_SBApplication] FOREIGN KEY([SBApplicationID])
	REFERENCES [dbo].[SBApplication] ([SBApplicationId])

	ALTER TABLE [dbo].[SBApplicationAffiliationTypeRT] CHECK CONSTRAINT [FK_SBApplicationAffiliationTypeRT_SBApplication]


end
";
		#endregion

		#region SeekerAffiliationTypeRT Adder
		public const string SEEKER_RT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBSeekerAffiliationTypeRT')
begin
	CREATE TABLE [dbo].[SBSeekerAffiliationTypeRT](
		[SBSeekerID] [int] NOT NULL,
		[SBAffiliationTypeIndex] [int] NOT NULL,
	 CONSTRAINT [PK_SBSeekerAffiliationTypeRT] PRIMARY KEY CLUSTERED 
	(
		[SBSeekerID] ASC,
		[SBAffiliationTypeIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[SBSeekerAffiliationTypeRT]  WITH CHECK ADD  CONSTRAINT [FK_SBSeekerAffiliationTypeRT_SBAffiliationTypeLUT] FOREIGN KEY([SBAffiliationTypeIndex])
	REFERENCES [dbo].[SBAffiliationTypeLUT] ([SBAffiliationTypeIndex])

	ALTER TABLE [dbo].[SBSeekerAffiliationTypeRT] CHECK CONSTRAINT [FK_SBSeekerAffiliationTypeRT_SBAffiliationTypeLUT]

	ALTER TABLE [dbo].[SBSeekerAffiliationTypeRT]  WITH CHECK ADD  CONSTRAINT [FK_SBSeekerAffiliationTypeRT_SBSeeker] FOREIGN KEY([SBSeekerID])
	REFERENCES [dbo].[SBSeeker] ([SBSeekerId])

	ALTER TABLE [dbo].[SBSeekerAffiliationTypeRT] CHECK CONSTRAINT [FK_SBSeekerAffiliationTypeRT_SBSeeker]
end
";
		#endregion
	}
}
