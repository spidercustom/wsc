using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(94)]
    public class AddScholarshipQuestionsTable : AddTableBase
    {
        public const string TABLE_NAME = "SBScholarshipQuestion";
        public const string PRIMARY_KEY_COLUMN = "SBScholarshipQuestionID";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("SBScholarshipID", DbType.Int32, ColumnProperty.NotNull),
                           new Column("QuestionText", DbType.String, 200, ColumnProperty.NotNull),
                           new Column("DisplayOrder", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())")
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new[]
                       {
                           CreateForeignKey("SBScholarshipID", "SBScholarship", "SBScholarshipID"),
                           CreateForeignKey("LastUpdateBy", "SBUser", "SBUserID")
                       };
        }
    }
}