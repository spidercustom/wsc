﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(319)]
	public class UpdateMatchCreateDate : Migration
    {
        public override void Up()
        {
        	UpdateSeekerMatchCreateDate();
        }

        public override void Down()
        {
		}

		#region update SeekerMatchBuilder
		private void UpdateSeekerMatchCreateDate()
        	{
        		Database.ExecuteNonQuery(@"

update SBMatch
set CreateDate = (select DateAdd(dd, -1, ISNULL(u.lastlogindate, u.LastUpdateDate)) as createdate
					from SBUser u
					inner join SBSeeker s on s.UserId = u.SBUserID
					where s.SBSeekerId = SBMatch.SBSeekerId) 
where CreateDate is null
");
        	}
		
		#endregion

	}
}