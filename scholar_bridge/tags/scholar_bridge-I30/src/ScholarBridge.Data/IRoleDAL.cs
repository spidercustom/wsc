﻿using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IRoleDAL : IDAL<Role>
    {
        Role FindById(int id);
        Role FindByName(string name);
    }
}