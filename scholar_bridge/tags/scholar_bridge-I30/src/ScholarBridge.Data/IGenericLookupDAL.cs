﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IGenericLookupDAL<T> : IDAL<T> where T : ILookup 
    {
        IList<T> FindAll();
        T FindById(object id);
        IList<T> FindAll(IList<object> ids);
    }
}
