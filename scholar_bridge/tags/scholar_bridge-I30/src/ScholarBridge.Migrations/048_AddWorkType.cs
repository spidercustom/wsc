﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(48)]
    public class AddWorkType : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "WorkType";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
