using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(187)]
    public class AddApplicationWorkHourRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationWorkHourRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBWorkHourLUT"; }
        }
    }
}