﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(252)]
	public class AddFaqsTable : AddTableBase
	{
		public const string TABLE_NAME = "SBFaq";
		public const string PRIMARY_KEY_COLUMN = "SBFaqId";

		public override string TableName
		{
			get { return TABLE_NAME; }
		}

		public override string PrimaryKeyColumn
		{
			get { return PRIMARY_KEY_COLUMN; }
		}

		public override Column[] CreateColumns()
		{
			return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("FaqTitle", DbType.String,250, ColumnProperty.NotNull),
                           new Column("FaqURL", DbType.String,100, ColumnProperty.Null),
                           new Column("FaqDescription", DbType.String,4000, ColumnProperty.NotNull),
                           new Column("FaqOrder", DbType.Int32, ColumnProperty.NotNull),
                           new Column("SBAttachmentID", DbType.Int32, ColumnProperty.Null),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())"),
                       };
		}

		public override ForeignKey[] CreateForeignKeys()
		{
            return new []
            {
                CreateForeignKey("SBAttachmentID", "SBAttachment", "SBAttachmentID"),
            };
		}
	}
}