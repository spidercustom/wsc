﻿<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <configSections>
    <sectionGroup name="spring">
      <section name='context' type='Spring.Context.Support.ContextHandler, Spring.Core'/>
      <section name="parsers" type="Spring.Context.Support.NamespaceParsersSectionHandler, Spring.Core" />
    </sectionGroup>
    <sectionGroup name="common">
      <section name="logging" type="Common.Logging.ConfigurationSectionHandler, Common.Logging" />
    </sectionGroup>
    <section name="databaseSettings" type="System.Configuration.NameValueSectionHandler, System, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,log4net" />
  </configSections>

  <spring>
    <parsers>
      <parser type="Spring.Data.Config.DatabaseNamespaceParser, Spring.Data" />
    </parsers>

    <context>
      <resource uri="assembly://ScholarBridge.Data/ScholarBridge.Data.NHibernate/DAL.xml" />
    </context>
  </spring>

  <!-- These properties are referenced in Dao.xml -->
  <databaseSettings>
    <add key="db.datasource" value="spider-database"/>
    <add key="db.user" value="scholarBridgeUser"/>
    <add key="db.password" value="scholarBridgeUser"/>
    <add key="db.database" value="ScholarBridge"/>
  </databaseSettings>

  <common>
    <logging>
      <factoryAdapter type="Common.Logging.Log4Net.Log4NetLoggerFactoryAdapter, Common.Logging.Log4Net">
        <!-- choices are INLINE, FILE, FILE-WATCH, EXTERNAL-->
        <!-- otherwise BasicConfigurer.Configure is used   -->
        <!-- log4net configuration file is specified with key configFile-->
        <arg key="configType" value="INLINE"/>
        <arg key="configFile" value="filename"/>
      </factoryAdapter>
    </logging>
  </common>
  <appSettings>
    <add key="EmailsDirectory" value ="c:\work\projects\scholar_bridge\src\Web\Emails"/>
    <add key="HECBAdminEmail" value ="bsingh@spiderlogic.com"/>
    <add key ="ProxyURL" value =""/>
    <add key="Spring.Data.NHibernate.Support.OpenSessionInViewModule.SessionFactoryObjectName"
         value="NHibernateSessionFactory"/>
  </appSettings>

  <log4net>
    <appender name="ConsoleAppender" type="log4net.Appender.ConsoleAppender">
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5level %logger - %message%newline" />
      </layout>
    </appender>

    <!-- Set default logging level to DEBUG -->
    <root>
      <level value="DEBUG" />
      <appender-ref ref="ConsoleAppender" />
    </root>

    <!-- Set logging for Spring.  Logger names in Spring correspond to the namespace -->
    <logger name="Spring">
      <level value="INFO" />
    </logger>
    <logger name="Spring.Data">
      <level value="DEBUG" />
    </logger>
    <logger name="NHibernate">
      <level value="INFO" />
    </logger>
  </log4net>

  <runtime>

    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="NHibernate" publicKeyToken="aa95f207798dfdb4" culture="neutral"/>
        <bindingRedirect oldVersion="2.0.0.4000" newVersion="2.0.1.4000"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="nunit.framework"
                          publicKeyToken="96d09a1eb7f44a77"
                          culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-65535.65535.65535.65535" newVersion="2.4.6.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>

</configuration>