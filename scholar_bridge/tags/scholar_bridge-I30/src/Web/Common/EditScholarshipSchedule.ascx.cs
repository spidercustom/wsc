﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class EditScholarshipSchedule : UserControl
    {
        public delegate void ScheduleChanged();
        public event ScheduleChanged ScheduleSaved;
        public event FormCanceled FormCanceled;
        public IScholarshipService ScholarshipService { get; set; }
        public Scholarship ScholarshipInContext { get; set; }
        public UserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
                if (ScholarshipInContext != null)
                {
                    if (!Page.IsPostBack)
                    {
                        calApplicationStartDate.SelectedDate = ScholarshipInContext.ApplicationStartDate;
                        calApplicationDueDate.SelectedDate = ScholarshipInContext.ApplicationDueDate;
                        calAwardDate.SelectedDate = ScholarshipInContext.AwardDate;
                    }
                }
 
        }
        protected void DueDateValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {
            if (calApplicationDueDate.SelectedDate < ScholarshipInContext.ApplicationDueDate)
            {
                DueDateCustomValidator.ErrorMessage =
                    "Application due date can't be earlier than current due date. Current due date is " +
                    ScholarshipInContext.ApplicationDueDate.ToString();
                DueDateCustomValidator.IsValid = false;
                args.IsValid = false;
            }
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            

            Page.Validate();
            if (Page.IsValid)
            {
                ScholarshipInContext.ApplicationStartDate = calApplicationStartDate.SelectedDate;
                ScholarshipInContext.ApplicationDueDate = calApplicationDueDate.SelectedDate;
                ScholarshipInContext.AwardDate = calAwardDate.SelectedDate;
                ScholarshipService.Save(ScholarshipInContext);
                if (null != ScheduleSaved )
                {
                    ScheduleSaved();
                }
            }
        }
        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
		
    }
}