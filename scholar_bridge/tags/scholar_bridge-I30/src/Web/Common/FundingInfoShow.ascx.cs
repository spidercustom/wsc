﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Provider.BuildScholarship;
using ScholarBridge.Web.Extensions;
namespace ScholarBridge.Web.Common
{
    public partial class FundingInfoShow : BaseScholarshipShow
    {
        public const string FAFSA = "FAFSA";
        public const string USER_DERIVED = "User Derived";

        public override int ResumeFrom { get { return WizardStepName.FundingProfile.GetNumericValue(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);

            //view funding info
			if (ScholarshipToView.FundingProfile.HasAttributeUsage(FundingProfileAttribute.Need))
			{
                lblFinancialInfo.Text = string.Empty;
                if (ScholarshipToView.IsApplicantDemonstrateFinancialNeedRequired.HasValue)
                {
                    if (ScholarshipToView.IsApplicantDemonstrateFinancialNeedRequired.Value == true)
                        lblFinancialInfo.Text = lblFinancialInfo.Text + "Applicant Must demonstrate Financial Need is required.<br />";
                }

                lblFafsa.Text = string.Empty;
                if (ScholarshipToView.IsFAFSARequired.HasValue)
                {
                    if (ScholarshipToView.IsFAFSARequired.Value == true)
                        lblFafsa.Text = lblFafsa.Text + "FAFSA is required.<br />";
                }

                if (ScholarshipToView.IsFAFSAEFCRequired.HasValue)
                {
                    if (ScholarshipToView.IsFAFSAEFCRequired.Value == true)
                        lblFafsa.Text = lblFafsa.Text + "FAFSA defined EFC is required.<br />";
                }

			}
			else
			{
                FundingNeedsContainer.Visible = false;
			}

			
            NoSelectionContainerControl.Visible = !(FundingNeedsContainer.Visible );
            SetupAttributeUsageTypeIcons();
        }

        private void SetupAttributeUsageTypeIcons()
        {
            Domain.ScholarshipParts.FundingProfile fundingProfile = ScholarshipToView.FundingProfile;
        }

    }
}