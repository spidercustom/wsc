﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditUserEmail.ascx.cs" Inherits="ScholarBridge.Web.Common.EditUserEmail" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>            
<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>
<br />
<br />
<label for="CurrentEmail">Current Email:</label>
<asp:Label ID="CurrentEmail" runat="server" Text="" />
<br />
<br />
<label for="EmailBox">New Email:</label>
<asp:TextBox ID="EmailBox" runat="server"></asp:TextBox>
<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailBox"
                ErrorMessage="New Email address is required." ToolTip="New Email address must be entered."></asp:RequiredFieldValidator>
    <br />            
 <elv:PropertyProxyValidator ID="EmailValidator" runat="server" ControlToValidate="EmailBox" PropertyName="EmailWaitingforVerification" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
                          
<br />
<label for="ConfirmEmailBox">Confirm Email:</label>
<asp:TextBox ID="ConfirmEmailBox" runat="server"></asp:TextBox>
<asp:RequiredFieldValidator ID="ConfirmEmailRequiredValidator" runat="server" ControlToValidate="ConfirmEmailBox"
                ErrorMessage="Confirm Email address is required." ToolTip="Confirm Email address must be entered."></asp:RequiredFieldValidator>
<br />                
<asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmailBox" 
                ControlToCompare="EmailBox" ErrorMessage="Confirm Email Address must match New Email Address." />
                            
<br />
<br />
<sbCommon:AnchorButton ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click" />
<sbCommon:AnchorButton ID="cancelButton" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelButton_Click" />

 