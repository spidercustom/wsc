﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipSearchBox.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipSearchBox" %>
<asp:Panel ID="pnlSearch" runat="server" Style="width: 190px; display:inline; padding-top: 20px; padding-left: 5px;"
    DefaultButton="SearchBtn" >
 
<asp:TextBox ID="txtSearch" runat="server"  Width="120px"/>
 
<asp:ImageButton ID="SearchBtn" runat="server"  ImageUrl="~/images/BtnSearch.gif" AlternateText="Search" CssClass="SearchButton" onclick="SearchBtn_Click"/> 
</asp:Panel>

 