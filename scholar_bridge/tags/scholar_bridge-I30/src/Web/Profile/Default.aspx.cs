﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Profile
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.UserSettings); 
            editUserName.CurrentUser = UserContext.CurrentUser;
             editUserEmail.CurrentUser = UserContext.CurrentUser;
        }


        protected void editUserName_OnUserSaved(User user)
        {
            SuccessMessageLabel.SetMessage("Your information has been updated");
            Response.Redirect("~/Profile/");
        }

        protected void ChangePassword1_ChangedPassword(object sender, EventArgs e)
        {
            SuccessMessageLabel.SetMessage("Your password has been changed.");
        }
    }
}