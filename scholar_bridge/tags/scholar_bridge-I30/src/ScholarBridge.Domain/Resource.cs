﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain
{
	public class Resource
	{
		public virtual int Id { get; set; }
		[NotNullValidator]
		[StringLengthValidator(1, 250)]
		public virtual string Title { get; set; }
		[StringLengthValidator(1, 100)]
		public virtual string URL { get; set; }
		[StringLengthValidator(1, 4000)]
		public virtual string Description { get; set; }
		
		public virtual int? DisplayOrder { get; set; }

		public virtual Attachment AttachedFile { get; set; }

		public virtual ActivityStamp LastUpdate { get; set; }
	}
}