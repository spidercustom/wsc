using System.Collections.Generic;

namespace ScholarBridge.Data
{
    public interface IDAL<T>
    {
        void Delete(T entity);
        T Insert(T entity);
        T Update(T entity);
        IList<T> FindAll(string sortProperty);
        void Evict(T entity);
    }
}