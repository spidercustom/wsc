using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class ArticleDAL : AbstractDAL<Article>, IArticleDAL
    {
    }
}