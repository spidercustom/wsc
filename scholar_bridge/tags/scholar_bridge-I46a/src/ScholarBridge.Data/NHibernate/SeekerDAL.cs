using System;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.NHibernate
{
    public class SeekerDAL : AbstractDAL<Seeker>, ISeekerDAL
    {
        public Seeker FindByUser(User user)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("User", user))
                .UniqueResult<Seeker>();
        }

    	public int CountRegisteredSeekers()
    	{
			var count = (Int32)Session.CreateCriteria(typeof(Seeker))
					.CreateCriteria("User").Add(Restrictions.Eq("IsActive", true))
				.SetProjection(Projections.Count("Id")).UniqueResult();
    		return count;
			
		}

    	public int CountSeekersWithActiveProfiles()
    	{
			var count = (Int32)Session.CreateCriteria(typeof(Seeker))
                    .Add(Restrictions.IsNotNull("ProfileActivatedOn"))
				.SetProjection(Projections.Count("Id")).UniqueResult();
			return count;
		}
    }
}