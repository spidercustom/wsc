using NHibernate.Type;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>SeekerType</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class ApplicationPartsApplicationType : EnumStringType
    {
        public ApplicationPartsApplicationType()
            : base(typeof(ApplicationType), 255)
        {
        }
    }

    public class ApplicationPartsApplicationStatus : EnumStringType
    {
        public ApplicationPartsApplicationStatus()
            : base(typeof(ApplicationStatus), 255)
        {
        }
    }
}