﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(119)]
	public class ResizeLookupTableColumns : Migration
	{
		private const int NEW_ATTRIB_SIZE = 50;
		private const int OLD_ATTRIB_SIZE = 20;
		private const int NEW_DESCRIPTION_SIZE = 250;
		private const int OLD_DESCRIPTION_SIZE = 50;

		public static readonly string[] LOOKUP_TABLES_TO_RESIZE = new[]
		                                                          	{
		                                                          		"SBAcademicAreaLUT",
		                                                          		"SBAffiliationTypeLUT",
		                                                          		"SBCommunityInvolvementCauseLUT",
		                                                          		"SBCareerLUT",
		                                                          		"SBClubLUT",
		                                                          		"SBEthnicityLUT",
		                                                          		"SBReligionLUT",
		                                                          		"SBSeekerHobbyLUT",
		                                                          		"SBSeekerSkillLUT",
		                                                          		"SBSeekerMatchOrganizationLUT",
		                                                          		"SBSeekerVerbalizingWordLUT",
		                                                          		"SBServiceTypeLUT",
		                                                          		"SBSportLUT",
		                                                          		"SBWorkHourLUT"
		                                                          	};


		public override void Up()
		{

			foreach (var table in LOOKUP_TABLES_TO_RESIZE)
			{
				Column[] columns = Database.GetColumns(table);
				Column changeColumn = columns[1];
				changeColumn.Size = NEW_ATTRIB_SIZE;
				Database.ChangeColumn(table, changeColumn);
				changeColumn = columns[2];
				changeColumn.Size = NEW_DESCRIPTION_SIZE;
				Database.ChangeColumn(table, changeColumn);
			}

		}

		public override void Down()
		{
			foreach (var table in LOOKUP_TABLES_TO_RESIZE)
			{
				Column[] columns = Database.GetColumns(table);
				Column changeColumn = columns[1];
				changeColumn.Size = OLD_ATTRIB_SIZE;
				Database.ChangeColumn(table, changeColumn);
				changeColumn = columns[2];
				changeColumn.Size = OLD_DESCRIPTION_SIZE;
				Database.ChangeColumn(table, changeColumn);
			}
		}
	}
}