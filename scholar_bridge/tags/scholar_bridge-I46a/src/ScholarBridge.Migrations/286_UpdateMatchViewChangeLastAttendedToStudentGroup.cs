﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(286)]
    public class UpdateMatchViewChangeLastAttendedToStudentGroup : Migration
    {
        public override void Up()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInEnum]");
            Database.ExecuteNonQuery(SB_MATCH_IN_ENUM);
        }

		public override void Down()
		{
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInEnum]");
			Database.ExecuteNonQuery(AddMatchViews.SBMatchInEnum);
		}

    	public const string SB_MATCH_IN_ENUM =
    @"
Create VIEW [dbo].[SBMatchInEnum]
AS
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'StudentGroup' as criterion, 
	s.MatchCriteriaStudentGroups as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on
		(	( (seeker.LastAttended & s.MatchCriteriaStudentGroups) = seeker.LastAttended ) or
			( (seeker.TypeOfCollegeStudent & s.MatchCriteriaStudentGroups) = seeker.TypeOfCollegeStudent) )
where match.SBMatchCriteriaAttributeIndex=26
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SchoolTypes', 
	s.MatchCriteriaSchoolTypes as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.SchoolTypes & s.MatchCriteriaSchoolTypes) = seeker.SchoolTypes
where match.SBMatchCriteriaAttributeIndex=18
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AcademicPrograms', 
	s.MatchCriteriaAcademicPrograms as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.AcademicPrograms & s.MatchCriteriaAcademicPrograms) = seeker.AcademicPrograms
where match.SBMatchCriteriaAttributeIndex=1
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerStatuses', 
	s.MatchCriteriaSeekerStatuses as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.SeekerStatuses & s.MatchCriteriaSeekerStatuses) = seeker.SeekerStatuses
where match.SBMatchCriteriaAttributeIndex=21
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ProgramLengths', 
	s.MatchCriteriaProgramLengths as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.ProgramLengths & s.MatchCriteriaGenders) = seeker.ProgramLengths
where match.SBMatchCriteriaAttributeIndex=15
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Gender', 
	s.MatchCriteriaGenders as val, seeker.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.Gender & s.MatchCriteriaGenders) = seeker.Gender
where match.SBMatchCriteriaAttributeIndex=10";
    }
}