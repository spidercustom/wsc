﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(42)]
    public class AddReligion : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "Religion";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
