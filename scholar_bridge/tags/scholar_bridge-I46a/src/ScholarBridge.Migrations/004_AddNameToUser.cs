using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(4)]
    public class AddNameToUser : Migration
    {
        public override void Up()
        {
            Database.AddColumn(AddUsers.TABLE_NAME, "FirstName", DbType.String, 40, ColumnProperty.Null);
            Database.AddColumn(AddUsers.TABLE_NAME, "MiddleName", DbType.String, 40, ColumnProperty.Null);
            Database.AddColumn(AddUsers.TABLE_NAME, "LastName", DbType.String, 40, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(AddUsers.TABLE_NAME, "FirstName");
            Database.RemoveColumn(AddUsers.TABLE_NAME, "MiddleName");
            Database.RemoveColumn(AddUsers.TABLE_NAME, "LastName");
        }
    }
}