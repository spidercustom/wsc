﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(167)]
    public class AddSeekerCityandSchoolDistrictandCounty : Migration
    {
        private const string TABLE_NAME = "SBSeeker";
        private const string FK_SCHOOLDISTRICT = "FK_SBSeeker_SchoolDistrict";
        private const string FK_COUNTY= "FK_SBSeeker_County";
        private const string FK_CITY = "FK_SBSeeker_City";


        public readonly string[] NEW_COLUMNS
            = {  "SchoolDistrictIndex","CountyIndex","CityIndex" };

        public override void Up()
        {
            
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[2], DbType.Int32, ColumnProperty.Null);

            Database.AddForeignKey(FK_SCHOOLDISTRICT, TABLE_NAME, "SchoolDistrictIndex", "SBSchoolDistrictLUT", "SBSchoolDistrictIndex");
            Database.AddForeignKey(FK_COUNTY, TABLE_NAME, "CountyIndex", "SBCountyLUT", "SBCountyIndex");
            Database.AddForeignKey(FK_CITY, TABLE_NAME, "CityIndex", "SBCityLUT", "SBCityIndex");
            Database.RemoveColumn(TABLE_NAME,"AddressCity");
            var addSeekerSchoolDistrict = new AddSeekerSchoolDistrictRT() { Database = this.Database };
            addSeekerSchoolDistrict.Down();
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_SCHOOLDISTRICT);
            Database.RemoveForeignKey(TABLE_NAME, FK_COUNTY);
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
            var addSeekerSchoolDistrict = new AddSeekerSchoolDistrictRT() {Database =this.Database};
            addSeekerSchoolDistrict.Up();
            Database.AddColumn(TABLE_NAME,"AddressCity",DbType.String,ColumnProperty.Null);
        }
    }
}
