using Migrator.Framework;

namespace ScholarBridge.Migrations.Common
{
    public class DataHelper
    {
        public static int GetAdminUserId(ITransformationProvider database)
        {
            return GetUserId(database, "admin");
        }

        public static int GetUserId(ITransformationProvider database, string userName)
        {
            return (int)database.SelectScalar("Id", AddUsers.TABLE_NAME, string.Format("Username='{0}'", userName));
        }

        public static int GetRoleId(ITransformationProvider database, string roleName)
        {
            return (int)database.SelectScalar("Id", AddRoles.TABLE_NAME, string.Format("Name='{0}'", roleName));
        }
    }
}