using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(294)]
	public class IncreaseLengthOfHonorsAndAPandIBCredits : Migration
    {
		private const string SEEKER_TABLE = "SBSeeker";
		private const string APPLICATION_TABLE = "SBApplication";
		private const string IB_CREDITS = "IBCreditsDetail";
		private const string AP_CREDITS = "APCreditsDetail";
		private const string HONORS = "HonorsDetail";

        public override void Up()
        {
			IncreaseFieldLengths(SEEKER_TABLE);
			IncreaseFieldLengths(APPLICATION_TABLE);
		}

    	public override void Down()
    	{
			DecreaseFieldLengths(SEEKER_TABLE);
			DecreaseFieldLengths(APPLICATION_TABLE);
		}

		private void IncreaseFieldLengths(string tableName)
		{
			Database.ChangeColumn(tableName, new Column(IB_CREDITS, DbType.String, 250, ColumnProperty.Null));
			Database.ChangeColumn(tableName, new Column(AP_CREDITS, DbType.String, 250, ColumnProperty.Null));
			Database.ChangeColumn(tableName, new Column(HONORS, DbType.String, 250, ColumnProperty.Null));
		}

		private void DecreaseFieldLengths(string tableName)
    	{
			TruncateData(tableName);
    		Database.ChangeColumn(tableName, new Column(IB_CREDITS, DbType.String, 50, ColumnProperty.Null));
    		Database.ChangeColumn(tableName, new Column(AP_CREDITS, DbType.String, 50, ColumnProperty.Null));
    		Database.ChangeColumn(tableName, new Column(HONORS, DbType.String, 50, ColumnProperty.Null));
    	}

    	private void TruncateData(string tableName)
		{
			Database.ExecuteNonQuery("Update " + tableName + 
@"  
set APCreditsDetail = SUBSTRING(APCreditsDetail, 1, 50),
	IBCreditsDetail = SUBSTRING(IBCreditsDetail, 1, 50),
	HonorsDetail = SUBSTRING(HonorsDetail, 1, 50)
");
		}
    }
}