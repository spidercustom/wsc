﻿
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(307)]
	public class DropCommunityServiceRT : Migration
	{
		private const string OTHER_FIELD_NAME = "CommunityInvolvementCauseOther";
		public override void Up()
		{
			Database.RemoveTable("SBApplicationCommunityServiceRT");
			Database.RemoveTable("SBScholarshipCommunityServiceRT");
			Database.RemoveTable("SBSeekerCommunityServiceRT");
			Database.RemoveTable("SBCommunityServiceLUT");
			Database.RemoveColumn("SBSeeker", OTHER_FIELD_NAME);
			Database.RemoveColumn("SBApplication", OTHER_FIELD_NAME);

		}

		public override void Down()
		{
			Database.ExecuteNonQuery(LUT_BUILD);
			Database.ExecuteNonQuery(SCHOLARSHIP_RT_BUILD);
			Database.ExecuteNonQuery(APPLICATION_RT_BUILD);
			Database.ExecuteNonQuery(SEEKER_RT_BUILD);
			Database.AddColumn("SBSeeker", OTHER_FIELD_NAME, DbType.String, 250, ColumnProperty.Null);
			Database.AddColumn("SBApplication", OTHER_FIELD_NAME, DbType.String, 250, ColumnProperty.Null);
		}

		#region Rebuild LUT

		public const string LUT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBCommunityServiceLUT')
begin
	CREATE TABLE [dbo].[SBCommunityServiceLUT](
		[SBCommunityServiceIndex] [int] IDENTITY(1,1) NOT NULL,
		[CommunityService] [nvarchar](50) NOT NULL,
		[Description] [nvarchar](250) NULL,
		[Deprecated] [bit] NOT NULL,
		[LastUpdateBy] [int] NOT NULL,
		[LastUpdateDate] [datetime] NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[SBCommunityServiceIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[SBCommunityServiceLUT]  WITH CHECK ADD  CONSTRAINT [FK_SBCommunityInvolvementCauseLUT_LastUpdateBy] FOREIGN KEY([LastUpdateBy])
	REFERENCES [dbo].[SBUser] ([SBUserID])

	ALTER TABLE [dbo].[SBCommunityServiceLUT] CHECK CONSTRAINT [FK_SBCommunityInvolvementCauseLUT_LastUpdateBy]

	ALTER TABLE [dbo].[SBCommunityServiceLUT] ADD  DEFAULT ((0)) FOR [Deprecated]

	ALTER TABLE [dbo].[SBCommunityServiceLUT] ADD  DEFAULT (getdate()) FOR [LastUpdateDate]
end
";

		#endregion

		#region ScolarshipCommunityServiceRT Adder
		public const string SCHOLARSHIP_RT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBScholarshipCommunityServiceRT')
begin
	CREATE TABLE [dbo].[SBScholarshipCommunityServiceRT](
		[SBScholarshipID] [int] NOT NULL,
		[SBCommunityServiceIndex] [int] NOT NULL,
	 CONSTRAINT [PK_SBScholarshipCommunityInvolvementCauseRT] PRIMARY KEY CLUSTERED 
	(
		[SBScholarshipID] ASC,
		[SBCommunityServiceIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[SBScholarshipCommunityServiceRT]  WITH CHECK ADD  CONSTRAINT [FK_SBScholarshipCommunityInvolvementCauseRT_SBCommunityInvolvementCauseLUT] FOREIGN KEY([SBCommunityServiceIndex])
	REFERENCES [dbo].[SBCommunityServiceLUT] ([SBCommunityServiceIndex])

	ALTER TABLE [dbo].[SBScholarshipCommunityServiceRT] CHECK CONSTRAINT [FK_SBScholarshipCommunityInvolvementCauseRT_SBCommunityInvolvementCauseLUT]

	ALTER TABLE [dbo].[SBScholarshipCommunityServiceRT]  WITH CHECK ADD  CONSTRAINT [FK_SBScholarshipCommunityInvolvementCauseRT_SBScholarship] FOREIGN KEY([SBScholarshipID])
	REFERENCES [dbo].[SBScholarship] ([SBScholarshipID])

	ALTER TABLE [dbo].[SBScholarshipCommunityServiceRT] CHECK CONSTRAINT [FK_SBScholarshipCommunityInvolvementCauseRT_SBScholarship]
end
";
		#endregion

		#region ApplicationCommunityServiceRT AdderO
		public const string APPLICATION_RT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBApplicationCommunityServiceRT')
begin
	CREATE TABLE [dbo].[SBApplicationCommunityServiceRT](
		[SBApplicationID] [int] NOT NULL,
		[SBCommunityServiceIndex] [int] NOT NULL,
	 CONSTRAINT [PK_SBApplicationCommunityServiceRT] PRIMARY KEY CLUSTERED 
	(
		[SBApplicationID] ASC,
		[SBCommunityServiceIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[SBApplicationCommunityServiceRT]  WITH CHECK ADD  CONSTRAINT [FK_SBApplicationCommunityServiceRT_SBApplication] FOREIGN KEY([SBApplicationID])
	REFERENCES [dbo].[SBApplication] ([SBApplicationId])

	ALTER TABLE [dbo].[SBApplicationCommunityServiceRT] CHECK CONSTRAINT [FK_SBApplicationCommunityServiceRT_SBApplication]

	ALTER TABLE [dbo].[SBApplicationCommunityServiceRT]  WITH CHECK ADD  CONSTRAINT [FK_SBApplicationCommunityServiceRT_SBCommunityServiceLUT] FOREIGN KEY([SBCommunityServiceIndex])
	REFERENCES [dbo].[SBCommunityServiceLUT] ([SBCommunityServiceIndex])

	ALTER TABLE [dbo].[SBApplicationCommunityServiceRT] CHECK CONSTRAINT [FK_SBApplicationCommunityServiceRT_SBCommunityServiceLUT]
end
";
		#endregion

		#region SeekerCommunityServiceRT Adder
		public const string SEEKER_RT_BUILD =
			@"
if not exists (select * from sys.tables where name = 'SBSeekerCommunityServiceRT')
begin
	CREATE TABLE [dbo].[SBSeekerCommunityServiceRT](
		[SBSeekerID] [int] NOT NULL,
		[SBCommunityServiceIndex] [int] NOT NULL,
	 CONSTRAINT [PK_SBSeekerCommunityInvolvementCauseRT] PRIMARY KEY CLUSTERED 
	(
		[SBSeekerID] ASC,
		[SBCommunityServiceIndex] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[SBSeekerCommunityServiceRT]  WITH CHECK ADD  CONSTRAINT [FK_SBSeekerCommunityInvolvementCauseRT_SBCommunityInvolvementCauseLUT] FOREIGN KEY([SBCommunityServiceIndex])
	REFERENCES [dbo].[SBCommunityServiceLUT] ([SBCommunityServiceIndex])

	ALTER TABLE [dbo].[SBSeekerCommunityServiceRT] CHECK CONSTRAINT [FK_SBSeekerCommunityInvolvementCauseRT_SBCommunityInvolvementCauseLUT]

	ALTER TABLE [dbo].[SBSeekerCommunityServiceRT]  WITH CHECK ADD  CONSTRAINT [FK_SBSeekerCommunityInvolvementCauseRT_SBSeeker] FOREIGN KEY([SBSeekerID])
	REFERENCES [dbo].[SBSeeker] ([SBSeekerId])

	ALTER TABLE [dbo].[SBSeekerCommunityServiceRT] CHECK CONSTRAINT [FK_SBSeekerCommunityInvolvementCauseRT_SBSeeker]
end
";
		#endregion
	}
}
