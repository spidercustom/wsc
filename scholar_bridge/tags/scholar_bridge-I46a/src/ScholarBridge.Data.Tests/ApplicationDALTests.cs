using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class ApplicationDALTests : TestBase
    {

        public ApplicationDAL ApplicationDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }
 
        public SeekerDAL SeekerDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public LookupDAL<Ethnicity> EthnicityDAL { get; set; }
        public LookupDAL<Religion> ReligionDAL { get; set; }
        public LookupDAL<AcademicArea> AcademicAreaDAL { get; set; }
        public LookupDAL<Career> CareerDAL { get; set; }
        public LookupDAL<SeekerMatchOrganization> SeekerMatchOrganizationDAL { get; set; }
        public LookupDAL<Company> CompanyDAL { get; set; }
        public LookupDAL<SeekerHobby> SeekerHobbyDAL { get; set; }
        public LookupDAL<Sport> SportDAL { get; set; }
        public LookupDAL<Club> ClubDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ApplicationQuestionAnswerDAL ApplicationQuestionAnswerDAL { get; set; }

        private User user;
        private Seeker seeker;
		private Scholarship scholarship;
		private Scholarship scholarship2;
		private Scholarship scholarship3;
		private Provider provider;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            seeker = SeekerDALTest.InsertSeeker(SeekerDAL, user, StateDAL);
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "AppTest", user);
			scholarship = ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStage.Activated));
			scholarship2 = ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStage.Activated));
			scholarship3 = ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStage.Activated));
		}

        [Test]
        public void can_insert_application()
        {
            var app = InsertApplication(ApplicationDAL, scholarship, seeker,StateDAL);

            Assert.IsNotNull(app);
            Assert.IsTrue(app.Id > 0);
        }

        [Test]
        public void can_find_all_applications_by_scholarship()
        {
             var app = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            var apps = ApplicationDAL.FindAllSubmitted(0, 100, "DateSubmitted", scholarship);

            Assert.IsNotNull(apps);
            CollectionAssert.IsNotEmpty(apps);
            Assert.AreEqual(app.Id, apps[0].Id);
        }

        [Test]
        public void can_count_applications_by_seeker()
        {
            var app = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            var appCount = ApplicationDAL.CountAllSubmittedBySeeker(seeker);

            Assert.AreEqual(1, appCount);
        }

        [Test]
        public void can_find_all_applications_by_seeker()
        {
            var app = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            var apps = ApplicationDAL.FindAll(seeker);

            Assert.IsNotNull(apps);
            CollectionAssert.IsNotEmpty(apps);
            Assert.AreEqual(app.Id, apps[0].Id);
        }


        [Test]
        public void can_delete_applications()
        {
            var app = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            ApplicationDAL.Delete(app);
            var found = ApplicationDAL.FindById(app.Id);
            Assert.IsNull(found );
            
        }

        [Test]
        public void can_find_by_seekerandScholarship()
        {
            var app = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            var found = ApplicationDAL.FindBySeekerandScholarship(seeker, scholarship);

            Assert.IsNotNull(found);
            Assert.AreEqual(app.Id, found.Id);
        }

        [Test]
        public void can_search_applications_by_seeker()
        {
            var app = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            var apps = ApplicationDAL.SearchSubmittedBySeeker(0, 100, "DateSubmitted", scholarship, seeker.Name.LastName);

            Assert.IsNotNull(apps);
            CollectionAssert.IsNotEmpty(apps);
            Assert.AreEqual(app.Id, apps[0].Id);
        }

		[Test]
		public void can_find_all_finalists()
		{
			var app1 = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
			var app2 = InsertApplication(ApplicationDAL, scholarship2, seeker, StateDAL);

			app1.Finalist = true;
			ApplicationDAL.Update(app1);

			var finalists = ApplicationDAL.FindAllFinalists(scholarship);
			Assert.IsNotNull(finalists);
			Assert.AreEqual(1, finalists.Count);
			Assert.IsTrue(finalists[0].Finalist);
		}

		[Test]
		public void can_count_submitted()
		{
			var currCount = ApplicationDAL.CountAllSubmitted();
			var app1 = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
			var app2 = InsertApplication(ApplicationDAL, scholarship2, seeker, StateDAL);
			var app3 = InsertApplication(ApplicationDAL, scholarship3, seeker, StateDAL);


			app1.Stage = ApplicationStage.Submitted;
			ApplicationDAL.Update(app1);
			app2.Stage = ApplicationStage.Submitted;
			ApplicationDAL.Update(app2);
			app3.Stage = ApplicationStage.NotActivated;
			ApplicationDAL.Update(app3);


			var newCount = ApplicationDAL.CountAllSubmitted();
			Assert.AreEqual(currCount + 2, newCount);
		}

    	[Test]
		public void can_count_started_but_not_submitted()
		{
			var currCount = ApplicationDAL.CountAllStartedButNotSubmitted();
			var app1 = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
			var app2 = InsertApplication(ApplicationDAL, scholarship2, seeker, StateDAL);
			var app3 = InsertApplication(ApplicationDAL, scholarship3, seeker, StateDAL);


			app1.Stage = ApplicationStage.Submitted;
			ApplicationDAL.Update(app1);
			app2.Stage = ApplicationStage.NotActivated;
			ApplicationDAL.Update(app2);
			app3.Stage = ApplicationStage.NotActivated;
			ApplicationDAL.Update(app3);


			var newCount = ApplicationDAL.CountAllStartedButNotSubmitted();
			Assert.AreEqual(currCount + 2, newCount);
		}

        public static Application InsertApplication(ApplicationDAL appDal, Scholarship s, Seeker seek,StateDAL stateDAL)
        {
            var wi = stateDAL.FindByAbbreviation("WI");
        	var app = new Application(ApplicationType.Internal)
        	          	{
        	          		Scholarship = s,
        	          		Seeker = seek,

        	          		Stage = ApplicationStage.Submitted,
        	          		PersonalStatement = "I like to do stuff",
        	          		MyGift = "I read minds",
        	          		Gender = Genders.Male,
        	          		EthnicityOther = "Plutarian",
        	          		ReligionOther = "Anarchy",
        	          		AcademicAreaOther = "AcademicOther",
        	          		CareerOther = "Test CareerOther",
        	          		MatchOrganizationOther = "Test MatchOrganization",
        	          		CompanyOther = "Test CompanyOther",
        	          		HobbyOther = "Test HobbiersOther",
        	          		SportOther = "Test SportsOther",
        	          		ClubOther = "Test ClubOther",
        	          		WorkHistory = "Test WorkHistory",
        	          		ServiceHistory = "Test ServiceHistory",
        	          		SeekerAcademics = new SeekerAcademics {GPA = 3, RunningStart = true, RunningStartDetail = "blah"},
							     
                                 DateOfBirth = DateTime.Now.AddYears(-18),
                                 IsEslEll = true,
                                  
                                 LastUpdate = new ActivityStamp(seek.User)
                          };
            app = appDal.Insert(app);
            return app;
        }

        #region Application ethnicity tests
        [Test]
        public void can_associate_application_with_ethnicities()
        {
            Application application = GetApplicationWithEthnicities();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Ethnicities);
            Assert.AreEqual(2, found.Ethnicities.Count);
        }

        [Test]
        public void can_remove_ethnicities_from_application()
        {
            Application application = GetApplicationWithEthnicities();
            Assert.AreEqual(2, application.Ethnicities.Count);

            application.Ethnicities.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Ethnicities);
            Assert.AreEqual(1, found.Ethnicities.Count);
        }
        #endregion

        #region Application religion tests
        [Test]
        public void can_associate_application_with_religions()
        {
            Application application = GetApplicationWithReligions();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Religions);
            Assert.AreEqual(2, found.Religions.Count);
        }

        [Test]
        public void can_remove_Religions_from_application()
        {
            Application application = GetApplicationWithReligions();
            Assert.AreEqual(2, application.Religions.Count);

            application.Religions.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id); ;
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Religions);
            Assert.AreEqual(1, found.Religions.Count);
        }
        #endregion
        #region Application AcademicAreas tests
        [Test]
        public void can_associate_application_with_AcademicAreas()
        {
            Application application = GetApplicationWithAcademicAreas();

            var found = ApplicationDAL.FindById(application.Id); ;
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AcademicAreas);
            Assert.AreEqual(2, found.AcademicAreas.Count);
        }

        [Test]
        public void can_remove_AcademicAreas_from_application()
        {
            Application application = GetApplicationWithAcademicAreas();
            Assert.AreEqual(2, application.AcademicAreas.Count);

            application.AcademicAreas.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AcademicAreas);
            Assert.AreEqual(1, found.AcademicAreas.Count);
        }
        #endregion

        #region Application Careers tests
        [Test]
        public void can_associate_application_with_Careers()
        {
            Application application = GetApplicationWithCareers();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Careers);
            Assert.AreEqual(2, found.Careers.Count);
        }

        [Test]
        public void can_remove_Careers_from_application()
        {
            Application application = GetApplicationWithCareers();
            Assert.AreEqual(2, application.Careers.Count);

            application.Careers.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Careers);
            Assert.AreEqual(1, found.Careers.Count);
        }
        #endregion

        #region Application ApplicationMatchOrganizations tests
        [Test]
        public void can_associate_application_with_ApplicationMatchOrganizations()
        {
            Application application = GetApplicationWithApplicationMatchOrganizations();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.MatchOrganizations);
            Assert.AreEqual(2, found.MatchOrganizations.Count);
        }

        [Test]
        public void can_remove_ApplicationMatchOrganizations_from_application()
        {
            Application application = GetApplicationWithApplicationMatchOrganizations();
            Assert.AreEqual(2, application.MatchOrganizations.Count);

            application.MatchOrganizations.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.MatchOrganizations);
            Assert.AreEqual(1, found.MatchOrganizations.Count);
        }
        #endregion

        #region Application Companies tests
        [Test]
        public void can_associate_application_with_Companies()
        {
            Application application = GetApplicationWithCompanies();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Companies);
            Assert.AreEqual(2, found.Companies.Count);
        }

        [Test]
        public void can_remove_Companies_from_application()
        {
            Application application = GetApplicationWithCompanies();
            Assert.AreEqual(2, application.Companies.Count);

            application.Companies.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Companies);
            Assert.AreEqual(1, found.Companies.Count);
        }
        #endregion

        #region Application Hobbies tests
        [Test]
        public void can_associate_application_with_Hobbies()
        {
            Application application = GetApplicationWithHobbies();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Hobbies);
            Assert.AreEqual(2, found.Hobbies.Count);
        }

        [Test]
        public void can_remove_Hobbies_from_application()
        {
            Application application = GetApplicationWithHobbies();
            Assert.AreEqual(2, application.Hobbies.Count);

            application.Hobbies.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Hobbies);
            Assert.AreEqual(1, found.Hobbies.Count);
        }
        #endregion

        #region Application Sports tests
        [Test]
        public void can_associate_application_with_Sports()
        {
            Application application = GetApplicationWithSports();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Sports);
            Assert.AreEqual(2, found.Sports.Count);
        }

        [Test]
        public void can_remove_Sports_from_application()
        {
            Application application = GetApplicationWithSports();
            Assert.AreEqual(2, application.Sports.Count);

            application.Sports.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Sports);
            Assert.AreEqual(1, found.Sports.Count);
        }
        #endregion

        #region Application Clubs tests
        [Test]
        public void can_associate_application_with_Clubs()
        {
            Application application = GetApplicationWithClubs();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Clubs);
            Assert.AreEqual(2, found.Clubs.Count);
        }

        [Test]
        public void can_remove_Clubs_from_application()
        {
            Application application = GetApplicationWithClubs();
            Assert.AreEqual(2, application.Clubs.Count);

            application.Clubs.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Clubs);
            Assert.AreEqual(1, found.Clubs.Count);
        }
        #endregion

        #region Application QuestionAnswers tests
        [Test]
        public void can_associate_application_with_QuestionAnswers()
        {
            Application application = GetApplicationWithQuestionAnswers();

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.QuestionAnswers);
            Assert.AreEqual(2, found.QuestionAnswers.Count);
        }

        [Test]
        public void can_remove_QuestionAnswers_from_application()
        {
            Application application = GetApplicationWithQuestionAnswers();
            Assert.AreEqual(2, application.QuestionAnswers.Count);

            application.QuestionAnswers.RemoveAt(0);
            ApplicationDAL.Update(application);

            var found = ApplicationDAL.FindById(application.Id);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.QuestionAnswers);
            Assert.AreEqual(1, found.QuestionAnswers.Count);
        }
        #endregion

        private Application GetApplicationWithEthnicities()
        {
            var s1 = EthnicityDAL.Insert(new Ethnicity { Name = "Martian", LastUpdate = new ActivityStamp(user) });
            var s2 = EthnicityDAL.Insert(new Ethnicity { Name = "Lunar", LastUpdate = new ActivityStamp(user) });
            var s3 = EthnicityDAL.Insert(new Ethnicity { Name = "Asteroidian", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.Ethnicities.Add(s1);
            application.Ethnicities.Add(s3);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithReligions()
        {
            var s1 = ReligionDAL.Insert(new Religion { Name = "Astrology", LastUpdate = new ActivityStamp(user) });
            var s2 = ReligionDAL.Insert(new Religion { Name = "Darwinism", LastUpdate = new ActivityStamp(user) });
            var s3 = ReligionDAL.Insert(new Religion { Name = "Athiesm", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.Religions.Add(s1);
            application.Religions.Add(s3);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithAcademicAreas()
        {
            var s1 = AcademicAreaDAL.Insert(new AcademicArea { Name = "Test Academic1", LastUpdate = new ActivityStamp(user) });
            var s2 = AcademicAreaDAL.Insert(new AcademicArea { Name = "Test Academic2", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.AcademicAreas.Add(s1);
            application.AcademicAreas.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithCareers()
        {
            var s1 = CareerDAL.Insert(new Career { Name = "Test Career1", LastUpdate = new ActivityStamp(user) });
            var s2 = CareerDAL.Insert(new Career { Name = "Test Career2", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.Careers.Add(s1);
            application.Careers.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithApplicationMatchOrganizations()
        {
            var s1 = SeekerMatchOrganizationDAL.Insert(new SeekerMatchOrganization { Name = "Test ApplicationMatchOrganization1", LastUpdate = new ActivityStamp(user) });
            var s2 = SeekerMatchOrganizationDAL.Insert(new SeekerMatchOrganization { Name = "Test ApplicationMatchOrganization2", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.MatchOrganizations.Add(s1);
            application.MatchOrganizations.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithCompanies()
        {
            var s1 = CompanyDAL.Insert(new Company { Name = "Test Companies1", LastUpdate = new ActivityStamp(user) });
            var s2 = CompanyDAL.Insert(new Company { Name = "Test Companies2", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.Companies.Add(s1);
            application.Companies.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithHobbies()
        {
            var s1 = SeekerHobbyDAL.Insert(new SeekerHobby { Name = "Test ApplicationHobby1", LastUpdate = new ActivityStamp(user) });
            var s2 = SeekerHobbyDAL.Insert(new SeekerHobby { Name = "Test ApplicationHobby2", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.Hobbies.Add(s1);
            application.Hobbies.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithSports()
        {
            var s1 = SportDAL.Insert(new Sport { Name = "Test Sport1", LastUpdate = new ActivityStamp(user) });
            var s2 = SportDAL.Insert(new Sport { Name = "Test Sport2", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.Sports.Add(s1);
            application.Sports.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithClubs()
        {
            var s1 = ClubDAL.Insert(new Club { Name = "Test Club1", LastUpdate = new ActivityStamp(user) });
            var s2 = ClubDAL.Insert(new Club { Name = "Test Club2", LastUpdate = new ActivityStamp(user) });

            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            application.Clubs.Add(s1);
            application.Clubs.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }

        private Application GetApplicationWithQuestionAnswers()
        {
            
            scholarship.AddAdditionalQuestion(new ScholarshipQuestion() { DisplayOrder = 1, QuestionText = "testQ1", Scholarship = scholarship, LastUpdate = new ActivityStamp(user) });
            scholarship.AddAdditionalQuestion(new ScholarshipQuestion() { DisplayOrder = 2, QuestionText = "testQ2", Scholarship = scholarship, LastUpdate = new ActivityStamp(user) });
            ScholarshipDAL.Update(scholarship);
            var application = InsertApplication(ApplicationDAL, scholarship, seeker, StateDAL);
            
            var s1 = ApplicationQuestionAnswerDAL.Insert(new ApplicationQuestionAnswer()
                                                             {
                                                                 AnswerText="TestAnswer1",
                                                                 Application=application,
                                                                 Question  =scholarship.AdditionalQuestions[0],
                                                                 LastUpdate=new ActivityStamp(user)

                                                             });
            var s2 = ApplicationQuestionAnswerDAL.Insert(new ApplicationQuestionAnswer()
            {
                AnswerText = "TestAnswer2",
                Application = application,
                Question = scholarship.AdditionalQuestions[1],
                LastUpdate = new ActivityStamp(user)

            });
            
            application.QuestionAnswers.Add(s1);
            application.QuestionAnswers.Add(s2);

            ApplicationDAL.Update(application);
            return application;
        }
    }
}
