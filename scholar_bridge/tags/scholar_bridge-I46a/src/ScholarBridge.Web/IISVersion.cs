﻿using System.Diagnostics;

namespace ScholarBridge.Web
{
	public class IISVersion
	{
		public static bool RunningIIS7
		{
			get
			{
				if (MajorVersion > 6)
					return true;
				return false;
			}
		}

		public static int MajorVersion
		{
			get
			{
				int version = 0;
				if (FullVersionInfo != null)
					version = FullVersionInfo.FileMajorPart;
				return version;
			}
		}

		public static FileVersionInfo FullVersionInfo
		{
			get
			{
				if (IISMainModule != null)
					return IISMainModule.FileVersionInfo;
				return null;	
			}
		}

		public static ProcessModule IISMainModule
		{
			get
			{
				return GetMainProcessModule();	
			}
		}

		private static ProcessModule GetMainProcessModule()
		{
			ProcessModule module = null;
			using (var process = Process.GetCurrentProcess())
			{
				using (var mainModule = process.MainModule)
				{   // main module would be w3wp        
					if (mainModule != null) module = mainModule;
				}
				return module;
			}

		}
	}
}
