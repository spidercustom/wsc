﻿using System.IO;

namespace ScholarBridge.Business
{
	public class LoginPageTextService : ILoginPageTextService
	{
		// DI loaded
		public IFtpService FtpService { get; set; }

		// DI loaded from config
		public string ScholarshipListDirectory { get; set; }

		private const string LOGIN_TEXT_INCLUDE_FILE_NAME = "loginInclude.htm";

		private string LoginPageIncludeFilePath
		{
			get
			{
				if (ScholarshipListDirectory.EndsWith("\\"))
					return ScholarshipListDirectory + LOGIN_TEXT_INCLUDE_FILE_NAME;

				return ScholarshipListDirectory + "\\" + LOGIN_TEXT_INCLUDE_FILE_NAME; 
			}
		}

		public string GetLoginPageText()
		{
			if (File.Exists(LoginPageIncludeFilePath))
			{
				return File.ReadAllText(LoginPageIncludeFilePath);	
			}

			return null;
		}

		public void PutLoginPageText(string loginPageText)
		{
			File.WriteAllText(LoginPageIncludeFilePath, loginPageText);
			if (FtpService.FTPEnabled)
				FtpService.PutFileToPublicServer(LOGIN_TEXT_INCLUDE_FILE_NAME, LoginPageIncludeFilePath);
		}
	}
}
