using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface IProviderService : IOrganizationService<Provider>
    {

    }
}