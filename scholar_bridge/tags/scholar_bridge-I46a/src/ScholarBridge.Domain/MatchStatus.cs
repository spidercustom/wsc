namespace ScholarBridge.Domain
{
    public enum MatchStatus
    {
        New,
        Saved,
        Submitted
    }
}