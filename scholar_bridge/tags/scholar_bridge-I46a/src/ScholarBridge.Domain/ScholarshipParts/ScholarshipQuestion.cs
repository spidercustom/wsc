﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipQuestion : ICloneable
    {
        public virtual int Id { get; set; }
		[StringLengthValidator(0, 500, MessageTemplate = "Scholarship questions have a 500 character limit.")]
		public virtual string QuestionText { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual bool IsRequired { get; set; }
        public virtual Scholarship Scholarship { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }
    
        #region ICloneable Members

        public virtual object Clone()
        {
			var questionClone = (ScholarshipQuestion)MemberwiseClone();
        	questionClone.Id = 0;
        	return questionClone;
        }

        #endregion
    }
}