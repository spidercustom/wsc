using System;

namespace ScholarBridge.Domain.Messaging
{
    public class SentMessage : IMessage
    {
        protected SentMessage() {}

        public SentMessage(IMessage message)
        {
            InitializeMembers(message);
        }

        private void InitializeMembers(IMessage message)
        {
            Subject = message.Subject;
            Content = message.Content;
            Date = message.Date;
            To = message.To;
            From = message.From;
            MessageTemplate = message.MessageTemplate;
        }

        public virtual int Id { get; set; }

        public virtual string Subject { get; set; }
        public virtual string Content { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string OrganizationName { get; set; }
        public virtual string RecipientName { get; set; }
        public virtual MessageType MessageTemplate { get; set; }

        public virtual MessageAddress To { get; set; }
        public virtual MessageAddress From { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }
    }
}