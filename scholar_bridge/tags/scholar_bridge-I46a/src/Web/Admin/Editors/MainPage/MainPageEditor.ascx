﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainPageEditor.ascx.cs"
    Inherits="ScholarBridge.Web.Admin.Editors.MainPage.MainPageEditor" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<div class="form-iceland-container">
    <div class="form-iceland">
        <br />
            <asp:label ForeColor="Green" ID="messageLabel" runat="server"></asp:label>
        <br />
        
            <label style="width:500px">Main Page &#39;News &amp; Announcements&#39; Text :&nbsp; (html markup is okay)</label><br />
        <table>
            <tr>
                <td valign="top">
                    <asp:TextBox ID="loginPageTextBox" runat="server" TextMode="MultiLine" Rows="10" Width="500px"
                        Height="300px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="BodyControlRequiredValidator" runat="server" ControlToValidate="loginPageTextBox"
                        ErrorMessage="Should not be empty."></asp:RequiredFieldValidator>
                </td>
                <td valign="top">
                    <div id="previewDiv" style="font-size: 14px; width: 240px; height: 260px; border-style: solid;
                        border-color: Gray;">
                        <asp:label ID="newAndAnnouncementHtml" runat="server"></asp:label>
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <sbCommon:AnchorButton ID="cancelButton" runat="server" Text="Preview" CausesValidation="false"
            OnClick="previewButton_Click" />
        <sbCommon:AnchorButton ID="saveButton" runat="server" Text="Save" OnClick="saveButton_Click" />
        <br />
        <br />
    </div>
</div>
