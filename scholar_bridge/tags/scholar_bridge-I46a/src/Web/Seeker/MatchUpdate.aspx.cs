﻿using System;
using System.Threading;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Seeker
{
	/// <summary>
	/// This page acts as a conduit between Login and seeker/default.  The page is not
	/// displayed, it simply starts a seeker MatchUpdate (in a thread) and then redirects to seeker/default.
	/// </summary>
	public partial class MatchUpdate : System.Web.UI.Page
	{
		public IUserContext UserContext { get; set; }
		public IMatchService MatchService { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.User.Identity.IsAuthenticated)
				Response.Redirect("~/seeker/anonymous.aspx");
			UserContext.EnsureSeekerIsInContext();

			UpdateSeekerMatches();

			Response.Redirect(ResolveUrl("~/seeker/"));
		}
		/// <summary>
		/// Since updating matches is somewhat time consuming, we're going to run this in 
		/// as separate thread.
		/// </summary>
		private void UpdateSeekerMatches()
		{
			var matchThread = new Thread(UpdateMatches) {Priority = ThreadPriority.Lowest};
			matchThread.Start(UserContext.CurrentSeeker);
		}

		/// <summary>
		/// Update seeker matches
		/// </summary>
		private void UpdateMatches(Object seekerObject)
		{
			Domain.Seeker seeker = (Domain.Seeker)seekerObject;
			MatchService.UpdateMatches(seeker);
		}

	}
}
