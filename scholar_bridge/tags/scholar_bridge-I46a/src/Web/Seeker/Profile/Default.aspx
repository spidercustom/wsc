﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Default"  Title="MyProfile" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="~/Seeker/Profile/Basics.ascx" tagname="Basics" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AboutMe.ascx" tagname="AboutMe" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AcademicInformation.ascx" tagname="AcademicInfo" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/Activities.ascx" tagname="Activities" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/FinancialNeed.ascx" tagname="FinancialNeed" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<%@ Register Src="~/Common/SeekerProfileActivationValidation.ascx" TagName="SeekerProfileActivationValidationErrors"  TagPrefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="My Profile" />

    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>

    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/styles/form.css") %>" rel="stylesheet" type="text/css" media="All"/> 

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="167" width="873" src="<%= ResolveUrl("~/images/header/my_profile.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <asp:ScriptManager ID="scriptmanager1" runat="server" ></asp:ScriptManager>
    <div id="confirmSaving" title="Confirm saving" style="display:none">
        You have change the data. Would you like to save the changes?
    </div>
    <div id="confirmSavingActivatedProfile" title="Confirm saving" style="display:none">
        <asp:Literal ID="ConfirmSaveIfActivatedLabel" runat="server" />
    </div>
    <sb:SeekerProfileActivationValidationErrors id="seekerProfileActivationValidationErrors" runat="server" />
     <div id="ProfileCompleteness">
         <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" ShowGotoProfile="false" />
     </div>
   
    <h1><img src="<%= ResolveUrl("~/images/PgTitle_MyProfile.gif") %>" width="249px" height="54px" /></h1>
    <img src="<%= ResolveUrl("~/images/EmphasisedMyProfilePage.gif") %>" width="513px" height="79px" />
   
    <div class="tabs" style="clear:both;" id="BuildSeekerProfileWizardTab">
        <ul>
            <li><a href="#tab"><span>Basics</span></a></li>
            <li><a href="#tab"><span>About Me</span></a></li>
            <li><a href="#tab"><span>My Academic Info</span></a></li>
            <li><a href="#tab"><span>My Activities</span></a></li>
            <li><a href="#tab"><span>My Financial Need</span></a></li>
        </ul>
         
        <div id="tab">
            <asp:MultiView ID="BuildSeekerProfileWizard" runat="server" OnActiveViewChanged="BuildSeekerProfileWizard_ActiveStepChanged">
              <asp:View ID="BasicInfoStep" runat="server">
                    <sb:Basics ID="Basics1" runat="server" />
              </asp:View>
              <asp:View ID="AboutMeStep" runat="server">
                    <sb:AboutMe ID="AboutMe1" runat="server" />
              </asp:View>
              <asp:View ID="AcademicStep" runat="server">
                    <sb:AcademicInfo ID="AcademicInfo1" runat="server" />
              </asp:View>
              <asp:View ID="ActivitiesStep" runat="server">
                    <sb:Activities ID="Activities1" runat="server"  />
              </asp:View>
              <asp:View ID="FinancialNeed" runat="server">
                    <sb:FinancialNeed ID="FinancialNeed1" runat="server" />
              </asp:View>
            </asp:MultiView>
        </div>
        <sbCommon:jQueryTabIntegrator id="buildSeekerProfileWizardTabIntegrator" runat="server" CausesValidation="true" MultiViewControlID="BuildSeekerProfileWizard" TabControlClientID="BuildSeekerProfileWizardTab" />
    </div>
    
    <div style="float:right">
        <sbCommon:SaveConfirmButton ID="PreviousButton" ToolTip="Save & navigate to the prior tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" runat="server" OnClick="PreviousButton_Click" Text="Previous"/>
        <sbCommon:SaveConfirmButton ID="NextButton" ToolTip="Save & navigate to the next tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" OnClick="NextButton_Click" runat="server" Text="Next" />
    </div>
    <sbCommon:SaveConfirmButton ID="SaveButton" ToolTip="Save all work on this tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" runat="server" OnClick="SaveButton_Click" Text="Save & Exit"/>
</asp:Content>


