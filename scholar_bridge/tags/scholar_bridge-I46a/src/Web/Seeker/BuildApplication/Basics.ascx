﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.Basics" %>
<%@ Register TagPrefix="sb" TagName="LookupItemCheckboxList" Src="~/Common/LookupItemCheckboxList.ascx" %>
<%@ Register Tagprefix="sb" tagname="LookupDialog" src="~/Common/Lookup/LookupDialog.ascx"   %>    
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>
<%@ Register TagPrefix="sb" TagName="Tip" Src="~/Common/Tip.ascx" %>
 
<div class="two-column with_divider">
    <div>
        <h2>My Contact Information</h2>
        <p style="font-style:italic; margin-top:0px;">
            Your contact information can only be changed in <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Seeker/Profile/" runat="server">your profile</asp:HyperLink>.
        </p>
        <p>
            <b>
                <asp:Literal ID="FirstNameControl" runat="server" /> 
                <asp:Literal ID="MidNameControl" runat="server" /> 
                <asp:Literal ID="LastNameControl" runat="server" />,
            </b>
            <asp:Literal ID="EmailControl" runat="server" />
            <br />
            <span id="HomePhoneArea" runat="server">
                <asp:Literal ID="PhoneControl" runat="server" /> (home)
                <br />
            </span>
            <span id="MobilePhoneArea" runat="server">
                <asp:Literal ID="MobilePhoneControl" runat="server" /> (mobile)
            </span>
        </p>
        <p>
            <asp:Literal ID="AddressLine1Control" runat="server" />
            <br />
            <span id="AddressLine2Area" runat="server">
                <asp:Literal ID="AddressLine2Control" runat="server" />
                <br />
            </span>
            <asp:Literal ID="CityControl" runat="server" />, 
            <asp:Literal ID="CountyControl" runat="server" />, 
            <asp:Literal ID="StateControl" runat="server" />
            <asp:Literal ID="ZipControl" runat="server" />
        </p>
    </div>
    <div class="question_list">
        <h2>My Background Information</h2>
        <div>
            <label>Birthdate</label>
            <asp:TextBox ID="DobControl" runat="server" CssClass="date"/>
            <elv:PropertyProxyValidator ID="DobValidator" runat="server" ControlToValidate="DobControl" PropertyName="DateOfBirth" SourceTypeName="ScholarBridge.Domain.Seeker"/>
            <asp:RangeValidator ID="dobRangeValid" runat="server" ControlToValidate="DobControl" MinimumValue="1/1/1900" Type="Date" Display="Dynamic" ErrorMessage="How old are you?" />
        </div>
        <div>
            <label><span class="tip" title="Some Scholarships include gender in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for.">Gender</span></label>
            <asp:RadioButtonList ID="GenderButtonList" runat="server">
                <asp:ListItem Text="Male" Value="2" />
                <asp:ListItem Text="Female" Value="4" />
                <asp:ListItem Text="I prefer not to answer" Value="10" />
            </asp:RadioButtonList>
        </div>
        <div>
            <label><span class="tip" title="Some Scholarships include religion in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for.">
                        Religion / Faith
                   </span>
            </label>
            <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" CheckBoxListWidth="210px" />
            <label></label>
            <div>
                <label>Other: </label>
                <asp:TextBox ID="OtherReligion" runat="server"/>  
                <elv:PropertyProxyValidator ID="OtherReligionValidator" runat="server" ControlToValidate="OtherReligion" PropertyName="ReligionOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>
            </div>
        </div>
        <div>
            <label><span class="tip" title="Some Scholarships include ethnic heritage in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for.">Ethnicity / Heritage</span></label>
            <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" CheckBoxListWidth="210px"/>
            <label></label>
            <div>
                <label>Other: </label>
                <asp:TextBox ID="OtherEthnicity" runat="server"/>
                <elv:PropertyProxyValidator ID="OtherEthnicityValidator" runat="server" ControlToValidate="OtherEthnicity" PropertyName="EthnicityOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>
            </div>
        </div>
        <div>
            <label><span class="tip" title="Is English your Second Language or are you an English Language Learner?">ESL/ELL</span></label>
            <asp:RadioButtonList id="ESLELLSelection" runat="server">
                <asp:ListItem Text="Yes" Value="True" />
                <asp:ListItem Text="No" Value="False" />
            </asp:RadioButtonList>
        </div>
    </div>
</div>
