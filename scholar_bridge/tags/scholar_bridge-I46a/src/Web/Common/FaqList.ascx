﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FaqList.ascx.cs" Inherits="ScholarBridge.Web.Common.FaqList" %>
<asp:Repeater ID="faqRepeater" runat="server">
    <ItemTemplate>
        <div class="question">Q: <%# Eval("Question") %></div>
        <div class="answer">A: <%# Eval("Answer") %></div>
    </ItemTemplate>
</asp:Repeater>
