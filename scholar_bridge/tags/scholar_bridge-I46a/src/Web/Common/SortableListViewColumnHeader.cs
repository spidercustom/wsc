﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

namespace ScholarBridge.Web.Common
{
    #region SortableListViewColumnHeader Class
    /// <summary>
    /// ListView Sort Column Header
    /// </summary>
    [
        ToolboxData("<{0}:SortableListViewColumnHeader runat=server></{0}:SortableListViewColumnHeader>"),
        ParseChildren(true),
        PersistChildren(true)
    ]
    public class SortableListViewColumnHeader : CompositeControl
    {
        #region Constructor
        /// <summary>
        /// Constructor.
        /// </summary>
        public SortableListViewColumnHeader()
        {

        }
        #endregion

        #region Private Properties

        // ViewState veriables
        private const string _Text = "_Text";
        private const string _Key = "_Key";
        private const string HEADERCSS_ASC = "headerSortDown";
        private const string HEADERCSS_DESC = "headerSortUp";
         
        #endregion

        

        #region Public Properties

        /// <summary>
        /// Text.
        /// </summary>
        public string Text
        {
            get
            {
                return (this.ViewState[_Text] == null) ? "" : (string)this.ViewState[_Text];
            }
            set
            {
                this.ViewState[_Text] = value;
            }
        }

        /// <summary>
        /// Key.
        /// </summary>
        public string Key
        {
            get
            {
                return (this.ViewState[_Key] == null) ? "" : (string)this.ViewState[_Key];
            }
            set
            {
                this.ViewState[_Key] = value;
            }
        }

    
         
        #endregion

        #region Protected Methods

        /// <summary>
        /// DataBind.
        /// </summary>
        public override void DataBind()
        {
            base.DataBind();
            EnsureChildControls();
        }

        /// <summary>
        /// CreateChildControls.
        /// </summary>
        protected override void CreateChildControls()
        {
            Controls.Clear();

            var lnkButton = new LinkButton();
            lnkButton.ID = "btn" + Key;
            lnkButton.CommandName = "Sort";
            lnkButton.CommandArgument = Key;
            lnkButton.Text = Text;
            this.CssClass="";
            Controls.Add(lnkButton);

            
        }
        #endregion

        #region Public Methods
        

        public void ResetSortDirectionIndicator()
        {
             this.CssClass = "";
        }


        public void SetSortDirectionIndicator(string sortExpression,SortDirection sortDirection)
        {
            this.CssClass = sortDirection == SortDirection.Ascending ? HEADERCSS_ASC : HEADERCSS_DESC;
        }

        #endregion

    }
    #endregion

     
}