﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NumberRangeConditionControl.ascx.cs" Inherits="ScholarBridge.Web.Common.NumberRangeConditionControl" %>

<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>

<asp:Panel id="ResetSizingDiv" runat="server" CssClass="resetSizing" >
    <%--<label id="TitleLabel" class="testlabel" runat="server"  for="GreaterThanControl" />--%>
    <label id="GreaterThanControlLabel" runat="server" for="GreaterThanControl">Greater than or equal</label>
    <sandTrap:NumberBox ID="GreaterThanControl" runat="server" Width="50px" Precision="0" />
    <asp:CustomValidator ID="GreaterThanControlValidator" runat="server" 
      ErrorMessage="" Text="" 
      onservervalidate="GreaterThanControlValidator_ServerValidate" ></asp:CustomValidator>


    <label id="LessThanControlLabel" runat="server" for="LessThanControl">Less than or equal</label>
    <sandTrap:NumberBox ID="LessThanControl" runat="server" Width="50px" Precision="0" />
    <asp:CustomValidator ID="LessThanControlValidator" runat="server" 
      ErrorMessage="" Text="" 
      onservervalidate="LessThanControlValidator_ServerValidate" ></asp:CustomValidator>
</asp:Panel>