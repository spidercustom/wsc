using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface ISeekerService
    {
        void SaveNew(Seeker seeker);
        void Update(Seeker context);

        void Activate(Seeker seeker);

    	int CountActivatedSeekers();
    	int CountSeekersWithActiveProfiles();
    }
}