using System;

namespace ScholarBridge.Business.Exceptions
{
    public class DuplicateRelationshipException : Exception
    {
        public DuplicateRelationshipException(string message) : base(message) { }
    }
}