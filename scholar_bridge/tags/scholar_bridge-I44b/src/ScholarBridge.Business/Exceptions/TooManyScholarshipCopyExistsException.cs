﻿using System;
using System.Runtime.Serialization;

namespace ScholarBridge.Business.Exceptions
{
    [Serializable]
    public class TooManyScholarshipCopyExistsException : Exception
    {
        private const string DEFAULT_MESSAGE = "There are too many copies of same scholarship. Please rename one or more from them and try again.";

        public TooManyScholarshipCopyExistsException() : base(DEFAULT_MESSAGE)
        {
        }

        protected TooManyScholarshipCopyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
