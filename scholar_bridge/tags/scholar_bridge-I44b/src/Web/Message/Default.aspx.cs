﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Message
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }
        protected override void OnInitComplete(EventArgs e)
        {
			if (User.IsInRole(Role.SEEKER_ROLE) || User.IsInRole(Role.INFLUENCER_ROLE))
            {
                messageTabPages.Views.Remove(messageTabPages.FindControl("ApprovedScholarshipsTabPage"));
                approvedTabHead.Visible = false;
            }
            base.OnInitComplete(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.Messages);

            if (User.IsInRole(Role.PROVIDER_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for notifications of scholarship status, scholarship applications and awards, and requests from other scholarship organizations. This inbox is shared between all your organizations users. Actions done by one user impact the other user's messages.";

            else if (User.IsInRole(Role.INTERMEDIARY_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for requests from other scholarship organizations, and notifications of scholarship status, applications and awards. This inbox is shared between all your organizations users. Actions done by one user impact the other user's messages.";
            else
                lblPageText.Visible = false;
            if (IsPostBack)
              SetupMessageList(messageTabPages.ActiveViewIndex );  
            
        }
        protected void messageTabPages_OnActiveViewChanged(object sender, EventArgs e)
        {
            SetupMessageList( messageTabPages.ActiveViewIndex  );
        }

        protected void SetupMessageList(int viewIndex)
        {
            

            switch (viewIndex)
            {
                case 0: //InboxView
                    messageList.Visible = true;
                    messageList.Messages = MessageService.FindAllForInbox(UserContext.CurrentUser, UserContext.CurrentOrganization);
                    messageList.PreserveSorting("");
                    sentMessageList.Visible = false;
                    sentMessageList.Messages=new List<SentMessage>();
                    break;
                case 1: //ArchivedView
                     
                    messageList.Visible = true;
                    messageList.Messages = MessageService.FindAllArchived(UserContext.CurrentUser, UserContext.CurrentOrganization);
                    messageList.PreserveSorting("");
                    sentMessageList.Visible = false;
                    sentMessageList.Messages = new List<SentMessage>();
                    break;
                case 2: //SentView
                    messageList.Visible = false;
                    sentMessageList.Visible = true;
                    sentMessageList.Messages = MessageService.FindAllSent(UserContext.CurrentUser, UserContext.CurrentOrganization);
                    sentMessageList.PreserveSorting("");
                   
                    break;
                case 3: //ApprovedScholarhsipView
                    messageList.Visible = true;
                    messageList.Messages = MessageService.FindAllApprovedScholarships(UserContext.CurrentUser, UserContext.CurrentOrganization);
                    messageList.PreserveSorting("");
                    sentMessageList.Visible = false;
                    sentMessageList.Messages = new List<SentMessage>();
                    break;
                default:
                    throw new NotSupportedException("Message View not supported.");

            }
        }

        
    }
}