﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Basics : WizardStepUserControlBase<Domain.Seeker>
    {
		public ISeekerService SeekerService { get; set; }
		public IStateDAL StateService { get; set; }
        public IUserContext UserContext { get; set; }
        public LookupDAL<City> CityDAL { get; set; }
        public LookupDAL<County> CountyDAL { get; set; }

		private Domain.Seeker seekerInContext;

		Domain.Seeker SeekerInContext
		{
			get
			{
				if (seekerInContext == null)
					seekerInContext = Container.GetDomainObject();
				return seekerInContext;
                
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			if (SeekerInContext == null)
				throw new InvalidOperationException("There is no seeker in context");

            dobRangeValid.MaximumValue = DateTime.Today.ToShortDateString();
			if (!Page.IsPostBack)
			{
				PopulateScreen();
			}
            SetWashigtonStateDropownEvent();
            SetStateControls();
			SetCountyAndCityValidators();
		}

		private void PopulateScreen()
		{

			StateDropDown.DataSource = StateService.FindAll();
			StateDropDown.DataTextField = "Name";
			StateDropDown.DataValueField = "Abbreviation";
			StateDropDown.DataBind();

			StateDropDown.Items.Insert(0, new ListItem("- Select One -", ""));

            CityDropDown.DataSource = CityDAL.FindAll() ;//.FindByState("WA");
            CityDropDown.DataTextField = "Name";
            CityDropDown.DataValueField = "Id";
            CityDropDown.DataBind();

		    CountyDropDown.DataSource = CountyDAL.FindAll(); // .FindByState("WA");
            CountyDropDown.DataTextField = "Name";
            CountyDropDown.DataValueField = "Id";
            CountyDropDown.DataBind();
            

            EmailBox.Text = !String.IsNullOrEmpty(SeekerInContext.User.EmailWaitingforVerification) ? string.Format("{0} - Email waiting for verification ({1})", SeekerInContext.User.Email, SeekerInContext.User.EmailWaitingforVerification)
                : SeekerInContext.User.Email;
		    UseEmailBox.Checked = SeekerInContext.User.IsRecieveEmails;
			LastNameBox.Text = SeekerInContext.User.Name.LastName;
			MidNameBox.Text = SeekerInContext.User.Name.MiddleName;
			FirstNameBox.Text = SeekerInContext.User.Name.FirstName;
			AddressLine1Box.Text = SeekerInContext.Address.Street;
			AddressLine2Box.Text = SeekerInContext.Address.Street2;
            if (null != SeekerInContext.Address.State)
            {
                StateDropDown.SelectedValue = SeekerInContext.Address.State.Abbreviation;
                if (SeekerInContext.Address.State.Abbreviation== State.WASHINGTON_STATE_ID)
                {
                     
                    CityDropDown.SelectedValue = SeekerInContext.Address.City.Id.ToString() ;
                    CountyDropDown.SelectedValue = SeekerInContext.County.Id.ToString() ;
                     
                }
                else
                {
                     
                    CityTextBox.Text = SeekerInContext.AddressCity;
                    CountyTextBox.Text = SeekerInContext.AddressCounty;

                }
            }

            
		   	ZipBox.Text = SeekerInContext.Address.PostalCode;
            
            PhoneBox.Text = SeekerInContext.Phone == null ? "" : SeekerInContext.Phone.FormattedPhoneNumber;
            MobilePhoneBox.Text = SeekerInContext.MobilePhone == null ? "" : SeekerInContext.MobilePhone.FormattedPhoneNumber;

            if (SeekerInContext.DateOfBirth.HasValue)
		        DobControl.Text = SeekerInContext.DateOfBirth.Value.ToString("MM/dd/yyyy");

			ReligionCheckboxList.SelectedValues = SeekerInContext.Religions.Cast<ILookup>().ToList();
			EthnicityControl.SelectedValues = SeekerInContext.Ethnicities.Cast<ILookup>().ToList();
			OtherReligion.Text = SeekerInContext.ReligionOther;
			GenderButtonList.SelectedIndex = (SeekerInContext.Gender.Equals(Genders.Male) ? 0 : (SeekerInContext.Gender.Equals(Genders.Female) ? 1 : -1));
			OtherEthnicity.Text = SeekerInContext.EthnicityOther;
            ESLELLCheckBoxControl.Checked = SeekerInContext.IsEslEll;
		}
		
		public override void PopulateObjects()
		{
			SeekerInContext.User.Name.LastName = LastNameBox.Text;
			SeekerInContext.User.Name.MiddleName = MidNameBox.Text;
			SeekerInContext.User.Name.FirstName = FirstNameBox.Text;
			SeekerInContext.Address.Street = AddressLine1Box.Text;
			SeekerInContext.Address.Street2 = AddressLine2Box.Text;
			SeekerInContext.Address.State = StateDropDown.SelectedValue == string.Empty 
															? null 
															: StateService.FindByAbbreviation(StateDropDown.SelectedValue);

            if (null != SeekerInContext.Address.State)
            {
				if (SeekerInContext.Address.State.Abbreviation == State.WASHINGTON_STATE_ID)
                {

                    SeekerInContext.Address.City =  CityDAL.FindById(Convert.ToInt32( CityDropDown.SelectedValue));
                    SeekerInContext.County = CountyDAL.FindById(Convert.ToInt32( CountyDropDown.SelectedValue));
                }
                else
                {
                    SeekerInContext.AddressCity = CityTextBox.Text;
                    SeekerInContext.AddressCounty = CountyTextBox.Text;
                }
            }
            
			SeekerInContext.Address.PostalCode = ZipBox.Text;
            SeekerInContext.Phone = new PhoneNumber(PhoneBox.Text);
            SeekerInContext.MobilePhone = new PhoneNumber(MobilePhoneBox.Text);
		    SeekerInContext.User.IsRecieveEmails = UseEmailBox.Checked;
		    DateTime dob;
		    if (DateTime.TryParse(DobControl.Text, out dob))
		    {
		        SeekerInContext.DateOfBirth = dob;
		    }
            else
		    {
                SeekerInContext.DateOfBirth = null;
		    }

			PopulateList(EthnicityControl, SeekerInContext.Ethnicities);
			PopulateList(ReligionCheckboxList, SeekerInContext.Religions);
			SeekerInContext.Gender = GenderButtonList.SelectedValue == string.Empty ? Genders.Unspecified : (Genders) int.Parse(GenderButtonList.SelectedValue) + 1;
			SeekerInContext.EthnicityOther = OtherEthnicity.Text;
			SeekerInContext.ReligionOther = OtherReligion.Text;

            SeekerInContext.IsEslEll = ESLELLCheckBoxControl.Checked;
		}

		private void SetCountyAndCityValidators()
		{
			if (StateDropDown.SelectedValue == State.WASHINGTON_STATE_ID)
			{
				citySelectRequired.Enabled = true;
				cityRequired.Enabled = false;
				countySelectRequired.Enabled = true;
				countyRequired.Enabled = false;
			}
			else
			{
				citySelectRequired.Enabled = false;
				cityRequired.Enabled = true;
				countySelectRequired.Enabled = false;
				countyRequired.Enabled = true;
			}
		}

		public void SetStateControls()
        {
            var script = "$('#" + StateDropDown.ClientID + "').change();";
            var scriptname = "RunSetWashigtonStateDropownEvent_scripts";
            var page = Page ;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
        }

        private void SetWashigtonStateDropownEvent()
        {
            var scriptname = "SetWashigtonStateDropownEvent_scripts";
            var script = "$('#" + StateDropDown.ClientID + "').change(function(e) { return CheckWashingtonState('" + StateDropDown.ClientID + "', '" + CityDropDownPanel.ClientID + "','" + CountyDropDownPanel.ClientID + "','" + cityTextPanel.ClientID + "' ,'" + countyTextPanel.ClientID + "'); });";

            var page = Page; // HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
        }
        
	    private static void PopulateList<T>(LookupItemCheckboxList checkboxList, IList<T> list)
		{
			checkboxList.PopulateListFromSelectedValues(list);
		}

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
			PopulateObjects();
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
			SeekerService.Update(SeekerInContext);
		}
		#endregion
	}
}