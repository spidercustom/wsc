﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="ScholarshipDetails.aspx.cs" Inherits="ScholarBridge.Web.ScholarshipDetails" Title="Scholarship" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="<%= MetaDescription %>" />
    <meta name="Keywords" content="<%= MetaKeywords %>" />   
    <style type="text/css">
        #Navigation, #Search{display:none;}
        h2{color:Black; margin-bottom:4px;}
        #Subtitle{margin-top:0px;}
        div.attribute{font-size:12px; clear:left; padding:7px 0px;}
        div.attribute > div{float:left;}

        /* Title child of attribute (first child)*/
        div.attribute > div:first-child{text-align:right; width:135px; margin-right:10px; font-weight:bold;}

        /* Content child of attribute (second child)*/
        div.attribute > div:first-child + div{width:670px; color:Black;}
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageImage" Runat="Server">
    <div style="width:873px; background-color:#7dc0de; height:25px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Body" Runat="Server">
    <h2><asp:literal ID="scholarship_name" runat="server"/> <span style="font-weight:normal; color:Gray;"></span></h2>
    <p id="Subtitle">Provided by <asp:literal ID="scholarship_provider" runat="server" /></p>
    <div class="attribute">
        <div>Description:</div>
        <div><em><asp:literal ID="scholarship_description" runat="server"/></em></div>
    </div> 
    <div class="attribute" ID="donor_row" runat="server">
        <div>Donor:</div>
        <div><asp:literal ID="scholarship_donor" runat="server" /></div>
    </div>       
    <div class="attribute">
        <div>Academic Year:</div>
        <div><asp:literal ID="scholarship_year" runat="server" /></div>
    </div>
    <div class="attribute">
        <div>Scholarship Amount:</div>
        <div><asp:literal ID="scholarship_amount" runat="server" /></div>
    </div>
    <div class="attribute">
        <div>Number of Awards:</div>
        <div><asp:literal ID="scholarship_awards" runat="server"/></div>
    </div>
    <div class="attribute">
        <div>Application Start Date:</div>
        <div><asp:literal ID="scholarship_app_start" runat="server"/></div>
    </div>
    <div class="attribute">
        <div>Application Due Date:</div>
        <div><asp:literal ID="scholarship_app_due" runat="server"/></div>
    </div>

    <p style="margin-top:35px;">
    TheWashBoard.org is a free web-based tool for Washington students seeking college scholarships.  We make 
    scholarship searching simple.  Create a profile and review your scholarship matches.  Register to apply for this and other scholarships at <a id="A1" href="http://www.theWashBoard.org" runat="server" >theWashBoard</a>.    
    </p>
</asp:Content>
