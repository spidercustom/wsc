﻿<%@ Page Title="Provider" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Default" %>
<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div>
        <asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderHome.gif" Width="918px" Height="15px"
            runat="server" /></div>
    <div>
        <asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderHome.gif" Width="918px"
            Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <!--Left floated content area starts here-->
   
    <div class="HomeContent">
        <img src="<%= ResolveUrl("~/images/PgTitle_SmarterSchoMatches.gif") %>" width="343px"
            height="28px"/>
        <p class="HighlightedTextMain">
            TheWashBoard.org makes scholarship searching simple.&nbsp;
            In one stop, students can search and apply for vetted scholarships 
            specific to their academic interests, college or university, or other criteria.
        </p>
        <%--<input type="image" src="<%= ResolveUrl("~/images/Btn_GotoMyProfile.gif") %>" width="145px"
            height="32px" action="submit">--%>
    </div>
    <!--Left floated content area ends here-->
    <!--Right Floated content area starts here-->
    <%--<div id="HomeContentRight">
                      <img src="<%= ResolveUrl("~/images/PageTitleSmall_ScholarshipCompleteness.gif") %>" width="181px" height="28px">
                      <div id="PercentageGraphic"></div>
                      <img src="<%= ResolveUrl("~/images/Percentile.gif") %>" width="248px" height="37px">
                      <P class="HighlightedTextRightContent">You’re almost there! We just need a bit more info about the scholarship.</P>
                      <P class="RightContentGreenText">>&nbsp;Finish Scholarship Info</P>
                 </div>--%>
    <!--Right Floated content area ends here-->
    <br />
    <br />
    <div id="Clear">
    </div>
    <hr>
    <div style='clear: both;'>
    </div>
    <!--The Three column starts here-->
    <div id="BoxWrapper">
        <div id="LeftBottomBox">
            <a href="<%= ResolveUrl("~/Provider/BuildScholarship") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox01_BuildScholarships.gif") %>" width="262px"
                    height="51px"/>
                <img src="<%= ResolveUrl("~/images/ProviderBottomBox01.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="display:block; height:3.1em;">
                We guide you through a simple step by step process to 
                define your scholarships unique criteria and application requirements.</p>
            <p class="IcoBoxArrow">
                <a href="<%= ResolveUrl("~/Provider/BuildScholarship") %>">
                    <img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;Start
                    Building</a></p>
        </div>
        <div id="CenterBottomBox">
            <a href="<%= ResolveUrl("~/Provider/Scholarships") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox02_ManageApplications.gif") %>" width="262px"
                    height="51px"/>
                <img src="<%= ResolveUrl("~/images/ProviderBottomBox02.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="display:block; height:3.1em;">
                We make reviewing and evaluating applications easy.&nbsp; 
                Review on line, or download applicant information. </p>
            <p class="IcoBoxArrow">
                <a href="<%= ResolveUrl("~/Provider/Scholarships") %>">
                    <img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;Start
                    Reviewing</a></p>
        </div>
        <div id="RightBottomBox">
            <a href="<%= ResolveUrl("~/Provider/Scholarships") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox03_AwardScholarship.gif") %>" width="262px"
                    height="51px"/>
                <img src="<%= ResolveUrl("~/images/ProviderBottomBox03.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="display:block; height:3.1em;">
                Designate finalists for further consideration.&nbsp; 
                Extend offers and notify applicants when scholarship is awarded.</p>
            <p class="IcoBoxArrow">
                <a href="<%= ResolveUrl("~/Provider/Scholarships") %>">
                    <img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;View
                    Applicants</a></p>
        </div>
    </div>
    <sb:Login ID="loginForm" runat="server" />
    <asp:LoginView ID="loginView" runat="server">
        <AnonymousTemplate>
            <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Provider/RegisterProvider.aspx">Register as a Scholarship Provider</asp:HyperLink>
        </AnonymousTemplate>
    </asp:LoginView>
</asp:Content>
