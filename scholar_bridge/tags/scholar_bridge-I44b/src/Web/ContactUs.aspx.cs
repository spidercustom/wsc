﻿using System;


namespace ScholarBridge.Web
{
	public partial class ContactUs : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				SetMessagePanelInvisible();
			}
		}

		private void SetMessagePanelInvisible()
		{
			contactUsMessagePanel.Visible = false;
			contactUsSelectionPanel.Visible = true;
		}

		protected void supportMessageLink_Click(object sender, EventArgs e)
		{
			ShowMessagePanel(Common.ContactUs.ContactUsMessageType.Support);
		}

		protected void problemMessageLink_Click(object sender, EventArgs e)
		{
			ShowMessagePanel(Common.ContactUs.ContactUsMessageType.Problem);
		}

		protected void suggestionMessageLink_Click(object sender, EventArgs e)
		{
			ShowMessagePanel(Common.ContactUs.ContactUsMessageType.Suggestion);
		}

		private void ShowMessagePanel(Common.ContactUs.ContactUsMessageType messageType)
		{
			contactUsMessagePanel.Visible = true;
			contactUsSelectionPanel.Visible = false;
			contactUsControl.ResetMessage = true;
			contactUsControl.MessageType = messageType;
		}

		protected void contactUsControl_Sent()
		{
			SetMessagePanelInvisible();
			Server.Transfer("~/ContactUs.aspx");
		}

		protected void contactUsControl_Canceled()
		{
			SetMessagePanelInvisible();
		}
	}
}
