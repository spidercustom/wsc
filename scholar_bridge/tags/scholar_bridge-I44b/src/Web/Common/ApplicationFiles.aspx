﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplicationFiles.aspx.cs" Inherits="ScholarBridge.Web.Common.ApplicationFiles" %>
<%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Application Files</title>
    <link rel="stylesheet" href="~/styles/main.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="~/styles/printable.css" type="text/css" media="print"  />
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
    <div id="content">
    <h4>Application Attachments</h4>
    <sb:FileList id="applicationAttachments" runat="server" />
    
    <asp:LinkButton ID="downloadAllBtn" runat="server" Text="Download All" OnClick="downloadAllBtn_OnClick" />
    
    </div>
    </div>
    </form>
</body>
</html>
