﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class ProviderRelationshipList : UserControl
    {
        public IList<Relationship> Relationships { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IUserContext UserContext { get; set; }
        private const string DEFAULT_PAGEURL = "~/Provider/Admin/#relationship-tab";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Bind();
        }

        protected void Bind()
        {
            lstRelationships.DataSource = (from r in Relationships orderby r.Intermediary.Name select r).ToList();
            lstRelationships.DataBind();
        }

        protected void InactivateBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (!e.DialogResult) return;
            var btn = (ConfirmButton)sender;
            var  id = int.Parse(btn.Attributes["value"]);
            var relationship = RelationshipService.GetById(id);
            if (!(relationship == null))
            {
                relationship.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                RelationshipService.InActivate(relationship);
                SuccessMessageLabel.SetMessage("Relationship Inactivated.");
                Response.Redirect(DEFAULT_PAGEURL);
            }
        }

        protected void lstRelationships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) 
                return;

            var relationship = ((Relationship) ((ListViewDataItem) e.Item).DataItem);
            var btnInactivate = (ConfirmButton)e.Item.FindControl("InactivateBtn");
            if (relationship.Status == RelationshipStatus.Active)
            {
                btnInactivate.Attributes.Add("value", relationship.Id.ToString());
                btnInactivate.Visible = true;
            }
            else
            {
                btnInactivate.Visible = false;
            }

            SetLabel(e, "lblName", relationship.Intermediary.Name);
            SetLabel(e, "lblStatus", relationship.Status.GetDisplayName());
            if (!(relationship.Intermediary.Phone ==null))
            SetLabel(e, "lblPhone", relationship.Intermediary.Phone.FormattedPhoneNumber);

            var admin = relationship.Intermediary.AdminUser;
            SetLabel(e, "lblAdminName", admin.Name.ToString());
            SetLabel(e, "lblAdminEmail", admin.Email);
        }

        private static void SetLabel(ListViewItemEventArgs e, string controlName, string value)
        {
            var lbl = (Literal)e.Item.FindControl(controlName);
            lbl.Text = value;
        }

        protected void lstRelationships_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
        }
    }
}