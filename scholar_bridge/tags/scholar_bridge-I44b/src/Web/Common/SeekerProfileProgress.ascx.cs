﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    
    public partial class SeekerProfileProgress: UserControl
    {
         
         
        public UserContext UserContext { get; set; }
         protected override void OnPreRender(EventArgs e)
        {
             
            var seeker = UserContext.CurrentSeeker;
            if (!(seeker == null))
            {
                 
                var percent = seeker.GetPercentComplete();
                 
            //these messages will be further refactored to come from database on a random basis
            
            if (percent <19)
            {
                ProfileCompletnessMessage.Text= "Complete as much of your profile as possible to ensure you are matched with scholarships you may qualify for.";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness10Percent.gif";
            }
            else if (percent < 29)
            {
                ProfileCompletnessMessage.Text =
                    "Tell us what Colleges you are considering in the My Academic Info tab to find scholarships for those schools!";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness20Percent.gif";
            }
            else if (percent < 39)
            {
                ProfileCompletnessMessage.Text =
                    "Scholarship Matches are displayed under My Matches. Add more info to your profile and see how it impacts your scholarship matches!";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness30Percent.gif";
            }
            else if (percent < 49)
            {
                ProfileCompletnessMessage.Text =
                    "Complete the My Activities tab to get matched with scholarships for specific Fields of Study or Careers.";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness40Percent.gif";
            }
            else if (percent < 59)
            {
                ProfileCompletnessMessage.Text =
                    "The more information you include in your profile, the more likely you are to get matched with scholarships you qualify for!";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness50Percent.gif";
            }
            else if (percent < 69)
            {
                ProfileCompletnessMessage.Text =
                    "Tell us what Colleges you are considering in the My Academic Info tab to find scholarships for those schools!";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness60Percent.gif";
            }
            else if (percent < 79)
            {
                ProfileCompletnessMessage.Text =
                    "Many Scholarships are awarded based on Financial Need. Complete the My Financial Need tab to get matched with these scholarships!";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness70Percent.gif";
            }
            else if (percent < 89)
            {
                ProfileCompletnessMessage.Text =
                    "Scholarship Matches are displayed under My Matches. Add more info to your profile and see how it impacts your scholarship matches!";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness80Percent.gif";
            }
            else if (percent < 95)
            {
                ProfileCompletnessMessage.Text =
                    "The more information you include in your profile, the more likely you are to get matched with scholarships you qualify for.";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness90Percent.gif";
            }
            else if (percent < 101)
            {
                ProfileCompletnessMessage.Text =
                    "Great job! By completing your profile, you ensure that your matches will include all the scholarships that you meet some of the eligibility criteria.";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness100Percent.gif";
            }
            else
            {
                ProfileCompletnessMessage.Text =
                    "Complete as much of your profile as possible to ensure you are matched with scholarships you may qualify for.";
                ProfileCompletnessImage.ImageUrl = "~/images/ProfileCompleteness10Percent.gif";
            }


            }
            else
            {
                Visible = false;
            }
             
        }
         
       public void ProfileCompletnessImage_Click(object sender, EventArgs e)
       {
           Response.Redirect("~/seeker/Profile");
       }

       
         
    }
}