﻿using System;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Common
{
    public partial class RegisterOrganization : UserControl
    {
        private const int VIEW_USER = 1;
        private const int VIEW_COMPLETION = 2;
        public IProviderService ProviderService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        public IStateDAL StateService { get; set; }
        public string Title { get; set; }
        public Type OrganizationType { get; set; }

        private LinkGenerator linkGenerator;

        protected LinkGenerator LinkGenerator
        {
            get
            {
                if (linkGenerator == null)
                    linkGenerator = new LinkGenerator();
                return linkGenerator;
            }
        }
         
        protected void Page_Load(object sender, EventArgs e)
        {
            pageTitle.Text = Title;
            if (!Page.IsPostBack)
            {
                AddressState.DataSource = StateService.FindAll();
                AddressState.DataTextField = "Name";
                AddressState.DataValueField = "Abbreviation";
                AddressState.DataBind();
                AddressState.Items.Insert(0, new ListItem("- Select One -", ""));
                CaptchaControl1.ValidationGroup = "CreateUserWizard1";
            }
        }

        private static PhoneNumber GetPhone(ITextControl phoneBox)
        {
            string phone = phoneBox.Text;
            if (String.IsNullOrEmpty(phone))
                return null;

            return new PhoneNumber(phone);
        }

        private Address GetAddress()
        {
            string street = AddressStreet.Text;
            string street2 = AddressStreet2.Text;
            string city = AddressCity.Text;
            string postalCode = AddressPostalCode.Text;
            string state = AddressState.SelectedValue;

            return new Address
                       {
                           Street = street,
                           Street2 = street2,
                           City = city,
                           PostalCode = postalCode,
                           State = StateService.FindByAbbreviation(state)
                       };
        }

        protected void ValidateWebsite_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Website.Text))
                ClientSideDialogs.ShowAlert("Cannot validate empty webiste. Please enter website and try again.", "");
            else
                PopupHelper.OpenURL(LinkGenerator.AddHttpPrefix(Website.Text));
        }

        private void ShowErrorPopup()
        {
            ClientSideDialogs.ShowDivAsDialog("ValidationErrorPopup");
        }

        protected void CompleteRegistration_Click(object sender, EventArgs e)
        {
            
            Page.Validate("CreateUserWizard1");
            if (!Page.IsValid)
            {
                CaptchaControl1.ClearGuess();
                ShowErrorPopup();
                return;
            }

            if (!WebSiteIsValid())
            {
                ClientSideDialogs.ShowAlert("Could not reach the organization website. Please verify the URL and try again.", "");
                return;
            }
            CreateOrganizationRecord();
        }

         
        protected void RegisterButton_Click(object sender, EventArgs e)
        {

            if (!Page.IsValid)
            {
                CaptchaControl1.ClearGuess();
                ShowErrorPopup();
                return;
            }

            CreateUserWizard.ActiveViewIndex = VIEW_USER;
        }

        private void CreateOrganizationRecord()
        {
            var provider = new SpiderMembershipProvider();
            provider.Initialize(string.Empty, new NameValueCollection());
            var user = provider.CreateUserInstance(UserName.Text, Password.Text , UserName.Text, null, null, false);

            if (user == null)
            {
                switch (provider.CreateUserStatus)
                {
                    case MembershipCreateStatus.InvalidEmail:
                        EmailCustomValidator.Text = "Email Address is Invalid";
                        EmailCustomValidator.IsValid = false;
                        ShowErrorPopup();
                        return;
                    case MembershipCreateStatus.DuplicateEmail:
                        EmailCustomValidator.Text = "Email Address is already in use by another user";
                        EmailCustomValidator.IsValid = false;
                        ShowErrorPopup();
                        return;
                    case MembershipCreateStatus.DuplicateUserName:
                        EmailCustomValidator.Text = "Email Address is already in use by another user";
                        EmailCustomValidator.IsValid = false;
                        ShowErrorPopup();
                        return;
                    default:
                        throw new ApplicationException("Unhandled User Creation status event: " + provider.CreateUserStatus);
                }
            }
            
            user.Name.FirstName = FirstName.Text;
            user.Name.MiddleName = MiddleName.Text;
            user.Name.LastName = LastName.Text;

            var org = (Organization)Activator.CreateInstance(OrganizationType);
            org.Name = Name.Text;
            org.TaxId = TaxId.Text;
            org.Website = String.IsNullOrEmpty(Website.Text.Trim())
                              ? null
                              : LinkGenerator.AddHttpPrefix(Website.Text);
            org.Address = GetAddress();
            user.Phone = org.Phone = GetPhone(Phone);
            user.Fax = org.Fax = GetPhone(Fax);
            user.OtherPhone = org.OtherPhone = GetPhone(OtherPhone);
             
            org.LastUpdate = new ActivityStamp(user);

            try
            {
                if (org is Domain.Provider)
                    ProviderService.SaveNewWithAdminUser((Domain.Provider) org, user);
                else  
                    IntermediaryService.SaveNewWithAdminUser((Domain.Intermediary) org, user);
            }
            catch (SmtpException)
            {
                emailSuccess.Visible = false;
                emailFail.Visible = true;
            }

            CreateUserWizard.ActiveViewIndex = VIEW_COMPLETION;
        }

        /// <summary>
        /// validate that the entered website exists
        /// </summary>
        /// <returns></returns>
        private bool WebSiteIsValid()
        {
            if (String.IsNullOrEmpty(Website.Text.Trim()))
            {
                return true;
            } // not entered, so skip validation

            if (Website.Text.Length > 128)
            {
                WebSiteCustomValidator.IsValid = false;
                WebSiteCustomValidator.ErrorMessage = "Online Application Url not to exceed 128 characters.";
                return false;
            }

            Website.Text = LinkGenerator.AddHttpPrefix(Website.Text);

            string webError;
            if (!LinkTester.IsLinkValid(Website.Text, out webError))
            {
                WebSiteCustomValidator.IsValid = false;
                WebSiteCustomValidator.ErrorMessage = string.Format("url '{0}' is not valid or cannot be accessed.<br />Web Message: {1}", Website.Text, webError);
                return false;
            }
            return true;
        }
    }
}