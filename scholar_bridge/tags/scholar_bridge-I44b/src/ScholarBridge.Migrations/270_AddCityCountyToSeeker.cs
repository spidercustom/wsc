﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(270)]
    public class AddCityCountyToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("AddressCity", DbType.String,  50));
            Database.AddColumn(TABLE_NAME, new Column("AddressCounty", DbType.String,  50));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "AddressCity");
            Database.RemoveColumn(TABLE_NAME, "AddressCounty");
            
        }
    }
}