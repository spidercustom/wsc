using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(93)]
    public class AddSentMessagesTable : AddTableBase
    {
        public const string TABLE_NAME = "SBSentMessage";
        public const string PRIMARY_KEY_COLUMN = "SBSentMessageID";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("Subject", DbType.String, 100, ColumnProperty.NotNull),
                           new Column("Content", DbType.String, 8000, ColumnProperty.NotNull),
                           new Column("SentDate", DbType.DateTime, ColumnProperty.NotNull),
                           new Column("ToUserId", DbType.Int32, ColumnProperty.Null),
                           new Column("ToRoleId", DbType.Int32, ColumnProperty.Null),
                           new Column("ToOrganizationId", DbType.Int32, ColumnProperty.Null),
                           new Column("FromUserId", DbType.Int32, ColumnProperty.Null),
                           new Column("FromRoleId", DbType.Int32, ColumnProperty.Null),
                           new Column("FromOrganizationId", DbType.Int32, ColumnProperty.Null),
                           new Column("MessageTemplate", DbType.String, 50, ColumnProperty.Null),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())")
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new[]
                       {
                           CreateForeignKey("ToUserId", "SBUser", "SBUserID"),
                           CreateForeignKey("FromUserId", "SBUser", "SBUserID"),
                           CreateForeignKey("ToRoleId", "SBRoleLUT", "SBRoleIndex"),
                           CreateForeignKey("FromRoleId", "SBRoleLUT", "SBRoleIndex"),
                           CreateForeignKey("ToOrganizationId", "SBOrganization", "SBOrganizationID"),
                           CreateForeignKey("FromOrganizationId", "SBOrganization", "SBOrganizationID"),
                           CreateForeignKey("LastUpdateBy", "SBUser", "SBUserID")
                       };
        }
    }
}