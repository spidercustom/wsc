using System.Collections.Generic;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;
using Spider.Common.Extensions.System.Collections;

namespace ScholarBridge.Migrations
{
    [Migration(57)]
    public class UpdateSupportValues : Migration
    {
        public const string TABLE_NAME = "SBSupportLUT";
        public static readonly string[] INSERT_COLUMNS = { "SBSupport", "Description", "SupportType", "LastUpdateBy" };

        public readonly List<string> NEW_COLUMNS = new List<string>
                                                       {
                                                           "Support for Tuition",
                                                           "Support for Mandatory Fees",
                                                           "Support for Room and Board",
                                                           "Support for Books"
                                                       };
        public readonly List<string> DEPRECATED_COLUMNS = new List<string>
                                                       {
                                                           "Support for tuition only",
                                                           "Support for tuition, room, board, fees and books"
                                                       };
        public override void Up()
        {
            InsertValues(NEW_COLUMNS);
            DeleteValues(DEPRECATED_COLUMNS);
        }

        public override void Down()
        {
            DeleteValues(NEW_COLUMNS);
            InsertValues(DEPRECATED_COLUMNS);
        }

        private void InsertValues(IEnumerable<string> names)
        {
            int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
            names.Each(
                columnName =>
                Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { columnName, "", "Traditional", adminId.ToString() }));
        }

        private void DeleteValues(IEnumerable<string> names)
        {
            names.Each(
                columnName =>
                {
                    Database.ExecuteNonQuery(string.Format("DELETE FROM SBScholarshipSupportRT WHERE SBSupportIndex=(SELECT SBSupportIndex FROM SBSupportLUT WHERE SBSupport='{0}')", columnName));
                    Database.Delete(TABLE_NAME, "SBSupport", columnName);
                });
        }
    }
}