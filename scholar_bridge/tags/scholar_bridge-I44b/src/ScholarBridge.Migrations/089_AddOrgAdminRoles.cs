using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(89)]
    public class AddOrgAdminRoles : Migration
    {
        private const string TABLE_NAME = "SBRoleLUT";
        private readonly string[] COLUMNS = new[] {"SBRole", "LastUpdateBy"};
        private readonly string[] ROLE_NAMES = new[] { "Provider Admin", "Intermediary Admin" };

        public override void Up()
        {
            int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
            foreach (var name in ROLE_NAMES)
            {
                Database.Insert(TABLE_NAME, COLUMNS, new[] { name, adminId.ToString() });
            }
        }

        public override void Down()
        {
            foreach (var name in ROLE_NAMES)
            {
                Database.Delete(TABLE_NAME, "SBRole", name);
            }
        }
    }
}