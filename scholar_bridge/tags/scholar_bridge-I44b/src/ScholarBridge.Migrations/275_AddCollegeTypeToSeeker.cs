﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(275)]
    public class AddCollegeTypeToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public readonly string[] NEW_COLUMNS
            = { "TypeOfCollegeStudent" };

        public override void Up()
        {
      

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Int32, ColumnProperty.Null);
           
        }

        public override void Down()
        {
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}