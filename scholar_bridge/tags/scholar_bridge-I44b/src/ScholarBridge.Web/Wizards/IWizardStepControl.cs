﻿
namespace ScholarBridge.Web.Wizards
{
    /// <summary>
    /// Wizard interface is used for communicating with wizard step
    /// by wizard step manager
    /// </summary>
    public interface IWizardStepControl<T>
    {
        bool ValidateStep();
        void PopulateObjects();
        /// <summary>
        /// Saves changes to retrival storage.
        /// </summary>
        void Save();
        IWizardStepsContainer<T> Container { get; set; }
        void Activated();
    }
}
