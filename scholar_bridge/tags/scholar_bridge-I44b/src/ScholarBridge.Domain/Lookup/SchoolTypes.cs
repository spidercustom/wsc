﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SchoolTypes
    {
        [DisplayName("Private 4-year University or College")]
        [DisplayOrder(1)]
        PrivateUniversityCollege = 1,
        [DisplayName("Private Vocational / Technical / Career College")]
        [DisplayOrder(2)]
        PrivateVocationalOrTechnical = 2,
        [DisplayName("Public 4-year University or College")]
        [DisplayOrder(3)]
        PublicUniversityOrCollege = 4,
        [DisplayName("Public 2-year Community / Technical College")]
        [DisplayOrder(4)]
        PublicCommunityORTechnicalCollege = 8,
        [DisplayName("Seminary")]
        [DisplayOrder(5)]
        Seminary = 16
    }
}
