using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class MatchDALTests : TestBase
    {
        public MatchDAL MatchDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public SeekerDAL SeekerDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        public ApplicationDAL ApplicationDAL { get; set; }

        private Seeker seeker;
        private Scholarship scholarship1;
		private Scholarship scholarship2;
		private Provider provider;
    	private User user;


        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            seeker = SeekerDALTest.InsertSeeker(SeekerDAL, user, StateDAL);
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "MatchTest Provider", user);
        	scholarship1 = CreateActiveScholarship();
        	scholarship2 = CreateInactiveScholarship();
        }

		private Scholarship CreateInactiveScholarship()
		{
			return ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStages.NotActivated));
		}

		private Scholarship CreateActiveScholarship()
		{
			return ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStages.Activated));
		}

        [Test]
        public void can_create_match()
        {
            var m = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            Assert.IsNotNull(m);
            Assert.AreEqual(2, m.SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, m.SeekerPreferredCriteriaCount);
        }

        [Test]
        public void can_find_all()
        {
            CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            CreateMatch(seeker, scholarship2, 2, 3, 4, 6);

            var matches = MatchDAL.FindAll(seeker);
            Assert.IsNotNull(matches);
            Assert.AreEqual(2, matches.Count);
        }

        [Test]
        [Ignore("Not sure why this isn't working on CI.")]
        public void can_update_matches_from_seeker()
        {
            MatchDAL.UpdateMatches(seeker);
            var initialCount = MatchDAL.FindAll(seeker).Count;
            ScholarshipDAL.Flush();

            scholarship1.SeekerProfileCriteria.GPA = new RangeCondition<double?>(2.0d, 5d);
            ScholarshipDAL.Update(scholarship1);
            ScholarshipDAL.Flush();

            MatchDAL.UpdateMatches(seeker);

            var matches = MatchDAL.FindAll(seeker);
            Assert.IsNotNull(matches);
            Assert.AreEqual(initialCount + 1, matches.Count);
        }

        [Test]
        [Ignore("Not sure why this isn't working")]
        public void can_update_matches_from_scholarship()
        {
            MatchDAL.UpdateMatches(seeker);
            var initialCount = MatchDAL.FindAll(seeker).Count;
            ScholarshipDAL.Flush();

            scholarship1.SeekerProfileCriteria.GPA = new RangeCondition<double?>(2.0d, 5d);
            ScholarshipDAL.Update(scholarship1);
            ScholarshipDAL.Flush();

            MatchDAL.UpdateMatches(scholarship1);

            var matches = MatchDAL.FindAll(seeker);
            Assert.IsNotNull(matches);
            Assert.AreEqual(initialCount + 1, matches.Count);
        }

        [Test]
        public void can_get_saved_matches_by_seeker()
        {
            var m1 = CreateMatch(seeker, scholarship1, 4, 4, 4, 4);
            var m = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            m.MatchStatus = MatchStatus.Saved;
            MatchDAL.Update(m);

            var found = MatchDAL.FindAll(seeker, MatchStatus.Saved);
            Assert.IsNotNull(found);
            Assert.AreEqual(2, found[0].SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, found[0].SeekerPreferredCriteriaCount);
        }

        [Test]
        public void can_get_saved_matches_by_seeker_and_scholarship()
        {
            var m1 = CreateMatch(seeker, scholarship1, 4, 4, 4, 4);

            var found = MatchDAL.Find(seeker, scholarship1.Id);
            Assert.IsNotNull(found);
            Assert.AreEqual(m1.SeekerMinimumCriteriaCount, found.SeekerMinimumCriteriaCount);
            Assert.AreEqual(m1.SeekerPreferredCriteriaCount, found.SeekerPreferredCriteriaCount);
        }

        [Test]
        public void can_get_saved_and_applied_matches_by_seeker()
        {
            SetupGetSavedAndApplied();

            var found = MatchDAL.FindAll(seeker, new[] {MatchStatus.Saved, MatchStatus.Applied} );
            Assert.IsNotNull(found);
            Assert.AreEqual(2, found.Count);

            //not sure in what order they will come
            Assert.AreEqual(2, found[0].SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, found[0].SeekerPreferredCriteriaCount);

            Assert.AreEqual(2, found[1].SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, found[1].SeekerPreferredCriteriaCount);
        }

        [Test]
        public void can_get_saved_and_applied_matches_by_scholarship()
        {
            SetupGetSavedAndApplied();

            var found = MatchDAL.FindAll(scholarship1, new[] { MatchStatus.Saved, MatchStatus.Applied });
            Assert.IsNotNull(found);
            Assert.AreEqual(2, found.Count);

            //not sure in what order they will come
            Assert.AreEqual(2, found[0].SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, found[0].SeekerPreferredCriteriaCount);

            Assert.AreEqual(2, found[1].SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, found[1].SeekerPreferredCriteriaCount);
        }

        private void SetupGetSavedAndApplied()
        {
            var m1 = CreateMatch(seeker, scholarship1, 4, 4, 4, 4);

            var m2 = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            m2.MatchStatus = MatchStatus.Saved;
            MatchDAL.Update(m2);

            var m3 = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            m3.MatchStatus = MatchStatus.Applied;
            MatchDAL.Update(m3);
        }


        [Test]
        public void can_get_new_matches()
        {
            var m1 = CreateMatch(seeker, scholarship1, 4, 4, 4, 4);
            var m = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            m.MatchStatus = MatchStatus.Saved;
            MatchDAL.Update(m);

            var found = MatchDAL.FindAll(seeker, MatchStatus.New);
            Assert.IsNotNull(found);
            Assert.AreEqual(4, found[0].SeekerMinimumCriteriaCount);
            Assert.AreEqual(4, found[0].SeekerPreferredCriteriaCount);
        }


		[Test]
		public void can_get_matches_with_applications()
		{
			var initialCount = MatchDAL.FindAllWithApplications(scholarship1, new[] { MatchStatus.New, MatchStatus.Saved, MatchStatus.Applied }).Count;

			var m1 = CreateMatch(seeker, scholarship1, 4, 4, 4, 4);
			var m2 = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
			var app = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship1, seeker, StateDAL);
			m2.Application = app;
			MatchDAL.Update(m2);

			var found = MatchDAL.FindAllWithApplications(scholarship1, new[] { MatchStatus.New, MatchStatus.Saved, MatchStatus.Applied });
			Assert.AreEqual(initialCount + 1, found.Count);
		}

		[Test]
		public void can_get_matches_without_applications()
		{
			var initialCountAll = MatchDAL.FindAllCurrentMatches(seeker, false).Count;
			var initialCountNoApplications = MatchDAL.FindAllCurrentMatches(seeker, true).Count;
			var scholarship3 = CreateActiveScholarship();
			var scholarship4 = CreateActiveScholarship();

			CreateMatch(seeker, scholarship1, 4, 4, 4, 4);
			CreateMatch(seeker, scholarship4, 2, 3, 4, 6);
			var m2 = CreateMatch(seeker, scholarship3, 2, 3, 4, 6);

			var updatedCountAll = MatchDAL.FindAllCurrentMatches(seeker, false).Count;
			var updatedCountNoApplications = MatchDAL.FindAllCurrentMatches(seeker, true).Count;

			Assert.AreEqual(initialCountAll + 3, updatedCountAll);
			Assert.AreEqual(initialCountNoApplications + 3, updatedCountNoApplications);

			var app = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship3, seeker, StateDAL);
			m2.Application = app;
			MatchDAL.Update(m2);

			updatedCountAll = MatchDAL.FindAllCurrentMatches(seeker, false).Count;
			updatedCountNoApplications = MatchDAL.FindAllCurrentMatches(seeker, true).Count;

			Assert.AreEqual(initialCountAll + 3, updatedCountAll);
			Assert.AreEqual(initialCountNoApplications + 2, updatedCountNoApplications);

		}



		[Test]
		public void can_get_matches_by_application()
		{
			var m2 = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
			var app = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship1, seeker, StateDAL);
			m2.Application = app;
			MatchDAL.Update(m2);

			var found = MatchDAL.FindAll(app);
			Assert.AreEqual(1, found.Count);
		}

		[Test]
		public void assure_computerank_is_called_on_insert_and_update()
		{
			var m2 = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
			Assert.AreEqual(50, m2.Rank);
			m2.SeekerMinimumCriteriaCount = 4;
			m2.ScholarshipMinimumCriteriaCount = 5;
			m2.SeekerPreferredCriteriaCount = 1;
			m2.ScholarshipPreferredCriteriaCount = 4;
			Assert.AreEqual(50, m2.Rank);
			MatchDAL.Update(m2);
			Assert.AreEqual(61.85m, m2.Rank);
		}

		[Test]
        public void can_unset_application()
        {
            var m2 = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            var app = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship1, seeker, StateDAL);
            m2.Application = app;
            MatchDAL.Update(m2);

            var found = MatchDAL.FindAll(app);
            Assert.AreEqual(1, found.Count);

            m2.Application = null;
            MatchDAL.Update(m2);

            var found2 = MatchDAL.FindAll(app);
            Assert.AreEqual(0, found2.Count);
        }

        public Match CreateMatch(Seeker seeker, Scholarship scholarship, int seekerMinCount, int seekerPrefCount, int scholMinCount, int scholPrefCount)
        {

        	var m = new Match
        	        	{
        	        		Seeker = seeker,
        	        		Scholarship = scholarship,
        	        		AddedViaMatch = true,
        	        		SeekerMinimumCriteriaCount = seekerMinCount,
        	        		SeekerPreferredCriteriaCount = seekerPrefCount,
        	        		ScholarshipMinimumCriteriaCount = scholMinCount,
        	        		ScholarshipPreferredCriteriaCount = scholPrefCount,
							LastUpdate = new ActivityStamp(seeker.User)
						};
            return MatchDAL.Insert(m);
        }

        [Test]
        public void MatchApplicationRelationTest()
        {
            var match = CreateMatch(seeker, scholarship1, 1, 1, 1, 1);
            var application = ApplicationDALTests.InsertApplication(ApplicationDAL, scholarship1, seeker, StateDAL);

            match.Application = application;
            match.MatchStatus = MatchStatus.Applied;
            MatchDAL.Update(match);
            
            var retrievedMatch = MatchDAL.Find(seeker, scholarship1);
            Assert.IsNotNull(retrievedMatch.Application);
        }
    }
}