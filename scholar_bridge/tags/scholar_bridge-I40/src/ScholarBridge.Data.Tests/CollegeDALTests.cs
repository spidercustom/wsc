﻿using System;
using NHibernate.Exceptions;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class CollegeDALTests : LookupTestBase<LookupDAL<College>, College>
    {
		[Test]
		public void insert_with_schooltype()
		{
			College college = CreateLookupObject("blah" + DateTime.Now);

			college.SchoolType = SchoolTypes.Seminary;
			DAL.Insert(college);
			Assert.Greater((int)college.Id, 0);

			College college2 = DAL.FindById(college.Id);
			Assert.AreEqual(college.Name, college2.Name);

			DAL.Delete(college);
		}

	}
}