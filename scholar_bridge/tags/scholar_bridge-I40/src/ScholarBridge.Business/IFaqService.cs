﻿using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
	public interface IFaqService
	{
		Faq GetById(int id);

		Faq Save(Faq resource);
		void Delete(Faq resource);

		IList<Faq> FindAll();
		IList<Faq> FindAll(FaqType type);
	}
}