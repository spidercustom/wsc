﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Intermediary
{
    public partial class RegisterIntermediary : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            registerOrg.OrganizationType = typeof(Domain.Intermediary);
        }
    }
}
