﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.PressRoom.Default" Title="Admin" %>
<%@ Register src="~/PressRoom/ArticleList.ascx" tagname="ArticleList" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<h2>Press Room</h2>
<p  class="HighlightedTextMain">
<b>What’s new on <a class="GreenLink" href='<%= ResolveUrl("~/") %>'>theWashboard.org</a></b> 
 </p>
 <p class="HighlightedTextMain">News Releases and Announcements </p>
<style>
.GreenLinks a {
    color:#98c53c;
	text-decoration: none;
}
</style>
<div class="GreenLinks">
<sb:ArticleList ID="ArticleList1" runat="server" LinkTo="~/PressRoom/Show.aspx" />
</div>
</asp:Content>
