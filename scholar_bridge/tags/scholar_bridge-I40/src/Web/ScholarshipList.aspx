﻿<%@ Page Language="C#" MasterPageFile="~/Simple.Master" AutoEventWireup="true" CodeBehind="ScholarshipList.aspx.cs" Inherits="ScholarBridge.Web.ScholarshipList" Title="Scholarship List" %>
<%@ Import Namespace="ScholarBridge.Web"%>

<asp:Content ID="Content5" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />

<h2>All Current Scholarships</h2>

<asp:ListView ID="lstScholarships" runat="server" DataSourceID="scholarshipXmlDataSource">
    <LayoutTemplate>
    <table class="sortableTable">
        <thead>
            <tr>
                <th>Scholarship Name</th>
                <th>Provider</th>
                <th>Academic Year</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><a href='<%# (IISVersion.RunningIIS7 ? "/ScholarshipDetails" : "/ScholarshipDetails.aspx") +  XPath("UrlKey") %>'><%# XPath("Name") %></a></td>
        <td><%# XPath("ProviderName") %></td>
        <td><%# XPath("AcademicYear") %></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><a href='<%# (IISVersion.RunningIIS7 ? "/ScholarshipDetails" : "/ScholarshipDetails.aspx") +  XPath("UrlKey") %>'><%# XPath("Name") %></a></td>
        <td><%# XPath("ProviderName")%></td>
        <td><%# XPath("AcademicYear")%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships to display. </p>
    
    </EmptyDataTemplate>
</asp:ListView>
<asp:XmlDataSource 
    runat="server"
    ID="scholarshipXmlDataSource" 
    XPath="doc/scholarship">
</asp:XmlDataSource>
</asp:Content>


