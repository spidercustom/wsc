﻿using System;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Provider
{
    public partial class Default : System.Web.UI.Page
    {
        public IUserContext UserContext { get; set; }
		protected void Page_Load(object sender, EventArgs e)
        {
            MainMenuHelper.ResetActiveMenu(this);
            if (!Page.User.Identity.IsAuthenticated)
                Response.Redirect("~/provider/anonymous.aspx");
            UserContext.EnsureProviderIsInContext();
            
        }
    }
}
