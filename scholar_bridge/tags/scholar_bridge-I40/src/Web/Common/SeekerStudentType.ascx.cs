﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Web.Common
{
    public partial class SeekerStudentType : UserControl
    {
        private StudentGroups group;
        private int? years;

        public StudentGroups StudentGroup { 
            get
            {
                var btn = SelectedButton();
                if (null != btn)
                {
                    group = (StudentGroups) Enum.Parse(typeof(StudentGroups), btn.ID);
                }
                return group;
            }
            set { group = value; } 
        }
        public int? Years
        {
            get
            {
                var btn = SelectedButton();
                if (null != btn)
                {
                    var ddl = (DropDownList)FindControl(btn.ID + "Year");
                    years = null != ddl ? Int32.Parse(ddl.SelectedValue) : 0;
                }
                else
                {
                    years = 0;    
                }
                return years;
            }
            set { years = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (! Page.IsPostBack)
            {
                string name = Enum.GetName(typeof(StudentGroups), group);
                SetSelectedButton(name);
                SetYearsValue(name, years);
            }
        }

        private void SetYearsValue(string parentControlName, int? theYears)
        {
            var ddl = (DropDownList)FindControl(parentControlName + "Year");
            if (null != ddl)
            {
                if (null != theYears)
                {
                    ddl.SelectedValue = theYears.ToString();
                }
                ddl.Enabled = true;
            }
        }

        private void SetSelectedButton(string controlName)
        {
            if (!String.IsNullOrEmpty(controlName))
            {
                var btn = (RadioButton) FindControl(controlName);
                btn.Checked = true;
            }
        }

        private RadioButton SelectedButton()
        {
            var btns = new[]
                           {
                               MiddleSchool, HighSchoolFreshMan,HighSchoolSophomore,HighSchoolJunior, HighSchoolSenior,HighSchoolGraduate
                           };

            return btns.FirstOrDefault(btn => btn.Checked);
        }
    }
}