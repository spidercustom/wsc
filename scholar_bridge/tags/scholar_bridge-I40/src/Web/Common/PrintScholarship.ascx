﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintScholarship.ascx.cs"
    Inherits="ScholarBridge.Web.Common.PrintScholarship" %>
    <%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>
<div class="subsection-big">
    <br />
    <br />
   <div>         
       <p class="smallText indentsection">Scholarship Information For:</p> 
       <p class="headerText indentsection"> <asp:Literal  ID="scholarshipName"  runat="server" /> </p>  
       <br />
       <br />
         
         <table class="viewonlyTable indentsection">
            <tr>
            <td><label class="sectionHeader">Application Due On:</label></td><td><asp:Label CssClass="sectionHeader" ID="ApplicationDueOn" runat="server" ></asp:Label> </td>
            
            </tr>
           
         </table>
 
  </div> 
   <br />
   <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Academic Year:</label></td><td><asp:Label CssClass="smallText" ID="AcademicYear" runat="server" ></asp:Label></td>
        </tr>
        
    </table>
   

   <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Mission Statement:</label></td><td><asp:Label CssClass="smallText" ID="MissionStatement" runat="server" ></asp:Label></td>
        </tr>
        <tr runat="server" id="DonorRow">
                <td><label class="captionMedium">Donor:</label></td><td><asp:Label CssClass="smallText" ID="Donor" runat="server" ></asp:Label></td>
        </tr>
    </table>
 <br />    
<div runat="server" id="scheduleDiv"> 
<label class="sectionHeader ">Schedule:</label>
    <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Application Starts On:</label></td><td><asp:Label CssClass="smallText" ID="ScheduleApplicationStart" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">Application Due On:</label></td><td><asp:Label CssClass="smallText" ID="ScheduleApplicationDue" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Award On:</label></td><td><asp:Label CssClass="smallText" ID="ScheduleApplicationAwardOn" runat="server" ></asp:Label></td>
        </tr>
        
    </table>  
</div> 
<br />
<div runat="server" id="fundingDiv"> 
<label class="sectionHeader">Funding:</label>
     <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Number of Awards:</label></td><td><asp:Label CssClass="smallText" ID="FundingNumberOfAwards" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">Award Amount:</label></td><td><asp:Label CssClass="smallText" ID="FundingAwardAmount" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Total Funds Available:</label></td><td><asp:Label CssClass="smallText" ID="FundingTotalFunds" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">Support Period:</label></td><td><asp:Label CssClass="smallText" ID="FundingSupportPeriod" runat="server" ></asp:Label></td>
        </tr>
    </table>  
  </div> 
   <br /> 
<div runat="server" id="contactInfoDiv"> 
<label class="sectionHeader">Contact Information:</label>
     <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Name:</label></td><td><asp:Label CssClass="smallText" ID="ContactName" runat="server" ></asp:Label></td>
        </tr>
         <tr runat="server" id="WebsiteRow">
            <td><label class="captionMedium">Website:</label></td><td><asp:Label CssClass="smallText" ID="ContactWebsite" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Address:</label></td><td><asp:Label CssClass="smallText" ID="ContactAddress" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">Phone:</label></td><td><asp:Label CssClass="smallText" ID="ContactPhone" runat="server" ></asp:Label></td>
        </tr>
    </table> 
  </div> 
   <br /> 
 <div runat="server" id="RenewDiv"> 
 <label class="sectionHeader">Renew/ReApply:</label>
    <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Renewable:</label></td><td><asp:Label CssClass="smallText" ID="Renewable" runat="server" ></asp:Label></td>
        </tr>
         <tr runat="server" id="RenewableGuidelinesRow">
            <td><label class="captionMedium">Renewable Guidelines:</label></td><td><asp:Label CssClass="smallText" ID="RenewableGuideLines" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Reapply:</label></td><td><asp:Label CssClass="smallText" ID="Reapply" runat="server" ></asp:Label></td>
        </tr>
    </table>  
  </div>        
 <br /> 
<hr />
<div runat="server" id="minimumEligibilityDiv">  
<label class="sectionHeader">Minimum Applicant Eligibility Guidelines:</label> 
    <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Type of Students Eligible to Apply:</label></td><td class="limitWidth"><asp:Label CssClass="smallText" ID="EligibilityTypeOfStudent" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">Situations the Scholarship will Fund:</label></td><td><asp:Label CssClass="smallText" ID="EligibilitySupportedSituation" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Colleges:</label></td><td><asp:Label CssClass="smallText" ID="EligibilityColleges" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">School Types:</label></td><td><asp:Label CssClass="smallText" ID="EligibilitySchoolType" runat="server" ></asp:Label></td>
        </tr>
         <tr>
            <td><label class="captionMedium">Academic Programs:</label></td><td><asp:Label CssClass="smallText" ID="EligibilityAcademicPrograms" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Enrollment Status:</label></td><td><asp:Label CssClass="smallText" ID="EligibilityEnrollmentStatus" runat="server" ></asp:Label></td>
        </tr>
    </table>
    <table class="viewonlyTable" runat="server" id="MinimumTable" >
                                 
        
    </table>

</div>
 <br /> 
<div runat="server" id="preferenceDiv">  
<label class="sectionHeader">Preference Given For:</label>
    <table class="viewonlyTable" runat="server" id="PreferenceTable" >
     </table>
</div>
 <br /> 
<div runat="server" id="financialNeedDiv">  
    <label class="sectionHeader">Financial Need:</label>
     <table class="viewonlyTable" runat="server" id="FinancialNeedTable" >
    </table>   
</div>    
 <br /> 
<div runat="server" id="additionalRequirementsDiv">    
<label class="sectionHeader">Additional Scholarship Requirements:</label>
    <table class="viewonlyTable" runat="server" id="AddtionalRequiremnetsTable">
        <tr>
            <td><label class="smallText">Please include the following with your application:</label></td><td></td>
        </tr> 
        
    </table>          
    <table class="viewonlyTable" runat="server" id="QuestionsTable">
        <tr>
            <td><label class="captionMedium">Scholarship Specific Instructions and Questions:</label></td>
            <td>
                    
            </td>
        </tr>
         <tr>
            <td><label class="smallText">Please complete the following instructions/questions:</label></td><td></td>
        </tr> 
        
    </table>   
    <table class="viewonlyTable" runat="server" id="FormsTable">
        <tr >
            <td><label class="captionMedium">Additional Application Forms:</label></td>
            <td>
                    
            </td>
        </tr>
                
        <tr>
        <td><sb:FileList id="scholarshipAttachments" runat="server" />
        </td>
        </tr>
    </table>       
</div> 
<table class="viewonlyTable" runat="server" id="ApplyAtTable">
        <tr >
            <td><label class="captionMedium">Apply at:</label></td>
            <td><asp:Label CssClass="smallText" ID="ApplyAtUrl" runat="server" ></asp:Label>       </td>
        </tr>
</table>
  <br />
  <br />
        
 <p>TheWashBoard.org is a free web-based tool for Washington students seeking college scholarships. We make scholarship searching simple.</p> 
 <p></p>
 <p>Create a profile and review your scholarship matches. Register Today at <a class="GreenLink" href=http://www.theWashBoard.org>theWashBoard.org</a></p>
 <br />
 <p class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</p>
</div>
