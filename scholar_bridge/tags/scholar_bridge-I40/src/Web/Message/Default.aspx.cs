﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Message
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        protected override void OnInitComplete(EventArgs e)
        {
			if (User.IsInRole(Role.SEEKER_ROLE) || User.IsInRole(Role.INFLUENCER_ROLE))
            {
                messageTabPages.Views.Remove(messageTabPages.FindControl("DeniedTabPage"));
                messageTabPages.Views.Remove(messageTabPages.FindControl("ApprovedScholarshipsTabPage"));
                deniedTabHead.Visible = false;
                approvedTabHead.Visible = false;
            }
            base.OnInitComplete(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.Messages);

            if (User.IsInRole(Role.PROVIDER_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for notifications of scholarship status, scholarship applications and awards, and requests from other scholarship organizations. This inbox is shared between all your organizations users. Actions done by one user impact the other user's messages.";

            else if (User.IsInRole(Role.INTERMEDIARY_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for requests from other scholarship organizations, and notifications of scholarship status, applications and awards. This inbox is shared between all your organizations users. Actions done by one user impact the other user's messages.";
            else
                lblPageText.Visible = false;
        }
    }
}