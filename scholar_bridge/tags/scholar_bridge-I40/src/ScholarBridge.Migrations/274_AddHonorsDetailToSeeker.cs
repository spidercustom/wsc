﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(274)]
    public class AddHonorsDetailToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public readonly string[] NEW_COLUMNS
            = { "APCredits", "IBCredits","HonorsDetail", "APCreditsDetail","IBCreditsDetail" };

        public override void Up()
        {
            Database.RemoveColumn(TABLE_NAME, NEW_COLUMNS[0]);
            Database.RemoveColumn(TABLE_NAME, NEW_COLUMNS[1]);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[2], DbType.String,50, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[3], DbType.String, 50, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[4], DbType.String, 50, ColumnProperty.Null);
        }

        public override void Down()
        {
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}