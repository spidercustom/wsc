﻿using System;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class ActivateRelationshipAction : IAction
    {
        public IRelationshipService RelationshipService { get; set; }
        public IMessageService MessageService { get; set; }

        public bool SupportsApprove { get { return true; } }
        public bool SupportsReject { get { return true; } }
        public string LastStatus { get; set; }
        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
            if (! (message is RelationshipMessage))
                throw new ArgumentException("Message must be a RelationshipMessage");

            var relationship = RelationshipService.GetByOrganizations(message.From.Organization, message.To.Organization);

            if (relationship == null)
                throw new NullReferenceException("RelatedRelationship can not be null");

            relationship.LastUpdate = new ActivityStamp(approver);
            RelationshipService.Approve(relationship);

            message.ActionTaken = MessageAction.Approve;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Relationship Approved";
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
            if (!(message is RelationshipMessage))
                throw new ArgumentException("Message must be a RelationshipMessage");

            var relationship = RelationshipService.GetByOrganizations(message.From.Organization, message.To.Organization);
            if (relationship == null)
                throw new NullReferenceException("RelatedRelationship can not be null");

            relationship.LastUpdate = new ActivityStamp(approver);
            RelationshipService.Reject(relationship);

            message.ActionTaken = MessageAction.Deny;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Relationship Rejected";
        }
    }
}
