﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerStudentType.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerStudentType" %>

<script type="text/javascript">
$(document).ready(function() {
    $("input[name='ctl00$bodyContentPlaceHolder$AcademicInfo1$CurrentStudentGroupControl$studentGroup']").click(
        function() {
            var checkedvalue = $(this).val(); 
            $.each(["HighSchoolSeniorYear", "AdultReturningYear", "TransferYear"], function(i, name) {
                $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + name).attr("disabled","disabled");
            });
            $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + checkedvalue + "Year").removeAttr("disabled");
        });
    });
</script>
<style type="text/css">
.radio, .radioSelect { width: 200px; float:left; text-align:left; height: 15px;}
</style>

<div>
    <div class="radio"><asp:RadioButton runat="server" ID="MiddleSchool" GroupName="studentGroup" /> Middle School</div><br />
    <div class="radio"><asp:RadioButton runat="server" ID="HighSchoolSenior" GroupName="studentGroup" /> High School</div>
    <div class="radioSelect">
    <asp:DropDownList id="HighSchoolSeniorYear" runat="server" name="HighSchoolSeniorYear" Enabled="false">
        <asp:ListItem Value="1" Text="Freshman" />
        <asp:ListItem Value="2" Text="Sophomore" />
        <asp:ListItem Value="3" Text="Junior" />
        <asp:ListItem Value="4" Text="Senior" />
    </asp:DropDownList>
    </div><br />
    <div class="radio"><asp:RadioButton runat="server" ID="HighSchoolGraduate" GroupName="studentGroup"/> High School Graduate</div><br />
    <div class="radio"><asp:RadioButton runat="server" ID="AdultFirstTime" GroupName="studentGroup" /> Adult First time </div><br />
    <div class="radio"><asp:RadioButton runat="server" ID="AdultReturning" GroupName="studentGroup"/> Adult Returning</div>
    <div class="radioSelect">
    <asp:DropDownList id="AdultReturningYear" runat="server" name="AdultReturningYear" Enabled="false">
        <asp:ListItem Value="1" Text="1" />
        <asp:ListItem Value="2" Text="2" />
        <asp:ListItem Value="3" Text="3" />
        <asp:ListItem Value="4" Text="4" />
        <asp:ListItem Value="5" Text="5" />
        <asp:ListItem Value="6" Text="6" />
    </asp:DropDownList>
    </div><br />
    <div class="radio"><asp:RadioButton runat="server" ID="Transfer" GroupName="studentGroup"/> Transfer</div>
    <div class="radioSelect">
    <asp:DropDownList id="TransferYear" runat="server" name="TransferYear" Enabled="false">
        <asp:ListItem Value="1" Text="1" />
        <asp:ListItem Value="2" Text="2" />
        <asp:ListItem Value="3" Text="3" />
        <asp:ListItem Value="4" Text="4" />
        <asp:ListItem Value="5" Text="5" />
        <asp:ListItem Value="6" Text="6" />
    </asp:DropDownList>
    </div><br />
    <div class="radio"><asp:RadioButton runat="server" ID="Undergraduate" GroupName="studentGroup"/> Undergraduate</div><br />
    <div class="radio"><asp:RadioButton runat="server" ID="Graduate" GroupName="studentGroup"/> Graduate</div><br />
</div>
