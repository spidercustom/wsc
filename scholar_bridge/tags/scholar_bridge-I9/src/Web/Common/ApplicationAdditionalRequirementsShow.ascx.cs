﻿using System;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationAdditionalRequirementsShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                additionalRequirements.DataSource = ApplicationToView.Scholarship.AdditionalRequirements;
                questions.DataSource = ApplicationToView.QuestionAnswers;
                questions.DataBind();
                applicationAttachments.Attachments = ApplicationToView.Attachments;

                SetupVisibility();
            }
        }

        private void SetupVisibility()
        {

            AddRequirementsBlock.Visible = ApplicationToView.Scholarship.AdditionalRequirements != null &&
                                           ApplicationToView.Scholarship.AdditionalRequirements.Count > 0;

            QABlock.Visible = ApplicationToView.QuestionAnswers != null && ApplicationToView.QuestionAnswers.Count > 0;
            AttachmentsBlock.Visible = ApplicationToView.Attachments != null && ApplicationToView.Attachments.Count > 0;
        }

        public override ApplicationStages Stage
        {
            get { return ApplicationStages.AdditionalCriteria; }
        }
    }
}