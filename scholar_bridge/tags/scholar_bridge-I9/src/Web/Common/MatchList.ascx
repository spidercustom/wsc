﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>
<asp:ListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    onpagepropertieschanging="matchList_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th>Action</th>
                <th>Scholarship Name</th>
                <th>Required Criteria</th>
                <th>Preferred Criteria</th>
                <th>Application Due Date</th>
                <th>#</th>
                <th>Scholarship Amount</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:Button ID="matchBtn" runat="server" OnCommand="matchBtn_OnCommand"/></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# Eval("ScholarshipName")%></asp:HyperLink></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:Button ID="matchBtn" runat="server" OnCommand="matchBtn_OnCommand"/></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# Eval("ScholarshipName")%></asp:HyperLink></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships at this time.</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="matchList" PageSize="20" >
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
</div> 