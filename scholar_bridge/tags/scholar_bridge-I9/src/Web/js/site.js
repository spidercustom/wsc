﻿$(document).ready(function() {

    var hoverFunc = function() { $(this).attr("style", "cursor: pointer;"); };
    var unhoverFunc = function() { $(this).attr("style", "cursor: default;"); }
    
    // convert print divs to clickable things that cause a print to occur
    $(".print").mouseup(function() {
        window.print();
    }).hover(hoverFunc, unhoverFunc);

    // close a parent element
    $(".closeBox").click(function(event) {
        $(this).parent().fadeOut();
    }).hover(hoverFunc, unhoverFunc);
    
    // close a parent elemetn
    $(".close").click(function(event) {
        window.close();
    }).hover(hoverFunc, unhoverFunc);

    try {
        // make table class="sortableTable" sortable
        $(".sortableTable").tablesorter();
    }
    catch (exception) {
    }

    // make div class="tabs" int tabs
    // remember what tab we were on
    $(".tabs").tabs(); // {cookie:{expires:30}}

    try {
        $("ul.menu").superfish({
            pathClass: 'current'
        });
    }
    catch (exception) {
    }
    
   $(".date").mask("99/99/9999");
   $(".phone").mask("999-999-9999");
   $(".ein").mask("99-9999999");
   $(".ssn").mask("999-99-9999");
});

function SelectAllCheckBoxIn(parentId) {
    CheckUncheckCheckBoxesIn(parentId, true);
}

function DeselectAllCheckBoxIn(parentId) {
    CheckUncheckCheckBoxesIn(parentId, false);
}

function CheckUncheckCheckBoxesIn(parentId, checkState) {
    $("#" + parentId + " > *").find("input:checkbox").each(function() {
        this.checked = checkState;
    });
}

function EnableDisableControls(parentId, controllerCheckBoxId, enableOnCheck) {
    var disableElements = enableOnCheck;
    if ($('#' + controllerCheckBoxId).is(':checked'))
        disableElements = !enableOnCheck;
    $("#" + parentId + " :input").filter(function(index) {
        return $(this).attr("id") != controllerCheckBoxId;
    }).attr('disabled', disableElements);
}

function confirmSaveDialog(msg, title) {
    var dlg = $('<div title="' + title + '" >' + msg + '</div>');
    $(dlg).dialog({
        autoOpen: true,
        width: 400,
        height: 150,
        modal: true,
        resizable: true,
        buttons: {
            "Save Changes": function() {
                $(this).dialog("destroy");
                return true;
            },
            "Cancel": function() {
                $(this).dialog("destroy");
                return false;
            }
        }
    });
    return false;
}

function yesnodialog(dialogdiv, successurl) {
    dialogdiv = '#' + dialogdiv;
    $(dialogdiv).dialog({
        autoOpen: true,
        width: 400,
        height: 200,
        modal: true,
        resizable: true,
        buttons: {
             "No": function() {
                $(this).dialog("destroy");
                return false;

            },
            
            "Yes": function() {
                $(this).dialog("destroy");
                if (successurl == '')
                    return true;
                else
                    window.location = successurl;

            }
           
        }
    });
    return false;
}

function alertdialog(msg, title) {
    var dlg = $('<div title="' + title + '" >' + msg + '</div>');
    $(dlg).dialog({
        autoOpen: true,
        width: 500,
        height: 'auto',
        modal: true,
        resizable: true,
        buttons: {
            "OK": function() {
                $(this).dialog("destroy");
                $(dlg).remove();
                return false;
            }
        }
    });
}

function showdialog(divId) {
    $("#" + divId).dialog({
        autoOpen: true,
        modal: true,
        width: 500,
        resizable: false,
        buttons: {
            "OK": function() {
                $(this).dialog("destroy");
                
                return false;
            }
        }
    });    
}

function applicationAttachments(url) {
    window.open(url, "Application Attachments", "width=800,height=600");
}

function printApplications(url) {
    window.open(url, "Print Applications", "width=800,height=600,scrollbars=1");
}

function searchBoxFocus(controlID) {

    controlID = "#" + controlID;
    
    $(controlID).focus(function() {
       if ($(controlID).val()=='Search')
           $(controlID).val('');
   });

   $(controlID).blur(function() {
       if ($(controlID).val() == '')
           $(controlID).val('Search');
   });

   if ($(controlID).val() == '')
       $(controlID).val('Search');
} 
