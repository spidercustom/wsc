﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class SubmitListChangeRequest : Page
    {
        public IUserContext UserContext { get; set; }
        public IProviderService ProviderService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            var organization = UserContext.CurrentOrganization;
            if (!IsPostBack)
            {
            
            var user = UserContext.CurrentUser;
            ListChangeRequest1.From = String.Format("{0}({1})-{2}",
                                                    user.Name.NameFirstLast, organization.Name, user.Email);
            }

    }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            
            if (UserContext.CurrentOrganization is Domain.Provider)
            ProviderService.SubmitListChangeRequest(UserContext.CurrentProvider,
                 UserContext.CurrentUser,ListChangeRequest1.ListType,ListChangeRequest1.NewValue,ListChangeRequest1.Reason);
            else
                IntermediaryService.SubmitListChangeRequest(UserContext.CurrentIntermediary,
                               UserContext.CurrentUser, ListChangeRequest1.ListType, ListChangeRequest1.NewValue, ListChangeRequest1.Reason);
         

            Response.Write("<script>alert('Request submitted.');window.close()</script>");
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.close()</script>");
        }
    }
}
