﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationSelector.ascx.cs"
    Inherits="ScholarBridge.Web.Provider.BuildScholarship.LocationSelector" %>
<%@ Register Src="~/Common/Lookup/LookupDialog.ascx" TagName="LookupDialog" TagPrefix="sb" %>
<div class="resetSizing">
    <label for="StateDropDownControl">
        State:</label>
    <asp:DropDownList ID="StateDropDownControl" runat="server" />
    <span>Limited by:</span>
    <asp:RadioButton ID="CountyRadioButton" runat="server" Checked="true" Text="County" GroupName="Location"
        onclick="javascript:ShowStateDependantLocationControl('<%# CountyContainerControl.ClientID %>');" />
    <asp:RadioButton ID="CityRadioButton" runat="server" Text="City" GroupName="Location"
        onclick="javascript:ShowStateDependantLocationControl('<%# CityContainerControl.ClientID %>');" />
    <asp:RadioButton ID="SchoolDistrictRadioButton" runat="server" Text="School District" GroupName="Location" 
        onclick="javascript:ShowStateDependantLocationControl('<%# SchoolDistrictContainerControl.ClientID %>');" />
    <asp:RadioButton ID="HighSchoolRadioButton" runat="server" Text="High School" GroupName="Location"
        onclick="javascript:ShowStateDependantLocationControl('<%# HighSchoolContainerControl.ClientID %>');" />
    <br />
</div>

<script type="text/javascript">
    function ShowStateDependantLocationControl(objectId) {
        $("#StateDependantLocationSelectorContainer > div").hide();
        var objectSelector = "#" + objectId ;
        $(objectSelector).show();
    };
</script>

<div id="StateDependantLocationSelectorContainer">
    <asp:Panel ID="CountyContainerControl" runat="server">
        <label id="CountyLabelControl" for="CountyControl">
            County:</label>
        <asp:TextBox ID="CountyControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
        <sb:LookupDialog ID="CountyControlDialogButton" runat="server" BuddyControl="CountyControl"
            ItemSource="CountyDAL" Title="County Selection" />
        <br />
    </asp:Panel>

    <asp:Panel ID="CityContainerControl" runat="server" style="display: none">
        <label id="CityLabelControl" for="CityControl">
            City:</label>
        <asp:TextBox ID="CityControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
        <sb:LookupDialog ID="CityControlDialogButton" runat="server" BuddyControl="CityControl"
            ItemSource="CityDAL" Title="City Selection" />
        <br />
    </asp:Panel>

    <asp:Panel ID="SchoolDistrictContainerControl" runat="server" style="display: none">
        <label id="SchoolDistrictLabelControl" for="SchoolDistrictControl">
            School District:</label>
        <asp:TextBox ID="SchoolDistrictControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
        <sb:LookupDialog ID="SchoolDistrictControlDialogButton" runat="server" BuddyControl="SchoolDistrictControl"
            ItemSource="SchoolDistrictDAL" Title="School District Selection" />
        <br />
    </asp:Panel>

    <asp:Panel ID="HighSchoolContainerControl" runat="server" style="display: none">
        <label id="HighSchoolLabelControl" for="HighSchoolControl">
            High School:</label>
        <asp:TextBox ID="HighSchoolControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
        <sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl"
            ItemSource="HighSchoolDAL" Title="High School Selection" />
        <br />
    </asp:Panel>
</div>