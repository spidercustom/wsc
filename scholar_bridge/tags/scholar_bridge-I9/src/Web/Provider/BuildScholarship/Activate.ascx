﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activate.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Activate" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<h3>Build Scholarship – Activate</h3>
<p>Activating the Scholarship will make the scholarship visible to applicants and allow the scholarship to be matched with qualified applicants. 
Once you request activation, you will not be able to edit the scholarship criteria. By activating this scholarship, you, the provider 
(or the intermediary on the provider's behalf), indicate your intention to award the scholarship as it has been entered.
</p>


<div id="requestActivationConfirmDiv" title="Request Activation" style="display:none">
    Requesting activation will send a message to an authorized agent to activate the scholarship. 
    You can expect the scholarship to be activated in approximately X number of days. 
    Once you request activation, you will not be able to edit the scholarship criteria.
    <br /> <br />
    Are you sure want to continue?
</div>   
<div>

<div id="ScholarshipValidationErrors" class="issueList" title="Scholarship activation" style="display:none">
    <p>
        One or more validation(s) have failed across all data entry fields for this scholarship. They must to be
        fixed before scholarship can be requested for activation. We recommend you to go through each tab, starting 
        from the first and fix these issues.
    </p>
    <asp:Repeater ID="IssueListControl" runat="server">
        <HeaderTemplate><ul></HeaderTemplate>
        <ItemTemplate><li><%# DataBinder.Eval(Container.DataItem, "ErrorMessage") %></li></ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>
    <br />
</div>

        
<sbCommon:ConfirmButton ID="confirmRequestActivationBtn" ConfirmMessageDivID ="requestActivationConfirmDiv" 
        Text="Request Activation"  runat="server" Width="150px" onclick="confirmRequestActivationBtn_Click" />
<br />
<br />
<br />        
</div>