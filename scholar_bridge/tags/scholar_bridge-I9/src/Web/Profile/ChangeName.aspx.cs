﻿using System;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Profile
{
    public partial class ChangeName : System.Web.UI.Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            editUserName.CurrentUser = UserContext.CurrentUser;
        }

        protected void editUserName_OnUserSaved(User user)
        {
            SuccessMessageLabel.SetMessage("Your name has been changed.");
            Response.Redirect("~/Profile/");
        }

        protected void editUserName_OnFormCanceled()
        {
            Response.Redirect("~/Profile/");
        }
    }
}
