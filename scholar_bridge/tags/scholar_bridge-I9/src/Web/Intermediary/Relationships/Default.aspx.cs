﻿using System;
using System.Web.UI;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Intermediary.Relationships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = UserContext.CurrentIntermediary;
            relationshiplist.Relationships = RelationshipService.GetByIntermediary(intermediary);
        }
    }
}