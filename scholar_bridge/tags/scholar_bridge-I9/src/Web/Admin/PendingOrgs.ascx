﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PendingOrgs.ascx.cs" Inherits="ScholarBridge.Web.Admin.PendingOrgs" %>
<asp:Repeater ID="pendingOrgs" runat="server" 
    onitemdatabound="pendingOrgs_ItemDataBound">
    <HeaderTemplate>
    <table class="sortableTable">
    <thead>
        <tr>
            <th>Name</th>
            <th>EIN</th>
            <th>Confirmed Email?</th>
        </tr>
    </thead>
    <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="row">
            <td><asp:HyperLink id="linktoOrg" runat="server"><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:HyperLink></td>
            <td><%# DataBinder.Eval(Container.DataItem, "TaxId") %></td>
            <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "AdminUser"), "IsActive") %></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:HyperLink id="linktoOrg" runat="server"><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:HyperLink></td>
            <td><%# DataBinder.Eval(Container.DataItem, "TaxId") %></td>
            <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "AdminUser"), "IsActive") %></td>
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
    </tbody>
    </table>
    </FooterTemplate>
</asp:Repeater>