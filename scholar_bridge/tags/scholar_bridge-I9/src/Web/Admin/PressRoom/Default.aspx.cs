﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Admin.PressRoom
{
    public partial class Default : System.Web.UI.Page
    {
        
        public const string LinkTo = "~/PressRoom/Show.aspx";
        public const string BUILD_PAGE = "~/Admin/PressRoom/BuildArticle.aspx?id={0}";
        public IArticleService ArticleService { get; set; }
        public IUserContext UserContext { get; set; }

         
        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            if (!IsPostBack)
                Bind();
        }

        protected void Bind()
        {
                var articles = ArticleService.FindAll();
                lstArticles.DataSource = articles;
                lstArticles.DataBind();
            
        }

        protected void lstArticles_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var article = ((Article)((ListViewDataItem)e.Item).DataItem);
                var link = (HyperLink)e.Item.FindControl("linktoArticle");
                link.NavigateUrl = LinkTo + "?id=" + article.Id;

                var chk = (CheckBox)e.Item.FindControl("chkArticle");
                chk.Attributes.Add("value",article.Id.ToString());
                var btnDelete = (ConfirmButton)e.Item.FindControl("SingleDeleteBtn");
                btnDelete.Attributes.Add("value", article.Id.ToString());
                var btnEdit = (Button)e.Item.FindControl("SingleEditBtn");
                btnEdit.Attributes.Add("value", article.Id.ToString());
            }
        }

        protected void lstArticles_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }

        protected void DeleteBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (!e.DialogResult) return;
            bool rebind = false;
            for (int i = 0; i < lstArticles.Items.Count; i++)
            {
                var chk = (CheckBox)lstArticles.Items[i].FindControl("chkArticle");
                if (chk.Checked)
                {
                    var articleid = int.Parse( chk.Attributes["value"]);
                    var article = ArticleService.GetById(articleid);
                    if (!(article ==null))
                        ArticleService.Delete(article);
                    rebind = true;
                }
            }

            if (rebind)
                Bind();
        }

        protected void SingleDeleteBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (!e.DialogResult) return;
            var btn = (ConfirmButton) sender;
            var articleid = int.Parse(btn.Attributes["value"]);
            var article = ArticleService.GetById(articleid);
            if (!(article == null))
                ArticleService.Delete(article);
            Bind();
        }

        protected void SingleEditBtn_Click(object sender, EventArgs e)
        {
            var btn = (Button) sender;
            var articleid = int.Parse(btn.Attributes["value"]);
            Response.Redirect(ResolveUrl(BUILD_PAGE.Build(articleid)));
        }
    }
}
