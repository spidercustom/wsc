﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Admin.PressRoom.Default" Title="Press Room Admin" %>
 <%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<p>Press Room</p>
<br />
<br />
<p>Place keeper for WSC provided content. Content could include: Basic facts about the organization, Financial Information, Location of Headquarters, Leadership profiles, Downloadable images, Overview of the organization’s commitment to social responsibility, Media contact information</p>
<br />
<br />
 <div>

    <asp:ListView ID="lstArticles" runat="server" 
        OnItemDataBound="lstArticles_ItemDataBound" 
        onpagepropertieschanging="lstArticles_PagePropertiesChanging" >
        <LayoutTemplate>
        <div>
        <ul class="pageNav">
                <li><asp:HyperLink ID="CreateArticleLinkTop" runat="server" NavigateUrl="~/Admin/PressRoom/BuildArticle.aspx">Add Article</asp:HyperLink></li>
        </ul>
        </div>
        <table class="sortableTable">
                <thead>
                <tr>
                    <th>Select</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Date</th>
                    <th>Title</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        
        </LayoutTemplate>
              
        <ItemTemplate>
        <tr class="row">
            <td><asp:CheckBox ID="chkArticle" runat="server" /></td>
            <td><asp:Button ID="SingleEditBtn" Text="Edit" runat="server" onclick="SingleEditBtn_Click" /></td>
            <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
                Text="Delete"  runat="server" onclick="SingleDeleteBtn_Click"   Width="60px" /></td>
            <td><%# DataBinder.Eval(Container.DataItem, "PostedDate", "{0:d}")%></td>
            <td><asp:HyperLink id="linktoArticle" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
        </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:CheckBox ID="chkArticle" runat="server" /></td>
            <td><asp:Button ID="SingleEditBtn" Text="Edit" runat="server" onclick="SingleEditBtn_Click" /></td>
            <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
                Text="Delete"  runat="server" onclick="SingleDeleteBtn_Click" Width="60px"  /></td>
            <td><%# DataBinder.Eval(Container.DataItem, "PostedDate", "{0:d}")%></td>
            <td><asp:HyperLink id="linktoArticle" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
        </tr>
        </AlternatingItemTemplate>
        <EmptyDataTemplate>
        <p>There are noArticles, </p>
        <ul class="pageNav">
            <li><asp:HyperLink ID="createArticleLnk" runat="server" NavigateUrl="~/Admin/PressRoom/BuildArticle.aspx">Add Article</asp:HyperLink></li>
        </ul>
        </EmptyDataTemplate>
    </asp:ListView>

    <div class="pager">
        <asp:DataPager runat="server" ID="pager"  PagedControlID="lstArticles" PageSize="50" >
            <Fields>
                <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
                <asp:NextPreviousPagerField />
            </Fields>
        </asp:DataPager>
        <br />
    </div> 
    <div>
        <sbCommon:ConfirmButton ID="DeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
            Text="Delete Selected"  runat="server" onclick="DeleteBtn_Click" Width="130px" />
    </div>
    <div id="deleteConfirmDiv" title="Confirm Delete" style="display:none">
            Are you sure want to delete?
    </div> 
    <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>  
 
 </div>
</asp:Content>
