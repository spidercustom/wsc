﻿using System;
using System.Linq;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin
{
    public partial class PendingProviders : System.Web.UI.Page
    {
        public IProviderService ProviderService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            pendingProviders.Organizations = ProviderService.FindPendingOrganizations()
                .Cast<Organization>().ToList();
        }
    }
}
