﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
    public partial class ListOfAdditionalRequirements : UserControl
    {
        private IList<AdditionalRequirement> _Requirements;
        public IList<AdditionalRequirement> Requirements
        {
            get { return _Requirements; }
            set
            {
                _Requirements = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
             
                Bind();
        }

        protected void Bind()
        {
            if (!(Requirements == null))
            {
                lstRequirements.DataSource = Requirements;
                lstRequirements.DataBind();
            }
        }

        
    }
}