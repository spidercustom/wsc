﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
    public enum WizardStepName
    {
        Basics,
        AboutMe,
        Academics,
        Activities,
        FinancialNeed,
        AdditionalCriteria
    }
}
