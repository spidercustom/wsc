﻿using System;
using System.Collections.Generic;

namespace ScholarBridge.Common.Extensions
{
    public static class IGenericEnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            foreach (var listItem in list)
            {
                action(listItem);
            }
        }
    }
}