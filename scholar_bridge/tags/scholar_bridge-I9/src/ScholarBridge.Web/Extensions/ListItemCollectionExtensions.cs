﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Extensions
{
    public static class ListItemCollectionExtensions
    {
        public static void SelectItems<T>(this ListItemCollection items, IEnumerable<T> domainItems, Func<T, string> fn)
        {
            var ids = domainItems.Select(fn).ToList();
            foreach (ListItem item in items)
            {
                if (ids.Contains(item.Value))
                {
                    item.Selected = true;
                }
            }
        }

        public static IEnumerable<TResult> Select<TResult>(this ListItemCollection items, Func<string, TResult> fn)
        {
            var selected = new List<TResult>();
            foreach (ListItem item in items)
            {
                if (item.Selected) 
                {
                    selected.Add(fn(item.Value));
                }
            }
            return selected;
        }

        public static IList<TResult> SelectedItems<TResult>(this ListItemCollection items, Func<string, TResult> convert)
        {
            return items.Select(convert).ToList();
        }
    }
}
