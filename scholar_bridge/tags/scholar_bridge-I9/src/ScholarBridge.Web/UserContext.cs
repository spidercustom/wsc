using System.Web;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Exceptions;

namespace ScholarBridge.Web
{
    public class UserContext : IUserContext
    {

        public IUserDAL UserService { get; set; }
        public IProviderDAL ProviderService { get; set; }
        public IIntermediaryDAL IntermediaryService { get; set; }
        public ISeekerDAL SeekerService { get; set; }

        public User CurrentUser
        {
            get
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    return null;
                return UserService.FindByUsername(HttpContext.Current.User.Identity.Name);
            }
        }

        public Organization CurrentOrganization
        {
            get
            {
                var org = CurrentProvider;
                if (null != org)
                    return org;
                return CurrentIntermediary;
            }
        }

        public Provider CurrentProvider
        {
            get
            {
                var u = CurrentUser;
                if (null == u)
                    return null;

                return ProviderService.FindByUser(u);
            }
        }

        public Intermediary CurrentIntermediary
        {
            get
            {
                var u = CurrentUser;
                if (null == u)
                    return null;

                return IntermediaryService.FindByUser(u);
            }
        }

        public Seeker CurrentSeeker 
        { 
            get
            {
                var u = CurrentUser;
                if (null == u)
                    return null;
                return SeekerService.FindByUser(u);
            } 
        }


        public void EnsureUserIsInContext()
        {
            if (null == CurrentUser)
                throw new NoUserIsInContextException();
        }

        public void EnsureProviderIsInContext()
        {
            if (null == CurrentProvider)
                throw new NoProviderIsInContextException();
        }

        public void EnsureIntermediaryIsInContext()
        {
            if (null == CurrentIntermediary)
                throw new NoIntermediaryIsInContextException();
        }

        public void EnsureSeekerIsInContext()
        {
            if (null == CurrentSeeker)
                throw new NoSeekerIsInContextException();
        }
    }
}