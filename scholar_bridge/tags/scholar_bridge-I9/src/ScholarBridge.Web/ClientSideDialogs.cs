﻿using System.Web;
using ScholarBridge.Web.Extensions;
using System.Web.UI;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web
{
    /// <summary>
    /// Displays jquery alert dialog from code behind
    /// </summary>
    public static class ClientSideDialogs
    {
        private const string SHOW_DIALOG_TEMPLATE = "showdialog('{0}');";
        private const string ALERT_DIALOG_SCRIPT_TEMPLATE = "alertdialog('{0}','{1}');";
        private const string SHOW_YESNO_DIALOG_SCRIPT_TEMPLATE = "yesnodialog('{0}','{1}');";
 
        /// <summary>
        /// Shows a client-side JavaScript alert in the browser.
        /// </summary>
        /// <param name="message">The message to appear in the alert.</param>
        /// /// <param name="title">The title to appear in the alert.</param>
        public static void ShowAlert(string message, string title)
        {
            // Cleans the message to allow single quotation marks
            var cleanMessage = message.Replace("'", "\'");
            var cleanTitle = title.Replace("'", "\'");
            var script = ALERT_DIALOG_SCRIPT_TEMPLATE.Build(cleanMessage, cleanTitle);

            // Gets the executing web page
            var page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), title))
            {
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), title, script);
            }
        }

        /// <summary>
        /// Shows a div content has dialog.
        /// </summary>
        /// <param name="divId">identifier (ID) of div</param>
        public static void ShowDivAsDialog(string divId)
        {
            // Gets the executing web page
            var page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (null != page && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), divId))
            {
                var script = SHOW_DIALOG_TEMPLATE.Build(divId);
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), divId, script);
            }
        }

        public static void ShowDivAsYesNoDialog(string divId, string successurl)
        {
            
            // Gets the executing web page
            var page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (null != page && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), divId))
            {
                var script = SHOW_YESNO_DIALOG_SCRIPT_TEMPLATE.Build(divId,successurl);
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), divId, script);
            }
        }
    }
}