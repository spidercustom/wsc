﻿
namespace ScholarBridge.Domain
{
    public enum ApplicationStages
    {
        None,
        Basics,
        AboutMe,
        AcademicInfo,
        Activities,
        FinancialNeed,
        AdditionalCriteria,
        Submitted
    }
}
