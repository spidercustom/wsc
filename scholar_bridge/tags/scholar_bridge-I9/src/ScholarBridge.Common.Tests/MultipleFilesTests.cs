using System.IO;
using NUnit.Framework;

namespace ScholarBridge.Common.Tests
{

    [TestFixture]
    public class MultipleFilesTests
    {

        private string file1;
        private string file2;
        private string zipFile;

        [SetUp]
        public void Setup()
        {
            file1 = Path.GetTempFileName();
            File.WriteAllText(file1, "Foo bar baz");
            file2 = Path.GetTempFileName();
            File.WriteAllText(file2, "It was the best of times, it was the worst of times");
            zipFile = Path.GetTempFileName();
        }

        [TearDown]
        public void Teardown()
        {
            try { File.Delete(file1); } catch {}
            try { File.Delete(file2); } catch {}
            try { File.Delete(zipFile); } catch {}
        }

        [Test]
        public void can_create_simple_structure()
        {
            var mp = new MultipleFiles();
            mp.AddFile("/test/Foo", file1);
            mp.AddFile("/test/Bar", file2);

            var r = mp.CreateZip(zipFile);
            Assert.AreEqual(r, zipFile);

            var fi = new FileInfo(zipFile);
            Assert.IsTrue(0 < fi.Length);
        }

        [Test]
        public void can_create_more_complex_structure()
        {
            var mp = new MultipleFiles();
            mp.AddFile("/test/dir1/dir2/Foo", file1);
            mp.AddFile("/test/dir1/dir3/Bar", file2);

            var r = mp.CreateZip(zipFile);
            Assert.AreEqual(r, zipFile);

            var fi = new FileInfo(zipFile);
            Assert.IsTrue(0 < fi.Length);
        }
    }
}