﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class ActivityStampTests
    {
        [Test]
        public void test_default_construct()
        {
            var startTime = DateTime.Now;
            var stamp = new ActivityStamp();
            var endTime = DateTime.Now;

            Assert.IsNull(stamp.By);
            Assert.That(stamp.On, Is.GreaterThanOrEqualTo(startTime) & Is.LessThanOrEqualTo(endTime));
        }

        [Test]
        public void test_construct_with_user()
        {
            var user = new User();
            var startTime = DateTime.Now;
            var stamp = new ActivityStamp(user);
            var endTime = DateTime.Now;

            Assert.That(stamp.On, Is.GreaterThanOrEqualTo(startTime) & Is.LessThanOrEqualTo(endTime));
            Assert.IsNotNull(stamp.By);
        }
    }
}
