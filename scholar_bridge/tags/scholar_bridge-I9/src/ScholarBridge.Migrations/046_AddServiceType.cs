﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(46)]
    public class AddServiceType : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "ServiceType";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
