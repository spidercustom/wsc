using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(147)]
    public class AddSeekerAppliedCollegesRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBSeekerCollegeAppliedRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCollegeLUT"; }
        }
    }
}