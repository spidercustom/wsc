using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class IntermediaryDALTest : TestBase
    {
        public IntermediaryDAL IntermediaryDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }

        [Test]
        public void can_create_intermediary()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual("TestProvider", intermediary.Name);
            Assert.IsNotNull(intermediary.Address);
            Assert.IsNotNull(intermediary.Address.State);
            Assert.AreEqual("WI", intermediary.Address.State.Abbreviation);
            Assert.IsNotNull(intermediary.Phone);
        }

        [Test]
        public void can_get_by_id()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            var foundIntermediary = IntermediaryDAL.FindById(intermediary.Id);
            Assert.IsNotNull(foundIntermediary);
            Assert.AreEqual(intermediary.Name, foundIntermediary.Name);
            Assert.AreEqual(intermediary.TaxId, foundIntermediary.TaxId);
            Assert.AreEqual(intermediary.ApprovalStatus, foundIntermediary.ApprovalStatus);
        }

        [Test]
        public void intermediary_starts_as_pending_approval()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual(ApprovalStatus.PendingApproval, intermediary.ApprovalStatus);

            var foundIntermediary = IntermediaryDAL.FindById(intermediary.Id);
            Assert.IsNotNull(foundIntermediary);
            Assert.AreEqual(ApprovalStatus.PendingApproval, foundIntermediary.ApprovalStatus);
        }

        [Test]
        public void can_find_all_intermediaries_pending_approval()
        {

            var initialCount = IntermediaryDAL.FindAllPending().Count;

            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary1 = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);
            var intermediary2 = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual(initialCount + 2, IntermediaryDAL.FindAllPending().Count);

            intermediary1.ApprovalStatus = ApprovalStatus.Approved;
            IntermediaryDAL.Update(intermediary1);

            Assert.AreEqual(initialCount + 1, IntermediaryDAL.FindAllPending().Count);

            intermediary2.ApprovalStatus = ApprovalStatus.Rejected;
            IntermediaryDAL.Update(intermediary2);

            Assert.AreEqual(initialCount, IntermediaryDAL.FindAllPending().Count);
        }

        [Test]
        public void can_add_admin_user_to_intermediary()
        {
            Intermediary intermediary = AddIntermediaryWithAdminUser();

            var foundIntermediary = IntermediaryDAL.FindById(intermediary.Id);
            Assert.IsNotNull(foundIntermediary);
            Assert.IsNotNull(foundIntermediary.AdminUser);
            Assert.IsNotNull(foundIntermediary.ActiveUsers);
            Assert.AreEqual(1, foundIntermediary.ActiveUsers.Count);
        }

        [Test]
        public void can_get_intermediary_by_user()
        {
            Intermediary intermediary = AddIntermediaryWithAdminUser();

            var foundIntermediary = IntermediaryDAL.FindByUser(intermediary.AdminUser);
            Assert.IsNotNull(foundIntermediary);
            Assert.AreEqual(intermediary.Id, foundIntermediary.Id);
        }

        [Test]
        public void can_get_user_by_intermediary_and_id()
        {
            var intermediary = AddIntermediaryWithAdminUser();

            var foundUser = IntermediaryDAL.FindUserInOrg(intermediary, intermediary.AdminUser.Id);
            Assert.IsNotNull(foundUser);
            Assert.AreEqual(intermediary.AdminUser.Id, foundUser.Id);
        }

        [Test]
        public void can_not_get_user_by_other_intermediary_and_id()
        {
            var intermediary = AddIntermediaryWithAdminUser();
            var user2 = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary2 = InsertIntermediary(IntermediaryDAL, StateDAL, "New Provider", user2);

            var foundUser = IntermediaryDAL.FindUserInOrg(intermediary2, intermediary.AdminUser.Id);
            Assert.IsNull(foundUser);
        }

        [Test]
        public void delete_user_shows_up_in_deleted_users_collection()
        {
            var intermediary = AddIntermediaryWithAdminUser();
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            intermediary.Users.Add(user);
            IntermediaryDAL.Update(intermediary);

            UserDAL.Delete(user);

            var foundIntermediary = IntermediaryDAL.FindById(intermediary.Id);
            Assert.IsNotNull(foundIntermediary);

            Assert.AreEqual(1, intermediary.ActiveUsers.Count); // Admin user is still in here
            Assert.AreEqual(1, intermediary.DeletedUsers.Count);
        }

        [Test]
        public void can_add_admin_notes()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);
            
            provider.AppendAdminNotes(user, "Testing the note.");
            IntermediaryDAL.Update(provider);

            provider.AppendAdminNotes(user, "Testing the note again.");
            IntermediaryDAL.Update(provider);

            var foundProvider = IntermediaryDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            StringAssert.Contains("Testing the note.", foundProvider.AdminNotes.ToString());
            StringAssert.Contains("Testing the note again.", foundProvider.AdminNotes.ToString());
        }

        private Intermediary AddIntermediaryWithAdminUser()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var intermediary = InsertIntermediary(IntermediaryDAL, StateDAL, "TestProvider", user);
            intermediary.AdminUser = user;

            IntermediaryDAL.Update(intermediary);
            return intermediary;
        }

        public static Intermediary InsertIntermediary(IIntermediaryDAL intermediaryDAL, StateDAL stateDAL, string name, User user)
        {
            var wi = stateDAL.FindByAbbreviation("WI");
            var intermediary = new Intermediary()
                               {
                                   Name = name,
                                   TaxId = "123-45678",
                                   LastUpdate = new ActivityStamp(user),
                                   Address = new Address
                                                 {
                                                     Street = "123 Foo St.",
                                                     City = "Milwaukee",
                                                     PostalCode = "53212",
                                                     State = wi
                                                 },
                                   Phone = new PhoneNumber("414 431-5593")
                               };

            intermediary = intermediaryDAL.Insert(intermediary);

            Assert.IsNotNull(intermediary);
            Assert.AreNotEqual(0, intermediary.Id);

            return intermediary;
        }
    }
}