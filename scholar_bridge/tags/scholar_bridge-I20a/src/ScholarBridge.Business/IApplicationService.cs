using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IApplicationService
    {
        Application GetById(int id);

        Application Save(Application application);
        void SaveNew(Application application);
        void Update(Application application);
        void Delete(Application application);

        void SubmitApplication(Application application);

        IList<Application> FindAllSubmitted(Scholarship scholarship);
        IList<Application> FindAllSubmitted(Scholarship scholarship, string lastNameSearch);
        IList<Application> FindAllFinalists(Scholarship scholarship);
        
        void Finalist(Application application);
        void NotFinalist(Application application);

		Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship);
		IList<Application> FindBySeeker(Seeker seeker);

        int CountAllSubmittedBySeeker(Seeker seeker);
        int CountAllSubmitted(Scholarship scholarship);

        void OfferScholarship(Application application, User awarder);
        void AwardScholarship(Application application, User awarder);
        void NotAwardScholarship(Application application, User awarder);
    }
}