﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace ScholarBridge.Web.Adapters
{
    public class ValidatorAdapter: WebControlAdapter
    {
        
        protected override void RenderContents(HtmlTextWriter writer)
        {
            
            var validator  = Control as BaseValidator;



            if ( validator.IsValid==false) 
            {
                var message = HttpUtility.HtmlEncode(validator.ErrorMessage + "<br/><br/>");
                string s =
                    "<img group=\"" + validator.ControlToValidate + "\" class=\"coolTipError\" id=error_\"" + Control.ClientID + "\" header=\"This field has following validation error(s)\" content=\"* " +
                    message + "\" src=\"" + Control.Page.ResolveClientUrl("~/Images/cooltipicon_error.png") + "\" />";

                writer.Write(s);
                //base.RenderContents(writer);

            } else
            {
                 
                    base.RenderContents(writer);
                
            }
            
        }
    }
}
