using System;
using Common.Logging;
using Quartz;

namespace ScholarBridge.Jobs
{
    public class LoggerJob : IJob
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LoggerJob));

        public void Execute(JobExecutionContext context)
        {
            log.Info("Executing LoggerJob");
            log.Info(String.Format("{0} : {1}", context.JobDetail.Name, context.JobDetail.Description));
            log.Info(context.FireTimeUtc);
        }
    }
}