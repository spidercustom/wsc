using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(155)]
    public class AddAdminNotesToScholarship : Migration
    {
        private const string TABLE = "SBScholarship";
        private const string COLUMN_NAME = "AdminNotes";

        public override void Up()
        {
            Database.AddColumn(TABLE, COLUMN_NAME, DbType.String, Int32.MaxValue, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE, COLUMN_NAME);
        }
    }
}