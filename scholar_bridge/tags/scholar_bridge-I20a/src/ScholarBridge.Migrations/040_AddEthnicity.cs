﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(40)]
    public class AddEthnicity : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "Ethnicity";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
