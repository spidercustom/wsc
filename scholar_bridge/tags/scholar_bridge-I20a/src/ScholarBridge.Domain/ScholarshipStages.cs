﻿
using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    public enum ScholarshipStages
    {
        None,
        [DisplayName("Not Activated")]
        NotActivated,
        Activated,
        Rejected,
        Awarded
    }


    
}
