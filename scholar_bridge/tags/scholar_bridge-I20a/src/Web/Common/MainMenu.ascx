﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainMenu.ascx.cs" Inherits="ScholarBridge.Web.Common.MainMenu" %>
<asp:LoginView ID="loginView" runat="server">
    <RoleGroups>
        <asp:RoleGroup Roles="Admin,WSCAdmin">
            <ContentTemplate>
                <a href="<%= ResolveClientUrl("~/Admin/PressRoom") %>"><asp:Image ID="adminPressRoomImage" runat="server" ImageUrl="~/images/NavAdmin_EditPagesInactive.gif" Width="145px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Profile") %>"><asp:Image  ID="adminSettingsImage" runat="server" ImageUrl="~/images/NavAdmin_MySettingsInactive.gif" Width="141px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Admin/PendingEmailConfirmation.aspx") %>"><asp:Image ID="adminPendingEmailConfirmationImage" runat="server" ImageUrl="~/images/NavAdmin_PendingEmailInactive.gif" Width="159px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Message") %>"><asp:Image ID="adminMessagesImage" runat="server" ImageUrl="~/images/NavAdmin_OurMsgsInactive.gif" Width="172px" Height="36px" /></a>
            </ContentTemplate>
        </asp:RoleGroup>
        <asp:RoleGroup Roles="Provider Admin,Provider">
            <ContentTemplate>
                <a href="<%= ResolveClientUrl("~/Provider/Scholarships") %>"><asp:Image ID="providerScholarshipsImage" runat="server" ImageUrl="~/images/NavProvider_OurScholarshipsInactive.gif" Width="179px" height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Provider/Admin") %>"><asp:Image ID="providerAdminImage" runat="server" ImageUrl="~/images/NavProvider_OurProfileInactive.gif" Width="124px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Profile") %>"><asp:Image ID="providerSettingsImage" runat="server" ImageUrl="~/images/NavProvider_MySettingsInactive.gif" Width="130px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Message") %>"><asp:Image ID="providerMessagesImage" runat="server" ImageUrl="~/images/NavProvider_OurMsgsInactive.gif" Width="183px" Height="36px" /></a>
            </ContentTemplate>
        </asp:RoleGroup>
        <asp:RoleGroup Roles="Intermediary Admin,Intermediary">
            <ContentTemplate>
                <a href="<%= ResolveClientUrl("~/Intermediary/Scholarships") %>"><asp:Image ID="intermediaryScholarshipsImage" runat="server" ImageUrl="~/images/NavProvider_OurScholarshipsInactive.gif" Width="179px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Intermediary/Admin") %>"><asp:Image ID="intermediaryAdminImage" runat="server" ImageUrl="~/images/NavProvider_OurProfileInactive.gif" Width="124px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Profile") %>"><asp:Image ID="intermediarySettingsImage" runat="server" ImageUrl="~/images/NavProvider_MySettingsInactive.gif" Width="130px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Message") %>"><asp:Image ID="intermediaryMessagesImage" runat="server" ImageUrl="~/images/NavProvider_OurMsgsInactive.gif" Width="183px" Height="36px" /></a>
            </ContentTemplate>
        </asp:RoleGroup>
        <asp:RoleGroup Roles="Seeker">
            <ContentTemplate>
                <a href="<%= ResolveClientUrl("~/Seeker/Profile") %>"><asp:Image  ID="seekerProfileImage" runat="server" ImageUrl="~/images/NavSeeker_MyProfileInactive.gif" Width="124px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Seeker/Matches") %>"><asp:Image ID="seekerMatchesImage" runat="server" ImageUrl="~/images/NavSeeker_MyMatchesInactive.gif" Width="113px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Seeker/Scholarships") %>"><asp:Image ID="seekerApplicationsImage" runat="server" ImageUrl="~/images/NavSeeker_MyApplicationsInactive.gif" Width="141px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Profile") %>"><asp:Image ID="seekerSettingsImage" runat="server" ImageUrl="~/images/NavSeeker_MySettingsInactive.gif" Width="111px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="<%= ResolveClientUrl("~/Message") %>"><asp:Image ID="seekerMessagesImage" runat="server" ImageUrl="~/images/NavSeeker_MyMegsInactive.gif" Width="126px" Height="36px" /></a>
            </ContentTemplate>
        </asp:RoleGroup>
    </RoleGroups>
</asp:LoginView>
<asp:Panel ID="SeekerPanel" runat="server" Visible="false">

    <a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavSeeker_MyProfileInactive.gif" Width="124px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavSeeker_MyMatchesInactive.gif" Width="113px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavSeeker_MyApplicationsInactive.gif" Width="141px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavSeeker_MySettingsInactive.gif" Width="111px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavSeeker_MyMegsInactive.gif" Width="126px" Height="36px" /></a>

</asp:Panel>
<asp:Panel ID="ProviderPanel" runat="server" Visible="false">

    <a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavProvider_OurScholarshipsInactive.gif" Width="179px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavProvider_OurProfileInactive.gif" Width="124px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavProvider_MySettingsInactive.gif" Width="130px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavProvider_OurMsgsInactive.gif" Width="183px" Height="36px" /></a>

</asp:Panel>
<asp:Panel ID="IntermediaryPanel" runat="server" Visible="false">

    <a href="#" onclick="loginalert();">
        <asp:Image runat="server" ImageUrl="~/images/NavProvider_OurScholarshipsInactive.gif"
            Width="179px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif"
                Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server"
                    ImageUrl="~/images/NavProvider_OurProfileInactive.gif" Width="124px" Height="36px" /></a><asp:Image
                        runat="server" ImageUrl="~/images/NavSeparator.gif" Width="2px" Height="36px" /><a
                            href="#" onclick="loginalert();"><asp:Image runat="server" ImageUrl="~/images/NavProvider_MySettingsInactive.gif"
                                Width="130px" Height="36px" /></a><asp:Image runat="server" ImageUrl="~/images/NavSeparator.gif"
                                    Width="2px" Height="36px" /><a href="#" onclick="loginalert();"><asp:Image runat="server"
                                        ImageUrl="~/images/NavProvider_OurMsgsInactive.gif" Width="183px" Height="36px" /></a>

</asp:Panel>
