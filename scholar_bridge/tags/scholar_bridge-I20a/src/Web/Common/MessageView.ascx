﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageView.ascx.cs" Inherits="ScholarBridge.Web.Common.MessageView" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<div class="message">
    <strong>From:</strong> <asp:Literal ID="fromLbl" runat="server" /><br /><br />
    <strong>Date:</strong> <asp:Literal ID="dateLbl" runat="server" /><br /><br />
    <strong>To: </strong><asp:Literal ID="toLbl" runat="server" /><br /><br />
    <h3>Subject: <asp:Literal ID="subjectLbl" runat="server"/></h2>
    <asp:PlaceHolder ID="relatedInfo" runat="server"></asp:PlaceHolder>
    
    <pre>
    <asp:Literal ID="contentLbl" runat="server"/>
    </pre>
</div>

<sbCommon:AnchorButton ID="archiveBtn" runat="server" Text="Archive" 
    onclick="archiveBtn_Click" />
<br />
<sbCommon:AnchorButton ID="approveBtn" runat="server" Text="Approve" 
    onclick="approveBtn_Click" />
<sbCommon:AnchorButton ID="rejectBtn" runat="server" Text="Deny" 
    onclick="rejectBtn_Click" />