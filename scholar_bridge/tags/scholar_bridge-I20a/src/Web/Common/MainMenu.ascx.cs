﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    public partial class MainMenu : UserControl
    {
        
        public IUserContext UserContext { get; set; }
        public string Referrer
        {
            get { return Request.ServerVariables["HTTP_REFERER"] ?? String.Empty; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserContext.CurrentUser ==null)
            {
                SetMenuForAnonymousUser();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            MainMenuHelper.ApplyActiveMenuImage(this);
        }

        private void SetMenuForAnonymousUser()
        {
            if (Referrer.Contains("/Provider/"))
            {
                ProviderPanel.Visible = true;
            }
            else if (Referrer.Contains("/Intermediary/"))
            {
                IntermediaryPanel.Visible = true;
            }
            else
            {
                SeekerPanel.Visible = true;
            }
        }
    }
}

