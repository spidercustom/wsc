﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalFooter.ascx.cs" Inherits="ScholarBridge.Web.Common.GlobalFooter" %>
<DIV class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</DIV>
<DIV class="GreenFooterLinks"><A href="">About Us</A>  |  <A href='<%= LinkGenerator.GetFullLink("/PressRoom/") %>'>Press Room</A>  |  <A href='<%= LinkGenerator.GetFullLink("/Terms.aspx") %>'>Terms and Conditions</A>  |  <A href="#">Contact Us</A>  |  <A href="#">Sitemap</A>  |  <A href="#">Resources</A>  |  <A href="#">Help</A></DIV>

