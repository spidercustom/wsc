﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Intermdiary.Scholarships
{
    public partial class PrintApplications : Page
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship ScholarshipToView { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["id"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = UserContext.CurrentIntermediary;
            if (intermediary.ApprovalStatus != ApprovalStatus.Approved)
                throw new Business.Exceptions.IntermediaryNotApprovedException();

            ScholarshipToView = ScholarshipService.GetById(ScholarshipId);
            if (ScholarshipToView != null)
            {
                if (!ScholarshipToView.Intermediary.Id.Equals(intermediary.Id))
                    throw new InvalidOperationException(
                        "Scholarship does not belong to Intermediary in context");
            }
        }

        protected void print_OnClick(object sender, EventArgs e)
        {
            printChoice.Visible = false;
            printContent.Visible = true;
            
            // Have to do it like this so it picks up the print=true coming as a hidden form parameter.
            var pvCtl = LoadControl("~/Common/PrintView.ascx");
            var pvContainer = new HtmlGenericControl("div");
            pvContainer.Controls.Add(pvCtl);
            printContent.Controls.Add(pvContainer);

            if (printList.Checked)
            {
                var allApplicantsCtl = (AllScholarshipApplicants)LoadControl("~/Common/AllScholarshipApplicants.ascx");
                allApplicantsCtl.Applications = ApplicationService.FindAllSubmitted(ScholarshipToView);
                printContent.Controls.Add(allApplicantsCtl);
            }

            if (printApplications.Checked)
            {
                var allApplicationsCtl = (ShowAllApplications) LoadControl("~/Common/ShowAllApplications.ascx");
                if (applicationChoice.SelectedValue == "all")
                {
                    allApplicationsCtl.Applications = ApplicationService.FindAllSubmitted(ScholarshipToView);
                }
                else
                {
                    allApplicationsCtl.Applications = ApplicationService.FindAllFinalists(ScholarshipToView);
                }

                if (printList.Checked)
                {
                    var spacer = new HtmlGenericControl("div");
                    spacer.Attributes.Add("style", "height: 50px; width:*;");
                    printContent.Controls.Add(spacer);
                }

                printContent.Controls.Add(allApplicationsCtl);
            }
        }
    }
}
