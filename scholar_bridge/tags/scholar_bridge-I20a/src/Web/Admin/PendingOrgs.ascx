﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PendingOrgs.ascx.cs" Inherits="ScholarBridge.Web.Admin.PendingOrgs" %>
 <%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:ListView ID="pendingOrgsListView" runat="server" 
    onitemdatabound="pendingOrgsListView_ItemDataBound">
    <LayoutTemplate>
        <table class="sortableTable">
                <thead>
                <tr>
                    <th>Admin Name</th>
                    <th>Org Name</th>
                    <th>Email Address</th>
                    <th>Date Sent</th>
                    <th>Confirmed</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
              
    <ItemTemplate>
        <tr class="row">
            <td><%# Eval("AdminUser.Name")%></td>
            <td><asp:HyperLink id="linktoOrg" NavigateUrl='<%# string.Format("{0}?id={1}", LinkTo, Eval("Id")) %>' runat="server"><%# Eval("Name") %></asp:HyperLink></td>
            <td><%# string.Format("<a href='mailto:{0}'>{0}</a>", Eval("AdminUser.Email"))%></td>
            <td><%# Eval("AdminUser.LastUpdate.On", "{0:d}")%></td>
            <td><%# Eval("AdminUser.IsActive") %></td>
            <td><sbCommon:ConfirmButton ID="resendCnfBtn" ConfirmMessageDivID="resendConfirmDiv" 
                Text="Resend"  runat="server" OnClickConfirm="resendCnfBtn_Click" Width="60px"  /> </td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><%# Eval("AdminUser.Name")%></td>
            <td><asp:HyperLink id="linktoOrg" NavigateUrl='<%# string.Format("{0}?id={1}", LinkTo, Eval("Id")) %>' runat="server"><%# Eval("Name") %></asp:HyperLink></td>
            <td><%# string.Format("<a href='mailto:{0}'>{0}</a>", Eval("AdminUser.Email"))%></td>
            <td><%# Eval("AdminUser.LastUpdate.On", "{0:d}")%></td>
            <td><%# Eval("AdminUser.IsActive") %></td>
            <td></td>
        </tr>
    </AlternatingItemTemplate>
</asp:ListView>
    <div id="resendConfirmDiv" title="Confirm Resend" style="display:none">
            Are you sure want to resend?
    </div> 
    <asp:ScriptManager ID="scriptManager1" runat="server" ></asp:ScriptManager>  
