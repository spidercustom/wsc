﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="EditProgramGuidelines.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.EditProgramGuidelines" Title="Scholarship | Edit Program Guidelines" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>             
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HasPrintView="false" />
<p class="additionInfoText">
Please enter the guidelines to be followed in the administration and award of this scholarship program. These guidelines should be based on the desire of the donor, and need to be reviewed annually (2000 character limit).
</p>

<label for="ProgramGuidelinesControl">Program Guidelines:</label>
<asp:TextBox ID="ProgramGuidelinesControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="ProgramGuidelinesValidator" runat="server" ControlToValidate="ProgramGuidelinesControl" PropertyName="ProgramGuidelines" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
<br />

<sbCommon:AnchorButton ID="saveBtn" runat="server" Text="Save"  
        onclick="saveBtn_Click" /> 
<sbCommon:AnchorButton ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" 
        onclick="cancelBtn_Click" />           

</asp:Content>
