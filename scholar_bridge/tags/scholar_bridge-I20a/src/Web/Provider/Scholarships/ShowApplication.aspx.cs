﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class ShowApplication : Page
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }
        
        public Application ApplicationToView { get; set; }

        private int ApplicationId
        {
            get
            {
                int applicationId;
                if (!Int32.TryParse(Request.Params["id"], out applicationId))
                    throw new ArgumentException("Cannot understand value of parameter id");
                return applicationId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Business.Exceptions.ProviderNotApprovedException();

            ApplicationToView = ApplicationService.GetById(ApplicationId);
            if (ApplicationToView != null)
            {
                if (!ApplicationToView.Scholarship.Provider.Id.Equals(provider.Id))
                    throw new InvalidOperationException("Application does not belong to provider in context");

                ScholarshipTitleStripeControl.UpdateView(ApplicationToView.Scholarship);
                showApplication.ApplicationToView = ApplicationToView;
            }
        }
    }
}
