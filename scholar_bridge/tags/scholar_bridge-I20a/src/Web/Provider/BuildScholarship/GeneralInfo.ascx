﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfo.ascx.cs"
    Inherits="ScholarBridge.Web.Provider.BuildScholarship.GeneralInfo" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedIntermediary" Src="~/Common/SelectRelatedIntermediary.ascx" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedProvider" Src="~/Common/SelectRelatedProvider.ascx" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  

<span><span class="requiredAttributeIndicator">*</span> = required</span><br />
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">Complete your profile</p>
        
        <label for="ScholarshipNameControl">Scholarship Name:<span class="requiredAttributeIndicator">*</span></label>
        <elv:PropertyProxyValidator ID="ScholarshipNameValidator" Display="Dynamic" runat="server"
            ControlToValidate="ScholarshipNameControl" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Scholarship" />
 
        <asp:TextBox CssClass="longtextarea" ID="ScholarshipNameControl" MaxLength="100" runat="server"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfo1" Content="This is the Name of the Scholarship that will be displayed to Seekers. They will also be able to search for Scholarships by Name." runat="server" />
         <br />
        <label for="ScholarshipAcademicYearControl">
            Academic Year:<span class="requiredAttributeIndicator">*</span></label>
        <elv:PropertyProxyValidator ID="ScholarshipAcademicYearValidator" runat="server"
            ControlToValidate="ScholarshipAcademicYearControl" PropertyName="AcademicYear"
            SourceTypeName="ScholarBridge.Domain.Scholarship" />
       
        <asp:DropDownList ID="ScholarshipAcademicYearControl" runat="server" />
        <sb:CoolTipInfo Content="Scholarships are presented to Seekers for the upcoming or current Academic year. Provider’s need to annually review and renew scholarship to ensure data is current and funds are available. Even is scholarships are awarded for more than a year, the scholarship must be recertified." runat="server" />
      
        <br />
        <label for="MissionStatementControl">
            Mission Statement:</label>
        <elv:PropertyProxyValidator ID="MissionStatementValidator" runat="server" ControlToValidate="MissionStatementControl"
            PropertyName="MissionStatement" SourceTypeName="ScholarBridge.Domain.Scholarship" />
    
        <asp:TextBox ID="MissionStatementControl" runat="server" TextMode="MultiLine"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfo2" runat="Server" Content="The mission statement is the description of the scholarship that will display to seekers. Note: This is not a narrative version of criteria but rather the inspiration that motivated the scholarship founder. It is limited to 250 words to provide focus." />  
        <br />
        <label for="ProgramGuidelinesControl">
            Program Guidelines:</label>
        <elv:PropertyProxyValidator ID="ProgramGuidelinesValidator" runat="server" ControlToValidate="ProgramGuidelinesControl"
            PropertyName="ProgramGuidelines" SourceTypeName="ScholarBridge.Domain.Scholarship" />
      
        <asp:TextBox ID="ProgramGuidelinesControl" runat="server" TextMode="MultiLine"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfo3" runat="Server" Content="This information is NOT displayed to Seekers. These are instructions for the Scholarship Administrators involved in application review and award selection." />  
        <br />
        
        <hr />
        <P class="form-section-title">Donor Information</P>

        <label for="DonorName">
            Name:</label>
        <elv:PropertyProxyValidator ID="DonorNameValidator" runat="server" ControlToValidate="DonorName"
            PropertyName="Name" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
      
        <asp:TextBox ID="DonorName" runat="server"></asp:TextBox>
         <br />
        <label for="AddressStreet">
            Address Line 1:</label>
        <elv:PropertyProxyValidator ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet"
            PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     
        <asp:TextBox ID="AddressStreet" runat="server"></asp:TextBox>
         <br />
        <label for="AddressStreet2">
            Address Line 2:</label>
        <elv:PropertyProxyValidator ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2"
            PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     
        <asp:TextBox ID="AddressStreet2" runat="server"></asp:TextBox>
        <br />
        <label for="AddressCity">
            City:</label>
        <elv:PropertyProxyValidator ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity"
            PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        
        <asp:TextBox ID="AddressCity" runat="server"></asp:TextBox>
        <br />
        <label for="AddressState">
            State:</label>
        <elv:PropertyProxyValidator ID="AddressStateValidator" runat="server" ControlToValidate="AddressState"
            PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     
        <asp:DropDownList ID="AddressState" runat="server">
        </asp:DropDownList>
        <br />
        <label for="AddressPostalCode">
            Postal Code:</label>
        <elv:PropertyProxyValidator ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode"
            PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     
        <asp:TextBox ID="AddressPostalCode" runat="server"></asp:TextBox>
        <br />
        <label for="Phone">
            Phone:</label>
        <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="Phone"
            PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
   
        <asp:TextBox ID="Phone" runat="server" CssClass="phone" />
        <br />
        <label for="EmailAddress">
            Email Address:</label>
       <elv:PropertyProxyValidator ID="EmailAddressValidator" runat="server" ControlToValidate="EmailAddress"
            PropertyName="Email" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
        <asp:RegularExpressionValidator ID="emailLengthValidator" runat="server" ControlToValidate="EmailAddress"
            ValidationExpression="^.{0,50}$" ErrorMessage="Email Address is should be less than 50 characters long." />
     
        <asp:TextBox ID="EmailAddress" runat="server"></asp:TextBox>
         <br />
        <hr />

        <P class="form-section-title">Organizations</P>
        
        <label for="ProviderSelected">Provider:</label>
        <elv:PropertyProxyValidator ID="ProviderSelectedValidator" runat="server" ControlToValidate="ProviderSelected"
            PropertyName="Provider" SourceTypeName="ScholarBridge.Domain.Scholarship" OnValueConvert="providerValidator_OnValueConvert" />
   
        <sb:SelectRelatedProvider ID="ProviderSelected" runat="server" />
        <sb:CoolTipInfo ID="CoolTipInfo4" runat="Server" Content="The Scholarship Provider is the Organization that manages the Scholarship funds.  The Provider Organization will have access in theWashBoard.org to administer the scholarship." />  

        <br />
        
        <label for="IntermediarySelected">Intermediary:</label>
        <sb:SelectRelatedIntermediary ID="IntermediarySelected" runat="server" />
        <sb:CoolTipInfo ID="CoolTipInfo5" runat="Server" Content="The Scholarship Intermediary is an Organization that the Provider has established a relationship with (under Our Profile tab) and has enlisted to assist in the administration of the scholarship. The Intermediary Organization will have access in theWashBoard.org to administer the scholarship" />  


    </div>        
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">  
        <P class="form-section-title">Schedule</P>
     
        <label for="calAppStart">Application Start Date</label>
         <elv:PropertyProxyValidator ID="ApplicationStartDateValidator" runat="server" ControlToValidate="calApplicationStartDate"
            PropertyName="ApplicationStartDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        
        <sb:CalendarControl ID="calApplicationStartDate" runat="server"></sb:CalendarControl>
        <sb:CoolTipInfo ID="CoolTipInfo6" runat="Server" Content="This date notifies seekers that you are accepting scholarship applications for the current academic year. If scholarship applications are accepted year round, then use today’s date. Applications can be submitted as soon as the scholarship has been activated." />  

        <br />
        <label for="calApplicationDueDate">
            Application Due Date</label>
         <elv:PropertyProxyValidator ID="ApplicationDueDateValidator" runat="server" ControlToValidate="calApplicationDueDate"
            PropertyName="ApplicationDueDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
      
        <sb:CalendarControl ID="calApplicationDueDate" runat="server"></sb:CalendarControl>
        <sb:CoolTipInfo ID="CoolTipInfo7" runat="Server" Content="This is the date the scholarship applications are due.  Applications will not be accepted on-line after this date. Once the scholarship is activated, applications are notified that they have until this date to submit applications. The date can only be extended after scholarship is activated." />  

         <br />
        
        <label for="calAwardDate">
            Scholarship Notification/Award Date:</label>
           <elv:PropertyProxyValidator ID="AwardDateValidator" runat="server" ControlToValidate="calAwardDate"
            PropertyName="AwardDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
     
        <sb:CalendarControl ID="calAwardDate" runat="server"></sb:CalendarControl>
        <sb:CoolTipInfo ID="CoolTipInfo8" runat="Server" Content="This is the date the provider anticipates notifying scholarship awardees. For information only." />  

        <br />
        
        <P class="form-section-title">Scholarship Funding</P>
        <label for="AnnualSupportAmount">
            Annual Amount of Support:<span class="requiredAttributeIndicator">*</span></label>
        <elv:PropertyProxyValidator ID="AnnualSupportAmountValidator" runat="server" ControlToValidate="AnnualSupportAmount"
            PropertyName="AnnualSupportAmount" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
      
        <sandTrap:CurrencyBox ID="AnnualSupportAmount" runat="server" Precision="0" />
          <br />
        
        <label for="MinimumNumberOfAwards">
            Minimum Number of Awards:<span class="requiredAttributeIndicator">*</span></label>
        <elv:PropertyProxyValidator ID="MinimumNumberOfAwardsValidator" runat="server" ControlToValidate="MinimumNumberOfAwards"
            PropertyName="MinimumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
  
        <sandTrap:NumberBox ID="MinimumNumberOfAwards" runat="server" Precision="0" />
        <br />
        
        <label for="MaximumNumberOfAwards">
            Maximum Number of Awards:<span class="requiredAttributeIndicator">*</span></label>
        <elv:PropertyProxyValidator ID="MaximumNumberOfAwardsValidator" runat="server" ControlToValidate="MaximumNumberOfAwards"
            PropertyName="MaximumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
       
        <sandTrap:NumberBox ID="MaximumNumberOfAwards" runat="server" Precision="0" />
        <br />
        
        <label for="TermsOfSupport">
            Terms of Support:<span class="requiredAttributeIndicator">*</span></label>
        <asp:RadioButtonList ID="TermsOfSupportControl" runat="server" class="control-set" />
        <br />
        
        <label for="MinimumAmount">
            Minimum Scholarship Award Amount:<span class="requiredAttributeIndicator">*</span></label>
        <elv:PropertyProxyValidator ID="MinimumAmountValidator" runat="server" ControlToValidate="MinimumAmount"
            PropertyName="MinimumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship" />
      
        <sandTrap:CurrencyBox ID="MinimumAmount" runat="server" Precision="0" />
         <br />
        
        <label for="MaximumAmount">
            Maximum Scholarship Award Amount:<span class="requiredAttributeIndicator">*</span></label>
        <elv:PropertyProxyValidator ID="MaximumAmountValidator" runat="server" ControlToValidate="MaximumAmount"
            PropertyName="MaximumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship" />
   
        <sandTrap:CurrencyBox ID="MaximumAmount" runat="server" Precision="0" />
        <br />
    </div>
</div>