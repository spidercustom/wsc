﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Profile.Default"  Title="MyProfile" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="~/Seeker/Profile/Basics.ascx" tagname="Basics" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AboutMe.ascx" tagname="AboutMe" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AcademicInformation.ascx" tagname="AcademicInfo" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/Activities.ascx" tagname="Activities" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/FinancialNeed.ascx" tagname="FinancialNeed" tagprefix="sb" %>
<%@ Register TagPrefix="sb" TagName="SeekerProfileActivationValidationErrors" Src="~/Common/SeekerProfileActivationValidation.ascx" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerProfile.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div id="confirmSaving" title="Confirm saving" style="display:none">
        You have change the data. Would you like to save the changes?
    </div>
    <div ID="confirmSavingActivatedProfile" title="Confirm saving" style="display:none">
        <asp:Literal ID="ConfirmSaveIfActivatedLabel" runat="server" />
    </div>
    <sb:SeekerProfileActivationValidationErrors id="seekerProfileActivationValidationErrors" runat="server" />
    <!--Left floated content area starts here-->
    <DIV id="HomeContentLeft">
      <Img src="<%= ResolveUrl("~/images/PgTitle_MyProfile.gif") %>" width="249px" height="54px">
      <img src="<%= ResolveUrl("~/images/EmphasisedMyProfilePage.gif") %>" width="513px" height="79px">
      </DIV>
      <!--Left floated content area ends here-->

     <!--Right Floated content area starts here-->
     <DIV id="HomeContentRight">
     <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" ShowGotoProfile="false" />
     </DIV>
    <BR><BR>

    <DIV id="Clear"></DIV>
    <div class="tabs" id="BuildSeekerProfileWizardTab">
        <ul class="linkarea">
            <li><a  href="#tab"><span>Basics</span></a></li>
            <li><a  href="#tab"><span>About Me</span></a></li>
            <li><a href="#tab"><span>My Academic Info</span></a></li>
            <li><a href="#tab"><span>My Activities</span></a></li>
            <li><a href="#tab"><span>My Financial Need</span></a></li>
        </ul>
         
        <div id="tab">
            <asp:MultiView  ID="BuildSeekerProfileWizard" 
                            runat="server"
                            OnActiveViewChanged="BuildSeekerProfileWizard_ActiveStepChanged">
              <asp:View ID="BasicInfoStep" runat="server">
                    <sb:Basics ID="Basics1" runat="server" />
              </asp:View>
              <asp:View ID="AboutMeStep" runat="server">
                    <sb:AboutMe ID="AboutMe1" runat="server" />
              </asp:View>
              <asp:View ID="AcademicStep" runat="server">
                    <sb:AcademicInfo ID="AcademicInfo1" runat="server" />
              </asp:View>
              <asp:View ID="ActivitiesStep" runat="server">
                    <sb:Activities ID="Activities1" runat="server"  />
              </asp:View>
              <asp:View ID="FinancialNeed" runat="server">
                    <sb:FinancialNeed ID="FinancialNeed1" runat="server" />
              </asp:View>
            </asp:MultiView>
        </div>
        <sbCommon:jQueryTabIntegrator id="buildSeekerProfileWizardTabIntegrator" 
                                runat="server" 
                                CausesValidation="true"
                                MultiViewControlID="BuildSeekerProfileWizard"
                                TabControlClientID="BuildSeekerProfileWizardTab" />

    </div>
    <div id="Clear"></div>
    <div id="ButtonWrapper">
        <div id="ButtonContainerLeft">
            <sbCommon:SaveConfirmButton ID="SaveButton" ToolTip="Save all work on this tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" runat="server" OnClick="SaveButton_Click" Text="Save & Exit"/>
        </div>
        <div id="ButtonContainerRight">
            <sbCommon:SaveConfirmButton ID="PreviousButton" ToolTip="Save & navigate to the prior tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" runat="server" OnClick="PreviousButton_Click" Text="Previous"/>
            <sbCommon:SaveConfirmButton ID="NextButton" ToolTip="Save & navigate to the next tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" OnClick="NextButton_Click" runat="server" Text="Next" />
        </div>
    </div>
</asp:Content>
