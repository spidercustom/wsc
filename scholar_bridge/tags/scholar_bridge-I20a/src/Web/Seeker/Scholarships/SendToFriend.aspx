﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SendToFriend.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Scholarships.SendToFriend" Title="Seeker | Scholarships | Send To Friend" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="~/Common/ScholarshipPublicView.ascx" tagname="ScholarshipPublicView" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
 <h3>Send to Friends</h3>
 <br />
 <br />
 
     <div>Enter up to 5 friend's email addresses, separated by commas. <br />
        <br />
         <asp:TextBox ID="EmailBox" runat="server" Width="750px"></asp:TextBox>  
          <br />
          Email addresses you provide here will not be used for any other purpose. For more information see our privacy statement in 
            <asp:HyperLink ID="tcLnk" runat="server" NavigateUrl="~/Seeker/Terms.aspx" Target="_blank"  class="terms">Terms of Use</asp:HyperLink>
             <br />
            <asp:RequiredFieldValidator ID="emailRequired" ControlToValidate="EmailBox" Display="Dynamic" runat="server" ErrorMessage="Friend's Email address is required" ToolTip="Please enter your friend's email address"></asp:RequiredFieldValidator>
             <br />
            <asp:RegularExpressionValidator ID="emailValidator" runat="server" ControlToValidate="EmailBox" ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([,])*)*" ErrorMessage="Please enter valid email addresses seperated by comma without space. example john@ythewashboard.org,tam@thewashboard.org"/>
            <br />
     </div>
      
     
    <label for="CommentBox">Your Comments (240 chars limit.):</label>
    <br />
    <asp:TextBox ID="CommentBox" runat="server" Height="150px" Width="750px" TextMode="MultiLine" Rows="5"></asp:TextBox>
    <asp:RegularExpressionValidator ID="commentsLengthValidator" runat="server" ControlToValidate="CommentBox" ValidationExpression="^(.|\n){0,240}$" ErrorMessage="Comment should not exceed 240 characters limit."/>
    <br />
    <sb:ScholarshipPublicView ID="ScholarshipPublicView1" runat="server"  />
<br />

<div> <sbCommon:AnchorButton ID="btnSend" Text="Send" runat="server" onclick="btnSend_Click" />  </div>
</asp:Content>
