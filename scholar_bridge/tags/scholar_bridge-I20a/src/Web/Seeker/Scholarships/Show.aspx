﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Scholarships.Show" Title="Seeker | Scholarships | Show" %>
<%@ Register src="~/Common/ShowScholarship.ascx" tagname="ShowScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PrintViewHead" runat="server">
    <style type="text/css">
        .right-align
        {
        	float:right;
        	width:480px;
        	vertical-align: middle;
        	height:120px; 
        	margin-bottom:2px;
        }
        
        .center-align-to-print
        {
        	padding-top: 40px;
        	float:left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
    <div class="right-align non-printable">
        <div class="center-align-to-print">
            <sbCommon:AnchorButton ID="AddtoMyScholarshipBtn" runat="server"  Text="Add to My Scholarships of Interest" OnClick="AddtoMyScholarshipBt_Click"/>
            <asp:Label ID="spacer" runat="server" Text="&nbsp;&nbsp;"></asp:Label>
            <sbCommon:AnchorButton ID="sendToFriendBtn" NavigateUrl="mailto:" runat="server" Text="Send to a friend" />
        </div>
        <sb:PrintView id="PrintViewControl" runat="server" /> 
    </div>
</asp:Content>

<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">



<sb:ScholarshipTitleStripe id="scholarshipTitleStripeControl" runat="server" HideInPrintView="true" />
<sb:ShowScholarship id="showScholarship" runat="server" />
</asp:Content>
