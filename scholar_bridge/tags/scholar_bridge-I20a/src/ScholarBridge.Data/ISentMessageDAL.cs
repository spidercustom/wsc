﻿using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface ISentMessageDAL : IDAL<SentMessage>
    {
        /// <summary>
        /// Ensures user has access to the Message by passing relavent context
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <param name="organization"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        SentMessage FindById(User user, IList<Role> roles, Organization organization, int id);

        IList<SentMessage> FindAll(User user, IList<Role> roles, Organization organization);
    }
}