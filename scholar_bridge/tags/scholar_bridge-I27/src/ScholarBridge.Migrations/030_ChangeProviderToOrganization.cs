using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(30)]
    public class ChangeProviderToOrganization : Migration
    {
        public override void Up()
        {
            var orgTypecolumn = new Column("OrganizationType", DbType.String, 30, ColumnProperty.Null);

            Database.RenameTable("SB_Provider", "SB_Organization");
            Database.AddColumn("SB_Organization", orgTypecolumn);
            Database.Update("SB_Organization", new[] {"OrganizationType"}, new []{"Provider"});

            orgTypecolumn.ColumnProperty = ColumnProperty.NotNull;
            Database.ChangeColumn("SB_Organization", orgTypecolumn);

            Database.RenameTable("SB_UserProviderRT", "SB_UserOrganizationRT");
            Database.RenameColumn("SB_UserOrganizationRT", "ProviderId", "OrganizationId");
        }

        public override void Down()
        {
            Database.RenameTable("SB_Organization", "SB_Provider");
            Database.RemoveColumn("SB_Provider", "OrganizationType");

            Database.RenameTable("SB_UserOrganizationRT", "SB_UserProviderRT");
            Database.RenameColumn("SB_UserProviderRT", "OrganizationId", "ProviderId");
        }
    }
}