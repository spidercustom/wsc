using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(108)]
    public class AddProgramGuidelinesToScholarship : Migration
    {
        public const string TABLE_NAME = "SBScholarship";
        public const string COLUMN_NAME = "ProgramGuidelines";

        public override void Up()
        {
            // nvarchar(max)
            Database.AddColumn(TABLE_NAME, new Column(COLUMN_NAME, DbType.String, 6000));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
        }
    }
}