using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public class MatchService : IMatchService
    {
        private static readonly MatchStatus[] SAVED_AND_APPLIED_STATUS = new[] { MatchStatus.Saved, MatchStatus.Applied };
        public IMatchDAL MatchDAL { get; set; }
        public ISeekerDAL SeekerDAL { get; set; }

        public Match GetMatch(Seeker seeker, int scholarshipId)
        {
            return MatchDAL.Find(seeker, scholarshipId);
        }
        
        public void SaveMatch(Seeker seeker, int scholarshipId)
        {
            ChangeStatus(seeker, scholarshipId, MatchStatus.Saved);
        }

        public void InsertCustomMatch(Match match)
        {
            if (null == match)
                throw new ArgumentNullException("match");

            MatchDAL.Insert(match);
        }
        
        public void UnSaveMatch(Seeker seeker, int scholarshipId)
        {
            ChangeStatus(seeker, scholarshipId, MatchStatus.New);
        }

        public void ApplyForMatch(Seeker seeker, int scholarshipId, Application application)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            var match = GetMatch(seeker, scholarshipId);
            match.Application = application;
            match.MatchStatus = MatchStatus.Applied;
            MatchDAL.Update(match);

            return;
        }

        public void ChangeStatus(Seeker seeker, int scholarshipId, MatchStatus newStatus)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            var match = MatchDAL.Find(seeker, scholarshipId);
            if (null != match)
            {
                match.MatchStatus = newStatus;
                match.LastUpdate = new ActivityStamp(seeker.User);
                MatchDAL.Update(match);
            }
        }

        public IList<Match> GetMatchesForSeeker(Seeker seeker)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            if ((! seeker.LastMatch.HasValue) || seeker.LastMatch.Value < seeker.ProfileLastUpdateDate)
            {
                MatchDAL.UpdateMatches(seeker);
                seeker.LastMatch = DateTime.Now;
                SeekerDAL.Update(seeker);
            }
            return MatchDAL.FindAllCurrentMatches(seeker);
        }

        public IList<Match> GetSavedMatches(Seeker seeker)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            return MatchDAL.FindAll(seeker, SAVED_AND_APPLIED_STATUS);
        }

        public IList<Match> GetSavedButNotAppliedMatches(Seeker seeker)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            return MatchDAL.FindAll(seeker, MatchStatus.Saved);
        }

        public IList<Match> GetAppliedMatches(Seeker seeker)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            return MatchDAL.FindAll(seeker, MatchStatus.Applied);
        }

        public IList<Match> GetSavedMatches(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return MatchDAL.FindAll(scholarship, SAVED_AND_APPLIED_STATUS);
        }

        public IList<Match> GetSavedMatchesWithApplications(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return MatchDAL.FindAllWithApplications(scholarship, new [] {MatchStatus.Saved});
        }

        public void UpdateMatches(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            MatchDAL.UpdateMatches(scholarship);
        }

        public void DisconnectApplication(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            var matches = MatchDAL.FindAll(application);
            if (null != matches)
            {
                foreach (Match match in matches)
                {
                    match.Application = null;
                    match.MatchStatus = MatchStatus.Saved;
                    MatchDAL.Update(match);
                }
            }
        }
    }
}