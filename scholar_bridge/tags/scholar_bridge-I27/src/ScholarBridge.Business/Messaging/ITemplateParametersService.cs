using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Messaging
{
    public interface ITemplateParametersService
    {
        void ConfirmationLink(User user, bool requireResetPassword, MailTemplateParams templateParams);
        void EmailAddressChangeVerificationLink(User user, MailTemplateParams templateParams);

        void ScholarshipApproved(Scholarship scholarship, User user, MailTemplateParams templateParams);
        void ScholarshipClosed(Scholarship scholarship, MailTemplateParams templateParams);

        void RequestOrganizationApproval(User user, Organization organization, MailTemplateParams templateParams);
        void OrganizationApproved(User user, MailTemplateParams templateParams);
        void OrganizationRejected(User user, MailTemplateParams templateParams);

        void RelationshipRequestCreated(Organization organization, MailTemplateParams templateParams);
        void RelationshipInActivated(Organization organization, MailTemplateParams templateParams);
        void RelationshipRequestApproved(Organization organization, MailTemplateParams templateParams);
        void RelationshipRequestRejected(Organization organization, MailTemplateParams templateParams);

        void ListChangeRequest(Organization organization, User user, string listType, string value, string reason,
                               MailTemplateParams templateParams);

        void AwardScholarship(Application application, MailTemplateParams templateParams);
        void OfferScholarship(Application application, MailTemplateParams templateParams);

        void ApplicationDue(Scholarship scholarship, int days, MailTemplateParams templateParams);
        void ApplicationSubmisionSuccess(Application application, MailTemplateParams templateParams);
        void ApplicationAcknowledgement(Application application, MailTemplateParams templateParams);

        void ScholarshipSendToFriend(Scholarship scholarship, string userComments, MailTemplateParams templateParams,
                                     string domainName, string senderEmail);

    	void ContactUs(string emailAddress, User user, string messageText, MailTemplateParams templateParams);

        void RegistrationInterest(string email, MailTemplateParams templateParams);
    }
}