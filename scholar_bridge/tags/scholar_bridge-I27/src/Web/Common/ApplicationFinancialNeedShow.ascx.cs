﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Seeker.BuildApplication;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationFinancialNeedShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                if (null != ApplicationToView.SupportedSituation)
                {
                    typesOfSupport.DataSource = ApplicationToView.SupportedSituation.TypesOfSupport;
                    typesOfSupport.DataBind();
                }
            }
        }

        public override int ResumeFrom
        {
            get { return WizardStepName.FinancialNeed.GetNumericValue(); }
        }
    }
}