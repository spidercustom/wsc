﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Seeker.BuildApplication;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationActivitiesShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                academicAreas.DataSource = ApplicationToView.AcademicAreas;
                academicAreas.DataBind();
                academicAreasOther.Text = ApplicationToView.AcademicAreaOther;

                careers.DataSource = ApplicationToView.Careers;
                careers.DataBind();
                careersOther.Text = ApplicationToView.CareerOther;

                communityServices.DataSource = ApplicationToView.CommunityServices;
                communityServices.DataBind();
                communityServicesOther.Text = ApplicationToView.CommunityServiceOther;

                hobbies.DataSource = ApplicationToView.Hobbies;
                hobbies.DataBind();
                hobbiesOther.Text = ApplicationToView.HobbyOther;

                sports.DataSource = ApplicationToView.Sports;
                sports.DataBind();
                sportsOther.Text = ApplicationToView.SportOther;

                clubs.DataSource = ApplicationToView.Clubs;
                clubs.DataBind();
                clubsOther.Text = ApplicationToView.ClubOther;

                organizations.DataSource = ApplicationToView.MatchOrganizations;
                organizations.DataBind();
                organizationsOther.Text = ApplicationToView.MatchOrganizationOther;

                affiliations.DataSource = ApplicationToView.AffiliationTypes;
                affiliations.DataBind();
                affiliationsOther.Text = ApplicationToView.AffiliationTypeOther;

                workTypes.DataSource = ApplicationToView.WorkTypes;
                workTypes.DataBind();
                workTypesOther.Text = ApplicationToView.WorkTypeOther;

                workHours.DataSource = ApplicationToView.WorkHours;
                workHours.DataBind();
                workHoursOther.Text = ApplicationToView.WorkHourOther;

                serviceTypes.DataSource = ApplicationToView.ServiceTypes;
                serviceTypes.DataBind();
                serviceTypesOther.Text = ApplicationToView.ServiceTypeOther;

                serviceHours.DataSource = ApplicationToView.ServiceHours;
                serviceHours.DataBind();
                serviceHoursOther.Text = ApplicationToView.ServiceHourOther;

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);

            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {

            AcademicAreasRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.AcademicArea);
            CareersRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Career);
            AcademicIntBlock.Visible =
                AcademicAreasRow.Visible ||
                CareersRow.Visible;
            
            OrganizationsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Organization);
            AffiliationTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Company);
            OrgAffilBlock.Visible =
                OrganizationsRow.Visible ||
                AffiliationTypesRow.Visible;

            CommunityServicesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.CommunityService);
            HobbiesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Honor);
            SportsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Sport);
            ClubsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Club);
            GroupsBlock.Visible =
                CommunityServicesRow.Visible ||
                HobbiesRow.Visible ||
                SportsRow.Visible ||
                ClubsRow.Visible;

            WorkTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.WorkType);
            WorkHoursRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.WorkHour);
            ServiceTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ServiceType);
            ServiceHoursRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ServiceHour);
            WorkServiceBlock.Visible =
                WorkTypesRow.Visible ||
                WorkHoursRow.Visible ||
                ServiceTypesRow.Visible ||
                ServiceHoursRow.Visible;
        }

        public override int ResumeFrom
        {
            get { return WizardStepName.Activities.GetNumericValue(); }
        }
    }
}