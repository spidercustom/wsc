﻿using System;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Web.Security;


namespace ScholarBridge.Web.Common
{
    public partial class RegisterOrganization : UserControl
    {
        public IProviderService ProviderService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        public IUserService UserService { get; set; }
        public IStateDAL StateService { get; set; }
        public string Title { get; set; }
        public Type OrganizationType { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            pageTitle.Text = Title;
            if (!Page.IsPostBack)
            {
                AddressState.DataSource = StateService.FindAll();
                AddressState.DataTextField = "Name";
                AddressState.DataValueField = "Abbreviation";
                AddressState.DataBind();
                AddressState.Items.Insert(0, new ListItem("- Select One -", ""));
                CaptchaControl1.ValidationGroup = "CreateUserWizard1";
            }
        }

        private static PhoneNumber GetPhone(ITextControl phoneBox)
        {
            string phone = phoneBox.Text;
            if (String.IsNullOrEmpty(phone))
                return null;

            return new PhoneNumber(phone);
        }

        private Address GetAddress()
        {
        	string street = AddressStreet.Text;
        	string street2 = AddressStreet2.Text;
        	string city = AddressCity.Text;
        	string postalCode = AddressPostalCode.Text;
        	string state = AddressState.SelectedValue;

            return new Address
            {
                Street = street,
                Street2 = street2,
                City = city,
                PostalCode = postalCode,
                State = StateService.FindByAbbreviation(state)
            };
        }

		protected void RegisterButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
                CaptchaControl1.ClearGuess();
                return;
			}
            
			Page.Validate("CreateUserWizard1");
			if (!Page.IsValid)
			{
                CaptchaControl1.ClearGuess();
                return;
			}

			var provider = new SpiderMembershipProvider();
			provider.Initialize(string.Empty, new NameValueCollection());
			User user =	provider.CreateUserInstance
				(
					UserName.Text, Password.Text, UserName.Text, null, null, false
				);
			
			if (user == null)
			{
				switch (provider.CreateUserStatus)
				{
					case MembershipCreateStatus.InvalidEmail:
						EmailCustomValidator.Text = "Email Address is Invalid";
						EmailCustomValidator.IsValid = false;
						return;
					case MembershipCreateStatus.DuplicateEmail:
						EmailCustomValidator.Text = "Email Address is already in use by another user";
						EmailCustomValidator.IsValid = false;
						return;
					case MembershipCreateStatus.DuplicateUserName:
						EmailCustomValidator.Text = "Email Address is already in use by another user";
						EmailCustomValidator.IsValid = false;
						return;
					default:
						throw new ApplicationException("Unhandled User Creation status event: " + provider.CreateUserStatus);
				}
			}
			user.Name.FirstName = FirstName.Text;
			user.Name.MiddleName = MiddleName.Text;
			user.Name.LastName = LastName.Text;

			var newOrganization = (Organization)Activator.CreateInstance(OrganizationType);
			newOrganization.Name = Name.Text;
			newOrganization.TaxId = TaxId.Text;
			newOrganization.Website = Website.Text;
			newOrganization.Address = GetAddress();
			user.Phone = newOrganization.Phone = GetPhone(Phone);
			user.Fax = newOrganization.Fax = GetPhone(Fax);
			user.OtherPhone = newOrganization.OtherPhone = GetPhone(OtherPhone);

			newOrganization.LastUpdate = new ActivityStamp(user);

            try
            {
                
			    // XXX: This is sort of lame.
			    if (newOrganization is Domain.Provider)
			    {
				    ProviderService.SaveNewWithAdminUser((Domain.Provider)newOrganization, user);
			    }
			    else if (newOrganization is Domain.Intermediary)
			    {
				    IntermediaryService.SaveNewWithAdminUser((Domain.Intermediary)newOrganization, user);
			    }
            } 
            catch (SmtpException)
            {
                emailSuccess.Visible = false;
                emailFail.Visible = true;
            }

			CreateUserWizard.ActiveViewIndex = 1; // CompletionView
		}
    }
}