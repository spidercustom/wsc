﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ChangeName.aspx.cs" Inherits="ScholarBridge.Web.Provider.Users.ChangeName" Title="Provider | Users | Change Name" %>
<%@ Register TagPrefix="sb" TagName="EditUserName" Src="~/Common/EditUserName.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div class="subsection">
<br /><br /><br />
<h3>Edit User</h3>
    <sb:EditUserName ID="editUserName" runat="server" 
        OnUserSaved="editUserName_OnUserSaved" 
        OnFormCanceled="editUserName_OnFormCanceled" />
</div>        
</asp:Content>
