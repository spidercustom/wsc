﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Message.Default" Title="Message" %>

<%@ Register TagPrefix="sb" TagName="MessageList" Src="~/Common/MessageList.ascx" %>
<%@ Register TagPrefix="sb" TagName="SentMessageList" Src="~/Common/SentMessageList.ascx" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
        <asp:LoginView ID="loginView2" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Intermediary, Intermediary Admin,Provider, Provider Admin,Admin, WSCAdmin">
                        <ContentTemplate>
                            <div><asp:Image ID="image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
                            <div><asp:Image ID="image2" ImageUrl="~/images/PicBottomOurMessages.gif" width="918px" height="170px" runat="server" /></div>                 
                        </ContentTemplate>
                    </asp:RoleGroup>
                     <asp:RoleGroup Roles="Seeker">
                        <ContentTemplate>
                            <div><asp:Image ID="imageSeeker1" ImageUrl="~/images/PictopMyMessages.gif" Width="918px" Height="15px" runat="server" /></div>
                            <div><asp:Image ID="imageSeeker2" ImageUrl="~/images/PicBottomMyMessages.gif" width="918px" height="170px" runat="server" /></div>                 
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
        </asp:LoginView>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
 
  <asp:LoginView ID="loginView3" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Intermediary, Intermediary Admin,Provider, Provider Admin">
                        <ContentTemplate>
                        <P class="PageTitle"><Img src="<%= ResolveUrl("~/images/PgTitle_OurMsgs.gif") %>" ></p>
                        </ContentTemplate>
                    </asp:RoleGroup>
                     <asp:RoleGroup Roles="Seeker">
                        <ContentTemplate>
                            <!--Left floated content area starts here-->
                            <div id="HomeContentLeft">

                              <Img src="<%= ResolveUrl("~/images/PgTitle_MyMessages.gif") %>" width="157px" height="54px">
                              <img src="<%= ResolveUrl("~/images/EmphasisedMyMessagesPage.gif") %>" width="513px" height="96px">
            
                             </div>
                              <!--Left floated content area ends here-->

                             <!--Right Floated content area starts here-->
                             <div id="HomeContentRight">
                             <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                             </div>
                            <BR>

                             <div id="Clear"></div>                        
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
        </asp:LoginView>
 
 
 
 <P class="HighlightedTextMain"><asp:Label ID="lblPageText" runat="server" /></P>
        <div class="tabs" id="messageTabs">
            <ul>
                <li><a href="#tab"><span>Inbox</span></a></li>
                <asp:PlaceHolder ID="deniedTabHead" runat="server">                
                    <li><a href="#tab"><span>Denied</span></a></li>
                </asp:PlaceHolder>
                <li><a href="#tab"><span>Archived</span></a></li>
                <li><a href="#tab"><span>Sent</span></a></li>
                <asp:PlaceHolder ID="approvedTabHead" runat="server">
                    <li><a href="#tab"><span>Scholarships Approved</span></a></li>
                </asp:PlaceHolder>
            </ul>
            <div id="tab">
                <asp:MultiView ID="messageTabPages" runat="server" ActiveViewIndex="0" >
                    <asp:View ID="InboxTabPage" runat="server">
                       <h3>Inbox</h3>
                       <sb:MessageList id="messageList" runat="server" LinkTo="~/Message/Show.aspx" />
                    </asp:View>
                    <asp:View ID="DeniedTabPage" runat="server">
                        <h3>Denied</h3>
                        <sb:MessageList id="denyMessages" runat="server" MessageAction="Deny" LinkTo="~/Message/Show.aspx" />
                    </asp:View>
                    <asp:View ID="ArchivedTabPage" runat="server">
                        <h3>Archived</h3>
                        <sb:MessageList id="archivedMessageList" runat="server" Archived="true" LinkTo="~/Message/Show.aspx" />
                    </asp:View>
                    <asp:View ID="SentTabPage" runat="server">
                        <h3>Sent</h3>
                        <sb:SentMessageList id="sentMessageList" runat="server" LinkTo="~/Message/Show.aspx" />
                    </asp:View>
                    <asp:View ID="ApprovedScholarshipsTabPage" runat="server">
                        <h3>Approved Scholarships</h3>
                        <sb:MessageList id="scholarshipApprovedMessages" runat="server" ScholarshipApprovals="true" LinkTo="~/Message/Show.aspx" />
                    </asp:View>
                </asp:MultiView>
                <sbCommon:jQueryTabIntegrator ID="messagesTabIntegrator" runat="server"
                    MultiViewControlID="messageTabPages" TabControlClientID="messageTabs"
                    CausesValidation="true" />
            </div>
        </div>        
</asp:Content>
