﻿using System;

namespace ScholarBridge.Web.Intermediary
{
    public partial class Anonymous : System.Web.UI.Page
    {
        public IUserContext UserContext { get; set; }
        private LinkGenerator linkGenerator;
		protected LinkGenerator LinkGenerator
		{
			get
			{
				if (linkGenerator == null)
					linkGenerator = new LinkGenerator();
				return linkGenerator;
			}
		}
		protected void Page_Load(object sender, EventArgs e)
        {
            if (UserContext.CurrentUser != null)
            {
                Response.Redirect("~/Intermediary");
            }
			loginForm.LinkTo = LinkGenerator.GetFullLink("/Intermediary/RegisterIntermediary.aspx");
        }
    }
}
