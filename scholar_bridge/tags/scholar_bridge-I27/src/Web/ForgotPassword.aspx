﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true"
    CodeBehind="ForgotPassword.aspx.cs" Inherits="ScholarBridge.Web.ForgotPassword"
    Title="Forgot Password" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #EntirePageWrapper
        {
            width: 500px;
            margin: auto;
            padding: 10px;
        }
        #ContentWrapper01
        {
            width: 490px;
        }
        #ContentWrapper02
        {
            width: 480px;
        }
        body
        {
            width: 500px;
        }
    </style>
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <asp:PasswordRecovery ID="passwordRecovery" runat="server" UserNameInstructionText="<br /><br />Enter your email address and click submit to reset your password. You will receive an email with your new password. If you forgot which email address you used, enter an email and click the submit button. The system will tell you if that email is not registered and you can try another email address.<br /><br />"
        UserNameLabelText="Email Address:" GeneralFailureText="An error has occured. Please check and try again."
        UserNameRequiredErrorMessage="Email is required." LabelStyle-HorizontalAlign="Right"
        UserNameFailureText="The entered email address is not in our system. Please check and try again."
        OnSendingMail="passwordRecovery_SendingMail">
        <LabelStyle HorizontalAlign="Right"></LabelStyle>
        <UserNameTemplate>
            <div class="form-iceland">
                <div class="subsection">
                <p class="form-section-title">Forgot Your Password?</p>
                <p style="width:450px;" >
                    Enter your email address and click submit to reset your password. You will receive
                    an email with your new password. If you forgot which email address you used, enter
                    an email and click the submit button. The system will tell you if that email is
                    not registered and you can try another email address.
                </p>
                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Email Address:</asp:Label>
                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="Email is required." ToolTip="Email is required." ValidationGroup="passwordRecovery">*</asp:RequiredFieldValidator>
                <br />
                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                <br />
                <label>&nbsp;</label>
                </div>
            </div>
            <br />
           <sbCommon:AnchorButton ID="SubmitButton" runat="server" CommandName="Submit" Text="Submit" ValidationGroup="passwordRecovery" />
 
        </UserNameTemplate>
        <SuccessTemplate>
            <h3>Your password has been sent to you.</h3>
            <p>
                Password has been generated and sent to your email address.
                This window can be closed now by clicking on "Close" button.
            </p>
            <sbCommon:AnchorButton ID="closeButton" runat="server" Text="Close" OnClientClick="javascript:window.close();" />
        </SuccessTemplate>
        <InstructionTextStyle BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left" />
    </asp:PasswordRecovery>
</asp:Content>
