﻿namespace ScholarBridge.Domain.Lookup
{
    public interface ILookup : IDeprecatable
    {
        object Id { get; set; }
        int GetIdAsInteger();
        string GetIdAsString();
        string Name { get; set; }
        string Description { get; set; }
        ActivityStamp LastUpdate { get; set; }
    }
}
