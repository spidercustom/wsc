using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(179)]
    public class AddApplicationAcademicAreaRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationAcademicAreaRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBAcademicAreaLUT"; }
        }
    }
}