﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(98)]
    public class AddIntermediaryRelationToScholarship : Migration
    {
        public const string TABLE_NAME = "SBScholarship";
        public const string COLUMN = "SBIntermediaryID";

        public readonly string FK = String.Format("FK_{0}_{1}", TABLE_NAME, COLUMN);

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column(COLUMN, DbType.Int32, ColumnProperty.Null));
            Database.AddForeignKey(FK, TABLE_NAME, COLUMN, "SBOrganization", "SBOrganizationId");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK);
            Database.RemoveColumn(TABLE_NAME, COLUMN);
        }
    }
}
