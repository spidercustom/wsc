﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="ScholarBridge.Web.ContactUs" Title="Contact Us" %>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />

<h2>Contact Us</h2>

<asp:panel ID="contactUsSelectionPanel" runat="server">
“The Washington Scholarship Coalition is committed to making theWashBoard.org your resource for scholarships.  We want to hear from you!”
<br /><br>
Support Hours: 8:00 am – 5:00 pm PST Monday thru Friday<br />
Phone: 888-535-0747, option #8

<p>
<ul>
    <li>
        <asp:linkbutton ID="supportMessageLink" runat="server" ToolTip="Contact our Support unit for help using theWashBoard"
            onclick="supportMessageLink_Click">Support</asp:linkbutton>
    </li>
</ul>
<ul>
    <li>
        <asp:linkbutton ID="problemMessageLink" runat="server" ToolTip="Click here to alert the theWashBoard.org administrator."
            onclick="problemMessageLink_Click">Report a Problem</asp:linkbutton>
    </li>
</ul>
<ul>
    <li>
        <asp:linkbutton ID="suggestionMessageLink" runat="server" tooltip="Send your ideas to theWashBoard development team."
            onclick="suggestionMessageLink_Click">Suggestions</asp:linkbutton>
    </li>
</ul>
</p>

</asp:panel>

<asp:panel runat="server" ID="contactUsMessagePanel">
    <uc1:ContactUs ID="contactUsControl" 
                    OnFormCanceled="contactUsControl_Canceled" 
                    OnFormSent="contactUsControl_Sent" 
                    runat="server" />
</asp:panel>


</asp:Content>
