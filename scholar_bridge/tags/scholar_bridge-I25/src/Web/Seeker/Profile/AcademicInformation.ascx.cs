﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class AcademicInformation : WizardStepUserControlBase<Domain.Seeker>
    {
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Seeker seeker;
        Domain.Seeker SeekerInContext
        {
            get
            {
                if (seeker == null)
                    seeker = Container.GetDomainObject();
                if (seeker == null)
                    throw new InvalidOperationException("There is no seeker in context");
                return seeker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
		}

        private void PopulateScreen()
        {
            FirstGenerationControl.Checked = SeekerInContext.FirstGeneration;
            if (null != SeekerInContext.CurrentSchool)
            {
                CurrentStudentGroupControl.StudentGroup = SeekerInContext.CurrentSchool.LastAttended;
                CurrentStudentGroupControl.Years = SeekerInContext.CurrentSchool.YearsAttended;

                if (null != SeekerInContext.CurrentSchool.College)
                    CollegeControlDialogButton.Keys = SeekerInContext.CurrentSchool.College.Id.ToString();
                CollegeOtherControl.Text = SeekerInContext.CurrentSchool.CollegeOther;

                if (null != SeekerInContext.CurrentSchool.HighSchool)
                    HighSchoolControlDialogButton.Keys = SeekerInContext.CurrentSchool.HighSchool.Id.ToString();
                HighSchoolOtherControl.Text = SeekerInContext.CurrentSchool.HighSchoolOther;

                if (null != SeekerInContext.CurrentSchool.SchoolDistrict)
                    SchoolDistrictControlLookupDialogButton.Keys = SeekerInContext.CurrentSchool.SchoolDistrict.Id.ToString();
                SchoolDistrictOtherControl.Text = SeekerInContext.CurrentSchool.SchoolDistrictOther;
            }


            if (null != SeekerInContext.SeekerAcademics)
            {
                CollegesAppliedControlDialogButton.Keys = SeekerInContext.SeekerAcademics.CollegesApplied.CommaSeparatedIds();
                CollegesAppliedOtherControl.Text = SeekerInContext.SeekerAcademics.CollegesAppliedOther;

                CollegesAcceptedControlDialogButton.Keys = SeekerInContext.SeekerAcademics.CollegesAccepted.CommaSeparatedIds();
                CollegesAcceptedOtherControl.Text = SeekerInContext.SeekerAcademics.CollegesAcceptedOther;

                if (SeekerInContext.SeekerAcademics.GPA.HasValue)
                    GPAControl.Amount = (decimal) SeekerInContext.SeekerAcademics.GPA.Value;

                if (SeekerInContext.SeekerAcademics.ClassRank.HasValue)
                    ClassRankControl.Amount = SeekerInContext.SeekerAcademics.ClassRank.Value;

                SchoolTypeControl.SelectedValues = (int)SeekerInContext.SeekerAcademics.SchoolTypes;
                AcademicProgramControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.AcademicPrograms;
                SeekerStatusControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.SeekerStatuses;
                ProgramLengthControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.ProgramLengths;
                
                if (null != SeekerInContext.SeekerAcademics.SATScore)
                {
                    SATReadingControl.Amount = SeekerInContext.SeekerAcademics.SATScore.CriticalReading.Value;
                    SATWritingControl.Amount = SeekerInContext.SeekerAcademics.SATScore.Writing.Value;
                    SATMathControl.Amount = SeekerInContext.SeekerAcademics.SATScore.Mathematics.Value;
                }
                if (null != SeekerInContext.SeekerAcademics.ACTScore)
                {
                    ACTEnglishControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.English.Value;
                    ACTMathControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.Mathematics.Value;
                    ACTReadingControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.Reading.Value;
                    ACTScienceControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.Science.Value;
                }

                APCreditsControl.Amount = SeekerInContext.SeekerAcademics.APCredits ?? 0;
                IBCreditsControl.Amount = SeekerInContext.SeekerAcademics.IBCredits ?? 0;
                HonorsControl.Checked = SeekerInContext.SeekerAcademics.Honors;
            }
        }

	    #region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

        public override void Save()
        {
            PopulateObjects();
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            SeekerService.Update(SeekerInContext);
        }

        public override void PopulateObjects()
        {
            SeekerInContext.FirstGeneration = FirstGenerationControl.Checked;
          
            if (null == SeekerInContext.SeekerAcademics)
            {
                SeekerInContext.SeekerAcademics = new SeekerAcademics();
            }

            CollegesAppliedControlDialogButton.PopulateListFromSelection(SeekerInContext.SeekerAcademics.CollegesApplied);
            SeekerInContext.SeekerAcademics.CollegesAppliedOther = CollegesAppliedOtherControl.Text;

            CollegesAcceptedControlDialogButton.PopulateListFromSelection(SeekerInContext.SeekerAcademics.CollegesAccepted);
            SeekerInContext.SeekerAcademics.CollegesAcceptedOther = CollegesAcceptedOtherControl.Text;

            SeekerInContext.SeekerAcademics.Honors = HonorsControl.Checked;
            SeekerInContext.SeekerAcademics.GPA = (double) GPAControl.Amount;
            SeekerInContext.SeekerAcademics.APCredits = (int) APCreditsControl.Amount;
            SeekerInContext.SeekerAcademics.IBCredits = (int) IBCreditsControl.Amount;
            SeekerInContext.SeekerAcademics.ClassRank = (int) ClassRankControl.Amount;

            if (null == SeekerInContext.SeekerAcademics.SATScore)
            {
                SeekerInContext.SeekerAcademics.SATScore = new SatScore();
            }

            SeekerInContext.SeekerAcademics.SATScore.CriticalReading = (int) SATReadingControl.Amount;
            SeekerInContext.SeekerAcademics.SATScore.Writing = (int) SATWritingControl.Amount;
            SeekerInContext.SeekerAcademics.SATScore.Mathematics = (int) SATMathControl.Amount;

            if (null == SeekerInContext.SeekerAcademics.ACTScore)
            {
                SeekerInContext.SeekerAcademics.ACTScore = new ActScore();
            }

            SeekerInContext.SeekerAcademics.ACTScore.English = (int) ACTEnglishControl.Amount;
            SeekerInContext.SeekerAcademics.ACTScore.Mathematics = (int) ACTMathControl.Amount;
            SeekerInContext.SeekerAcademics.ACTScore.Reading = (int) ACTReadingControl.Amount;
            SeekerInContext.SeekerAcademics.ACTScore.Science = (int) ACTScienceControl.Amount;

            if (null == SeekerInContext.CurrentSchool)
            {
                SeekerInContext.CurrentSchool = new CurrentSchool();
            }
            SeekerInContext.CurrentSchool.LastAttended = CurrentStudentGroupControl.StudentGroup;
            SeekerInContext.CurrentSchool.YearsAttended = CurrentStudentGroupControl.Years;

            SeekerInContext.CurrentSchool.College = (College)CollegeControlDialogButton.GetSelectedLookupItem();
            SeekerInContext.CurrentSchool.CollegeOther = CollegeOtherControl.Text;

            SeekerInContext.CurrentSchool.HighSchool = (HighSchool)HighSchoolControlDialogButton.GetSelectedLookupItem();
            SeekerInContext.CurrentSchool.HighSchoolOther = HighSchoolOtherControl.Text;

            SeekerInContext.CurrentSchool.SchoolDistrict = (SchoolDistrict)SchoolDistrictControlLookupDialogButton.GetSelectedLookupItem();
            SeekerInContext.CurrentSchool.SchoolDistrictOther = SchoolDistrictOtherControl.Text;

            SeekerInContext.SeekerAcademics.SchoolTypes = (SchoolTypes) SchoolTypeControl.SelectedValues;
            SeekerInContext.SeekerAcademics.AcademicPrograms = (AcademicPrograms) AcademicProgramControl.SelectedValue;
            SeekerInContext.SeekerAcademics.SeekerStatuses = (SeekerStatuses) SeekerStatusControl.SelectedValue;
            SeekerInContext.SeekerAcademics.ProgramLengths = (ProgramLengths) ProgramLengthControl.SelectedValue;

        }
		#endregion

    }
}