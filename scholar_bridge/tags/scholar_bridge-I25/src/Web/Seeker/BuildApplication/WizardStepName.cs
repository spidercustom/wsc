﻿namespace ScholarBridge.Web.Seeker.BuildApplication
{
    public enum WizardStepName
    {
        Basics,
        AboutMe,
        Academics,
        Activities,
        FinancialNeed,
        AdditionalCriteria
    }
}
