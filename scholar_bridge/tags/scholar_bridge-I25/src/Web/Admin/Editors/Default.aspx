﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Admin.Editors.Default" Title="Edit Pages" %>
<%-- <%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
--%>
<%@ Register src="~/Admin/Editors/Resources/ResourceList.ascx" tagname="ResourceList" tagprefix="sb" %>
<%@ Register src="~/Admin/Editors/PressRoom/ArticleList.ascx" tagname="ArticleList" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgScholarship.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div id="editorTabs" class="tabs" >
    <ul>
        <li><a href="#tab"><span>FAQ Editor</span></a></li>
        <li><a href="#tab"><span>Press Room Editor</span></a></li>
        <li><a href="#tab"><span>Resource Editor</span></a></li>
    </ul>
    
   <div id="tab">
        <asp:MultiView ID="AdminEditors" runat="server">
            <asp:View ID="FAQEditor" runat="server">
                <h2>FAQ Editor - Coming Soon!</h2>
            </asp:View>
            <asp:View ID="PressRoomEditor" runat="server">
                <sb:ArticleList ID="articleList" runat="server" />
            </asp:View>
            <asp:View ID="SeekerProfileStep" runat="server">
                <sb:ResourceList ID="resourceList1" runat="server" />
            </asp:View>

        </asp:MultiView>
        <sbCommon:jQueryTabIntegrator ID="adminEditorIntegrator" runat="server"
            MultiViewControlID="AdminEditors" TabControlClientID="editorTabs"
            CausesValidation="false" />
    </div>
</div>
</asp:Content>
