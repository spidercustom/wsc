﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticleList.ascx.cs" Inherits="ScholarBridge.Web.Admin.Editors.PressRoom.ArticleList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register src="~/Admin/Editors/PressRoom/ArticleEntry.ascx" tagname="ArticleEntry" tagprefix="sb" %>

<h3>Press Room Editor</h3>
<asp:Panel ID="articleListPanel" runat="server">

    <asp:ListView ID="lstArticles" runat="server" 
        OnItemDataBound="lstArticles_ItemDataBound" 
        onpagepropertieschanging="lstArticles_PagePropertiesChanging" >
        <LayoutTemplate>
         <br />
             <sbCommon:AnchorButton ID="createArticleLnkTop" runat="server" CausesValidation="false" OnClick="createArticleLnk_Click" Text="Add Article" /> 
 
        <br />
        <table class="sortableTable">
                <thead>
                <tr>
                    <th>Select</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Date</th>
                    <th>Title</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        
        </LayoutTemplate>
              
        <ItemTemplate>
        <tr class="row">
            <td><asp:CheckBox ID="chkArticle" runat="server" /></td>
            <td><sbCommon:AnchorButton ID="SingleEditBtn" Text="Edit" runat="server" CausesValidation="false" onclick="SingleEditBtn_Click" /></td>
            <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
                Text="Delete"  runat="server" OnClickConfirm="SingleDeleteBtn_Click"   Width="60px" /></td>
            <td><%# DataBinder.Eval(Container.DataItem, "PostedDate", "{0:d}")%></td>
            <td><asp:HyperLink id="linktoArticle" Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
        </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:CheckBox ID="chkArticle" runat="server" /></td>
            <td><sbCommon:AnchorButton ID="SingleEditBtn" Text="Edit" runat="server" CausesValidation="false" onclick="SingleEditBtn_Click" /></td>
            <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
                Text="Delete"  runat="server" OnClickConfirm="SingleDeleteBtn_Click" Width="60px"  /></td>
            <td><%# DataBinder.Eval(Container.DataItem, "PostedDate", "{0:d}")%></td>
            <td><asp:HyperLink id="linktoArticle"  Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
        </tr>
        </AlternatingItemTemplate>
        <EmptyDataTemplate>
            <p>There are currently no Press Room Articles. </p>
        
            <sbCommon:AnchorButton ID="createArticleLnk" CausesValidation="false" runat="server" OnClick="createArticleLnk_Click" Text="Add Article" /> 
 
        </EmptyDataTemplate>
    </asp:ListView>

    <div class="pager">
        <asp:DataPager runat="server" ID="pager" PagedControlID="lstArticles" 
            PageSize="50" onprerender="pager_PreRender"  >
            <Fields>
                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                    ShowNextPageButton="false" ShowLastPageButton="false" />
                <sbCommon:CustomNumericPagerField
                    CurrentPageLabelCssClass="pagerlabel"
                    NextPreviousButtonCssClass="pagerlink"
                    PagingPageLabelCssClass="pagingPageLabel"
                    NumericButtonCssClass="pagerlink"/>
                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                    ShowNextPageButton="true" ShowLastPageButton="false" />
            </Fields>
        </asp:DataPager>
        <br />
    </div> 
    <div>
        <sbCommon:ConfirmButton ID="DeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
            Text="Delete Selected"  runat="server" OnClickConfirm="DeleteBtn_Click" Width="130px" />
    </div>
    <div id="deleteConfirmDiv" title="Confirm Delete" style="display:none">
            Are you sure want to delete?
    </div> 
 
</asp:Panel>

<asp:Panel ID="articleEntryPanel" runat="server" Visible="false">
    <sb:articleentry ID="articleEntry" 
                    runat="server" 
                    OnFormCanceled="articleEntry_FormCanceled" 
                    OnFormSaved="articleEntry_FormSaved"/>
</asp:Panel>
