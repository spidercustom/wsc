﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="ScholarBridge.Web.Help" Title="Help" %>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />

<h2>Help</h2>



<p><b>“We are here to help you”.</b></p>

<p>Support Hours: 8:00 am – 5:00 pm PST Monday thru Friday</p>

<p>Phone: 888-535-0747, option #8</p>

<p><A href='<%= LinkGenerator.GetFullLink("/ContactUs.aspx") %>'>Contact Us Via the Web</A>  
</p>

<h5>Frequently Asked Questions</h5>
<ul>
    <li>
        <a href="#generalFAQ">General FAQ</a>
    </li>
    <li>
        <a href="#seekerFAQ">Seeker FAQ</a>
    </li>
    <li>
        <a href="#providerFAQ">Provider FAQ</a>
    </li>
</ul>

<br />
<a name="generalFAQ"><h5>General FAQ</h5></a>
<br />
<a name="seekerFAQ"><h5>Seeker FAQ</h5></a>
<br />
<a name="providerFAQ"><h5>Provider FAQ</h5></a>

</asp:Content>
