﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;


namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class MatchCriteriaSelection : WizardStepUserControlBase<Scholarship>
    {
        public MatchCriteriaSelection()
        {
            SetupDefaultUpdaters();    
        }

        public IScholarshipService ScholarshipService { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        

        #region default value update
        private readonly Dictionary<SeekerProfileAttribute, Action> defaultUpdaters = new Dictionary<SeekerProfileAttribute, Action>();

        private void SetupDefaultUpdaters()
        {
            defaultUpdaters.Add(SeekerProfileAttribute.StudentGroup, () => 
                ScholarshipInContext.SeekerProfileCriteria.StudentGroups =
                (StudentGroups)EnumExtensions.SelectAll(typeof(StudentGroups)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.SchoolType, () => 
                ScholarshipInContext.SeekerProfileCriteria.SchoolTypes = 
                (SchoolTypes)EnumExtensions.SelectAll(typeof(SchoolTypes)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.AcademicProgram, () => 
                ScholarshipInContext.SeekerProfileCriteria.AcademicPrograms = 
                (AcademicPrograms)EnumExtensions.SelectAll(typeof(AcademicPrograms)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.SeekerStatus, () => 
                ScholarshipInContext.SeekerProfileCriteria.SeekerStatuses = 
                (SeekerStatuses) EnumExtensions.SelectAll(typeof(SeekerStatuses)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.ProgramLength, () => 
                ScholarshipInContext.SeekerProfileCriteria.ProgramLengths = 
                (ProgramLengths)EnumExtensions.SelectAll(typeof(ProgramLengths)));
        }

        private void PopulateDefaults(SeekerProfileAttribute attribute)
        {
            if (defaultUpdaters.ContainsKey(attribute))
            {
                defaultUpdaters[attribute]();
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;

            StudentGroupControl.SelectedValues = (int)seekerProfileCriteria.StudentGroups;
            CollegesRadioButtons.SelectedValue = (int) seekerProfileCriteria.CollegeType;
            
            CollegesControlDialogButton.Keys = seekerProfileCriteria.Colleges.CommaSeparatedIds();
            SchoolTypeControl.SelectedValues = (int)seekerProfileCriteria.SchoolTypes;
            AcademicProgramControl.SelectedValues = (int)seekerProfileCriteria.AcademicPrograms;
            SeekerStatusControl.SelectedValues = (int)seekerProfileCriteria.SeekerStatuses;
            ProgramLengthControl.SelectedValues = (int)seekerProfileCriteria.ProgramLengths;
            PopulateListControl(TypesOfSupport, SupportDAL.FindAll());
            if (null != ScholarshipInContext.FundingProfile.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(ScholarshipInContext.FundingProfile.SupportedSituation.TypesOfSupport,
                                                 ts => ts.Id.ToString());
            }
        }
        public void PopulateListControl(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override void PopulateObjects()
        {
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            seekerProfileCriteria.StudentGroups = (StudentGroups)StudentGroupControl.SelectedValues;
            seekerProfileCriteria.CollegeType =(CollegeType ) CollegesRadioButtons.SelectedValue;
            if (seekerProfileCriteria.CollegeType != CollegeType.Specify)
                CollegesControlDialogButton.Keys = "";
           
            PopulateList(CollegesContainerControl, CollegesControlDialogButton, seekerProfileCriteria.Colleges);

            seekerProfileCriteria.SchoolTypes = (SchoolTypes)SchoolTypeControl.SelectedValues;
            seekerProfileCriteria.AcademicPrograms =
                (AcademicPrograms)AcademicProgramControl.SelectedValues;
            seekerProfileCriteria.SeekerStatuses = (SeekerStatuses)SeekerStatusControl.SelectedValues;
            seekerProfileCriteria.ProgramLengths =
                (ProgramLengths)ProgramLengthControl.SelectedValues;

            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            ScholarshipInContext.FundingProfile.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStages.NotActivated;
        }
        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }
         
         

        
    }
}