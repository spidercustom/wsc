﻿using System;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web
{
    public partial class ExternalMasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PopupViewHeadContainer.Visible = Page.IsInPopupView();
            PopupViewLogoPlaceHolder.Visible = Page.IsInPopupView();
        }
    }
}
