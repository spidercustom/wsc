﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerProfileActivationValidation.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerProfileActivationValidation" %>

<div runat="server" id="ActivationValidationErrors" class="issueList" title="Profile activation" style="display:none">
    <asp:Panel ID="BasicTabPageErrorPanel" runat="server">
        <h3>Basic</h3>
        <ul>
            <li id="FirstNameValidationError" runat="server">First Name is Required</li>
            <li id="LastNameValidationError" runat="server">Last Name is Required</li>
            <li id="StreetLine1ValidationError" runat="server">Street must be specified and must be less than 50 characters</li>
            <li id="CityValidationError" runat="server">City must be specified</li>
            <li id="StateValidationError" runat="server">State must be specified</li>
            <li id="PostalCodeValidationError" runat="server">Postal Code must be at-least 5 characters and must be less than 10 characters</li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="MyAcademicInfoTabPageErrorPanel" runat="server">
        <h3>My Academic Info</h3>
        <ul>
            <li id="StudentTypeValidationError" runat="server">What type of student are you currently?</li>
            <li id="SchoolTypeValidationError" runat="server">What type of school are you considering?</li>
            <li id="AcademicProgramValidationError" runat="server">Academic Program is required</li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="MyFinancialNeedTabPageErrorPanel" runat="server">
        <h3>My Financial Need</h3>
        <ul>
            <li id="SupportTypeValidationError" runat="server">Specify at-least one support type</li>
        </ul>
    </asp:Panel>
    <br />
    <p>
        All issues specified in above list must to be fixed for profile that is activating or is active.
    </p>
</div>