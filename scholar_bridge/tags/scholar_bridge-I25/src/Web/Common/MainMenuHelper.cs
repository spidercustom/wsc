using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public static class MainMenuHelper
    {
        private const string ACTIVE_MENU_KEY = "ACTIVE_MENU_KEY";
        private const char SEPARATOR = ';';


        static MainMenuHelper()
        {
            BuildMap();
        }

        public static void ApplyActiveMenuImage(MainMenu mainMenu)
        {
            var activeMenuKeys = GetActiveMenuKeys(mainMenu.Page);

            if (null != activeMenuKeys && activeMenuKeys.Length > 0)
            {
                ApplyActiveMenuImage(mainMenu, activeMenuKeys);
            }
        }

        public static void ApplyActiveMenuImage(MainMenu mainMenu, string[] activeMenuKeys)
        {
            Array.ForEach(activeMenuKeys, activeMenuKey =>
            {
               var menuImage = mainMenu.FindControlRecursive(activeMenuKey) as Image;
               if (null == menuImage)
                   return;
               var map = MenuImageMapList.Find(match => match.Key == activeMenuKey);
               if (null == map)
                   return;

               menuImage.ImageUrl = map.ActiveImageUrl;
            });
        }

        public static void SetupActiveMenuKey(Page page, params MainMenuKey[] activeMenuKeys)
        {
            if (activeMenuKeys == null || activeMenuKeys.Length == 0)
            {
                SetupActiveMenuKeys(page, null);
                return;
            }
            var activeMenuKeyStrings = new List<string>();
            Array.ForEach(activeMenuKeys, activeMenuKey =>
              {
                  var keyStrings = MenuKeyMapping[activeMenuKey];
                  if (null != keyStrings)
                      activeMenuKeyStrings.AddRange(keyStrings);
              });
            if (activeMenuKeyStrings.Count == 0)
                SetupActiveMenuKeys(page, null);
            else
                SetupActiveMenuKeys(page, activeMenuKeyStrings.ToArray());
        }

        public static void SetupActiveMenuKeys(Page page, params string[] activeMenuKeys)
        {
            page.Response.Cookies.Remove(ACTIVE_MENU_KEY);
            if (activeMenuKeys == null)
                return;
            var activeMenuCookie = new HttpCookie(ACTIVE_MENU_KEY, string.Join(SEPARATOR.ToString(), activeMenuKeys));
            activeMenuCookie.Expires = DateTime.Now.AddMinutes(page.Session.Timeout);
            page.Response.Cookies.Add(activeMenuCookie);
        }

        private static string[] GetActiveMenuKeys(Page page)
        {
            //if the cookie has been added in this request's response should check that first
            var activeMenuCookie = page.Response.Cookies[ACTIVE_MENU_KEY];
            if (null != activeMenuCookie && !String.IsNullOrEmpty(activeMenuCookie.Value))
                return activeMenuCookie.Value.Split(SEPARATOR);
            //if cookie was set in prior request response, it should be in request
            activeMenuCookie = page.Request.Cookies[ACTIVE_MENU_KEY];
            if (null != activeMenuCookie && !String.IsNullOrEmpty(activeMenuCookie.Value))
                return activeMenuCookie.Value.Split(SEPARATOR);

            //not setup, should result to null
            return null;
        }

        #region TODO: refactor this to xml file, better would be to use sitemap
        private static List<MainMenuImageMap> MenuImageMapList;
        private static Dictionary<MainMenuKey, string[]> MenuKeyMapping;
        private static void BuildMap()
        {
            MenuImageMapList = new List<MainMenuImageMap>();
            MenuImageMapList.Add(new MainMenuImageMap("adminPressRoomImage", "~/images/NavAdmin_EditPagesInactive.gif", "~/images/NavAdmin_EditPagesActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("adminPendingEmailConfirmationImage", "~/images/NavAdmin_PendingEmailInactive.gif", "~/images/NavAdmin_PendingEmailActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("adminMessagesImage", "~/images/NavAdmin_OurMsgsInactive.gif", "~/images/NavAdmin_OurMsgsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("adminSettingsImage", "~/images/NavAdmin_MySettingsInactive.gif", "~/images/NavAdmin_MySettingsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("providerScholarshipsImage", "~/images/NavProvider_OurScholarshipsInactive.gif", "~/images/NavProvider_OurScholarshipsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("providerAdminImage", "~/images/NavProvider_OurProfileInactive.gif", "~/images/NavProvider_OurProfileActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("providerSettingsImage", "~/images/NavProvider_MySettingsInactive.gif", "~/images/NavProvider_MySettingsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("providerMessagesImage", "~/images/NavProvider_OurMsgsInactive.gif", "~/images/NavProvider_OurMsgsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("intermediaryScholarshipsImage", "~/images/NavProvider_OurScholarshipsInactive.gif", "~/images/NavProvider_OurScholarshipsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("intermediaryAdminImage", "~/images/NavProvider_OurProfileInactive.gif", "~/images/NavProvider_OurProfileActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("intermediarySettingsImage", "~/images/NavProvider_MySettingsInactive.gif", "~/images/NavProvider_MySettingsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("intermediaryMessagesImage", "~/images/NavProvider_OurMsgsInactive.gif", "~/images/NavProvider_OurMsgsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("seekerProfileImage", "~/images/NavSeeker_MyProfileInactive.gif", "~/images/NavSeeker_MyProfileActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("seekerMatchesImage", "~/images/NavSeeker_MyMatchesInactive.gif", "~/images/NavSeeker_MyMatchesActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("seekerApplicationsImage", "~/images/NavSeeker_MyApplicationsInactive.gif", "~/images/NavSeeker_MyApplicationsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("seekerSettingsImage", "~/images/NavSeeker_MySettingsInactive.gif", "~/images/NavSeeker_MySettingsActive.gif"));
            MenuImageMapList.Add(new MainMenuImageMap("seekerMessagesImage", "~/images/NavSeeker_MyMegsInactive.gif", "~/images/NavSeeker_MyMegsActive.gif"));

            MenuKeyMapping = new Dictionary<MainMenuKey, string[]>();
            MenuKeyMapping.Add(MainMenuKey.None, null);
			MenuKeyMapping.Add(MainMenuKey.AdminPressRoom, new[] { "adminPressRoomImage" });
			MenuKeyMapping.Add(MainMenuKey.AdminResources, new[] { "adminResourcesImage" });
			MenuKeyMapping.Add(MainMenuKey.AdminPendingEmailConfirmtion, new[] { "adminPendingEmailConfirmationImage" });
            MenuKeyMapping.Add(MainMenuKey.ProviderScholarships, new[] { "providerScholarshipsImage" });
            MenuKeyMapping.Add(MainMenuKey.ProviderAdmin, new[] { "providerAdminImage" });
            MenuKeyMapping.Add(MainMenuKey.IntermediaryScholarships, new[] { "intermediaryScholarshipsImage" });
            MenuKeyMapping.Add(MainMenuKey.IntermediaryAdmin, new[] { "intermediaryAdminImage" });
            MenuKeyMapping.Add(MainMenuKey.SeekerProfile, new[] { "seekerProfileImage" });
            MenuKeyMapping.Add(MainMenuKey.SeekerMatches, new[] { "seekerMatchesImage" });
            MenuKeyMapping.Add(MainMenuKey.SeekerApplications, new[] { "seekerApplicationsImage" });
            MenuKeyMapping.Add(MainMenuKey.UserSettings, new[] { "adminSettingsImage", "providerSettingsImage", "intermediarySettingsImage", "seekerSettingsImage" });
            MenuKeyMapping.Add(MainMenuKey.Messages, new[] { "adminMessagesImage", "providerMessagesImage", "intermediaryMessagesImage", "seekerMessagesImage" });

        }
        

        private class MainMenuImageMap
        {
            public MainMenuImageMap(string key, string inactiveImageUrl, string activeImageUrl)
            {
                Key = key;
                InactiveImageUrl = inactiveImageUrl;
                ActiveImageUrl = activeImageUrl;
            }

            public string Key { get; set; }
            private string InactiveImageUrl { get; set; }
            public string ActiveImageUrl { get; set; }
        }

        public enum MainMenuKey
        {
            None,
			AdminPressRoom,
			AdminResources,
			AdminPendingEmailConfirmtion,
            ProviderScholarships,
            ProviderAdmin,
            IntermediaryScholarships,
            IntermediaryAdmin,
            SeekerProfile,
            SeekerMatches,
            SeekerApplications,
            UserSettings,
            Messages
        }
        #endregion
    }
}