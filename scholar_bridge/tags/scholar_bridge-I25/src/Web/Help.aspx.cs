﻿using System;


namespace ScholarBridge.Web
{
	public partial class Help : System.Web.UI.Page
	{
		private LinkGenerator linkGenerator;
		protected LinkGenerator LinkGenerator
		{
			get
			{
				if (linkGenerator == null)
					linkGenerator = new LinkGenerator();
				return linkGenerator;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
			}
		}
	}
}
