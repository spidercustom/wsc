﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="ScholarBridge.Web.AboutUs" Title="About Us" %>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />

<h2>About Us</h2>
    <p >
        <b>Washington Scholarship Coalition</b>: A 
        partnership of public and nonprofit agencies coming together to build an online 
        scholarship marketplace and provide a trusted source of scholarships. The 
        Washington Scholarship Coalition exists to create connections and ensure 
        scholarship funds are reaching those in need.</p>
    <p >
        <b> 
        Scholarship Coalition Partners</b>:
    </p>
    <ul style="margin-top:0in" type="disc">
        <li  >
            <a href="http://www.hecb.wa.gov/"><asp:Image ID="Image3" runat="server" ImageUrl="~/images/PartnerLogos/HECB+LgLogo.jpg"/><br />Washington Higher Education Coordinating Board (HECB)</a>
            </li>
        <li  >
            <a href="http://www.collegespark.org/"><asp:Image ID="Image4" runat="server" ImageUrl="~/images/PartnerLogos/CS_logo_Tag_color.png"/><br />College Spark Washington<br />&nbsp;</a>
            </li>
        <li  >
            <a href="http://www.nela.net/"><img alt="Nela" src="images/PartnerLogos/nela_logo.jpg" 
                style="width: 166px; height: 60px" /><br />Northwest Education Loan Association (NELA)<br />&nbsp;</a>
            <span style="mso-spacerun:yes">&nbsp;</span></li>
        <li  >
            <a href="http://www.seattlefoundation.org/"><asp:Image ID="Image5" runat="server" ImageUrl="~/images/PartnerLogos/TSF_1line_blue_tag.jpg"/><br />The Seattle Foundation<br />&nbsp;</a>
            <span style="mso-spacerun:yes">&nbsp;</span></li>
        <li  >
            <a href="http://www.collegesuccessfoundation.org/">The College Success Foundation<br />&nbsp;</a> </li>
        <li  >
            <a href="http://www.icwashington.org/">Independent Colleges of Washington<br />&nbsp;</a>
        </li>
        <li  >
            <a href="http://www.collegeplan.org/">College Planning Network<br />&nbsp;</a> </li>
    </ul>
    <p >
        <b>Our Mission</b></st1:place>:
        <i style="mso-bidi-font-style:normal">“Making college possible, one person at 
        a time.”<o:p></o:p></i></p>
    <p >
        <b>Our Goals</b>:</p>
    <ul style="margin-top:0in" type="disc">
        <li  >Increase 
            access to scholarships for Washington
            students, with emphasis around low-income and underserved students</li>
        <li  >Increase 
            awareness of financial aid &amp; scholarship opportunities</li>
        <li  >Increase 
            private scholarship funding by providing donors with information on effective, 
            efficient ways to contribute</li>
        <li  >Improve 
            efficiencies in private scholarship application process</li>
        <li  >Improve 
            availability of data on private scholarships for scholarship-seeking students</li>
    </ul>
    <p >
        <b>Contact the Washington
        Scholarship Coalition c/o</b>:
    </p>
    <p >
        Northwest Education Loan Association<br />
        Danette Knudson, Director, External Relations<br />
        (206) 461-5491<br />
        danette.knudson@nela.net</p>


</asp:Content>
