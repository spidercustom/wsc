﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using Spring.Context.Support;

namespace ScholarBridge.Web.MatchList
{
    public class MatchListIconHelper
    {
        private const string DEFAULT_ICON_ID = "IconButton";
        private const string SAVE_MATCH_CMD_NAME = "save-match";
        private const string UNSAVE_MATCH_CMD_NAME = "unsave-match";

        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }

        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        public Action PostIconClick { get; set;}

        public void SetupListView(ListView list)
        {
            list.ItemDataBound += List_ItemDataBound;
            list.ItemCommand += List_ItemCommand;
        }

        private void List_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match) ((ListViewDataItem) e.Item).DataItem);
                var iconButton = (ImageButton)e.Item.FindControl(DEFAULT_ICON_ID);

                ConfigureIcon(match, iconButton);
            }

        }

        private static void ConfigureIcon(Match match, ImageButton icon)
        {
            icon.CommandArgument = match.Scholarship.Id.ToString(); 
            switch (match.MatchStatus)
            {
                case MatchStatus.New:
                    icon.ImageUrl = "~/images/matched_scholarship.png";
                    icon.ToolTip = "Click to add into My Scholarships of Interest";
                    icon.CommandName = SAVE_MATCH_CMD_NAME;
                    break;
                case MatchStatus.Saved:
                    icon.ImageUrl = "~/images/saved_scholarship.png";
                    icon.ToolTip = "Click to remove from My Scholarships of Interest";
                    icon.CommandName = UNSAVE_MATCH_CMD_NAME;
                    break;
                case MatchStatus.Applied:
                    icon.ImageUrl = "~/images/applied-scholarship.png";
                    icon.ToolTip = "Application is created for the scholarship. Scholarship cannot be removed from My Scholarship";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void List_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int scholarshipId = -1;

            if (e.CommandArgument != null && e.CommandArgument.ToString().Length > 0)
                scholarshipId = Convert.ToInt32(e.CommandArgument);
            if (scholarshipId < 0)
                return;

            if (SAVE_MATCH_CMD_NAME.Equals(e.CommandName))
            {
                DoSaveMatch(scholarshipId);
            }
            if (UNSAVE_MATCH_CMD_NAME.Equals(e.CommandName))
            {
                DoUnsaveMatch(scholarshipId);
            }
        }


        private void DoSaveMatch(int scholarshipId)
        {
            MatchService.SaveMatch(UserContext.CurrentSeeker, scholarshipId);
            DoPostIconClick();
        }

        private void DoUnsaveMatch(int scholarshipId)
        {
            MatchService.UnSaveMatch(UserContext.CurrentSeeker, scholarshipId);
            DoPostIconClick();
        }

        private void DoPostIconClick()
        {
            if (null != PostIconClick)
                PostIconClick();
        }
    }
}