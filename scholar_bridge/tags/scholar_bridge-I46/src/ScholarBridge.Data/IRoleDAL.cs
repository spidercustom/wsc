﻿using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data
{
    public interface IRoleDAL : IDAL<Role>
    {
        Role FindByName(string name);
    }
}