using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IApplicationDAL : IDAL<Application>
    {
        Application FindById(int id);
        IList<Application> FindAllSubmitted(Scholarship scholarship);
        int CountAllSubmitted(Scholarship scholarship);
        IList<Application> FindAllSubmitted(int startIndex, int rowCount, string sortExpression, Scholarship scholarship);
        IList<Application> FindAll(Seeker seeker);
        int CountAll(Seeker seeker);
        IList<Application> FindAll(int startIndex, int rowCount, string sortExpression, Seeker seeker);
        IList<Application> SearchSubmittedBySeeker(int startIndex, int rowCount, string sortExpression, Scholarship scholarship, string lastName);
        int CountSubmittedBySeeker(Scholarship scholarship, string lastName);
        IList<Application> FindAllFinalists(Scholarship scholarship);

        Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship);
        int CountAllHavingAttachment(Attachment  attachment);
        int CountAllSubmittedBySeeker(Seeker seeker);
		int CountAllSubmitted();
		int CountAllStartedButNotSubmitted();
	}
}