using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class ApplicationDAL : AbstractDAL<Application>, IApplicationDAL
    {
        public IList<Application> FindAllSubmitted(int startIndex, int rowCount, string sortExpression, Scholarship scholarship)
        {
            var sql = "select {{cat.*}} from SBApplicationView  {{cat}} where ScholarshipId={0} And Stage='{1}'  order by ";
            sql = sql.Build(scholarship.Id, Enum.GetName(typeof (ApplicationStage), ApplicationStage.Submitted));

            var orderby = GetOrderByColumn(sortExpression);
            if (!String.IsNullOrEmpty(orderby))
            //need to fire sql query in order to get sorted results using view
            {

                return Session.CreateSQLQuery((sql + orderby), "cat", typeof(Application))
                    .SetFirstResult(startIndex).SetMaxResults(rowCount).List<Application>();
            }

            var query= CreateCriteria().SetFirstResult(startIndex).SetMaxResults(rowCount)
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted));

               query=BuildOrderByCriteria(query,sortExpression );

               return query.List<Application>();
        }

        public IList<Application> FindAllSubmitted( Scholarship scholarship)
        {
            return  CreateCriteria() 
                 .Add(Restrictions.Eq("Scholarship", scholarship))
                 .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
                  .List<Application>();
        }
        public int CountAllSubmitted(Scholarship scholarship)
        {
            return CreateCriteria()
                 .Add(Restrictions.Eq("Scholarship", scholarship))
                 .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
                  .SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
        }

        public IList<Application> FindAllFinalists(Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Finalist", true))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
                .List<Application>();
        }
       

		/// <summary>
		/// Count all Submitted applications site-wide for active scholarships
		/// </summary>
		/// <returns></returns>
		public int CountAllSubmitted()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

		/// <summary>
		/// Count all Submitted applications site-wide for active scholarships
		/// </summary>
		/// <returns></returns>
		public int CountAllStartedButNotSubmitted()
		{
			return CreateCriteria()
				.Add(Restrictions.Not(Restrictions.Eq("Stage", ApplicationStage.Submitted)))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

        public int CountAllSubmittedBySeeker(Seeker seeker)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker ))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
                .SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }

        public int CountAllHavingAttachment(Attachment attachment)
        {
            var sql = @"select count(*) as attachmentcount from SBApplicationAttachmentRT where SBApplicationAttachmentRT.SBAttachmentID in 
            (select SBAttachment.SBAttachmentId from SBAttachment where SBAttachment.UniqueName ='{0}') ".Build(attachment.UniqueName);
            
			ISQLQuery query = Session.CreateSQLQuery(sql).AddScalar("attachmentcount", NHibernateUtil.Int32);
    	    return  (int) query.UniqueResult();
    	 
        }
         

        public IList<Application> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .List<Application>();
        }
       public int CountAll(Seeker seeker)
        {
           return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }

       public IList<Application> FindAll(int startIndex, int rowCount, string sortExpression, Seeker seeker)
       {

           var query = CreateCriteria().SetFirstResult(startIndex).SetMaxResults(rowCount)
               .Add(Restrictions.Eq("Seeker", seeker));
           query = BuildOrderByCriteria(query, sortExpression);
           return query.List<Application>();
       }

         


        private string GetOrderByColumn(string sortExpression)
        {
            var orderby = "";
            switch (sortExpression)
            {
                case "EligibilityCriteria":
                case "EligibilityCriteria ASC":
                    orderby = "MatchMinimumCriteriaPercent ASC";
                    break;

                case "EligibilityCriteria DESC":
                    orderby = "MatchMinimumCriteriaPercent DESC";
                    break;

                case "PreferredCriteria":
                case "PreferredCriteria ASC":

                    orderby = "MatchPreferredCriteriaPercent ASC";
                    break;

                case "PreferredCriteria DESC":
                    orderby = "MatchPreferredCriteriaPercent DESC";
                    break;
                case "ApplicantName":
                case "ApplicantName ASC":
                    orderby = "ApplicantName ASC";

                    break;

                case "ApplicantName DESC":
                    orderby = "ApplicantName DESC";
                    break;
            }

            return orderby;
        }

        public IList<Application> SearchSubmittedBySeeker(int startIndex, int rowCount, string sortExpression, Scholarship scholarship, string lastName)
        {

            var sql = "select {{cat.*}} from SBApplicationView  {{cat}} where ScholarshipId={0} And Stage='{1}'  order by ";
            sql = sql.Build(scholarship.Id, Enum.GetName(typeof(ApplicationStage), ApplicationStage.Submitted));

            var orderby = GetOrderByColumn(sortExpression);
            

            if (!String.IsNullOrEmpty(orderby))
            //need to fire sql query in order to get sorted results using view
            {

                return Session.CreateSQLQuery((sql + orderby), "cat", typeof(Application))
                    .SetFirstResult(startIndex).SetMaxResults(rowCount).List<Application>();
            }


            var query = CreateCriteria().SetFirstResult(startIndex).SetMaxResults(rowCount)
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted));
            query = BuildOrderByCriteria(query , sortExpression);
            var crit1 = query.CreateCriteria("Seeker")
                .CreateCriteria("User")
                .Add(Restrictions.Like("Name.LastName", string.Format("%{0}%", lastName)));
                 

            


            return crit1.List<Application>();
        }

        public  int CountSubmittedBySeeker(Scholarship scholarship, string lastName)
        {
            var query = CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted));

            var crit1 = query.CreateCriteria("Seeker")
                .CreateCriteria("User")
                .Add(Restrictions.Like("Name.LastName", string.Format("%{0}%", lastName)));
            return crit1.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
             
        }

        public Application FindBySeekerandScholarship(Seeker seeker,Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .UniqueResult<Application>();
        }

        private ICriteria BuildOrderByCriteria(ICriteria query, string sortExpression)
        {
            switch (sortExpression)
            {
                case "ScholarshipName":
                case "ScholarshipName ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("Name", true));
                    break;

                case "ScholarshipName DESC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("Name", false));
                    break;

                case "ApplicationDueDate":
                case "ApplicationDueDate ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("ApplicationDueDate", true));
                    break;

                case "ApplicationDueDate DESC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("ApplicationDueDate", false));
                    break;

                case "NumberOfAwards":
                case "NumberOfAwards ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", true));
                    break;

                case "NumberOfAwards DESC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", false));
                    break;

                case "ScholarshipAmount":
                case "ScholarshipAmount ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("MaximumAmount", true));
                    break;

                case "ScholarshipAmount DESC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("MaximumAmount", false));
                    break;

                case "Status":
                case "Status ASC":
                    query.AddOrder(new Order("ApplicationStatus", true));

                    break;

                case "Status DESC":
                    query.AddOrder(new Order("ApplicationStatus", false));
                    break;


                case "DateSubmitted":
                case "DateSubmitted ASC":
                    query.AddOrder(new Order("SubmittedDate", true));
                    break;

                case "DateSubmitted DESC":
                    query.AddOrder(new Order("SubmittedDate", false));
                    break;
                

                case "AwardStatus":
                case "AwardStatus ASC":

                    query.AddOrder(new Order("AwardStatus", true));
                    break;

                case "AwardStatus DESC":
                    query.AddOrder(new Order("AwardStatus", false));
                    break;
                case "Finalist":
                case "Finalist ASC":

                    query.AddOrder(new Order("Finalist", true));
                    break;

                case "Finalist DESC":
                    query.AddOrder(new Order("Finalist", false));
                    break;

                    
                default:
                    throw new NotSupportedException(string.Format("SortExpression {0} not supported", sortExpression));
            }
            return query;
        }


        
    }
}