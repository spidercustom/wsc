using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IMatchDAL : IDAL<Match>
    {
        IList<Match> FindAll(Application application);
        IList<Match> FindAll(  Seeker seeker, MatchStatus status);
        IList<Match> FindAll( Scholarship scholarship, MatchStatus[] status);

        IList<Match> FindAllCurrentMatches(int startIndex, int rowCount, string sortExpression, Seeker seeker,
										   bool excludeMatchesWithApplications, MatchFilterCriteria criteria);

		int CountAllCurrentMatches(Seeker seeker, bool excludeMatchesWithApplications, MatchFilterCriteria criteria);
		Match Find(Seeker seeker, int scholarshipId);

        void UpdateMatches(Seeker seeker);
    }
}