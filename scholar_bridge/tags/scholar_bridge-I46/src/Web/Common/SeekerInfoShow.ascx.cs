﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Provider.BuildScholarship;

namespace ScholarBridge.Web.Common
{
    public partial class SeekerInfoShow : BaseScholarshipShow
    {
        private const string REQUIRED = "Required";
        private const string SHOULD_NOT_BE_FIRST_GENERATION = "Should not be first generation";
        private const string HONOR_SHOULD_NOT_BE_AWARDED = "Honor should not be awarded";

        public override int ResumeFrom { get { return WizardStepName.SeekerProfile.GetNumericValue(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);

            var seeker = ScholarshipToView.SeekerProfileCriteria;

            MapSeekerPersonality(seeker);
            MapSeekerDemographics(seeker);
            MapSeekerInterests(seeker);
            MapSeekerActivities(seeker);
            MapSeekerPerformace(seeker);

            SetupAttributeUsageTypeIcons(seeker);
            SetupVisibility(seeker);
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            StudentGroupRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.StudentGroup);
            SchoolTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SchoolType);
            AcademicProgramsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.AcademicProgram);
            EnrollmentStatusesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SeekerStatus);
            LengthOfProgrammRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ProgramLength);
            CollegesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.College);
            ApplicantStudentTypeTable.Visible  = ApplicantStudentTypeHeader.Visible =
                StudentGroupRow.Visible ||
                SchoolTypesRow.Visible ||
                AcademicProgramsRow.Visible ||
                EnrollmentStatusesRow.Visible ||
                LengthOfProgrammRow.Visible ||
                CollegesRow.Visible;


            FirstGenerationRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.FirstGeneration);
            EthnicityRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Ethnicity);
            ReligionRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Religion);
            GendersRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Gender);
            ApplicantDemographicsPeronalTable.Visible  = ApplicantDemographicsPeronalHeader.Visible = 
                FirstGenerationRow.Visible ||
                EthnicityRow.Visible ||
                ReligionRow.Visible ||
                GendersRow.Visible;

            StatesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.State);
            CountiesRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.County;
            CitiesRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.City;
            SchoolDistrictsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.SchoolDistrict;
            HighSchoolsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.HighSchool;
            ApplicantDemographicsGeographicTable.Visible = 
                ApplicantDemographicsGeographicHeader.Visible =
                StatesRow.Visible;


            AcademicAreasRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.AcademicArea);
            CareersRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Career);
            OrganizationsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Organization);
            CompanyRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Company);
            ApplicantInterestsTable.Visible  = ApplicantInterestsHeader.Visible = 
                AcademicAreasRow.Visible ||
                CareersRow.Visible ||
                OrganizationsRow.Visible ||
                CompanyRow.Visible;

            HobbiesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SeekerHobby);
            SportsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Sport);
            ClubsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Club);
            WorkTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.WorkType);
            WorkHoursRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.WorkHour);
            ServiceTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ServiceType);
            ServiceHoursRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ServiceHour);
            ApplicantActivitiesTable.Visible  = ApplicantActivitiesHeader.Visible = 
                HobbiesRow.Visible ||
                SportsRow.Visible ||
                ClubsRow.Visible ||
                WorkTypesRow.Visible ||
                WorkHoursRow.Visible ||
                ServiceTypesRow.Visible ||
                ServiceHoursRow.Visible;

            GPARow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.GPA);
			SATWritingScoreRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SATWriting);
			SATCriticalReadingScoreRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SATCriticalReading);
			SATMathematicsScoreRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SATMathematics);
			ACTEnglishScoreRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTEnglish);
			ACTMathematicsScoreRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTMathematics);
			ACTReadingScoreRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTReading);
			ACTScienceScoreRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTScience);
			ClassRankRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ClassRank);
            APCreditsEarnedRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.APCreditsEarned);
            IBCreditsEarnedRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.IBCreditsEarned);
            HonorsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Honor);
            ApplicantStudentPerformanceTable.Visible = ApplicantStudentPerformanceHeader.Visible =
                GPARow.Visible ||
				SATCriticalReadingScoreRow.Visible ||
				SATMathematicsScoreRow.Visible ||
				SATWritingScoreRow.Visible ||
				ACTEnglishScoreRow.Visible ||
				ACTMathematicsScoreRow.Visible ||
				ACTReadingScoreRow.Visible ||
				ACTScienceScoreRow.Visible ||
				ClassRankRow.Visible ||
                APCreditsEarnedRow.Visible ||
                IBCreditsEarnedRow.Visible ||
                HonorsRow.Visible;

            NoSelectionContainerControl.Visible = !(
                ApplicantStudentTypeHeader.Visible ||
                ApplicantDemographicsPeronalHeader.Visible ||
                ApplicantDemographicsGeographicHeader.Visible ||
                ApplicantInterestsHeader.Visible ||
                ApplicantActivitiesHeader.Visible ||
                ApplicantStudentPerformanceHeader.Visible);
        }

        private void MapSeekerPerformace(SeekerProfileCriteria seeker)
        {
            if (seeker.GPA != null)
            {
                if (seeker.GPA.Maximum.HasValue && seeker.GPA.Maximum != Scholarship.MAX_GPA)
                    lblGPA.Text = string.Format("{0} - {1}", seeker.GPA.Minimum, seeker.GPA.Maximum);
                else
                    lblGPA.Text = string.Format("Greater than {0}", seeker.GPA.Minimum);
            }

            if (seeker.SATScore != null)
            {
                lblSATWritingScore.Text = string.Format("Writing: {0} - {1} ",
                                                 seeker.SATScore.Writing.Minimum,
                                                 seeker.SATScore.Writing.Maximum);

                lblSATCriticalReadingScore.Text += string.Format("Critical Reading: {0} - {1} ",
                                                  seeker.SATScore.CriticalReading.Minimum,
                                                  seeker.SATScore.CriticalReading.Maximum);
                lblSATMathematicsScore.Text += string.Format("Mathematics: {0} - {1} ",
                                                  seeker.SATScore.Mathematics.Minimum,
                                                  seeker.SATScore.Mathematics.Maximum);
            }

            if (seeker.ACTScore != null)
            {
                lblACTEnglishScore.Text = string.Format("English: {0} - {1} ",
                                                seeker.ACTScore.English.Minimum,
                                                seeker.ACTScore.English.Maximum);

                lblACTMathematicsScore.Text += string.Format("Mathematics: {0} - {1} ",
                                          seeker.ACTScore.Mathematics.Minimum,
                                          seeker.ACTScore.Mathematics.Maximum);

                lblACTReadingScore.Text += string.Format("Reading: {0} - {1} ",
                                          seeker.ACTScore.Reading.Minimum,
                                          seeker.ACTScore.Reading.Maximum);

                lblACTScienceScore.Text += string.Format("Science: {0} - {1} ",
                                          seeker.ACTScore.Science.Minimum,
                                          seeker.ACTScore.Science.Maximum);
            }

           MapIf(lblClassRank, seeker.ClassRanks);

            lblAPCredits.Text = seeker.APCreditsEarned.ToString();
            lblIBCredits.Text = seeker.IBCreditsEarned.ToString();
            lblHonors.Text = seeker.Honors ? REQUIRED : HONOR_SHOULD_NOT_BE_AWARDED;
        }

        private void MapSeekerActivities(SeekerProfileCriteria seeker)
        {
            MapIf(lblHobbies, seeker.Hobbies);
            MapIf(lblSports, seeker.Sports);
            MapIf(lblClubs, seeker.Clubs);
            MapIf(lblServiceHours, seeker.ServiceHours);
            MapIf(lblWorkHours, seeker.WorkHours);
            MapIf(lblWorkType, seeker.WorkTypes);
            MapIf(lblServiceHours, seeker.ServiceHours);
            MapIf(lblServiceType, seeker.ServiceTypes);
        }

        private void MapSeekerInterests(SeekerProfileCriteria seeker)
        {
            MapIf(lblAcademicAreas, seeker.AcademicAreas);
            MapIf(lblCareers, seeker.Careers);
            MapIf(lblOrganizations, seeker.Organizations);
            MapIf(lblCompanies, seeker.Companies);
        }

        private void MapSeekerDemographics(SeekerProfileCriteria seeker)
        {
            MapIf(lblEthinicity, seeker.Ethnicities);
            MapIf(lblReligion, seeker.Religions);
            MapIf(lblCounty, seeker.Counties);
            MapIf(lblCity, seeker.Cities);
            MapIf(lblSchoolDistrict, seeker.SchoolDistricts);
            MapIf(lblHighSchool, seeker.HighSchools);
            MapIf(lblState, seeker.States);

            lblGender.Text = seeker.Genders.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
        }

        private void SetupAttributeUsageTypeIcons(SeekerProfileCriteria criteria)
        {
            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(MinimumCriteriaIconControl,
                                                                    ScholarshipAttributeUsageType.Minimum);
            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(PreferenceIconCcontrol,
                                                                    ScholarshipAttributeUsageType.Preference);

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(StudentGroupUsageTypeIconControl,
                           criteria.Attributes.GetUsageType(SeekerProfileAttribute.StudentGroup));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SchoolTypeUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SchoolType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicProgramUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.AcademicProgram));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerStatusUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SeekerStatus));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ProgramLengthUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ProgramLength));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CollegesUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.College));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FirstGenerUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.FirstGeneration));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(EthnicityUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Ethnicity));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ReligionUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Religion));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GendersUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Gender));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicAreasUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.AcademicArea));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CareersUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Career));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(OrganizationsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Organization));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CompaniesUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Company));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerHobbiesUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SeekerHobby));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SportsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Sport));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClubsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Club));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkTypeUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.WorkType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkHoursUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.WorkHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceTypeUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ServiceType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceHoursUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ServiceHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GPAUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.GPA));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClassRankUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ClassRank));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SATWritingScoreUsageTypeIconControl,
							criteria.Attributes.GetUsageType(SeekerProfileAttribute.SATWriting));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SATCriticalReadingScoreUsageTypeIconControl,
				criteria.Attributes.GetUsageType(SeekerProfileAttribute.SATCriticalReading));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SATMathematicsScoreUsageTypeIconControl,
				criteria.Attributes.GetUsageType(SeekerProfileAttribute.SATMathematics));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ACTEnglishScoreUsageTypeIconControl,
							criteria.Attributes.GetUsageType(SeekerProfileAttribute.ACTEnglish));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ACTMathematicsScoreUsageTypeIconControl,
				criteria.Attributes.GetUsageType(SeekerProfileAttribute.ACTMathematics));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ACTReadingScoreUsageTypeIconControl,
				criteria.Attributes.GetUsageType(SeekerProfileAttribute.ACTReading));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ACTScienceScoreUsageTypeIconControl,
				criteria.Attributes.GetUsageType(SeekerProfileAttribute.ACTScience));

			CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(HonorsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Honor));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(APCUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.APCreditsEarned));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(IBCUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.IBCreditsEarned));

        }

        private void MapSeekerPersonality(SeekerProfileCriteria seeker)
        {
            MapIf(lblColleges, seeker.Colleges);

            lblStudentGroups.Text = seeker.StudentGroups.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            lblSchoolTypes.Text = seeker.SchoolTypes.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            lblAcademicPrograms.Text = seeker.AcademicPrograms.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            lblSeekerStatuses.Text = seeker.SeekerStatuses.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            lblLengthOfProgram.Text = seeker.ProgramLengths.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            lblFirstGeneration.Text = seeker.FirstGeneration ? REQUIRED : SHOULD_NOT_BE_FIRST_GENERATION;
        }

        private static void MapIf<T>(ITextControl lbl, IList<T> items) where T : ILookup
        {
            if (null != items && items.Count > 0)
                lbl.Text = items.CommaSeparatedNames();
        }
    }
}