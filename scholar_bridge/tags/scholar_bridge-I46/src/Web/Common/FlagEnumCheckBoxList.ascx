﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlagEnumCheckBoxList.ascx.cs" Inherits="ScholarBridge.Web.Common.FlagEnumCheckBoxList" %>

<div class="control-set">
    <asp:CheckBoxList ID="CheckBoxList" runat="server"  RepeatDirection="Vertical" RepeatLayout="Flow" Width="170px" CssClass="checkBoxListWrap">
    </asp:CheckBoxList>
    <br />
    <asp:CustomValidator ID="SelectNoneValidator" runat="server" 
      ErrorMessage="Select atleast one" ></asp:CustomValidator>
    <br />
    <input type="button"  class="ListButton ui-corner-all" ID="SelectAllButton"  runat="server" value="Select All"   />
</div>