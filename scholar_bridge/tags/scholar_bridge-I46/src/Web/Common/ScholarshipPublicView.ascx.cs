﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipPublicView : UserControl
    {
        
        public Scholarship Scholarship { get; set; }

    	private LinkGenerator linkGenerator;
    	private LinkGenerator LinkGenerator
    	{
    		get
    		{
    			if (linkGenerator == null)
    				linkGenerator = new LinkGenerator();
    			return linkGenerator;
    		}
    	}

        protected void Page_Load(object sender, EventArgs e)
        {
            registrationLink.Text = ConfigHelper.GetDomainName();
			registrationLink.NavigateUrl = LinkGenerator.GetFullLink("/Seeker/Register.aspx");
            if (null != Scholarship)
            {
                scholarshipName.Text = Scholarship.Name;
                providerName.Text = Scholarship.Provider == null ? "" : Scholarship.Provider.Name;
                intermediaryName.Text = Scholarship.Intermediary == null ? "" : Scholarship.Intermediary.Name;
                donorName.Text = Scholarship.Donor == null ? "" : Scholarship.Donor.Name;
                missionStatement.Text = Scholarship.MissionStatement;
            }
        }
    }
}