﻿using System;
using System.Web;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{

    public partial class Tip : UserControl
    {
        public string content { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string text = HttpUtility.HtmlEncode(content);
            infotip.ToolTip = text;
            infotip.AlternateText = text;
        }
    }
}