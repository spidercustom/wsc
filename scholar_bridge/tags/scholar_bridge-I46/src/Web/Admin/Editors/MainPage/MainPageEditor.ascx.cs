﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin.Editors.MainPage 
{

    public partial class MainPageEditor : UserControl
    {
		public ILoginPageTextService LoginPageTextService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
				newAndAnnouncementHtml.Text = loginPageTextBox.Text = LoginPageTextService.GetLoginPageText();	
            }
		}

    	protected void saveButton_Click(object sender, EventArgs e)
        {
			messageLabel.Text = "";
			Page.Validate();
            if (!Page.IsValid) return;
			LoginPageTextService.PutLoginPageText(loginPageTextBox.Text);
			newAndAnnouncementHtml.Text = loginPageTextBox.Text;
			messageLabel.Text = "Login page text updated successfully.";
		}

    	protected void previewButton_Click(object sender, EventArgs e)
    	{
			messageLabel.Text = "";
			newAndAnnouncementHtml.Text = loginPageTextBox.Text;
			messageLabel.Text = "Preview updated successfully.";

    	}
    }
}