﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class PrintScholarship : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["id"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsInPrintView())
                Response.Redirect("~/Provider/Scholarships/printScholarship.aspx?id=" + ScholarshipId + "&print=true");

            var currentScholarship = ScholarshipService.GetById(ScholarshipId);
            if (UserContext.ScholarshipBelongsToOrganization(currentScholarship))
            {
                showScholarship.Scholarship = currentScholarship;
                scholarshipTitleStripeControl.UpdateView(currentScholarship);
            }
            else
            {
                Response.Redirect("~/ScholarshipDetails.aspx?sid=" + ScholarshipId);
            }
            EmailtoOthers.NavigationUrl = ResolveUrl("~/SendToFriend.aspx?popup=true&id={0}".Build(ScholarshipId));
        }
    }
}