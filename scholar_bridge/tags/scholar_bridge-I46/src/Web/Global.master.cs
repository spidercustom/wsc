﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Domain.Auth;
using Spring.Context.Support;

namespace ScholarBridge.Web
{
    public partial class GlobalMasterPage : MasterPage
    {
        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext");}
        }
        
        public string ImpersonatingUser
        {
            get
            {
                return Session[ImpersonateUser.IMPERSONATING_USER_SESSION_VARIABLE_NAME] as string;
            }
            set
            {
                Session[ImpersonateUser.IMPERSONATING_USER_SESSION_VARIABLE_NAME] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "theWashBoard.org | " + Page.Header.Title;

            if (UserContext.CurrentUser != null)
            {
                WelcomeContainer.Visible = true;

                var currentUser = UserContext.CurrentUser;

                UserFullName.Text = (currentUser.Name == null) ? currentUser.Username : UserContext.CurrentUser.Name.FirstName;

                if (Page.User.IsInRole(Role.PROVIDER_ROLE) || Page.User.IsInRole(Role.INTERMEDIARY_ROLE))
                {
                    OrganizationName.Text = UserContext.CurrentOrganization.Name;
                }
            }


            if (!Page.IsPostBack)
            {
                string[] names = Page.User.Identity.Name.Split('!');
                if (names.Length > 1)
                {
                    ImpersonatingUser = names[1];
                    FormsAuthentication.SetAuthCookie(names[0], false);

                }
                if (string.IsNullOrEmpty(ImpersonatingUser))
                    ImpersonationPlaceholder.Visible = false;
            }
        }

        protected void stopImpersonationButton_Command(object sender, CommandEventArgs e)
        {
            FormsAuthentication.SignOut();

            string redirUrl = FormsAuthentication.GetRedirectUrl(ImpersonatingUser, false) + Page.Request.PathInfo;
            if (Page.Request.QueryString.HasKeys())
            {
                redirUrl += "?" + Page.Request.QueryString;
            }

            FormsAuthentication.SetAuthCookie(ImpersonatingUser, false);
            AbandonSessionAndRedirect(redirUrl);
        }

        public void AbandonSessionAndRedirect(string path)
        {
            Session.Abandon();
            Response.Redirect(path, true);
        }

    }
}
