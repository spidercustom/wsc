﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Intermediary.Users
{
    public partial class Create : Page
    {
        public IUserContext UserContext { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            if (!User.IsInRole(Role.INTERMEDIARY_ADMIN_ROLE))
                Response.Redirect("~/default.aspx");
        }

        protected void userForm_OnUserSaved(User user)
        {
            user.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var intermediary = UserContext.CurrentIntermediary;

            IntermediaryService.SaveNewUser(intermediary, user, true);

            SuccessMessageLabel.SetMessage("User was created successfully.");
            PopupHelper.CloseSelf(false);
            PopupHelper.RefreshParent(ResolveUrl("~/Intermediary/Admin/#user-tab"));
        }

        protected void userForm_OnFormCanceled()
        {
            PopupHelper.CloseSelf(false);
        }
    }
}
