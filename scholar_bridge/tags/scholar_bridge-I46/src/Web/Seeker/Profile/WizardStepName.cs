﻿namespace ScholarBridge.Web.Seeker.Profile
{
    public enum WizardStepName
    {
        Basics,
        AboutMe,
        Academics,
        Activities,
        FinancialNeed
    }
}
