﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.Default"  Title="Build Application" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="~/Seeker/BuildApplication/Basics.ascx" tagname="Basics" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/AboutMe.ascx" tagname="AboutMe" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/AcademicInformation.ascx" tagname="AcademicInfo" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/Activities.ascx" tagname="Activities" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/FinancialNeed.ascx" tagname="FinancialNeed" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/AdditionalCriteria.ascx" tagname="AdditionalCriteria" tagprefix="sb" %>
<%@ Register Src="~/Common/EntityTitleStripe.ascx" TagName="ScholarshipTitleStripe" TagPrefix="sb" %>
<%@ Register Src="~/Common/PrintView.ascx" TagName="PrintView" TagPrefix="sb" %>


<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="Build Application" />

    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>

    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/styles/form.css") %>" rel="stylesheet" type="text/css" media="All"/> 
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="167" width="873" src="<%= ResolveUrl("~/images/header/my_profile.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <div id="canStartApplicationWarning" title="" style="display:none">
        Provider is not accepting applications until <asp:Literal ID="lblAppStartDateForWarning" runat="Server" />. 
        You may submit your application after this date.    
    </div>
    <div id="confirmSaving" title="Confirm saving" style="display:none">
        You have changed the data. Would you like to save the changes?
    </div>
    <div id="PostalWarningDiv" title="Confirm saving" style="display:none">
        <p>This provider requires all materials must be submitted by mail to:</p>
        <p><asp:Literal ID="PostalOrgName" runat="server" /></p>
        <p><asp:Literal ID="PostalOrgAddress" runat="server" /></p>
        <p><asp:Literal ID="PostalOrgPhone" runat="server" /></p>
    </div>
    <div id="confirmDelete" title="Confirm Delete" style="display:none">
        Scholarship application will be deleted permanently. Are you sure want to delete?
    </div>
    <div id="confirmSubmit" title="Confirm Submission" style="display:none">
        <p>The scholarship application will be saved and can not be edited once submitted.</p>
        <p>Please certify that all information provided in the application is true and accurate to the best of your knowledge.</p>
        <p>Do you want to continue and submit?</p>
    </div>
    <div id="warnAttachments" title="Confirm Submission" style="display:none">
        <p><b> Scholarship has additional requirements. Make sure you have attached or included all the requested materials.</b></p>
        <p>The scholarship application will be saved and can not be edited once submitted. </p>
        <p>Please certify that all information provided in the application is true and accurate to the best of your knowledge.</p>
        <p>Do you want to continue and submit?</p>
    </div>
    <a id="A1" class="button" style="float:right;" target="_blank" href="/Seeker/Applications/Show.aspx?aid=<asp:Literal ID="current_app_id" runat="server" />&print=true"><span>Print View</span></a>
    <h1>Edit Application</h1>
    <h2><asp:Literal ID="scholarshipName" runat="server" />, <asp:Literal ID="scholarshipProvider" runat="server"></asp:Literal></h2>

    <div class="tabs" style="clear:both;" id="BuildApplicationWizardTab">
        <ul>
            <li><a href="#tab"><span>Basics</span></a></li>
            <li><a href="#tab"><span>About Me</span></a></li>
            <li><a href="#tab"><span>My Academic Info</span></a></li>
            <li><a href="#tab"><span>My Activities</span></a></li>
            <li><a href="#tab"><span>My Financial Need</span></a></li>
            <li><a href="#tab"><span>Additional Requirements</span></a></li>
        </ul>
         
        <div id="tab">
            <asp:MultiView  ID="BuildApplicationWizard" runat="server" OnActiveViewChanged="BuildApplicationWizard_ActiveStepChanged">
              <asp:View ID="BasicInfoStep" runat="server">
                <sb:Basics ID="Basics1" runat="server" />
              </asp:View>
              <asp:View ID="AboutMeStep" runat="server">
                    <sb:AboutMe ID="AboutMe1" runat="server" />
              </asp:View>
              <asp:View ID="AcademicStep" runat="server">
                    <sb:AcademicInfo ID="AcademicInfo1" runat="server" />
              </asp:View>
              <asp:View ID="ActivitiesStep" runat="server">
                    <sb:Activities ID="Activities1" runat="server"  />
              </asp:View>
              <asp:View ID="FinancialNeed" runat="server">
                    <sb:FinancialNeed ID="FinancialNeed1" runat="server" />
              </asp:View>
              <asp:View ID="AdditionalCriteriaStep" runat="server">
                    <sb:AdditionalCriteria ID="AdditionalCriteria" runat="server" />
              </asp:View>
            </asp:MultiView>
        </div>
        <sbCommon:jQueryTabIntegrator id="buildApplicationWizardTabIntegrator" runat="server" CausesValidation="true" MultiViewControlID="BuildApplicationWizard" TabControlClientID="BuildApplicationWizardTab" />
    </div>
    
    <div id="SubmissionValidationErrors" title="Application submission" style="display:none">
        <asp:Repeater ID="IssueListControl" runat="server">
            <HeaderTemplate><ul></HeaderTemplate>
            <ItemTemplate><li><%# DataBinder.Eval(Container.DataItem, "Message") %></li></ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
        <p>All issues specified in above list must to be fixed for application that is submitting or is submitted.</p>
    </div>
    
    <div style="float:right">
        <sbCommon:SaveConfirmButton ID="PreviousButton" runat="server" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" OnClick="PreviousButton_Click" Text="Previous"/>
        <sbCommon:SaveConfirmButton ID="NextButton" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" OnClick="NextButton_Click" runat="server" Text="Next" />
        <sbCommon:SaveConfirmButton ID="SubmitButton" OnClick="SubmitButton_Click" runat="server" ConfirmMessageDivID="confirmSaving" Text="Submit"/>
    </div>
    <sbCommon:SaveConfirmButton ID="SaveAndExitButton" ConfirmMessageDivID="confirmSaving" NeedsConfirmation="false" OnClick="SaveAndExit_Click" runat="server" Text="Save & Exit" />
    <sbCommon:ConfirmButton ID="DeleteButton" CausesValidation="false" ConfirmMessageDivID ="confirmDelete" Text="Delete"  runat="server" OnClickConfirm="DeleteButton_Click"    />   

    <!-- hack to get the WebForm_DoPostBackWithOptions to be emitted -->
    <span style="visibility:hidden"><sbCommon:AnchorButton ID="SaveButton" OnClick="SaveButton_Click" runat="server"/></span>
</asp:Content>
