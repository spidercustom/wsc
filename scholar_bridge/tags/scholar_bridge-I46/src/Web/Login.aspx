﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ScholarBridge.Web.Login" Title="Login" %>
<%@ Import Namespace="ScholarBridge.Web"%>
<%@ Register Src="~/Common/Login.ascx" TagName="Login" TagPrefix="sb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="Log in to theWashBoard.org"/>
    <style type="text/css">
        #Header{background-image:url(<%= ResolveUrl("~/images/header/top_background_plain.png") %>);}
        #PageImage{top:-56px;}
        #PageContent-Wrapper{top:-66px;}
        #Navigation{display:none;}
        
        #Login{position:absolute; left:81px; top:335px; color:White;}
        #Login h2{font-size:16px;}
        #Login a{color:#cbe591;}
        #Login input.button{margin:0px; vertical-align:middle;}
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageImage" Runat="Server">
    <div style="width:873px; background-image:url(<%= ResolveUrl("~/images/header/front_page.jpg") %>); height:526px;">&nbsp;</div>
    <sb:Login ID="loginForm" runat="server" LinkTo="/Seeker/Register.aspx" ShowFailureDialog="true" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Body" Runat="Server">
    <div class="three-column">
        <div>
            <a href="<%= ResolveUrl("~/Seeker/Anonymous.aspx") %>">
                <img alt="" src="<%= ResolveUrl("~/images/HomepageBottomBox01_ForSeekers.gif") %>" width="262px" height="41px"/>
                <img alt="" src="<%= ResolveUrl("~/images/OverallHomepageBottomBox01.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">
                Create a profile and let us do the rest. We will match you with scholarships you
                are most likely to qualify for and applying online is easy.
            </p>
            <a class="circle-icon-link" href="<%= LinkGenerator.GetFullLinkStatic("/Seeker/Register.aspx") %>" style="font-size:14px;" >Seeker Registration</a>
        </div>
        <div style="font-size:14px;">
            <a href='<%= LinkGenerator.GetFullLinkStatic("/PressRoom/") %>'><img alt="News and Announcements" src="<%= ResolveUrl("~/images/news_announcements.png") %>"/></a>
            <!--#include file="_ScholarshipData\loginInclude.htm" -->
        </div>
        <div>
            <a href="<%= ResolveUrl("~/Provider/Anonymous.aspx") %>">
                <img alt="" src="<%= ResolveUrl("~/images/HomepageBottomBox03_ForProviders.gif") %>" width="262px" height="41px"/>
                <img alt="" src="<%= ResolveUrl("~/images/OverallHomepageBottomBox03.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">
                Post your scholarship to reach a larger group of students. We make reviewing and
                evaluating applications easy. Find the next recipients of your scholarship.
            </p>
            <a class="circle-icon-link" href="<%= LinkGenerator.GetFullLinkStatic("/Provider/RegisterProvider.aspx") %>" style="font-size:14px;" >Provider Registration</a>
        </div>
    </div>
</asp:Content>
