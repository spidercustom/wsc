using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class DomainListExtensionsTests
    {

        [Test]
        public void to_csv_names()
        {
            var l = new List<City>();
            Assert.AreEqual("", l.CommaSeparatedNames());

            l.Add(new City { Name = "Milwaukee" });
            Assert.AreEqual("Milwaukee", l.CommaSeparatedNames());

            l.Add(new City { Name = "Seattle" });
            Assert.AreEqual("Milwaukee, Seattle", l.CommaSeparatedNames());
        }

        [Test]
        public void to_csv_ids()
        {
            var list = new List<City>();
            Assert.AreEqual("", list.CommaSeparatedIds());

            list.Add(new City { Id="MIL", Name = "Milwaukee" });
            Assert.AreEqual("MIL", list.CommaSeparatedIds());

            list.Add(new City { Id = "SEA", Name = "Seattle" });
            Assert.AreEqual("MIL, SEA", list.CommaSeparatedIds());
        }
        
    }
}