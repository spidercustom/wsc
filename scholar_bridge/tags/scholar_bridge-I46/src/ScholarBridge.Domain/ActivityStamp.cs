﻿using System;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Domain
{
    public class ActivityStamp
    {
        public ActivityStamp()
        {
            On = DateTime.Now;
        }

        public ActivityStamp(User by)
        {
            By = by;
            On = DateTime.Now;
        }

        public virtual User By { get; set; }
        public virtual DateTime On { get; set; }
    }
}
