using System;

namespace ScholarBridge.Domain.Messaging
{
    public interface IMessage
    {
        int Id { get; set; }

        string Subject { get; }
        string Content { get; }
        DateTime Date { get; }
        MessageType MessageTemplate { get; }

        MessageAddress To { get; }
        MessageAddress From { get; }
    }
}