using Common.Logging;
using Quartz;
using ScholarBridge.Business;

namespace ScholarBridge.Jobs
{
	/// <summary>
	/// Job to create a list of scholarships (in xml) and publish them locally and to the public facing web-site.
	/// </summary>
    public class ScholarshipListJob : NHibernateBaseJob
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(NotifySeekersOfClosedScholarship));

        private IScholarshipListService scholarshipListService;

		// spring loaded...
		public IScholarshipListService ScholarshipListService { set { scholarshipListService = value; } }

        protected override void ExecuteTransactional(JobExecutionContext ctx)
        {
            Log.Info("Begin ScholarshipListJob");

        	scholarshipListService.GenerateAndStoreScholarshipList();

			Log.Info("End ScholarshipListJob");
        }
    }
}