using NHibernate;
using Quartz;
using Spring.Data.NHibernate.Support;
using Spring.Scheduling.Quartz;

namespace ScholarBridge.Jobs
{
    public abstract class NHibernateBaseJob : QuartzJobObject
    {
        private ISessionFactory sessionFactory;
        // Set only so it doesn't get serialized
        public ISessionFactory SessionFactory { set { sessionFactory = value; } }

        /// <summary>
        /// Implemented by class to perform real functionality with a valid Hibernate session.
        /// </summary>
        /// <param name="ctx"></param>
        protected abstract void ExecuteTransactional(JobExecutionContext ctx);

        protected override void ExecuteInternal(JobExecutionContext ctx)
        {
            // Session scope is the same thing as used by OpenSessionInView
            using (var ss = new SessionScope(sessionFactory, true))
            {
                ExecuteTransactional(ctx);
                ss.Close();
            }
        }
    }
}