using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public interface IMessageService
    {
        Message FindMessage(User user, Organization organization, int id);
        SentMessage FindSentMessage(User user, Organization organization, int id);

        IList<Message> FindAll(int startIndex, int rowCount, string sortExpression, MessageAction action, User user, Organization organization);
        IList<SentMessage> FindAllSent(int startIndex, int rowCount, string sortExpression, User user, Organization organization);
        IList<Message> FindAllArchived(int startIndex, int rowCount, string sortExpression, User user, Organization organization);
        IList<Message> FindAllApprovedScholarships(int startIndex, int rowCount, string sortExpression, User user, Organization organization);
        IList<Message> FindAllForInbox(int startIndex, int rowCount, string sortExpression, User user, Organization organization);
		int CountUnread(User user, Organization organization);
        int CountAll(  MessageAction action, User user, Organization organization);
        int CountAllArchived(  User user, Organization organization);
        int CountAllApprovedScholarships(  User user, Organization organization);
        int CountAllForInbox(  User user, Organization organization);
        int CountAllSent(  User user, Organization organization);
      
		void ContactUsSupport(string fromEmail, User fromUser, string messageText);
		void ContactUsProblem(string fromEmail, User fromUser, string messageText);
		void ContactUsSuggestion(string fromEmail, User fromUser, string messageText);

		void SendMessage(Message message);
		void ArchiveMessage(Message message);
        void MarkMessageRead(Message message);

        void DeletedRelatedMessages(Scholarship scholarship);
        void SaveNewSentMessage(Message message);
    }
}