using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
	public class MatchService : IMatchService
	{
		private static readonly MatchStatus[] SAVED_AND_APPLIED_STATUS = new[] { MatchStatus.Saved, MatchStatus.Submitted };
		public IMatchDAL MatchDAL { get; set; }
		public ISeekerDAL SeekerDAL { get; set; }

		public Match GetMatch(Seeker seeker, int scholarshipId)
		{
			return MatchDAL.Find(seeker, scholarshipId);
		}

		public void SaveMatch(Seeker seeker, int scholarshipId)
		{
			ChangeStatus(seeker, scholarshipId, MatchStatus.Saved);
		}

		public void InsertCustomMatch(Match match)
		{
			if (null == match)
				throw new ArgumentNullException("match");

			MatchDAL.Insert(match);
		}

		public void UnSaveMatch(Seeker seeker, int scholarshipId)
		{
			ChangeStatus(seeker, scholarshipId, MatchStatus.New);
		}

		public void ApplyForMatch(Seeker seeker, int scholarshipId, Application application)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");

			var match = GetMatch(seeker, scholarshipId);
			match.Application = application;
			match.MatchStatus = MatchStatus.Submitted;
			MatchDAL.Update(match);

			return;
		}

		public void ChangeStatus(Seeker seeker, int scholarshipId, MatchStatus newStatus)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");

			var match = MatchDAL.Find(seeker, scholarshipId);
			if (null != match)
			{
				match.MatchStatus = newStatus;
				match.LastUpdate = new ActivityStamp(seeker.User);
				MatchDAL.Update(match);
			}
		}

		public IList<Match> GetMatchesForSeekerWithoutApplications(int startIndex, 
																	int rowCount, 
																	string sortExpression, 
																	Seeker seeker, 
																	MatchFilterCriteria criteria)
		{
			return GetMatchesForSeeker(startIndex, rowCount, sortExpression, seeker, true, criteria);
		}

		public int CountMatchesForSeekerWithoutApplications(Seeker seeker, MatchFilterCriteria criteria)
		{
			return CountMatchesForSeeker(seeker, true, criteria);
		}

		private int CountMatchesForSeeker(Seeker seeker, bool excludeMatchesWithApplications, MatchFilterCriteria criteria)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");

			if ((!seeker.LastMatch.HasValue) || seeker.LastMatch.Value < seeker.ProfileLastUpdateDate)
			{
				MatchDAL.UpdateMatches(seeker);
				seeker.LastMatch = DateTime.Now;
				SeekerDAL.Update(seeker);
			}

			return MatchDAL.CountAllCurrentMatches(seeker, excludeMatchesWithApplications, criteria);
		}

		private IList<Match> GetMatchesForSeeker(int startIndex, 
												int rowCount, 
												string sortExpression, 
												Seeker seeker, 
												bool excludeMatchesWithApplications, 
												MatchFilterCriteria criteria)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");

			if ((!seeker.LastMatch.HasValue) || seeker.LastMatch.Value < seeker.ProfileLastUpdateDate)
			{
				MatchDAL.UpdateMatches(seeker);
				seeker.LastMatch = DateTime.Now;
				SeekerDAL.Update(seeker);
			}

			return MatchDAL.FindAllCurrentMatches(startIndex, rowCount, sortExpression, seeker, excludeMatchesWithApplications, criteria);
		}

		public IList<Match> GetSavedButNotAppliedMatches(Seeker seeker)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");

			return MatchDAL.FindAll(seeker, MatchStatus.Saved);
		}

		public IList<Match> GetSavedMatches(Scholarship scholarship)
		{
			if (null == scholarship)
				throw new ArgumentNullException("scholarship");

			return MatchDAL.FindAll(scholarship, SAVED_AND_APPLIED_STATUS);
		}

		public void UpdateMatches(Seeker seeker)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");

			MatchDAL.UpdateMatches(seeker);
		}

		public void DisconnectApplication(Application application)
		{
			if (null == application)
				throw new ArgumentNullException("application");

			var matches = MatchDAL.FindAll(application);
			if (null != matches)
			{
				foreach (Match match in matches)
				{
					match.Application = null;
					match.MatchStatus = MatchStatus.Saved;
					MatchDAL.Update(match);
				}
			}
		}
	}
}