﻿using System;
using System.Web.UI;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Extensions
{
    public static class ClientScriptExtensions
    {
        private  const string ON_DOCUMENT_READY_HANDLER_TEMPLATE = @"$(document).ready(function() {{ {0} }});";
        public static void RegisterOnDocumentReadyBlock(this ClientScriptManager clientScript, Type type, string key, string scriptToRun)
        {
            if (clientScript.IsClientScriptBlockRegistered(type, key))
                return;

            clientScript.RegisterClientScriptBlock(type, key,
                ON_DOCUMENT_READY_HANDLER_TEMPLATE.Build(scriptToRun), true);
        }
    }
}
