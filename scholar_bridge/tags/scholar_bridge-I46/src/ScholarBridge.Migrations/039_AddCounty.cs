﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(39)]
    public class AddCounty : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "County";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
