﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(34)]
    public class AddCareer : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "Career";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
