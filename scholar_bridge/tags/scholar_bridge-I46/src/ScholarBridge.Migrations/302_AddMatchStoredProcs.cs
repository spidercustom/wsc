﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(302)]
	public class AddMatchStoredProcs : Migration
    {
        public override void Up()
        {
        	DropFunction();
			Database.ExecuteNonQuery(MATCH_RANK_FUNCTION);

			DropStoredProcs();
			Database.ExecuteNonQuery(SEEKER_MATCH_PROC);
			Database.ExecuteNonQuery(SCHOLARSHIP_MATCH_PROC);
		}

		public override void Down()
		{
			DropStoredProcs();
			DropFunction();
		}

		private void DropFunction()
		{
			Database.ExecuteNonQuery("if exists (select * from sys.objects where name = 'ComputeMatchRank') drop function dbo.ComputeMatchRank");
		}

		private void DropStoredProcs()
		{
			Database.ExecuteNonQuery("if exists (select * from sys.procedures where name = 'BuildSeekerMatches') drop procedure dbo.BuildSeekerMatches");
			Database.ExecuteNonQuery("if exists (select * from sys.procedures where name = 'BuildScholarshipMatches') drop procedure dbo.BuildScholarshipMatches");
		}

		#region Match Rank Function

    	public const string MATCH_RANK_FUNCTION =
    		@"
-- =============================================
-- Author:		Dave Richard
-- Create date: 1/4/2010
-- Description:	compute a match rank
-- =============================================
create FUNCTION ComputeMatchRank
(
	@SeekerPreferredCount int,
	@SeekerMinimumCount   int,
	@ScholarshipPreferredCount int,
	@ScholarshipMinimumCount int
)
RETURNS decimal(19,2)
AS
BEGIN

	declare @MinimumCriteriaPercent decimal(8,3), 
			@PreferredCriteriaPercent decimal(8,3)
			
	if 0 = @ScholarshipMinimumCount
         set @MinimumCriteriaPercent = 1
    else
		set @MinimumCriteriaPercent = cast(@SeekerMinimumCount  as decimal(8,2)) / cast(@ScholarshipMinimumCount  as decimal(8,2)) 
			
	if 0 = @ScholarshipPreferredCount
         set @PreferredCriteriaPercent = 1
    else
		set @PreferredCriteriaPercent = cast(@SeekerPreferredCount  as decimal(8,2)) / cast(@ScholarshipPreferredCount  as decimal(8,2)) 
            

	return Round(@MinimumCriteriaPercent * 67.0 + @PreferredCriteriaPercent * 33.0, 2) ;

END
		";
		#endregion

		#region Seeker Match Stored Proc

		public const string SEEKER_MATCH_PROC = @"
-- =============================================
-- Author:		Dave Richard
-- Create date: 7 Jan 2010
-- Description:	Redo the match rows for a given seeker
-- =============================================
CREATE PROCEDURE dbo.BuildSeekerMatches
	@SeekerId int,
	@UpdateUserId int 
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

MERGE SBMatch AS sbm
USING
(
	select 
		SBScholarshipID,
		SBSeekerId,
		seekerPrefCrit,
		seekerMinCrit,
		scholarshipPrefCrit,
		scholarshipMinCrit,
		deletable,
		[rank] = dbo.ComputeMatchRank(seekerPrefCrit,
									seekerMinCrit,
									scholarshipPrefCrit,
									scholarshipMinCrit)

	from

	(
		select 
			pv.SBScholarshipID,
			pv.SBSeekerId,
			pv.[1] as seekerPrefCrit,
			pv.[2] as seekerMinCrit,
			cc.prefCrit as scholarshipPrefCrit,
			cc.minCrit as scholarshipMinCrit,
			0 as deletable
		from
		(
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchBools  where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchRanges where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchInEnum where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchInList where SBSeekerId=@seekerid 
				group by SBScholarshipId, SBSeekerId, SBUsageTypeIndex, criterion
		) as sourcetable
		PIVOT
		(
			COUNT(SBUsageTypeIndex) FOR SBUsageTypeIndex IN ([1], [2])
		)  AS pv
		
		inner join SBScholarshipCriteriaCounts as cc
			on pv.SBScholarshipID = cc.SBScholarshipID
		where pv.SBSeekerId is not null
		  and not exists (select * from SBMatchExclusion me
							where me.SBSeekerId = pv.SBSeekerId
							and me.SBScholarshipId = pv.SBScholarshipId)
		  and not (pv.[2] = 0 and cc.minCrit > 0) -- if a seeker doesn't meet any minimum requirements then skip 'em
	)	as newMatches
	
	union all
	-- rows to delete are new matches that no longer fit the seekers profile
	Select SBMatch.SBScholarshipID, sbmatch.SBSeekerID,  
		SBMatch.SeekerPreferredCriteriaCount,
		sbmatch.SeekerMinimumCriteriaCount,
		SBMatch.ScholarshipPreferredCriteriaCount,
		sbmatch.ScholarshipMinimumCriteriaCount,
		1 as deletable,
		0 as [rank]
	
	from SBMatch 
	where MatchStatus not in ('Submitted', 'Saved')
			and AddedViaMatch = 1
			and SBSeekerId = @seekerid 
			and SBScholarshipId 
			not in	(
							select SBScholarshipId from SBMatchBools  where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchRanges where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchInEnum where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchInList where SBSeekerId=@seekerid 
					)
	
) AS nm
ON sbm.SBScholarshipId = nm.SBScholarshipId and sbm.SBSeekerId = nm.SBSeekerId
WHEN MATCHED and nm.deletable = 0
	THEN UPDATE SET sbm.isDeleted = 0, 
					sbm.SeekerPreferredCriteriaCount = nm.SeekerPrefCrit,
					sbm.SeekerMinimumCriteriaCount = nm.seekerMinCrit,
					sbm.ScholarshipPreferredCriteriaCount = nm.scholarshipPrefCrit,
					sbm.ScholarshipMinimumCriteriaCount = nm.scholarshipMinCrit,
					sbm.LastUpdateDate = getdate(),
					sbm.LastUpdateBy = @updateuserid,
					sbm.AddedViaMatch = 1,
					sbm.[rank] = nm.[rank]
WHEN MATCHED and nm.deletable = 1 THEN Delete
WHEN NOT MATCHED  and nm.deletable = 0
	THEN INSERT(SBSeekerId, SBScholarshipId, MatchStatus, IsDeleted, SeekerPreferredCriteriaCount, SeekerMinimumCriteriaCount, ScholarshipPreferredCriteriaCount, ScholarshipMinimumCriteriaCount, LastUpdateBy, LastUpdateDate, SBApplicationId, AddedViaMatch, [Rank])
		VALUES(nm.SBSeekerId, nm.SBScholarshipId, 'New', 0, nm.SeekerPrefCrit, nm.seekerMinCrit, nm.scholarshipPrefCrit, nm.scholarshipMinCrit, 1, getdate(), null, @updateuserid, nm.[rank]);
END
";
		#endregion

		#region Scholarship Match Stored Proc

		public const string SCHOLARSHIP_MATCH_PROC = @"
-- =============================================
-- Author:		Dave Richard
-- Create date: 11 Jan 2010
-- Description:	Redo the match rows for a given activated scholarship		

-- =============================================
CREATE PROCEDURE dbo.BuildScholarshipMatches
	@ScholarshipId int,
	@UpdateUserId int 
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

MERGE SBMatch AS sbm
USING
(
	select 
		SBScholarshipID,
		SBSeekerId,
		seekerPrefCrit,
		seekerMinCrit,
		scholarshipPrefCrit,
		scholarshipMinCrit,
		deletable,
		[rank] = dbo.ComputeMatchRank(seekerPrefCrit,
									seekerMinCrit,
									scholarshipPrefCrit,
									scholarshipMinCrit)

	from

	(
		select 
			pv.SBScholarshipID,
			pv.SBSeekerId,
			pv.[1] as seekerPrefCrit,
			pv.[2] as seekerMinCrit,
			cc.prefCrit as scholarshipPrefCrit,
			cc.minCrit as scholarshipMinCrit,
			0 as deletable
		from
		(
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchBools where SBScholarshipId=@scholarshipid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex  from SBMatchRanges where SBScholarshipId=@scholarshipid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex  from SBMatchInEnum where SBScholarshipId=@scholarshipid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex  from SBMatchInList where SBScholarshipId=@scholarshipid
		) 
		as sourcetable
		PIVOT
		(
			COUNT(SBUsageTypeIndex) FOR SBUsageTypeIndex IN ([1], [2])
		)  
		AS pv
		inner join SBScholarshipCriteriaCounts as cc on pv.SBScholarshipID = cc.SBScholarshipID
		where pv.SBSeekerId is not null
		  and not exists (select * from SBMatchExclusion me
							where me.SBSeekerId = pv.SBSeekerId
							  and me.SBScholarshipId = pv.SBScholarshipId)
		  and not (pv.[2] = 0 and cc.minCrit > 0) -- if a seeker doesn't meet any minimum requirements then skip 'em
	)	as newMatches
	
	union all
	-- rows to delete are new matches that no longer fit the seekers profile
	Select SBMatch.SBScholarshipID, sbmatch.SBSeekerID,  
		SBMatch.SeekerPreferredCriteriaCount,
		sbmatch.SeekerMinimumCriteriaCount,
		SBMatch.ScholarshipPreferredCriteriaCount,
		sbmatch.ScholarshipMinimumCriteriaCount,
		1 as deletable,
		0 as [rank]
	
	from SBMatch 
	where MatchStatus not in ('Submitted', 'Saved')
			and AddedViaMatch = 1
			and SBScholarshipId = @ScholarshipId
			and SBSeekerId 
			not in	(
							select SBSeekerId from SBMatchBools  where SBScholarshipId=@ScholarshipId
							union all
							select SBSeekerId from SBMatchRanges where SBScholarshipId=@ScholarshipId
							union all
							select SBSeekerId from SBMatchInEnum where SBScholarshipId=@ScholarshipId
							union all
							select SBSeekerId from SBMatchInList where SBScholarshipId=@ScholarshipId 
					)
	
) AS nm
ON sbm.SBScholarshipId = nm.SBScholarshipId and sbm.SBSeekerId = nm.SBSeekerId
WHEN MATCHED and nm.deletable = 0
	THEN UPDATE SET sbm.isDeleted = 0, 
					sbm.SeekerPreferredCriteriaCount = nm.SeekerPrefCrit,
					sbm.SeekerMinimumCriteriaCount = nm.seekerMinCrit,
					sbm.ScholarshipPreferredCriteriaCount = nm.scholarshipPrefCrit,
					sbm.ScholarshipMinimumCriteriaCount = nm.scholarshipMinCrit,
					sbm.LastUpdateDate = getdate(),
					sbm.LastUpdateBy = @updateuserid,
					sbm.AddedViaMatch = 1,
					sbm.[rank] = nm.[rank]
WHEN MATCHED and nm.deletable = 1 THEN Delete
WHEN NOT MATCHED and nm.deletable = 0 
	THEN INSERT(SBSeekerId, SBScholarshipId, MatchStatus, IsDeleted, SeekerPreferredCriteriaCount, SeekerMinimumCriteriaCount, ScholarshipPreferredCriteriaCount, ScholarshipMinimumCriteriaCount, LastUpdateBy, LastUpdateDate, SBApplicationId, AddedViaMatch, [Rank])
		VALUES(nm.SBSeekerId, nm.SBScholarshipId, 'New', 0, nm.SeekerPrefCrit, nm.seekerMinCrit, nm.scholarshipPrefCrit, nm.scholarshipMinCrit, 1, getdate(), null, @updateuserid, nm.[rank]);
END
";
		#endregion

	}
}