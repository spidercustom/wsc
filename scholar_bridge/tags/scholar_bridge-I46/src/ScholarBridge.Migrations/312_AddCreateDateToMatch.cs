﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(312)]
	public class AddCreateDateToMatch : Migration
    {
        private const string TABLE_NAME = "SBMatch";
        private const string COLUMN_NAME = "CreateDate";

        public override void Up()
        {
			Database.AddColumn(TABLE_NAME, COLUMN_NAME, DbType.DateTime);
        	SetInitialCreateDate();
        	DropStoredProcs();
        	UpdateSeekerMatchBuilderSP();
        }

        public override void Down()
        {
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
			DropStoredProcs();
			Database.ExecuteNonQuery(ChangeMatchStoredProcs.SEEKER_MATCH_PROC);
			Database.ExecuteNonQuery(ChangeMatchStoredProcs.SCHOLARSHIP_MATCH_PROC);
		}

		private void DropStoredProcs()
		{
			Database.ExecuteNonQuery("if exists (select * from sys.procedures where name = 'BuildScholarshipMatches') drop procedure dbo.BuildScholarshipMatches");
			Database.ExecuteNonQuery("if exists (select * from sys.procedures where name = 'BuildSeekerMatches') drop procedure dbo.BuildSeekerMatches");
		}

		/// <summary>
        /// set all Application to 'Internal' type for now.
		/// </summary>
		private void SetInitialCreateDate()
		{
			Database.ExecuteNonQuery(@"
update SBMatch
set CreateDate = dateadd(day, -1, LastUpdateDate)
");
		}

		#region update SeekerMatchBuilder
        	private void UpdateSeekerMatchBuilderSP()
        	{
        		Database.ExecuteNonQuery(SEEKER_MATCH_PROC);
        	}
			public const string SEEKER_MATCH_PROC = @"

-- =============================================
-- Author:		Dave Richard
-- Create date: 7 Jan 2010
-- Description:	Redo the match rows for a given seeker
-- =============================================
Create PROCEDURE [dbo].[BuildSeekerMatches]
	@SeekerId int,
	@UpdateUserId int 
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

MERGE SBMatch AS sbm
USING
(
	select 
		SBScholarshipID,
		SBSeekerId,
		seekerPrefCrit,
		seekerMinCrit,
		scholarshipPrefCrit,
		scholarshipMinCrit,
		deletable,
		[rank] = dbo.ComputeMatchRank(seekerPrefCrit,
									seekerMinCrit,
									scholarshipPrefCrit,
									scholarshipMinCrit)

	from

	(
		select 
			pv.SBScholarshipID,
			pv.SBSeekerId,
			pv.[1] as seekerPrefCrit,
			pv.[2] as seekerMinCrit,
			cc.prefCrit as scholarshipPrefCrit,
			cc.minCrit as scholarshipMinCrit,
			0 as deletable
		from
		(
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchBools  where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchRanges where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchInEnum where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchInList where SBSeekerId=@seekerid 
				group by SBScholarshipId, SBSeekerId, SBUsageTypeIndex, criterion
		) as sourcetable
		PIVOT
		(
			COUNT(SBUsageTypeIndex) FOR SBUsageTypeIndex IN ([1], [2])
		)  AS pv
		
		inner join SBScholarshipCriteriaCounts as cc
			on pv.SBScholarshipID = cc.SBScholarshipID
		where pv.SBSeekerId is not null
		  and not exists (select * from SBMatchExclusion me
							where me.SBSeekerId = pv.SBSeekerId
							and me.SBScholarshipId = pv.SBScholarshipId)
		  and not (pv.[2] = 0 and cc.minCrit > 0) -- if a seeker doesn't meet any minimum requirements then skip 'em
	)	as newMatches
	
	union all
	-- rows to delete are new matches that no longer fit the seekers profile
	Select SBMatch.SBScholarshipID, sbmatch.SBSeekerID,  
		SBMatch.SeekerPreferredCriteriaCount,
		sbmatch.SeekerMinimumCriteriaCount,
		SBMatch.ScholarshipPreferredCriteriaCount,
		sbmatch.ScholarshipMinimumCriteriaCount,
		1 as deletable,
		0 as [rank]
	
	from SBMatch 
	where MatchStatus not in ('Submitted', 'Saved')
			and AddedViaMatch = 1
			and SBSeekerId = @seekerid 
			and SBScholarshipId 
			not in	(
							select SBScholarshipId from SBMatchBools  where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchRanges where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchInEnum where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchInList where SBSeekerId=@seekerid 
					)
	
) AS nm
ON sbm.SBScholarshipId = nm.SBScholarshipId and sbm.SBSeekerId = nm.SBSeekerId
WHEN MATCHED and nm.deletable = 0
	THEN UPDATE SET sbm.isDeleted = 0, 
					sbm.SeekerPreferredCriteriaCount = nm.SeekerPrefCrit,
					sbm.SeekerMinimumCriteriaCount = nm.seekerMinCrit,
					sbm.ScholarshipPreferredCriteriaCount = nm.scholarshipPrefCrit,
					sbm.ScholarshipMinimumCriteriaCount = nm.scholarshipMinCrit,
					sbm.LastUpdateDate = getdate(),
					sbm.LastUpdateBy = @updateuserid,
					sbm.AddedViaMatch = 1,
					sbm.[rank] = nm.[rank]
WHEN MATCHED and nm.deletable = 1 THEN Delete
WHEN NOT MATCHED  and nm.deletable = 0
	THEN INSERT(SBSeekerId, SBScholarshipId, MatchStatus, 
				IsDeleted, SeekerPreferredCriteriaCount, 
				SeekerMinimumCriteriaCount, ScholarshipPreferredCriteriaCount, 
				ScholarshipMinimumCriteriaCount, LastUpdateBy, LastUpdateDate, 
				SBApplicationId, AddedViaMatch, [Rank], CreateDate)
		VALUES(nm.SBSeekerId, nm.SBScholarshipId, 'New', 
				0, nm.SeekerPrefCrit, 
				nm.seekerMinCrit, nm.scholarshipPrefCrit, 
				nm.scholarshipMinCrit, 1, getdate(), 
				null, @updateuserid, nm.[rank], GetDate());
END


";
		
		#endregion

	}
}