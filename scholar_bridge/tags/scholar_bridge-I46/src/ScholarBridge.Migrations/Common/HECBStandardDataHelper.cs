using Migrator.Framework;

namespace ScholarBridge.Migrations.Common
{
    // The Table and ID changed, so this code isn't going to work below a certain
    // migration. We might need to look at have a pre/post version of this?
    public class HECBStandardDataHelper
    {
        public static int GetAdminUserId(ITransformationProvider database)
        {
            int result= GetUserId(database, "admin");
            if (result<1)
              result= GetUserId(database, "admin@scholarbridge.com");
            return result;
        }

        public static int GetUserId(ITransformationProvider database, string userName)
        {
            var result = database.SelectScalar("SBUserId", "SBUser", string.Format("Username='{0}'", userName));
            return result == null ? 0 : (int) result;
                             
        }

        public static int GetRoleId(ITransformationProvider database, string roleName)
        {
            return (int)database.SelectScalar("SBUserId", "SBUser", string.Format("Name='{0}'", roleName));
        }
    }
}