﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(304)]
	public class ChangeMatchStoredProcs : Migration
    {
        public override void Up()
        {
			DropStoredProcs();
			Database.ExecuteNonQuery(SEEKER_MATCH_PROC);
			Database.ExecuteNonQuery(SCHOLARSHIP_MATCH_PROC);
		}

		public override void Down()
		{
			DropStoredProcs();
			Database.ExecuteNonQuery(AddMatchStoredProcs.SEEKER_MATCH_PROC);
			Database.ExecuteNonQuery(AddMatchStoredProcs.SCHOLARSHIP_MATCH_PROC);

		}

		private void DropStoredProcs()
		{
			Database.ExecuteNonQuery("if exists (select * from sys.procedures where name = 'BuildSeekerMatches') drop procedure dbo.BuildSeekerMatches");
			Database.ExecuteNonQuery("if exists (select * from sys.procedures where name = 'BuildScholarshipMatches') drop procedure dbo.BuildScholarshipMatches");
		}

		#region Seeker Match Stored Proc

		public const string SEEKER_MATCH_PROC = @"
-- =============================================
-- Author:		Dave Richard
-- Create date: 7 Jan 2010
-- Description:	Redo the match rows for a given seeker
-- =============================================
CREATE PROCEDURE [dbo].[BuildSeekerMatches]
	@SeekerId int,
	@UpdateUserId int 
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

MERGE SBMatch AS sbm
USING
(
	select 
		SBScholarshipID,
		SBSeekerId,
		seekerPrefCrit,
		seekerMinCrit,
		scholarshipPrefCrit,
		scholarshipMinCrit,
		deletable,
		AddedViaMatch,
		[rank] = dbo.ComputeMatchRank(seekerPrefCrit,
									seekerMinCrit,
									scholarshipPrefCrit,
									scholarshipMinCrit)

	from

	(
		select 
			pv.SBScholarshipID,
			pv.SBSeekerId,
			pv.[1] as seekerPrefCrit,
			pv.[2] as seekerMinCrit,
			cc.prefCrit as scholarshipPrefCrit,
			cc.minCrit as scholarshipMinCrit,
			
			case -- delete previous matches that no longer match (except for saved/submitted matches)
				when exists (select * from SBMatchExclusion me
							where me.SBSeekerId = pv.SBSeekerId
							and me.SBScholarshipId = pv.SBScholarshipId) and
							not exists (select * from sbmatch m2 where m2.SBSeekerId = pv.SBSeekerId and m2.SBScholarshipID = pv.SBScholarshipId and m2.MatchStatus in ('Submitted', 'Saved'))
							then 1
				when pv.[2] = 0 and cc.minCrit > 0 and
							not exists (select * from sbmatch m2 where m2.SBSeekerId = pv.SBSeekerId and m2.SBScholarshipID = pv.SBScholarshipId and m2.MatchStatus in ('Submitted', 'Saved'))
							then 1 
				else 0 end as deletable,
				
			case -- set the AddedViaMatch flag to true if not on the exclusion table and not a no minimum match situation 
				when exists (select * from SBMatchExclusion me
							where me.SBSeekerId = pv.SBSeekerId
							and me.SBScholarshipId = pv.SBScholarshipId) then 0
				when pv.[2] = 0 and cc.minCrit > 0 
							then 0 
				else 1 end as AddedViaMatch
		from
		(
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchBools  where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchRanges where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchInEnum where SBSeekerId=@seekerid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchInList where SBSeekerId=@seekerid 
				group by SBScholarshipId, SBSeekerId, SBUsageTypeIndex, criterion
		) as sourcetable
		
		PIVOT
		(
			COUNT(SBUsageTypeIndex) FOR SBUsageTypeIndex IN ([1], [2])
		)  AS pv
		
		inner join SBScholarshipCriteriaCounts as cc
			on pv.SBScholarshipID = cc.SBScholarshipID
		where pv.SBSeekerId is not null
	)	as newMatches
	
	union all
	-- rows to delete are new matches that no longer fit the seekers profile
	Select SBMatch.SBScholarshipID, sbmatch.SBSeekerID,  
		SBMatch.SeekerPreferredCriteriaCount,
		sbmatch.SeekerMinimumCriteriaCount,
		SBMatch.ScholarshipPreferredCriteriaCount,
		sbmatch.ScholarshipMinimumCriteriaCount,
		1 as deletable,
		0 as AddedViaMatch,
		0 as [rank]
	
	from SBMatch 
	where MatchStatus not in ('Submitted', 'Saved')
			and AddedViaMatch = 1
			and SBSeekerId = @seekerid 
			and SBScholarshipId 
			not in	(
							select SBScholarshipId from SBMatchBools  where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchRanges where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchInEnum where SBSeekerId=@seekerid
							union all
							select SBScholarshipId from SBMatchInList where SBSeekerId=@seekerid 
					)
	
) AS nm
ON sbm.SBScholarshipId = nm.SBScholarshipId and sbm.SBSeekerId = nm.SBSeekerId
WHEN MATCHED and nm.deletable = 0
	THEN UPDATE SET sbm.isDeleted = 0, 
					sbm.SeekerPreferredCriteriaCount = nm.SeekerPrefCrit,
					sbm.SeekerMinimumCriteriaCount = nm.seekerMinCrit,
					sbm.ScholarshipPreferredCriteriaCount = nm.scholarshipPrefCrit,
					sbm.ScholarshipMinimumCriteriaCount = nm.scholarshipMinCrit,
					sbm.LastUpdateDate = getdate(),
					sbm.LastUpdateBy = @updateuserid,
					sbm.AddedViaMatch = nm.AddedViaMatch,
					sbm.[rank] = nm.[rank]
WHEN MATCHED and nm.deletable = 1 
	THEN Delete
WHEN NOT MATCHED  and nm.deletable = 0
	THEN INSERT(SBSeekerId, SBScholarshipId, MatchStatus, 
					IsDeleted, SeekerPreferredCriteriaCount, 
					SeekerMinimumCriteriaCount, ScholarshipPreferredCriteriaCount, 
					ScholarshipMinimumCriteriaCount, LastUpdateBy, LastUpdateDate, 
					SBApplicationId, AddedViaMatch, [Rank])
		VALUES(nm.SBSeekerId, nm.SBScholarshipId, 'New', 0, nm.SeekerPrefCrit, 
					nm.seekerMinCrit, nm.scholarshipPrefCrit, nm.scholarshipMinCrit, 
					@UpdateUserId, getdate(), null, nm.AddedViaMatch, nm.[rank]);
END
";
		#endregion

		#region Scholarship Match Stored Proc

		public const string SCHOLARSHIP_MATCH_PROC = @"
-- =============================================
-- Author:		Dave Richard
-- Create date: 11 Jan 2010
-- Description:	Redo the match rows for a given activated scholarship		

-- =============================================
CREATE PROCEDURE [dbo].[BuildScholarshipMatches]
	@ScholarshipId int,
	@UpdateUserId int 
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

MERGE SBMatch AS sbm
USING
(
	select 
		SBScholarshipID,
		SBSeekerId,
		seekerPrefCrit,
		seekerMinCrit,
		scholarshipPrefCrit,
		scholarshipMinCrit,
		deletable,
		AddedViaMatch,
		[rank] = dbo.ComputeMatchRank(seekerPrefCrit,
									seekerMinCrit,
									scholarshipPrefCrit,
									scholarshipMinCrit)

	from

	(
		select 
			pv.SBScholarshipID,
			pv.SBSeekerId,
			pv.[1] as seekerPrefCrit,
			pv.[2] as seekerMinCrit,
			cc.prefCrit as scholarshipPrefCrit,
			cc.minCrit as scholarshipMinCrit,
			
			case -- delete previous matches that no longer match (except for saved/submitted matches)
				when exists (select * from SBMatchExclusion me
							where me.SBSeekerId = pv.SBSeekerId
							and me.SBScholarshipId = pv.SBScholarshipId) and
							not exists (select * from sbmatch m2 where m2.SBSeekerId = pv.SBSeekerId and m2.SBScholarshipID = pv.SBScholarshipId and m2.MatchStatus in ('Submitted', 'Saved'))
							then 1
				when pv.[2] = 0 and cc.minCrit > 0 and
							not exists (select * from sbmatch m2 where m2.SBSeekerId = pv.SBSeekerId and m2.SBScholarshipID = pv.SBScholarshipId and m2.MatchStatus in ('Submitted', 'Saved'))
							then 1 
				else 0 end as deletable,
				
			case -- set the AddedViaMatch flag to true if not on the exclusion table and not a no minimum match situation 
				when exists (select * from SBMatchExclusion me
							where me.SBSeekerId = pv.SBSeekerId
							and me.SBScholarshipId = pv.SBScholarshipId) then 0
				when pv.[2] = 0 and cc.minCrit > 0 
							then 0 
				else 1 end as AddedViaMatch
		from
		(
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex from SBMatchBools where SBScholarshipId=@scholarshipid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex  from SBMatchRanges where SBScholarshipId=@scholarshipid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex  from SBMatchInEnum where SBScholarshipId=@scholarshipid
			union all
			select SBScholarshipId, SBSeekerId, SBUsageTypeIndex  from SBMatchInList where SBScholarshipId=@scholarshipid
		) 
		as sourcetable
		PIVOT
		(
			COUNT(SBUsageTypeIndex) FOR SBUsageTypeIndex IN ([1], [2])
		)  
		AS pv
		inner join SBScholarshipCriteriaCounts as cc on pv.SBScholarshipID = cc.SBScholarshipID
		where pv.SBSeekerId is not null
	)	as newMatches
	
	union all
	-- rows to delete are new matches that no longer fit the seekers profile
	Select SBMatch.SBScholarshipID, sbmatch.SBSeekerID,  
		SBMatch.SeekerPreferredCriteriaCount,
		sbmatch.SeekerMinimumCriteriaCount,
		SBMatch.ScholarshipPreferredCriteriaCount,
		sbmatch.ScholarshipMinimumCriteriaCount,
		1 as deletable,
		0 as AddedViaMatch,
		0 as [rank]
	
	from SBMatch 
	where MatchStatus not in ('Submitted', 'Saved')
			and AddedViaMatch = 1
			and SBScholarshipId = @ScholarshipId
			and SBSeekerId 
			not in	(
							select SBSeekerId from SBMatchBools  where SBScholarshipId=@ScholarshipId
							union all
							select SBSeekerId from SBMatchRanges where SBScholarshipId=@ScholarshipId
							union all
							select SBSeekerId from SBMatchInEnum where SBScholarshipId=@ScholarshipId
							union all
							select SBSeekerId from SBMatchInList where SBScholarshipId=@ScholarshipId 
					)
	
) AS nm
ON sbm.SBScholarshipId = nm.SBScholarshipId and sbm.SBSeekerId = nm.SBSeekerId
WHEN MATCHED and nm.deletable = 0
	THEN UPDATE SET sbm.isDeleted = 0, 
					sbm.SeekerPreferredCriteriaCount = nm.SeekerPrefCrit,
					sbm.SeekerMinimumCriteriaCount = nm.seekerMinCrit,
					sbm.ScholarshipPreferredCriteriaCount = nm.scholarshipPrefCrit,
					sbm.ScholarshipMinimumCriteriaCount = nm.scholarshipMinCrit,
					sbm.LastUpdateDate = getdate(),
					sbm.LastUpdateBy = @updateuserid,
					sbm.AddedViaMatch = nm.AddedViaMatch,
					sbm.[rank] = nm.[rank]
WHEN MATCHED and nm.deletable = 1 
	THEN Delete
WHEN NOT MATCHED and nm.deletable = 0 
	THEN INSERT(SBSeekerId, SBScholarshipId, MatchStatus, IsDeleted, 
				SeekerPreferredCriteriaCount, SeekerMinimumCriteriaCount, 
				ScholarshipPreferredCriteriaCount, ScholarshipMinimumCriteriaCount, 
				LastUpdateBy, LastUpdateDate, SBApplicationId, AddedViaMatch, [Rank])
		VALUES(nm.SBSeekerId, nm.SBScholarshipId, 'New', 0, 
				nm.SeekerPrefCrit, nm.seekerMinCrit, 
				nm.scholarshipPrefCrit, nm.scholarshipMinCrit, 
				@updateuserid, getdate(), null, nm.AddedViaMatch, nm.[rank]);
END
";
		#endregion

	}
}