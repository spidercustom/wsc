﻿using System;
using System.Data;
using Migrator.Framework;
namespace ScholarBridge.Migrations
{
    [Migration(236)]
	public class AddStatusDateToOrganization : Migration
	{
        private const string TABLE_NAME = "SBOrganization";

        public readonly string[]  COLUMNS
			= { "ApprovalStatusDate" };

        public override void Up()
        {

            Database.AddColumn(TABLE_NAME, COLUMNS[0], DbType.DateTime, ColumnProperty.Null);

        	Database.ExecuteNonQuery("update " + TABLE_NAME + " set " + COLUMNS[0] + " = '" + DateTime.Now + "'");

			Database.ChangeColumn(TABLE_NAME, new Column(COLUMNS[0], DbType.DateTime, ColumnProperty.NotNull));
		}

		public override void Down()
		{
			Database.RemoveColumn(TABLE_NAME, COLUMNS[0]);
		}
	}
}
