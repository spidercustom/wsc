using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    class BuildApplicationAction : OpenUrlAction
    {
        private const string BUILD_APPLICATION_URL_TEMPLATE = "~/Seeker/BuildApplication/Default.aspx?sid={0}";
        private const string ONLINE_APPLICATION_NOT_APPROVED_URL_TEMPLATE = "~/Seeker/Matches/OnlineApplicationAlert.aspx";
        public BuildApplicationAction() : base("Apply") { }

        protected override string ConstructUrl(Match match)
        {
            string url = BUILD_APPLICATION_URL_TEMPLATE.Build(match.Scholarship.Id);

            if (match.Scholarship.IsUseOnlineApplication)
            {
                if (match.Scholarship.IsOnlineApplicationApproved)
                {
                    url = match.Scholarship.OnlineApplicationUrl;
                }
                else
                {
                    IsShowInPopup = true;
                    url = ONLINE_APPLICATION_NOT_APPROVED_URL_TEMPLATE;
                }
            }

       
            return url;
        }
    }
}