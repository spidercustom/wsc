﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Web.Security;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;
using Spider.Common.Extensions.System.Collections;
using Spring.Context.Support;

namespace ScholarBridge.Web.Security
{
    public class SpiderRoleProvider : RoleProvider
    {
        private const string APPLICATION_NAME = "/";

        private string applicationName;

        public SpiderRoleProvider()
        {
            applicationName = APPLICATION_NAME;
        }

        protected virtual IRoleDAL Service
        {
            get { return (IRoleDAL)ContextRegistry.GetContext().GetObject("RoleDAL"); }
        }

        protected virtual IUserDAL UserService
        {
            get { return (IUserDAL)ContextRegistry.GetContext().GetObject("UserDAL"); }
        }

        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            IList<Role> roles = GetRolesForUsername(username);
            return roles.Any(r => r.Name.Equals(roleName));
        }

        public override string[] GetRolesForUser(string username)
        {
            IList<Role> roles = GetRolesForUsername(username);
            return ConvertToStringArray(roles, r => r.Name);
        }

        private static string[] ConvertToStringArray<T>(ICollection<T> objects, Func<T, string> method)
        {
            var result = new string[objects.Count];
            int counter = 0;
            foreach (T t in objects)
            {
                result[counter++] = method.Invoke(t);
            }
            return result;
        }

        public override void CreateRole(string roleName)
        {
            // Make sure we are not attempting to insert an existing role.
          
            IList<Role> roles = Service.FindAll(roleName);
            if (roles.Count > 0)
            {
                throw new ProviderException(string.Format("'{0}' Role already exists.", roleName));
            }
            
            try
            {
                var role = new Role {Name = roleName};
                Service.Insert(role);
            }
            catch (Exception ex)
            {
                throw new ProviderException(string.Format("'{0}' Unable to create role.", ex));
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            // Assume we are unable to perform the operation.
            bool result = false;
            
            IList<User> users = UserService.FindByRoleName(roleName);
            if (throwOnPopulatedRole)
            {
                if (users.Count > 0)
                {
                   throw new ProviderException(string.Format("'{0}' role is not empty", roleName));
                  
                }
            }

            // delete role from found users
            users.ForAllDo(u => DeleteRoleFromUser(roleName, u));

            try
            {
                // FindById the Role information.
                var role = Service.FindByName(roleName);
                if (null != role)
                {
                    // Delete the role record.
                    Service.Delete(role);
                    result = true;
                }
            }

           catch (Exception ex)
            {
                throw new ProviderException("Unable to delete user.", ex);
            }

            return result;
        }

        private void DeleteRoleFromUser(string roleName, User user)
        {
            user.Roles.Remove(FindRole(user, roleName));
            UserService.Update(user);
        }

        public override bool RoleExists(string roleName)
        {
            return Service.FindByName(roleName) != null;
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            usernames.ForAllDo(uname => AddRoleToUser(uname, roleNames));
        }

        private void AddRoleToUser(string s, IEnumerable<string> roleNames)
        {
            User user = UserService.FindByUsername(s);
            roleNames.ForAllDo(rn => user.Roles.Add(Service.FindByName(rn)));
            UserService.Update(user);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            IUserDAL userService = UserService;
            foreach (string s in usernames)
            {
                User user = userService.FindByUsername(s);
                roleNames.ForAllDo(rn => user.Roles.Remove(FindRole(user, rn)));
                userService.Update(user);
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return Convert(UserService.FindByRoleName(roleName));
        }

        public override string[] GetAllRoles()
        {
            return Convert(Service.FindAll(null));
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return Convert(UserService.FindByRoleNameAndUsernameWildcard(roleName, usernameToMatch));
        }

        #region private methods

        private IList<Role> GetRolesForUsername(string username)
        {
        	string[] names = username.Split('!');
			User user = UserService.FindByUsername(names[0]);
            if (user == null)
            {
                throw new ArgumentException("No user found with username: " + username);
            }
            return user.Roles;
        }

        private static string[] Convert(ICollection<Role> roles)
        {
            return ConvertToStringArray(roles, r => r.Name);
        }

        private static string[] Convert(ICollection<User> users)
        {
            return ConvertToStringArray(users, r => r.Username);
        }

        private static Role FindRole(User user, string roleName)
        {
            return user.Roles.First(r => r.Name.Equals(roleName));
        }

        #endregion
    }
}