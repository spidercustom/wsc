using System;
using System.Data;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Type;
using NHibernate.UserTypes;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.NHibernate
{
    public class AcademicYearType : ICompositeUserType
    {
        public object GetPropertyValue(object component, int property)
        {
            // only one property, so don't need to check that
            var academicYear = (AcademicYear) component;
            return academicYear.Year;
        }

        public void SetPropertyValue(object component, int property, object value)
        {
            var academicYear = (AcademicYear) component;
            academicYear.Year = (int) value;
        }

        public new bool Equals(object x, object y)
        {
            if (x == y) return true;
            if (x == null || y == null) return false;
            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            if (null == x)
                return -1;
            return x.GetHashCode();
        }

        public object NullSafeGet(IDataReader dr, string[] names, ISessionImplementor session, object owner)
        {
            if (dr == null)
            {
                return null;
            }

            var yearColumn = names[0];
            var year = (int) NHibernateUtil.Int32.NullSafeGet(dr, yearColumn, session, owner);
            return new AcademicYear(year);
        }

        public void NullSafeSet(IDbCommand cmd, object value, int index, ISessionImplementor session)
        {
            if (value == null)
                return;

            double year = ((AcademicYear) value).Year;
            NHibernateUtil.Int32.NullSafeSet(cmd, year, index, session);
        }

        public object DeepCopy(object value)
        {
            return new AcademicYear(((AcademicYear) value).Year);
        }

        public object Disassemble(object value, ISessionImplementor session)
        {
            return DeepCopy(value);
        }

        public object Assemble(object cached, ISessionImplementor session, object owner)
        {
            return DeepCopy(cached);
        }

        public object Replace(object original, object target, ISessionImplementor session, object owner)
        {
            return DeepCopy(original);
        }

        public string[] PropertyNames
        {
            get { return new[] {"Year"}; }
        }

        public IType[] PropertyTypes
        {
            get { return new[] {NHibernateUtil.Int32}; }
        }

        public Type ReturnedClass
        {
            get { return typeof (AcademicYear); }
        }

        public bool IsMutable
        {
            get { return true; }
        }
    }
}