﻿using NHibernate.Criterion;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
	public class ResourceDAL : AbstractDAL<Resource>, IResourceDAL
	{
		public Resource FindById(int id)
		{
			return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Resource>();
		}


		public Resource Save(Resource resource)
		{
			return resource.Id < 1 ?
				Insert(resource) :
				Update(resource);

		}


	}
}