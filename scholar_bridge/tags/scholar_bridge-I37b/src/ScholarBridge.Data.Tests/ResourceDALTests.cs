﻿using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{

	[TestFixture]
	public class ResourceDALTests : TestBase
	{

		public ResourceDAL ResourceDAL { get; set; }

		public UserDAL UserDAL { get; set; }

		private User user;
		protected override void OnSetUpInTransaction()
		{
			user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
		}

		[Test]
		public void can_insert_resource()
		{
			var resource = InsertResource(ResourceDAL, user);

			Assert.IsNotNull(resource);
			Assert.IsTrue(resource.Id > 0);
		}

		[Test]
		public void can_delete_resources()
		{
			var resource = InsertResource(ResourceDAL, user);
			ResourceDAL.Delete(resource);
			var found = ResourceDAL.FindById(resource.Id);
			Assert.IsNull(found);
		}

		[Test]
		public void can_find_all_resources()
		{
			var all = ResourceDAL.FindAll("DisplayOrder");
			var currentCount = all.Count;
			var a1 = InsertResource(ResourceDAL, user);
			var a2 = InsertResource(ResourceDAL, user);

			all = ResourceDAL.FindAll("DisplayOrder");
			Assert.IsNotNull(all);
			Assert.AreEqual(currentCount + 2, all.Count);
		}

		public static Resource InsertResource(IResourceDAL resourceDal, User user)
		{

			var resource = new Resource
			               	{
			               		Title = "test1",
			               		Description = "test test test",
			               		URL = "here@there.org",
			               		DisplayOrder = 1,
								LastUpdate = new ActivityStamp(user)
							};
			resource = resourceDal.Insert(resource);
			return resource;
		}


	}
}
