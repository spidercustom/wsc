using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(53)]
    public class FixAdminNotesOnOrganization : Migration
    {
        private const string COLUMN_NAME = "AdminNotes";

        public override void Up()
        {
            // Fix to migrator, to make this a NVARCHAR(MAX)
            Database.ChangeColumn("SBOrganization", new Column(COLUMN_NAME, DbType.String, 8000, ColumnProperty.Null));
        }

        public override void Down()
        {
        }
    }
}