﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(131)]
    public class AddSeekerSeekerHobbyRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSeekerHobbyLUT"; }
        }
    }
}
