﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(22)]
    public class AddScholarshipSupport : Migration
    {
        public const string TABLE_NAME = "SB_ScholarshipSupportRT";

        protected static readonly string[] COLUMNS = new[] { "ScholarshipId", "SupportId" };
        protected static readonly string FK_SCHOLARSHIP = string.Format("FK_{0}_Scholarship", TABLE_NAME);
        protected static readonly string FK_SUPPORT = string.Format("FK_{0}_Support", TABLE_NAME);


        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKey),
                              new Column(COLUMNS[1], DbType.Int32, ColumnProperty.PrimaryKey)
                );

            Database.AddForeignKey(FK_SCHOLARSHIP, TABLE_NAME, COLUMNS[0], AddScholarship.TABLE_NAME, "Id");
            Database.AddForeignKey(FK_SUPPORT, TABLE_NAME, COLUMNS[1], AddSupport.TABLE_NAME, "Id");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_SCHOLARSHIP);
            Database.RemoveForeignKey(TABLE_NAME, FK_SUPPORT);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}