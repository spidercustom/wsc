using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(190)]
    public class AddApplicationNeedGapRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationNeedGapRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBNeedGapLUT"; }
        }
    }
}