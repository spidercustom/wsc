using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(178)]
    public class AddApplicationSportRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationSportRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSportLUT"; }
        }
    }
}