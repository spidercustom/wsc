﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(209)]
	public class AddIncludeWithApplications : Migration
	{
		public const string TABLE_NAME = "SBAttachment";
		public const string COLUMN_NAME = "IncludeWithApplications";

		public override void Up()
		{
			Database.AddColumn(TABLE_NAME, new Column(COLUMN_NAME, DbType.Boolean, ColumnProperty.NotNull, "'false'"));
		}

		public override void Down()
		{
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
		}
	}
}