﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipTitleStripe : UserControl
    {

        protected override void OnPreRender(EventArgs e)
        {
            if (HideInPrintView && Page.IsInPrintView())
                Visible = false;
            var showTitle = !string.IsNullOrEmpty(TitleControl.Text);
            if (Page.IsInPrintView())
                showTitle &= ShowTitleInPrintView;
            TitleContainer.Visible = showTitle;
            base.OnPreRender(e);
        }

        public void UpdateView(Scholarship scholarship)
        {
            if (scholarship == null) throw new ArgumentNullException("scholarship");

            TitleControl.Text = scholarship.DisplayNameWithOutDonor;
            AdditionalInfoControl.Text = string.Format(" ({0})", scholarship.GetStatusText());
        }

        public bool HasPrintView
        {
            get { return PrintViewControl.Visible; }
            set { PrintViewControl.Visible = value; }
        }

        public bool ShowTitleInPrintView { get; set;}
        public bool HideInPrintView { get; set; }
    }
}
