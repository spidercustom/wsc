using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Business.Messaging;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class MailTemplateParamsTests
    {

        [Test]
        public void can_merge_variables_into_params()
        {
            var mtp = new MailTemplateParams();
            Assert.That(mtp.MergeVariables.Count, Is.EqualTo(0));

            mtp.MergeVariablesInto(new Dictionary<string, string>{{"Foo", "1"}, {"Bar", "2"}});
            Assert.That(mtp.MergeVariables.Count, Is.EqualTo(2));
        }
    }
}