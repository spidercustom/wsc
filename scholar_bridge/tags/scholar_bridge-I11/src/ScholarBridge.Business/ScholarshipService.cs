﻿using System;
using System.Linq;
using System.Collections.Generic;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class ScholarshipService : IScholarshipService
    {
        private const int MAXIMUM_COPY_NAME_ALLOWED = 10;
        private const string COPY_NAME_TEMPLATE = "Copy ({0}) of {1}";

        public IScholarshipDAL ScholarshipDAL { get; set; }
        public IMatchService MatchService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }
        public IMessagingService MessagingService { get; set; }
        public IRoleDAL RoleService { get; set; }
        public IMailerService MailerService { get; set; }
        public string AttachmentsDirectory { get; set; }

        public Scholarship GetById(int id)
        {
            return ScholarshipDAL.FindById(id);
        }

        public IList<Scholarship> GetByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.FindByProvider(provider);
        }

        public Scholarship Save(Scholarship scholarship)
        {
            if (scholarship == null)
                throw new ArgumentNullException("scholarship");
            if (scholarship.Provider == null)
                throw new ArgumentNullException("scholarship.Provider");
            if (scholarship.Provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.Save(scholarship);
        }

        public void Delete(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");
            if (! scholarship.CanEdit())
                throw new ArgumentException("You can only delete an editable scholarship");

            // Rarely should there be messages sent to a deletable Scholarship, but just in case
            MessagingService.DeleteRelatedMessages(scholarship);
        	foreach (var attachment in scholarship.Attachments)
        	{
                attachment.RemoveFile(AttachmentsDirectory);
        	}
            ScholarshipDAL.Delete(scholarship);
        }

        public Scholarship ScholarshipExists(Provider provider, string name, int year)
        {
            if (null == provider)
                throw new ArgumentNullException("provider");
            if (null == name)
                throw new ArgumentNullException("name");

            return ScholarshipDAL.FindByBusinessKey(provider, name, year);
        }

        public IList<Scholarship> GetByProvider(Provider provider, ScholarshipStages stage)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.FindByProvider(provider, stage);
        }

        public IList<Scholarship> GetByProvider(Provider provider, ScholarshipStages[] stages)
        {
            return ScholarshipDAL.FindByProvider(provider, stages);
        }

        public IList<Scholarship> GetByProviderNotActivated(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.FindByProviderNotActivated(provider);
        }

        public IList<Scholarship> GetBySearchCriteria(string criteria)
        {
            criteria = criteria.ToLower();
            // FIXME: Need to do the query in the database, this loads all active scholarships which could be really big
            var query = ScholarshipDAL.FindByStage(ScholarshipStages.Activated);
            return (from s in query
                    where s.Name.ToLower().Contains(criteria) 
                   || (s.Provider ==null ? "" : s.Provider.Name.ToLower()).Contains(criteria)
                   || (s.Intermediary==null ? "":  s.Intermediary.Name.ToLower() ).Contains(criteria)  
                   || (s.Donor==null ? "": s.Donor.Name.ToLower()).Contains(criteria)
             select s).ToList();
        }

        public IList<Scholarship> GetByIntermediary(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");
            if (intermediary.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            return ScholarshipDAL.FindByIntermediary(intermediary);
        }
       
        public IList<Scholarship> GetByIntermediary(Intermediary intermediary, ScholarshipStages stage)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindByIntermediary(intermediary, stage);
        }

        public IList<Scholarship> GetByIntermediaryNotActivated(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");
            return ScholarshipDAL.FindByIntermediaryNotActivated(intermediary);
        }

        public IList<Scholarship> GetByOrganizations(Provider provider, Intermediary intermediary)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");
            return ScholarshipDAL.FindByOrganizations(provider, intermediary);
        }

        public IList<Scholarship> GetByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages stage)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindByOrganizations(provider, intermediary, stage);
        }

        public IList<Scholarship> GetByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages[] stages)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindByOrganizations(provider, intermediary, stages);
        }

        public IList<Scholarship> GetNotActivatedByOrganizations(Provider provider, Intermediary intermediary)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindNotActivatedByOrganizations(provider, intermediary);
        }

        /// <summary>
        /// Handles a null Intermediary which means the WSC Admin acts as the intermediary
        /// </summary>
        /// <param name="scholarship"></param>
        /// <returns></returns>
        public MessageAddress ActivationHandlerForScholarship(Scholarship scholarship)
        {
            if (null == scholarship.Intermediary)
            {
                return new MessageAddress { Role = RoleService.FindByName(Role.WSC_ADMIN_ROLE) };
            }

            return new MessageAddress {Organization = scholarship.Intermediary};
        }

        public void RequestActivation(Scholarship scholarship)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }
            if (!scholarship.CanSubmitForActivation())
            {
                throw new InvalidOperationException("Scholarship is either already in activation process or is already activated/rejected");
            }

            scholarship.Stage = ScholarshipStages.RequestedActivation;
            ScholarshipDAL.Update(scholarship);

            // Send message to scholarship provider that the scholarship has been approved.
            var templateParams = new MailTemplateParams();
            TemplateParametersService.RequestScholarshipActivation(scholarship, templateParams);
            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.RequestScholarshipActivation,
                              RelatedScholarship = scholarship,
                              To = ActivationHandlerForScholarship(scholarship),
                              From = new MessageAddress { Organization = scholarship.Provider },
                              LastUpdate = scholarship.LastUpdate
                           };
            MessagingService.SendMessage(msg, templateParams, true);
        }

        public void Approve(Scholarship scholarship, User approver)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }
            
            if (scholarship.IsActivated())
                return;

            if (!scholarship.IsRequestedForActivation())
            {
                throw new ArgumentException("Scholarship must be in the RequestedActivation stage.");
            }

            scholarship.Stage = ScholarshipStages.Activated;
            scholarship.ActivatedOn = DateTime.Now;
            ScholarshipDAL.Update(scholarship);
            ScholarshipDAL.Flush();

            MatchService.UpdateMatches(scholarship);

            // Send message to scholarship provider that the scholarship has been approved.
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipApproved(scholarship, templateParams);
            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.ScholarshipApproved,
                              RelatedScholarship = scholarship,
                              To = new MessageAddress { Organization = scholarship.Provider },
                              From = ActivationHandlerForScholarship(scholarship),
                              LastUpdate = new ActivityStamp(approver)
                          };
            MessagingService.SendMessage(msg, templateParams, true);
        }

        
        public void Reject(Scholarship scholarship, User approver)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }

            if (scholarship.IsRejected())
                return;
            
            if (!scholarship.IsRequestedForActivation())
            {
                throw new ArgumentException("Scholarship must be in the RequestedActivation stage.");
            }

            scholarship.Stage = ScholarshipStages.Rejected;
            ScholarshipDAL.Update(scholarship);

            // Send message to scholarship provider that the scholarship has been rejected.
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipRejected(scholarship, templateParams);
            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.ScholarshipRejected,
                              RelatedScholarship = scholarship,
                              To = new MessageAddress { Organization = scholarship.Provider },
                              From = ActivationHandlerForScholarship(scholarship),
                              LastUpdate = new ActivityStamp(approver)
                          };
            MessagingService.SendMessage(msg, templateParams, true);
        }

        public Scholarship CopyScholarship(Scholarship copyFrom)
        {
            var copiedScholarship = (Scholarship) copyFrom.Clone(AttachmentsDirectory);
            for (int copyIndex = 1; copyIndex < MAXIMUM_COPY_NAME_ALLOWED; copyIndex ++)
            {
                var newName = COPY_NAME_TEMPLATE.Build(copyIndex, copiedScholarship.Name);
                var foundScholarship = ScholarshipExists(copiedScholarship.Provider, newName, copiedScholarship.AcademicYear.Year);
                bool copyNameExists = null != foundScholarship;
                if (!copyNameExists)
                {
                    copiedScholarship.Name = newName;
                    return copiedScholarship;
                }
            }

            throw new TooManyScholarshipCopyExistsException();
        }

        public void CloseAwardPeriod(Scholarship scholarship, DateTime time)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");

            scholarship.AwardPeriodClosed = time;
            ScholarshipDAL.Update(scholarship);
        }

        public void NotifySeekersClosedSince(DateTime date)
        {
            var scholarships = ScholarshipDAL.FindAllClosedSince(date);
            foreach (var s in scholarships)
            {
                NotifySeekersOfClose(s);
            }
        }

        public void NotifySeekersOfClose(Scholarship scholarship)
        {
            // RULE: Send to all users with scholarships saved in My Scholarships

            var matches = MatchService.GetSavedMatches(scholarship);
            if (null != matches && matches.Count > 0)
            {
                var seekersToNotify = matches.Select(m => m.Seeker);

                var templateParams = new MailTemplateParams();
                TemplateParametersService.ScholarshipClosed(scholarship, templateParams);
                foreach (var seeker in seekersToNotify)
                {
                    var msg = new Message
                                  {
                                      MessageTemplate = MessageType.ScholarshipClosed,
                                      To = new MessageAddress {User = seeker.User},
                                      From = new MessageAddress {Organization = scholarship.Provider},
                                      LastUpdate = new ActivityStamp(scholarship.LastUpdate.By)
                                  };

                    MessagingService.SendMessage(msg, templateParams, seeker.User.IsRecieveEmails);
                }
            }
        }

        public void NotifySeekersScholarshipDue(int inDays)
        {
            var day = DateTime.Today.AddDays(inDays);
            var scholarships = ScholarshipDAL.FindAllDueOn(day);
            foreach (var s in scholarships)
            {
                NotifySeekersDue(s, inDays);
            }
        }

        public void NotifySeekersDue(Scholarship scholarship, int inDays)
        {
            // RULE: Send to all users with scholarships saved in My Scholarships that have not submitted an application

            var matches = MatchService.GetSavedMatches(scholarship);
            if (null != matches && matches.Count > 0)
            {
                var seekersToNotify = matches.Where(m => null == m.Application || m.Application.Stage != ApplicationStages.Submitted).Select(m => m.Seeker);

                var templateParams = new MailTemplateParams();
                TemplateParametersService.ApplicationDue(scholarship, inDays, templateParams);
                foreach (var seeker in seekersToNotify)
                {
                    var msg = new Message
                    {
                        MessageTemplate = MessageType.ApplicationDue,
                        To = new MessageAddress { User = seeker.User },
                        From = new MessageAddress { Organization = scholarship.Provider },
                        LastUpdate = new ActivityStamp(scholarship.LastUpdate.By)
                    };

                    MessagingService.SendMessage(msg, templateParams, seeker.User.IsRecieveEmails);
                }
            }
        }


        public void SendToFriend(Scholarship scholarship, string userComments,string toEmail, User sender,string domainName)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }
            if (null == scholarship)
            {
                throw new ArgumentNullException("sender", "Sender can not be null");
            }
            if (string.IsNullOrEmpty(toEmail))
            {
                throw new ArgumentNullException("toEmail",   "Recipient can not be null");
            }
             
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipSendToFriend(scholarship,userComments, templateParams,domainName,sender.Email);
            templateParams.From = "{0} ({1})<{2}>".Build(sender.Name.NameFirstLast, domainName,  sender.Email);
            templateParams.To = toEmail;
            MessagingService.SendEmail(toEmail, MessageType.ScholarshipSendToFriend, templateParams, true);
        }
    }
}
