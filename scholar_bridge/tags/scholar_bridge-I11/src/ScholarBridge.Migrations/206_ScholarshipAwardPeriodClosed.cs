﻿
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(206)]
    public class ScholarshipAwardPeriodClosed : Migration
    {
        private const string SCHOLARSHIP_TABLE_NAME = "SBScholarship";
        private const string APP_TABLE_NAME = "SBApplication";
        private const string COLUMN_NAME = "AwardPeriodClosed";

        public override void Up()
        {
            Database.AddColumn(SCHOLARSHIP_TABLE_NAME, COLUMN_NAME, DbType.DateTime, ColumnProperty.Null);

            Database.RemoveColumn(APP_TABLE_NAME, COLUMN_NAME);
		}

        public override void Down()
        {
            Database.RemoveColumn(SCHOLARSHIP_TABLE_NAME, COLUMN_NAME);

            Database.AddColumn(APP_TABLE_NAME, COLUMN_NAME, DbType.DateTime, ColumnProperty.Null);
		}
    }
}