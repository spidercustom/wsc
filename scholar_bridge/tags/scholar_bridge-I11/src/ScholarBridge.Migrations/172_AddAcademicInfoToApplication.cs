﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(172)]
    public class AddAcademicInfoToApplication : Migration
    {
        private const string TABLE_NAME = "SBApplication";

        public readonly string[] NEW_COLUMNS
            = { "FirstGeneration", "GPA", "ClassRank", "Honors", "APCredits", "IBCredits",
              "SchoolTypes", "AcademicPrograms", "SeekerStatuses", "ProgramLengths",
              "SATWriting", "SATCriticalReading", "SATMathematics",
              "ACTEnglish", "ACTReading", "ACTMathematics", "ACTScience"};

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Double, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[2], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[3], DbType.Boolean, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[4], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[5], DbType.Int32, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[6], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[7], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[8], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[9], DbType.Int32, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[10], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[11], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[12], DbType.Int32, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[13], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[14], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[15], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[16], DbType.Int32, ColumnProperty.Null);
        }

        public override void Down()
        {
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}