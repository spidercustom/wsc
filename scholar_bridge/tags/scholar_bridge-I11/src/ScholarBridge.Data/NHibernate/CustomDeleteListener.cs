using System.Linq;
using System.Collections.Generic;
using Iesi.Collections;
using NHibernate.Engine;
using NHibernate.Event;
using NHibernate.Event.Default;
using NHibernate.Persister.Entity;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class CustomDeleteListener : DefaultDeleteEventListener
    {
        private static readonly List<DeleteRule> deleteRules = new List<DeleteRule>
                                                          {
                                                              new DeprecatableRule(),
                                                              new SoftDeleteRule()
                                                          };

        protected override void DeleteEntity(IEventSource session, object entity, EntityEntry entityEntry,
                                             bool isCascadeDeleteEnabled, IEntityPersister persister,
                                             ISet transientEntities)
        {
            var deleteRule = DeleteRuleForEntity(entity);
            if (null != deleteRule)
            {
                deleteRule.Apply(entity);

                CascadeBeforeDelete(session, persister, entity, entityEntry, transientEntities);
                CascadeAfterDelete(session, persister, entity, transientEntities);
            }
            else
            {
                base.DeleteEntity(session, entity, entityEntry, isCascadeDeleteEnabled, persister, transientEntities);
            }
        }

        private static DeleteRule DeleteRuleForEntity(object obj)
        {
            return deleteRules.Where(rule => rule.Applies(obj)).SingleOrDefault();
        }
    }

    internal interface DeleteRule
    {
        bool Applies(object obj);
        void Apply(object obj);
    }

    internal class SoftDeleteRule : DeleteRule
    {
        public bool Applies(object obj)
        {
            return obj is ISoftDeletable;
        }

        public void Apply(object obj)
        {
            ((ISoftDeletable)obj).IsDeleted = true;
        }
    }

    internal class DeprecatableRule : DeleteRule
    {
        public bool Applies(object obj)
        {
            return obj is IDeprecatable;
        }

        public void Apply(object obj)
        {
            ((IDeprecatable) obj).Deprecated = true;
        }
    }
}