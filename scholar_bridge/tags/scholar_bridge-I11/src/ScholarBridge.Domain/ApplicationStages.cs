﻿
namespace ScholarBridge.Domain
{
    public enum ApplicationStages
    {
        None,
        NotActivated,
        Submitted
    }
}
