using System;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain
{
    public class Attachment
    {
        public virtual int Id { get; set; }

        [NotNullValidator]
        [StringLengthValidator(1, 100)]
        public virtual string Name { get; set; }
        public virtual double Bytes { get; set; }
        public virtual string MimeType { get; set; }

        [StringLengthValidator(1, 36)]
        public virtual string UniqueName { get; set; }

        [StringLengthValidator(0, 250)]
        public virtual string Comment { get; set; }

		public virtual bool IncludeWithApplications { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual string GetFullPath(string basePath)
        {
            return Path.Combine(basePath, UniqueName);
        }

        public virtual string GenerateUniqueName()
        {
            UniqueName = Guid.NewGuid().ToString("D");
            return UniqueName;
        }

        // Should be big enough. :)
        private readonly string[] SizeNames = new[] {"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "Yib"};

        public virtual string DisplaySize
        {
            get
            {
                double current = Bytes;
                int i = 0;
                while ((current/1024.0) >= 1)
                {
                    current /= 1024.0;
                    i++;
                }

                return String.Format("{0}{1}", Math.Round(current, 1), SizeNames[i]);
            }
        }

		public virtual object Clone(string basePath)
		{
			var cloneAttachment = (Attachment)MemberwiseClone();
			cloneAttachment.Id = 0;
			cloneAttachment.GenerateUniqueName();

            File.Copy(GetFullPath(basePath), cloneAttachment.GetFullPath(basePath));
            File.SetCreationTime(cloneAttachment.GetFullPath(basePath), DateTime.Now);
            File.SetLastAccessTime(cloneAttachment.GetFullPath(basePath), DateTime.Now);

			return cloneAttachment;
		}

        public virtual void RemoveFile(string basePath)
        {
            try
            {
                // If there was an error attempt to cleanup the file
                File.Delete(GetFullPath(basePath));
            }
            catch { }
        }
	}
}