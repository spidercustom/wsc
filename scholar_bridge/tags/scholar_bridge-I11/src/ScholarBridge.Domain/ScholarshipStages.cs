﻿
using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    public enum ScholarshipStages
    {
        None,
        [DisplayName("Not Activated")]
        NotActivated,
        [DisplayName("Activation requested")]
        RequestedActivation,
        Activated,
        Rejected,
        Awarded
    }


    
}
