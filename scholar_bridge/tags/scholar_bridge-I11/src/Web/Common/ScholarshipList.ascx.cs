﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipList : UserControl
    {
        public IApplicationService ApplicationService { get; set; }


        private IList<Scholarship> _Scholarships;
        public string LinkTo { get; set; }

        public IList<Scholarship> Scholarships
        {
            get { return _Scholarships; }
            set { _Scholarships = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Bind();
        }

        protected void Bind()
        {
            if (!(Scholarships == null))
            {
                lstScholarships.DataSource = (from s in Scholarships orderby s.Name select s).ToList();
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship) ((ListViewDataItem) e.Item).DataItem);

                var link = (HyperLink) e.Item.FindControl("linktoScholarship");
				link.NavigateUrl = LinkTo + "?id=" + scholarship.Id + (LinkTo.Contains("BuildScholarship") ? "&resumefrom=0" : string.Empty);

                var applicantCountControl = (Label)e.Item.FindControl("applicantCountControl");
                applicantCountControl.Text = ApplicationService.CountAllSubmitted(scholarship).ToString();
            }
        }

        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }
    }
}