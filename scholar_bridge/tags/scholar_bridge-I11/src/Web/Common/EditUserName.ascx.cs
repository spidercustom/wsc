﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Web.Common
{
    public partial class EditUserName : UserControl
    {
        public event UserSaved UserSaved;
        public event FormCanceled FormCanceled;

        public IUserService UserService { get; set; }
        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (null == CurrentUser)
                {
                    errorMessage.Visible = true;
                    return;
                }

				if (CurrentUser.Name != null)
				{
					FirstName.Text = CurrentUser.Name.FirstName;
					MiddleName.Text = CurrentUser.Name.MiddleName;
					LastName.Text = CurrentUser.Name.LastName;
				}
            	PhoneNumber.Text = FormatPhone(CurrentUser.Phone);
            	FaxNumber.Text = FormatPhone(CurrentUser.Fax);
            	OtherPhoneNumber.Text = FormatPhone(CurrentUser.OtherPhone);
			}
        }

		private string FormatPhone(PhoneNumber phoneNumber)
		{
			return phoneNumber == null ? string.Empty : CurrentUser.Phone.FormattedPhoneNumber;
		}

        protected void saveButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CurrentUser.Name.FirstName = FirstName.Text;
                CurrentUser.Name.MiddleName = MiddleName.Text;
                CurrentUser.Name.LastName = LastName.Text;
                CurrentUser.Phone = SetPhone(PhoneNumber.Text);
                CurrentUser.Fax = SetPhone(FaxNumber.Text);
                CurrentUser.OtherPhone = SetPhone(OtherPhoneNumber.Text);
                UserService.Update(CurrentUser);

                if (null != UserSaved)
                {
                    UserSaved(CurrentUser);
                }
            }
        }

		private PhoneNumber SetPhone(string p)
		{
			return String.IsNullOrEmpty(p) ? null : new PhoneNumber(p);
		}

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}