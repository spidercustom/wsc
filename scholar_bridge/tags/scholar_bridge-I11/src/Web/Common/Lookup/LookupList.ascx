﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupList.ascx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupList" %>
<div id="lookuplist_holder">
    <div id="lookuplist_header">
        <p>Select single item using click or multiple items either using ctrl+click or mouse drag/lasso.</p>
    </div>
    <div id="lookuplist_left" class="ui-corner-all">
        <h1>Available</h1>
        <div id="lookuplist_search" >
            <asp:TextBox CssClass="lookupsearchbox text ui-widget-content ui-corner-all" runat="server" ID="txtSearch"  ></asp:TextBox>
            <asp:HyperLink  runat="server" NavigateUrl="#" ID="btnSearch"><img  src="/images/lookupdialog/magnifier.png" Alt="Go"  /></asp:HyperLink>
        </div>
        <div id="lookuplist_available_container">
            <asp:Panel  id="lookuplist_available" runat="server"> 
            </asp:Panel>
        </div>
            <asp:Panel  id="pager" runat="server" CssClass="jqpager"></asp:Panel>
    </div>
    
    <div id="lookuplist_middle">
        <table>
            <tr><td> <asp:button id="btnAdd" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="Add>" Width="100px" Height="23px" CausesValidation="False" />
            </td></tr>
            <tr><td><asp:button id="btnRemove" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="<Remove" Width="100px" Height="23px" CausesValidation="False"  />
            </td></tr>
            <tr><td><asp:button id="btnRemoveAll" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="<<Remove All" Width="100px" Height="23px" CausesValidation="False"  />
            </td></tr>
            <tr><td><asp:Image runat="server" ImageUrl="/images/lookupdialog/ajax-loader.gif"  id="ajaxprogress" CssClass="ajaxprogress" AlternateText="Wait..."  /></td></tr>
        </table>
    </div>
    <asp:Panel id="lookuplist_right" class="ui-corner-all" runat="server">
        <h1>Selections</h1>
         <asp:Panel  id="lookuplist_selections" runat="server"> </asp:Panel>
         
     </asp:Panel>
   
    <asp:Panel id="lookuplist_otherbox"  CssClass="text ui-widget-content ui-corner-all"  runat="server">
        <h1>Others:</h1>
         <asp:TextBox CssClass="noborder" runat="server" ID="txtOther" TextMode="MultiLine" Rows="3" Height="90px"  ></asp:TextBox>
    </asp:Panel>
         
    
</div> <%--End of holder--%>

  

