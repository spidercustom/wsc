﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SentMessageView.ascx.cs" Inherits="ScholarBridge.Web.Common.SentMessageView" %>

<div class="message">
    <h2><asp:Literal ID="subjectLbl" runat="server"/></h2>
    <h3><asp:Literal ID="fromLbl" runat="server" /></h3>
    <span id="sent"><asp:Literal ID="dateLbl" runat="server" /></span>
    
    <pre>
    <asp:Literal ID="contentLbl" runat="server"/>
    </pre>
</div>
