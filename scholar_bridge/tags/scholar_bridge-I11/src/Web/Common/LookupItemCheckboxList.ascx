﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupItemCheckboxList.ascx.cs" Inherits="ScholarBridge.Web.Common.LookupItemCheckboxList" %>
<div id="aa" class="lookupItemCheckboxList">
<asp:CheckBoxList ID="CheckBoxList" runat="server"  Width="450px" >

</asp:CheckBoxList>
</div>
<br />
<asp:CustomValidator ID="SelectNoneValidator" runat="server" 
  ErrorMessage="Select atleast one" 
  onservervalidate="SelectNoneValidator_ServerValidate"></asp:CustomValidator>
<br />
<asp:panel id="buttonPanel" runat="server" visible="<%# !AllBoxesUnchecked %>">
<input id="SelectAllButton" type="button" style="width:50px" value="All" onclick="javascript:SelectAllCheckBoxIn('<%# CheckBoxList.ClientID %>');" />
<input id="SelectNoneButton" type="button" style="width:50px" value="None" onclick="javascript:DeselectAllCheckBoxIn('<%# CheckBoxList.ClientID %>');" />
</asp:panel>