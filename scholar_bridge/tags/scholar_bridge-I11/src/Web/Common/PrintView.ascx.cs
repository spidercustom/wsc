﻿using System;
using ScholarBridge.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class PrintView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PrintViewContainer.Visible = Page.IsInPrintView();
            ScreenViewContainer.Visible = !PrintViewContainer.Visible;

            printViewLink.HRef = BuildPrintViewUrl();
        }
        public string CustomUrl { get; set; }
        private string BuildPrintViewUrl()
        {
            if (!String.IsNullOrEmpty(CustomUrl))
            return CustomUrl;
            var uriBuilder = new UrlBuilder(Request.Url.AbsoluteUri);
            if (! uriBuilder.QueryString.ContainsKey("print"))
            {
                uriBuilder.QueryString.Add("print", "true");
            }
            return uriBuilder.ToString();
        }
    }
}