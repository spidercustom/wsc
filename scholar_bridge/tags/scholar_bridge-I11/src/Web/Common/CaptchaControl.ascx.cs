﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    public partial class CaptchaControl : UserControl
    {
        private const int CAPTCHA_LENGTH = 5;

        private const string dictionary = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        private static readonly string[] dictArray;
        private static readonly int MAX_LENGTH;
        static CaptchaControl()
        {
            dictArray = dictionary.Split(',');
            MAX_LENGTH = dictArray.Length - 1;
        }

        public string ValidationGroup { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (! IsPostBack || ImageHipChallenge1.Words.Count == 0)
            {
                HipValidator1.ValidationGroup = ValidationGroup;
                GenerateNewCaptcha();
            }
        }

        private static string CreateRandomNumber(int codeLength)
        {
            string randomCode = "";

            var random = new Random((int)DateTime.Now.Ticks);
            while (randomCode.Length < codeLength)
            {
                int t = random.Next(MAX_LENGTH);
                var nextChar = dictArray[t];
                // Don't repeat two values next to each other.
                if (randomCode.EndsWith(nextChar))
                {
                    continue;
                }
                randomCode += nextChar;
            }

            return randomCode;
        }

        public void ClearGuess()
        {
            txtCaptcha.Text = null;
        }

        protected void trynewButton_Click(object sender, EventArgs e)
        {
            GenerateNewCaptcha();
        }

        private void GenerateNewCaptcha()
        {
            var word = CreateRandomNumber(CAPTCHA_LENGTH);
            ImageHipChallenge1.Words.Add(word.ToLower());
        }

         
    }
}

