﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ShowAllApplications : UserControl
    {
        private const string MESSAGE_TEMPLATE = "No {0} selected for the scholarship";
        public string CriteriaString { get; set;}
        public IList<Application> Applications { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            allApplications.DataSource = Applications;
            allApplications.DataBind();
            MessagePara.InnerText = MESSAGE_TEMPLATE.Build(CriteriaString);
            NoApplicantMessage.Visible = Applications == null || Applications.Count == 0;
        }
    }
}