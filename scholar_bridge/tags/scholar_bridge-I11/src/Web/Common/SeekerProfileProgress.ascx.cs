﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    
    public partial class SeekerProfileProgress: UserControl
    {
         
        public UserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            var seeker = UserContext.CurrentSeeker;
            if (!(seeker == null))
            {
                lblStatus.Text = seeker.Stage == SeekerStages.Published
                    ? "Profile activated on {0}".Build(seeker.ProfileActivatedOn.Value.ToShortDateString())
                    : "Profile not activated yet.";

                lblLastUpdate.Text = (seeker.ProfileLastUpdateDate.HasValue)
                    ? "Profile last updated on {0}".Build(seeker.ProfileLastUpdateDate.Value.ToShortDateString())
                    : "Profile not yet started.";

                var percent = seeker.GetPercentComplete();

                lblPercent.Text = string.Format("  Your profile is {0}% complete", percent);
                var page = HttpContext.Current.CurrentHandler as Page;
                if (page != null)
                    page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), ClientID + "_script", GetJS(percent));

            }
            else
            {
                Visible = false;
            }
			Visible = !Page.IsInPrintView();
            base.OnPreRender(e);
        }
         
        private static string GetJS(int percent)
        {
            var script = "$(function() {$('#progressbar').progressbar({value: " + percent + "});});";
            return script;
        }



    }
}