﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterOrganization.ascx.cs"
    Inherits="ScholarBridge.Web.Common.RegisterOrganization" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
    <%@ Register Src="~/Common/CaptchaControl.ascx" tagname="CaptchaControl" tagprefix="sb" %>
<asp:MultiView runat="server" ID="CreateUserWizard" ActiveViewIndex="0">
    <asp:View ID="DataEntryView" runat="server">
        <h2>
            Register My Organization</h2>
        <h3>
            Admin User Information</h3>
        <p>
            Placekeeper for note on Provider Admin role</p>
        <table>
            <tr>
                <td>
                    <label for="FirstName">
                        First Name*:</label>
                </td>
                <td>
                    <asp:TextBox ID="FirstName" runat="server" Columns="40" MaxLength="40"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="FirstNameRequired" runat="server"
                        ControlToValidate="FirstName" ErrorMessage="First Name is required." ToolTip="First Name must be entered."
                        EnableClientScript="true"></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="firstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName"
                        SourceTypeName="ScholarBridge.Domain.PersonName" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="MiddleName">
                        Middle Name:</label>
                </td>
                <td>
                    <asp:TextBox ID="MiddleName" runat="server" Columns="40" MaxLength="40"></asp:TextBox>
                </td>
                <td>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName"
                        SourceTypeName="ScholarBridge.Domain.PersonName" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="LastName">
                        Last Name*:</label>
                </td>
                <td>
                    <asp:TextBox ID="LastName" runat="server" Columns="40" MaxLength="40"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="LastNameRequired" runat="server"
                        ControlToValidate="LastName" ErrorMessage="Last Name is required." ToolTip="Last Name must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName"
                        SourceTypeName="ScholarBridge.Domain.PersonName" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="UserName">
                        Email Address*:</label>
                </td>
                <td>
                    <asp:TextBox ID="UserName" runat="server" Columns="70" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="EmailAddressRequired" runat="server"
                        ControlToValidate="UserName" ErrorMessage="Email address is required." ToolTip="Email address must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email"
                        SourceTypeName="ScholarBridge.Domain.Auth.User" />
                    <asp:CustomValidator ID="EmailCustomValidator" runat="server"></asp:CustomValidator>
                    <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="ConfirmEmail">
                        Confirm Email Address*:</label>
                </td>
                <td>
                    <asp:TextBox ID="ConfirmEmail" runat="server" Columns="70" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredConfirmEmailValidator"
                        runat="server" ControlToValidate="ConfirmEmail" ErrorMessage="Confirm Email is required."
                        ToolTip="Confirm Email is required."></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail"
                        ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address." />
                </td>
            </tr>
            <tr>
                <td colspan="3"><br />
                    <span class="noteBene">Password must be at least 8 characters with one capital letter
                        and one special character or number</span>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Password">
                        Password*:</label>
                </td>
                <td>
                    <asp:TextBox ID="Password" runat="server" TextMode="Password" Columns="50" MaxLength="20"></asp:TextBox>
                </td>
                <td>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password"
                        SourceTypeName="ScholarBridge.Domain.Auth.User" />
                    <asp:RequiredFieldValidator Display="Dynamic" ID="passwordRequired" runat="server"
                        ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required."></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="ConfirmPassword">
                        Confirm Password*:</label>
                </td>
                <td>
                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Columns="50"
                        MaxLength="20"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="ConfirmPasswordRequired" runat="server"
                        ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required."
                        ToolTip="Confirm Password is required."></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                        ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."></asp:CompareValidator>
                </td>
            </tr>
            
            
            <tr>
                <td colspan="3">
                    <h3>
                        Organization Information</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Name">
                        Name*:</label>
                </td>
                <td>
                    <asp:TextBox ID="Name" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="OrgNameRequired" runat="server"
                        ControlToValidate="Name" ErrorMessage="Organization name is required." ToolTip="Organization name must be selected."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="NameValidator" runat="server" ControlToValidate="Name" PropertyName="Name"
                        SourceTypeName="ScholarBridge.Domain.Organization" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="TaxId">
                        Tax Id (EIN)*:</label>
                </td>
                <td>
                    <asp:TextBox ID="TaxId" runat="server" Columns="10" MaxLength="10" CssClass="ein"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="TaxIDRequired" runat="server" ControlToValidate="TaxId"
                        ErrorMessage="Tax ID is required." ToolTip="Tax ID must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="TaxIdValidator" runat="server" ControlToValidate="TaxId" PropertyName="TaxId"
                        SourceTypeName="ScholarBridge.Domain.Organization" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Website">
                        Website:</label>
                </td>
                <td>
                    <asp:TextBox ID="Website" runat="server" Columns="70" MaxLength="128"></asp:TextBox>
                </td>
                <td>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="WebsiteValidator" runat="server" ControlToValidate="Website" PropertyName="Website"
                        SourceTypeName="ScholarBridge.Domain.Organization" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <h4>
                        Address</h4>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="AddressStreet">
                        Street*:</label>
                </td>
                <td>
                    <asp:TextBox ID="AddressStreet" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="AddressStreetRequired" runat="server"
                        ControlToValidate="AddressStreet" ErrorMessage="Street Address is required."
                        ToolTip="Street address must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet"
                        PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="AddressStreet2">
                        Street:</label>
                </td>
                <td>
                    <asp:TextBox ID="AddressStreet2" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2"
                        PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="AddressCity">
                        City*:</label>
                </td>
                <td>
                    <asp:TextBox ID="AddressCity" runat="server" Columns="50" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="CityRequired" runat="server" ControlToValidate="AddressCity"
                        ErrorMessage="City is required." ToolTip="City must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City"
                        SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="AddressState">
                        State*:</label>
                </td>
                <td>
                    <asp:DropDownList ID="AddressState" runat="server">
                    </asp:DropDownList>
                    
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="StateRequired" runat="server" ControlToValidate="AddressState"
                        ErrorMessage="State is required." ToolTip="State must be selected."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State"
                        SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="AddressPostalCode">
                        Postal Code*:</label>
                </td>
                <td>
                    <asp:TextBox ID="AddressPostalCode" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="PostalCodeRequired" runat="server"
                        ControlToValidate="AddressPostalCode" ErrorMessage="Postal Code is required."
                        ToolTip="Postal code must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode"
                        PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <h4>
                        Phones</h4>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Phone">
                        Phone*:</label>
                </td>
                <td>
                    <asp:TextBox ID="Phone" runat="server" Columns="10" MaxLength="10" CssClass="phone"></asp:TextBox>
                </td>
                <td>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number"
                        SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
                    <asp:RequiredFieldValidator Display="Dynamic" ID="phoneRequired" runat="server" ControlToValidate="Phone"
                        ErrorMessage="Phone number is required." ToolTip="Phone number must be entered."></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="Fax">
                        Fax:</label>
                </td>
                <td>
                    <asp:TextBox ID="Fax" runat="server" Columns="10" MaxLength="10" CssClass="phone"></asp:TextBox>
                </td>
                <td>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="FaxValidator" runat="server" ControlToValidate="Fax" PropertyName="Number"
                        SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="OtherPhone">
                        Other Phone:</label>
                </td>
                <td>
                    <asp:TextBox ID="OtherPhone" runat="server" Columns="10" MaxLength="10" CssClass="phone"></asp:TextBox>
                </td>
                <td>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="OtherPhoneValidator" runat="server" ControlToValidate="OtherPhone" PropertyName="Number"
                        SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
                </td>
            </tr>
            <tr>
                <td><br /><br />  Word Verification:<br /><br /><sb:CaptchaControl ID="CaptchaControl1" runat="server" /><br /><br /></td>
            </tr>
            
            <tr>
                <td colspan="3">
                    <p>
                       * = Required Field</p>
                    <p>
                        Placekeeper for note on junk email filter</p>
                    <asp:Button ID="RegisterButton" runat="server" Text="Register" OnClick="RegisterButton_Click"
                        CausesValidation="true" />
                </td>
            </tr>
        </table>
    </asp:View>
    <asp:View ID="CompletionView" runat="server">
        <div id="emailSuccess" runat="server">
            <h2>Thanks for registering with us</h2>
            <p>
                Now wait for an email to be sent to the email address you specified with instructions
                to enable your account and login.
            </p>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
            </p>
        </div>

        <p id="emailFail" runat="server" visible="false">
            There was a problem sending a confirmation email so no account was created. Please contact us for assistance.
        </p>
    </asp:View>
</asp:MultiView>
