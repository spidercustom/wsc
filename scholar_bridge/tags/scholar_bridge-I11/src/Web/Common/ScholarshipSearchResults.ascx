﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipSearchResults.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipSearchResults" %>
<br /><br />
<asp:Label ID="lbladdto" runat="server" Visible="false">Click on Scholarship Name to add to your My Scholarships tab</asp:Label>

<asp:ListView ID="lstScholarships" runat="server" 
    OnItemDataBound="lstScholarships_ItemDataBound" 
    onpagepropertieschanging="lstScholarships_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
        <thead>
            <tr>
                <th>Scholarship Name</th>
                <th>Provider/Intermediary</th>
                <th>Donor</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:LinkButton ID="linkBtn" runat="server"   ><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:LinkButton></td>
        <td><asp:Literal ID="lblProvider" runat="server" /></td>
        <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Donor"),"Name")%></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:LinkButton ID="linkBtn" runat="server" ><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:LinkButton></td>
        <td><asp:Literal ID="lblProvider" runat="server" /></td>
        <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Donor"),"Name")%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships matching search criteria, please try again using new criteria. </p>
    
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="lstScholarships" PageSize="20" >
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
</div> 
