﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageView.ascx.cs" Inherits="ScholarBridge.Web.Common.MessageView" %>

<div class="message">
    <h2><asp:Literal ID="subjectLbl" runat="server"/></h2>
    <h3><asp:Literal ID="fromLbl" runat="server" /></h3>
    <span id="sent"><asp:Literal ID="dateLbl" runat="server" /></span>
    
    <asp:PlaceHolder ID="relatedInfo" runat="server"></asp:PlaceHolder>
    
    <pre>
    <asp:Literal ID="contentLbl" runat="server"/>
    </pre>
</div>

<asp:Button ID="archiveBtn" runat="server" Text="Archive" 
    onclick="archiveBtn_Click" />
<br />
<asp:Button ID="approveBtn" runat="server" Text="Approve" 
    onclick="approveBtn_Click" />
<asp:Button ID="rejectBtn" runat="server" Text="Deny" 
    onclick="rejectBtn_Click" />