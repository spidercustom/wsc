﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.PressRoom 
{
    public partial class ArticleList : UserControl
    {
        private IList<Article> _Articles;
        public string LinkTo { get; set; }

        public IList<Article> Articles
        {
            get { return _Articles; }
            set
            {
                _Articles = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            Bind();
        }

        protected void Bind()
        {
            if (!(Articles == null))
            {
                lstArticles.DataSource = Articles;
                lstArticles.DataBind();
            }
        }

        protected void lstArticles_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var article = ((Article)((ListViewDataItem)e.Item).DataItem);

                var link = (HyperLink)e.Item.FindControl("linktoArticle");
                link.NavigateUrl = LinkTo + "?id=" + article.Id;

               

            }
        }

        protected void lstArticles_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }

        
    }
}