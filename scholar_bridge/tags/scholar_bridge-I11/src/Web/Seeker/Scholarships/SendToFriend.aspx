﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SendToFriend.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Scholarships.SendToFriend" Title="Seeker | Scholarships | Send To Friend" %>

<%@ Register src="~/Common/ScholarshipPublicView.ascx" tagname="ScholarshipPublicView" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
 <h3>Send to freind</h3>
 <br />
 <br />
 
     <label for="EmailBox">Friend's Email Address:</label>
     <br />
    <asp:TextBox ID="EmailBox" runat="server"></asp:TextBox>
    <br />
    <asp:RequiredFieldValidator ID="emailRequired" ControlToValidate="EmailBox" Display="Dynamic" runat="server" ErrorMessage="Friend's Email address is required" ToolTip="Please enter your friend's email address"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="emailValidator" runat="server" ControlToValidate="EmailBox" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" ErrorMessage="Please enter a valid email address"/>
    <br />
    
    <label for="CommentBox">Your Comments (240 chars limit.):</label>
    <br />
    <asp:TextBox ID="CommentBox" runat="server" Height="40px" Width="500px" TextMode="MultiLine" Rows="3"></asp:TextBox>
    <asp:RegularExpressionValidator ID="commentsLengthValidator" runat="server" ControlToValidate="CommentBox" ValidationExpression="^.{0,240}$" ErrorMessage="Comment should not exceed 240 characters limit."/>
    <br />
    <sb:ScholarshipPublicView ID="ScholarshipPublicView1" runat="server"  />
<br />

<div> <asp:Button ID="btnSend" Text="Send" runat="server" onclick="btnSend_Click" />  </div>
</asp:Content>
