﻿<%@ Page Language="C#" MasterPageFile="~/Seeker/Seeker.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Scholarships.Default"  Title="Seeker | My Scholarships" %>
<%@ Register src="~/Common/MatchList.ascx" tagname="MatchList" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<h3>List of scholarship that I'm interested in</h3>
<asp:Label ID="lblProfileActivationMessage" Text="You must activate your profile to save and apply for scholarships." runat="server" CssClass="noteBene" />
<asp:ListView ID="myScholarhipList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    onpagepropertieschanging="matchList_PagePropertiesChanging" >
    <LayoutTemplate>
        <table class="sortableTable">
                <thead>
                <tr>
                    <th>Scholarship Name</th>
                    <th>Application Due Date</th>
                    <th># of Awards</th>
                    <th>Amount $</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th>&nbsp</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
          
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton id="linkToScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
            <td><%# Eval("MatchApplicationStatusString")%></td>
            <td><asp:Button ID="DefaultActionButton" runat="server" OnCommand="DefaultActionButton_OnCommand"/></td>
            <td><asp:Button ID="RemoveButton" runat="server" Text="Remove" OnCommand="RemoveButton_OnCommand"/></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton id="linkToScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
            <td><%# Eval("MatchApplicationStatusString")%></td>
            <td><asp:Button ID="DefaultActionButton" runat="server" OnCommand="DefaultActionButton_OnCommand"/></td>
            <td><asp:Button ID="RemoveButton" runat="server" Text="Remove" OnCommand="RemoveButton_OnCommand"/></td>
        </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
        <p>There are no scholarships at this time.</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="myScholarhipList" PageSize="20" >
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
</div> 

</asp:Content>
