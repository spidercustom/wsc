﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Matches
{
    public partial class Show : Page
    {
        public IMatchService MatchService { get; set; }
        public IUserContext UserContext { get; set; }
       
        private Match currentMatch { get; set; }
        private Domain.Seeker currentSeeker;

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["id"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
            currentSeeker = UserContext.CurrentSeeker;

            currentMatch = MatchService.GetMatch(currentSeeker, ScholarshipId);

            if (null != currentMatch)
            {
                showScholarship.Scholarship = currentMatch.Scholarship;
                scholarshipTitleStripeControl.UpdateView(currentMatch.Scholarship);
            }
            sendToFriendBtn.NavigateUrl =
                ResolveUrl("~/Seeker/Scholarships/SendToFriend.aspx?id={0}".Build(ScholarshipId));
        }

        
    }
}
