﻿using System;
namespace ScholarBridge.Web.Seeker
{
    public partial class Default : System.Web.UI.Page
    {
        public UserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(UserContext.CurrentSeeker ==null))
                registerLnk.Visible = false;
        }
    }
}