﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Admin.Default" %>


<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div >
   <h3>Provider Administration</h3>
</div>
<ul class="pageNav">
    <li><asp:HyperLink ID="relationshipsLink" runat="server" NavigateUrl="~/Provider/Relationships/Default.aspx">Manage Relationships with Intermediaries</asp:HyperLink></li>
    <li><asp:HyperLink ID="usersLink" runat="server" NavigateUrl="~/Provider/Users/Default.aspx">Manage Users</asp:HyperLink></li>
    <li><asp:HyperLink ID="orgInfoLink" runat="server" NavigateUrl="~/Provider/Org/Edit.aspx">Edit My Organization Information</asp:HyperLink></li>
</ul>
    
</asp:Content>
