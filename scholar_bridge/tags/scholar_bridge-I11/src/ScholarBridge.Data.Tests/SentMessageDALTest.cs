using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class SentMessageDALTest : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public RoleDAL RoleDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public MessageDAL MessageDAL { get; set; }
        public SentMessageDAL SentMessageDAL { get; set; }

        private SentMessage userMessage;
        private SentMessage roleOrgMessage;
        private User user;
        private Role role;
        private Provider organization;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "test@test.com");
            organization = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "Test Provider", user);
            role = RoleDAL.FindByName(Role.WSC_ADMIN_ROLE);

            userMessage = InsertSentMessage(CreateMessage(user, null, null));
            roleOrgMessage = InsertSentMessage(CreateMessage(null, role, organization));
        }

        [Test]
        public void can_find_by_id_with_other_params()
        {
            var found = SentMessageDAL.FindById(null, new List<Role> {role}, organization, roleOrgMessage.Id);
            Assert.That(found, Is.Not.Null);
            Assert.That(found.Subject, Is.EqualTo(userMessage.Subject));
        }

        [Test]
        public void can_find_by_id_with_user()
        {
            var found = SentMessageDAL.FindById(user, null, null, userMessage.Id);
            Assert.That(found, Is.Not.Null);
            Assert.That(found.Subject, Is.EqualTo(userMessage.Subject));
        }

        [Test]
        public void can_find_by_user()
        {
            var found = SentMessageDAL.FindAll(user, null, null);

            Assert.That(found, Is.Not.Null);
            CollectionAssert.IsNotEmpty(found);
            Assert.That(found[0].Subject, Is.EqualTo(userMessage.Subject));
        }

        [Test]
        public void can_find_by_role_and_org()
        {
            var found = SentMessageDAL.FindAll(null, new List<Role> {role}, organization);

            Assert.That(found, Is.Not.Null);
            CollectionAssert.IsNotEmpty(found);
            Assert.That(found[found.Count-1].Subject, Is.EqualTo(roleOrgMessage.Subject));
        }

        private SentMessage InsertSentMessage(Message m)
        {
            var sent = new SentMessage(m)
                           {
                               LastUpdate = new ActivityStamp(user)
                           };
            sent = SentMessageDAL.Insert(sent);

            Assert.IsNotNull(sent);
            Assert.AreNotEqual(0, sent.Id);

            return sent;
        }

        private static Message CreateMessage(User u, Role r, Organization o)
        {
            var to = new MessageAddress
                         {
                             User = u,
                             Organization = o,
                             Role = r
                         };
            var from = to;

            var message = new Message
                              {
                                  Subject = "Test Message",
                                  Content = "content",
                                  From = from,
                                  To = to
                              };

            return message;
        }
    }
}