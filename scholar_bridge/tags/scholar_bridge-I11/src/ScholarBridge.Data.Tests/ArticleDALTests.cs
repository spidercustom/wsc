using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class ArticleDALTests : TestBase
    {

        public ArticleDAL ArticleDAL { get; set; }

        public UserDAL UserDAL { get; set; }
    
        private User user;
        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
        }

        [Test]
        public void can_insert_article()
        {
            var article = InsertArticle(ArticleDAL, user);

            Assert.IsNotNull(article);
            Assert.IsTrue(article.Id > 0);
        }

        
        [Test]
        public void can_delete_articles()
        {
            var article = InsertArticle(ArticleDAL, user);
            ArticleDAL.Delete(article);
            var found = ArticleDAL.FindById(article.Id);
            Assert.IsNull(found );
            
        }

        
        [Test]
        public void can_find_all_articles()
        {
            var all = ArticleDAL.FindAll("PostedDate");
        	var currentCount = all.Count;
            var a1 = InsertArticle(ArticleDAL,user );
            var a2 = InsertArticle(ArticleDAL, user);


            all = ArticleDAL.FindAll("PostedDate");
            Assert.IsNotNull(all);
            Assert.AreEqual(currentCount + 2, all.Count);

        }

        public static Article InsertArticle(ArticleDAL articleDal,User user)
        {

            var article = new Article
                              {
                                  Title = "test1",
                                  Body = "test test test",
                                  PostedDate = DateTime.Today,
                                 LastUpdate = new ActivityStamp(user)
                          };
            article = articleDal.Insert(article);
            return article;
        }

      
    }
}
