using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ProviderDALTest : TestBase
    {
        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }

        [Test]
        public void CanCreateProvider()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual("TestProvider", provider.Name);
            Assert.IsNotNull(provider.Address);
            Assert.IsNotNull(provider.Address.State);
            Assert.AreEqual("WI", provider.Address.State.Abbreviation);
            Assert.IsNotNull(provider.Phone);
        }

        [Test]
        public void CanGetProviderById()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);

            var foundProvider = ProviderDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            Assert.AreEqual(provider.Name, foundProvider.Name);
            Assert.AreEqual(provider.TaxId, foundProvider.TaxId);
            Assert.AreEqual(provider.ApprovalStatus, foundProvider.ApprovalStatus);
        }

        [Test]
        public void ProviderStartsOutAsPendingApproval()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual(ApprovalStatus.PendingApproval, provider.ApprovalStatus);

            var foundProvider = ProviderDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            Assert.AreEqual(ApprovalStatus.PendingApproval, foundProvider.ApprovalStatus);
        }

        [Test]
        public void CanFindAllProvidersPendingApproval()
        {
            var initialCount = ProviderDAL.FindAllPending().Count;

            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider1 = InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            var provider2 = InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);

            Assert.AreEqual(initialCount + 2, ProviderDAL.FindAllPending().Count);

            provider1.ApprovalStatus = ApprovalStatus.Approved;
            ProviderDAL.Update(provider1);

            Assert.AreEqual(initialCount + 1, ProviderDAL.FindAllPending().Count);

            provider2.ApprovalStatus = ApprovalStatus.Denied;
            ProviderDAL.Update(provider2);

            Assert.AreEqual(initialCount, ProviderDAL.FindAllPending().Count);
        }

        [Test]
        public void CanAddAdminUserToProvider()
        {
            var provider = AddProviderWithAdminUser();

            var foundProvider = ProviderDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            Assert.IsNotNull(foundProvider.AdminUser);
            Assert.IsNotNull(foundProvider.ActiveUsers);
            Assert.AreEqual(1, foundProvider.ActiveUsers.Count);
        }

        [Test]
        public void can_get_user_by_provider_and_id()
        {
            var provider = AddProviderWithAdminUser();

            var foundUser = ProviderDAL.FindUserInOrg(provider, provider.AdminUser.Id);
            Assert.IsNotNull(foundUser);
            Assert.AreEqual(provider.AdminUser.Id, foundUser.Id);
        }

        [Test]
        public void can_not_get_user_by_other_provider_and_id()
        {
            var provider = AddProviderWithAdminUser();
            var user2 = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider2 = InsertProvider(ProviderDAL, StateDAL, "New Provider", user2);

            var foundUser = ProviderDAL.FindUserInOrg(provider2, provider.AdminUser.Id);
            Assert.IsNull(foundUser);
        }

        [Test]
        public void delete_user_shows_up_in_deleted_users_collection()
        {
            var provider = AddProviderWithAdminUser();
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider.Users.Add(user);
            ProviderDAL.Update(provider);

            UserDAL.Delete(user);

            var foundProvider = ProviderDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);

            Assert.AreEqual(1, provider.ActiveUsers.Count); // Admin user is still in here
            Assert.AreEqual(1, provider.DeletedUsers.Count);
        }

        [Test]
        public void CanGetProviderByUser()
        {
            var provider = AddProviderWithAdminUser();
            
            var foundProvider = ProviderDAL.FindByUser(provider.AdminUser);
            Assert.IsNotNull(foundProvider);
            Assert.AreEqual(provider.Id, foundProvider.Id);
        }

        [Test]
        public void CanAddProviderAdminNotes()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            
            provider.AppendAdminNotes(user, "Testing the note.");
            ProviderDAL.Update(provider);

            provider.AppendAdminNotes(user, "Testing the note again.");
            ProviderDAL.Update(provider);

            var foundProvider = ProviderDAL.FindById(provider.Id);
            Assert.IsNotNull(foundProvider);
            StringAssert.Contains("Testing the note.", foundProvider.AdminNotes.ToString());
            StringAssert.Contains("Testing the note again.", foundProvider.AdminNotes.ToString());
        }

        private Provider AddProviderWithAdminUser()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            var provider = InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            provider.AdminUser = user;

            ProviderDAL.Update(provider);
            return provider;
        }

        public static Provider InsertProvider(IProviderDAL providerDAL, StateDAL stateDAL, string name, User user)
        {
            var wi = stateDAL.FindByAbbreviation("WI");
            var provider = new Provider
                               {
                                   Name = name,
                                   TaxId = "123-45678",
                                   LastUpdate = new ActivityStamp(user),
                                   Address = new Address
                                                 {
                                                     Street = "123 Foo St.",
                                                     City = "Milwaukee",
                                                     PostalCode = "53212",
                                                     State = wi
                                                 },
                                   Phone = new PhoneNumber("414 431-5593")
                               };

            provider = providerDAL.Insert(provider);

            Assert.IsNotNull(provider);
            Assert.AreNotEqual(0, provider.Id);

            return provider;
        }
    }
}