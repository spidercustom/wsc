using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    class BuildApplicationAction : OpenUrlAction
    {
        private const string BUILD_APPLICATION_URL_TEMPLATE = "~/Seeker/BuildApplication/Default.aspx?sid={0}";

        public BuildApplicationAction() : base("Apply") { }

        protected override string ConstructUrl(Match match)
        {
            return BUILD_APPLICATION_URL_TEMPLATE.Build(match.Scholarship.Id);
        }
    }
}