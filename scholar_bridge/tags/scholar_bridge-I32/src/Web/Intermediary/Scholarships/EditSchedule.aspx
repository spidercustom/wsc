﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="EditSchedule.aspx.cs" Inherits="ScholarBridge.Web.Intermdiary.Scholarships.EditSchedule" Title="Scholarship | Edit Schedule" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/EditScholarshipSchedule.ascx" tagname="EditScholarshipSchedule" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>             
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HasPrintView="false" />
<sb:EditScholarshipSchedule ID="EditSchedule1" runat="server"
 OnScheduleSaved="EditSchedule1_OnScheduleSaved"   OnFormCanceled="EditSchedule1_OnFormCanceled" />
</asp:Content>
