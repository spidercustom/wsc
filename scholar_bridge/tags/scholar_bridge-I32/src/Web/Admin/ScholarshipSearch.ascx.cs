﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin
{
	public partial class ScholarshipSearch : System.Web.UI.UserControl
	{
		public IScholarshipService ScholarshipService { get; set; }


		private IList<Scholarship> allScholarships;
		private IList<Scholarship> AllScholarships
		{
			get
			{
				if (allScholarships == null)
				{
					allScholarships = ScholarshipService.GetAllScholarships();
				}
				return allScholarships;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void providerDropDownSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = this;
		}

		protected void intermediaryDropDownSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = this;
		}

		protected void scholarshipDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			if (!Page.IsPostBack)
				e.ObjectInstance = null;
			else
				e.ObjectInstance = this;
		}

		protected void btnSearchScholarships_Click(object sender, EventArgs e)
		{
			gvScholarshipList.DataSourceID = scholarshipDataSource.ID;
			gvScholarshipList.DataBind();
		}

		private IEnumerable<Scholarship> GetScholarships()
		{
			var scholarships = from s in AllScholarships
			                   select s;
			
			if (ddlProvider.SelectedIndex > 0)
			{
				scholarships = scholarships.Where(s => s.Provider.Id == int.Parse(ddlProvider.SelectedValue));
			}
			if (ddlIntermediary.SelectedIndex > 0)
			{
				scholarships = scholarships.Where(s => s.Intermediary.Id == int.Parse(ddlIntermediary.SelectedValue));
			}
			if (ddlScholarshipStatus.SelectedIndex > 0)
			{
				switch (ddlScholarshipStatus.SelectedValue)
				{
					case "Activated":
						scholarships =
							scholarships.Where(s => s.Stage == ScholarshipStages.Activated && s.ApplicationDueDate >= DateTime.Now);
						break;
					case "NotActivated":
						scholarships =
							scholarships.Where(s => ScholarshipDAL.GetNonActivatedStages().Contains(s.Stage));
						break;
					case "Closed":
						scholarships =
							scholarships.Where(s => s.Stage == ScholarshipStages.Activated && s.ApplicationDueDate < DateTime.Now);
						break;
				}	
			}
			if (!string.IsNullOrEmpty(txtNameSearch.Text))
			{
				scholarships = scholarships.Where(s => s.Name.Contains(txtNameSearch.Text));
			}
			return scholarships;
		}

		#region DataSource method

		public List<Organization> SelectProvidersForDropDown()
		{
			List<Organization> providerOrgs = new List<Organization>();
			var providers = (from s in AllScholarships
							 orderby s.Provider.Name
							 select s.Provider).Distinct();
			foreach (var provider in providers)
			{
				providerOrgs.Add(provider);
			}
			return providerOrgs;
		}
		public IEnumerable<Organization> SelectIntermediariesForDropDown()
		{
			List<Organization> intermediaryOrgs = new List<Organization>();
			var providers = (from s in AllScholarships
							 where s.Intermediary != null
							 orderby s.Intermediary.Name
							 select s.Intermediary).Distinct();
			foreach (var provider in providers)
			{
				intermediaryOrgs.Add(provider);
			}
			return intermediaryOrgs;
		}
		public IEnumerable<Scholarship> SelectScholarships()
		{
			return GetScholarships();
		}

		#endregion
	}
}