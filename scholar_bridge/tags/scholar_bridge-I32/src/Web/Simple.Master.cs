﻿using System;
using System.Web.UI;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web
{
    public partial class SimpleMasterPage : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "theWashBoard.org | " + Page.Header.Title;

			PrintViewHeadContainer.Visible = Page.IsInPrintView();
            PrintViewPageHeaderPlaceHolder.Visible = Page.IsInPrintView();
			ScreenViewHeadContainer.Visible = !PrintViewHeadContainer.Visible;
        }
    }
}
