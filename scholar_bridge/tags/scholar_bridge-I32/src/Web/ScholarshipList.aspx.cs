﻿using System;
using System.Diagnostics;
using ScholarBridge.Business;


namespace ScholarBridge.Web
{
	public partial class ScholarshipList: System.Web.UI.Page
	{
		public bool RunningIIS7
		{
			get
			{
				if (GetIISVersion() > 6)
					return true;
				return false;
			}
		}
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Header.Title = "theWashBoard.org - Complete Scholarship List";
			scholarshipXmlDataSource.DataFile = string.Format("~/_ScholarshipData/{0}", ScholarshipListService.SCHOLARSHIP_LIST_FILE_NAME );
			if (!Page.IsPostBack)
			{

			}
		}

		private static int GetIISVersion()
		{
			int version = 0;
			using (var process = Process.GetCurrentProcess()){    
				using (var mainModule = process.MainModule)    
				{   // main module would be w3wp        
					if (mainModule != null) version = mainModule.FileVersionInfo.FileMajorPart;
				}
			}
			return version;
			
		}
	}
}
