﻿<%@ Page Language="C#" MasterPageFile="~/Simple.Master" AutoEventWireup="true" CodeBehind="ScholarshipDetails.aspx.cs" Inherits="ScholarBridge.Web.ScholarshipDetails" Title="Scholarship" %>
<asp:Content ID="metaContent" ContentPlaceHolderID="seoMeta" runat="server">
    <meta name="Description" content="<%= MetaDescription %>">
    <meta name="Keywords" content="<%= MetaKeywords %>">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <br />
    <br />
    <div class="form-iceland-container">
        <div class="form-iceland">
            <label for="scholarshipNameLabel">Scholarship Name:</label><asp:label ID="scholarshipNameLabel" runat="server"></asp:label>
            <br />
            <label for="providerNameLabel">Provider Name:</label><asp:label ID="providerNameLabel" runat="server"></asp:label>
            <br />
            <label for="intermediaryNameLabel">Intermediary Name:</label><asp:label ID="intermediaryNameLabel" runat="server"></asp:label>
            <br />
            <label for="donorNameLabel">Donor Name:</label><asp:label ID="donorNameLabel" runat="server"></asp:label>
            <br />
            <label for="academicYearLabel">Academic Year:</label><asp:label ID="academicYearLabel" runat="server"></asp:label>
            <br />
            <label for="scholarshipAmountLabel">Scholarship Amount:</label><asp:label ID="scholarshipAmountLabel" runat="server"></asp:label>
            <br />
            <label for="applicationStartDateLabel">Application Start Date:</label><asp:label ID="applicationStartDateLabel" runat="server"></asp:label>
            <br />
            <label for="applicationDueDateLabel">Application Due Date:</label><asp:label ID="applicationDueDateLabel" runat="server"></asp:label>
            <br />
            <label for="missionStatementLabel">Mission Statement -</label>
            <br />
            <asp:label ID="missionStatementLabel" runat="server"></asp:label>
            <br />
            <br />

            <div>
                <div >Sign-up to apply for this and other scholarships <a href="<%# ResolveUrl("~/Seeker/Register.aspx") %>">here</a>.    </div>
                <br />
            </div>

            <div > <a href="<%# ResolveUrl("~/Seeker/Default.aspx") %>">Login for existing users</a>  </div>
            <br />
        </div>
    </div>
    
</asp:Content>


