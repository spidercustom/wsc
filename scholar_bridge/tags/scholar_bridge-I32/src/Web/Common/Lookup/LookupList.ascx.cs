﻿using System;
using System.Web.UI;
using JSHelper = ScholarBridge.Web.Common.Lookup.LookupDialogJSHelper;
namespace ScholarBridge.Web.Common.Lookup
{
    public partial class LookupList : UserControl
    {
        public string DialogDiv { get; set; }
        public string BuddyControlId { get; set; }
        public string OtherControlId { get; set; }
        public string OtherTextboxId { get; set; }
        public string OtherLabelId { get; set; }

        public string HiddenIds { get; set; }
        public string OtherControl { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //var selectionId = txtSelection.ClientID;
            var gridId = FindControl("lookuplist_available").ClientID;

            btnAdd.OnClientClick = JSHelper.BuildaddToSelectionsJS(HiddenIds);
            btnRemove.OnClientClick = JSHelper.BuildremoveFromSelectionsJS(false);
            btnRemoveAll.OnClientClick = JSHelper.BuildremoveFromSelectionsJS(true);
            btnSave.OnClientClick = JSHelper.BuildSaveDialogJS(DialogDiv, BuddyControlId, HiddenIds, OtherControlId, OtherTextboxId, OtherLabelId);
            btnCancel.OnClientClick = JSHelper.BuildCancelDialogJS(DialogDiv);
        }
    }
}
