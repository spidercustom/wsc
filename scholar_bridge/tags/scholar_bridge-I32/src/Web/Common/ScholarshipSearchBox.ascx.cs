﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipSearchBox : UserControl
    {
    	public const string SEARCH_BOX_ID = "txtSearch";

        private const string RESULTS_PAGE = "~/Seeker/Scholarships/SearchResults.aspx?val={0}";

         
       
        protected void Page_Load(object sender, EventArgs e)
        {
			if (txtSearch.ID != SEARCH_BOX_ID)
			{
				throw new ArgumentException("The Search Entry text box ID (" + txtSearch.ID + ") does not match the published Static value (" + SEARCH_BOX_ID + ").  Has it been renamed?");
			}
            var page = HttpContext.Current.CurrentHandler as Page;
            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (page != null)
            {
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), txtSearch.ClientID + "_script", GetJS());
            }
            
        }

        private string GetJS()
        {
            return "$(function() {searchBoxFocus('" + txtSearch.ClientID + "'); });";
        }

        protected void SearchBtn_Click(object sender, ImageClickEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            string value = txtSearch.Text;
            Response.Redirect(ResolveUrl(RESULTS_PAGE.Build(value)));
        }
    }
}