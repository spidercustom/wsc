﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Provider.BuildScholarship;

namespace ScholarBridge.Web.Common
{
    public partial class AdditionalCriteriaShow : BaseScholarshipShow
    {
        public override int ResumeFrom { get { return WizardStepName.AdditionalCriteria.GetNumericValue(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);

            if (!Page.IsPostBack)
            {
                AdditionalRequirementsRepeater.DataSource = ScholarshipToView.AdditionalRequirements;
                AdditionalRequirementsRepeater.DataBind();
                RequirementsFooterRow.Visible = ScholarshipToView.AdditionalRequirements.Count <= 0;

                AdditionalCriteriaRepeater.DataSource = ScholarshipToView.AdditionalQuestions;
                AdditionalCriteriaRepeater.DataBind();
            }

            scholarshipAttachments.Attachments = ScholarshipToView.Attachments;
            scholarshipAttachments.View = Page.IsInPrintView()
                                              ? FileList.FileListView.List
                                              : FileList.FileListView.Detail;
        }
    }
}