﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using System.Collections.Generic;

namespace ScholarBridge.Web.Common
{
    
    public delegate void MatchIconHandler(Match match, ImageButton iconButton);
    public delegate void MatchActionButtonHandler(Match match, Button button);
    
    public partial class MatchList : UserControl
    {
        public string LinkTo { get; set; }
        public ListView List
        {
            get { return matchList; }
        }

        public MatchIconHandler IconConfigurator { get; set; }
        public MatchActionButtonHandler ActionButtonConfigurator { get; set;}

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            matchList.ItemCommand += matchList_ItemCommand;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //    BindMatches();
        }

        private IList<Match> matches;
        public IList<Match> Matches
        {
            get
            {
                return matches;
            }
            set
            {
                matches = value;
            }
        }

        public void BindMatches()
        {
            matchList.DataSource = matches;
            matchList.DataBind();
        }

        void matchList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            Console.WriteLine(e.CommandArgument);
        }


        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match)((ListViewDataItem)e.Item).DataItem);

                var link = (LinkButton)e.Item.FindControl("linktoScholarship");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) {Path = ResolveUrl(LinkTo)}.ToString()
                + "?id=" + match.Scholarship.Id + "&print=true";
                link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url  ));

                
            }
        }

        
        public int PageSize
        {
            get { return pager.PageSize; }
            set { pager.PageSize = value; }
        }


        protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }

        protected void matchList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            //pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            //BindMatches();
        }

        protected void matchList_PagePropertiesChanged(object sender, EventArgs e)
        {
            BindMatches();
        }
    }
}