﻿using System.Collections.Generic;
using ScholarBridge.Domain.Location;
using Spider.Common.Core.DAL;
using System.Collections;

namespace ScholarBridge.Data
{
    public interface IStateDAL : IDAL<State>
    {
        State FindByAbbreviation(string abbreviation);
        IList<State> FindAll(IList<string> abbreviations);
        IList<State> FindAll();
    }
}