using NHibernate.Type;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>MessageAction</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class MessageActionType : EnumStringType
    {
        public MessageActionType()
            : base(typeof(MessageAction), 10)
        {
        }
    }
}