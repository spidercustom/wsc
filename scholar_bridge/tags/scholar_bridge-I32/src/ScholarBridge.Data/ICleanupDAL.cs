
namespace ScholarBridge.Data
{
    /// <summary>
    /// Contains non-standard operations that are used for doing out-of-the-ordinary
    /// things that we don't want cluttering up the normal DAL Interfaces and confusing users.
    /// </summary>
    public interface ICleanupDAL
    {
        /// <summary>
        /// Truly deletes all of the User rows from the database if they have not
        /// confirmed their emails in more than <para>days</para> number of days.
        /// 
        /// You normally do not want to call this method. The Delete method of the UserDAL
        /// is the normal way of deleting a user which performs a soft-delete. This
        /// method actually attempts to remove the rows from the database which is not normally
        /// what you want.
        /// </summary>
        /// <param name="days">The number of days of grace period. All older users will be deleted.</param>
        void RemoveNonConfirmedUsersFromDatabase(int days);
    }
}