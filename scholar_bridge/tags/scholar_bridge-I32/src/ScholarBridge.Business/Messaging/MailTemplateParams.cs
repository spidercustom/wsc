using System.Collections.Generic;

namespace ScholarBridge.Business.Messaging
{
    public class MailTemplateParams
    {
        public MailTemplateParams()
        {
            MergeVariables = new Dictionary<string, string>();
        }

        public string From { get; set; }
        public string To { get; set; }

        public Dictionary<string, string> MergeVariables { get; protected set; }

        public void MergeVariablesInto(Dictionary<string,string> orig)
        {
            foreach (var key in orig.Keys)
            {
                MergeVariables[key] = orig[key];
            }
        }
    }
}