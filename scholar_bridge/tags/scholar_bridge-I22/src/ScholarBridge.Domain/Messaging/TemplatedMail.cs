using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ScholarBridge.Domain.Messaging
{
    public class TemplatedMail
    {
        private const string VARIABLE_TAG = "##";
        private const string VARIABLE_FORMAT = VARIABLE_TAG + "{0}" + VARIABLE_TAG;

        public TemplatedMail() {}

        public TemplatedMail(string rawText)
        {
            using (var reader = new StringReader(rawText))
            {
                ReadValues(reader);
            }
        }

        public void ReadValues(TextReader reader)
        {
            RawSubject = reader.ReadLine();
            RawBody = reader.ReadToEnd();
        }
        public string RawSubject { get; set; }
        public string Subject (Dictionary<string, string> templateVariables)
        {
            var subjectContent = new StringBuilder(RawSubject);
            foreach (KeyValuePair<string, string> item in templateVariables)
            {
                string key = string.Format(VARIABLE_FORMAT, item.Key);
                subjectContent.Replace(key, item.Value);
            }
            return subjectContent.ToString();
        }

        public string RawBody { get; set; }
       

        public string Body(Dictionary<string, string> templateVariables)
        {
            var bodyContent = new StringBuilder(RawBody);
            foreach (KeyValuePair<string, string> item in templateVariables)
            {
                string key = string.Format(VARIABLE_FORMAT, item.Key);
                bodyContent.Replace(key, item.Value);
            }
            return bodyContent.ToString();
        }
    }
}