﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain.ScholarshipParts
{
    /// <summary>
    /// IMPORTANT: Database views of scholarship seeker match depends up
    /// integer representation of enum elements. If someone changes these
    /// they must also change database views for the matching algorimth to work
    /// correctly.
    /// </summary>
    public enum SeekerProfileAttribute
    {
        [DisplayName("Academic Area")]
        AcademicArea = 0,

        [DisplayName("Academic Program")]
        AcademicProgram = 1,
               
        Career = 2,
        
        City = 3,
        
        Club = 4,
        
        College = 5,
        
        [DisplayName("First Generation")]
        FirstGeneration = 6,
        
        [DisplayName("Community Service/Involvement")]
        CommunityService = 7,
        
        County = 8,
        
        Ethnicity = 9,
        
        Gender = 10,
        
        [DisplayName("Seeker Demographics - Geographic")]
        SeekerGeographicsLocation = 11,
        
        State = 12,
        
        HighSchool = 13,

        [DisplayName("Organization")]
        OrganizationAndAffiliationType = 14,
        
        [DisplayName("Program Length")]
        ProgramLength = 15,
        
        Religion = 16,
        
        [DisplayName("School District")]
        SchoolDistrict = 17,
        
        [DisplayName("School Type")]
        SchoolType = 18,
        
        [DisplayName("Hobby")]
        SeekerHobby = 19,
        
        [DisplayName("Skill")]
        SeekerSkill = 20,
        
        [DisplayName("Seeker Status")]
        SeekerStatus = 21,
        
        [DisplayName("Words")]
        SeekerVerbalizingWord = 22,
        
        [DisplayName("Service Hour")]
        ServiceHour = 23,
        
        [DisplayName("Service Type")]
        ServiceType = 24,
        
        Sport = 25,
        
        [DisplayName("Student Group")]
        StudentGroup = 26,
        
        [DisplayName("Work Hour")]
        WorkHour = 27,
        
        [DisplayName("Work Type")]
        WorkType = 28,
        
        GPA = 29,
        
        [DisplayName("Class Rank")]
        ClassRank = 30,
        
		//[DisplayName("SAT Writing Score")]
		//SAT = 31,
        
		//[DisplayName("ACT English Score")]
		//ACT = 32,
        
        Honor = 33,
        
        [DisplayName("AP Credits Earned")]
        APCreditsEarned = 34,
        
        [DisplayName("IB Credits Earned")]
        IBCreditsEarned = 35,

		[DisplayName("SAT Writing Score")]
		SATWriting = 36,

		[DisplayName("SAT Critical Reading Score")]
		SATCriticalReading = 37,

		[DisplayName("SAT Mathematics Score")]
		SATMathematics = 38,

		[DisplayName("ACT English Score")]
		ACTEnglish = 39,

		[DisplayName("ACT Mathematics Score")]
		ACTMathematics = 40,

		[DisplayName("ACT Reading Score")]
		ACTReading = 41,

		[DisplayName("ACT Science Score")]
		ACTScience = 42
	}
}
