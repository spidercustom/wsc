using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IIntermediaryDAL : IOrganizationDAL<Intermediary>
    {
    }
}