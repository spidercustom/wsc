﻿using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class SupportDAL : LookupDAL<Support>, ISupportDAL
    {
        public IList<Support> FindAll(SupportType supportType)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("SupportType", supportType))
                .List<Support>();
        }
    }
}