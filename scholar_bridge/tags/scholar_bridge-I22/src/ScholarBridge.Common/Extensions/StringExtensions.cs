﻿using System.Text.RegularExpressions;

namespace ScholarBridge.Common.Extensions
{
    public static class StringExtensions
    {
        public static string Build(this string stringToFormat, params object[] args)
        {
            return string.Format(stringToFormat, args);
        }

        public static double? CreateNullableDouble(this string stringToConvert)
        {
            double? result;
            if (string.IsNullOrEmpty(stringToConvert))
                result = null;
            else
                result = double.Parse(stringToConvert);

            return result;
        }

        public static string BuildLinks(this string strContent)
        {
            if (strContent == null)
                return "";
            var urlregex = new Regex("(http:\\/\\/([\\w.]+\\/?)\\S*)", (RegexOptions.IgnoreCase | RegexOptions.Compiled));

            strContent = urlregex.Replace(strContent, "<a href=\"$1\" target=\"_blank\">$1</a>");

            var secureurlregex = new Regex("(https:\\/\\/([\\w.]+\\/?)\\S*)", (RegexOptions.IgnoreCase | RegexOptions.Compiled));

            strContent = secureurlregex.Replace(strContent, "<a href=\"$1\" target=\"_blank\">$1</a>");

            var emailregex = new Regex("([a-zA-Z_0-9.-]+\\@[a-zA-Z_0-9.-]+\\.\\w+)", (RegexOptions.IgnoreCase | RegexOptions.Compiled));

            strContent = emailregex.Replace(strContent, "<a href=mailto:$1>$1</a>");

            return strContent;
        }
        public static string PreserveLineBreaks(this string strContent)
        {
            return strContent.Replace("\n", "<br />");

        }
    }
}
