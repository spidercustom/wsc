using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(6)]
    public class AddProvider : Migration
    {
        public static string TABLE_NAME = "SB_Provider";
        public const string PRIMARY_KEY_COLUMN = "Id";
        protected static readonly string FK_LAST_UPDATED_BY = string.Format("FK_{0}_LastUpdatedBy", TABLE_NAME);
        protected static readonly string FK_STATE = string.Format("FK_{0}_AddressState", TABLE_NAME);

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                              new Column("Name", DbType.String, 50, ColumnProperty.NotNull),
                              new Column("Website", DbType.String, 128, ColumnProperty.Null),
                              new Column("TaxId", DbType.String, 10, ColumnProperty.NotNull),

                              new Column("AddressStreet1", DbType.String, 50, ColumnProperty.NotNull),
                              new Column("AddressStreet2", DbType.String, 50, ColumnProperty.Null),
                              new Column("AddressCity", DbType.String, 50, ColumnProperty.NotNull),
                              new Column("AddressPostalCode", DbType.String, 10, ColumnProperty.NotNull),
                              new Column("AddressState", DbType.String, 2, ColumnProperty.NotNull),

                              new Column("PhoneNumber", DbType.String, 10, ColumnProperty.Null),
                              new Column("FaxNumber", DbType.String, 10, ColumnProperty.Null),
                              new Column("OtherPhoneNumber", DbType.String, 10, ColumnProperty.Null),

                              new Column("LastUpdatedBy", DbType.Int32, ColumnProperty.NotNull),
                              new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.Null, "(getdate())")
                );

            Database.AddForeignKey(FK_STATE, TABLE_NAME, "AddressState", "SB_State", "Abbreviation");
            Database.AddForeignKey(FK_LAST_UPDATED_BY, TABLE_NAME, "LastUpdatedBy", AddUsers.TABLE_NAME, AddUsers.PRIMARY_KEY_COLUMN);
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_STATE);
            Database.RemoveForeignKey(TABLE_NAME, FK_LAST_UPDATED_BY);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}