﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	/// <summary>
	/// High school names are quite long
	/// </summary>
	[Migration(92)]
	public class AlterHighschoolTable : Migration
	{
		private const string HIGH_SCHOOL_TABLE = "SBHighSchoolLUT";
		private const string DISTRICT_TABLE = "SBSchoolDistrictLUT";
		private const string COLLEGE_TABLE = "SBCollegeLUT";

		private const string HIGH_SCHOOL_COL = "HighSchool";
		private const string HS_DESCRIPTION_COL = "Description";

		private const string COLLEGE_COL = "College";
		private const string COLLEGE_DESCRIPTION_COL = "Description";

		private const string SD_SCHOOL_DISTRICT_COL = "SchoolDistrict";
		private const string SD_DESCRIPTION_COL = "Description";

		private const string SB_DISTRICT_ID_COL = "SBSchoolDistrictIndex";
		private const string HS_CITY_COL = "AddressCity";
		private const string HS_STATE_COL = "AddressState";
		private const string FK_SCHOLARSHIP = "FK_HighSchool_District";

		public override void Up()
		{
			var DistrictColumn = new Column(SD_SCHOOL_DISTRICT_COL, DbType.String, 80, ColumnProperty.NotNull);
			var DistrictDescriptionColumn = new Column(SD_DESCRIPTION_COL, DbType.String, 150, ColumnProperty.NotNull);

			var highSchoolColumn = new Column(HIGH_SCHOOL_COL, DbType.String, 80, ColumnProperty.NotNull);
			var highSchoolDescriptionColumn = new Column(HS_DESCRIPTION_COL, DbType.String, 150, ColumnProperty.NotNull);

			var collegeColumn = new Column(COLLEGE_COL, DbType.String, 80, ColumnProperty.NotNull);
			var collegeDescriptionColumn = new Column(COLLEGE_DESCRIPTION_COL, DbType.String, 150, ColumnProperty.NotNull);

			var highSchoolDistrictColumn = new Column(SB_DISTRICT_ID_COL, DbType.Int32, ColumnProperty.Null);
			var highSchoolCityColumn = new Column(HS_CITY_COL, DbType.String, 50, ColumnProperty.Null);
			var highSchoolStateColumn = new Column(HS_STATE_COL, DbType.String, 2, ColumnProperty.Null);

			Database.ChangeColumn(HIGH_SCHOOL_TABLE, highSchoolColumn);
			Database.ChangeColumn(HIGH_SCHOOL_TABLE, highSchoolDescriptionColumn);

			Database.AddColumn(HIGH_SCHOOL_TABLE, highSchoolDistrictColumn);
			Database.AddColumn(HIGH_SCHOOL_TABLE, highSchoolCityColumn);
			Database.AddColumn(HIGH_SCHOOL_TABLE, highSchoolStateColumn);

			Database.ChangeColumn(DISTRICT_TABLE, DistrictColumn);
			Database.ChangeColumn(DISTRICT_TABLE, DistrictDescriptionColumn);

			Database.ChangeColumn(COLLEGE_TABLE, collegeColumn);
			Database.ChangeColumn(COLLEGE_TABLE, collegeDescriptionColumn);

			Database.AddForeignKey(FK_SCHOLARSHIP, HIGH_SCHOOL_TABLE, SB_DISTRICT_ID_COL, DISTRICT_TABLE, SB_DISTRICT_ID_COL);
		}

		public override void Down()
		{
			var highSchoolColumn = new Column(HIGH_SCHOOL_COL, DbType.String, 20, ColumnProperty.NotNull);
			var highSchoolDescriptionColumn = new Column(HS_DESCRIPTION_COL, DbType.String, 50, ColumnProperty.NotNull);
			var DistrictColumn = new Column(SD_SCHOOL_DISTRICT_COL, DbType.String, 20, ColumnProperty.NotNull);
			var DistrictDescriptionColumn = new Column(SD_DESCRIPTION_COL, DbType.String, 50, ColumnProperty.NotNull);
			var collegeColumn = new Column(COLLEGE_COL, DbType.String, 80, ColumnProperty.NotNull);
			var collegeDescriptionColumn = new Column(COLLEGE_DESCRIPTION_COL, DbType.String, 150, ColumnProperty.NotNull);

			Database.RemoveForeignKey(HIGH_SCHOOL_TABLE, FK_SCHOLARSHIP);

			Database.ChangeColumn(HIGH_SCHOOL_TABLE, highSchoolColumn);
			Database.ChangeColumn(HIGH_SCHOOL_TABLE, highSchoolDescriptionColumn);

			Database.ChangeColumn(DISTRICT_TABLE, DistrictColumn);
			Database.ChangeColumn(DISTRICT_TABLE, DistrictDescriptionColumn);

			Database.ChangeColumn(COLLEGE_TABLE, collegeColumn);
			Database.ChangeColumn(COLLEGE_TABLE, collegeDescriptionColumn);

			Database.RemoveColumn(HIGH_SCHOOL_TABLE, SB_DISTRICT_ID_COL);
			Database.RemoveColumn(HIGH_SCHOOL_TABLE, HS_CITY_COL);
			Database.RemoveColumn(HIGH_SCHOOL_TABLE, HS_STATE_COL);
		}
	}
}