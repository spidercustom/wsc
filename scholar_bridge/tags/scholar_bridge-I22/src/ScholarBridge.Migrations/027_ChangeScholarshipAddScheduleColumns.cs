﻿using Migrator.Framework;
using System.Data;

namespace ScholarBridge.Migrations
{
    [Migration(27)]
    public class ChangeScholarshipAddScheduleColumns : Migration
    {
        public override void Up()
        {
            Database.AddColumn(AddScholarship.TABLE_NAME, "ScheduleType", DbType.String, 10, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, "ScheduleId", DbType.Int32, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "ScheduleType");
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "ScheduleId");
        }

    }
}
