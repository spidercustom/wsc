﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Message
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.Messages);

            if (User.IsInRole(Role.PROVIDER_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for notifications of scholarship status, scholarship applications and awards, and requests from other scholarship organizations.";

            else if (User.IsInRole(Role.INTERMEDIARY_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for requests from other scholarship organizations, and notifications of scholarship status, applications and awards.";
            else
                lblPageText.Visible = false;
      
        }
    }
}