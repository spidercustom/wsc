﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Web.Extensions;
namespace ScholarBridge.Web.Admin
{
    public partial class Default : Page
	{
		#region Properties
		public IProviderService ProviderService {get;set;}
		public IIntermediaryService IntermediaryService { get; set; }
		public IApplicationService ApplicationService { get; set; }
		public ISeekerService SeekerService { get; set; }


		private string providerSortColumn
		{
			get
			{
				if (ViewState["providerSortColumn"] == null)
				{
					ViewState["providerSortColumn"] = "Name";
				}
				return (string)ViewState["providerSortColumn"];
			}
			set
			{
				ViewState["ApprovedProviders"] = value;
			}

		}

		private string intermediarySortColumn
		{
			get
			{
				if (ViewState["intermediarySortColumn"] == null)
				{
					ViewState["intermediarySortColumn"] = "Name";
				}
				return (string)ViewState["intermediarySortColumn"];
			}
			set
			{
				ViewState["ApprovedProviders"] = value;
			}

		}
		private IList<Domain.Organization> ApprovedProviders
		{
			get
			{
				if (Session["ApprovedProviders"] == null)
				{
					var providers = ProviderService.FindActiveOrganizations();
					IEnumerable<Domain.Organization> providerEnum = from p in providers select p as Domain.Organization;
					Session["ApprovedProviders"] = providerEnum.ToList();
				}
				return (IList<Domain.Organization>)Session["ApprovedProviders"];
			}
			set
			{
				Session["ApprovedProviders"] = value;
			}

		}

		private IList<Domain.Organization> ApprovedIntermediaries
		{
			get
			{
				if (Session["ApprovedIntermediaries"] == null)
				{
					var intermediaries = IntermediaryService.FindActiveOrganizations();
					IEnumerable<Domain.Organization> intermediaryEnum = from p in intermediaries select p as Domain.Organization;
					Session["ApprovedIntermediaries"] = intermediaryEnum.ToList();
				}
				return (IList<Domain.Organization>)Session["ApprovedIntermediaries"];
			}
			set
			{
				Session["ApprovedIntermediaries"] = value;
			}

		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
        {
			if (!Page.IsPostBack)
			{
				InitializeObjects();
				LoadStatistics();
			}
		}

		#region Private Methods
		private void LoadStatistics()
    	{
    		providerCountLabel.Text = ApprovedProviders.Count().ToString();
    		intermediaryCountLabel.Text = ApprovedIntermediaries.Count().ToString();
			registeredSeekersLabel.Text = SeekerService.CountActivatedSeekers().ToString();
			activeProfilesLabel.Text = SeekerService.CountSeekersWithActiveProfiles().ToString();
			appsStartedLabel.Text = ApplicationService.CountAllSavedButNotSubmitted().ToString();
			appsSubmittedLabel.Text = ApplicationService.CountAllSubmitted().ToString();

			providersHidden.Value = "true";
    		intermediariesHidden.Value = "true";
    	}

    	private void InitializeObjects()
		{
			ApprovedProviders = null;
			ApprovedIntermediaries = null;
		}
		protected virtual void SetResponseHeadersForExcel(GridView gridView)
		{
			Response.Clear();

			Response.AddHeader("content-disposition", "attachment; filename=FileName.xls");

			Response.Charset = "";

			gridView.AllowPaging = false;
			gridView.DataBind();

			// If you want the option to open the Excel file without saving then
			// comment out the line below
			// Response.Cache.SetCacheability(HttpCacheability.NoCache);

			Response.ContentType = "application/vnd.xls";

			System.IO.StringWriter stringWrite = new System.IO.StringWriter();

			HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

			gridView.RenderControl(htmlWrite);

			Response.Write(stringWrite.ToString());

			Response.End();
			gridView.AllowPaging = true;
		}


		#endregion

		#region datasource methods

		public IList<Domain.Organization> SelectProviders(string sortColumnName)
		{
			if (sortColumnName != providerSortColumn)
			{
				ApprovedProviders = sortOrganizations(ApprovedProviders, sortColumnName);
				providerSortColumn = sortColumnName;
			}
			return ApprovedProviders; 
		}

		public IEnumerable<Domain.Organization> SelectIntermediaries(string sortColumnName)
		{
			if (sortColumnName != intermediarySortColumn)
			{
				ApprovedProviders = sortOrganizations(ApprovedProviders, sortColumnName);
				intermediarySortColumn = sortColumnName;
			}
			return ApprovedIntermediaries;
		}

		private static IList<Domain.Organization> sortOrganizations(IList<Domain.Organization> orgs, string sortColumn)
		{
			string[] sortWords = sortColumn.Split(' ');

			bool ascending = true;
			string sortColumnName = string.Empty;

			if (sortWords.Length > 0)
			{
				sortColumnName = sortWords[0];
				if (sortWords.Length > 1)
					ascending = false;
			}

			if (sortColumnName == string.Empty)
				sortColumnName = "Name";

			switch (sortColumnName)
			{
				case "Name":
					return orgs.OrderBy(o => o.Name, ascending).ToList();
				case "Website":
					return orgs.OrderBy(o => o.Website, ascending).ToList();
				case "TaxId":
					return orgs.OrderBy(o => o.TaxId, ascending).ToList();
				case "AdminUser":
					return orgs.OrderBy(o => o.AdminUser.Name, ascending).ToList();
				case "ApprovedStatusDate":
					return orgs.OrderBy(o => o.ApprovalStatusDate, ascending).ToList();
			}
			return orgs;
		}

		#endregion

		#region event handlers

		protected void providerDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
    		e.ObjectInstance = this;
		}

    	protected void intermediaryDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
			e.ObjectInstance = this;
		}

		protected void saveProvidersToExcel_Click(object sender, EventArgs e)
		{
			SetResponseHeadersForExcel(providerGrid);
		}

    	protected void saveIntermediariesToExcel_Click(object sender, EventArgs e)
    	{
			SetResponseHeadersForExcel(intermediaryGrid);
    	}

		/// <summary>
		/// this empty override prevents asp from killing the GridView render to excel
		/// </summary>
		/// <param name="control"></param>
		public override void VerifyRenderingInServerForm(Control control)
		{
			// Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
		}

		#endregion
	}
}
