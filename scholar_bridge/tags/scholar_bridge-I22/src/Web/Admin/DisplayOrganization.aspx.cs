﻿using System;
using System.Web.UI;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Admin
{
	public partial class DisplayOrganization : Page
    {
		public IIntermediaryService IntermediaryService { get; set; }
		public IProviderService ProviderService { get; set; }

        public int CurrentOrganizationId
        {
            get { return Int32.Parse(Request["id"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			if (!Page.IsPostBack)
			{
				if (CurrentOrganizationId > 0)
				{
					Domain.Provider provider = ProviderService.FindById(CurrentOrganizationId);
					if (provider == null)
					{
						Domain.Intermediary intermediary = IntermediaryService.FindById(CurrentOrganizationId);
						if (intermediary != null)
						{
							showOrg.Organization = intermediary;
							orgName.Text = "Intermediary: " + intermediary.Name;
						}
						else
						{
							throw new ArgumentException("Cannot find organization for ID: " + CurrentOrganizationId);
						}
					}
					else
					{
						showOrg.Organization = provider;
						orgName.Text = "Provider: " + provider.Name;

					}
				}
				else
				{
					throw new ArgumentException("Invalid organization id passed to " + GetType() + ": " + CurrentOrganizationId);
				}
			}
        }

    }
}
