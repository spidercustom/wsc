﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Admin.Default" %>
<%@ Register TagPrefix="sb" TagName="ProviderRelationshipList" Src="~/Common/ProviderRelationshipList.ascx" %>
<%@ Register TagPrefix="sb" TagName="OrgUserList" Src="~/Common/OrgUserList.ascx" %>
<%@ Register TagPrefix="sb" TagName="UserDetailsCompact" Src="~/Common/UserDetailsCompact.ascx" %>
<%@ Register TagPrefix="sb" TagName="EditOrganization" Src="~/Common/EditOrganization.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgProfile.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div class="tabs">
            <ul>
                <li><a href="#organization-tab"><span>Organization</span></a></li>
                <li><a href="#user-tab"><span>Users</span></a></li>
                <li><a href="#relationship-tab"><span>Relationships</span></a></li>
            </ul>
            <div id="organization-tab">
               <p class="FormHeader">Organization Information</p>
            
                <sb:editorganization ID="editOrg" runat="server" 
                            OnOrganizationSaved="editOrg_OnOrganizationSaved" />
               
                 <hr />  
                 <p></p>         
                <p class="FormHeader">Organization Administrator</p>
                <sb:UserDetailsCompact ID="adminDetails1" runat="server" />
                
            </div>
                  
            <div id="user-tab">
                <p class="FormHeader">Organization Administrator</p>
                <sb:UserDetailsCompact ID="adminDetails" runat="server" />
               
                <P>Only the Organization Administrator has access to update information and add users for your organization. </p>
                <hr />
                <p class="FormHeader">Organization Users</p>
                <sbCommon:AnchorButton  ID="createUserLink" runat="server" Text="Create User" />
                
                <sb:OrgUserList id="orgUserList" runat="server" LinkTo="/Provider/Users/Show.aspx" />
                <br />
                    
            </div>
                  
            <div id="relationship-tab">
                <p class="FormHeader">Organization Relationships</p>
                    <p>You may request a relationship with an Intermediary organization. The Intermediary organization must approve your request. Establishing a relationship with an Intermediary will let you designate them as a scholarship intermediary on specific scholarships and granting them administrative access to the   scholarship. You should contact the Organization directly to determine how they will assist in the administration of your scholarship.</p>
                     
                         
                     
                 <sbCommon:AnchorButton ID="addRequestLink" runat="server" Text="Add Request" />
                    <sb:ProviderRelationshipList ID="relationshiplist" runat="server" InactivateLinkTo="~/Provider/Relationships/Inactivate.aspx" />
            </div>
</div>     







   
</asp:Content>
