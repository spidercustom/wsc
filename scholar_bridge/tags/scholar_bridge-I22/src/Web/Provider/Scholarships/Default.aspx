<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Default" Title="Provider | Scholarships" %>

<%@ Register TagPrefix="sb" TagName="ScholarshipList" Src="~/Common/ScholarshipList.ascx" %>
<%@ Register TagPrefix="sb" TagName="ScholarshipListViewOptions" Src="~/Common/ScholarshipListViewOptions.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgScholarship.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div id="CenterContent">
    <div id="HomeContentLeft">
    </div>
    <div id="HomeContentRight">
    </div>

    <sb:ScholarshipListViewOptions id="scholarshipsListViewOptions" runat="server" 
        OnUpdateView="UpdateViewBtn_Click" />
        

    <sb:ScholarshipList id="scholarshipsList" runat="server" 
        ViewActionLink="~/Provider/Scholarships/Show.aspx"
        EditActionLink="~/Provider/BuildScholarship/Default.aspx"  />            
</div>
</asp:Content>
