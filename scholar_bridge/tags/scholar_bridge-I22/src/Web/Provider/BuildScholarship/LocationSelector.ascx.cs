﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Data;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.Location;
using ScholarBridge.Web.Common.Lookup;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class LocationSelector : System.Web.UI.UserControl
    {
        private const string SHOW_SELECTED_LOCATION_CONTROL_CALL_TEMPLATE = "javascript:ShowStateDependantLocationControl('{0}');";

        private static readonly string[] SupportedStateAbbreviations = new[] { "WA" };
        public IStateDAL StateService { get; set; }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            SetupOnClick();
            SetupFilterScript();
            if (!IsPostBack)
            {
                PopulateStates();
            }
        }

        private void SetupFilterScript()
        {
            var builtScript = SETUP_FILTER_FUNCTION_TEMPLATE.Build(
                StateDropDownControl.ClientID,
                CountyControlDialogButton.UserDataClientID,
                CityControlDialogButton.UserDataClientID,
                SchoolDistrictControlDialogButton.UserDataClientID,
                HighSchoolControlDialogButton.UserDataClientID);

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "SETUP_FILTER", builtScript, true);
        }

        public void PopulateScreen(IMatchLocationCriteria matchLocation)
        {
            if (matchLocation == null) 
                throw new ArgumentNullException("matchLocation");

            if (null == matchLocation.State)
                return;
            
            StateDropDownControl.SelectedValue = matchLocation.State.Abbreviation;
            RadioButton radioButton;
            LookupDialog lookupDialog;
            GetStateDependentControls(matchLocation.StateDependentLocation, out radioButton, out lookupDialog);
            if (null != radioButton)
                radioButton.Checked = true;
            if (null != lookupDialog)
                lookupDialog.Keys = matchLocation.StateDependentLocations.CommaSeparatedIds();
        }

        public void PopulateObject(IMatchLocationCriteria matchLocationCriteria)
        {
            if (matchLocationCriteria == null) throw new ArgumentNullException("matchLocationCriteria");
            
            var state = StateService.FindByAbbreviation(StateDropDownControl.SelectedValue);
            matchLocationCriteria.State = state;
            var list = new List<ILookup>();
            if (CountyRadioButton.Checked)
            {
                CountyControlDialogButton.PopulateListFromSelection(list);
            }
            else if (CityRadioButton.Checked)
            {
                CityControlDialogButton.PopulateListFromSelection(list);
            }
            else if (SchoolDistrictRadioButton.Checked)
            {
                SchoolDistrictControlDialogButton.PopulateListFromSelection(list);
            }
            else if (HighSchoolRadioButton.Checked)
            {
                HighSchoolControlDialogButton.PopulateListFromSelection(list);
            }
            else
            {
                throw new NotSupportedException();
            }
            matchLocationCriteria.ResetStateDependentLocations(list);

        }

        private void GetStateDependentControls(StateDependentLocation stateDepentLocation, out RadioButton radioButton, out LookupDialog lookupDialog)
        {
            radioButton = null;
            lookupDialog = null;
            switch (stateDepentLocation)
            {
                case StateDependentLocation.None:
                    break;
                case StateDependentLocation.County:
                    radioButton = CountyRadioButton;
                    lookupDialog = CountyControlDialogButton;
                    break;
                case StateDependentLocation.City:
                    radioButton = CityRadioButton;
                    lookupDialog = CityControlDialogButton;
                    break;
                case StateDependentLocation.SchoolDistrict:
                    radioButton = SchoolDistrictRadioButton;
                    lookupDialog = SchoolDistrictControlDialogButton;
                    break;
                case StateDependentLocation.HighSchool:
                    radioButton = HighSchoolRadioButton;
                    lookupDialog = HighSchoolControlDialogButton;
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        private void SetupOnClick()
        {
            CountyRadioButton.Attributes.Add("onclick", SHOW_SELECTED_LOCATION_CONTROL_CALL_TEMPLATE.Build(CountyContainerControl.ClientID));
            CityRadioButton.Attributes.Add("onclick", SHOW_SELECTED_LOCATION_CONTROL_CALL_TEMPLATE.Build(CityContainerControl.ClientID));
            SchoolDistrictRadioButton.Attributes.Add("onclick", SHOW_SELECTED_LOCATION_CONTROL_CALL_TEMPLATE.Build(SchoolDistrictContainerControl.ClientID));
            HighSchoolRadioButton.Attributes.Add("onclick", SHOW_SELECTED_LOCATION_CONTROL_CALL_TEMPLATE.Build(HighSchoolContainerControl.ClientID));
        }

        private void PopulateStates()
        {
            StateDropDownControl.DataSource = StateService.FindAll(SupportedStateAbbreviations.ToList());
            StateDropDownControl.DataTextField = "Name";
            StateDropDownControl.DataValueField = "Abbreviation";
            StateDropDownControl.DataBind();
            StateDropDownControl.SelectedIndex = 0;
        }

        private const string SETUP_FILTER_FUNCTION_TEMPLATE =
        @"
            $(window).load(function() {{
                setupStateFilter();
            }});

            function setupStateFilter()
            {{
                var selectedStateAbbriviation = $('select#{0} option:selected').val();
                $('{1}').val(selectedStateAbbriviation);
                $('{2}').val(selectedStateAbbriviation);
                $('{3}').val(selectedStateAbbriviation);
                $('{4}').val(selectedStateAbbriviation);
            }}
        ";

    }
}