﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.FundingProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<h3>Build Scholarship – Financial Need</h3>



<div class="form-iceland-container">
    <div class="form-iceland">
        <asp:PlaceHolder ID="FAFSAContainer" runat="server">
            <p class="form-section-title">FAFSA </p>
            
            <label>FAFSA Required?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FAFSARequiredControl" Text="Yes" runat="server" GroupName="FAFSARequired" />
                <asp:RadioButton ID="FAFSANotRequiredControl" Text="No" runat="server" GroupName="FAFSARequired"/>
            </div>
            <br />

            <label>FAFSA defined EFC required?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FAFSAEFCRequiredControl" Text="Yes" runat="server" GroupName="FAFSAEFCRequired" />
                <asp:RadioButton ID="FAFSAEFCNotRequiredControl" Text="No" runat="server" GroupName="FAFSAEFCRequired"/>
            </div>
            <br />
        </asp:PlaceHolder> 
        <hr />
        <asp:PlaceHolder ID="FinancialInformationContainer" runat="server">
            <p class="form-section-title">Financial Information</p>
            
            <label>Number of Family Members in Household required?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FamilyMembersInHouseHoldRequiredControl" Text="Yes" runat="server" GroupName="FamilyMembersInHouseHoldRequired" />
                <asp:RadioButton ID="FamilyMembersInHouseHoldNotRequiredControl" Text="No" runat="server" GroupName="FamilyMembersInHouseHoldRequired"/>
            </div>
            <br />

            <label>Number of Family Dependents attending College required?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FamilyDependentsAttendingCollegeRequiredControl" Text="Yes" runat="server" GroupName="FamilyDependentsAttendingCollegeRequired" />
                <asp:RadioButton ID="FamilyDependentsAttendingCollegeNotRequiredControl" Text="No" runat="server" GroupName="FamilyDependentsAttendingCollegeRequired"/>
            </div>
            <br />

            <label>Family income required?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FamilyIncomeRequiredControl" Text="Yes" runat="server" GroupName="FamilyIncomeRequired" />
                <asp:RadioButton ID="FamilyIncomeNotRequiredControl" Text="No" runat="server" GroupName="FamilyIncomeRequired"/>
            </div>
            <br />
            <asp:PlaceHolder ID="FamilyIncomeRangeContainer" runat="server">
            </asp:PlaceHolder>
        </asp:PlaceHolder>     
        <hr />
        <asp:PlaceHolder ID="SituationsContainer" runat="server">
            <label>Situations the Scholarship will Fund</label>
            <div class="control-set">
                <asp:RadioButton ID="SupportSituationUsageMinimumControl" runat="server" GroupName="UsageSelector" Text="Minimum" onclick="javascript:$('#SupportSituationContainer :input').attr('disabled', false)" />
                <br />
                <asp:RadioButton ID="SupportSituationUsagePreferenceControl" runat="server" GroupName="UsageSelector" Text="Preference" onclick="javascript:$('#SupportSituationContainer :input').attr('disabled', false)" />
                <br />
                <asp:RadioButton ID="SupportSituationUsageNotUsedControl" runat="server" GroupName="UsageSelector" Text="Not Used" Checked="true" onclick="javascript:$('#SupportSituationContainer :input').attr('disabled', true)" />
                <script type="text/javascript">
                //<![CDATA[
                    $(document).ready(function() {
                        var query = '#<%= SupportSituationUsageNotUsedControl.ClientID %>';
                        var notUsedIsChecked = $(query).attr("checked");
                        $('#SupportSituationContainer :input').attr('disabled', notUsedIsChecked);
                    });
                //]]>
                </script>

            </div>
            
            <div id="SupportSituationContainer" class="control-set">
                <asp:CheckBoxList ID="TypesOfSupport" runat="server" />
            </div>
            <br />
            
        </asp:PlaceHolder> 
    </div>
</div>