﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class AdminScholarshipNotes : UserControl
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship Scholarship { get; set; }

        protected void Page_PreRender(object sender, EventArgs e)
        {
			if (Scholarship.AdminNotes != null)
				notesLiteral.Text = Scholarship.AdminNotes.ToStringTableFormat();
        }

        protected void addNotes_Click(object sender, EventArgs e)
        {
            ReturnToTab();

            Scholarship.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            Scholarship.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            ScholarshipService.Save(Scholarship);
        	newNotesTb.Text = string.Empty;
        }

        private void ReturnToTab()
        {
            // This is a neat hack to get the page to reload into the proper tab
            Page.ClientScript.RegisterStartupScript(GetType(), "anchor", "location.hash = '#notes-tab';", true);
        }
    }
}