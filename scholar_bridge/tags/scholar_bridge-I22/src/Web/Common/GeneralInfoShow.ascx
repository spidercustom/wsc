﻿<%@ Import Namespace="ScholarBridge.Web.Extensions"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.GeneralInfoShow" %>

<div class="recordinfo">
<p class="form-section-title section-level-1">General Information</p>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>

<table class="viewonlyTable">
    <tr>
        <th>Scholarship Name:</th>
        <td ><asp:Literal  ID="lblName" runat="server"  /></td>
    </tr>
    <tr>
        <th>Academic Year:</th>
        <td ><asp:Literal  ID="lblAcademicYear" runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Date Scholarship activated:</th>
        <td><asp:Literal ID="lblScholarshipActivationDate" runat="server"/></td>
    </tr>
    <tr>
        <th scope="row">Donor:</th>
        <td><asp:Literal ID="lblDonor" runat="server" /></td>
    </tr>
    <tr>
        <th scope="row">Mission Statement:</th>
        <td><asp:Literal ID="lblMission" runat="server" /></td>
    </tr>
    <tr class="exclude-in-print" runat="server" visible="<% #!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this.Page) %>">
        <th scope="row">Program Guidelines: 
            <span id="EditControlContainer" runat="server">
                [<asp:HyperLink ID="editProgramGuidelinesLnk" runat="server">Edit</asp:HyperLink>]
            </span>    
        </th>
        <td><asp:Literal ID="lblProgramGuidelines"  runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Schedule
        <span id="EditScheduleContainer" runat="server">
                [<asp:HyperLink ID="editScheduleLnk" runat="server">Edit</asp:HyperLink>]
            </span>    
        </th>
        <td>
            <table>
                <tbody>
                    <tr>
                        <th>Application Start On:</th>
                        <td><asp:Literal  ID="lblAppStart" runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Application Due On:</th>
                        <td><asp:Literal ID="lblAppDue"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Award On:</th>
                        <td><asp:Literal ID="lblAwardOn"  runat="server"  /></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <th scope="row">Annual Amount of Support:</th>
        <td><asp:Literal ID="lblAnnualSupportAmount" runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Number of Awards:</th>
        <td><asp:Literal ID="lblNumberOfAwards" runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Term of Support:</th>
        <td><asp:Literal ID="lblTermOfSupport" runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Award Amount:</th>
        <td><asp:Literal ID="lblAwardAmount" runat="server"  /></td>
    </tr>
</table>

<hr />
<p class="form-section-title section-level-2">Contact Information</p>
<table class="viewonlyTable">
    <tr>
        <th>Provider Name:</th>
        <td><asp:Literal  ID="lblProviderName" runat="server"  /></td>
    </tr>
    <tr>
        <th>Intermediary Name:</th>
        <td><asp:Literal  ID="lblIntermediaryName" runat="server"  /></td>
    </tr>
    <tr>
        <th>Web site:</th>
        <td><asp:HyperLink ID="lblProviderWebSite" runat="server" /></td>
    </tr>
    <tr>
        <th>Address:</th>
        <td>
            <address>
            <asp:Literal  ID="lblAddressLine1" runat="server" /><br />
            <asp:Literal  ID="lblAddressLine2" runat="server" /><br />
            <asp:Literal  ID="lblCityStateZip" runat="server" /><br />
            </address>
        </td>
    </tr>
    <tr>
        <th>Phone:</th>
        <td>
            <asp:Literal  ID="lblPhone" runat="server"  /><br />
        </td>
    </tr>
</table>

</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
