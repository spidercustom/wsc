﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupDialog.ascx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupDialog" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<div class="control-set">
  
    <asp:button  ID="btnLookup" Width="20px" runat="server" Text="..." 
            UseSubmitBehavior="False"/>
    <asp:HiddenField ID="hiddenids" runat="server" Value=""  /> 
    <asp:HiddenField ID="HiddenUserData" runat="server" Value=""  />     
    <asp:HiddenField ID="HiddenOthers" runat="server" Value=""  />     
</div>