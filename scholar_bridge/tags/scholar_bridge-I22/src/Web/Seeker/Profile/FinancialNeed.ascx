﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">What is your financial need?</p>
        <label for="MyChallengeControl">Describe your financial Challenge:</label>
        <asp:TextBox ID="MyChallengeControl" runat="server" TextMode="MultiLine"></asp:TextBox>
        <elv:PropertyProxyValidator ID="MyChallengeValidator" runat="server" ControlToValidate="MyChallengeControl" PropertyName="MyChallenge" SourceTypeName="ScholarBridge.Domain.Seeker"/>
        <br />
        
        <asp:PlaceHolder ID="FAFSAContainer" runat="server">
            <hr />
            <p class="form-section-title"><a href="http://www.fafsa.ed.gov/" class="GreenLink" target="_blank">FAFSA</a> (Free Application for Federal Student Aid)</p>
            <label>Have you completed the FAFSA?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FAFSACompletedControl" Text="Yes" runat="server" GroupName="FAFSAFilled" />
                <asp:RadioButton ID="FAFSANotCompletedControl" Text="No" runat="server" GroupName="FAFSAFilled"/>
            </div>
            <br />
            <label>What is your FAFSA defined EFC (Expected Family Contribution)?</label> 
            <sandTrap:NumberBox ID="ExpectedFamilyContributionControl" runat="server" Precision="0" Enabled="false"/>
            <br />
        </asp:PlaceHolder>
    </div>
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">
        <p class="form-section-title">&nbsp;</p>
        <asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
            <label>Types of Support</label>
            <asp:CheckBoxList ID="TypesOfSupport" runat="server" CssClass="control-set"/>
            <br />
        </asp:PlaceHolder>
        <asp:ScriptManager ID="scriptmanager1" runat="server" ></asp:ScriptManager>
  </div>
</div>
