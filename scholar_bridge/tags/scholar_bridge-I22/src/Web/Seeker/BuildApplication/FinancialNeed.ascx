﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<div class="form-iceland">
    <p class="form-section-title">What is your financial need?</p>
    <asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
        <label>Types of Support</label> 
        <asp:CheckBoxList ID="TypesOfSupport" runat="server" CssClass="control-set"/>
        <br />
    </asp:PlaceHolder>
    
</div>