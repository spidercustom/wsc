using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(194)]
    public class AddApplicationSupportRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationSupportRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSupportLUT"; }
        }
    }
}