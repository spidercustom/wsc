﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(201)]
    public class AddFirstNameLastNameEmailtoApplication : Migration
    {
        private const string TABLE_NAME = "SBApplication";
        private const string FIRST_NAME_COLUMN = "FirstName";
        private const string MIDDLE_NAME_COLUMN = "MiddleName";
        private const string LAST_NAME_COLUMN = "LastName";
        private const string EMAIL_COLUMN = "Email";

        
        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, FIRST_NAME_COLUMN, DbType.String, 40, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, MIDDLE_NAME_COLUMN, DbType.String, 40, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, LAST_NAME_COLUMN, DbType.String, 40, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, EMAIL_COLUMN, DbType.String, 50, ColumnProperty.Null);

             
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, FIRST_NAME_COLUMN);
            Database.RemoveColumn(TABLE_NAME, MIDDLE_NAME_COLUMN);
            Database.RemoveColumn(TABLE_NAME, LAST_NAME_COLUMN);
            Database.RemoveColumn(TABLE_NAME, EMAIL_COLUMN);
        }
    }
}