﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(113)]
    public class AddSchololarshipAttachmentsRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBScholarship"; }
        }

        protected override string SecondTableName
        {
            get { return "SBAttachment"; }
        }
    }
}
