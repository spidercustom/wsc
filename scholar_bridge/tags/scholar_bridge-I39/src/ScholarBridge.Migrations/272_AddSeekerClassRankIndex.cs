﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(272)]
    public class AddSeekerClassRankIndex : Migration
    {
        private const string TABLE_NAME = "SBSeeker";
        private const string FK_CLASSRANK= "FK_SBSeeker_ClassRank";
      

        public readonly string[] NEW_COLUMNS
            = {   "ClassRankIndex"  };

        public override void Up()
        {
            
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Int32, ColumnProperty.Null);
          
            Database.AddForeignKey(FK_CLASSRANK, TABLE_NAME, "ClassRankIndex", "SBClassRankLUT", "SBClassRankIndex");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_CLASSRANK);
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}
