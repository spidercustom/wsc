﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(58)]
    public class UpdateDefaultAdminUsers : Migration
    {
        private readonly string[] Columns = new[] {"IsDeleted", "IsLockedOut"};
        private readonly string[] UpdatedValues = new[] { "False", "False" };
        private readonly string[] OrignialValues = new string[] { null ,  null };

        public override void Up()
        {
            Database.Update("SBUser", Columns, UpdatedValues, "Username in ('admin', 'wscadmin')");
        }

        public override void Down()
        {
            Database.Update("SBUser", Columns, OrignialValues, "Username in ('admin', 'wscadmin')");
        }
    }
}
