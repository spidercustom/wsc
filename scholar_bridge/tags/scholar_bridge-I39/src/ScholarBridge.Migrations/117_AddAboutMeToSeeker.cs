﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(117)]
    public class AddAboutMeToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("PersonalStatement", DbType.String, 1500));
            Database.AddColumn(TABLE_NAME, new Column("MyChallenge", DbType.String, 150));
            Database.AddColumn(TABLE_NAME, new Column("MyGift", DbType.String, 150));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "PersonalStatement");
            Database.RemoveColumn(TABLE_NAME, "MyChallenge");
            Database.RemoveColumn(TABLE_NAME, "MyGift");
        }
    }
}