﻿using Migrator.Framework;
using System.Data;

namespace ScholarBridge.Migrations
{
    [Migration(96)]
    public class ChangeMessageAddRelationshpColumn: Migration
    {
        private const string FK_SBMessage_SBRelationship = "FK_SBMessage_RelatedRelationshipId";

        public override void Up()
        {
            Database.AddColumn(AddMessagesTable.TABLE_NAME, "RelatedRelationshipId", DbType.Int32, ColumnProperty.Null);
            Database.AddForeignKey( FK_SBMessage_SBRelationship, AddMessagesTable.TABLE_NAME,
                                   "RelatedRelationshipId", AddRelationship.TABLE_NAME, AddRelationship.PRIMARY_KEY_COLUMN);
        }

        public override void Down()
        {
            Database.RemoveColumn(AddMessagesTable.TABLE_NAME, "RelatedRelationshipId");
            Database.RemoveForeignKey(AddMessagesTable.TABLE_NAME, FK_SBMessage_SBRelationship);
        }
    }
}
