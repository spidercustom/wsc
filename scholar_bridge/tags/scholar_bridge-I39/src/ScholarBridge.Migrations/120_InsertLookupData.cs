﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(120)]
	public class AddLookupData : Migration
	{
		private const string SBAcademicAreaLUT = "SBAcademicAreaLUT";
		private const string SBAdditionalRequirementLUT = "SBAdditionalRequirementLUT";
		private const string SBAffiliationTypeLUT = "SBAffiliationTypeLUT";
		private const string SBCareerLUT = "SBCareerLUT";
		private const string SBCommunityInvolvementCauseLUT = "SBCommunityInvolvementCauseLUT";
		private const string SBClubLUT = "SBClubLUT";
		private const string SBEthnicityLUT = "SBEthnicityLUT";
		private const string SBSeekerMatchOrganizationLUT = "SBSeekerMatchOrganizationLUT";
		private const string SBReligionLUT = "SBReligionLUT";
		private const string SBSeekerHobbyLUT = "SBSeekerHobbyLUT";
		private const string SBSeekerSkillLUT = "SBSeekerSkillLUT";
		private const string SBSeekerVerbalizingWordLUT = "SBSeekerVerbalizingWordLUT";
		private const string SBServiceTypeLUT = "SBServiceTypeLUT";
		private const string SBSportLUT = "SBSportLUT";
		private const string SBWorkTypeLUT = "SBWorkTypeLUT";

		private const string TRANSCRIPTS  = "Transcripts";
		private const string TRANSCRIPTS_HS = "Transcripts - High School";

		public override void Up()
		{

			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			string tableName = SBAcademicAreaLUT;
			string[] columns = GetInsertColumns(tableName);

			Database.Insert(tableName, columns, new[] {"Architecture", "Architecture", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Business", "Business", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns,
			                new[]
			                	{
			                		"Computer Science/Information Technology", "Computer Science/Information Technology", "0",
			                		adminId.ToString(), DateTime.Now.ToString()
			                	});
			Database.Insert(tableName, columns, new[] {"Education", "Education", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Engineering", "Engineering", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Healthcare", "Healthcare", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Journalism", "Journalism", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Law", "Law", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns,
			                new[] {"Leadership and Policy Studies", "Leadership and Policy Studies", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Mathematics", "Mathematics", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Music", "Music", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Psychology", "Psychology", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Science", "Science", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns,
			                new[] {"Social and Public Service", "Social and Public Service", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Theatre", "Theatre", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Insert(tableName, columns, new[] {"Other", "Other (please specify", "0", adminId.ToString(), DateTime.Now.ToString()});

			tableName = SBAdditionalRequirementLUT;
			columns = GetInsertColumns(tableName);

			Database.Insert(tableName, columns,
			                new[]
			                	{"Transcript - College", "Transcripts from any prior College(s) attended", "0", adminId.ToString(), DateTime.Now.ToString()});
			Database.Update(tableName, 
							columns, 
							new string[]
			                                    	{
			                                    		TRANSCRIPTS_HS,
			                                    		"Transcripts from any prior high school(s) attended",
			                                    		"0",
			                                    		adminId.ToString(), DateTime.Now.ToString()
			                                    	}, 
							"SBAdditionalRequirement = '" + TRANSCRIPTS + "'");




			tableName = SBAffiliationTypeLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Member", "Member", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Participant", "Participant", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Resident", "Resident", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBCareerLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Arts", "Arts", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Architecture", "Architecture", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Business", "Business", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Computer Science/Information Technology", "Computer Science/Information Technology", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Education", "Education", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Engineering", "Engineering", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Healthcare", "Healthcare", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Journalism", "Journalism", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Law", "Law", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Social and Public Service", "Social and Public Service", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBClubLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Book Club",              "Book Club"               , "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Chess Club",             "Chess Club"              , "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Junior Achievement",     "Junior Achievement"      , "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Boy Scouts",             "Boy Scouts"              , "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Girl Scouts",            "Girl Scouts"             , "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBCommunityInvolvementCauseLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Food Drive", "Food Drive", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "United Way", "United Way", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBEthnicityLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "American Indian or Alaska Native", "American Indian or Alaska Native", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Asian", "Asian", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Black or African American", "Black or African American", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Hispanic", "Hispanic", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Native Hawaiian or Pacific Islander", "Native Hawaiian or Pacific Islander", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "White", "White", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Multi racial ", "Multi racial ", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBReligionLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Christian", "Christian", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Hindu", "Hindu", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Islamic", "Islamic", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Judaism", "Judaism", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Buddhist", "Buddhist", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBSeekerHobbyLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Reading", "Reading", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Archery", "Archery", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Stamp Collecting", "Stamp Collecting", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Coin Collecting", "Coin Collecting", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Insect/Butterfly Collecting", "Insect/Butterfly Collecting", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBSeekerMatchOrganizationLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Boeing", "Boeing", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Seattle Metropolitan Credit Union", "Seattle Metropolitan Credit Union", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBSeekerSkillLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Achieving goals", "Achieving goals", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Expanding my knowledge and awareness", "Expanding my knowledge and awareness", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });


			tableName = SBSeekerVerbalizingWordLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Energetic", "Energetic", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Creative", "Creative", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Hardworking", "Hardworking", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Enthusiastic", "Enthusiastic", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Inquisitive", "Inquisitive", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Outgoing", "Outgoing", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Devout", "Devout", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Friendly", "Friendly", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Kind", "Kind", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Passionate", "Passionate", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBServiceTypeLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Volunteer", "Volunteer", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Intern", "Intern", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = SBSportLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Baseball/Softball", "Baseball/Softball", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Basketball", "Basketball", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Football", "Football", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Gymnastics", "Gymnastics", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Track", "Track", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Soccer", "Soccer", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Wrestling", "Wrestling", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });


			tableName = SBWorkTypeLUT;
			columns = GetInsertColumns(tableName);
			Database.Insert(tableName, columns, new[] { "Paid", "Paid", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "In the Home", "In the Home", "0", adminId.ToString(), DateTime.Now.ToString() });
			Database.Insert(tableName, columns, new[] { "Other", "Other (please specify)", "0", adminId.ToString(), DateTime.Now.ToString() });
		}

		public override void Down()
		{
			Database.ExecuteNonQuery("DELETE FROM " + SBAcademicAreaLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBAffiliationTypeLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBCareerLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBClubLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBEthnicityLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBSeekerHobbyLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBSeekerSkillLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBSeekerVerbalizingWordLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBServiceTypeLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBSportLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBWorkTypeLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBCommunityInvolvementCauseLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBSeekerMatchOrganizationLUT);
			Database.ExecuteNonQuery("DELETE FROM " + SBReligionLUT);


			Database.ExecuteNonQuery("DELETE FROM " + SBAdditionalRequirementLUT + " where SBAdditionalRequirement = 'Transcripts - College'");
			Database.ExecuteNonQuery("update " + SBAdditionalRequirementLUT +
			                         @" set SBAdditionalRequirement = 'Transcripts', Description = 'Transcripts one or more previous schools.'
										where SBAdditionalRequirement = '" +
			                          TRANSCRIPTS_HS + "'");
			
		}

		private string[] GetInsertColumns(string tableName)
		{
			Column[] columns = Database.GetColumns(tableName);
			return new string[]
			       	{
			       		columns[1].Name,
						columns[2].Name,
						"Deprecated",
                        "LastUpdateBy",
                        "LastUpdateDate"
					};
		}
	}
}
