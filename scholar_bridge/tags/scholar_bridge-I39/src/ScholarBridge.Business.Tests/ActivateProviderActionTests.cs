using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Actions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ActivateProviderActionTests
    {
        private MockRepository mocks;
        private ActivateProviderAction activateProvider;
        private IProviderService providerService;
        private IMessageService messageService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            providerService = mocks.StrictMock<IProviderService>();
            messageService = mocks.StrictMock<IMessageService>();
            activateProvider = new ActivateProviderAction
                                      {
                                          ProviderService = providerService,
                                          MessageService = messageService
                                      };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(providerService);
            mocks.BackToRecord(messageService);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void approve_with_wrong_message_type_throws_exception()
        {
            activateProvider.Approve(new Message(), null, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void reject_with_wrong_message_type_throws_exception()
        {
            activateProvider.Reject(new Message(), null, null);
        }

        [Test]
        public void approve_with_provider()
        {
            var p = new Provider {AdminUser = new User()};
            var m = new OrganizationMessage {RelatedOrg = p};

            Expect.Call(() => providerService.Approve(p));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateProvider.Approve(m, null, p.AdminUser);

            mocks.VerifyAll();
        }

        [Test]
        public void reject_with_provider()
        {
            var p = new Provider { AdminUser = new User() };
            var m = new OrganizationMessage { RelatedOrg = p };

            Expect.Call(() => providerService.Reject(p));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateProvider.Reject(m, null, p.AdminUser);

            mocks.VerifyAll();
        }
    }
}