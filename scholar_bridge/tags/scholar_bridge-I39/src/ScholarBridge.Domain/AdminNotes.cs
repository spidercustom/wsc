﻿using System;
using System.Text;
using System.Xml;

namespace ScholarBridge.Domain
{
	public class AdminNotes
	{
		private const string NOTES_EMPTY_XML = "<adminNotes></adminNotes>";
		private const string NOTE      = "note";
		private const string NOTE_DATE = "date";
		private const string NOTE_TEXT = "text";
		private const string NOTE_USERNAME = "username";
		private const string NOTE_USERID = "userid";

		private XmlDocument notesXmlDom;
        public virtual string NotesXml 
		{	
			get
			{
				if (notesXmlDom == null)
				{
					return null;
				}
				return notesXmlDom.InnerXml; 
			}
			set
			{
				if (value == null)
				{
					notesXmlDom = null;
				}
				else
				{
					notesXmlDom = new XmlDocument();
					notesXmlDom.LoadXml(value);
				}
			}
		}

		private XmlDocument NotesXmlDom
		{
			get
			{
				if (notesXmlDom == null)
				{
					NotesXml = NOTES_EMPTY_XML;				
				}
				return notesXmlDom;
			}
		}

		public override string ToString()
		{
			return NotesXml;
		}

		/// <summary>
		/// Format the notes as an HTML Table
		/// </summary>
		/// <returns>HTML Table with the notes displayed</returns>
		public string ToStringTableFormat()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<table>");
			XmlNode root = NotesXmlDom.DocumentElement;
			foreach (XmlNode note in root)
			{
				string dateString = string.Empty;
				string text = string.Empty;
				string userName = string.Empty;
				foreach (XmlNode node in note.ChildNodes)
				{
					switch (node.Name)
					{
						case NOTE_DATE:	
							dateString = node.InnerText;
							DateTime dt;
							if (DateTime.TryParse(dateString, out dt))
							{
								dateString = dt.ToShortDateString() + " " + dt.ToShortTimeString();
							}
							break;
						case NOTE_TEXT:
							text = node.InnerText.Replace("\n", "<br />");
							break;
						case NOTE_USERNAME:
							userName = node.InnerText;
							break;
						default:
							break;
					}
				}
				sb.Append(string.Format("<tr><td>-- {0} {1} --</td></tr>", userName, dateString));
				sb.Append(string.Format("<tr><td>{0}</td></tr>", text));
			}
			sb.Append("</table>");

			return sb.ToString();
		}

		public class AdminNote
		{
			public DateTime NoteDate
			{ get; set;}

			public string Note
			{ get; set;}
		}


		public void AppendAdminNote(Auth.User userAppendingNotes, string notes)
		{
			if (null == userAppendingNotes)
				throw new ArgumentNullException("userAppendingNotes", "Must supply a user to attribute these notes to.");
			if (null == notes)
				throw new ArgumentNullException("notes", "Must supply notes.");

			XmlDocumentFragment newNoteFragment = NotesXmlDom.CreateDocumentFragment();

			string noteFragmentXML =
				string.Format("<{0}><{1}>{2}</{1}><{3}>{4}</{3}><{5}>{6}</{5}><{7}>{8}</{7}></{0}>",
					NOTE, NOTE_DATE, DateTime.Now, NOTE_TEXT, notes, NOTE_USERID, userAppendingNotes.Id, NOTE_USERNAME, userAppendingNotes.Name);
			newNoteFragment.InnerXml = noteFragmentXML;

			XmlElement root = NotesXmlDom.DocumentElement;
			if (root.HasChildNodes)
			{
				root.InsertAfter(newNoteFragment, root.LastChild);
			}
			else
			{
				root.AppendChild(newNoteFragment);
			}

		}
	}
}
