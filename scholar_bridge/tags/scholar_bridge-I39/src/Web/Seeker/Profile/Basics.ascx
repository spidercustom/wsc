﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Basics" %>
<%@ Register TagPrefix="sb" Src="~/Common/LookupItemCheckboxList.ascx" TagName="LookupItemCheckboxList" %>
<%@ Register Tagprefix="sb" tagname="LookupDialog" src="~/Common/Lookup/LookupDialog.ascx"   %>    
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  

<script type="text/javascript" language="javascript">
	 
	function CheckWashingtonState(selfId, cityDropDownId, countyDropDownId, cityTextBoxId, countyTextBoxId ) {
	    
	    if (selfId == undefined) {
	        //alert('i am undefined');
	        return false;
	    }
	    
	    var atext = $("#" + selfId).find(':selected').text();
	    if (atext == "Washington") {
	        hideElementById(cityTextBoxId);
	        hideElementById(countyTextBoxId);
	        showElementById(cityDropDownId);
	        showElementById(countyDropDownId);
	    } else {
	        showElementById(cityTextBoxId);
	        showElementById(countyTextBoxId);
	        hideElementById(cityDropDownId);
	        hideElementById(countyDropDownId);
	    }
	} 
	</script>
<h3>My Profile – Basics</h3>
 
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
    
        <p class="form-section-title">My Name:</p>
        <label for="FirstNameBox">First:</label>
        <asp:TextBox ID="FirstNameBox" runat="server" Columns="20" MaxLength="40" />
        <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstNameBox" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
        <br />
        <label for="MidNameBox">Middle:</label>
        <asp:TextBox ID="MidNameBox" runat="server" Columns="10" MaxLength="40"/>
        <elv:PropertyProxyValidator ID="MidNameValidator" runat="server" ControlToValidate="MidNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
        <br />
        <label for="LastNameBox">Last:</label>
        <asp:TextBox ID="LastNameBox" runat="server" Columns="20" MaxLength="40"/>
        <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>       
        <br />

        <hr />

        <p class="form-section-title">My Permanent Address: <sb:CoolTipInfo ID="CoolTipInfo3" Content="Required. Your Address is used to match you with Scholarships  offered in your area." runat="server" /></p>
        <label for="AddressLine1Box">Address Line 1:</label>
        <asp:TextBox ID="AddressLine1Box" runat="server" Columns="30" MaxLength="50"/>
        <elv:PropertyProxyValidator ID="AddressLine1Validator" runat="server" ControlToValidate="AddressLine1Box" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        <br />
        <label for="AddressLine2Box">Address Line 2:</label>
        <asp:TextBox ID="AddressLine2Box" runat="server" Columns="30" MaxLength="50"/>
        <elv:PropertyProxyValidator ID="AddressLine2Validator" runat="server" ControlToValidate="AddressLine2Box" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        <br />
             
        <label for="StateDropDown">State:</label>
        <asp:DropDownList ID="StateDropDown" runat="server"   ></asp:DropDownList>
        <br />
        <label for="CityDropDown">City:</label>
        <asp:DropDownList ID="CityDropDown" runat="server"></asp:DropDownList> <asp:TextBox ID="CityTextBox" runat="server" Columns="30" MaxLength="50"/>
      
        <br />
        <br />
        <label for="CountyDropDown">County:</label>
        <asp:DropDownList ID="CountyDropDown" runat="server"></asp:DropDownList> <asp:TextBox ID="CountyTextBox" runat="server" Columns="30" MaxLength="50"/>
<%--                  <asp:RequiredFieldValidator ID="CountyRequiredValidator" ControlToValidate="CountyControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
--%>                  <br />
    
        
        <label for="ZipBox">Postal Code:</label>
        <asp:TextBox ID="ZipBox" runat="server" Columns="10" MaxLength="10"/>
        <elv:PropertyProxyValidator ID="ZipValidator" runat="server" ControlToValidate="ZipBox" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        <br />
        <label for="PhoneBox">Phone:</label>
        <asp:TextBox ID="PhoneBox" runat="server" Columns="12" CssClass="phone"/>
        <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="PhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
            <br />
        <label for="MobilePhoneBox">Mobile Phone:</label>
        <asp:TextBox ID="MobilePhoneBox" runat="server" Columns="12" CssClass="phone"/>
        <elv:PropertyProxyValidator ID="MobilePhoneValidator" runat="server" ControlToValidate="MobilePhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
        <br />
    </div>
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">  
        <p class="form-section-title">My Email</p>
        <label for="EmailBox">Email:</label>
        <asp:Literal ID="EmailBox" runat="server" />
        <sbCommon:AnchorButton ID="LinkUpdateEmail" runat="server" NavigationUrl="~/Profile" Text=" Update Email Address" />
        <br /><br />
        
        <label for="UseEmailBox">Email Notifications:</label>
        <asp:CheckBox ID="UseEmailBox" runat="server" Text="" Width="50px" />
        <sb:CoolTipInfo ID="CoolTipInfo1" Content="Scholarship status updates and application due date reminders will be sent to the Messages tab. Click this box if you would also like these messages sent to your email box." runat="server" />
        <%--<p class="noteBene" >Notifications are sent for updates on the Scholarships saved to your "My Scholarships" tab. These updates include reminders of applications due dates and responses to submitted applications. These notifications will be posted on the Seeker In box tab. If you also would like to receive an e-mail of these notifications, Please check the above email notifications check box.</p>--%>
        <br />
        
        <hr />
        
        <p class="form-section-title">My Background</p>       
        <label for="DobControl">Birthdate</label>
        <asp:TextBox ID="DobControl" runat="server" CssClass="date"/>
        <elv:PropertyProxyValidator ID="DobValidator" runat="server" ControlToValidate="DobControl" PropertyName="DateOfBirth" SourceTypeName="ScholarBridge.Domain.Seeker"/>
        <asp:RangeValidator ID="dobRangeValid" runat="server" ControlToValidate="DobControl" MinimumValue="1/1/1900" Type="Date" Display="Dynamic" ErrorMessage="How old are you?" />
        <br />

        <label for="GenderButtonList">My Gender:</label>
        <asp:RadioButtonList ID="GenderButtonList" runat="server" CssClass="control-set">
            <asp:ListItem Text="Male" Value="1"></asp:ListItem>
            <asp:ListItem Text="Female" Value="2"></asp:ListItem>
        </asp:RadioButtonList>
        <sb:CoolTipInfo ID="CoolTipInfo4" Content="Optional. Some Scholarships include gender in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for." runat="server" />
        <br /><br />
        <label for="ReligionCheckboxList">My Religion/Faith:</label>
        <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" CheckBoxListWidth="210px" />
        <label for="OtherReligion">Other Religion?</label>
        <asp:TextBox ID="OtherReligion" runat="server"/>  
        <elv:PropertyProxyValidator ID="OtherReligionValidator" runat="server" ControlToValidate="OtherReligion" PropertyName="ReligionOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>
        <sb:CoolTipInfo ID="CoolTipInfo5" Content="Optional. Some Scholarships include Religion in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for." runat="server" />
        <br /><br />

        <label for="EthnicityControl">My Heritage:</label>
        <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" CheckBoxListWidth="210px"/>
        <sb:CoolTipInfo ID="CoolTipInfo6" Content="Optional. Some Scholarships include ethnic heritage in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for." runat="server" />
        <br />
        <label for="OtherEthnicity">Other Ethnicity?</label>
        <asp:TextBox ID="OtherEthnicity" runat="server"/>
        <elv:PropertyProxyValidator ID="OtherEthnicityValidator" runat="server" ControlToValidate="OtherEthnicity" PropertyName="EthnicityOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>
        <br />
   </div>
</div>



