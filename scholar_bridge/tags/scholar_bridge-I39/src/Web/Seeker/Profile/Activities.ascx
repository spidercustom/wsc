﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activities.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Activities" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<div class="form-iceland-container">
    <div class="form-iceland two-columns">

        <p class="form-section-title">What are your academic interests?</p>
        <br />    
        <asp:PlaceHolder ID="AcademicAreasContainerControl" runat="server">
            <label id="AcademicAreasLabelControl" for="AcademicAreasControl">Field of Study: <sb:CoolTipInfo ID="CoolTipInfo2" Content="Optional. Indicate the academic programs or fields of study you are considering to be matched with scholarships for those fields of study." runat="server" />
           </label> 
            <sb:LookupDialog ID="AcademicAreasControlDialogButton" runat="server" BuddyControl="AcademicAreasControl" 
                OtherControl="AcademicAreasOtherControl" OtherControlLabel="AcademicAreasOtherControlLabel" ItemSource="AcademicAreaDAL" Title="Academic Areas"/>
            
            <asp:TextBox ID="AcademicAreasControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
           <br />
            <label id="AcademicAreasOtherControlLabel" for="AcademicAreasOtherControl" class="othercontrollabel">Others </label> 
            <asp:TextBox CssClass="othercontroltextarea" ID="AcademicAreasOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="CareersContainerControl" runat="server">
            <label id="CareersLabelControl" for="CareersControl">Careers:<sb:CoolTipInfo ID="CoolTipInfo1" Content="Optional. Indicate the careers you are interested in pursuing to be matched with scholarships for those careers." runat="server" />
            </label>
            <sb:LookupDialog ID="CareersControlDialogButton" runat="server" BuddyControl="CareersControl"  OtherControl="CareersOtherControl"
            OtherControlLabel="CareersOtherControlLabel"  ItemSource="CareerDAL" Title="Career Selection"/>
            <asp:TextBox ID="CareersControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <label id="CareersOtherControlLabel" for="CareersOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="CareersOtherControl"  TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
        <hr />  
        <p class="form-section-title">What other groups or interests are you involved in?</p><br />
        
        <asp:PlaceHolder ID="HobbiesContainerControl" runat="server">
            <label id="SeekerHobbiesLabelControl" for="SeekerHobbiesControl">Hobbies:</label>
            <sb:LookupDialog ID="SeekerHobbiesControlDialogButton" runat="server" OtherControl="SeekerHobbiesOtherControl" BuddyControl="SeekerHobbiesControl" 
            OtherControlLabel="SeekerHobbiesOtherControlLabel"  ItemSource="SeekerHobbyDAL" Title="Hobby Selection"/>
            <asp:TextBox ID="SeekerHobbiesControl" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="SeekerHobbiesOtherControlLabel" for="SeekerHobbiesOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="SeekerHobbiesOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
  
        <asp:PlaceHolder ID="SportsContainerControl" runat="server">
            <label id="SportsLabelControl" for="SportsControl">Sports:</label>
            <sb:LookupDialog ID="SportsControlDialogButton" runat="server" OtherControl="SportsOtherControl" BuddyControl="SportsControl" 
            OtherControlLabel="SportsOtherControlLabel" ItemSource="SportDAL" Title="Sport Participation Selection"/>
            <asp:TextBox ID="SportsControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="SportsOtherControlLabel" for="SportsOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="SportsOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="ClubsContainerControl" runat="server">
            <label id="ClubsLabelControl" for="ClubsControl">Clubs:</label>
            <sb:LookupDialog ID="ClubsControlDialogButton" runat="server" OtherControl="ClubsOtherControl" BuddyControl="ClubsControl" 
            OtherControlLabel="ClubsOtherControlLabel" ItemSource="ClubDAL" Title="Club Participation Selection"/>
            <asp:TextBox ID="ClubsControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="ClubsOtherControlLabel" for="ClubsOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="ClubsOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
    </div>
    <div class="vertical-devider-two"></div>
       
    <div class="form-iceland two-columns">
    
        <p class="form-section-title">Are you or any family members affiliated with specific Organizations or companies?</p>  
        <asp:PlaceHolder ID="OrganizationsContainerControl" runat="server">
            <label id="OrganizationsLabelControl" for="OrganizationsControl">Organizations:</label>
            <sb:LookupDialog ID="OrganizationsControlDialogButton" runat="server"  OtherControl="OrganizationsOtherControl" BuddyControl="OrganizationsControl" 
            OtherControlLabel="OrganizationsOtherControlLabel" ItemSource="SeekerMatchOrganizationDAL" Title="Organization Affiliation Selection"/>
            <asp:TextBox ID="OrganizationsControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="OrganizationsOtherControlLabel" for="OrganizationsOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="OrganizationsOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>    
            <br />
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="AffiliationTypesContainerControl" runat="server">
            <label id="AffiliationTypesLabelControl" for="AffiliationTypesControl">Companies:</label>
            <sb:LookupDialog ID="AffiliationTypesControlDialogButton" runat="server" OtherControl="AffiliationTypesOtherControl" BuddyControl="AffiliationTypesControl" 
            OtherControlLabel="AffiliationTypesOtherControlLabel" ItemSource="CompanyDAL" Title="Companies Selection"/>
            <asp:TextBox ID="AffiliationTypesControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="AffiliationTypesOtherControlLabel" for="AffiliationTypesOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="AffiliationTypesOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>    
            <br />
        </asp:PlaceHolder>
        <hr />
        
        <p class="form-section-title">Are you working? Volunteering or doing community service?</p>  
        <asp:PlaceHolder ID="WorkTypeContainerControl" runat="server">
            <label id="WorkingLabelControl" for="WorkingControl">Working?:</label>
            <asp:CheckBox ID="WorkingControl" runat="server" />
<sb:CoolTipInfo ID="CoolTipInfo3" Content="Optional. Are you working full time, part-time, or for a family business? Many scholarships take your work situation into consideration when awarding scholarships. Please provide brief details." runat="server" />
            <br />
            <label id="WorkHistoryLabelControl" for="WorkHistoryControl" style="margin-bottom:0px;">Work History:</label>
            <asp:TextBox ID="WorkHistoryControl" cols="" style="width:320px;" Rows="5" TextMode="MultiLine" runat="server"></asp:TextBox>
             <elv:PropertyProxyValidator ID="WorkHistoryControlValidator" Display="Dynamic" runat="server"
            ControlToValidate="WorkHistoryControl" PropertyName="WorkHistory" SourceTypeName="ScholarBridge.Domain.Seeker" />

            <br />
        </asp:PlaceHolder>
        <br />
        <asp:PlaceHolder ID="WorkHoursContainerControl" runat="server">
            <label id="VolunteeringLabelControl" for="VolunteeringControl">Volunteering:</label>
            <asp:CheckBox ID="VolunteeringControl" runat="server" />
            <sb:CoolTipInfo ID="CoolTipInfo4" Content="Optional. Are you, or have you volunteered or done community service? Many scholarships take volunteering into consideration when awarding scholarships. Please provide brief details." runat="server" />
            <br />
            <label id="ServiceHistoryLabelControl"  for="ServiceHistoryControl"  style="margin-bottom:0px;">Service History:</label>
            
             <asp:TextBox ID="ServiceHistoryControl" cols="" style="width:320px;" Rows="5" TextMode="MultiLine" runat="server"></asp:TextBox>
             <elv:PropertyProxyValidator ID="ServiceHistoryControlValidator" Display="Dynamic" runat="server"
            ControlToValidate="ServiceHistoryControl" PropertyName="ServiceHistory" SourceTypeName="ScholarBridge.Domain.Seeker" />

            <br />
        </asp:PlaceHolder>

    </div>
  
</div>