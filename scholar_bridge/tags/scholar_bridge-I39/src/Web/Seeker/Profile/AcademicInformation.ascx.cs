﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class AcademicInformation : WizardStepUserControlBase<Domain.Seeker>
    {
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Seeker seeker;
        Domain.Seeker SeekerInContext
        {
            get
            {
                if (seeker == null)
                    seeker = Container.GetDomainObject();
                if (seeker == null)
                    throw new InvalidOperationException("There is no seeker in context");
                return seeker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context"); 

            if (!IsPostBack)
            {
                
                PopulateScreen();
            }
            else
            {
                SetHonorsControlState();
                SetIBCreditsControlState();
                SetAPCreditsControlState();
                SetSATandACTScoreEvent();
            }
        }

        private void PopulateScreen()
        {
            FirstGenerationControl.Checked = SeekerInContext.FirstGeneration;
            
            if (null != SeekerInContext.CurrentSchool)
            {
                CurrentStudentGroupControl.StudentGroup = SeekerInContext.CurrentSchool.LastAttended;
                CurrentStudentGroupControl.Years = SeekerInContext.CurrentSchool.YearsAttended;

                if (null != SeekerInContext.CurrentSchool.College)
                    CollegeControlDialogButton.Keys = SeekerInContext.CurrentSchool.College.Id.ToString();
                CollegeOtherControl.Text = SeekerInContext.CurrentSchool.CollegeOther;

                if (null != SeekerInContext.CurrentSchool.HighSchool)
                    HighSchoolControlDialogButton.Keys = SeekerInContext.CurrentSchool.HighSchool.Id.ToString();
                HighSchoolOtherControl.Text = SeekerInContext.CurrentSchool.HighSchoolOther;

                if (null != SeekerInContext.CurrentSchool.SchoolDistrict)
                    SchoolDistrictControlLookupDialogButton.Keys = SeekerInContext.CurrentSchool.SchoolDistrict.Id.ToString();
                SchoolDistrictOtherControl.Text = SeekerInContext.CurrentSchool.SchoolDistrictOther;

                 if (null != SeekerInContext.CurrentSchool.TypeOfCollegeStudent)
                SeekerStudentCollegeTypeControl.StudentGroup = SeekerInContext.CurrentSchool.TypeOfCollegeStudent;
            }

            if (null != SeekerInContext.SeekerAcademics)
            {
                CollegesAppliedControlDialogButton.Keys = SeekerInContext.SeekerAcademics.CollegesApplied.CommaSeparatedIds();
                CollegesAppliedOtherControl.Text = SeekerInContext.SeekerAcademics.CollegesAppliedOther;
                InWashingtonCheckbox.Checked = SeekerInContext.SeekerAcademics.IsCollegeAppliedInWashington;
                OutOfStateCheckbox.Checked = SeekerInContext.SeekerAcademics.IsCollegeAppliedOutOfState;

                //CollegesAcceptedControlDialogButton.Keys = SeekerInContext.SeekerAcademics.CollegesAccepted.CommaSeparatedIds();
                //CollegesAcceptedOtherControl.Text = SeekerInContext.SeekerAcademics.CollegesAcceptedOther;

                if (SeekerInContext.SeekerAcademics.GPA.HasValue)
                    GPAControl.Amount = (decimal) SeekerInContext.SeekerAcademics.GPA.Value;
                if (SeekerInContext.SeekerAcademics.ClassRank !=null)
                     ClassRankControl.SelectedValue = SeekerInContext.SeekerAcademics.ClassRank;

                SchoolTypeControl.SelectedValues = (int)SeekerInContext.SeekerAcademics.SchoolTypes;
                AcademicProgramControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.AcademicPrograms;
                SeekerStatusControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.SeekerStatuses;
                //ProgramLengthControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.ProgramLengths;
                
                if (null != SeekerInContext.SeekerAcademics.SATScore)
                {
                    SATReadingControl.Amount = SeekerInContext.SeekerAcademics.SATScore.CriticalReading.Value;
                    SATWritingControl.Amount = SeekerInContext.SeekerAcademics.SATScore.Writing.Value;
                    SATMathControl.Amount = SeekerInContext.SeekerAcademics.SATScore.Mathematics.Value;
                    var SATSum =  SeekerInContext.SeekerAcademics.SATScore.CriticalReading.Value
                                                     + SeekerInContext.SeekerAcademics.SATScore.Writing.Value + SeekerInContext.SeekerAcademics.SATScore.Mathematics.Value;

                    if (SeekerInContext.SeekerAcademics.SATScore.Commulative.HasValue)

                    {
                        if (SeekerInContext.SeekerAcademics.SATScore.Commulative.Value > 0)
                            ACTScoreCommulativeControl.Amount = SeekerInContext.SeekerAcademics.SATScore.Commulative.Value;
                        else
                            SATScoreCommulativeControl.Amount = SATSum;

                    } else
                    {
                        SATScoreCommulativeControl.Amount = SATSum;
                    }


                    hiddenSATCommulative.Value = SATScoreCommulativeControl.Text;
                }
                if (null != SeekerInContext.SeekerAcademics.ACTScore)
                {
                    ACTEnglishControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.English.Value;
                    ACTMathControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.Mathematics.Value;
                    ACTReadingControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.Reading.Value;
                    ACTScienceControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.Science.Value;
                    var ACTSum = SeekerInContext.SeekerAcademics.ACTScore.English.Value +
                        SeekerInContext.SeekerAcademics.ACTScore.Mathematics.Value + SeekerInContext.SeekerAcademics.ACTScore.Reading.Value
                        + SeekerInContext.SeekerAcademics.ACTScore.Science.Value;

                    if (SeekerInContext.SeekerAcademics.ACTScore.Commulative.HasValue)
                    {
                        if (SeekerInContext.SeekerAcademics.ACTScore.Commulative.Value >0)
                         ACTScoreCommulativeControl.Amount = SeekerInContext.SeekerAcademics.ACTScore.Commulative.Value;
                        else
                            ACTScoreCommulativeControl.Amount = ACTSum;
                    }
                    else
                    {    ACTScoreCommulativeControl.Amount = ACTSum;}

                    hiddenACTCommulative.Value = ACTScoreCommulativeControl.Text;
                }

                APCreditsTextControl.Text = SeekerInContext.SeekerAcademics.APCreditsDetail;
                IBCreditsTextBoxControl.Text = SeekerInContext.SeekerAcademics.IBCreditsDetail;
                HonorsTextControl.Text = SeekerInContext.SeekerAcademics.HonorsDetail;

                HonorsCheckboxControl.Checked = SeekerInContext.SeekerAcademics.Honors;
                APCreditsCheckBoxControl.Checked = SeekerInContext.SeekerAcademics.APCredits;
                IBCreditsCheckBoxControl.Checked = SeekerInContext.SeekerAcademics.IBCredits;

                 
                HonorsCheckboxControl.Attributes.Add("onclick", HonorsTextControl.ClientID + ".disabled = ! " + HonorsCheckboxControl.ClientID + ".checked;");
                APCreditsCheckBoxControl.Attributes.Add("onclick", APCreditsTextControl.ClientID + ".disabled = ! " + APCreditsCheckBoxControl.ClientID + ".checked;");
                IBCreditsCheckBoxControl.Attributes.Add("onclick", IBCreditsTextBoxControl.ClientID + ".disabled = ! " + IBCreditsCheckBoxControl.ClientID + ".checked;");

                SetHonorsControlState();
                SetIBCreditsControlState();
                SetAPCreditsControlState();
                SetSATandACTScoreEvent();
            }
        }

        private void SetSATandACTScoreEvent()
        {
            var scriptnameTemplate = "SetUpdateEvent_{0}_script";
            var scriptTemplate = "$('#CONTROLID').blur(function(e) { return UpdateSATComulative('" + SATWritingControl.ClientID + "', '" + SATReadingControl.ClientID + "','" + SATMathControl.ClientID + "','" + hiddenSATCommulative.ClientID + "','" + SATScoreCommulativeControl.ClientID + "'); });";
            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(SATWritingControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(SATWritingControl.ClientID), scriptTemplate.Replace("CONTROLID",SATWritingControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(SATReadingControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(SATReadingControl.ClientID), scriptTemplate.Replace("CONTROLID", SATReadingControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(SATMathControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(SATMathControl.ClientID), scriptTemplate.Replace("CONTROLID", SATMathControl.ClientID));
            
            SetACTScoreEvent();
        }
        private void SetACTScoreEvent()
        {
            var scriptnameTemplate = "SetUpdateEvent_{0}_script";
            var scriptTemplate = "$('#CONTROLID').blur(function(e) { return UpdateACTComulative('" + ACTEnglishControl.ClientID + "', '" + ACTReadingControl.ClientID + "','" + ACTMathControl.ClientID + "','" + ACTScienceControl.ClientID + "','" +hiddenACTCommulative.ClientID + "','" + ACTScoreCommulativeControl.ClientID + "'); });";
            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTEnglishControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTEnglishControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTEnglishControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTReadingControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTReadingControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTReadingControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTMathControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTMathControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTMathControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTScienceControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTScienceControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTScienceControl.ClientID));

        }

        public void SetHonorsControlState()
        {
             
            HonorsTextControl.Enabled = HonorsCheckboxControl.Checked ;
        }
        public void SetAPCreditsControlState()
        {
             
            APCreditsTextControl.Enabled = APCreditsCheckBoxControl.Checked;
        }
        public void SetIBCreditsControlState()
        {
             
            IBCreditsTextBoxControl.Enabled = IBCreditsCheckBoxControl.Checked;
        }

        protected void SATWritingCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(SATWritingControl.Text);
            if (num !=0)
               ValidateSATScore(num,(CustomValidator)source,args);
        }

        protected void SATReadingCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(SATReadingControl.Text);
            if (num != 0)
            ValidateSATScore(num, (CustomValidator)source, args);
        }
        protected void SATMathCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(SATMathControl.Text);
            if (num != 0)
            ValidateSATScore(num, (CustomValidator)source, args);
        }

        protected void SATCommulativeCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


     
            double num = 0;
            double.TryParse(hiddenSATCommulative.Value, out num);
            if (num != 0)
            {
                if (num > 2400 || num < 600)
                {
                    SATCommulativeCustomValidator.IsValid = false;
                    args.IsValid = false;
                }
            }
        }




        private void ValidateSATScore(double num, CustomValidator validator, ServerValidateEventArgs args)
        {
            
                if (num > 800 || num < 200)
                {
                    validator.IsValid = false;
                    args.IsValid = false;
                }
        }

        protected void ACTEnglishCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTEnglishControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTReadingCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTReadingControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTMathCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTMathControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTScienceCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTScienceControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTCommulativeCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {

             
            double num=0;
            double.TryParse(hiddenACTCommulative.Value,out num);
            if (num != 0)
            {
                if (num > 144 || num < 0)
                {
                    ACTCommulativeCustomValidator.IsValid = false;
                    args.IsValid = false;
                }
            }
        }
        private void ValidateACTScore(double num, CustomValidator validator, ServerValidateEventArgs args)
        {

            if (num> 36   || num < 0)
            {
                validator.IsValid = false;
                args.IsValid = false;
            }
        }

	    #region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

        public override void Save()
        {
            PopulateObjects();
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            SeekerService.Update(SeekerInContext);
        }

        public override void PopulateObjects()
        {
            SeekerInContext.FirstGeneration = FirstGenerationControl.Checked;
          
            if (null == SeekerInContext.SeekerAcademics)
            {
                SeekerInContext.SeekerAcademics = new SeekerAcademics();
            }

            CollegesAppliedControlDialogButton.PopulateListFromSelection(SeekerInContext.SeekerAcademics.CollegesApplied);
            SeekerInContext.SeekerAcademics.CollegesAppliedOther = CollegesAppliedOtherControl.Text;

            SeekerInContext.SeekerAcademics.IsCollegeAppliedInWashington = InWashingtonCheckbox.Checked;
            SeekerInContext.SeekerAcademics.IsCollegeAppliedOutOfState = OutOfStateCheckbox.Checked;

            //CollegesAcceptedControlDialogButton.PopulateListFromSelection(SeekerInContext.SeekerAcademics.CollegesAccepted);
            //SeekerInContext.SeekerAcademics.CollegesAcceptedOther = CollegesAcceptedOtherControl.Text;

            SeekerInContext.SeekerAcademics.Honors = HonorsCheckboxControl.Checked;
            SeekerInContext.SeekerAcademics.APCredits= APCreditsCheckBoxControl.Checked;
            SeekerInContext.SeekerAcademics.IBCredits = IBCreditsCheckBoxControl.Checked;


            SeekerInContext.SeekerAcademics.GPA = (double) GPAControl.Amount;
           
            SeekerInContext.SeekerAcademics.APCreditsDetail = APCreditsCheckBoxControl.Checked ? APCreditsTextControl.Text :"";
            SeekerInContext.SeekerAcademics.IBCreditsDetail = IBCreditsCheckBoxControl.Checked ? IBCreditsTextBoxControl.Text : "";
            
            SeekerInContext.SeekerAcademics.HonorsDetail =HonorsCheckboxControl.Checked ? HonorsTextControl.Text: "";

            if (ClassRankControl.SelectedValue!=null)
               SeekerInContext.SeekerAcademics.ClassRank = (ClassRank) ClassRankControl.SelectedValue;

            if (null == SeekerInContext.SeekerAcademics.SATScore)
            {
                SeekerInContext.SeekerAcademics.SATScore = new SatScore();
            }

            SeekerInContext.SeekerAcademics.SATScore.CriticalReading = (int) SATReadingControl.Amount;
            SeekerInContext.SeekerAcademics.SATScore.Writing = (int) SATWritingControl.Amount;
            SeekerInContext.SeekerAcademics.SATScore.Mathematics = (int) SATMathControl.Amount;
            if ((SeekerInContext.SeekerAcademics.SATScore.CriticalReading+
                SeekerInContext.SeekerAcademics.SATScore.Writing + SeekerInContext.SeekerAcademics.SATScore.Mathematics 
                
                )>0)
            {

                SeekerInContext.SeekerAcademics.SATScore.Commulative = 0;
                                                                       
            } else
            {
                SeekerInContext.SeekerAcademics.SATScore.Commulative = (int)SATScoreCommulativeControl.Amount;
            }
           

            if (null == SeekerInContext.SeekerAcademics.ACTScore)
            {
                SeekerInContext.SeekerAcademics.ACTScore = new ActScore();
            }

            SeekerInContext.SeekerAcademics.ACTScore.English = (int) ACTEnglishControl.Amount;
            SeekerInContext.SeekerAcademics.ACTScore.Mathematics = (int) ACTMathControl.Amount;
            SeekerInContext.SeekerAcademics.ACTScore.Reading = (int) ACTReadingControl.Amount;
            SeekerInContext.SeekerAcademics.ACTScore.Science = (int) ACTScienceControl.Amount;

            if ((SeekerInContext.SeekerAcademics.ACTScore.English +
               SeekerInContext.SeekerAcademics.ACTScore.Mathematics + SeekerInContext.SeekerAcademics.ACTScore.Reading
               + SeekerInContext.SeekerAcademics.ACTScore.Science

               ) > 0)
            {

                SeekerInContext.SeekerAcademics.ACTScore.Commulative = 0;

            }
            else
            {
                SeekerInContext.SeekerAcademics.ACTScore.Commulative = (int)ACTScoreCommulativeControl.Amount;
            }
           
           

            if (null == SeekerInContext.CurrentSchool)
            {
                SeekerInContext.CurrentSchool = new CurrentSchool();
            }
            SeekerInContext.CurrentSchool.LastAttended = CurrentStudentGroupControl.StudentGroup;

            SeekerInContext.CurrentSchool.TypeOfCollegeStudent = SeekerStudentCollegeTypeControl.StudentGroup;

            SeekerInContext.CurrentSchool.YearsAttended = CurrentStudentGroupControl.Years;

            SeekerInContext.CurrentSchool.College = (College)CollegeControlDialogButton.GetSelectedLookupItem();
            SeekerInContext.CurrentSchool.CollegeOther = CollegeOtherControl.Text;

            SeekerInContext.CurrentSchool.HighSchool = (HighSchool)HighSchoolControlDialogButton.GetSelectedLookupItem();
            SeekerInContext.CurrentSchool.HighSchoolOther = HighSchoolOtherControl.Text;

            SeekerInContext.CurrentSchool.SchoolDistrict = (SchoolDistrict)SchoolDistrictControlLookupDialogButton.GetSelectedLookupItem();
            SeekerInContext.CurrentSchool.SchoolDistrictOther = SchoolDistrictOtherControl.Text;

            SeekerInContext.SeekerAcademics.SchoolTypes = (SchoolTypes) SchoolTypeControl.SelectedValues;
            SeekerInContext.SeekerAcademics.AcademicPrograms = (AcademicPrograms) AcademicProgramControl.SelectedValue;
            SeekerInContext.SeekerAcademics.SeekerStatuses = (SeekerStatuses) SeekerStatusControl.SelectedValue;
            //SeekerInContext.SeekerAcademics.ProgramLengths = (ProgramLengths) ProgramLengthControl.SelectedValue;

        }
		#endregion

    }
}