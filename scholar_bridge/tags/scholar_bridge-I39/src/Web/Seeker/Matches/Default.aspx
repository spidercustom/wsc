﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Matches.Default"  Title="Seeker | My Matches" %>

<%@ Register src="~/Common/MatchList.ascx" tagname="MatchList" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopMyMatches.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomMyMatches.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
                <!--Left floated content area starts here-->
                <div id="HomeContentLeft">

                  <Img src="<%= ResolveUrl("~/images/PgTitle_MyMatches.gif") %>" width="399px" height="54px">
                  <img src="<%= ResolveUrl("~/images/EmphasisedMyMatchesPage.gif") %>" width="513px" height="96px">
                 </div>
                  <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <div id="HomeContentRight">
                 <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                 </div>
                <BR>

                 <div id="Clear"></div>





    <h3>My Scholarships of Interest</h3>
    <div>
            <asp:Image ID="Image3" ImageUrl="~/images/star_empty.png" runat="server"/>
            <span>Matched scholarship not yet saved to <strong>My Scholarships</strong> list</span><br />
            <asp:Image ID="Image4" ImageUrl="~/images/star.png" runat="server"/>
            <span>Scholarship is saved to <strong>My Scholarships</strong> list</span>
    </div>                    
    <br />
    
    <sb:MatchList id="savedList" runat="server" OnMatchAction="list_OnMatchAction" PageSize="4" />
    <br />
<br />
    <h3>My Matches</h3>
    <p>Scholarships you qualify for:</p>
    <sb:MatchList id="qualifyList" runat="server" OnMatchAction="list_OnMatchAction" />

</asp:Content>
