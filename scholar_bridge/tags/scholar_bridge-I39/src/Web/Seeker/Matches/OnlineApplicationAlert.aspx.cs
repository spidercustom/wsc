﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Matches
{
    public partial class OnlineApplicationAlert : Page
    {
        
        public IUserContext UserContext { get; set; }




        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
            
        }
    }
}
