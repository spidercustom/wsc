﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteria.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.AdditionalCriteria" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<%@ Register src="EditQuestionAnswers.ascx" tagname="EditQuestionAnswers" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="ListOfAdditionalRequirements.ascx" tagname="ListOfAdditionalRequirements" tagprefix="sb" %>

<div class="form-iceland">
    <p class="form-section-title">Additional Requirements with Scholarship Application</p>
    <sb:ListOfAdditionalRequirements ID="AdditionalRequirementsList" runat="server" />
    <hr />

    <p class="form-section-title">Please answer following questions:</p>
    <sb:EditQuestionAnswers ID="QAEditor" runat="server" />
    <br />
    <hr />


    <p class="form-section-title">Upload files</p>
    <label for="AttachFile">Select File:</label>
    <asp:FileUpload ID="AttachFile" runat="server" />
    <br />
    
    <label for="AttachmentComments">Notes related to file:</label>
    <asp:TextBox ID="AttachmentComments" runat="server" />
    <elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments" PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment"/>
    <span class="noteBene">Please enter notes . This will accompany the file.</span>
    <br />
    
    <sbCommon:AnchorButton ID="UploadFile" runat="server" Text="Attach File" onclick="UploadFile_Click" />
    <br />
    
    <asp:ListView ID="attachedFiles" runat="server" OnItemDeleting="attachedFiles_OnItemDeleting">
        <LayoutTemplate>
        <table class="sortableTable">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>File</th>
                    <th>Size</th>
                    <th>Type</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:LinkButton ID="deleteAttachmentBtn" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("Id")%>' /></td>
                <td><%# Eval("Name") %></td>
                <td><%# Eval("DisplaySize") %></td>
                <td><%# Eval("MimeType") %></td>
            </tr>
        </ItemTemplate>
    </asp:ListView>

</div>