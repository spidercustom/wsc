﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class AcademicInformation : WizardStepUserControlBase<Domain.Application>
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Application application;
        Domain.Application ApplicationInContext
        {
            get
            {
                if (application == null)
                    application = Container.GetDomainObject();
                if (application == null)
                    throw new InvalidOperationException("There is no application in context");
                return application;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ApplicationInContext == null)
                throw new InvalidOperationException("There is no application in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
            else
            {
                SetHonorsControlState();
                SetIBCreditsControlState();
                SetAPCreditsControlState();
            }
		}

        private void PopulateScreen()
        {
            FirstGenerationControl.Checked = ApplicationInContext.FirstGeneration;

            if (null != ApplicationInContext.CurrentSchool)
            {
                CurrentStudentGroupControl.StudentGroup = ApplicationInContext.CurrentSchool.LastAttended;
                CurrentStudentGroupControl.Years = ApplicationInContext.CurrentSchool.YearsAttended;

                if (null != ApplicationInContext.CurrentSchool.College)
                    CollegeControlDialogButton.Keys = ApplicationInContext.CurrentSchool.College.Id.ToString();
                CollegeOtherControl.Text = ApplicationInContext.CurrentSchool.CollegeOther;

                if (null != ApplicationInContext.CurrentSchool.HighSchool)
                    HighSchoolControlDialogButton.Keys = ApplicationInContext.CurrentSchool.HighSchool.Id.ToString();
                HighSchoolOtherControl.Text = ApplicationInContext.CurrentSchool.HighSchoolOther;

                if (null != ApplicationInContext.CurrentSchool.SchoolDistrict)
                    SchoolDistrictControlLookupDialogButton.Keys = ApplicationInContext.CurrentSchool.SchoolDistrict.Id.ToString();
                SchoolDistrictOtherControl.Text = ApplicationInContext.CurrentSchool.SchoolDistrictOther;

                if (null != ApplicationInContext.CurrentSchool.TypeOfCollegeStudent)
                    SeekerStudentCollegeTypeControl.StudentGroup = ApplicationInContext.CurrentSchool.TypeOfCollegeStudent;
            }

            if (null != ApplicationInContext.SeekerAcademics)
            {
                CollegesAppliedControlDialogButton.Keys = ApplicationInContext.SeekerAcademics.CollegesApplied.CommaSeparatedIds();
                CollegesAppliedOtherControl.Text = ApplicationInContext.SeekerAcademics.CollegesAppliedOther;
                InWashingtonCheckbox.Checked = ApplicationInContext.SeekerAcademics.IsCollegeAppliedInWashington;
                OutOfStateCheckbox.Checked = ApplicationInContext.SeekerAcademics.IsCollegeAppliedOutOfState;

                //CollegesAcceptedControlDialogButton.Keys = ApplicationInContext.SeekerAcademics.CollegesAccepted.CommaSeparatedIds();
                //CollegesAcceptedOtherControl.Text = ApplicationInContext.SeekerAcademics.CollegesAcceptedOther;

                if (ApplicationInContext.SeekerAcademics.GPA.HasValue)
                    GPAControl.Amount = (decimal)ApplicationInContext.SeekerAcademics.GPA.Value;
                if (ApplicationInContext.SeekerAcademics.ClassRank != null)
                    ClassRankControl.SelectedValue = ApplicationInContext.SeekerAcademics.ClassRank;

                SchoolTypeControl.SelectedValues = (int)ApplicationInContext.SeekerAcademics.SchoolTypes;
                AcademicProgramControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.AcademicPrograms;
                SeekerStatusControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.SeekerStatuses;
                //ProgramLengthControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.ProgramLengths;

                if (null != ApplicationInContext.SeekerAcademics.SATScore)
                {
                    SATReadingControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.CriticalReading.Value;
                    SATWritingControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Writing.Value;
                    SATMathControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Mathematics.Value;
                    if (ApplicationInContext.SeekerAcademics.SATScore.Commulative.HasValue)
                        SATScoreCommulativeControl.Amount = ACTScoreCommulativeControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Commulative.Value;
                    else
                        SATScoreCommulativeControl.Amount = 0;
                }
                if (null != ApplicationInContext.SeekerAcademics.ACTScore)
                {
                    ACTEnglishControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.English.Value;
                    ACTMathControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Mathematics.Value;
                    ACTReadingControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Reading.Value;
                    ACTScienceControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Science.Value;
                    if (ApplicationInContext.SeekerAcademics.ACTScore.Commulative.HasValue)
                        ACTScoreCommulativeControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Commulative.Value;
                    else
                        ACTScoreCommulativeControl.Amount = 0;

                }

                APCreditsTextControl.Text = ApplicationInContext.SeekerAcademics.APCreditsDetail;
                IBCreditsTextBoxControl.Text = ApplicationInContext.SeekerAcademics.IBCreditsDetail;
                HonorsTextControl.Text = ApplicationInContext.SeekerAcademics.HonorsDetail;

                HonorsCheckboxControl.Checked = ApplicationInContext.SeekerAcademics.Honors;
                APCreditsCheckBoxControl.Checked = ApplicationInContext.SeekerAcademics.APCredits;
                IBCreditsCheckBoxControl.Checked = ApplicationInContext.SeekerAcademics.IBCredits;


                HonorsCheckboxControl.Attributes.Add("onclick", HonorsTextControl.ClientID + ".disabled = ! " + HonorsCheckboxControl.ClientID + ".checked;");
                APCreditsCheckBoxControl.Attributes.Add("onclick", APCreditsTextControl.ClientID + ".disabled = ! " + APCreditsCheckBoxControl.ClientID + ".checked;");
                IBCreditsCheckBoxControl.Attributes.Add("onclick", IBCreditsTextBoxControl.ClientID + ".disabled = ! " + IBCreditsCheckBoxControl.ClientID + ".checked;");

                SetHonorsControlState();
                SetIBCreditsControlState();
                SetAPCreditsControlState();
            }
        }
        public void SetHonorsControlState()
        {

            HonorsTextControl.Enabled = HonorsCheckboxControl.Checked;
        }
        public void SetAPCreditsControlState()
        {

            APCreditsTextControl.Enabled = APCreditsCheckBoxControl.Checked;
        }
        public void SetIBCreditsControlState()
        {

            IBCreditsTextBoxControl.Enabled = IBCreditsCheckBoxControl.Checked;
        }

	    #region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

        public override void Save()
        {
            PopulateObjects();
            ApplicationService.Update(ApplicationInContext);
        }

        public override void PopulateObjects()
        {
            ApplicationInContext.FirstGeneration = FirstGenerationControl.Checked;

            if (null == ApplicationInContext.SeekerAcademics)
            {
                ApplicationInContext.SeekerAcademics = new SeekerAcademics();
            }

            CollegesAppliedControlDialogButton.PopulateListFromSelection(ApplicationInContext.SeekerAcademics.CollegesApplied);
            ApplicationInContext.SeekerAcademics.CollegesAppliedOther = CollegesAppliedOtherControl.Text;

            ApplicationInContext.SeekerAcademics.IsCollegeAppliedInWashington = InWashingtonCheckbox.Checked;
            ApplicationInContext.SeekerAcademics.IsCollegeAppliedOutOfState = OutOfStateCheckbox.Checked;

            //CollegesAcceptedControlDialogButton.PopulateListFromSelection(ApplicationInContext.SeekerAcademics.CollegesAccepted);
            //ApplicationInContext.SeekerAcademics.CollegesAcceptedOther = CollegesAcceptedOtherControl.Text;

            ApplicationInContext.SeekerAcademics.Honors = HonorsCheckboxControl.Checked;
            ApplicationInContext.SeekerAcademics.APCredits = APCreditsCheckBoxControl.Checked;
            ApplicationInContext.SeekerAcademics.IBCredits = IBCreditsCheckBoxControl.Checked;


            ApplicationInContext.SeekerAcademics.GPA = (double)GPAControl.Amount;

            ApplicationInContext.SeekerAcademics.APCreditsDetail = APCreditsCheckBoxControl.Checked ? APCreditsTextControl.Text : "";
            ApplicationInContext.SeekerAcademics.IBCreditsDetail = IBCreditsCheckBoxControl.Checked ? IBCreditsTextBoxControl.Text : "";

            ApplicationInContext.SeekerAcademics.HonorsDetail = HonorsCheckboxControl.Checked ? HonorsTextControl.Text : "";

            if (ClassRankControl.SelectedValue != null)
                ApplicationInContext.SeekerAcademics.ClassRank = (ClassRank)ClassRankControl.SelectedValue;

            if (null == ApplicationInContext.SeekerAcademics.SATScore)
            {
                ApplicationInContext.SeekerAcademics.SATScore = new SatScore();
            }

            ApplicationInContext.SeekerAcademics.SATScore.CriticalReading = (int)SATReadingControl.Amount;
            ApplicationInContext.SeekerAcademics.SATScore.Writing = (int)SATWritingControl.Amount;
            ApplicationInContext.SeekerAcademics.SATScore.Mathematics = (int)SATMathControl.Amount;
            ApplicationInContext.SeekerAcademics.SATScore.Commulative = (int)SATScoreCommulativeControl.Amount;

            if (null == ApplicationInContext.SeekerAcademics.ACTScore)
            {
                ApplicationInContext.SeekerAcademics.ACTScore = new ActScore();
            }

            ApplicationInContext.SeekerAcademics.ACTScore.English = (int)ACTEnglishControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Mathematics = (int)ACTMathControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Reading = (int)ACTReadingControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Science = (int)ACTScienceControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Commulative = (int)ACTScoreCommulativeControl.Amount;

            if (null == ApplicationInContext.CurrentSchool)
            {
                ApplicationInContext.CurrentSchool = new CurrentSchool();
            }
            ApplicationInContext.CurrentSchool.LastAttended = CurrentStudentGroupControl.StudentGroup;

            ApplicationInContext.CurrentSchool.TypeOfCollegeStudent = SeekerStudentCollegeTypeControl.StudentGroup;

            ApplicationInContext.CurrentSchool.YearsAttended = CurrentStudentGroupControl.Years;

            ApplicationInContext.CurrentSchool.College = (College)CollegeControlDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.CollegeOther = CollegeOtherControl.Text;

            ApplicationInContext.CurrentSchool.HighSchool = (HighSchool)HighSchoolControlDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.HighSchoolOther = HighSchoolOtherControl.Text;

            ApplicationInContext.CurrentSchool.SchoolDistrict = (SchoolDistrict)SchoolDistrictControlLookupDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.SchoolDistrictOther = SchoolDistrictOtherControl.Text;

            ApplicationInContext.SeekerAcademics.SchoolTypes = (SchoolTypes)SchoolTypeControl.SelectedValues;
            ApplicationInContext.SeekerAcademics.AcademicPrograms = (AcademicPrograms)AcademicProgramControl.SelectedValue;
            ApplicationInContext.SeekerAcademics.SeekerStatuses = (SeekerStatuses)SeekerStatusControl.SelectedValue;
            //ApplicationInContext.SeekerAcademics.ProgramLengths = (ProgramLengths)ProgramLengthControl.SelectedValue;
        }
		#endregion

    }
}