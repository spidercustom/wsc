﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>
<%@ Import Namespace="ScholarBridge.Domain.Auth"%>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<asp:ListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    OnPagePropertiesChanged="matchList_PagePropertiesChanged"
    onpagepropertieschanging="matchList_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th></th>
                <th>Scholarship Name</th>
                <th>Rank</th>
                <th>Eligibility Criteria  <sb:CoolTipInfo ID="CoolTipInfo1" Header="Required Criteria"  Content="Determines if you qualify for a scholarship." runat="server" /></th>
                <th>Preferred Criteria <sb:CoolTipInfo ID="CoolTipInfo2" Header="Preferred Criteria"  Content="Shows the number of additional selection criteria that you meet." runat="server" /></th>
                <th>Application Due Date</th>
                <th># of Awards</th>
                <th>Scholarship Amount</th>
                <th>Action</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:ImageButton id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png"/></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:0.00}")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="ListButton" Width="70px" /></td>
        <td><%# Eval("MatchApplicationStatusString")%></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:ImageButton  id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png" /></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:0.00}")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server"  CssClass="ListButton" Width="70px"/></td>
        <td><%# Eval("MatchApplicationStatusString")%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>&lt;There are no scholarships at this time.&gt;</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="matchList" PageSize="20" onprerender="pager_PreRender" >
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
</div> 