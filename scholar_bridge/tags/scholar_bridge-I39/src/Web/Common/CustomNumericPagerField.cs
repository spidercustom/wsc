﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    public class CustomNumericPagerField : DataPagerField
    {
        private const string GOTO_PAGE = "gotoPage";

        public int VisiblePageNumberCount
        {
            get { return Convert.ToInt32(ViewState["PageNumberCount"] ?? 5); }
            set { ViewState["PageNumberCount"] = value; }
        }

        public string CurrentPageLabelCssClass
        {
            get { return Convert.ToString(ViewState["CurrentPageLabelCssClass"] ?? string.Empty); }
            set { ViewState["CurrentPageLabelCssClass"] = value; }
        }

        public string NumericButtonCssClass
        {
            get { return Convert.ToString(ViewState["NumericButtonCssClass"] ?? string.Empty); }
            set { ViewState["NumericButtonCssClass"] = value; }
        }

        public string NextPreviousButtonCssClass
        {
            get { return Convert.ToString(ViewState["NextPreviousButtonCssClass"] ?? string.Empty); }
            set { ViewState["NextPreviousButtonCssClass"] = value; }
        }

        public string PagingPageLabelCssClass
        {
            get { return Convert.ToString(ViewState["PagingPageLabelCssClass"] ?? string.Empty); }
            set { ViewState["PagingPageLabelCssClass"] = value; }
        }

        private int startRowIndex;
        private int maximumRows;
        private int totalRowCount;

        protected int TotalRowCount
        {
            get { return totalRowCount; }
        }

        protected int MaximumRows
        {
            get { return maximumRows; }
        }

        protected int StartRowIndex
        {
            get { return startRowIndex; }
        }

        protected int CurrentPageIndex
        {
            get { return TotalRowCount > 0 ? (StartRowIndex / DataPager.PageSize) + 1 : 0; }
        }

        protected int TotalPages
        {
            get { return (Int32) Math.Ceiling(TotalRowCount / (double) DataPager.PageSize); }
        }

        public override void CreateDataPagers(DataPagerFieldItem container, int startRowIndex, int maximumRows, int totalRowCount, int fieldIndex)
        {
            this.startRowIndex = startRowIndex;
            this.maximumRows = maximumRows;
            this.totalRowCount = totalRowCount;

            if (string.IsNullOrEmpty(DataPager.QueryStringField))
            {
                CreateDataPagersForCommand(container, fieldIndex);
            }
            else
            {
                CreateDataPagersForQueryString(container, fieldIndex);
            }
        }

        private static void CreateDataPagersForQueryString(DataPagerFieldItem container, int index)
        {
            throw new NotSupportedException("CustomNumericPagerField with query string is not supported");
        }

        private void CreateDataPagersForCommand(DataPagerFieldItem container, int index)
        {
            var renderPageIndexFrom = Math.Max(1, CurrentPageIndex - (VisiblePageNumberCount / 2));
            if (renderPageIndexFrom + VisiblePageNumberCount - 1 > TotalPages)
                renderPageIndexFrom = Math.Max(1, TotalPages - VisiblePageNumberCount + 1);
            int renderPageIndexTill = Math.Min(TotalPages, renderPageIndexFrom + VisiblePageNumberCount - 1);


            if (renderPageIndexFrom > 1)
                CreatePriorPagingPageControls(renderPageIndexFrom).ForEach(control => container.Controls.Add(control));
            for (int pageIndex = renderPageIndexFrom; pageIndex <= renderPageIndexTill; pageIndex++)
                CreatePageControls(pageIndex).ForEach(control => container.Controls.Add(control));
            if (renderPageIndexTill < TotalPages)
                CreateNextPagingPageControls(renderPageIndexTill).ForEach(control => container.Controls.Add(control));
        }

        protected virtual IList<Control> CreatePriorPagingPageControls(int renderPageIndexFrom)
        {
            var controls = new List<Control>();
            var pageLabel = new Label()
            {
                Text = "...",
                CssClass = PagingPageLabelCssClass
            };
            controls.Add(pageLabel);
            return controls.ToArray();
        }

        protected virtual IList<Control> CreatePageControls(int pageIndex)
        {
            if (CurrentPageIndex == pageIndex)
            {
                var controls = new List<Control>();
                var pageLabel = new Label()
                {
                    Text = pageIndex.ToString(),
                    CssClass = CurrentPageLabelCssClass
                };
                controls.Add(pageLabel);
                return controls.ToArray();
            }
            else
            {
                var controls = new List<Control>();
                var pageLink = new LinkButton
                                   {
                                       CommandName = GOTO_PAGE,
                                       CommandArgument = pageIndex.ToString(),
                                       Text = pageIndex.ToString(),
                                       CssClass = NumericButtonCssClass
                                   };
                controls.Add(pageLink);
                return controls.ToArray();
            }
        }

        protected virtual IList<Control> CreateNextPagingPageControls(int renderPageIndexTill)
        {
            var controls = new List<Control>();
            var pageLabel = new Label()
            {
                Text = "...",
                CssClass = PagingPageLabelCssClass
            };
            controls.Add(pageLabel);
            return controls.ToArray();
        }

        protected override DataPagerField CreateField()
        {
            return new CustomNumericPagerField();
        }

        public override void HandleEvent(CommandEventArgs e)
        {
            if (GOTO_PAGE.Equals(e.CommandName))
            {
                int newStartRowIndex = (Convert.ToInt32(e.CommandArgument) -1) * DataPager.PageSize;
                DataPager.SetPageProperties(newStartRowIndex, DataPager.PageSize, true);
            }
        }
    }
}
