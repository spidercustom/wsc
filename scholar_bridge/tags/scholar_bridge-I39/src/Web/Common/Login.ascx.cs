﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Common
{
    public partial class Login : UserControl
    {
        
       
     
        private System.Web.UI.WebControls.Login Login1
    	{
    		get
    		{
				return (System.Web.UI.WebControls.Login)loginView.FindControl("Login1");
			}
    	}

        protected string PostbackURL
        {
            get
            {
                var linker = new LinkGenerator();
                return linker.GetFullLink(Request.AppRelativeCurrentExecutionFilePath.StartsWith("~") ? Request.AppRelativeCurrentExecutionFilePath.Remove(0, 1) : Request.AppRelativeCurrentExecutionFilePath);
            }
        }

        protected bool IsCrossSitePost
        {
            get; set;
        }
        private void SetFailureDialogText(string s)
        {

            if (Login1 != null)
            {
                var failuretext = Login1.FindControl("FailureDialogText") as Literal;
                if (failuretext != null)
                    failuretext.Text = s;
            }
        }

        public IUserService UserService { get; set; }
        public string LinkTo { get; set; }

        public bool ShowRegisterToday { get; set; }

        private System.Web.UI.WebControls.Login LoginControl
        {
            get { return ((System.Web.UI.WebControls.Login) loginView.FindControl("Login1")); }
        }

        private string Username
        {
            get { return LoginControl.UserName; }
        }

        private void SetDestinationUrl(string url)
        {
            LoginControl.DestinationPageUrl = url;
        }

        private LinkGenerator linkGenerator;
        private LinkGenerator LinkGenerator
        {
            get
            {
                if (linkGenerator == null)
                    linkGenerator = new LinkGenerator();
                return linkGenerator;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
		{
			// Note this
			Response.Cache.SetNoStore();
            

		}
		protected void Page_Load(object sender, EventArgs e)
		{
            

		    if (!IsPostBack)
			{
				if (Login1 != null)
				{
                    

				    var link = Login1.FindControl("registerlink") as HyperLink ;
				    link.NavigateUrl = LinkTo;

                    ApplyForgotPasswordOnClick();

				    TextBox txtUser = Login1.FindControl("UserName") as TextBox;
					TextBox txtPass = Login1.FindControl("Password") as TextBox;
                    Button login = Login1.FindControl("Login") as Button;
                    
                    if (Request.RequestType.ToLower() == "post")
                    {
                        IsCrossSitePost = true;
                        AttemptLoginWithPostedValues(txtUser, txtPass);
                    }
                    else
                    {
						login.PostBackUrl = PostbackURL;
                    }
                    
					//if (txtUser != null)
					//    Page.SetFocus(txtUser);
					Page.ClientScript.RegisterOnDocumentReadyBlock(
						Page.GetType(),
						txtUser.ClientID + "_usertxtScript",
						string.Format(
							"$.watermark.className = 'watermark'; $('#{0}').watermark('Email Address'); $('#{1}').watermark('Password'); ",
							txtUser.ClientID, txtPass.ClientID)
						);
				}
			}
		}

        /// <summary>
        /// if the page appears to have been posted from the public site then try to
        /// authenticate from the values passed in the post.
        /// </summary>
        /// <param name="txtUser"></param>
        /// <param name="txtPass"></param>
        private void AttemptLoginWithPostedValues(TextBox txtUser, TextBox txtPass)
        {
            string userName = Request.Form[txtUser.ClientID.Replace('_', '$')];
            string password = Request.Form[txtPass.ClientID.Replace('_', '$')];
            txtUser.Text = userName;
            txtPass.Text = password;
            var provider = new SpiderMembershipProvider();
			provider.Initialize(string.Empty, new NameValueCollection());

            if (!provider.ValidateUser(userName, password))
            {
                return;   
            }

            FormsAuthentication.SetAuthCookie(userName, false);
            LoggedIn();
            Response.Redirect(LoginControl.DestinationPageUrl, true);
        }

        private void ApplyForgotPasswordOnClick()
        {
            var forgotPasswordUrl = "showinpopup('{0}?popup=true', '500', '400'); return false;".Build(LinkGenerator.GetFullLink("/ForgotPassword.aspx"));
            var forgotPasswordLink = Login1.FindControl("forgotPassword") as HyperLink;
            forgotPasswordLink.Attributes.Add("onclick", forgotPasswordUrl);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
			if (!IsPostBack)
			{
				if (Request.Cookies["myCookie"] != null)
				{
					HttpCookie cookie = Request.Cookies.Get("myCookie");

					if (Login1 != null)
					{
						Login1.UserName = cookie.Values["username"];

						Login1.RememberMeSet = (!String.IsNullOrEmpty(Login1.UserName));
					}
				}
					
			}
			if (Login1 != null)
			{
				var registerTodayContainer = Login1.FindControl("registerTodayContainer");
				registerTodayContainer.VisibleIf(() => ShowRegisterToday);
			}


		}
  
        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            LoggedIn();
        }

        private void LoggedIn()
        {
            HttpCookie myCookie = new HttpCookie("myCookie");
            Boolean remember = Login1.RememberMeSet;

            if (remember)
            {
                Int32 persistDays = 30;
                myCookie.Values.Add("username", Login1.UserName);
                myCookie.Expires = DateTime.Now.AddDays(persistDays); //you can add years and months too here
            }
            else
            {
                myCookie.Values.Add("username", string.Empty); // overwrite empty string is safest
                myCookie.Expires = DateTime.Now.AddMinutes(5); //you can add years and months too here
            }

            Response.Cookies.Add(myCookie);
            if (null == Request["ReturnUrl"])
            {
                SetDestinationUrl(DestinationForUser(Username));
            }
        }

        public string DestinationForUser(string username)
        {
            var roles = new List<string>(Roles.GetRolesForUser(username));
            if (roles.Any(r => Role.PROVIDER_ROLE.Equals(r)))
            {
                return "~/Provider/";
            }
            if (roles.Any(r => Role.PROVIDER_ADMIN_ROLE.Equals(r)))
            {
                return "~/Provider/";
            }
            if (roles.Any(r => Role.INTERMEDIARY_ROLE.Equals(r)))
            {
                return "~/Intermediary/";
            }
            if (roles.Any(r => Role.INTERMEDIARY_ADMIN_ROLE .Equals(r)))
            {
                return "~/Intermediary/";
            }
            if (roles.Any(r => Role.WSC_ADMIN_ROLE.Equals(r)))
            {
                return "~/Admin/";
            }
            if (roles.Any(r => Role.ADMIN_ROLE.Equals(r)))
            {
                return "~/Admin/";
            }
            if (roles.Any(r => Role.SEEKER_ROLE.Equals(r) || Role.INFLUENCER_ROLE.Equals(r)))
            {
                return "~/Seeker/";
            }

            return "~/";
        }

        protected void Login1_LoginError(object sender, EventArgs e)
        {
            var u = UserService.FindByEmail(Username);
            if (null != u)
            {
                // Just want to check a couple of properties to see why the user can't login
                // Otherwise just use the default error
                if (!u.IsApproved)
                {
                    var msg = "You have not yet been approved by the administrators of the site. You will receive an email when you are approved and can login.";
                    LoginControl.FailureText = msg;
                    SetFailureDialogText(msg);
                   

                }
            }
            ClientSideDialogs.ShowDivAsDialog("ValidationErrorPopup");
           

        }

         
    }
}
