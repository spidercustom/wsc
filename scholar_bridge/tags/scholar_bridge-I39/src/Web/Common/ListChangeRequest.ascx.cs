﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Common;

namespace ScholarBridge.Web.Common
{
    public partial class ListChangeRequest : UserControl
    {
        public string From { get; set; }

        public string ListType
        {
            get { return cboListType.Text; }
        }

        public string Reason
        {
            get { return txtReason.Text; }
        }

        public string NewValue
        {
            get { return txtValue.Text; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblFrom.Text = From;
                var list = GetDisplayNames(
                                           SeekerProfileAttribute.Career,
                                           SeekerProfileAttribute.City,
                                           SeekerProfileAttribute.ClassRank,
                                           SeekerProfileAttribute.Club,
                                           SeekerProfileAttribute.College,
                                           SeekerProfileAttribute.Company,
                                           SeekerProfileAttribute.Ethnicity,
                                           SeekerProfileAttribute.AcademicArea,
                                           SeekerProfileAttribute.HighSchool,
                                           SeekerProfileAttribute.SeekerHobby,
                                           SeekerProfileAttribute.Organization,
                                           SeekerProfileAttribute.Religion,
                                           SeekerProfileAttribute.SchoolDistrict,
                                           SeekerProfileAttribute.SchoolType,
                                           FundingProfileAttribute.SupportedSituation,
                                           SeekerProfileAttribute.Sport,
                                           SeekerProfileAttribute.StudentGroup);
                list.Add("Other - Please Specify");
               
                cboListType.DataSource = list;
                cboListType.DataBind();
            }
        }

        public List<string> GetDisplayNames(params Enum[] @enums)
        {
            var list=new List<string>();
            foreach (Enum @enum in @enums)
            {

                list.Add(@enum.GetDisplayName());

            }
            return list;
        }
    }
}