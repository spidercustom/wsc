﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerCollegeType.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerCollegeType" %>

<script type="text/javascript">
//$(document).ready(function() {
//    $("input[name='ctl00$bodyContentPlaceHolder$AcademicInfo1$CurrentStudentGroupControl$studentGroup']").click(
//        function() {
//            var checkedvalue = $(this).val(); 
//            $.each(["HighSchoolSeniorYear", "AdultReturningYear", "TransferYear"], function(i, name) {
//                $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + name).attr("disabled","disabled");
//            });
//            $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + checkedvalue + "Year").removeAttr("disabled");
//        });
//    });
</script>

<style type="text/css">
    .radioSelect
    {
        width: 200px;
        text-align: left;
        height: 18px;
    }
    
    .fix-size-label-container > label
    {
        width: 120px;
        display: inline-table;
        vertical-align:top;
        padding: 0px;
        margin: 5px 5px 10px 5px; 
    }
</style>

<div class="control-set fix-size-label-container">
     
    
     <asp:RadioButton runat="server" ID="AdultFirstTime" GroupName="studentGroup" /> <label>Adult First time </label>
    <br /> 
    
     <asp:RadioButton runat="server" ID="AdultReturning" GroupName="studentGroup"/>  <label>Adult Returning</label> 
    
     
    <br /> 
    
     <asp:RadioButton runat="server" ID="Transfer" GroupName="studentGroup"/> <label>Transfer</label> 
 
    <br /> 
    
     <asp:RadioButton runat="server" ID="Undergraduate" GroupName="studentGroup"/> <label>Undergraduate</label>
    <br />
    
    <asp:RadioButton runat="server" ID="Graduate" GroupName="studentGroup"/> <label>Graduate</label>
    <br /> 
</div>
