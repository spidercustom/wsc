﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activate.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Activate" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<h3>Build Scholarship – Activate</h3>
<p>Activating the Scholarship will make the scholarship visible to applicants and allow the scholarship to be matched with qualified applicants. 
Once you activate, you will not be able to edit the scholarship criteria. By activating this scholarship, you, the provider 
(or the intermediary on the provider's behalf), indicate your intention to award the scholarship as it has been entered.
</p>
<hr />
<div id="certifyAvailabilityDiv" title="Certify Availability" runat="server">
    <fieldset>
        <legend>Specify Scholarship Availability Before Activation</legend>
        I certify, on behalf of the scholarship provider, that this scholarship is available to one or both of the following:<br /><br />
        <asp:checkboxlist ID="availabilityCheckBoxList" runat="server">
            <asp:ListItem Text="Residents of Washington State" Value="residents"></asp:ListItem>
            <asp:ListItem Text="Students attending a college in Washington State" Value="students"></asp:ListItem>
        </asp:checkboxlist>
    </fieldset>
</div>
<asp:customvalidator ID="availabilityCheckCustomValidator" runat="server" Text="Scholarship not activated!  The scholarship must be available to residents of Washington or to students attending a college in Washington"
    ErrorMessage="Scholarship not activated!  The scholarship must be available to residents of Washington or to students attending a college in Washington"></asp:customvalidator>

<div id="activationConfirmDiv" title="Request Activation" style="display:none">
    Once you activate the scholarship, you will not be able to edit the scholarship criteria.<br /><br />
    Activate scholarship?
</div> 
  
<div>
    <div id="showAdministratorsDiv" runat="server" visible="false">
        <br />
        <fieldset>
            <legend>Scholarship Provider/Intermediary Administrators</legend>

            <p>
                Note!  Only a Provider or Intermediary Admin has access to activate the scholarship. To 
                activate this scholarship, contact your administrator:  
            </p>
            <asp:Repeater runat="server" ID="adminRepeater" runat="server">
                <HeaderTemplate><ul></HeaderTemplate>
                <ItemTemplate><li><%# DataBinder.Eval(Container.DataItem, "value") %></li></ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
         </fieldset>
        <br />
    </div>
    
    <div id="scholarshipValidationErrors" class="issueList" runat="server" title="Scholarship activation">
        <br />
        <p class="form-section-title">Scholarship Validation Errors</p>

        <p>
            One or more validation(s) have failed across all data entry fields for this scholarship. They must to be
            fixed before scholarship can be activated. We recommend you to go through each tab, starting 
            from the first and fix these issues.
        </p>
        <asp:Repeater ID="IssueListControl" runat="server">
            <HeaderTemplate><ul></HeaderTemplate>
            <ItemTemplate><li><%# DataBinder.Eval(Container.DataItem, "ErrorMessage") %></li></ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
        
        <br />
        <hr />
    </div>

    <sbCommon:AnchorButton id="previewButton" runat="server" Text="Preview Scholarship" />
            
    <sbCommon:ConfirmButton ID="confirmActivationBtn" ConfirmMessageDivID="activationConfirmDiv"  
            Text="Activate Scholarship"  runat="server" Width="150px" OnClickConfirm="confirmActivationBtn_Click" />
    <br />
    <br />
    <br />        
</div>