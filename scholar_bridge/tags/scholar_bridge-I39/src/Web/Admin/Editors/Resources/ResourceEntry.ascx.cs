﻿using System;
using System.IO;
using System.Web.Caching;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Admin.Editors.Resources 
{

    public partial class ResourceEntry : UserControl
    {
        public IResourceService ResourceService { get; set; }
		public IUserContext UserContext { get; set; }

        public delegate void ResourceSaved(Resource resource);
        public delegate void ResourceCanceled();

        public event ResourceSaved FormSaved;
        public event ResourceCanceled FormCanceled;

		public TempAttachment Attachment
    	{
    		get
    		{
				if (ViewState["Attachment"] == null)
    				return null;
				return (TempAttachment)ViewState["Attachment"];
    		}
			set
			{
				ViewState["Attachment"] = value;
			}
    	}


		public int? ResourceID 
		{ 
			get
			{
				if (ViewState["ResourceID"] == null)
					return new int?();
				return (int?) ViewState["ResourceID"];
			} 
			set
			{
				ViewState["ResourceID"] = value;
				resourceInContext = null;
				SetPageValues();
			} 
		}

    	private Resource resourceInContext;
        private Resource ResourceInContext
    	{
			get
			{
				if (resourceInContext == null)
				{
					resourceInContext = !ResourceID.HasValue 
						? new Resource() 
						: ResourceService.GetById(ResourceID.Value);
				}
				return resourceInContext;
			}
    	}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) 
				return;

            if (null == ResourceInContext)
            {
                errorMessage.Visible = true;
                return;
            }
        	SetPageValues();
        }

		private void SetPageValues()
		{
			titleControl.Text = ResourceInContext.Title;
			urlTextBox.Text = ResourceInContext.URL;
			if (ResourceInContext.AttachedFile != null)
			{
				ShowAttachedFilePanel();
				attachedFile.Text = ResourceInContext.AttachedFile.Name;
				Attachment = new TempAttachment(ResourceInContext.AttachedFile.Name, ResourceInContext.AttachedFile.Bytes, ResourceInContext.AttachedFile.MimeType, true);
			}
			else
			{
				HideAttachedFilePanel();
				attachedFile.Text = "";
				Attachment = null;
			}
			displayOrderBox.Text = ResourceInContext.DisplayOrder.HasValue ? ResourceInContext.DisplayOrder.Value.ToString() : "";
			descriptionControl.Text = ResourceInContext.Description;
		}


        protected void saveButton_Click(object sender, EventArgs e)
        {   Page.Validate();
            if (!Page.IsValid) return;
            
			ResourceInContext.Title = titleControl.Text;
        	ResourceInContext.Description = descriptionControl.Text;
        	ResourceInContext.URL = urlTextBox.Text;
        	resourceInContext.DisplayOrder = int.Parse(displayOrderBox.Text);
        	Attachment deletableAttachment = null;
			if (Attachment != null)
			{
				if (resourceInContext.AttachedFile != null)
					ReplaceAttachment();
				else
					CreateAttachment();

				SaveFile();
			}
			else
			{
				if (resourceInContext.AttachedFile != null)
					deletableAttachment = resourceInContext.AttachedFile;
				resourceInContext.AttachedFile = null;	
			}
            ResourceInContext.LastUpdate=new ActivityStamp(UserContext.CurrentUser);
            ResourceService.Save(ResourceInContext);
			if (deletableAttachment != null)
				deletableAttachment.RemoveFile(ConfigHelper.GetAttachmentsDirectory());

            if (null != FormSaved)
            {
                FormSaved(ResourceInContext);
            }
        }

		private void SaveFile()
		{
			if (!Attachment.IsCurrentlyAttached)
			{
				FileStream fs = new FileStream(ResourceInContext.AttachedFile.GetFullPath(ConfigHelper.GetAttachmentsDirectory()), 
												FileMode.Create, 
												FileAccess.ReadWrite);
				BinaryWriter bw = new BinaryWriter(fs);
				bw.Write((byte[])Cache[Attachment.UniqueName]);
				bw.Close(); 
			}
		}

		 
        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }

		protected void UploadFile_Click(object sender, EventArgs e)
		{
			if (attachFile.HasFile)
			{
				//check if a valid filename is being uploaded
				if (String.IsNullOrEmpty(attachFile.FileName))
				{
					attachmentValidator.IsValid = false;
					attachmentValidator.Text = "Please choose a valid file to attach and try again.";
					return;
				}
				
				if (Attachment == null)
					Attachment = CreateTempAttachment();

				try
				{
					//cache the uploaded file for 10 minutes
					//AttachFile.MoveTo(attachment.GetFullPath(ConfigHelper.GetAttachmentsDirectory()), MoveToOptions.Overwrite);
					if (!(Cache[Attachment.UniqueName] == null))
						Cache.Remove(Attachment.UniqueName);

					CacheTheFile(attachFile.FileBytes);
				}
				catch (Exception)
				{
					Cache.Remove(Attachment.UniqueName);
					attachmentValidator.IsValid = false;
					attachmentValidator.Text = "Could not upload the file. Please try again.";
				}
				attachedFile.Text = Attachment.Name;
				ShowAttachedFilePanel();
			}
		}

    	private void CacheTheFile(Byte[] fileByteArray)
    	{
			Cache.Add(Attachment.UniqueName, fileByteArray, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
    	}

    	private void ShowAttachedFilePanel()
		{
			selectedFilePanel.Visible = true;
			selectFilePanel.Visible = false;
		}

		private void HideAttachedFilePanel()
		{
			selectedFilePanel.Visible = false;
			selectFilePanel.Visible = true;
		}

		private TempAttachment CreateTempAttachment()
		{
			string mimeType = attachFile.PostedFile.ContentType;
			if (String.IsNullOrEmpty(mimeType))
				mimeType = "application/octet-stream";
			var attachment = new TempAttachment(Path.GetFileName(attachFile.FileName), attachFile.FileContent.Length, mimeType, false);
			return attachment;
		}

		private void CreateAttachment()
		{
			var attachment = new Attachment
			{
				Name = Attachment.Name,
				Comment = "Resources File - " + Attachment.Name,
				Bytes = Attachment.Bytes,
				MimeType = Attachment.MimeType,
				LastUpdate = new ActivityStamp(UserContext.CurrentUser)
			};
			attachment.GenerateUniqueName();
			ResourceInContext.AttachedFile = attachment;
			
        }
		private void ReplaceAttachment()
		{
			if (!Attachment.IsCurrentlyAttached)
			{
				ResourceInContext.AttachedFile.Name = Attachment.Name;
				ResourceInContext.AttachedFile.Comment = "Resources File - " + Attachment.Name;
				ResourceInContext.AttachedFile.Bytes = Attachment.Bytes;
				ResourceInContext.AttachedFile.MimeType = Attachment.MimeType;
				ResourceInContext.AttachedFile.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
			}
		}

		[Serializable]
		public class TempAttachment
		{
			public string Name { get; set; }
			public string MimeType { get; set; }
			public double Bytes { get; set; }
			public string UniqueName { get; set; }
			public bool IsCurrentlyAttached { get; set; }
			public TempAttachment(string name, double bytes, string mimeType, bool isCurrentlyAttached)
			{
				Name = name;
				Bytes = bytes;
				MimeType = mimeType;
				UniqueName = new Guid().ToString();
				IsCurrentlyAttached = isCurrentlyAttached;
			}
		}

		protected void removeFile_Click(object sender, EventArgs e)
		{
			if (Cache[Attachment.UniqueName] != null)
				Cache.Remove(Attachment.UniqueName);

			Attachment = null;
			HideAttachedFilePanel();
		}

    }
}