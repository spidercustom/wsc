﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FaqEntry.ascx.cs" Inherits="ScholarBridge.Web.Admin.Editors.Faqs.FaqEntry" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>           
<asp:Label ID="errorMessage" runat="server" Text="Faq not found." Visible="false" CssClass="errorMessage"/>
<div class="form-iceland-container">
    <div class="form-iceland">
        <label id="faqTypeLabel" for="faqTypeButtons">FAQ Category:</label>
        <sb:EnumRadioButtonList id="faqTypeButtons" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.FaqType, ScholarBridge.Domain" />
        <br />
        
        <label for="questionControl">Faq Question:</label>
        <br />
        <asp:TextBox ID="questionControl" runat="server" TextMode="MultiLine" Rows="3" Width="600px" Height="50px"></asp:TextBox>
        
        <sb:CoolTipInfo Content="This will appear as the FAQ question." runat="server" />
        <asp:RequiredFieldValidator ID="questionControlRequiredValidator" runat="server" ControlToValidate="questionControl"
         ErrorMessage="Should not be empty"></asp:RequiredFieldValidator>
        <elv:PropertyProxyValidator ID="questionControlValidator" runat="server" ControlToValidate="questionControl" 
        PropertyName="Question" SourceTypeName="ScholarBridge.Domain.Faq" ErrorMessage="should not exceed 250 characters." />
        <br />
        <label for="answerControl">Faq Answer:</label><br />
        <asp:TextBox ID="answerControl" runat="server" TextMode="MultiLine" Rows="10" Width="600px" Height="300px"></asp:TextBox>
        <sb:CoolTipInfo Content="This will appear as the answer associated with the FAQ question." runat="server" />
        <asp:RequiredFieldValidator ID="answerControlRequiredValidator" runat="server" ControlToValidate="answerControl"
         ErrorMessage="Should not be empty."></asp:RequiredFieldValidator>
        <elv:PropertyProxyValidator ID="answerControlValidator" runat="server" ControlToValidate="answerControl" 
        PropertyName="Answer" SourceTypeName="ScholarBridge.Domain.Faq" ErrorMessage="Should not exceed 4000 characters."  />
        <br />
        <br />
        <sbcommon:anchorbutton ID="saveButton" runat="server" Text="Save" 
            onclick="saveButton_Click"   />
        <sbcommon:anchorbutton ID="cancelButton" runat="server" Text="Cancel" 
            CausesValidation="false" onclick="cancelButton_Click" />
        <br />
</div> 
</div>