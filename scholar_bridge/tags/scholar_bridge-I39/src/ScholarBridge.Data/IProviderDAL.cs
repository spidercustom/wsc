using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IProviderDAL : IOrganizationDAL<Provider>
    {
    }
}