using NUnit.Framework;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class ApplicationTests
    {

        [Test]
        public void can_edit_if_not_submitted()
        {
            var a = new Application();
            Assert.IsTrue(a.CanEdit());

            a.Stage = ApplicationStages.NotActivated;
            Assert.IsTrue(a.CanEdit());

            a.Stage = ApplicationStages.Submitted;
            Assert.IsFalse(a.CanEdit());
        }


        [Test]
        public void submit_sets_values()
        {
            var a = new Application();
            a.Submit();
            Assert.AreEqual(ApplicationStages.Submitted, a.Stage);
            Assert.IsTrue(a.SubmittedDate.HasValue);
        }
    }
}