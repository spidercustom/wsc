﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain.Lookup
{
    public class LookupBase : ILookup
    {
        public virtual object Id { get; set; }

        [NotNullValidator]
        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual bool Deprecated { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }


        public virtual int GetIdAsInteger()
        {
            return (int)Id;
        }

        public virtual string GetIdAsString()
        {
            return (string)Id;
        }

    }
}
