﻿
namespace ScholarBridge.Domain
{
    public enum ApplicationStage
    {
        None,
        NotActivated,
        Submitted
    }
}
