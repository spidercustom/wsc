﻿using System;

namespace ScholarBridge.Common
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class DisplayOrderAttribute : Attribute
    {
        public int DisplayOrder { get; set; }
        public DisplayOrderAttribute(int displayOrder)
        {
            DisplayOrder = displayOrder;
        }
    }
}
