using System;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class MatchTests
    {
        [Test]
        public void scholarship_name_null_by_default()
        {
            var m = new Match();
            Assert.IsNull(m.ScholarshipName);
        }

        [Test]
        public void scholarship_name_same_as_scholarship()
        {
            var s = new Scholarship {Name = "Test"};
            var m = new Match {Scholarship = s};
            Assert.AreEqual(s.Name, m.ScholarshipName);
        }

        [Test]
        public void criteria_percents_handle_zero()
        {
            var m = new Match();
            Assert.AreEqual(1, m.ComputeMinimumCriteriaPercent());
            Assert.AreEqual(1, m.ComputePreferredCriteriaPercent());
        }

        [Test]
        public void criteria_percents_handle_val()
        {
            var m = new Match
                        {
                            SeekerMinimumCriteriaCount = 2,
                            ScholarshipMinimumCriteriaCount = 4,
                            SeekerPreferredCriteriaCount = 2,
                            ScholarshipPreferredCriteriaCount = 10
                        };
            Assert.AreEqual(.5, m.ComputeMinimumCriteriaPercent(), 0.001);
            Assert.AreEqual(.2, m.ComputePreferredCriteriaPercent(), 0.001);
        }

        [Test]
        public void criteria_percents_to_strings()
        {
            var m = new Match
            {
                SeekerMinimumCriteriaCount = 2,
                ScholarshipMinimumCriteriaCount = 4,
                SeekerPreferredCriteriaCount = 2,
                ScholarshipPreferredCriteriaCount = 10
            };
            Assert.AreEqual("2 of 4", m.MinumumCriteriaString);
            Assert.AreEqual("2 of 10", m.PreferredCriteriaString);
        }

        [Test]
        public void validate_not_applied_state_when_match_is_new()
        {
            var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(1));
            Assert.AreEqual(MatchApplicationStatus.NotApplied, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.NotApplied.GetDisplayName(), match.MatchApplicationStatusString);
        }

        [Test]
        public void validate_not_applied_state_when_match_is_saved()
        {
            var match = CreateMatch(MatchStatus.Saved, DateTime.Today.AddDays(1));
            Assert.AreEqual(MatchApplicationStatus.NotApplied, match.MatchApplicationStatus);
        }

        [Test]
        public void validate_applying_state_when_applied_but_not_submitted()
        {
            var match = CreateMatch(MatchStatus.Submitted, DateTime.Today.AddDays(1));
            match.Application = CreateApplication(ApplicationStage.NotActivated, match.Scholarship);
            match.Application.ApplicationStatus = ApplicationStatus.Applying ;
            Assert.AreEqual(MatchApplicationStatus.Applying, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.Applying.GetDisplayName(), match.MatchApplicationStatusString);
        }
        
        [Test]
        public void validate_applied_state_when_application_is_submitted()
        {
            var match = CreateMatch(MatchStatus.Submitted, DateTime.Today.AddDays(1));
            match.Application = CreateApplication(ApplicationStage.Submitted, match.Scholarship);
            match.Application.ApplicationStatus = ApplicationStatus.Submitted;
            Assert.AreEqual(MatchApplicationStatus.Applied, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.Applied.GetDisplayName(), match.MatchApplicationStatusString);
        }

        [Test]
        public void validate_being_consider_state_when_submitted_and_due_date_passed()
        {
            var match = CreateMatch(MatchStatus.Submitted, DateTime.Today.AddDays(-1));
            match.Application = CreateApplication(ApplicationStage.Submitted, match.Scholarship);
            match.Application.ApplicationStatus = ApplicationStatus.BeingConsidered;
            Assert.AreEqual(MatchApplicationStatus.BeingConsidered, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.BeingConsidered.GetDisplayName(), match.MatchApplicationStatusString);
        }

        [Test]
        public void validate_closed_state_when_not_applied_and_due_date_passed()
        {
            var match = CreateMatch(MatchStatus.Saved, DateTime.Today.AddDays(-1));
            Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.Closed.GetDisplayName(), match.MatchApplicationStatusString);
        }

		[Test]
		public void validate_closed_state_when_not_submitted_and_due_date_passed()
		{
			var match = CreateMatch(MatchStatus.Saved, DateTime.Today.AddDays(-1));
			match.Application = CreateApplication(ApplicationStage.NotActivated, match.Scholarship);
            match.Application.ApplicationStatus = ApplicationStatus.Closed;
			Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
		}

		[Test]
		public void validate_closed_state_when_new_and_due_date_passed()
		{
			var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(-1));
			match.Application = CreateApplication(ApplicationStage.NotActivated, match.Scholarship);
            match.Application.ApplicationStatus = ApplicationStatus.Closed;
			Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
		}

		[Test]
		public void validate_notapplied_state_when_new_and_on_due_date()
		{
			var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(-1));
			match.Application = CreateApplication(ApplicationStage.NotActivated, match.Scholarship);
            match.Application.ApplicationStatus = ApplicationStatus.Closed;
			Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
		}
		[Test]
		public void validate_minimum_match_percentage()
		{
			var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(-1));
			match.SeekerMinimumCriteriaCount = 3;
			match.ScholarshipMinimumCriteriaCount = 6;
            Assert.AreEqual(0.5, match.ComputeMinimumCriteriaPercent());
			match.ScholarshipMinimumCriteriaCount = 0;
            Assert.AreEqual(1.0, match.ComputeMinimumCriteriaPercent());
			match.ScholarshipMinimumCriteriaCount = 8;
            Assert.AreEqual(.375, match.ComputeMinimumCriteriaPercent());
		}

		[Test]
		public void validate_preferred_match_percentage()
		{
			var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(-1));
			match.SeekerPreferredCriteriaCount = 3;
			match.ScholarshipPreferredCriteriaCount = 6;
            Assert.AreEqual(0.5, match.ComputePreferredCriteriaPercent());
			match.ScholarshipPreferredCriteriaCount = 0;
            Assert.AreEqual(1.0, match.ComputePreferredCriteriaPercent());
			match.ScholarshipPreferredCriteriaCount = 8;
            Assert.AreEqual(.375, match.ComputePreferredCriteriaPercent());
		}

		[Test]
		public void validate_compute_rank_functions_correctly()
		{
			var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(-1));
			match.SeekerMinimumCriteriaCount = 3;
			match.ScholarshipMinimumCriteriaCount = 6;
			match.SeekerPreferredCriteriaCount = 3;
			match.ScholarshipPreferredCriteriaCount = 6;
			match.ComputeRank();
			Assert.AreEqual(50m, match.Rank);
			match.SeekerMinimumCriteriaCount = 4;
			match.ScholarshipMinimumCriteriaCount = 5;
			match.SeekerPreferredCriteriaCount = 1;
			match.ScholarshipPreferredCriteriaCount = 4;
			match.ComputeRank();
			Assert.AreEqual(61.85m, match.Rank);
		}

		private static Application CreateApplication(ApplicationStage stage, Scholarship scholarship)
		{
            return new Application(ApplicationType.Internal)
			       	{
			       		Stage = stage,
			       		Scholarship = scholarship
                        
			};
		}

		private static Match CreateMatch(MatchStatus status, DateTime scholarshipDueDate)
        {
            return CreateMatch(status, scholarshipDueDate, scholarshipDueDate.AddDays(1));
        }

        private static Match CreateMatch(MatchStatus status, DateTime scholarshipDueDate, DateTime scholarshipAwardDate)
        {
            return new Match
                       {
                           MatchStatus = status,
                           Scholarship = new Scholarship
                                             {
                                                 ApplicationDueDate = scholarshipDueDate,
                                                 AwardDate = scholarshipAwardDate
                                             }
                       };
        }





    }
}