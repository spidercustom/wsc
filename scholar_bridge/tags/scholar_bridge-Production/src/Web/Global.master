﻿<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="Global.master.cs" Inherits="ScholarBridge.Web.GlobalMasterPage" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>
<%@ Register Src="~/Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="sb" %>
<%@ Register Src="~/Common/GlobalFooter.ascx" TagName="GlobalFooter" TagPrefix="sb" %>
<%@ Register Src="~/Common/ScholarshipSearchBox.ascx" TagName="ScholarshipSearchBox" TagPrefix="sb" %>
<%@ Register Src="~/Common/UnreadMessageLink.ascx" TagName="UnreadMessageLink" TagPrefix="sb" %>
<%@ Register Src="~/Common/MyHomeLink.ascx" TagName="MyHomeLink" TagPrefix="sb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="FORMAT" content="text/html" />
    <meta name="CHARSET" content="ISO-8859-1" />
    <meta name="DOCUMENTLANGUAGECODE" content="en" />
    <meta name="DOCUMENTCOUNTRYCODE" content="us" />
    <meta name="DC.LANGUAGE" scheme="rfc1766" content="en-us" />
    <meta name="COPYRIGHT" content="Copyright (c) 2009 by Washington Scholarship Coalition" />
    <meta name="SECURITY" content="Public" />
    <meta name="ROBOTS" content="index,follow" />
    <meta name="GOOGLEBOT" content="index,follow" />
    <meta name="Keywords" content="washington, scholarship, matches, students, college, high school, school" />
    <meta name="Author" content="theWashBoard.org" />
    
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">google.load("jquery", "1.3");</script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.7.1.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tablesorter.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.pager.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tinysort.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskedinput.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.cookie.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.watermark-2.0.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-idleTimeout.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/superfish.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/site.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/cooltip.js") %>"></script> 
    <script type="text/javascript" src="<%= ResolveUrl("~/js/tooltip.js") %>"></script> 
        <script type="text/javascript">
            /** Jquery Session Time out Popup **/
            $(document).ready(function() {
                $(document).idleTimeout({
                    redirect_url: '<%= LinkGenerator.GetFullLinkStatic("/login.aspx")  %>',
                    logout_url: '<%= LinkGenerator.GetFullLinkStatic("/logout.aspx")  %>'
                });
            });        
        </script>
    <link href="<%= ResolveUrl("~/styles/jquery-ui-1.7.1.custom.css") %>" rel="stylesheet" type="text/css" media="All"/> 
    <link href="<%= ResolveUrl("~/styles/global.css") %>" rel="stylesheet" type="text/css" media="All"/> 
    <link href="<%= ResolveUrl("~/styles/print.css") %>" rel="stylesheet" type="text/css" media="print" />
    
    <asp:ContentPlaceHolder id="Head" runat="server" />
</head>
<body onkeypress="return disableEnterKey(event)">
    <form id="defaultForm" runat="server">
    <asp:PlaceHolder runat="server" ID="ImpersonationPlaceholder">
        <div id="ImpersonationInfo">
            Impersonated by  <%= ImpersonatingUser ?? "" %> <asp:LinkButton ID="stopImpersonationButton" runat="server" CausesValidation="false" OnCommand="stopImpersonationButton_Command">Stop Impersonation</asp:LinkButton>
        </div>
    </asp:PlaceHolder>
    <div id="MainWrapper">
        <div id="Header">
            <a style="float:left" href="<%= ResolveUrl("~/") %>"><img alt="theWashBoard.org" src="<%= ResolveUrl("~/images/LogoTheWashBoard.gif") %>" /></a>
            <asp:PlaceHolder runat="server" ID="WelcomeContainer" Visible="false">
                <div id="WelcomeMessage">
                    <div id="UserName">Welcome back <asp:literal ID="UserFullName" runat="server"></asp:literal>!</div>
                    <div id="OrgName"><asp:Literal ID="OrganizationName" runat="server" /></div>
                </div>
                
                <div id="UserLinks">
                    <sb:MyHomeLink ID="MyHomeLink" runat="server" />
                    &nbsp; | &nbsp;
                    <sb:UnreadMessageLink ID="UnreadMessageLink2" runat="server" />
                    <asp:LoginStatus ID="LoginStatus1" LogoutAction="RedirectToLoginPage" runat="server" />
                </div>
            </asp:PlaceHolder>
            
            <sb:ScholarshipSearchBox ID="ScholarshipSearchBox1" runat="server" />
            <div id="Navigation"><sb:MainMenu ID="MainMenu1" runat="server" /></div>
        </div>
        
        <div id="PageImage">
            <asp:ContentPlaceHolder ID="PageImage" runat="server" />
        </div> 
        
        <div id="PageContent-Wrapper"><div id="PageContent">
           <asp:ContentPlaceHolder id="Body" runat="server" />
           <sb:GlobalFooter ID="GlobalFooter" runat="server" />
        </div></div>  
    </div>
    </form>
<div id="TooltipContainer"></div>

<script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));</script>
<script type="text/javascript"> try { var pageTracker = _gat._getTracker("UA-16097208-1"); pageTracker._trackPageview(); } catch (err) { }</script>
</body>
</html>
