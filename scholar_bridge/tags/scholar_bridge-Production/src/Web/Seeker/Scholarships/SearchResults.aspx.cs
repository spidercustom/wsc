﻿using System;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Seeker.Scholarships
{
    public partial class SearchResults : SBBasePage
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        private string Criteria
        {
            get
            {
                return Request.Params["val"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
				if ((Criteria == null) || (Criteria.Trim() == string.Empty) || (Criteria.Trim() == "Search Scholarships"))
				{
					matchesFoundPanel.Visible = false;
					noMachPanel.Visible = true;
				}
				else
				{
					matchesFoundPanel.Visible = true;
					noMachPanel.Visible = false;
					if (Page.User.Identity.IsAuthenticated)
					{
						ScholarshipSearchResults1.Criteria = Criteria;
					}
					else
					{
						ScholarshipSearchResultsAnonymous1.Criteria = Criteria;
					}
					CriteriaLabel.Text = "'" + Criteria + "'";					
				}
            }
        }
    }
}
