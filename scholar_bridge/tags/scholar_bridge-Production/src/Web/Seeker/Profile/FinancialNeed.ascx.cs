﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class FinancialNeed : WizardStepUserControlBase<Domain.Seeker>
    {
        Domain.Seeker _seekerInContext;
        private const string DISABLE_SCRIPT_TEMPLATE = "javascript:$('#{0}').attr('disabled', 'disabled')";
        private const string ENABLE_SCRIPT_TEMPLATE = "javascript:$('#{0}').removeAttr('disabled')";

	    public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        Domain.Seeker SeekerInContext
        {
            get
            {
                if (_seekerInContext == null)
                    _seekerInContext = Container.GetDomainObject();
                return _seekerInContext;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context");

            if (!Page.IsPostBack)
            {
                PopulateScreen();
            }
        }
        private void PopulateScreen()
        {
            MyChallengeControl.Text = SeekerInContext.MyChallenge;
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());
            PopulateScreenFAFSA();
            if (null != SeekerInContext.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(SeekerInContext.SupportedSituation.TypesOfSupport, ts => ts.Id.ToString());
            }
        }

	    private void PopulateScreenFAFSA()
	    {
            FAFSACompletedControl.Checked = SeekerInContext.HasFAFSACompleted ?? false;
            FAFSANotCompletedControl.Checked = SeekerInContext.HasFAFSACompleted.HasValue && !SeekerInContext.HasFAFSACompleted.Value;
	        ExpectedFamilyContributionControl.Enabled = FAFSACompletedControl.Checked;
	        ExpectedFamilyContributionControl.Amount = SeekerInContext.ExpectedFamilyContribution ?? 0;
	    }

	    public override void PopulateObjects()
        {
            SeekerInContext.MyChallenge = MyChallengeControl.Text;
            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            SeekerInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            PopulateObjectsFAFSA();

            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (SeekerInContext.Stage < SeekerStage.NotActivated)
                SeekerInContext.Stage = SeekerStage.NotActivated;
        }

	    private void PopulateObjectsFAFSA()
	    {
            SeekerInContext.HasFAFSACompleted = null;
	        SeekerInContext.ExpectedFamilyContribution = null;
	        if (FAFSACompletedControl.Checked)
	        {
                SeekerInContext.HasFAFSACompleted = true;
	            SeekerInContext.ExpectedFamilyContribution = ExpectedFamilyContributionControl.Amount;
	        } 
	        if (FAFSANotCompletedControl.Checked)
                SeekerInContext.HasFAFSACompleted = false;
	    }

	    protected override void OnPreRender(EventArgs e)
        {
            FAFSACompletedControl.Attributes.Add("onclick", ENABLE_SCRIPT_TEMPLATE.Build(ExpectedFamilyContributionControl.ClientID));
            FAFSANotCompletedControl.Attributes.Add("onclick", DISABLE_SCRIPT_TEMPLATE.Build(ExpectedFamilyContributionControl.ClientID));
            base.OnPreRender(e);
        }

        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
       
        #region WizardStepUserControlBase methods

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            SeekerService.Update(SeekerInContext);
        }
        #endregion
	}
}