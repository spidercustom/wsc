﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activities.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.Activities" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>

<div class="two-column with_divider">
    <div class="question_list">
        <h2>Academic Interests</h2>
        <asp:PlaceHolder ID="AcademicAreasContainerControl" runat="server">
            <div>
                <label>
                    <span class="tip" title="Indicate the academic programs or fields of study you are considering to be matched with scholarships for those fields of study.">
                        Field of Study:
                    </span>
                </label>
                <div>
                    <sb:LookupDialog ID="AcademicAreasControlDialogButton" runat="server" BuddyControl="AcademicAreasControl" OtherControl="AcademicAreasOtherControl" OtherControlLabel="AcademicAreasOtherControlLabel" ItemSource="AcademicAreaDAL" Title="Field of Study"/>
                    <asp:TextBox ID="AcademicAreasControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="AcademicAreasOtherControlLabel" >
                <label>Others</label> 
                <asp:TextBox ID="AcademicAreasOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="CareersContainerControl" runat="server">
            <div>
                <label>
                    <span class="tip" title="Indicate the careers you are interested in pursuing to be matched with scholarships for those careers.">
                        Careers:
                    </span>
                </label>
                <div>
                    <sb:LookupDialog ID="CareersControlDialogButton" runat="server" BuddyControl="CareersControl"  OtherControl="CareersOtherControl" OtherControlLabel="CareersOtherControlLabel"  ItemSource="CareerDAL" Title="Career Selection"/>
                    <asp:TextBox ID="CareersControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="CareersOtherControlLabel">
                <label>Others:</label>
                <asp:TextBox ID="CareersOtherControl"  TextMode="MultiLine" runat="server" ></asp:TextBox>
            </div>
        </asp:PlaceHolder>
        <hr />
        <h2>Groups and Hobbies</h2>
        <asp:PlaceHolder ID="HobbiesContainerControl" runat="server">
            <div>
                <label>Hobbies:</label>
                <div>
                    <sb:LookupDialog ID="SeekerHobbiesControlDialogButton" runat="server" OtherControl="SeekerHobbiesOtherControl" BuddyControl="SeekerHobbiesControl" OtherControlLabel="SeekerHobbiesOtherControlLabel"  ItemSource="SeekerHobbyDAL" Title="Hobby Selection"/>
                    <asp:TextBox ID="SeekerHobbiesControl" TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="SeekerHobbiesOtherControlLabel">
                <label>Others:</label>
                <asp:TextBox ID="SeekerHobbiesOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="SportsContainerControl" runat="server">
            <div>
                <label>
                    <span class="tip" title="Indicate any organized sports you participate or have participated in.">
                        Sports
                    </span>
                </label>
                <div>
                    <sb:LookupDialog ID="SportsControlDialogButton" runat="server" OtherControl="SportsOtherControl" BuddyControl="SportsControl" OtherControlLabel="SportsOtherControlLabel" ItemSource="SportDAL" Title="Sport Participation Selection"/>
                    <asp:TextBox ID="SportsControl"  TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
                </div>                   
            </div>
            <div id="SportsOtherControlLabel">
                <label>Others:</label>
                <asp:TextBox ID="SportsOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="ClubsContainerControl" runat="server">
            <div>
                <label>
                    <span class="tip" title="Indicate any clubs you belong to, or have been a member of.">
                        Clubs:
                    </span>
                </label>
                <div>
                    <sb:LookupDialog ID="ClubsControlDialogButton" runat="server" OtherControl="ClubsOtherControl" BuddyControl="ClubsControl" OtherControlLabel="ClubsOtherControlLabel" ItemSource="ClubDAL" Title="Club Participation Selection"/>
                    <asp:TextBox ID="ClubsControl"  TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="ClubsOtherControlLabel">
                <label for="ClubsOtherControl" class="othercontrollabel">Others:</label>
                <asp:TextBox CssClass="othercontroltextarea" ID="ClubsOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>
            </div>
        </asp:PlaceHolder>        
    </div>
    <div class="question_list">
        <h2>Affiliations</h2>
        <asp:PlaceHolder ID="OrganizationsContainerControl" runat="server">
            <div>
                <label>
                   <span class="tip" title="Some scholarships are available to members of an organization and their family. Use to indicate if you or any family members are affiliated with a specific Organization.">
                        Organizations
                   </span>
                </label>
                <div>
                    <sb:LookupDialog ID="OrganizationsControlDialogButton" runat="server"  OtherControl="OrganizationsOtherControl" BuddyControl="OrganizationsControl" OtherControlLabel="OrganizationsOtherControlLabel" ItemSource="SeekerMatchOrganizationDAL" Title="Organization Affiliation Selection"/>
                    <asp:TextBox ID="OrganizationsControl"  TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="OrganizationsOtherControlLabel">
                <label>Others:</label>
                <asp:TextBox ID="OrganizationsOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>    
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="AffiliationTypesContainerControl" runat="server">
            <div>
                <label>
                   <span class="tip" title="Some scholarships are available to company employees and their children. Use to indicate if you or any family members work for a specific company.">
                       Companies
                   </span>
                </label>
                <div>
                    <sb:LookupDialog ID="AffiliationTypesControlDialogButton" runat="server" OtherControl="AffiliationTypesOtherControl" BuddyControl="AffiliationTypesControl" OtherControlLabel="AffiliationTypesOtherControlLabel" ItemSource="CompanyDAL" Title="Companies Selection"/>
                    <asp:TextBox ID="AffiliationTypesControl"  TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="AffiliationTypesOtherControlLabel">
                <label>Others:</label>
                <asp:TextBox ID="AffiliationTypesOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>    
            </div>
        </asp:PlaceHolder>
        <hr />
        <h2>Work and Service</h2>
        <asp:PlaceHolder ID="WorkTypeContainerControl" runat="server">
            <div>
                <label>
                    <span class="tip" title="Are you working full time, part-time, or for a family business? Many scholarships take your work situation into consideration when awarding scholarships. Please provide brief details.">
                        Working?
                    </span>
                </label>
                <div>
                    <asp:CheckBox ID="WorkingControl" runat="server" /><br />
                    <asp:TextBox ID="WorkHistoryControl" Rows="5" TextMode="MultiLine" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="WorkHistoryControlValidator" Display="Dynamic" runat="server" ControlToValidate="WorkHistoryControl" PropertyName="WorkHistory" SourceTypeName="ScholarBridge.Domain.Seeker" />
                </div>
            </div>
        </asp:PlaceHolder>  
        <asp:PlaceHolder ID="WorkHoursContainerControl" runat="server">
            <div>
                <label>
                    <span class="tip" title="Are you, or have you volunteered or done community service? Many scholarships take volunteering into consideration when awarding scholarships. Please provide brief details.">
                        Volunteering:
                    </span>
                </label>
                <div>
                    <asp:CheckBox ID="VolunteeringControl" runat="server" /><br />
                    <asp:TextBox ID="ServiceHistoryControl" Rows="5" TextMode="MultiLine" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="ServiceHistoryControlValidator" Display="Dynamic" runat="server" ControlToValidate="ServiceHistoryControl" PropertyName="ServiceHistory" SourceTypeName="ScholarBridge.Domain.Seeker" />
                </div>
            </div>
        </asp:PlaceHolder>              
    </div>
</div>
