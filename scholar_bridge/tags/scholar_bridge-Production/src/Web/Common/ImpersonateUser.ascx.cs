﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
	public partial class ImpersonateUser : System.Web.UI.UserControl
	{
		public IUserService UserService { get; set; }

		readonly ArrayList previouslyImpersonatedUsers = new ArrayList();

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			LoadPriorImpersonationsFromCookie();
			if (!IsPostBack)
			{
				previousUsersList.Visible = true;
				previousUsersList.DataSource = previouslyImpersonatedUsers;
				previousUsersList.DataBind();
			}
			else
			{
				previousUsersList.Visible = false;
			}
		}

		private void LoadPriorImpersonationsFromCookie()
		{
			if (Page.Request.Cookies[Web.ImpersonateUser.IMPERSONATION_COOKIE_NAME] != null)
			{
				foreach (string id in Page.Request.Cookies[Web.ImpersonateUser.IMPERSONATION_COOKIE_NAME].Values.GetValues(null))
				{
					if (id.Length > 0)
					{
						previouslyImpersonatedUsers.Add(UserService.FindByUsername(id));
						if (previouslyImpersonatedUsers.Count > 8)
							break;
					}
				}
			}
		}

		protected void SearchButton_Click(object sender, EventArgs e)
		{
			IList<User> userRows = UserService.SearchByUserName(userToFind.Text, 0, 50);

			listBox1.DataSource = userRows;
			listBox1.DataTextField = "Username";
			listBox1.DataValueField = "Username";
			listBox1.DataBind();
			if (listBox1.Items.Count == 1)
				listBox1.SelectedIndex = 0;

			listBox1.Visible = true;
			impersonateButton.Visible = true;
		}

		protected void ImpersonateButton_Click(object sender, EventArgs e)
		{
			Web.ImpersonateUser.Impersonate(Page.User.Identity.Name, listBox1.SelectedValue, Page, Session);
		}

		protected void PreviousUsersList_ItemCommand(object source, DataListCommandEventArgs e)
		{
			Web.ImpersonateUser.Impersonate(Page.User.Identity.Name, ((Label)e.Item.FindControl("Username")).Text, Page, Session);
		}
	}
}