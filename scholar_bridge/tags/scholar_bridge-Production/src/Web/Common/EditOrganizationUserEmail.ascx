﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditOrganizationUserEmail.ascx.cs" Inherits="ScholarBridge.Web.Common.EditOrganizationUserEmail" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>                         
<h2>Change E-mail Address</h2>
 
<p>
If you change your e-mail address you will logged out of the system and sent a confirmation e-mail. 
You will have to confirm your new e-mail address before you can sign in again.
</p>

<div class="question_list">
    <div>
        <label>Email Address:</label>
        <asp:TextBox ValidationGroup="EditOrganizationUserEmail_Validations" ID="Email" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ValidationGroup="EditOrganizationUserEmail_Validations" ID="EmailValidator" runat="server" ControlToValidate="Email" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
        <asp:CompareValidator  ValidationGroup="EditOrganizationUserEmail_Validations" ID="compareValidator" runat="server" ControlToValidate="Email" ControlToCompare="ConfirmEmail" ErrorMessage="Email and Confirm Email must match." />
    </div>
    <div>
        <label>Confirm Email Address:</label>
        <asp:TextBox  ValidationGroup="EditOrganizationUserEmail_Validations" ID="ConfirmEmail" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ValidationGroup="EditOrganizationUserEmail_Validations" ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" />
    </div>
    <div>
        <label></label>
        <sbCommon:AnchorButton ID="saveButton" runat="server" Text="Change E-mail Address" onclick="saveButton_Click" />
    </div>
</div>

<script language="javascript" type="text/javascript">
    $(document).ready(function() {
        $("#pnledituseremail").keypress(function(e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $("#pnledituseremail > a[id$='_saveButton']").click();
                return true;
            }
        });
    });
 </script>    
