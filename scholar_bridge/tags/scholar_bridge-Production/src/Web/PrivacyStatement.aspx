<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="PrivacyStatement.aspx.cs" Inherits="ScholarBridge.Web.PrivacyStatement" Title="Privacy Statement" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Privacy Statement" />
    <style type="text/css">
        #PageContent p{text-align:justify; font-size:12px; color:black;}
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="" height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Privacy Policy Statement</h2>

    <p>This Privacy Policy Statement
    explains the policies of The Washington Scholarship Coalition, a public/private
    collaboration of entities including, the Washington State Higher Education
    Coordinating Board, the Washington Financial Aid Association, The Seattle
    Foundation, Northwest Education Loan Association, College Spark, College
    Success Foundation, and College Planning Network (altogether, the
    &quot;Companies&quot; or &quot;us,&quot; &quot;We,&quot; &quot;we,&quot;
    &quot;Our,&quot; or &quot;our&quot;)� with respect to any personal identifying
    information you might supply to us or use in connection with this product or
    site on the Internet (the �Site�) as a service for scholarship seekers
    (students, parents and financial aid professionals) (�Seekers�) and scholarship
    intermediaries or providers (�Providers�) in the State of Washington.� The
    Companies respect the privacy of all participants on the Site, and we are
    committed to protecting user privacy on the Internet.�� </p>

    <p>We may include Provider information in analytical reporting in
    connection with the Site, but we will not report on a segment that has less
    than five Providers in it.� Participation in the Site by Providers is for the sole
    purpose of gathering and analyzing information leading to the successful award
    of scholarship funds to eligible Seekers.� Providers will only receive access
    to a specific Seeker profile when the Seeker submits his/her application.�
    Providers will retain and share with their representatives or agents (e.g.,
    selection committees) only the Seeker�s information that is necessary to
    fulfill their selection, disbursement and record-keeping obligations.� </p>

    <p>Providers will not use
    the Site to assemble a prospect list for alternative uses not connected with
    the Site nor for marketing any services/products/ideas of Provider or others in
    their regular course of business.� Providers will not distribute or otherwise
    share with any other entity, for financial gain or for free, the names or other
    personal information about Seekers or Providers.� Providers will have access to
    aggregated data about participants on the Site, both Seekers and Providers.� We
    can immediately de-activate your access to and participation in the Site for misuse
    of Seeker or Provider information.</p>



    <p>Providers may use the name, photo or other personal information about
    an award recipient in their marketing materials.� Providers will disclose their
    intent to use award recipient information and photos in the scholarship profile
    or attached guidelines included in the Site that is provided to Seekers.� </p>


    <p>We may access and use Provider
    and Seeker information to provide technical or other customer support in
    connection with the Site.� We may include Provider and Seeker information for
    analytical purposes and in reports, both published and internally used.� We
    will use our best efforts to present such information in a non-attributable form,
    unless a specific release has been provided to us by the Provider or Seeker.�
    We may collect email addresses through various features on the Site, including
    the �Refer a Friend� functionality. �These email addresses will be used only for
    purposes in connection with the Site and will not be used for marketing purposes
    by us, by Providers, or by any other third party.</p>

    <p>We may use and analyze
    Provider and Seeker information in order to study and present findings based on
    the information to educate and advocate on behalf of scholarship philanthropy
    and access to higher education.� We will access and use Provider and Seeker
    information in order to deliver services in connection with the Site.</p>

    <p>We may use &quot;cookies&quot; to enhance your online experience.�
    Cookies are pieces of data that we place on your computer's hard drive to help
    us identify you.� The cookies that we place on your hard drive allow us to
    immediately tell us who you are when you return to the Site.� They are not used
    to collect any nonpublic personal information.� Our Web server automatically
    recognizes your domain name, but not your e-mail address.� We do not collect
    nonpublic personal information from visitors to our Site unless you expressly
    provide it to us.� The information we do collect from this Site (such as domain
    name, number of hits, pages visited and length of user session) may be combined
    to analyze how our Site is used.� This information is then used to improve the
    usefulness of the Site. </p>

    <p>We collect only limited �personally identifiable information� on the
    Site.� Personally identifiable information is information about you, such as
    your name, address, telephone number and e-mail address.� We gather this
    information so we can administer the Site and so scholarships can be awarded by
    Providers to Seekers.� </p>

    <p>The types of personally identifiable information about you that we
    collect on this Site and the ways in which we use such information include the
    following: </p>
    <ul>
        <li>
            <p>Contact information, such as your name, address, e-mail address or
            telephone number (land-line or cellular).� Both the Companies and Providers may
            use this information, for example, to send you information that you request or
            to contact you about your scholarship applications.</p>
        </li>
        <li>
            <p>Identification information, such as a password or password
            challenge question/answer.� This information helps us, for example, identify
            you as a Site participant and prevent unauthorized use or unauthorized
            disclosure of your information.</p>
        </li>
        <li>
            <p>Financial information, such as your or your family�s income,
            assets or liabilities.� The Companies and Providers may use this information,
            for example, to evaluate scholarship applications.</p>
        </li>
    </ul>
    
    <p>The Companies carefully limit
    and control how we share personally identifiable information that we collect on
    this Site.� We may share this information with the following types of entities:
    </p>
    <ul>
        <li>
            <p><em>With an affiliated organization</em>.� We may share your
            nonpublic personal information with our affiliates as permitted by federal law,
            specifically the Gramm-Leach-Bliley Act of 1999.</p>
        </li>
        <li>
            <p><em>With other companies</em>.� To improve efficiency with
            which we provide you service, the Companies have contracted with other
            companies to perform various technical services in connection with the Site.�
            These contracted vendors are not affiliated with the Companies.� As part of
            these business relationships, the Companies may share your personal information
            with these contracted vendors to solve a technical Site issue or problem.� We
            require these contracted vendors to comply with strict standards of security
            and confidentiality, and they are not permitted to release, use or transfer any
            personal information to any other party for their own use.�</p>
        </li>
        <li>
            <p><em>In general</em>.� The Companies and Providers do not sell Seeker
            lists or individual Seeker information.� We will, however, exchange personal
            information about you with each other and with Providers in connection with the
            services provided on the Site, with the goal of matching scholarship Providers
            and Seekers.� The Companies also will exchange certain personal information
            about you when we are legally required, such as in response to a subpoena and
            to prevent fraud or to comply with a legally permitted inquiry by a government
            agency or regulator.</p>
        </li>
    </ul>
    
    <p>Unless prohibited by law, you
    have the right, upon reasonable notice, to view any personally identifiable
    information collected from you through your visits to or use of this Site, to
    make corrections to such information, if necessary, or to request that such
    information be removed from our systems.� You may do so by contacting us at the
    address indicated at the end of this Privacy Policy Statement. </p>

    <p>Our Site may also permit you to
    access non-affiliated Sites directly from our Site to provide you with
    value-added information, functionality or services.� If you provide personal
    information via these Sites, your information may be shared with these
    third-party entities.� It is also important to remember that, if you link to a
    non-affiliated Site from our Site, that party's privacy policy and its terms
    and conditions will apply to you.� We encourage you to learn about each third
    party's privacy policy before sharing personal information with them. </p>

    <p>The Companies do not knowingly
    collect, maintain or use personal information from this Site about children
    under age 13.� If a child whom we know to be under age 13 sends personal
    information to us online, we will use that information only to respond directly
    to that child, notify parents or seek parental consent.� We are not, however,
    responsible for the collection and use of information by non-affiliated
    companies and organizations that may be linked to this Site. </p>

    <p>Please remember that this
    Privacy Policy Statement applies only to the Companies and this Site.� It does
    not apply to Sites that may be linked to or from our Site or any other company
    or organization.� If you link to a non-affiliated Site from our Site, that
    party's privacy policy and its terms and conditions may apply to you.� We
    encourage you to learn about each third party's privacy policy before giving
    personal information to them. </p>

    <p>The effective date of this
    Privacy Statement is September 14, 2009.� It replaces all prior Privacy Policy
    Statements issued by us.� We reserve the right to change our Privacy Policy
    Statement.� Any such changes will be reflected in the updated version displayed
    on this Site. </p>

    <p>For additional information about
    this Privacy Policy Statement, please write: Washington Scholarship Coalition
    c/o Danette Knudson, NELA Director, External Relations, 190 Queen Anne Avenue
    N. Suite 300, Seattle, WA 98109</p>

</asp:Content>
