﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowOrg.ascx.cs" Inherits="ScholarBridge.Web.Admin.ShowOrg" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<br /><br />

<label for="statusLabel">Status:</label>
<asp:Label ID="statusLabel" runat="server" />
<br />

<label for="adminConfirmLabel">Admin User Has Confirmed Email?:</label>
<asp:Label ID="adminConfirmLabel" runat="server" />
<br />

<label for="orgNameLabel">Org Name:</label>
<asp:Label ID="orgNameLabel" runat="server" />
<br />

<label for="adminNameLabel">Admin Name:</label>
<asp:Label ID="adminNameLabel" runat="server" />
<br />

<label for="emailLiteral">Email:</label>
<asp:literal ID="emailLiteral" runat="server" />
<br />

<label for="taxIdLabel">TaxId (EIN):</label>
<asp:Label ID="taxIdLabel" runat="server" />
<br />

<label for="websiteLabel">Website:</label>
<asp:HyperLink ID="websiteLabel" runat="server" Target="_blank"></asp:HyperLink>
<br />

<label for="addressLabel">Address:</label>    
<asp:Label ID="addressLabel" runat="server" />
<br />

<label for="phoneLabel">Phone:</label>  
<asp:Label ID="phoneLabel" runat="server" />
<br />

<label for="faxLabel">Fax:</label>  
<asp:Label ID="faxLabel" runat="server" />
<br />

<label for="otherPhoneLabel">Other Phone:</label>  
<asp:Label ID="otherPhoneLabel" runat="server" />
<br />

    <table>
        <tr>
            <td>
                <label for="existingNotesTb">Administrative Notes:</label>  
            </td>
            <td>
                <asp:panel ID="existingNotesPanel" runat="server" ScrollBars="Vertical" CssClass="adminNoteDisplay">
                    <asp:Literal ID="notesLiteral" runat="server"></asp:Literal>
                </asp:panel>
            </td>
        </tr>
        <tr>
            <td>
                <label for="existingNotesTb">Add Notes:</label>  
            </td>
            <td>
                <asp:TextBox ID="newNotesTb" CssClass="adminNoteEntry"  runat="server" TextMode="MultiLine" Columns="300" Rows="5"></asp:TextBox><br />
            </td>        
        </tr>
        <tr>
            <td>
            
            </td>
            <td>
                <sbCommon:AnchorButton ID="addNotes" runat="server" Text="Add Note" onclick="addNotes_Click"  />
            </td>
        </tr>
    </table>
    <br />
