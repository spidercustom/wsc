﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScholarBridge.Web
{
	public interface IChangeCheckingPage
	{
		#region Form Data Changed checker fields

		/// <summary>
		/// Indicate whether or not to check for changed data entry controls.
		bool CheckForDataChanges
		{
			get; set;
		}
		/// <summary>
		/// This property is used to track the dirty state of the data on
		/// a form if change checking is enabled.
		/// </summary>
		bool Dirty { get; set; }

		/// <summary>
		/// This property is used to set or get the message displayed if
		/// change checking is enabled and the user attempts to leave without
		/// saving the changes <b>(Internet Explorer only)</b>.  A default
		/// message is used if not set.
		/// </summary>
		string ConfirmLeaveMessage
		{
			get; set;
		}

		/// <summary>
		/// This is used to set a list of control IDs that should not
		/// trigger the data change check (i.e. a save button).
		/// </summary>
		List<string> BypassPromptIds
		{
			get; set;
		}

		/// <summary>
		/// This is used to set a list of control IDs that should not
		/// be included when checking for data changes (i.e. changeable
		/// message text boxes, read-only or criteria fields that get
		/// modified but do not affect the state of the data to save, etc).
		/// </summary>
		List<string> SkipDataCheckIds
		{
			get; set;
		}

		#endregion
	}
}
