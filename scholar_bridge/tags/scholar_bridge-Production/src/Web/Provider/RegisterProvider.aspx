<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="RegisterProvider.aspx.cs" Inherits="ScholarBridge.Web.Provider.RegisterProvider" Title="Register Provider" %>
<%@ Register TagPrefix="sb" TagName="RegisterOrganization" Src="~/Common/RegisterOrganization.ascx" %>


<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="Provider Organization Registration" />
    
    <link href="<%= ResolveUrl("~/styles/form.css") %>" rel="stylesheet" type="text/css" media="All"/> 

    <script type="text/javascript">
        $(document).ready(function() {
            $(".tip").tooltip({
                position: "center right",
                effect: "slide",
                direction: "right",
                bounce: true,
                tip: '#TooltipContainer'
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="270" width="873" src="<%= ResolveUrl("~/images/header/provide_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <sb:RegisterOrganization id="registerOrg" runat="server" Title="Provider Organization Registration" />
</asp:Content>
