﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using Spring.Context.Support;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Default : Page
    {
         
         
        public const string EditActionLink = "~/Provider/BuildScholarship/Default.aspx";
        private const string DEFAULT_SORTEXPRESSION = "Status DESC";
        protected void Page_Init(object sender, EventArgs e)
        {
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.ProviderScholarships);
            UserContext.EnsureProviderIsInContext();
            
        }
        public IScholarshipService ScholarshipService
        {
            get { return (IScholarshipService)ContextRegistry.GetContext().GetObject("ScholarshipService"); }
        }

        public IIntermediaryService IntermediaryService
        {
            get { return (IIntermediaryService)ContextRegistry.GetContext().GetObject("IntermediaryService"); }
        }

        public IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship)((ListViewDataItem)e.Item).DataItem);
                var literal = (PlaceHolder)e.Item.FindControl("literalApplicants");

                if (scholarship.IsActivated())
                {
                    var link = new HyperLink();
                    link.Text = scholarship.ApplicantsCount.ToString();
                    link.Visible = true;
                    if (scholarship.ApplicantsCount > 0)
                    {
                        link.NavigateUrl = EditActionLink  + "?resumefrom=6&id=" + scholarship.Id.ToString();
                        literal.Controls.Add(link);
                    }
                    else
                    {
                        literal.Controls.Add(new Label() { Text = scholarship.ApplicantsCount.ToString() });
                    }
                }
                else
                {
                    literal.Controls.Add(new Label() { Text = scholarship.ApplicantsCount.ToString() });
                }
            }
        }

        protected void ScholarshipDataSource_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            var arrstages = scholarshipsListViewOptions.GetSelectedStages();
            var stages = "";
            foreach (ScholarshipStage s in arrstages)
            {
                stages = stages + ","+ Enum.GetName(typeof(ScholarshipStage), s);
            }

            e.InputParameters["orgId"] = scholarshipsListViewOptions.GetSelectedOrganizationId();
            e.InputParameters["stages"] = stages;
            
            

        }
        public IList<Scholarship> GetScholarships(string sortExpression, int startRowIndex, int maximumRows,int orgId,string stages)
        {
            if (string.IsNullOrEmpty(sortExpression))
                sortExpression = DEFAULT_SORTEXPRESSION;

            var stagesArray = GetStagesArray(stages);

            var provider = UserContext.CurrentProvider;

            if (orgId == 0)
                return ScholarshipService.GetByProvider(startRowIndex, maximumRows, sortExpression, provider, stagesArray);


            var intermediary = IntermediaryService.FindById(orgId);
            return ScholarshipService.GetByOrganizations(startRowIndex, maximumRows, sortExpression, provider, intermediary,
                                                          stagesArray);
         
        }


        public int CountScholarships(int orgId, string stages)
        {
            var stagesArray = GetStagesArray(stages);

            var provider = UserContext.CurrentProvider;

            if (orgId == 0)
                return ScholarshipService.CountByProvider(  provider, stagesArray);


            var intermediary = IntermediaryService.FindById(orgId);
            return ScholarshipService.CountByOrganizations(  provider, intermediary,
                                                          stagesArray);
        }

        private ScholarshipStage[] GetStagesArray(string stages)
        {
            var stageList=new List<ScholarshipStage>();
            
            if (!string.IsNullOrEmpty(stages))
            {
                var arrstages = stages.Split(",".ToCharArray());
                foreach (string s in arrstages)
                {
                    if (!string.IsNullOrEmpty(s))
                   stageList.Add((ScholarshipStage) Enum.Parse(typeof (ScholarshipStage), s, true));
                }
            }
            return stageList.ToArray();
        }

        protected void UpdateViewBtn_Click(object sender, EventArgs e)
        {
            lstScholarships.DataBind();
        }




    }
}