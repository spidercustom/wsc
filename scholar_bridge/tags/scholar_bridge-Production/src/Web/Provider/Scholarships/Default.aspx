<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>
<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Default" Title="Provider | Scholarships" %>

<%--<%@ Register TagPrefix="sb" TagName="ScholarshipList" Src="~/Common/ScholarshipList.ascx" %>--%>
<%@ Register TagPrefix="sb" TagName="ScholarshipListViewOptions" Src="~/Common/ScholarshipListViewOptions.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgScholarship.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div id="CenterContent">
    <div id="HomeContentLeft">
    </div>
    <div id="HomeContentRight">
    </div>

    <sb:ScholarshipListViewOptions id="scholarshipsListViewOptions" runat="server" 
        OnUpdateView="UpdateViewBtn_Click" />

<asp:ObjectDataSource ID="ScholarshipDataSource" runat="server"
    SelectMethod="GetScholarships" TypeName="ScholarBridge.Web.Provider.Scholarships.Default"    
      SortParameterName="sortExpression"   OnSelecting="ScholarshipDataSource_OnSelecting"
      SelectCountMethod="CountScholarships"  EnablePaging="true"  MaximumRowsParameterName="maximumRows" 
       >
       <SelectParameters>
       <asp:Parameter Name="orgId" Type="Int32"   />
       <asp:Parameter Name="stages" Type="String"   />
       </SelectParameters>
</asp:ObjectDataSource>
<sbCommon:SortableListView ID="lstScholarships" runat="server" 
     
     OnItemDataBound="lstScholarships_ItemDataBound"  
     DataKeyNames="Id" DataSourceID="ScholarshipDataSource" SortExpressionDefault="Status"
			SortDirectionDefault="Descending"
			>
    <LayoutTemplate>
    <table class="sortableListView" cellpadding="0" cellspacing="0" width="90%">
            <thead>
            <tr>
                <th style="width:60px">
                <sbCommon:SortableListViewColumnHeader Key="Status" Text="Status"  ID="SortableListViewColumnHeader1" runat="server"  />
                 </th>
                <th ><sbCommon:SortableListViewColumnHeader Key="Name" Text="Name"  ID="SortableListViewColumnHeader2" runat="server"  /></th>
                <th><sbCommon:SortableListViewColumnHeader Key="AcademicYear" Text="Academic Year"  ID="SortableListViewColumnHeader3" runat="server"  /></th>
                <th><sbCommon:SortableListViewColumnHeader Key="ApplicationStartDate" Text="Application Start Date"  ID="SortableListViewColumnHeader4" runat="server"  /> </th>
                <th>
                <sbCommon:SortableListViewColumnHeader Key="ApplicationDueDate" Text="Application Due Date"  ID="SortableListViewColumnHeader5" runat="server"  />
                </th>
                <th><sbCommon:SortableListViewColumnHeader Key="NumberofApplicants" Text="Number of Applicants"  ID="SortableListViewColumnHeader6" runat="server"  /></th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
        <div class="pager">
        <asp:DataPager runat="server" ID="pager" PagedControlID="lstScholarships" PageSize="20" 
                   >
            <Fields>
                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                    ShowNextPageButton="false" ShowLastPageButton="false" />
                <sbCommon:CustomNumericPagerField
                    CurrentPageLabelCssClass="pagerlabel"
                    NextPreviousButtonCssClass="pagerlink"
                    PagingPageLabelCssClass="pagingPageLabel"
                    NumericButtonCssClass="pagerlink"/>
                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                    ShowNextPageButton="true" ShowLastPageButton="false" />
            </Fields>
        </asp:DataPager>
    </div>

    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><%# ((ScholarshipStage)DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName()%></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server" NavigateUrl= <%#EditActionLink  +"?id="+ DataBinder.Eval(Container.DataItem, "Id") %>   ><%# DataBinder.Eval(Container.DataItem, "Name")%></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
         <td><asp:PlaceHolder id="literalApplicants"  runat="server" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><%# ((ScholarshipStage)DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName()%></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server" NavigateUrl= <%#EditActionLink  +"?id="+ DataBinder.Eval(Container.DataItem, "Id") %>   ><%# DataBinder.Eval(Container.DataItem, "Name")%></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
        <td><asp:PlaceHolder id="literalApplicants"  runat="server" /></td>
    
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships</p>
    </EmptyDataTemplate>
</sbCommon:SortableListView>

    <%--<sb:ScholarshipList id="scholarshipsList" runat="server" 
        ViewActionLink="~/Provider/BuildScholarship/Default.aspx"
        EditActionLink="~/Provider/BuildScholarship/Default.aspx"  />    --%>        
</div>
</asp:Content>
