﻿using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Domain.Location;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class StateDAL : AbstractDAL<State>, IStateDAL, IGenericLookupDAL<State>
    {
        public State FindByAbbreviation(string abbreviation)
        {
            return UniqueResult("Abbreviation", abbreviation);
        }

        public IList<State> FindAll(IList<object> abbreviations)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Abbreviation", abbreviations.ToArray()))
                .List<State>();
        }

        public IList<State> FindAll()
        {
            return CreateCriteria()
                .SetCacheable(true)
                .SetCacheRegion("Lookup")
                .AddOrder(Order.Asc("Abbreviation"))
                .List<State>();
        }

        public State FindById(object id)
        {
            return FindByAbbreviation(id.ToString());
        }

        public List<KeyValuePair<string, string>> GetLookupItems(string userData)
        {
            var list = FindAll();
            var result = list.Select(
                o => new KeyValuePair<string, string>(o.Abbreviation, o.Name)).ToList();
            return result;
        }
    }
}