using System;
using System.Collections.Generic;
using NHibernate;
using Spring.Data.NHibernate.Generic.Support;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public abstract class AbstractDAL<T> : HibernateDaoSupport, IDAL<T>
    {
        protected ICriteria CreateCriteria()
        {
            return Session.CreateCriteria(typeof (T));
        }

        public T FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public void Delete(T entity)
        {
            if (null == entity)
            {
                throw new NullReferenceException("entity cannot be null");
            }

            Session.Delete(entity);
        }

        public T Save(T entity)
        {
            if (null == entity)
            {
                throw new NullReferenceException("entity cannot be null");
            }

            Session.SaveOrUpdate(entity);
            return entity;
        }

        public T Insert(T entity)
        {
            if (null == entity)
            {
                throw new NullReferenceException("entity cannot be null");
            }

            Session.Save(entity);
            return entity;
        }

        public T Update(T entity)
        {
            if (null == entity)
            {
                throw new NullReferenceException("entity cannot be null");
            }

            Session.Update(entity);
            return entity;
        }

        public void Evict(T entity)
        {
            Session.Evict(entity);
        }

        public IList<T> FindAll(string sortProperty)
        {
            ICriteria criteria = CreateCriteria();
            AddSortProperty(sortProperty, criteria);
            return criteria.List<T>();
        }

        protected static void AddSortProperty(string sortProperty, ICriteria criteria)
        {
            if (!string.IsNullOrEmpty(sortProperty))
            {
                criteria.AddOrder(BuildOrder(sortProperty));
            }
        }

        private static Order BuildOrder(string sortProperty)
        {
            return Order.Asc(sortProperty);
        }

        protected T UniqueResult(string property, object value)
        {
            return CreateCriteria().Add(Restrictions.Eq(property, value)).UniqueResult<T>();
        }
    }
}