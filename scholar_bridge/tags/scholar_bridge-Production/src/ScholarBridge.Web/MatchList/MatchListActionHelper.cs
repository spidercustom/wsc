﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using Spring.Context.Support;

namespace ScholarBridge.Web.MatchList
{
    public class MatchListActionHelper
    {
        private const string ACTION_COMMAND_NAME = "match-action";
        private const string ACTION_DEFAULT_CONTROL_ID = "matchBtn";

        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }

        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        public void SetupListView(ListView list)
        {
            list.ItemDataBound += List_ItemDataBound;
            list.ItemCommand += List_ItemCommand;
        }

        private void List_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match)((ListViewDataItem)e.Item).DataItem);
                var btn = (Button)e.Item.FindControl(ACTION_DEFAULT_CONTROL_ID);
                btn.CommandName = ACTION_COMMAND_NAME;
                btn.CommandArgument = match.Scholarship.Id.ToString();
                
                var matchAction = GetMatchAction(match);
                btn.Visible = !(matchAction is BlankAction);
                btn.Text = matchAction.Text;
            }
        }

        private void List_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (!ACTION_COMMAND_NAME.Equals(e.CommandName))
                return;
            int scholarshipId = -1;
            if (e.CommandArgument != null && e.CommandArgument.ToString().Length > 0)
                scholarshipId = Convert.ToInt32(e.CommandArgument);
            if (scholarshipId < 0)
                return;
            var match = MatchService.GetMatch(UserContext.CurrentSeeker, scholarshipId);
            if (null != match)
                GetMatchAction(match).Execute(match);
        }

        private static MatchAction GetMatchAction(Match match)
        {
            if (match.Scholarship.IsUseOnlineApplication)
            {
                if (match.MatchApplicationStatus == MatchApplicationStatus.Unknown
                    || match.MatchApplicationStatus == MatchApplicationStatus.NotApplied
                      || match.MatchApplicationStatus == MatchApplicationStatus.Applying
                      || match.MatchApplicationStatus == MatchApplicationStatus.Applied
                     || match.MatchApplicationStatus == MatchApplicationStatus.BeingConsidered
                    )
                    return new BuildApplicationAction();
            }
            switch (match.MatchApplicationStatus)
            {
                case MatchApplicationStatus.Unknown:
                    return BlankAction.Instance;
                case MatchApplicationStatus.NotApplied:
                    return new BuildApplicationAction();
                case MatchApplicationStatus.Applying:
                    return new EditApplicationAction();
                case MatchApplicationStatus.Applied:
                    return new ViewApplicationAction();
                case MatchApplicationStatus.BeingConsidered:
                    return new ViewApplicationAction();
                case MatchApplicationStatus.Closed:
                    return BlankAction.Instance;
                case MatchApplicationStatus.Offered:
                    return new ViewContactInfoAction();
                case MatchApplicationStatus.Awarded:
                    return new ViewApplicationAction();
                default:
                    throw new NotSupportedException();
            }
        }
    }
}