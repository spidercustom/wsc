using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    abstract class MatchAction
    {
        public string Text { get; protected set; }
        public abstract void Execute(Match match);
		public abstract string GetActionUrl(Match match);
    }
}