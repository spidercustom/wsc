﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(248)]
    public class InsertCompanyLookupData : Migration
	{
        private const string tableName = "SBCompanyLUT";
		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
			string[] columns = GetInsertColumns();

            Database.Insert(tableName, columns, new[] { "Boeing", "Boeing", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Costco", "Costco", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Milliman", "Milliman", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Pacific Northwest Title Holding Company", "Pacific Northwest Title Holding Company", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "WaMu/Chase", "WaMu/Chase", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "West Coast Paper", "West Coast Paper", "0", adminId.ToString(), DateTime.Now.ToString() });
		}

		public override void Down()
		{
            Database.ExecuteNonQuery("DELETE FROM " + tableName);
			
		}

		private string[] GetInsertColumns()
		{
			Column[] columns = Database.GetColumns(tableName);
			return new string[]
			       	{
			       		columns[1].Name,
						columns[2].Name,
						"Deprecated",
                        "LastUpdateBy",
                        "LastUpdateDate"
					};
		}
	}
}
