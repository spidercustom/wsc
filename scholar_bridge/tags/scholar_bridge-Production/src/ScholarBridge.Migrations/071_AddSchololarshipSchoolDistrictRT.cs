﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(71)]
    public class AddSchololarshipSchoolDistrictRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBScholarship"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSchoolDistrictLUT"; }
        }
    }
}
