﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(133)]
    public class AddSeekerSeekerMatchOrganizationRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSeekerMatchOrganizationLUT"; }
        }
    }
}
