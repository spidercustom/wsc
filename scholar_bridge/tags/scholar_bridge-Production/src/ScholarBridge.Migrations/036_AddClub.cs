﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(36)]
    public class AddClub : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "Club";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
