using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(278)]
    public class AddApplicationCompanyRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationCompanyRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }
        protected override string SecondTableName
        {
            get { return "SBCompanyLUT"; }
        }
    }
}