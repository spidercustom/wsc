﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(280)]
    public class AddApplicatonFAFSAFields : Migration
    {
        private const string TABLE_NAME = "SBApplication";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, "HasFAFSACompleted", DbType.Boolean);
            Database.AddColumn(TABLE_NAME, "ExpectedFamilyContribution", DbType.Decimal);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "HasFAFSACompleted");
            Database.RemoveColumn(TABLE_NAME, "ExpectedFamilyContribution");
        }
    }
}