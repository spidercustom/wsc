using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(157)]
    public class AddMatchTable : AddTableBase
    {
        private const string TABLE_NAME = "SBMatch";
        private const string PRIMARY_KEY_COLUMN = "SBMatchId";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new []
            {
                new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                new Column("SBSeekerId", DbType.Int32, ColumnProperty.NotNull),
                new Column("SBScholarshipId", DbType.Int32, ColumnProperty.NotNull),
                new Column("MatchStatus", DbType.String, 30, ColumnProperty.NotNull, "'New'"),
                new Column("IsDeleted", DbType.Boolean, ColumnProperty.NotNull, "0"),
                new Column("SeekerPreferredCriteriaCount", DbType.Int32, ColumnProperty.NotNull),
                new Column("SeekerMinimumCriteriaCount", DbType.Int32, ColumnProperty.NotNull),
                new Column("ScholarshipPreferredCriteriaCount", DbType.Int32, ColumnProperty.NotNull),
                new Column("ScholarshipMinimumCriteriaCount", DbType.Int32, ColumnProperty.NotNull),

                new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull)
            };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new []
            {
                CreateForeignKey("LastUpdateBy", "SBUser",  "SBUserId"),
                CreateForeignKey("SBSeekerId", "SBSeeker",  "SBSeekerId"),
                CreateForeignKey("SBScholarshipId", "SBScholarship",  "SBScholarshipId"),
            };
        }
    }
}