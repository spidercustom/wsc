using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class MessageDALTest : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public RoleDAL RoleDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public MessageDAL MessageDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        private Message userMessage;
        private Message roleOrgMessage;
        private User user;
        private Role role;
        private Provider organization;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "test@test.com");
            organization = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "Test Provider", user);
            role = RoleDAL.FindByName(Role.WSC_ADMIN_ROLE);
            userMessage = InsertMessage(user, null, null);
            roleOrgMessage = InsertMessage(null, role, organization);
        }

        [Test]
        public void new_message_is_not_read()
        {
            Assert.That(userMessage.IsRead, Is.False);
        }

        [Test]
        public void new_message_is_not_archived()
        {
            Assert.That(userMessage.IsArchived, Is.False);
        }

        [Test]
        public void can_find_by_id_with_other_params()
        {
            var found = MessageDAL.FindById(null, new List<Role> {role}, organization, roleOrgMessage.Id);
            Assert.That(found, Is.Not.Null);
            Assert.That(found.Subject, Is.EqualTo(userMessage.Subject));
        }

        [Test]
        public void can_find_by_id_with_user()
        {
            var found = MessageDAL.FindById(user, null, null, userMessage.Id);
            Assert.That(found, Is.Not.Null);
            Assert.That(found.Subject, Is.EqualTo(userMessage.Subject));
        }

        [Test]
        public void can_find_by_user_and_message_action()
        {
            userMessage.ActionTaken = MessageAction.Approve;
            MessageDAL.Update(userMessage);

            var found = MessageDAL.FindAll(0,100,"SenderName",MessageAction.Approve, user, null, null);
            Assert.That(found, Is.Not.Null);
            CollectionAssert.IsNotEmpty(found);

            var notfound = MessageDAL.FindAll(0, 100, "SenderName", MessageAction.Deny, user, null, null);
            Assert.That(found, Is.Not.Null);
            CollectionAssert.IsEmpty(notfound);
        }

		[Test]
		public void can_find_by_for_specific_message_Type()
		{
            var foundCount = MessageDAL.FindAllByMessageType(0, 100, "Date", MessageType.ScholarshipClosed, user, null, null).Count;
			Message msg = InsertMessage(user, null, null);

			// insert a scholarship-closed message
			msg.MessageTemplate = MessageType.ScholarshipClosed;
			MessageDAL.Update(msg);

			// insert another message
			InsertMessage(user, null, null);

            var messages = MessageDAL.FindAllByMessageType(0, 100, "Date", MessageType.ScholarshipClosed, user, null, null);
			Assert.That(messages, Is.Not.Null);
			CollectionAssert.IsNotEmpty(messages);
			Assert.That(messages.Count, Is.EqualTo(foundCount + 1));
		}

		[Test]
		public void can_find_all_for_inbox()
		{
            var foundCount = MessageDAL.FindAllForInbox(0, 100, "Date", user, null, null).Count;
			Message msg = InsertMessage(user, null, null);

			// insert a scholarship-closed message (should be skipped)
			msg.MessageTemplate = MessageType.ScholarshipApproved;
			MessageDAL.Update(msg);

			// insert a few more messages
			InsertMessage(user, null, null);
			InsertMessage(user, null, null);
			InsertMessage(user, null, null);
			InsertMessage(user, null, null);

			// set message as deny (should be skipped)
			Message denyMsg = InsertMessage(user, null, null);
			denyMsg.ActionTaken = MessageAction.Deny;
			MessageDAL.Update(denyMsg);

			// set message as archived (should be skipped)
			Message archiveMsg = InsertMessage(user, null, null);
			archiveMsg.IsArchived = true;
			MessageDAL.Update(archiveMsg);


            var messages = MessageDAL.FindAllForInbox(0, 100, "Date", user, null, null);
			Assert.That(messages, Is.Not.Null);
			CollectionAssert.IsNotEmpty(messages);
			Assert.That(messages.Count, Is.EqualTo(foundCount + 4));
		}

		[Test]
        public void can_delete_scholarship_messages()
        {
            var s =
                ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, organization,
                                                                          ScholarshipStage.NotActivated));
            var to = new MessageAddress
                         {
                             User = null,
                             Organization = organization,
                             Role = role
                         };
            var from = to;
            var message = new ScholarshipMessage()
                              {
                                  RelatedScholarship = s,
                                  Subject = "Test Message",
                                  Content = "content",
                                  From = from,
                                  To = to,
                                  LastUpdate = new ActivityStamp(user)
                              };
            MessageDAL.Insert(message);

            var m1 = MessageDAL.FindById(null, new List<Role> {role}, organization, message.Id);
            Assert.IsNotNull(m1);

            MessageDAL.DeleteRelated(s);

            var m2 = MessageDAL.FindById(null, new List<Role> { role }, organization, message.Id);
            Assert.IsNull(m2);
        }

        private Message InsertMessage(User u, Role r, Organization o)
        {
            var to = new MessageAddress
                         {
                             User = u,
                             Organization = o,
                             Role = r
                         };
            var from = to;

            var message = new Message
                              {
                                  Subject = "Test Message",
                                  Content = "content",
                                  From = from,
                                  To = to,
                                  LastUpdate = new ActivityStamp(user)
                              };

            var m = MessageDAL.Insert(message);
            Assert.That(m, Is.Not.Null);

            return m;
        }
    }
}