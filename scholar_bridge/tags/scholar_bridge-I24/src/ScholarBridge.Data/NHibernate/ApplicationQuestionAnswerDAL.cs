using System;
using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.ScholarshipParts;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class ApplicationQuestionAnswerDAL : AbstractDAL<ApplicationQuestionAnswer>, IApplicationQuestionAnswerDAL
    {
        public ApplicationQuestionAnswer FindById(int id)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<ApplicationQuestionAnswer>();
        }

        public ApplicationQuestionAnswer FindByApplicationandQuestion(Application application, ScholarshipQuestion question)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))
                .Add(Restrictions.Eq("Question", question))
                .UniqueResult<ApplicationQuestionAnswer>();
        }

        public IList<ApplicationQuestionAnswer> FindByApplication(Application application)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))
                .List<ApplicationQuestionAnswer>();
        }



        public ApplicationQuestionAnswer Save(ApplicationQuestionAnswer applicationQuestionAnswer)
        {
            return applicationQuestionAnswer.Id < 1 ?
                Insert(applicationQuestionAnswer) :
                Update(applicationQuestionAnswer);
        }
    }
}