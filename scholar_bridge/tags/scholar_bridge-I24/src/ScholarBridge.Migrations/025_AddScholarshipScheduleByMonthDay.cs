﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(25)]
    public class AddScholarshipScheduleByMonthDay : AddTableBase
    {
        public const string PRIMARY_KEY_COLUMN = "Id";
        public const string TABLE_NAME = "SB_ScholarshipScheduleByMonthDay";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column("Id", DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("StartFromMonth", DbType.Int16),
                           new Column("StartFromDayOfMonth", DbType.Int16),
                           new Column("DueOnMonth", DbType.Int16),
                           new Column("DueOnDayOfMonth", DbType.Int16),
                           new Column("AwardOnDayOfMonth", DbType.Int16),
                           new Column("AwardOnMonth", DbType.Int16)
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return null;
        }
    }
}