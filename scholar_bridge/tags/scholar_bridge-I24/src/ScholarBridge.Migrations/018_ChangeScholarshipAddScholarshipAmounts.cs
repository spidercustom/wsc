﻿using System;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(18)]
    public class ChangeScholarshipAddScholarshipAmounts : Migration
    {

        public const string MINIMUM_AMOUNT_COLUMN_NAME = "MinimumAmount";
        public const string MAXMIMUM_AMOUNT_COLUMN_NAME = "MaximumAmount";

        public override void Up()
        {
            Database.AddColumn(AddScholarship.TABLE_NAME, MINIMUM_AMOUNT_COLUMN_NAME, System.Data.DbType.Currency, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, MAXMIMUM_AMOUNT_COLUMN_NAME, System.Data.DbType.Currency, ColumnProperty.Null);
            Database.Update(AddScholarship.TABLE_NAME, new[] { MINIMUM_AMOUNT_COLUMN_NAME }, new[] { "10" });
            Database.Update(AddScholarship.TABLE_NAME, new[] { MAXMIMUM_AMOUNT_COLUMN_NAME }, new[] { "10" });
            Database.ChangeColumn(AddScholarship.TABLE_NAME, new Column(MINIMUM_AMOUNT_COLUMN_NAME, System.Data.DbType.Currency, ColumnProperty.NotNull));
            Database.ChangeColumn(AddScholarship.TABLE_NAME, new Column(MAXMIMUM_AMOUNT_COLUMN_NAME, System.Data.DbType.Currency, ColumnProperty.NotNull));
        }

        public override void Down()
        {
            Database.RemoveColumn(AddScholarship.TABLE_NAME, MINIMUM_AMOUNT_COLUMN_NAME);
            Database.RemoveColumn(AddScholarship.TABLE_NAME, MAXMIMUM_AMOUNT_COLUMN_NAME);
        }

    }
}
