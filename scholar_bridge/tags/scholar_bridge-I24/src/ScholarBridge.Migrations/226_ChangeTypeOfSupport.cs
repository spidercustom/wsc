﻿using System.Collections.Generic;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;
using Spider.Common.Extensions.System.Collections;

namespace ScholarBridge.Migrations
{
    [Migration(226)]
    public class ChangeTypeOfSupport : Migration
    {
        public static readonly string[] INSERT_COLUMNS = { "SBSupport", "Description", "SupportType", "LastUpdateBy" };
        private const string TABLE_NAME = "SBSupportLUT";
        private static readonly string[] NEW_SUPPORT_TYPE = new[] {"Support for Transportation"};
        public override void Up()
        {
            InsertValues(NEW_SUPPORT_TYPE);
            Database.Update(TABLE_NAME, new[] {"SBSupport"}, new[] {"Support for Books and  Supplies"},
                            "SBSupport = 'Support for Books'");
        }

        public override void Down()
        {
            DeleteValues(NEW_SUPPORT_TYPE);
            Database.Update(TABLE_NAME, new[] { "SBSupport" }, new[] { "Support for Books" },
                            "SBSupport = 'Support for Books and  Supplies'");
        }

        private void InsertValues(IEnumerable<string> names)
        {
            int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
            names.Each(
                columnName =>
                Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { columnName, "", "Traditional", adminId.ToString() }));
        }

        private void DeleteValues(IEnumerable<string> names)
        {
            names.Each(
                columnName =>
                {
                    Database.ExecuteNonQuery(string.Format("DELETE FROM SBScholarshipSupportRT WHERE SBSupportIndex=(SELECT SBSupportIndex FROM SBSupportLUT WHERE SBSupport='{0}')", columnName));
                    Database.Delete(TABLE_NAME, "SBSupport", columnName);
                });
        }

    }
}