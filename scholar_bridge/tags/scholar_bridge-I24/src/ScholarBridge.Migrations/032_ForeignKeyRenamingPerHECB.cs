﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    /// <summary>
    /// Part 2 of several steps to get to HECB's liking.
    /// 
    /// HECB standard says:
    /// 1) foreign keys s/b named like the refering primary keys (unless it is impossible because there are more than one ref to the same table)
    /// 2) all tables contain LastUpdateBy (int not null) & LastUpdateDate (datetime not null).
    ///		* these are misnomers because these also are the CreateBy & CreateDate fields as well so they should never be empty
    /// 3) Lookup table names should end in LUT 
    ///		* there really isn't a good definition of lookup table other than it contains static data that generally
    ///			doesn't have user CRUD UI but mainly is used to fill drop-downs.  The distinction between an entity table
    ///			and a LUT is a bit arbitrary.  The lookup values 'describe' or 'decorate' an entity.
    /// </summary>
	[Migration(32)]
    public class ForeignKeyRenamingPerHECB : Migration
    {
		// these tables are lookups (I think)
		private string[] lookupTables = new [] { 
											"SBAdditionalRequirement", 
											"SBNeedGap", 
											"SBRole", 
											"SBScholarshipStage", 
											"SBState", 
											"SBSupport" 
										};
		public override void Up()
        {
			// change the name of Look Up Tables to have LUT extension and change the ID to be Index
			foreach(var table in lookupTables)
			{
				Database.RenameTable(table, table + "LUT");
				Database.RenameColumn(table + "LUT", table + "ID", table + "Index");
			}

			// rename foreign keys & other columns
			Database.RenameColumn("SBAdditionalRequirementLUT", "LastUpdatedBy", "LastUpdateBy");
			Database.RenameColumn("SBNeedGapLUT", "LastUpdatedBy", "LastUpdateBy");
			Database.RenameColumn("SBOrganization", "LastUpdatedBy", "LastUpdateBy");
			Database.RenameColumn("SBRoleLUT", "LastUpdatedBy", "LastUpdateBy");
			Database.RenameColumn("SBScholarship", "LastUpdatedBy", "LastUpdateBy");
			Database.RenameColumn("SBScholarship", "LastUpdatedOn", "LastUpdateDate");
			Database.RenameColumn("SBSupportLUT", "LastUpdatedBy", "LastUpdateBy");
			Database.RenameColumn("SBUser", "LastUpdatedBy", "LastUpdateBy");
			Database.RenameColumn("SBOrganization", "AdminUser", "AdminUserID");
			Database.RenameColumn("SBScholarship", "Provider", "SBProviderID");
			Database.RenameColumn("SBScholarship", "Stage", "SBStageIndex");
			Database.RenameColumn("SBScholarshipAdditionalRequirementRT", "ScholarshipId", "SBScholarshipId");
			Database.RenameColumn("SBScholarshipAdditionalRequirementRT", "AdditionalRequirementId", "SBAdditionalRequirementIndex");
			Database.RenameColumn("SBScholarshipNeedGapRT", "ScholarshipId", "SBScholarshipId");
			Database.RenameColumn("SBScholarshipNeedGapRT", "NeedGapId", "SBNeedGapIndex");
			Database.RenameColumn("SBScholarshipSupportRT", "ScholarshipId", "SBScholarshipId");
			Database.RenameColumn("SBScholarshipSupportRT", "SupportId", "SBSupportIndex");
			Database.RenameColumn("SBUserOrganizationRT", "UserId", "SBUserId");
			Database.RenameColumn("SBUserOrganizationRT", "OrganizationId", "SBOrganizationId");
			Database.RenameColumn("SBUserRolesRT", "UserId", "SBUserId");
			Database.RenameColumn("SBUserRolesRT", "RoleId", "SBRoleIndex");
			Database.RenameColumn("SBAdditionalRequirementLUT", "Name", "SBAdditionalRequirement");
			Database.RenameColumn("SBSupportLUT", "Name", "SBSupport");
			Database.RenameColumn("SBRoleLUT", "Name", "SBRole");
			Database.RenameColumn("SBNeedGapLUT", "Name", "SBNeedGap");
			Database.RenameColumn("SBScholarshipStageLUT", "Name", "SBScholarshipStage");
		}

        public override void Down()
        {
			// rename foreign keys and columns back
			Database.RenameColumn("SBAdditionalRequirementLUT", "LastUpdateBy", "LastUpdatedBy");
			Database.RenameColumn("SBNeedGapLUT", "LastUpdateBy", "LastUpdatedBy");
			Database.RenameColumn("SBOrganization", "LastUpdateBy", "LastUpdatedBy");
			Database.RenameColumn("SBRoleLUT", "LastUpdateBy", "LastUpdatedBy");
			Database.RenameColumn("SBScholarship", "LastUpdateBy", "LastUpdatedBy");
			Database.RenameColumn("SBScholarship", "LastUpdateDate", "LastUpdatedOn");
			Database.RenameColumn("SBSupportLUT", "LastUpdateBy", "LastUpdatedBy");
			Database.RenameColumn("SBUser", "LastUpdateBy", "LastUpdatedBy");
			Database.RenameColumn("SBOrganization", "AdminUserID", "AdminUser");
			Database.RenameColumn("SBScholarship", "SBProviderID", "Provider");
			Database.RenameColumn("SBScholarship", "SBStageIndex", "Stage");
			Database.RenameColumn("SBScholarshipAdditionalRequirementRT", "SBScholarshipId", "ScholarshipId");
			Database.RenameColumn("SBScholarshipAdditionalRequirementRT", "SBAdditionalRequirementIndex", "AdditionalRequirementId");
			Database.RenameColumn("SBScholarshipNeedGapRT", "SBScholarshipId", "ScholarshipId");
			Database.RenameColumn("SBScholarshipNeedGapRT", "SBNeedGapIndex", "NeedGapId");
			Database.RenameColumn("SBScholarshipSupportRT", "SBScholarshipId", "ScholarshipId");
			Database.RenameColumn("SBScholarshipSupportRT", "SBSupportIndex", "SupportId");
			Database.RenameColumn("SBUserOrganizationRT", "SBUserId", "UserId");
			Database.RenameColumn("SBUserOrganizationRT", "SBOrganizationId", "OrganizationId");
			Database.RenameColumn("SBUserRolesRT", "SBUserId", "UserId");
			Database.RenameColumn("SBUserRolesRT", "SBRoleIndex", "RoleId");
			Database.RenameColumn("SBAdditionalRequirementLUT", "SBAdditionalRequirement", "Name");
			Database.RenameColumn("SBSupportLUT", "SBSupport", "Name");
			Database.RenameColumn("SBRoleLUT", "SBRole", "Name");
			Database.RenameColumn("SBNeedGapLUT", "SBNeedGap", "Name");
			Database.RenameColumn("SBScholarshipStageLUT", "SBScholarshipStage", "Name");

			// remove LUT extension from Look Up Tables and change the Index back to ID
			foreach (var table in lookupTables)
			{
				Database.RenameTable(table + "LUT", table);
				Database.RenameColumn(table, table + "Index", table + "ID");
			}

		}
    }
}
