using System;
using System.Collections.Generic;

namespace ScholarBridge.Common
{
    public class UrlBuilder : UriBuilder
    {
        private Dictionary<string, string> queryString = new Dictionary<string, string>();

        public UrlBuilder() : base()
        {
        }

        public UrlBuilder(string uri) : base(uri)
        {
            PopulateQueryString();
        }

        public UrlBuilder(Uri uri) : base(uri)
        {
            PopulateQueryString();
        }

        public UrlBuilder(string schemeName, string hostName) : base(schemeName, hostName)
        {
        }

        public UrlBuilder(string scheme, string host, int portNumber) : base(scheme, host, portNumber)
        {
        }

        public UrlBuilder(string scheme, string host, int port, string pathValue) : base(scheme, host, port, pathValue)
        {
        }

        public UrlBuilder(string scheme, string host, int port, string path, string extraValue)
            : base(scheme, host, port, path, extraValue)
        {
        }

        public Dictionary<string, string> QueryString
        {
            get
            {
                return queryString;
            }
        }

        public string PageName
        {
            get
            {
                string path = Path;
                return path.Substring(path.LastIndexOf("/") + 1);
            }
            set
            {
                string path = Path;
                path = path.Substring(0, path.LastIndexOf("/"));
                Path = string.Concat(path, "/", value);
            }
        }

        public new string ToString()
        {
            GetQueryString();
            return Uri.AbsoluteUri;
        }

        private void PopulateQueryString()
        {
            if (string.IsNullOrEmpty(Query))
            {
                return;
            }

            queryString.Clear();
            var query = Query.Substring(1); //remove the ?

            string[] pairs = query.Split(new[] {'&'});
            foreach (string s in pairs)
            {
                string[] pair = s.Split(new[] {'='});
                queryString[pair[0]] = (pair.Length > 1) ? pair[1] : string.Empty;
            }
        }

        private void GetQueryString()
        {
            if (0 == queryString.Count)
            {
                Query = string.Empty;
                return;
            }

            var pairs = new List<string>();
            foreach (KeyValuePair<string, string> pair in queryString)
            {
                pairs.Add(String.Concat(pair.Key, "=", pair.Value));
            }

            Query = String.Join("&", pairs.ToArray());
        }
    }
}