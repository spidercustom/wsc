﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Scholarships.Show" EnableViewState="true" %>
<%@ Register src="~/Common/ShowScholarship.ascx" tagname="ShowScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
    <div class="right-align-print-panel-controls non-printable">
        <div class="center-align-to-print">
            <sbCommon:AnchorButton ID="sendToFriendBtn" runat="server" Text="Send to a friend" />
        </div>
        <sb:PrintView id="PrintViewControl" runat="server" /> 
    </div>
</asp:Content>

<asp:Content ID="bodyContent" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HideInPrintView="true"/>

<div id="linkarea" class="exclude-in-print"  runat="server" visible="<% #!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this) %>">
<sbCommon:ConfirmButton ID="deleteConfirmBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
        Text="Delete Scholarship" Width="150px"  runat="server" OnClickConfirm="deleteConfirmBtn_Click" />
        
        <br />
        <br />
        <sbCommon:AnchorButton ID="linkCopy" runat="server" Text="Copy this Scholarship to create new" />
</div>

<div id="deleteConfirmDiv" title="Confirm delete" style="display:none">
    Deleting the scholarship will remove it from the system. 
    Only scholarships that have yet to be activated can be deleted. 
    <br /> <br />Are you sure want to delete?
  </div>   
  <div id="requestActivationConfirmDiv" title="Request Activation" style="display:none">
    Requesting activation will send a message to an authorized agent to activate the scholarship. 
    You can expect the scholarship to be activated in approximately X number of days. 
    Once you request activation, you will not be able to edit the scholarship criteria.
    <br /> <br />
    Are you sure want to continue?
  </div>   

<sb:ShowScholarship id="showScholarship" runat="server" LinkTo="~/Provider/BuildScholarship/Default.aspx"
    ApplicationLinkTo="~/Intermediary/Scholarships/ShowApplication.aspx"
    LinkToEditProgramGuideLines ="~/Intermediary/Scholarships/EditProgramGuidelines.aspx"  
    LinkToEditSchedule  ="~/Intermediary/Scholarships/EditSchedule.aspx"     />

        
  <asp:ScriptManager ID="ConfirmButtoncriptmanager" runat="server"></asp:ScriptManager>

</asp:Content>