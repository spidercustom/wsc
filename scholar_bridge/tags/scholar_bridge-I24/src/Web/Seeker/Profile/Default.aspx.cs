﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Default : SBBasePage, IWizardStepsContainer<Domain.Seeker>
    {

        #region properties

        public ISeekerService SeekerService { get; set; }
		public IUserContext UserContext { get; set; }
        public IApplicationService ApplicationService { get; set; }

		#endregion

		#region Page Lifecycle Events

		protected void Page_Load(object sender, EventArgs e)
        {
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.SeekerProfile); 
			Steps.ForEach(step => step.Container = this);
			buildSeekerProfileWizardTabIntegrator.TabChanging += (buildSeekerProfileWizardTabIntegrator_TabChanging);
			if (!IsPostBack)
			{
			    CheckForDataChanges = true;

				BypassPromptIds.AddRange(
					new[]	{
								PreviousButton.ID,                            
								NextButton.ID,                            
								SaveButton.ID,
                                buildSeekerProfileWizardTabIntegrator.ID,
                                "btnActivateProfile"
                                  
							});

				SkipDataCheckIds.Add(ScholarshipSearchBox.SEARCH_BOX_ID); // from the ScholarshipSearchBox control in the GlobalMenu - this gets populated by javascript

				ActiveStepIndex = WizardStepName.Basics.GetNumericValue();
			}
		}

	    protected void Page_PreRender(object sender, EventArgs e)
		{
			PreviousButton.Enabled = ActiveStepIndex != 0;
			NextButton.Enabled = ActiveStepIndex != Steps.Length - 1;
            RemoveActivateButtonIfAlreadyActivated();
        }

        private void RemoveActivateButtonIfAlreadyActivated()
        {
            var seeker = GetDomainObject();
            if (seeker == null)
                return;

            if (seeker.Stage == SeekerStages.Published)
            {
                var msg = @"You have updated your profile information. Once saved, the changes will replace the previous information in your profile. Do you want to update your profile with these changes?";

                if (ApplicationService.CountAllSubmittedBySeeker(seekerInContext) > 0)
                    msg = @"You have updated your profile information. Once saved, the changes will replace the previous information in your profile but your changes will not be updated in the Scholarship applications that you have already submitted.  Do you want to update your profile with these changes?";

                ConfirmSaveIfActivatedLabel.Text = msg;
            }
        }

        #endregion

		#region Control event handlers

		protected void BuildSeekerProfileWizard_ActiveStepChanged(object sender, EventArgs e)
    	{
			WizardStepContainerCommon.NotifyStepActivated(this);
		}

		protected void PreviousButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
    	{
            if (Save())
    		    GoPrior();
		}

		protected void NextButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
    	{
            if (Save())
    		    GoNext();
    	}

		protected void SaveButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (Save())
                Response.Redirect("../");
        }

		private void buildSeekerProfileWizardTabIntegrator_TabChanging(object sender, EventArgs e)
		{
			Save();
		}

		#endregion

		#region Private Methods

		private bool ValidateStep()
		{
			return WizardStepContainerCommon.ValidateStep(this);
		}

        private bool Save()
		{
            var seekerProfile = GetDomainObject();
            if (!Page.IsValid || !ValidateStep())
                return false;

            WizardStepContainerCommon.PopulateObjectsFromActiveStep(this);
            if (seekerProfile.IsPublished() && !ValidateActivationAndShowErrors())
                return false;
            
            if (Dirty)
				seekerProfile.ProfileLastUpdateDate = DateTime.Now;

			SeekerService.Update(seekerProfile);
	        Dirty = false;
	        return true;
		}

	    private bool ValidateActivationAndShowErrors()
	    {
	        return seekerProfileActivationValidationErrors.ValidateActivationAndPopulateErrors(GetDomainObject());
	    }

	    #endregion

		#region IWizardStepsContainer<Seeker> Members

		public IWizardStepControl<Domain.Seeker>[] Steps
		{
			get
			{
				IWizardStepControl<Domain.Seeker>[] stepControls = BuildSeekerProfileWizard.Views.FindWizardStepControls<Domain.Seeker>();
				return stepControls;
			}
		}

		Domain.Seeker seekerInContext;
        public Domain.Seeker GetDomainObject()
		{
			if (seekerInContext == null)
			{
			    seekerInContext = UserContext.CurrentSeeker;
			}
			return seekerInContext;
		}

		public void GoPrior()
		{
			WizardStepContainerCommon.GoPrior(this);
		}

		public void GoNext()
		{
			WizardStepContainerCommon.GoNext(this);
		}

		public void Goto(int index)
		{
			if (Save())
				WizardStepContainerCommon.Goto(this, index);
		}

		public int ActiveStepIndex
		{
			get { return BuildSeekerProfileWizard.ActiveViewIndex; }
			set { BuildSeekerProfileWizard.ActiveViewIndex = value; }
		}

		#endregion

	    public Domain.Seeker GetUpdatedSeeker()
	    {
	        Save();
            return GetDomainObject();
        }
    }
}