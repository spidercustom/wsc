﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;
using ScholarBridge.Domain.Extensions;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class SeekerProfile : WizardStepUserControlBase<Scholarship>
    {
        private const string GPA_VALUE_FORMAT = "0.000";
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IStateDAL StateService { get; set; }
        public const string WashingtonState = "WA";
        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context"); 

            if (!IsPostBack)
            {
                EthnicityControl.DataBind();
                ReligionControl.DataBind();
                ClassRankControl.DataBind();
                BindAttibuteSelectionControls();
                PopulateStates();
                
                PopulateScreen();
            } else
            {
                FirstGenerationControl.Text = AttributeSelectionFirstGenerationControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
                HonorsControl.Text = AttributeSelectionHonorsControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
        
            }
        }

        public override void Activated()
        {
            base.Activated();
            SetSectionsEnabledState();
            PopulateScreen();
        }
        public void BindAttibuteSelectionControls()
        {
             
            AttributeSelectionFirstGenerationControl.AttributeKey = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.FirstGeneration).First().Key;
            AttributeSelectionFirstGenerationControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.FirstGeneration).First().Value;
            AttributeSelectionFirstGenerationControl.Bind();

            AttributeSelectionEthinicityControl.AttributeKey = EnumExtensions.GetKeyValue(
                   SeekerProfileAttribute.Ethnicity).First().Key;
            AttributeSelectionEthinicityControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Ethnicity).First().Value;
            AttributeSelectionEthinicityControl.Bind();


            AttributeSelectionReligionControl.AttributeKey = EnumExtensions.GetKeyValue(
                   SeekerProfileAttribute.Religion).First().Key;
            AttributeSelectionReligionControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Religion).First().Value;
            AttributeSelectionReligionControl.Bind();


            AttributeSelectionGendersControl.AttributeKey = EnumExtensions.GetKeyValue(
                  SeekerProfileAttribute.Gender).First().Key;
            AttributeSelectionGendersControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Gender).First().Value;
            AttributeSelectionGendersControl.Bind();

            AttributeSelectionGPAControl.AttributeKey = EnumExtensions.GetKeyValue(
                  SeekerProfileAttribute.GPA).First().Key;
            AttributeSelectionGPAControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.GPA ).First().Value;
            AttributeSelectionGPAControl.Bind();

            AttributeSelectionSATWritingControl.AttributeKey = EnumExtensions.GetKeyValue(
                 SeekerProfileAttribute.SATWriting).First().Key;
            AttributeSelectionSATWritingControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SATWriting ).First().Value;
            AttributeSelectionSATWritingControl.Bind();

            AttributeSelectionSATCriticalReadingControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.SATCriticalReading).First().Key;
            AttributeSelectionSATCriticalReadingControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SATCriticalReading).First().Value;
            AttributeSelectionSATCriticalReadingControl.Bind();

            AttributeSelectionSATMathematicsControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.SATMathematics).First().Key;
            AttributeSelectionSATMathematicsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SATMathematics).First().Value;
            AttributeSelectionSATMathematicsControl.Bind();

            AttributeSelectionACTEnglishControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.ACTEnglish).First().Key;
            AttributeSelectionACTEnglishControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTEnglish).First().Value;
            AttributeSelectionACTEnglishControl.Bind();

            AttributeSelectionACTMathematicsControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ACTMathematics).First().Key;
            AttributeSelectionACTMathematicsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTMathematics).First().Value;
            AttributeSelectionACTMathematicsControl.Bind();

            AttributeSelectionACTReadingControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ACTReading).First().Key;
            AttributeSelectionACTReadingControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTReading).First().Value;
            AttributeSelectionACTReadingControl.Bind();

            AttributeSelectionACTScienceControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ACTScience).First().Key;
            AttributeSelectionACTScienceControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ACTScience).First().Value;
            AttributeSelectionACTScienceControl.Bind();

            AttributeSelectionClassRankControl.AttributeKey = EnumExtensions.GetKeyValue(
              SeekerProfileAttribute.ClassRank).First().Key;
            AttributeSelectionClassRankControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ClassRank).First().Value;
            AttributeSelectionClassRankControl.Bind();

            AttributeSelectionHonorsControl.AttributeKey = EnumExtensions.GetKeyValue(
              SeekerProfileAttribute.Honor ).First().Key;
            AttributeSelectionHonorsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Honor ).First().Value;
            AttributeSelectionHonorsControl.Bind();

            AttributeSelectionAPCreditControl.AttributeKey = EnumExtensions.GetKeyValue(
             SeekerProfileAttribute.APCreditsEarned).First().Key;
            AttributeSelectionAPCreditControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.APCreditsEarned).First().Value;
            AttributeSelectionAPCreditControl.Bind();

            AttributeSelectionIBCreditControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.IBCreditsEarned).First().Key;
            AttributeSelectionIBCreditControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.IBCreditsEarned ).First().Value;
            AttributeSelectionIBCreditControl.Bind();

            AttributeSelectionStateControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.State).First().Key;
            AttributeSelectionStateControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.State ).First().Value;
            AttributeSelectionStateControl.Bind();

            AttributeSelectionHighSchoolControl.AttributeKey = EnumExtensions.GetKeyValue(
           SeekerProfileAttribute.HighSchool).First().Key;
            AttributeSelectionHighSchoolControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.HighSchool).First().Value;
            AttributeSelectionHighSchoolControl.Bind();

            AttributeSelectionSchoolDistrictControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.SchoolDistrict).First().Key;
            AttributeSelectionSchoolDistrictControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SchoolDistrict).First().Value;
            AttributeSelectionSchoolDistrictControl.Bind();

            AttributeSelectionCityControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.City).First().Key;
            AttributeSelectionCityControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.City).First().Value;
            AttributeSelectionCityControl.Bind();

            AttributeSelectionCountyControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.County).First().Key;
            AttributeSelectionCountyControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.County).First().Value;
            AttributeSelectionCountyControl.Bind();

            AttributeSelectionAcademicAreasControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.AcademicArea).First().Key;
            AttributeSelectionAcademicAreasControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.AcademicArea).First().Value;
            AttributeSelectionAcademicAreasControl.Bind();

            AttributeSelectionCareersControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.Career).First().Key;
            AttributeSelectionCareersControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Career).First().Value;
            AttributeSelectionCareersControl.Bind();

            AttributeSelectionOrganizationsControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.Organization).First().Key;
            AttributeSelectionOrganizationsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Organization).First().Value;
            AttributeSelectionOrganizationsControl.Bind();

            AttributeSelectionCompanyControl.AttributeKey = EnumExtensions.GetKeyValue(
              SeekerProfileAttribute.Company).First().Key;
            AttributeSelectionCompanyControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Company).First().Value;
            AttributeSelectionCompanyControl.Bind();

            AttributeSelectionHobbiesControl.AttributeKey = EnumExtensions.GetKeyValue(
             SeekerProfileAttribute.SeekerHobby ).First().Key;
            AttributeSelectionHobbiesControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SeekerHobby).First().Value;
            AttributeSelectionHobbiesControl.Bind();

            AttributeSelectionSportsControl.AttributeKey = EnumExtensions.GetKeyValue(
            SeekerProfileAttribute.Sport ).First().Key;
            AttributeSelectionSportsControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Sport).First().Value;
            AttributeSelectionSportsControl.Bind();

            AttributeSelectionClubsControl.AttributeKey = EnumExtensions.GetKeyValue(
           SeekerProfileAttribute.Club).First().Key;
            AttributeSelectionClubsControl .AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.Club).First().Value;
            AttributeSelectionClubsControl.Bind();

            AttributeSelectionWorkTypeControl.AttributeKey = EnumExtensions.GetKeyValue(
                SeekerProfileAttribute.WorkType ).First().Key;
            AttributeSelectionWorkTypeControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.WorkType).First().Value;
            AttributeSelectionWorkTypeControl.Bind();

            AttributeSelectionServiceTypeControl.AttributeKey = EnumExtensions.GetKeyValue(
               SeekerProfileAttribute.ServiceType).First().Key;
            AttributeSelectionServiceTypeControl.AttributeValue = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.ServiceType ).First().Value;
            AttributeSelectionServiceTypeControl.Bind();
        }

        public void PopulateSelectionAttributesScreen(SingleAttributeSelectionControl attributeSelectionControl)
        {
             
                   var attribute = (SeekerProfileAttribute)attributeSelectionControl.UsageButtonGroup.AttributeEnumInt;

                   var attributeUsage = ScholarshipInContext.SeekerProfileCriteria.Attributes.Find(attribute);

                   attributeSelectionControl.UsageButtonGroup.Value =
                       null == attributeUsage ?
                       ScholarshipAttributeUsageType.NotUsed :
                       attributeUsage.UsageType;
               
        }

        #region Visibilty of the controls
        
        public void SetControlsEnabledState(PlaceHolder container, bool state)
        {
            ControlHelper.SetControlStateRecursive(container, state);
        }

        private void SetSectionsEnabledState()
        {
            SeekerInterestsControlContainer.Visible = true;
            SeekerDemographicsPersonalContainerControl.Visible = true;
            SeekerDemographicsGeographicContainerControl.Visible = true;
            SeekerInterestsControlContainer.Visible = true;
            SeekerActivitiesControlContainer.Visible = true;
            SeekerPerformanceControlContainer.Visible = true;

            SeekerProfileCriteria criteria = ScholarshipInContext.SeekerProfileCriteria;
            FirstGenerationControl.Text = AttributeSelectionFirstGenerationControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
            HonorsControl.Text = AttributeSelectionHonorsControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";

            SetControlsEnabledState(EthnicityContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Ethnicity));
            SetControlsEnabledState(ReligionContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Religion));
            SetControlsEnabledState(GendersContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Gender));
            
            SetControlsEnabledState(GPAContainerControl , criteria.Attributes.HasAttribute(SeekerProfileAttribute.GPA));
            SetControlsEnabledState(SATWritingContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.SATWriting));
            SetControlsEnabledState(SATCriticalReadingContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.SATCriticalReading));
            SetControlsEnabledState(SATMathematicsContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.SATMathematics));
            SetControlsEnabledState(ACTEnglishContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.ACTEnglish ));
            SetControlsEnabledState(ACTMathematicsContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.ACTMathematics));
            SetControlsEnabledState(ACTReadingContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.ACTReading ));
            SetControlsEnabledState(ACTScienceContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.ACTScience));

            SetControlsEnabledState(ClassRankContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.ClassRank));
            SetControlsEnabledState(HonorsContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Honor ));
            SetControlsEnabledState(APCreditContainerControl , criteria.Attributes.HasAttribute(SeekerProfileAttribute.APCreditsEarned ));
            SetControlsEnabledState(IBCreditContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.IBCreditsEarned ));
            SetControlsEnabledState(StateContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.State ));

            SetControlsEnabledState(HighSchoolContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.HighSchool));
            SetControlsEnabledState(SchoolDistrictContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.SchoolDistrict));
            SetControlsEnabledState(CityContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.City ));
            SetControlsEnabledState(CountyContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.County ));
           
            SetControlsEnabledState(AcademicAreasContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.AcademicArea));
            SetControlsEnabledState(CareersContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Career ));
            SetControlsEnabledState(OrganizationsContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Organization));
            SetControlsEnabledState(CompanyContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Company));

            SetControlsEnabledState(HobbiesContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.SeekerHobby ));
            SetControlsEnabledState(SportsContainerControl , criteria.Attributes.HasAttribute(SeekerProfileAttribute.Sport ));
            SetControlsEnabledState(ClubsContainerControl, criteria.Attributes.HasAttribute(SeekerProfileAttribute.Club));
            WorkTypeControl.Text = AttributeSelectionWorkTypeControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
            ServiceTypeControl.Text = AttributeSelectionServiceTypeControl.UsageButtonGroup.Value == ScholarshipAttributeUsageType.NotUsed ? "" : "Yes";
   

        }

        
        #endregion
        private void PopulateStates()
        {
            StateControl.DataSource = StateService.FindAll();
            StateControl.DataTextField = "Name";
            StateControl.DataValueField = "Abbreviation";
            StateControl.DataBind();
            StateControl.SelectedIndex = 0;
        }

        private void PopulateScreen()
        {
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            PopulateSelectionAttributesScreen(AttributeSelectionFirstGenerationControl);
            PopulateSelectionAttributesScreen(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionReligionControl);
            PopulateSelectionAttributesScreen(AttributeSelectionGendersControl);
            PopulateSelectionAttributesScreen(AttributeSelectionEthinicityControl);

            PopulateSelectionAttributesScreen(AttributeSelectionGPAControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSATWritingControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSATCriticalReadingControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSATMathematicsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTEnglishControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTMathematicsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTReadingControl);
            PopulateSelectionAttributesScreen(AttributeSelectionACTScienceControl);
            PopulateSelectionAttributesScreen(AttributeSelectionClassRankControl);
            PopulateSelectionAttributesScreen(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionAPCreditControl);
            PopulateSelectionAttributesScreen(AttributeSelectionIBCreditControl);
            PopulateSelectionAttributesScreen(AttributeSelectionStateControl);

            PopulateSelectionAttributesScreen(AttributeSelectionHighSchoolControl);
            PopulateSelectionAttributesScreen(AttributeSelectionSchoolDistrictControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCityControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCountyControl);

            PopulateSelectionAttributesScreen(AttributeSelectionAcademicAreasControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCareersControl);
            PopulateSelectionAttributesScreen(AttributeSelectionOrganizationsControl);
            PopulateSelectionAttributesScreen(AttributeSelectionCompanyControl);

            HighSchoolControlDialogButton.Keys = seekerProfileCriteria.HighSchools.CommaSeparatedIds();
            SchoolDistrictControlDialogButton.Keys = seekerProfileCriteria.SchoolDistricts.CommaSeparatedIds();
            CityControlDialogButton.Keys = seekerProfileCriteria.Cities.CommaSeparatedIds();
            CountyControlDialogButton.Keys = seekerProfileCriteria.Counties.CommaSeparatedIds();
            FirstGenerationControl.Text = seekerProfileCriteria.FirstGeneration ? "Yes" : "";
            HonorsControl.Text = seekerProfileCriteria.Honors ? "Yes" : "";
            EthnicityControl.SelectedValues = seekerProfileCriteria.Ethnicities.Cast<ILookup>().ToList();

            ReligionControl.SelectedValues = seekerProfileCriteria.Religions.Cast<ILookup>().ToList();
            GendersControl.SelectedValues = (int)seekerProfileCriteria.Genders;
            if (seekerProfileCriteria.State !=null)
            StateControl.SelectedValue = seekerProfileCriteria.State.Abbreviation;
            
            AcademicAreasControlDialogButton.Keys = seekerProfileCriteria.AcademicAreas.CommaSeparatedIds();
            CareersControlDialogButton.Keys = seekerProfileCriteria.Careers.CommaSeparatedIds();
            OrganizationsControlDialogButton.Keys = seekerProfileCriteria.Organizations.CommaSeparatedIds();
            CompanyControlDialogButton.Keys = seekerProfileCriteria.Companies.CommaSeparatedIds();
            SeekerHobbiesControlDialogButton.Keys = seekerProfileCriteria.Hobbies.CommaSeparatedIds();
            SportsControlDialogButton.Keys = seekerProfileCriteria.Sports.CommaSeparatedIds();
            ClubsControlDialogButton.Keys = seekerProfileCriteria.Clubs.CommaSeparatedIds();
            ClassRankControl.SelectedValues = seekerProfileCriteria.ClassRanks.Cast<ILookup>().ToList();
            PopulateGPAControls(seekerProfileCriteria);

            if (null != seekerProfileCriteria.SATScore)
            {
                SATWritingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Writing);
                SATCriticalReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.CriticalReading);
                SATMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Mathematics);
            }

            if (null != seekerProfileCriteria.ACTScore)
            {
                ACTEnglishControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.English);
                ACTMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Mathematics);
                ACTReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Reading);
                ACTScienceControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Science);
            }
            if (seekerProfileCriteria.APCreditsEarned.HasValue)
                APCreditControl.Amount = seekerProfileCriteria.APCreditsEarned.Value;
            if (seekerProfileCriteria.IBCreditsEarned.HasValue)
                IBCreditControl.Amount = seekerProfileCriteria.IBCreditsEarned.Value;
        }

        private void PopulateGPAControls(SeekerProfileCriteria seekerProfileCriteria)
        {
            /* see comment below marked as WSC-352 */
            if (null != seekerProfileCriteria.GPA)
            {
                GPAGreaterThanControl.Amount = (decimal) seekerProfileCriteria.GPA.Minimum;
                if (null == seekerProfileCriteria.GPA.Maximum)
                    GPALessThanControl.Text = string.Empty;
                else
                    GPALessThanControl.Text = seekerProfileCriteria.GPA.Maximum.Value.ToString(GPA_VALUE_FORMAT);
            }
            else
            {
                GPAGreaterThanControl.Amount = 0m;
                GPALessThanControl.Text = string.Empty;
            }
        }

        public override void PopulateObjects()
        {
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            PopulateSelectionAttributesObject(AttributeSelectionFirstGenerationControl);
            PopulateSelectionAttributesObject(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesObject(AttributeSelectionEthinicityControl);
            PopulateSelectionAttributesObject(AttributeSelectionReligionControl);
            PopulateSelectionAttributesObject(AttributeSelectionGendersControl);

            PopulateSelectionAttributesObject(AttributeSelectionGPAControl);
            PopulateSelectionAttributesObject(AttributeSelectionSATWritingControl);
            PopulateSelectionAttributesObject(AttributeSelectionSATCriticalReadingControl);
            PopulateSelectionAttributesObject(AttributeSelectionSATMathematicsControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTEnglishControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTMathematicsControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTReadingControl);
            PopulateSelectionAttributesObject(AttributeSelectionACTScienceControl);
            PopulateSelectionAttributesObject(AttributeSelectionClassRankControl);
            PopulateSelectionAttributesObject(AttributeSelectionHonorsControl);
            PopulateSelectionAttributesObject(AttributeSelectionAPCreditControl);
            PopulateSelectionAttributesObject(AttributeSelectionIBCreditControl);
            PopulateSelectionAttributesObject(AttributeSelectionStateControl);

            PopulateSelectionAttributesObject(AttributeSelectionAcademicAreasControl);
            PopulateSelectionAttributesObject(AttributeSelectionCareersControl);
            PopulateSelectionAttributesObject(AttributeSelectionOrganizationsControl);
            PopulateSelectionAttributesObject(AttributeSelectionCompanyControl);

            if (StateControl.SelectedValue != WashingtonState)
            {
                AttributeSelectionHighSchoolControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
                AttributeSelectionSchoolDistrictControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
                AttributeSelectionCityControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
                AttributeSelectionCountyControl.UsageButtonGroup.Value = ScholarshipAttributeUsageType.NotUsed;
            }

            PopulateSelectionAttributesObject(AttributeSelectionHighSchoolControl);
            PopulateSelectionAttributesObject(AttributeSelectionSchoolDistrictControl);
            PopulateSelectionAttributesObject(AttributeSelectionCityControl);
            PopulateSelectionAttributesObject(AttributeSelectionCountyControl);
           


            
            seekerProfileCriteria.FirstGeneration = AttributeSelectionFirstGenerationControl.UsageButtonGroup.Value != ScholarshipAttributeUsageType.NotUsed;
            seekerProfileCriteria.Honors = AttributeSelectionHonorsControl.UsageButtonGroup.Value != ScholarshipAttributeUsageType.NotUsed;
            PopulateList(EthnicityContainerControl, EthnicityControl, seekerProfileCriteria.Ethnicities);
            PopulateList(ReligionContainerControl, ReligionControl, seekerProfileCriteria.Religions);
            seekerProfileCriteria.Genders = GendersContainerControl.Visible
                                              ? (Genders) GendersControl.SelectedValues
                                              : 0;
            var state = StateService.FindByAbbreviation(StateControl.SelectedValue);
            seekerProfileCriteria.State  = state;
            


            if (StateControl.SelectedValue == WashingtonState)
            {
                PopulateList(HighSchoolContainerControl, HighSchoolControlDialogButton,
                             seekerProfileCriteria.HighSchools);

                PopulateList(SchoolDistrictContainerControl, SchoolDistrictControlDialogButton,
                             seekerProfileCriteria.SchoolDistricts);
                PopulateList(CityContainerControl, CityControlDialogButton, seekerProfileCriteria.Cities);
                PopulateList(CountyContainerControl, CountyControlDialogButton, seekerProfileCriteria.Counties);
            } else
            {
                seekerProfileCriteria.HighSchools.Clear();
                seekerProfileCriteria.SchoolDistricts.Clear();
                seekerProfileCriteria.Cities.Clear();
                seekerProfileCriteria.Counties.Clear(); 
            }

            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, seekerProfileCriteria.AcademicAreas);
            PopulateList(CareersContainerControl, CareersControlDialogButton, seekerProfileCriteria.Careers);

            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, seekerProfileCriteria.Organizations);
            PopulateList(CompanyContainerControl, CompanyControlDialogButton, seekerProfileCriteria.Companies);
            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, seekerProfileCriteria.Hobbies);
            PopulateList(SportsContainerControl, SportsControlDialogButton, seekerProfileCriteria.Sports);
            PopulateList(ClubsContainerControl, ClubsControlDialogButton, seekerProfileCriteria.Clubs);
            
            PopulateList(ClassRankContainerControl, ClassRankControl, seekerProfileCriteria.ClassRanks);
            PopulateGPAObjects();
             
                seekerProfileCriteria.SATScore = new SatScore();
                seekerProfileCriteria.SATScore.Writing = SATWritingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.CriticalReading = SATCriticalReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.Mathematics = SATMathematicsControl.CreateIntegerRangeCondition();
             
                seekerProfileCriteria.ACTScore = new ActScore();
                seekerProfileCriteria.ACTScore.English = ACTEnglishControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Mathematics = ACTMathematicsControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Reading = ACTReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Science = ACTScienceControl.CreateIntegerRangeCondition();
             
            if (string.IsNullOrEmpty(APCreditControl.Text))
                seekerProfileCriteria.APCreditsEarned = null;
            else
                seekerProfileCriteria.APCreditsEarned = Convert.ToInt32(APCreditControl.Amount);

            if (string.IsNullOrEmpty(IBCreditControl.Text))
                seekerProfileCriteria.IBCreditsEarned = null;
            else
                seekerProfileCriteria.IBCreditsEarned = Convert.ToInt32(IBCreditControl.Amount);

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage <ScholarshipStages.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStages.NotActivated;
        }


        private void PopulateSelectionAttributesObject(SingleAttributeSelectionControl attributeSelectionControl)
        {
              SingleAttributeSelectionControl.AttributeUsageButtonGroup buttonGroup = BuildButtonGroup(attributeSelectionControl);
                var attribute = (SeekerProfileAttribute)buttonGroup.AttributeEnumInt;
                if (!buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
                {
                    var attributeUsage = new SeekerProfileAttributeUsage
                    {
                        Attribute = attribute,
                        UsageType = buttonGroup.Value
                    };
                    ScholarshipInContext.SeekerProfileCriteria.Attributes.Add(attributeUsage);
                }
                else
                {
                    ScholarshipInContext.SeekerProfileCriteria.Attributes.Remove(attribute);
                }
             
        }
        private void PopulateGPAObjects()
        {
            var minimum = (double)GPAGreaterThanControl.Amount;
            var maximum = GPALessThanControl.Text.CreateNullableDouble();

            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            seekerProfileCriteria.GPA = new RangeCondition<double?>(minimum, maximum);
        }

        private static SingleAttributeSelectionControl.AttributeUsageButtonGroup BuildButtonGroup(SingleAttributeSelectionControl attributeSelectionControl)
        {
            var usageMinimumRB =(RadioButton)attributeSelectionControl.FindControl("Minimum");
            var usagePreferenceRB = (RadioButton)attributeSelectionControl.FindControl("Preference");
            var usageNotUsedRB = (RadioButton)attributeSelectionControl.FindControl("Minimum");
            var attributeControl = (HtmlInputHidden)attributeSelectionControl.FindControl("KeyControl");
            return new SingleAttributeSelectionControl.AttributeUsageButtonGroup(Int32.Parse(attributeControl.Value), usageMinimumRB, usagePreferenceRB, usageNotUsedRB);
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool ValidateStep()
        {
            return true;
        }

        #endregion

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int) WizardStepName.MatchCriteria);
        }
    }
}

/* In ref: WSC-352 and .aspx of GPA 
    Per WSC-352, GPA Less than Field is expected to be null and number control that we use do not support blank (null)
    value. So, for that, GPA is not implimented as like other fields. JavaScript code for third party control
    has been modified to support this situation.
*/