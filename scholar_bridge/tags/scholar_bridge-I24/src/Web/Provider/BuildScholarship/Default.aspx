﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Default" %>

<%@ Register Src="GeneralInfo.ascx" TagName="GeneralInfo" TagPrefix="sb" %>
<%@ Register Src="SeekerProfile.ascx" TagName="SeekerProfile" TagPrefix="sb" %>
<%@ Register Src="FundingProfile.ascx" TagName="FundingProfile" TagPrefix="sb" %>
<%@ Register Src="MatchCriteriaSelection.ascx" TagName="MatchCriteriaSelection" TagPrefix="sb" %>
<%@ Register Src="AdditionalCriteria.ascx" TagName="AdditionalCriteria" TagPrefix="sb" %>
<%@ Register Src="Activate.ascx" TagName="Activate" TagPrefix="sb" %>
<%@ Register Src="~/Common/EntityTitleStripe.ascx" TagName="ScholarshipTitleStripe"
    TagPrefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register Src="~/Common/PrintView.ascx" TagName="PrintView" TagPrefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>

    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div id="confirmSaving" title="Confirm saving" style="display: none">
        You have change the data. Would you like to save the changes?
    </div>
    
    <br />
    <div id="deleteConfirmDiv" title="Confirm delete" style="display: none">
        Deleting the scholarship will remove it from the system. Only scholarships that
        have yet to be activated can be deleted.
        <br />
        <br />
        Are you sure want to delete?
    </div>
    
    <div id="HomeContentLeft">
        <sb:ScholarshipTitleStripe ID="ScholarshipTitleStripeControl" runat="server" HasPrintView="false" />
    </div>
    <div id="HomeContentRight">
        <sb:PrintView ID="PrintView1" runat="server" />
    </div>
    <div class="tabs" id="BuildScholarshipWizardTab">
        <ul>
            <li><a href="#tab"><span>General</span> </a></li>
            <li><a href="#tab"><span>Applicant Guidelines</span> </a></li>
            <li><a href="#tab"><span>Seeker</span> </a></li>
            <li><a href="#tab"><span>Financial Need</span> </a></li>
            <li><a href="#tab"><span>+Requirements</span> </a></li>
            <li><a href="#tab"><span>Activate</span> </a></li>
        </ul>

        <div id="tab">
            <asp:MultiView ID="BuildScholarshipWizard" runat="server" OnActiveViewChanged="BuildScholarshipWizard_ActiveStepChanged">
                <asp:View ID="GeneralInfoStep" runat="server">
                    <sb:GeneralInfo ID="generalInfo" runat="server" />
                </asp:View>
                <asp:View ID="MatchCriteriaSelectionStep" runat="server">
                    <sb:MatchCriteriaSelection ID="matchCriteriaSelection" runat="server" />
                </asp:View>
                <asp:View ID="SeekerProfileStep" runat="server">
                    <div style="float: right">
                        <sbCommon:AnchorButton id="SeekerProfileStepListChangeButton" runat="server" Text="List Change" 
                            OnClientClick="javascript:window.open('../scholarships/SubmitListChangeRequest.aspx');" />
                    </div>
                    <br />
                    <sb:SeekerProfile ID="seekerProfile" runat="server" />
                </asp:View>
                <asp:View ID="FundingProfileStep" runat="server">
                    <div style="float: right">
                        <sbCommon:AnchorButton id="FundingProfileStepListChangeButton" runat="server" Text="List Change" 
                            OnClientClick="javascript:window.open('../scholarships/SubmitListChangeRequest.aspx');" />
                    </div>
                    <br />
                    <sb:FundingProfile ID="fundingProfile" runat="server" />
                </asp:View>
                <asp:View ID="CriteriaStep" runat="server">
                    <sb:AdditionalCriteria ID="additionalCriteria" runat="server" />
                </asp:View>
                <asp:View ID="ActivateScholarshipStep" runat="server">
                    <sb:Activate ID="ActivateStep" runat="server" />
                </asp:View>
            </asp:MultiView>
            <sbCommon:jQueryTabIntegrator ID="buildScholarshipWizardTabIntegrator" runat="server"
                MultiViewControlID="BuildScholarshipWizard" TabControlClientID="BuildScholarshipWizardTab"
                CausesValidation="true" />
        </div>
    </div>
    <div id="Clear">
    </div>
    <div id="ButtonWrapper">
        <div id="ButtonContainerLeft">
            <sbCommon:SaveConfirmButton ID="SaveNoConfirmButton" CauseValidation="false" NeedsConfirmation="false" OnClick="SaveNoConfirmButton_Click"
                runat="server" Text="Save & Exit" />
            <sbCommon:ConfirmButton ID="deleteConfirmBtn" ConfirmMessageDivID="deleteConfirmDiv"
                Width="150px" Text="Delete Scholarship" runat="server" OnClickConfirm="deleteConfirmBtn_Click" />
        </div>
        <div id="ButtonContainerRight">
            <sbCommon:SaveConfirmButton ID="previousButton" runat="server" NeedsConfirmation="false" CauseValidation="false"
                ToolTip="Save & navigate to the prior tab" OnClick="PreviousButton_Click" Text="Previous" />
            <sbCommon:SaveConfirmButton ID="nextButton" runat="server" NeedsConfirmation="false"
                ToolTip="Save & navigate to the next tab" OnClick="NextButton_Click" Text="Next" />
        </div>
    </div>
    <!-- hack to get the WebForm_DoPostBackWithOptions to be emitted -->
    <span style="visibility: hidden;">
        <sbCommon:AnchorButton ID="saveButton" runat="server" OnClick="SaveButton_Click" ToolTip="Save any work on this tab" Text="Save" />
    </span>
</asp:Content>
