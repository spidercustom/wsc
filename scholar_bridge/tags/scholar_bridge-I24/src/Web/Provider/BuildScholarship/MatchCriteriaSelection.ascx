﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchCriteriaSelection.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.MatchCriteriaSelection" %>
<%@ Register TagPrefix="sb" TagName="AttributeSelectionControl" Src="AttributeSelectionControl.ascx" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<span><span class="requiredAttributeIndicator">*</span> = required</span><br />
<h2>Build Scholarship - Applicant's Profile</h2>
<p>Are there restrictions on who can apply for the scholarship? The school, type of program, or expenses the scholarship can be used for? Indicate with these fields what type of student can apply for the scholarship and what the scholarship can be used for. All are selected by default.</p>
<div class="form-iceland-container">
    <div class="form-iceland">
        
        <div class="form-iceland-container">
                <div class="form-iceland two-columns">
                    <p class="form-section-title-small">Type of Student Eligible to Apply</p>
                      <asp:PlaceHolder ID="StudentGroupContainerControl" runat="server">
                      <label id="StudentGroupLabelControl" for="StudentGroupControl">Student Groups:<span class="requiredAttributeIndicator">*</span></label>
                        <sb:FlagEnumCheckBoxList id="StudentGroupControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.StudentGroups, ScholarBridge.Domain"></sb:FlagEnumCheckBoxList>
                        <sb:CoolTipInfo ID="CoolTipInfoStudentGroupControl" Content="Select all that apply. Will match with applicants answer to 'What type of student are you currently?'  Use to restrict who can apply." runat="server" />

                      <br />
                      </asp:PlaceHolder>
                      
                      <br />
                      <p class="form-section-title-small">College and School type Scholarship can be used for</p>
                      <asp:PlaceHolder ID="CollegesContainerControl" runat="server">
                        <label id="CollegesControlLabel1" for="CollegesRadioButtons">Colleges:<span class="requiredAttributeIndicator">*</span></label>
                        <sb:EnumRadioButtonList id="CollegesRadioButtons" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.CollegeType, ScholarBridge.Domain" />
                        <sb:CoolTipInfo ID="CoolTipInfoCollegesControl" Content="Use to indicate if the scholarship can only be used at specific colleges., Use to restrict who can apply." runat="server" />
                         <label id="CollegesControlLabel2" for="CollegesControl">.</label>
                       
                        <sb:LookupDialog ID="CollegesControlDialogButton" runat="server" BuddyControl="CollegesControl" ItemSource="CollegeDAL" Title="College Selection"/>
                        <asp:TextBox ID="CollegesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
                        
                        <asp:RequiredFieldValidator ID="CollegesRequiredValidator" ControlToValidate="CollegesControl" runat="server" ErrorMessage="Select at least one college"></asp:RequiredFieldValidator>
                        <br />
                      </asp:PlaceHolder>
                      <br />
                      <asp:PlaceHolder ID="SchoolTypeContainerControl" runat="server">
                      <label id="SchoolTypeControlLabel" for="SchoolTypeControl">School types:<span class="requiredAttributeIndicator">*</span></label>
                       <sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
                        <sb:CoolTipInfo ID="CoolTipInfoSchoolTypeControl" Content="Select all that apply. Will match with applicants answer to 'What types of schools are you considering?' Use to restrict the type of school the scholarship can be used for." runat="server" />

                      <br />
                      </asp:PlaceHolder>
                      
                </div>
                
                
                
                <div class="vertical-devider"></div>
                <div class="form-iceland two-columns">
                      <p class="form-section-title-small">Program or Expenses Scholarship can be used for</p>
                      <asp:PlaceHolder ID="AcademicProgramContainerControl" runat="server">
                      <label id="AcademicProgramControlLabel" for="AcademicProgramControl">Academic Programs:<span class="requiredAttributeIndicator">*</span></label>
                        <sb:FlagEnumCheckBoxList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
                       <sb:CoolTipInfo ID="CoolTipInfoAcademicProgramControl" Content="Select all that apply. Use to indicate the type of program the scholarship can be used for." runat="server" />
                        <br />
                      </asp:PlaceHolder>
                      <br />
                      
                      <asp:PlaceHolder ID="SeekerStatusContainerControl" runat="server">
                      <label id="SeekerStatusControlLabel" for="SeekerStatusControl">Enrollment Status:<span class="requiredAttributeIndicator">*</span></label>
                       <sb:FlagEnumCheckBoxList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
                        <sb:CoolTipInfo ID="CoolTipInfoSeekerStatusControl" Content="Select all that apply. Use to indicate the level of enrollment the scholarship can be used for." runat="server" />
                      <br />
                      </asp:PlaceHolder>
                      <br />
                      
                      <asp:PlaceHolder ID="ProgramLengthContainerControl" runat="server">
                      <label id="ProgramLengthControlLabel" for="ProgramLengthControl">Length of Program:<span class="requiredAttributeIndicator">*</span></label>
                      <sb:FlagEnumCheckBoxList id="ProgramLengthControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.ProgramLengths, ScholarBridge.Domain"  />
                       <sb:CoolTipInfo ID="CoolTipInfoProgramLengthControl" Content="Select all that apply. Use to indicate the length of program the scholarship can be used for." runat="server" />
                      <br />
                      </asp:PlaceHolder>
                      <br />
                      
                      <asp:PlaceHolder ID="TypesOfSupportContainerControl" runat="server">
                      <label id="TypesOfSupportControlLabel" for="TypesOfSupportControl">Situations the Scholarship will fund:<span class="requiredAttributeIndicator">*</span></label>
                        <sb:CoolTipInfo ID="CoolTipInfoTypesOfSupport" Content="Select all that apply. Use to indicate what expenses the scholarship can be used to cover." runat="server" />
                     
                        <div class="control-set">
                            <asp:CheckBoxList ID="TypesOfSupport" runat="server"   RepeatDirection="Vertical" RepeatLayout="Flow"  Width="170px"/>
                        </div>
                        
                       
                        <br />
                      </asp:PlaceHolder>
                      <br />
                      
                      
                </div>
        </div>      
        <br />
    </div> 
</div>  

         