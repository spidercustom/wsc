﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web;
namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class SingleAttributeSelectionControl : System.Web.UI.UserControl
    {
        public int AttributeKey
        {
            get
            {
                if (ViewState["AttributeKey"] == null)
                    return 0;
                return int.Parse( ViewState["AttributeKey"].ToString());
            }
            set
            {
                ViewState["AttributeKey"] = value;
            }
        }
        public string AttributeValue
        {
            get
            {
                if (ViewState["AttributeValue"] == null)
                    return null;
                return (string)ViewState["AttributeValue"];
            }
            set
            {
                ViewState["AttributeValue"] = value;
            }
        }
        public bool DisplayHeader
        {

            get
            {
                if (ViewState["DisplayHeader"] == null)
                    return false;
                return (bool)ViewState["DisplayHeader"];
            }
            set
            {
                ViewState["DisplayHeader"] = value;
            }
        }
         

        public string BuddyControl { get; set; }
        public AttributeUsageButtonGroup UsageButtonGroup {
            get
            {
                var usageMinimumRB = Minimum;
                var usagePreferenceRB = Preference;
                var usageNotUsedRB = NotUsed;
                var attributeControl = KeyControl;
                var attributeEnumID = int.Parse(attributeControl.Value);
                return new AttributeUsageButtonGroup(attributeEnumID, usageMinimumRB, usagePreferenceRB, usageNotUsedRB);
            }
        
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            headerrow.Visible = DisplayHeader;
        }

        private void SetBuddyControlState(bool state)
        {
             
            var buddy = (PlaceHolder)ControlHelper.FindControlRecursive(Page, BuddyControl);
            if (buddy != null)
            {
                ControlHelper.SetControlStateRecursive(buddy, state);
            }
        }

        

        
        public void Bind()
        {
            attributeName.Text = AttributeValue;
            KeyControl.Value = AttributeKey.ToString();

            
        }
        
        protected void CheckBox_Changed (object sender, EventArgs e)
        {
            SetBuddyControlState(!NotUsed.Checked);

        }

        public class AttributeUsageButtonGroup
		{
			RadioButton Minimum
			{
				get;
				set;
			}
			RadioButton Preference
			{
				get;
				set;
			}
			RadioButton NotUsed
			{
				get;
				set;
			}
			public int AttributeEnumInt
			{
				get; private set;
			}
            
		    public ScholarshipAttributeUsageType Value
			{
				get
				{
					if (Minimum.Checked)
						return ScholarshipAttributeUsageType.Minimum;
					if (Preference.Checked)
						return ScholarshipAttributeUsageType.Preference;
					return ScholarshipAttributeUsageType.NotUsed;
				}
				set
				{
					Minimum.Checked = Preference.Checked = NotUsed.Checked = false;
					switch (value)
					{
						case ScholarshipAttributeUsageType.Minimum:
							Minimum.Checked = true;
							break;
						case ScholarshipAttributeUsageType.Preference:
							Preference.Checked = true;
							break;
						default:
							NotUsed.Checked = true;
							break;
					}
				}
			}
			public AttributeUsageButtonGroup(int attribute, RadioButton minimum, RadioButton preference, RadioButton notUsed)
			{
				AttributeEnumInt = attribute; 
				Minimum = minimum;
				Preference = preference;
				NotUsed = notUsed;
			}
		}

        

	}
}