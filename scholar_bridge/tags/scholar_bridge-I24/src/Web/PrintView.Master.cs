﻿using System;
using System.Web.UI;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web
{
    public partial class PrintViewMasterPage : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "ScholarBridge | " + Page.Header.Title;

            PrintViewHeadContainer.Visible = Page.IsInPrintView();
             PrintViewPageHeaderPlaceHolder.Visible = Page.IsInPrintView();
        }
    }
}
