﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectRelatedIntermediary.ascx.cs" Inherits="ScholarBridge.Web.Common.SelectRelatedIntermediary" %>
<asp:DropDownList ID="intermediaryDDL" runat="server" onselectedindexchanged="intermediaryDDL_SelectedIndexChanged"  AutoPostBack="true" />
<div id="intermediaryNotAvailable" class="errorMessage" runat="server" visible="false">Intermediary is no longer valid (since you no longer have a relationship with them). Please select a valid Intermediary from the list.</div>
