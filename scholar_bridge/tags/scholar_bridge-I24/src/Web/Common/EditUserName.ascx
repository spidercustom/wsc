﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditUserName.ascx.cs" Inherits="ScholarBridge.Web.Common.EditUserName" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
 <%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>            
<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>

<div class="form-iceland-container">
    <div class="form-iceland">
     <label for="FirstName">First Name:</label>
    <asp:TextBox ValidationGroup="EditUserName_Validations" ID="FirstName" runat="server" ></asp:TextBox>
    <elv:PropertyProxyValidator  ValidationGroup="EditUserName_Validations" ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" />
    <br />
    <label for="MiddleName">Middle Name:</label>
    <asp:TextBox ValidationGroup="EditUserName_Validations" ID="MiddleName" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ValidationGroup="EditUserName_Validations" ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName" />
    <br />
    <label for="LastName">Last Name:</label>
    <asp:TextBox ValidationGroup="EditUserName_Validations" ID="LastName" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ValidationGroup="EditUserName_Validations" ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" />
    <br />
    <label for="PhoneNumber">Phone Number:</label>
    <asp:TextBox ValidationGroup="EditUserName_Validations" ID="PhoneNumber" runat="server" CssClass="phone" />
    <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="PhoneNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
    <br />
    <label for="FaxNumber">Fax Number:</label>
    <asp:TextBox ValidationGroup="EditUserName_Validations" ID="FaxNumber" runat="server" CssClass="phone" />
    <elv:PropertyProxyValidator ValidationGroup="EditUserName_Validations" ID="PropertyProxyValidator1" runat="server" ControlToValidate="FaxNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
    <br />
    <label for="OtherPhoneNumber">Other Phone:</label>
    <asp:TextBox ValidationGroup="EditUserName_Validations" ID="OtherPhoneNumber" runat="server" CssClass="phone" />
    <elv:PropertyProxyValidator ValidationGroup="EditUserName_Validations" ID="PropertyProxyValidator2" runat="server" ControlToValidate="OtherPhoneNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
     <br />
    <label for="LastLoginBox">Last Login On:</label>
    <asp:TextBox ID="LastLoginBox" runat="server" Enabled="false" ></asp:TextBox>
     <br />
      <label for="MemberSinceBox">Member Since:</label>
    <asp:TextBox ID="MemberSinceBox" runat="server" Enabled="false" ></asp:TextBox>
     <br />
    <sbCommon:AnchorButton ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click" ValidationGroup="EditUserName_Validations" />
     <br /><hr><br />
    </div>
    </div> 