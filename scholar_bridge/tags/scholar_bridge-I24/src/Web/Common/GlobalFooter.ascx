﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalFooter.ascx.cs" Inherits="ScholarBridge.Web.Common.GlobalFooter" %>
<div class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</div>
<div class="GreenFooterLinks"><A href="">About Us</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/PressRoom/") %>'>Press Room</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/Terms.aspx") %>'>Terms and Conditions</A>  |  
                            <A href='<%= LinkGenerator.GetFullLink("/ContactUs.aspx") %>'>Contact Us</A>  |  
                            <A href="#">Sitemap</A>  |  
                            <A href="#">Resources</A>  |  
                            <A href="#">Help</A>  
                            <A id="loginLink" runat="server">| Log In</A></div>

