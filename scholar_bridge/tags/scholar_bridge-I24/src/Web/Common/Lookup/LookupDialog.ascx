﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupDialog.ascx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupDialog" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<div >
  
    <asp:ImageButton  CssClass="LookupSelectButton" ID="btnLookup"  runat="server"   ImageUrl="~/Images/Btn_Select.gif" ImageAlign="AbsMiddle"       UseSubmitBehavior="False"/>
    <asp:HiddenField ID="hiddenids" runat="server" Value=""  /> 
    <asp:HiddenField ID="HiddenUserData" runat="server" Value=""  />     
    <asp:HiddenField ID="HiddenOthers" runat="server" Value=""  />     
</div>