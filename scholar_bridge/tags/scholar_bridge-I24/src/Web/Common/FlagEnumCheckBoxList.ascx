﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlagEnumCheckBoxList.ascx.cs" Inherits="ScholarBridge.Web.Common.FlagEnumCheckBoxList" %>
<div class="control-set">
    <asp:CheckBoxList ID="CheckBoxList" runat="server"  RepeatDirection="Vertical" RepeatLayout="Flow" Width="170px">
    </asp:CheckBoxList>
    <br />
    <asp:CustomValidator ID="SelectNoneValidator" runat="server" 
      ErrorMessage="Select atleast one" 
      onservervalidate="SelectNoneValidator_ServerValidate"></asp:CustomValidator>
    <br />
</div>