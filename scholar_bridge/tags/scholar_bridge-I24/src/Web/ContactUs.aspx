﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="ScholarBridge.Web.ContactUs" Title="Contact Us" %>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />

<h2>Contact Us</h2>

<asp:panel ID="contactUsSelectionPanel" runat="server">
    <p>We are looking foward to hearing from you.&nbsp;&nbsp;Please select one of the links 
        below to address your questions, comments and concerns to the right member of 
        our staff.</p>

<p>
<ul>
    <li>
        Do you need help using a feature of theWashBoard website?&nbsp;&nbsp; 
        <asp:linkbutton ID="supportMessageLink" runat="server" 
            onclick="supportMessageLink_Click">Click here to send a message to theWashBoard.org support unit.</asp:linkbutton>
    </li>
</ul>
<ul>
    <li>
        Have you discovered a problem or error on our website?&nbsp;&nbsp; 
        <asp:linkbutton ID="problemMessageLink" runat="server" 
            onclick="problemMessageLink_Click">Click here to alert the theWashBoard.org administrator.</asp:linkbutton>
    </li>
</ul>
<ul>
    <li>
        Got a suggestion for a new feature or improvement for theWashBoard?&nbsp;&nbsp; 
        <asp:linkbutton ID="suggestionMessageLink" runat="server" 
            onclick="suggestionMessageLink_Click">Send your ideas to theWashBoard development team.</asp:linkbutton>
    </li>
</ul>
    <p>
    </p>
    <p>
        Thanks for making theWashBoard.org part of your college exploration experience.</p>
</p>

</asp:panel>

<asp:panel runat="server" ID="contactUsMessagePanel">
    <uc1:ContactUs ID="contactUsControl" 
                    OnFormCanceled="contactUsControl_Canceled" 
                    OnFormSent="contactUsControl_Sent" 
                    runat="server" />
</asp:panel>


</asp:Content>
