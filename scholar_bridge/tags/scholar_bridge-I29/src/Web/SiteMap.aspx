﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SiteMap.aspx.cs" Inherits="ScholarBridge.Web.SiteMap" Title="Terms of Use" %>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />

<h2>Site Map</h2>

<asp:image runat="server" ID="underConstruction" ImageUrl="~/images/under_construction.jpg" ToolTip="The Sitemap is being built.  Coming soon!" />

<h5>Under Construction - Hard-hats required.<asp:TreeView ID="TreeView1" 
        runat="server" DataSourceID="siteMapDataSource">
    </asp:TreeView>
    </h5>

<asp:SiteMapDataSource ID="siteMapDataSource"  runat="server" />
</asp:Content>
