﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListChangeRequest.ascx.cs" Inherits="ScholarBridge.Web.Common.ListChangeRequest" %>


<div>
<h2>Submit Request for Scholarship Criteria</h2>
<h3>Use this form to submit a request to have new criteria values added to the system. An e-mail will be sent to the System Administrator and a copy will be sent to your "Our Messages" Sent tab. Requests will be responded to within 5 business days.</h3>
<h3> You can also add Scholarship Specific Questions on the +Requirements tab.</h3>

<p  style="font-size:12px;"><span  style="font-weight:bold;">From : </span><asp:Label ID="lblFrom" runat="server"    /></p>
<table class="viewonlyTable">
    <tbody>
        <tr>
            <th>Choose the field you would like to submit a change to</th>
            <td><asp:DropDownList runat="server" ID="cboListType"  Width="207px"></asp:DropDownList></td>
        </tr>
        <tr>
            <th>Enter the new Value you would like to see listed in the selection criteria you chose (200 character limit)</th>
            <td>
            <asp:TextBox runat="server" ID="txtValue" MaxLength="200" Width="467px" />
            <asp:RequiredFieldValidator ID="ValueRequiredValidator" runat="server" ControlToValidate="txtValue" ErrorMessage="Value cannot be empty."></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator  runat="server" ID="valueValidator" ControlToValidate="txtValue" ValidationExpression="^[\w\s]{1,200}$" ErrorMessage="Value should not exceed 200 character limit."></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <th>Reason for Change: Please describe if this is a new value or a change to an existing list value 
and why you are requesting this change (500 Character limit).</th>
            <td>
            <asp:TextBox runat="server" ID="txtReason" MaxLength="100" Rows="5"  Height="69px" Width="467px" TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="reasonRequiredValidtor" runat="server" ControlToValidate="txtReason" ErrorMessage="Reason cannot be empty."></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator  runat="server" ID="reasonValidator" ControlToValidate="txtReason" ValidationExpression="^[\w\s]{1,500}$" ErrorMessage="Reason should not exceed 500 character limit."></asp:RegularExpressionValidator>
        </td>
        </tr>
    </tbody>
</table>
</div>