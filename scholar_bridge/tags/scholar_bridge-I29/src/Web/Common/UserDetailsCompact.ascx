﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetailsCompact.ascx.cs" Inherits="ScholarBridge.Web.Common.UserDetailsCompact" %>

<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>

<div id="memberInfo">
    <table>
        <tr>
            <td>
                Name: 
            </td>
            <td>
                <asp:Literal ID="nameLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Email: 
            </td>
            <td>
                <asp:Literal ID="emailAddressLbl" runat="server" />
            </td>
        </tr>
        
        <tr>
            <td>
                Phone: 
            </td>
            <td>
                <asp:Literal ID="phoneLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Fax: 
            </td>
            <td>
                <asp:Literal ID="faxLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Other Phone: 
            </td>
            <td>
                <asp:Literal ID="otherPhoneLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Member Since:&nbsp;&nbsp; 
            &nbsp;</td>
            <td>
                <asp:Literal ID="memberSinceLbl" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Last Login: 
            </td>
            <td>
                <asp:Literal ID="lastLoginLbl" runat="server" />
            </td>
        </tr>
    </table>
   
</div>