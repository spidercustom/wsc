﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerStudentType.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerStudentType" %>

<script type="text/javascript">
$(document).ready(function() {
    $("input[name='ctl00$bodyContentPlaceHolder$AcademicInfo1$CurrentStudentGroupControl$studentGroup']").click(
        function() {
            var checkedvalue = $(this).val(); 
            $.each(["HighSchoolSeniorYear", "AdultReturningYear", "TransferYear"], function(i, name) {
                $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + name).attr("disabled","disabled");
            });
            $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + checkedvalue + "Year").removeAttr("disabled");
        });
    });
</script>

<style type="text/css">
    .radioSelect
    {
        width: 200px;
        text-align: left;
        height: 18px;
    }
    
    .fix-size-label-container > label
    {
        width: 120px;
        display: inline-table;
        vertical-align:top;
        padding: 0px;
        margin: 5px 5px 10px 5px; 
    }
</style>

<div class="control-set fix-size-label-container">
    <asp:RadioButton runat="server" ID="MiddleSchool" GroupName="studentGroup" /><label>Middle School</label>
    <br />
    
    <asp:RadioButton runat="server" ID="HighSchoolSenior" GroupName="studentGroup" /><label>High School</label>
    <asp:DropDownList id="HighSchoolSeniorYear" runat="server" name="HighSchoolSeniorYear" Enabled="false" CssClass="radioSelect">
        <asp:ListItem Value="1" Text="Freshman" />
        <asp:ListItem Value="2" Text="Sophomore" />
        <asp:ListItem Value="3" Text="Junior" />
        <asp:ListItem Value="4" Text="Senior" />
    </asp:DropDownList>
    <br />
    
    <asp:RadioButton runat="server" ID="HighSchoolGraduate" GroupName="studentGroup"/><label>High School Graduate</label>
    <br />
    
    <asp:RadioButton runat="server" ID="AdultFirstTime" GroupName="studentGroup" /> <label>Adult First time </label>
    <br />
    
    <asp:RadioButton runat="server" ID="AdultReturning" GroupName="studentGroup"/>  <label>Adult Returning</label> 
    <asp:DropDownList id="AdultReturningYear" runat="server" name="AdultReturningYear" Enabled="false" CssClass="radioSelect">
        <asp:ListItem Value="1" Text="1" />
        <asp:ListItem Value="2" Text="2" />
        <asp:ListItem Value="3" Text="3" />
        <asp:ListItem Value="4" Text="4" />
        <asp:ListItem Value="5" Text="5" />
        <asp:ListItem Value="6" Text="6" />
    </asp:DropDownList>
    <br />
    
    <asp:RadioButton runat="server" ID="Transfer" GroupName="studentGroup"/> <label>Transfer</label> 
    <asp:DropDownList id="TransferYear" runat="server" name="TransferYear" Enabled="false" CssClass="radioSelect">
        <asp:ListItem Value="1" Text="1" />
        <asp:ListItem Value="2" Text="2" />
        <asp:ListItem Value="3" Text="3" />
        <asp:ListItem Value="4" Text="4" />
        <asp:ListItem Value="5" Text="5" />
        <asp:ListItem Value="6" Text="6" />
    </asp:DropDownList>
    <br />
    
    <asp:RadioButton runat="server" ID="Undergraduate" GroupName="studentGroup"/> <label>Undergraduate</label>
    <br />
    
    <asp:RadioButton runat="server" ID="Graduate" GroupName="studentGroup"/> <label>Graduate</label>
    <br />
</div>
