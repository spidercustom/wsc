﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    [ValidationProperty("SelectedDate")]
    public partial class CalendarControl : UserControl
    {
        
        public DateTime? SelectedDate
        {
            get
            {
                DateTime result;
                var x = DateTime.TryParse(datepicker.Text, out result) ? result : new DateTime?(new DateTime(1752,1,1));
                
                return x;
            }
            set {
                SetText(value);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetText(SelectedDate);
        }

        protected override void OnPreRender(EventArgs e)
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (page != null)
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), datepicker.ClientID + "_script", GetJS());
            base.OnPreRender(e);
        }

        private void SetText(DateTime? value)
        {
           if (value.HasValue )
           {
               datepicker.Text = value.Value==new DateTime(1752,1,1) ? "" : value.Value.ToString("d");
           }else
           {
               datepicker.Text = "";
           }
            
        }

        private const string DATEPICKER_SCRIPT_TEMPLATE = "$(function() {{  $('#{0}').datepicker({{ showButtonPanel: true, showOn: 'button',currentText: 'Got to Today', closeText: 'Close', buttonImage: '{1}', buttonImageOnly: true }}); }});";
        private string GetJS()
        {
            return DATEPICKER_SCRIPT_TEMPLATE.Build(datepicker.ClientID, ResolveUrl("~/images/calendar.gif"));
        }
    }
}