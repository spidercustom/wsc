﻿using System;
using ScholarBridge.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class PrintView : System.Web.UI.UserControl
    {
		public string CustomUrl
		{
			get
			{
				if (ViewState["CustomPrintUrl"] == null)
					return null;
				return (string) ViewState["CustomPrintUrl"];
			}
			set
			{
				ViewState["CustomPrintUrl"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
        {
            PrintViewContainer.Visible = Page.IsInPrintView();
            ScreenViewContainer.Visible = !PrintViewContainer.Visible;

        }

		protected void Page_PreRender(object sender, EventArgs e)
		{
            printViewLink.HRef = BuildPrintViewUrl();
		}

        private string BuildPrintViewUrl()
        {
            if (!String.IsNullOrEmpty(CustomUrl))
				return CustomUrl;

            var uriBuilder = new UrlBuilder(Request.Url.AbsoluteUri);
            if (! uriBuilder.QueryString.ContainsKey("print"))
            {
                uriBuilder.QueryString.Add("print", "true");
            }
            return uriBuilder.ToString();
        }
    }
}