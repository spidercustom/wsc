﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Profile.Default" Title="Profile" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %> 

<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="EditUserName" Src="~/Common/EditUserName.ascx" %>
<%@ Register TagPrefix="sb" TagName="EditOrganizationUserEmail" Src="~/Common/EditOrganizationUserEmail.ascx" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
     
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script language="javascript" type="text/javascript">
    $(document).ready(function() {
        $("#pnlchangepassword").keypress(function(e) {

            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $("#pnlchangepassword > a[id$='_ChangePasswordPushButton']").click();
                return true;
            }
        });
    });
 </script>  
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopMySettings.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomMySettings.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
        <asp:LoginView ID="loginView2" runat="server">
            <RoleGroups>
                 <asp:RoleGroup Roles="Seeker">
                    <ContentTemplate>
                        <!--Left floated content area starts here-->
                        <div id="HomeContentLeft">

                          <Img src="<%= ResolveUrl("~/images/PgTitle_MySettings.gif") %>" width="149px" height="54px">
                          <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerLoggedIn.gif") %>" width="513px" height="96px">
                          </div>
                          <!--Left floated content area ends here-->

                         <!--Right Floated content area starts here-->
                         <div id="HomeContentRight">
                         <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                         </div>
                        <BR><BR>

                        <div id="Clear"></div>  
                        </ContentTemplate>             
                 </asp:RoleGroup>
                 <asp:RoleGroup Roles="Intermediary, Intermediary Admin, Provider, Provider Admin">
                    <ContentTemplate>
                         <Img src="<%= ResolveUrl("~/images/PgTitle_MySettings.gif") %>" width="149px" height="54px">
                        <BR><BR>
                        <div id="Clear"></div>  
                        </ContentTemplate>             
                 </asp:RoleGroup>
            </RoleGroups>
        </asp:LoginView>

<div>
    <p class="FormHeader">Change User Details</p>
    <br />
    <sb:EditUserName ID="editUserName" runat="server"  
        OnUserSaved="editUserName_OnUserSaved" />
</div>

<div>
    <p class="FormHeader">Change Email Address</p>
    <br />
    <sb:EditOrganizationUserEmail runat="server" ID= "editUserEmail" />   
    <br /><hr><br />
</div>    
 
<div>
    <p class="FormHeader">Change Password</p>
    <br />
        <asp:ChangePassword ID="ChangePassword1" runat="server" 
            SuccessPageUrl="~/Profile/Default.aspx"  
            
            
            onchangedpassword="ChangePassword1_ChangedPassword" BorderPadding="4"   ChangePasswordButtonType="Image" ChangePasswordButtonImageUrl ="~/images/btn_save.gif" CancelButtonStyle-CssClass="hidden">
        <ChangePasswordTemplate>
        <div class="form-iceland-container">
        <div id ="pnlchangepassword"  class="form-iceland">
                <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Current Password:</asp:Label>
                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox> 
                <asp:RequiredFieldValidator  ID="CurrentPasswordRequired"   runat="server" 
                                             ControlToValidate="CurrentPassword"
                                             ErrorMessage="Password is required." 
                                             ToolTip="Password is required." 
                                             ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                <br />
                <asp:Label  ID="NewPasswordLabel"  runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                <asp:TextBox  ID="NewPassword"   runat="server" TextMode="Password"></asp:TextBox>
                <sb:CoolTipInfo ID="CoolTipInfoPassword" runat="Server" Content="Password must be at least 8 characters with atleast one capital and one lower case letter and one special character or number" />
         
                <asp:RequiredFieldValidator 
                     ID="NewPasswordRequired" 
                     runat="server" 
                     ControlToValidate="NewPassword"
                     ErrorMessage="New Password is required." 
                     ToolTip="New Password is required."
                     ValidationGroup="ChangePassword1">*Required</asp:RequiredFieldValidator>   
                <br />
                
                <asp:Label ID="ConfirmNewPasswordLabel" runat="server"  AssociatedControlID="ConfirmNewPassword">Confirm Password:</asp:Label>
                <asp:TextBox  ID="ConfirmNewPassword" runat="server"  TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator 
                     ID="ConfirmNewPasswordRequired" 
                     runat="server" 
                     ControlToValidate="ConfirmNewPassword"
                     ErrorMessage="Confirm New Password is required." 
                     ToolTip="Confirm New Password is required."
                     ValidationGroup="ChangePassword1">*Required</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="NewPasswordCompare"  runat="server"  ControlToCompare="NewPassword"    ControlToValidate="ConfirmNewPassword" 
                                             Display="Dynamic" 
                                             ErrorMessage="The Confirm New Password must match the New Password entry."
                                             ValidationGroup="ChangePassword1"></asp:CompareValidator>                
                
                
                <br />
                <br />
                <sbCommon:AnchorButton ID="ChangePasswordPushButton" runat="server" Text="Save"   CommandName="ChangePassword" ValidationGroup="ChangePassword1"/>
                <asp:Literal  ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
        
        </div>
     </div>
        
        
        </ChangePasswordTemplate>
        </asp:ChangePassword>            
     
 </div>
</asp:Content>
