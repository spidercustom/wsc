﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
    public partial class EditQuestionAnswers : UserControl
    {
        private IList<ApplicationQuestionAnswer> _QuestionAnswers;

        public IList<ApplicationQuestionAnswer> QuestionAnswers
        {
            get { return _QuestionAnswers; }
            set
            {
                _QuestionAnswers = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
             
                Bind();
        }

        protected void Bind()
        {
            if (!(QuestionAnswers == null))
            {
                lstQA.DataSource = QuestionAnswers;
                lstQA.DataBind();
            }
        }

        protected void lstQA_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var qa = ((ApplicationQuestionAnswer)((ListViewDataItem)e.Item).DataItem);

                var answer = (TextBox)e.Item.FindControl("txtAnswer");
             
                answer.Text = qa.AnswerText;
               
                
            }
        }
        public  void  GetUpdatedAnswers(Application application )
        {
            foreach ( var qa in lstQA.Items)
            {
                var answer = (TextBox) qa.FindControl("txtAnswer");
                application.QuestionAnswers[qa.DataItemIndex].AnswerText = answer.Text;
            }
            
        }
    }
}