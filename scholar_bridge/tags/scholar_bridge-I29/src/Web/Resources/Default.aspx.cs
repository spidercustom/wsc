﻿using System;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Resources
{
    public partial class Default : System.Web.UI.Page
    {
        public IResourceService ResourceService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            resourceList1.Resources  = ResourceService.FindAll();
        }
    }
}
