/* Author: Bhupindra
Jquery Plugin to wrap all validation errors and information messages in to jtip sytle display.
*/

$(document).ready(Load_CoolTipErrors);
$(document).ready(CoolTip_init);

function CoolTip_init() {
    	       $("img.coolTip")
    		   .hover(function() { CoolTip_Show(this.id, $(this).attr('header'), $(this).attr('content'), $(this).attr('contentdivid')) }, function() { $('#COOLTIP').hide() })
               //.click(function() { $('#COOLTIP').remove() });
    	       //$("body").hover(function() { $('#COOLTIP').remove() });
}

function Load_CoolTipErrors() {
    var errorgroups = new Array();

    //group all error messages as per the validation control group
    var arrvalues = [];
    $("img.coolTipError").each(function() {

        errorgroups.push[1];
        errorgroups[errorgroups.length] = $(this).attr('group');
        var value = $(this).attr('group');
        var found = $.inArray(value, arrvalues);
        if (found < 0) {
            arrvalues.push[1];
            arrvalues[arrvalues.length] = value;
        }
    });


    $.each(arrvalues, function(index, value) {
        var messages = ''; ;
        $("img.coolTipError[group='" + value + "']").each(function() {
        messages = messages + $(this).attr('title');
            $(this).hide();


        });

        $("img.coolTipError[group='" + value + "']:first").each(function() {
            $(this).attr('title', messages);
            $(this).addClass('coolTip');
            $(this).removeClass('coolTipError');
            $(this).show();
        });
    });
}


function CoolTip_Show(linkId, title, content,contentdivId) {
    $('#COOLTIP').remove();
    if (title == false) title = "&nbsp;";

    if (content == false) content = "&nbsp;";
    
	var de = document.documentElement;
	var w = self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
	var hasArea = w - getAbsoluteLeft(linkId);
	var clickElementy = getAbsoluteTop(linkId) - 3; //set y position
	var width=350;


	if (hasArea > ((width * 1) + 75)) {
	    $("body").append("<div id='COOLTIP' style='width:" + width * 1 + "px'><div id='COOLTIP_arrow_left'></div><div id='COOLTIP_close_left'>" + title + "</div><div id='COOLTIP_CONTENT'><div class='COOLTIP_loader'><div></div></div>"); //right side
		var arrowOffset = getElementWidth(linkId) + 11;
		var clickElementx = getAbsoluteLeft(linkId) + arrowOffset; //set x position
	}else{
	$("body").append("<div id='COOLTIP' style='width:" + width * 1 + "px'><div id='COOLTIP_arrow_right' style='left:" + ((width * 1) + 1) + "px'></div><div id='COOLTIP_close_right'>" + title + "</div><div id='COOLTIP_CONTENT'><div class='JT_loader'><div></div></div>"); //left side
	var clickElementx = getAbsoluteLeft(linkId) - ((width * 1) + 15); //set x position
}

    $('#COOLTIP').css({left: clickElementx+"px", top: clickElementy+"px"});
    $('#COOLTIP').show();
    if (contentdivId.length >0) {
        $('#COOLTIP_CONTENT').append($('#'+contentdivId).html());
    } else {
        $('#COOLTIP_CONTENT').append(content);
    }
    
}

function getElementWidth(objectId) {
	x = document.getElementById(objectId);
	return x.offsetWidth;
}

function getAbsoluteLeft(objectId) {
	// Get an object left position from the upper left viewport corner
	o = document.getElementById(objectId)
	oLeft = o.offsetLeft            // Get left position from the parent object
	while(o.offsetParent!=null) {   // Parse the parent hierarchy up to the document element
		oParent = o.offsetParent    // Get parent object reference
		oLeft += oParent.offsetLeft // Add parent left position
		o = oParent
	}
	return oLeft
}

function getAbsoluteTop(objectId) {
	// Get an object top position from the upper left viewport corner
	o = document.getElementById(objectId)
	oTop = o.offsetTop            // Get top position from the parent object
	while(o.offsetParent!=null) { // Parse the parent hierarchy up to the document element
		oParent = o.offsetParent  // Get parent object reference
		oTop += oParent.offsetTop // Add parent top position
		o = oParent
	}
	return oTop
}

function parseQuery ( query ) {
   var Params = new Object ();
   if ( ! query ) return Params; // return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) continue;
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

function blockEvents(evt) {
              if(evt.target){
              evt.preventDefault();
              }else{
              evt.returnValue = false;
              }
}