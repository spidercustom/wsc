﻿$(document).ready(function() {
    var hoverFunc = function() { $(this).attr("style", "cursor: pointer;"); };
    var unhoverFunc = function() { $(this).attr("style", "cursor: default;"); }

    $(".print").mouseup(function() {
        window.print();
    }).hover(hoverFunc, unhoverFunc);
   
    
});
