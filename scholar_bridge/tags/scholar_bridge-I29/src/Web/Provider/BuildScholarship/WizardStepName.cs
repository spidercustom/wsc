﻿namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public enum WizardStepName
    {
        GeneralInformation,
        MatchCriteria,
        SeekerProfile,
        FundingProfile,
        AdditionalCriteria,
        Activate,
        Award
    }
}
