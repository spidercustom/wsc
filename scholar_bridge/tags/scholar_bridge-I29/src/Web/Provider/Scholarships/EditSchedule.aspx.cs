﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class EditSchedule : Page
    {
        private const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship Scholarship { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params[SCHOLARSHIP_ID], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
           
            Scholarship = ScholarshipService.GetById(ScholarshipId);
            if (Scholarship != null)
            {
                if (!Scholarship.IsBelongToOrganization(UserContext.CurrentOrganization))
                    throw new InvalidOperationException("Scholarship do not belong to organization in context");

                ScholarshipTitleStripeControl.UpdateView(Scholarship);
                    EditSchedule1.ScholarshipInContext = Scholarship;
                    
            }
        }

        protected void EditSchedule1_OnFormCanceled()
        {
            Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + ScholarshipId);
        }
        protected void EditSchedule1_OnScheduleSaved()
        {
            Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + ScholarshipId);
        }


    }
}
