﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;
using System.Data;

namespace ScholarBridge.Migrations
{
    [Migration(246)]
    public class AddCompany : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "Company";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
