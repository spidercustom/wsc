﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(37)]
    public class AddCollege : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "College";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
