﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(9)]
    public class AddScholarship : AddTableBase
    {
        public const string TABLE_NAME = "SB_Scholarship";
        public const string PRIMARY_KEY_COLUMN = "Id";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new ForeignKey[]
            {
                CreateForeignKey("LastUpdatedBy", AddUsers.TABLE_NAME, AddUsers.PRIMARY_KEY_COLUMN),
                CreateForeignKey("Provider", AddProvider.TABLE_NAME, AddProvider.PRIMARY_KEY_COLUMN),
            };
        }


        public override Column[] CreateColumns()
        {
            return new Column[]
            {
                new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                new Column("Provider", DbType.Int32, ColumnProperty.NotNull),

                new Column("Name", DbType.String, 50, ColumnProperty.NotNull), 
                new Column("MissionStatement", DbType.String, 2000, ColumnProperty.Null),
                new Column("StartFrom", DbType.Date, ColumnProperty.Null),
                new Column("DueOn", DbType.Date, 50, ColumnProperty.Null),
                new Column("AwardOn", DbType.Date, 50, ColumnProperty.Null),
                
                new Column("LastUpdatedBy", DbType.Int32, ColumnProperty.NotNull),
                new Column("LastUpdatedOn", DbType.DateTime, ColumnProperty.NotNull)
            };
        }

        

    }

}
