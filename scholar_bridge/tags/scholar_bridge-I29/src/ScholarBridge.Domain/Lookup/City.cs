﻿using ScholarBridge.Domain.Location;

namespace ScholarBridge.Domain.Lookup
{
    public class City : LookupBase
    {
        public virtual State State { get; set; }
    }
}
