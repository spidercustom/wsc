using NHibernate.Criterion;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class ArticleDAL : AbstractDAL<Article>, IArticleDAL
    {
        public Article FindById(int id)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Article>();
        }


        public Article Save(Article article)
        {
            return article.Id < 1 ?
                Insert(article) :
                Update(article);
             
        }

        
    }
}