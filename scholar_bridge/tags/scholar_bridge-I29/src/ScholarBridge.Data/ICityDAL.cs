﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ICityDAL : IGenericLookupDAL<City>
    {
        IList<City> FindByState(string stateAbbreviation);
    }
}
