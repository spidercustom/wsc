using System.Web.UI.WebControls;
using NUnit.Framework;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Tests
{

    [TestFixture]
    public class RepeaterItemExtensionsTests
    {

        [Test]
        public void is_row_true_for_item_and_alt()
        {
            var item = new RepeaterItem(0, ListItemType.Item);
            Assert.IsTrue(item.IsRow());

            var alt = new RepeaterItem(0, ListItemType.AlternatingItem);
            Assert.IsTrue(alt.IsRow());
        }

        [Test]
        public void is_row_false_for_others() 
        {
            var header = new RepeaterItem(0, ListItemType.Header);
            Assert.IsFalse(header.IsRow());

            var footer = new RepeaterItem(0, ListItemType.Footer);
            Assert.IsFalse(footer.IsRow());
        }
    }
}