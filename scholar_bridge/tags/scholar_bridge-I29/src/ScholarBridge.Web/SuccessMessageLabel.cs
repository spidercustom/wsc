using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web
{
    [ToolboxData("<{0}:SuccessMessageLabel runat=\"server\" />")]
    public class SuccessMessageLabel : Label
    {
        private const string SESSION_KEY = "SB_WEB_SUCCESS_MSG";

        public SuccessMessageLabel()
        {
            CloseImageUrl = "~/images/close.png";
        }

        public static void SetMessage(string message)
        {
            HttpContext.Current.Session[SESSION_KEY] = message;
        }

        [Bindable(true),
         Category("Behavior"),
         Description("Whether to retain the value across displays"),
         DefaultValue(false)]
        public bool Retain { get; set; }

        [Bindable(true), Category("Appearance"), DefaultValue("~/images/close.png")]
        public string CloseImageUrl { get; set; }

        public override string Text
        {
            get { return (string) HttpContext.Current.Session[SESSION_KEY]; }
            set { HttpContext.Current.Session[SESSION_KEY] = value; }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.BeginRender();
            if (! string.IsNullOrEmpty(Text) && ! this.Page.IsPostBack)
            {
                writer.InnerWriter.Write("<div id=\"{0}\" class=\"SuccessfullOperationConatiner\">", ClientID);
                writer.InnerWriter.Write("<div class=\"SuccessfullOperationText\">");
                writer.WriteEncodedText(Text);
                writer.InnerWriter.WriteLine("</div>");
                //writer.InnerWriter.Write("<img src=\"{0}\" alt=\"close\" class=\"closeBox\"/>", ResolveUrl(CloseImageUrl));
                writer.InnerWriter.WriteLine("</div>");

                if (!Retain)
                {
                    HttpContext.Current.Session.Remove(SESSION_KEY);
                }
            }
            writer.EndRender();
        }
    }
}