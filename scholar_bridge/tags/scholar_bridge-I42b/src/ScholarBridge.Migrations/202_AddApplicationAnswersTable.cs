using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(202)]
    public class AddApplicationQuestionsTable : AddTableBase
    {
        public const string TABLE_NAME = "SBApplicationQuestionAnswer";
        public const string PRIMARY_KEY_COLUMN = "SBApplicationQuestionAnswerID";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("ApplicationID", DbType.Int32, ColumnProperty.NotNull),
                           new Column("ScholarshipQuestionID", DbType.Int32, ColumnProperty.NotNull),
                           new Column("AnswerText", DbType.String, 200, ColumnProperty.NotNull),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())")
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new[]
                       {
                           CreateForeignKey("ApplicationID", "SBApplication", "SBApplicationID"),
                           CreateForeignKey("LastUpdateBy", "SBUser", "SBUserID"),
                           CreateForeignKey("ScholarshipQuestionID", "SBScholarshipQuestion", "SBScholarshipQuestionID")
                       };
        }
    }
}