using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(264)]
    public class AddRenewReapplyToScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";
        
        private const string RENEWABLE = "Renewable";
        private const string REAPPLY = "Reapply";
        private const string RENEWAWBLE_GUIDELINES = "RenewableGuidelines";
        private const string DONOR_PROFILE = "DonorProfile";


        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, RENEWABLE, DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, REAPPLY, DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, RENEWAWBLE_GUIDELINES, DbType.String,1000 , ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, DONOR_PROFILE, DbType.String, 1000, ColumnProperty.Null);
             
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, RENEWABLE);
            Database.RemoveColumn(TABLE_NAME, REAPPLY);
            Database.RemoveColumn(TABLE_NAME, RENEWAWBLE_GUIDELINES);
            Database.RemoveColumn(TABLE_NAME, DONOR_PROFILE);

        }
    }
}