﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(118)]
    public class AddStageToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("Stage", DbType.String, 50, ColumnProperty.NotNull, "'None'"));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "Stage");
        }
    }
}