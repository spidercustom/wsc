using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class MatchDAL : AbstractDAL<Match>, IMatchDAL
    {
        private static readonly SimpleExpression NOT_DELETED = Restrictions.Eq("IsDeleted", false);

        public IList<Match> FindAll(Application application)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))
                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus status)
        {
            return FindAll(seeker, new[] {status});
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.In("MatchStatus", status))
                .List<Match>();
        }

    	public IList<Match> FindAll(Scholarship scholarship, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.In("MatchStatus", status))
                .List<Match>();
        }

		public IList<Match> FindAllWithApplications(Scholarship scholarship, MatchStatus[] statuses)
		{
			return CreateCriteria()
				.Add(NOT_DELETED)
				.Add(Restrictions.IsNotNull("Application"))
				.Add(Restrictions.Eq("Scholarship", scholarship))
				.Add(Restrictions.In("MatchStatus", statuses))
				.SetFetchMode("Application", FetchMode.Join)
				.List<Match>();
		}

		public IList<Match> FindAllCurrentMatches(Seeker seeker)
		{
			return FindAllCurrentMatches(seeker, false);
		}

		public IList<Match> FindAllCurrentMatches(Seeker seeker, bool excludeMatchesWithApplications)
		{
			var criteria = CreateCriteria()
				.Add(NOT_DELETED)
				.Add(Restrictions.Eq("Seeker", seeker))
				.Add(Restrictions.Eq("AddedViaMatch", true))
				.AddOrder(Order.Desc("Rank"));

			if (excludeMatchesWithApplications)
				criteria.Add(Restrictions.IsNull("Application"));

			return criteria.List<Match>();
		}

		public Match Find(Seeker seeker, Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .UniqueResult<Match>();
        }

        public Match Find(Seeker seeker, int scholarshipId)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship.Id", scholarshipId))
                .UniqueResult<Match>();
        }

        public void RemoveZeroedMatches(Seeker seeker)
        {
            foreach (var match in CreateCriteria()
                .Add(Restrictions.Eq("SeekerMinimumCriteriaCount", 0))
                .Add(Restrictions.Not(Restrictions.Eq("ScholarshipMinimumCriteriaCount", 0)))
                .Add(Restrictions.Not(Restrictions.Eq("MatchStatus", MatchStatus.Saved)))
                .CreateCriteria("Seeker").Add(Restrictions.Eq("Id", seeker.Id)).List<Match>())
            {
                Delete(match);
            }
        }

        public void UpdateMatches(Seeker seeker)
        {
            var sqlQuery = Session.GetNamedQuery("findMatchesForSeeker");
            sqlQuery.SetEntity("seekerId", seeker);
            var results = sqlQuery.List<Object[]>();

			DeleteDroppedMatches(results, seeker);

			foreach (var r in results)
            {
                var scholarshipId = (int) r[0];
                var scholarship = (Scholarship)Session.Get(typeof(Scholarship), scholarshipId);

                CreateOrUpdateMatch(seeker, scholarship, r);
            }

            RemoveZeroedMatches(seeker);
		}

		public void UpdateMatches(Scholarship newScholarship)
        {
            var sqlQuery = Session.GetNamedQuery("findMatchesForScholarship");
            sqlQuery.SetEntity("scholarshipId", newScholarship);
            var results = sqlQuery.List<Object[]>();

			foreach (var r in results)
            {
                var scholarshipId = (int)r[0];
                var seekerId = (int)r[1];
                var scholarship = (Scholarship)Session.Get(typeof(Scholarship), scholarshipId);
                var seeker = (Seeker)Session.Get(typeof(Seeker), seekerId);

                CreateOrUpdateMatch(seeker, scholarship, r);
            }
        }
		/// <summary>
		/// delete any rows in SBMatch that are not current matches and/or scholarships of interest
		/// </summary>
		/// <param name="newMatches"></param>
		/// <param name="seeker"></param>
		private void DeleteDroppedMatches(IEnumerable<object[]> newMatches, Seeker seeker)
		{
			IList<Match> currentMatches = FindAll(seeker);
			foreach (var match in currentMatches)
			{
				if (match.AddedViaMatch)
				{
					bool matchFound = false;
					foreach (var newMatch in newMatches)
					{
						if (match.Seeker.Id == (int)newMatch[1] &&
							match.Scholarship.Id == (int)newMatch[0])
						{
							matchFound = true;
							break;
						}
					}
					if (!matchFound)
					{
						if (match.MatchStatus == MatchStatus.Saved || match.MatchStatus == MatchStatus.Submitted)
						{
							match.AddedViaMatch = false;
							Session.Update(match);
						}
						else
						{
							Session.Delete(match);
						}
					}
				}
				else
				{
					if (match.MatchStatus == MatchStatus.New)
					{
						Session.Delete(match);
					}
				}
			}
		}

		#region Method Overrides
		public new Match Insert(Match match)
		{
			match.ComputeRank();
			return base.Insert(match);
		}
		public new Match Update(Match match)
		{
			match.ComputeRank();
			return base.Update(match);
		}
		#endregion

		private void CreateOrUpdateMatch(Seeker seeker, Scholarship scholarship, object[] r)
		{
            var seekerPrefCount = (int)r[2];
            var seekerMinCount = (int)r[3];
            var scholPrefCount = (int)r[4];
            var scholMinCount = (int)r[5];

            var match = Find(seeker, scholarship);
            if (null == match)
            {
                match = new Match
                {
                    Seeker = seeker,
                    Scholarship = scholarship
                };
            }

			match.IsDeleted = false;
			match.AddedViaMatch = true;
            match.SeekerMinimumCriteriaCount = seekerMinCount;
            match.SeekerPreferredCriteriaCount = seekerPrefCount;
            match.ScholarshipMinimumCriteriaCount = scholMinCount;
            match.ScholarshipPreferredCriteriaCount = scholPrefCount;
            match.LastUpdate = new ActivityStamp(seeker.User);
			match.ComputeRank();
            Session.SaveOrUpdate(match);
        }
		#region Test Methods
		public IList<BoolMatchView> GetBoolMatchViewRowsForSeekerAndScholarship(Seeker seeker, Scholarship scholarship)
		{
			var sqlQuery = Session.GetNamedQuery("findBoolMatchesForSeekerAndScholarship");
			sqlQuery.SetEntity("SBSeekerId", seeker);
			sqlQuery.SetEntity("SBScholarshipId", scholarship);
			var results = sqlQuery.List<Object[]>();
			List<BoolMatchView> matches = new List<BoolMatchView>(results.Count);
			foreach (var r in results)
			{
				BoolMatchView matchViewRow = new BoolMatchView()
				{
					SBScholarshipId = (int)r[0],
					AcademicYear = (Int16)r[1],
					SBUsageTypeIndex = (int)r[2],
					criterion = (string)r[3],
					selected = (bool)r[4],
					SBSeekerId = (int)r[5],
					seekerValue = (bool)r[6]
				};
				matches.Add(matchViewRow);
			}
			return matches;
		}

		public IList<BoolMatchView> GetBoolMatchViewRowsForScholarship(Scholarship scholarship)
		{
			var sqlQuery = Session.GetNamedQuery("findBoolMatchesForScholarship");
			sqlQuery.SetEntity("SBScholarshipId", scholarship);
			var results = sqlQuery.List<Object[]>();
			List<BoolMatchView> matches = new List<BoolMatchView>(results.Count);
			foreach (var r in results)
			{
				BoolMatchView matchViewRow = new BoolMatchView()
				{
					SBScholarshipId = (int)r[0],
					AcademicYear = (Int16)r[1],
					SBUsageTypeIndex = (int)r[2],
					criterion = (string)r[3],
					selected = (bool)r[4],
					SBSeekerId = (int)r[5],
					seekerValue = (bool)r[6]
				};
				matches.Add(matchViewRow);
			}
			return matches;
		}
		#endregion


    }

	public class BoolMatchView
	{
		public int SBScholarshipId { get; set; }
		public int AcademicYear  { get; set; }
		public int SBUsageTypeIndex { get; set; }
		public string criterion { get; set; }
		public bool selected { get; set; }
		public int SBSeekerId { get; set; }
		public bool seekerValue { get; set; }
	}

}
