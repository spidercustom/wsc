using System;
using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ApplicationServiceTests
    {
        private MockRepository mocks;
        private ApplicationService applicationService;
        private IApplicationDAL applicationDal;
        private IMatchService matchService;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            applicationDal = mocks.StrictMock<IApplicationDAL>();
            matchService = mocks.StrictMock<IMatchService>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();

            applicationService = new ApplicationService
                                {
                                    ApplicationDAL = applicationDal,
                                    MatchService = matchService,
                                    MessagingService = messagingService,
                                    TemplateParametersService = templateParametersService
                                };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(applicationDal);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void submit_throws_exception_if_null_passed()
        {
            applicationService.SubmitApplication(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_new_throws_exception_if_null_passed()
        {
            applicationService.SaveNew(null);
            Assert.Fail();
        }


        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_throws_exception_if_null_passed()
        {
            applicationService.Save(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void update_throws_exception_if_null_passed()
        {
            applicationService.Update(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void finalist_throws_exception_if_null_passed()
        {
            applicationService.Finalist(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void findbyseekerandscholarship_throws_exception_if_null_seeker_passed()
        {
            applicationService.FindBySeekerandScholarship(null, new Scholarship());
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void findbyseekerandscholarship_throws_exception_if_null_scholarship_passed()
        {
            applicationService.FindBySeekerandScholarship(new Seeker(), null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void notfinalist_throws_exception_if_null_passed()
        {
            applicationService.NotFinalist(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void count_throws_exception_if_null_passed()
        {
            applicationService.CountAllSubmitted(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void countSubmittedbyseeker_throws_exception_if_null_passed()
        {
            applicationService.CountAllSubmittedBySeeker(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_all_finalistsr_throws_exception_if_null_passed()
        {
            applicationService.FindAllFinalists(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_all_submitted_throws_exception_if_null_passed()
        {
            applicationService.FindAllSubmitted(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_all_submitted_by_name_throws_exception_if_null_passed()
        {
            applicationService.FindAllSubmitted(null, "foo");
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void award_scholarship_throws_exception_if_null_passed()
        {
            applicationService.AwardScholarship(null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void notaward_scholarship_throws_exception_if_null_passed()
        {
            applicationService.NotAwardScholarship(null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void offer_scholarship_throws_exception_if_null_passed()
        {
            applicationService.OfferScholarship(null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void delete_throws_exception_if_null_passed()
        {
            applicationService.Delete(null);
            Assert.Fail();
        }
        
        [Test]
        public void save_new_calls_insert()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };
            Expect.Call(applicationDal.Insert(app)).Return(app);
            Expect.Call(() => matchService.ApplyForMatch(app.Seeker, app.Scholarship.Id, app));
            mocks.ReplayAll();

            applicationService.SaveNew(app);

            mocks.VerifyAll();
        }

        [Test]
        public void save_delegates_to_dal()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };
            Expect.Call(applicationDal.Save(app)).Return(app);
            mocks.ReplayAll();

            applicationService.Save(app);

            mocks.VerifyAll();
        }
        
        [Test]
        public void finalist_when_not_finalist_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.Finalist(app);

            mocks.VerifyAll();
            Assert.IsTrue(app.Finalist);
        }

        [Test]
        public void finalist_by_id_when_not_finalist_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };
            Expect.Call(applicationDal.FindById(1)).Return(app);
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.Finalist(1);

            mocks.VerifyAll();
            Assert.IsTrue(app.Finalist);
        }

        [Test]
        public void finalist_when_finalist_does_not_update_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
                Finalist = true
            };
            mocks.ReplayAll();

            applicationService.Finalist(app);

            mocks.VerifyAll();
            Assert.IsTrue(app.Finalist);
        }

        [Test]
        public void notfinalist_when_finalist_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
                Finalist = true
            };
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.NotFinalist(app);

            mocks.VerifyAll();
            Assert.IsFalse(app.Finalist);
        }

        [Test]
        public void not_finalist_by_id_when_not_finalist_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
                Finalist = true
            };
            Expect.Call(applicationDal.FindById(1)).Return(app);
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.NotFinalist(1);

            mocks.VerifyAll();
            Assert.IsFalse(app.Finalist);
        }

        [Test]
        public void not_finalist_when_not_finalist_does_not_update_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            mocks.ReplayAll();

            applicationService.NotFinalist(app);

            mocks.VerifyAll();
            Assert.IsFalse(app.Finalist);
        }

        [Test]
        public void count_delegates_to_dal()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            Expect.Call(applicationDal.CountAllSubmitted(app.Scholarship)).Return(2);
            mocks.ReplayAll();

            applicationService.CountAllSubmitted(app.Scholarship);

            mocks.VerifyAll();
        }

        [Test]
        public void update_delegates_to_dal()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.Update(app);

            mocks.VerifyAll();
        }

        [Test]
        public void delete_delegates_to_dal()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            Expect.Call(() => matchService.DisconnectApplication(app));
            Expect.Call(() => applicationDal.Delete(app));
            mocks.ReplayAll();

            applicationService.Delete(app);

            mocks.VerifyAll();
        }

        [Test]
        public void countSubmittedBySeeker_delegates_to_dal()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            Expect.Call(applicationDal.CountAllSubmittedBySeeker(app.Seeker)).Return(2);
            mocks.ReplayAll();

            applicationService.CountAllSubmittedBySeeker(app.Seeker);

            mocks.VerifyAll();
        }

        [Test]
        public void find_all_finalists_delegates_to_dal()
        {

            var s = new Scholarship();
            var apps = new List<Application>();
            Expect.Call(applicationDal.FindAllFinalists(s)).Return(apps);
            mocks.ReplayAll();

            applicationService.FindAllFinalists(s);

            mocks.VerifyAll();
        }

        [Test]
        public void award_scholarship_sends_emails_and_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker { User = new User() },
                Scholarship = new Scholarship { Provider = new Provider() }
            };
            var awarder = new User();
            Expect.Call(applicationDal.Update(app)).Return(app);
            Expect.Call(() => templateParametersService.AwardScholarship(Arg<Application>.Is.Equal(app), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
            mocks.ReplayAll();

            applicationService.AwardScholarship(app, awarder);

            mocks.VerifyAll();
            Assert.AreEqual(AwardStatuses.Awarded, app.AwardStatus);
        }

        [Test]
        public void submit_application_sends_emails_and_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker { User = new User() },
                Scholarship = new Scholarship { Provider = new Provider(),ApplicationDueDate=DateTime.Today }
                
            };
            
            Expect.Call(applicationDal.Update(app)).Return(app);
            Expect.Call(() => templateParametersService.ApplicationSubmisionSuccess (Arg<Application>.Is.Equal(app), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SaveNewSentMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => templateParametersService.ApplicationAcknowledgement(Arg<Application>.Is.Equal(app), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
          
            mocks.ReplayAll();

            applicationService.SubmitApplication(app);

            mocks.VerifyAll();
            Assert.AreEqual(ApplicationStages.Submitted, app.Stage);
            Assert.IsTrue(app.SubmittedDate.HasValue);
        }

        
        [Test]
        public void offer_scholarship_sends_emails_and_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker { User = new User() },
                Scholarship = new Scholarship { Provider = new Provider() }
            };
            var awarder = new User();
            Expect.Call(applicationDal.Update(app)).Return(app);
            Expect.Call(() => templateParametersService.OfferScholarship(Arg<Application>.Is.Equal(app), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
            mocks.ReplayAll();

            applicationService.OfferScholarship(app, awarder);

            mocks.VerifyAll();
            Assert.AreEqual(AwardStatuses.Offered, app.AwardStatus);
        }

        [Test]
        public void notaward_scholarship_updates_application()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            var awarder = new User();
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.NotAwardScholarship(app, awarder);

            mocks.VerifyAll();
            Assert.AreEqual(AwardStatuses.NotAwarded, app.AwardStatus);
        }
    }
}