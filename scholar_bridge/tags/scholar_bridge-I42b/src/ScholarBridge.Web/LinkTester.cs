﻿using System.Net;

namespace ScholarBridge.Web
{
    public class LinkTester
    {
        public static bool IsLinkValid(string url)
        {
            string exceptionMessage;
            return IsLinkValid(url, out exceptionMessage);
        }

        public static bool IsLinkValid(string url, out string webException)
        {
            webException = string.Empty;
            var browser = new WebClient();
            try
            {
                browser.DownloadData(url);
                return true;
            }
            catch (WebException we)
            {
                webException = we.Message;
                return false;
            }
        }
    }
}
