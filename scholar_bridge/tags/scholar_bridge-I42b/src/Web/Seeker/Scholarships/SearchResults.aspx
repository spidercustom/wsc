﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SearchResults.aspx.cs"
    Inherits="ScholarBridge.Web.Seeker.Scholarships.SearchResults" Title="Seeker | Scholarship SearchResults" %>

<%@ Register Src="~/Common/ScholarshipSearchResults.ascx" TagName="ScholarshipSearchResults"
    TagPrefix="sb" %>
<%@ Register Src="~/Common/SeekerProfileProgress.ascx" TagName="SeekerProfileProgress"
    TagPrefix="sb" %>
<%@ Register Src="~/Common/ScholarshipSearchResultsAnonymous.ascx" TagName="ScholarshipSearchResultsAnonymous"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div>
        <asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px"
            Height="15px" runat="server" /></div>
    <div>
        <asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px"
            Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <!--Left floated content area starts here-->
    <img src="<%= ResolveUrl("~/images/PgTitle_ScholarshipResults.gif") %>" width="311px"
        height="54px">
    <asp:panel runat="server" ID="matchesFoundPanel">
        <h3 style="margin-bottom: 20px;">
            The following scholarships matched your search for <span style="color: Blue;">
                <asp:Label ID="CriteriaLabel" Text="aa" runat="Server" />
            </span>.</h3>
    </asp:panel>
    <asp:panel runat="server" ID="noMachPanel">
        <h3 style="margin-bottom: 20px;">
            You must enter a non-blank search value before searching. Please try again.</h3>
    </asp:panel>
    <asp:LoginView ID="loginView2" runat="server">
        <AnonymousTemplate>
            <p>
                To see full details on these scholarships, as well as get matched to scholarships
                you may qualify for, create a profile. To get started creating your profile, <a href="<%= ResolveUrl("~/Seeker/Register.aspx") %>">
                    click here</a></p>
        </AnonymousTemplate>
    </asp:LoginView>
    <sb:ScholarshipSearchResults ID="ScholarshipSearchResults1" runat="server" LinkTo="~/Seeker/Matches/Create.aspx" />
    <uc1:ScholarshipSearchResultsAnonymous ID="ScholarshipSearchResultsAnonymous1" runat="server" />
</asp:Content>
