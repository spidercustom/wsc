﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Web.Seeker.Applications
{
    public partial class OnlineApplication : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IApplicationService ApplicationService { get; set; }
         
        private const string APPLICATIONS_PAGEURL = "~/Seeker/Scholarships";
        
        
        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["sid"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter aid");
                return scholarshipId;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
                UserContext.EnsureSeekerIsInContext();
        }
        protected void saveBtn_Click(object sender, EventArgs e)
        {
            var scholarship = ScholarshipService.GetById(ScholarshipId);

            var application =ApplicationService.FindBySeekerandScholarship(UserContext.CurrentSeeker, scholarship);
            if (application ==null )
            {
                 
					//Create New one
					 
                        application = new Application(ApplicationType.External)
						{
							//intialize default values here
							Seeker = UserContext.CurrentSeeker,
							Scholarship = scholarship,

						};
                        application.CopyFromSeekerandScholarship();
                        application.SubmittedDate = null;
                       
						application.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
						ApplicationService.SaveNew(application);
                       
			}
            //Response.Redirect(APPLICATIONS_PAGEURL,false);
            PopupHelper.OpenURL(scholarship.OnlineApplicationUrl);
           
            
        }
        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect(APPLICATIONS_PAGEURL);
        }
    }
}
