﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
    Inherits="ScholarBridge.Web.Seeker.Default" Title="Seeker" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<%@ Register Src="~/Common/ScholarshipSearchBox.ascx" TagName="ScholarshipSearchBox"
    TagPrefix="sb" %>
<%@ Register Src="~/Common/SeekerProfileProgress.ascx" TagName="SeekerProfileProgress"
    TagPrefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div>
        <asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px"
            Height="15px" runat="server" /></div>
    <div>
        <asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px"
            Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <!--Left floated content area starts here-->
    <div id="HomeContentLeft">
        <img src="<%= ResolveUrl("~/images/PgTitle_SmarterScholarshipMatches.gif") %>" width="340px"
            height="54px"/>
        <div style="margin-bottom:1.5em; font-style:italic; font-size:1.5em; font-weight:bold;">Now that you're logged in we can begin helping you find the scholarships best suited for you. Simply complete your profile and let us do the rest, or if you have matched scholarships, review them in My Matches.</div>

        <!-- <P class="HighlightedTextMain">Now that you’re logged in we can begin helping you find the scholarships best suited for you. Simply complete your profile and let us do the rest, or if you have, review your scholarships in My Matches. </P> -->
        <a href="<%= ResolveUrl("~/Seeker/Profile") %>">
            <img src="<%= ResolveUrl("~/images/Btn_GotoMyProfile.gif") %>" width="144px" height="32px"/></a>
    </div>
    <!--Left floated content area ends here-->
    <!--Right Floated content area starts here-->
    <div id="HomeContentRight">
        <sb:SeekerProfileProgress ID="ProfileProgess" runat="server" ShowGotoProfile="false" />
    </div>
    <!--Right Floated content area ends here-->
    <br />
    <br />
    <div id="Clear">
    </div>
    <hr>
    <div style='clear: both;'>
    </div>
    <!--The Three column starts here-->
    <div id="BoxWrapper">
        <div id="LeftBottomBox">
            <a href="<%= ResolveUrl("~/Seeker/Profile") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox01_DontForget.gif") %>" width="262px"
                    height="51px"/>
                <img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox01.gif") %>" width="237px"
                    height="86px"/>
            </a>
            <p> In order for us to give you the best possible scholarship matches, please complete your profile.</p>
            <p class="IcoBoxArrow"  style="margin-top:3.5em;">
                <a href="<%= ResolveUrl("~/Seeker/Profile") %>">
                    <img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;Go
                    to My Profile</a></p>
        </div>
        <div id="CenterBottomBox">
            <a href="<%= ResolveUrl("~/Seeker/Matches") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox02_WhatsNew.gif") %>" width="262px" height="51px"/>
                <img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox02.gif") %>" width="237px"
                    height="86px"/>
            </a>
            <p>Your scholarship matches are ranked based on the number of eligibility criteria that match your profile. Start reviewing scholarships you are most likely to qualify for.</p>
            <p class="IcoBoxArrow">
                <a href="<%= ResolveUrl("~/Seeker/Matches") %>">
                    <img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;Go
                    to My Matches</a></p>
        </div>
        <div id="RightBottomBox">
            <a href="<%= ResolveUrl("~/Seeker/Scholarships") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox03_ApplyToday.gif") %>" width="262px"
                    height="51px"/>
                <img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox03.gif") %>" width="237px"
                    height="86px"/>
            </a>
            <p>Save scholarships to your scholarships of interest and we will send you reminders of upcoming application due dates!</p>
            <p class="IcoBoxArrow" style="margin-top:2em;">
                <a href="<%= ResolveUrl("~/Seeker/Scholarships") %>">
                    <img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;Go
                    to My Applications</a></p>
        </div>
    </div>
    <br />
</asp:Content>
