﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class Basics : WizardStepUserControlBase<Application>
    {
		public IApplicationService ApplicationService { get; set; }
		public IStateDAL StateService { get; set; }
        public IUserContext UserContext { get; set; }
        public LookupDAL<City> CityDAL { get; set; }
        public LookupDAL<County> CountyDAL { get; set; }
        private Application _applicationInContext;

		Application ApplicationInContext
		{
			get
			{
				if (_applicationInContext == null)
					_applicationInContext = Container.GetDomainObject();
				return _applicationInContext;
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			if (ApplicationInContext == null)
				throw new InvalidOperationException("There is no application in context");

            dobRangeValid.MaximumValue = DateTime.Today.ToShortDateString();
			if (!Page.IsPostBack)
			{
				PopulateScreen();
			    DisableNameBoxesIfActive();
			}
            SetWashigtonStateDropownEvent();
            SetStateControls();
			SetCountyAndCityValidators();
		}

        private void DisableNameBoxesIfActive()
        {
            var application = ApplicationInContext;
            if (null != application && application.Stage == ApplicationStages.Submitted)
            {
                FirstNameBox.Enabled = false;
                MidNameBox.Enabled = false;
                LastNameBox.Enabled = false;
            }
        }

		private void PopulateScreen()
		{
		    StateDropDown.DataSource = StateService.FindAll();
		    StateDropDown.DataTextField = "Name";
		    StateDropDown.DataValueField = "Abbreviation";
		    StateDropDown.DataBind();
		    StateDropDown.Items.Insert(0, new ListItem("- Select One -", ""));

            CityDropDown.DataSource = CityDAL.FindAll();//.FindByState("WA");
            CityDropDown.DataTextField = "Name";
            CityDropDown.DataValueField = "Id";
            CityDropDown.DataBind();

            CountyDropDown.DataSource = CountyDAL.FindAll(); // .FindByState("WA");
            CountyDropDown.DataTextField = "Name";
            CountyDropDown.DataValueField = "Id";
            CountyDropDown.DataBind();
		    if (null != ApplicationInContext.ApplicantName)
		    {
		    
		        LastNameBox.Text = ApplicationInContext.ApplicantName.LastName;
		        MidNameBox.Text = ApplicationInContext.ApplicantName.MiddleName;
		        FirstNameBox.Text = ApplicationInContext.ApplicantName.FirstName;
		    }

	        EmailControl.Text = ApplicationInContext.Email;
             
			AddressLine1Box.Text = ApplicationInContext.Address.Street;
			AddressLine2Box.Text = ApplicationInContext.Address.Street2;
            if (null != ApplicationInContext.Address.State)
            {
                StateDropDown.SelectedValue = ApplicationInContext.Address.State.Abbreviation;
                if (ApplicationInContext.Address.State.Abbreviation == State.WASHINGTON_STATE_ID)
                {
                    CityDropDown.SelectedValue = ApplicationInContext.Address.City.Id.ToString();
                    CountyDropDown.SelectedValue = ApplicationInContext.County.Id.ToString();
               }
                else
                {
                    CityTextBox.Text = ApplicationInContext.AddressCity;
                    CountyTextBox.Text = ApplicationInContext.AddressCounty;
                }
            }

            
            
            ZipBox.Text = ApplicationInContext.Address.PostalCode;
            
            PhoneBox.Text = ApplicationInContext.Phone == null ? "" : ApplicationInContext.Phone.FormattedPhoneNumber;
            MobilePhoneBox.Text = ApplicationInContext.MobilePhone == null ? "" : ApplicationInContext.MobilePhone.FormattedPhoneNumber;

            if (ApplicationInContext.DateOfBirth.HasValue)
		        DobControl.Text = ApplicationInContext.DateOfBirth.Value.ToString("MM/dd/yyyy");

			ReligionCheckboxList.SelectedValues = ApplicationInContext.Religions.Cast<ILookup>().ToList();
			EthnicityControl.SelectedValues = ApplicationInContext.Ethnicities.Cast<ILookup>().ToList();
			OtherReligion.Text = ApplicationInContext.ReligionOther;
			GenderButtonList.SelectedIndex = (ApplicationInContext.Gender.Equals(Genders.Male) ? 0 : (ApplicationInContext.Gender.Equals(Genders.Female) ? 1 : -1));
			OtherEthnicity.Text = ApplicationInContext.EthnicityOther;
		}
		private void SetCountyAndCityValidators()
		{
			if (StateDropDown.SelectedValue == State.WASHINGTON_STATE_ID)
			{
				cityRequired.Enabled = false;
				countyRequired.Enabled = false;
			}
			else
			{
				cityRequired.Enabled = true;
				countyRequired.Enabled = true;
			}
		}

		public void SetStateControls()
        {
            var script = "$('#" + StateDropDown.ClientID + "').change();";
            var scriptname = "RunSetWashigtonStateDropownEvent_scripts";
            var page = Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
        }
        private void SetWashigtonStateDropownEvent()
        {
            var scriptname = "SetWashigtonStateDropownEvent_scripts";


            var script = "$('#" + StateDropDown.ClientID + "').change(function(e) { return CheckWashingtonState('" + StateDropDown.ClientID + "', '" + CityDropDown.ClientID + "','" + CountyDropDown.ClientID + "','" + cityTextPanel.ClientID + "' ,'" + countyTextPanel.ClientID + "'); });";

            var page = Page; // HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
        }
		public override void PopulateObjects()
		{
            ApplicationInContext.ApplicantName.LastName = LastNameBox.Text;
            ApplicationInContext.ApplicantName.MiddleName = MidNameBox.Text;
            ApplicationInContext.ApplicantName.FirstName = FirstNameBox.Text;
             
			ApplicationInContext.Address.Street = AddressLine1Box.Text;
			ApplicationInContext.Address.State = StateDropDown.SelectedValue == string.Empty 
															? null 
															: StateService.FindByAbbreviation(StateDropDown.SelectedValue);
			ApplicationInContext.Address.Street2 = AddressLine2Box.Text;
            if (null != ApplicationInContext.Address.State)
            {
                if (ApplicationInContext.Address.State.Abbreviation == State.WASHINGTON_STATE_ID)
                {

                    ApplicationInContext.Address.City = CityDAL.FindById(Convert.ToInt32(CityDropDown.SelectedValue));
                    ApplicationInContext.County = CountyDAL.FindById(Convert.ToInt32(CountyDropDown.SelectedValue));
                }
                else
                {
                    ApplicationInContext.AddressCity = CityTextBox.Text;
                    ApplicationInContext.AddressCounty = CountyTextBox.Text;
                }
            }
            
			ApplicationInContext.Address.PostalCode = ZipBox.Text;
            ApplicationInContext.Phone = new PhoneNumber(PhoneBox.Text);
            ApplicationInContext.MobilePhone = new PhoneNumber(MobilePhoneBox.Text);

		    DateTime dob;
		    if (DateTime.TryParse(DobControl.Text, out dob))
		    {
		        ApplicationInContext.DateOfBirth = dob;
		    }
            else
		    {
                ApplicationInContext.DateOfBirth = null;
		    }

			PopulateList(EthnicityControl, ApplicationInContext.Ethnicities);
			PopulateList(ReligionCheckboxList, ApplicationInContext.Religions);
			ApplicationInContext.Gender = GenderButtonList.SelectedValue == string.Empty ? Genders.Unspecified : (Genders) int.Parse(GenderButtonList.SelectedValue);
			ApplicationInContext.EthnicityOther = OtherEthnicity.Text;
			ApplicationInContext.ReligionOther = OtherReligion.Text;
            ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
		}

		private static void PopulateList<T>(LookupItemCheckboxList checkboxList, IList<T> list)
		{
			checkboxList.PopulateListFromSelectedValues(list);
		}

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
			PopulateObjects();
			ApplicationService.Update(ApplicationInContext);
		}
		#endregion
	}
}