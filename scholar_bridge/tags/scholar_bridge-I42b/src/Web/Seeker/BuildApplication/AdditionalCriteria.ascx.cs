﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
    public partial class AdditionalCriteria : WizardStepUserControlBase<Application>
	{
		#region DI Properties
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }
		#endregion

		#region Properties
		
        private Application ApplicationInContext
		{
            get { return Container.GetDomainObject(); }
		}

		#endregion

		#region Page Events
		
		public void Page_Load(object sender, EventArgs e)
        {
            if (ApplicationInContext == null)
                throw new InvalidOperationException("There is no Application in context");

            if (!IsPostBack)
            {
                PopulateScreen();
            }
		}

		#endregion

		#region Private Methods

		

		private void PopulateScreen()
        {
            AdditionalRequirementsList.Requirements = ApplicationInContext.Scholarship.AdditionalRequirements;
           
		    QAEditor.QuestionAnswers = ApplicationInContext.QuestionAnswers;

			BindScholarshipFiles();

            BindAttachedFiles();
        }

		private void BindScholarshipFiles()
		{
			if (ApplicationInContext.Scholarship.Attachments.Count > 0)
			{
				scholarshipFilesPanel.Visible = true;
				scholarshipFiles.DataSource = ApplicationInContext.Scholarship.Attachments;
				scholarshipFiles.DataBind();
			}
			else
			{
				scholarshipFilesPanel.Visible = false;
			}
		}


		private void BindAttachedFiles()
        {
            attachedFiles.DataSource = ApplicationInContext.Attachments;
            attachedFiles.DataBind();
        }

        public override void PopulateObjects()
        {
            QAEditor.GetUpdatedAnswers(ApplicationInContext);
             if (ApplicationInContext.Stage < ApplicationStages.NotActivated)
                 ApplicationInContext.Stage = ApplicationStages.NotActivated;

        }

		#endregion

		#region Wizard Control Override Methods
		public override void Save()
        {
            PopulateObjects();
            ApplicationService.Update(ApplicationInContext);
        }

        public override bool ValidateStep()
        {
            return true;
		}
        #endregion

		#region control event handlers
		

        protected void UploadFile_Click(object sender, EventArgs e)
        {
            if (null != AttachFile.PostedFile)
            {
                //check if a valid filename is being uploaded
                if (String.IsNullOrEmpty(AttachFile.PostedFile.FileName))
                {
                    AttachmentCommentsValidator.IsValid = false;
                    AttachmentCommentsValidator.Text = "Please choose a valid file to attach and try again.";
                    return;
                }

                var attachment = CreateAttachment();
                try
                {
                    AttachFile.PostedFile.SaveAs(attachment.GetFullPath(ConfigHelper.GetAttachmentsDirectory()));
                    ApplicationInContext.Attachments.Add(attachment);
                    ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ApplicationService.Update(ApplicationInContext);
                }
                catch (Exception)
                {
                    attachment.RemoveFile(ConfigHelper.GetAttachmentsDirectory());
                    AttachmentCommentsValidator.IsValid = false;
                    AttachmentCommentsValidator.Text = "Could not upload the file. Please try again.";
                    // FIXME: Display error to user
                    if (ApplicationInContext.Attachments.Contains(attachment))
                        ApplicationInContext.Attachments.RemoveAt(ApplicationInContext.Attachments.Count - 1);
                }
                AttachmentComments.Text = null;
                BindAttachedFiles();
            }
        }

        private Attachment CreateAttachment()
        {
            string mimeType = AttachFile.PostedFile.ContentType;
            if (String.IsNullOrEmpty(mimeType))
                mimeType = "application/octet-stream";
            var attachment = new Attachment
                                 {
                                     Name = Path.GetFileName(AttachFile.PostedFile.FileName),
                                     Comment = AttachmentComments.Text,
                                     Bytes = AttachFile.PostedFile.ContentLength,
                                     MimeType = mimeType,
                                     LastUpdate = new ActivityStamp(UserContext.CurrentUser)
                                 };
            attachment.GenerateUniqueName();
            return attachment;
        }

		protected void attachedFiles_OnItemDeleting(object sender, ListViewDeleteEventArgs e)
		{
            if (ApplicationInContext.Attachments.Count > e.ItemIndex)
            {
                var attachment = ApplicationInContext.Attachments[e.ItemIndex];
                if (null != attachment)
                {
                    ApplicationInContext.Attachments.RemoveAt(e.ItemIndex);

                    ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ApplicationService.Update(ApplicationInContext);

                    attachment.RemoveFile(ConfigHelper.GetAttachmentsDirectory());
                }
            }
            BindAttachedFiles();
		}

		
		#endregion

    	protected void scholarshipFiles_ItemCommand(object sender, ListViewCommandEventArgs e)
    	{
    		if (e.CommandName == "Download")
    		{
				var id = Int32.Parse((string)e.CommandArgument);
				var attachment = ApplicationInContext.Scholarship.Attachments.First(a => a.Id == id);
				FileHelper.SendFile(Response, attachment.Name, attachment.MimeType, attachment.GetFullPath(ConfigHelper.GetAttachmentsDirectory()));
    		}
    	}
	}
}
