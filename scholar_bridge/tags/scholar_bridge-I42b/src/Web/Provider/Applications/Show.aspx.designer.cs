﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3603
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScholarBridge.Web.Provider.Applications {
    
    
    public partial class Show {
        
        /// <summary>
        /// PrintViewControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.PrintView PrintViewControl;
        
        /// <summary>
        /// ScholarshipTitleStripeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.ScholarshipTitleStripe ScholarshipTitleStripeControl;
        
        /// <summary>
        /// printApplication control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.PrintApplication printApplication;
    }
}
