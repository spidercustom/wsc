﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditOrganizationUserEmail.ascx.cs" Inherits="ScholarBridge.Web.Common.EditOrganizationUserEmail" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>                         
<script language="javascript" type="text/javascript">
     $(document).ready(function() {
     $("#pnledituseremail").keypress(function(e) {

             if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                 $("#pnledituseremail > a[id$='_saveButton']").click();
                 return true;
             }
         });
     });
 </script>      
<p >
Changing your Email Address will require you to confirm that email before logging in again.<br />
The new Email Address will be how you log into the system.
</p>

<div   class="form-iceland-container"  >
    <div class="form-iceland" ID="pnledituseremail">
        <label for="Email">Email Address:</label>
        <asp:TextBox  ValidationGroup="EditOrganizationUserEmail_Validations" ID="Email" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ValidationGroup="EditOrganizationUserEmail_Validations" ID="EmailValidator" runat="server" ControlToValidate="Email" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
        <asp:CompareValidator  ValidationGroup="EditOrganizationUserEmail_Validations" ID="compareValidator" runat="server" ControlToValidate="Email" ControlToCompare="ConfirmEmail" ErrorMessage="Email and Confirm Email must match." />
        <br />
        <label for="ConfirmEmail">Confirm Email Address:</label>
        <asp:TextBox  ValidationGroup="EditOrganizationUserEmail_Validations" ID="ConfirmEmail" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ValidationGroup="EditOrganizationUserEmail_Validations" ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" />
        <br /><br />
        <sbCommon:AnchorButton ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click" />
         <br />
    </div>
</div>      