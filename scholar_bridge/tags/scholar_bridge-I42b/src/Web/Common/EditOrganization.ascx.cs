﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Web.Common
{
    public delegate void OrganizationSaved(Organization org);

    public partial class EditOrganization : System.Web.UI.UserControl
    {
        public IStateDAL StateService { get; set; }

        public Organization Organization { get; set; }
        public bool ViewOnlyMode
        {
            
            set
            {
                if (value == true)
                {
                    saveBtn.Visible = false;
                    Website.Enabled = false;
                    Fax.Enabled = false;
                    OtherPhone.Enabled = false;
                }
            }
        }
        public event OrganizationSaved OrganizationSaved;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateStates();
                PopulateOrganizationInfo();
               
            }
        }
        protected void ValidateWebsite_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Website.Text))
                ClientSideDialogs.ShowAlert("Cannot validate empty webiste. Please enter website and try again.", "");
            else

                PopupHelper.OpenURL(LinkGenerator.AddHttpPrefix(Website.Text));
        }
        private void PopulateOrganizationInfo()
        {
            if (null != Organization)
            {
                Name.Text = Organization.Name;
                TaxId.Text = Organization.TaxId;
                Website.Text = Organization.Website;

                if (null != Organization.Address)
                {
                    AddressStreet.Text = Organization.Address.Street;
                    AddressStreet2.Text = Organization.Address.Street2;
                    AddressCity.Text = Organization.Address.City;
                    if (null != Organization.Address.State)
                    {
                        AddressState.SelectedValue = Organization.Address.State.Abbreviation;
                    }
                    AddressPostalCode.Text = Organization.Address.PostalCode;
                }

                Phone.Text = Organization.Phone == null ? null : Organization.Phone.ToString();
                Fax.Text = Organization.Fax == null ? null : Organization.Fax.ToString();
                OtherPhone.Text = Organization.OtherPhone == null ? null : Organization.OtherPhone.ToString();
            }
        }

        private void PopulateStates()
        {
            AddressState.DataSource = StateService.FindAll();
            AddressState.DataTextField = "Name";
            AddressState.DataValueField = "Abbreviation";
            AddressState.DataBind();
            AddressState.Items.Insert(0, new ListItem("- Select One -", ""));
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                // Organization.Name = Name.Text;
                Organization.Website = Website.Text;

                if (null == Organization.Address)
                {
                    Organization.Address = new Address();
                }
                
                // Organization.Address.Street = AddressStreet.Text;
                // Organization.Address.Street2 = AddressStreet2.Text;
                // Organization.Address.City = AddressCity.Text;
                // Organization.Address.State = StateService.FindByAbbreviation(AddressState.SelectedValue);
                // Organization.Address.PostalCode= AddressPostalCode.Text;

                // Organization.Phone = new PhoneNumber(Phone.Text);
                Organization.Fax = new PhoneNumber(Fax.Text);
                Organization.OtherPhone = new PhoneNumber(OtherPhone.Text);

                if (null != OrganizationSaved)
                {
                    OrganizationSaved(Organization);
                }
            }
        }

        
    }
}