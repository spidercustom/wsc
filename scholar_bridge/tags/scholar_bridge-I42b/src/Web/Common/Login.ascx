﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ScholarBridge.Web.Common.Login" %>
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login   ID="Login1" runat="server" onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError"  FailureText="(!) Invalid email or password. Please try again."   >
            <%--UserNameLabelText=""  
            PasswordRecoveryUrl="~/ForgotPassword.aspx"  
            LoginButtonImageUrl="~/images/Btn_Login.gif"
            PasswordLabelText=""
            RememberMeText="Remember my username on this computer?" 
            TitleTextStyle-CssClass="loginTitleText" 
            onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError" 
            LoginButtonType="Image" 
            DisplayRememberMe="False">--%>
           
            
     <LayoutTemplate >
     <div id="ValidationErrorPopup" title="" style="display: none">
     
        <asp:Image ID="ValidationErrorPopupimage" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/cooltipicon_errorbig.png"/> 
        <asp:Literal ID="FailureDialogText" runat="server"> Invalid email or password. Please try again.</asp:Literal>
        
    </div>
        <asp:panel ID="loginPanel" DefaultButton="Login" runat="server">
            <div class="ReturningUser">Returning User? Log in here.</div>
            <asp:Textbox id="UserName" text="Email Address" runat="server" Cssclass="DashboardUnlogged" />
            <asp:TextBox TextMode="Password"  id="Password" text="password" runat="server" Cssclass="DashboardUnlogged" />
            <div class="GreenText">New User? <asp:HyperLink id="registerlink" runat="server" Text="Register" /><br /></div>
            <asp:Button  Cssclass="Button"  ID="Login"  CommandName="Login" runat="server" />
            <div>
                <span class="ForgotPassword"> <asp:HyperLink id="forgotPassword" runat="server" Text="Forgot Username or Password?" NavigateUrl="#"  /> </span>
                <span id="registerTodayContainer" runat="server" class="register-today"><asp:HyperLink id="registerToday" runat="server" Text="Register Today!" NavigateUrl="~/Seeker/Register.aspx" /></span>
                <br />
                <span class="ErrorMsg"><asp:Literal ID="FailureText"  runat="Server"  Visible="false" /></span>
           </div>
        </asp:panel>
     </LayoutTemplate>
     
     </asp:Login>
    </AnonymousTemplate>    
</asp:LoginView>