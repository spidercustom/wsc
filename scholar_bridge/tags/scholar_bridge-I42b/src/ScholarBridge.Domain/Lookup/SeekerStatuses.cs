﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SeekerStatuses
    {
        [DisplayName("Full-time")]
        FullTime = 1,
        [DisplayName("Half-time")]
        PartTime = 2,
        [DisplayName("Less than half-time")]
        LessThanPartTime = 4
    }
}
