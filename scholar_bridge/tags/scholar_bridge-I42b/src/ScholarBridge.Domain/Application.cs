using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Domain
{
    public class Application : CriteriaCountBase
    {
		public const string APPLICATION_SUMBISSION_RULESET = "Application-Submission";
		public const string WA_ADDRESS_VALIDATOR = "WA-Address";

        private SeekerAddress address;

		#region C'tors

        public Application(ApplicationType t)
        {
            ApplicationType = t;
            InitializeMembers();
        }
        public Application()
        {
            ApplicationType = ApplicationType.Internal;
            InitializeMembers();
        }

        public Application(Match m,ApplicationType t) : this(t)
        {
            Scholarship = m.Scholarship;
            Seeker = m.Seeker;
            
            InitializeMembers();
        }

		#endregion


        private void InitializeMembers()
        {
           
            ApplicantName = new PersonName();
            Attachments = new List<Attachment>();
            Ethnicities = new List<Ethnicity>();
            Religions = new List<Religion>();
            Sports = new List<Sport>();

            AcademicAreas = new List<AcademicArea>();
            Careers = new List<Career>();
            CommunityServices = new List<CommunityService>();
            Hobbies = new List<SeekerHobby>();
            Clubs = new List<Club>();
            MatchOrganizations = new List<SeekerMatchOrganization>();
            Companies = new List<Company>();
            Address = new SeekerAddress();
            SupportedSituation = new SupportedSituation();
            QuestionAnswers = new List<ApplicationQuestionAnswer>(); 
        }

        public virtual int Id { get; set; }
        public virtual Scholarship Scholarship { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual ApplicationStages Stage { get; set; }
        public virtual ApplicationType ApplicationType { get; set; }
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should be between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? SubmittedDate { get; set; }
        public virtual bool Finalist { get; set; }
        public virtual AwardStatuses AwardStatus { get; set; }

		[ObjectValidator(Ruleset = APPLICATION_SUMBISSION_RULESET)]
        public virtual PersonName ApplicantName { get; set; }
        public virtual string Email { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual PhoneNumber MobilePhone { get; set; }
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should be between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? DateOfBirth { get; set; }

        public virtual County County { get; set; }

		[ObjectValidator(AddressBase.ADDRESS_REQUIRED, Ruleset = WA_ADDRESS_VALIDATOR)]
        public virtual SeekerAddress Address
        {
            get
            {
                if (address == null)
                    address = new SeekerAddress();
                return address;
            }
            set
            {
                address = value;
            }
        }

        public virtual bool FirstGeneration { get; set; }

        public virtual Genders Gender { get; set; }

        public virtual CurrentSchool CurrentSchool { get; set; }

        [StringLengthValidator(0, 1500)]
        public virtual string PersonalStatement { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyChallenge { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyGift { get; set; }

        public virtual SeekerAcademics SeekerAcademics { get; set; }

        [StringLengthValidator(0, 100)]
        public virtual string Words { get; set; }
        [StringLengthValidator(0, 100)]
        public virtual string Skills { get; set; }

        public virtual IList<Ethnicity> Ethnicities { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string EthnicityOther { get; set; }

        public virtual IList<Religion> Religions { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ReligionOther { get; set; }

        public virtual IList<Sport> Sports { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string SportOther { get; set; }

        public virtual IList<AcademicArea> AcademicAreas { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string AcademicAreaOther { get; set; }

        public virtual IList<Career> Careers { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CareerOther { get; set; }

        public virtual IList<CommunityService> CommunityServices { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CommunityServiceOther { get; set; }

        public virtual IList<SeekerHobby> Hobbies { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string HobbyOther { get; set; }

        public virtual IList<Club> Clubs { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ClubOther { get; set; }

        public virtual IList<SeekerMatchOrganization> MatchOrganizations { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string MatchOrganizationOther { get; set; }

        public virtual IList<Company> Companies { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CompanyOther { get; set; }

        public virtual bool IsWorking { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string WorkHistory { get; set; }

        public virtual bool IsService { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ServiceHistory { get; set; }
        [StringLengthValidator(0, 50)]
        public virtual string AddressCity { get; set; }
        [StringLengthValidator(0, 50)]
        public virtual string AddressCounty { get; set; }
        public virtual SupportedSituation SupportedSituation { get; set; }
        
        public virtual bool? HasFAFSACompleted { get; set; }
        public virtual decimal? ExpectedFamilyContribution { get; set; }

        public virtual IList<Attachment> Attachments { get; protected set; }
        public virtual IList<ApplicationQuestionAnswer> QuestionAnswers { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual bool HasAttachments { get { return Attachments != null && Attachments.Count > 0; } }
        public virtual void CopyFromSeekerandScholarship()
        {
            var seeker = Seeker;
            if (seeker == null)
                throw new  NullReferenceException("Seeker");

            var scholarship = Scholarship;

            if (scholarship == null)
                throw new  NullReferenceException("Scholarship");

            ApplicantName = seeker.Name;
            
            AcademicAreaOther = seeker.AcademicAreaOther;
            AcademicAreas = new List<AcademicArea>(seeker.AcademicAreas);
            Address = seeker.Address;
            CompanyOther = seeker.CompanyOther;
            Companies =new List<Company>(seeker.Companies);
            ApplicantName = seeker.Name;
            CareerOther = seeker.CareerOther;
            Careers =new List<Career>( seeker.Careers);
            ClubOther = seeker.ClubOther;
            Clubs =new List<Club>(seeker.Clubs);
            CommunityServiceOther = seeker.CommunityServiceOther;
            CommunityServices =new List<CommunityService>( seeker.CommunityServices);
            County = seeker.County;
            CurrentSchool = seeker.CurrentSchool;
            DateOfBirth = seeker.DateOfBirth;
            Email = seeker.User.Email;
            Ethnicities = new List<Ethnicity>(seeker.Ethnicities);
            EthnicityOther = seeker.EthnicityOther;
            FirstGeneration = seeker.FirstGeneration;
            Gender = seeker.Gender;
            Hobbies = new List<SeekerHobby>(seeker.Hobbies);
            HobbyOther = seeker.HobbyOther;
            MatchOrganizations = new List<SeekerMatchOrganization>(seeker.MatchOrganizations);
            MatchOrganizationOther = seeker.MatchOrganizationOther;
            MobilePhone = seeker.MobilePhone;
            MyChallenge = seeker.MyChallenge;
            MyGift = seeker.MyGift;
            ExpectedFamilyContribution = seeker.ExpectedFamilyContribution;
            HasFAFSACompleted = seeker.HasFAFSACompleted;
            PersonalStatement = seeker.PersonalStatement;
            Phone = seeker.Phone;
            ReligionOther = seeker.ReligionOther;
            Religions = new List<Religion>(seeker.Religions);
            SeekerAcademics =(SeekerAcademics) seeker.SeekerAcademics.Clone();
            //SupportedSituation=new SupportedSituation();
            SupportedSituation.ResetTypesOfSupport(seeker.SupportedSituation.TypesOfSupport); 
            Skills = seeker.Skills;
            SportOther = seeker.SportOther;
            Sports=new List<Sport>(seeker.Sports);
            Words = seeker.Words;
            ServiceHistory = seeker.ServiceHistory;
            WorkHistory = seeker.WorkHistory;
            IsService = seeker.IsService;
            IsWorking = seeker.IsWorking;
            AddressCity = seeker.AddressCity;
			AddressCounty = seeker.AddressCounty;
       
            //copy attachments

            foreach (Attachment a in seeker.Attachments )
            {
                 if (a.IncludeWithApplications)
                 {
                    var attachment = new Attachment()
                                         {

                                             Bytes = a.Bytes,
                                             Comment = a.Comment,
                                             MimeType = a.MimeType,
                                             Name = a.Name,
                                             UniqueName = a.UniqueName,
                                             LastUpdate=new ActivityStamp(seeker.User)
                                         };

                    Attachments.Add(attachment);
                 }
            }
            //copy questions from scholarship
            foreach (ScholarshipQuestion q in scholarship.AdditionalQuestions)
            {

                var qans = new ApplicationQuestionAnswer
                           	{
                                   Question = q,
                                   Application = this,
                                   AnswerText = "",
                                   LastUpdate = new ActivityStamp(seeker.User)


                               };
                QuestionAnswers.Add(qans);
            }
        }

		public virtual ApplicationStatus ApplicationStatus
		{
			get
			{
                if (ApplicationType == ApplicationParts.ApplicationType.External)
                    return ApplicationStatus.ApplyatProviderSite;

                DateTime applicationDueDate = Scholarship.ApplicationDueDate.Value.AddDays(1).AddSeconds(-1);
				if (Scholarship.Stage == ScholarshipStages.Awarded || Scholarship.Stage == ScholarshipStages.Awarded)
					return ApplicationStatus.Closed;

				var result = ApplicationStatus.Unknown;

				if (Stage == ApplicationStages.Submitted)
				{

					if (DateTime.Today < Scholarship.ApplicationStartDate) result = ApplicationStatus.Submitted;

					if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate, applicationDueDate))
						result = ApplicationStatus.Submitted;

					if (DateTime.Today >= applicationDueDate) result = ApplicationStatus.BeingConsidered;
					if (Scholarship.AwardPeriodClosed.HasValue) result = ApplicationStatus.BeingConsidered;
				}

				else
				{
					if (DateTime.Today < Scholarship.ApplicationStartDate) result = ApplicationStatus.Applying;

					if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate, applicationDueDate))
						result = ApplicationStatus.Applying;

					if (DateTime.Today >= applicationDueDate) result = ApplicationStatus.Closed;

					if (Scholarship.AwardPeriodClosed.HasValue) result = ApplicationStatus.Closed;
				}

				return result;
			}
		}



        public virtual void Submit()
        {
            if (Stage != ApplicationStages.Submitted)
            {
                Stage = ApplicationStages.Submitted;
                SubmittedDate = DateTime.Now; 
            }
        }

        public virtual bool CanEdit()
        {
            return Stage != ApplicationStages.Submitted;
        }

        public virtual ValidationResults ValidateSubmission()
        {
        	var results = Validate(APPLICATION_SUMBISSION_RULESET);
			if (Address.State == null || Address.State.Abbreviation == State.WASHINGTON_STATE_ID)
				results.AddAllResults(Validate(WA_ADDRESS_VALIDATOR));
            //FIXME: We need to return validation results as applicable

			CheckForRequiredFields(results);
            return results;
        }

    	private void CheckForRequiredFields(ValidationResults results)
    	{
			foreach (ScholarshipAttributeUsage<SeekerProfileAttribute> attribute in Scholarship.SeekerProfileCriteria.Attributes)
    		{
    			if (attribute.UsageType == ScholarshipAttributeUsageType.Minimum)
    			{
					switch (attribute.Attribute)
					{
						
						case SeekerProfileAttribute.AcademicArea:
							// not validated yet
							break;
						case SeekerProfileAttribute.AcademicProgram:
							if (SeekerAcademics.AcademicPrograms.GetDisplayNames().Length == 0)
								results.AddResult(CreateRequiredResult("Academic Program"));
							break;
						case SeekerProfileAttribute.ACTEnglish:
							if (SeekerAcademics.ACTScore.English == null || SeekerAcademics.ACTScore.English == 0)
								results.AddResult(CreateRequiredResult("ACT English Score"));
							break;
						case SeekerProfileAttribute.ACTMathematics:
							if (SeekerAcademics.ACTScore.Mathematics == null || SeekerAcademics.ACTScore.Mathematics == 0)
								results.AddResult(CreateRequiredResult("ACT Mathematics Score"));
							break;
						case SeekerProfileAttribute.ACTReading:
							if (SeekerAcademics.ACTScore.Reading == null || SeekerAcademics.ACTScore.Reading == 0)
								results.AddResult(CreateRequiredResult("ACT Reading Score"));
							break;
						case SeekerProfileAttribute.ACTScience:
							if (SeekerAcademics.ACTScore.Science == null || SeekerAcademics.ACTScore.Science == 0)
								results.AddResult(CreateRequiredResult("ACT Science Score"));
							break;
						case SeekerProfileAttribute.APCreditsEarned:
							// not validated yet
							break;
						case SeekerProfileAttribute.Career:
							// not validated yet
							break;
						case SeekerProfileAttribute.City:
							if (string.IsNullOrEmpty(Address.GetCity) && string.IsNullOrEmpty(AddressCity))
								results.AddResult(CreateRequiredResult("Your Current City"));
							break;
						case SeekerProfileAttribute.ClassRank:
							// not validated yet
							break;
						case SeekerProfileAttribute.Club:
							// not validated yet
							break;
						case SeekerProfileAttribute.College:
							if ((SeekerAcademics.CollegesApplied == null || SeekerAcademics.CollegesApplied.Count == 0) && string.IsNullOrEmpty(SeekerAcademics.CollegesAppliedOther))
								results.AddResult(CreateRequiredResult("Your intended College"));
							break;
						//not used - I think...
						//case SeekerProfileAttribute.CommunityService:
						//    throw new NotImplementedException("not implemented");
						//    break;
						case SeekerProfileAttribute.Company:
							// not validated yet
							break;
						case SeekerProfileAttribute.County:
							if (County == null && string.IsNullOrEmpty(AddressCounty))
								results.AddResult(CreateRequiredResult("Your Current County"));
							break;
						case SeekerProfileAttribute.Ethnicity:
							// not validated yet
							break;
						case SeekerProfileAttribute.FirstGeneration:
							// not validated yet
							break;
						case SeekerProfileAttribute.Gender:
							if (Gender == Genders.Unspecified)
								results.AddResult(CreateRequiredResult("Gender specification"));
							break;
						case SeekerProfileAttribute.GPA:
							if (!SeekerAcademics.GPA.HasValue || SeekerAcademics.GPA.Value == 0 )
								results.AddResult(CreateRequiredResult("Your GPA"));
							break;
						case SeekerProfileAttribute.HighSchool:
							if (CurrentSchool.HighSchool == null && string.IsNullOrEmpty(CurrentSchool.HighSchoolOther))
								results.AddResult(CreateRequiredResult("Your High School of Graduation"));
							break;
						case SeekerProfileAttribute.Honor:
							// not validated yet
							break;
						case SeekerProfileAttribute.IBCreditsEarned:
							// not validated yet
							break;
						case SeekerProfileAttribute.Organization:
							// not validated yet
							break;

						case SeekerProfileAttribute.Religion:
							// not validated yet
							break;
						case SeekerProfileAttribute.SATCriticalReading:
							if (SeekerAcademics.SATScore.CriticalReading == null || SeekerAcademics.SATScore.CriticalReading == 0)
								results.AddResult(CreateRequiredResult("SAT Critical Reading Score"));
							break;
						case SeekerProfileAttribute.SATMathematics:
							if (SeekerAcademics.SATScore.Mathematics == null || SeekerAcademics.SATScore.Mathematics == 0)
								results.AddResult(CreateRequiredResult("SAT Mathematics Score"));
							break;
						case SeekerProfileAttribute.SATWriting:
							if (SeekerAcademics.SATScore.Writing == null || SeekerAcademics.SATScore.Writing == 0)
								results.AddResult(CreateRequiredResult("SAT Writing Score"));
							break;
						case SeekerProfileAttribute.SchoolType:
							if (SeekerAcademics.SchoolTypes.GetNumericValue() < 1)
								results.AddResult(CreateRequiredResult("School Type You are Considering"));
							break;
						case SeekerProfileAttribute.SeekerGeographicsLocation:
							// not validated yet
							break;
						case SeekerProfileAttribute.SeekerHobby:
							// not validated yet
							break;
						case SeekerProfileAttribute.SeekerSkill:
							// not validated yet
							break;
						case SeekerProfileAttribute.SeekerStatus:
							if (SeekerAcademics.SeekerStatuses.GetDisplayNames().Length == 0)
								results.AddResult(CreateRequiredResult("Current Enrollment Status"));
							break;
						case SeekerProfileAttribute.SeekerVerbalizingWord:
						 //not used in match
						    break;
						case SeekerProfileAttribute.ServiceType:
							// not validated yet
							break;
						case SeekerProfileAttribute.Sport:
							// not validated yet
							break;
						case SeekerProfileAttribute.State:
							if (Address.State == null)
								results.AddResult(CreateRequiredResult("State Selection"));
							break;
						case SeekerProfileAttribute.WorkType:
							// not validated yet
							break;
						default:
							break;
					}
				}
    		}
			foreach (FundingProfileAttributeUsage attribute in Scholarship.FundingProfile.AttributesUsage)
			{
				if (attribute.UsageType == ScholarshipAttributeUsageType.Minimum)
				{
					switch (attribute.Attribute)
					{
						case FundingProfileAttribute.Need:
							if (string.IsNullOrEmpty(MyChallenge))
								results.AddResult(CreateRequiredResult("Your Financial Need Statement"));
							break;
						default:
							break;
					}
				}
			}
			if (SupportedSituation.TypesOfSupport.Count < 1)
				results.AddResult(CreateRequiredResult("Type of Support Needed"));

    	}

		private ValidationResult CreateRequiredResult(string requiredAttribute)
		{
			return new ValidationResult(string.Format("{0} is required for this scholarship", requiredAttribute), this, "", "", null);
		}

    	private ValidationResults Validate(string ruleSet)
        {
            var validator = ValidationFactory.CreateValidator<Application >(ruleSet);
            return validator.Validate(this);
        }

		
    }
}