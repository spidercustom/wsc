﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(106)]
	public class AddPhonesToUsers : Migration
	{
		private const string TABLE_NAME = "SBUser";
		private readonly Column[] Columns = new[]
                {
                    new Column("PhoneNumber", DbType.String, 10, ColumnProperty.Null), 
                    new Column("FaxNumber", DbType.String, 10, ColumnProperty.Null), 
                    new Column("OtherPhoneNumber", DbType.String, 10, ColumnProperty.Null)
                };

		public override void Up()
		{
			foreach (var column in Columns)
			{
				Database.AddColumn(TABLE_NAME, column);
			}
		}

		public override void Down()
		{
			foreach (var column in Columns)
			{
				Database.RemoveColumn(TABLE_NAME, column.Name);
			}
		}
	}
}
