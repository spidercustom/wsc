using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(5)]
    public class AddState : Migration
    {
        public const string TABLE_NAME = "SB_State";
        protected static readonly string[] COLUMNS = new[] { "Abbreviation", "Name", "CountryCode" };

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                  new Column(COLUMNS[0], DbType.String, 2, ColumnProperty.PrimaryKey),
                  new Column(COLUMNS[1], DbType.String, 50, ColumnProperty.NotNull),
                  new Column(COLUMNS[2], DbType.String, 2, ColumnProperty.NotNull)
            );

            Database.Insert(TABLE_NAME, COLUMNS, new[] { "AL", "Alabama", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "AK", "Alaska", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "AZ", "Arizona", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "AR", "Arkansas", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "CA", "California", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "CO", "Colorado", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "CT", "Connecticut", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "DE", "Delaware", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "DC", "District of Columbia", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "FL", "Florida", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "GA", "Georgia", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "HI", "Hawaii", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "ID", "Idaho", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "IL", "Illinois", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "IN", "Indiana", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "IA", "Iowa", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "KS", "Kansas", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "KY", "Kentucky", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "LA", "Louisiana", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "ME", "Maine", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "MD", "Maryland", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "MA", "Massachusetts", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "MI", "Michigan", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "MN", "Minnesota", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "MS", "Mississippi", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "MO", "Missouri", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "MT", "Montana", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "NE", "Nebraska", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "NV", "Nevada", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "NH", "New Hampshire", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "NJ", "New Jersey", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "NM", "New Mexico", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "NY", "New York", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "NC", "North Carolina", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "ND", "North Dakota", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "OH", "Ohio", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "OK", "Oklahoma", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "OR", "Oregon", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "PA", "Pennsylvania", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "PR", "Puerto Rico", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "RI", "Rhode Island", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "SC", "South Carolina", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "SD", "South Dakota", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "TN", "Tennessee", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "TX", "Texas", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "UT", "Utah", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "VT", "Vermont", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "VA", "Virginia", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "WA", "Washington", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "WV", "West Virginia", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "WI", "Wisconsin", "US" });
            Database.Insert(TABLE_NAME, COLUMNS, new[] { "WY", "Wyoming", "US" });
        }

        public override void Down()
        {
            Database.RemoveTable(TABLE_NAME);
        }
    }
}