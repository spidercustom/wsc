﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(33)]
    public class AddAffiliationType : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "AffiliationType";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
