using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(197)]
    public class AddApplicationCollegeAcceptedRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationCollegeAcceptedRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCollegeLUT"; }
        }
    }
}