﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(218)]
    public class ChangeSeekerAddProfileLastUpdateDate : Migration
    {
    	private const string ProfileLastUpdateDate = "ProfileLastUpdateDate";
        public override void Up()
        {
			Database.AddColumn(AddSeekerTable.TABLE_NAME, new Column(ProfileLastUpdateDate, DbType.DateTime));
        	Database.ExecuteNonQuery(string.Format("update {0} set {1} = LastUpdateDate",
												   AddSeekerTable.TABLE_NAME, ProfileLastUpdateDate));
        }

        public override void Down()
        {
			Database.RemoveColumn(AddSeekerTable.TABLE_NAME, "ProfileLastUpdatedDate");
        }
    }
}