﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(208)]
    public class ChangeScholarshipDonorNameToSingleField : Migration
    {
        private const string TABLE = "SBScholarship";

        public override void Up()
        {
            Database.AddColumn(TABLE, new Column("DonorName", DbType.String, 250));

            Database.ExecuteNonQuery("UPDATE SBScholarship SET DonorName=ISNULL(DonorFirstName, '') + ISNULL(DonorMiddleName, '') + ISNULL(DonorLastName, '') WHERE DonorFirstName is not null OR DonorMiddleName is not null or DonorLastName is not null");

            Database.RemoveColumn(TABLE, "DonorFirstName");
            Database.RemoveColumn(TABLE, "DonorMiddleName");
            Database.RemoveColumn(TABLE, "DonorLastName");
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE, "DonorName");
            Database.AddColumn(TABLE, new Column("DonorFirstName", DbType.String, 50));
            Database.AddColumn(TABLE, new Column("DonorMiddleName", DbType.String, 50));
            Database.AddColumn(TABLE, new Column("DonorLastName", DbType.String, 50));
        }
    }
}
