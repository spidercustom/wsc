﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum StudentGroups
    {//need to re-order because of sorting while its going to be bound with some control.
        [DisplayName("Middle School")]
        MiddleSchool = 1,
        [DisplayName("High School Freshman")]
        HighSchoolFreshMan = 256,
        [DisplayName("High School Sophomore")]
        HighSchoolSophomore = 512,
        [DisplayName("High School Junior")]
        HighSchoolJunior = 1024,
        [DisplayName("High School Senior")]
        HighSchoolSenior = 2,
        [DisplayName("High School Graduate")]
        HighSchoolGraduate = 4,
        [DisplayName("Adult Returning")]
        AdultReturning = 8,
        [DisplayName("Adult First-time")]
        AdultFirstTime = 16,
        [DisplayName("Transfer")]
        Transfer = 32,
        [DisplayName("Undergraduate")]
        Undergraduate = 64,
        [DisplayName("Graduate")]
        Graduate = 128
        

    }
}
