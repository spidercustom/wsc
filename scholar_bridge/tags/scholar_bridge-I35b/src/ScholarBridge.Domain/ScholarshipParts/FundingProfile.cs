﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingProfile : ICloneable
    {
        public FundingProfile()
        {
            InitializeMembers();
        }

        private void InitializeMembers()
        {
            SupportedSituation = new SupportedSituation();
            AttributesUsage = new List<FundingProfileAttributeUsage>();
        }

        public virtual IList<FundingProfileAttributeUsage> AttributesUsage { get; set; }
        public virtual bool HasAttributeUsage(FundingProfileAttribute attribute)
        {
            return AttributesUsage.Any(o => o.Attribute == attribute);
        }

        public virtual FundingProfileAttributeUsage FindAttributeUsage(FundingProfileAttribute attribute)
        {
            return AttributesUsage.FirstOrDefault(o => o.Attribute == attribute);
        }

        public virtual void RemoveAttributeUsage(FundingProfileAttribute attribute)
        {
            var attributeUsage = FindAttributeUsage(attribute);
            if (null != attributeUsage)
                AttributesUsage.Remove(attributeUsage);
        }

        public virtual ScholarshipAttributeUsageType GetAttributeUsageType(FundingProfileAttribute attribute)
        {
            var attributeUsage = FindAttributeUsage(attribute);
            if (null != attributeUsage)
                return attributeUsage.UsageType;

            return ScholarshipAttributeUsageType.NotUsed;
        }

        public virtual SupportedSituation SupportedSituation { get; set; }

        public virtual object Clone()
        {
            var result = (FundingProfile) MemberwiseClone();
            result.AttributesUsage = new List<FundingProfileAttributeUsage>(AttributesUsage);
            result.SupportedSituation = (SupportedSituation)SupportedSituation.Clone();
            
            return result;
        }

    }
}
