﻿using System;
using ScholarBridge.Business;

namespace ScholarBridge.Web.PressRoom
{
    public partial class Default : System.Web.UI.Page
    {
        public IArticleService ArticleService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            ArticleList1.Articles = ArticleService.FindAll();
        }
    }
}
