﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PrintApplications.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.PrintApplications" Title="Applications | Print" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
$(document).ready(function() {
    $("#<%= printApplications.ClientID %>").change(function() {
        var radios = "#<%= applicationChoice.ClientID %> :input";
        if ($(this).is(':checked')) {
            $(radios).removeAttr('disabled');
        } else {
            $(radios).attr('disabled', true);
        }  
    });
});
</script>
</asp:Content>

<asp:Content ID="PrintViewPageHeader" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
    <asp:PlaceHolder id="PrintViewPageHeaderPlaceHolder" runat="server">
    
    </asp:PlaceHolder>
</asp:Content>

<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgScholarship.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div id="CenterContent">
        <div id="HomeContentLeft"></div>
        <div id="HomeContentRight"></div>

    <asp:Panel ID="printChoice" runat="server">
        <input type="hidden" name="print" value="true" /><br />
        <asp:CheckBox ID="printList" runat="server" Text="Print list of Applicants" /><br />
        <asp:CheckBox ID="printApplications" runat="server" Text="Print Applications" Checked="true"/>
        <asp:RadioButtonList ID="applicationChoice" runat="server">
            <asp:ListItem  Text="Finalists" Value="finalists" Selected="True" />
            <asp:ListItem  Text="All" Value="all" />
        </asp:RadioButtonList>
        </ul>
        <asp:Button ID="printBtn" runat="server" Text="Print" OnClick="print_OnClick"/>
    </asp:Panel>

    <asp:Panel ID="printContent" runat="server" Visible="false">

    </asp:Panel>
</div>

</asp:Content>
