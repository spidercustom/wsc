﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.MatchList;

namespace ScholarBridge.Web.Seeker.Scholarships
{
	public partial class Default : SBBasePage
	{
        private const string SCHOLARSHIP_VIEW_TEMPLATE = "~/Seeker/Scholarships/Show.aspx";
		public IMatchService MatchService { get; set; }
		public IApplicationService ApplicationService { get; set; }
		public IUserContext UserContext { get; set; }

        private Domain.Seeker currentSeeker;
        private readonly ApplicationListActionHelper actionHelper = new ApplicationListActionHelper();

		private IList<Application> applications;
		public IList<Application> Applications
		{
			get
			{
				if (applications == null)
				{
					if (currentSeeker == null)
						throw new InvalidOperationException("There is no seeker in context");
					applications = ApplicationService.FindBySeeker(currentSeeker);
				}
				return applications;
			}
		}

		protected override void OnInitComplete(EventArgs e)
        {
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.SeekerApplications); 
            UserContext.EnsureSeekerIsInContext();
            currentSeeker = UserContext.CurrentSeeker;
		    actionHelper.SetupListView(myScholarhipList);
            BindMyScholarships();
            base.OnInitComplete(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void BindMyScholarships()
        {
            myScholarhipList.DataSource = Applications;
            myScholarhipList.DataBind();
        }

        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var application = ((Application)((ListViewDataItem)e.Item).DataItem);
                var link = (LinkButton)e.Item.FindControl("linkToScholarship");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(SCHOLARSHIP_VIEW_TEMPLATE) }.ToString()
							+ "?id=" + application.Scholarship.Id + "&print=true";
                link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
           
                var submittedDateLabel = (Label)e.Item.FindControl("submittedDateLabel");
				submittedDateLabel.Text = application.SubmittedDate == null
                                              ? "&nbsp;"
                                              : application.SubmittedDate.Value.ToShortDateString();
            }
        }


        protected void matchList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindMyScholarships();
        }

	    protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }
	}
}