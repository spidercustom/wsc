﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalMenu.ascx.cs"
    Inherits="ScholarBridge.Web.Common.GlobalMenu" %>
<%@ Register Src="UnreadMessageLink.ascx" TagName="UnreadMessageLink" TagPrefix="sb" %>
<%@ Register Src="MyHomeLink.ascx" TagName="MyHomeLink" TagPrefix="sb" %>
<%@ Register Src="ScholarshipSearchBox.ascx" TagName="ScholarshipSearchBox" TagPrefix="sb" %>
<asp:LoginView ID="loginView2" runat="server">
    <RoleGroups>
        <asp:RoleGroup Roles="WSCAdmin, Admin">
            <ContentTemplate>
                <div id="GlobaNavLinksContainer">
                    <div class="GlobalNavLinks">
                        <sb:MyHomeLink runat="server" />
                        &nbsp; | &nbsp;
                        <sb:UnreadMessageLink ID="UnreadMessageLink1" runat="server" />
                        <asp:LoginStatus ID="LoginStatus1" LogoutAction="RedirectToLoginPage" runat="server" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:RoleGroup>
        <asp:RoleGroup Roles="Intermediary, Intermediary Admin,Provider, Provider Admin, Seeker">
            <ContentTemplate>
                <div id="GlobaNavLinksContainerSeeker">
                    <div class="GlobalNavLinks">
                        <sb:MyHomeLink runat="server" />
                        &nbsp; | &nbsp;
                        <sb:UnreadMessageLink ID="UnreadMessageLink2" runat="server" />
                        <asp:LoginStatus ID="LoginStatus2" LogoutAction="RedirectToLoginPage" runat="server" />
                        &nbsp;&nbsp;
                        <sb:ScholarshipSearchBox runat="server" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:RoleGroup>
    </RoleGroups>
</asp:LoginView>
<asp:Panel ID="AnonymousPanel" runat="server" Visible="false">
    <div id="GlobaNavLinksContainerSeeker">
        <div class="GlobalNavLinks">
            <sb:ScholarshipSearchBox ID="ScholarshipSearchBox1" runat="server" />
        </div>
    </div>
</asp:Panel>
