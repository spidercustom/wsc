﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Business.Actions;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Web.Admin;
using ScholarBridge.Web.Config;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;
namespace ScholarBridge.Web.Common
{
    public partial class MessageView : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }
        public IMessageActionService MessageActionService { get; set; }

        public string ListUrl { get; set; }

        public int MessageId { get { return Int32.Parse(Request["id"]); } }

        public Domain.Messaging.Message CurrentMessage { get; set; }
        public IAction MessageAction { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            CurrentMessage = MessageService.FindMessage(UserContext.CurrentUser, UserContext.CurrentOrganization, MessageId);
            if (null != CurrentMessage)
            {
                if (CurrentMessage.IsRead == false)
                {
                    MessageService.MarkMessageRead(CurrentMessage);
                    PopupHelper.RefreshParent();
                }
                MapRelatedData();

                MessageAction = MessageActionService.GetAction(CurrentMessage.MessageTemplate);
                if (! Page.IsPostBack)
                {
                    ToggleButtons();

                    subjectLbl.Text = CurrentMessage.Subject;
                    fromLbl.Text = CurrentMessage.From.EmailAddress().BuildLinks();
                    toLbl.Text = CurrentMessage.To.EmailAddress().BuildLinks();
                    dateLbl.Text = CurrentMessage.Date.ToLocalTime().ToShortDateString();
                    contentLbl.Text = CurrentMessage.Content; //.BuildLinks();
                }
            }
        }

        private void MapRelatedData()
        {
            if (CurrentMessage is OrganizationMessage)
            {
                var orgMessage = CurrentMessage as OrganizationMessage;
                if (null != orgMessage && null != orgMessage.RelatedOrg)
                {
                    var control = (ShowOrg)LoadControl("~/Admin/ShowOrg.ascx");
                    control.Organization = orgMessage.RelatedOrg;
                    relatedInfo.Controls.Add(control);
                    return;
                }
            }
            
            if (CurrentMessage is ScholarshipMessage)
            {
                if (CurrentMessage.MessageTemplate != MessageType.ScholarshipApproved)
                {
                    var scholarshipMessage = CurrentMessage as ScholarshipMessage;
                    if (null != scholarshipMessage && null != scholarshipMessage.RelatedScholarship)
                    {
                        var control = (ShowScholarship) LoadControl("~/Common/ShowScholarship.ascx");
                        control.Scholarship = scholarshipMessage.RelatedScholarship;
                        relatedInfo.Controls.Add(control);
                        return;
                    }
                }
            }
        }

        private void ToggleButtons()
        {
            if (CurrentMessage.IsArchived)
            {
				// put the approve button on a denied item - just in case it was done in error
				// or the circumstances change.
				if (CurrentMessage.ActionTaken == Domain.Messaging.MessageAction.Deny)
					approveBtn.Visible = true;
				else
				{
					approveBtn.Visible = false;
				}
					
                archiveBtn.Visible = false;
                rejectBtn.Visible = false;
            }
            else
            {
                approveBtn.Visible = MessageAction.SupportsApprove;
                rejectBtn.Visible = MessageAction.SupportsReject;
				if (MessageAction.SupportsApprove && MessageAction.SupportsReject)
					archiveBtn.Visible = false;
            }
        }

        protected void approveBtn_Click(object sender, EventArgs e)
        {
            MessageAction.Approve(CurrentMessage, ConfigHelper.GetMailParams(null), UserContext.CurrentUser);
            MessageService.ArchiveMessage(CurrentMessage);

            SuccessMessageLabel.SetMessage(MessageAction.LastStatus + " - Message archived.");
            PopupHelper.CloseSelf(true);
            
        }

        protected void rejectBtn_Click(object sender, EventArgs e)
        {
            MessageAction.Reject(CurrentMessage, ConfigHelper.GetMailParams(null), UserContext.CurrentUser);

            SuccessMessageLabel.SetMessage(MessageAction.LastStatus);
            PopupHelper.CloseSelf(true);
        }

        protected void archiveBtn_Click(object sender, EventArgs e)
        {
            MessageService.ArchiveMessage(CurrentMessage);

            SuccessMessageLabel.SetMessage("Message archived.");
            
            PopupHelper.CloseSelf(true);
        }

         
    }
}