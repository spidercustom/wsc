﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowScholarship.ascx.cs"
    Inherits="ScholarBridge.Web.Common.ShowScholarship" %>
<%@ Register Src="~/Common/GeneralInfoShow.ascx" TagName="GeneralInfoShow" TagPrefix="sb" %>
<%@ Register Src="~/Common/SeekerInfoShow.ascx" TagName="SeekerInfoShow" TagPrefix="sb" %>
<%@ Register Src="~/Common/FundingInfoShow.ascx" TagName="FundingInfoShow" TagPrefix="sb" %>
<%@ Register Src="~/Common/AdditionalCriteriaShow.ascx" TagName="AdditionalCriteriaShow"
    TagPrefix="sb" %>

<div class="tabs">
    <ul class="linkarea"   runat="server" visible="<%#!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this.Page) %>">
        <li><a href="#generalinfo-tab"><span>General Information</span></a></li>
        <li><a href="#seekerprofile-tab"><span>Seeker Profile</span></a></li>
        <li><a href="#fundingprofile-tab"><span>Financial Need</span></a></li>
        <li><a href="#additionalcriteria-tab"><span>+Requirements</span></a></li>
        
    </ul>
    <h2 class="only-in-print">
        Scholarship Information For
        <asp:Literal ID="scholarshipName" runat="server" /></h2>
    <div id="generalinfo-tab">
        <sb:GeneralInfoShow ID="GeneralInfoShow1" runat="server" />
    </div>
    <div id="seekerprofile-tab">
        <sb:SeekerInfoShow ID="SeekerInfoShow1" runat="server" />
    </div>
    <div id="fundingprofile-tab">
        <sb:FundingInfoShow ID="FundingInfoShow1" runat="server" />
    </div>
    <div id="additionalcriteria-tab">
        <sb:AdditionalCriteriaShow ID="AdditionalCriteriaShow1" runat="server" />
    </div>
   
</div>
