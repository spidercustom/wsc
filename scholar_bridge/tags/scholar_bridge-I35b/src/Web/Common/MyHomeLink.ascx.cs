﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class MyHomeLink : UserControl
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.IsInRole(Role.WSC_ADMIN_ROLE))
            {
                LinkToMyHome.NavigateUrl = "~/Admin/";
            }
            else if (HttpContext.Current.User.IsInRole(Role.PROVIDER_ROLE))
            {
                LinkToMyHome.NavigateUrl = "~/Provider/";
            }
            else if (HttpContext.Current.User.IsInRole(Role.INTERMEDIARY_ROLE))
            {
                LinkToMyHome.NavigateUrl = "~/Intermediary/";
            }
            else
            {
                LinkToMyHome.NavigateUrl = "~/Seeker/";
            }
        }
    }
}

