﻿

namespace ScholarBridge.Business
{
	public interface IScholarshipListService
	{
		string GetScholarshipList();
		void GenerateAndStoreScholarshipList();
	}
}
