using System;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    sealed class BlankAction : MatchAction
    {
        public static readonly MatchAction Instance = new BlankAction();

        private BlankAction()
        {
            Text = String.Empty;
        }

        public override void Execute(Match match) { }
    }
}