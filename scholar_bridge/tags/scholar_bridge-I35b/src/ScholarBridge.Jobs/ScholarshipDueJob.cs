using System;
using Common.Logging;
using Quartz;
using ScholarBridge.Business;

namespace ScholarBridge.Jobs
{
    public class ScholarshipDueJob : NHibernateBaseJob
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NotifySeekersOfClosedScholarship));

        private IScholarshipService scholarshipService;

        // Set only so it doesn't get serialized
        public IScholarshipService ScholarshipService { set { scholarshipService = value; } }

        // This could be injected as a list of ints if it needs to be configurable.
        // see the RemoveNonConfirmed for example
        private static readonly int[] DAYS = {10, 3};

        protected override void ExecuteTransactional(JobExecutionContext ctx)
        {
            log.Info("Begin ScholarshipDueJob");

            foreach (var days in DAYS)
            {
                log.Info(String.Format("Notifying seekers of Scholarships due in '{0}'", days));

                scholarshipService.NotifySeekersScholarshipDue(days);
            }

            log.Info("End ScholarshipDueJob");
        }
    }
}