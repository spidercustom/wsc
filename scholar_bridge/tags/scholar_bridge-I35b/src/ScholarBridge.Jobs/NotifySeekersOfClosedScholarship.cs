using System;
using Common.Logging;
using Quartz;
using ScholarBridge.Business;

namespace ScholarBridge.Jobs
{
    public class NotifySeekersOfClosedScholarship : NHibernateBaseJob
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NotifySeekersOfClosedScholarship));

        private IScholarshipService scholarshipService;

        // Set only so it doesn't get serialized
        public IScholarshipService ScholarshipService { set { scholarshipService = value; } }

        protected override void ExecuteTransactional(JobExecutionContext ctx)
        {
            log.Info("Begin NotifySeekersOfClosedScholarship");

            var date = DateTime.Now.AddDays(-1);
            if (ctx.PreviousFireTimeUtc.HasValue)
            {
                date = ctx.PreviousFireTimeUtc.Value.ToLocalTime();
            }

            log.Info(String.Format("Notifying seekers of closed Scholarships since '{0}'", date));

            scholarshipService.NotifySeekersClosedSince(date);

            log.Info("End NotifySeekersOfClosedScholarship");
        }
    }
}