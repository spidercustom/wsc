﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Admin.Editors.Resources
{
	public partial class ResourceList : System.Web.UI.UserControl
	{
		public const string LinkTo = "~/Resources/Show.aspx";
		public IResourceService ResourceService { get; set; }
		public IUserContext UserContext { get; set; }


		protected void Page_Load(object sender, EventArgs e)
		{
			MainMenuHelper.SetupActiveMenuKey(Page, MainMenuHelper.MainMenuKey.AdminResources);
			UserContext.EnsureUserIsInContext();
			if (!IsPostBack)
				Bind();
		}

		protected void Bind()
		{
			var resources = ResourceService.FindAll();
			lstResources.DataSource = resources;
			lstResources.DataBind();
		}

		protected void lstResources_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var resource = ((Resource)((ListViewDataItem)e.Item).DataItem);
				//var link = (HyperLink)e.Item.FindControl("linktoResource");
				//link.NavigateUrl = LinkTo + "?id=" + resource.Id;

				var chk = (CheckBox)e.Item.FindControl("chkResource");
				chk.Attributes.Add("value", resource.Id.ToString());
				var btnDelete = (ConfirmButton)e.Item.FindControl("SingleDeleteBtn");
				btnDelete.Attributes.Add("value", resource.Id.ToString());
				var btnEdit = (AnchorButton)e.Item.FindControl("SingleEditBtn");
				btnEdit.Attributes.Add("value", resource.Id.ToString());
			}
		}

		protected void lstResources_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			Bind();
		}

		protected void DeleteBtn_Click(object sender, ConfirmButtonClickEventArgs e)
		{
			if (!e.DialogResult) return;
			bool rebind = false;
			for (int i = 0; i < lstResources.Items.Count; i++)
			{
				var chk = (CheckBox)lstResources.Items[i].FindControl("chkResource");
				if (chk.Checked)
				{
					var resourceid = int.Parse(chk.Attributes["value"]);
					var resource = ResourceService.GetById(resourceid);
					if (!(resource == null))
						ResourceService.Delete(resource);
					rebind = true;
				}
			}

			if (rebind)
				Bind();
		}

		protected void SingleDeleteBtn_Click(object sender, ConfirmButtonClickEventArgs e)
		{
			if (!e.DialogResult) return;
			var btn = (ConfirmButton)sender;
			var resourceid = int.Parse(btn.Attributes["value"]);
			var resource = ResourceService.GetById(resourceid);
			if (!(resource == null))
				ResourceService.Delete(resource);
			Bind();
		}

		protected void SingleEditBtn_Click(object sender, EventArgs e)
		{
			var btn = (AnchorButton)sender;
			var resourceid = int.Parse(btn.Attributes["value"]);
			resourceEntry.ResourceID = resourceid;
			ShowAddUpdatePanel();
		}

		protected void pager_PreRender(object sender, EventArgs e)
		{
			if (pager.TotalRowCount <= pager.PageSize)
			{
				pager.Visible = false;
			}
		}

		protected void createResourceLnk_Click(object sender, EventArgs e)
		{

			resourceEntry.ResourceID = null;
			ShowAddUpdatePanel();
		}

		private void ShowAddUpdatePanel()
		{
			resourceListPanel.Visible = false;
			addUpdatePanel.Visible = true;
		}

		protected void ResourceEntry1_OnFormSaved(Resource resource)
		{
			HideAddUpdatePanel();
			Bind();
		}

		private void HideAddUpdatePanel()
		{
			resourceListPanel.Visible = true;
			addUpdatePanel.Visible = false;
		}

		protected void ResourceEntry1_OnFormCanceled()
		{
			HideAddUpdatePanel();
		}
	}
}