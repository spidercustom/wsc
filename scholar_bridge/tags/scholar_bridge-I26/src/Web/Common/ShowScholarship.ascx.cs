﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class ShowScholarship : UserControl
    {
        public string LinkTo { get; set; }
        public string ApplicationLinkTo { get; set; }
        public Scholarship Scholarship { get; set; }
        public string LinkToEditProgramGuideLines { get; set; }
        public string LinkToEditSchedule { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            GeneralInfoShow1.ScholarshipToView = Scholarship;
            GeneralInfoShow1.LinkTo = LinkTo;
            GeneralInfoShow1.LinkToEditProgramGuidelines = LinkToEditProgramGuideLines;
            GeneralInfoShow1.LinkToEditSchedule = LinkToEditSchedule;
            SeekerInfoShow1.ScholarshipToView = Scholarship;
            SeekerInfoShow1.LinkTo = LinkTo;

			scholarshipName.Text = Scholarship.Name;
            FundingInfoShow1.ScholarshipToView = Scholarship;
            FundingInfoShow1.LinkTo = LinkTo;

            AdditionalCriteriaShow1.ScholarshipToView = Scholarship;
            AdditionalCriteriaShow1.LinkTo = LinkTo;

            

            ScholarshipNotes.Scholarship = Scholarship;

            //ScholarshipNotesTabPageContainer.Visible =
            //    ScholarshipNotesTitleContainer.Visible =
            //    !Page.IsInPrintView();

            var applicantsControl = (ScholarshipApplicants)applicantTabContentLoginView.FindControl("ScholarshipApplicants1");
            if (null != applicantsControl)
            {
                applicantsControl.Scholarship = Scholarship;
                applicantsControl.LinkTo = ApplicationLinkTo;

                if (!Page.IsPostBack)
                {
                    var applicantsLbl = (HtmlGenericControl)applicantTitleLoginView.FindControl("applicantsLbl");
                    if (null != applicantsLbl)
                    {
                        var count = applicantsControl.ApplicationCount;
                        applicantsLbl.InnerHtml += String.Format("&nbsp;<span style=\"color: green;\">{0}</span>", count);
                    }
                }
                if (Page.IsInPrintView())
                    applicantsControl.Visible = false;
            }
        }
         
    }
}