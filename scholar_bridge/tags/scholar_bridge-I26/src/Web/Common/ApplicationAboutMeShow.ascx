﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationAboutMeShow.ascx.cs" Inherits="ScholarBridge.Web.Common.ApplicationAboutMeShow" %>

<div>
<div class="viewLabel">Personal Statement:</div> <div class="viewValue"><asp:Literal ID="personalStatement" runat="server" /></div>
<br />
</div>

<div>
<div class="viewLabel">My Challenge:</div> <div class="viewValue"><asp:Literal ID="myChallenge" runat="server" /></div>
<br />
</div>

<div>
<div class="viewLabel">My Gift:</div> <div class="viewValue"><asp:Literal ID="myGift" runat="server" /></div>
<br />
</div>

<div>
<div class="viewLabel">5 Words:</div> <div class="viewValue"><asp:Literal ID="words" runat="server" /></div>
<br />
</div>

<div>
<div class="viewLabel">5 Skills:</div> <div class="viewValue"><asp:Literal ID="skills" runat="server" /></div>
<br />
</div>



<br />
