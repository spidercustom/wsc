﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web
{
    public partial class SendToFriend : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        
        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["id"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            var currentScholarship = ScholarshipService.GetById(ScholarshipId);
            if (null == currentScholarship)
                throw new ArgumentNullException("scholarship");

            ScholarshipPublicView1.Scholarship = currentScholarship;
             
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            
            var currentScholarship = ScholarshipService.GetById(ScholarshipId);
            ScholarshipService.SendToFriend(currentScholarship,CommentBox.Text,EmailBox.Text,UserContext.CurrentUser,ConfigHelper.GetDomainName());
            ClientSideDialogs.ShowAlertNative("Scholarship successfully sent to {0}".Build(EmailBox.Text),"Send to friend");
            PopupHelper.CloseSelf(false);
        }
    }
}
