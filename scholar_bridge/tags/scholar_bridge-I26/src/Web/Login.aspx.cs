﻿using System;
using System.Web;

namespace ScholarBridge.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
            	string[] names = HttpContext.Current.User.Identity.Name.Split('!');
                Response.Redirect(loginForm.DestinationForUser(names[0]));
            }
        }
    }
}
