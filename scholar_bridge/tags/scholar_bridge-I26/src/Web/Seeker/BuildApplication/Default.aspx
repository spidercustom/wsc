﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.BuildApplication.Default"  Title="Build Application" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="~/Seeker/BuildApplication/Basics.ascx" tagname="Basics" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/AboutMe.ascx" tagname="AboutMe" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/AcademicInformation.ascx" tagname="AcademicInfo" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/Activities.ascx" tagname="Activities" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/FinancialNeed.ascx" tagname="FinancialNeed" tagprefix="sb" %>
<%@ Register src="~/Seeker/BuildApplication/AdditionalCriteria.ascx" tagname="AdditionalCriteria" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image3" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image4" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div id="confirmSaving" title="Confirm saving" style="display:none">
        You have changed the data. Would you like to save the changes?
    </div>
     <div id="confirmDelete" title="Confirm Delete" style="display:none">
        Scholarship application will be deleted permanently. Are you sure want to delete?
    </div>
    <div id="confirmSubmit" title="Confirm Submission" style="display:none">
        The scholarship application will be saved and can not be edited once submitted. 
        <br />
        <br />
        Please certify that all information provided in the application is true and accurate to the best of your knowledge.
        <br />
        <br />
        <br />
        <p>Do you want to continue and submit?</p>
    </div>
    <br />
    <!--Left floated content area starts here-->
    <div id="HomeContentLeft">
      <Img src="<%= ResolveUrl("~/images/PgTitle_MyProfile.gif") %>" width="249px" height="54px">
      <img src="<%= ResolveUrl("~/images/EmphasisedMyProfilePage.gif") %>" width="513px" height="79px">
      </div>
      <!--Left floated content area ends here-->

     <!--Right Floated content area starts here-->
     <div id="HomeContentRight">
     <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
     </div>
    <BR><br/>

    <div id="ButtonWrapper">
        <div id="ButtonContainerLeft">
        <sbCommon:AnchorButton ID="DeleteButton" runat="server" OnClick="DeleteButton_Click" Text="Delete" CausesValidation="false"/>
        <sbCommon:SaveConfirmButton ID="SubmitButton" OnClick="SubmitButton_Click" runat="server" ConfirmMessageDivID="confirmSaving" Text="Submit"/>
        </div>
        <div id="ButtonContainerRight"></div>
    </div>
    <div id="Clear"></div>
    <div class="tabs" id="BuildApplicationWizardTab">
        <ul class="linkarea">
            <li><a  href="#tab"><span>Basics</span></a></li>
            <li><a  href="#tab"><span>About Me</span></a></li>
            <li><a href="#tab"><span>My Academic Info</span></a></li>
            <li><a href="#tab"><span>My Activities</span></a></li>
            <li><a href="#tab"><span>My Financial Need</span></a></li>
            <li><a href="#tab"><span>+Requirements</span></a></li>
        </ul>
         
        <div id="tab">
            <asp:MultiView  ID="BuildApplicationWizard" 
                            runat="server"
                            OnActiveViewChanged="BuildApplicationWizard_ActiveStepChanged">
              <asp:View ID="BasicInfoStep" runat="server">
                    <sb:Basics ID="Basics1" runat="server" />
              </asp:View>
              <asp:View ID="AboutMeStep" runat="server">
                    <sb:AboutMe ID="AboutMe1" runat="server" />
              </asp:View>
              <asp:View ID="AcademicStep" runat="server">
                    <sb:AcademicInfo ID="AcademicInfo1" runat="server" />
              </asp:View>
              <asp:View ID="ActivitiesStep" runat="server">
                    <sb:Activities ID="Activities1" runat="server"  />
              </asp:View>
              <asp:View ID="FinancialNeed" runat="server">
                    <sb:FinancialNeed ID="FinancialNeed1" runat="server" />
              </asp:View>
              <asp:View ID="AdditionalCriteriaStep" runat="server">
                    <sb:AdditionalCriteria ID="AdditionalCriteria" runat="server" />
              </asp:View>
            </asp:MultiView>
        </div>
        <sbCommon:jQueryTabIntegrator id="buildApplicationWizardTabIntegrator" 
                                runat="server" 
                                CausesValidation="true"
                                MultiViewControlID="BuildApplicationWizard"
                                TabControlClientID="BuildApplicationWizardTab" />

    </div>
    
    <div id="SubmissionValidationErrors" class="issueList" title="Application submission" style="display:none">
        <asp:Repeater ID="IssueListControl" runat="server">
            <HeaderTemplate><ul></HeaderTemplate>
            <ItemTemplate><li><%# DataBinder.Eval(Container.DataItem, "Message") %></li></ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
        <br />
        <p>
            All issues specified in above list must to be fixed for application that is submitting or is submitted.
        </p>
    </div>
    <div id="Clear"></div>
    
    <div id="ButtonWrapper">
        <div id="ButtonContainerLeft">
            <sbCommon:SaveConfirmButton ID="SaveAndExitButton" ConfirmMessageDivID="confirmSaving" NeedsConfirmation="false" OnClick="SaveAndExit_Click" runat="server" Text="Save & Exit" />
            
        </div>
        <div id="ButtonContainerRight">
            <sbCommon:SaveConfirmButton ID="PreviousButton" runat="server" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" OnClick="PreviousButton_Click" Text="Previous"/>
            <sbCommon:SaveConfirmButton ID="NextButton" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" OnClick="NextButton_Click" runat="server" Text="Next" />
        </div>
    </div>
    <!-- hack to get the WebForm_DoPostBackWithOptions to be emitted -->
    <span style="visibility:hidden"><sbCommon:AnchorButton ID="SaveButton" OnClick="SaveButton_Click" runat="server"/></span>
</asp:Content>
