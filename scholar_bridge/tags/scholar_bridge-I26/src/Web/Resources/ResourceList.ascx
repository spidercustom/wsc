﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceList.ascx.cs" 
Inherits="ScholarBridge.Web.Resources.ResourceList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<asp:ListView ID="resourceList" runat="server" 
    OnItemDataBound="resourceList_ItemDataBound" >
    <LayoutTemplate>
        <asp:PlaceHolder id="itemPlaceholder" runat="server" />
    </table>
    
    </LayoutTemplate>
          
    <ItemTemplate>
        <table>
            <tr>
                <td style='width: 250px;'></td>
                <td><hr style="width: 300px;" /></td>
            </tr>
        </table>
        <h4><%# Eval("Title")%></h4>
        <p>
            </p><asp:HyperLink ID="resourceUrl" runat="server" Text='<%# Eval("URL") %>' NavigateUrl='<%# Eval("URL") %>' ></asp:HyperLink>
            <asp:LinkButton ID="resourceFileLinkButton" CommandName="ShowFile" CommandArgument='<%# Eval("Id") %>' OnCommand="resourceFileLinkButton_OnCommand" runat="server"><%# Eval("AttachedFile.Name") %></asp:LinkButton>
        </p>
        <p><asp:label ID="descriptionLabel" runat="server" text='<%# Eval("Description") %>'></asp:label></p>
    </ItemTemplate>
    <EmptyDataTemplate>
    <p>There are currently no resources to display. </p>
    </EmptyDataTemplate>
</asp:ListView>

