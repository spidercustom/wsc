﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
	public enum MatchApplicationStatus
	{
		Unknown,

		[DisplayName("Not Started")]
		NotApplied,

		[DisplayName("Started")]
		Applying,

		[DisplayName("Submitted")]
		Applied,

		[DisplayName("Being considered")]
		BeingConsidered,

		Offered,
		Closed,
		Awarded
	}
}
