﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(44)]
    public class AddSeekerHobby : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "SeekerHobby";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
