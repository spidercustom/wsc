﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(240)]
	public class TrimTheCollegeListPerHECB : Migration
	{
		public override void Up()
		{
			Database.ExecuteNonQuery(collegeUpdateSQL);
		}

		public override void Down()
		{
			// can't really revert this change
		}

		private const string collegeUpdateSQL =
			@"

	create table #tempColleges(
		ID          int,
		name        nvarchar (100))

	create table #tempDeleteColleges
		(sbcollegeIndex int)
		
	insert into #tempColleges
	(id, name)
	select -1, 'A.T. Still University' union all
	select 19, 'Antioch University' union all
	select 97, 'Apollo College' union all
	select 187, 'Argosy University' union all
	select -1, 'Armour Bible College and Armour Seminary' union all
	select 118, 'Art Institute of Seattle' union all
	select -1, 'Ashford University' union all
	select -1, 'Bainbridge Graduate Institute' union all
	select 54, 'Bakke Graduate University' union all
	select 20, 'Bastyr University' union all
	select 91, 'Bates Technical College' union all
	select 802, 'Baylor University' union all
	select 60, 'Bellevue Community College' union all
	select 92, 'Bellingham Technical College' union all
	select 61, 'Big Bend Community College' union all
	select -1, 'Bishop A.L. Hardy Academy of Theology' union all
	select -1, 'Calvary Chapel Bible College' union all
	select -1, 'Calvary Lighthouse Bible Institute' union all
	select -1, 'Capella University' union all
	select -1, 'Cascade Bible College' union all
	select 89, 'Cascadia Community College' union all
	select 191, 'Central Texas College' union all
	select 9, 'Central Washington University' union all
	select 62, 'Centralia College' union all
	select 36, 'Chapman University' union all
	select -1, 'Charter College - Pasco' union all
	select -1, 'Christos Institute' union all
	select 34, 'City University' union all
	select 63, 'Clark College' union all
	select 93, 'Clover Park Technical College' union all
	select -1, 'College for Global Deployment' union all
	select 323, 'Collins College' union all
	select 64, 'Columbia Basin College' union all
	select 537, 'Columbia College - MO' union all
	select -1, 'Columbia Evangelical Seminary' union all
	select 321, 'Cornell University' union all
	select 21, 'Cornish College of Arts' union all
	select -1, 'Covenant Bible Seminary' union all
	select 255, 'Devry University' union all
	select 46, 'Digipen Institute of Technology' union all
	select 10, 'Eastern Washington University' union all
	select 65, 'Edmonds Community College' union all
	select 916, 'Embry-Riddle Aeronautical University - WA' union all
	select 864, 'Everest College - Bremerton' union all
	select 894, 'Everest College - Everett' union all
	select 117, 'Everest College - Renton' union all
	select 893, 'Everest College - Tacoma' union all
	select -1, 'Everest College - Vancouver' union all
	select 66, 'Everett Community College' union all
	select -1, 'Evergreen Christian University' union all
	select 59, 'Faith Evangelical Seminary' union all
	select -1, 'First Christian Seminary' union all
	select 8, 'Fred Hutchinson Cancer Research Center' union all
	select 170, 'Fuller Theological Seminary - Northwest' union all
	select -1, 'Goddard College' union all
	select 178, 'Golden Gate Baptist Theological Seminary' union all
	select 171, 'Golden Gate University - Seattle' union all
	select 23, 'Gonzaga University' union all
	select 791, 'Grand Canyon University' union all
	select 69, 'Grays Harbor College' union all
	select 70, 'Green River Community College' union all
	select -1, 'Heart 4 the Nations Bible School and Ministry Training Center' union all
	select 22, 'Heritage University' union all
	select 71, 'Highline Community College' union all
	select -1, 'Hope International University' union all
	select -1, 'Horizon College of Ministry' union all
	select -1, 'HUMUH Transcendental Awareness Institute' union all
	select -1, 'The Institute for Biblical Studies' union all
	select -1, 'The Institute for Christian Works' union all
	select 113, 'Interface College' union all
	select 196, 'International Academy of Design and Technology' union all
	select -1, 'International College of Metaphysical Theology' union all
	select -1, 'International Graduate School of Ministry' union all
	select 457, 'ITT Technical Institute - Everett' union all
	select -1, 'ITT Technical Institute - Indianapolis' union all
	select 107, 'ITT Technical Institute - Seattle' union all
	select 108, 'ITT Technical Institute - Spokane' union all
	select -1, 'KAES Bible College & Seminary' union all
	select 157, 'Kaplan College' union all
	select -1, 'Kepler College' union all
	select -1, 'KPCA Northwest Presbyterian Theological Seminary' union all
	select 94, 'Lake Washington Technical College' union all
	select 222, 'Lesley University' union all
	select 377, 'Lewis and Clark College' union all
	select -1, 'Life Ministry Institute ' union all
	select -1, 'Life Pacific College' union all
	select -1, 'Lincoln College of Technology' union all
	select -1, 'Living Faith Fellowship College of Ministry' union all
	select -1, 'The Lorian Center for Incarnational Spirituality' union all
	select 72, 'Lower Columbia College' union all
	select 18, 'Mars Hill Graduate School' union all
	select -1, 'Missio Dei Learning Community' union all
	select 141, 'Moody Bible Institute - Spokane' union all
	select -1, 'North Park Theological Seminary' union all
	select 77, 'North Seattle Community College' union all
	select 102, 'Northwest Aviation College' union all
	select 48, 'Northwest Baptist Seminary' union all
	select 35, 'Northwest College of Art' union all
	select -1, 'Northwest Institute of Literary Arts/Whidbey Writers Workshop' union all
	select 88, 'Northwest Indian College' union all
	select -1, 'Northwest School of Wooden Boatbuilding' union all
	select -1, 'Northwest Theological Seminary' union all
	select 24, 'Northwest University' union all
	select 737, 'Nova Southeastern University' union all
	select 719, 'Old Dominion University' union all
	select 74, 'Olympic College' union all
	select 357, 'Oregon Health and Science' union all
	select 25, 'Pacific Lutheran University' union all
	select -1, 'Pacific Northwest Bible College' union all
	select 200, 'Pacific Northwest University of Health Sciences' union all
	select 58, 'Pacific Theological Seminary' union all
	select 176, 'Park University - Fairchild' union all
	select 75, 'Peninsula College' union all
	select 68, 'Pierce College - Puyallup' union all
	select -1, 'Pierce College - Fort Steilacoom' union all
	select 131, 'Pima Medical Institute' union all
	select -1, 'Portland Bible College' union all
	select 13, 'Portland State University' union all
	select -1, 'Renewed Life Seminary' union all
	select 95, 'Renton Technical College' union all
	select -1, 'Resurgence Training Center' union all
	select 26, 'Saint Martin''s University' union all
	select -1, 'Saybrook Graduate School and Research Center' union all
	select 52, 'Seattle Bible College' union all
	select 76, 'Seattle Central Community College' union all
	select -1, 'Seattle Institute of Oriental Medicine' union all
	select 27, 'Seattle Pacific University' union all
	select -1, 'Seattle Theological Seminary' union all
	select 28, 'Seattle University' union all
	select -1, 'Seattle Urban Bible College' union all
	select -1, 'Shepherds Bible College' union all
	select 79, 'Shoreline Community College' union all
	select 80, 'Skagit Valley Community College' union all
	select -1, 'Sophia Divinity School' union all
	select -1, 'Sound Baptist Bible College' union all
	select 73, 'South Puget Sound Community College' union all
	select 78, 'South Seattle Community College' union all
	select 382, 'Southern Illinois University' union all
	select 81, 'Spokane Community College' union all
	select 82, 'Spokane Falls Community College' union all
	select -1, 'St. Anthony''s University' union all
	select -1, 'Tacoma Bible College ' union all
	select 83, 'Tacoma Community College' union all
	select 11, 'The Evergreen State College' union all
	select 55, 'Trinity Lutheran College' union all
	select 312, 'Trinity Western University' union all
	select -1, 'Triune Biblical University' union all
	select 195, 'Troy University' union all
	select -1, 'United Theological Seminary and Bible College' union all
	select -1, 'Universal Technical Institute' union all
	select -1, 'University of Christian Studies and Seminary' union all
	select -1, 'University of Phoenix' union all
	select 30, 'University of Puget Sound' union all
	select 1, 'University of Washington' union all
	select 3, 'University of Washington - Bothell' union all
	select 2, 'University of Washington - Tacoma' union all
	select 126, 'Vincennes University' union all
	select 709, 'Walden University' union all
	select 84, 'Walla Walla Community College' union all
	select 31, 'Walla Walla University' union all
	select 278, 'Warner Pacific College' union all
	select -1, 'The Washington Bible Institute' union all
	select -1, 'Washington College and International Seminary' union all
	select -1, 'Washington Seminary ' union all
	select 4, 'Washington State University' union all
	select 5, 'Washington State University - Tri-Cities' union all
	select 6, 'Washington State University - Vancouver' union all
	select 188, 'Webster University' union all
	select 85, 'Wenatchee Valley College' union all
	select 766, 'Western Culinary Institute' union all
	select 378, 'Western Oregon University' union all
	select 49, 'Western Reformed Seminary' union all
	select 12, 'Western Washington University' union all
	select 86, 'Whatcom Community College' union all
	select 32, 'Whitman College' union all
	select 33, 'Whitworth University' union all
	select -1, 'Wisdom Bible Institute' union all
	select -1, 'Wisdom for Life Leadership School' union all
	select -1, 'Woolston-Steen Theological Seminary' union all
	select -1, 'Worship Arts Conservatory' union all
	select -1, 'WyoTech' union all
	select 87, 'Yakima Valley Community College'

	--select * from #tempColleges
	declare @updater int
	select @updater = sbuserid from SBUser 
	where Username = 'wscadmin@scholarbridge.com'

	insert into #tempDeleteColleges
	select sbcollegeIndex from SBCollegeLUT
	where  SBCollegeIndex not in (select id from #tempColleges)

	declare @toDeleteCount int

	select @toDeleteCount = COUNT(*) from #tempDeleteColleges

	while @toDeleteCount > 0
	begin
		declare @deleteID int
		select top 1 @deleteID = sbcollegeIndex from #tempDeleteColleges 
		
		begin try
			delete from SBCollegeLUT
			where SBCollegeIndex = @deleteID 
		end try
		begin catch
			print 'delete error caught'
		end catch
			
		delete from #tempdeletecolleges
		where sbcollegeIndex = @deleteID
		
		select @toDeleteCount = COUNT(*) from #tempdeletecolleges
	end
	drop table #tempDeleteColleges


	declare @toAddUpdateCount int
	select @toAddUpdateCount = COUNT(*) from #tempColleges

	while @toAddUpdateCount > 0
	begin
		declare @id int
		declare @name varchar(100)
		
		select top 1 @id = ID, @name = name from #tempColleges 
		
		--begin transaction
		
			declare @count int
			if @id = -1
			begin
				print 'inserting.. (1)'
				insert into SBCollegeLUT 
				(College, Description, Deprecated, LastUpdateBy, LastUpdateDate)
				values (@name, @name, 0, @updater, GETDATE()) 
			end
			else
			begin
				select @count = COUNT(*) from SBCollegeLUT 
				where SBCollegeIndex = @id
				if @count > 0
				begin
				print 'updating.. (1)'
					update SBCollegeLUT 
					set College = @name, Description = @name 
					where SBCollegeIndex = @id
				end
				else
				begin
				print 'inserting.. (2)'
					insert into SBCollegeLUT 
					(College, Description, Deprecated, LastUpdateBy, LastUpdateDate)
					values (@name, @name, 0, @updater, GETDATE()) 
				end
			end

		--rollback transaction
		
		delete from #tempColleges 
		where ID = @id and name = @name

		select @toAddUpdateCount = COUNT(*) from #tempColleges
	end

	drop table #tempColleges
";
	}
}
