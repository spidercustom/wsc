using System;
using System.Collections.Generic;
using System.Configuration;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class MessageService : IMessageService
    {
        public IMessageDAL MessageDAL { get; set; }
		public IMessagingService MessagingService { get; set; }
		public ITemplateParametersService TemplateParametersService { get; set; }
		public ISentMessageDAL SentMessageDAL { get; set; }
		public IUserService UserService { get; set; }

		public const string WASHBOARD_PROBLEM_EMAIL_CONFIG_KEY = "WashBoardProblemEmail";
		public const string WASHBOARD_SUPPORT_EMAIL_CONFIG_KEY = "WashBoardSupportEmail";
		public const string WASHBOARD_SUGGESTION_EMAIL_CONFIG_KEY = "WashBoardSuggestionEmail";

		public Message FindMessage(User user, Organization organization, int id)
        {
            return MessageDAL.FindById(user, user.Roles, organization, id);
        }

        public SentMessage FindSentMessage(User user, Organization organization, int id)
        {
            return SentMessageDAL.FindById(user, user.Roles, organization, id);
        }

        public IList<Message> FindAll(User user, Organization organization)
        {
            return MessageDAL.FindAll(user, user.Roles, organization);
        }

        public IList<Message> FindAll(MessageAction messageAction, User user, Organization organization)
        {
            return MessageDAL.FindAll(messageAction, user, user.Roles, organization);
        }

        public IList<SentMessage> FindAllSent(User user, Organization organization)
        {
            return SentMessageDAL.FindAll(user, user.Roles, organization);
        }

        public IList<Message> FindAllArchived(User user, Organization organization)
        {
			return MessageDAL.FindAllArchived(user, user.Roles, organization);
		}

    	public IList<Message> FindAllApprovedScholarships(User user, Organization organization)
    	{
            return MessageDAL.FindAllByMessageType(MessageType.ScholarshipApproved, user, user.Roles, organization);
    	}

    	public IList<Message> FindAllForInbox(User user, Organization organization)
    	{
			return MessageDAL.FindAllForInbox(user, user.Roles, organization);
		}

    	public int CountUnread(User user, Organization organization)
        {
            return MessageDAL.CountUnread(user, user.Roles, organization);
        }

    	public void ContactUsSupport(string fromEmail, User fromUser, string messageText)
    	{
    		ContactUs(fromEmail, fromUser, messageText, MessageType.ContactUsSupport);
    	}

    	public void ContactUsProblem(string fromEmail, User fromUser, string messageText)
    	{
			ContactUs(fromEmail, fromUser, messageText, MessageType.ContactUsProblem);
		}

    	public void ContactUsSuggestion(string fromEmail, User fromUser, string messageText)
    	{
			ContactUs(fromEmail, fromUser, messageText, MessageType.ContactUsSuggestion);
		}

    	private void ContactUs(string fromEmailAddress, User fromUser, string messageText, MessageType messageType)
    	{
			var templateParams = new MailTemplateParams();
    		templateParams.From = fromEmailAddress;
    		templateParams.To = GetContactUsRecipientEmailAddress(messageType);

    		User fromUserChecked = fromUser ?? UserService.FindByUsername(Business.UserService.ANONYMOUS_USER_ID);

    		TemplateParametersService.ContactUs(fromEmailAddress, fromUser, messageText, templateParams);
    		var msg = new ContactUsMessage
    		          	{
    		          		MessageTemplate = messageType,
    		          		From = new MessageAddress {User = fromUserChecked},
    		          		LastUpdate =
    		          			new ActivityStamp(fromUserChecked)
						};

			MessagingService.SendMessageToAdmin(msg, templateParams, true);
		}

		private static string GetContactUsRecipientEmailAddress(MessageType type)
    	{
    		string emailConfigKey;
    		var reader = new AppSettingsReader();
			switch (type)
			{
				case MessageType.ContactUsProblem:
					emailConfigKey = WASHBOARD_PROBLEM_EMAIL_CONFIG_KEY;
					break;

				case MessageType.ContactUsSuggestion:
					emailConfigKey = WASHBOARD_SUGGESTION_EMAIL_CONFIG_KEY;
					break;

				default:
					emailConfigKey = WASHBOARD_SUPPORT_EMAIL_CONFIG_KEY;
					break;

			}
			return reader.GetValue(emailConfigKey, typeof(string)).ToString();
    	}

    	public void SendMessage(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");
            if (message.MessageTemplate==MessageType.ScholarshipApproved)
                message.IsRead = true;
            MessageDAL.Insert(message);
            SentMessageDAL.Insert(new SentMessage(message) { LastUpdate = message.LastUpdate });
        }

        public void ArchiveMessage(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");

            if (! message.IsArchived)
            {
                message.IsArchived = true;
                MessageDAL.Update(message);
            }
        }

        public void MarkMessageRead(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");

            if (! message.IsRead)
            {
                message.IsRead = true;
                MessageDAL.Update(message);
            }
        }

        public void DeletedRelatedMessages(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            MessageDAL.DeleteRelated(scholarship);
        }

        public void SaveNewSentMessage(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");
            SentMessageDAL.Insert(new SentMessage(message) { LastUpdate = message.LastUpdate });
        }
    }
}