using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IOrganizationService<T> where T : Organization
    {
        T FindById(int id);

        void SaveNewWithAdminUser(T organization, User user);

        void SaveNewUser(T organization, User user);
        void SaveNewUser(T organization, User user, bool requireResetPassword);
        void DeleteUser(T organization, User user);
        void ReactivateUser(T organization, User user);

        void Update(T organization);

		IList<T> FindPendingOrganizations();
		IList<T> FindActiveOrganizations();

        void Approve(T organization);
        void Reject(T organization);

        User FindUserInOrg(T organization, int userId);
        void SubmitListChangeRequest(T organization,User user, string ListType, string value, string reason);
    }
}