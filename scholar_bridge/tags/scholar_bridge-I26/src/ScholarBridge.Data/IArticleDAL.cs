using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IArticleDAL : IDAL<Article>
    {
        Article FindById(int id);
        Article Save(Article article);
      
    }
}