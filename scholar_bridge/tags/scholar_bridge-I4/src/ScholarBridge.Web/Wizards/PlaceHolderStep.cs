﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScholarBridge.Web.Wizards
{
    public class PlaceHolderStep<T> : IWizardStepControl<T>
    {
        private int stepIndex;
        public PlaceHolderStep(int stepIndex)
        {
            this.stepIndex = stepIndex;
        }
        #region IWizardStepControl<T> Members

        public bool ValidateStep()
        {
            return true;
        }

        public void Save() {}

        public bool ChangeNextStepIndex()
        {
            return false;
        }

        public int GetChangedNextStepIndex() { return 0; }

        public bool WasSuspendedFrom(T @object)
        {
            return false;
        }

        public bool CanResume(T @object)
        {
            return false;
        }

        private bool isComplete;
        public bool IsCompleted
        {
            get { return isComplete; }
            set { isComplete = value; }
        }

        public IWizardStepsContainer<T> Container { get; set; } 

        public void Activated() {}
        
        #endregion
    }
}
