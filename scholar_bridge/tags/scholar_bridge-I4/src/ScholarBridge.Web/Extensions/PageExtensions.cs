﻿using System.Web.UI;
using System;

namespace ScholarBridge.Web.Extensions
{
    public static class PageExtensions
    {

        public static int GetIntegerPageParameterValue(this Page page, StateBag viewState, string key, int defaultValue)
        {
            return GetIntegerPageParameterValue(page, viewState, key, false, defaultValue);
        }

        public static int GetIntegerPageParameterValue(this Page page, StateBag viewState, string key, bool throwIfNotFound, int defaultValue)
        {
            string idString = page.Request.Params[key];//retrive from parameters
            
            if (string.IsNullOrEmpty(idString)) //if not in parameters
            {
                //search in view state
                if (null == viewState[key])
                {
                    if (throwIfNotFound)
                        throw new Exception(string.Format("Cannot find parameter", key));
                    else
                        return defaultValue;
                }

                idString = viewState[key].ToString();
            }

            int result;
            if (!Int32.TryParse(idString, out result))
                throw new ArgumentException("Cannot understand value of parameter");

            return result;
        }
    }
}
