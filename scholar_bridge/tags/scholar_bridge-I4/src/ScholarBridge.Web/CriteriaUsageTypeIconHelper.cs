﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.ScholarshipParts;


namespace ScholarBridge.Web
{
    public static class CriteriaUsageTypeIconHelper
    {
        private const string MINIMUM_CRITERIA_TOOLTIP = "Will be use as minimum criteria";
        private const string PREFERENCE_TOOLTIP = "Will be use as preference";
        private const string CRITERIA_MINIMUM_ICON_PATH = "~/images/criteria_filter.png";
        private const string CRITERIA_PREFERENCE_ICON_PATH = "~/images/criteria_preference.png";

        public static void SetupAttributeUsageTypeIcon(Image iconControl, ScholarshipAttributeUsageType scholarshipAttributeUsageType)
        {
            switch (scholarshipAttributeUsageType)
            {
                case ScholarshipAttributeUsageType.NotUsed:
                    iconControl.Visible = false;
                    break;
                case ScholarshipAttributeUsageType.Preference:
                    iconControl.AlternateText = ScholarshipAttributeUsageType.Preference.ToString();
                    iconControl.ImageUrl = CRITERIA_PREFERENCE_ICON_PATH;
                    iconControl.DescriptionUrl = string.Empty;
                    iconControl.ToolTip = PREFERENCE_TOOLTIP;
                    iconControl.Visible = true;
                    break;
                case ScholarshipAttributeUsageType.Minimum:
                    iconControl.AlternateText = ScholarshipAttributeUsageType.Minimum.ToString();
                    iconControl.ImageUrl = CRITERIA_MINIMUM_ICON_PATH;
                    iconControl.DescriptionUrl = string.Empty;
                    iconControl.ToolTip = MINIMUM_CRITERIA_TOOLTIP;
                    iconControl.Visible = true;
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

    }
}