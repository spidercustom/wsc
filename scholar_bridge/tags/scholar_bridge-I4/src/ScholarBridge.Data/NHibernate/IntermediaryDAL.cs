using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class IntermediaryDAL : AbstractDAL<Intermediary>, IIntermediaryDAL
    {
        public IList<Intermediary> FindAllPending()
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("ApprovalStatus", ApprovalStatus.PendingApproval))
                .List<Intermediary>();
        }

        public Intermediary FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public Intermediary FindByUser(User user)
        {
            return CreateCriteria()
                .CreateCriteria("Users")
                .Add(Restrictions.Eq("Id", user.Id)).UniqueResult<Intermediary>();
        }

        public User FindUserInOrg(Intermediary organization, int userId)
        {
            // XXX: Couldn't figure out how to do this with Criteria
            var query = Session.CreateQuery(@"select user from Intermediary as intermediary
inner join intermediary.Users as user
where intermediary.Id=:intermediaryId AND user.id=:userId");

            query.SetInt32("intermediaryId", organization.Id);
            query.SetInt32("userId", userId);
            return query.UniqueResult<User>();
        }
    }
}