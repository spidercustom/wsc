﻿using System;
using System.Collections.Generic;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class ScholarshipDAL : AbstractDAL<Scholarship>, IScholarshipDAL
    {
       
        #region IScholarshipDAL Members
        public Scholarship FindById(int id)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Scholarship>();
        }

        public Scholarship FindByBusinessKey(Provider provider, string name, int year)
        {
            return CreateCriteria()
               .Add(Restrictions.Eq("Name", name))
               .Add(Restrictions.Eq("Provider", provider))
               .Add(Restrictions.Eq("AcademicYear.Year", year))
               .UniqueResult<Scholarship>();
        }
        public Scholarship Save(Scholarship scholarship)
        {
            if (scholarship == null)
                throw new ArgumentNullException("scholarship");

            return scholarship.Id < 1 ?
                Insert(scholarship) :
                Update(scholarship);
        }



        public IList<Scholarship> FindByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            return CreateCriteria().Add(Restrictions.Eq("Provider", provider)).List<Scholarship>(); 
        }

        public IList<Scholarship> FindByProvider(Provider provider, ScholarshipStages stage)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            return CreateCriteria().Add(Restrictions.And(
                                            Restrictions.Eq("Stage", stage),
                                            Restrictions.Eq("Provider", provider)
                                            )).List<Scholarship>();
        }

        public IList<Scholarship> FindByProviderNotActivated(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");


            return CreateCriteria().Add(Restrictions.In("Stage", GetNonActivatedStages()))
                  .Add(Restrictions.Eq("Provider", provider)).List<Scholarship>();

        }
        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages stage)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return CreateCriteria().Add(Restrictions.Eq("Stage", stage))
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary)).List<Scholarship>();


        }

        public IList<Scholarship> FindNotActivatedByOrganizations(Provider provider, Intermediary intermediary)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return CreateCriteria().Add(Restrictions.In("Stage", GetNonActivatedStages()))
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary)).List<Scholarship>();
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return CreateCriteria().Add(Restrictions.Eq("Intermediary", intermediary)).List<Scholarship>(); 
        }
        public IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStages stage)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return CreateCriteria().Add(Restrictions.And(
                                            Restrictions.Eq("Stage", stage),
                                            Restrictions.Eq("Intermediary", intermediary)
                                            )).List<Scholarship>();
        }

        public IList<Scholarship> FindByIntermediaryNotActivated(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");


            return CreateCriteria().Add(Restrictions.In("Stage", GetNonActivatedStages()))
                  .Add(Restrictions.Eq("Intermediary", intermediary)).List<Scholarship>();
        }

        
       

        #endregion
        public static ScholarshipStages[] GetNonActivatedStages()
        {
            return new[]
                       {
                           ScholarshipStages.None,
                           ScholarshipStages.GeneralInformation,
                           ScholarshipStages.MatchCriteriaSelection,
                           ScholarshipStages.SeekerProfile,
                           ScholarshipStages.FundingProfile,
                           ScholarshipStages.AdditionalCriteria,
                           ScholarshipStages.PreviewCandidatePopulation,
                           ScholarshipStages.BuildScholarshipMatchLogic,
                           ScholarshipStages.BuildScholarshipRenewalCriteria
                       };
        }

    }
}
