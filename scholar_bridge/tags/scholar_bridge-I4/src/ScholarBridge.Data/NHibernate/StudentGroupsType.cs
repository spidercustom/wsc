using NHibernate.Type;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>StudentGroups</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class StudentGroupsType : EnumStringType
    {
        public StudentGroupsType()
            : base(typeof(StudentGroups), 50)
        {
        }
    }
}