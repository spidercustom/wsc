using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using NUnit.Framework;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class PhoneNumberTests
    {
        [Test]
        public void NullPhoneNumberParsed()
        {
            var p = new PhoneNumber(null);
            Assert.AreEqual(null, p.Number);
        }

        [Test]
        public void PhoneNumberParsed()
        {
            var p = new PhoneNumber("(414) 290-8031");
            Assert.AreEqual("4142908031", p.Number);
        }

        [Test]
        public void PhoneNumberWithOneParsed()
        {
            var p = new PhoneNumber("1 (414) 290-8031");
            Assert.AreEqual("4142908031", p.Number);
        }

        [Test]
        public void PhoneNumberFormatted()
        {
            var p = new PhoneNumber("(414) 290-8031");
            Assert.AreEqual("414-290-8031", p.FormattedPhoneNumber);
            Assert.AreEqual(p.FormattedPhoneNumber, p.ToString());
        }

        [Test]
        public void PhoneNumberFormattedNull()
        {
            var p = new PhoneNumber();
            Assert.IsNull(p.FormattedPhoneNumber);
        }

        [Test]
        public void validates_good_numbers()
        {
            var phone = new PhoneNumber();

            phone.Number = "14144315593";
            Assert.IsTrue(Validation.Validate(phone).IsValid);

            phone.Number = "414 431-5593";
            Assert.IsTrue(Validation.Validate(phone).IsValid);

            phone.Number = "(414) 431-5593";
            Assert.IsTrue(Validation.Validate(phone).IsValid);

            phone.Number = "(414)431-5593";
            Assert.IsTrue(Validation.Validate(phone).IsValid);

            phone.Number = "414-431-5593";
            Assert.IsTrue(Validation.Validate(phone).IsValid);

            phone.Number = "414 431 5593";
            Assert.IsTrue(Validation.Validate(phone).IsValid);
        }

        [Test]
        public void validates_bad_numbers()
        {
            var phone = new PhoneNumber();

            phone.Number = "56";
            Assert.IsFalse(Validation.Validate(phone).IsValid);

            phone.Number = "414 431-55931";
            Assert.IsFalse(Validation.Validate(phone).IsValid);
        }
    }
}