using NUnit.Framework;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class NameTests
    {
        
        [Test]
        public void FormatNameFirstLast()
        {
            var name = new PersonName {FirstName = "Geoff", LastName = "Lane"};
            Assert.AreEqual("Geoff Lane", name.NameFirstLast);
        }

        [Test]
        public void FormatNameFirstLastWithMiddle()
        {
            var name = new PersonName { FirstName = "Geoff", LastName = "Lane", MiddleName = "M"};
            Assert.AreEqual("Geoff M Lane", name.NameFirstLast);
        }

        [Test]
        public void FormatNameLastFirst()
        {
            var name = new PersonName { FirstName = "Geoff", LastName = "Lane" };
            Assert.AreEqual("Lane, Geoff", name.NameLastFirst);
        }

        [Test]
        public void FormatNameLastFirstWithMiddle()
        {
            var name = new PersonName { FirstName = "Geoff", LastName = "Lane", MiddleName = "M" };
            Assert.AreEqual("Lane, Geoff M", name.NameLastFirst);
        }
    }
}