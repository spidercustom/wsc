using NUnit.Framework;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class AddressTests
    {
        [Test]
        public void ToStringProperlyFormatsAddressWithStreet1Only()
        {
            var a = new Address
                        {
                            Street = "123 Foo St",
                            City = "Milwaukee",
                            State = new State {Abbreviation = "WI"},
                            PostalCode = "53212"
                        };

            var expected = @"123 Foo St
Milwaukee, WI 53212";

            Assert.AreEqual(expected, a.ToString());
        }

        [Test]
        public void ToStringProperlyFormatsAddressWithBothStreets()
        {
            var a = new Address
            {
                Street = "123 Foo St",
                Street2 = "Apt 2",
                City = "Milwaukee",
                State = new State { Abbreviation = "WI" },
                PostalCode = "53212"
            };

            var expected = @"123 Foo St
Apt 2
Milwaukee, WI 53212";

            Assert.AreEqual(expected, a.ToString());
        }
    }
}