using System;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using Rhino.Mocks;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class IntermediaryServiceTests
    {
        private MockRepository mocks;
        private IntermediaryService intermediaryService;
        private IIntermediaryDAL intermediaryDAL;
        private IRoleDAL roleDAL;
        private IUserService userService;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            intermediaryDAL = mocks.StrictMock<IIntermediaryDAL>();
            roleDAL = mocks.StrictMock<IRoleDAL>();
            userService = mocks.StrictMock<IUserService>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();
            intermediaryService = new IntermediaryService
                                      {
                                          OrganizationDAL = intermediaryDAL,
                                          RoleDAL = roleDAL,
                                          UserService = userService,
                                          MessagingService = messagingService,
                                          TemplateParametersService = templateParametersService
                                      };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(intermediaryDAL);
            mocks.BackToRecord(roleDAL);
            mocks.BackToRecord(userService);
            mocks.BackToRecord(messagingService);
        }

        [Test]
        public void save_new_intermediary_sets_user_as_admin_user_with_proper_roles()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary {Name = "Test Intermediary"};
            var user = new User {Username = "NewUser"};
            Expect.Call(intermediaryDAL.Insert(intermediary)).Return(intermediary);
            Expect.Call(roleDAL.FindByName(Role.INTERMEDIARY_ROLE)).Return(new Role { Id = 3, Name = Role.INTERMEDIARY_ROLE });
            Expect.Call(roleDAL.FindByName(Role.INTERMEDIARY_ADMIN_ROLE)).Return(new Role { Id = 2, Name = Role.INTERMEDIARY_ADMIN_ROLE });
			Expect.Call(() => userService.Insert(user));
			Expect.Call(() => userService.Update(user));
			Expect.Call(() => userService.SendConfirmationEmail(user, false));
            mocks.ReplayAll();

            intermediaryService.SaveNewWithAdminUser(intermediary, user);
            mocks.VerifyAll();

            Assert.AreEqual(user, intermediary.AdminUser);
            CollectionAssert.IsNotEmpty(user.Roles);
            CollectionAssert.AllItemsAreNotNull(user.Roles);
        }

        [Test]
        public void save_new_user_sets_up_roles_and_adds_user()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary {Name = "Test Intermediary"};
            var user = new User {Username = "NewUser"};
            Expect.Call(intermediaryDAL.Update(intermediary)).Return(intermediary);
            Expect.Call(roleDAL.FindByName(Role.INTERMEDIARY_ROLE)).Return(new Role {Id = 3, Name = Role.INTERMEDIARY_ROLE});
            Expect.Call(() => userService.SendConfirmationEmail(user, false));
            mocks.ReplayAll();

            intermediaryService.SaveNewUser(intermediary, user);
            mocks.VerifyAll();

            CollectionAssert.IsNotEmpty(intermediary.ActiveUsers);
            CollectionAssert.IsNotEmpty(user.Roles);
            CollectionAssert.AllItemsAreNotNull(user.Roles);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void save_new_user_throws_exception_if_org_null()
        {
            var user = new User {Username = "NewUser"};
            intermediaryService.SaveNewUser(null, user);
            Assert.Fail();
        }

        [Test]
        public void delete_user_passes_to_user_dal()
        {
            var intermediary = new Intermediary {Name = "Test Intermediary"};
            var user = new User {Id = 1, Username = "NewUser"};
            intermediary.Users.Add(user);
            Expect.Call(() => userService.Delete(user));
            mocks.ReplayAll();

            intermediaryService.DeleteUser(intermediary, user);
            mocks.VerifyAll();
        }

        [Test]
        public void reactivate_user_passes_to_user_dal()
        {
            var intermediary = new Intermediary {Name = "Test Intermediary"};
            var user = new User {Id = 1, Username = "NewUser"};
            intermediary.Users.Add(user);
            Expect.Call(() => userService.Update(user));
            mocks.ReplayAll();

            intermediaryService.ReactivateUser(intermediary, user);
            Assert.That(user.IsDeleted, Is.False);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void SaveNullThrowsException()
        {
            intermediaryService.SaveNewWithAdminUser(null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void UpdateNullThrowsException()
        {
            intermediaryService.Update(null);
            Assert.Fail();
        }

        [Test]
        public void approve_sets_proper_statuses()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary
                                   {
                                       Name = "Test Intermediary",
                                       AdminUser = new User {Name = new PersonName {FirstName = "TestName"}}
                                   };

            Expect.Call(intermediaryDAL.Update(intermediary)).Return(intermediary);

            Expect.Call(() => templateParametersService.OrganizationApproved(Arg<User>.Is.Equal(intermediary.AdminUser), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageFromAdmin(Arg<OrganizationMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            mocks.ReplayAll();

            intermediaryService.Approve(intermediary);

            Assert.AreEqual(ApprovalStatus.Approved, intermediary.ApprovalStatus);
            Assert.IsTrue(intermediary.AdminUser.IsApproved);
            mocks.VerifyAll();
        }

        [Test]
        public void reject_sets_proper_statuses()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary
                                   {
                                       Name = "Test Intermediary",
                                       AdminUser = new User {Name = new PersonName {FirstName = "TestName"}}
                                   };

            Expect.Call(intermediaryDAL.Update(intermediary)).Return(intermediary);
            Expect.Call(() => templateParametersService.OrganizationRejected(Arg<User>.Is.Equal(intermediary.AdminUser), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageFromAdmin(Arg<OrganizationMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            mocks.ReplayAll();

            intermediaryService.Reject(intermediary);

            Assert.AreEqual(ApprovalStatus.Rejected, intermediary.ApprovalStatus);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void ApprovalRequiresNonNullProvider()
        {
            intermediaryService.Approve(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void RejectRequiresNonNullProvider()
        {
            intermediaryService.Reject(null);
            Assert.Fail();
        }
        [Test]
        public void Submit_Change_Request()
        {
            // TODO: As more things are added, mock them out as well
            var intermediary = new Intermediary() { Name = "TestIntermediary", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };


            Expect.Call(() => templateParametersService.ListChangeRequest(Arg<Organization>.Is.Equal(intermediary), Arg<User>.Is.Equal(intermediary.AdminUser), Arg<string>.Is.Anything, Arg<string>.Is.Anything, Arg<string>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageToAdmin(Arg<OrganizationMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));

            mocks.ReplayAll();

            intermediaryService.SubmitListChangeRequest(intermediary, intermediary.AdminUser, "test", "test", "test");

            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SubmitListChangeRequiresNonNullIntermediary()
        {
            intermediaryService.SubmitListChangeRequest(null, new User() { Id = 1 }, "test", "test", "test");
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SubmitListChangeRequiresNonNullUser()
        {
            intermediaryService.SubmitListChangeRequest(new Intermediary() { Id = 1 }, null, "test", "test", "test");
            Assert.Fail();
        }
    }

    
}