using System;
using NUnit.Framework;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class ScholarshipServiceTests
    {
        private MockRepository mocks;
        private ScholarshipService scholarshipService;
        private IScholarshipDAL scholarshipDAL;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;
        private IRoleDAL roleDal;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            scholarshipDAL = mocks.StrictMock<IScholarshipDAL>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();
            roleDal = mocks.StrictMock<IRoleDAL>();
            scholarshipService = new ScholarshipService
                                     {
                                         ScholarshipDAL = scholarshipDAL, 
                                         MessagingService = messagingService,
                                         TemplateParametersService = templateParametersService,
                                         RoleService = roleDal
                                     };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(scholarshipDAL);
            mocks.BackToRecord(messagingService);
            mocks.BackToRecord(templateParametersService);
            mocks.BackToRecord(roleDal);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetByProviderThrowsExceptionIfNullPassed()
        {
            scholarshipService.GetByProvider(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void GetByProviderThrowsExceptionIfProviderNotApproved()
        {
            scholarshipService.GetByProvider(new Provider { ApprovalStatus = ApprovalStatus.PendingApproval });
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveThrowsExceptionIfNullPassed()
        {
            scholarshipService.Save(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveThrowsExceptionIfNoProviderAssociated()
        {
            var scholarship = new Scholarship {Provider = null};
            scholarshipService.Save(scholarship);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void SaveThrowsExceptionIfProviderNotApproved()
        {
            var scholarship = new Scholarship { Provider = new Provider { ApprovalStatus = ApprovalStatus.PendingApproval } };
            scholarshipService.Save(scholarship);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void delete_throws_exception_if_null_passed()
        {
            scholarshipService.Delete(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void delete_throws_exception_if_cant_edit()
        {
            var s = new Scholarship {Stage = ScholarshipStages.Activated};
            Assert.IsFalse(s.CanEdit());

            scholarshipService.Delete(s);
            Assert.Fail();
        }

        [Test]
        public void can_delete_scholarship()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.BuildScholarshipMatchLogic };

            Expect.Call(() => messagingService.DeleteRelatedMessages(s));
            Expect.Call(() => scholarshipDAL.Delete(s));
            mocks.ReplayAll();

            scholarshipService.Delete(s);

            mocks.VerifyAll();
        }

        [Test]
        public void verify_scholarship_exists_by_name_and_provider_returns_true()
        {
            var name = "Scholarship-1";
            var provider = new Provider();
            var scholarship = new Scholarship { AcademicYear = new AcademicYear(2009)};
            Expect.Call(scholarshipDAL.FindByBusinessKey(provider, name, 2009)).Return(scholarship);
            mocks.ReplayAll();

            var exists = scholarshipService.ScholarshipExists(provider, name, 2009);
            mocks.VerifyAll();
            Assert.IsNotNull(exists);
        }

        [Test]
        public void verify_scholarship_exists_by_name_and_provider_returns_false()
        {
            var name = "Scholarship-1";
            var provider = new Provider();
            Expect.Call(scholarshipDAL.FindByBusinessKey(provider, name, 2008)).Return(null);
            mocks.ReplayAll();

            var exists = scholarshipService.ScholarshipExists(provider, name, 2008);
            mocks.VerifyAll();

            Assert.IsNull(exists);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_by_name_and_provider_name_arg_exception()
        {
            scholarshipService.ScholarshipExists(null, string.Empty, 2009);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_stage_and_provider_provider_arg_exception()
        {
            scholarshipService.GetByProvider(null, ScholarshipStages.None);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_NotActivatedScholarshipsByProvider_provider_arg_exception()
        {
            scholarshipService.GetByProviderNotActivated(null);
        }
        
        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void get_NotActivatedScholarshipsByProviderThrowsExceptionIfProviderNotApproved()
        {
            scholarshipService.GetByProviderNotActivated(new Provider { ApprovalStatus = ApprovalStatus.PendingApproval });
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_by_name_and_provider_provider_arg_exception()
        {
            scholarshipService.ScholarshipExists(null, "string", 2009);
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void GetByStageAndProviderThrowsExceptionIfProviderNotApproved()
        {
            scholarshipService.GetByProvider(new Provider { ApprovalStatus = ApprovalStatus.PendingApproval }, ScholarshipStages.None);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_minimum_amount_less_than_zero()
        {
            scholarshipService.Save(new Scholarship{ MinimumAmount = -1});
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_maximum_amount_less_than_one()
        {
            scholarshipService.Save(new Scholarship { MaximumAmount = 0 });
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_maximum_amount_less_than_minimum()
        {
            scholarshipService.Save(new Scholarship { MinimumAmount = 2, MaximumAmount = 1 } );
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void approve_throws_exception_on_null()
        {
            var u = new User();
            scholarshipService.Approve(null, u);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void approve_throws_exception_if_bad_stage()
        {
            var u = new User();
            var s = new Scholarship { Stage = ScholarshipStages.BuildScholarshipMatchLogic };
            scholarshipService.Approve(s, u);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void reject_throws_exception_on_null()
        {
            var u = new User();
            scholarshipService.Reject(null, u);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void reject_throws_exception_if_bad_stage()
        {
            var u = new User();
            var s = new Scholarship { Stage = ScholarshipStages.BuildScholarshipMatchLogic };
            scholarshipService.Reject(s, u);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void request_activation_throws_exception_on_null()
        {
            scholarshipService.RequestActivation(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void request_activation_throws_exception_if_cant_submit()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.RequestedActivation };
            scholarshipService.RequestActivation(s);
            Assert.Fail();
        }

        [Test]
        public void can_submit_scholarship_for_activation()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.BuildScholarshipMatchLogic };

            Expect.Call(scholarshipDAL.Update(s)).Return(s);
            Expect.Call(() => templateParametersService.RequestScholarshipActivation(Arg<Scholarship>.Is.Equal(s), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(roleDal.FindByName(Role.WSC_ADMIN_ROLE)).Return(new Role {Name = Role.WSC_ADMIN_ROLE});
            Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            mocks.ReplayAll();

            scholarshipService.RequestActivation(s);

            Assert.AreEqual(ScholarshipStages.RequestedActivation, s.Stage);
            mocks.VerifyAll();
        }

        [Test]
        public void can_approve_scholarship()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var u = new User();
            var s = new Scholarship {Provider = p, Stage = ScholarshipStages.RequestedActivation };

            Expect.Call(scholarshipDAL.Update(s)).Return(s);
            Expect.Call(() => templateParametersService.ScholarshipApproved(Arg<Scholarship>.Is.Equal(s), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(roleDal.FindByName(Role.WSC_ADMIN_ROLE)).Return(new Role { Name = Role.WSC_ADMIN_ROLE });
            Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            mocks.ReplayAll();

            scholarshipService.Approve(s, u);

            Assert.AreEqual(ScholarshipStages.Activated, s.Stage);
            mocks.VerifyAll();
        }

        [Test]
        public void can_reject_scholarship()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var u = new User();
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.RequestedActivation, Intermediary = new Intermediary()};

            Expect.Call(scholarshipDAL.Update(s)).Return(s);
            Expect.Call(() => templateParametersService.ScholarshipRejected(Arg<Scholarship>.Is.Equal(s), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            mocks.ReplayAll();

            scholarshipService.Reject(s, u);

            Assert.AreEqual(ScholarshipStages.Rejected, s.Stage);
            mocks.VerifyAll();
        }
    }
}
