using ScholarBridge.Domain.Auth;
using Role=ScholarBridge.Domain.Auth.Role;

namespace ScholarBridge.Domain.Messaging
{
    public class MessageAddress
    {
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
        public virtual Organization Organization { get; set; }

        public virtual string EmailAddress()
        {
            if (null != User)
            {
                return User.Email;
            }

            if (null != Organization)
            {
                return Organization.AdminUser.Email;
            }

            return null;
        }

        public override string ToString()
        {
            if (null != User)
            {
                return User.Name.NameFirstLast;
            } 
            if (null != Role)
            {
                if (null != Organization)
                {
                    return string.Format("{0} : {1}", Organization.Name, Role.Name);
                }

                return Role.Name;
            }
            if (null != Organization)
            {
                return Organization.Name;
            }

            return "HECB";
        }
    }
}