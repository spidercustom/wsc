using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.SeekerParts
{
    public class CurrentSchool
    {
        public virtual HighSchool HighSchool { get; set; }
        public virtual College College { get; set; }
        public virtual StudentGroups LastAttended { get; set; }
        public virtual int? YearsAttended { get; set; }
    }
}