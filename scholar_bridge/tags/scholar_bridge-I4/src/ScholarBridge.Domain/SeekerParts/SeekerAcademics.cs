using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.SeekerParts
{
    public class SeekerAcademics
    {
        public virtual double? GPA { get; set; }
        public virtual int? ClassRank { get; set; }

        public virtual SatScore SATScore { get; set; }
        public virtual ActScore ACTScore { get; set; }

        public virtual bool Honors { get; set; }
        public virtual int? APCredits { get; set; }
        public virtual int? IBCredits { get; set; }

        public virtual SchoolTypes SchoolTypes { get; set; }
        public virtual AcademicPrograms AcademicPrograms { get; set; }
        public virtual SeekerStatuses SeekerStatuses { get; set; }
        public virtual ProgramLengths ProgramLengths { get; set; }

        public virtual IList<College> CollegesApplied { get; protected set; }
        public virtual IList<College> CollegesAccepted { get; protected set; }

    }
}