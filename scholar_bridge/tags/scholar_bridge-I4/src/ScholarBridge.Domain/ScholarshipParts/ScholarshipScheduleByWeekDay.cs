﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipScheduleByWeekDay : ScholarshipScheduleBase
    {
        public ScholarshipScheduleByWeekDay()
        {
            StartFrom = new ByWeekDay();
            DueOn = new ByWeekDay();
            AwardOn = new ByWeekDay();
        }

        // TODO: Find out better way to move start, due and award here
        [NotNullValidator]
        public virtual ByWeekDay StartFrom { get; set; }

        [NotNullValidator]
        [PropertyComparisonValidator("StartFrom", ComparisonOperator.GreaterThanEqual)]
        public virtual ByWeekDay DueOn { get; set; }

        [NotNullValidator]
        [PropertyComparisonValidator("DueOn", ComparisonOperator.GreaterThanEqual)]
        public virtual ByWeekDay AwardOn { get; set; }

        public override string StartFromDisplayString
        {
            get { return StartFrom.ToString(); }
        }

        public override string DueOnDisplayString
        {
            get { return DueOn.ToString(); }
        }

        public override string AwardOnDisplayString
        {
            get { return AwardOn.ToString(); }
        }

    }
}
