﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public enum FundingProfileAttribute
    {
        Need,
        [DisplayName("Situations the Scholarship will Fund")]
        SupportedSituation,
        [DisplayName("Funding Parameters")]
        FundingParameters
    }
}
