﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public enum SeekerProfileAttribute
    {
        [DisplayName("Academic Area")]
        AcademicArea,

        [DisplayName("Academic Program")]
        AcademicProgram,
               
        Career,
        
        City,
        
        Club,
        
        College,
        
        [DisplayName("First Generation")]
        FirstGeneration,
        
        [DisplayName("Community Service/Involvement")]
        CommunityService,
        
        County,
        
        Ethnicity,
        
        Gender,
        
        [DisplayName("Seeker Demographics - Geographic")]
        SeekerGeographicsLocation,
        
        State,
        
        HighSchool,

        [DisplayName("Organization")]
        OrganizationAndAffiliationType,
        
        [DisplayName("Program Length")]
        ProgramLength,
        
        Religion,
        
        [DisplayName("School District")]
        SchoolDistrict,
        
        [DisplayName("School Type")]
        SchoolType,
        
        [DisplayName("Hobby")]
        SeekerHobby,
        
        [DisplayName("Skill")]
        SeekerSkill,
        
        [DisplayName("Seeker Status")]
        SeekerStatus,
        
        [DisplayName("Words")]
        SeekerVerbalizingWord,
        
        [DisplayName("Service Hour")]
        ServiceHour,
        
        [DisplayName("Service Type")]
        ServiceType,
        
        Sport,
        
        [DisplayName("Student Group")]
        StudentGroup,
        
        [DisplayName("Work Hour")]
        WorkHour,
        
        [DisplayName("Work Type")]
        WorkType,
        
        GPA,
        
        [DisplayName("Class Rank")]
        ClassRank,
        
        [DisplayName("SAT Score")]
        SAT,
        
        [DisplayName("ACT Score")]
        ACT,
        
        Honor,
        
        [DisplayName("AP Credits Earned")]
        APCreditsEarned,
        
        [DisplayName("IB Credits Earned")]
        IBCreditsEarned
    }
}
