using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingParameters
    {
        public virtual decimal AnnualSupportAmount { get; set; }

        public virtual int MinimumNumberOfAwards { get; set; }

        [PropertyComparisonValidator("MinimumNumberOfAwards", ComparisonOperator.GreaterThan)]
        public virtual int MaximumNumberOfAwards { get; set; }

        [RangeValidator(1, RangeBoundaryType.Inclusive, 16, RangeBoundaryType.Inclusive)]
        public virtual int TermOfSupportUnitCount { get; set; }
        public virtual TermOfSupport TermOfSupport { get; set; }
    }
}