﻿
using System.Runtime.InteropServices;

namespace ScholarBridge.Common.DayOfYear
{
    // TODO: Move to common
    public enum MonthOfYear
    {
        January,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }
}
