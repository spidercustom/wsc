﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScholarBridge.Common.Extensions
{
    public static class StringExtensions
    {
        public static string Build(this string stringToFormat, params object[] args)
        {
            return string.Format(stringToFormat, args);
        }
    }
}
