﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class RelationshipService : IRelationshipService
    {
        public IRelationshipDAL RelationshipDAL { get; set; }
        public IMessagingService MessagingService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }

        public void Save(Relationship relationship)
        {
            RelationshipDAL.Save(relationship);
        }

        public Relationship GetById(int id)
        {
            return RelationshipDAL.FindById(id);
        }

        public IList<Relationship> GetByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Exceptions.ProviderNotApprovedException();
            return RelationshipDAL.FindByProvider(provider);
        }

        public IList<Relationship> GetByIntermediary(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return RelationshipDAL.FindByIntermediary(intermediary);
        }

        public IList<Organization> GetActiveByOrganization(Organization organization)
        {
            if (organization == null)
                throw new ArgumentNullException("organization");

            if (organization is Intermediary)
            {
                var relationships = GetByIntermediary((Intermediary)organization);
                return (from r in relationships
                        where r.Status == RelationshipStatus.Active
                        orderby r.Provider.Name
                        select r.Provider as Organization).ToList();
            }
            else
            {
                var relationships = GetByProvider((Provider)organization);
                return (from r in relationships
                        where r.Status == RelationshipStatus.Active
                        orderby r.Provider.Name
                        select r.Intermediary as Organization).ToList();
            }
        }

        public void InActivate(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            if (!(relationship.Status == RelationshipStatus.InActive))
            {
                relationship.Status = RelationshipStatus.InActive;
                relationship.InActivatedOn = DateTime.Now;
                Save(relationship);


                var templateParams = new MailTemplateParams();
                TemplateParametersService.RelationshipInActivated(relationship.FromOrg, templateParams);
                var msg = new RelationshipMessage
                {
                    MessageTemplate = MessageType.RelationshipInactivated,
                    From = new MessageAddress { Organization = relationship.FromOrg },
                    To = new MessageAddress { User = relationship.ToOrg.AdminUser, Organization = relationship.ToOrg },
                    LastUpdate = new ActivityStamp(relationship.FromOrg.AdminUser)
                };
                MessagingService.SendMessage(msg, templateParams);
            }
        }

        public void CreateRequest(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            relationship.Status = RelationshipStatus.Pending;
            relationship.RequestedOn = DateTime.Today;
            Save(relationship);

            var templateParams = new MailTemplateParams();
            TemplateParametersService.RelationshipRequestCreated(relationship.FromOrg, templateParams);
            var msg = new RelationshipMessage
                    {
                        MessageTemplate = MessageType.RelationshipCreateRequest,
                        From = new MessageAddress { Organization = relationship.FromOrg },
                        To = new MessageAddress { User = relationship.ToOrg.AdminUser, Organization = relationship.ToOrg },
                        LastUpdate = new ActivityStamp(relationship.FromOrg.AdminUser)

                    };
            MessagingService.SendMessage(msg, templateParams);
        }

        public void Approve(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            relationship.Status = RelationshipStatus.Active;
            Save(relationship);

            var templateParams = new MailTemplateParams();
            TemplateParametersService.RelationshipRequestApproved(relationship.FromOrg, templateParams);

            // This is a Response, so From and To are swapped
            var msg = new RelationshipMessage
                {
                    MessageTemplate = MessageType.RelationshipRequestApproved,
                    From = new MessageAddress { Organization = relationship.ToOrg },
                    To = new MessageAddress { User = relationship.FromOrg.AdminUser, Organization = relationship.FromOrg },
                    LastUpdate = new ActivityStamp(relationship.ToOrg.AdminUser)
                };
            MessagingService.SendMessage(msg, templateParams);
        }

        public void Reject(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            RelationshipDAL.Delete(relationship);

            var templateParams = new MailTemplateParams();
            TemplateParametersService.RelationshipRequestRejected(relationship.FromOrg, templateParams);

            // This is a Response, so From and To are swapped
            var msg = new RelationshipMessage
                {
                    MessageTemplate = MessageType.RelationshipRequestRejected,
                    From = new MessageAddress { Organization = relationship.ToOrg },
                    To = new MessageAddress { User = relationship.FromOrg.AdminUser, Organization = relationship.FromOrg },
                    LastUpdate = new ActivityStamp(relationship.ToOrg.AdminUser)
                };
            MessagingService.SendMessage(msg, templateParams);
        }


        public Relationship GetByOrganizations(Organization OrgA, Organization OrgB)
        {
            if (OrgA == null)
                throw new ArgumentNullException("OrgA");
            if (OrgB == null)
                throw new ArgumentNullException("OrgB");

            Provider provider = null;
            Intermediary intermediary = null;

            if (OrgA is Provider)
                provider = OrgA as Provider;
            else if (OrgA is Intermediary)
                intermediary = OrgA as Intermediary;

            if (OrgB is Provider)
                provider = OrgB as Provider;
            else if (OrgB is Intermediary)
                intermediary = OrgB as Intermediary;

            if (provider == null)
                throw new ArgumentNullException("provider");
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return RelationshipDAL.FindByProviderandIntermediary(provider, intermediary);
        }
    }
}
