using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Messaging
{
    public interface IMessagingService
    {

        void SendMessageToAdmin(Message message, MailTemplateParams templateParams);

        void SendMessageFromAdmin(Message message, MailTemplateParams templateParams);

        void SendMessage(Message message, MailTemplateParams templateParams);

        void SendEmail(User toUser, MessageType type, MailTemplateParams templateParams);

        void DeleteRelatedMessages(Scholarship scholarship);
    }
}