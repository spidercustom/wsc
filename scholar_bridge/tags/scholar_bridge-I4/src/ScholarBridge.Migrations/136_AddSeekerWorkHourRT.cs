﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(136)]
    public class AddSeekerWorkHourRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBWorkHourLUT"; }
        }
    }
}
