﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(101)]
    public class AdIBCredits : Migration
    {
        public override void Up()
        {
            Database.AddColumn("SBScholarship", new Column("MatchCriteriaIBCreditsEarned", DbType.Int32, ColumnProperty.Null));
        }

        public override void Down()
        {
            Database.RemoveColumn("SBScholarship", "MatchCriteriaIBCreditsEarned");
        }
    }
}
