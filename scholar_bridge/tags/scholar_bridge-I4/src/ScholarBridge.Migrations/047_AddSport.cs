﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(47)]
    public class AddSport : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "Sport";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
