﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(45)]
    public class AddServiceHour : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "ServiceHour";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
