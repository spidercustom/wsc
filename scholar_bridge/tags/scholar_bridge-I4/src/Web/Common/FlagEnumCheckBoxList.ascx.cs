﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class FlagEnumCheckBoxList : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            EnabledValidation = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private string datasourceEnumTypeName;
        public string DatasourceEnumTypeName
        {
            get { return datasourceEnumTypeName; }
            set
            {
                datasourceEnumTypeName = value;
                DataBind();
            }
        }

        public void SelectAll()
        {
            foreach (ListItem item in CheckBoxList.Items)
                item.Selected = true;
        }

        public void DeselectAll()
        {
            foreach (ListItem item in CheckBoxList.Items)
                item.Selected = true;
        }

        public override void DataBind()
        {
            if (!string.IsNullOrEmpty(DatasourceEnumTypeName))
            {
                Type enumType = Type.GetType(DatasourceEnumTypeName, true);
                CheckBoxList.DataSource = EnumExtensions.GetKeyValue(enumType);
                CheckBoxList.DataTextField = "Value";
                CheckBoxList.DataValueField = "Key";
            }

            base.DataBind();
            SelectAll();
        }

        [DefaultValue(false)]
        public bool SelectNoneAllowed { get; set; }

        [Browsable(false)]
        public int SelectedValues
        {
            get
            {
                int result = 0;
                var selectedItems = from ListItem item in CheckBoxList.Items
                                    where item.Selected
                                    select item;

                foreach (var item in selectedItems)
                {
                    result |= Int32.Parse(item.Value);
                }
                return result;
            }
            set
            {
                foreach (ListItem item in CheckBoxList.Items)
                {
                    int itemValue = Int32.Parse(item.Value);
                    item.Selected = (itemValue & value).Equals(itemValue);
                }
            }
        }

        public bool EnabledValidation
        {
            get { return SelectNoneValidator.Enabled; }
            set { SelectNoneValidator.Enabled = value;}
        }

        public event ServerValidateEventHandler CustomValidation;

        protected void SelectNoneValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            if (null != CustomValidation)
                CustomValidation(source, args);
        }
    }
}