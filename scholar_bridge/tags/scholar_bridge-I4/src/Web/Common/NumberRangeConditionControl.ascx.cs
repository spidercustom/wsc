﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class NumberRangeConditionControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GreaterThanControl.Amount = DefaultMinimumValue.HasValue ? DefaultMinimumValue.Value : 0;
                LessThanControl.Amount = DefaultMaximumValue.HasValue ? DefaultMaximumValue.Value : 0;
            }
        }

        #region properties
        public decimal? DefaultMinimumValue { get; set; }
        public decimal? DefaultMaximumValue { get; set; }

        public decimal MinimumAllowedValue
        {
            get
            {
                return GreaterThanControl.MinAmount;
            }
            set 
            {
                GreaterThanControl.MinAmount = value;
                LessThanControl.MinAmount = value;
            }
        }

        
        public decimal MaximumAllowedValue
        {
            get
            {
                return GreaterThanControl.MaxAmount;
            }
            set
            {
                GreaterThanControl.MaxAmount = value;
                LessThanControl.MaxAmount = value;
            }
        }

        public decimal MinimumValue
        {
            get { return GreaterThanControl.Amount; }
            set { GreaterThanControl.Amount = value; }
        }


        public decimal MaximumValue
        {
            get { return LessThanControl.Amount; }
            set { LessThanControl.Amount = value; }
        }
        #endregion

        #region validation functions
        protected void GreaterThanControlValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid &= ValidateMinimumAllowedValue(MinimumValue, GreaterThanControlValidator);
            args.IsValid &= ValidateMaximumAllowedValue(MinimumValue, GreaterThanControlValidator);
        }


        protected void LessThanControlValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid &= ValidateMinimumAllowedValue(MaximumValue, LessThanControlValidator);
            args.IsValid &= ValidateMaximumAllowedValue(MaximumValue, LessThanControlValidator);
            if (MaximumValue < MinimumValue)
            {
                args.IsValid = false;
                LessThanControlValidator.Text = string.Format("Should be greater than or equal to {0}", MinimumValue);
                return;
            }
        }

        private bool ValidateMinimumAllowedValue(decimal value, CustomValidator validator)
        {
            if (value < MinimumAllowedValue)
            {
                validator.IsValid = false;
                validator.Text = string.Format("Should be greater than {0}", MinimumAllowedValue);
                
            }
            return validator.IsValid;
        }

        private bool ValidateMaximumAllowedValue(decimal value, CustomValidator validator)
        {
            if (value > MaximumAllowedValue)
            {
                validator.IsValid = false;
                validator.Text = string.Format("Should be less than {0}", MaximumAllowedValue);
                
            }
            return validator.IsValid;
        }
        #endregion

        #region population to and from RangeCondition type
        public RangeCondition<int> CreateIntegerRangeCondition()
        {
            var result = new RangeCondition<int>
                             {
                                 Minimum = (int) MinimumValue,
                                 Maximum = (int) MaximumValue
                             };
            return result;
        }

        public void PopulateFromRangeCondition(RangeCondition<int> condition)
        {
            MinimumValue = condition.Minimum;
            MaximumValue = condition.Maximum;
        }

        public RangeCondition<float> CreateFloatRangeCondition()
        {
            var result = new RangeCondition<float>
            {
                Minimum = (float)MinimumValue,
                Maximum = (float)MaximumValue
            };
            return result;
        }
        #endregion
    }
}