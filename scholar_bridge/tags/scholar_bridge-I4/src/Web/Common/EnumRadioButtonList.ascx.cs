﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class EnumRadioButtonList : UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            EnabledValidation = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private string datasourceEnumTypeName;
        public string DatasourceEnumTypeName
        {
            get { return datasourceEnumTypeName; }
            set
            {
                datasourceEnumTypeName = value;
                DataBind();
            }
        }

        public void DeselectAll()
        {
            foreach (ListItem item in RadioButtonList.Items)
                item.Selected = false;
        }

        public override void DataBind()
        {
            if (!string.IsNullOrEmpty(DatasourceEnumTypeName))
            {
                Type enumType = Type.GetType(DatasourceEnumTypeName, true);
                RadioButtonList.DataSource = EnumExtensions.GetKeyValue(enumType);
                RadioButtonList.DataTextField = "Value";
                RadioButtonList.DataValueField = "Key";
            }

            base.DataBind();
        }

        [DefaultValue(false)]
        public bool SelectNoneAllowed { get; set; }

        [Browsable(false)]
        public int SelectedValue
        {
            get
            {
                return Int32.Parse(RadioButtonList.SelectedValue);
            }
            set
            {
                foreach (ListItem item in RadioButtonList.Items)
                {
                    int itemValue = Int32.Parse(item.Value);
                    item.Selected = value.Equals(itemValue);
                }
            }
        }

        public bool EnabledValidation
        {
            get { return SelectNoneValidator.Enabled; }
            set { SelectNoneValidator.Enabled = value; }
        }

        public event ServerValidateEventHandler CustomValidation;

        protected void SelectNoneValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            if (null != CustomValidation)
                CustomValidation(source, args);
        }
    }
}