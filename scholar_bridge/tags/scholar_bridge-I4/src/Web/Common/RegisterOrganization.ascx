﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterOrganization.ascx.cs" Inherits="ScholarBridge.Web.Common.RegisterOrganization"%>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<asp:MultiView runat="server" ID="CreateUserWizard" ActiveViewIndex="0">
    <asp:View ID="DataEntryView" runat="server">
            <h3>Admin User Information</h3>
            <p>Placekeeper for note on Provider Admin role</p>
            
            <label for="FirstName">First Name:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="FirstName" runat="server" Columns="25" 
                MaxLength="40"></asp:TextBox>
            <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstName"
                ErrorMessage="First Name is required." ToolTip="First Name must be entered."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
            <br />
            <label for="MiddleName">Middle Name:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="MiddleName" runat="server" Columns="25" 
                MaxLength="40"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName" />
            <br />
            <label for="LastName">Last Name:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="LastName" runat="server" Columns="25" 
                MaxLength="40"></asp:TextBox>
            <asp:RequiredFieldValidator ID="LastNameRequired" runat="server" ControlToValidate="LastName"
                ErrorMessage="Last Name is required." ToolTip="Last Name must be entered."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
            <br />
            <label for="UserName">Email Address:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="UserName" runat="server" Columns="35" 
                MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="UserName"
                ErrorMessage="Email address is required." ToolTip="Email address must be entered."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
            <asp:CustomValidator ID="EmailCustomValidator" runat="server"></asp:CustomValidator>
            <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
            <br />
            <label for="ConfirmEmail">Confirm Email Address:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="ConfirmEmail" runat="server" Columns="35" 
                MaxLength="50"></asp:TextBox>
            <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" 
                ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address." />
            <br />
            
            <label for="Password">Password:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="Password" runat="server" TextMode="Password" 
                Columns="20" MaxLength="20"></asp:TextBox>
            <span class="noteBene">Password needs to be at least 8 characters with one capital letter and one special character</span>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
            <br />
            <label for="ConfirmPassword">Confirm Password:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="ConfirmPassword" runat="server" 
                TextMode="Password" Columns="20" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."
               ></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
               ></asp:CompareValidator>
            <br />

            <h3>Organization Information</h3>
            <label for="Name">Name:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="Name" runat="server" Columns="35" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="OrgNameRequired" runat="server" ControlToValidate="Name"
                ErrorMessage="Organization name is required." ToolTip="Organization name must be selected."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="NameValidator" runat="server" ControlToValidate="Name" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Organization"/>
            <br />
            <label for="TaxId">Tax Id (EIN):</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TaxId" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="TaxIDRequired" runat="server" ControlToValidate="TaxId"
                ErrorMessage="Tax ID is required." ToolTip="Tax ID must be entered."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="TaxIdValidator" runat="server" ControlToValidate="TaxId" PropertyName="TaxId" SourceTypeName="ScholarBridge.Domain.Organization"/>
            <br />
            <label for="Website">Website:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="Website" runat="server" Columns="35" 
                MaxLength="128"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="WebsiteValidator" runat="server" ControlToValidate="Website" PropertyName="Website" SourceTypeName="ScholarBridge.Domain.Organization"/>
            <br />
            
            <h4>Address</h4>
            <label for="AddressStreet">Street:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="AddressStreet" runat="server" Columns="30" 
                MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="AddressStreetRequired" runat="server" ControlToValidate="AddressStreet"
                ErrorMessage="Street Address is required." ToolTip="Street address must be entered."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address"/>
            <br />
            <label for="AddressStreet2">Street:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="AddressStreet2" runat="server" Columns="30" 
                MaxLength="50"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address"/>
            <br />
            <label for="AddressCity">City:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="AddressCity" runat="server" Columns="25" 
                MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="CityRequired" runat="server" ControlToValidate="AddressCity"
                ErrorMessage="City is required." ToolTip="City must be entered."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address"/>
            <br />
            <label for="AddressState">State:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="AddressState" runat="server"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="StateRequired" runat="server" ControlToValidate="AddressState"
                ErrorMessage="State is required." ToolTip="State must be selected."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address"/>
            <br />
            <label for="AddressPostalCode">Postal Code:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="AddressPostalCode" runat="server" Columns="10" 
                MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="PostalCodeRequired" runat="server" ControlToValidate="AddressPostalCode"
                ErrorMessage="Postal Code is required." ToolTip="Postal code must be entered."
               ></asp:RequiredFieldValidator>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address"/>
            <br />
            
            <h4>Phones</h4>
            <label for="Phone">Phone:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="Phone" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
            <br />
            <label for="Fax">Fax:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="Fax" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="FaxValidator" runat="server" ControlToValidate="Fax" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
            <br />
            <label for="OtherPhone">Other Phone:</label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="OtherPhone" runat="server" Columns="10" 
                MaxLength="10"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" ID="OtherPhoneValidator" runat="server" ControlToValidate="OtherPhone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
            <br />
            
            <p>Placekeeper for note on junk email filter</p>
            <asp:Button ID="RegisterButton" runat="server" Text="Register" 
                onclick="RegisterButton_Click" CausesValidation="true"/>
    </asp:View>
    
    <asp:View ID="CompletionView" runat="server">
            <h2>Thanks for registering with us</h2>
            <p>Now wait for an email to be sent to the email address you specified with instructions
            to enable your account and login.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </asp:View>

</asp:MultiView>

