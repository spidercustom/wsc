﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class FundingInfoShow : UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }

        public const string FAFSA = "FAFSA";
        public const string USER_DERIVED = "User Derived";

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks();

            //view funding info
            var need = ScholarshipToView.FundingProfile.Need;

            if (need.Fafsa)
                lblNeed.Text = FAFSA;
            if (need.Fafsa && need.UserDerived)
                lblNeed.Text += ", " + USER_DERIVED;
            else if (need.UserDerived)
                lblNeed.Text = USER_DERIVED;

            lblMinMaxNeed.Text = string.Format("{0} - {1}", 
                                    need.MaximumSeekerNeed.ToString("c"),
                                    need.MaximumSeekerNeed.ToString("c"));

            if (need.NeedGaps.Count > 0)
                lblNeedGAP.Text = need.NeedGaps.CommaSeparatedNames();
        }

        private void SetupEditLinks()
        {
            if (String.IsNullOrEmpty(LinkTo))
            {
                linkarea1.Visible = false;
                linkarea2.Visible = false;
            }
            else
            {
                string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                              (int)ScholarshipStages.FundingProfile-1);
                linkBottom.NavigateUrl = navurl;
                linkTop.NavigateUrl = navurl;
            }
            linkarea1.Visible = ScholarshipToView.CanEdit();
            linkarea2.Visible = ScholarshipToView.CanEdit();
        }
    }
}