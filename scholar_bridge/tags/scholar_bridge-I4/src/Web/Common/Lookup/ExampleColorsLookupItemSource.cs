﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Data;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Web.Common.Lookup
{
    public class ExampleColorsLookupItemSource : ILookupDAL
    {
        public IList<ILookup> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public IList<ILookup> FindAll(IList<object> ids)
        {
            throw new System.NotImplementedException();
        }

        public ILookup FindById(object id)
        {
            throw new System.NotImplementedException();
        }

        

        #region ILookupDAL Members


        List<KeyValuePair<string, string>> ILookupDAL.GetLookupItems(string UserData)
        {
            var colorlist = Enum.GetNames(typeof(System.Drawing.KnownColor));
            var result = colorlist.Select(
                o => new KeyValuePair<string, string>(o.Substring(1,3),o)).ToList();
            return result;
            
        }

        #endregion
    }
}
