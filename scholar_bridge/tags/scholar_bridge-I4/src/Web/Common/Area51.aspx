﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Area51.aspx.cs" Inherits="ScholarBridge.Web.Common.Area51" %>
<%@ Register src="FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("jquery", "1.3");
    </script>
    <script type="text/javascript" src="/js/jquery-ui-1.6rc6.min.js"></script>
    <script type="text/javascript" src="/js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="/js/site.js"></script>
</head>
<body>
    <form id="form1" runat="server">
      <sb:FlagEnumCheckBoxList id="flagEnum1" runat="server" ></sb:FlagEnumCheckBoxList>
      <asp:TextBox ID="textBox" runat="server" />
      <asp:Button ID="SaveButton" runat="server" Text="Save" 
        onclick="SaveButton_Click" />
      <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" />
    </form>
</body>
</html>
