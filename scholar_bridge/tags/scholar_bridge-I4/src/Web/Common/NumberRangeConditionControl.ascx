﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NumberRangeConditionControl.ascx.cs" Inherits="ScholarBridge.Web.Common.NumberRangeConditionControl" %>

<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>

<label for="GreaterThanControl">Greater than:</label>
<sandTrap:NumberBox ID="GreaterThanControl" runat="server" Precision="0" />
<asp:CustomValidator ID="GreaterThanControlValidator" runat="server" 
  ErrorMessage="" Text="" 
  onservervalidate="GreaterThanControlValidator_ServerValidate" ></asp:CustomValidator>
<br />

<label for="LessThanControl">Less than:</label>
<sandTrap:NumberBox ID="LessThanControl" runat="server" Precision="0" />
<asp:CustomValidator ID="LessThanControlValidator" runat="server" 
  ErrorMessage="" Text="" 
  onservervalidate="LessThanControlValidator_ServerValidate" ></asp:CustomValidator>
<br />
