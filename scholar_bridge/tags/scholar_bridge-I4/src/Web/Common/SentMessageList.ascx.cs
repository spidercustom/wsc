﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class SentMessageList : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }

        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            messageList.DataSource =  MessageService.FindAllSent(UserContext.CurrentUser, UserContext.CurrentOrganization);
            messageList.DataBind();
        }

        protected void messageList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var link = (HyperLink)e.Item.FindControl("linkToMessage");
                link.NavigateUrl = LinkTo + "?sent=true&id=" + ((Domain.Messaging.SentMessage)e.Item.DataItem).Id;
            }
        }
    }
}