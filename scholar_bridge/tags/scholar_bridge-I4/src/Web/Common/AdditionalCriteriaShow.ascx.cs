﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Common
{
    public partial class AdditionalCriteriaShow : UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks();

            if (!Page.IsPostBack)
            {
                AdditionalRequirementsRepeater.DataSource = ScholarshipToView.AdditionalRequirements;
                AdditionalRequirementsRepeater.DataBind();
                RequirementsFooterRow.Visible = ScholarshipToView.AdditionalRequirements.Count <= 0;

                AdditionalCriteriaRepeater.DataSource = ScholarshipToView.AdditionalQuestions;
                AdditionalCriteriaRepeater.DataBind();

                AttachedFiles.DataSource = ScholarshipToView.Attachments;
                AttachedFiles.DataBind();
            }
        }

        private void SetupEditLinks()
        {
            if (String.IsNullOrEmpty(LinkTo))
            {
                linkarea1.Visible = false;
                linkarea2.Visible = false;
            }
            else
            {
                string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                              (int)ScholarshipStages.AdditionalCriteria-1);
                linkBottom.NavigateUrl = navurl;
                linkTop.NavigateUrl = navurl;
            }
            linkarea1.Visible = ScholarshipToView.CanEdit();
            linkarea2.Visible = ScholarshipToView.CanEdit();
        }

        protected void downloadAttachmentBtn_OnCommand(object sender, CommandEventArgs e)
        {
            var id = Int32.Parse((string) e.CommandArgument);
            var attachment = ScholarshipToView.Attachments.First(a => a.Id == id);
            SendFile(attachment);
        }

        private void SendFile(Attachment attachment)
        {
            Response.ClearHeaders();
            Response.AddHeader("Content-Type", attachment.MimeType);
            Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", attachment.Name));
            Response.WriteFile(attachment.GetFullPathToFile(ConfigHelper.GetAttachmentsDirectory()));
        }
    }
}