﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListChangeRequest.ascx.cs" Inherits="ScholarBridge.Web.Common.ListChangeRequest" %>


<div>
<h3>Submit List Change Request</h3>
<br />
<p>From : <asp:Label ID="lblFrom" runat="server"  /></p>
<br />
<h3>Complete this request to add or change a list value to the system:</h3>
<br />
<table class="viewonlyTable">
    <tbody>
        <tr>
            <th>Choose the field you would like to submit a change to</th>
            <td><asp:DropDownList runat="server" ID="cboListType"  Width="207px"></asp:DropDownList></td>
        </tr>
        <tr>
            <th>Enter new Value (200 character limit)</th>
            <td><asp:TextBox runat="server" ID="txtValue" MaxLength="200" Width="467px" /></td>
        </tr>
        <tr>
            <th>Reason for Change: Please describe if this is a new value or a change to an existing list value 
and why you are requesting this change (500 Character limit).</th>
            <td><asp:TextBox runat="server" ID="txtReason" MaxLength="500" Rows="5" 
        Height="69px" Width="467px" TextMode="MultiLine" /></td>
        </tr>
    </tbody>
</table>
</div>