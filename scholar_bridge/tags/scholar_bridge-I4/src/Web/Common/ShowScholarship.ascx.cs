﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ShowScholarship : UserControl
    {
        public string LinkTo { get; set; }
        public Scholarship Scholarship { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            GeneralInfoShow1.ScholarshipToView = Scholarship;
            GeneralInfoShow1.LinkTo = LinkTo;

            SeekerInfoShow1.ScholarshipToView = Scholarship;
            SeekerInfoShow1.LinkTo = LinkTo;

            FundingInfoShow1.ScholarshipToView = Scholarship;
            FundingInfoShow1.LinkTo = LinkTo;

            AdditionalCriteriaShow1.ScholarshipToView = Scholarship;
            AdditionalCriteriaShow1.LinkTo = LinkTo;
        }
    }
}