﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Default" Title="Intermediary" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <p>
        This will be the home page for an Intermediary. It will contain the dashboard elements
        based on their organization
    </p>
    <sb:Login ID="loginForm" runat="server" />

    <asp:LoginView ID="loginView" runat="server">
        <AnonymousTemplate>
            <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Intermediary/RegisterIntermediary.aspx">Register as a Scholarship Intermediary</asp:HyperLink>
        </AnonymousTemplate>
    </asp:LoginView>
   
</asp:Content>
