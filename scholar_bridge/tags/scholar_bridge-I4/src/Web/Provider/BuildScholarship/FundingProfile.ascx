﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.FundingProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<h3>Build Scholarship – Funding Profile</h3>
<h4><asp:Label ID="scholarshipNameLbl" runat="server" /></h4>

<div id="legends">
    <ul>
        <li><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" /> <span>indicates that field will be used as minimum requirement</span></li>
        <li><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" /> <span>indicates that field will be used as preference</span></li>
    </ul>
</div>

<asp:PlaceHolder ID="NeedContainerControl" runat="server">
  <h5>Need <asp:Image ID="NeedUsageTypeIconControl" runat="server" /></h5>
  
  <asp:CheckBox ID="Fafsa" runat="server" Text="FAFSA Definition of Need" TextAlign="Left"/>
  <asp:CheckBox ID="UserDerived" runat="server" Text="User Derived Definition of Need" TextAlign="Left"/>
  <br />
  <label for="MinimumSeekerNeed">Minimum Need:</label>
  <sandTrap:CurrencyBox ID="MinimumSeekerNeed" runat="server" Precision="0" />
  <elv:PropertyProxyValidator ID="MinimumSeekerNeedValidator" runat="server" ControlToValidate="MinimumSeekerNeed" PropertyName="MinimumSeekerNeed" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
  <br />
  <label for="MaximumSeekerNeed">Maximum Need:</label>
  <sandTrap:CurrencyBox ID="MaximumSeekerNeed" runat="server" Precision="0" />
  <elv:PropertyProxyValidator ID="MaximumSeekerNeedValidator" runat="server" ControlToValidate="MaximumSeekerNeed" PropertyName="MaximumSeekerNeed" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
  <br />
  <label for="NeedGaps">Need Gap Threshold:</label>
  <asp:CheckBoxList ID="NeedGaps" runat="server" TextAlign="Left"/>
  <br />
</asp:PlaceHolder>

<asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
  <h5>Situations the Scholarship will Fund <asp:Image ID="SupportSituationUsageTypeIconControl" runat="server" /></h5>
  
  <asp:CheckBox ID="Emergency" runat="server" Text="Emergency funding" />
  <asp:CheckBox ID="Traditional" runat="server" Text="Traditional support" />
  <br />
  <label for="TypesOfSupport">Types of Support:</label>
  <asp:CheckBoxList ID="TypesOfSupport" runat="server" TextAlign="Left"/>
  <br />
</asp:PlaceHolder>

<asp:PlaceHolder ID="FundingParametersContainerControl" runat="server">
  <h5>Funding Parameters <asp:Image ID="FundingParametersUsageTypeIconControl" runat="server" /></h5>
  
  <label for="AnnualSupportAmount">Annual Amount of Support:</label>
  <sandTrap:CurrencyBox ID="AnnualSupportAmount" runat="server" Precision="0"/>
  <elv:PropertyProxyValidator ID="AnnualSupportAmountValidator" runat="server" ControlToValidate="AnnualSupportAmount" PropertyName="AnnualSupportAmount" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters"/>
  <br />
  <label for="MinimumNumberOfAwards">Minimum Number of Awards:</label>
  <sandTrap:NumberBox ID="MinimumNumberOfAwards" runat="server" Precision="0"/>
  <elv:PropertyProxyValidator ID="MinimumNumberOfAwardsValidator" runat="server" ControlToValidate="MinimumNumberOfAwards" PropertyName="MinimumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters"/>
  <br />
  <label for="MaximumNumberOfAwards">Maximum Number of Awards:</label>
  <sandTrap:NumberBox ID="MaximumNumberOfAwards" runat="server" Precision="0"  />
  <elv:PropertyProxyValidator ID="MaximumNumberOfAwardsValidator" runat="server" ControlToValidate="MaximumNumberOfAwards" PropertyName="MaximumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters"/>
  <br />
  <label for="TermsOfSupport">Terms of Support:</label>
  <sandTrap:NumberBox ID="TermsOfSupportUnitCountControl" runat="server" Precision="0" MinAmount="1" MaxAmount="16"/>
  <asp:DropDownList ID="TermsOfSupportTermUnitControl" runat="server" />
  <elv:PropertyProxyValidator ID="TermsOfSupportUnitCountControlValidator" runat="server" ControlToValidate="TermsOfSupportUnitCountControl" PropertyName="TermOfSupportUnitCount" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters"/>
  <br />
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
  <p>No Scholarship Match Criteria Selected. Please go to the <asp:LinkButton ID="BuildMatchCriteriaLinkControl" runat="server" OnClick="BuildMatchCriteriaLinkControl_Click">Build Match Criteria</asp:LinkButton> tab to choose fields to be used for Scholarship selection</p>
</asp:PlaceHolder>
