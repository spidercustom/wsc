﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class AdditionalCriteria : WizardStepUserControlBase<Scholarship>
	{
		#region DI Properties
		public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IAdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
		#endregion

		#region Properties
		private List<Question> Questions
    	{
    		get
    		{
    		    if (ViewState["Questions"] == null)
    		    {
    		        ViewState["Questions"]
    		            = scholarshipInContext.AdditionalQuestions
    		                .Select(q => new Question
    		                                 {
    		                                     DisplayOrder = q.DisplayOrder, 
                                                 QuestionText = q.QuestionText
    		                                 }).ToList();
    		    }
    		    return (List<Question>)ViewState["Questions"];
    		}
    	}

		private Scholarship _scholarshipInContext;
		private Scholarship scholarshipInContext
		{
			get
			{
				if (_scholarshipInContext == null)
					_scholarshipInContext = Container.GetDomainObject();
				return _scholarshipInContext;
			}
		}

		#endregion

		#region Page Events
		protected void Page_Init(object sender, EventArgs e)
		{
		}


		public void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
            {
                PopulateScreen();
				SetButtonsToSkip();
            }
		}

		#endregion

		#region Private Methods

		private void SetButtonsToSkip()
		{
			if (Page is IChangeCheckingPage)
			{
				var ccPage = (IChangeCheckingPage)Page;
				ccPage.BypassPromptIds.AddRange(new[] { "updateQuestionButton", 
															"cancelQuestionUpdateButton", 
															"editQuestionButton", 
															"deleteQuestionButton", 
															"addQuestionButton", 
															"addEmptyQuestionButton" });
			}
		}

		private void UpdateQuestion(int DisplayOrder, string QuestionText)
		{
			int removeIndex = -1;
			bool remove = false;
			foreach (var question in Questions)
			{
				removeIndex++;
				if (question.DisplayOrder == DisplayOrder)
				{
					if (QuestionText == null || QuestionText.Trim() == string.Empty)
						remove = true;
					else
						question.QuestionText = QuestionText;

					break;
				}
			}
			if (remove)
			{
				Questions.RemoveAt(removeIndex);
				int i = 0;
				foreach (var q in Questions)
				{
					q.DisplayOrder = ++i;
				}
			}
		}

		private void PopulateScreen()
        {
            if (null != scholarshipInContext.AdditionalQuestions &&
                scholarshipInContext.AdditionalQuestions.Count > 0)
            {
                PopulateCheckBoxes(AdditionalRequirements, AdditionalRequirementDAL.FindAll());
                AdditionalRequirements.Items.SelectItems(scholarshipInContext.AdditionalRequirements, ar => ar.Id.ToString());
                scholarshipNameLbl.Text = scholarshipInContext.Name;
            }
			BindQuestionGrid();
            BindAttachedFiles();
        }

        private void BindAttachedFiles()
        {
            attachedFiles.DataSource = scholarshipInContext.Attachments;
            attachedFiles.DataBind();
        }

		private void BindQuestionGrid()
		{
			questionsGrid.DataSource = Questions;
			questionsGrid.DataBind();
		}

        private void PopulateObjects()
        {
            var selectedAdditionalReqs = AdditionalRequirements.Items.SelectedItems(item => (object)item);
            scholarshipInContext.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll(selectedAdditionalReqs));

            int qNum = CreateOrUpdateQuestions();
            RemoveDeletedQuestions(qNum);

            if (scholarshipInContext.Stage < ScholarshipStages.AdditionalCriteria)
                scholarshipInContext.Stage = ScholarshipStages.AdditionalCriteria;

        }

        private int CreateOrUpdateQuestions()
        {
            var user = UserContext.CurrentUser;

            int qNum = 0;
            foreach (var q in Questions)
            {
            	string questionText = q.QuestionText;
                if (! String.IsNullOrEmpty(questionText))
                {
                    ScholarshipQuestion question;
                    qNum++;
                    if (qNum > scholarshipInContext.AdditionalQuestions.Count)
                    {
                        question = new ScholarshipQuestion();
                        scholarshipInContext.AddAdditionalQuestion(question);
                    }
                    else
                    {
                        question = scholarshipInContext.AdditionalQuestions[qNum - 1];
                    }
                    question.QuestionText = questionText;
                    question.DisplayOrder = qNum;
                    question.LastUpdate = new ActivityStamp(user);
                }
            }
            return qNum;
        }

        private void RemoveDeletedQuestions(int qNum)
        {
            if (qNum < scholarshipInContext.AdditionalQuestions.Count)
            {
                for (int del = scholarshipInContext.AdditionalQuestions.Count; del > qNum; del--)
                    scholarshipInContext.AdditionalQuestions.RemoveAt(del - 1);
            }
		}

		private static void PopulateCheckBoxes(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
		}
		#endregion

		#region Wizard Control Override Methods
		public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.AdditionalCriteria;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.AdditionalCriteria;
        }

        public override bool IsCompleted
        {
            get { return scholarshipInContext.IsStageCompleted(ScholarshipStages.AdditionalCriteria); }
            set
            {
                scholarshipInContext.MarkStageCompletion(ScholarshipStages.AdditionalCriteria, value);
                ScholarshipService.Save(scholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            return true;
		}
        #endregion

		#region control event handlers
		protected void addQuestionButton_Click(object sender, EventArgs e)
		{
            AddQuestion("questionAddTextBox");
		}

		protected void addEmptyQuestionButton_Click(object sender, EventArgs e)
		{
		    AddQuestion("questionEmptyAddTextBox");
		}
        
        private void AddQuestion(string controlName)
        {
            var questionText = questionsGrid.FooterRow.FindControl(controlName) as TextBox;
            if (questionText.Text != null && questionText.Text.Trim() != string.Empty)
            {
                Questions.Add(new Question { DisplayOrder = Questions.Count + 1, QuestionText = questionText.Text });
                questionsGrid.DataBind();
            }
        }

        protected void UploadFile_Click(object sender, EventArgs e)
        {
            if (null != AttachFile.PostedFile)
            {
                var attachment = CreateAttachment();
                string fullPath = attachment.GetFullPathToFile(ConfigHelper.GetAttachmentsDirectory());
                try
                {
                    AttachFile.PostedFile.SaveAs(fullPath);
                    scholarshipInContext.Attachments.Add(attachment);
                    scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ScholarshipService.Save(scholarshipInContext);
                } 
                catch (Exception)
                {
                    RemoveFile(fullPath);
                    AttachmentCommentsValidator.IsValid = false;
                    AttachmentCommentsValidator.Text = "Could not upload the file. Please try again.";
                    // FIXME: Display error to user
                    scholarshipInContext.Attachments.RemoveAt(scholarshipInContext.Attachments.Count -1);
                }
                BindAttachedFiles();
            }
        }

        private Attachment CreateAttachment()
        {
            string mimeType = AttachFile.PostedFile.ContentType;
            if (String.IsNullOrEmpty(mimeType))
                mimeType = "application/octet-stream";
            var attachment = new Attachment
                                 {
                                     Name = Path.GetFileName(AttachFile.PostedFile.FileName),
                                     Comment = AttachmentComments.Text,
                                     Bytes = AttachFile.PostedFile.ContentLength,
                                     MimeType = mimeType,
                                     LastUpdate = new ActivityStamp(UserContext.CurrentUser)
                                 };
            attachment.GenerateUniqueName();
            return attachment;
        }

        protected void questionsGrid_RowEditing(object sender, GridViewEditEventArgs e)
		{
			questionsGrid.EditIndex = e.NewEditIndex;
			BindQuestionGrid();
		}

		protected void questionsGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "Update")
			{
				int displayOrder = int.Parse(e.CommandArgument.ToString());
				GridViewRow row = questionsGrid.Rows[questionsGrid.EditIndex];
				TextBox questionBox = (TextBox)row.FindControl("questionTextBox");
				UpdateQuestion(displayOrder, questionBox.Text);
				questionsGrid.EditIndex = -1;
				BindQuestionGrid();
			}
		}

		protected void questionsGrid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
		{
			questionsGrid.EditIndex = -1;
			BindQuestionGrid();
		}
		#endregion


		#region Inner Classes
		[Serializable]
		public class Question
		{
			public int DisplayOrder
			{
				get; set;
			}
			public string QuestionText
			{
				get; set;
			}
		}
		#endregion

        protected void attachedFiles_OnItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            var attachment = scholarshipInContext.Attachments[e.ItemIndex];
            if (null != attachment)
            {
                scholarshipInContext.Attachments.RemoveAt(e.ItemIndex);

                scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                ScholarshipService.Save(scholarshipInContext);

                RemoveFile(attachment.GetFullPathToFile(ConfigHelper.GetAttachmentsDirectory()));
            }
            BindAttachedFiles();
        }

        private static void RemoveFile(string fullPath)
        {
            try
            {
                // If there was an error attempt to cleanup the file
                File.Delete(fullPath);
            }
            catch { }
        }
	}
}
