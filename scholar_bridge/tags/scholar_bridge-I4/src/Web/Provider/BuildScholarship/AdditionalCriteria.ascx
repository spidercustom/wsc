﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteria.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<h3>Build Scholarship – Criteria</h3>
<h4><asp:Label ID="scholarshipNameLbl" runat="server" /></h4>

<br /><br />
<p>
Place keeper for text to be defined by WSC explaining the +Criteria.  Each question can be 200 characters maximum
</p>
<br />
<fieldset>
<h5>Additional Requirements with Scholarship Application</h5>
<asp:CheckBoxList ID="AdditionalRequirements" runat="server" TextAlign="Left"/>
</fieldset>
<br />
<table width="90%">
<tr>
<td>
<asp:GridView ID="questionsGrid" runat="server"  ShowFooter="true" 
        AutoGenerateColumns="False" 
        onrowediting="questionsGrid_RowEditing" 
        onrowcommand="questionsGrid_RowCommand" 
        onrowcancelingedit="questionsGrid_RowCancelingEdit">
        
        <EmptyDataTemplate>
            <table>
                <tr>
                    <th colspan="2">
                        Scholarship Specific Questions
                    </th>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine" ID="questionEmptyAddTextBox" runat="server"></asp:TextBox>
                    
                    </td>
                    <td>
                        <asp:LinkButton ID="addEmptyQuestionButton" runat="server" 
                                Text="Add" onclick="addEmptyQuestionButton_Click" />
                    </td>
                
                </tr>
            </table>

        </EmptyDataTemplate>
    <Columns>
        <asp:TemplateField HeaderText="#">
            <EditItemTemplate>
                <asp:Label ID="displayOrderEditLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>'></asp:Label>
                <asp:HiddenField runat="server" ID="questionOrderHidden" Value='<%# Bind("DisplayOrder") %>' />
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="displayOrderLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>'></asp:Label><br />&nbsp;
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Scholarship Specific Questions">
            <EditItemTemplate>
                <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine" ID="questionTextBox" runat="server" Text='<%# Bind("QuestionText") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="questionTextLabel" runat="server" Text='<%# Bind("QuestionText") %>'></asp:Label><br />&nbsp;
            </ItemTemplate>
            <FooterTemplate>
                &nbsp;<br />
                <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine" ID="questionAddTextBox" runat="server"></asp:TextBox>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField ShowHeader="False">
            <EditItemTemplate>
                <asp:LinkButton ID="updateQuestionButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="Update" CommandArgument='<%# Bind("DisplayOrder") %>'></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="cancelQuestionUpdateButton" runat="server" CausesValidation="False" 
                    CommandName="Cancel" Text="Cancel"></asp:LinkButton>
           </EditItemTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="editQuestionButton" runat="server" CausesValidation="False" 
                    CommandName="Edit" Text="Edit"></asp:LinkButton>&nbsp;
            </ItemTemplate>
            <FooterTemplate>
                &nbsp;<br />
                    <asp:LinkButton ID="addQuestionButton" runat="server" 
            Text="Add" onclick="addQuestionButton_Click" />
            </FooterTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</td>
</tr>
</table>


<fieldset>
<h5>Upload scholarship application forms</h5>
<p>These forms will be available to scholarship seekers to download.</p>

<label for="AttachFile">Select File:</label>
<asp:FileUpload ID="AttachFile" runat="server" />
<br />
<label for="AttachmentComments">Instructions for applicant:</label>
<asp:TextBox ID="AttachmentComments" runat="server" />
<elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments" PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment"/>

<span class="noteBene">Please enter instructions for applicant. This will accompany the form.</span>
<br />
<asp:Button ID="UploadFile" runat="server" Text="Attach File" onclick="UploadFile_Click" />
</fieldset>

<asp:ListView ID="attachedFiles" runat="server" OnItemDeleting="attachedFiles_OnItemDeleting">
    <LayoutTemplate>
    <table class="sortableTable">
        <thead>
            <tr>
                <th></th>
                <th>File</th>
                <th>Size</th>
                <th>Type</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr
            <td><asp:LinkButton ID="deleteAttachmentBtn" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("Id")%>' /></td>
            <td><%# Eval("Name") %></td>
            <td><%# Eval("DisplaySize") %></td>
            <td><%# Eval("MimeType") %></td>
        </tr>
    </ItemTemplate>
</asp:ListView>

