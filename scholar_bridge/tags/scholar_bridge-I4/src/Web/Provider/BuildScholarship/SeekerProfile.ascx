﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.SeekerProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>
<%@ Register src="~/Common/NumberRangeConditionControl.ascx" tagname="NumberRangeCondition" tagprefix="sb" %>
<%@ Register Assembly="Web"
             Namespace="ScholarBridge.Web.Common"
             TagPrefix="sbCommon" %>
<%@ Register src="LocationSelector.ascx" tagname="LocationSelector" tagprefix="sb" %>

<h2>Build Scholarship – Seeker Profile</h2>
<h3><asp:Label ID="scholarshipNameLbl" runat="server" /></h3>

<div id="legends">
    <ul>
        <li><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" /> <span>indicates that field will be used as minimum requirement</span></li>
        <li><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" /> <span>indicates that field will be used as preference</span></li>
    </ul>
</div>

<asp:PlaceHolder runat="server" ID="SeekerPersonalityContainerControl">
  <h4>Seeker's Personality</h4>
  <p class="additionInfoText">
    Translate the scholarship Mission into specific criteria that describe the Seeker and can be used to perform a "strict" match.
  </p>

  <asp:PlaceHolder ID="FiveWordsContainerControl" runat="server">
    <label id="FiveWordsLabelControl" for="FiveWordsControl">5 Words:</label>
    <asp:Image ID="FiveWordsUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="FiveWordsControl" TextMode="MultiLine" ReadOnly="true" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="FiveWordsControlDialogButton" runat="server" SelectionLimit="5" BuddyControl="FiveWordsControl" ItemSource="SeekerVerbalizingWordDAL" Title="Five Words Describing the Seeker"/>
    <asp:RequiredFieldValidator ID="FiveWordsRequiredValidator" ControlToValidate="FiveWordsControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="FiveSkillsContainerControl" runat="server">
    <label id="FiveSkillsLabelControl" for="FiveSkillsControl">5 Skills:</label>
    <asp:Image ID="FiveSkillsUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="FiveSkillsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="FiveSkillsControlDialogButton" runat="server" SelectionLimit="5" BuddyControl="FiveSkillsControl" ItemSource="SeekerSkillDAL" Title="Five Seeker Skills"/>
    <asp:RequiredFieldValidator ID="FiveSkillsRequiredValidator" ControlToValidate="FiveWordsControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
</asp:PlaceHolder>

<asp:PlaceHolder ID="SeekerTypeContainerControl" runat="server">
  <h4>Seeker Student Type</h4>
  <asp:PlaceHolder ID="StudentGroupContainerControl" runat="server">
    <label id="StudentGroupLabelControl" for="StudentGroupControl">Student groups:</label>
    <asp:Image ID="StudentGroupUsageTypeIconControl" runat="server" />
    <sb:FlagEnumCheckBoxList id="StudentGroupControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.StudentGroups, ScholarBridge.Domain"></sb:FlagEnumCheckBoxList>
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="SchoolTypeContainerControl" runat="server">
    <label id="SchoolTypeLabelControl" for="SchoolTypeControl">School types:</label>
    <asp:Image ID="SchoolTypeUsageTypeIconControl" runat="server" />
    <sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="AcademicProgramContainerControl" runat="server">
    <label id="AcademicProgramLabelControl" for="AcademicProgramControl">Academic programs:</label>
    <asp:Image ID="AcademicProgramUsageTypeIconControl" runat="server" />
    <sb:FlagEnumCheckBoxList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="SeekerStatusContainerControl" runat="server">
      <label id="SeekerStatusLabelControl" for="SeekerStatusControl">Enrollment statuses:</label>
      <asp:Image ID="SeekerStatusUsageTypeIconControl" runat="server" />
      <sb:FlagEnumCheckBoxList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
      <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="ProgramLengthContainerControl" runat="server">
    <label id="ProgramLengthLabelControl" for="ProgramLengthControl">Length of program:</label>
    <asp:Image ID="ProgramLengthUsageTypeIconControl"  runat="server" />
    <sb:FlagEnumCheckBoxList id="ProgramLengthControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.ProgramLengths, ScholarBridge.Domain" />
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="CollegesContainerControl" runat="server">
    <label id="CollegesLabelControl" for="CollegesControl">Colleges:</label>
    <asp:Image ID="CollegesUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="CollegesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="CollegesControlDialogButton" runat="server" BuddyControl="CollegesControl" ItemSource="CollegeDAL" Title="College Selection"/>
    <asp:RequiredFieldValidator ID="CollegesRequiredValidator" ControlToValidate="CollegesControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>

</asp:PlaceHolder>

<asp:PlaceHolder ID="SeekerDemographicsPersonalContainerControl" runat="server">
  <h4>Seeker Demographics - Personal</h4>

  <asp:PlaceHolder ID="FirstGenerationContainerControl" runat="server">
    <label for="FirstGenerationControl">First Generation:</label>
    <asp:Image ID="FirstGenerUsageTypeIconControl" runat="server" />
    <asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="EthnicityContainerControl" runat="server">
    <label id="EthnicityLabelControl" for="EthnicityControl">Ethnicity:</label>
    <asp:Image ID="EthnicityUsageTypeIconControl" runat="server" />
    <sb:LookupItemCheckboxList id="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL"/>
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="ReligionContainerControl" runat="server">
    <label id="ReligionLabelControl" for="ReligionControl">Religion:</label>
    <asp:Image ID="ReligionUsageTypeIconControl" runat="server" />
    <sb:LookupItemCheckboxList id="ReligionControl" runat="server" LookupServiceSpringContainerKey="ReligionDAL"/>
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="GendersContainerControl" runat="server">
    <label id="GendersLabelControl" for="GendersControl">Genders:</label>
    <asp:Image ID="GendersUsageTypeIconControl" runat="server" />
    <sb:FlagEnumCheckBoxList id="GendersControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.Genders, ScholarBridge.Domain" />
    <br />
  </asp:PlaceHolder>
</asp:PlaceHolder>

<asp:PlaceHolder ID="SeekerDemographicsGeographicContainerControl" runat="server">
  <h4>Seeker Demographics - Geographic <asp:Image ID="SeekerDemographicsGeographicUsageTypeIconControl" runat="server" /></h4>
  
  <asp:PlaceHolder ID="LocationSelectorContainer" runat="server">
      <sb:LocationSelector id="LocationSelectorControl" runat="server" />
      <br />
  </asp:PlaceHolder>

</asp:PlaceHolder>

<asp:PlaceHolder ID="SeekerInterestsControlContainer" runat="server">
  <h4>Seeker Interests</h4>

  <asp:PlaceHolder ID="AcademicAreasContainerControl" runat="server">
    <label id="AcademicAreasLabelControl" for="AcademicAreasControl">Academic areas:</label>
    <asp:Image ID="AcademicAreasUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="AcademicAreasControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="AcademicAreasControlDialogButton" runat="server" BuddyControl="AcademicAreasControl" ItemSource="AcademicAreaDAL" Title="Academic Area Selection"/>
    <asp:RequiredFieldValidator ID="AcademicAreasControlRequiredValidator" ControlToValidate="AcademicAreasControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="CareersContainerControl" runat="server">
    <label id="CareersLabelControl" for="CareersControl">Careers:</label>
    <asp:Image ID="CareersUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="CareersControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="CareersControlDialogButton" runat="server" BuddyControl="CareersControl" ItemSource="CareerDAL" Title="Career Selection"/>
    <asp:RequiredFieldValidator ID="CareersControlRequiredValidator" ControlToValidate="CareersControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="CommunityServicesContainerControl" runat="server">
    <label id="CommunityServicesLabelControl" for="CommunityServicesControl">Community Service/Involvement:</label>
    <asp:Image ID="CommunityServicesUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="CommunityServicesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="CommunityServicesControlDialogButton" runat="server" BuddyControl="CommunityServicesControl" ItemSource="CommunityServiceDAL" Title="Community Service/Involvement Selection"/>
    <asp:RequiredFieldValidator ID="CommunityServicesRequiredValidator" ControlToValidate="CommunityServicesControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="OrganizationsContainerControl" runat="server">
    <label id="OrganizationsLabelControl" for="OrganizationsControl">Organizations:</label>
    <asp:Image ID="OrganizationsUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="OrganizationsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="OrganizationsControlDialogButton" runat="server" BuddyControl="OrganizationsControl" ItemSource="SeekerMatchOrganizationDAL" Title="Organization Affiliation Selection"/>
    <asp:RequiredFieldValidator ID="OrganizationsControlRequiredValidator" ControlToValidate="OrganizationsControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="AffiliationTypesContainerControl" runat="server">
    <label id="AffiliationTypesLabelControl" for="AffiliationTypesControl">Affiliation types:</label>
    <asp:Image ID="AffiliationTypesUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="AffiliationTypesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="AffiliationTypesControlDialogButton" runat="server" BuddyControl="AffiliationTypesControl" ItemSource="AffiliationTypeDAL" Title="Affiliation Type Selection"/>
    <asp:RequiredFieldValidator ID="AffiliationTypesControlRequiredValidator" ControlToValidate="AffiliationTypesControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
</asp:PlaceHolder>

<asp:PlaceHolder ID="SeekerActivitiesControlContainer" runat="server">
  <h4>Seeker Activities</h4>

  <asp:PlaceHolder ID="HobbiesContainerControl" runat="server">
    <label id="SeekerHobbiesLabelControl" for="SeekerHobbiesControl">Hobbies:</label>
    <asp:Image ID="SeekerHobbiesUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="SeekerHobbiesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="SeekerHobbiesControlDialogButton" runat="server" BuddyControl="SeekerHobbiesControl" ItemSource="SeekerHobbyDAL" Title="Hobby Selection"/>
    <asp:RequiredFieldValidator ID="SeekerHobbiesControlRequiredValidator" ControlToValidate="SeekerHobbiesControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="SportsContainerControl" runat="server">
    <label id="SportsLabelControl" for="SportsControl">Sports:</label>
    <asp:Image ID="SportsUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="SportsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="SportsControlDialogButton" runat="server" BuddyControl="SportsControl" ItemSource="SportDAL" Title="Sport Participation Selection"/>
    <asp:RequiredFieldValidator ID="SportsControlRequiredValidator" ControlToValidate="SportsControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="ClubsContainerControl" runat="server">
    <label id="ClubsLabelControl" for="ClubsControl">Clubs:</label>
    <asp:Image ID="ClubsUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="ClubsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="ClubsControlDialogButton" runat="server" BuddyControl="ClubsControl" ItemSource="ClubDAL" Title="Club Participation Selection"/>
    <asp:RequiredFieldValidator ID="ClubsControlRequiredValidator" ControlToValidate="ClubsControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="WorkTypeContainerControl" runat="server">
    <label id="WorkTypeLabelControl" for="WorkTypeControl">Work Type:</label>
    <asp:Image ID="WorkTypeUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="WorkTypeControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="WorkTypeControlDialogButton" runat="server" BuddyControl="WorkTypeControl" ItemSource="WorkTypeDAL" Title="Work Type Selection"/>
    <asp:RequiredFieldValidator ID="WorkTypeControlRequiredValidator" ControlToValidate="WorkTypeControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="WorkHoursContainerControl" runat="server">
    <label id="WorkHoursLabelControl" for="WorkHoursControl">Work Hours:</label>
    <asp:Image ID="WorkHoursUsageTypeIconControl" runat="server" />
    <sb:LookupItemCheckboxList id="WorkHoursControl" runat="server" LookupServiceSpringContainerKey="WorkHourDAL"/>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="ServiceTypeContainerControl" runat="server">
    <label id="ServiceTypeLabelControl" for="ServiceTypeControl">Service Type:</label>
    <asp:Image ID="ServiceTypeUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="ServiceTypeControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
    <sb:LookupDialog ID="ServiceTypeControlDialogButton" runat="server" BuddyControl="ServiceTypeControl" ItemSource="ServiceTypeDAL" Title="Service Type Selection"/>
    <asp:RequiredFieldValidator ID="ServiceTypeControlRequiredValidator" ControlToValidate="ServiceTypeControl" runat="server" ErrorMessage="Select at least one"></asp:RequiredFieldValidator>
    <br />
  </asp:PlaceHolder>
  
  <asp:PlaceHolder ID="ServiceHoursContainerControl" runat="server">
    <label id="ServiceHoursLabelControl" for="ServiceHoursControl">Service hours:</label>
    <asp:Image ID="ServiceHoursUsageTypeIconControl" runat="server" />
    <sb:LookupItemCheckboxList id="ServiceHoursControl" runat="server" LookupServiceSpringContainerKey="ServiceHourDAL"/>
    <br />
  </asp:PlaceHolder>
  
</asp:PlaceHolder>

<asp:PlaceHolder ID="SeekerPerformanceControlContainer" runat="server">
  <h4>Seeker Student Performance</h4>

  <asp:PlaceHolder ID="GPAControlContainerControl" runat="server">
    <label id="GPALabelControl" for="GPAControl">GPA</label>
    <asp:Image ID="GPAUsageTypeIconControl" runat="server" />
    <br />
    <sb:NumberRangeCondition id="GPAControl" runat="server" 
      MaximumAllowedValue="5" MinimumAllowedValue="0" DefaultMinimumValue="0" DefaultMaximumValue="5"/>
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="ClassRankContainerControl"  runat="server">
    <label id="ClassRankLabelControl" runat="server" for="ClassRankControl">Class rank:</label>
    <asp:Image ID="ClassRankUsageTypeIconControl" runat="server" />
    <br />
    <sb:NumberRangeCondition id="ClassRankControl" runat="server"
       MaximumAllowedValue="100" MinimumAllowedValue="0" DefaultMaximumValue="100" DefaultMinimumValue="0"/>
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="SATContainerControl"  runat="server">
    <h5>SAT Score: <asp:Image ID="SATScoreUsageTypeIconControl" runat="server" /></h5>
    <div class="subsection">
        <label for="SATWritingControl">Writing:</label>
        <br />
        <sb:NumberRangeCondition id="SATWritingControl" runat="server" 
          MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
        <br />
        <label for="SATCriticalReadingControl">Critical Reading:</label>
        <br />
        <sb:NumberRangeCondition id="SATCriticalReadingControl" runat="server"
          MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
        <br />
        <label for="SATMathematicsControl">Mathematics:</label>
        <br />
        <sb:NumberRangeCondition id="SATMathematicsControl" runat="server"
          MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
    </div>
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="ACTContainerControl"  runat="server">
    <h5>ACT Score: <asp:Image ID="ACTScoreUsageTypeIconControl" runat="server" /></h5>
    <div class="subsection">
        <label for="ACTEnglishControl">English:</label>
        <br />
        <sb:NumberRangeCondition id="ACTEnglishControl" runat="server"
          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
        <br />    
        <label for="ACTMathematicsControl">Mathematics:</label>
        <br />
        <sb:NumberRangeCondition id="ACTMathematicsControl" runat="server"
          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
        <br />
        <label for="ACTReadingControl">Reading:</label>
        <br />
        <sb:NumberRangeCondition id="ACTReadingControl" runat="server"
          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
        <br />
        <label for="ACTScienceControl">Science:</label>
        <br />
        <sb:NumberRangeCondition id="ACTScienceControl" runat="server"
          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
    </div>
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="HonorsContainerControl" runat="server">
    <label for="HonorsLabelControl">Honors:</label>
    <asp:Image ID="HonorsUsageTypeIconControl" runat="server" />
    <asp:CheckBox ID="HonorsControl" Text="" runat="server" />
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="APCreditContainerControl" runat="server">
    <label for="APCreditControl">AP Credits Earned:</label>
    <asp:Image ID="APCUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="APCreditControl" runat="server" />
    <br />
  </asp:PlaceHolder>

  <asp:PlaceHolder ID="IBCreditContainerControl" runat="server">
    <label for="IBCreditControl">IB Credits Earned:</label>
    <asp:Image ID="IBCUsageTypeIconControl" runat="server" />
    <asp:TextBox ID="IBCreditControl" runat="server" />
    <br />
  </asp:PlaceHolder>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
  <p>No Scholarship Match Criteria Selected. Please go to the <asp:LinkButton ID="BuildMatchCriteriaLinkControl" runat="server" OnClick="BuildMatchCriteriaLinkControl_Click">Build Match Criteria</asp:LinkButton> tab to choose fields to be used for Scholarship selection</p>
</asp:PlaceHolder>

