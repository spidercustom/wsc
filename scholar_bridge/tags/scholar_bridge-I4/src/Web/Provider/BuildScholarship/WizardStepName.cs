﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public enum WizardStepName
    {
        GeneralInformation,
        MatchCriteria,
        SeekerProfile,
        FundingProfile,
        AdditionalCriteria,
        Activate,
        Award,
        CopyOrPrint
    }
}
