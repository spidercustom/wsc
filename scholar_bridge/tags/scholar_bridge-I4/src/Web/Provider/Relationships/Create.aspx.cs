﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Relationships
{
    public partial class Create: Page
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipDAL RelationshipDAL { get; set; }
        public IIntermediaryDAL IntermediaryDAL { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        private const string DEFAULT_PAGEURL = "~/Provider/Relationships/Default.aspx";
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               Page.Form.DefaultButton = cancelBtn.UniqueID;
               Page.Form.DefaultFocus = cancelBtn.UniqueID;
            	var intermediaries =
            		(from imeds in IntermediaryDAL.FindAll("Id") select imeds);
                if (intermediaries.Count()==0)
                {
                    SuccessMessageLabel.SetMessage("No intermediary is available to add relationship request");
                    Response.Redirect(DEFAULT_PAGEURL);
                }
                orgList.DataSource = intermediaries;
                orgList.DataTextField = "Name";
                orgList.DataValueField = "Id";
                orgList.DataBind();
            }
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect(DEFAULT_PAGEURL);
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            var provider = UserContext.CurrentProvider;
            int selectedID = int.Parse(orgList.SelectedValue);
            var intermediary = IntermediaryService.FindById(selectedID);
            var relationship = new Relationship()
                                  {
                                      Provider=provider,
                                      Intermediary=intermediary,
                                      Requester = RelationshipRequester.Provider,
                                      LastUpdate=new ActivityStamp(UserContext.CurrentUser),
                                  };
            RelationshipService.CreateRequest(relationship);
            SuccessMessageLabel.SetMessage("Request Created Successfully.");
            Response.Redirect(DEFAULT_PAGEURL);
            
        }

    }
}
