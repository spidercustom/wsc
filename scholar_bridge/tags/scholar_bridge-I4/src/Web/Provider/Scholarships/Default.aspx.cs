﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            var orgs =
                (from i in RelationshipService.GetByProvider(provider) orderby i.Intermediary.Name
                 select new {i.Intermediary.Id, i.Intermediary.Name}).ToList();

            orgs.Insert(0, new { Id = 0, Name = "- ALL -" });
            
            cboOrganization.DataValueField = "Id";
            cboOrganization.DataTextField = "Name";
            cboOrganization.DataSource = orgs;
            cboOrganization.DataBind();
            FillScholarships();
        }

        private void FillScholarships()
        {
            var provider = UserContext.CurrentProvider;
                
                if (cboOrganization.SelectedValue == "0")
                {
                    //fetch all
                   
                    scholarshipNotActivatedList.Scholarships =
                        ScholarshipService.GetByProviderNotActivated(provider);
                    scholarshipActivatedList.Scholarships =
                        ScholarshipService.GetByProvider(provider, ScholarshipStages.Activated);
                    scholarshipPendingList.Scholarships =
                        ScholarshipService.GetByProvider(provider, ScholarshipStages.RequestedActivation);
                    scholarshipAwardedList.Scholarships =
                        ScholarshipService.GetByProvider(provider, ScholarshipStages.Awarded);

                }
                else
                {
                    var intermediary = IntermediaryService.FindById(int.Parse( cboOrganization.SelectedValue));

                    scholarshipNotActivatedList.Scholarships =
                        ScholarshipService.GetNotActivatedByOrganizations(provider, intermediary);

                    scholarshipActivatedList.Scholarships =
                        ScholarshipService.GetByOrganizations(provider, intermediary, ScholarshipStages.Activated);
                    scholarshipPendingList.Scholarships =
                        ScholarshipService.GetByOrganizations(provider, intermediary, ScholarshipStages.RequestedActivation);
                    scholarshipAwardedList.Scholarships =
                        ScholarshipService.GetByOrganizations(provider, intermediary, ScholarshipStages.Awarded);
                }
            }
        

            

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void cboOrganization_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillScholarships();
        }
    }
}