﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class EditProgramGuidelines : Page
    {
        private const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship Scholarship { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params[SCHOLARSHIP_ID], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
           
            Scholarship = ScholarshipService.GetById(ScholarshipId);
            if (Scholarship != null)
            {
                if (!Scholarship.IsBelongToOrganization(UserContext.CurrentOrganization))
                    throw new InvalidOperationException("Scholarship do not belong to organization in context");

                scholarshipNameLiteral.Text = Scholarship.DisplayName;
                if (! Page.IsPostBack)
                {
                    ProgramGuidelinesControl.Text = Scholarship.ProgramGuidelines;
                }
            }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            Scholarship.ProgramGuidelines = ProgramGuidelinesControl.Text;
            Scholarship.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(Scholarship);

            SuccessMessageLabel.SetMessage("Scholarship Program Guidelines updated.");
            
            Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + ScholarshipId);
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + ScholarshipId);
        }
    }
}
