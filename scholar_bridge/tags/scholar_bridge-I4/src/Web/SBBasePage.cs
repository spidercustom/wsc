﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace ScholarBridge.Web
{
	public class SBBasePage : Spring.Web.UI.Page, IChangeCheckingPage
	{

		public SBBasePage()
		{
			ctlForm = this;
		}


		#region Private Data

		// This is a reference to the form that this page contains.  If the
		// page contains no form, the control points to the page itself.

		private Control ctlForm;

		#endregion

		#region Properties

		/// <summary>
		/// This property is used to set or get a reference to the form
		/// control that appears on the page as indicated by the presence
		/// of a &lt;form&gt; tag.
		/// </summary>
		/// <value>If not explicitly set, it defaults to the form on
		/// the page or the page itself if there isn't one.
		/// <p/>Derived classes can use this property to access the form
		/// in order to insert additional controls into it.</value>
		/// <exception cref="System.ArgumentException">
		/// This property must be set to an
		/// <see cref="System.Web.UI.HtmlControls.HtmlForm"/> or a
		/// <see cref="System.Web.UI.Page"/> object or it will throw an
		/// exception.</exception>
		[Browsable(false),
		 DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Control PageForm
		{
			get { return ctlForm; }
			set
			{
				// Must be derived from one of these types
				if (value is HtmlForm ||
					value is Page)
					ctlForm = value;
				else
					throw new ArgumentException(
						"PageForm must be set to an HtmlForm or Page object");
			}
		}

		#endregion

		#region Form Data Changed checker fields

		/// <summary>
		/// Indicate whether or not to check for changed data entry controls.
		/// </summary>
		/// <remarks>This property is used to indicate whether or not the page
		/// should check for changes in data entry controls and track the dirty
		/// state before leaving the page in some manner (i.e. by clicking
		/// an exit button, clicking the browser's back or forward buttons,
		/// navigating to a different URL, or closing the browser).  If set
		/// to true, the page will render additional JavaScript to help ensure
		/// that the user is prompted that they will lose their changes to
		/// the controls on the page and it gives them an option to cancel
		/// leaving and stay on the current page or continuing and lose the
		/// changes <b>(Internet Explorer only)</b>.  For non-IE browsers,
		/// only dirty state tracking is available.</remarks>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <seealso cref="SkipDataCheckIds"/>
		public bool CheckForDataChanges
		{
			get
			{
				Object oCheck = ViewState["CheckForDataChanges"];
				return (oCheck == null) ? false : (bool)oCheck;
			}
			set { ViewState["CheckForDataChanges"] = value; }
		}

		/// <summary>
		/// This property is used to track the dirty state of the data on
		/// a form if change checking is enabled.
		/// </summary>
		/// <remarks>This property is used to track the dirty state of a
		/// data entry web form.  It can also be used to force the page to
		/// a dirty state so that the <see cref="ConfirmLeaveMessage"/>
		/// is always shown <b>(Internet Explorer only)</b>. This is useful
		/// if the form contains auto-postback controls such as a checkbox
		/// or a button that causes other controls to get enabled or disabled,
		/// get set to specific values, etc.  In such cases, it is impossible
		/// to detect changes, as we no longer have the original values from
		/// before the postback.  Setting this property to true in the event
		/// handler of postback controls will ensure that the user is prompted
		/// to save their changes.</remarks>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <seealso cref="SkipDataCheckIds"/>
		[Browsable(false)]
		public bool Dirty { get; set; }

		/// <summary>
		/// This property is used to set or get the message displayed if
		/// change checking is enabled and the user attempts to leave without
		/// saving the changes <b>(Internet Explorer only)</b>.  A default
		/// message is used if not set.
		/// </summary>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <seealso cref="SkipDataCheckIds"/>
		public string ConfirmLeaveMessage
		{
			get
			{
				Object oLeave = ViewState["ConfirmLeaveMsg"];
				return (oLeave != null)
						? (string)oLeave
						:
							"You haven't saved your changes.  Leaving now " +
							"will lose all changes made.";
			}
			set { ViewState["ConfirmLeaveMsg"] = value; }
		}

		/// <summary>
		/// This is used to set a list of control IDs that should not
		/// trigger the data change check (i.e. a save button).
		/// </summary>
		/// <value>Set it to a string array of control ID values.  Any
		/// controls with IDs specified in this list will allow postback or
		/// leaving of the page without prompting to save <b>(Internet
		/// Explorer only)</b>.</value>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="SkipDataCheckIds"/>
		/// <example>
		/// C#:
		/// <code>
		/// this.BypassPromptIds.AddRange(new [] { "cboApplication",
		///     "chkLimitToTeam", "cboGroupKey", "btnSave",
		///     "btnDelete", "btnCancel" });
		/// </code>
		/// </example>
		public List<string> BypassPromptIds
		{
			get
			{
				Object oBypass = ViewState["BypassList"] ?? (ViewState["BypassList"] = new List<string>());
				return (List<string>)oBypass;
			}
			set { ViewState["BypassList"] = value; }
		}

		/// <summary>
		/// This is used to set a list of control IDs that should not
		/// be included when checking for data changes (i.e. changeable
		/// message text boxes, read-only or criteria fields that get
		/// modified but do not affect the state of the data to save, etc).
		/// </summary>
		/// <value>Set it to a string array of control ID values.  Any
		/// controls with IDs specified in this list will not affect the
		/// dirty state of the page <b>(any browser)</b> and will not
		/// cause prompting when leaving the page <b>(Internet Explorer
		/// only)</b>.</value>
		/// <seealso cref="CheckForDataChanges"/>
		/// <seealso cref="Dirty"/>
		/// <seealso cref="ConfirmLeaveMessage"/>
		/// <seealso cref="BypassPromptIds"/>
		/// <example>
		/// C#:
		/// <code>
		/// this.SkipDataCheckIds.AddRange (new [] { "cboDept", "cboEmployee",
		///     "txtEntryDate" });
		/// </code>
		/// </example>
		public List<string> SkipDataCheckIds
		{
			get
			{
				Object oSkipList = ViewState["SkipList"] ?? (ViewState["SkipList"] = new List<string>());
				return (List<string>)oSkipList;
			}
			set { ViewState["SkipList"] = value; }
		}

		#endregion

		#region Private and Internal methods

		/// <summary>
		/// This is used to find the form on the page
		/// </summary>
		private static Control FindPageForm(Page page)
		{
			MasterPage master;
			Control form = null;

			// Find the form on this page if there is one
			foreach (Control ctlItem in page.Controls)
				if (ctlItem is HtmlForm)
				{
					form = ctlItem;
					break;
				}

			// If not there and it has a master page, search all
			// master pages.
			if (form == null)
			{
				master = page.Master;

				while (master != null && form == null)
				{
					foreach (Control ctlItem in master.Controls)
						if (ctlItem is HtmlForm)
						{
							form = ctlItem;
							break;
						}

					master = master.Master;
				}
			}

			return form;
		}

		#endregion

		/// <summary>
		/// OnInit is overridden to locate the form control on the page and
		/// set the <see cref="PageForm"/> property to it.  For postbacks, it
		/// will also retrieve the value of the dirty flag and store it in
		/// the <see cref="Dirty"/> property.
		/// </summary>
		/// <param name="e">Event arguments</param>
		/// <remarks>If you override this method in a derived class, you
		/// must call this version too.</remarks>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			// Retrieve the state of the Dirty flag (if used)
			Dirty = Convert.ToBoolean(Request.Form["BP_bIsDirty"],
									  CultureInfo.InvariantCulture);

			// Find the form on the page if there is one
			if (ctlForm is Page)
			{
				Control form = FindPageForm((Page)ctlForm);

				if (form != null)
					ctlForm = form;
			}
		}

		/// <summary>
		/// OnPreRender is overridden to generate the data change checking
		/// variables and script and the focused control script if necessary.
		/// It also adds some additional tags to the page header if needed.
		/// </summary>
		/// <param name="e">Event arguments</param>
		/// <remarks>If you override this method in a derived class, you
		/// must call this version too.
		/// <p/>If a page header control exists, the following additional tags
		/// are inserted into it using the values from the related properties
		/// in this class.
		/// <code>
		/// &lt;meta name="GENERATOR" content="APPNAME.CLASSNAME Class"&gt;
		/// &lt;meta name="Title" content="BasePage.PageTitle"&gt;
		/// &lt;meta name="Description" content="BasePage.PageDescription"&gt;
		/// &lt;meta name="Keywords" content="BasePage.PageKeywords"&gt;
		/// &lt;meta name="Robots" content="BasePage.Robots"&gt;
		/// </code>
		/// <p/>The <b>APPNAME.CLASSNAME</b> section of the &lt;meta name&gt;
		/// tag will contain the class name of the object that generated the
		/// HTML page.  <b>BasePage.PageTitle</b> is also used to set the
		/// <b>Page.Title</b> property.
		/// </remarks>
		protected override void OnPreRender(EventArgs e)
		{
			StringBuilder sb;

			base.OnPreRender(e);

			// If data change checking has been requested, output the dirty
			// flag, exclusion arrays, confirm message, and the script.
			if (CheckForDataChanges)
			{
				// Register a hidden field so that the client can pass
				// back changes to the Dirty flag.
				Page.ClientScript.RegisterHiddenField("BP_bIsDirty",
													  Dirty.ToString(CultureInfo.InvariantCulture).ToLower(
														CultureInfo.InvariantCulture));

				// Register an OnSubmit function call so that we can get the
				// state of the Dirty flag and put it in the hidden field.  It
				// can't occur in the OnBeforeUnload event as everything has
				// been packaged up ready for sending to the server and changes
				// made in that event don't get sent to the server.
				//Page.ClientScript.RegisterOnSubmitStatement(
				//    typeof(SBBasePage), "BP_DirtyCheck",
				//    "__UnloadPage();");

				// Create a script block containing the array declarations,
				// the data loss message variable, the dirty flag, and the
				// change checking script.
				sb = new StringBuilder(
					"<script type='text/javascript'>\n<!--\n" +
					"var BP_arrBypassList = new Array(", 4096);

				string[] idList = BypassPromptIds.ToArray();
				if (idList.Length > 0)
				{
					sb.Append('\"');
					sb.Append(String.Join("\",\"", idList));
					sb.Append('\"');
				}

				sb.Append(");\nvar BP_arrSkipList = new Array(");
				idList = SkipDataCheckIds.ToArray();
				if (idList.Length > 0)
				{
					sb.Append('\"');
					sb.Append(String.Join("\",\"", idList));
					sb.Append('\"');
				}

				sb.Append(");\nvar BP_strDataLossMsg = \"");
				sb.Append(ConfirmLeaveMessage);
				sb.Append("\";\nvar BP_strFormName = \"");

				// BP_strFormName tells the script what form to use
				// for the change checking.
				sb.Append(PageForm.UniqueID);
				sb.Append("\";\n//-->\n</script>\n");

				ClientScript.RegisterClientScriptBlock(
					typeof(SBBasePage), "BP_DCCJS", sb.ToString());

				ClientScript.RegisterClientScriptInclude(
					typeof(SBBasePage), "BP_DCCJSFile",
				ResolveUrl("~/js/DataChange.js"));
			}
		}
	}

}
