﻿$(document).ready(function() {
    // convert print divs to clickable things that cause a print to occur
    $(".print").mouseup(function() {
        window.print();
    }).hover(
        function() { $(this).attr("style", "cursor: pointer;"); },
        function() { $(this).attr("style", "cursor: default;"); }
    );

    // close a parent element
    $(".closeBox").click(function(event) {
        $(this).parent().fadeOut();
    }).hover(
        function() { $(this).attr("style", "cursor: pointer;"); },
        function() { $(this).attr("style", "cursor: default;"); }
    );

    try {
        // make table class="sortableTable" sortable
        $(".sortableTable").tablesorter();
    }
    catch (exception) {
    }

    // make div class="tabs" int tabs
    $(".tabs").tabs();

    try {
        $("ul.menu").superfish({
            pathClass: 'current'
        });
    }
    catch (exception) {
    }
});

function SelectAllCheckBoxIn(parentId) {
    CheckUncheckCheckBoxesIn(parentId, true);
}

function DeselectAllCheckBoxIn(parentId) {
    CheckUncheckCheckBoxesIn(parentId, false);
}

function CheckUncheckCheckBoxesIn(parentId, checkState) {
    $("#" + parentId + " > *").find("input:checkbox").each(function() {
        this.checked = checkState;
    });
}

function EnableDisableControls(parentId, controllerCheckBoxId, enableOnCheck) {
    var disableElements = enableOnCheck;
    if ($('#' + controllerCheckBoxId).is(':checked'))
        disableElements = !enableOnCheck;
    $("#" + parentId + " :input").filter(function(index) {
        return $(this).attr("id") != controllerCheckBoxId;
    }).attr('disabled', disableElements);
}

function yesnodialog(dialogdiv,successurl) {
    $(dialogdiv).dialog({
        autoOpen: true,
        width: 400,
        height: 150,
        modal: true,
        resizable: true,
        buttons: {
            "Yes": function() {
                $(this).dialog("destroy");
                if (successurl == '')
                    return true;
                else
                    window.location = successurl;

            },
            "No": function() {
                $(this).dialog("destroy");
                return false;

            }
        }
    });
    return false;
}

function alertdialog(msg,title) {
    var dlg = $('<div title='+title+' >' +msg+'</div>');
    $(dlg).dialog({
        autoOpen: true,
        width: 400,
        height: 150,
        modal: true,
        resizable: true,
        buttons: {
            "OK": function() {
                $(this).dialog("destroy");
                $(dlg).remove();
                return false;
            }
        }
    });
    
}