﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Basics" %>
<%@ Register Src="~/Common/LookupItemCheckboxList.ascx" TagName="LookupItemCheckboxList"
    TagPrefix="sb" %>
<h3>
    My Profile – Basics</h3>
<br />
<table>
    <tr>
        <td colspan="6">
            My Name:
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="First: " />
        </td>
        <td>
            <asp:TextBox ID="FirstNameBox" runat="server" Columns="20" MaxLength="50" OnTextChanged="FirstNameBox_TextChanged"></asp:TextBox>
        </td>
        <td>
            <asp:Label runat="server" Text="Mid: "></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="MidNameBox" runat="server" Columns="10" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:Label runat="server" Text="Last: "></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="LastNameBox" runat="server" Columns="20" MaxLength="50"></asp:TextBox>
        </td>
    </tr>
</table>
<br />
<fieldset>
    <table>
        <tr>
            <td colspan="6" >
                My Permanent Address:
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Line 1: " />
            </td>
            <td colspan="5">
                <asp:TextBox ID="AddressLine1Box" runat="server" Columns="35" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Line 2: "></asp:Label>
            </td>
            <td colspan="5">
                <asp:TextBox ID="AddressLine2Box" runat="server" Columns="35" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="City: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="CityBox" runat="server" Columns="25" MaxLength="50"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="Label5" runat="server" Text="State: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="StateDropDown" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Zip: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="ZipBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
    </table>
</fieldset>
<br />
<fieldset>
<asp:Label ID="Label7" runat="server" Text="My Gender: "></asp:Label><br /><br />
                <asp:RadioButtonList ID="GenderButtonList" runat="server">
                    <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Female" Value="2"></asp:ListItem>
                </asp:RadioButtonList><br />
                <br /><br />
</fieldset>

<fieldset>
    <label id="ReligionLabelControl" for="ReligionControl">
        My Religion/Faith:
    </label><br /><br />
    <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true"
        ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" />
    <table>
        <tr>
            <td>Other Religion?&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:TextBox ID="OtherReligion" runat="server"></asp:TextBox>                        
                
            </td>
        </tr>
    
    </table>
    <br /><br />
</fieldset>


<fieldset>
    <asp:Label ID="Label9" runat="server" Text="My Heritage: "></asp:Label><br /><br />
    <br /><br />
                    <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true"
                        ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" />
    <table>
        <tr>
            <td>Other Ethnicity?&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:TextBox ID="OtherEthnicity" runat="server"></asp:TextBox>                        
                
            </td>
        </tr>

    </table>
</fieldset>



