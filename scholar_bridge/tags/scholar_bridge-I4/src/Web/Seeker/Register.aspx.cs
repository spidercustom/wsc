﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Seeker
{
    public partial class Register : Page
    {
        public IUserDAL UserService { get; set; }
        public ISeekerService SeekerService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
        {
            var cuw = (CreateUserWizard)sender;
            cuw.Email = cuw.UserName;
        }

        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            User user = GetCurrentUser(sender);
            user.Name.FirstName = GetTextBoxValue(CreateUserWizardStep1, "FirstName");
            user.Name.MiddleName = GetTextBoxValue(CreateUserWizardStep1, "MiddleName");
            user.Name.LastName = GetTextBoxValue(CreateUserWizardStep1, "LastName");

            var seeker = new Domain.Seeker
                             {
                                 User = user,
                                 LastUpdate = new ActivityStamp(user)
                             };
            SeekerService.SaveNew(seeker);
        }

        private User GetCurrentUser(object sender)
        {
            var cuw = (CreateUserWizard)sender;
            return UserService.FindByUsername(cuw.Email);
        }

        private static string GetTextBoxValue(TemplatedWizardStep step, string name)
        {
            return GetTextBoxFromStep(step, name).Text;
        }

        private static TextBox GetTextBoxFromStep(TemplatedWizardStep step, string name)
        {
            return (TextBox)step.ContentTemplateContainer.FindControl(name);
        }

        protected void CreateUserWizard1_CreateUserError(object sender, CreateUserErrorEventArgs e)
        {
            switch (e.CreateUserError)
            {
                case MembershipCreateStatus.DuplicateUserName:
                case MembershipCreateStatus.DuplicateEmail:
                    var valid = (PropertyProxyValidator)
                                CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserNameValidator");
                    valid.Text = "EmailAddress is already in use, please choose another one.";
                    valid.IsValid = false;
                    break;
            }
        }

        protected void CreateUserWizard1_SendingMail(object sender, MailMessageEventArgs e)
        {
            // We handled sending it ourselves in the SaveNew, so cancel
            e.Cancel = true;
        }
    }
}
