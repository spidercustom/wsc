﻿using System;
using System.Web;

namespace ScholarBridge.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect(loginForm.DestinationForUser(HttpContext.Current.User.Identity.Name));
            }
        }
    }
}
