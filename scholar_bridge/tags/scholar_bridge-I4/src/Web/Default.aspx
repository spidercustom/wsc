﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.DefaultPage" Title="Welcome" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <h2>Welcome to ScholarBridge</h2>
    <p>
        This will be the initial page for the entire site. It will have a seeker focus and
        contain the seeker value proposition.
    </p>

    <sb:Login ID="loginForm" runat="server" />
    
    <br />    
    <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Seeker/Register.aspx">Create Seeker Login</asp:HyperLink>
    <asp:HyperLink ID="seekerTCLnk" runat="server" NavigateUrl="~/Seeker/Terms.aspx">Seeker Terms &amp; Conditions</asp:HyperLink>
    
    <br />
    <asp:HyperLink ID="providerLnk" runat="server" NavigateUrl="~/Provider/Default.aspx">Scholarship Providers Home</asp:HyperLink>
    <br />
    <asp:HyperLink ID="intermediaryLnk" runat="server" NavigateUrl="~/Intermediary/Default.aspx">Scholarship Intermediaries Home</asp:HyperLink>
</asp:Content>
