﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Message.Default" Title="Message" %>

<%@ Register TagPrefix="sb" TagName="MessageList" Src="~/Common/MessageList.ascx" %>
<%@ Register TagPrefix="sb" TagName="SentMessageList" Src="~/Common/SentMessageList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div class="tabs">
    <ul>
        <li><a href="#work-tab"><span>Work Queue</span></a></li>
        <li><a href="#archived-tab"><span>Archived</span></a></li>
        <li><a href="#sent-tab"><span>Sent</span></a></li>
    </ul>
    <div id="work-tab">
       <h3>Work Queue</h3>
        <sb:MessageList id="messageList" runat="server" LinkTo="~/Message/Show.aspx" />
    </div>
    <div id="archived-tab">
        <h3>Archived</h3>
        <sb:MessageList id="archivedMessageList" runat="server" Archived="true" LinkTo="~/Message/Show.aspx" />
    </div>
    <div id="sent-tab">
        <h3>Sent</h3>
        <sb:SentMessageList id="sentMessageList" runat="server" LinkTo="~/Message/Show.aspx" />
    </div>
</div>

</asp:Content>
