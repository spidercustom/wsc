﻿using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipScheduleTest : TestBase
    {
        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        private User user;
        private Provider provider;
        private ScholarshipStages stage;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            stage = ScholarshipStages.GeneralInformation;
        }

        [Test]
        public void check_schedule_bymonth_persist_and_can_be_retrived()
        {
            var scholarship = SaveScholarship(ScholarshipDALTest.CreateScheduleByMonthDay());
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(retrivedScholarship);
            Assert.IsNotNull(retrivedScholarship.Schedule);
            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.AreEqual(typeof(ScholarshipScheduleByMonthDay), retrivedScholarship.Schedule.GetType());
        }

        [Test]
        public void check_schedule_byweek_persist_and_can_be_retrived()
        {
            var scholarship = SaveScholarship(ScholarshipDALTest.CreateScheduleByWeekDay());
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(retrivedScholarship);
            Assert.IsNotNull(retrivedScholarship.Schedule);
            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.AreEqual(typeof(ScholarshipScheduleByWeekDay), retrivedScholarship.Schedule.GetType());
        }

        [Test]
        public void can_update_schedule_with_new_type()
        {
            var scholarship = SaveScholarship(ScholarshipDALTest.CreateScheduleByMonthDay());

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            retrivedScholarship.Schedule = ScholarshipDALTest.CreateScheduleByWeekDay();
            retrivedScholarship.LastUpdate = new ActivityStamp(user);
            retrivedScholarship = ScholarshipDAL.Save(retrivedScholarship);

            var afterUpdate = ScholarshipDAL.FindById(retrivedScholarship.Id);

            Assert.IsNotNull(afterUpdate);
            Assert.IsNotNull(afterUpdate.Schedule);
            Assert.AreEqual(typeof(ScholarshipScheduleByWeekDay), afterUpdate.Schedule.GetType());
        }

        private Scholarship SaveScholarship(ScholarshipScheduleBase schedule)
        {
            var scholarship = ScholarshipDALTest.CreateTestObject(user, provider, stage);
            scholarship.Schedule = schedule;
            scholarship = ScholarshipDAL.Save(scholarship);
            ScholarshipDAL.Evict(scholarship);
            return scholarship;
        }
    }
}
