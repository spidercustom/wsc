﻿using System.Collections.Generic;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public interface IMatchLocationCriteria
    {
        State State { get; set; }
        StateDependentLocation StateDependentLocation { get; }
        IList<ILookup> StateDependentLocations { get; }
        void ResetStateDependentLocations(IList<ILookup> list);
    }

    public enum StateDependentLocation
    {
        None,
        County,
        City,
        SchoolDistrict,
        HighSchool
    }
}
