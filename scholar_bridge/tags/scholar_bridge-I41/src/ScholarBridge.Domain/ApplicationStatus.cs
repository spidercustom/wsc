﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
	/// <summary>
	/// ApplicationStatus shows the current state of a given Seeker Application
	/// Note! MatchApplicationStatus and ApplicationStatus are closely tied.
	/// </summary>
    public enum ApplicationStatus
    {
		Unknown, 

        [DisplayName("Started")]
        Applying,

        [DisplayName("Submitted")]
        Submitted,

        [DisplayName("Being considered")]
        BeingConsidered,

        Offered,
        Closed,
        Awarded
    }
}
