using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.SeekerParts
{
    public class SeekerAcademics:ICloneable
    {
        public virtual double? GPA { get; set; }
        public virtual ClassRank ClassRank { get; set; }

        public virtual SatScore SATScore { get; set; }
        public virtual ActScore ACTScore { get; set; }

        public virtual bool Honors { get; set; }
        public virtual bool APCredits { get; set; }
        public virtual bool IBCredits { get; set; }

        public virtual string  HonorsDetail { get; set; }
        public virtual string APCreditsDetail { get; set; }
        public virtual string IBCreditsDetail { get; set; }

        //[DomainValidator((SchoolTypes)0, MessageTemplate = "School type must be selected", Negated = true, Ruleset = Seeker.PROFILE_ACTIVATION_RULESET)]
        public virtual SchoolTypes SchoolTypes { get; set; }
       // [DomainValidator((AcademicPrograms)0, MessageTemplate = "Academic programs must be selected", Negated = true, Ruleset = Seeker.PROFILE_ACTIVATION_RULESET)]
        public virtual AcademicPrograms AcademicPrograms { get; set; }
        public virtual SeekerStatuses SeekerStatuses { get; set; }
        public virtual ProgramLengths ProgramLengths { get; set; }

        public virtual IList<College> CollegesApplied { get; protected set; }
        public virtual string CollegesAppliedOther { get; set; }
        
        public virtual bool IsCollegeAppliedInWashington { get; set; }
        
        public virtual bool IsCollegeAppliedOutOfState { get; set; }


        public virtual IList<College> CollegesAccepted { get; protected set; }
        public virtual string CollegesAcceptedOther { get; set; }

        public ValidationResults ValidateActivation()
        {
            var validator = ValidationFactory.CreateValidator<SeekerAcademics>(Seeker.PROFILE_ACTIVATION_RULESET);
            return validator.Validate(this);
        }

        #region ICloneable Members

        public virtual object Clone()
        {
            var result = (SeekerAcademics)MemberwiseClone();
            result.CollegesAccepted = new  List<College>(CollegesAccepted);
            result.CollegesApplied = new List<College>(CollegesApplied);
            return result;
        }

        #endregion
    }
}