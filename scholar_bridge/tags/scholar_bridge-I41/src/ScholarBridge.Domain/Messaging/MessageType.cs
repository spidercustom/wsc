﻿using System;

namespace ScholarBridge.Domain.Messaging
{
    public enum MessageType
    {
        // a mail template enum should be the firstname of email template file
        // for example if ProverDenied is enum then system will look for
        // providerdenied.mail file.
        None,
        AwardScholarship,
        ConfirmationLink,
        ForgotPassword,
        IntermediaryApproved,
        IntermediaryRejected,
        OfferScholarship,
        ProviderRejected,
        ProviderApproved,
        RequestIntermediaryApproval,
        RequestProviderApproval,
        RelationshipCreateRequest,
        RelationshipInactivated,
        ScholarshipApproved,
        ScholarshipRejected,
        ScholarshipClosed,
        RelationshipRequestApproved,
        RelationshipRequestRejected,
        ListChangeRequest,
        EmailAddressChangeVerificationLink,
        ApplicationDue,
        ApplicationSubmisionSuccess,
        ApplicationAcknowledgement,
        ScholarshipSendToFriend,
        SeekerInterestedToRegister,
		ContactUsSupport,
		ContactUsProblem,
		ContactUsSuggestion,
        RequestOnlineApplicationApproval,
        ScholarshipOnlineApplicationUrlApproved,
        ScholarshipOnlineApplicationUrlRejected
    }

    public static class MessageTypeExtensions
    {
        public static string TemplateName(this MessageType messageType)
        {
            return string.Format("{0}.mail", Enum.GetName(typeof (MessageType), messageType));
        }
    }
}