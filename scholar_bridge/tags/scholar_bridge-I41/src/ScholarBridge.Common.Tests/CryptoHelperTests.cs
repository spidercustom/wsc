﻿using NUnit.Framework;
namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class CryptoHelperTests
    {
        [Test]
        public void EncryptDecryptTest()
        {
            string data = "id=Blue1$Umbr*ella&key=98&url=abc.aspx";
            string actual = CryptoHelper.Encrypt (data);
            string expected = CryptoHelper.Decrypt(actual);

            Assert.AreEqual(data, expected);
        }
    }
}
