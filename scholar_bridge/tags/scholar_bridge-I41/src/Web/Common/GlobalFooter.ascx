﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalFooter.ascx.cs" Inherits="ScholarBridge.Web.Common.GlobalFooter" %>
<div id="Footer">
    <div style="float:right;">
        <a href="http://www.facebook.com/pages/theWashBoardorg/208349032424"><img src="/images/FaceBook_32x32.png" /></a>
        <a style="margin-left:10px;" href="http://twitter.com/thewashboardorg"><img src="/images/Twitter_32x32.png" /></a>
    </div>
    <a href='<%= LinkGenerator.GetFullLink("/AboutUs.aspx") %>'>About Us</a>  |  
    <a href='<%= LinkGenerator.GetFullLink("/PressRoom/") %>'>Press Room</a>  |  
    <a href='<%= LinkGenerator.GetFullLink("/Terms.aspx") %>'>Terms &amp; Conditions</a>  |  
    <a href='<%= LinkGenerator.GetFullLink("/ContactUs.aspx") %>'>Contact Us</a>  |  
    <a href='<%= ResolveUrl("~/SiteMap.aspx") %>'>Sitemap</a>  |  
    <a href='<%= LinkGenerator.GetFullLink("/Resources/") %>'>Resources</a>  |  
    <a href='<%= LinkGenerator.GetFullLink("/Help.aspx") %>'>Help</a>  
    <a id="loginLink" runat="server">| Log In</a>
    <div style="padding-top:5px; clear:left;">&copy; 2009 Washington Scholarship Coalition</div>
</div>
