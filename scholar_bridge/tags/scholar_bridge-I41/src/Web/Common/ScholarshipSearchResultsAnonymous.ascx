﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipSearchResultsAnonymous.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipSearchResultsAnonymous" %>
<%@ Import Namespace="ScholarBridge.Web"%>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<%--<%= "iisversion: " + IISVersion.IISMainModule.FileName + " " + IISVersion.FullVersionInfo %>
--%>
<asp:ListView ID="lstScholarships" runat="server"
                onpagepropertieschanging="lstScholarships_PagePropertiesChanging">
    <LayoutTemplate>
    <table class="sortableTable">
        <thead>
            <tr>
                <th>Scholarship Name</th>
                <th>Provider/Intermediary</th>
                <th>Donor</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><a href='<%# (IISVersion.RunningIIS7 ? ResolveUrl("~/ScholarshipDetails") : ResolveUrl("~/ScholarshipDetails.aspx")) + Eval("Element(\"UrlKey\").Value") %>'><%# Eval("Element(\"Name\")")%></a></td>
        <td><%# Eval("Element(\"ProviderName\")")%></td>
        <td><%# Eval("Element(\"DonorName\")")%></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><a href='<%# (IISVersion.RunningIIS7 ? ResolveUrl("~/ScholarshipDetails") : ResolveUrl("~/ScholarshipDetails.aspx")) + Eval("Element(\"UrlKey\").Value") %>'><%# Eval("Element(\"Name\")")%></a></td>
        <td><%# Eval("Element(\"ProviderName\")")%></td>
        <td><%# Eval("Element(\"DonorName\")")%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships matching search criteria, please try again using new criteria. </p>
    
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="lstScholarships" PageSize="20" onprerender="pager_PreRender" >
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
</div> 
