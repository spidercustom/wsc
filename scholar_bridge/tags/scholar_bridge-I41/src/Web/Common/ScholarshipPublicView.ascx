﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipPublicView.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipPublicView" %>

<div>
    <div class="viewLabel">Scholarship Name:</div> <div class="viewValue"><asp:Literal ID="scholarshipName" runat="server" /></div>
    <br />
</div>

<div>
    <div class="viewLabel">Provider Name:</div> <div class="viewValue"><asp:Literal ID="providerName" runat="server" /></div>
    <br />
</div>

<div>
    <div class="viewLabel">Intermediary Name:</div> <div class="viewValue"><asp:Literal ID="intermediaryName" runat="server" /></div>
    <br />
</div>

<div>
    <div class="viewLabel">Donor Name:</div> <div class="viewValue"><asp:Literal ID="donorName" runat="server" /></div>
    <br />
</div>

<div>
    <div class="viewLabel">Mission Statement:</div> <div class="viewValue"><asp:Literal ID="missionStatement" runat="server" /></div>
    <br />
</div>

<div>
    <div >Sign-up to apply to this and other scholarships at <asp:HyperLink ID="registrationLink" runat="server" ></asp:HyperLink>  </div>
    <br />
</div>

<div > <a href="<%= ResolveUrl("~/Seeker/Default.aspx") %>">Login for existing users</a>  </div>
<br />
