﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FaqList.ascx.cs" Inherits="ScholarBridge.Web.Common.FaqList" %>
<asp:Repeater ID="faqRepeater" runat="server">
    <ItemTemplate>
        <b>Q:</b>
        <%# Eval("Question") %><br />
        <b>A:</b>
        <%# Eval("Answer") %><br /><br />
    </ItemTemplate>
</asp:Repeater>
