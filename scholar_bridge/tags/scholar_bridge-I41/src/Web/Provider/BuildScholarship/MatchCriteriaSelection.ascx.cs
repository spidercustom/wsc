﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;


namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class MatchCriteriaSelection : WizardStepUserControlBase<Scholarship>
    {
        public MatchCriteriaSelection()
        {
            SetupDefaultUpdaters();    
        }

        public IScholarshipService ScholarshipService { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        public IUserContext UserContext { get; set; }

        private bool ImpersonationInProgress
        {
            get { return ImpersonateUser.IsImpersonationInProgress(Session); }
        }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        #region default value update
        private readonly Dictionary<SeekerProfileAttribute, Action> defaultUpdaters = new Dictionary<SeekerProfileAttribute, Action>();

        private void SetupDefaultUpdaters()
        {
            defaultUpdaters.Add(SeekerProfileAttribute.StudentGroup, () => 
                ScholarshipInContext.SeekerProfileCriteria.StudentGroups =
                (StudentGroups)EnumExtensions.SelectAll(typeof(StudentGroups)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.SchoolType, () => 
                ScholarshipInContext.SeekerProfileCriteria.SchoolTypes = 
                (SchoolTypes)EnumExtensions.SelectAll(typeof(SchoolTypes)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.AcademicProgram, () => 
                ScholarshipInContext.SeekerProfileCriteria.AcademicPrograms = 
                (AcademicPrograms)EnumExtensions.SelectAll(typeof(AcademicPrograms)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.SeekerStatus, () => 
                ScholarshipInContext.SeekerProfileCriteria.SeekerStatuses = 
                (SeekerStatuses) EnumExtensions.SelectAll(typeof(SeekerStatuses)));
            
        }

        private void PopulateDefaults(SeekerProfileAttribute attribute)
        {
            if (defaultUpdaters.ContainsKey(attribute))
            {
                defaultUpdaters[attribute]();
            }
        }

        #endregion
         
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetCollegeRadioButtons();
                PopulateScreen();

                SetButtonsToSkip();
                
               
            }
            SetSelectAllTypesOfSupportButtonEvent();
        }

        private void SetButtonsToSkip()
        {
            if (Page is IChangeCheckingPage)
            {
                var ccPage = (IChangeCheckingPage)Page;
                ccPage.BypassPromptIds.AddRange(new[]
			                                        {
			                                            StudentGroupControl.SelectAllButtonClientID,
                                                        SchoolTypeControl.SelectAllButtonClientID,
                                                        AcademicProgramControl.SelectAllButtonClientID,
                                                        SeekerStatusControl.SelectAllButtonClientID,
                                                        SchoolTypeControl.SelectAllButtonClientID,
                                                        SelectAllTypesOfSupporButton.ClientID

			                                        });
            }
        }
        private void SetCollegeRadioButtons()
        {
            CollegesRadioButtonAny.Attributes.Add("onclick", "$('#" + CollegesControl.ClientID+"').val('');disableElementById('" + CollegesControlDialogButton.SelectButton.ClientID + "');");
            CollegesRadioButtonWashington.Attributes.Add("onclick", "$('#" + CollegesControl.ClientID + "').val('');disableElementById('" + CollegesControlDialogButton.SelectButton.ClientID + "');");
            CollegesRadioButtonOutOfState.Attributes.Add("onclick", "$('#" + CollegesControl.ClientID + "').val('');disableElementById('" + CollegesControlDialogButton.SelectButton.ClientID + "');");
       
            CollegesRadioButtonSpecify.Attributes.Add("onclick", "enableElementById('" + CollegesControlDialogButton.SelectButton.ClientID + "')");
           }

        public void SetCollegesRadioButtonState(bool state)
        {
            CollegesControlDialogButton.SelectButton.Enabled = state;
         
        }
        private void SetSelectAllTypesOfSupportButtonEvent()
        {

            var scriptname = "SelectAllTypesOfSupportButton_script";
            var script = "$('#" + SelectAllTypesOfSupporButton.ClientID + "').click(function() {$('#" + TypesOfSupport.ClientID +
                         "').find(':checkbox').attr('checked', 'checked');});";
            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);

        }
        private void DisableControls()
        {
            StudentGroupControl.DisableControls();
            CollegesControl.Enabled = false;
            CollegesRadioButtonAny.Enabled = false;
            CollegesRadioButtonOutOfState.Enabled = false;
            CollegesRadioButtonSpecify.Enabled = false;
            CollegesRadioButtonWashington.Enabled = false;
            CollegesControlDialogButton.SelectButton.Enabled = false;
            AcademicProgramControl.DisableControls();
            SeekerStatusControl.DisableControls();
            TypesOfSupport.Enabled = false;
            SchoolTypeControl.DisableControls();
        }

        private void PopulateScreen()
        {
            if (ScholarshipInContext.IsActivated() && !ImpersonationInProgress)
                DisableControls();
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            StudentGroupControl.SelectedValues = (int)seekerProfileCriteria.StudentGroups;

            switch (seekerProfileCriteria.CollegeType)
            {
                case CollegeType.Any:
                    CollegesRadioButtonAny.Checked = true;
                    SetCollegesRadioButtonState(false );
                    break;
                case CollegeType.InWashingTonState:
                    
                    CollegesRadioButtonWashington.Checked = true;
                    SetCollegesRadioButtonState(false);
                    break;
                case CollegeType.Specify:
                    CollegesRadioButtonSpecify.Checked = true;
                    SetCollegesRadioButtonState(true);
                    break;
                case CollegeType.OutOfState:
                    
                    CollegesRadioButtonOutOfState.Checked = true;
                    SetCollegesRadioButtonState(true);
                    break;
                default:
                    CollegesRadioButtonAny.Checked = true;
                    SetCollegesRadioButtonState(false );
                    
                    break;
            }
            CollegesControlDialogButton.Keys = seekerProfileCriteria.Colleges.CommaSeparatedIds();
            SchoolTypeControl.SelectedValues = (int)seekerProfileCriteria.SchoolTypes;
            AcademicProgramControl.SelectedValues = (int)seekerProfileCriteria.AcademicPrograms;
            SeekerStatusControl.SelectedValues = (int)seekerProfileCriteria.SeekerStatuses;
            PopulateListControl(TypesOfSupport, SupportDAL.FindAll());
            if (null != ScholarshipInContext.FundingProfile.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(ScholarshipInContext.FundingProfile.SupportedSituation.TypesOfSupport,
                                                 ts => ts.Id.ToString());
            }
            
        }
        public void PopulateListControl(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
        public override bool ValidateStep()
        {
            bool result = true;

            if (StudentGroupControl.SelectedValues==0){
                result = false;
                StudentGroupValidator.IsValid = false;
            }

            if (CollegesRadioButtonSpecify.Checked)
            {
                if (CollegesControlDialogButton.Keys.Count() == 0)
                {
                    result = false;
                    CollegesControlValidator.IsValid = false;
                }
            }

            if (SchoolTypeControl.SelectedValues == 0)
            {
                result = false;
                SchoolTypeControlValidator.IsValid = false;
            }

            if (AcademicProgramControl.SelectedValues == 0)
            {
                result = false;
                AcademicProgramControlValidator.IsValid = false;
            }
            
            if (SeekerStatusControl.SelectedValues == 0)
            {
                result = false;
                SeekerStatusControlValidator.IsValid = false;
            }

            if (TypesOfSupport.SelectedIndex<0)
            {
                result = false;
                TypesOfSupportControlValidator.IsValid = false;
            }
             
            return result;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override void PopulateObjects()
        {
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            seekerProfileCriteria.StudentGroups = (StudentGroups)StudentGroupControl.SelectedValues;
            if (CollegesRadioButtonAny.Checked)
                seekerProfileCriteria.CollegeType = CollegeType.Any;
            if (CollegesRadioButtonOutOfState.Checked)
                seekerProfileCriteria.CollegeType = CollegeType.OutOfState;

            if (CollegesRadioButtonWashington.Checked)
                seekerProfileCriteria.CollegeType = CollegeType.InWashingTonState;

            if (CollegesRadioButtonSpecify.Checked)
                seekerProfileCriteria.CollegeType = CollegeType.Specify;

            if (seekerProfileCriteria.CollegeType != CollegeType.Specify)
                CollegesControlDialogButton.Keys = "";

            
            PopulateList(CollegesContainerControl, CollegesControlDialogButton, seekerProfileCriteria.Colleges);
            
            seekerProfileCriteria.SchoolTypes = (SchoolTypes)SchoolTypeControl.SelectedValues;
            seekerProfileCriteria.AcademicPrograms =
                (AcademicPrograms)AcademicProgramControl.SelectedValues;
            seekerProfileCriteria.SeekerStatuses = (SeekerStatuses)SeekerStatusControl.SelectedValues;

            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            ScholarshipInContext.FundingProfile.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStages.NotActivated;
            
        }
        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        
        }
    
}