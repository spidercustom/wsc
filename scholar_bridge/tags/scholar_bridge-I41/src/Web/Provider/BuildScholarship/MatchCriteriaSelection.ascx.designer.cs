﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScholarBridge.Web.Provider.BuildScholarship {
    
    
    public partial class MatchCriteriaSelection {
        
        /// <summary>
        /// StudentGroupContainerControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder StudentGroupContainerControl;
        
        /// <summary>
        /// StudentGroupControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList StudentGroupControl;
        
        /// <summary>
        /// CoolTipInfoStudentGroupControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.CoolTipInfo CoolTipInfoStudentGroupControl;
        
        /// <summary>
        /// StudentGroupValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator StudentGroupValidator;
        
        /// <summary>
        /// CollegesContainerControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder CollegesContainerControl;
        
        /// <summary>
        /// CoolTipInfoCollegesControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.CoolTipInfo CoolTipInfoCollegesControl;
        
        /// <summary>
        /// CollegesRadioButtonAny control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton CollegesRadioButtonAny;
        
        /// <summary>
        /// CollegesRadioButtonWashington control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton CollegesRadioButtonWashington;
        
        /// <summary>
        /// CollegesRadioButtonOutOfState control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton CollegesRadioButtonOutOfState;
        
        /// <summary>
        /// CollegesRadioButtonSpecify control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton CollegesRadioButtonSpecify;
        
        /// <summary>
        /// CollegesControlDialogButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.Lookup.LookupDialog CollegesControlDialogButton;
        
        /// <summary>
        /// CollegesControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox CollegesControl;
        
        /// <summary>
        /// CollegesControlValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator CollegesControlValidator;
        
        /// <summary>
        /// SchoolTypeContainerControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder SchoolTypeContainerControl;
        
        /// <summary>
        /// SchoolTypeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList SchoolTypeControl;
        
        /// <summary>
        /// CoolTipInfoSchoolTypeControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.CoolTipInfo CoolTipInfoSchoolTypeControl;
        
        /// <summary>
        /// SchoolTypeControlValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator SchoolTypeControlValidator;
        
        /// <summary>
        /// AcademicProgramContainerControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder AcademicProgramContainerControl;
        
        /// <summary>
        /// CoolTipInfoAcademicProgramControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.CoolTipInfo CoolTipInfoAcademicProgramControl;
        
        /// <summary>
        /// AcademicProgramControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList AcademicProgramControl;
        
        /// <summary>
        /// AcademicProgramControlValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator AcademicProgramControlValidator;
        
        /// <summary>
        /// SeekerStatusContainerControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder SeekerStatusContainerControl;
        
        /// <summary>
        /// CoolTipInfoSeekerStatusControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.CoolTipInfo CoolTipInfoSeekerStatusControl;
        
        /// <summary>
        /// SeekerStatusControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.FlagEnumCheckBoxList SeekerStatusControl;
        
        /// <summary>
        /// SeekerStatusControlValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator SeekerStatusControlValidator;
        
        /// <summary>
        /// TypesOfSupportContainerControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder TypesOfSupportContainerControl;
        
        /// <summary>
        /// CoolTipInfoTypesOfSupport control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ScholarBridge.Web.Common.CoolTipInfo CoolTipInfoTypesOfSupport;
        
        /// <summary>
        /// TypesOfSupportControlValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator TypesOfSupportControlValidator;
        
        /// <summary>
        /// TypesOfSupport control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList TypesOfSupport;
        
        /// <summary>
        /// SelectAllTypesOfSupporButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton SelectAllTypesOfSupporButton;
    }
}
