﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.Basics" %>
<%@ Register TagPrefix="sb" TagName="LookupItemCheckboxList" Src="~/Common/LookupItemCheckboxList.ascx" %>
<%@ Register Tagprefix="sb" tagname="LookupDialog" src="~/Common/Lookup/LookupDialog.ascx"   %>    
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<script type="text/javascript" language="javascript">
	 
	function CheckWashingtonState(selfId, cityDropDownId, countyDropDownId, cityTextBoxId, countyTextBoxId ) {
	    
	    if (selfId == undefined) {
	        //alert('i am undefined');
	        return false;
	    }
	    
	    var atext = $("#" + selfId).find(':selected').text();
	    if (atext == "Washington") {
	        hideElementById(cityTextBoxId);
	        hideElementById(countyTextBoxId);
	        showElementById(cityDropDownId);
	        showElementById(countyDropDownId);
	    } else {
	        showElementById(cityTextBoxId);
	        showElementById(countyTextBoxId);
	        hideElementById(cityDropDownId);
	        hideElementById(countyDropDownId);
	    }
	} 
	</script>
<h3>My Profile – Basics</h3>
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">My Name:</p>
        <label for="FirstNameBox">First:</label>
        <asp:TextBox ID="FirstNameBox" runat="server" Columns="20" MaxLength="40" />
        <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstNameBox" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
        <br />
        <label for="MidNameBox">Middle:</label>
        <asp:TextBox ID="MidNameBox" runat="server" Columns="20" MaxLength="40"/>
        <elv:PropertyProxyValidator ID="MidNameValidator" runat="server" ControlToValidate="MidNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
        <br />
        <label for="LastNameBox">Last:</label>
        <asp:TextBox ID="LastNameBox" runat="server" Columns="20" MaxLength="40"/>
        <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
        <br />
        <hr />
        <p class="form-section-title">My Permanent Address:</p>
        <label for="AddressLine1Box">Street Line 1:</label>
        <asp:TextBox ID="AddressLine1Box" runat="server" Columns="30" MaxLength="50"/>
        <asp:requiredfieldvalidator ID="AddressLine1Required" Display="Dynamic" runat="server" ControlToValidate="AddressLine1Box" ErrorMessage="Address line 1 is required"></asp:requiredfieldvalidator>
        <elv:PropertyProxyValidator ID="AddressLine1Validator" runat="server" ControlToValidate="AddressLine1Box" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        <br />
        <label for="AddressLine2Box">Street Line 2:</label>
        <asp:TextBox ID="AddressLine2Box" runat="server" Columns="30" MaxLength="50"/>
        <elv:PropertyProxyValidator ID="AddressLine2Validator" runat="server" ControlToValidate="AddressLine2Box" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        <br />
       <label for="StateDropDown">State:</label>
        <asp:DropDownList ID="StateDropDown" runat="server"   ></asp:DropDownList>
        <asp:requiredfieldvalidator ControlToValidate="StateDropDown" Display="Dynamic" runat="server" ID="stateRequired" ErrorMessage="Address State is required"></asp:requiredfieldvalidator>
        <br />
        <label for="CityDropDown">City:</label>
        <asp:DropDownList ID="CityDropDown" runat="server"></asp:DropDownList> 
        <asp:panel ID="cityTextPanel" runat="server">
            <asp:TextBox ID="CityTextBox" runat="server" Columns="30" MaxLength="50"/>
            <asp:requiredfieldvalidator ControlToValidate="CityTextBox" 
                                        Display="Dynamic" runat="server" ID="cityRequired" 
                                        ErrorMessage="Address City is required">
            </asp:requiredfieldvalidator>
        </asp:panel>
        <br />
        <label for="CountyDropDown">County:</label>
        <asp:DropDownList ID="CountyDropDown" runat="server"></asp:DropDownList> 
        <asp:panel ID="countyTextPanel" runat="server">
            <asp:TextBox ID="CountyTextBox" runat="server" Columns="30" MaxLength="50"/>
            <asp:requiredfieldvalidator ControlToValidate="CountyTextBox" Display="Dynamic" runat="server" ID="countyRequired" ErrorMessage="Address County is required"></asp:requiredfieldvalidator>
        </asp:panel>        
        <br />
        <label for="ZipBox">Postal Code:</label>
        <asp:TextBox ID="ZipBox" runat="server" Columns="10" MaxLength="10"/>
        <asp:requiredfieldvalidator ControlToValidate="ZipBox" Display="Dynamic" runat="server" ID="postalCodeRequired" ErrorMessage="Address Postal Code is required"></asp:requiredfieldvalidator>
        <elv:PropertyProxyValidator ID="ZipValidator" runat="server" ControlToValidate="ZipBox" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        <br />
        <label for="PhoneBox">Phone:</label>
        <asp:TextBox ID="PhoneBox" runat="server" Columns="12" CssClass="phone"/>
        <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="PhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
            <br />
        <label for="MobilePhoneBox">Mobile Phone:</label>
        <asp:TextBox ID="MobilePhoneBox" runat="server" Columns="12" CssClass="phone"/>
        <elv:PropertyProxyValidator ID="MobilePhoneValidator" runat="server" ControlToValidate="MobilePhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
        <br />
    </div>
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">  
        <p class="form-section-title">My Email</p>
        <label>Email</label>
        <asp:Label ID="EmailControl" runat="server" />
       
        <br />
        <hr />
        
        <p class="form-section-title">My Background</p>       
        <label for="DobControl">Birthdate</label>
        <asp:TextBox ID="DobControl" runat="server" CssClass="date"/>
        <asp:RangeValidator ID="dobRangeValid" runat="server" ControlToValidate="DobControl" MinimumValue="1/1/1900" Type="Date" Display="Dynamic" ErrorMessage="How old are you?" />
        <br />

        <label for="GenderButtonList">My Gender:</label>
        <asp:RadioButtonList ID="GenderButtonList" runat="server">
            <asp:ListItem Text="Male" Value="1"></asp:ListItem>
            <asp:ListItem Text="Female" Value="3"></asp:ListItem>
        </asp:RadioButtonList>
        <br />

        <label for="ReligionCheckboxList">My Religion/Faith:</label>
        <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" CheckBoxListWidth="210px" />
        <label for="OtherReligion">Other Religion?:</label>
        <asp:TextBox ID="OtherReligion" runat="server" />
        <elv:PropertyProxyValidator ID="OtherReligionValidator" runat="server" ControlToValidate="OtherReligion" PropertyName="ReligionOther" SourceTypeName="ScholarBridge.Domain.Application"/>
        <br />

        <label for="EthnicityControl">My Heritage:</label>
        <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" CheckBoxListWidth="210px" />
        <label for="OtherEthnicity">Other Ethnicity?</label>
        <asp:TextBox ID="OtherEthnicity" runat="server" />
        <elv:PropertyProxyValidator ID="OtherEthnicityValidator" runat="server" ControlToValidate="OtherEthnicity" PropertyName="EthnicityOther" SourceTypeName="ScholarBridge.Domain.Application"/>
   </div>
</div>
