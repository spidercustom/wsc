﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicInformation.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.AcademicInformation" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemRadioButtonList.ascx" tagname="LookupItemRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerStudentType.ascx" tagname="SeekerStudentType" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerCollegeType.ascx" tagname="SeekerCollegeType" tagprefix="sb" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
 <script type="text/javascript">
	
	function UpdateSATComulative(satWritingId, satReadingId, satMathId,satHiddenComulativeId, satComulativeId) {

	    
	    if (satComulativeId == undefined) {
	        //alert('i am undefined');
	        return false;
	    }
	    var sum = 0;
	        //add only if the value is number

	        if (!isNaN($("#" + satWritingId).val()) && $("#" + satWritingId).val().length != 0) {
	        sum += parseFloat($("#" + satWritingId).val());
	        }

	        if (!isNaN($("#" + satReadingId).val()) && $("#" + satReadingId).val().length != 0) {
	            sum += parseFloat($("#" + satReadingId).val());
	        }

	        if (!isNaN($("#" + satMathId).val()) && $("#" + satMathId).val().length != 0) {
	            sum += parseFloat($("#" + satMathId).val());
	        }
	        if (sum > 0) {

	            disableElementById(satComulativeId); $("#" + satComulativeId).val(sum); $("#" + satHiddenComulativeId).val(sum);
	        }
	        else
	        { enableElementById(satComulativeId); $("#" + satHiddenComulativeId).val(''); }
	    }


	    function UpdateACTComulative(actEnglishId, actReadingId, actMathId, actScienceId,actHiddenComulativeId, actComulativeId) {


	        if (actComulativeId == undefined) {
	            //alert('i am undefined');
	            return false;
	        }
	        var sum = 0;
	        //add only if the value is number

	        if (!isNaN($("#" + actEnglishId).val()) && $("#" + actEnglishId).val().length != 0) {
	            sum += parseFloat($("#" + actEnglishId).val());
	        }

	        if (!isNaN($("#" + actReadingId).val()) && $("#" + actReadingId).val().length != 0) {
	            sum += parseFloat($("#" + actReadingId).val());
	        }

	        if (!isNaN($("#" + actMathId).val()) && $("#" + actMathId).val().length != 0) {
	            sum += parseFloat($("#" + actMathId).val());
	        }

	        if (!isNaN($("#" + actScienceId).val()) && $("#" + actScienceId).val().length != 0) {
	            sum += parseFloat($("#" + actScienceId).val());
	        }
	        //alert(sum);
	        if (sum > 0) {

	            $("#" + actComulativeId).val(sum); $("#" + actHiddenComulativeId).val(sum); disableElementById(actComulativeId);
	        }
	        else
	        { enableElementById(actComulativeId); $("#" + actHiddenComulativeId).val(''); }
	    }
	</script>
 <style type="text/css">
        .large-label-size
        {
            width: 325px !important;
        }
        .large-label-size-checkbox
        {
            width: 280px !important;
        }
        
    </style>
    
<div class="form-iceland-container">
    <div class="form-iceland">
     <div class="form-iceland two-columns ">
     
     <p class="form-section-title">My Academic Information - High School</p>
    
        <label for="CurrentStudentGroupControl" class="large-label-size">What type of student are you currently?<sb:CoolTipInfo ID="CoolTipInfo2" Content="Required. Be sure to update your student information to find all the scholarships that you can apply for." runat="server" />
        </label>
        <sb:SeekerStudentType id="CurrentStudentGroupControl" runat="server" />
        <br /><br />

        <label id="HighSchoolLabelControl" class="large-label-size" for="HighSchoolControl">High School you are currently attending or graduated from:</label>
        <br />
        <sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl" 
            OtherControl="HighSchoolOtherControl" OtherControlLabel="HighSchoolOtherLabelControl" ItemSource="HighSchoolDAL" Title="High School Selection" SelectionLimit="1" />
        <asp:TextBox ID="HighSchoolControl" ReadOnly="true" runat="server"></asp:TextBox>
        <br />
        <label id="HighSchoolOtherLabelControl" for="HighSchoolOtherControl">Other:</label>
        <asp:TextBox CssClass="othercontroltextarea" ID="HighSchoolOtherControl" runat="server" />
        <br />
        
        <asp:PlaceHolder ID="SchoolDistrictContainerControl" runat="server">
            <label id="SchoolDistrictLabelControl" for="SchoolDistrictControl">School District:</label>
            <br />
            <sb:LookupDialog ID="SchoolDistrictControlLookupDialogButton" runat="server" BuddyControl="SchoolDistrictControl" ItemSource="SchoolDistrictDAL" Title="School District Selection" SelectionLimit="1" 
                OtherControl="SchoolDistrictOtherControl" OtherControlLabel="SchoolDistrictOtherLabelControl"/>
            <asp:TextBox ID="SchoolDistrictControl" ReadOnly="true" runat="server"></asp:TextBox>
            
            <br />
            <label id="SchoolDistrictOtherLabelControl" for="SchoolDistrictOtherControl">Other:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="SchoolDistrictOtherControl" runat="server" />
            <br />
        </asp:PlaceHolder>
     
     </div>
     
     <div class="vertical-devider"></div>
     
     <div class="form-iceland two-columns">
     <p class="form-section-title">Academic Performance</p>
        <label for="GPAControl">GPA:</label>
        <sandTrap:NumberBox ID="GPAControl"  runat="server"  Precision="3"  MinAmount="0" MaxAmount="5"/>
        <elv:PropertyProxyValidator ID="GPAValidator" runat="server" ControlToValidate="GPAControl" PropertyName="GPA" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
        <br />
        <label for="ClassRankControl">Class Rank:</label>
        <sb:LookupItemRadioButtonList id="ClassRankControl" runat="server" RadioButtonListWidth="250px" MultiColumn="true" LookupServiceSpringContainerKey="ClassRankDAL" ></sb:LookupItemRadioButtonList>
        <asp:CustomValidator ID="ClassRankControlValidator" runat="server"  ErrorMessage="Please select class rank."    />
         <br />
         <br />
         <label >SAT Score:<sb:CoolTipInfo ID="CoolTipInfo3" Content="Optional. Note: only individual SAT score for each category are used in scholarship matching. If you only know your cumulative score, please enter it but you are encouraged to find and enter the scores for each category." runat="server" />
         </label>
          <br />
        <label for="SATWritingControl">&nbsp;&nbsp;&nbsp;&nbsp;Writing:</label>
        <sandTrap:NumberBox ID="SATWritingControl"  runat="server" Precision="0" MinAmount="200" MaxAmount="800" />
        <elv:PropertyProxyValidator ID="SATWritingControlValidator" runat="server" ControlToValidate="SATWritingControl" PropertyName="Writing" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
        <asp:CustomValidator ID="SATWritingCustomValidator" runat="server" ControlToValidate="SATWritingControl" ErrorMessage="Value entered must be 200-800"   OnServerValidate="SATWritingCustomValidator_OnServerValidate" />
        <br />
        <label for="SATReadingControl">&nbsp;&nbsp;&nbsp;&nbsp;Critical Reading:</label>
        <sandTrap:NumberBox ID="SATReadingControl" runat="server" Precision="0"   MinAmount="200" MaxAmount="800"/>
        <elv:PropertyProxyValidator ID="SATReadingValidator" runat="server" ControlToValidate="SATReadingControl" PropertyName="CriticalReading" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
         <asp:CustomValidator ID="SATReadingCustomValidator" runat="server" ControlToValidate="SATReadingControl" ErrorMessage="Value entered must be 200-800"   OnServerValidate="SATReadingCustomValidator_OnServerValidate" />
   
        <br />
        <label for="SATMathControl">&nbsp;&nbsp;&nbsp;&nbsp;Mathematics:</label>
        <sandTrap:NumberBox ID="SATMathControl" runat="server" Precision="0"   MinAmount="200" MaxAmount="800" />
        <elv:PropertyProxyValidator ID="SATMathValidator" runat="server" ControlToValidate="SATMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
        <asp:CustomValidator ID="SATMathCustomValidator" runat="server" ControlToValidate="SATMathControl" ErrorMessage="Value entered must be 200-800"   OnServerValidate="SATMathCustomValidator_OnServerValidate" />
   
        <br />
        <label for="SATScoreCommulativeControl">&nbsp;&nbsp;&nbsp;&nbsp;Cumulative:</label>
        <sandTrap:NumberBox ID="SATScoreCommulativeControl"  runat="server" Precision="0"  MinAmount="600" MaxAmount="2400"/>
        <sb:CoolTipInfo ID="CoolTipInfo4" Content="Enter cumulative amount, or enter scores for each category and the cumulative total will be calculated." runat="server" />
        <elv:PropertyProxyValidator ID="PropertyProxyValidator1" runat="server" ControlToValidate="SATScoreCommulativeControl" PropertyName="Commulative" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
        <asp:CustomValidator ID="SATCommulativeCustomValidator" runat="server" ControlToValidate="SATScoreCommulativeControl" ErrorMessage="Value entered must be 600-2400"   OnServerValidate="SATCommulativeCustomValidator_OnServerValidate" />
        <asp:HiddenField ID="hiddenSATCommulative" runat="server" />
        <br />
        
        <label >ACT Score:<sb:CoolTipInfo ID="CoolTipInfo5" Content="Optional. Note: only individual ACT score for each category are used in scholarship matching. If you only know your cumulative score, please enter it but you are encouraged to find and enter the scores for each category." runat="server" /></label>
         <br />
        <label for="ACTEnglishControl">&nbsp;&nbsp;&nbsp;&nbsp;English:</label>
        <sandTrap:NumberBox ID="ACTEnglishControl" runat="server" Precision="0" MinAmount="0" MaxAmount="36" />
        <elv:PropertyProxyValidator ID="ACTEnglishValidator" runat="server" ControlToValidate="ACTEnglishControl" PropertyName="English" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <asp:CustomValidator ID="ACTEnglishCustomValidator" runat="server" ControlToValidate="ACTEnglishControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTEnglishCustomValidator_OnServerValidate" />
  
        <br />

        <label for="ACTReadingControl">&nbsp;&nbsp;&nbsp;&nbsp;Reading:</label>
        <sandTrap:NumberBox ID="ACTReadingControl" runat="server" Precision="0"   MinAmount="0" MaxAmount="36"/>
        <elv:PropertyProxyValidator ID="ACTReadingValidator" runat="server" ControlToValidate="ACTReadingControl" PropertyName="Reading" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <asp:CustomValidator ID="ACTReadingCustomValidator" runat="server" ControlToValidate="ACTReadingControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTReadingCustomValidator_OnServerValidate" />
  
        <br />

        <label for="ACTMathControl">&nbsp;&nbsp;&nbsp;&nbsp;Mathematics:</label>
        <sandTrap:NumberBox ID="ACTMathControl" runat="server" Precision="0"  MinAmount="0" MaxAmount="36"/>
        <elv:PropertyProxyValidator ID="ACTMathValidator" runat="server" ControlToValidate="ACTMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
         <asp:CustomValidator ID="ACTMathCustomValidator" runat="server" ControlToValidate="ACTMathControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTMathCustomValidator_OnServerValidate" />
  
        <br />

        <label for="ACTScienceControl">&nbsp;&nbsp;&nbsp;&nbsp;Science:</label>
        <sandTrap:NumberBox ID="ACTScienceControl" runat="server" Precision="0"  MinAmount="0" MaxAmount="36"/>
        <elv:PropertyProxyValidator ID="ACTScienceValidator" runat="server" ControlToValidate="ACTScienceControl" PropertyName="Science" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
         <asp:CustomValidator ID="ACTScienceCustomValidator" runat="server" ControlToValidate="ACTScienceControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTScienceCustomValidator_OnServerValidate" />
  
        <br />
        <label for="ACTScoreCommulativeControl">&nbsp;&nbsp;&nbsp;&nbsp;Cumulative:</label>
        <sandTrap:NumberBox ID="ACTScoreCommulativeControl" runat="server" Precision="0"   MinAmount="0" MaxAmount="144"/>
        <sb:CoolTipInfo ID="CoolTipInfo6" Content="Enter cumulative amount, or enter scores for each category and the cumulative total will be calculated." runat="server" />
        <elv:PropertyProxyValidator ID="ACTScoreCommulativeValidator" runat="server" ControlToValidate="ACTScoreCommulativeControl" PropertyName="Commulative" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <asp:CustomValidator ID="ACTCommulativeCustomValidator" runat="server" ControlToValidate="ACTScoreCommulativeControl" ErrorMessage="Value entered must be 0-144."   OnServerValidate="ACTCommulativeCustomValidator_OnServerValidate" />
        <asp:HiddenField ID="hiddenACTCommulative" runat="server" />
        <br />
        

        <label for="HonorsControl">Honors:</label>
        <asp:CheckBox ID="HonorsCheckboxControl" Text="" runat="server" />
        <asp:TextBox TextMode="MultiLine" ID="HonorsTextControl" MaxLength="50"  Width="320px" runat="server"></asp:TextBox>
       <sb:CoolTipInfo ID="CoolTipInfo1" Content="Optional. Have you received honors awards, participated in honors classes or programs? Please provide brief details." runat="server" />
        <br />

        <label for="APCreditsControl">AP Credits:</label>
        <asp:CheckBox ID="APCreditsCheckBoxControl" Text="" runat="server" />
        <asp:TextBox TextMode="MultiLine" ID="APCreditsTextControl" MaxLength="50"  Width="320px" runat="server"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfo7" Content="Optional. Have you taken advance placement courses or earned advanced placement credits? Please provide brief details." runat="server" />
        <br />
        <label for="IBCreditsControl">IB Credits:</label>
         <asp:CheckBox ID="IBCreditsCheckBoxControl" Text="" runat="server"  />
        <asp:TextBox TextMode="MultiLine" ID="IBCreditsTextBoxControl" MaxLength="50"  Width="320px" runat="server"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfo8" Content="Optional. Have you participated in an International Baccalaureate program? Please provide brief details." runat="server" />
        <br />
     </div>
 
    </div>
    
</div>
 <hr />
 
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">My Academic Information - College</p>
         <label for="SchoolTypeControl" class="large-label-size">What types of schools are you considering?<sb:CoolTipInfo ID="CoolTipInfo10" Content="Optional. Please indicate any colleges that you are considering attending to ensure you are matched with scholarships that are for use at those schools." runat="server" />
        </label>
        <sb:FlagEnumCheckBoxList id="SchoolTypeControl"    runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
        <br />

               
        <label for="AcademicProgramControl">Academic program:</label><br />
        <sb:EnumRadioButtonList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
        <br />

        <label for="SeekerStatusControl">Enrollment status:</label><br />
        <sb:EnumRadioButtonList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
        <br />

          
        <label for="FirstGenerationControl" class="large-label-size-checkbox">First Generation in Your Family to Attend College:</label>
        <asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
        <br />
    </div>
    <div class="vertical-devider"></div>
        <div class="form-iceland two-columns">
        <br /><br /><br />
        <label for="SeekerStudentCollegeTypeControl" class="large-label-size">What type of college student are you?</label>
        <sb:SeekerCollegeType id="SeekerStudentCollegeTypeControl" runat="server" />
        <sb:CoolTipInfo ID="CoolTipInfo9" Content="Required. Be sure to update your student information to find all the scholarships that you can apply for." runat="server" />
        <br /><br />
        
        <label for="CollegeControl" class="large-label-size" >College you are currently attending:</label>
        <br/>
        <sb:LookupDialog ID="CollegeControlDialogButton" runat="server" BuddyControl="CollegeControl" OtherControl="CollegeOtherControl" OtherControlLabel="CollegeOtherLabelControl" ItemSource="CollegeDAL" Title="College Selection" SelectionLimit="1" />
        <asp:TextBox ID="CollegeControl" ReadOnly="true" Width="200px" runat="server"></asp:TextBox>
        <br />
        <label id="CollegeOtherLabelControl" for="CollegeOtherControl">Other:</label>
        <asp:TextBox CssClass="othercontroltextarea" ID="CollegeOtherControl" runat="server" />
        <br />

            <label for="CollegesAppliedLabelControl" class="large-label-size" >Colleges you are considering:</label>
            <asp:CheckBox ID="InWashingtonCheckbox" Text="In Washington" runat="server" />
            <asp:CheckBox ID="OutOfStateCheckbox" Text="Out of State" runat="server" />
            <br />
            <sb:LookupDialog ID="CollegesAppliedControlDialogButton" runat="server" BuddyControl="CollegesAppliedLabelControl" OtherControl="CollegesAppliedOtherControl" ItemSource="CollegeDAL" Title="College Selection"/>
            <asp:TextBox ID="CollegesAppliedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <asp:TextBox CssClass="othercontroltextarea" ID="CollegesAppliedOtherControl" runat="server" />
            <br />

            <%--<label for="CollegesAcceptedLabelControl" class="large-label-size">List of Colleges I’m accepted at:</label>
            <sb:LookupDialog ID="CollegesAcceptedControlDialogButton" runat="server" BuddyControl="CollegesAcceptedLabelControl" OtherControl="CollegesAcceptedOtherControl" ItemSource="CollegeDAL" Title="College Selection"/>
            <asp:TextBox ID="CollegesAcceptedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <asp:TextBox CssClass="othercontroltextarea" ID="CollegesAcceptedOtherControl" runat="server" />
            <br />--%>
        </div>
</div>