﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Web.Admin
{
    public partial class ShowScholarshipNotes : System.Web.UI.UserControl
    {
        public Organization Organization { get; set; }
        public Scholarship Scholarship { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
		public IUserContext UserContext { get; set; }

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (Scholarship.AdminNotes != null)
                notesLiteral.Text = Scholarship.AdminNotes.ToStringTableFormat();
		}
		
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (null != Organization)
                {
                    scholarshipLabel.Text = Scholarship.Name;
                    orgNameLabel.Text = Organization.Name;
					adminNameLabel.Text = Organization.AdminUser.Name.ToString();
					
					emailLiteral.Text = "<a href='mailto:" + Organization.AdminUser.Email + "'>" + Organization.AdminUser.Email + "</a>";
                    phoneLabel.Text = Organization.Phone == null ? null : Organization.Phone.ToString();


                    websiteLabel.Text = Scholarship.OnlineApplicationUrl;

                    websiteLabel.NavigateUrl = Scholarship.OnlineApplicationUrl;
                    
                }
            }
        }
		protected void addNotes_Click(object sender, EventArgs e)
		{
            Scholarship.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            Scholarship.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            ScholarshipService.Save(Scholarship);
			 
			newNotesTb.Text = null;
		}


    }
}