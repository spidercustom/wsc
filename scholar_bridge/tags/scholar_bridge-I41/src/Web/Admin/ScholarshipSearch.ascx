﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipSearch.ascx.cs" Inherits="ScholarBridge.Web.Admin.ScholarshipSearch" %>
<%@ Import Namespace="ScholarBridge.Domain"%>
<asp:label ID="lblMessage" runat="server" ForeColor="Green"></asp:label>
<asp:panel runat="server" ID="pnlSearch">
    <table style="border: solid 1px black;">
        <tr>
            <td>
                Status:</td>
            <td>
                <asp:DropDownList ID="ddlScholarshipStatus" runat="server">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="Activated" Value="Activated"></asp:ListItem>
                    <asp:ListItem Text="Not Activated" Value="NotActivated"></asp:ListItem>
                    <asp:ListItem Text="Closed" Value="Closed"></asp:ListItem>
                    <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Provider:</td>
            <td>
                <asp:DropDownList ID="ddlProvider" runat="server" AppendDataBoundItems="true" 
                    DataSourceID="providerDropDownSource" DataTextField="Name" DataValueField="Id">
                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Name (contains):
            </td>
            <td>
                <asp:TextBox ID="txtNameSearch" runat="server"></asp:TextBox>
            </td>
            <td>
                Intermediary:</td>
            <td>
                <asp:DropDownList ID="ddlIntermediary" runat="server" 
                    AppendDataBoundItems="true" DataSourceID="intermediaryDropDownSource" 
                    DataTextField="Name" DataValueField="Id">
                    <asp:ListItem Text="" Value="-1"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Button ID="btnSearchScholarships" runat="server" 
                    OnClick="btnSearchScholarships_Click" Text="Search Scholarships" 
                    CausesValidation="False" />
                    
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="gvScholarshipList" runat="server" AutoGenerateColumns="False" 
        onrowcommand="gvScholarshipList_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="Scholarship Name" SortExpression="Name">
                <ItemTemplate>
                    <asp:LinkButton 
                        ID="showScholarshipButton" runat="server" 
                        CommandArgument='<%# Eval("Id") %>' 
                        CommandName="ShowScholarship">
                        <asp:Label ID="lblScholarshipName" runat="server" Text='<%# Eval("Name") %>'>
                        </asp:Label>
                    </asp:LinkButton>                
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Provider" SortExpression="Provider">
                <ItemTemplate>
                    <asp:Label ID="label1" runat="server" Text='<%# Eval("Provider.Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AcademicYear" HeaderText="Year" SortExpression="AcademicYear" />
            <asp:BoundField DataField="Stage" HeaderText="Stage" SortExpression="Stage" />
            <asp:BoundField DataField="ApplicationDueDate" HeaderText="App Due Date" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}" SortExpression="ApplicationDueDate" />
            <asp:BoundField DataField="AwardDate" HeaderText="Award Date" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:d}" SortExpression="AwardDate"/>
            <asp:TemplateField HeaderText="# of Awards {min/max}" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# string.Format("{0} / {1}", Eval("FundingParameters.MinimumNumberOfAwards"), Eval("FundingParameters.MaximumNumberOfAwards") ) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Award Amount"  ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("AmountRange") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="lbDeactivate" runat="server" 
                        CausesValidation="false" 
                        Visible='<%# (ScholarshipStages)Eval("Stage") == ScholarshipStages.Rejected || (ScholarshipStages)Eval("Stage") == ScholarshipStages.Activated %>'
                        CommandName='<%# (ScholarshipStages)Eval("Stage") == ScholarshipStages.Rejected 
                                            ? "reactivate" 
                                            : ((ScholarshipStages)Eval("Stage") == ScholarshipStages.Activated 
                                                    ? "deactivate" 
                                                    : "")  %>'
                        CommandArgument='<%# Eval("Id") %>' 
                        Text='<%# (ScholarshipStages)Eval("Stage") == ScholarshipStages.Rejected 
                                    ? "Reactivate" 
                                    : ((ScholarshipStages)Eval("Stage") == ScholarshipStages.Activated 
                                                ? "Deeactivate" 
                                                : "")  %>'>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:panel> 

<asp:panel ID="pnlDeactivateReactivate" runat="server" BorderColor="LightGray" BorderWidth="8px" HorizontalAlign="Center">

    <table>
        <tr>
            <td><b><asp:Label ID="lblDeactivateReactivate" runat="server"></asp:Label></b> scholarship <b> 
            <asp:Label ID="lblScholarshipName" runat="server"></asp:Label></b>
            </td>
        </tr>
        <tr>
            <td>
                *Comment - <br />
                <asp:textbox ID="txtAdminComment" runat="server" Rows="4" Columns="80" TextMode="MultiLine">
                </asp:textbox>
                <asp:requiredfieldvalidator ID="rfvCommentRequired" runat="server" 
                        ValidationGroup="vgActivation" 
                        EnableClientScript="true"
                        ControlToValidate="txtAdminComment"
                        ErrorMessage="Comment is required" Text="Comment is required" 
                        Display="Dynamic">
                </asp:requiredfieldvalidator>
            </td>
        </tr>
        <tr>
            <td style="text-align:center">
                <asp:button ID="btnDeactivateReactivate" runat="server" CausesValidation="true" ValidationGroup="vgActivation" OnClick="btnDeactivateReactivate_Click" Text="Update" />&nbsp;&nbsp;&nbsp;
                <asp:button ID="btnCancelDeactivateReactivate" runat="server" CausesValidation="false" OnClick="btnCancelDeactivateReactivate_Click" Text="Cancel" />&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>

</asp:panel>




<asp:ObjectDataSource ID="providerDropDownSource" 
    runat="server" 
    SelectMethod="SelectProvidersForDropDown"
    TypeName="ScholarBridge.Web.Admin.ScholarshipSearch"
    OnObjectCreating="providerDropDownSource_ObjectCreating">
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="intermediaryDropDownSource" 
    runat="server" 
    SelectMethod="SelectIntermediariesForDropDown"
    TypeName="ScholarBridge.Web.Admin.ScholarshipSearch"
    OnObjectCreating="intermediaryDropDownSource_ObjectCreating">
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="scholarshipDataSource" 
    runat="server" 
    SelectMethod="SelectScholarships"
    TypeName="ScholarBridge.Web.Admin.ScholarshipSearch"
    OnObjectCreating="scholarshipDataSource_ObjectCreating">
</asp:ObjectDataSource>




