﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Admin.Editors.Lookups
{
	public partial class LookupTableList : System.Web.UI.UserControl
	{
		public UserContext UserContext { get; set; }
		public LookupTableService LookupTableService { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{

			}
		}

		#region Object Data Source methods

		public List<Type> GetLookupTableList()
		{
			return LookupTableService.GetLookupTableList();
		}

		public IEnumerable GetLookupTableData(string qualifiedClassName, string sortColumn)
		{
			if (string.IsNullOrEmpty(qualifiedClassName))
				return null;
			var rowsUnsortedList = LookupTableService.GetLookupTableData(qualifiedClassName).Cast<ILookup>();
			string[] sortWords = sortColumn.Split(' ');

			bool ascending = true;
			string sortColumnName = string.Empty;

			if (sortWords.Length > 0)
			{
				sortColumnName = sortWords[0];
				if (sortWords.Length > 1)
					ascending = false;
			}

			if (sortColumnName == string.Empty)
				sortColumnName = "Name";

			switch (sortColumnName)
			{
				case "Description":
					return rowsUnsortedList.OrderBy(r => r.Description, ascending).ToList();
				case "Deprecated":
					return rowsUnsortedList.OrderBy(r => r.Deprecated.ToString() + r.Name, ascending).ToList();
				case "On":
					return rowsUnsortedList.OrderBy(r => r.LastUpdate.On + r.Name, ascending).ToList();
				default: // "Name":
					return rowsUnsortedList.OrderBy(r => r.Name, ascending).ToList();
			}
		}

		public void UpdateLookupTableData(string Name, string Description, bool Deprecated, int Id)
		{
			LookupTableService.UpdateLookupTableData(tableDropDown.SelectedValue, UserContext.CurrentUser, Name, Description, Deprecated, Id);
		}

		public void DeleteLookupTableData(int Id)
		{
			LookupTableService.DeleteLookupTableData(tableDropDown.SelectedValue, Id);
		}

		#endregion

		#region Control Event Handlers

		protected void dataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = this;
		}

		protected void AddLookupTableData(object sender, EventArgs e)
		{
			Page.Validate();
			if (!Page.IsValid)
				return;

			TextBox nameAddBox = (TextBox)gvLookupTables.FooterRow.FindControl("NameAddBox");
			TextBox descriptionAddBox = (TextBox)gvLookupTables.FooterRow.FindControl("DescriptionAddBox");

			LookupTableService.InsertLookupTableData(tableDropDown.SelectedValue, UserContext.CurrentUser, nameAddBox.Text, descriptionAddBox.Text);
			nameAddBox.Text = string.Empty;
			descriptionAddBox.Text = string.Empty;
			gvLookupTables.DataBind();
		}

		#endregion
	}
}