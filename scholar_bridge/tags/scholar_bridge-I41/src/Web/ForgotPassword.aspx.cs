﻿using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web
{
    public partial class ForgotPassword : Page
    {
        public IMailerService MailerService { get; set; }

        protected void passwordRecovery_SendingMail(object sender, MailMessageEventArgs e)
        {
            //create message from template
            var templateParams = ConfigHelper.GetMailParams(e.Message.To[0].Address);
            templateParams.MergeVariables.Add("Password", e.Message.Body);

            MailerService.SendMail(MessageType.ForgotPassword, templateParams);
            // We handled sending it ourselves, so cancel
            e.Cancel = true;
        }
    }
}
