﻿<%@ Page Language="C#"  AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.DefaultPage" Title="Welcome" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<HTML xmlns="http://www.w3.org/1999/xhtml"></HTML>

<Head>
<TITLE>theWashBoard.org</TITLE>
    <META name="FORMAT" content="text/html">
    <META name="CHARSET" content="ISO-8859-1">
    <META name="DOCUMENTLANGUAGECODE" content="en">
    <META name="DOCUMENTCOUNTRYCODE" content="us">
    <META name="DC.LANGUAGE" scheme="rfc1766" content="en-us">
    <META name="COPYRIGHT" content="Copyright (c) 2009 by Washington Scholarship Coalition">
    <META name="SECURITY" content="Public">
    <META name="ROBOTS" content="index,follow">
    <META name="GOOGLEBOT" content="index,follow">
    <META name="Description" content="theWashBoard.org Provider Home ">
    <META name="Keywords" content="">
    <META name="Author" content="theWashBoard.org">
    <!-- Base keywords here-->
    <SCRIPT type="text/javascript" src="https://www.google.com/jsapi"></SCRIPT>
    <SCRIPT type="text/javascript">
        google.load("jquery", "1.3");
    </SCRIPT>
   <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.7.1.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tablesorter.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.pager.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tinysort.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskedinput.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.cookie.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.watermark-2.0.min.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/superfish.js") %>"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/js/site.js") %>"></script>
        <%--<link rel="stylesheet" href="<%= ResolveUrl("~/styles/main.css") %>" type="text/css" />--%>
        <LINK href="<%= ResolveUrl("~/styles/WSCStyles.CSS") %>" rel="stylesheet" type="text/css" media="All"/>
        <LINK href="<%= ResolveUrl("~/styles/global.css") %>" rel="stylesheet" type="text/css" media="All"/>        
</HEAD>
<BODY>
<form id="default" runat="server">
<!--Page wrapper starts here-->

</form> 
</BODY>
</HTML>

    