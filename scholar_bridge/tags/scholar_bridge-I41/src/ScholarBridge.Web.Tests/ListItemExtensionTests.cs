using System.Collections.Generic;
using System.Web.UI.WebControls;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Domain.Location;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Tests
{

    [TestFixture]
    public class ListItemExtensionTests
    {

        [Test]
        public void can_select_items()
        {
            var lic = new ListItemCollection {new ListItem("Wisconsin", "WI"), new ListItem("Texas", "TX")};
            var items = new List<State> {new State {Abbreviation = "WI", Name = "Wisconsin"}};
            lic.SelectItems(items, lookupItem => lookupItem.Abbreviation);

            Assert.That(lic[0].Selected, Is.True);
            Assert.That(lic[1].Selected, Is.False);
        }

        [Test]
        public void can_get_select_items()
        {
            var lic = new ListItemCollection { new ListItem("Wisconsin", "WI"), new ListItem("Texas", "TX") };
            var items = new List<State> { new State { Abbreviation = "WI", Name = "Wisconsin" } };
            lic.SelectItems(items, lookupItem => lookupItem.Abbreviation);

            var selected = lic.SelectedItems(item => item);

            Assert.That(selected, Is.Not.Null);
            Assert.That(selected[0], Is.EqualTo("WI"));
        }
    }
}