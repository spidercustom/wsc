﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(291)]
	public class CorrectMatchCountViewIssue : Migration
    {
        public override void Up()
        {
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBScholarshipCriteriaCounts]");
			Database.ExecuteNonQuery(SB_SCHOLARSHIP_CRITERIA_COUNTS);

		}

		public override void Down()
		{
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBScholarshipCriteriaCounts]");
			Database.ExecuteNonQuery(UpdateBoolMatchFromListView.SB_SCHOLARSHIP_CRITERIA_COUNTS); //287

		}
		#region New Criteria count view

    	public const string SB_SCHOLARSHIP_CRITERIA_COUNTS = @"
SELECT SBScholarshipID, [1] as prefCrit, [2] as minCrit
FROM
(
    SELECT SBScholarshipID, SBUsageTypeIndex 
    FROM SBScholarshipMatchCriteriaAttributeUsageRT
    where SBMatchCriteriaAttributeIndex not in (1, 5, 18, 21, 26) -- these criteria are handled below

    union all

    SELECT SBScholarshipID, SBUsageTypeIndex 
    FROM SBScholarshipFundingProfileAttributeUsageRT
    where SBFundingProfileAttributeIndex <> 2 -- exclude 2 Types of support (always required)

    union all
	-- determine if Academic Program (1) is required (no match if all selected)
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    where SBScholarship.MatchCriteriaAcademicPrograms between 1 and 6 -- 7 = All Academic Programs
		
    union all
	-- determine if college (5) is required
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship where SBScholarship.MatchCriteriaCollegeType > 1 -- 1=Any
		
    union all
	-- determine if School Types (18) is required (no match if all selected)
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    where SBScholarship.MatchCriteriaSchoolTypes between 1 and 30 -- 31 = All school types
		
    union all
	-- determine if Seeker Status (21) is required (no match if all selected)
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    where SBScholarship.MatchCriteriaSeekerStatuses between 1 and 6 -- 7 = All seeker statuses
		
    union all
	-- Student Groups (26) is always requred so we'll just add a minimum requirement to the count
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    
    union all
	-- SupportedSituation (Funding Profile - 2) (Types of Support) is always requred so we'll just add a minimum requirement to the count
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
		
) AS SourceTable
PIVOT
(
    COUNT(SBUsageTypeIndex)
    FOR SBUsageTypeIndex IN ([1], [2])
) AS PivotTable
";
		#endregion

	}
}