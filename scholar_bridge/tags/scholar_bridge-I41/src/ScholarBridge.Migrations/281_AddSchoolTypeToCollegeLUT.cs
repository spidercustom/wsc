﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(281)]
	public class AddSchoolTypeToCollegeLUT : Migration
    {
		private const string TABLE_NAME = "SBCollegeLUT";
		private const string COLUMN_NAME = "SchoolType";

        public override void Up()
        {
			Database.AddColumn(TABLE_NAME, COLUMN_NAME, DbType.Int32);
        	LoadSchoolTypes();
        	SetNullTypesToBlank();
			Column typeColumn = Database.GetColumnByName(TABLE_NAME, COLUMN_NAME);
        	typeColumn.ColumnProperty = ColumnProperty.NotNull;
        	typeColumn.Type = DbType.Int32;
			Database.ChangeColumn(TABLE_NAME, typeColumn);
        }

		private void SetNullTypesToBlank()
		{
			Database.ExecuteNonQuery(
				@"
					update sbcollegelut
					set SchoolType = 0
					where schooltype is null
				");
		}

        public override void Down()
        {
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
        }

		private void LoadSchoolTypes()
		{
			Database.ExecuteNonQuery(loadSql);
		}

    	private string loadSql =
			@"
with tempCollege (collegeName, sector)
as 
(
select 'Alfred University', 1 union all
select 'Adelphi University', 1 union all
select 'A.T. Still University', 1 union all
select 'Antioch University', 1 union all
select 'Apollo College', 2 union all
select 'Argosy University', 2 union all
select 'Armour Bible College and Armour Seminary', 16 union all
select 'Art Institute of Seattle', 2 union all
select 'Ashford University', 2 union all
select 'Bainbridge Graduate Institute', 2 union all
select 'Bakke Graduate University', 2 union all
select 'Bastyr University', 1 union all
select 'Bates Technical College', 8 union all
select 'Baylor University', 1 union all
select 'Bellevue College', 8 union all
select 'Bellevue Community College', 8 union all
select 'Bellingham Technical College', 8 union all
select 'Big Bend Community College', 8 union all
select 'Bishop A.L. Hardy Academy of Theology', 16 union all
select 'Calvary Chapel Bible College', 16 union all
select 'Calvary Lighthouse Bible Institute', 16 union all
select 'Capella University', 2 union all
select 'Cascade Bible College', 16 union all
select 'Cascadia Community College', 8 union all
select 'CCS Institute for Extended Learning', 8 union all
select 'Central Texas College', 8 union all
select 'Central Washington University', 4 union all
select 'Centralia College', 8 union all
select 'Chamberlain College of Nursing', 2 union all
select 'Chapman University', 1 union all
select 'Charter College - Pasco', 2 union all
select 'Christos Institute', 16 union all
select 'City University', 1 union all
select 'Clark College', 8 union all
select 'Clover Park Technical College', 8 union all
select 'College for Global Deployment', 16 union all
select 'Collins College', 2 union all
select 'Columbia Basin College', 8 union all
select 'Columbia College - MO', 1 union all
select 'Columbia Evangelical Seminary', 16 union all
select 'Cornell University', 1 union all
select 'Cornish College of Arts', 1 union all
select 'Covenant Bible Seminary', 16 union all
select 'DeVry University', 2 union all
select 'Digipen Institute of Technology', 1 union all
select 'Eastern Washington University', 4 union all
select 'Edmonds Community College', 8 union all
select 'Embry-Riddle Aeronautical University - WA', 1 union all
select 'Everest College - Bremerton', 2 union all
select 'Everest College - Everett', 2 union all
select 'Everest College - Renton', 2 union all
select 'Everest College - Tacoma', 2 union all
select 'Everest College - Vancouver', 2 union all
select 'Everett Community College', 8 union all
select 'Evergreen Christian University', 16 union all
select 'Faith Evangelical Seminary', 1 union all
select 'First Christian Seminary', 16 union all
select 'Fred Hutchinson Cancer Research Center', 1 union all
select 'Fuller Theological Seminary - Northwest', 16 union all
select 'Goddard College', 1 union all
select 'Golden Gate Baptist Theological Seminary', 1 union all
select 'Golden Gate University - Seattle', 1 union all
select 'Gonzaga University', 1 union all
select 'Grand Canyon University', 2 union all
select 'Grays Harbor College', 8 union all
select 'Green River Community College', 8 union all
select 'Heart 4 the Nations Bible School and Ministry Training Center', 16 union all
select 'Heritage University', 1 union all
select 'Highline Community College', 8 union all
select 'Hope International University', 1 union all
select 'Horizon College of Ministry', 16 union all
select 'HUMUH Transcendental Awareness Institute', 16 union all
select 'Interface College', 2 union all
select 'International Academy of Design and Technology', 2 union all
select 'International College of Metaphysical Theology', 16 union all
select 'International Graduate School of Ministry', 16 union all
select 'International Air and Hospitality Academy', 2 union all
select 'ITT Technical Institute - Everett', 2 union all
select 'ITT Technical Institute - Indianapolis', 2 union all
select 'ITT Technical Institute - Seattle', 2 union all
select 'ITT Technical Institute - Spokane', 2 union all
select 'KAES Bible College & Seminary', 16 union all
select 'Kaplan College', 2 union all
select 'Kepler College', 1 union all
select 'KPCA Northwest Presbyterian Theological Seminary', 16 union all
select 'Lake Washington Technical College', 8 union all
select 'Lesley University', 1 union all
select 'Lewis and Clark College', 1 union all
select 'Life Ministry Institute ', 16 union all
select 'Life Pacific College', 1 union all
select 'Lincoln College of Technology', 2 union all
select 'Living Faith Fellowship College of Ministry', 16 union all
select 'Lower Columbia College', 8 union all
select 'Mars Hill Graduate School', 1 union all
select 'Missio Dei Learning Community', 16 union all
select 'Moody Bible Institute - Spokane', 1 union all
select 'North Park Theological Seminary', 16 union all
select 'North Seattle Community College', 8 union all
select 'Northwest Aviation College', 1 union all
select 'Northwest Baptist Seminary', 16 union all
select 'Northwest College of Art', 1 union all
select 'Northwest Indian College', 8 union all
select 'Northwest Institute of Literary Arts/Whidbey Writers Workshop', 1 union all
select 'Northwest School of Wooden Boatbuilding', 2 union all
select 'Northwest Theological Seminary', 16 union all
select 'Northwest University', 1 union all
select 'Nova Southeastern University', 1 union all
select 'Old Dominion University', 4 union all
select 'Olympic College', 8 union all
select 'Oregon Health and Science', 4 union all
select 'Pacific Lutheran University', 1 union all
select 'Pacific Northwest Bible College', 16 union all
select 'Pacific Northwest University of Health Sciences', 1 union all
select 'Pacific Theological Seminary', 16 union all
select 'Park University - Fairchild', 1 union all
select 'Peninsula College', 8 union all
select 'Pierce College - Fort Steilacoom', 8 union all
select 'Pierce College - Puyallup', 8 union all
select 'Pima Medical Institute', 2 union all
select 'Portland Bible College', 16 union all
select 'Portland State University', 4 union all
select 'Renewed Life Seminary', 16 union all
select 'Renton Technical College', 8 union all
select 'Resurgence Training Center', 16 union all
select 'Saint Martin''s University', 1 union all
select 'Saybrook Graduate School and Research Center', 1 union all
select 'Seattle Bible College', 16 union all
select 'Seattle Central Community College', 8 union all
select 'Seattle Institute of Oriental Medicine', 2 union all
select 'Seattle Pacific University', 1 union all
select 'Seattle Theological Seminary', 2 union all
select 'Seattle University', 1 union all
select 'Seattle Urban Bible College', 16 union all
select 'Shepherds Bible College', 16 union all
select 'Shoreline Community College', 8 union all
select 'Skagit Valley Community College', 8 union all
select 'Sophia Divinity School', 16 union all
select 'Sound Baptist Bible College', 16 union all
select 'South Puget Sound Community College', 8 union all
select 'South Seattle Community College', 8 union all
select 'Southern Illinois University', 4 union all
select 'Spokane Community College', 8 union all
select 'Spokane Falls Community College', 8 union all
select 'St. Anthony''s University', 16 union all
select 'Tacoma Bible College ', 16 union all
select 'Tacoma Community College', 8 union all
select 'The Evergreen State College', 4 union all
select 'The Institute for Biblical Studies', 16 union all
select 'The Institute for Christian Works', 16 union all
select 'The Lorian Center for Incarnational Spirituality', 16 union all
select 'The Washington Bible Institute', 16 union all
select 'Trinity Lutheran College', 1 union all
select 'Trinity Western University', 1 union all
select 'Triune Biblical University', 16 union all
select 'Troy University', 4 union all
select 'United Theological Seminary and Bible College', 16 union all
select 'Universal Technical Institute', 2 union all
select 'University of Christian Studies and Seminary', 16 union all
select 'University of Phoenix', 2 union all
select 'University of Puget Sound', 1 union all
select 'University of Washington', 4 union all
select 'University of Washington - Bothell', 4 union all
select 'University of Washington - Tacoma', 4 union all
select 'Vincennes University', 4 union all
select 'Walden University', 2 union all
select 'Walla Walla Community College', 8 union all
select 'Walla Walla University', 1 union all
select 'Warner Pacific College', 1 union all
select 'Washington College and International Seminary', 16 union all
select 'Washington Seminary ', 16 union all
select 'Washington State University', 4 union all
select 'Washington State University - Tri-Cities', 4 union all
select 'Washington State University - Vancouver', 4 union all
select 'Webster University', 1 union all
select 'Wenatchee Valley College', 8 union all
select 'Western Culinary Institute', 2 union all
select 'Western Oregon University', 4 union all
select 'Western Reformed Seminary', 16 union all
select 'Western Washington University', 4 union all
select 'Whatcom Community College', 8 union all
select 'Whitman College', 1 union all
select 'Whitworth University', 1 union all
select 'Wisdom Bible Institute', 16 union all
select 'Wisdom for Life Leadership School', 16 union all
select 'Woolston-Steen Theological Seminary', 16 union all
select 'Worship Arts Conservatory', 1 union all
select 'WyoTech', 2 union all
select 'Yakima Valley Community College', 4
)
update sbcollegelut
set schooltype = (select sector from tempcollege tc where tc.collegename = college) 
";
    }
}