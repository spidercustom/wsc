using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class ApplicationDAL : AbstractDAL<Application>, IApplicationDAL
    {
        public Application FindById(int id)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Application>();
        }

        public IList<Application> FindAllSubmitted(Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStages.Submitted))
                .List<Application>();
        }

        public IList<Application> FindAllFinalists(Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Finalist", true))
                .Add(Restrictions.Eq("Stage", ApplicationStages.Submitted))
                .List<Application>();
        }


		/// <summary>
		/// Count all 
		/// </summary>
		/// <param name="scholarship"></param>
		/// <returns></returns>
        public int CountAllSubmitted(Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStages.Submitted))
                .SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }

		/// <summary>
		/// Count all Submitted applications site-wide for active scholarships
		/// </summary>
		/// <returns></returns>
		public int CountAllSubmitted()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ApplicationStages.Submitted))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

		/// <summary>
		/// Count all Submitted applications site-wide for active scholarships
		/// </summary>
		/// <returns></returns>
		public int CountAllStartedButNotSubmitted()
		{
			return CreateCriteria()
				.Add(Restrictions.Not(Restrictions.Eq("Stage", ApplicationStages.Submitted)))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}


        public int CountAllSubmittedBySeeker(Seeker seeker)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker ))
                .Add(Restrictions.Eq("Stage", ApplicationStages.Submitted))
                .SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }

        public IList<Application> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .List<Application>();
        }

        public Application Save(Application application)
        {
            return application.Id < 1 ?
                Insert(application) :
                Update(application);
        }

        public IList<Application> SearchSubmittedBySeeker(Scholarship scholarship, string lastName)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStages.Submitted))
                .Add(Restrictions.Like("ApplicantName.LastName", string.Format("%{0}%", lastName)))
                .List<Application>();
        }

        public Application FindBySeekerandScholarship(Seeker seeker,Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .UniqueResult<Application>();
        }
    }
}