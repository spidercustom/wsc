﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using Spring.Context.Support;

namespace ScholarBridge.Web.MatchList
{
    public class ApplicationListActionHelper
    {
        public const string ACTION_COMMAND_NAME = "application-action";
        private const string ACTION_CONTROL_DEFAULT_ID = "defaultActionButton";

        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }

        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        public void SetupListView(ListView list)
        {
            list.ItemDataBound += List_ItemDataBound;
            list.ItemCommand += List_ItemCommand;
        }

        private void List_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var application = ((Application)((ListViewDataItem)e.Item).DataItem);
                var btn = (Button)e.Item.FindControl(ACTION_CONTROL_DEFAULT_ID);
                var applicationAction = GetApplicationAction(application);

				btn.Visible = !(applicationAction is BlankAction);
				btn.Text = applicationAction.Text;
				if (applicationAction is OpenUrlAction)
				{
					var action = (OpenUrlAction) applicationAction;
					if (action.IsShowInPopup)
					{
						var match = MatchService.GetMatch(UserContext.CurrentSeeker, application.Scholarship.Id);
						if (null != match)
						{
							btn.CommandName = string.Empty;
							btn.CommandArgument = string.Empty;

							btn.OnClientClick = String.Format("printApplications('{0}'); return false;", action.GetActionUrl(match));
							return;
						}
					}
				}
                btn.CommandName = ACTION_COMMAND_NAME;
                btn.CommandArgument = application.Scholarship.Id.ToString();
            }
        }

        private void List_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (!ACTION_COMMAND_NAME.Equals(e.CommandName))
                return;

            int scholarshipId = -1;
            if (e.CommandArgument != null && e.CommandArgument.ToString().Length > 0)
                scholarshipId = Convert.ToInt32(e.CommandArgument);
            if (scholarshipId < 0)
                return;
            var match = MatchService.GetMatch(UserContext.CurrentSeeker, scholarshipId);
            if (null != match)
                GetApplicationAction(match.Application).Execute(match);
        }

		private static MatchAction GetApplicationAction(Application application)
		{
            if (application.Scholarship.IsUseOnlineApplication)
            {
                
                return new BuildApplicationAction();
            }
            
			switch (application.ApplicationStatus)
			{
				case ApplicationStatus.Unknown:
					return BlankAction.Instance;
				case ApplicationStatus.Applying:
					return new EditApplicationAction();
				case ApplicationStatus.Submitted:
					return new ViewApplicationAction();
				case ApplicationStatus.BeingConsidered:
					return new ViewApplicationAction();
				case ApplicationStatus.Closed:
					return BlankAction.Instance;
				case ApplicationStatus.Offered:
					return new ViewContactInfoAction();
				case ApplicationStatus.Awarded:
					return new ViewApplicationAction();
				default:
					throw new NotSupportedException();
			}
		}

    }
}