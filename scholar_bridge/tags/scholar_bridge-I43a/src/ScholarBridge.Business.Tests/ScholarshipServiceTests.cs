using System;
using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class ScholarshipServiceTests
    {
		private const string NAME_OF_SCHOLARSHIP_TO_COPY = "CopyMe";
		private const string FIRST_COPY_SCHOLARSHIP_NAME = "Copy (1) of CopyMe";
        private const string SECOND_COPY_SCHOLARSHIP_NAME = "Copy (2) of CopyMe";

        private MockRepository mocks;
        private ScholarshipService scholarshipService;
        private IScholarshipDAL scholarshipDAL;
        private IMatchService matchService;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;
        private IRoleDAL roleDal;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            scholarshipDAL = mocks.StrictMock<IScholarshipDAL>();
            matchService = mocks.StrictMock<IMatchService>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();
            roleDal = mocks.StrictMock<IRoleDAL>();
            scholarshipService = new ScholarshipService
                                     {
                                         ScholarshipDAL = scholarshipDAL, 
                                         MatchService = matchService,
                                         MessagingService = messagingService,
                                         TemplateParametersService = templateParametersService,
                                         RoleService = roleDal
                                     };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(scholarshipDAL);
            mocks.BackToRecord(messagingService);
            mocks.BackToRecord(templateParametersService);
            mocks.BackToRecord(roleDal);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetByProviderThrowsExceptionIfNullPassed()
        {
            scholarshipService.GetByProvider(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_intermediary_throws_exception_if_null_passed()
        {
            scholarshipService.GetByIntermediary(null);
            Assert.Fail();
        }


        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_intermediary_not_activated_throws_exception_if_null_passed()
        {
            scholarshipService.GetByIntermediaryNotActivated(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_intermediary_with_stage_throws_exception_if_null_passed()
        {
            scholarshipService.GetByIntermediary(null, ScholarshipStages.Rejected);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void GetByProviderThrowsExceptionIfProviderNotApproved()
        {
            scholarshipService.GetByProvider(new Provider { ApprovalStatus = ApprovalStatus.PendingApproval });
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveThrowsExceptionIfNullPassed()
        {
            scholarshipService.Save(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveThrowsExceptionIfNoProviderAssociated()
        {
            var scholarship = new Scholarship {Provider = null};
            scholarshipService.Save(scholarship);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void SaveThrowsExceptionIfProviderNotApproved()
        {
            var scholarship = new Scholarship { Provider = new Provider { ApprovalStatus = ApprovalStatus.PendingApproval } };
            scholarshipService.Save(scholarship);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void delete_throws_exception_if_null_passed()
        {
            scholarshipService.Delete(null);
            Assert.Fail();
        }

         

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_organizations_throws_exception_if_null_provider_passed()
        {
            scholarshipService.GetByOrganizations(null, new Intermediary(), ScholarshipStages.Awarded);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_organizations_throws_exception_if_null_intermediary_passed()
        {
            scholarshipService.GetByOrganizations(new Provider {ApprovalStatus = ApprovalStatus.Approved}, null, ScholarshipStages.Awarded);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_organizations_without_stage_overload_throws_exception_if_null_provider_passed()
        {
            scholarshipService.GetByOrganizations(null, new Intermediary());
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_organizations_without_stage_overload_throws_exception_if_null_intermediary_passed()
        {
            scholarshipService.GetByOrganizations(new Provider { ApprovalStatus = ApprovalStatus.Approved }, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void get_by_organizations_without_stage_overload_throws_exception_if_unapproved_provider_passed()
        {
            scholarshipService.GetByOrganizations(new Provider { ApprovalStatus = ApprovalStatus.PendingApproval }, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_not_activated_by_organizations_throws_exception_if_null_provider_passed()
        {
            scholarshipService.GetNotActivatedByOrganizations(null, new Intermediary());
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_not_activated_by_organizations_throws_exception_if_null_intermediary_passed()
        {
            scholarshipService.GetNotActivatedByOrganizations(new Provider { ApprovalStatus = ApprovalStatus.Approved }, null);
            Assert.Fail();
        }


        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void close_award_period_throws_exception_if_null_passed()
        {
            scholarshipService.CloseAwardPeriod(null, DateTime.Now);
            Assert.Fail();
        }

        [Test]
        public void can_delete_scholarship()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.NotActivated };

            Expect.Call(() => messagingService.DeleteRelatedMessages(s));
            Expect.Call(() => scholarshipDAL.Delete(s));
            mocks.ReplayAll();

            scholarshipService.Delete(s);

            mocks.VerifyAll();
        }

        [Test]
        public void verify_scholarship_exists_by_name_and_provider_returns_true()
        {
            var name = "Scholarship-1";
            var provider = new Provider();
            var scholarship = new Scholarship { AcademicYear = new AcademicYear(2009)};
            Expect.Call(scholarshipDAL.FindByBusinessKey(provider, name, 2009)).Return(scholarship);
            mocks.ReplayAll();

            var exists = scholarshipService.ScholarshipExists(provider, name, 2009);
            mocks.VerifyAll();
            Assert.IsNotNull(exists);
        }

        [Test]
        public void verify_scholarship_exists_by_name_and_provider_returns_false()
        {
            var name = "Scholarship-1";
            var provider = new Provider();
            Expect.Call(scholarshipDAL.FindByBusinessKey(provider, name, 2008)).Return(null);
            mocks.ReplayAll();

            var exists = scholarshipService.ScholarshipExists(provider, name, 2008);
            mocks.VerifyAll();

            Assert.IsNull(exists);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_by_name_and_provider_name_arg_exception()
        {
            scholarshipService.ScholarshipExists(null, string.Empty, 2009);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_by_stage_and_provider_provider_arg_exception()
        {
            scholarshipService.GetByProvider(null, ScholarshipStages.None);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_NotActivatedScholarshipsByProvider_provider_arg_exception()
        {
            scholarshipService.GetByProviderNotActivated(null);
        }
        
        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void get_NotActivatedScholarshipsByProviderThrowsExceptionIfProviderNotApproved()
        {
            scholarshipService.GetByProviderNotActivated(new Provider { ApprovalStatus = ApprovalStatus.PendingApproval });
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_by_name_and_provider_provider_arg_exception()
        {
            scholarshipService.ScholarshipExists(null, "string", 2009);
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void GetByStageAndProviderThrowsExceptionIfProviderNotApproved()
        {
            scholarshipService.GetByProvider(new Provider { ApprovalStatus = ApprovalStatus.PendingApproval }, ScholarshipStages.None);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_minimum_amount_less_than_zero()
        {
            scholarshipService.Save(new Scholarship{ MinimumAmount = -1});
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_maximum_amount_less_than_one()
        {
            scholarshipService.Save(new Scholarship { MaximumAmount = 0 });
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_maximum_amount_less_than_minimum()
        {
            scholarshipService.Save(new Scholarship { MinimumAmount = 2, MaximumAmount = 1 } );
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void activate_throws_exception_on_null()
        {
            var u = new User();
			scholarshipService.Activate(null, u);
            Assert.Fail();
        }

        [Test]
        public void can_activate_scholarship()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var u = new User();
            var s = new Scholarship {Provider = p, Stage = ScholarshipStages.NotActivated };

            Expect.Call(scholarshipDAL.Update(s)).Return(s);
            Expect.Call(() => scholarshipDAL.Flush());
            Expect.Call(() => matchService.UpdateMatches(s));
			Expect.Call(() => templateParametersService.ScholarshipApproved(Arg<Scholarship>.Is.Equal(s), Arg<User>.Is.Equal(u), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(roleDal.FindByName(Role.WSC_ADMIN_ROLE)).Return(new Role { Name = Role.WSC_ADMIN_ROLE });
			Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
			Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
			mocks.ReplayAll();


            Assert.IsNull(s.ActivatedOn, "scholarship having activated-on property assiged, is not suitable for this test");
            scholarshipService.Activate(s, u);
            Assert.IsNotNull(s.ActivatedOn);
            Assert.AreEqual(ScholarshipStages.Activated, s.Stage);
            mocks.VerifyAll();
        }

        [Test]
		public void activate_do_not_throws_exception_if_already_activated()
        {
            var u = new User();
            var s = new Scholarship { Stage = ScholarshipStages.Activated };
            scholarshipService.Activate(s, u);
        }

		[Test]
		public void copying_scholarship_basic_case()
		{
			var scholarship = new Scholarship { Provider = new Provider(), Name = NAME_OF_SCHOLARSHIP_TO_COPY };

			Expect.Call(scholarshipDAL.FindByBusinessKey(scholarship.Provider,
				FIRST_COPY_SCHOLARSHIP_NAME, AcademicYear.CurrentScholarshipYear.Year)).Return(null);

			mocks.ReplayAll();

			var copiedScholarship = scholarshipService.CopyScholarship(scholarship);
			Assert.IsNotNull(copiedScholarship);
			Assert.AreEqual(FIRST_COPY_SCHOLARSHIP_NAME, copiedScholarship.Name);

			mocks.VerifyAll();
		}

		[Test]
		public void copying_scholarship_truncate_name()
		{
			string xes = new String('x', 100);

			string originalName = (NAME_OF_SCHOLARSHIP_TO_COPY + xes).Substring(0, 100);
			string expectedName = (FIRST_COPY_SCHOLARSHIP_NAME + xes).Substring(0, 100);


			var scholarship = new Scholarship { Provider = new Provider(), Name = originalName };

			Expect.Call(scholarshipDAL.FindByBusinessKey(scholarship.Provider,
				expectedName, AcademicYear.CurrentScholarshipYear.Year)).Return(null);

			mocks.ReplayAll();

			var copiedScholarship = scholarshipService.CopyScholarship(scholarship);
			Assert.IsNotNull(copiedScholarship);
			Assert.AreEqual(expectedName, copiedScholarship.Name);

			mocks.VerifyAll();
		}

		[Test]
        public void copying_scholarship_second_copy()
        {
            var scholarship = new Scholarship {Provider = new Provider(), Name = NAME_OF_SCHOLARSHIP_TO_COPY};

            Expect.Call(scholarshipDAL.FindByBusinessKey(scholarship.Provider, FIRST_COPY_SCHOLARSHIP_NAME,
                                                         AcademicYear.CurrentScholarshipYear.Year)).Return(scholarship);
            
            Expect.Call(scholarshipDAL.FindByBusinessKey(scholarship.Provider, SECOND_COPY_SCHOLARSHIP_NAME,
                                                         AcademicYear.CurrentScholarshipYear.Year)).Return(null);
            mocks.ReplayAll();

            var copiedScholarship = scholarshipService.CopyScholarship(scholarship);
            Assert.IsNotNull(copiedScholarship);
            Assert.AreEqual("Copy (2) of CopyMe", copiedScholarship.Name);

            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(TooManyScholarshipCopyExistsException))]
        public void coping_scholarship_till_exception()
        {
            var scholarship = new Scholarship {Provider = new Provider(), Name = NAME_OF_SCHOLARSHIP_TO_COPY};

            Expect.Call(scholarshipDAL.FindByBusinessKey(null, null, 0)).IgnoreArguments().Return(scholarship).Repeat.Times(9, 9);
            mocks.ReplayAll();

            scholarshipService.CopyScholarship(scholarship);
            Assert.Fail("Expected exception didn't occurred");
        }

        [Test]
        public void close_award_period()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.NotActivated, Intermediary = new Intermediary() };

            Expect.Call(scholarshipDAL.Update(s)).Return(s);
            mocks.ReplayAll();

            scholarshipService.CloseAwardPeriod(s, DateTime.Now);

            Assert.IsTrue(s.AwardPeriodClosed.HasValue);
            mocks.VerifyAll();
        }

        [Test]
        public void notify_seeker_closed_since_empty_list()
        {
            var since = DateTime.Now;
            Expect.Call(scholarshipDAL.FindAllClosedSince(since)).Return(new List<Scholarship>());
            mocks.ReplayAll();

            scholarshipService.NotifySeekersClosedSince(since);

            mocks.VerifyAll();
        }

        [Test]
        public void notify_seeker_closed_since_but_no_matches()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.Activated, Intermediary = new Intermediary(), LastUpdate = new ActivityStamp(new User()) };

            var since = DateTime.Now;
            Expect.Call(scholarshipDAL.FindAllClosedSince(since)).Return(new List<Scholarship> { s });
            Expect.Call(matchService.GetSavedMatches(s)).Return(new List<Match>());
            mocks.ReplayAll();

            scholarshipService.NotifySeekersClosedSince(since);

            mocks.VerifyAll();
        }

        [Test]
        public void notify_seeker_closed_since()
        {
            var p = new Provider{ Name = "TestProvider", AdminUser = new User {Name = new PersonName {FirstName = "TestName"}}};
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.Activated, Intermediary = new Intermediary(), LastUpdate = new ActivityStamp(new User())};
            var match = new Match {Scholarship = s, Seeker = new Seeker {User = new User {IsRecieveEmails = true}}};

            var since = DateTime.Now;
            Expect.Call(scholarshipDAL.FindAllClosedSince(since)).Return(new List<Scholarship> { s });
            Expect.Call(matchService.GetSavedMatches(s)).Return(new List<Match> { match });
            Expect.Call(() => templateParametersService.ScholarshipClosed(Arg<Scholarship>.Is.Equal(s), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
            mocks.ReplayAll();

            scholarshipService.NotifySeekersClosedSince(since);

            mocks.VerifyAll();
        }

        [Test]
        public void notify_seeker_scholarship_due_empty_list()
        {
            var since = DateTime.Today;
            Expect.Call(scholarshipDAL.FindAllDueOn(since.AddDays(3))).Return(new List<Scholarship>());
            mocks.ReplayAll();

            scholarshipService.NotifySeekersScholarshipDue(3);

            mocks.VerifyAll();
        }

        [Test]
        public void notify_seeker_scholarship_due_but_no_matches()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.Activated, Intermediary = new Intermediary(), LastUpdate = new ActivityStamp(new User()) };

            var since = DateTime.Today;
            Expect.Call(scholarshipDAL.FindAllDueOn(since.AddDays(3))).Return(new List<Scholarship> { s });
            Expect.Call(matchService.GetSavedMatches(s)).Return(new List<Match>());
            mocks.ReplayAll();

            scholarshipService.NotifySeekersScholarshipDue(3);

            mocks.VerifyAll();
        }

        [Test]
        public void notify_seeker_scholarship_due_doesnt_message_submitted()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.Activated, Intermediary = new Intermediary(), LastUpdate = new ActivityStamp(new User()) };
            var match = new Match
                            {
                                Scholarship = s, Seeker = new Seeker { User = new User { IsRecieveEmails = true } },
                                Application = new Application(ApplicationType.Internal) { Stage = ApplicationStages.Submitted }
                            };

            var since = DateTime.Today;
            Expect.Call(scholarshipDAL.FindAllDueOn(since.AddDays(3))).Return(new List<Scholarship> { s });
            Expect.Call(matchService.GetSavedMatches(s)).Return(new List<Match> { match });
            Expect.Call(() => templateParametersService.ApplicationDue(Arg<Scholarship>.Is.Equal(s), Arg<int>.Is.Equal(3), Arg<MailTemplateParams>.Is.Anything));
            mocks.ReplayAll();

            scholarshipService.NotifySeekersScholarshipDue(3);

            mocks.VerifyAll();
        }

        [Test]
        public void notify_seeker_scholarship_due_messages_nonsubmitted()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.Activated, Intermediary = new Intermediary(), LastUpdate = new ActivityStamp(new User()) };
            var match = new Match
            {
                Scholarship = s,
                Seeker = new Seeker { User = new User { IsRecieveEmails = true } },
                Application = new Application(ApplicationType.Internal) { Stage = ApplicationStages.None }
            };

            var since = DateTime.Today;
            Expect.Call(scholarshipDAL.FindAllDueOn(since.AddDays(3))).Return(new List<Scholarship> { s });
            Expect.Call(matchService.GetSavedMatches(s)).Return(new List<Match> { match });
            Expect.Call(() => templateParametersService.ApplicationDue(Arg<Scholarship>.Is.Equal(s), Arg<int>.Is.Equal(3), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
            mocks.ReplayAll();

            scholarshipService.NotifySeekersScholarshipDue(3);

            mocks.VerifyAll();
        }

        [Test]
        public void get_by_organization_calls_dal_with_parameter()
        {
            var p = new Provider { Name = "Provider", ApprovalStatus = ApprovalStatus.Approved, AdminUser = new User { Name = new PersonName { FirstName = "Admin" } } };
            var i = new Intermediary { Name = "Intermediary", AdminUser = new User { Name = new PersonName { FirstName = "Admin" } } };
            var scholarships = new List<Scholarship>();
            Expect.Call(scholarshipDAL.FindByOrganizations(p, i)).Return(scholarships);
            mocks.ReplayAll();

            var retrivedScholarship = scholarshipService.GetByOrganizations(p, i);

            mocks.VerifyAll();
            Assert.AreEqual(scholarships, retrivedScholarship);
        }

        [Test]
        public void get_by_organization_with_stage_calls_dal_with_parameter()
        {
            var p = new Provider { Name = "Provider", ApprovalStatus = ApprovalStatus.Approved, AdminUser = new User { Name = new PersonName { FirstName = "Admin" } } };
            var i = new Intermediary { Name = "Intermediary", AdminUser = new User { Name = new PersonName { FirstName = "Admin" } } };
            var scholarships = new List<Scholarship>();
            Expect.Call(scholarshipDAL.FindByOrganizations(p, i, ScholarshipStages.None)).Return(scholarships);
            mocks.ReplayAll();

            var retrivedScholarship = scholarshipService.GetByOrganizations(p, i, ScholarshipStages.None);

            mocks.VerifyAll();
            Assert.AreEqual(scholarships, retrivedScholarship);
        }

        [Test]
        public void can_approve_scholarshiponlineapplicationUrl()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var provideruser = p.AdminUser.Organizations;
            var u = new User();
       

            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.NotActivated,OnlineApplicationUrl="www.testme.com",IsOnlineApplicationApproved=false };

            Expect.Call(scholarshipDAL.Update(s)).Return(s);
            Expect.Call(() => templateParametersService.ScholarshipOnlineApplicationUrlApproved(Arg<Scholarship>.Is.Equal(s), Arg<User>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
            mocks.ReplayAll();
            scholarshipService.ApproveOnlineApplicationUrl(s, u,p.AdminUser);
            Assert.IsTrue(s.IsOnlineApplicationApproved);
             
            mocks.VerifyAll();
        }
        [Test]
        public void can_reject_scholarshiponlineapplicationUrl()
        {
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };
            var provideruser = p.AdminUser.Organizations;
            var u = new User();


            var s = new Scholarship { Provider = p, Stage = ScholarshipStages.NotActivated, OnlineApplicationUrl = "www.testme.com", IsOnlineApplicationApproved = true  };

            Expect.Call(scholarshipDAL.Update(s)).Return(s);
            Expect.Call(() => templateParametersService.ScholarshipOnlineApplicationUrlRejected(Arg<Scholarship>.Is.Equal(s), Arg<User>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(Arg<ScholarshipMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));
            mocks.ReplayAll();
            scholarshipService.RejectOnlineApplicationUrl(s, u, p.AdminUser);
            Assert.IsFalse(s.IsOnlineApplicationApproved);

            mocks.VerifyAll();
        }
    }
}
