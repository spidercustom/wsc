using NUnit.Framework;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class AttachmentTests
    {

        [Test]
        public void display_size_handles_bytes()
        {
            var attachment = new Attachment {Bytes = 0};
            Assert.AreEqual("0B", attachment.DisplaySize);

            attachment.Bytes = 1;
            Assert.AreEqual("1B", attachment.DisplaySize);

            attachment.Bytes = 1023;
            Assert.AreEqual("1023B", attachment.DisplaySize);
        }

        [Test]
        public void display_size_handles_kibibytes()
        {
            var attachment = new Attachment { Bytes = 1024 };
            Assert.AreEqual("1KiB", attachment.DisplaySize);

            attachment.Bytes = 1024 + 512;
            Assert.AreEqual("1.5KiB", attachment.DisplaySize);

            attachment.Bytes = 1024 * 1024 - 100;
            Assert.AreEqual("1023.9KiB", attachment.DisplaySize);
        }

        [Test]
        public void display_size_handles_mibibytes()
        {
            var attachment = new Attachment { Bytes = 1024*1024 };
            Assert.AreEqual("1MiB", attachment.DisplaySize);

            attachment.Bytes = 1024 * 1024 *3;
            Assert.AreEqual("3MiB", attachment.DisplaySize);
        }

		[Test]
		public void generated_name_creates_guid()
		{
			var attachment = new Attachment();
			var guid = attachment.GenerateUniqueName();
			Assert.AreEqual(guid, attachment.UniqueName);
			StringAssert.IsMatch(@"([\w]{8}-)(([\w]{4}-){3})[\w]{12}", guid);
		}

		[Test]
		public void attachment_has_IncludeWithApplicationFlag()
		{
			var attachment = new Attachment();
			Assert.AreEqual(false, attachment.IncludeWithApplications);
			attachment.IncludeWithApplications = true;
			Assert.AreEqual(true, attachment.IncludeWithApplications);
		}
	}
}
