﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Intermediary.Relationships
{
    public partial class Create: Page
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }

        private const string DEFAULT_PAGEURL = "~/Intermediary/Admin/#relationship-tab";
        
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                var providers = RelationshipService.FindAvailableProviders(UserContext.CurrentIntermediary);
                if (providers.Count == 0)
                {
                    SuccessMessageLabel.SetMessage("No provider is available to add relationship request");
                    Response.Redirect(DEFAULT_PAGEURL);
                }
                orgList.DataSource = providers;
                orgList.DataTextField = "Name";
                orgList.DataValueField = "Id";
                orgList.DataBind();
            }
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            PopupHelper.CloseSelf(false);
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            var intermediary = UserContext.CurrentIntermediary;
            int selectedId = int.Parse(orgList.SelectedValue);
            var provider = ProviderService.FindById(selectedId);
            var relationship = new Relationship()
                                  {
                                      Provider=provider,
                                      Intermediary=intermediary,
                                      Requester = RelationshipRequester.Intermediary,
                                      LastUpdate=new ActivityStamp(UserContext.CurrentUser),
                                  };
            try
            {
                RelationshipService.CreateRequest(relationship);
                SuccessMessageLabel.SetMessage("Request Created Successfully.");
                PopupHelper.CloseSelf(false);
                PopupHelper.RefreshParent(ResolveUrl(DEFAULT_PAGEURL));
            }
            catch (DuplicateRelationshipException)
            {
                validateorganization.IsValid = false;
                validateorganization.Text = "You already have a relationship with selected organization. Please choose a different one.";
            }

        }

    }
}
