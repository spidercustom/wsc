<?xml version="1.0"?>

<configuration>
    <configSections>
        <section name="log4net"  type="log4net.Config.Log4NetConfigurationSectionHandler,log4net"/>
        
        <sectionGroup name="system.web.extensions" type="System.Web.Configuration.SystemWebExtensionsSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
            <sectionGroup name="scripting" type="System.Web.Configuration.ScriptingSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
                <section name="scriptResourceHandler" type="System.Web.Configuration.ScriptingScriptResourceHandlerSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                <sectionGroup name="webServices" type="System.Web.Configuration.ScriptingWebServicesSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
                    <section name="jsonSerialization" type="System.Web.Configuration.ScriptingJsonSerializationSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="Everywhere"/>
                    <section name="profileService" type="System.Web.Configuration.ScriptingProfileServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                    <section name="authenticationService" type="System.Web.Configuration.ScriptingAuthenticationServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                    <section name="roleService" type="System.Web.Configuration.ScriptingRoleServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                </sectionGroup>
            </sectionGroup>
        </sectionGroup>

        <sectionGroup name="spring">
            <section name="context" type="Spring.Context.Support.WebContextHandler, Spring.Web"/>
            <section name="parsers" type="Spring.Context.Support.NamespaceParsersSectionHandler, Spring.Core"/>
        </sectionGroup>
        <sectionGroup name="common">
            <section name="logging" type="Common.Logging.ConfigurationSectionHandler, Common.Logging"/>
        </sectionGroup>
        <section name="databaseSettings" type="System.Configuration.NameValueSectionHandler, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"/>

        <section name="neatUpload"
             type="Brettle.Web.NeatUpload.ConfigSectionHandler, Brettle.Web.NeatUpload"
             allowLocation="true" />

		<section name="rewriter"
		 requirePermission="false"
		 type="Intelligencia.UrlRewriter.Configuration.RewriterConfigurationSectionHandler, Intelligencia.UrlRewriter" />

    </configSections>

    <appSettings>
        <!-- AttachmentsDirectory - where to store the attachments that are uploaded. -->
        <add key="AttachmentsDirectory" value ="c:\temp"/>
		<add key="DomainName" value ="www.TheWashBoard.Org"/>
        <!-- EmailsDirectory - Where the Email templates are found -->
        <add key="EmailsDirectory" value ="c:\work\projects\scholar_bridge\src\Web\Emails"/>

		<!-- If BCCEmail has value, then send a blind copy of all emails sent to this address -->
		<add key="BCCEmail" value =""/>
        <!-- NoReplyEmail - The default from address for emails  -->
		<add key="NoReplyEmail" value ="NoReply@theWashBoard.org"/>
        <!-- HECBAdminEmail -  where to send "Admin" messages -->
        <add key="HECBAdminEmail" value ="glane@spiderlogic.com"/>
      
        <!-- Email addresses for the 'ContactUs' function (Support, Problems & Suggestions) -->
        <add key="WashBoardSupportEmail" value ="support@theWashBoard.org"/>
        <add key="WashBoardProblemEmail" value ="problems@theWashBoard.org"/>
        <add key="WashBoardSuggestionEmail" value ="suggestions@theWashBoard.org"/>

           <!--ProxyURL - sets the URL of the website to be used in emails (or other documents) when running behind the 
                          reverse proxy firewall - Fortress
                          
                        Set the value to "https://fortress.wa.gov/hecb/wsc" when in demo/prod; blank for dev
        -->  
        <add key ="ProxyURL" value =""/>
        <add key="DisableSeekerRegistrationWithMessage" value=""/>
		<!--
			The following settings are for managing the production of the public Scholarship List.
			This list is produced by Quartz job ScholarshipListJob.
            
			ScholarshipListDirectory - local directory in which to put the public scholarship list
			ScholarshipListFTPEnabled -a bool that indicates if we've turned on the FTP send of the list
			ScholarshipListFTPSite - ftp site name or ip
			ScholarshipListFTPDirectory - directory on the ftp site to put the scholarship list
			ScholarshipListFTPLogon - logon id for the ftp site
			ScholarshipListFTPPassword - password for the ftp site
		-->
		<add key ="ScholarshipListDirectory" value ="C:\WorkingCopy\scholar_bridge_trunk\src\Web\_ScholarshipData"/>
        <add key ="ScholarshipListFTPEnabled" value ="false"/>
		<add key ="ScholarshipListFTPSite" value =""/>
		<add key ="ScholarshipListFTPDirectory" value =""/>
		<add key ="ScholarshipListFTPLogon" value =""/>
		<add key ="ScholarshipListFTPPassword" value =""/>
		<!--
			If ShowErrorText is set to true, then the full text of the exception will be shown to the 
			user on the common 'Oops' error page (error.aspx).  Otherwise, just show the default message.
		-->
		<add key ="ShowErrorText" value="true"/>

    </appSettings>

    <neatUpload xmlns="http://www.brettle.com/neatupload/config/2008"
                maxRequestLength="11264">
        <providers>
            <add name="FilesystemUploadStorageProvider"
                 type="Brettle.Web.NeatUpload.FilesystemUploadStorageProvider"
                 tempDirectory="C:\Temp" />
        </providers>
        
    </neatUpload>
    
    <!-- Database settings to use to connect to the database-->

    <databaseSettings>
        <add key="db.datasource" value="localhost"/>
        <add key="db.user" value="scholarBridgeUser"/>
        <add key="db.password" value="scholarBridgeUser"/>
        <add key="db.database" value="ScholarBridge"/>
    </databaseSettings>








    <system.net>
        <mailSettings>
            <smtp deliveryMethod="Network" from="admin@scholarbridge.com">
                <network
                    host="GBEXCH0.CORP.WIPFLI.COM"
                    defaultCredentials="true"/>
            </smtp>
        </mailSettings>
    </system.net>

    <spring>
        <parsers>
            <parser type="Spring.Data.Config.DatabaseNamespaceParser, Spring.Data"/>
        </parsers>
        <context>
            <resource uri="assembly://ScholarBridge.Data/ScholarBridge.Data.NHibernate/DAL.xml"/>
            <resource uri="assembly://ScholarBridge.Business/ScholarBridge.Business/Business.xml"/>
            <resource uri="assembly://ScholarBridge.Jobs/ScholarBridge.Jobs/Jobs.xml"/>
            <resource uri="assembly://ScholarBridge.Web/ScholarBridge.Web/Web.xml"/>

            <resource uri="~/SpringConfig.xml"/>
            <resource uri="~/Admin/SpringConfig.xml"/>
            <resource uri="~/Common/SpringConfig.xml"/>
            <resource uri="~/Intermediary/SpringConfig.xml"/>
            <resource uri="~/Profile/SpringConfig.xml"/>
            <resource uri="~/Provider/SpringConfig.xml"/>
            <resource uri="~/Seeker/SpringConfig.xml"/>
        </context>
    </spring>

    <common>
        <logging>
            <factoryAdapter type="Common.Logging.Log4Net.Log4NetLoggerFactoryAdapter, Common.Logging.Log4Net">
                <!-- choices are INLINE, FILE, FILE-WATCH, EXTERNAL-->
                <!-- otherwise BasicConfigurer.Configure is used   -->
                <!-- log4net configuration file is specified with key configFile-->
                <arg key="configType" value="INLINE"/>
                <arg key="configFile" value="filename"/>
            </factoryAdapter>
        </logging>
    </common>

    <system.web>

        <!-- maxRequestLength="11264" = allow files of about ~10MB to be uploaded to the server -->
        <!-- executionTimeout="240" - allow uploads to take 4 minutes, which might be to small -->
        <httpRuntime
             maxRequestLength="11264"
             executionTimeout="240"
        />

        <!-- 
            Set compilation debug="true" to insert debugging 
            symbols into the compiled page. Because this 
            affects performance, set this value to true only 
            during development.
        -->
        <compilation debug="true">

            <assemblies>
                <add assembly="System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
                <add assembly="System.Data.DataSetExtensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
                <add assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
                <add assembly="System.Xml.Linq, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
            </assemblies>

        </compilation>
        <!--
            timeout=129600 means 90 days. This is basically only used for "remember me" users. All others will expire when the browser closes
        -->
        <authentication mode="Forms">
            <forms loginUrl="~/login.aspx" 
                   name=".ASPXFORMSAUTH" 
                   timeout="20"
                   slidingExpiration="true"/>
        </authentication>
        <!--
            The <customErrors> section enables configuration 
            of what to do if/when an unhandled error occurs 
            during the execution of a request. Specifically, 
            it enables developers to configure html error pages 
            to be displayed in place of a error stack trace.

        <customErrors mode="RemoteOnly" defaultRedirect="GenericErrorPage.htm">
            <error statusCode="403" redirect="NoAccess.htm" />
            <error statusCode="404" redirect="FileNotFound.htm" />
        </customErrors>
        -->

        <customErrors mode="RemoteOnly" defaultRedirect="~/Error.aspx">
            <error statusCode="413" redirect="~/NeatUpload/Error413.aspx" />
        </customErrors>

        <siteMap defaultProvider="secureProvider">
            <providers>
              <!--
                Added a custom SiteMapProvider because the System.Web version just didn't seem to correctly check the roles. DR
              -->
              <add name="secureProvider" type="ScholarBridge.Web.SBXmlSiteMapProvider" siteMapFile="~/Web.sitemap" securityTrimmingEnabled="true" />
              <!--add name="secureProvider" type="System.Web.XmlSiteMapProvider" siteMapFile="~/Web.sitemap" securityTrimmingEnabled="true" /-->
            </providers>
        </siteMap>

        <pages>
            <controls>
                <add tagPrefix="asp" namespace="System.Web.UI" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
                <add tagPrefix="asp" namespace="System.Web.UI.WebControls" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            </controls>
        </pages>

        <httpHandlers>
            <remove verb="*" path="*.asmx"/>
            <add verb="*" path="*.asmx" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add verb="*" path="*_AppService.axd" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add verb="GET,HEAD" path="ScriptResource.axd" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" validate="false"/>
            <add verb="*" path="*ImageHipChallenge.aspx"  type="Msdn.Web.UI.WebControls.ImageHipChallengeHandler, Hip"/>
          <add verb="*" path="*.aspx" type="Spring.Web.Support.PageHandlerFactory, Spring.Web"/>
        </httpHandlers>
        <httpModules>
            <add name="UploadHttpModule" type="Brettle.Web.NeatUpload.UploadHttpModule, Brettle.Web.NeatUpload" />
            <add name="ScriptModule" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
            <add name="OpenSessionInView" type="Spring.Data.NHibernate.Support.OpenSessionInViewModule, Spring.Data.NHibernate20"/>
            <add name="Spring" type="Spring.Context.Support.WebSupportModule, Spring.Web"/>
			<add name="UrlRewriter" type="Intelligencia.UrlRewriter.RewriterHttpModule, Intelligencia.UrlRewriter" />
        </httpModules>
        <roleManager enabled="true" defaultProvider="SpiderRoleProvider">
            <providers>
                <clear/>
                <add applicationName="/" name="SpiderRoleProvider" type="ScholarBridge.Web.Security.SpiderRoleProvider"/>
            </providers>
        </roleManager>
        <membership defaultProvider="SpiderMembershipProvider" userIsOnlineTimeWindow="20">
            <providers>
                <clear/>
                <add name="SpiderMembershipProvider"
                     type="ScholarBridge.Web.Security.SpiderMembershipProvider"
                     applicationName="/"
                     minRequiredPasswordLength="8"
                     passwordFormat="Hashed"
                     passwordStrengthRegularExpression="^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d\W]).*$"
                     requiresQuestionAndAnswer="false"
                     enablePasswordRetrieval="false"
                     enablePasswordReset="true" 
                     />
            </providers>
        </membership>
    </system.web>

    <log4net debug="false">
        <!-- Error Log Handler -->
        <appender name="ErrorRollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
            <file value="logs\\errorLog.txt"/>
            <appendToFile value="true"/>
            <rollingStyle value="Size"/>
            <maxSizeRollBackups value="20"/>
            <maximumFileSize value="5MB"/>
            <staticLogFileName value="true"/>
            <filter type="log4net.Filter.LevelMatchFilter">
                <levelToMatch value="Error"/>
            </filter>
            <threshold value="Error"/>
            <filter type="log4net.Filter.DenyAllFilter"/>
            <evaluator type="log4net.Core.LevelEvaluator">
                <threshold value="Error"/>
            </evaluator>
            <layout type="log4net.Layout.PatternLayout">
                <conversionPattern value="%-5p %d %5rms %c{1} %M - %m%n"/>
            </layout>
        </appender>
        <!-- Information Log Handler -->
        <appender name="InfoRollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
            <file value="logs\\infoLog.txt"/>
            <appendToFile value="true"/>
            <rollingStyle value="Size"/>
            <maxSizeRollBackups value="20"/>
            <maximumFileSize value="5MB"/>
            <staticLogFileName value="true"/>
            <filter type="log4net.Filter.LevelMatchFilter">
                <levelToMatch value="Info"/>
            </filter>
            <threshold value="Info"/>
            <filter type="log4net.Filter.DenyAllFilter"/>
            <evaluator type="log4net.Core.LevelEvaluator">
                <threshold value="Info"/>
            </evaluator>
            <layout type="log4net.Layout.PatternLayout">
                <conversionPattern value="%-5p %d %5rms %c{1} %M - %m%n"/>
            </layout>
        </appender>
        <appender name="DebugRollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
            <file value="logs\\debugLog.txt"/>
            <appendToFile value="true"/>
            <rollingStyle value="Size"/>
            <maxSizeRollBackups value="20"/>
            <maximumFileSize value="5MB"/>
            <staticLogFileName value="true"/>
            <threshold value="Debug"/>
            <layout type="log4net.Layout.PatternLayout">
                <conversionPattern value="%-5p %d %5rms %c{1} %M - %m%n"/>
            </layout>
        </appender>

        <appender name="SmtpAppender" type="log4net.Appender.SmtpAppender,log4net">
          <!--
              multiple email addresses can be specified in the 'to' address by using a comma separated list
              e.g. <to value="daver@hecb.wa.gov,d.b.richard@gmail.com"/>
            -->
          <to value="daver@hecb.wa.gov" />
          <from value="log4net@hecb.wa.gov" />
          <subject value="theWashBoard error - LOCALHOST SITE" />
          <smtpHost value="mail" />
          <bufferSize value="512" />
          <lossy value="false" />
          <filter type="log4net.Filter.LevelMatchFilter">
            <levelToMatch value="Error"/>
          </filter>
          <threshold value="Error"/>
          <evaluator type="log4net.Core.LevelEvaluator">
            <threshold value="WARN"/>
          </evaluator>
          <layout type="log4net.Layout.PatternLayout,log4net">
            <conversionPattern value="%property{log4net:HostName} :: %level :: %message %newlineLogger: %logger%newlineThread: %thread%newlineDate: %date%newlineNDC: %property{NDC}%newline%newline" />
          </layout>
        </appender>

        <root>
            <level value="ALL"/>
            <appender-ref ref="ErrorRollingLogFileAppender"/>
            <appender-ref ref="InfoRollingLogFileAppender"/>
            <appender-ref ref="DebugRollingLogFileAppender"/>
            <!--
              uncomment the following to send log errors via email 
            -->
            <!--<appender-ref ref="SmtpAppender"/>-->
        </root>
        <!-- Set logging for Spring.  Logger names in Spring correspond to the namespace -->
        <logger name="Spring">
            <level value="WARN"/>
        </logger>
        <logger name="Spring.Data">
            <level value="WARN"/>
        </logger>
        <!-- having NHibernate or NHibernate.Loader.Loader set to INFO or 
            better will log SQL statements -->
        <logger name="NHibernate">
            <level value="INFO"/>
        </logger>
        <logger name="NHibernate.SQL">
            <level value="DEBUG"/>
        </logger>
        <!-- turn off SQL statements by making this something higher than INFO -->
        <logger name="NHibernate.Loader.Loader">
            <level value="WARN"/>
        </logger>
        <logger name="Quartz">
            <level value="INFO"/>
        </logger>
    </log4net>

    <system.codedom>
        <compilers>
            <compiler language="c#;cs;csharp" extension=".cs" warningLevel="4" type="Microsoft.CSharp.CSharpCodeProvider, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
                <providerOption name="CompilerVersion" value="v3.5"/>
                <providerOption name="WarnAsError" value="false"/>
            </compiler>
        </compilers>
    </system.codedom>

    <!-- 
        The system.webServer section is required for running ASP.NET AJAX under Internet
        Information Services 7.0.  It is not necessary for previous version of IIS.
    -->
    <system.webServer>
        <validation validateIntegratedModeConfiguration="false"/>
        <modules runAllManagedModulesForAllRequests="true">
            <remove name="ScriptModule"/>
            <add name="UploadHttpModule" type="Brettle.Web.NeatUpload.UploadHttpModule, Brettle.Web.NeatUpload" preCondition="managedHandler"/>
			<add name="UrlRewriter" type="Intelligencia.UrlRewriter.RewriterHttpModule, Intelligencia.UrlRewriter" />
            <add name="ScriptModule" preCondition="managedHandler" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        </modules>
        <handlers>
            <remove name="WebServiceHandlerFactory-Integrated"/>
            <remove name="ScriptHandlerFactory"/>
            <remove name="ScriptHandlerFactoryAppServices"/>
            <remove name="ScriptResource"/>
            <add name="ScriptHandlerFactory" verb="*" path="*.asmx" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add name="ScriptHandlerFactoryAppServices" verb="*" path="*_AppService.axd" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add name="ScriptResource" preCondition="integratedMode" verb="GET,HEAD" path="ScriptResource.axd" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        </handlers>
    </system.webServer>

    <runtime>
        <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
            <dependentAssembly>
                <assemblyIdentity name="NHibernate" publicKeyToken="aa95f207798dfdb4" culture="neutral"/>
                    <bindingRedirect oldVersion="2.0.0.4000" newVersion="2.0.1.4000"/>
            </dependentAssembly>
            <dependentAssembly>
                <assemblyIdentity name="Quartz" publicKeyToken="f6b8c98a402cc8a4"/>
                <bindingRedirect oldVersion="0.0.0.0-65535.65535.65535.65535" newVersion="1.0.0.3"/>
            </dependentAssembly>
            <dependentAssembly>
                <assemblyIdentity name="System.Web.Extensions" publicKeyToken="31bf3856ad364e35"/>
                <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0"/>
            </dependentAssembly>
            <dependentAssembly>
                <assemblyIdentity name="System.Web.Extensions.Design" publicKeyToken="31bf3856ad364e35"/>
                <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0"/>
            </dependentAssembly>
        </assemblyBinding>
    </runtime>

	<rewriter>
		<rewrite url="~/ScholarshipDetails/(.+)" to="~/ScholarshipDetails.aspx?urlkey=/$1" />
	</rewriter>

</configuration>
