﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Common.Extensions;


namespace ScholarBridge.Web.Admin.Editors.Faqs 
{

    public partial class FaqEntry : UserControl
    {
        public IFaqService FaqService { get; set; }
		public IUserContext UserContext { get; set; }

        public delegate void FaqSaved(Faq faq);
        public delegate void FaqCanceled();

        public event FaqSaved FormSaved;
        public event FaqCanceled FormCanceled;

		public int? FaqID 
		{ 
			get
			{
				if (ViewState["FaqID"] == null)
					return new int?();
				return (int?) ViewState["FaqID"];
			} 
			set
			{
				ViewState["FaqID"] = value;
				faqInContext = null;
				SetPageValues();
			} 
		}

    	private Faq faqInContext;
        private Faq FaqInContext
    	{
			get
			{
				if (faqInContext == null)
				{
					faqInContext = !FaqID.HasValue 
						? new Faq() 
						: FaqService.GetById(FaqID.Value);
				}
				return faqInContext;
			}
    	}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) 
				return;

            if (null == FaqInContext)
            {
                errorMessage.Visible = true;
                return;
            }
        	SetPageValues();
        }

		private void SetPageValues()
		{
			questionControl.Text = FaqInContext.Question;
			answerControl.Text = FaqInContext.Answer;
			faqTypeButtons.SelectedValue = (int)FaqInContext.FaqType;
		}


        protected void saveButton_Click(object sender, EventArgs e)
        {   Page.Validate();
            if (!Page.IsValid) return;
            
			FaqInContext.Question = questionControl.Text;
        	FaqInContext.Answer = answerControl.Text;
			faqInContext.FaqType = (FaqType)faqTypeButtons.SelectedValue;
			FaqInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            FaqService.Save(FaqInContext);
            if (null != FormSaved)
            {
                FormSaved(FaqInContext);
            }
        }

		 
        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}