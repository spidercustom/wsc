﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class FlagEnumCheckBoxList : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
        }

        

        public string ExcludeKeys { get; set; }
        public bool MultiColumn { get; set; }
        public bool ShowSelectAllButton { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (MultiColumn)
                CheckBoxList.RepeatColumns = 2;

           
        }
        public Unit CheckboxListWidth { get; set; }

        
        public FlagEnumCheckBoxList()
        {
            CheckboxListWidth = new Unit(450, UnitType.Pixel);
        }

        public void DisableControls()
        {
            CheckBoxList.Enabled = false;
        }
        public void EnableControls()
        {
            CheckBoxList.Enabled = true ;
        }
        protected override void OnPreRender(EventArgs e)
        {
            CheckBoxList.Width = CheckboxListWidth;
            SelectAllButton.Visible = ShowSelectAllButton;
            SetSelectAllButtonEvent();
           base.OnPreRender(e);
        }

        private string datasourceEnumTypeName;
        public string DatasourceEnumTypeName
        {
            get { return datasourceEnumTypeName; }
            set
            {
                datasourceEnumTypeName = value;
                DataBind();
            }
        }

        public void SelectAll()
        {
            foreach (ListItem item in CheckBoxList.Items)
                item.Selected = true;
        }

        public void DeselectAll()
        {
            foreach (ListItem item in CheckBoxList.Items)
                item.Selected = true;
        }
        private void SetSelectAllButtonEvent()
        {

            var scriptname = "SetSelectAllButtonEvent_"+ CheckBoxList.ClientID+ "_script";
            var script = "$('#" + SelectAllButton.ClientID + "').click(function() {$('#" + CheckBoxList.ClientID +
                         "').find(':checkbox').attr('checked', 'checked');});";
            var page = HttpContext.Current.CurrentHandler as Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptname))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptname, script);
         
        }
        public string SelectAllButtonClientID
        {
            get
            {
                if (SelectAllButton != null)
                    return SelectAllButton.ClientID;
                else
                    return "";
            }

        }
        public override void DataBind()
        {
            if (!string.IsNullOrEmpty(DatasourceEnumTypeName))
            {
                Type enumType = Type.GetType(DatasourceEnumTypeName, true);
                //exclude if any keys need to be excluded from data bind display
                if (!String.IsNullOrEmpty(ExcludeKeys))
                {
                    string[] strings = ExcludeKeys.Split(",".ToCharArray());
                    int[] ints = strings.Select(x => int.Parse(x)).ToArray();
                    CheckBoxList.DataSource = enumType.GetKeyValue(ints);    
                }
                else
                {
                    CheckBoxList.DataSource = enumType.GetKeyValue();    
                }
                
                CheckBoxList.DataTextField = "Value";
                CheckBoxList.DataValueField = "Key";
            }

            base.DataBind();
            SelectAll();
        }

        [DefaultValue(false)]
        public bool SelectNoneAllowed { get; set; }

        [Browsable(false)]
        public int SelectedValues
        {
            get
            {
                int result = 0;
                var selectedItems = from ListItem item in CheckBoxList.Items
                                    where item.Selected
                                    select item;

                foreach (var item in selectedItems)
                {
                    result |= Int32.Parse(item.Value);
                }
                return result;
            }
            set
            {
                foreach (ListItem item in CheckBoxList.Items)
                {
                    int itemValue = Int32.Parse(item.Value);
                    item.Selected = (itemValue & value).Equals(itemValue);
                }
            }
        }
       
        
    }
}