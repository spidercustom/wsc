﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>
<%@ Import Namespace="ScholarBridge.Domain.Auth"%>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<asp:ListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    OnPagePropertiesChanged="matchList_PagePropertiesChanged"
    onpagepropertieschanging="matchList_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th></th>
                <th style="width:250px;">Scholarship Name</th>
                <th id="th2" runat="server"  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Rank', 'Matches are ranked based on your profile and the number of minimum eligibility and preferred criteria you meet. Minimum requirements make up 2/3 of the rank, while preferences count for 1/3.', '')">Rank</th>
                <th id="th7" runat="server"  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Eligibility Criteria', 'The number of minimum eligibility requirements that match your profile and are defined in the Scholarship.', '')">Eligibility Criteria</th>
                <th id="th3" runat="server"  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Preferred Criteria', 'The number of preferred criteria that match your profile and are defined in the scholarship.', '')">Preferred Criteria</th>
                <th id="th4" runat="server"  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Application Due Date', 'The date the scholarship application is due.', '')">Application Due Date</th>
                <th id="th5" runat="server"  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Number of Awards', 'The anticipated number of scholarship awards available.', '')"># of Awards</th>
                <th id="th6" runat="server"  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Scholarship Amount', 'The anticipated amount of the scholarship award.', '')">Scholarship Amount</th>
                <th id="th1" runat="server"  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Action', 'Scholarship Applications are saved on your My Applications tab. If a scholarship is closed, you can save it to your scholarships of interest and apply next year.', '')">Action</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:ImageButton id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png"/></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:##}%")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="ListButton" Width="70px" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:ImageButton  id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png" /></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:##}%")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="ListButton" Width="70px" /></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships saved to <b>My Scholarships of Interest.</b> Click <asp:Image ID="Image4" ImageUrl="~/images/star_empty.png" runat="server"/> to save.</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="matchList" PageSize="20" onprerender="pager_PreRender" >
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
</div> 