﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;
namespace ScholarBridge.Web.Common
{
    public partial class EditUserEmail : UserControl
    {
        public delegate void UserEmailChanged(User user,string Message);
        public event UserEmailChanged UserEmailSaved;
        public event FormCanceled FormCanceled;
        
        public IUserService UserService { get; set; }
        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (null == CurrentUser)
                {
                    errorMessage.Visible = true;
                    return;
                }

                CurrentEmail.Text = CurrentUser.Email;
                EmailBox.Text = CurrentUser.EmailWaitingforVerification;
                ConfirmEmailBox.Text = "";
                
			}
        }

		

        protected void saveButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                UserService.UpdateEmailAddress(CurrentUser,EmailBox.Text);

                if (null != UserEmailSaved)
                {
                    var msg = "Dear User, A verification email has been sent to " + EmailBox.Text +
                              ". Your new email address will be activated once you verify it.";
                    UserEmailSaved(CurrentUser,msg);
                }
            }
        }

		

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}