﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Message
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        protected override void OnInitComplete(EventArgs e)
        {
			if (User.IsInRole(Role.SEEKER_ROLE) || User.IsInRole(Role.INFLUENCER_ROLE))
            {
                messageTabPages.Views.Remove(messageTabPages.FindControl("ApprovedScholarshipsTabPage"));
                approvedTabHead.Visible = false;
            }
            base.OnInitComplete(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.Messages);

            if (User.IsInRole(Role.PROVIDER_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for notifications of scholarship status, scholarship applications and awards, and requests from other scholarship organizations. This inbox is shared between all your organizations users. Actions done by one user impact the other user's messages.";

            else if (User.IsInRole(Role.INTERMEDIARY_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for requests from other scholarship organizations, and notifications of scholarship status, applications and awards. This inbox is shared between all your organizations users. Actions done by one user impact the other user's messages.";
            else
                lblPageText.Visible = false;
        }

        protected  void messageTabPages_OnActiveViewChanged(object sender,EventArgs e)
        {
            var viewIndex = messageTabPages.ActiveViewIndex;
            
            switch (viewIndex)
            {
                case 0: //InboxView
                    
                    AddInboxList(LoadMessageListControl() );
                    break;
                case 1: //ArchivedView
                     var archivecontrol = LoadMessageListControl();
                    archivecontrol.Archived = true;
                    AddInboxList(archivecontrol);
                    break;
                case 2: //SentView
                    var sentcontrol = (SentMessageList)LoadControl("~/Common/SentMessageList.ascx");
                    sentcontrol.LinkTo = "~/Message/Show.aspx";
                    var view = messageTabPages.GetActiveView();
                    view.Controls.Add(sentcontrol); 
                    break;
                case 3: //ApprovedScholarhsipView
                    var approvedcontrol = LoadMessageListControl();
                    approvedcontrol.ScholarshipApprovals = true;
                    AddInboxList(approvedcontrol);
                    break;
                 default:
                    throw new NotSupportedException("Message View not supported.");
                   
            }

            
        }

        private MessageList LoadMessageListControl()
        {
            var control = (MessageList)LoadControl("~/Common/MessageList.ascx");
            control.LinkTo = "~/Message/Show.aspx";
            return control;
        }

        private void AddInboxList(UserControl control)
        {
            var view = messageTabPages.GetActiveView();
            view.Controls.Add(control); 
                
        }
    }
}