<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Register" Title="Register as a Scholarship Seeker" %>
<%@ Import Namespace="ScholarBridge.Domain"%>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Src="~/Common/CaptchaControl.ascx" tagname="CaptchaControl" tagprefix="sb" %>
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .large-label-size > label 
        {
            width: 290px !important;
        }
        .createpassword-label-size
        {
            width: 290px !important;
            font-style:italic !important;
            font-weight:normal !important;
        }
    </style>
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">


<asp:MultiView runat="server" ID="CreateUserWizard" ActiveViewIndex="0">
    <asp:View ID="DataEntryView" runat="server"> 
            <div class="form-iceland large-label-size">               
                <h1><asp:Literal ID="pageTitle" runat="server" Text="Seeker Registration"/></h1>
                <p><span class="requiredAttributeIndicator">*</span> Indicates a required field.</p>
                <label for="seekerTypeList">I am a (please select one):<span class="requiredAttributeIndicator">*</span></label>
                    <asp:RadioButtonList ID="seekerTypeList" runat="server" class="control-set" />
                    <asp:requiredfieldvalidator ID="seekerTypeRequiredValidator" runat="server" ErrorMessage="Please select an option" ControlToValidate="seekerTypeList"></asp:requiredfieldvalidator>
                    <sb:CoolTipInfo ID="CoolTipInfo1" runat="Server" Content='"Students" will be able to apply for scholarships.  Select one of the other roles if you are not a student but want to create a profile, see matches, or demonstrate the benefits of theWashBoard.org' />
                <br />
                <br />
                <label for="FirstName">First Name:<span class="requiredAttributeIndicator">*</span></label>
                <asp:TextBox ID="FirstName"  Columns="40" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="firstNameRequired" ControlToValidate="FirstName" runat="server" Display="Dynamic" ErrorMessage="First name is required"></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="MiddleName">Middle Name:</label>
                <asp:TextBox ID="MiddleName"   Columns="40" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"  ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="LastName">Last Name:<span class="requiredAttributeIndicator">*</span></label>
                <asp:TextBox ID="LastName"   Columns="40" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="lastNameRequired" ControlToValidate="LastName" Display="Dynamic" runat="server" ErrorMessage="Last name is required" ></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="UserName">Email Address:<span class="requiredAttributeIndicator">*</span></label>
                <asp:TextBox ID="UserName"   Columns="40" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="emailRequired" ControlToValidate="UserName" Display="Dynamic" runat="server" ErrorMessage="Email address is required"></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
                <br />
                <label for="ConfirmEmail">Confirm Email Address:<span class="requiredAttributeIndicator">*</span></label>
                <asp:TextBox ID="ConfirmEmail"   Columns="40" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ErrorMessage="Confirm Email is required" ID="confirmEmailRequired" ControlToValidate="ConfirmEmail" runat="server" ></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" 
                    ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address."/>
                 
                <br />
                    <label for="Password">
                        Create Password:<span class="requiredAttributeIndicator">*</span></label>
                <asp:TextBox ID="Password"   Columns="40" runat="server" TextMode="Password"  onblur="$('#COOLTIP').hide();" onfocus="CoolTip_Show(this.id, 'Password Requirements', 'Your password must be at least 7 characters with at least one special character or number, Example: p@sword', '')"></asp:TextBox>
                <asp:RequiredFieldValidator ID="passwordRequired" Display="Dynamic"  ControlToValidate="Password" runat="server" ErrorMessage="A password is required and must be at least 7 characters with at least one special character or number."></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="ConfirmPassword">Confirm Password:<span class="requiredAttributeIndicator">*</span></label>
                <asp:TextBox ID="ConfirmPassword"   Columns="40" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" Display="Dynamic" runat="server" ControlToValidate="ConfirmPassword"
                    ErrorMessage="Confirm Password is required." ToolTip="Please re-enter your new password here."></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                    ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                    ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                <br />
                
                
                <sbCommon:AnchorButton ID="RegisterButton" runat="server"  Text="Next"
                                        onclick="RegisterButton_Click"/>
                <br />
                <p class="GeneralBodyText">Please add noreply@theWashBoard.org to your safe senders list to avoid emails being identified as junk email.</p>
            </div>
        
    </asp:View>
    <asp:View ID="CaptchaView" runat="server">
        <div      >
        <h1><asp:Literal ID="Literal1" runat="server" Text="Seeker Registration"/></h1>
        <h2>Word Verification</h2>
                <sb:CaptchaControl ID="CaptchaControl1" runat="server" />
                <br />
         <br />
              <sbCommon:AnchorButton ID="CompleteRegistration" runat="server" Text="Complete Registration" OnClick="CompleteRegistration_Click" CausesValidation="true" />
      
                <br />
                 <br />
                <p class="GeneralBodyText">Please add noreply@theWashBoard.org to your safe senders list to avoid emails being identified as junk email.</p>
    </div>
    </asp:View>
    
    <asp:View ID="CompletionView" runat="server">
                <h2><b>Thanks for registering with us</b></h2>
                <p><h3>An email has been sent to the email address you provided with instructions to complete the registration process. Please follow the instructions in the e-mail to enable your account.</h3></p>
                <p style="text-align:center;">
                    <table>
                        <tr>
                            <td align="center">
                                <span title="Press OK to proceed to Login Page"><sbCommon:AnchorButton ID="okNavigateToLogin" runat="server" 
                                                    OnClick="okNavigateToLogin_Click"
                                                    /></span>
                            </td>
                        </tr>
                    </table>
                </p>
    </asp:View>

</asp:MultiView>

</asp:Content>
