﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;
namespace ScholarBridge.Web.Seeker.Scholarships
{
    public partial class Show : Page
    {
        public IMatchService MatchService { get; set; }
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["id"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsInPrintView())
                Response.Redirect("~/Seeker/Scholarships/show.aspx?id="+ScholarshipId.ToString()+"&print=true" );

            UserContext.EnsureSeekerIsInContext();
            var currentScholarship = ScholarshipService.GetById(ScholarshipId);
            if (null != currentScholarship)
            {
                showScholarship.Scholarship = currentScholarship;
                scholarshipTitleStripeControl.UpdateView(currentScholarship);
            }
            sendToFriendBtn.NavigationUrl =
                ResolveUrl("~/SendToFriend.aspx?popup=true&id={0}".Build(ScholarshipId));
        }

        

        protected void AddtoMyScholarshipBt_Click(object sender, EventArgs e)
        {
            
            var scholarship = ScholarshipService.GetById(ScholarshipId);
            var seeker = UserContext.CurrentSeeker;
            var match = MatchService.GetMatch(seeker, ScholarshipId);

            if (!(match == null))
            {
                if (match.MatchStatus == MatchStatus.Saved ||match.MatchStatus ==MatchStatus.Submitted )
                {
                    var msg = "Scholarship {0} is already exists in My Scholarships of Interest".Build(scholarship.Name);
                    ClientSideDialogs.ShowAlertNative(msg, "Add to My Scholarships of Interest.");
                    return;
                } 
              
               MatchService.SaveMatch(seeker,scholarship.Id);
            } 
            else
            {
                match = new Match
                {
                    Seeker = seeker,
                    Scholarship = scholarship,
                    MatchStatus = MatchStatus.Saved,
                    LastUpdate = new ActivityStamp(UserContext.CurrentUser),
                };
                MatchService.InsertCustomMatch(match);

            }
            ClientSideDialogs.ShowAlertNative("Scholarship added to My Scholarships of Interest.", "Add to My Scholarships of Interest.");
        }

    }
}
