﻿using System;
using System.Collections.Specialized;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Security;


namespace ScholarBridge.Web.Seeker
{
    public partial class Register : Page
    {
        public IUserService UserService { get; set; }
        public ISeekerService SeekerService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            HaltRegistration.CheckRegistrationHalted();
			if (!Page.IsPostBack)
			{
				
				RegisterButton.CausesValidation = true;
				okNavigateToLogin.Text = "OK";
				okNavigateToLogin.ToolTip = "Press OK to proceed to Login Page";
				seekerTypeList.DataSource = typeof(SeekerType).GetKeyValue();
				seekerTypeList.DataTextField = "Value";
				seekerTypeList.DataValueField = "Key";
				seekerTypeList.DataBind();
			}
        }

        private User NewUser
        {
            get
            {
                if (Session["NewUser"] != null)
                    return (User)Session["NewUser"];
                else
                    return null;


            }
            set
            {
                if (Session["NewUser"] == null)
                    Session.Add("NewUser", value);
                else
                    Session["NewUser"] = value;
            }

        }

        private Domain.Seeker NewSeeker
        {
            get
            {
                if (Session["NewSeeker"] != null)
                    return (Domain.Seeker )Session["NewSeeker"];
                else
                    return null;


            }
            set
            {
                if (Session["NewSeeker"] == null)
                    Session.Add("NewSeeker", value);
                else
                    Session["NewSeeker"] = value;
            }

        }

        protected void RegisterButton_Click(object sender, EventArgs e)
    	{
			Page.Validate("CreateUserWizard1");
			if (!Page.IsValid)
			{
                CaptchaControl1.ClearGuess();
                return;
			}

			var provider = new SpiderMembershipProvider();
			provider.Initialize(string.Empty, new NameValueCollection());
			User user = provider.CreateUserInstance
				(
					UserName.Text, Password.Text, UserName.Text, null, null, false
				);

			if (user == null)
			{
				switch (provider.CreateUserStatus)
				{
					case MembershipCreateStatus.InvalidEmail:
                        UserNameValidator.ErrorMessage = "Email Address is Invalid";
						UserNameValidator.IsValid = false;
						return;
					case MembershipCreateStatus.DuplicateEmail:
                    case MembershipCreateStatus.DuplicateUserName:
						UserNameValidator.ErrorMessage = "Email Address is already in use by another user.  Please choose another Email.";
						UserNameValidator.IsValid = false;
						return;
					default:
						throw new ApplicationException("Unhandled User Creation status event: " + provider.CreateUserStatus);
				}
			}
			user.Name.FirstName = FirstName.Text;
			user.Name.MiddleName = MiddleName.Text;
			user.Name.LastName = LastName.Text;
           
			var seeker = new Domain.Seeker { User = user, SeekerType = (SeekerType)(int.Parse(seekerTypeList.SelectedValue))};
            
            NewSeeker = seeker;
    		 
			CreateUserWizard.ActiveViewIndex = 1; // CaptchaView
		}
        protected void CompleteRegistration_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                CaptchaControl1.ClearGuess();

                return;
            }
            CreateSeekerRecord();

        }
        private void CreateSeekerRecord()
        {
            SeekerService.SaveNew(NewSeeker);
            CreateUserWizard.ActiveViewIndex = 2; // CompletionView
        }
    	protected void okNavigateToLogin_Click(object sender, EventArgs e)
    	{
    		Response.Redirect(LinkGenerator.GetFullLinkStatic("/seeker/anonymous.aspx"), true);
    	}
    }
}
