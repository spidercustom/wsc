﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.Basics" %>
<%@ Register TagPrefix="sb" TagName="LookupItemCheckboxList" Src="~/Common/LookupItemCheckboxList.ascx" %>
<%@ Register Tagprefix="sb" tagname="LookupDialog" src="~/Common/Lookup/LookupDialog.ascx"   %>    
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
 <%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>
	 
<h3>My Profile – Basics</h3>
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">My Name:<sb:CoolTipInfo ID="CoolTipInfo1" Content="My Name can only be updated in My Profile" runat="server" />
       </p>
        <label for="FFirstNameControl">First:</label>
        <asp:Label ID="FirstNameControl" runat="server" />
        
         <br />
        <label for="MidNameControl">Middle:</label>
        <asp:Label ID="MidNameControl" runat="server" />
         <br />
        <label for="LastNameControl">Last:</label>
        <asp:Label ID="LastNameControl" runat="server" />
         <br />
        <hr />
        <p class="form-section-title">My Permanent Address:<sb:CoolTipInfo ID="CoolTipInfo2" Content="My Permanent Address can only be updated in My Profile" runat="server" />
       </p>
        <label for="AddressLine1Control">Street Line 1:</label>
        <asp:Label ID="AddressLine1Control" runat="server" />
             <br />
        <label for="AddressLine2Control">Street Line 2:</label>
       <asp:Label ID="AddressLine2Control" runat="server" />
         <br />
       <label for="StateControl">State:</label>
        <asp:Label ID="StateControl" runat="server" />
        <br />
        <label for="CityControl">City:</label>
        <asp:Label ID="CityControl" runat="server" />
     
         
        <br />
        <label for="CountyControl">County:</label>
       <asp:Label ID="CountyControl" runat="server" />
              
        <br />
        <label for="ZipControl">Postal Code:</label>
        <asp:Label ID="ZipControl" runat="server" />
         <br />
        <label for="PhoneControl">Phone:</label>
        <asp:Label ID="PhoneControl" runat="server" />
             <br />
        <label for="MobilePhoneControl">Mobile Phone:</label>
         <asp:Label ID="MobilePhoneControl" runat="server" />
         <br />
    </div>
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">  
        <p class="form-section-title">My Email:<sb:CoolTipInfo ID="CoolTipInfo3" Content="My Email can only be updated in My Profile" runat="server" /></p>
        <label>Email</label>
        <asp:Label ID="EmailControl" runat="server" />
       
        <br />
        <hr />
        
        <p class="form-section-title">My Background:</p>       
        <label for="DobControl">Birthdate</label>
        <asp:TextBox ID="DobControl" runat="server" CssClass="date"/>
        <asp:RangeValidator ID="dobRangeValid" runat="server" ControlToValidate="DobControl" MinimumValue="1/1/1900" Type="Date" Display="Dynamic" ErrorMessage="How old are you?" />
        <br />

        <label for="GenderButtonList">My Gender:</label>
        <asp:RadioButtonList ID="GenderButtonList" runat="server">
            <asp:ListItem Text="Male" Value="2"></asp:ListItem>
            <asp:ListItem Text="Female" Value="4"></asp:ListItem>
        </asp:RadioButtonList>
        <br />

        <label for="ReligionCheckboxList">My Religion/Faith:</label>
        <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" CheckBoxListWidth="210px" />
        <label for="OtherReligion">Other Religion?:</label>
        <asp:TextBox ID="OtherReligion" runat="server" />
        <elv:PropertyProxyValidator ID="OtherReligionValidator" runat="server" ControlToValidate="OtherReligion" PropertyName="ReligionOther" SourceTypeName="ScholarBridge.Domain.Application"/>
        <br />

        <label for="EthnicityControl">My Heritage:</label>
        <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" CheckBoxListWidth="210px" />
        <label for="OtherEthnicity">Other Ethnicity?</label>
        <asp:TextBox ID="OtherEthnicity" runat="server" />
        <elv:PropertyProxyValidator ID="OtherEthnicityValidator" runat="server" ControlToValidate="OtherEthnicity" PropertyName="EthnicityOther" SourceTypeName="ScholarBridge.Domain.Application"/>
   </div>
</div>
