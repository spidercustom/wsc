﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.PressRoom.Show" Title="Admin" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Import Namespace="ScholarBridge.Web.Extensions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
   <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderGeneralImage.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderGeneralImage.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<sb:PrintView id="PrintViewControl" runat="server" /> 

<h2>Press Room</h2>
<div>

<asp:Label ID="errorMessage" runat="server" Text="Article not found." Visible="false" CssClass="errorMessage"/>
<h3><asp:Literal ID="DateControl" runat="server" /> - <asp:Literal ID="TitleControl" runat="server" /></h3>

<pre style="font-family:Arial; font-size:11px; margin:10px 0px;">
<asp:Label ID="BodyControl" runat="server" CssClass="word_wrap"/>
</pre>

</div>
<div id="linkarea" class="exclude-in-print"  runat="server" visible="<% #!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this) %>">

   <ul class="pageNav">
      <li><asp:HyperLink ID="PressRoomLnk" runat="server" NavigateUrl="~/PressRoom/">Return to Press Room</asp:HyperLink></li>
      <li><asp:HyperLink ID="HomeLnk" runat="server" NavigateUrl="~/">Home</asp:HyperLink></li>
  </ul>
</div>            


</asp:Content>
