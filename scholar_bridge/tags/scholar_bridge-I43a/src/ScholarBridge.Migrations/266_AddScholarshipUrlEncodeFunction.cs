﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(266)]
	public class AddScholarshipUrlEncodeFunction
		: Migration
	{

		public override void Up()
		{
			AddFunction();

		}

		private void AddFunction()
		{
			Database.ExecuteNonQuery(dropFunctionQuery);
			Database.ExecuteNonQuery(createFunctionQuery);
		}

		public override void Down()
		{
			Database.ExecuteNonQuery(dropFunctionQuery);
		}

		private const string dropFunctionQuery =
			@"
if exists (select * from sys.all_objects where name = 'ScholarshipURLEncode')
begin
	drop function ScholarshipURLEncode
end
";
		private const string createFunctionQuery =
			@"
/* 
	ScholarshipURLEncode - Encode strings used in building Scholarship List & Details urls.
	
	select dbo.ScholarshipURLEncode('asdf...~ fd''sa 1234') yields 'asdf   %+fd%sa+1234'
*/
CREATE FUNCTION dbo.ScholarshipURLEncode(@url varchar(1024))
RETURNS varchar(3072)

BEGIN
    DECLARE @count int, @c char(1), @int int, @i int, @urlReturn varchar(3072)
    SET @count = Len(@url)
    SET @i = 1
    SET @urlReturn = ''    
    WHILE (@i <= @count)
     BEGIN
        SET @c = substring(@url, @i, 1)	-- walk thru the string a character at a time
        set @int = cast(CAST(@c as varbinary(2)) as int) -- convert the character to an int 
        
        declare @newChar varchar(3)
        select @newChar = 
			case
				when @int = 130 then  'e'
				when @c like '[.,]' then ''
				when @c LIKE '[-A-Za-z0-9()*-_!]' then @c
				when @c = ' ' then '+'
				else '%' + SUBSTRING(sys.fn_varbintohexstr(CAST(@c as varbinary(max))),3,2)
			end
		set @urlReturn = @urlReturn + @newChar
        SET @i = @i +1
     END
    RETURN @urlReturn
 END
";
	}

}
