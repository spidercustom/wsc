using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(10)]
    public class AddUserProvider : Migration
    {
        protected const string TABLE_NAME = "SB_UserProviderRT";

        protected static readonly string[] COLUMNS = new[] { "UserId", "ProviderId" };
        protected static readonly string FK_ROLES = string.Format("FK_{0}_Provider", TABLE_NAME);
        protected static readonly string FK_USERS = string.Format("FK_{0}_User", TABLE_NAME);

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKey),
                              new Column(COLUMNS[1], DbType.Int32, ColumnProperty.PrimaryKey)
                );

            Database.AddForeignKey(FK_USERS, TABLE_NAME, COLUMNS[0], AddUsers.TABLE_NAME, AddUsers.PRIMARY_KEY_COLUMN);
            Database.AddForeignKey(FK_ROLES, TABLE_NAME, COLUMNS[1], AddProvider.TABLE_NAME, AddProvider.PRIMARY_KEY_COLUMN);
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_ROLES);
            Database.RemoveForeignKey(TABLE_NAME, FK_USERS);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}