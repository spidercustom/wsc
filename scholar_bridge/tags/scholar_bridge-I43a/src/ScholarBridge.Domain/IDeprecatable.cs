namespace ScholarBridge.Domain
{
    public interface IDeprecatable
    {
        bool Deprecated { get; set; }
    }
}