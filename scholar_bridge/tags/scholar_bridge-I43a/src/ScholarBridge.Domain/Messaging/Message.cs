using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Resources;

namespace ScholarBridge.Domain.Messaging
{
    public class Message : IMessage
    {
        public Message()
        {
            InitializeMembers();
        }

        private void InitializeMembers()
        {
            IsRead = false;
            IsArchived = false;
            To = new MessageAddress();
            From = new MessageAddress();
            Date = DateTime.UtcNow;
        }

        public virtual int Id { get; set; }

        public virtual MessageAddress To { get; set; }
        public virtual MessageAddress From { get; set; }

        public virtual Message InResponseTo { get; set; }
        public virtual MessageType MessageTemplate { get; set; }

		[StringLengthValidator(1, 100, MessageTemplateResourceName = "NamedStringLengthBetweenMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Subject { get; set; }
        public virtual string Content { get; set; }
        public virtual DateTime Date { get; set; }

        public virtual bool IsRead { get; set; }
        public virtual bool IsArchived { get; set; }
        public virtual MessageAction ActionTaken { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }
    }
}