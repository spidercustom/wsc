﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.Resources;

namespace ScholarBridge.Domain.Location
{
    [Serializable]
    public class State : ILookup
    {
    	public const string WASHINGTON_STATE_ID = "WA";

        [StringLengthValidator(2,2)]
		[StringLengthValidator(2, 2, MessageTemplate = "State abbreviation must be 2 characters.")]
		public virtual string Abbreviation { get; set; }

		[StringLengthValidator(0, 30, MessageTemplateResourceName = "NamedStringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Name { get; set; }


        #region ILookup Members

        object ILookup.Id
        {
            get
            {
                return Abbreviation;
            }
            set
            {
                Abbreviation = value.ToString();
            }
        }

        int ILookup.GetIdAsInteger()
        {
            throw new NotImplementedException();
        }

        string ILookup.GetIdAsString()
        {
            return Abbreviation;
        }

        string ILookup.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }

        string ILookup.Description
        {
            get
            {
                return Name;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        ActivityStamp ILookup.LastUpdate
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IDeprecatable Members

        bool IDeprecatable.Deprecated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}