using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Resources;

namespace ScholarBridge.Domain
{
    public class Article 
    {
        public virtual int Id { get; set; }
        [NotNullValidator]
        public virtual DateTime PostedDate { get; set; }
		[StringLengthValidator(1, 250, MessageTemplateResourceName = "NamedStringLengthBetweenMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Title { get; set; }
        [NotNullValidator]
		[StringLengthValidator(1, 4000, MessageTemplateResourceName = "NamedStringLengthBetweenMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Body { get; set; }
      
        public virtual ActivityStamp LastUpdate { get; set; }

        
        
    }
}