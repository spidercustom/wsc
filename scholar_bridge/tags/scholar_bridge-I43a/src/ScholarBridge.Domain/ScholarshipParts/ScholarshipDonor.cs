﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Contact;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipDonor : ICloneable
    {
        public ScholarshipDonor()
        {
            
        }

         

		[StringLengthValidator(0, 250, MessageTemplate = "Donor Name cannot exceed 250 characters.")]
        public virtual string Name { get; set; }
        [StringLengthValidator(0, 1000, MessageTemplate = "Donor Profile cannot exceed 1000 characters.")]
       
        public virtual string Profile { get; set; }

        // ASP.NET passes an empty string from a form in the PropertyProxyValidator
        // The empty string fails the EmailValidator (where a null would have passed since it's not checked)
        // which means we have to do this for any optional, but validated fields to get PropertyProxyValidator
        // to work (and we would have to have TextBox.Text check for empty string and convert them to nulls on data binding out of the form)
        // Yes, this is insane.
        
        public virtual object Clone()
        {
            return MemberwiseClone();
        }
    }
}
