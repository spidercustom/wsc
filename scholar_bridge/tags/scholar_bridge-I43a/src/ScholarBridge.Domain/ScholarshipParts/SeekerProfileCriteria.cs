﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class SeekerProfileCriteria : ICloneable, IMatchLocationCriteria
    {
        public SeekerProfileCriteria()
        {
            InitializeMembers();
        }

        private void InitializeMembers()
        {
            Attributes = new ScholarshipAttributeCollection<SeekerProfileAttribute>();
            Colleges = new List<College>();
            Ethnicities = new List<Ethnicity>();
            Religions = new List<Religion>();
            States = new List<State>();
            Counties = new List<County>();
            Cities = new List<City>();  
            SchoolDistricts = new List<SchoolDistrict>();
            HighSchools = new List<HighSchool>();
            AcademicAreas = new List<AcademicArea>();
            Careers = new List<Career>();
            Hobbies = new List<SeekerHobby>();
            Organizations = new List<SeekerMatchOrganization>();
            Companies  = new List<Company >();
            CommunityServices = new List<CommunityService>();
            AffiliationTypes = new List<AffiliationType>();
            Sports = new List<Sport>();
            Clubs = new List<Club>();
            WorkTypes = new List<WorkType>();
            WorkHours = new List<WorkHour>();
            ServiceTypes = new List<ServiceType>();
            ServiceHours = new List<ServiceHour>();
            ClassRanks  = new List<ClassRank >();

            //We don't want to set GPA, ClassRank, SAT Score and ACTScore here
            //As they can be null if user hasn't set criteria on it.
            //Basically, don't uncomment following lines
            //{
            //	GPA = new RangeCondition<int>();
            //	ClassRank = new RangeCondition<int>();
            //	SATScore = new SatScore();
            //	ACTScore = new ActScore();
            //}

        }

        public virtual ScholarshipAttributeCollection<SeekerProfileAttribute> Attributes { get; protected set; }
        public virtual StudentGroups StudentGroups { get; set; }
        public virtual SchoolTypes SchoolTypes { get; set; }
        public virtual AcademicPrograms AcademicPrograms { get; set; }
        public virtual SeekerStatuses SeekerStatuses { get; set; }
        public virtual ProgramLengths ProgramLengths { get; set; }
        public virtual IList<College> Colleges { get; protected set; }
        public virtual bool FirstGeneration { get; set; }
        public virtual IList<Ethnicity> Ethnicities { get; protected set; }
        public virtual IList<Religion> Religions { get; protected set; }
        public virtual Genders Genders { get; set; }
        public virtual IList<State> States { get; protected set; }
        public virtual IList<County> Counties { get; protected set; }
        public virtual IList<City> Cities { get; protected set; }
        public virtual IList<SchoolDistrict> SchoolDistricts { get; protected set; }
        public virtual IList<HighSchool> HighSchools { get; protected set; }
        public virtual IList<AcademicArea> AcademicAreas { get; protected set; }
        public virtual IList<Career> Careers { get; protected set; }
        public virtual IList<SeekerHobby> Hobbies { get; protected set; }
        public virtual IList<SeekerMatchOrganization> Organizations { get; protected set; }
        public virtual IList<Company> Companies { get; protected set; }
        public virtual IList<CommunityService> CommunityServices { get; protected set; }
        public virtual IList<AffiliationType> AffiliationTypes { get; protected set; }
        public virtual IList<Sport> Sports { get; protected set; }
        public virtual IList<Club> Clubs { get; protected set; }
        public virtual IList<WorkType> WorkTypes { get; protected set; }
        public virtual IList<WorkHour> WorkHours { get; protected set; }
        public virtual IList<ServiceType> ServiceTypes { get; protected set; }
        public virtual IList<ServiceHour> ServiceHours { get; protected set; }
        public virtual RangeCondition<double?> GPA { get; set; }
        public virtual IList<ClassRank > ClassRanks { get; protected set; }
        public virtual SatScore SATScore { get; set; }
        public virtual ActScore ACTScore { get; set; }
        public virtual bool Honors { get; set; }
        public virtual CollegeType CollegeType { get; set; }
        
        public virtual bool? APCreditsEarned { get; set; }

        public virtual bool? IBCreditsEarned { get; set; }

        public virtual IMatchLocationCriteria MatchLocationCriteria
        {
            get { return this; }
        }

        public virtual object Clone()
        {
            var result = (SeekerProfileCriteria) MemberwiseClone();
            result.Attributes = (ScholarshipAttributeCollection<SeekerProfileAttribute>) Attributes.Clone();
            result.AcademicAreas = new List<AcademicArea>(AcademicAreas);
            result.AffiliationTypes = new List<AffiliationType>(AffiliationTypes);
            result.Careers = new List<Career>(Careers);
            result.Cities = new List<City>(Cities);
            result.Clubs = new List<Club>(Clubs);
            result.Colleges = new List<College>(Colleges);
            result.CommunityServices = new List<CommunityService>(CommunityServices);
            result.Counties = new List<County>(Counties);
            result.Ethnicities = new List<Ethnicity>(Ethnicities);
            result.HighSchools = new List<HighSchool>(HighSchools);
            result.Hobbies = new List<SeekerHobby>(Hobbies);
            result.Organizations = new List<SeekerMatchOrganization>(Organizations);
            result.Companies = new List<Company >(Companies );
            result.Religions = new List<Religion>(Religions);
            result.SchoolDistricts = new List<SchoolDistrict>(SchoolDistricts);
            result.ServiceHours = new List<ServiceHour>(ServiceHours);
            result.ServiceTypes = new List<ServiceType>(ServiceTypes);
            result.Sports = new List<Sport>(Sports);
            result.States = new List<State>(States);
            result.WorkHours = new List<WorkHour>(WorkHours);
            result.WorkTypes = new List<WorkType>(WorkTypes);
            result.Colleges=new List<College>(Colleges);
            result.CollegeType = CollegeType;
            result.FirstGeneration = FirstGeneration;
            result.Genders = Genders;
            result.MatchLocationCriteria.State = State;
            result.Organizations=new List<SeekerMatchOrganization>(Organizations);
            result.Companies=new List<Company>(Companies);

            result.Attributes =(ScholarshipAttributeCollection<SeekerProfileAttribute>) Attributes.Clone();
            
            result.StudentGroups = StudentGroups;
            result.SchoolTypes = SchoolTypes;
            result.AcademicPrograms = AcademicPrograms;
            result.SeekerStatuses = SeekerStatuses;
            result.ProgramLengths = ProgramLengths;
            result.GPA = GPA;
            result.SATScore = SATScore;
            result.ACTScore = ACTScore;
            result.ClassRanks=new List<ClassRank>(ClassRanks);
            result.Honors = Honors;
            result.APCreditsEarned = APCreditsEarned;
            result.IBCreditsEarned = IBCreditsEarned;

            return result;
        }

        #region IMatchLocationCriteria Members

        public virtual State State
        {
            get
            {
                if (States.Count > 0)
                    return States[0];

                return null;
            }
            set
            {
                States.Clear();
                States.Add(value);
                //if (!States.Any(o => o.Abbreviation.Equals(value.Abbreviation)))
                //{
                //    States.Clear();
                //    States.Add(value);
                //}
            }
        }

        public virtual StateDependentLocation StateDependentLocation
        {
            get
            {
                if (StateDependentLocations == null || StateDependentLocations.Count == 0)
                    return StateDependentLocation.None;
                if (StateDependentLocations[0] is County)
                    return StateDependentLocation.County;
                if (StateDependentLocations[0] is City)
                    return StateDependentLocation.City;
                if (StateDependentLocations[0] is SchoolDistrict)
                    return StateDependentLocation.SchoolDistrict;
                if (StateDependentLocations[0] is HighSchool)
                    return StateDependentLocation.HighSchool;

                throw new NotSupportedException();
            }
        }

        public virtual IList<ILookup> StateDependentLocations
        {
            get
            {
                if (Counties.Count > 0)
                    return Counties.Cast<ILookup>().ToList();
                if (Cities.Count > 0)
                    return Cities.Cast<ILookup>().ToList();
                if (SchoolDistricts.Count > 0)
                    return SchoolDistricts.Cast<ILookup>().ToList();
                if (HighSchools.Count > 0)
                    return HighSchools.Cast<ILookup>().ToList();

                return null;
            }
        }

        public void ResetStateDependentLocations(IList<ILookup> value)
        {
            Counties.Clear();
            Cities.Clear();
            SchoolDistricts.Clear();
            HighSchools.Clear();
            if (value == null || value.Count == 0)
                return;

            if (value[0] is County)
                value.Cast<County>().ForEach(o => Counties.Add(o));
            else if (value[0] is City)
                value.Cast<City>().ForEach(o => Cities.Add(o));
            else if (value[0] is County)
                value.Cast<SchoolDistrict>().ForEach(o => SchoolDistricts.Add(o));
            else if (value[0] is HighSchool)
                value.Cast<HighSchool>().ForEach(o => HighSchools.Add(o));
            else
                throw new NotSupportedException();
        }

        #endregion
    }
}
