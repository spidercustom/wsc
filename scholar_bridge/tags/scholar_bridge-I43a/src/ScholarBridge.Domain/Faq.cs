﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Resources;

namespace ScholarBridge.Domain
{
	public enum FaqType
	{
		// a FAQ type indicates where in the Help page the question is displayed
		General,
		Seeker,
		Provider
	}

	public class Faq
	{
		public virtual int Id { get; set; }
		[NotNullValidator]
		[StringLengthValidator(1, 250, MessageTemplateResourceName = "NamedStringLengthBetweenMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Question { get; set; }
		[StringLengthValidator(1, 4000, MessageTemplateResourceName = "NamedStringLengthBetweenMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Answer { get; set; }

		public virtual FaqType FaqType { get; set; }

		public virtual ActivityStamp LastUpdate { get; set; }
	}
}
