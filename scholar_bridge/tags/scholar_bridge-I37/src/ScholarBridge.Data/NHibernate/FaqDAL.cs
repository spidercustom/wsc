﻿using NHibernate.Criterion;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
	public class FaqDAL : AbstractDAL<Faq>, IFaqDAL
	{
		public Faq FindById(int id)
		{
			return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Faq>();
		}


		public Faq Save(Faq faq)
		{
			return faq.Id < 1 ?
				Insert(faq) :
				Update(faq);

		}
	}
}