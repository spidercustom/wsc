using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>MessageType</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class FaqTypeType : EnumStringType
    {
        public FaqTypeType()
            : base(typeof(FaqType), 50)
        {
        }
    }
}