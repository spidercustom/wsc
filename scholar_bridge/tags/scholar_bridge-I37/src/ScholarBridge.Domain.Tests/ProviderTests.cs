using System;
using NUnit.Framework;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class ProviderTests
    {
        [Test]
        public void AddingAdminUserAddsUserToUsersCollection()
        {
            var p = new Provider();

            Assert.AreEqual(0, p.ActiveUsers.Count);
            p.AdminUser = new User {Id = 1};

            CollectionAssert.IsNotEmpty(p.ActiveUsers);
            Assert.AreEqual(1, p.ActiveUsers.Count);

            p.AdminUser = new User {Id = 1};
            Assert.AreEqual(1, p.ActiveUsers.Count);
        }

        [Test]
        public void CanAppendAdminNotesToProvider()
        {
            var p = new Provider();
            var u = new User
                        {
                            Name =
                                {
                                    FirstName = "TestFirst",
                                    LastName = "TestLast"
                                }
                        };

            p.AppendAdminNotes(u, "This is a test note.");
            StringAssert.Contains("-- TestFirst TestLast", p.AdminNotes.ToStringTableFormat());
            StringAssert.Contains("This is a test note.", p.AdminNotes.ToString());
        }

        [Test]
        public void adding_users_to_active_and_deleted_get_pooled_in_users()
        {
            var p = new Provider();
            var u1 = new User
                         {
                             Name =
                                 {
                                     FirstName = "TestFirst",
                                     LastName = "TestLast"
                                 }
                         };
            var u2 = new User
                         {
                             Name =
                                 {
                                     FirstName = "TestFirst",
                                     LastName = "TestLast"
                                 },
                             IsDeleted = true
                         };

            p.Users.Add(u1);
            p.Users.Add(u2);

            Assert.AreEqual(2, p.Users.Count);
            Assert.AreEqual(1, p.ActiveUsers.Count);
            Assert.AreEqual(1, p.DeletedUsers.Count);
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void CanNotAppendAdminNotesWithoutNotes()
        {
            var p = new Provider();
            p.AppendAdminNotes(new User(), null);

            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void CanNotAppendAdminNotesWithoutUser()
        {
            var p = new Provider();
            p.AppendAdminNotes(null, "This is a test note.");

            Assert.Fail();
        }
    }
}