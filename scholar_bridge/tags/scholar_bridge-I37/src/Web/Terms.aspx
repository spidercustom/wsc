﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Terms.aspx.cs" Inherits="ScholarBridge.Web.Terms" Title="Terms & Conditions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
      <div><asp:Image ID="Image3" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image4" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div class="form-iceland-container">
    <div class="form-iceland">
        <h2>Terms &amp; Conditions</h2>
        <p class="HighlightedTextMain">theWashBoard.org is a no-fee, advertising-free, and charitably-motivated community resource for both colleges and residents of Washington State.  We do not share or sell personal information.</p>

    <ul>
    <li><asp:HyperLink ID="privacyStatementLnk"  CssClass="GreenLink" runat="server" NavigateUrl="~/PrivacyStatement.aspx">Privacy Policy Statement</asp:HyperLink></li>
    <li><asp:HyperLink ID="seekerTermsLnk" runat="server"  CssClass="GreenLink" NavigateUrl="~/TermsOfUse.aspx">Terms Of Use</asp:HyperLink></li>
    <li><u style="text-decoration:none;">System Requirements</u></li>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                    <br />Internet Browsers Supported
                    <ul>
                        <li>Internet Explorer 7.0 and higher</li>
                        <li>Mozilla Firefox 2.0 and higher</li>
                        <li>Safari 3.1 and higher</li>
                    </ul>
                    <br />
                    Plug-ins
                    <ul>
                        <li><a href="http://get.adobe.com/reader/"  class="GreenLink" >Adobe Acrobat Reader</a></li>
                    </ul>
                </td>
            </tr>
        </table>
</ul>
    
    
    </div>
</div>    




</asp:Content>
