﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="ScholarBridge.Web.Help" Title="Help" %>
<%@ Import Namespace="ScholarBridge.Domain"%>
<%@ Register src="Common/ContactUs.ascx" tagname="ContactUs" tagprefix="uc1" %>
<%@ Register src="Common/FaqList.ascx" tagname="FaqList" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<br />
<style>
.GreenLinks a {
    color:#98c53c;
	text-decoration: none;
}
</style>
<h2>Help</h2>

<p>Support Hours: 8:00 am – 5:00 pm PST Monday thru Friday <br />
    Call 888-535-0747, option #8 or <A class="GreenLink" href='/ContactUs.aspx'>contact us online.</A>  
</p>

<h3>Frequently Asked Questions</h3>
<ul>
    <li>
        <a  class="GreenLink" href="#generalFAQ">General FAQ</a>
    </li>
    <li>
        <a class="GreenLink" href="#seekerFAQ">Seeker FAQ</a>
    </li>
    <li>
        <a class="GreenLink" href="#providerFAQ">Provider FAQ</a>
    </li>
</ul>
<div class="GreenLinks">
<br />
<a name="generalFAQ"><h4>General FAQ</h4></a><br />
    <uc2:FaqList ID="generalFaqList" runat="server"/>
   
<br />
<a name="seekerFAQ"><h4>Seeker FAQ</h4></a> <br />
    <uc2:FaqList ID="seekerFaqList" runat="server"/>
    
<br />
 <a name="providerFAQ"><h4>Provider FAQ</h4></a><br />
    <uc2:FaqList ID="providerFaqList" runat="server"/>
</div>
</asp:Content>
