﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicInformation.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.AcademicInformation" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemRadioButtonList.ascx" tagname="LookupItemRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerStudentType.ascx" tagname="SeekerStudentType" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerCollegeType.ascx" tagname="SeekerCollegeType" tagprefix="sb" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
 <style type="text/css">
        .large-label-size
        {
            width: 325px !important;
        }
        .large-label-size-checkbox
        {
            width: 280px !important;
        }
        
    </style>
<div class="form-iceland-container">
    
    <div class="form-iceland">
     <div class="form-iceland two-columns ">
     
     <p class="form-section-title">My Academic Information - High School</p>
    
        <label for="CurrentStudentGroupControl" class="large-label-size">What type of student are you currently?</label>
        <sb:SeekerStudentType id="CurrentStudentGroupControl" runat="server" />
        <br /><br />

        <label id="HighSchoolLabelControl" class="large-label-size" for="HighSchoolControl">High School you are currently attending or graduated from:</label>
        <br />
        <sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl" 
            OtherControl="HighSchoolOtherControl" OtherControlLabel="HighSchoolOtherLabelControl" ItemSource="HighSchoolDAL" Title="High School Selection" SelectionLimit="1" />
        <asp:TextBox ID="HighSchoolControl" ReadOnly="true" runat="server"></asp:TextBox>
        <br />
        <label id="HighSchoolOtherLabelControl" for="HighSchoolOtherControl">Other:</label>
        <asp:TextBox CssClass="othercontroltextarea" ID="HighSchoolOtherControl" runat="server" />
        <br />
        
        <asp:PlaceHolder ID="SchoolDistrictContainerControl" runat="server">
            <label id="SchoolDistrictLabelControl" for="SchoolDistrictControl">School District:</label>
            <br />
            <sb:LookupDialog ID="SchoolDistrictControlLookupDialogButton" runat="server" BuddyControl="SchoolDistrictControl" ItemSource="SchoolDistrictDAL" Title="School District Selection" SelectionLimit="1" 
                OtherControl="SchoolDistrictOtherControl" OtherControlLabel="SchoolDistrictOtherLabelControl"/>
            <asp:TextBox ID="SchoolDistrictControl" ReadOnly="true" runat="server"></asp:TextBox>
            
            <br />
            <label id="SchoolDistrictOtherLabelControl" for="SchoolDistrictOtherControl">Other:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="SchoolDistrictOtherControl" runat="server" />
            <br />
        </asp:PlaceHolder>
     
     </div>
     
     <div class="vertical-devider"></div>
     
     <div class="form-iceland two-columns">
     <p class="form-section-title">Academic Performance</p>
        <label for="GPAControl">GPA:</label>
        <sandTrap:NumberBox ID="GPAControl" runat="server" Precision="3" MinAmount="0" MaxAmount="5"/>
        <elv:PropertyProxyValidator ID="GPAValidator" runat="server" ControlToValidate="GPAControl" PropertyName="GPA" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
        <br />
        <label for="ClassRankControl">Class Rank:</label>
        <sb:LookupItemRadioButtonList id="ClassRankControl" runat="server" RadioButtonListWidth="250px" MultiColumn="true" LookupServiceSpringContainerKey="ClassRankDAL" ></sb:LookupItemRadioButtonList>
        <asp:CustomValidator ID="ClassRankControlValidator" runat="server"  ErrorMessage="Please select class rank."    />
         <br />
         <br />
        <label for="SATScoreCommulativeControl">SAT Score Cumulative:</label>
        <sandTrap:NumberBox ID="SATScoreCommulativeControl"  runat="server" Precision="0" MinAmount="0"/>
        <elv:PropertyProxyValidator ID="PropertyProxyValidator1" runat="server" ControlToValidate="SATScoreCommulativeControl" PropertyName="Commulative" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
        <br />
        
        <label for="SATWritingControl">&nbsp;&nbsp;&nbsp;&nbsp;Writing:</label>
        <sandTrap:NumberBox ID="SATWritingControl" runat="server" Precision="0" MinAmount="0"/>
        <elv:PropertyProxyValidator ID="SATWritingControlValidator" runat="server" ControlToValidate="SATWritingControl" PropertyName="Writing" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
        <br />
        <label for="SATReadingControl">&nbsp;&nbsp;&nbsp;&nbsp;Critical Reading:</label>
        <sandTrap:NumberBox ID="SATReadingControl" runat="server" Precision="0"  MinAmount="0"/>
        <elv:PropertyProxyValidator ID="SATReadingValidator" runat="server" ControlToValidate="SATReadingControl" PropertyName="CriticalReading" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
        <br />
        <label for="SATMathControl">&nbsp;&nbsp;&nbsp;&nbsp;Mathematics:</label>
        <sandTrap:NumberBox ID="SATMathControl" runat="server" Precision="0"  MinAmount="0"/>
        <elv:PropertyProxyValidator ID="SATMathValidator" runat="server" ControlToValidate="SATMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
        <br />
     
        <label for="ACTScoreCommulativeControl">ACT Score Cumulative:</label>
        <sandTrap:NumberBox ID="ACTScoreCommulativeControl" runat="server" Precision="0"  MinAmount="0"/>
        <elv:PropertyProxyValidator ID="ACTScoreCommulativeValidator" runat="server" ControlToValidate="ACTScoreCommulativeControl" PropertyName="Commulative" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <br />
        
        <label for="ACTEnglishControl">&nbsp;&nbsp;&nbsp;&nbsp;English:</label>
        <sandTrap:NumberBox ID="ACTEnglishControl" runat="server" Precision="0"  MinAmount="0"/>
        <elv:PropertyProxyValidator ID="ACTEnglishValidator" runat="server" ControlToValidate="ACTEnglishControl" PropertyName="English" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <br />

        <label for="ACTReadingControl">&nbsp;&nbsp;&nbsp;&nbsp;Reading:</label>
        <sandTrap:NumberBox ID="ACTReadingControl" runat="server" Precision="0"  MinAmount="0"/>
        <elv:PropertyProxyValidator ID="ACTReadingValidator" runat="server" ControlToValidate="ACTReadingControl" PropertyName="Reading" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <br />

        <label for="ACTMathControl">&nbsp;&nbsp;&nbsp;&nbsp;Mathematics:</label>
        <sandTrap:NumberBox ID="ACTMathControl" runat="server" Precision="0"  MinAmount="0"/>
        <elv:PropertyProxyValidator ID="ACTMathValidator" runat="server" ControlToValidate="ACTMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <br />

        <label for="ACTScienceControl">&nbsp;&nbsp;&nbsp;&nbsp;Science:</label>
        <sandTrap:NumberBox ID="ACTScienceControl" runat="server" Precision="0"  MinAmount="0"/>
        <elv:PropertyProxyValidator ID="ACTScienceValidator" runat="server" ControlToValidate="ACTScienceControl" PropertyName="Science" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
        <br />
        

        <label for="HonorsControl">Honors:</label>
        <asp:CheckBox ID="HonorsCheckboxControl" Text="" runat="server" />
        <asp:TextBox ID="HonorsTextControl" MaxLength="50"  Width="110px" runat="server"></asp:TextBox>
       
        <br />

        <label for="APCreditsControl">AP Credits:</label>
        <asp:CheckBox ID="APCreditsCheckBoxControl" Text="" runat="server" />
        <asp:TextBox ID="APCreditsTextControl" MaxLength="50"  Width="110px" runat="server"></asp:TextBox>
    
        <br />
        <label for="IBCreditsControl">IB Credits:</label>
         <asp:CheckBox ID="IBCreditsCheckBoxControl" Text="" runat="server" />
        <asp:TextBox ID="IBCreditsTextBoxControl" MaxLength="50"  Width="110px" runat="server"></asp:TextBox>
    
        <br />
     </div>
 
    </div>
    
</div>
 <hr />
 
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">My Academic Information - College</p>
         <label for="SchoolTypeControl" class="large-label-size">What types of schools are you considering?</label>
        <sb:FlagEnumCheckBoxList id="SchoolTypeControl"    runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
        <br />

        
        <label for="ProgramLengthControl">Length of program:</label><br />
        <sb:EnumRadioButtonList id="ProgramLengthControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.ProgramLengths, ScholarBridge.Domain" />
        <br />
       
        <label for="AcademicProgramControl">Academic program:</label><br />
        <sb:EnumRadioButtonList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
        <br />

        <label for="SeekerStatusControl">Enrollment status:</label><br />
        <sb:EnumRadioButtonList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
        <br />

          
        <label for="FirstGenerationControl" class="large-label-size-checkbox">First Generation in Your Family to Attend College:</label>
        <asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
        <br />
    </div>
    <div class="vertical-devider"></div>
        <div class="form-iceland two-columns">
        <label for="SeekerStudentCollegeTypeControl" class="large-label-size">What type of college student are you?</label>
        <sb:SeekerCollegeType id="SeekerStudentCollegeTypeControl" runat="server" />
        <br /><br />
        
        <label for="CollegeControl" class="large-label-size" >College you are currently attending:</label>
        <br/>
        <sb:LookupDialog ID="CollegeControlDialogButton" runat="server" BuddyControl="CollegeControl" OtherControl="CollegeOtherControl" OtherControlLabel="CollegeOtherLabelControl" ItemSource="CollegeDAL" Title="College Selection" SelectionLimit="1" />
        <asp:TextBox ID="CollegeControl" ReadOnly="true" Width="200px" runat="server"></asp:TextBox>
        <br />
        <label id="CollegeOtherLabelControl" for="CollegeOtherControl">Other:</label>
        <asp:TextBox CssClass="othercontroltextarea" ID="CollegeOtherControl" runat="server" />
        <br />

            <label for="CollegesAppliedLabelControl" class="large-label-size" >Colleges you are considering:</label>
            <asp:CheckBox ID="InWashingtonCheckbox" Text="In Washington" runat="server" />
            <asp:CheckBox ID="OutOfStateCheckbox" Text="Out of State" runat="server" />
            <br />
            <sb:LookupDialog ID="CollegesAppliedControlDialogButton" runat="server" BuddyControl="CollegesAppliedLabelControl" OtherControl="CollegesAppliedOtherControl" ItemSource="CollegeDAL" Title="College Selection"/>
            <asp:TextBox ID="CollegesAppliedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <asp:TextBox CssClass="othercontroltextarea" ID="CollegesAppliedOtherControl" runat="server" />
            <br />

            <%--<label for="CollegesAcceptedLabelControl" class="large-label-size">List of Colleges I’m accepted at:</label>
            <sb:LookupDialog ID="CollegesAcceptedControlDialogButton" runat="server" BuddyControl="CollegesAcceptedLabelControl" OtherControl="CollegesAcceptedOtherControl" ItemSource="CollegeDAL" Title="College Selection"/>
            <asp:TextBox ID="CollegesAcceptedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            <br />
            <asp:TextBox CssClass="othercontroltextarea" ID="CollegesAcceptedOtherControl" runat="server" />
            <br />--%>
        </div>
</div>