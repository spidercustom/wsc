﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutMe.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.AboutMe" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>

<div class="form-iceland">
    <p class="form-section-title">About me</p>

    <label for="PersonalStatementControl">Personal Statement:</label>
    <asp:TextBox ID="PersonalStatementControl" runat="server"   Width="400px"  TextMode="MultiLine"></asp:TextBox>
    <elv:PropertyProxyValidator ID="PersonalStatementValidator" runat="server" ControlToValidate="PersonalStatementControl" PropertyName="PersonalStatement" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    <br />

    <label for="MyChallengeControl">My Challenge:</label>
    <asp:TextBox ID="MyChallengeControl" runat="server" Width="400px"  TextMode="MultiLine"></asp:TextBox>
    <elv:PropertyProxyValidator ID="MyChallengeValidator"   runat="server" ControlToValidate="MyChallengeControl" PropertyName="MyChallenge" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    <br />

    <label for="MyGiftControl">My Gift:</label>
    <asp:TextBox ID="MyGiftControl" runat="server" Width="400px" TextMode="MultiLine"></asp:TextBox>
    <elv:PropertyProxyValidator ID="MyGiftValidator"   runat="server" ControlToValidate="MyGiftControl" PropertyName="MyGift" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    <br />

    <label id="FiveWordsLabelControl" for="FiveWordsControl">5 Words That Describe Me:</label>
    <asp:TextBox ID="FiveWordsControl" TextMode="MultiLine"   Width="400px" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="FiveWordsValidator" runat="server" ControlToValidate="FiveWordsControl" PropertyName="Words" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    <br />

    <label id="FiveSkillsLabelControl" for="FiveSkillsControl">5 Skills I Have:</label>
    <asp:TextBox ID="FiveSkillsControl" TextMode="MultiLine"  Width="400px" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="FiveSkillsValidator" runat="server" ControlToValidate="FiveSkillsControl" PropertyName="Skills" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    <br />
</div>