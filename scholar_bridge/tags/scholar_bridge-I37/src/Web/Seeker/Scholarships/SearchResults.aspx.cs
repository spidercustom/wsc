﻿using System;
using System.Web.Security;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Seeker.Scholarships
{
    public partial class SearchResults : SBBasePage
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        private string Criteria
        {
            get
            {
                if (string.IsNullOrEmpty(Request.Params["val"]))
                    throw new ArgumentException("Cannot understand value of parameter criteria");
                return Request.Params["val"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
				if (Page.User.Identity.IsAuthenticated)
				{
					//ScholarshipSearchBox1.Criteria = Criteria;
					ScholarshipSearchResults1.Criteria = Criteria;
				}
				else
				{
					ScholarshipSearchResultsAnonymous1.Criteria = Criteria;	
				}
				CriteriaLabel.Text = "'" + Criteria + "'";
            }
        }

         
    }
}
