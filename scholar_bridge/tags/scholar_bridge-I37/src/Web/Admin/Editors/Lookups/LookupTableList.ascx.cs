﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Extensions;
using Spring.Context.Support;

namespace ScholarBridge.Web.Admin.Editors.Lookups
{
	public partial class LookupTableList : System.Web.UI.UserControl
	{
		public UserContext UserContext { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{

			}
		}

		#region Object Data Source methods

		public List<Type> GetLookupTableList()
		{
			return GetClasses(typeof(LookupBase));
		}

		public IEnumerable GetLookupTableData(string qualifiedClassName, string sortColumn)
		{
			if (string.IsNullOrEmpty(qualifiedClassName))
				return null;

			var rowsUnsorted = BuildTableRowList(qualifiedClassName);
			var rowsUnsortedList = rowsUnsorted.Cast<ILookup>();
			string[] sortWords = sortColumn.Split(' ');

			bool ascending = true;
			string sortColumnName = string.Empty;

			if (sortWords.Length > 0)
			{
				sortColumnName = sortWords[0];
				if (sortWords.Length > 1)
					ascending = false;
			}

			if (sortColumnName == string.Empty)
				sortColumnName = "Name";

			switch (sortColumnName)
			{
				case "Description":
					return rowsUnsortedList.OrderBy(r => r.Description, ascending).ToList();
				case "Deprecated":
					return rowsUnsortedList.OrderBy(r => r.Deprecated.ToString() + r.Name, ascending).ToList();
				case "On":
					return rowsUnsortedList.OrderBy(r => r.LastUpdate.On + r.Name, ascending).ToList();
				default: // "Name":
					return rowsUnsortedList.OrderBy(r => r.Name, ascending).ToList();
			}
		}

		public void UpdateLookupTableData(string Name, string Description, bool Deprecated, int Id)
		{
			object dal;
			Type dalType = GetDALType(tableDropDown.SelectedValue, out dal);
			// first read in the table row
			ILookup tableRowObject = GetTableRowObject(dalType, dal, Id);
			// now update the row
			MethodInfo updateMethod = dalType.GetMethod("Update", new[] { Type.GetType(tableDropDown.SelectedValue) });
			tableRowObject.Name = Name;
			tableRowObject.Description = Description;
			tableRowObject.Deprecated = Deprecated;
			tableRowObject.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
			updateMethod.Invoke(dal, new object[] {tableRowObject});

		}

		public void DeleteLookupTableData(int Id)
		{
			object dal;
			Type dalType = GetDALType(tableDropDown.SelectedValue, out dal);
			// first read in the table row
			ILookup tableRowObject = GetTableRowObject(dalType, dal, Id);
			// now delete the row
			MethodInfo deleteMethod = dalType.GetMethod("Delete", new[] { Type.GetType(tableDropDown.SelectedValue) });
			deleteMethod.Invoke(dal, new object[] { tableRowObject });

		}
		#endregion

		#region private & protected methods
		private ILookup GetTableRowObject(Type dalType, object dal, int Id)
		{
			MethodInfo findByIdMethod = dalType.GetMethod("FindById", new[] { typeof(int)});
			return (ILookup)findByIdMethod.Invoke(dal, new object[] { Id });
		}

		private IEnumerable BuildTableRowList(string className)
		{
			object dal;
			Type dalType = GetDALType(className, out dal);

			MethodInfo findAllMethod = dalType.GetMethod("FindAll", new [] {typeof(bool)});

			var results = (IEnumerable)findAllMethod.Invoke(dal, new object[] { true });
			return results;
		}

		private static Type GetDALType(string className, out object dal)
		{
			Type tableType = Type.GetType(className);

			Type lookupDalType = typeof(LookupDAL<>);
			var dalType = lookupDalType.MakeGenericType(tableType);

			var foundObjects = ContextRegistry.GetContext().GetObjectsOfType(Type.GetType(dalType.AssemblyQualifiedName));
			if (null == foundObjects || 0.Equals(foundObjects.Count))
				throw new ArgumentException(string.Format("Cannot find object of type {0} in spring container", dalType.AssemblyQualifiedName));

			//What those days were, lines of code to retrive value from collection, 
			//We can probably live without putting ifs
			IEnumerator objectsEnumerable = foundObjects.Values.GetEnumerator();
			if (null == objectsEnumerable)
				throw new InvalidOperationException("Cannot retrive enumerator");
			if (!objectsEnumerable.MoveNext())
				throw new InvalidOperationException("Unexpectedly cannot move to next");
			dal = objectsEnumerable.Current;
			return dal.GetType();
		}

		public static List<Type> GetClasses(Type baseType)
        {
			Assembly assem = Assembly.Load(baseType.Assembly.FullName);
			return (from a in assem.GetTypes() where a.IsSubclassOf(baseType) orderby a.Name select a).ToList();
		}

		#endregion

		#region Control Event Handlers

		protected void dataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = this;
		}

		protected void AddLookupTableData(object sender, EventArgs e)
		{
			Page.Validate();
			if (!Page.IsValid)
				return;

			TextBox nameAddBox = (TextBox)gvLookupTables.FooterRow.FindControl("NameAddBox");
			TextBox descriptionAddBox = (TextBox)gvLookupTables.FooterRow.FindControl("DescriptionAddBox");

			Type tableType = Type.GetType(tableDropDown.SelectedValue); 
			ILookup lookupRow = (ILookup)Activator.CreateInstance(tableType);

			lookupRow.Name = nameAddBox.Text;
			lookupRow.Description = descriptionAddBox.Text;
			lookupRow.Deprecated = false;
			lookupRow.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
			
			object dal;
			Type dalType = GetDALType(tableDropDown.SelectedValue, out dal);

			MethodInfo insertMethod = dalType.GetMethod("Insert", new[] { Type.GetType(tableDropDown.SelectedValue) });
			insertMethod.Invoke(dal, new object[] { lookupRow });
			nameAddBox.Text = string.Empty;
			descriptionAddBox.Text = string.Empty;
			gvLookupTables.DataBind();
		}

		#endregion
	}
}