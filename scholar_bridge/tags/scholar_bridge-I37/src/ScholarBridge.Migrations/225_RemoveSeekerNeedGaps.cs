﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(225)]
    public class RemoveSeekerNeedGaps : Migration
    {
        public override void Up()
        {
            Database.RemoveTable("SBSeekerNeedGapRT");
            Database.RemoveTable("SBApplicationNeedGapRT");
            
        }

        public override void Down()
        {
            new AddSeekerNeedGAPRT().Up();
            new AddApplicationNeedGapRT().Up();
        }
    }
}
