using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(265)]
    public class RemoveDonorInformationFromScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        

        public override void Down()
        {
            
        }

        public override void Up()
        {

            Database.RemoveColumn(TABLE_NAME, "DonorAddressStreet1");
            Database.RemoveColumn(TABLE_NAME, "DonorAddressStreet2");
            Database.RemoveColumn(TABLE_NAME, "DonorAddressCity");
            Database.RemoveColumn(TABLE_NAME, "DonorAddressState");

            Database.RemoveColumn(TABLE_NAME, "DonorAddressPostalCode");
            Database.RemoveColumn(TABLE_NAME, "DonorPhoneNumber");
            Database.RemoveColumn(TABLE_NAME, "DonorEmail");
        }

      
    }
}