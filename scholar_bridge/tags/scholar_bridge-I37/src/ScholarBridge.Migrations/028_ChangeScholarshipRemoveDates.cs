﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(28)]
    public class ChangeScholarshipRemoveDates : Migration
    {
        public override void Up()
        {
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "StartFrom");
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "DueOn");
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "AwardOn");
        }

        public override void Down()
        {
            var scholarship = new AddScholarship();
            var originalColumms = scholarship.CreateColumns();

            Database.AddColumn(AddScholarship.TABLE_NAME, originalColumms[4]);
            Database.AddColumn(AddScholarship.TABLE_NAME, originalColumms[5]);
            Database.AddColumn(AddScholarship.TABLE_NAME, originalColumms[6]);
        }
    }
}
