namespace ScholarBridge.Business.Messaging
{
    public interface ILinkGenerator
    {
        string GetFullLink(string relativePath);
        string GetFullLink(string relativePath, string queryString);
        string UrlEncode(string toEncode);
    }
}