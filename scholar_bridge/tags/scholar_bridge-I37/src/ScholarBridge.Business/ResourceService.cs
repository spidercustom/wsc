﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
	public class ResourceService : IResourceService
	{
		public IResourceDAL ResourceDAL { get; set; }

		public Resource GetById(int id)
		{
			return ResourceDAL.FindById(id);
		}


		public Resource Save(Resource resource)
		{
			if (resource == null)
				throw new ArgumentNullException("resource");

			return ResourceDAL.Save(resource);
		}



		public IList<Resource> FindAll()
		{
			return (from a in ResourceDAL.FindAll("DisplayOrder") orderby a.DisplayOrder ascending select a).ToList();
		}


		public void Delete(Resource resource)
		{
			if (null == resource)
				throw new ArgumentNullException("resource");


			ResourceDAL.Delete(resource);
		}
	}
}