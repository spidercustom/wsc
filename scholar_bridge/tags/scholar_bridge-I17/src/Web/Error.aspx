﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="ScholarBridge.Web.Error" Title="Application Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<h2>Oops!</h2>
<p>Something unexpected has occurred. Please go back and try again.</p>

<pre id="errorMessage" runat="server"></pre>
</asp:Content>
