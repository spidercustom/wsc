﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web
{
    public partial class DefaultPage : Page
    {
        public IUserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (UserContext.CurrentUser != null)
                Response.Redirect("~/Login.aspx");
        }
    }
}
