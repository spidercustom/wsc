﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalMenu.ascx.cs" Inherits="ScholarBridge.Web.Common.GlobalMenu" %>
<%@ Register src="UnreadMessageLink.ascx" tagname="UnreadMessageLink" tagprefix="sb" %>
<%@ Register src="MyHomeLink.ascx" tagname="MyHomeLink" tagprefix="sb" %>
<%@ Register src="ScholarshipSearchBox.ascx" tagname="ScholarshipSearchBox" tagprefix="sb" %>

<asp:LoginView ID="loginView2" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Intermediary, Intermediary Admin,Provider, Provider Admin,WSC Admin, Admin">
                        <ContentTemplate>
                            <DIV id="GlobaNavLinksContainer">
                            <DIV class="GlobalNavLinks">
                            <sb:MyHomeLink runat="server" /> &nbsp; | &nbsp; <sb:UnreadMessageLink ID="UnreadMessageLink1" runat="server" />
                                             <asp:LoginStatus ID="LoginStatus1" LogoutAction="RedirectToLoginPage" runat="server" />
                                       
                             </DIV>
                             </DIV>                        
                       </ContentTemplate>
                    </asp:RoleGroup>
                     <asp:RoleGroup Roles="Seeker">
                        <ContentTemplate>
                             <DIV id="GlobaNavLinksContainerSeeker">
                                    <DIV class="GlobalNavLinks">
                                    <sb:MyHomeLink runat="server" /> &nbsp; | &nbsp; <sb:UnreadMessageLink ID="UnreadMessageLink2" runat="server" />
                                    <asp:LoginStatus ID="LoginStatus2" LogoutAction="RedirectToLoginPage" runat="server" />&nbsp;&nbsp;<sb:ScholarshipSearchBox runat="server" />
                                       
                             </DIV>
                             </DIV>                       
                             </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
        </asp:LoginView>
