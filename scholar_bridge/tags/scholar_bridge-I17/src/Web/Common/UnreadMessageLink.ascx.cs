﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class UnreadMessageLink : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService{get;set;}
        public int MessageCount { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
              

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (UserContext.CurrentUser != null)
            {
                MessageCount = MessageService.CountUnread(UserContext.CurrentUser, UserContext.CurrentOrganization);
                lblCount.Text = MessageCount.ToString() + " New Message(s)";
            }
            if (MessageCount == 0)
                Visible = false;
        }
    }
}

