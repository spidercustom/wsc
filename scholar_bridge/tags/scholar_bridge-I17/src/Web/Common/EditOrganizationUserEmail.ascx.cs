﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
namespace ScholarBridge.Web.Common
{
    public partial class EditOrganizationUserEmail : UserControl
    {
        
        public IUserService UserService { get; set; }

        public User CurrentUser { get; set; }
        public const string VALIDATION_GROUP = "EditOrganizationUserEmail_Validations";
        protected void Page_Load(object sender, EventArgs e)
        {
             
      
        }
        protected void saveButton_Click(object sender, EventArgs e)
        {
            Page.Validate(VALIDATION_GROUP);
            if (Page.IsValid)
            {
                CurrentUser.LastUpdate = new ActivityStamp(CurrentUser);
                UserService.ResetUsername(CurrentUser, Email.Text);
                SuccessMessageLabel.SetMessage("Your Email Address has been changed.");
            }
        }
    }
}