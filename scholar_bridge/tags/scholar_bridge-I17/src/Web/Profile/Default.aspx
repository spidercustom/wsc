﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Profile.Default" Title="Profile" %>


<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="EditUserName" Src="~/Common/EditUserName.ascx" %>
<%@ Register TagPrefix="sb" TagName="EditOrganizationUserEmail" Src="~/Common/EditOrganizationUserEmail.ascx" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopMySettings.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomMySettings.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
        <asp:LoginView ID="loginView2" runat="server">
            <RoleGroups>
                 <asp:RoleGroup Roles="Seeker">
                    <ContentTemplate>
                        <!--Left floated content area starts here-->
                        <DIV id="HomeContentLeft">

                          <Img src="<%= ResolveUrl("~/images/PgTitle_MySettings.gif") %>" width="149px" height="54px">
                          <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerLoggedIn.gif") %>" width="513px" height="96px">
                          </DIV>
                          <!--Left floated content area ends here-->

                         <!--Right Floated content area starts here-->
                         <DIV id="HomeContentRight">
                         <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                         </DIV>
                        <BR><BR>

                        <DIV id="Clear"></DIV>  
                        </ContentTemplate>             
                 </asp:RoleGroup>
                 <asp:RoleGroup Roles="Intermediary, Intermediary Admin, Provider, Provider Admin">
                    <ContentTemplate>
                         <Img src="<%= ResolveUrl("~/images/PgTitle_MySettings.gif") %>" width="149px" height="54px">
                        <BR><BR>
                        <DIV id="Clear"></DIV>  
                        </ContentTemplate>             
                 </asp:RoleGroup>
            </RoleGroups>
        </asp:LoginView>

<div>
    <p class="FormHeader">Change User Details</p>
    
    <sb:EditUserName ID="editUserName" runat="server"  
        OnUserSaved="editUserName_OnUserSaved" />
       
   
</div>

<div>
    <p class="FormHeader">Change Email Address</p>
    <sb:EditOrganizationUserEmail runat="server" ID= "editUserEmail" />   
    <br /><hr><br />
</div>    
 
<div>
    <p class="FormHeader">Change Password</p>
    <div class="form-iceland-container">
        <div class="form-iceland">
        <asp:ChangePassword ID="ChangePassword1" runat="server" 
            SuccessPageUrl="~/Profile/Default.aspx"  
            
            
            onchangedpassword="ChangePassword1_ChangedPassword" BorderPadding="4"   ChangePasswordButtonType="Image" ChangePasswordButtonImageUrl ="~/images/btn_save.gif" CancelButtonStyle-CssClass="hidden">
        </asp:ChangePassword>            
     </div>
     </div>
 </div>
</asp:Content>
