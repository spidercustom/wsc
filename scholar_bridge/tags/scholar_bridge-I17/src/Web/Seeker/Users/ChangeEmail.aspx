﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ChangeEmail.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Users.ChangeEmail" Title="Seeker | Users | Change Email" %>
<%@ Register TagPrefix="sb" TagName="EditUserEmail" Src="~/Common/EditUserEmail.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:EditUserEmail ID="editUserEmail" runat="server" 
         OnUserEmailSaved="editUserEmail_OnUserEmailSaved" 
        OnFormCanceled="editUserEmail_OnFormCanceled" />
</asp:Content>
