﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SearchResults.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Scholarships.SearchResults"  Title="Seeker | Scholarship SearchResults" %>
<%@ Register src="~/Common/ScholarshipSearchResults.ascx" tagname="ScholarshipSearchResults" tagprefix="sb" %>
<%@ Register src="~/Common/ScholarshipSearchBox.ascx" tagname="ScholarshipSearchBox" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<!--Left floated content area starts here-->
                <DIV id="HomeContentLeft">

                  <Img src="<%= ResolveUrl("~/images/PgTitle_ScholarshipResults.gif") %>" width="311px" height="54px">
<%--                  <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerLoggedIn.gif") %>" width="513px" height="96px">--%>
                  </DIV>
                  <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <DIV id="HomeContentRight">
                 <%--<sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />--%>
                 </DIV>
                <BR><BR>

                 <DIV id="Clear"></DIV>
                 
<%--<p>Looking for a specific Scholarship? Enter the Scholarship name or the name of the Scholarship Provider/Donor and we will display the matching results.</p>
    
<sb:ScholarshipSearchBox ID="ScholarshipSearchBox1" runat="server" />
<br />
<br />
<asp:Button ID="BackBtn" runat="server" Text="Go Back" onclick="BackBtn_Click" />
<br />--%>

<p>The following scholarships matched your search criteria. To see full details on these scholarships,
as well as get matched to scholarships you may qualify for, create a profile.
To get started creating your profile,  <a href="<%= ResolveUrl("~/Seeker/Register.aspx") %>">click here</a></p>
<br /><br />


<sb:ScholarshipSearchResults ID="ScholarshipSearchResults1" runat="server" LinkTo="~/Seeker/Matches/Create.aspx" />
</asp:Content>
