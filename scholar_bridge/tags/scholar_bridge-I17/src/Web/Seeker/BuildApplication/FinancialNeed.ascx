﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<div>
<h4>What is your financial need?</h4>
<br />
    <asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
        <h5>Types of Support</h5> 
        <asp:CheckBoxList ID="TypesOfSupport" runat="server" TextAlign="Left"/>
        <br />
    </asp:PlaceHolder>
    
</div>