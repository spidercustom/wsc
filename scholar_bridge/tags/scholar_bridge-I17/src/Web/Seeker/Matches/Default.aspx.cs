﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Web.MatchList;

namespace ScholarBridge.Web.Seeker.Matches
{
	public partial class Default : SBBasePage
	{

        public IMatchService MatchService { get; set; }
		public IUserContext UserContext { get; set; }

		protected void Page_Load(object sender, EventArgs e)
        {

		}

	    protected override void OnInitComplete(EventArgs e)
        {
	        base.OnInitComplete(e);
	        UserContext.EnsureSeekerIsInContext();
	        PopulatePage();
        }

	    private void PopulatePage()
	    {
	        ConfigureLists();
	        BindLists();
	    }

	    private void ConfigureLists()
	    {
	        var iconHelper = new IconHelper();
            var actionHelper = new ActionHelper();
            iconHelper.PostIconClick = BindLists;

            savedList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
            savedList.IconConfigurator = iconHelper.IconConfigurator;
            savedList.ActionButtonConfigurator = actionHelper.ActionConfigurator;

	        qualifyList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
            qualifyList.IconConfigurator = iconHelper.IconConfigurator;
            qualifyList.ActionButtonConfigurator = actionHelper.ActionConfigurator;
        }

	    private void BindLists()
	    {
            var matches = MatchService.GetMatchesForSeeker(UserContext.CurrentSeeker);
	        qualifyList.Matches = matches;
	        qualifyList.BindMatches();
            
            var savedMatches = MatchService.GetSavedButNotAppliedMatches(UserContext.CurrentSeeker);
	        savedList.Matches = savedMatches;
            savedList.BindMatches();
	    }

	    protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.SaveMatch(UserContext.CurrentSeeker, scholarshipId);
        }
	}
}