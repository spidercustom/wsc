using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(170)]
    public class AddPhoneAndDobToSeeker: Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, "MobilePhoneNumber", DbType.String, 10);
            Database.AddColumn(TABLE_NAME, "DateOfBirth", DbType.DateTime);
        }
        
        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "MobilePhoneNumber");
            Database.RemoveColumn(TABLE_NAME, "DateOfBirth");
        }
    }
}