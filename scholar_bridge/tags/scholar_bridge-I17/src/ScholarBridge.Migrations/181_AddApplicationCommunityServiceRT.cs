using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(181)]
    public class AddApplicationCommunityServiceRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationCommunityServiceRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCommunityServiceLUT"; }
        }
    }
}