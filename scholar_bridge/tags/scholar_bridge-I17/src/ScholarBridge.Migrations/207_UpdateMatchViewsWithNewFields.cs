using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(207)]
    public class UpdateMatchViewsWithNewFields : Migration
    {
        public override void Up()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInList]");
            Database.ExecuteNonQuery(SBMatchInList);
        }

        public override void Down()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInList]");
            Database.ExecuteNonQuery(SBMatchInListOrig);
        }

        private const string SBMatchInList =
    @"CREATE VIEW [dbo].[SBMatchInList]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AcademicArea' as criterion, 
	rt.SBAcademicAreaIndex as idx, sRt.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAcademicAreaRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAcademicAreaRT sRt on rt.SBAcademicAreaIndex=sRt.SBAcademicAreaIndex
where match.SBMatchCriteriaAttributeIndex=0
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Career', 
	rt.SBCareerIndex as idx, sRt.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCareerRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCareerRT sRt on rt.SBCareerIndex=sRt.SBCareerIndex
where match.SBMatchCriteriaAttributeIndex=2
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'City', 
	rt.SBCityIndex as idx, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.SBCityIndex=seeker.CityIndex
where match.SBMatchCriteriaAttributeIndex=3
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Club', 
	rt.SBClubIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipClubRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerClubRT sRt on rt.SBClubIndex=sRt.SBClubIndex
where match.SBMatchCriteriaAttributeIndex=4
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'College', 
	rt.SBCollegeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCollegeAppliedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
where match.SBMatchCriteriaAttributeIndex=5
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'College', 
	rt.SBCollegeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCollegeAcceptedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
where match.SBMatchCriteriaAttributeIndex=5
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'CommunityService', 
	rt.SBCommunityServiceIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCommunityServiceRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCommunityServiceRT sRt on rt.SBCommunityServiceIndex=sRt.SBCommunityServiceIndex
where match.SBMatchCriteriaAttributeIndex=7
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'County', 
	rt.SBCountyIndex as idx, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCountyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.SBCountyIndex=seeker.CountyIndex
where match.SBMatchCriteriaAttributeIndex=8
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Ethnicity', 
	rt.SBEthnicityIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipEthnicityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerEthnicityRT sRt on rt.SBEthnicityIndex=sRt.SBEthnicityIndex
where match.SBMatchCriteriaAttributeIndex=9
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'State', 
	rt.StateAbbreviation as idx, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipStateRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.StateAbbreviation=seeker.AddressState
where match.SBMatchCriteriaAttributeIndex=12
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'HighSchool', 
	rt.SBHighSchoolIndex as idx, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipHighSchoolRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.SBHighSchoolIndex=seeker.CurrentHighSchoolIndex
where match.SBMatchCriteriaAttributeIndex=13
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AffiliationType', 
	rt.SBAffiliationTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAffiliationTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAffiliationTypeRT sRt on rt.SBAffiliationTypeIndex=sRt.SBAffiliationTypeIndex
where match.SBMatchCriteriaAttributeIndex=14
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'MatchOrganization', 
	rt.SBSeekerMatchOrganizationIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerMatchOrganizationRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerMatchOrganizationRT sRt on rt.SBSeekerMatchOrganizationIndex=sRt.SBSeekerMatchOrganizationIndex
where match.SBMatchCriteriaAttributeIndex=14
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Religion', 
	rt.SBReligionIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipReligionRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerReligionRT sRt on rt.SBReligionIndex=sRt.SBReligionIndex
where match.SBMatchCriteriaAttributeIndex=16
--union all
--select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SchoolDistrict', 
--	rt.SBSchoolDistrictIndex as idx, sRt.SBSeekerId 
--from SBScholarshipMatchCriteriaAttributeUsageRT match
--inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
--inner join SBScholarshipSchoolDistrictRT rt on s.SBScholarshipId=rt.SBScholarshipId
--left join SBSeekerSchoolDistrictRT sRt on rt.SBSchoolDistrictIndex=sRt.SBSchoolDistrictIndex
--where match.SBMatchCriteriaAttributeIndex=17
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerHobby', 
	rt.SBSeekerHobbyIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerHobbyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerHobbyRT sRt on rt.SBSeekerHobbyIndex=sRt.SBSeekerHobbyIndex
where match.SBMatchCriteriaAttributeIndex=19
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerSkill', 
	rt.SBSeekerSkillIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerSkillRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerSkillRT sRt on rt.SBSeekerSkillIndex=sRt.SBSeekerSkillIndex
where match.SBMatchCriteriaAttributeIndex=20
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerVerbalizingWord', 
	rt.SBSeekerVerbalizingWordIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerVerbalizingWordRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerVerbalizingWordRT sRt on rt.SBSeekerVerbalizingWordIndex=sRt.SBSeekerVerbalizingWordIndex
where match.SBMatchCriteriaAttributeIndex=22
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ServiceHour', 
	rt.SBServiceHourIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipServiceHourRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerServiceHourRT sRt on rt.SBServiceHourIndex=sRt.SBServiceHourIndex
where match.SBMatchCriteriaAttributeIndex=23
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ServiceType', 
	rt.SBServiceTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipServiceTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerServiceTypeRT sRt on rt.SBServiceTypeIndex=sRt.SBServiceTypeIndex
where match.SBMatchCriteriaAttributeIndex=24
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Sport', 
	rt.SBSportIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSportRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSportRT sRt on rt.SBSportIndex=sRt.SBSportIndex
where match.SBMatchCriteriaAttributeIndex=25
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'WorkHour', 
	rt.SBWorkHourIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipWorkHourRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerWorkHourRT sRt on rt.SBWorkHourIndex=sRt.SBWorkHourIndex
where match.SBMatchCriteriaAttributeIndex=27
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'WorkType',
	rt.SBWorkTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipWorkTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerWorkTypeRT sRt on rt.SBWorkTypeIndex=sRt.SBWorkTypeIndex
where match.SBMatchCriteriaAttributeIndex=28";


        private const string SBMatchInListOrig =
            @"CREATE VIEW [dbo].[SBMatchInList]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AcademicArea' as criterion, 
	rt.SBAcademicAreaIndex as idx, sRt.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAcademicAreaRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAcademicAreaRT sRt on rt.SBAcademicAreaIndex=sRt.SBAcademicAreaIndex
where match.SBMatchCriteriaAttributeIndex=0
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Career', 
	rt.SBCareerIndex as idx, sRt.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCareerRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCareerRT sRt on rt.SBCareerIndex=sRt.SBCareerIndex
where match.SBMatchCriteriaAttributeIndex=2
union all
--select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'City', 
--	rt.SBCityIndex as idx, sRt.SBSeekerId 
--from SBScholarshipMatchCriteriaAttributeUsageRT match
--inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
--inner join SBScholarshipCityRT rt on s.SBScholarshipId=rt.SBScholarshipId
--left join SBSeekerCityRT sRt on rt.SBCityIndex=sRt.SBCityIndex
--where match.SBMatchCriteriaAttributeIndex=3
--union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Club', 
	rt.SBClubIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipClubRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerClubRT sRt on rt.SBClubIndex=sRt.SBClubIndex
where match.SBMatchCriteriaAttributeIndex=4
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'College', 
	rt.SBCollegeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCollegeAppliedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
where match.SBMatchCriteriaAttributeIndex=5
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'College', 
	rt.SBCollegeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCollegeAcceptedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
where match.SBMatchCriteriaAttributeIndex=5
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'CommunityService', 
	rt.SBCommunityServiceIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCommunityServiceRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCommunityServiceRT sRt on rt.SBCommunityServiceIndex=sRt.SBCommunityServiceIndex
where match.SBMatchCriteriaAttributeIndex=7
--union all
--select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'County', 
--	rt.SBCountyIndex as idx, sRt.SBSeekerId 
--from SBScholarshipMatchCriteriaAttributeUsageRT match
--inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
--inner join SBScholarshipCountyRT rt on s.SBScholarshipId=rt.SBScholarshipId
--left join SBSeekerCountyRT sRt on rt.SBCountyIndex=sRt.SBCountyIndex
--where match.SBMatchCriteriaAttributeIndex=8
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Ethnicity', 
	rt.SBEthnicityIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipEthnicityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerEthnicityRT sRt on rt.SBEthnicityIndex=sRt.SBEthnicityIndex
where match.SBMatchCriteriaAttributeIndex=9
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'State', 
	rt.StateAbbreviation as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipStateRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker sRt on rt.StateAbbreviation=sRt.AddressState
where match.SBMatchCriteriaAttributeIndex=12
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'HighSchool', 
	rt.SBHighSchoolIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipHighSchoolRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker sRt on rt.SBHighSchoolIndex=sRt.CurrentHighSchoolIndex
where match.SBMatchCriteriaAttributeIndex=13
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AffiliationType', 
	rt.SBAffiliationTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAffiliationTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAffiliationTypeRT sRt on rt.SBAffiliationTypeIndex=sRt.SBAffiliationTypeIndex
where match.SBMatchCriteriaAttributeIndex=14
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'MatchOrganization', 
	rt.SBSeekerMatchOrganizationIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerMatchOrganizationRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerMatchOrganizationRT sRt on rt.SBSeekerMatchOrganizationIndex=sRt.SBSeekerMatchOrganizationIndex
where match.SBMatchCriteriaAttributeIndex=14
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Religion', 
	rt.SBReligionIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipReligionRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerReligionRT sRt on rt.SBReligionIndex=sRt.SBReligionIndex
where match.SBMatchCriteriaAttributeIndex=16
--union all
--select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SchoolDistrict', 
--	rt.SBSchoolDistrictIndex as idx, sRt.SBSeekerId 
--from SBScholarshipMatchCriteriaAttributeUsageRT match
--inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
--inner join SBScholarshipSchoolDistrictRT rt on s.SBScholarshipId=rt.SBScholarshipId
--left join SBSeekerSchoolDistrictRT sRt on rt.SBSchoolDistrictIndex=sRt.SBSchoolDistrictIndex
--where match.SBMatchCriteriaAttributeIndex=17
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerHobby', 
	rt.SBSeekerHobbyIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerHobbyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerHobbyRT sRt on rt.SBSeekerHobbyIndex=sRt.SBSeekerHobbyIndex
where match.SBMatchCriteriaAttributeIndex=19
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerSkill', 
	rt.SBSeekerSkillIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerSkillRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerSkillRT sRt on rt.SBSeekerSkillIndex=sRt.SBSeekerSkillIndex
where match.SBMatchCriteriaAttributeIndex=20
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerVerbalizingWord', 
	rt.SBSeekerVerbalizingWordIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerVerbalizingWordRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerVerbalizingWordRT sRt on rt.SBSeekerVerbalizingWordIndex=sRt.SBSeekerVerbalizingWordIndex
where match.SBMatchCriteriaAttributeIndex=22
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ServiceHour', 
	rt.SBServiceHourIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipServiceHourRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerServiceHourRT sRt on rt.SBServiceHourIndex=sRt.SBServiceHourIndex
where match.SBMatchCriteriaAttributeIndex=23
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ServiceType', 
	rt.SBServiceTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipServiceTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerServiceTypeRT sRt on rt.SBServiceTypeIndex=sRt.SBServiceTypeIndex
where match.SBMatchCriteriaAttributeIndex=24
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Sport', 
	rt.SBSportIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSportRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSportRT sRt on rt.SBSportIndex=sRt.SBSportIndex
where match.SBMatchCriteriaAttributeIndex=25
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'WorkHour', 
	rt.SBWorkHourIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipWorkHourRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerWorkHourRT sRt on rt.SBWorkHourIndex=sRt.SBWorkHourIndex
where match.SBMatchCriteriaAttributeIndex=27
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'WorkType',
	rt.SBWorkTypeIndex as idx, sRt.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipWorkTypeRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerWorkTypeRT sRt on rt.SBWorkTypeIndex=sRt.SBWorkTypeIndex
where match.SBMatchCriteriaAttributeIndex=28";


    }
}