using System.Collections.Generic;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IOrganizationDAL<T> : IDAL<T>
    {
        T FindById(int id);
        User FindUserInOrg(T organization, int userId);
        T FindByUser(User user);
        IList<T> FindAllPending();
    }
}