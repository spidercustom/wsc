using System.Collections.Generic;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IApplicationDAL : IDAL<Application>
    {
        Application FindById(int id);
        Application Save(Application application);

        IList<Application> FindAllSubmitted(Scholarship scholarship);
        IList<Application> FindAll(Seeker seeker);
        IList<Application> SearchSubmittedBySeeker(Scholarship scholarship, string lastName);
        IList<Application> FindAllFinalists(Scholarship scholarship);

        Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship);

        int CountAllSubmittedBySeeker(Seeker seeker);
        int CountAllSubmitted(Scholarship scholarship);
    }
}