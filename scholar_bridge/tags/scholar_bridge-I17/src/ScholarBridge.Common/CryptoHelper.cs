﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace ScholarBridge.Common
{
    public static class CryptoHelper
    {
        private const string PARAMETER_NAME = "enc=";

        // XXX: Should the encryption key be looked up as a config item?
        private const string ENCRYPTION_KEY = "EiJei1vai0leiJievaRicheeHoamoo";
        private static readonly byte[] SALT = new MD5CryptoServiceProvider().ComputeHash(Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString()));

        /// <summary>
        /// Encrypts any string using the Rijndael algorithm.
        /// </summary>
        /// <param name="inputText">The string to encrypt.</param>
        /// <returns>A Base64 encrypted string.</returns>
        public static string Encrypt(string inputText)
        {
            var plainText = Encoding.Unicode.GetBytes(inputText);
            var rijndaelCipher = new RijndaelManaged();
            var secretKey = new Rfc2898DeriveBytes(ENCRYPTION_KEY, SALT);

            using (var encryptor = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        return PARAMETER_NAME + Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts a previously encrypted string.
        /// </summary>
        /// <param name="inputText">The encrypted string to decrypt.</param>
        /// <returns>A decrypted string.</returns>
        public static string Decrypt(string inputText)
        {
            var encryptedData = Convert.FromBase64String(inputText.Replace(PARAMETER_NAME, string.Empty));
            var rijndaelCipher = new RijndaelManaged();
            var secretKey = new Rfc2898DeriveBytes(ENCRYPTION_KEY, SALT);

            using (var decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
            {
                using (var cryptoStream = new CryptoStream(new MemoryStream(encryptedData), decryptor, CryptoStreamMode.Read))
                {
                    var plainText = new byte[encryptedData.Length];
                    var decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                    return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                }
            }
        }
    }
}