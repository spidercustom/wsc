﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        private UserService userService;
        private MockRepository mocks;
        private IUserDAL userDAL;
        private IProviderDAL providerDAL;
        private IIntermediaryDAL intermediaryDAL;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            userDAL = mocks.StrictMock<IUserDAL>();
            providerDAL = mocks.StrictMock<IProviderDAL>();
            intermediaryDAL = mocks.StrictMock<IIntermediaryDAL>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();
            userService = new UserService
                              {
                                  UserDAL = userDAL,
                                  ProviderDAL = providerDAL,
                                  IntermediaryDAL = intermediaryDAL,
                                  MessagingService = messagingService,
                                  TemplateParametersService = templateParametersService
                              };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(userDAL);
            mocks.BackToRecord(providerDAL);
            mocks.BackToRecord(intermediaryDAL);
            mocks.BackToRecord(messagingService);
        }

        [Test]
        public void activate_seeker_user()
        {
            var user = new User { Id = 5 };

            Expect.Call(userDAL.Update(user)).Return(user);
            mocks.ReplayAll();

            userService.ActivateSeekerUser(user);
            Assert.IsTrue(user.IsActive);
            Assert.IsTrue(user.IsApproved);

            mocks.VerifyAll();
        }

        [Test]
        public void activate_provider_user()
        {
            var user = new User {Id = 5};
            var provider = new Provider {Name = "Provider"};

            Expect.Call(userDAL.Update(user)).Return(user);
            Expect.Call(providerDAL.FindByUser(user)).Return(provider);
            Expect.Call(() => templateParametersService.RequestOrganizationApproval(Arg<User>.Is.Equal(user),
                Arg<Organization>.Is.Equal(provider), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageToAdmin(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));

            mocks.ReplayAll();

            userService.ActivateProviderUser(user);
            Assert.IsTrue(user.IsActive);

            mocks.VerifyAll();
        }

        [Test]
        public void activate_intermediary_user()
        {
            var user = new User { Id = 5 };
            var intermediary = new Intermediary { Name = "Intermediary" };

            Expect.Call(userDAL.Update(user)).Return(user);
            Expect.Call(intermediaryDAL.FindByUser(user)).Return(intermediary);
            Expect.Call(() => templateParametersService.RequestOrganizationApproval(Arg<User>.Is.Equal(user),
                    Arg<Organization>.Is.Equal(intermediary), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageToAdmin(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything));

            mocks.ReplayAll();

            userService.ActivateIntermediaryUser(user);
            Assert.IsTrue(user.IsActive);

            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void activate_user_throws_exception_if_null_user_passed()
        {
            userService.ActivateProviderUser(null);
            Assert.Fail();
        }

        [Test]
        public void ResetUsername()
        {
            var user = new User {Id = 5, IsActive = true};

            Expect.Call(userDAL.Update(user)).Return(user);
            Expect.Call(() => templateParametersService.ConfirmationLink(Arg<User>.Is.Equal(user), Arg<bool>.Is.Equal(false), Arg<MailTemplateParams>.Is.Anything));
            messagingService.SendEmail(Arg<User>.Is.Equal(user), Arg<MessageType>.Is.Equal(MessageType.ConfirmationLink),
                                       Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything);
            mocks.ReplayAll();

            userService.ResetUsername(user, "foo@bar.com");
            Assert.IsFalse(user.IsActive);
            Assert.AreEqual(user.Username, user.Email);
            Assert.AreEqual("foo@bar.com", user.Email);

            mocks.VerifyAll();
        }

        [Test]
        public void UpdateEmailAddressTest()
        {
            var emailWaiting = "foo@bar.com";
            var user = new User { Id = 5, IsActive = true,Username="testuser", Email="aa@aa.com",EmailWaitingforVerification=null };

            Expect.Call(userDAL.Update(user)).Return(user);
            Expect.Call(() => templateParametersService.EmailAddressChangeVerificationLink(Arg<User>.Is.Equal(user),  Arg<MailTemplateParams>.Is.Anything));
            messagingService.SendEmail(Arg<String>.Is.Equal(emailWaiting), Arg<MessageType>.Is.Equal(MessageType.EmailAddressChangeVerificationLink),
                                       Arg<MailTemplateParams>.Is.Anything, Arg<bool>.Is.Anything);
            mocks.ReplayAll();

            userService.UpdateEmailAddress(user, emailWaiting);
            Assert.AreNotEqual (user.EmailWaitingforVerification, user.Email);
            Assert.AreEqual(emailWaiting, user.EmailWaitingforVerification);

            mocks.VerifyAll();
        }
        [Test]
        public void ConfrimEmailAddressVerification_Test()
        {
            var emailWaiting = "foo@bar.com";
            var user = new User { Id = 5, IsActive = true, Username = "testuser", Email = "aa@aa.com", EmailWaitingforVerification = emailWaiting };

            Expect.Call(userDAL.Update(user)).Return(user);
            mocks.ReplayAll();

            userService.ConfirmEmailAddressVerification(user);
            Assert.AreNotEqual(user.Username, user.Email);
            Assert.AreEqual(emailWaiting, user.Email);
            Assert.AreEqual(null, user.EmailWaitingforVerification);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void resetusername_user_throws_exception_if_null_user_passed()
        {
            userService.ResetUsername(null, "foo@bar.com");
            Assert.Fail();
        }
    }
}