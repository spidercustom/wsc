﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(121)]
    public class ChangeCityRelateWithState : Migration
    {
        private const string TABLE_NAME = "SBCityLUT";
        private static readonly Column STATE_COLUMN =
            new Column("StateAbbreviation", DbType.String, 2, ColumnProperty.NotNull, "'WA'");
        private static readonly string FK_STATE_ABBREVIATION = string.Format("FK_{0}_StateAbbreviation", TABLE_NAME);

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, STATE_COLUMN);
            Database.AddForeignKey(FK_STATE_ABBREVIATION, TABLE_NAME, STATE_COLUMN.Name, "SBStateLUT", "Abbreviation");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_STATE_ABBREVIATION);
            Database.RemoveColumn(TABLE_NAME, STATE_COLUMN.Name);
        }
    }
}