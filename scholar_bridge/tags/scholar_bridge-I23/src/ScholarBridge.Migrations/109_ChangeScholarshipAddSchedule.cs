﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(109)]
    public class ChangeScholarshipAddSchedule : AddTableBase
    {
        private string SCHOLARSHIP_TABLE_NAME = "SBScholarship";
        private string TABLE_NAME = "SBScholarshipSchedule";
        private string PRIMARY_KEY_COLUMN = "SBScholarshipScheduleId";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                          new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                          new Column("SBScholarshipId", DbType.Int32, ColumnProperty.NotNull),
                          new Column("ScheduleType", DbType.String, 10, ColumnProperty.NotNull),

                          new Column("ScheduleStartFromMonth", DbType.Int16, ColumnProperty.NotNull, 0),
                          new Column("ScheduleDueOnMonth", DbType.Int16, ColumnProperty.NotNull, 0),
                          new Column("ScheduleAwardOnMonth", DbType.Int16, ColumnProperty.NotNull,  0),

                          new Column("MonthlyScheduleStartFromDayOfMonth", DbType.Int16),
                          new Column("MonthlyScheduleDueOnDayOfMonth", DbType.Int16),
                          new Column("MonthlyScheduleAwardOnDayOfMonth", DbType.Int16),
                          
                          new Column("WeeklyScheduleStartFromWeekDayOccurrence", DbType.Int16), 
                          new Column("WeeklyScheduleStartFromDayOfWeek", DbType.Int16),
                          new Column("WeeklyScheduleDueOnWeekDayOccurrence", DbType.Int16), 
                          new Column("WeeklyScheduleDueOnDayOfWeek", DbType.Int16),
                          new Column("WeeklyScheduleAwardOnWeekDayOccurrence", DbType.Int16), 
                          new Column("WeeklyScheduleAwardOnDayOfWeek", DbType.Int16)
                           };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new[] 
            {
                CreateForeignKey("SBScholarshipID", SCHOLARSHIP_TABLE_NAME, "SBScholarshipID")
            }
            ;
        }

        public override void Up()
        {
            base.Up();

            Database.ExecuteNonQuery(COPY_MONTHLY_SCHEDULE_DATA);
            Database.ExecuteNonQuery(COPY_WEEKLY_SCHEDULE_DATA);

            Database.RemoveColumn(SCHOLARSHIP_TABLE_NAME, "ScheduleID");
            Database.RemoveColumn(SCHOLARSHIP_TABLE_NAME, "ScheduleType");
            Database.RemoveTable("SBScholarshipScheduleByMonthDay");
            Database.RemoveTable("SBScholarshipScheduleByWeekDay");
        }

        public override void Down()
        {
            Database.AddColumn(AddScholarship.TABLE_NAME, "ScheduleType", DbType.String, 10, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, "ScheduleId", DbType.Int32, ColumnProperty.Null);

            var monthDayMigration = new ReAddScholarshipScheduleByMonthDay { Database = Database };
            monthDayMigration.Up();

            var weekDayMigration = new ReAddScholarshipScheduleByWeekDay { Database = Database };
            weekDayMigration.Up();

            base.Down();
        }

        // Names had changed from SB_ to SB
        private class ReAddScholarshipScheduleByMonthDay : AddScholarshipScheduleByMonthDay
        {
            public override string TableName
            {
                get { return "SBScholarshipScheduleByMonthDay"; }
            }

            public override string PrimaryKeyColumn
            {
                get { return "SBScholarshipScheduleByMonthDayID"; }
            }
        }

        private class ReAddScholarshipScheduleByWeekDay : AddScholarshipScheduleByMonthDay
        {
            public override string TableName
            {
                get { return "SBScholarshipScheduleByWeekDay"; }
            }

            public override string PrimaryKeyColumn
            {
                get { return "SBScholarshipScheduleByWeekDayID"; }
            }
        }

        private const string COPY_MONTHLY_SCHEDULE_DATA = @"
INSERT INTO SBScholarshipSchedule
(
	SBScholarshipID,
	ScheduleType,
	ScheduleStartFromMonth, 
	ScheduleDueOnMonth,
	ScheduleAwardOnMonth,
	MonthlyScheduleStartFromDayOfMonth,
	MonthlyScheduleDueOnDayOfMonth,
	MonthlyScheduleAwardOnDayOfMonth
)
SELECT
		SBScholarship.SBScholarshipID,	
		SBScholarship.ScheduleType,
		SBScholarshipScheduleByMonthDay.StartFromMonth,
		SBScholarshipScheduleByMonthDay.DueOnMonth,
		SBScholarshipScheduleByMonthDay.AwardOnMonth,
		SBScholarshipScheduleByMonthDay.StartFromDayOfMonth,
		SBScholarshipScheduleByMonthDay.DueOnDayOfMonth,
		SBScholarshipScheduleByMonthDay.AwardOnDayOfMonth
FROM
	SBScholarship,
	SBScholarshipScheduleByMonthDay
WHERE
	SBScholarship.ScheduleType = 'ByMonth' AND
    SBScholarship.ScheduleID = SBScholarshipScheduleByMonthDay.SBScholarshipScheduleByMonthDayID
                ;";

        private const string COPY_WEEKLY_SCHEDULE_DATA = @"
INSERT INTO SBScholarshipSchedule
(
	SBScholarshipID,
	ScheduleType,
	ScheduleStartFromMonth, 
	ScheduleDueOnMonth,
	ScheduleAwardOnMonth,
	WeeklyScheduleStartFromWeekDayOccurrence,
	WeeklyScheduleDueOnWeekDayOccurrence,
	WeeklyScheduleAwardOnWeekDayOccurrence,
	WeeklyScheduleStartFromDayOfWeek,
	WeeklyScheduleDueOnDayOfWeek,
	WeeklyScheduleAwardOnDayOfWeek
)
SELECT
		SBScholarship.SBScholarshipID,	
		SBScholarship.ScheduleType,
		SBScholarshipScheduleByWeekDay.StartFromMonth,
		SBScholarshipScheduleByWeekDay.DueOnMonth,
		SBScholarshipScheduleByWeekDay.AwardOnMonth,
		SBScholarshipScheduleByWeekDay.StartFromWeekDayOccurrence,
		SBScholarshipScheduleByWeekDay.DueOnWeekDayOccurrence,
		SBScholarshipScheduleByWeekDay.AwardOnWeekDayOccurrence,
		SBScholarshipScheduleByWeekDay.StartFromDayOfWeek,
		SBScholarshipScheduleByWeekDay.DueOnDayOfWeek,
		SBScholarshipScheduleByWeekDay.AwardOnDayOfWeek
FROM
	SBScholarship,
	SBScholarshipScheduleByWeekDay
WHERE
	SBScholarship.ScheduleType = 'ByWeek' AND
    SBScholarship.ScheduleID = SBScholarshipScheduleByWeekDay.SBScholarshipScheduleByWeekDayID
        ;";
    }
}