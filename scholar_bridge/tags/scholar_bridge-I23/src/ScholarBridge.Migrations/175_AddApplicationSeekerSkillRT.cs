using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(175)]
    public class AddApplicationSeekerSkillRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationSeekerSkillRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSeekerSkillLUT"; }
        }
    }
}