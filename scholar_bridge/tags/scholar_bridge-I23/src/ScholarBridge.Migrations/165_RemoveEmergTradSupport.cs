using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(165)]
    public class RemoveEmergTradSupport : Migration
    {
        private const string SCHOLARSHIP_TABLE = "SBScholarship";
        private const string SEEKER_TABLE = "SBSeeker";

        public override void Up()
        {
            Database.RemoveColumn(SCHOLARSHIP_TABLE, "Emergency");
            Database.RemoveColumn(SCHOLARSHIP_TABLE, "Traditional");

            Database.RemoveColumn(SEEKER_TABLE, "Emergency");
            Database.RemoveColumn(SEEKER_TABLE, "Traditional");
        }
        
        public override void Down()
        {
            Database.AddColumn(SCHOLARSHIP_TABLE, "Emergency", DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(SCHOLARSHIP_TABLE, "Traditional", DbType.Boolean, ColumnProperty.Null);

            Database.AddColumn(SEEKER_TABLE, "Emergency", DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(SEEKER_TABLE, "Traditional", DbType.Boolean, ColumnProperty.Null);
        }
    }
}