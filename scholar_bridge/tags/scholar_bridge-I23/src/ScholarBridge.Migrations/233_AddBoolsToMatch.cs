﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(233)]
    public class AddBoolsToMatch : Migration
    {
        private const string TABLE_NAME = "SBMatch";

        public readonly string[]  COLUMNS
            = { "AddedViaMatch"};

        public override void Up()
        {

            Database.AddColumn(TABLE_NAME, COLUMNS[0], DbType.Boolean, ColumnProperty.Null);

        	Database.ExecuteNonQuery("update SBMatch set " + COLUMNS[0] + " = 1");

			Database.ChangeColumn(TABLE_NAME, new Column(COLUMNS[0], DbType.Boolean, ColumnProperty.NotNull));
		}

		public override void Down()
		{
			Database.RemoveColumn(TABLE_NAME, COLUMNS[0]);
		}
	}
}