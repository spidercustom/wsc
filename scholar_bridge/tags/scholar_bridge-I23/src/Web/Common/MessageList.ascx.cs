﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class MessageList : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }
        public IUserService UserService { get; set; }

		public bool Archived { get; set; }
		public bool ScholarshipApprovals { get; set; }
		public MessageAction MessageAction { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            IList<Domain.Messaging.Message> messages;
			if (Archived)
			{
				messages = MessageService.FindAllArchived(UserContext.CurrentUser, UserContext.CurrentOrganization);
			}
			else if (ScholarshipApprovals)
			{
				messages = MessageService.FindAllApprovedScholarships(UserContext.CurrentUser, UserContext.CurrentOrganization);
			}
			else if (MessageAction != MessageAction.None)
            {
                messages = MessageService.FindAll(MessageAction, UserContext.CurrentUser, UserContext.CurrentOrganization);
            }
            else
            {
                messages = MessageService.FindAllWithoutApprovedScholarships(UserContext.CurrentUser, UserContext.CurrentOrganization);
            }
            messageList.DataSource = (from m in messages orderby m.Date descending select m).ToList();
            messageList.DataBind();
            pager.VisibleIf(() => pager.PageSize < pager.TotalRowCount);
            messageList.PagePropertiesChanged += (s1, e1) => messageList.DataBind();
        }

        protected void messageList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var org = (Label)e.Item.FindControl("lblOrg");
                var from = (Label)e.Item.FindControl("lblFrom");
                var message = ((Domain.Messaging.Message) ((ListViewDataItem)e.Item).DataItem);
                
                if (!(message.From.Organization ==null))
                  org.Text=  message.From.Organization.Name;

                var user = UserService.FindByEmail(message.From.EmailAddress());
				
                if (!(user == null))
                {
                    from.Text = user.Name.NameFirstLast;
                	if (string.IsNullOrEmpty(org.Text))
                	{
                		if (user.Organizations.Count > 0)
                		{
                			org.Text = user.Organizations[0].Name;
                		}
                	}
                }

                var link = (LinkButton)e.Item.FindControl("linkToMessage");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
                + "?popup=true&id=" + +(message).Id;
                link.Attributes.Add("onclick", String.Format("messageView('{0}'); return false;", url));
                link.Attributes.Add("class", message.IsRead ? "GreenLink" : "BlueLink");
            }
        }
    }
}