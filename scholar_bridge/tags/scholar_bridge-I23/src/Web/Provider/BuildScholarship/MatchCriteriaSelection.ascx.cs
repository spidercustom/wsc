﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;


namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class MatchCriteriaSelection : WizardStepUserControlBase<Scholarship>
    {
        public MatchCriteriaSelection()
        {
            SetupDefaultUpdaters();    
        }

        public IScholarshipService ScholarshipService { get; set; }

        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        private void Attributes_Added(object sender, ScholarshipAttributeCollection<SeekerProfileAttribute>.AttributeUsageEventArgs e)
        {
            PopulateDefaults(e.Usage.Attribute);
        }

        #region default value update
        private readonly Dictionary<SeekerProfileAttribute, Action> defaultUpdaters = new Dictionary<SeekerProfileAttribute, Action>();

        private void SetupDefaultUpdaters()
        {
            defaultUpdaters.Add(SeekerProfileAttribute.StudentGroup, () => 
                ScholarshipInContext.SeekerProfileCriteria.StudentGroups =
                (StudentGroups)EnumExtensions.SelectAll(typeof(StudentGroups)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.SchoolType, () => 
                ScholarshipInContext.SeekerProfileCriteria.SchoolTypes = 
                (SchoolTypes)EnumExtensions.SelectAll(typeof(SchoolTypes)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.AcademicProgram, () => 
                ScholarshipInContext.SeekerProfileCriteria.AcademicPrograms = 
                (AcademicPrograms)EnumExtensions.SelectAll(typeof(AcademicPrograms)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.SeekerStatus, () => 
                ScholarshipInContext.SeekerProfileCriteria.SeekerStatuses = 
                (SeekerStatuses) EnumExtensions.SelectAll(typeof(SeekerStatuses)));
            
            defaultUpdaters.Add(SeekerProfileAttribute.ProgramLength, () => 
                ScholarshipInContext.SeekerProfileCriteria.ProgramLengths = 
                (ProgramLengths)EnumExtensions.SelectAll(typeof(ProgramLengths)));
        }

        private void PopulateDefaults(SeekerProfileAttribute attribute)
        {
            if (defaultUpdaters.ContainsKey(attribute))
            {
                defaultUpdaters[attribute]();
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ScholarshipInContext.SeekerProfileCriteria.Attributes.Added += Attributes_Added;
            if (!IsPostBack)
            {
                SeekerStudentTypeControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.StudentGroup,
                    SeekerProfileAttribute.SchoolType,
                    SeekerProfileAttribute.AcademicProgram,
                    SeekerProfileAttribute.SeekerStatus,
                    SeekerProfileAttribute.ProgramLength,
                    SeekerProfileAttribute.College);
                SeekerStudentTypeControl.RepeaterControl.DataBind();

                SeekerDemographicsPersonalControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.FirstGeneration,
                    SeekerProfileAttribute.Ethnicity,
                    SeekerProfileAttribute.Religion,
                    SeekerProfileAttribute.Gender);
                SeekerDemographicsPersonalControl.RepeaterControl.DataBind();

                SeekerDemographicsGeographicControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SeekerGeographicsLocation);
                SeekerDemographicsGeographicControl.RepeaterControl.DataBind();

                SeekerInterestsControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.AcademicArea,
                    SeekerProfileAttribute.Career,
                    SeekerProfileAttribute.CommunityService,
                    SeekerProfileAttribute.OrganizationAndAffiliationType);
                SeekerInterestsControl.RepeaterControl.DataBind();

                SeekerActivitiesControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SeekerHobby,
                    SeekerProfileAttribute.Sport,
                    SeekerProfileAttribute.Club,
                    SeekerProfileAttribute.WorkType,
                    SeekerProfileAttribute.WorkHour,
                    SeekerProfileAttribute.ServiceType,
                    SeekerProfileAttribute.ServiceHour);
                SeekerActivitiesControl.RepeaterControl.DataBind();

                SeekerStudentPerformanceControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.GPA,
                    SeekerProfileAttribute.ClassRank,
					SeekerProfileAttribute.SATCriticalReading,
					SeekerProfileAttribute.SATWriting,
					SeekerProfileAttribute.SATMathematics,
					SeekerProfileAttribute.ACTEnglish,
					SeekerProfileAttribute.ACTMathematics,
					SeekerProfileAttribute.ACTReading,
					SeekerProfileAttribute.ACTScience,
					SeekerProfileAttribute.Honor,
                    SeekerProfileAttribute.APCreditsEarned,
                    SeekerProfileAttribute.IBCreditsEarned);
                SeekerStudentPerformanceControl.RepeaterControl.DataBind();


                SupportSituationsControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(

                    FundingProfileAttribute.SupportedSituation
                    
                    );

                SupportSituationsControl.RepeaterControl.DataBind();
                
                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {
            PopulateSeekerProfileCriteriaAttributesScreen();
        }

        private void PopulateSeekerProfileCriteriaAttributesScreen()
        {
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerStudentTypeControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerDemographicsPersonalControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerDemographicsGeographicControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerInterestsControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerActivitiesControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerStudentPerformanceControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SupportSituationsControl);
        }

        public void PopulateSeekerProfileCriteriaSectionAttributesScreen(AttributeSelectionControl attributeSelectionControl)
        {
            attributeSelectionControl.
               ForEachRepeaterItem(usageButtonGroup =>
               {
                   var attribute = (SeekerProfileAttribute)usageButtonGroup.AttributeEnumInt;

                   var attributeUsage = ScholarshipInContext.SeekerProfileCriteria.Attributes.Find(attribute);

                   usageButtonGroup.Value =
                       null == attributeUsage ?
                       ScholarshipAttributeUsageType.NotUsed :
                       attributeUsage.UsageType;
               });
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override void PopulateObjects()
        {
            PopulateSeekerProfileCriteriaAttributesObject();

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStages.NotActivated;
        }

        private void PopulateSeekerProfileCriteriaAttributesObject()
        {
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerStudentTypeControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerDemographicsPersonalControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerDemographicsGeographicControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerInterestsControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerActivitiesControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerStudentPerformanceControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SupportSituationsControl); 
        }

        private void PopulateSeekerProfileCriteriaSectionAttributesObject(AttributeSelectionControl attributeSelectionControl)
        {
            foreach (RepeaterItem repeaterItem in attributeSelectionControl.RepeaterControl.Items)
            {
                AttributeSelectionControl.AttributeUsageButtonGroup buttonGroup = BuildButtonGroup(repeaterItem);
                var attribute = (SeekerProfileAttribute)buttonGroup.AttributeEnumInt;
                if (!buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
                {
                    var attributeUsage = new SeekerProfileAttributeUsage
                                             {
                                                 Attribute = attribute,
                                                 UsageType = buttonGroup.Value
                                             };
                    ScholarshipInContext.SeekerProfileCriteria.Attributes.Add(attributeUsage);
                }
                else
                {
                    ScholarshipInContext.SeekerProfileCriteria.Attributes.Remove(attribute);
                }
            }
        }

        private static AttributeSelectionControl.AttributeUsageButtonGroup BuildButtonGroup(Control repeaterItem)
		{
            var usageMinimumRB = (RadioButton)repeaterItem.FindControl("Minimum");
            var usagePreferenceRB = (RadioButton)repeaterItem.FindControl("Preference");
            var usageNotUsedRB = (RadioButton)repeaterItem.FindControl("Minimum");
			var attributeControl = (HtmlInputHidden)repeaterItem.FindControl("KeyControl");
			return new AttributeSelectionControl.AttributeUsageButtonGroup(Int32.Parse(attributeControl.Value), usageMinimumRB, usagePreferenceRB, usageNotUsedRB);
		}
    }
}