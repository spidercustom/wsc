﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfo.ascx.cs"
    Inherits="ScholarBridge.Web.Provider.BuildScholarship.GeneralInfo" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedIntermediary" Src="~/Common/SelectRelatedIntermediary.ascx" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedProvider" Src="~/Common/SelectRelatedProvider.ascx" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  

<span><span class="requiredAttributeIndicator">*</span> = required</span><br />
<div class="form-iceland-container">
    <div class="form-iceland two-columns">
        <p class="form-section-title">Complete your profile</p>
        
        <label for="ScholarshipNameControl">Scholarship Name:<span class="requiredAttributeIndicator">*</span></label>
 
        <asp:TextBox CssClass="longtextarea" ID="ScholarshipNameControl" MaxLength="100" runat="server"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfo1" Content="This is the Name of the Scholarship that will be displayed to Seekers. They will also be able to search for Scholarships by Name." runat="server" />
        
        <elv:PropertyProxyValidator ID="ScholarshipNameValidator" Display="Dynamic" runat="server"
            ControlToValidate="ScholarshipNameControl" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Scholarship" />

         <br />
        <label for="ScholarshipAcademicYearControl">
            Academic Year:<span class="requiredAttributeIndicator">*</span></label>
        
        <asp:DropDownList ID="ScholarshipAcademicYearControl" runat="server" />
        <sb:CoolTipInfo Content="Scholarships are presented to Seekers for the upcoming or current Academic year. Provider’s need to annually review and renew scholarship to ensure data is current and funds are available. Even if scholarships are awarded for more than a year, the scholarship must be recertified." runat="server" />
        <elv:PropertyProxyValidator ID="ScholarshipAcademicYearValidator" runat="server"
            ControlToValidate="ScholarshipAcademicYearControl" PropertyName="AcademicYear"
            SourceTypeName="ScholarBridge.Domain.Scholarship" />
     
        <br />
        <label for="MissionStatementControl">
            Mission Statement:<span class="requiredAttributeIndicator">*</span></label>
       
        <asp:TextBox ID="MissionStatementControl" runat="server" TextMode="MultiLine"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfoMissionStatement" runat="Server" ContentDiv="CoolTipInfoMissionStatementContents" />  
        <div id="CoolTipInfoMissionStatementContents" style="display:none;">
            The mission statement is the description of the scholarship that will display to seekers. 
            <br /><br />
            Note: This is not a narrative version of criteria but rather the inspiration that motivated the scholarship founder. It is limited to 250 words to provide focus.
        </div>
        <asp:RequiredFieldValidator ID="MissionStatementRequiredValidator" runat="server" ErrorMessage="Required. Please provide a description of the scholarship. This will be displayed to Seekers." ControlToValidate="MissionStatementControl" />
         <elv:PropertyProxyValidator ID="MissionStatementValidator" runat="server" ControlToValidate="MissionStatementControl"
            PropertyName="MissionStatement" SourceTypeName="ScholarBridge.Domain.Scholarship" />
    
        <br />
        <label for="ProgramGuidelinesControl">
            Program Guidelines:</label>
        <asp:TextBox ID="ProgramGuidelinesControl" runat="server" TextMode="MultiLine"></asp:TextBox>
        <sb:CoolTipInfo ID="CoolTipInfoProgramGuideLines" runat="Server" 
          ContentDiv="CoolTipInfoProgramGuideLinesContents" /> 
            <div id="CoolTipInfoProgramGuideLinesContents" style="display:none;">
            This information is NOT displayed to Seekers. These are instructions for the Scholarship Administrators involved in application review and award selection.
            <br /><br />
            EXAMPLE: Scholarship Applications to be reviewed and finalists selected by the Application Due Date. Finalists will be reviewed by the Scholarship Awards committee and offers extended within one week of Anticipated Award Date. Preference should be given to applicants with a history of community service and who have demonstrated leadership qualities.
            
        </div>
          <elv:PropertyProxyValidator ID="ProgramGuidelinesValidator" runat="server" ControlToValidate="ProgramGuidelinesControl"
            PropertyName="ProgramGuidelines" SourceTypeName="ScholarBridge.Domain.Scholarship" />
      
      
        <br />
        
        <hr />
        <P class="form-section-title">Donor Information</P>

        <label for="DonorName">
            Name:</label>
       
        <asp:TextBox ID="DonorName" runat="server"></asp:TextBox>
         <sb:CoolTipInfo ID="CoolTipInfo15" runat="Server" Content="Donor Information is not required but will be displayed to scholarship seekers. Complete if you wish to acknowledge a scholarship donor other than the provider." />  
         <elv:PropertyProxyValidator ID="DonorNameValidator" runat="server" ControlToValidate="DonorName"
            PropertyName="Name" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
        
         <br />
        <label for="AddressStreet">
            Address Line 1:</label>
       
        <asp:TextBox ID="AddressStreet" runat="server"></asp:TextBox>
         <elv:PropertyProxyValidator ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet"
            PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     
         <br />
        <label for="AddressStreet2">
            Address Line 2:</label>
        
        <asp:TextBox ID="AddressStreet2" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2"
            PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     
        <br />
        <label for="AddressCity">
            City:</label>
         
        <asp:TextBox ID="AddressCity" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity"
            PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
       
        <br />
        <label for="AddressState">
            State:</label>
      
        <asp:DropDownList ID="AddressState" runat="server">
        </asp:DropDownList>
          <elv:PropertyProxyValidator ID="AddressStateValidator" runat="server" ControlToValidate="AddressState"
            PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     
        <br />
        <label for="AddressPostalCode">
            Postal Code:</label>
        <asp:TextBox ID="AddressPostalCode" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode"
            PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
     

        <br />
        <label for="Phone">
            Phone:</label>
         <asp:TextBox ID="Phone" runat="server" CssClass="phone" />
       <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="Phone"
            PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
   
 
        <br />
        <label for="EmailAddress">
            Email Address:</label>
        <asp:TextBox ID="EmailAddress" runat="server"></asp:TextBox>
       <elv:PropertyProxyValidator ID="EmailAddressValidator" runat="server" ControlToValidate="EmailAddress"
            PropertyName="Email" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
        <asp:RegularExpressionValidator ID="emailLengthValidator" runat="server" ControlToValidate="EmailAddress"
            ValidationExpression="^.{0,50}$" ErrorMessage="Email Address is should be less than 50 characters long." />
     
   
         <br />
        <hr />

        <P class="form-section-title">Organizations</P>
        
        <label for="ProviderSelected">Provider:</label>
    
        <sb:SelectRelatedProvider ID="ProviderSelected" runat="server" />
        <sb:CoolTipInfo ID="CoolTipInfo4" runat="Server" Content="The Scholarship Provider is the Organization that manages the Scholarship funds.  The Provider Organization will have access in theWashBoard.org to administer the scholarship." />  
    <elv:PropertyProxyValidator ID="ProviderSelectedValidator" runat="server" ControlToValidate="ProviderSelected"
            PropertyName="Provider" SourceTypeName="ScholarBridge.Domain.Scholarship" OnValueConvert="providerValidator_OnValueConvert" />
   
        <br />
        
        <label for="IntermediarySelected">Intermediary:</label>
        <sb:SelectRelatedIntermediary ID="IntermediarySelected" runat="server" />
        <sb:CoolTipInfo ID="CoolTipInfo5" runat="Server" Content="The Scholarship Intermediary is an Organization that the Provider has established a relationship with (under Our Profile tab) and has enlisted to assist in the administration of the scholarship. The Intermediary Organization will have access in theWashBoard.org to administer the scholarship" />  


    </div>        
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">  
        <P class="form-section-title">Schedule</P>
     
        <label for="calAppStart">Application Start Date</label>
        <sb:CalendarControl ID="calApplicationStartDate" runat="server"></sb:CalendarControl>
        <sb:CoolTipInfo ID="CoolTipInfo6" runat="Server" Content="This date notifies seekers that you are accepting scholarship applications for the current academic year. If scholarship applications are accepted year round, then use today’s date. Applications can be submitted as soon as the scholarship has been activated." />  
        <elv:PropertyProxyValidator ID="ApplicationStartDateValidator" runat="server" ControlToValidate="calApplicationStartDate"
            PropertyName="ApplicationStartDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        
    
        <br />
        <label for="calApplicationDueDate">
            Application Due Date</label>
       <sb:CalendarControl ID="calApplicationDueDate" runat="server"></sb:CalendarControl>
        <sb:CoolTipInfo ID="CoolTipInfo7" runat="Server" Content="This is the date the scholarship applications are due.  Applications will not be accepted on-line after this date. Once the scholarship is activated, applications are notified that they have until this date to submit applications. The date can only be extended after scholarship is activated." />  
   <elv:PropertyProxyValidator ID="ApplicationDueDateValidator" runat="server" ControlToValidate="calApplicationDueDate"
            PropertyName="ApplicationDueDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
      
       
         <br />
        
        <label for="calAwardDate">Anticipated Award Date:</label>
      
        <sb:CalendarControl ID="calAwardDate" runat="server"></sb:CalendarControl>
        <sb:CoolTipInfo ID="CoolTipInfo8" runat="Server" Content="This is the date the provider anticipates notifying scholarship awardees. For information only." />  
     <elv:PropertyProxyValidator ID="AwardDateValidator" runat="server" ControlToValidate="calAwardDate"
            PropertyName="AwardDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
     
        <br />
        <hr />
     
        <P class="form-section-title">Scholarship Funding</P>
        
        
        <label for="MinimumNumberOfAwards">
            Minimum Number of Awards:<span class="requiredAttributeIndicator">*</span></label>
        
        <sandTrap:NumberBox ID="MinimumNumberOfAwards" runat="server" Precision="0" />
        <sb:CoolTipInfo ID="CoolTipInfo10" runat="Server" Content="The minimum number of individual scholarship awards anticipated. If not known, please estimate." />  
        <elv:PropertyProxyValidator ID="MinimumNumberOfAwardsValidator" runat="server" ControlToValidate="MinimumNumberOfAwards"
            PropertyName="MinimumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
  
        <br />
        
        <label for="MaximumNumberOfAwards">
            Maximum Number of Awards:<span class="requiredAttributeIndicator">*</span></label>
         <sandTrap:NumberBox ID="MaximumNumberOfAwards" runat="server" Precision="0" />
         <sb:CoolTipInfo ID="CoolTipInfo11" runat="Server" Content="The maximum number of individual scholarship awards anticipated. If not known please, estimate." />  
        <elv:PropertyProxyValidator ID="MaximumNumberOfAwardsValidator" runat="server" ControlToValidate="MaximumNumberOfAwards"
            PropertyName="MaximumNumberOfAwards" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
       
      
        <br />
        
        
        <label for="MinimumAmount">
            Minimum Scholarship Award Amount:<span class="requiredAttributeIndicator">*</span></label>
       
        <sandTrap:CurrencyBox ID="MinimumAmount" runat="server" Precision="0" />
        <sb:CoolTipInfo ID="CoolTipInfo13" runat="Server" Content="Estimated minimum individual award amount. Informational only." />  
       <elv:PropertyProxyValidator ID="MinimumAmountValidator" runat="server" ControlToValidate="MinimumAmount"
            PropertyName="MinimumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship" />
      
         <br />
        
        <label for="MaximumAmount">
            Maximum Scholarship Award Amount:<span class="requiredAttributeIndicator">*</span></label>
        
        <sandTrap:CurrencyBox ID="MaximumAmount" runat="server" Precision="0" />
        <sb:CoolTipInfo ID="CoolTipInfo14" runat="Server" Content="Estimated maximum individual award amount. Informational only." />  
       <elv:PropertyProxyValidator ID="MaximumAmountValidator" runat="server" ControlToValidate="MaximumAmount"
            PropertyName="MaximumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship" />
   
        <br />
        <label for="AnnualSupportAmount">
            Total Funds Available:<span class="requiredAttributeIndicator">*</span></label>
       
        <sandTrap:CurrencyBox ID="AnnualSupportAmount" runat="server" Precision="0" />
         <sb:CoolTipInfo ID="CoolTipInfo9" runat="Server" Content="The total pool of scholarship funds available this academic year.  If not known, please estimate." />  
         <elv:PropertyProxyValidator ID="AnnualSupportAmountValidator" runat="server" ControlToValidate="AnnualSupportAmount"
            PropertyName="AnnualSupportAmount" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.FundingParameters" />
      
          <br />
          
          <label for="TermsOfSupport">
            Support Period:<span class="requiredAttributeIndicator">*</span></label>
        <asp:RadioButtonList ID="TermsOfSupportControl" runat="server" class="control-set" />
        <sb:CoolTipInfo ID="CoolTipInfo12" runat="Server" Content="The period of time that the scholarship is intended to assist with.  Informational only." />  
        <br />
        
    </div>
</div>