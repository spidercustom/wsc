﻿using System;

namespace ScholarBridge.Web.Provider
{
    public partial class Anonymous : System.Web.UI.Page
    {
		private LinkGenerator linkGenerator;
		protected LinkGenerator LinkGenerator
		{
			get
			{
				if (linkGenerator == null)
					linkGenerator = new LinkGenerator();
				return linkGenerator;
			}
		}
		protected void Page_Load(object sender, EventArgs e)
        {
			loginForm.LinkTo = LinkGenerator.GetFullLink("/Provider/RegisterProvider.aspx");
        }
    }
}
