using System;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class ActivateIntermediaryAction : IAction
    {
        public IIntermediaryService IntermediaryService { get; set; }
        public IMessageService MessageService { get; set; }

        public bool SupportsApprove { get { return true; } }

        public bool SupportsReject { get { return true; } }
        public string LastStatus { get; set; }
       
        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
            if (! (message is OrganizationMessage))
                throw new ArgumentException("Message must be an OrganizationMessage");

            var org = ((OrganizationMessage) message).RelatedOrg;
            org.LastUpdate = new ActivityStamp(approver);
            IntermediaryService.Approve((Intermediary)org);

            message.ActionTaken = MessageAction.Approve;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Intermediary Registration Approved";
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
            if (!(message is OrganizationMessage))
                throw new ArgumentException("Message must be an OrganizationMessage");

            var org = ((OrganizationMessage)message).RelatedOrg;
            org.LastUpdate = new ActivityStamp(approver);
            IntermediaryService.Reject((Intermediary)org);

            message.ActionTaken = MessageAction.Deny;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Intermediary Registration Denied";
        }
    }
}