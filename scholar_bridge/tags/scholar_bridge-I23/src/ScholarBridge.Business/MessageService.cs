using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class MessageService : IMessageService
    {
        public IMessageDAL MessageDAL { get; set; }
        public ISentMessageDAL SentMessageDAL { get; set; }

        public Message FindMessage(User user, Organization organization, int id)
        {
            return MessageDAL.FindById(user, user.Roles, organization, id);
        }

        public SentMessage FindSentMessage(User user, Organization organization, int id)
        {
            return SentMessageDAL.FindById(user, user.Roles, organization, id);
        }

        public IList<Message> FindAll(User user, Organization organization)
        {
            return MessageDAL.FindAll(user, user.Roles, organization);
        }

        public IList<Message> FindAll(MessageAction messageAction, User user, Organization organization)
        {
            return MessageDAL.FindAll(messageAction, user, user.Roles, organization);
        }

        public IList<SentMessage> FindAllSent(User user, Organization organization)
        {
            return SentMessageDAL.FindAll(user, user.Roles, organization);
        }

        public IList<Message> FindAllArchived(User user, Organization organization)
        {
			return MessageDAL.FindAllArchived(user, user.Roles, organization);
		}

    	public IList<Message> FindAllApprovedScholarships(User user, Organization organization)
    	{
            return MessageDAL.FindAllByMessageType(MessageType.ScholarshipApproved, user, user.Roles, organization);
    	}

    	public IList<Message> FindAllWithoutApprovedScholarships(User user, Organization organization)
    	{
			return MessageDAL.FindAll(user, user.Roles, organization, new List<MessageType> {MessageType.ScholarshipApproved});
		}

    	public int CountUnread(User user, Organization organization)
        {
            return MessageDAL.CountUnread(user, user.Roles, organization);
        }
        public void SendMessage(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");

            MessageDAL.Insert(message);
            SentMessageDAL.Insert(new SentMessage(message) { LastUpdate = message.LastUpdate });
        }

        public void ArchiveMessage(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");

            if (! message.IsArchived)
            {
                message.IsArchived = true;
                MessageDAL.Update(message);
            }
        }

        public void MarkMessageRead(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");

            if (! message.IsRead)
            {
                message.IsRead = true;
                MessageDAL.Update(message);
            }
        }

        public void DeletedRelatedMessages(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            MessageDAL.DeleteRelated(scholarship);
        }

        public void SaveNewSentMessage(Message message)
        {
            if (null == message)
                throw new ArgumentNullException("message");
            SentMessageDAL.Insert(new SentMessage(message) { LastUpdate = message.LastUpdate });
        }
    }
}