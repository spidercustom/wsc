﻿
using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class RelationshipRequesterType : EnumStringType
    {
        public RelationshipRequesterType()
            : base(typeof(RelationshipRequester), 30)
        {
        }
    }
}