using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Domain.Messaging;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class MessageDALBase<T> : AbstractDAL<T> where T : IMessage
    {
        protected AbstractCriterion CreateInCriteriaForMessageQuery<T2>(string criteriaName, IEnumerable<T2> values)
        {
            if (null != values)
                return Restrictions.Or(Restrictions.In(criteriaName, values.ToArray()), Restrictions.IsNull(criteriaName));

            return Restrictions.IsNull(criteriaName);
        }

        protected AbstractCriterion CreateCriteriaForMessageQuery(string criteriaName, object value)
        {
            if (null != value)
                return Restrictions.Or(Restrictions.Eq(criteriaName, value), Restrictions.IsNull(criteriaName));

            return Restrictions.IsNull(criteriaName);
        }
    }
}