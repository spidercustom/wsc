﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Domain.ScholarshipParts
{
    //TODO: Write unit tests for this type
    public class ScholarshipAttributeCollection<TAttribute> : ICollection, ICloneable
    {

        protected virtual IList<ScholarshipAttributeUsage<TAttribute>> InternalAttributesUsage { get; set; }
        public ScholarshipAttributeCollection()
        {
            InternalAttributesUsage = new List<ScholarshipAttributeUsage<TAttribute>>();
        }

        #region element CRUD
        public ScholarshipAttributeUsage<TAttribute> this[int index]
        {
            get { return InternalAttributesUsage[index]; }
            set { InternalAttributesUsage[index] = value; }
        }



        public virtual void Add(ScholarshipAttributeUsage<TAttribute> attributeUsage)
        {
            var existing = Find(attributeUsage.Attribute);
            if (null == existing)
            {
                InternalAttributesUsage.Add(attributeUsage);
                OnAttributeAdded(attributeUsage);
                return;
            }
            existing.UsageType = attributeUsage.UsageType;
        }

        public virtual IList<ScholarshipAttributeUsage<TAttribute>> GetReadOnlyList()
        {
            return new ReadOnlyCollection<ScholarshipAttributeUsage<TAttribute>>(InternalAttributesUsage);
        }

        public virtual bool HasAttribute(TAttribute attribute)
        {
            return InternalAttributesUsage.Any(o => o.Attribute.Equals(attribute));
        }

        public virtual ScholarshipAttributeUsage<TAttribute> Find(TAttribute attribute)
        {
            return InternalAttributesUsage.FirstOrDefault(o => o.Attribute.Equals(attribute));
        }

        public virtual ScholarshipAttributeUsageType GetUsageType(TAttribute attribute)
        {
            var attributeUsage = Find(attribute);
            if (null == attributeUsage)
                return ScholarshipAttributeUsageType.NotUsed;
            return attributeUsage.UsageType;
        }

        public virtual void Remove(TAttribute attribute)
        {
            var attributeUsage = Find(attribute);
            if (null != attributeUsage)
            {
                InternalAttributesUsage.Remove(attributeUsage);
                OnAttributeRemoved(attributeUsage);
            }
        }
        #endregion

        #region events
        public class AttributeUsageEventArgs : EventArgs
        {
            public AttributeUsageEventArgs(ScholarshipAttributeUsage<TAttribute> usage)
            {
                Usage = usage;
            }
            public ScholarshipAttributeUsage<TAttribute> Usage { get; set; }
        }


        public event EventHandler<AttributeUsageEventArgs> Removed;
        protected void OnAttributeRemoved(ScholarshipAttributeUsage<TAttribute> attribute)
        {
            if (null != Removed)
                Removed(this, new AttributeUsageEventArgs(attribute));
        }

        public event EventHandler<AttributeUsageEventArgs> Added;
        private object syncRoot = new object();

        protected void OnAttributeAdded(ScholarshipAttributeUsage<TAttribute> attribute)
        {
            if (null != Added)
                Added(this, new AttributeUsageEventArgs(attribute));
        }
        #endregion

        #region ICollection implimentation
        public IEnumerator GetEnumerator()
        {
            return InternalAttributesUsage.GetEnumerator();
        }

        public void CopyTo(Array array, int index)
        {
            InternalAttributesUsage.CopyTo((ScholarshipAttributeUsage<TAttribute>[]) array, index);
        }

        public int Count
        {
            get { return InternalAttributesUsage.Count; }
        }

        public object SyncRoot
        {
            get { return syncRoot; }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }
        #endregion

        #region IClonable implimentation
        public object Clone()
        {
            var result = new ScholarshipAttributeCollection<TAttribute>();
            InternalAttributesUsage.ForEach(o => result.InternalAttributesUsage.Add(o));
            return result;
        }
        #endregion
    }
}
