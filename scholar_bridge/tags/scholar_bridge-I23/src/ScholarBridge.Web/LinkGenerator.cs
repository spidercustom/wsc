using System;
using System.Configuration;
using System.Text;
using System.Web;
using ScholarBridge.Business.Messaging;

namespace ScholarBridge.Web
{
    public class LinkGenerator : ILinkGenerator
    {
        /// <summary>
        /// Gets the url of the Proxy website - needed when running behind the DIS Fortress Firewall
        /// </summary>
        public static string ProxyURL
        {
            get
            {
                var reader = new AppSettingsReader();
                return reader.GetValue("ProxyURL", typeof(string)).ToString();
            }
        }

        public string UrlEncode(string toEncode)
        {
            return HttpUtility.UrlEncode(toEncode);
        }

        public string GetFullLink(string relativePath)
        {
            return GetFullLinkStatic(relativePath, null);
        }

        public string GetFullLink(string relativePath, string queryString)
        {
            // check for imbedded query string because the '?' will get converted to %3f otherwise
            return GetFullLinkStatic(relativePath, queryString);
        }

        public static string GetFullLinkStatic(string relativePath)
        {
            return GetFullLinkStatic(relativePath, null);
        }

        public static string GetFullLinkStatic(string relativePath, string queryString)
        {
            if (string.IsNullOrEmpty(queryString))
            {
                if (relativePath.Contains("?"))
                {
                    string[] urlParts = relativePath.Split('?');
                    if (urlParts.Length > 1)
                    {
                        relativePath = urlParts[0];
                        queryString = urlParts[1];
                    }
                }
            }
            if (!ShouldUseProxyURL())
                return BuildURLFromContext(relativePath, queryString);

            return BuildRelativePath(ProxyURL.Trim(), relativePath, queryString);
        }

        private static string BuildURLFromContext(string path, string queryString)
        {
            // need to include the application path before building the link
            string fullPath = BuildRelativePath(HttpContext.Current.Request.ApplicationPath, path, "");
            return BuildUrl(HttpContext.Current.Request.Url.ToString(), fullPath, queryString);
        }

        /// <summary>
        /// Set the url imbedded in emails based on the web.config value for the proxy web-site url.
        /// </summary>
        /// <param name="applicationPath">proxy url</param>
        /// <param name="path">path to the page relative to the root</param>
        /// <param name="queryString">any query string values</param>
        /// <returns></returns>
        private static string BuildRelativePath(string applicationPath, string path, string queryString)
        {
            var sb = new StringBuilder(applicationPath);
            if (!applicationPath.EndsWith("/") && !path.StartsWith("/"))
                sb.Append("/");

			// prevent doubled slashes (when running under dev server)
            if (applicationPath.EndsWith("/") && path.StartsWith("/"))
                path = path.Substring(1);

            sb.Append(path);
            if (!(queryString == null))
            {
                if (!(queryString.Trim() == string.Empty))
                {
                    sb.Append("?");
                    sb.Append(queryString);
                }
            }
            return sb.ToString();
        }

        private static string BuildUrl(string baseUrl, string relativePath, string queryString)
        {
            var builder = new UriBuilder(baseUrl) { Query = queryString, Path = relativePath };
            return builder.Uri.AbsoluteUri;
        }

        private static bool ShouldUseProxyURL()
        {
            return ProxyURL != null && ProxyURL.Trim() != string.Empty;
        }
    }
}
