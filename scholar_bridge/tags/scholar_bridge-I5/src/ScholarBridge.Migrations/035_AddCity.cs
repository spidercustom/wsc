﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(35)]
    public class AddCity : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "City";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
