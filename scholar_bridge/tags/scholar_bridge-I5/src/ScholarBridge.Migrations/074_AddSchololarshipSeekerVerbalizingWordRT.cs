﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(74)]
    public class AddSchololarshipSeekerVerbalizingWordRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBScholarship"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSeekerVerbalizingWordLUT"; }
        }
    }
}
