﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations.Common
{
    public static class LookupTableHelper
    {
        private const string LOOKUP_TABLE_NAME_TEMPLATE = "SB{0}LUT";
        public static string CreateTableName(string indicativeName)
        {
            return string.Format(LOOKUP_TABLE_NAME_TEMPLATE, indicativeName);
        }


        private const string LOOKUP_TABLE_PRIMARY_KEY_COLUMN_TEMPLATE = "SB{0}Index";
        public static string CreatePrimaryKeyColumnName(string indicativeName)
        {
            return string.Format(LOOKUP_TABLE_PRIMARY_KEY_COLUMN_TEMPLATE, indicativeName);
        }

        public static Column[] MergeNameWithStandardFields(string indicatedName, int columnSize)
        {
            return MergeNameWithStandardFields(indicatedName,
                                        new Column(indicatedName, DbType.String, columnSize, ColumnProperty.NotNull));
        }

        public static Column[] MergeNameWithStandardFields(string indicatedName, Column nameColumn)
        {
            return new[]
            {
                new Column(CreatePrimaryKeyColumnName(indicatedName), DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                nameColumn,
                new Column("Description", DbType.String, 50, ColumnProperty.Null),
                new Column("Deprecated", DbType.Boolean, ColumnProperty.NotNull, 0),
                new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())")
            };
        }
    }
}
