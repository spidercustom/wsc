using System;
using System.Text;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Common.Extensions;
using Spider.Common.Extensions.System.Text;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class StringBuilderExtensionTests
    {
        [Test]
        public void ends_with_finds_character()
        {
            var sb = new StringBuilder("foo;");
            Assert.That(sb.EndsWith(';'), Is.True);
            Assert.That(sb.EndsWith(' '), Is.False);
            Assert.That(sb.EndsWith('f'), Is.False);
        }

        [Test]
        public void can_build_string()
        {
            Assert.That("{0} foo".Build("hello"), Is.EqualTo("hello foo"));
        }

        [Test]
        public void create_nullable_double_test()
        {
            Assert.AreEqual(null, string.Empty.CreateNullableDouble());
            Assert.AreEqual(0d, "0".CreateNullableDouble());

            Assert.AreEqual(0.123d, "0.123".CreateNullableDouble());
        }

        [Test]
        [ExpectedException(typeof(FormatException))]
        public void create_nullable_incorrect_format_test()
        {
            "A".CreateNullableDouble();
        }
    }
}