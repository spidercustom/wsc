using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;
using System.Linq;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class SeekerAddressTests
    {
        [Test]
        public void ToStringProperlyFormatsSeekerAddressWithStreet1Only()
        {
            var a = new SeekerAddress
                        {
                            Street = "123 Foo St",
                            City = new City() { Name = "Milwaukee" },
                            State = new State {Abbreviation = "WI"},
                            PostalCode = "53212"
                        };

            var expected = @"123 Foo St
Milwaukee, WI 53212";

            Assert.AreEqual(expected, a.ToString());
        }

        [Test]
        public void ToStringProperlyFormatsSeekerAddressWithBothStreets()
        {
            var a = new SeekerAddress
            {
                Street = "123 Foo St",
                Street2 = "Apt 2",
                City = new City() { Name = "Milwaukee" },
                State = new State { Abbreviation = "WI" },
                PostalCode = "53212"
            };

            var expected = @"123 Foo St
Apt 2
Milwaukee, WI 53212";

            Assert.AreEqual(expected, a.ToString());
        }

        [Test]
        public void validate_unfilled_Seekeraddress_required_ruleset()
        {
            var address = new SeekerAddress();
            var results = address.ValidateAsRequired();
            Assert.That(results.Count, Is.EqualTo(4));
            Assert.That(results.Any(result => result.Key.Equals("Street") && result.Validator is StringLengthValidator));
            Assert.That(results.Any(result => result.Key.Equals("City") && result.Validator is NotNullValidator));
            Assert.That(results.Any(result => result.Key.Equals("PostalCode") && result.Validator is StringLengthValidator));
            Assert.That(results.Any(result => result.Key.Equals("State") && result.Validator is NotNullValidator));
        }

        [Test]
        public void validate_fill_address_with_required_ruleset()
        {
            var address = new SeekerAddress { Street = "Line-1", City = new City() { Name = "Milwaukee" }, PostalCode = "12345", State = new State() };
            var results = address.ValidateAsRequired();
            Assert.That(results.Count, Is.EqualTo(0));
            Assert.That(results.IsValid, Is.True);
        }
    }
}