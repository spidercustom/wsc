﻿using NUnit.Framework;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class FundingProfileTests
    {

        private FundingProfileAttributeUsage CreateMinimumUsageTypeAttribute(FundingProfileAttribute attribute)
        {
            return new FundingProfileAttributeUsage
            {
                Attribute = attribute,
                UsageType = ScholarshipAttributeUsageType.Minimum
            };
        }

        [Test]
        public void test_has_attribute()
        {
            var fundingProfile = new FundingProfile();
            Assert.IsFalse(fundingProfile.HasAttributeUsage(FundingProfileAttribute.FundingParameters));
            fundingProfile.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.FundingParameters));

            Assert.IsTrue(fundingProfile.HasAttributeUsage(FundingProfileAttribute.FundingParameters));
        }

        [Test]
        public void test_clone_implimentation_exists()
        {
            var fundingProfile = new FundingProfile();
            fundingProfile.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.FundingParameters));

            var clonedCriteria = (FundingProfile)fundingProfile.Clone();
            Assert.AreEqual(fundingProfile.AttributesUsage.Count, clonedCriteria.AttributesUsage.Count);
            Assert.AreEqual(fundingProfile.AttributesUsage[0].Attribute, clonedCriteria.AttributesUsage[0].Attribute);
            Assert.AreEqual(fundingProfile.AttributesUsage[0].UsageType, clonedCriteria.AttributesUsage[0].UsageType);
        }

        [Test]
        public void remove_attribute()
        {
            var criteria = new FundingProfile();
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.FundingParameters));
            Assert.AreEqual(1, criteria.AttributesUsage.Count);
            criteria.RemoveAttributeUsage(FundingProfileAttribute.Need);
            Assert.AreEqual(1, criteria.AttributesUsage.Count);
            criteria.RemoveAttributeUsage(FundingProfileAttribute.FundingParameters);
            Assert.AreEqual(0, criteria.AttributesUsage.Count);
        }

        [Test]
        public void find_attribute_usage()
        {
            var criteria = new FundingProfile();
            Assert.IsNull(criteria.FindAttributeUsage(FundingProfileAttribute.Need));
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.FundingParameters));

            Assert.IsNull(criteria.FindAttributeUsage(FundingProfileAttribute.Need));
            Assert.IsNotNull(criteria.FindAttributeUsage(FundingProfileAttribute.FundingParameters));
        }

        [Test]
        public void get_attribute_usage_type()
        {
            var criteria = new FundingProfile();
            Assert.AreEqual(ScholarshipAttributeUsageType.NotUsed, criteria.GetAttributeUsageType(FundingProfileAttribute.Need));

            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.FundingParameters));
            Assert.AreEqual(ScholarshipAttributeUsageType.NotUsed, criteria.GetAttributeUsageType(FundingProfileAttribute.Need));
            Assert.AreEqual(ScholarshipAttributeUsageType.Minimum, criteria.GetAttributeUsageType(FundingProfileAttribute.FundingParameters));
        }
    }
}
