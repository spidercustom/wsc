using NUnit.Framework;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class MatchTests
    {
        [Test]
        public void scholarship_name_null_by_default()
        {
            var m = new Match();
            Assert.IsNull(m.ScholarshipName);
        }

        [Test]
        public void scholarship_name_same_as_scholarship()
        {
            var s = new Scholarship {Name = "Test"};
            var m = new Match {Scholarship = s};
            Assert.AreEqual(s.Name, m.ScholarshipName);
        }

        [Test]
        public void criteria_percents_handle_zero()
        {
            var m = new Match();
            Assert.AreEqual(1, m.MinimumCriteriaPercent());
            Assert.AreEqual(1, m.PreferredCriteriaPercent());
        }

        [Test]
        public void criteria_percents_handle_val()
        {
            var m = new Match
                        {
                            SeekerMinimumCriteriaCount = 2,
                            ScholarshipMinimumCriteriaCount = 4,
                            SeekerPreferredCriteriaCount = 2,
                            ScholarshipPreferredCriteriaCount = 10
                        };
            Assert.AreEqual(.5, m.MinimumCriteriaPercent(), 0.001);
            Assert.AreEqual(.2, m.PreferredCriteriaPercent(), 0.001);
        }

        [Test]
        public void criteria_percents_to_strings()
        {
            var m = new Match
            {
                SeekerMinimumCriteriaCount = 2,
                ScholarshipMinimumCriteriaCount = 4,
                SeekerPreferredCriteriaCount = 2,
                ScholarshipPreferredCriteriaCount = 10
            };
            Assert.AreEqual("2 of 4", m.MinumumCriteriaString);
            Assert.AreEqual("2 of 10", m.PreferredCriteriaString);
        }
    }
}