using System;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class ActivateScholarshipAction : IAction
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IMessageService MessageService { get; set; }

        public bool SupportsApprove { get { return true; } }

        public bool SupportsReject { get { return true; } }

        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
            if (! (message is ScholarshipMessage))
                throw new ArgumentException("Message must be a ScholarshipMessage");

            var scholarship = ((ScholarshipMessage)message).RelatedScholarship;
            scholarship.LastUpdate = new ActivityStamp(approver);
            ScholarshipService.Approve(scholarship, approver);

            message.ActionTaken = MessageAction.Approve;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
            if (!(message is ScholarshipMessage))
                throw new ArgumentException("Message must be a ScholarshipMessage");

            var scholarship = ((ScholarshipMessage)message).RelatedScholarship;
            scholarship.LastUpdate = new ActivityStamp(approver);
            ScholarshipService.Reject(scholarship, approver);

            message.ActionTaken = MessageAction.Deny;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
        }
    }
}