using System;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public class SeekerService : ISeekerService
    {
        public ISeekerDAL SeekerDAL { get; set; }
        public IRoleDAL RoleService { get; set; }
        public IUserService UserService { get; set; }

        public void SaveNew(Seeker seeker)
        {
            if (null == seeker)
            {
                throw new ArgumentNullException("seeker", "Seeker can not be null");
            }
			if (seeker.User.Id < 1)
			{
				UserService.Insert(seeker.User);
			}

            seeker.User.Roles.Add(RoleService.FindByName(Role.SEEKER_ROLE));
            UserService.Update(seeker.User);
			seeker.LastUpdate = new ActivityStamp() {By = seeker.User, On = DateTime.Now};
            SeekerDAL.Insert(seeker);

            UserService.SendConfirmationEmail(seeker.User, false);
        }

        public void Update(Seeker seeker)
        {
            SeekerDAL.Update(seeker);
        }

    	public Seeker FindByUser(User user)
    	{
    		return SeekerDAL.FindByUser(user);
    	}
    }
}
