using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class IntermediaryService : OrganizationService<Intermediary>, IIntermediaryService
    {
        protected override void AddRolesToNewUser(User user, bool isAdmin)
        {
            user.Roles.Add(RoleDAL.FindByName(Role.INTERMEDIARY_ROLE));
            if (isAdmin)
                user.Roles.Add(RoleDAL.FindByName(Role.INTERMEDIARY_ADMIN_ROLE));
        }

        protected override MessageType TemplateForApproval
        {
            get { return MessageType.IntermediaryApproved; }
        }

        protected override MessageType TemplateForRejection
        {
            get { return MessageType.IntermediaryRejected; }
        }
    }
}