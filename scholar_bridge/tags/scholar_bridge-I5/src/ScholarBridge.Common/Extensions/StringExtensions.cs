﻿namespace ScholarBridge.Common.Extensions
{
    public static class StringExtensions
    {
        public static string Build(this string stringToFormat, params object[] args)
        {
            return string.Format(stringToFormat, args);
        }

        public static double? CreateNullableDouble(this string stringToConvert)
        {
            double? result;
            if (string.IsNullOrEmpty(stringToConvert))
                result = null;
            else
                result = double.Parse(stringToConvert);

            return result;
        }
    }
}
