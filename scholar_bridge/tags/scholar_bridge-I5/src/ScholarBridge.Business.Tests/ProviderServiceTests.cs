using System;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using Rhino.Mocks;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ProviderServiceTests
    {
        private MockRepository mocks;
        private ProviderService providerService;
        private IProviderDAL providerDAL;
        private IRoleDAL roleDAL;
        private IUserService userService;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            providerDAL = mocks.StrictMock<IProviderDAL>();
            roleDAL = mocks.StrictMock<IRoleDAL>();
            userService = mocks.StrictMock<IUserService>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();
            providerService = new ProviderService
                                  {
                                      OrganizationDAL = providerDAL,
                                      RoleDAL = roleDAL,
                                      UserService = userService,
                                      MessagingService = messagingService,
                                      TemplateParametersService = templateParametersService
                                  };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(providerDAL);
            mocks.BackToRecord(roleDAL);
            mocks.BackToRecord(userService);
            mocks.BackToRecord(messagingService);
        }

        [Test]
        public void save_new_provider_sets_user_as_admin_user_with_proper_roles()
        {
            // TODO: As more things are added, mock them out as well
            var p = new Provider {Name = "TestProvider"};
            var user = new User {Username = "NewUser"};
            Expect.Call(providerDAL.Insert(p)).Return(p);
            Expect.Call(roleDAL.FindByName(Role.PROVIDER_ROLE)).Return(new Role { Id = 3, Name = Role.PROVIDER_ROLE });
            Expect.Call(roleDAL.FindByName(Role.PROVIDER_ADMIN_ROLE)).Return(new Role { Id = 4, Name = Role.PROVIDER_ADMIN_ROLE });
			Expect.Call(() => userService.Insert(user));
			Expect.Call(() => userService.Update(user));
			Expect.Call(() => userService.SendConfirmationEmail(user, false));
            mocks.ReplayAll();

            providerService.SaveNewWithAdminUser(p, user);
            mocks.VerifyAll();

            Assert.AreEqual(user, p.AdminUser);
            CollectionAssert.IsNotEmpty(user.Roles);
            CollectionAssert.AllItemsAreNotNull(user.Roles);
        }

        [Test]
        public void save_new_user_sets_up_roles_and_adds_user()
        {
            // TODO: As more things are added, mock them out as well
            var provider = new Provider { Name = "Test Provider" };
            var user = new User { Username = "NewUser" };
            Expect.Call(providerDAL.Update(provider)).Return(provider);
            Expect.Call(roleDAL.FindByName(Role.PROVIDER_ROLE)).Return(new Role { Id = 3, Name = Role.PROVIDER_ROLE });
            Expect.Call(() => userService.SendConfirmationEmail(user, false));
            mocks.ReplayAll();

            providerService.SaveNewUser(provider, user);
            mocks.VerifyAll();

            CollectionAssert.IsNotEmpty(provider.ActiveUsers);
            CollectionAssert.IsNotEmpty(user.Roles);
            CollectionAssert.AllItemsAreNotNull(user.Roles);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_new_user_throws_exception_if_org_null()
        {
            var user = new User { Username = "NewUser" };
            providerService.SaveNewUser(null, user);
            Assert.Fail();
        }

        [Test]
        public void delete_user_passes_to_user_dal()
        {
            var provider = new Provider { Name = "Test Provider" };
            var user = new User { Id = 1, Username = "NewUser" };
            provider.Users.Add(user);
            Expect.Call(() => userService.Delete(user));
            mocks.ReplayAll();

            providerService.DeleteUser(provider, user);
            mocks.VerifyAll();
        }

        [Test]
        public void reactivate_user_passes_to_user_dal()
        {
            var provider = new Provider { Name = "Test Provider" };
            var user = new User { Id = 1, Username = "NewUser" };
            provider.Users.Add(user);
            Expect.Call(() => userService.Update(user));
            mocks.ReplayAll();

            providerService.ReactivateUser(provider, user);
            Assert.That(user.IsDeleted, Is.False);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveNullThrowsException()
        {
            providerService.SaveNewWithAdminUser(null, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateNullThrowsException()
        {
            providerService.Update(null);
            Assert.Fail();
        }

        [Test]
        public void ApproveProvider()
        {
            // TODO: As more things are added, mock them out as well
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };

            Expect.Call(providerDAL.Update(p)).Return(p);
            Expect.Call(() => templateParametersService.OrganizationApproved(Arg<User>.Is.Equal(p.AdminUser), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageFromAdmin(Arg<OrganizationMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));

            mocks.ReplayAll();

            providerService.Approve(p);

            Assert.AreEqual(ApprovalStatus.Approved, p.ApprovalStatus);
            Assert.IsTrue(p.AdminUser.IsApproved);
            mocks.VerifyAll();
        }

        [Test]
        public void RejectProvider()
        {
            // TODO: As more things are added, mock them out as well
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };

            Expect.Call(providerDAL.Update(p)).Return(p);
            Expect.Call(() => templateParametersService.OrganizationRejected(Arg<User>.Is.Equal(p.AdminUser), Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageFromAdmin(Arg<OrganizationMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => userService.Delete(p.AdminUser));
            mocks.ReplayAll();

            providerService.Reject(p);

            Assert.AreEqual(ApprovalStatus.Rejected, p.ApprovalStatus);
            mocks.VerifyAll();
        }

        [Test]
        public void Submit_Change_Request()
        {
            // TODO: As more things are added, mock them out as well
            var p = new Provider { Name = "TestProvider", AdminUser = new User { Name = new PersonName { FirstName = "TestName" } } };


            Expect.Call(() => templateParametersService.ListChangeRequest(Arg<Organization>.Is.Equal(p), Arg<User>.Is.Equal(p.AdminUser), Arg<string>.Is.Anything, Arg<string>.Is.Anything, Arg<string>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessageToAdmin(Arg<OrganizationMessage>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));

            mocks.ReplayAll();

            providerService.SubmitListChangeRequest(p, p.AdminUser, "test", "test", "test");

            mocks.VerifyAll();
        }
        
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SubmitListChangeRequiresNonNullProvider()
        {
            providerService.SubmitListChangeRequest(null,new User(){Id=1},"test","test","test");
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SubmitListChangeRequiresNonNullUser()
        {
            providerService.SubmitListChangeRequest(new Provider() { Id = 1 },null , "test", "test", "test");
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ApprovalRequiresNonNullProvider()
        {
            providerService.Approve(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RejectRequiresNonNullProvider()
        {
            providerService.Reject(null);
            Assert.Fail();
        }
    }
}