using System.Collections.Generic;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IMatchDAL : IDAL<Match>
    {
        IList<Match> FindAll(Seeker seeker);
        IList<Match> FindAll(Seeker seeker, MatchStatus status);

        Match Find(Seeker seeker, int scholarshipId);

        void UpdateMatches(Seeker seeker);
        void UpdateMatches(Scholarship newScholarship);
    }
}