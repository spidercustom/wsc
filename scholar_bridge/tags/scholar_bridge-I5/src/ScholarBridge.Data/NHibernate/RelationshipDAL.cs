﻿using System;
using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;
namespace ScholarBridge.Data.NHibernate
{
    public class RelationshipDAL : AbstractDAL<Relationship>, IRelationshipDAL
    {
        #region IRelationshipDAL Members

        public Relationship FindById(int id)
        {
            return UniqueResult("Id", id);
        }
       
        public IList<Relationship> FindByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            return CreateCriteria().Add(Restrictions.Eq("Provider", provider)).List<Relationship>(); 
        }

        public IList<Relationship> FindByIntermediary(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return CreateCriteria().Add(Restrictions.Eq("Intermediary", intermediary)).List<Relationship>(); 
        }

        public Relationship FindByProviderandIntermediary(Provider provider, Intermediary intermediary)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return CreateCriteria().Add(Restrictions.Eq("Provider", provider)).Add(Restrictions.Eq("Intermediary", intermediary)).UniqueResult<Relationship>();
        }

        #endregion

        public Relationship Save(Relationship relationship)
        {
            if (relationship == null)
                throw new ArgumentNullException("relationship");

            return relationship.Id < 1 ?
                Insert(relationship) :
                Update(relationship);
        }
       
       
       
    }
}
