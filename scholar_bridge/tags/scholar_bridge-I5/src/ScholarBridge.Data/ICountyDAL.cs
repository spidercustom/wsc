﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ICountyDAL : IGenericLookupDAL<County>
    {
        IList<County> FindByState(string stateAbbreviation);
    }
}
