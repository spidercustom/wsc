﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain
{
    public class RangeCondition<T>
    {

        public RangeCondition() {}
        public RangeCondition(T min, T max)
        {
            Minimum = min;
            Maximum = max;
        }

        public virtual T Minimum { get; set; }

        [PropertyComparisonValidator("Minimum", ComparisonOperator.GreaterThanEqual)]
        public virtual T Maximum { get; set; }
    }
}
