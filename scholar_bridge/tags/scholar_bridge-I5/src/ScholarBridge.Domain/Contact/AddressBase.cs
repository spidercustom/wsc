﻿using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Location;
using Microsoft.Practices.EnterpriseLibrary.Validation;

namespace ScholarBridge.Domain.Contact
{
    public abstract class AddressBase
    {
        public const string ADDRESS_REQUIRED = "Address-Required";

        [StringLengthValidator(0, 50, MessageTemplate = "Street must be less than 50 characters.")]
        [StringLengthValidator(1, 50, MessageTemplate = "Street must be specified and must be less than 50 characters.", Ruleset = ADDRESS_REQUIRED)]
        public virtual string Street { get; set; }

        [StringLengthValidator(0, 50, MessageTemplate = "Street2 must be less than 50 characters.")]
        public virtual string Street2 { get; set; }

        [StringLengthValidator(0, 50, MessageTemplate = "City must be less than 50 characters.")]
        [StringLengthValidator(1, 50, MessageTemplate = "City must be specified and must be less than 50 characters.",
            Ruleset = ADDRESS_REQUIRED)]
        
        public abstract string GetCity { get; }

        [StringLengthValidator(0, 10, MessageTemplate = "Postal Code must be less than 10 characters.")]
        [StringLengthValidator(5, 10, MessageTemplate = "Postal Code must be at-least 5 charactors and must be less than 10 characters.", Ruleset = ADDRESS_REQUIRED)]
        public virtual string PostalCode { get; set; }

        [NotNullValidator(MessageTemplate = "State must be specified", Ruleset = ADDRESS_REQUIRED)]
        public virtual State State { get; set; }

        public abstract ValidationResults ValidateAsRequired();
       

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (null != Street)
            {
                sb.AppendLine(Street);
            }

            if (null != Street2)
            {
                sb.AppendLine(Street2);
            }

            
            if (!string.IsNullOrEmpty(GetCity))
            {
                sb.Append(GetCity);
                if (null != State)
                {
                    sb.Append(", ");
                }
            }

            if (null != State)
            {
                sb.Append(State.Abbreviation);
            }

            if (null != PostalCode)
            {
                sb.Append(" ").Append(PostalCode);
            }

            return sb.ToString();
        }
    }
}