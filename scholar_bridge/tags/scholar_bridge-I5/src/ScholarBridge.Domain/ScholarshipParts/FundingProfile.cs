﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingProfile : ICloneable
    {
        public FundingProfile()
        {
            Need = new DefinitionOfNeed();
            SupportedSituation = new SupportedSituation();
            FundingParameters = new FundingParameters();
            AttributesUsage = new List<FundingProfileAttributeUsage>();
        }

        public virtual IList<FundingProfileAttributeUsage> AttributesUsage { get; set; }
        public virtual bool HasAttributeUsage(FundingProfileAttribute attribute)
        {
            return AttributesUsage.Any(o => o.Attribute == attribute);
        }

        public virtual FundingProfileAttributeUsage FindAttributeUsage(FundingProfileAttribute attribute)
        {
            return AttributesUsage.FirstOrDefault(o => o.Attribute == attribute);
        }

        public virtual void RemoveAttributeUsage(FundingProfileAttribute attribute)
        {
            var attributeUsage = FindAttributeUsage(attribute);
            if (null != attributeUsage)
                AttributesUsage.Remove(attributeUsage);
        }

        public virtual ScholarshipAttributeUsageType GetAttributeUsageType(FundingProfileAttribute attribute)
        {
            var attributeUsage = FindAttributeUsage(attribute);
            if (null != attributeUsage)
                return attributeUsage.UsageType;

            return ScholarshipAttributeUsageType.NotUsed;
        }

        public virtual DefinitionOfNeed Need { get; set; }
        public virtual FundingParameters FundingParameters { get; set; }
        public virtual SupportedSituation SupportedSituation { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }

    }
}
