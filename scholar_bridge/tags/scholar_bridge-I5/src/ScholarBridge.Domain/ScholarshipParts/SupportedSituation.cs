using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Common.Validation;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class SupportedSituation
    {
        private const string SUPPORTED_SITUATION_REQUIRED = "Supported-Situation-Required";
        public SupportedSituation()
        {
            TypesOfSupport = new List<Support>();
        }

        [ListIsNotEmpty(Ruleset = SUPPORTED_SITUATION_REQUIRED, MessageTemplate = "Specify at-least one support type")]
        public virtual IList<Support> TypesOfSupport { get; protected set; }

        public virtual void ResetTypesOfSupport(IList<Support> supports)
        {
            TypesOfSupport.Clear();
            foreach (var ar in supports)
            {
                TypesOfSupport.Add(ar);
            }
        }

        public ValidationResults ValidateAsRequired()
        {
            var validator = ValidationFactory.CreateValidator<SupportedSituation>(SUPPORTED_SITUATION_REQUIRED);
            return validator.Validate(this);
        }
    }
}