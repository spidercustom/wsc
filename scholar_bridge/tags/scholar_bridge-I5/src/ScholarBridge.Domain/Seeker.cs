using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;
using SupportedSituation=ScholarBridge.Domain.ScholarshipParts.SupportedSituation;

namespace ScholarBridge.Domain
{
    public class Seeker
    {
        public const string PROFILE_ACTIVATION_RULESET = "Profile-Activation";
        
        public Seeker()
        {
        	InitializeListsAndComponents();
        }

		private void InitializeListsAndComponents()
		{
            Words = new List<SeekerVerbalizingWord>();
            Skills = new List<SeekerSkill>();
			Ethnicities = new List<Ethnicity>();
			Religions = new List<Religion>();
            Sports = new  List<Sport>();

            AcademicAreas = new  List<AcademicArea>();
            Careers =new List<Career>();
            CommunityServices =new List<CommunityService>();
            Hobbies =new List<SeekerHobby>();
            Clubs = new List<Club>();
            MatchOrganizations =new List<SeekerMatchOrganization>();
            AffiliationTypes =new List<AffiliationType>();
            WorkTypes =new List<WorkType>();
            WorkHours =new List<WorkHour>();
            ServiceTypes =new List<ServiceType>();
            ServiceHours =new List<ServiceHour>();
			Address = new SeekerAddress();            
			Need =new DefinitionOfNeed();
            SupportedSituation=new SupportedSituation();
		}
        public virtual int Id { get; set; }
		public virtual User User { get; set; }
        public virtual PhoneNumber Phone { get; set; }
		public virtual County County
		{
			get; set;
		}
    	private SeekerAddress _address;

        public virtual SeekerAddress Address 
		{ 
			get
			{
				if (_address == null)
                    _address = new SeekerAddress();
				return _address;
			}
			 set
			 {
			 	_address = value;
			 }
		}

        public virtual PersonName Name { get { return null == User ? null : User.Name; } }

        public virtual bool FirstGeneration { get; set; }

        public virtual Genders Gender { get; set; }

        public virtual SeekerStages Stage { get; set; }

        public virtual CurrentSchool CurrentSchool { get; set; }

        [StringLengthValidator(0, 1500)]
        public virtual string PersonalStatement { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyChallenge { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyGift { get; set; }

        public virtual SeekerAcademics SeekerAcademics { get; set; }

        public virtual IList<SeekerVerbalizingWord> Words { get; set; }
        public virtual IList<SeekerSkill> Skills { get; set; }

		public virtual IList<Ethnicity> Ethnicities { get; set; }
		public virtual string EthnicityOther { get; set; }

        public virtual IList<Religion> Religions { get; set; }
		public virtual string ReligionOther { get; set; }
        
        public virtual IList<Sport> Sports { get; set; }
        public virtual string SportOther { get; set; }

        public virtual IList<AcademicArea> AcademicAreas { get; set; }
        public virtual string AcademicAreaOther { get; set; }

        public virtual IList<Career> Careers { get; set; }
        public virtual string CareerOther { get; set; }

        public virtual IList<CommunityService> CommunityServices { get; set; }
        public virtual string CommunityServiceOther { get; set; }

        public virtual IList<SeekerHobby> Hobbies { get; set; }
        public virtual string HobbyOther { get; set; }

        public virtual IList<Club> Clubs { get; set; }
        public virtual string ClubOther { get; set; }

        public virtual IList<SeekerMatchOrganization> MatchOrganizations { get; set; }
        public virtual string MatchOrganizationOther { get; set; }

        public virtual IList<AffiliationType> AffiliationTypes { get; set; }
        public virtual string AffiliationTypeOther { get; set; }

        public virtual IList<WorkType> WorkTypes { get; set; }
        public virtual string WorkTypeOther { get; set; }

        public virtual IList<WorkHour> WorkHours { get; set; }
        public virtual string WorkHourOther { get; set; }

        public virtual IList<ServiceType> ServiceTypes { get; set; }
        public virtual string ServiceTypeOther { get; set; }

        public virtual IList<ServiceHour> ServiceHours { get; set; }
        public virtual string ServiceHourOther { get; set; }

        public virtual DefinitionOfNeed Need { get; set; }
        public virtual SupportedSituation SupportedSituation { get; set; }

        public virtual DateTime? LastMatch { get; set;}

        public virtual ActivityStamp LastUpdate { get; set; }
        
        public virtual ValidationResults ValidateActivation()
        {
            var results = Validate(PROFILE_ACTIVATION_RULESET);
            results.AddAllResults(CurrentSchool.ValidateActivation());
            results.AddAllResults(SeekerAcademics.ValidateActivation());
            results.AddAllResults(Name.ValidateAsRequired());
            results.AddAllResults(Address.ValidateAsRequired());
            results.AddAllResults(SupportedSituation.ValidateAsRequired());
            return results;
        }

        private ValidationResults Validate(string ruleSet)
        {
            var validator = ValidationFactory.CreateValidator<Seeker>(ruleSet);
            return validator.Validate(this);
        }
    }
}