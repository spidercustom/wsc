﻿using System;

namespace ScholarBridge.Domain.Messaging
{
    public enum MessageType
    {
        // a mail template enum should be the firstname of email template file
        // for example if ProverDenied is enum then system will look for
        // providerdenied.mail file.
        None,
        ConfirmationLink,
        ForgotPassword,
        IntermediaryApproved,
        IntermediaryRejected,
        ProviderRejected,
        ProviderApproved,
        RequestIntermediaryApproval,
        RequestProviderApproval,
        RequestScholarshipActivation,
        RelationshipCreateRequest,
        RelationshipInactivated,
        ScholarshipApproved,
        ScholarshipRejected,
        RelationshipRequestApproved,
        RelationshipRequestRejected,
        ListChangeRequest,
        EmailAddressChangeVerificationLink
    }

    public static class MessageTypeExtensions
    {
        public static string TemplateName(this MessageType messageType)
        {
            return string.Format("{0}.mail", Enum.GetName(typeof (MessageType), messageType));
        }
    }
}