﻿
namespace ScholarBridge.Domain
{
    public enum SeekerStages
    {
        None,
        Basics,
        AboutMe,
        AcademicInfo,
        Activities,
        FinancialNeed,
        Published
    }
}
