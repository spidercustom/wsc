namespace ScholarBridge.Domain
{
    public class Match : ISoftDeletable
    {
        public virtual int Id { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual int SeekerPreferredCriteriaCount { get; set; }
        public virtual int SeekerMinimumCriteriaCount { get; set; }

        public virtual Scholarship Scholarship { get; set; }
        public virtual int ScholarshipPreferredCriteriaCount { get; set; }
        public virtual int ScholarshipMinimumCriteriaCount { get; set; }

        public virtual MatchStatus MatchStatus { get; set; }
        public virtual bool IsDeleted { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual string ScholarshipName
        {
            get { return null == Scholarship ? null : Scholarship.DisplayName; }
        }

        public virtual string MinumumCriteriaString
        {
            get { return string.Format("{0} of {1}", SeekerMinimumCriteriaCount, ScholarshipMinimumCriteriaCount); }
        }

        public virtual double MinimumCriteriaPercent()
        {
            if (0 == ScholarshipMinimumCriteriaCount)
                return 1;
            return SeekerMinimumCriteriaCount / (double) ScholarshipMinimumCriteriaCount;
        }

        public virtual string PreferredCriteriaString
        {
            get { return string.Format("{0} of {1}", SeekerPreferredCriteriaCount, ScholarshipPreferredCriteriaCount); }
        }

        public virtual double PreferredCriteriaPercent()
        {
            if (0 == ScholarshipPreferredCriteriaCount)
                return 1;
            return SeekerPreferredCriteriaCount / (double) ScholarshipPreferredCriteriaCount;
        }
    }
}