﻿using System.Web.UI.WebControls;
using NUnit.Framework;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Tests
{
    [TestFixture]
    public class CriteriaUsageTypeIconHelperTests
    {
        [Test]
        public void test_minimul_img_setup()
        {
            var img = new Image();
            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(img, ScholarshipAttributeUsageType.Minimum);

            Assert.IsNotEmpty(img.AlternateText);
            Assert.IsNotEmpty(img.ImageUrl);
            Assert.IsNotEmpty(img.ToolTip);
            Assert.IsTrue(img.Visible);

            Assert.AreEqual(ScholarshipAttributeUsageType.Minimum.ToString(), img.AlternateText);
            Assert.AreEqual(CriteriaUsageTypeIconHelper.CRITERIA_MINIMUM_ICON_PATH, img.ImageUrl);
            Assert.AreEqual(CriteriaUsageTypeIconHelper.MINIMUM_CRITERIA_TOOLTIP, img.ToolTip);

        }

        [Test]
        public void test_preference_img_setup()
        {
            var img = new Image();
            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(img, ScholarshipAttributeUsageType.Preference);

            Assert.IsNotEmpty(img.AlternateText);
            Assert.IsNotEmpty(img.ImageUrl);
            Assert.IsNotEmpty(img.ToolTip);
            Assert.IsTrue(img.Visible);

            Assert.AreEqual(ScholarshipAttributeUsageType.Preference.ToString(), img.AlternateText);
            Assert.AreEqual(CriteriaUsageTypeIconHelper.CRITERIA_PREFERENCE_ICON_PATH, img.ImageUrl);
            Assert.AreEqual(CriteriaUsageTypeIconHelper.PREFERENCE_TOOLTIP, img.ToolTip);

        }
    }
}
