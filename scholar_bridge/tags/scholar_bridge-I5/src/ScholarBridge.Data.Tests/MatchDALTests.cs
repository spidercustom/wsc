using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class MatchDALTests : TestBase
    {
        public MatchDAL MatchDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public SeekerDAL SeekerDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        private Seeker seeker;
        private Scholarship scholarship1;
        private Scholarship scholarship2;
        private Provider provider;

        protected override void OnSetUpInTransaction()
        {
            var user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            seeker = SeekerDALTest.InsertSeeker(SeekerDAL, user, StateDAL);
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "MatchTest Provider", user);
            scholarship1 = ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStages.Activated);
            scholarship1.SeekerProfileCriteria = new SeekerProfileCriteria {GPA = new RangeCondition<double?>(3.25d, 5d)};
            scholarship1 = ScholarshipDAL.Insert(scholarship1);
            scholarship2 = ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, provider, ScholarshipStages.GeneralInformation));
        }

        [Test]
        public void can_create_match()
        {
            var m = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            Assert.IsNotNull(m);
            Assert.AreEqual(2, m.SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, m.SeekerPreferredCriteriaCount);
        }

        [Test]
        public void can_find_all()
        {
            CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            CreateMatch(seeker, scholarship2, 2, 3, 4, 6);

            var matches = MatchDAL.FindAll(seeker);
            Assert.IsNotNull(matches);
            Assert.AreEqual(2, matches.Count);
        }

        [Test]
        [Ignore("Not sure why this isn't working on ci.")]
        public void can_update_matches_from_seeker()
        {
            MatchDAL.UpdateMatches(seeker);

            var matches = MatchDAL.FindAll(seeker);
            Assert.IsNotNull(matches);
            Assert.AreEqual(1, matches.Count);
        }

        [Test]
        [Ignore("Not sure why this isn't working on ci.")]
        public void can_update_matches_from_scholarship()
        {
            MatchDAL.UpdateMatches(scholarship1);

            var matches = MatchDAL.FindAll(seeker);
            Assert.IsNotNull(matches);
            Assert.AreEqual(1, matches.Count);
        }

        [Test]
        public void can_get_saved_matches()
        {
            var m1 = CreateMatch(seeker, scholarship1, 4, 4, 4, 4);
            var m = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            m.MatchStatus = MatchStatus.Saved;
            MatchDAL.Update(m);

            var found = MatchDAL.FindAll(seeker, MatchStatus.Saved);
            Assert.IsNotNull(found);
            Assert.AreEqual(2, found[0].SeekerMinimumCriteriaCount);
            Assert.AreEqual(3, found[0].SeekerPreferredCriteriaCount);
        }

        [Test]
        public void can_get_new_matches()
        {
            var m1 = CreateMatch(seeker, scholarship1, 4, 4, 4, 4);
            var m = CreateMatch(seeker, scholarship1, 2, 3, 4, 6);
            m.MatchStatus = MatchStatus.Saved;
            MatchDAL.Update(m);

            var found = MatchDAL.FindAll(seeker, MatchStatus.New);
            Assert.IsNotNull(found);
            Assert.AreEqual(4, found[0].SeekerMinimumCriteriaCount);
            Assert.AreEqual(4, found[0].SeekerPreferredCriteriaCount);
        }

        public Match CreateMatch(Seeker seeker, Scholarship scholarship, int seekerMinCount, int seekerPrefCount, int scholMinCount, int scholPrefCount)
        {
            var m = new Match
            {
                Seeker = seeker,
                Scholarship = scholarship,
                SeekerMinimumCriteriaCount = seekerMinCount,
                SeekerPreferredCriteriaCount = seekerPrefCount,
                ScholarshipMinimumCriteriaCount = scholMinCount,
                ScholarshipPreferredCriteriaCount = scholPrefCount,
                LastUpdate = new ActivityStamp(seeker.User)
            };
            return MatchDAL.Insert(m);
        }
    }
}