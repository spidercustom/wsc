﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Basics" %>
<%@ Register Src="~/Common/LookupItemCheckboxList.ascx" TagName="LookupItemCheckboxList"
    TagPrefix="sb" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>    
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>    
<h3>
    My Profile – Basics</h3>
<br />
<label >My Name:</label>
<br />
<br />
<label for="FirstNameBox">First:</label>
<asp:TextBox ID="FirstNameBox" runat="server" Columns="20" MaxLength="50" OnTextChanged="FirstNameBox_TextChanged"></asp:TextBox>
<br />
<label for="MidNameBox">Middle:</label>
<asp:TextBox ID="MidNameBox" runat="server" Columns="10" MaxLength="50"></asp:TextBox>
<br />
<label for="LastNameBox">Last:</label>
<asp:TextBox ID="LastNameBox" runat="server" Columns="20" MaxLength="50"></asp:TextBox>
<br />
<br />
<label for="EmailBox">Email:</label>
<asp:Label ID="EmailBox" runat="server" />
<asp:HyperLink ID="LinkUpdateEmail" runat="server" NavigateUrl="~/Seeker/Users/ChangeEmail.aspx"   Text=" Update Email Address" />
<br />
<br />
<fieldset>
    <label >My Permanent Address:</label>
    <br />
    <br />
    <label for="AddressLine1Box">Line 1:</label>
    <asp:TextBox ID="AddressLine1Box" runat="server" Columns="35" MaxLength="50"></asp:TextBox>
    <br />
    <label for="AddressLine2Box">Line 2:</label>
    <asp:TextBox ID="AddressLine2Box" runat="server" Columns="35" MaxLength="50"></asp:TextBox>
    <br />
    <label for="StateDropDown">State:</label>
    <asp:DropDownList ID="StateDropDown" runat="server"></asp:DropDownList>
    <br />
    <label for="CityControl">City:</label>
    <asp:TextBox ID="CityControl" runat="server" Columns="25" ReadOnly="true"></asp:TextBox>
                    <sb:LookupDialog ID="CityControlDialogButton" runat="server" BuddyControl="CityControl" 
                    ItemSource="CityDAL" Title="City" SelectionLimit="1"/>
    <br />
    <label for="CountyControl">County:</label>
    <asp:TextBox ID="CountyControl" runat="server" Columns="25" ReadOnly="true"></asp:TextBox>
                    <sb:LookupDialog ID="CountyControlDialogButton" runat="server" BuddyControl="CountyControl" 
                    ItemSource="CountyDAL" Title="County" SelectionLimit="1" />
    <br />
    <label for="ZipBox">Zip:</label>
    <asp:TextBox ID="ZipBox" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
    <br />
    <label for="PhoneBox">Phone:</label>
    <asp:TextBox ID="PhoneBox" runat="server" Columns="35" MaxLength="10" 
                        ontextchanged="PhoneBox_TextChanged"></asp:TextBox>
    <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="PhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
</fieldset>
<br />
<fieldset>
    <label for="GenderButtonList">My Gender:</label>
    <asp:RadioButtonList ID="GenderButtonList" runat="server">
        <asp:ListItem Text="Male" Value="1"></asp:ListItem>
        <asp:ListItem Text="Female" Value="2"></asp:ListItem>
    </asp:RadioButtonList>
</fieldset>
<br />
<fieldset>
    <label for="ReligionCheckboxList">My Religion/Faith:</label>
    <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true"
        ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" />
    <label for="OtherReligion">Other Religion?:</label>
    <asp:TextBox ID="OtherReligion" runat="server"></asp:TextBox>  
 </fieldset>
<br />

<fieldset>
<label for="EthnicityControl">My Heritage:</label>
<sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true"
                        ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" />
<label for="OtherEthnicity">Other Ethnicity?</label>
<asp:TextBox ID="OtherEthnicity" runat="server"></asp:TextBox>    
</fieldset>



