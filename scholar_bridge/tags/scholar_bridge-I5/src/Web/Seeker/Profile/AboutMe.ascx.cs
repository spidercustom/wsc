﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class AboutMe : WizardStepUserControlBase<Domain.Seeker>
    {
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Seeker seeker;
        Domain.Seeker SeekerInContext
        {
            get
            {
                if (seeker == null)
                    seeker = Container.GetDomainObject();
                if (seeker == null)
                    throw new InvalidOperationException("There is no seeker in context");
                return seeker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
		}

        private void PopulateScreen()
        {
            PersonalStatementControl.Text = SeekerInContext.PersonalStatement;
            MyGiftControl.Text = SeekerInContext.MyGift;
            MyChallengeControl.Text = SeekerInContext.MyChallenge;
            FiveWordsControlDialogButton.Keys = SeekerInContext.Words.CommaSeparatedIds();
            FiveSkillsControlDialogButton.Keys = SeekerInContext.Skills.CommaSeparatedIds();
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
		    PopulateObjects();
            SeekerService.Update(SeekerInContext);
		}

        public override void PopulateObjects()
        {
            SeekerInContext.PersonalStatement = PersonalStatementControl.Text;
            SeekerInContext.MyGift = MyGiftControl.Text;
            SeekerInContext.MyChallenge = MyChallengeControl.Text;

            FiveWordsControlDialogButton.PopulateListFromSelection(SeekerInContext.Words);
            FiveSkillsControlDialogButton.PopulateListFromSelection(SeekerInContext.Skills);
        }

		public override bool WasSuspendedFrom(Domain.Seeker @object)
		{
            return @object.Stage == SeekerStages.AboutMe;
		}

		public override bool CanResume(Domain.Seeker @object)
		{
            return @object.Stage >= SeekerStages.AboutMe;
		}

		public override bool IsCompleted
		{
			get { return true; }
			set {  }
		}
		#endregion

	}
}