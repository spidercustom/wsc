﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Default : SBBasePage, IWizardStepsContainer<Domain.Seeker>
    {
        #region consts
        private const string ACTIVATION_VALIDATION_ERRORS_DIV_ID = "ActivationValidationErrors";
	    private const string ACTIVATION_SUCCESS_NOTE = 
            @"Congratulations, you have successfully activated the profile.<br/>
             Wishing you all the best for positive interations with scholarship providers";

	    private const string ACTIVATION_MESSAGES_TITLE = "Profile activation";
        #endregion

        #region properties

        public ISeekerService SeekerService { get; set; }
		public IUserContext UserContext { get; set; }

		#endregion

		#region Page Lifecycle Events
		protected void Page_Load(object sender, EventArgs e)
        {
			Steps.ForEach(step => step.Container = this);

			if (!IsPostBack)
			{
			    RemoveActivateButtonIfAlreadyActivated();

			    CheckForDataChanges = true;

				BypassPromptIds.AddRange(
					new[]	{
								"PreviousButton",
								"NextButton",
								"SaveButton",
                                "ActivateButton"
							});

				ActiveStepIndex = WizardStepName.Basics.GetNumericValue();
			}
		}

	    private void RemoveActivateButtonIfAlreadyActivated()
	    {
	        var seeker = GetDomainObject();
            if (null != seeker && seeker.Stage == SeekerStages.Published)
            {
                ActivateButton.Visible = false;

                const string msg1 =
                    "You have updated your profile information. Once saved, the changes will replace the previous information in your profile. Do you want to update your profile with these changes?";
                
                // FIXME: Find out if a seeker has submitted applications and if so show msg2
                const string msg2 =
                    "You have updated your profile information. Once saved, the changes will replace the previous information in your profile but your changes will not be updated in the Scholarship applications that you have already submitted.  Do you want to update your profile with these changes?";
                
                SaveButton.Attributes.Add("onclick", String.Format("return confirmSaveDialog('{0}', 'Save Changes?')", msg1));
            }
	    }

	    protected void Page_PreRender(object sender, EventArgs e)
		{
			PreviousButton.Enabled = ActiveStepIndex != 0;
			NextButton.Enabled = ActiveStepIndex != Steps.Length - 1;
		}

		#endregion

		#region Control event handlers

		protected void BuildSeekerProfileWizard_ActiveStepChanged(object sender, EventArgs e)
    	{
			WizardStepContainerCommon.NotifyStepActivated(this);
		}

        protected void PreviousButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
    	{
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
    		    GoPrior();
		}

        protected void NextButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
    	{
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
    		    GoNext();
    	}

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void ActivateButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            WizardStepContainerCommon.PopulateObjectsFromActiveStep(this);
            ValidateActivated();
        }

        protected void ExitButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
            {
                Response.Redirect("../");
            }
        }

		#endregion

		#region Private Methods

		private bool ValidateStep()
		{
			return WizardStepContainerCommon.ValidateStep(this);
		}

        private bool Save()
		{
            var seekerProfile = GetDomainObject();
            if (!Page.IsValid || !ValidateStep())
                return false;

            WizardStepContainerCommon.PopulateObjectsFromActiveStep(this);
            if (seekerProfile.Stage == SeekerStages.Published && !ValidateActivated())
                return false;
	        
            SeekerService.Update(GetDomainObject());
	        Dirty = false;
	        return true;
		}

	    private bool ValidateActivated()
        {
            var seekerProfile = GetDomainObject();

            var results = seekerProfile.ValidateActivation();
            if (results.IsValid)
            {
                if (seekerProfile.Stage < SeekerStages.Published)
                {
                    seekerProfile.Stage = SeekerStages.Published;
                    SeekerService.Update(seekerProfile);
                    ClientSideDialogs.ShowAlert(ACTIVATION_SUCCESS_NOTE, ACTIVATION_MESSAGES_TITLE);
                }
                return true;
            }
                
            IssueListControl.DataSource = results;
            IssueListControl.DataBind();
            ClientSideDialogs.ShowDivAsDialog(ACTIVATION_VALIDATION_ERRORS_DIV_ID);
            return false;
        }

		#endregion

		#region IWizardStepsContainer<Seeker> Members

		public IWizardStepControl<Domain.Seeker>[] Steps
		{
			get
			{
				IWizardStepControl<Domain.Seeker>[] stepControls = BuildSeekerProfileWizard.Views.FindWizardStepControls<Domain.Seeker>();
				return stepControls;
			}
		}

		Domain.Seeker seekerInContext;
        public Domain.Seeker GetDomainObject()
		{
			if (seekerInContext == null)
			{
			    seekerInContext = UserContext.CurrentSeeker;
			}
			return seekerInContext;
		}

		public void GoPrior()
		{
			WizardStepContainerCommon.GoPrior(this);
		}

		public void GoNext()
		{
			WizardStepContainerCommon.GoNext(this);
		}

		public void Goto(int index)
		{
			WizardStepContainerCommon.Goto(this, index);
		}

		public int ActiveStepIndex
		{
			get { return BuildSeekerProfileWizard.ActiveViewIndex; }
			set { BuildSeekerProfileWizard.ActiveViewIndex = value; }
		}

		#endregion
	}
}