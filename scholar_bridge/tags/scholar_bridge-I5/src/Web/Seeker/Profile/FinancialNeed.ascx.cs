﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class FinancialNeed : WizardStepUserControlBase<Domain.Seeker>
    {
        Domain.Seeker _seekerInContext;
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        public INeedGapDAL NeedGapDAL { get; set; }
        Domain.Seeker seekerInContext
        {
            get
            {
                if (_seekerInContext == null)
                    _seekerInContext = Container.GetDomainObject();
                return _seekerInContext;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (seekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context");

            if (!Page.IsPostBack)
            {

                NeedGaps.DataBind();
                TypesOfSupport.DataBind();

                PopulateScreen();
            }
        }
        private void PopulateScreen()
        {
            PopulateCheckBoxes(NeedGaps, NeedGapDAL.FindAll());
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());
            if (null != seekerInContext.Need)
            {
                Fafsa.Checked = seekerInContext.Need.Fafsa;
                UserDerived.Checked = seekerInContext.Need.UserDerived;
                MinimumSeekerNeed.Amount = seekerInContext.Need.MinimumSeekerNeed;
                MaximumSeekerNeed.Amount = seekerInContext.Need.MaximumSeekerNeed;
                NeedGaps.Items.SelectItems(seekerInContext.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != seekerInContext.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(seekerInContext.SupportedSituation.TypesOfSupport, ts => ts.Id.ToString());
            }
        }

        public override void PopulateObjects()
        {
            // Need
            if (null == seekerInContext.Need)
                seekerInContext.Need = new DefinitionOfNeed();
            seekerInContext.Need.Fafsa = Fafsa.Checked;
            seekerInContext.Need.UserDerived = UserDerived.Checked;
            seekerInContext.Need.MinimumSeekerNeed = MinimumSeekerNeed.Amount;
            seekerInContext.Need.MaximumSeekerNeed = MaximumSeekerNeed.Amount;

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => (object)item);
            seekerInContext.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            seekerInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            seekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (seekerInContext.Stage < SeekerStages.FinancialNeed)
                seekerInContext.Stage = SeekerStages.FinancialNeed;
        }


        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
       
        #region WizardStepUserControlBase methods

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            SeekerService.Update(seekerInContext);
        }

        public override bool WasSuspendedFrom(Domain.Seeker @object)
        {
            return @object.Stage == SeekerStages.FinancialNeed;
        }

        public override bool CanResume(Domain.Seeker @object)
        {
            return @object.Stage >= SeekerStages.FinancialNeed;
        }

        public override bool IsCompleted
        {
            get { return true; }
            set { }
        }
        #endregion
	}
}