﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Basics : WizardStepUserControlBase<Domain.Seeker>
    {
		public ISeekerService SeekerService { get; set; }
		public IStateDAL StateService { get; set; }
		Domain.Seeker _seekerInContext;
		Domain.Seeker seekerInContext
		{
			get
			{
				if (_seekerInContext == null)
					_seekerInContext = Container.GetDomainObject();
				return _seekerInContext;
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			if (seekerInContext == null)
				throw new InvalidOperationException("There is no seeker in context");

			if (!Page.IsPostBack)
			{
				PopulateScreen();
			}
		}

		private void PopulateScreen()
		{
			StateDropDown.DataSource = StateService.FindAll();
			StateDropDown.DataTextField = "Name";
			StateDropDown.DataValueField = "Abbreviation";
			StateDropDown.DataBind();
			StateDropDown.Items.Insert(0, new ListItem("- Select One -", ""));
            EmailBox.Text = !String.IsNullOrEmpty(seekerInContext.User.EmailWaitingforVerification) ? string.Format("{0} - Email waiting for verification ({1})", seekerInContext.User.Email, seekerInContext.User.EmailWaitingforVerification)
                : seekerInContext.User.Email;

			LastNameBox.Text = seekerInContext.User.Name.LastName;
			MidNameBox.Text = seekerInContext.User.Name.MiddleName;
			FirstNameBox.Text = seekerInContext.User.Name.FirstName;
			AddressLine1Box.Text = seekerInContext.Address.Street;
			AddressLine2Box.Text = seekerInContext.Address.Street2;
			if (null != seekerInContext.Address.State)
				StateDropDown.SelectedValue = seekerInContext.Address.State.Abbreviation;

		    CountyControlDialogButton.Keys = seekerInContext.County == null ? "" : seekerInContext.County.Id.ToString();
            CityControlDialogButton.Keys = seekerInContext.Address.City == null ? "" : seekerInContext.Address.City.Id.ToString();
           
			ZipBox.Text = seekerInContext.Address.PostalCode;
		    PhoneBox.Text =seekerInContext.Phone == null ? "" : seekerInContext.Phone.FormattedPhoneNumber;
			ReligionCheckboxList.SelectedValues = seekerInContext.Religions.Cast<ILookup>().ToList();
			EthnicityControl.SelectedValues = seekerInContext.Ethnicities.Cast<ILookup>().ToList();
			OtherReligion.Text = seekerInContext.ReligionOther;
			GenderButtonList.SelectedIndex = (seekerInContext.Gender.Equals(Genders.Male) ? 0 : (seekerInContext.Gender.Equals(Genders.Female) ? 1 : -1));
			OtherEthnicity.Text = seekerInContext.EthnicityOther;
		}
		
		public override void PopulateObjects()
		{
			seekerInContext.User.Name.LastName = LastNameBox.Text;
			seekerInContext.User.Name.MiddleName = MidNameBox.Text;
			seekerInContext.User.Name.FirstName = FirstNameBox.Text;
			seekerInContext.Address.Street = AddressLine1Box.Text;
			seekerInContext.Address.Street2 = AddressLine2Box.Text;
			seekerInContext.Address.State = StateDropDown.SelectedValue == string.Empty 
															? null 
															: StateService.FindByAbbreviation(StateDropDown.SelectedValue);
		    seekerInContext.County = (County) CountyControlDialogButton.GetSelectedLookupItem();
		    seekerInContext.Address.City = (City) CityControlDialogButton.GetSelectedLookupItem();

			seekerInContext.Address.PostalCode = ZipBox.Text;
            seekerInContext.Phone = new PhoneNumber(PhoneBox.Text);
			PopulateList(EthnicityControl, seekerInContext.Ethnicities);
			PopulateList(ReligionCheckboxList, seekerInContext.Religions);
			seekerInContext.Gender = GenderButtonList.SelectedValue == string.Empty ? Genders.Unspecified : (Genders) int.Parse(GenderButtonList.SelectedValue);
			seekerInContext.EthnicityOther = OtherEthnicity.Text;
			seekerInContext.ReligionOther = OtherReligion.Text;
		}

		private static void PopulateList<T>(LookupItemCheckboxList checkboxList, IList<T> list)
		{
			checkboxList.PopulateListFromSelectedValues(list);
		}

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
			PopulateObjects();
			SeekerService.Update(seekerInContext);
		}

		public override bool WasSuspendedFrom(Domain.Seeker @object)
		{
			return false;
		}

		public override bool CanResume(Domain.Seeker @object)
		{
			return true;
		}

		public override bool IsCompleted
		{
			get { return true; }
			set { }
		}
		#endregion

		protected void FirstNameBox_TextChanged(object sender, EventArgs e)
		{

		}

        protected void PhoneBox_TextChanged(object sender, EventArgs e)
        {

        }
	}
}