﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain.Lookup;
namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Activities : WizardStepUserControlBase<Domain.Seeker>
    {
        Domain.Seeker _seekerInContext;
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Seeker seekerInContext
        {
            get
            {
                if (_seekerInContext == null)
                    _seekerInContext = Container.GetDomainObject();
                return _seekerInContext;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (seekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context");

            if (!Page.IsPostBack)
            {
                
                WorkHoursControl.DataBind();
                ServiceHoursControl.DataBind();
                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {

            AcademicAreasControlDialogButton.Keys = seekerInContext.AcademicAreas.CommaSeparatedIds();
            AcademicAreasOtherControl.Text = seekerInContext.AcademicAreaOther;
            
            CareersControlDialogButton.Keys = seekerInContext.Careers.CommaSeparatedIds();
            CareersOtherControl.Text = seekerInContext.CareerOther;
            
            CommunityServiceControlDialogButton.Keys = seekerInContext.CommunityServices.CommaSeparatedIds();
            CommunityServiceOtherControl.Text = seekerInContext.CommunityServiceOther;

            OrganizationsControlDialogButton.Keys = seekerInContext.MatchOrganizations.CommaSeparatedIds();
            OrganizationsOtherControl.Text = seekerInContext.MatchOrganizationOther;

            AffiliationTypesControlDialogButton.Keys = seekerInContext.AffiliationTypes.CommaSeparatedIds();
            AffiliationTypesOtherControl.Text = seekerInContext.AffiliationTypeOther;

            SeekerHobbiesControlDialogButton.Keys = seekerInContext.Hobbies.CommaSeparatedIds();
            SeekerHobbiesOtherControl.Text = seekerInContext.HobbyOther;

            SportsControlDialogButton.Keys = seekerInContext.Sports.CommaSeparatedIds();
            SportsOtherControl.Text = seekerInContext.SportOther;

            ClubsControlDialogButton.Keys = seekerInContext.Clubs.CommaSeparatedIds();
            ClubsOtherControl.Text = seekerInContext.ClubOther;

            WorkTypeControlDialogButton.Keys = seekerInContext.WorkTypes.CommaSeparatedIds();
            
            WorkHoursControl.SelectedValues = seekerInContext.WorkHours.Cast<ILookup>().ToList();

            ServiceTypeControlDialogButton.Keys = seekerInContext.ServiceTypes.CommaSeparatedIds();

            ServiceHoursControl.SelectedValues = seekerInContext.ServiceHours.Cast<ILookup>().ToList();
        }

        public override void PopulateObjects()
        {
            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, seekerInContext.AcademicAreas);
            seekerInContext.AcademicAreaOther = AcademicAreasOtherControl.Text;

            PopulateList(CareersContainerControl, CareersControlDialogButton, seekerInContext.Careers);
            seekerInContext.CareerOther = CareersOtherControl.Text;

            PopulateList(CommunityServiceContainerControl, CommunityServiceControlDialogButton, seekerInContext.CommunityServices);
            seekerInContext.CommunityServiceOther = CommunityServiceOtherControl.Text;

            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, seekerInContext.MatchOrganizations);
            seekerInContext.MatchOrganizationOther = OrganizationsOtherControl.Text;

            PopulateList(AffiliationTypesContainerControl, AffiliationTypesControlDialogButton, seekerInContext.AffiliationTypes);
            seekerInContext.AffiliationTypeOther = AffiliationTypesOtherControl.Text;

            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, seekerInContext.Hobbies);
            seekerInContext.HobbyOther = SeekerHobbiesOtherControl.Text;

            PopulateList(SportsContainerControl, SportsControlDialogButton, seekerInContext.Sports);
            seekerInContext.SportOther = SportsOtherControl.Text;

            PopulateList(ClubsContainerControl, ClubsControlDialogButton, seekerInContext.Clubs);
            seekerInContext.ClubOther = ClubsOtherControl.Text;

            PopulateList(WorkTypeContainerControl, WorkTypeControlDialogButton, seekerInContext.WorkTypes);
            
            PopulateList(WorkHoursContainerControl, WorkHoursControl, seekerInContext.WorkHours);
            
            PopulateList(ServiceTypeContainerControl, ServiceTypeControlDialogButton, seekerInContext.ServiceTypes);
            PopulateList(ServiceHoursContainerControl, ServiceHoursControl, seekerInContext.ServiceHours);
            
            seekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (seekerInContext.Stage < SeekerStages.Activities)
                seekerInContext.Stage = SeekerStages.Activities;
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }
        private static bool Validate(PlaceHolder applicabilityControl, LookupDialog dialogButton)
        {
            return true;
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
            PopulateObjects();
            SeekerService.Update(seekerInContext);
		}

		public override bool WasSuspendedFrom(Domain.Seeker @object)
		{
		    return @object.Stage == SeekerStages.Activities;
		}

		public override bool CanResume(Domain.Seeker @object)
		{
            return @object.Stage >= SeekerStages.Activities;
		}

		public override bool IsCompleted
		{
			get { return true; }
			set { }
		}
		#endregion
	}
}