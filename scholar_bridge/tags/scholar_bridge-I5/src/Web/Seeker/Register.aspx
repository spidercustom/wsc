﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Register" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

 
<asp:MultiView runat="server" ID="CreateUserWizard" ActiveViewIndex="0">
    <asp:View ID="DataEntryView" runat="server">            
                <h3>User Information</h3>
                <label for="FirstName">First Name:</label>
                <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="firstNameRequired" ControlToValidate="FirstName" runat="server" Display="Dynamic" ErrorMessage="First name is required" ToolTip="Please enter your first name"></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="MiddleName">Middle Name:</label>
                <asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"  ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="LastName">Last Name:</label>
                <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="lastNameRequired" ControlToValidate="LastName" Display="Dynamic" runat="server" ErrorMessage="Last name is required" ToolTip="Please enter your last name"></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="UserName">Email Address:</label>
                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="emailRequired" ControlToValidate="UserName" Display="Dynamic" runat="server" ErrorMessage="Email address is required" ToolTip="Please enter your email address"></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
                <br />
                <label for="ConfirmEmail">Confirm Email Address:</label>
                <asp:TextBox ID="ConfirmEmail" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator Display="Dynamic" ErrorMessage="Confirm Email is required" ID="confirmEmailRequired" ControlToValidate="ConfirmEmail" runat="server" ></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" 
                    ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address." ToolTip="Please assure that both email addresses match" />
                <br />
                
                <span class="noteBene">Password must be at least 8 characters with one capital letter and one special character or number.</span><br /><br />
                <label for="Password">Password:</label>
                <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="passwordRequired" Display="Dynamic"  ControlToValidate="Password" runat="server" ErrorMessage="A password is required" ToolTip="Please enter a password (8 character minimum with a capital and either a number or special character)"></asp:RequiredFieldValidator>
                <elv:PropertyProxyValidator ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="ConfirmPassword">Confirm Password:</label>
                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" Display="Dynamic" runat="server" ControlToValidate="ConfirmPassword"
                    ErrorMessage="Confirm Password is required." ToolTip="Please re-enter your new password here."></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                    ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                    ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                <br />

                <p>Placekeeper for note on junk email filter</p>
                <asp:Button ID="RegisterButton" runat="server" Text="Register" 
                onclick="RegisterButton_Click" CausesValidation="true"/>

    </asp:View>
    
    <asp:View ID="CompletionView" runat="server">
                <h2>Thanks for registering with us</h2>
                <p>Now wait for an email to be sent to the email address you specified with instructions
                to enable your account and login.</p>
    </asp:View>

</asp:MultiView>

</asp:Content>
