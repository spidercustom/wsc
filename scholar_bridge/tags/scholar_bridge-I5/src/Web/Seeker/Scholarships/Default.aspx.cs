﻿using System;
using System.Collections.Generic;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Scholarships
{
	public partial class Default : SBBasePage
	{
        public IMatchService MatchService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Seeker currentSeeker;

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
            currentSeeker = UserContext.CurrentSeeker;

            if (!Page.IsPostBack)
            {
                PopulatePage();
            }
        }

        private void PopulatePage()
        {
            IList<Match> matches = MatchService.GetSavedMatches(currentSeeker);
            savedList.Matches = matches;
            savedList.LinkTo = "~/Seeker/Matches/Show.aspx";
            savedList.ActionText = "Remove";
            savedList.IsActionEnabled = (m => true);
        }

        protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.UnSaveMatch(currentSeeker, scholarshipId);
        }
	}
}