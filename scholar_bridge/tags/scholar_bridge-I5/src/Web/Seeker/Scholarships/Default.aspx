﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Scholarships.Default"  Title="Seeker | My Scholarships" %>
<%@ Register src="~/Common/MatchList.ascx" tagname="MatchList" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
Placeholder for My Scholarships page

<h3>My Scholarships:</h3>
<sb:MatchList id="savedList" runat="server" OnMatchAction="list_OnMatchAction" />
</asp:Content>
