﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web
{
    public partial class VerifyUserEmailChange : Page
    {
        public IUserService UserService { get; set; }
        public IUserContext UserContext { get; set; }
        public string Key { get { return Request.QueryString["key"]; } }

        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            if (String.IsNullOrEmpty(Key) || !IsValidKey(Key))
            {
                SetError();
                return;
            }

             var email = GetEmailAddress(Key);

            CurrentUser = UserContext.CurrentUser;
            if (null == CurrentUser)
            {
                SetError();
                return;
            }

            if (email != CurrentUser.EmailWaitingforVerification)
            {
                SetError();
                return;
            }

            if (!Page.IsPostBack)
            {
               
               //confirm email verification 
                CurrentUser.LastUpdate =new ActivityStamp(CurrentUser);
                UserService.ConfrimEmailAddressVerification(CurrentUser);
                SuccessMessageLabel.SetMessage("Dear User, Your email address is verified successfully. Thank you!");
                Response.Redirect("~/Default.aspx");
            }
        }

        

       

        private static bool IsValidKey(string key)
        {
            return key.StartsWith("enc");
        }

        public void SetError()
        {
            lblStatus.Text = "The link you have followed is not valid.";
        }

        

        public string GetEmailAddress(string encryptedParam)
        {
            var decrypted = CryptoHelper.Decrypt(encryptedParam);
            if (decrypted.Contains("&"))
            {
                var parts = decrypted.Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries);
                return parts[0];
            }

            return decrypted;
        }
    }
}