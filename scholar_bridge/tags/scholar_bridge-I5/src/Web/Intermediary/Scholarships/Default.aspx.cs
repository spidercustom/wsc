﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Intermediary.Scholarships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = IntermediaryService.FindById(UserContext.CurrentIntermediary.Id);

            var orgs =
                (from i in RelationshipService.GetActiveByOrganization(intermediary)
                 orderby i.Name
                 select new {i.Id, i.Name}).ToList();

            orgs.Insert(0, new {Id = 0, Name = "- ALL -"});

            cboOrganization.DataValueField = "Id";
            cboOrganization.DataTextField = "Name";
            cboOrganization.DataSource = orgs;
            cboOrganization.DataBind();
            FillScholarships();
        }

        private void FillScholarships()
        {
            var intermediary = UserContext.CurrentIntermediary;

            if (cboOrganization.SelectedValue == "0")
            {
                //fetch all
                scholarshipNotActivatedList.Scholarships = ScholarshipService.GetByIntermediaryNotActivated(intermediary);
                scholarshipActivatedList.Scholarships = ScholarshipService.GetByIntermediary(intermediary, ScholarshipStages.Activated);
                scholarshipPendingList.Scholarships = ScholarshipService.GetByIntermediary(intermediary, ScholarshipStages.RequestedActivation);
                scholarshipAwardedList.Scholarships = ScholarshipService.GetByIntermediary(intermediary, ScholarshipStages.Awarded);
            }
            else
            {
                var provider = ProviderService.FindById(int.Parse(cboOrganization.SelectedValue));

                scholarshipNotActivatedList.Scholarships = ScholarshipService.GetNotActivatedByOrganizations(provider, intermediary);
                scholarshipActivatedList.Scholarships = ScholarshipService.GetByOrganizations(provider, intermediary, ScholarshipStages.Activated);
                scholarshipPendingList.Scholarships = ScholarshipService.GetByOrganizations(provider, intermediary, ScholarshipStages.RequestedActivation);
                scholarshipAwardedList.Scholarships = ScholarshipService.GetByOrganizations(provider, intermediary, ScholarshipStages.Awarded);
            }
        }


        protected void cboOrganization_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillScholarships();
        }
    }
}