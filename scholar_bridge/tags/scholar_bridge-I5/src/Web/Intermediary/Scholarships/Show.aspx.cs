﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Intermediary.Scholarships
{
    public partial class Show : Page
    {
        private const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship ScholarshipToView { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params[SCHOLARSHIP_ID], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            var intermediary = UserContext.CurrentIntermediary;
            if (intermediary.ApprovalStatus != ApprovalStatus.Approved)
                throw new Business.Exceptions.IntermediaryNotApprovedException();

            ScholarshipToView = ScholarshipService.GetById(ScholarshipId);
            if (ScholarshipToView != null)
            {
                if (!ScholarshipToView.Intermediary.Id.Equals(intermediary.Id))
                    throw new InvalidOperationException("Scholarship do not belong to Intermediary in context");

                showScholarship.Scholarship = ScholarshipToView;

                requestActivationBtn.Visible = ScholarshipToView.CanSubmitForActivation();
                deleteBtn.Visible = ScholarshipToView.CanEdit();
                linkCopy.NavigateUrl = "~/Provider/BuildScholarship/Default.aspx?copyfrom=" + ScholarshipToView.Id;
                ScholarshipTitleStripeControl.UpdateView(ScholarshipToView);
            }
        }

        protected void requestActivationBtn_Click(object sender, EventArgs e)
        {
            ScholarshipToView.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.RequestActivation(ScholarshipToView);
            SuccessMessageLabel.SetMessage("Scholarship submitted for activation.");
            Response.Redirect("~/Intermediary/Scholarships/Default.aspx#pending-activated-tab");
        }

        protected void deleteBtn_Click(object sender, EventArgs e)
        {
            ScholarshipToView.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Delete(ScholarshipToView);
            SuccessMessageLabel.SetMessage("Scholarship was deleted.");
            Response.Redirect("~/Intermediary/Scholarships/Default.aspx");
        }
    }
}
