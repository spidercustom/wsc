﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;
using System.Globalization;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class GeneralInfo : WizardStepUserControlBase<Scholarship>
    {
        private const string AMOUNT_FORMAT = "########0";
        private const int BY_WEEK_PAGE_INDEX = 0;
        private const int BY_MONTH_PAGE_INDEX = 1;

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IStateDAL StateService { get; set; }

        Scholarship _scholarshipInContext;
        Scholarship scholarshipInContext
        {
            get
            {
                if (_scholarshipInContext == null)
                    _scholarshipInContext = Container.GetDomainObject();
                return _scholarshipInContext;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");


            //add calendar support



            if (!Page.IsPostBack)
            {
                AddressState.DataSource = StateService.FindAll();
                AddressState.DataTextField = "Name";
                AddressState.DataValueField = "Abbreviation";
                AddressState.DataBind();
                AddressState.Items.Insert(0, new ListItem("- Select One -", ""));

                ScholarshipAcademicYearControl.DataSource = DisplayAcademicYears();
                ScholarshipAcademicYearControl.DataTextField = "DisplayName";
                ScholarshipAcademicYearControl.DataValueField = "Year";
                ScholarshipAcademicYearControl.DataBind();
            }

            if (!IsPostBack)
                PopulateScreen();
        }

        private IEnumerable<AcademicYear> DisplayAcademicYears()
        {
            var midyear = scholarshipInContext.AcademicYear ?? AcademicYear.CurrentScholarshipYear;
            return Enumerable.Range(-2, 6).Select(year => new AcademicYear(year + midyear.Year));
        }

        public override void PopulateObjects()
        {
            scholarshipInContext.Name = ScholarshipNameControl.Text;
            scholarshipInContext.AcademicYear = new AcademicYear(Int32.Parse(ScholarshipAcademicYearControl.SelectedValue));
            scholarshipInContext.MissionStatement = MissionStatementControl.Text;
            scholarshipInContext.ProgramGuidelines = ProgramGuidelinesControl.Text;
            scholarshipInContext.Intermediary = IntermediarySelected.SelectedValue;
            scholarshipInContext.Provider = ProviderSelected.SelectedValue;
            scholarshipInContext.MinimumAmount = MinimumAmount.Amount;
            scholarshipInContext.MaximumAmount = MaximumAmount.Amount;
            scholarshipInContext.ApplicationStartDate = calApplicationStartDate.SelectedDate;
            scholarshipInContext.ApplicationDueDate = calApplicationDueDate.SelectedDate;
            scholarshipInContext.AwardDate = calAwardDate.SelectedDate;
            PopulateDonorObject();

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage < ScholarshipStages.GeneralInformation)
                scholarshipInContext.Stage = ScholarshipStages.GeneralInformation;
        }

        private void PopulateDonorObject()
        {
            scholarshipInContext.Donor.Name.FirstName = FirstName.Text;
            scholarshipInContext.Donor.Name.MiddleName = MiddleName.Text;
            scholarshipInContext.Donor.Name.LastName = LastName.Text;

            scholarshipInContext.Donor.Address.Street = AddressStreet.Text;
            scholarshipInContext.Donor.Address.Street2 = AddressStreet2.Text;
            scholarshipInContext.Donor.Address.City = AddressCity.Text;
            scholarshipInContext.Donor.Address.PostalCode = AddressPostalCode.Text;
            var state = StateService.FindByAbbreviation(AddressState.SelectedValue);
            scholarshipInContext.Donor.Address.State = state;

            scholarshipInContext.Donor.Phone = new PhoneNumber(Phone.Text);
            scholarshipInContext.Donor.Email = EmailAddress.Text;
        }

        
        private void PopulateScreen()
        {
            IntermediarySelected.SelectedValue = scholarshipInContext.Intermediary;
            ProviderSelected.SelectedValue = scholarshipInContext.Provider;
            ScholarshipNameControl.Text = scholarshipInContext.Name;

            int academicYear = null == scholarshipInContext.AcademicYear
                ? AcademicYear.CurrentScholarshipYear.Year
                : scholarshipInContext.AcademicYear.Year;

            ScholarshipAcademicYearControl.SelectedValue = academicYear.ToString();
            MissionStatementControl.Text = scholarshipInContext.MissionStatement;
            ProgramGuidelinesControl.Text = scholarshipInContext.ProgramGuidelines;

            calApplicationStartDate.SelectedDate = scholarshipInContext.ApplicationStartDate;
            calApplicationDueDate.SelectedDate = scholarshipInContext.ApplicationDueDate;
            calAwardDate.SelectedDate = scholarshipInContext.AwardDate;
            MaximumAmount.Amount = scholarshipInContext.MaximumAmount;
            MinimumAmount.Amount = scholarshipInContext.MinimumAmount;
            PopulateDonorScreen();
        }

        private void PopulateDonorScreen()
        {
            FirstName.Text = scholarshipInContext.Donor.Name.FirstName;
            MiddleName.Text = scholarshipInContext.Donor.Name.MiddleName;
            LastName.Text = scholarshipInContext.Donor.Name.LastName;

            AddressStreet.Text = scholarshipInContext.Donor.Address.Street;
            AddressStreet2.Text = scholarshipInContext.Donor.Address.Street2;
            AddressCity.Text = scholarshipInContext.Donor.Address.City;
            AddressPostalCode.Text = scholarshipInContext.Donor.Address.PostalCode;
            if (null != scholarshipInContext.Donor.Address.State)
                AddressState.SelectedValue = scholarshipInContext.Donor.Address.State.Abbreviation;
            if (null != scholarshipInContext.Donor.Phone)                Phone.Text = scholarshipInContext.Donor.Phone.Number;            EmailAddress.Text = scholarshipInContext.Donor.Email;
        }

        

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.GeneralInformation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.GeneralInformation;
        }

        public override bool IsCompleted
        {
            get { return scholarshipInContext.IsStageCompleted(ScholarshipStages.GeneralInformation); }
            set
            {
                scholarshipInContext.MarkStageCompletion(ScholarshipStages.GeneralInformation, value);
                ScholarshipService.Save(scholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            bool result = true;

            var foundScholarship = ScholarshipService.ScholarshipExists(ProviderSelected.SelectedValue, ScholarshipNameControl.Text, Int32.Parse(ScholarshipAcademicYearControl.SelectedValue));
            if (null != foundScholarship && foundScholarship.Id != scholarshipInContext.Id)
            {
                result &= false;
                ScholarshipNameValidator.IsValid = false;
                ScholarshipNameValidator.Text = "Scholarship in the selected Academic Year with this Name already exists";
            }
            return result;
        }

       
    }
}
