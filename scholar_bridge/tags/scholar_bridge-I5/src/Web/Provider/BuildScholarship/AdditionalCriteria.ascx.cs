﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class AdditionalCriteria : WizardStepUserControlBase<Scholarship>
	{
		#region DI Properties
		public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IAdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
		#endregion

		#region Properties
		private List<Question> Questions
    	{
    		get
    		{
    		    if (ViewState["Questions"] == null)
    		    {
    		        ViewState["Questions"]
    		            = scholarshipInContext.AdditionalQuestions
    		                .Select(q => new Question
    		                                 {
    		                                     DisplayOrder = q.DisplayOrder, 
                                                 QuestionText = q.QuestionText
    		                                 }).ToList();
    		    }
    		    return (List<Question>)ViewState["Questions"];
    		}
    	}

		private Scholarship _scholarshipInContext;
		private Scholarship scholarshipInContext
		{
			get
			{
				if (_scholarshipInContext == null)
					_scholarshipInContext = Container.GetDomainObject();
				return _scholarshipInContext;
			}
		}

		#endregion

		#region Page Events
		protected void Page_Init(object sender, EventArgs e)
		{
		}


		public void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
            {
                PopulateScreen();
				SetButtonsToSkip();
            }
		}

		#endregion

		#region Private Methods

		private void SetButtonsToSkip()
		{
			if (Page is IChangeCheckingPage)
			{
				var ccPage = (IChangeCheckingPage)Page;
				ccPage.BypassPromptIds.AddRange(new[] { "updateQuestionButton", 
															"cancelQuestionUpdateButton", 
															"editQuestionButton", 
															"deleteQuestionButton", 
															"addQuestionButton", 
															"addEmptyQuestionButton" });
			}
		}

		private void PopulateScreen()
        {
            PopulateCheckBoxes(AdditionalRequirements, AdditionalRequirementDAL.FindAll());
            AdditionalRequirements.Items.SelectItems(scholarshipInContext.AdditionalRequirements, ar => ar.Id.ToString());
            BindAttachedFiles();
        }

        private void BindAttachedFiles()
        {
            attachedFiles.DataSource = scholarshipInContext.Attachments;
            attachedFiles.DataBind();
        }

        public override void PopulateObjects()
        {
            var selectedAdditionalReqs = AdditionalRequirements.Items.SelectedItems(item => (object)item);
            scholarshipInContext.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll(selectedAdditionalReqs));

            int qNum = CreateOrUpdateQuestions();
            RemoveDeletedQuestions(qNum);

            if (scholarshipInContext.Stage < ScholarshipStages.AdditionalCriteria)
                scholarshipInContext.Stage = ScholarshipStages.AdditionalCriteria;

        }

        private int CreateOrUpdateQuestions()
        {
            var user = UserContext.CurrentUser;

            int qNum = 0;
            foreach (var q in Questions)
            {
            	string questionText = q.QuestionText;
                if (! String.IsNullOrEmpty(questionText))
                {
                    ScholarshipQuestion question;
                    qNum++;
                    if (qNum > scholarshipInContext.AdditionalQuestions.Count)
                    {
                        question = new ScholarshipQuestion();
                        scholarshipInContext.AddAdditionalQuestion(question);
                    }
                    else
                    {
                        question = scholarshipInContext.AdditionalQuestions[qNum - 1];
                    }
                    question.QuestionText = questionText;
                    question.DisplayOrder = qNum;
                    question.LastUpdate = new ActivityStamp(user);
                }
            }
            return qNum;
        }

        private void RemoveDeletedQuestions(int qNum)
        {
            if (qNum < scholarshipInContext.AdditionalQuestions.Count)
            {
                for (int del = scholarshipInContext.AdditionalQuestions.Count; del > qNum; del--)
                    scholarshipInContext.AdditionalQuestions.RemoveAt(del - 1);
            }
		}

		private static void PopulateCheckBoxes(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
		}

		private static void RemoveFile(string fullPath)
		{
			try
			{
				// If there was an error attempt to cleanup the file
				File.Delete(fullPath);
			}
			catch { }
		}

		#endregion

		#region Wizard Control Override Methods
		public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.AdditionalCriteria;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.AdditionalCriteria;
        }

        public override bool IsCompleted
        {
            get { return scholarshipInContext.IsStageCompleted(ScholarshipStages.AdditionalCriteria); }
            set
            {
                scholarshipInContext.MarkStageCompletion(ScholarshipStages.AdditionalCriteria, value);
                ScholarshipService.Save(scholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            return true;
		}
        #endregion

		#region Inner Classes

		public class QuestionDataProvider
		{
			private readonly IList<Question> _questions;
			public QuestionDataProvider(IList<Question> questions)
			{
				_questions = questions;
			}
			/// <summary>
			/// method to select all questions for display & edit
			/// </summary>
			/// <returns></returns>
			public IList<Question> Select()
			{
				return _questions;
			}

			/// <summary>
			/// Method used by the GridView to update a given questions text (or remove it).
			/// </summary>
			/// <param name="DisplayOrder"></param>
			/// <param name="QuestionText"></param>
			public void Update(int DisplayOrder, string QuestionText)
			{
				UpdateQuestion(DisplayOrder, QuestionText);
			}

			/// <summary>
			/// Updating may also remove a question if the text goes to string.empty.
			/// </summary>
			/// <param name="DisplayOrder"></param>
			/// <param name="QuestionText"></param>
			private void UpdateQuestion(int DisplayOrder, string QuestionText)
			{
				int removeIndex = -1;
				bool remove = false;
				foreach (var question in _questions)
				{
					removeIndex++;
					if (question.DisplayOrder == DisplayOrder)
					{
						if (QuestionText == null || QuestionText.Trim() == string.Empty)
							remove = true;
						else
							question.QuestionText = QuestionText;

						break;
					}
				}
				if (remove)
				{
					_questions.RemoveAt(removeIndex);
					int i = 0;
					foreach (var q in _questions)
					{
						q.DisplayOrder = ++i;
					}
				}
			}
		}

		[Serializable]
		public class Question
		{
			public int DisplayOrder
			{
				get;
				set;
			}
			public string QuestionText
			{
				get;
				set;
			}
		}
		#endregion

		#region control event handlers
		protected void addQuestionButton_Click(object sender, EventArgs e)
		{
            var questionText = questionsGrid.FooterRow.FindControl("questionAddTextBox") as TextBox;
            AddQuestion(questionText);
		}

		protected void addEmptyQuestionButton_Click(object sender, EventArgs e)
		{
            var questionText = questionsGrid.Controls[0].Controls[0].FindControl("questionEmptyAddTextBox") as TextBox;
            AddQuestion(questionText);
		}

        private void AddQuestion(ITextControl questionText)
        {
            if (questionText.Text != null && questionText.Text.Trim() != string.Empty)
            {
                Questions.Add(new Question { DisplayOrder = Questions.Count + 1, QuestionText = questionText.Text });
                questionsGrid.DataBind();
            }
        }

        protected void UploadFile_Click(object sender, EventArgs e)
        {
            if (null != AttachFile.PostedFile)
            {

                //check if a valid filename is being uploaded
                if (String.IsNullOrEmpty(AttachFile.PostedFile.FileName))
                {
                    AttachmentCommentsValidator.IsValid = false;
                    AttachmentCommentsValidator.Text = "Please choose a valid file to attach and try again.";
                    return;
                }

                var attachment = CreateAttachment();
                string fullPath = attachment.GetFullPathToFile(ConfigHelper.GetAttachmentsDirectory());
                try
                {
                    AttachFile.PostedFile.SaveAs(fullPath);
                    scholarshipInContext.Attachments.Add(attachment);
                    scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ScholarshipService.Save(scholarshipInContext);
                } 
                catch (Exception)
                {
                    RemoveFile(fullPath);
                    AttachmentCommentsValidator.IsValid = false;
                    AttachmentCommentsValidator.Text = "Could not upload the file. Please try again.";
                    // FIXME: Display error to user
                    if (scholarshipInContext.Attachments.Contains(attachment))
                        scholarshipInContext.Attachments.RemoveAt(scholarshipInContext.Attachments.Count -1);
                }
                BindAttachedFiles();
            }
        }

        private Attachment CreateAttachment()
        {
            string mimeType = AttachFile.PostedFile.ContentType;
            if (String.IsNullOrEmpty(mimeType))
                mimeType = "application/octet-stream";
            var attachment = new Attachment
                                 {
                                     Name = Path.GetFileName(AttachFile.PostedFile.FileName),
                                     Comment = AttachmentComments.Text,
                                     Bytes = AttachFile.PostedFile.ContentLength,
                                     MimeType = mimeType,
                                     LastUpdate = new ActivityStamp(UserContext.CurrentUser)
                                 };
            attachment.GenerateUniqueName();
            return attachment;
        }

		protected void attachedFiles_OnItemDeleting(object sender, ListViewDeleteEventArgs e)
		{
			var attachment = scholarshipInContext.Attachments[e.ItemIndex];
			if (null != attachment)
			{
				scholarshipInContext.Attachments.RemoveAt(e.ItemIndex);

				scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
				ScholarshipService.Save(scholarshipInContext);

				RemoveFile(attachment.GetFullPathToFile(ConfigHelper.GetAttachmentsDirectory()));
			}
			BindAttachedFiles();
		}

		protected void QuestionSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = new QuestionDataProvider(Questions);
		}
		#endregion

	}
}
