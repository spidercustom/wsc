﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Activate : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship _scholarshipInContext;
        Scholarship scholarshipInContext
        {
            get
            {
                if (_scholarshipInContext == null)
                    _scholarshipInContext = Container.GetDomainObject();
                if (_scholarshipInContext == null)
                    throw new InvalidOperationException("There is no scholarship in context");
                return _scholarshipInContext;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (! Page.IsPostBack)
                PopulateScreen();
        }

        private void PopulateScreen()
        {
            if (null != scholarshipInContext.AdditionalQuestions &&
                scholarshipInContext.AdditionalQuestions.Count > 0)
            {
            }
        }

        public override void PopulateObjects()
        {
            
        }

        public override void Save()
        {
            ScholarshipService.RequestActivation(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.RequestedActivation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.RequestedActivation;
        }

        public override bool IsCompleted
        {
            get { return scholarshipInContext.IsStageCompleted(ScholarshipStages.RequestedActivation); }
            set
            {
                scholarshipInContext.MarkStageCompletion(ScholarshipStages.RequestedActivation, value);
                ScholarshipService.Save(scholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            var result = Validation.Validate(scholarshipInContext);
            return result.IsValid;
        }
    }
}
