﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Show : Page
    {
        private const string SCHOLARSHIP_ID = "id";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship ScholarshipToView { get; set; }

        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params[SCHOLARSHIP_ID], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new Business.Exceptions.ProviderNotApprovedException();

            ScholarshipToView = ScholarshipService.GetById(ScholarshipId);
            if (ScholarshipToView != null)
            {
                if (!ScholarshipToView.Provider.Id.Equals(provider.Id))
                    throw new InvalidOperationException("Scholarship do not belong to provider in context");

                showScholarship.Scholarship = ScholarshipToView;

                requestActivationBtn.Visible = (ScholarshipToView.Stage != ScholarshipStages.None &&
                                                ! ScholarshipToView.IsInActivationProcess());
                
                deleteBtn.Visible = ScholarshipToView.CanEdit();
                linkCopy.NavigateUrl = "~/Provider/BuildScholarship/Default.aspx?copyfrom=" + ScholarshipToView.Id;
                ScholarshipTitleStripeControl.UpdateView(ScholarshipToView);
            }
        }

        protected void requestActivationBtn_Click(object sender, EventArgs e)
        {
            ScholarshipToView.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipToView.CanSubmitForActivation())
            {
                ScholarshipService.RequestActivation(ScholarshipToView);
                SuccessMessageLabel.SetMessage("Scholarship submitted for activation.");
                Response.Redirect("~/Provider/Scholarships/Default.aspx#pending-activated-tab");

            }
            else
            {
                ClientSideDialogs.ShowAlert("Dear User,<br><br>All sections of the scholarship are not marked as completed.<br><br> Please complete all sections and try again.</p>", "Cannot Send Activation Request!");
            }
        }

        protected void deleteBtn_Click(object sender, EventArgs e)
        {
            ScholarshipToView.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Delete(ScholarshipToView);
            SuccessMessageLabel.SetMessage("Scholarship was deleted.");
            Response.Redirect("~/Provider/Scholarships/Default.aspx");
        }
    }
}
