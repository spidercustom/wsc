﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EntityTitleStripe.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipTitleStripe" %>
<asp:Panel id="EntityTitleStripeContainer" CssClass="entity-title" runat="server" >
    <asp:Label runat="server" ID="TitleControl" CssClass="title"></asp:Label>
    <asp:Label runat="server" id="AdditionalInfoControl" CssClass="addition-info" ></asp:Label>
    <asp:Label id="PrintControl" runat="server" class="print">Print View</asp:Label>
    <br />
</asp:Panel>