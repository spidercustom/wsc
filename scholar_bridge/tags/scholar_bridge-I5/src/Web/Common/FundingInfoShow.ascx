﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.FundingInfoShow" %>

 <div id="recordinfo">
<h4>Funding Profile</h4>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>     

<table class="viewonlyTable">
    <tbody>
    <tr runat="server" id="needRow">
        <td>
            <table>
                <thead><tr><th>Need </th></tr></thead>
                <tbody>
                    <tr>
                        <th>Need Definitions</th>
                        <td ><asp:Label  ID="lblNeed" runat="server"  /></td>
                    </tr>
                    
                    <tr>
                        <th  scope="row">Seeker Need Amount</th>
                        <td><asp:Label ID="lblMinMaxNeed"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Need GAP Threshold </th>
                        <td><asp:Label ID="lblNeedGAP"  runat="server"  /></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr runat="server" id="fundingSituationRow">
        <td>
            <table>
                <thead><tr><td>Funding Situation</td></tr></thead>
                <tr>
                    <td>Types of Support:</td>
                    <td><asp:Label ID="lblTypeOfSupport" runat="server"></asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr runat="server" id="fundingParametersRow">
        <td>
            <table>
                <thead><tr><td>Funding Parameters</td></tr></thead>
                <tr>
                    <td>Annual Support Amount: </td>
                    <td><asp:Label ID="lblAnnualSupportAmount" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>Minimum Number Of Awards: </td>
                    <td><asp:Label ID="lblMinimumNumberOfAwards" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>Maximum Number Of Awards: </td>
                    <td><asp:Label ID="lblMaximumNumberOfAwards" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>Terms Of Support: </td>
                    <td><asp:Label ID="lblTermsOfSupport" runat="server"></asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>


</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
