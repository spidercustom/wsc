﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminScholarshipNotes.ascx.cs" Inherits="ScholarBridge.Web.Common.AdminScholarshipNotes" %>

<label for="existingNotesTb">Administrative Notes:</label>  
<asp:TextBox ID="existingNotesTb" runat="server" Enabled="false" TextMode="MultiLine" Columns="300" Rows="10"></asp:TextBox><br />
<label for="existingNotesTb">Add Notes:</label>  
<asp:TextBox ID="newNotesTb" runat="server" TextMode="MultiLine" Columns="300" Rows="5"></asp:TextBox><br />
<asp:Button ID="addNotes" runat="server" Text="Add Note" onclick="addNotes_Click"  />
<br />