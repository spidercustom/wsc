﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipTitleStripe : UserControl
    {
        protected override void OnPreRender(EventArgs e)
        {
            //show only if there is something to show.
            EntityTitleStripeContainer.Visible = !string.IsNullOrEmpty(TitleControl.Text);
            base.OnPreRender(e);
        }

        public void UpdateView(Scholarship scholarship)
        {
            TitleControl.Text = scholarship.DisplayName;
            AdditionalInfoControl.Text = string.Format("({0})", scholarship.GetStatusText());
        }

        public bool HasPrintView
        {
            get { return PrintControl.Visible; }
            set { PrintControl.Visible = value; }
        }
    }
}
