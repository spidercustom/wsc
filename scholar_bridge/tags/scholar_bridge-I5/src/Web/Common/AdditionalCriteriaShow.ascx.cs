﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Common
{
    public partial class AdditionalCriteriaShow : BaseScholarshipShow
    {
        public override ScholarshipStages Stage { get { return ScholarshipStages.AdditionalCriteria; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);

            if (!Page.IsPostBack)
            {
                AdditionalRequirementsRepeater.DataSource = ScholarshipToView.AdditionalRequirements;
                AdditionalRequirementsRepeater.DataBind();
                RequirementsFooterRow.Visible = ScholarshipToView.AdditionalRequirements.Count <= 0;

                AdditionalCriteriaRepeater.DataSource = ScholarshipToView.AdditionalQuestions;
                AdditionalCriteriaRepeater.DataBind();

                AttachedFiles.DataSource = ScholarshipToView.Attachments;
                AttachedFiles.DataBind();
            }
        }

        protected void downloadAttachmentBtn_OnCommand(object sender, CommandEventArgs e)
        {
            var id = Int32.Parse((string) e.CommandArgument);
            var attachment = ScholarshipToView.Attachments.First(a => a.Id == id);
            SendFile(attachment);
        }

        private void SendFile(Attachment attachment)
        {
            Response.ClearHeaders();
            Response.AddHeader("Content-Type", attachment.MimeType);
            Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", attachment.Name));
            Response.WriteFile(attachment.GetFullPathToFile(ConfigHelper.GetAttachmentsDirectory()));
        }
    }
}