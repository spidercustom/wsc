﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowScholarship.ascx.cs" Inherits="ScholarBridge.Web.Common.ShowScholarship" %>

<%@ Register src="~/Common/GeneralInfoShow.ascx" tagname="GeneralInfoShow" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerInfoShow.ascx" tagname="SeekerInfoShow" tagprefix="sb" %>
<%@ Register src="~/Common/FundingInfoShow.ascx" tagname="FundingInfoShow" tagprefix="sb" %>
<%@ Register src="~/Common/AdditionalCriteriaShow.ascx" tagname="AdditionalCriteriaShow" tagprefix="sb" %>
<%@ Register src="~/Common/AdminScholarshipNotes.ascx" tagname="AdminScholarshipNotes" tagprefix="sb" %>

<div class="tabs">
    <ul class="linkarea">
        <li><a  href="#generalinfo-tab"><span>General Information</span></a></li>
        <li><a href="#seekerprofile-tab"><span>Seeker Profile</span></a></li>
        <li><a href="#fundingprofile-tab"><span>Funding Profile</span></a></li>
        <li><a href="#additionalcriteria-tab"><span>+Requirements</span></a></li>
        <asp:LoginView ID="loginView" runat="server">
        <RoleGroups>
            <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
                <ContentTemplate>
        <li><a href="#notes-tab"><span>Admin Notes</span></a></li>
                </ContentTemplate>
            </asp:RoleGroup>
         </RoleGroups>
         </asp:LoginView>
    </ul>
     
    <div id="generalinfo-tab">
        <sb:GeneralInfoShow ID="GeneralInfoShow1" runat="server" />
    </div>
    <div id="seekerprofile-tab">
        <sb:SeekerInfoShow ID="SeekerInfoShow1" runat="server" />
    </div>
    <div id="fundingprofile-tab">
        <sb:FundingInfoShow ID="FundingInfoShow1" runat="server"  />
    </div>
    <div id="additionalcriteria-tab">
        <sb:AdditionalCriteriaShow ID="AdditionalCriteriaShow1" runat="server" />
    </div>
    <asp:LoginView ID="loginView1" runat="server">
    <RoleGroups>
        <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
            <ContentTemplate>
    <div id="notes-tab">
        <sb:AdminScholarshipNotes ID="AdminScholarshipNotes1" runat="server" />
    </div>
            </ContentTemplate>
        </asp:RoleGroup>
     </RoleGroups>
    </asp:LoginView>
</div>