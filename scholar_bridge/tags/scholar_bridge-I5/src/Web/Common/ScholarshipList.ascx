﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipList.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipList" %>

<asp:ListView ID="lstScholarships" runat="server" 
    OnItemDataBound="lstScholarships_ItemDataBound" 
    onpagepropertieschanging="lstScholarships_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Academic Year</th>
                <th>Current Stage</th>
                <th>Application Start Date</th>
                <th>Application Due Date</th>
                <th>Number of Awards</th>
                <th>Scholarship Amount (total dollars available for all awards)</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# DataBinder.Eval(Container.DataItem, "DisplayName") %></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# ((ScholarshipStages) DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName() %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate")%></td>
        <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "FundingProfile.FundingParameters"), "MaximumNumberOfAwards")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "MaximumAmount", "{0:c}")%></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# DataBinder.Eval(Container.DataItem, "DisplayName") %></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# ((ScholarshipStages) DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName() %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate")%></td>
        <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "FundingProfile.FundingParameters"), "MaximumNumberOfAwards")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "MaximumAmount", "{0:c}")%></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships, </p>
    <ul class="pageNav">
        <li><asp:HyperLink ID="createScholarshipLnk" runat="server" NavigateUrl="~/Provider/BuildScholarship/">Create a New Scholarship</asp:HyperLink></li>
    </ul>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="lstScholarships" PageSize="20" >
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
</div> 
