﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class AdminScholarshipNotes : UserControl
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship Scholarship { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            existingNotesTb.Text = Scholarship.AdminNotes;
        }

        protected void addNotes_Click(object sender, EventArgs e)
        {
            Scholarship.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            Scholarship.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            ScholarshipService.Save(Scholarship);
            newNotesTb.Text = null;
            existingNotesTb.Text = Scholarship.AdminNotes;
        }
    }
}