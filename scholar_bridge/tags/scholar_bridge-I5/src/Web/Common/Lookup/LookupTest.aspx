<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="LookupTest.aspx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupTest"  %>
<%@ Import Namespace="ScholarBridge.Web.Common.Lookup"%>

<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="uc1" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="uc2" %>

<%@ Register src="../CalendarControl.ascx" tagname="CalendarControl" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="/js/lookupdialog.js" type="text/javascript"></script>
        <link href="/styles/lookupdialog.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(function() {
        $("#datepicker").datepicker({ showOn: 'button', buttonImage: '/images/calendar.gif', buttonImageOnly: true });
    });
	</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<table>
        
 <tr><td>
    <asp:TextBox runat="server" ID="txtbackColor" > </asp:TextBox>
 </td><td> <uc1:LookupDialog ID="LookupDialog1" runat="server" BuddyControl="txtbackColor"  ItemSource="SeekerVerbalizingWordDAL"   Title="5 Words Selection Dialog"   /></td></tr>
 
 <tr><td><asp:Button runat="server" Text="Submit" ID="btnSubmit" 
         onclick="btnSubmit_Click"  /> 
         <asp:Button runat="server" Text="SetItems" ID="btnSet" 
         onclick="btnSet_Click" /> 
     
     </td></tr>
  <tr><td>
  
  </td></tr> 
  <tr><td>
  <asp:TextBox runat="server" ID="txtforeColor" > </asp:TextBox>
      <uc2:LookupDialog ID="LookupDialog2" UserData="State:'MH'" BuddyControl="txtforeColor" SelectionLimit="5" ItemSource="StateDAL"  runat="server"  Title="Select Your State" />
      
 </td></tr>  
 <tr><td>Calendar Test :<input type="text" id="datepicker"> </td></tr>
 <tr><td>ascx : 
     <uc3:CalendarControl ID="CalendarControl1" runat="server" />
     </td></tr>
 </table>
   
   
</asp:Content>

