﻿using System.Text;

namespace ScholarBridge.Web.Common.Lookup
{
    public static class LookupDialogJSHelper
    {
        public static string BuildCreateDialogJS(string dialogId, string buddyId,string hiddenID,string otherControlID,string rightDivID,string otherboxDivID,string otherTextBoxID,string otherLabelID)
        {
            var sb = new StringBuilder();
            sb.Append("<script language='javascript'> $(function() {");
            sb.AppendFormat("createDialog('#{0}','#{1}','#{2}','#{3}','#{4}','#{5}','#{6}','#{7}');", dialogId, buddyId, hiddenID, otherControlID, rightDivID, otherboxDivID, otherTextBoxID,otherLabelID);
            sb.Append("}); </script>");
            return sb.ToString();
        }

        public static string BuildShowDialogJS(string dialogId, string availableDivId, string selectionDivId, string pagerDivId, string dataUrl, string hiddenID, string searchButtonId, string searchBoxId, string ajaxprogress, string selectionLimit, string hiddenUserDataID)
        {
            return string.Format("JavaScript: return showDialog('#{0}','#{1}','#{2}','#{3}','{4}','#{5}','#{6}','#{7}','#{8}',{9},'#{10}');", dialogId, availableDivId, selectionDivId, pagerDivId, dataUrl, hiddenID, searchButtonId, searchBoxId, ajaxprogress, selectionLimit, hiddenUserDataID);
        }
        
        public static string BuildaddToSelectionsJS(string hiddenid)
        {
            return string.Format("addToSelections();");
        }

        public static string BuildremoveFromSelectionsJS(bool withAll)
        {
            return string.Format("removeFromSelections('{0}');", withAll);
        }
    }
}
