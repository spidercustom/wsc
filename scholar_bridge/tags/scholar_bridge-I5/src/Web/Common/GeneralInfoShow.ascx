﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.GeneralInfoShow" %>

<div id="recordinfo">
<h4>General Information</h4>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>
<table class="viewonlyTable">
    <tbody>
        <tr>
            <th>Name</th>
            <td ><asp:Label  ID="lblName" runat="server"  /></td>
        </tr>
        <tr>
            <th>Academic Year</th>
            <td ><asp:Label  ID="lblAcademicYear" runat="server"  /></td>
        </tr>
        <tr>
            <th  scope="row">Mission Statement</th>
            <td><asp:Label ID="lblMission"  runat="server"  /></td>
        </tr>
        <tr>
            <th  scope="row">Program Guidelines 
                <span id="EditControlContainer" runat="server">
                    [<asp:HyperLink ID="editProgramGuidelinesLnk" runat="server">Edit</asp:HyperLink>]
                </span>    
            </th>
            <td><asp:Label ID="lblProgramGuidelines"  runat="server"  /></td>
        </tr>
        <tr>
            <th scope="row">Schedule</th>
            <td>
            <table>
                <tbody>
                    <tr>
                        <th>Application Start On</th>
                        <td><asp:Label  ID="lblAppStart" runat="server"  /></td>
                    </tr>
                    <tr>
                        <th  scope="row">Application Due On</th>
                        <td><asp:Label ID="lblAppDue"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th  scope="row">Award On</th>
                        <td><asp:Label ID="lblAwardOn"  runat="server"  /></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <th scope="row">Award Amount</th>
            <td><asp:Label ID="lblAwardAmount" runat="server"  /></td>
        </tr>
        <tr>
            <th scope="row">Donor</th>
            <td><asp:Label ID="lblDonor"  runat="server"  /></td>
        </tr>
    </tbody>
</table>

</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
