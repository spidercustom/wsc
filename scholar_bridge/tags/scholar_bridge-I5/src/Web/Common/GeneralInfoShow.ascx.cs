﻿using System;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class GeneralInfoShow : BaseScholarshipShow
    {
        public override ScholarshipStages Stage { get { return ScholarshipStages.GeneralInformation; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //setup links
            SetupEditLinks(linkarea1, linkarea2);

            lblName.Text = ScholarshipToView.DisplayName;
            lblAcademicYear.Text = ScholarshipToView.AcademicYear.ToString();
            lblMission.Text = ScholarshipToView.MissionStatement;
            lblProgramGuidelines.Text = ScholarshipToView.ProgramGuidelines;
            lblAppStart.Text = ScholarshipToView.ApplicationStartDate.HasValue
                                   ? ScholarshipToView.ApplicationStartDate.Value.ToString("d")
                                   : "";
            lblAppDue.Text = ScholarshipToView.ApplicationDueDate.HasValue
                                 ? ScholarshipToView.ApplicationDueDate.Value.ToString("d")
                                 : "";
            lblAwardOn.Text = ScholarshipToView.AwardDate.HasValue
                                  ? ScholarshipToView.AwardDate.Value.ToString("d")
                                  : "";

            lblAwardAmount.Text = string.Format("{0} - {1}", ScholarshipToView.MinimumAmount.ToString("c" ),
                                                ScholarshipToView.MaximumAmount.ToString("c"));
            //view donor info
            lblDonor.Text = GetDonorDisplayString(ScholarshipToView.Donor);
        }

        private static string GetDonorDisplayString(ScholarshipDonor donor)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("{0}, {1}", donor.Name, donor.Address);
            
            if (null != donor.Phone && !string.IsNullOrEmpty(donor.Phone.Number))
                sb.AppendFormat(" Phone : {0}", donor.Phone.Number);

            if (!string.IsNullOrEmpty(donor.Email))
                sb.AppendFormat(" Email : {0}", donor.Email);            
            return sb.ToString();
        }

        protected override void SetupEditLinks(HtmlGenericControl linkarea1, HtmlGenericControl linkarea2)
        {
            base.SetupEditLinks(linkarea1, linkarea2);

            editProgramGuidelinesLnk.NavigateUrl = "~/Provider/Scholarships/EditProgramGuidelines.aspx?id=" + ScholarshipToView.Id;
            EditControlContainer.Visible = CanEditGuidelines();
        }

        private bool CanEditGuidelines()
        {
            return !ScholarshipToView.CanEdit() &&
                   (Roles.IsUserInRole(Role.PROVIDER_ROLE) || Roles.IsUserInRole(Role.INTERMEDIARY_ROLE));
        }
    }
}
