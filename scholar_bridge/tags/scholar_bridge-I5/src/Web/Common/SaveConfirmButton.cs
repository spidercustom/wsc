﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:SaveConfirmButton runat=server></{0}:SaveConfirmButton>")]
    public class SaveConfirmButton : WebControl, IPostBackEventHandler
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue("")]
        public string ConfirmMessageDivID
        {
            get
            {
                var s = (String)ViewState["ConfirmMessageDivID"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["ConfirmMessageDivID"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptInclude(GetType(), "DEFAULT", ResolveUrl("~/js/SaveConfirmButton.js"));
            base.OnPreRender(e);
        }

        private const string ON_CLICK_SCRIPT_TEMPLATE = "javascript:SavingYesNoCancelDialog('{0}', '{1}');";
        protected override void Render(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(ConfirmMessageDivID))
                throw new ArgumentNullException("ConfirmMessageDivID property cannot be left empty");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, ON_CLICK_SCRIPT_TEMPLATE.Build(ConfirmMessageDivID, UniqueID));
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
            writer.WriteLine();
        }

        public event EventHandler<SaveConfirmButtonClickEventArgs> Click;

        public void RaisePostBackEvent(string eventArgument)
        {
            if (null != Click)
            {
                Page.Validate();
                var saveAndContinue = bool.Parse(eventArgument);
                var args = new SaveConfirmButtonClickEventArgs {SaveBeforeContinue = saveAndContinue};
                Click(this, args);
            }
        }
    }

    public class SaveConfirmButtonClickEventArgs : EventArgs
    {
        public bool SaveBeforeContinue { get; set; }    
    }
}
