﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Reconfirm.aspx.cs" Inherits="ScholarBridge.Web.Reconfirm" Title="Resend Confirmation Email" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<asp:Wizard ID="reconfirmWizard" runat="server" 
        OnNextButtonClick="StepNextButton_ButtonClick" DisplaySideBar="False">
    <WizardSteps>
        <asp:WizardStep ID="getUserInfo" runat="server" Title="Resend Confirmation Email">
            <asp:Label id="errorLbl" runat="server" Visible="false" />
            <br />
            <label for="EmailAddress">Email Address:</label>
            <asp:TextBox ID="EmailAddress" runat="server" />
            <br />
            <asp:RequiredFieldValidator ID="emailAddressReqValid" ControlToValidate="EmailAddress" runat="server" ErrorMessage="Email Address is required to continue." />
            <br />
        </asp:WizardStep>
        <asp:WizardStep ID="complete" runat="server" Title="Confirmation Email Sent">
            <h3><asp:Label id="doneMessage" runat="server"/></h3>
            <asp:HyperLink ID="loginLnk" runat="server" NavigateUrl="~/Login.aspx">Return to Login page</asp:HyperLink>
        </asp:WizardStep>
    </WizardSteps>
    <StepNavigationTemplate>
      <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" CausesValidation="True" Text="Send" />
    </StepNavigationTemplate>
    <FinishNavigationTemplate></FinishNavigationTemplate>
</asp:Wizard>
</asp:Content>
