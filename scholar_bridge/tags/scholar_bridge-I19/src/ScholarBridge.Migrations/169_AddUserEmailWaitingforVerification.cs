﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(169)]
    public class AddUserEmailWaitingforVerification : Migration
    {
        private const string TABLE_NAME = "SBUser";
        private const string EMAIL = "EmailWaitingforVerification";
         
 
        public override void Up()
        {
            Database.AddColumn(TABLE_NAME,EMAIL,DbType.String,50,ColumnProperty.Null  );
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME,EMAIL);
        }
    }
}
