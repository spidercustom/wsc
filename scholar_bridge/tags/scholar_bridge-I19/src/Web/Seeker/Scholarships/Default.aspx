﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" EnableEventValidation="false"
     Inherits="ScholarBridge.Web.Seeker.Scholarships.Default"  Title="Seeker | My Scholarships" %>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopMyApplications.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomMyApplications.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<!--Left floated content area starts here-->
                <DIV id="HomeContentLeft">

                  <Img src="<%= ResolveUrl("~/images/PgTitle_MyApplications.gif") %>" width="211px" height="54px">
                  <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerLoggedIn.gif") %>" width="513px" height="96px">
                  </DIV>
                  <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <DIV id="HomeContentRight">
                 <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                 </DIV>
                <BR><BR>

                 <DIV id="Clear"></DIV>
                 
<h3>List of My Applications</h3>
<asp:Label ID="lblProfileActivationMessage" Text="You must activate your profile to save and apply for scholarships." runat="server" CssClass="noteBene" />
<asp:ListView ID="myScholarhipList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    onpagepropertieschanging="matchList_PagePropertiesChanging" >
    <LayoutTemplate>
        <table class="sortableTable">
                <thead>
                <tr>
                    <th>Scholarship Name</th>
                    <th>Application Due Date</th>
                    <th># of Awards</th>
                    <th>Amount $</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th>&nbsp</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
          
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton id="linkToScholarship" runat="server" ><%# Eval("Scholarship.Name")%></asp:LinkButton></td>
            <td><%# ((Application)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((Application)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0}", ((Application)Container.DataItem).Scholarship.AmountRange)%></td>
            <td><%# Eval("ApplicationStatus") %></td>
            <td><asp:Button ID="defaultActionButton" runat="server"  CssClass="ListButton" Width="60px"  /></td>
            <td><asp:Label ID="submittedDateLabel" runat="server" /></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton id="linkToScholarship" runat="server" ><%# Eval("Scholarship.Name")%></asp:LinkButton></td>
            <td><%# ((Application)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((Application)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0}", ((Application)Container.DataItem).Scholarship.AmountRange)%></td>
            <td><%# Eval("ApplicationStatus")%></td>
            <td><asp:Button ID="defaultActionButton" runat="server"  CssClass="ListButton" Width="60px"/></td>
            <td><asp:Label ID="submittedDateLabel" runat="server" /></td>
        </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
        <p>You have no scolarship applications at this time.</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="myScholarhipList" PageSize="20" onprerender="pager_PreRender" >
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
</div> 

</asp:Content>
