﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Web.Seeker.Applications
{
    public partial class Submit : Page
    {
        public IUserContext UserContext { get; set; }
        public IApplicationService ApplicationService { get; set; }
        private const string DEFAULT_PAGEURL = "~/Seeker/Applications/Show.aspx?aid={0}";
        private const string MATCHES_PAGEURL = "~/Seeker/Matches";
        private const string SUBMISSION_SUCCESS_NOTE = @"Congratulations, you have successfully Submitted the application. Good luck!";
	   
        private int ApplicationId
        {
            get
            {
                int applicationId;
                if (!Int32.TryParse(Request.Params["aid"], out applicationId))
                    throw new ArgumentException("Cannot understand value of parameter aid");
                return applicationId;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UserContext.EnsureSeekerIsInContext();

                var application = ApplicationService.GetById(ApplicationId);
                if (application == null)
                    throw new ArgumentNullException("application");

                if (!(application.Seeker == UserContext.CurrentSeeker))
                    throw new InvalidOperationException("Application doesn't belong to seeker in context.");



                ApplicationService.SubmitApplication(application);
                SuccessMessageLabel.SetMessage(SUBMISSION_SUCCESS_NOTE);
                Response.Redirect(DEFAULT_PAGEURL.Build(application.Id),false );
            } catch (Exception ex)
            {
                var msg =
                    "Dear User, An error has occured while submitting your application, Please try again. Error: ({0})";
                msg=msg.Build(Server.HtmlEncode(ex.Message));
                SuccessMessageLabel.SetMessage(msg);
                Response.Redirect(MATCHES_PAGEURL,false );


            }
        }
    }
}
