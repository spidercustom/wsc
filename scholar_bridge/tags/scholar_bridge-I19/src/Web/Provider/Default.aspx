﻿<%@ Page Title="Provider" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Default" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
  <!--Left floated content area starts here-->
               

                  <Img src="<%= ResolveUrl("~/images/PgTitle_SmarterSchoMatches.gif") %>" width="343px" height="28px">
                 <P class="HighlightedTextMain">tlskjdlsfjd olor sit amet, consectetur adipiscing elit.
                  Nullam viverra facilisis massa, sed porta justo
                  dapibus vitae. Aenean congue pellentesque massa, at
                  euismod lorem bibendum sit amet. </P>
                  <input type="image" src="<%= ResolveUrl("~/images/Btn_GotoMyProfile.gif") %>" width="145px" height="32px" action="submit">
                  
                 <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <%--<DIV id="HomeContentRight">
                      <img src="<%= ResolveUrl("~/images/PageTitleSmall_ScholarshipCompleteness.gif") %>" width="181px" height="28px">
                      <DIV id="PercentageGraphic"></DIV>
                      <img src="<%= ResolveUrl("~/images/Percentile.gif") %>" width="248px" height="37px">
                      <P class="HighlightedTextRightContent">You’re almost there! We just need a bit more info about the scholarship.</P>
                      <P class="RightContentGreenText">>&nbsp;Finish Scholarship Info</P>
                 </DIV>--%>
                 <!--Right Floated content area ends here-->
                 <BR><BR>

                 <DIV id="Clear"></DIV>
                 <HR>
                  


                <div style='clear: both;'></DIV>


                 <!--The Three column starts here-->
                 <DIV id="BoxWrapper">
                      <DIV id="LeftBottomBox">
                           <Img src="<%= ResolveUrl("~/images/BottomBox01_BuildScholarships.gif") %>" width="262px" height="51px">
                           <Img src="<%= ResolveUrl("~/images/ProviderBottomBox01.gif") %>" width="237px" height="86px">

                           <P>Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                           <p class="IcoBoxArrow"><A href="<%= ResolveUrl("~/Provider/BuildScholarship") %>"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px">&nbsp;Start Building</A></P>

                      </DIV>
                      <DIV id="CenterBottomBox">
                            <img src="<%= ResolveUrl("~/images/BottomBox02_ManageApplications.gif") %>" width="262px" height="51px">
                            <Img src="<%= ResolveUrl("~/images/ProviderBottomBox02.gif") %>" width="237px" height="86px">
                            <P>Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                            <p class="IcoBoxArrow"><A href="<%= ResolveUrl("~/Provider/Scholarships") %>"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px">&nbsp;Start Reviewing</A></P>
                      </DIV>
                      <DIV id="RightBottomBox">
                            <img src="<%= ResolveUrl("~/images/BottomBox03_AwardScholarship.gif") %>" width="262px" height="51px">
                            <Img src="<%= ResolveUrl("~/images/ProviderBottomBox03.gif") %>" width="237px" height="86px">
                            <P>Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                            <p class="IcoBoxArrow"><A href="<%= ResolveUrl("~/Provider/Scholarships") %>"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px">&nbsp;View Applicants</A></P>
                      </DIV>



                 </DIV>   
           
           
    <sb:Login ID="loginForm" runat="server" />
    
    <asp:LoginView ID="loginView" runat="server">
        <AnonymousTemplate>
            <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Provider/RegisterProvider.aspx">Register as a Scholarship Provider</asp:HyperLink>
        </AnonymousTemplate>
    </asp:LoginView>
  
</asp:Content>
