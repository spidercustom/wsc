﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Provider.Admin
{
	public partial class Default : Page
	{
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            
                editOrg.Organization = UserContext.CurrentProvider;

            relationshiplist.Relationships = RelationshipService.GetByProvider(provider);
            orgUserList.Users = provider.Users;
            adminDetails.UserToView = UserContext.CurrentOrganization.AdminUser;
            adminDetails1.UserToView = UserContext.CurrentOrganization.AdminUser;
			LinkGenerator linker = new LinkGenerator();
			createUserLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", linker.GetFullLink("Provider/Users/Create.aspx?popup=true"));
			addRequestLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", linker.GetFullLink("Provider/Relationships/Create.aspx?popup=true")); 

            
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!User.IsInRole(Role.PROVIDER_ADMIN_ROLE))
            {
                createUserLink.Visible = false;
                editOrg.ViewOnlyMode = true;
            }
        }
        protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.Update((Domain.Provider)org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Provider/Admin");
        }

        
	}
}
