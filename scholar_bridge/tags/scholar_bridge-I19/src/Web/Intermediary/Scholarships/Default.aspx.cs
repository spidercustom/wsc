﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Intermediary.Scholarships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            FillScholarships();
        }

        private void FillScholarships()
        {
            var intermediary = UserContext.CurrentIntermediary;

            if (scholarshipsListViewOptions.GetSelectedOrganizationId() == 0)
            {
                //fetch all
                scholarshipsList.Scholarships = ScholarshipService.GetByIntermediary(intermediary, scholarshipsListViewOptions.GetSelectedStages());
            }
            else
            {
                var provider = ProviderService.FindById(scholarshipsListViewOptions.GetSelectedOrganizationId());

                scholarshipsList.Scholarships = ScholarshipService.GetByOrganizations(provider, intermediary, scholarshipsListViewOptions.GetSelectedStages());
            }
        }

        protected void UpdateViewBtn_Click(object sender, EventArgs e)
        {
            FillScholarships();
        }
    }
}