﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ScholarBridge.Web.Common.Login" %>
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login   ID="Login1" runat="server" onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError"  FailureText="(!) Invalid email or password. Please try again."   >
            <%--UserNameLabelText=""  
            PasswordRecoveryUrl="~/ForgotPassword.aspx"  
            LoginButtonImageUrl="~/images/Btn_Login.gif"
            PasswordLabelText=""
            RememberMeText="Remember my username on this computer?" 
            TitleTextStyle-CssClass="loginTitleText" 
            onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError" 
            LoginButtonType="Image" 
            DisplayRememberMe="False">--%>
           
            
     <LayoutTemplate >
       <h1>Returning User? Log in here.</h1>
       <asp:Textbox id="UserName" text="Email Address" runat="server" Cssclass="DashboardUnlogged" />
       <asp:TextBox TextMode="Password"  id="Password" text="password" runat="server" Cssclass="DashboardUnlogged" />
       <asp:Button  Cssclass="Button"  ID="Login"  CommandName="Login" runat="server"  />
       <p class="ErrorMsg"><asp:Literal ID="FailureText"  runat="Server"  /></P>
       <p class="GreenText">New User? <asp:HyperLink id="registerlink" runat="server"  Text="Register" /></P>
     </LayoutTemplate>
     
     </asp:Login>
    </AnonymousTemplate>    
</asp:LoginView>