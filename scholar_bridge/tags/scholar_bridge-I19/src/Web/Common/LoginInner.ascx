﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginInner.ascx.cs" Inherits="ScholarBridge.Web.Common.LoginInner" %>
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login   ID="Login1" runat="server" onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError"  FailureText="(!) Invalid email or password. Please try again."   >
            <%--UserNameLabelText=""  
            PasswordRecoveryUrl="~/ForgotPassword.aspx"  
            LoginButtonImageUrl="~/images/Btn_Login.gif"
            PasswordLabelText=""
            RememberMeText="Remember my username on this computer?" 
            TitleTextStyle-CssClass="loginTitleText" 
            onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError" 
            LoginButtonType="Image" 
            DisplayRememberMe="False">--%>
           
            
     <LayoutTemplate >
     
                    <P class="LoginItalicText"><asp:label runat="server" ID="returningLabel" Text="Returning User? Log in here."></asp:label></P>
                      <TABLE cellpadding="2" cellspacing="2" border="0">
                         <TR>
                             <TD><Label>Username:</Label></TD>
                             <TD><asp:Textbox id="UserName" text="Email Address" runat="server" /></TD>

                         </TR>
                         
                         <TR>
                             <TD><Label>Password:</Label></TD>
                             <TD><asp:TextBox TextMode="Password"  id="Password" text="password" runat="server" /></TD>
                         </TR>
                         
                         <TR>
                             <TD>&nbsp;</TD>
                             <TD align="right"><asp:ImageButton ID="Login"  CommandName="Login" runat="server" ImageUrl ="~/images/Btn_Login.gif" width="88px" height="31px" ImageAlign="AbsMiddle"/></TD>
                         </TR>
                         <tr>
                                <td colspan="3" class="LoginErrorMessage"> <asp:Literal ID="Literal1"  runat="Server"  /></td>
                                </tr>
                         <TR>
                             <TD>&nbsp;</TD>
                             <TD align="right" class="GeneralBodyText">New User? <asp:LinkButton id="registerlink" runat="server" OnClick="registerlink_click"  Text="Register" Cssclass="GreenLink"/></TD>
                         </TR>
                      </TABLE>   
     </LayoutTemplate>
     
     </asp:Login>
    </AnonymousTemplate>    
</asp:LoginView>