﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Data;
using ScholarBridge.Domain.Lookup;
using Spring.Context.Support;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class LookupItemCheckboxList : System.Web.UI.UserControl
    {
        public LookupItemCheckboxList()
        {
            CheckboxListWidth = new Unit(450, UnitType.Pixel);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
       

        public void DeselectAll()
        {
            foreach (ListItem item in CheckBoxList.Items)
                item.Selected = false;
        }


        private ILookupDAL lookupService;
        private string lookupServiceSpringContainerKey;
        public Unit CheckboxListWidth { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            CheckBoxList.Width = CheckboxListWidth;
            base.OnPreRender(e);
        }

        public string LookupServiceSpringContainerKey
        {
            get { return lookupServiceSpringContainerKey; }
            set
            {
                lookupServiceSpringContainerKey = value;
                var context = ContextRegistry.GetContext();
                var retrivedObject = context.GetObject(value);
                if (null == retrivedObject)
                    throw new ArgumentException(string.Format("Cannot retrieve object by key {0}", value));
                lookupService = retrivedObject as ILookupDAL;
                if (null == lookupService)
                    throw new ArgumentException(string.Format("Cannot cast object retrieved by key {0} to ILookupDAL", value)); 
                DataBind();
            }
        }


        public override void DataBind()
        {
            if (null != lookupService)
            {
                CheckBoxList.DataSource = lookupService.FindAll();
                CheckBoxList.DataTextField = "Name";
                CheckBoxList.DataValueField = "Id";
            }

            base.DataBind();
			
        }

        [DefaultValue(false)]
        public bool SelectNoneAllowed { get; set; }

        [Browsable(false)]
        public IList<ILookup> SelectedValues
        {
            get
            {
                var selectedItems = from ListItem item in CheckBoxList.Items
                                    where item.Selected
                                    select item;
                
                var result = new List<ILookup>();
                
                foreach (var item in selectedItems)
                {
                    int selectedItemId = Int32.Parse(item.Value);
                    var retrivedObject = lookupService.FindById(selectedItemId);
                    result.Add(retrivedObject);
                }
                return result;
            }
            set
            {
                DeselectAll();
                CheckBoxList.Items.SelectItems(value, lookupItem => lookupItem.Id.ToString());
            }
        }

        public void PopulateListFromSelectedValues<T>(IList<T> list)
        {
            list.Clear();
            IEnumerable<T> selectedItems = SelectedValues.Cast<T>();
            selectedItems.ForEach(o => list.Add(o));
        }

       

       
       
    }
}