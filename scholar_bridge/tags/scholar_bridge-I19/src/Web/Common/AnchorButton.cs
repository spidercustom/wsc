﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{

    /// <summary>
    /// AnchorButton is action control. It's primarily built to
    /// give better look to a button like action control.
    /// It's combination of two elements span contained by anchor
    /// </summary>
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:AnchorButton runat=server></{0}:AnchorButton>")]
    public class AnchorButton : WebControl, IPostBackEventHandler
    {
        private const string CLICK_EVENT_KEY = "click";
        [DefaultValue(true)]
        public bool CausesValidation
        {
            get
            {
                return Convert.ToBoolean(ViewState["CausesValidation"] ?? true);
            }
            set
            {
                ViewState["CausesValidation"]  = value;
            }
        }


        /// <summary>
        /// Gets or sets the text of the button.
        /// </summary>
        /// <value>The text.</value>
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["Text"] = value;

            }
        }


        /// <summary>
        /// Gets or sets the URL to navigate to if user clicks on elements.
        /// 
        /// If navigation url is set, OnClientClick and OnClick are ignored
        /// </summary>
        /// <value>The navigation URL.</value>
        public string NavigationUrl
        {
            get 
            {
                return Convert.ToString(ViewState["NavigationUrl"] ?? String.Empty);
            }
            set
            {
                ViewState["NavigationUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the validation group of the control.
        /// 
        /// Only make sense in case of OnClick
        /// </summary>
        /// <value>The validation group.</value>
        public string ValidationGroup
        {
            get 
            {
                return Convert.ToString(ViewState["ValidationGroup"] ?? String.Empty);
            }
            set
            {
                ViewState["ValidationGroup"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the anchor (contaner) CSS class.
        /// </summary>
        /// <value>The anchor CSS class.</value>
        public string AnchorCssClass { get; set; }

        /// <summary>
        /// Gets or sets the javascript to execute on client click.
        /// 
        /// If OnClientClick is set, OnClick is ignored.
        /// </summary>
        /// <value>The on client click.</value>
        public string OnClientClick { get; set; }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("a");
            writer.WriteAttribute("class", BuildContainerCssClass());
            var controlwidth = Width.IsEmpty ? 150 : Width.Value;
            writer.WriteAttribute("id", ClientID);
            writer.WriteAttribute("name", UniqueID);
            writer.WriteAttribute("width", controlwidth + "px");
            if (Enabled)
            {
                if (!string.IsNullOrEmpty(NavigationUrl))
                    writer.WriteAttribute("href", ResolveClientUrl(NavigationUrl));
                else if (!string.IsNullOrEmpty(OnClientClick))
                    writer.WriteAttribute("onclick", OnClientClick);
                else
                    writer.WriteAttribute("onclick", BuildPostBackScript());
            }
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();

            renderText(writer);
            writer.WriteLine();

            writer.WriteEndTag("a");
        }


        private string BuildContainerCssClass()
        {
            var result = new StringBuilder();
            
            result.Append(Enabled ? "button" : "disabled-button");
            if (!string.IsNullOrEmpty(CssClass))
                result.AppendFormat(",{0}", CssClass);
            if (!string.IsNullOrEmpty(AnchorCssClass))
                result.AppendFormat(",{0}", AnchorCssClass);

            return result.ToString();
        }

        private void renderText(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("span");
            writer.WriteAttribute("class", Enabled ? "button-text" : "disabled-button-text");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.Write(Text);
            writer.WriteEndTag("span");
        }


        protected virtual string BuildPostBackScript()
        {
            var postBackOptions = new PostBackOptions(this, CLICK_EVENT_KEY);
            postBackOptions.ValidationGroup = ValidationGroup;
            postBackOptions.PerformValidation = CausesValidation;
            var result = Page.ClientScript.GetPostBackEventReference(postBackOptions);
            return result.Replace("\"", "&quot;");
        }


        /// <summary>
        /// Occurs on server when element is clicked.
        /// 
        /// Priority is given to NavigationUrl and OnClientClick than this.
        /// </summary>
        public event EventHandler Click;

        public virtual void RaisePostBackEvent(string eventArgument)
        {
            if (CLICK_EVENT_KEY.Equals(eventArgument) && null != Click)
            {
                if (CausesValidation)
                    Page.Validate(ValidationGroup);
                Click(this, EventArgs.Empty);
            }
        }
    }
}
