﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Admin.Default" Title="Admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        p {
        padding: 0 0 1em;
        }
        .msg_list {
        margin: 0px;
        padding: 0px;
        width: 383px;
        }
        .msg_head {
        padding: 5px 10px;
        cursor: pointer;
        position: relative;
        background-color:#FFCCCC;
        margin:1px;
        }
        .msg_body {
        padding: 5px 10px 15px;
        background-color:#F4F4F8;
        }    
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            //hide the all of the element with class msg_body

            var providersHidden = '#' + '<%= providersHidden.ClientID %>';
            var intermediariesHidden = '#' + '<%= intermediariesHidden.ClientID %>';

            if ($(providersHidden).val() == 'true') {
                $("#providersDiv").hide();
            }

            //toggle the componenet with class msg_body
            $("#showHideProvider").click(function() {
                $("#providersDiv").slideToggle(200);
                if ($(providersHidden).val() == 'false')
                    $(providersHidden).val('true');
                else
                    $(providersHidden).val('false');
            }
            );
            if ($(intermediariesHidden).val() == 'true') {
                $("#intermediaryDiv").hide();
            }

            //toggle the componenet with class msg_body
            $("#showHideIntermediaries").click(function() {
                $("#intermediaryDiv").slideToggle(200);
                if ($(intermediariesHidden).val() == 'true')
                    $(intermediariesHidden).val('false');
                else
                    $(intermediariesHidden).val('true');
            });
        });
    </script>
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgScholarship.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgScholarship.gif" Width="918px" Height="170px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<table>
    <tr>
        <td><label>Providers (# of active Orgs):</label></td>
        <td><asp:Label ID="providerCountLabel" runat="server"></asp:Label></td>
        <td>&nbsp;&nbsp;&nbsp;<a href="#" class="GreenText" id="showHideProvider">[show/hide]<asp:hiddenfield ID="providersHidden" runat="server" /></a></td>
    </tr>
    <tr>
        <td colspan="3">
            <div id="providersDiv">
                <asp:gridview ID="providerGrid" DataSourceID="providerDataSource" 
                    runat="server" AutoGenerateColumns="False" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:TemplateField HeaderText="Web Site" SortExpression="Website">
                            <ItemTemplate>
                                <a href='<%# ((string)Eval("Website")).Length > 3 ? ((string)Eval("Website")).Substring(0,4).ToLower() == "http" ? Eval("Website") : string.Concat("http://", (string)Eval("Website")) : Eval("Website") %>'><%# Eval("Website") %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tax ID" SortExpression="TaxId">
                            <ItemTemplate>
                                <asp:label runat="server" ID="label1" text='<%# string.Concat(((string)Eval("TaxId")).Substring(0, 2), "-", ((string)Eval("TaxId")).Substring(2)) %>'></asp:label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:gridview>    
            </div>
            <br />
        </td>
    </tr>
    <tr>
        <td>Intermediaries (# of active Orgs):</td>
        <td><asp:Label ID="intermediaryCountLabel" runat="server"></asp:Label></td>
        <td>&nbsp;&nbsp;&nbsp;<a href="#" class="GreenText" id="showHideIntermediaries">[show/hide]</a><asp:hiddenfield ID="intermediariesHidden" runat="server" /></a></td>
    </tr>
    <tr>
        <td colspan="3">
            <div id="intermediaryDiv">
                <asp:gridview ID="intermediaryGrid" runat="server" 
                    DataSourceID="intermediaryDataSource" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" 
                            SortExpression="Name" />
                        <asp:TemplateField HeaderText="Web Site" SortExpression="Website">
                            <ItemTemplate>
                                <a href='<%# ((string)Eval("Website")).Length > 3 ? ((string)Eval("Website")).Substring(0,4).ToLower() == "http" ? Eval("Website") : string.Concat("http://", (string)Eval("Website")) : Eval("Website") %>'><%# Eval("Website") %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tax ID" SortExpression="TaxId">
                            <ItemTemplate>
                                <asp:label runat="server" ID="label1" text='<%# string.Concat(((string)Eval("TaxId")).Substring(0, 2), "-", ((string)Eval("TaxId")).Substring(2)) %>'></asp:label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:gridview>    
            </div>
        </td>
    </tr>
</table>
<br /><br />

<fieldset>
<legend><b>Seekers</b></legend>
<table>
    <tr>
        <td>Count of Registered Seekers:</td>
        <td><asp:Label ID="registeredSeekersLabel" runat="server"></asp:Label></td>
        <td></td>
    </tr>
    <tr>
        <td>Count of Active Profiles:</td>
        <td><asp:Label ID="activeProfilesLabel" runat="server"></asp:Label></td>
        <td></td>
    </tr>
</table>

</fieldset>

<br /><br />
<fieldset>
<legend><b>Scholarship Applications (open scholarships only)</b></legend>
<table>
    <tr>
        <td>Count of Apps Started:</td>
        <td><asp:Label ID="appsStartedLabel" runat="server"></asp:Label></td>
        <td></td>
    </tr>
    <tr>
        <td>>Count of Apps Submitted:</td>
        <td><asp:Label ID="appsSubmittedLabel" runat="server"></asp:Label></td>
        <td></td>
    </tr>
</table>
</fieldset>

<br /><br />
<fieldset>
<legend><b>Google Analytics</b></legend>
<a href="https://www.google.com/analytics/">Google Analytics - Home</a><br /><br />
<a href="https://www.google.com/analytics/siteopt/splash?hl=en">Google Analytics - Web Site Optimizer</a>
</fieldset>

<asp:ObjectDataSource ID="providerDataSource" 
    runat="server" SelectMethod="SelectProviders"
    TypeName="ScholarBridge.Web.Admin.Default"
    OnObjectCreating="providerDataSource_ObjectCreating">
</asp:ObjectDataSource>


<asp:ObjectDataSource ID="intermediaryDataSource" 
    runat="server" 
    SelectMethod="SelectIntermediaries"
    TypeName="ScholarBridge.Web.Admin.Default"
    OnObjectCreating="intermediaryDataSource_ObjectCreating">
</asp:ObjectDataSource>


</fieldset></asp:Content>
