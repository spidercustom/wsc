﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
namespace ScholarBridge.Web.Admin
{
    public partial class Default : System.Web.UI.Page
	{
		#region Properties
		public IProviderService ProviderService
		{
			get;
			set;
		}
		public IIntermediaryService IntermediaryService
		{
			get;
			set;
		}

		private IList<Domain.Provider> ApprovedProviders
		{
			get
			{
				if (Session["ApprovedProviders"] == null)
				{
					Session["ApprovedProviders"] = ProviderService.FindActiveOrganizations();
				}
				return (IList<Domain.Provider>)Session["ApprovedProviders"];
			}
			set
			{
				Session["ApprovedProviders"] = value;
			}

		}

		private IList<Domain.Intermediary> ApprovedIntermediaries
		{
			get
			{
				if (Session["ApprovedIntermediaries"] == null)
				{
					Session["ApprovedIntermediaries"] = IntermediaryService.FindActiveOrganizations();
				}
				return (IList<Domain.Intermediary>)Session["ApprovedIntermediaries"];
			}
			set
			{
				Session["ApprovedIntermediaries"] = value;
			}

		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
        {
			if (!Page.IsPostBack)
			{
				InitializeObjects();
				LoadStatistics();
			}
		}

		#region Private Methods
		private void LoadStatistics()
    	{
    		providerCountLabel.Text = ApprovedProviders.Count.ToString();
    		intermediaryCountLabel.Text = ApprovedIntermediaries.Count.ToString();
			providersHidden.Value = "true";
    		intermediariesHidden.Value = "true";
    	}

    	private void InitializeObjects()
		{
			ApprovedProviders = null;
			ApprovedIntermediaries = null;
		}
		#endregion

		#region datasource methods

		public IList<Domain.Provider> SelectProviders()
		{
			return ApprovedProviders;
		}
		public IList<Domain.Intermediary> SelectIntermediaries()
		{
			return ApprovedIntermediaries;
		}
		#endregion

		#region event handlers
		protected void providerDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
    		e.ObjectInstance = this;
		}

    	protected void intermediaryDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
			e.ObjectInstance = this;
		}
		#endregion
	}
}
