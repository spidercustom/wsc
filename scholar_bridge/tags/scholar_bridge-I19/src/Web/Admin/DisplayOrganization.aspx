﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="DisplayOrganization.aspx.cs" Inherits="ScholarBridge.Web.Admin.DisplayOrganization" Title="Admin | Display Organization" %>

<%@ Register TagPrefix="sb" TagName="ShowOrg" Src="~/Admin/ShowOrg.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <h2><asp:label ID="orgName" runat="server"></asp:label></h2>
    <sb:ShowOrg id="showOrg" runat="server" />

</asp:Content>
