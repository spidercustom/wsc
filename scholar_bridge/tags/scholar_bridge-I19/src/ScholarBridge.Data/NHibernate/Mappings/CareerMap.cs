﻿using FluentNHibernate.Mapping;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate.Mappings
{
    public class CareerMap : ClassMap<Career>
    {
        public CareerMap()
        {
            Id(o => o.Id, "SBCareerIndex");
            Map(o => o.Name, "CareerName");
            Map(o => o.Description);
            Component(o => o.LastUpdate,
                c =>
                {
                    c.Map(o => o.By, "LastUpdateBy");
                    c.Map(o => o.On, "LastUpdateDate");
                });
        }
    }
}
