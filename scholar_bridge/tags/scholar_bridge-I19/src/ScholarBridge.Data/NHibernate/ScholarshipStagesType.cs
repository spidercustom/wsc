using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>ScholarshipStages</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class ScholarshipStagesType : EnumStringType
    {
        public ScholarshipStagesType()
            : base(typeof(ScholarshipStages), 50)
        {
        }
    }
}