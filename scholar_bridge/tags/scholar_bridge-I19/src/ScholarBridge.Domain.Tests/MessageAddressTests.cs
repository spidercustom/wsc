using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class MessageAddressTests
    {

        [Test]
        public void email_address_uses_users_email_first()
        {
            MessageAddress ma = SetupMessageAddress();
            Assert.That(ma.EmailAddress(), Is.EqualTo("user@example.com"));
        }

        [Test]
        public void email_address_uses_org_admin_email_if_no_user()
        {
            MessageAddress ma = SetupMessageAddress();
            ma.User = null;
            Assert.That(ma.EmailAddress(), Is.EqualTo("admin@example.com"));
        }

        [Test]
        public void to_string_uses_users_name_first()
        {
            MessageAddress ma = SetupMessageAddress();
            Assert.That(ma.ToString(), Is.EqualTo("Foo Bar"));
        }

        [Test]
        public void to_string_uses_role_name_if_no_user_or_org()
        {
            MessageAddress ma = SetupMessageAddress();
            ma.User = null;
            ma.Organization = null;
            Assert.That(ma.ToString(), Is.EqualTo("Role"));
        }

        [Test]
        public void to_string_uses_role_and_org_if_both()
        {
            MessageAddress ma = SetupMessageAddress();
            ma.User = null;
            Assert.That(ma.ToString(), Is.EqualTo("Provider : Role"));
        }

        [Test]
        public void to_string_uses_org_name_if_no_user_or_role()
        {
            MessageAddress ma = SetupMessageAddress();
            ma.User = null;
            ma.Role = null;
            Assert.That(ma.ToString(), Is.EqualTo("Provider"));
        }

        [Test]
        public void wscadmin_role_returns_HECBAdminEmail()
        {
            // note! admin email comes from app.config...
            MessageAddress ma = SetupMessageAddress();
            ma.User = null;
            ma.Organization = null;
            ma.Role = new Role { Name = Role.WSC_ADMIN_ROLE };
            Assert.That(ma.EmailAddress(), Is.EqualTo("test@hecb.wa.gov"));
        }

        [Test]
        public void other_role_returns_null_email()
        {
            MessageAddress ma = SetupMessageAddress();
            ma.User = null;
            ma.Organization = null;
            Assert.That(ma.EmailAddress(), Is.Null);
        }

        private static MessageAddress SetupMessageAddress()
        {
            var u = new User { Email = "user@example.com", Name = new PersonName { FirstName = "Foo", LastName = "Bar"}};
            var org = new Provider
                          {
                              Name = "Provider",
                              AdminUser = new User {Email = "admin@example.com"}
                          };
            var role = new Role {Name = "Role" };
            return new MessageAddress
                       {
                           User = u,
                           Organization = org,
                           Role = role
                       };
        }
    }
}