using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public interface IMessageService
    {
        Message FindMessage(User user, Organization organization, int id);
        SentMessage FindSentMessage(User user, Organization organization, int id);

        IList<Message> FindAll(User user, Organization organization);
        IList<Message> FindAll(MessageAction action, User user, Organization organization);
        IList<SentMessage> FindAllSent(User user, Organization organization);
        IList<Message> FindAllArchived(User user, Organization organization);
        int CountUnread(User user, Organization organization);

        void SendMessage(Message message);
        void ArchiveMessage(Message message);
        void MarkMessageRead(Message message);

        void DeletedRelatedMessages(Scholarship scholarship);
        void SaveNewSentMessage(Message message);
    }
}