﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Seeker.Scholarships
{
    public partial class Public : Page
    {
        
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        private int ScholarshipId
        {
            get
            {
                int scholarshipId;
                if (!Int32.TryParse(Request.Params["id"], out scholarshipId))
                    throw new ArgumentException("Cannot understand value of parameter scholarship");
                return scholarshipId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserContext.CurrentSeeker !=null) 
                Response.Redirect(ResolveUrl("~/Seeker/Scholarships/Show.aspx?print=true&id="+ScholarshipId  ));
            ScholarshipPublicView1.Scholarship = ScholarshipService.GetById(ScholarshipId);
        }
      
    }
}
