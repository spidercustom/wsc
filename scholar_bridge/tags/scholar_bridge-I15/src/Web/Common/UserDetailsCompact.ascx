﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetailsCompact.ascx.cs" Inherits="ScholarBridge.Web.Common.UserDetailsCompact" %>

<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>

<div id="memberInfo">
    <table>
        <tr>
            <td>
                <small>Name:</small> 
            </td>
            <td>
                <small><asp:Literal ID="nameLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Email:</small> 
            </td>
            <td>
                <small><asp:Literal ID="emailAddressLbl" runat="server" /></small>
            </td>
        </tr>
        
        <tr>
            <td>
                <small>Phone:</small> 
            </td>
            <td>
                <small><asp:Literal ID="phoneLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Fax:</small> 
            </td>
            <td>
                <small><asp:Literal ID="faxLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Other Phone:</small> 
            </td>
            <td>
                <small><asp:Literal ID="otherPhoneLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Member Since:&nbsp;&nbsp;</small> 
            &nbsp;</td>
            <td>
                <small><asp:Literal ID="memberSinceLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Last Login:</small> 
            </td>
            <td>
                <small><asp:Literal ID="lastLoginLbl" runat="server" /></small>
            </td>
        </tr>
    </table>
    </p>
</div>