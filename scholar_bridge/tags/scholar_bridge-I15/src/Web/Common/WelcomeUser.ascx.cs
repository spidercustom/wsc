﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class WelcomeUser : UserControl
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var currentUser = UserContext.CurrentUser;
            if (null != currentUser)
            {
                if (null == currentUser.Name)
                    LoginFirstLastName.Text = currentUser.Username;
                else
                    LoginFirstLastName.Text = UserContext.CurrentUser.Name.FirstName;

                //set organization name
                if (Page.User.IsInRole(Role.PROVIDER_ROLE) || Page.User.IsInRole(Role.INTERMEDIARY_ROLE))
                {
                    OrgName.Text = UserContext.CurrentOrganization.Name;
                }


            }
        }
    }
}

