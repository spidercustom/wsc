﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeUser.ascx.cs" Inherits="ScholarBridge.Web.Common.WelcomeUser" %>
<!--Welcome text starts here-->
     <DIV id="WelcomeTextContainer">
        <DIV class="WelcomeText">Welcome back
        <%--<asp:HyperLink ID="profileLnk" runat="server" NavigateUrl="~/Profile/Default.aspx">--%>
        <asp:Literal ID="LoginFirstLastName" runat="server" />!!                   
        <%--</asp:HyperLink>--%>
         </DIV>
    </DIV>
<!--Organization name starts here-->
 <DIV id="OrganizationNameContainer">
      <DIV class="OrganizationName"><asp:Literal ID="OrgName" runat="server" /></DIV>
 </DIV>
 <!--Organization name ends here-->
