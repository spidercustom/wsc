﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class OrgUserList : UserControl
    {

        public IList<User> Users { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            orgUsers.DataSource = Users;
            orgUsers.DataBind();
        }

        protected void orgUsers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var user=(User)e.Item.DataItem;
                var link = (HyperLink)e.Item.FindControl("linktoUser");
                link.NavigateUrl = LinkTo + "?id=" + user.Id;
                var status = (Label)e.Item.FindControl("lblStatus");
                status.Text = user.IsActive ? "Active" : "InActive";
                var activateButton = (ConfirmButton)e.Item.FindControl("ActivateConfirmBtn");
                activateButton.Text = user.IsActive ? "Deactivate" : "Activate";
                activateButton.ConfirmMessageDivID = user.IsActive ? "confirmDeactivateDiv" : "confirmActivateDiv";
               
            }
        }

        protected void ActivateConfirmBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (e.DialogResult)
            {
                 //activate the user
            }
        }
    }
}