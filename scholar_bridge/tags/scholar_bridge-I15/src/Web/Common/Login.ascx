﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ScholarBridge.Web.Common.Login" %>
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login ID="Login1" runat="server" UserNameLabelText="Email Address:"  PasswordRecoveryUrl="~/ForgotPassword.aspx"  
            LoginButtonImageUrl="~/images/Btn_OverAllHomepageLogin.gif"
            PasswordRecoveryText="Forgot Your Password?" 
            RememberMeText="Remember my username on this computer?" 
            TitleTextStyle-CssClass="loginTitleText" onloggedin="Login1_LoggedIn" 
            onloginerror="Login1_LoginError" LoginButtonType="Image" />
            
    <asp:HyperLink ID="reconfirmLnk" runat="server" NavigateUrl="~/Reconfirm.aspx">Resend Confirmation Email?</asp:HyperLink>            
    </AnonymousTemplate>    
</asp:LoginView>