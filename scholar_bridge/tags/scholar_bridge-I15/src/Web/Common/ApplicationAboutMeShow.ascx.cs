﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Seeker.BuildApplication;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationAboutMeShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {

                personalStatement.Text = LineBreakFormat(ApplicationToView.PersonalStatement);
                myChallenge.Text = LineBreakFormat(ApplicationToView.MyChallenge);
                myGift.Text = LineBreakFormat(ApplicationToView.MyGift);
                words.Text = LineBreakFormat(ApplicationToView.Words);
                skills.Text = LineBreakFormat(ApplicationToView.Skills);
                
                
            }
        }

        

        private static string LineBreakFormat(string value)
        {
            if (! String.IsNullOrEmpty(value))
            {
                value.Replace("\r\n\r\n", "<br/>\r\n");
            }

            return value;
        }

        public override int ResumeFrom
        {
            get { return WizardStepName.AboutMe.GetNumericValue(); }
        }
    }
}