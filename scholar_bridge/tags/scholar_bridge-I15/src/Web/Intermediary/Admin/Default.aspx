﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Admin.Default" %>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div >
   <h3>Intermediary Administration</h3>
</div>
<ul class="pageNav">
    <li><asp:HyperLink ID="usersLink" runat="server" NavigateUrl="~/Intermediary/Users/Default.aspx">Manage Users</asp:HyperLink></li>
    <li><asp:HyperLink ID="orgInfoLink" runat="server" NavigateUrl="~/Intermediary/Org/Edit.aspx">Edit My Organization Information</asp:HyperLink></li>
</ul>
    
</asp:Content>
