﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Relationships.Create" Title="Intermediary | Relationships | Create " %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<h3>Place keeper for WSC provided text explaining the ScholarBridge process for establishing a relationship including contacting the organization before submitting this request</h3>    
<label for="orgList" >Select Organization</label>
<asp:DropDownList ID="orgList" runat="server"></asp:DropDownList>
<asp:RequiredFieldValidator runat="server" ID="validateorganization" ControlToValidate="orgList" />
<asp:Button ID="saveBtn" runat="server" Text="Submit Request" 
        onclick="saveBtn_Click"  /> 
<asp:Button ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" 
        onclick="cancelBtn_Click" />
</asp:Content>
