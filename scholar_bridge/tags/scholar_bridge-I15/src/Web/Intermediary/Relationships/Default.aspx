﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Relationships.Default" Title="Intermediary | Relationships" %>

<%@ Register TagPrefix="sb" TagName="IntermediaryRelationshipList" Src="~/Common/IntermediaryRelationshipList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div >
   <h3>Intermediary Relationships</h3>
   <ul class="pageNav">
    <li><asp:HyperLink ID="addRequestLinkTop" runat="server" NavigateUrl="~/Intermediary/Relationships/Create.aspx">Add Request</asp:HyperLink></li>
    </ul>
    <sb:IntermediaryRelationshipList id="relationshiplist" runat="server" InactivateLinkTo="~/Intermediary/Relationships/Inactivate.aspx" />
</div>
   
</asp:Content>
