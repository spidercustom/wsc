﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Provider
{
    public partial class RegisterProvider : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            registerOrg.OrganizationType = typeof (Domain.Provider);
        }
    }
}