﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Provider.Admin
{
	public partial class Default : System.Web.UI.Page
	{
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            editOrg.Organization = UserContext.CurrentProvider;
            relationshiplist.Relationships = RelationshipService.GetByProvider(provider);
            orgUserList.Users = provider.ActiveUsers;
            adminDetails.UserToView = UserContext.CurrentOrganization.AdminUser;
            adminDetails1.UserToView = UserContext.CurrentOrganization.AdminUser;

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!User.IsInRole(Role.PROVIDER_ADMIN_ROLE))
            {
                createUserLnk.Visible = false;
            }
        }
        protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.Update((Domain.Provider)org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Provider/Admin");
        }

        protected void editOrg_OnFormCanceled()
        {
            Response.Redirect("~/Provider/Admin");
        }
	}
}
