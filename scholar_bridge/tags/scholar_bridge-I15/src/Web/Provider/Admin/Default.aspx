﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master"  AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Admin.Default" %>
<%@ Register TagPrefix="sb" TagName="ProviderRelationshipList" Src="~/Common/ProviderRelationshipList.ascx" %>
<%@ Register TagPrefix="sb" TagName="OrgUserList" Src="~/Common/OrgUserList.ascx" %>
<%@ Register TagPrefix="sb" TagName="UserDetailsCompact" Src="~/Common/UserDetailsCompact.ascx" %>
<%@ Register TagPrefix="sb" TagName="EditOrganization" Src="~/Common/EditOrganization.ascx" %>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div class="tabs">
            <ul>
                <li><a href="#organization-tab"><span>Organization</span></a></li>
                <li><a href="#user-tab"><span>Users</span></a></li>
                <li><a href="#relationship-tab"><span>Relationships</span></a></li>
            </ul>
            <div id="organization-tab">
               <h3>Organization Information</h3>
                 <sb:editorganization ID="editOrg" runat="server" 
                    OnOrganizationSaved="editOrg_OnOrganizationSaved" 
                    OnFormCanceled="editOrg_OnFormCanceled" />
                    <HR>
                    <h3>Organization Administrator</h3>
                    <sb:UserDetailsCompact ID="adminDetails1" runat="server" />
            </div>
                  
            <div id="user-tab">
                <h3>Organization Administrator</h3>
                <sb:UserDetailsCompact ID="adminDetails" runat="server" />
               
                <P>Only the Organization Administrator has access to update information and add users for your organization. </p>
                <HR>
                <h3>Organization Users</h3>
                <sb:OrgUserList id="orgUserList" runat="server" LinkTo="~/Provider/Users/Show.aspx" />
                <ul class="pageNav">
                    <li><asp:HyperLink ID="createUserLnk" runat="server" NavigateUrl="~/Provider/Users/Create.aspx">Create User</asp:HyperLink></li>
                </ul>
                    
            </div>
                  
            <div id="relationship-tab">
                <h3>Organization Relationships</h3>
                    <p>You may request a relationship with an Intermediary organization. The Intermediary organization must approve your request. Establishing a relationship with an Intermediary will let you designate them as a scholarship intermediary on specific scholarships and granting them administrative access to the   scholarship. You should contact the Organization directly to determine how they will assist in the administration of your scholarship.</p>
                    <ul class="pageNav">
                        <li><asp:HyperLink ID="addRequestLinkTop" runat="server" NavigateUrl="~/Provider/Relationships/Create.aspx">Add Request</asp:HyperLink></li>
                    </ul>
                 
                    <sb:ProviderRelationshipList ID="relationshiplist" runat="server" InactivateLinkTo="~/Provider/Relationships/Inactivate.aspx" />
            </div>
</div>     







   
</asp:Content>
