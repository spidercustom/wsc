﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Provider.Users.Show" Title="Provider | Users | Show" %>

<%@ Register TagPrefix="sb" TagName="UserDetails" Src="~/Common/UserDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:UserDetails ID="userDetails" runat="server" />
    <asp:LinkButton ID="editLinkButton" runat="server" onclick="editLinkButton_Click">Edit User</asp:LinkButton>
    <asp:Button ID="deleteBtn" runat="server" Text="Deactivate User" onclick="deleteBtn_Click" />
    <asp:Button ID="reactivateBtn" runat="server" Text="Reactivate User" onclick="reactivateBtn_Click" />
</asp:Content>
