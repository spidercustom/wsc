﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Message
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            if (User.IsInRole(Role.PROVIDER_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for notifications of scholarship status, scholarship applications and awards, and requests from other scholarship organizations.";

            else if (User.IsInRole(Role.INTERMEDIARY_ROLE))
                lblPageText.Text =
                    "Check your organizations Inbox for requests from other scholarship organizations, and notifications of scholarship status, applications and awards.";
            else
                lblPageText.Text =
                    "Check your Inbox for important updates on your Scholarship applications and awards.  You can also receive these updates via e-mail by updating the email preferences under My Profile, Basics.";
      
        }
    }
}