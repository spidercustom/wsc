﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Message.Default" Title="Message" %>

<%@ Register TagPrefix="sb" TagName="MessageList" Src="~/Common/MessageList.ascx" %>
<%@ Register TagPrefix="sb" TagName="SentMessageList" Src="~/Common/SentMessageList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
 <P class="PageTitle"><Img src="<%= ResolveUrl("~/images/PgTitle_MyMsgs.gif") %>" width="402px" height="36Px"></p>
 <P class="HighlightedTextMain"><asp:Label ID="lblPageText" runat="server" /></P>
        <div class="tabs">
            <ul>
                <li><a href="#work-tab"><span>Inbox</span></a></li>
                <asp:LoginView ID="loginView" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
                        <ContentTemplate>
                <li><a href="#denied-tab"><span>Denied</span></a></li>
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
                </asp:LoginView>
                <li><a href="#archived-tab"><span>Archived</span></a></li>
                <li><a href="#sent-tab"><span>Sent</span></a></li>
            </ul>
            <div id="work-tab">
               <h3>Inbox</h3>
                <sb:MessageList id="messageList" runat="server" LinkTo="~/Message/Show.aspx" />
            </div>
            <asp:LoginView ID="loginView1" runat="server">
                <RoleGroups>
                    <asp:RoleGroup Roles="Admin,WSCAdmin,Intermediary">
                        <ContentTemplate>            
            <div id="denied-tab">
                <h3>Denied</h3>
                <sb:MessageList id="MessageList2" runat="server" MessageAction="Deny" LinkTo="~/Message/Show.aspx" />
            </div>
                        </ContentTemplate>
                    </asp:RoleGroup>
                </RoleGroups>
            </asp:LoginView>
            <div id="archived-tab">
                <h3>Archived</h3>
                <sb:MessageList id="archivedMessageList" runat="server" Archived="true" LinkTo="~/Message/Show.aspx" />
            </div>
            <div id="sent-tab">
                <h3>Sent</h3>
                <sb:SentMessageList id="sentMessageList" runat="server" LinkTo="~/Message/Show.aspx" />
            </div>
        </div>        
</asp:Content>
