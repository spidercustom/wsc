﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Profile.Default" Title="Profile" %>

<%@ Register TagPrefix="sb" TagName="UserDetails" Src="~/Common/UserDetails.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div id="CenterContent">
        <div id="HomeContentLeft">
            
        </div>
        <div id="HomeContentRight">
            
        </div>
  <br />
<h3>User Settings</h3>
<br />
<br />
<sb:UserDetails ID="userDetails" runat="server" />

<br />

<ul class="pageNav">
    <li><asp:HyperLink ID="changePasswordLnk" runat="server" NavigateUrl="~/Profile/ChangePassword.aspx">Change Password</asp:HyperLink></li>
    <li><asp:HyperLink ID="changeNameLnk" runat="server" NavigateUrl="~/Profile/ChangeName.aspx">Change Name</asp:HyperLink></li>
    <li><asp:HyperLink ID="changeEmailLnk" runat="server" NavigateUrl="~/Profile/ChangeEmail.aspx">Change Email</asp:HyperLink></li>
</ul>          
</div>
</asp:Content>
