﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ChangeEmail.aspx.cs" Inherits="ScholarBridge.Web.Profile.ChangeEmail" Title="Profile | Change Email" %>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div id="CenterContent">
        <div id="HomeContentLeft">
            
        </div>
        <div id="HomeContentRight">
            
        </div>
  
<p class="noteBene">
Changing your Email Address will require you to confirm that email before logging in again.<br />
The new Email Address will be how you log into the system.
</p>

<label for="Email">Email Address:</label>
<asp:TextBox ID="Email" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="EmailValidator" runat="server" ControlToValidate="Email" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
<asp:CompareValidator ID="compareValidator" runat="server" ControlToValidate="Email" ControlToCompare="ConfirmEmail" ErrorMessage="Email and Confirm Email must match." />
<br />
<label for="ConfirmEmail">Confirm Email Address:</label>
<asp:TextBox ID="ConfirmEmail" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
<br />

<asp:Button ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click" />
<asp:Button ID="cancelButton" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelButton_Click" />
          
</div>
</asp:Content>
