﻿
using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class MatchStatusType : EnumStringType
    {
        public MatchStatusType()
            : base(typeof(MatchStatus), 30)
        {
        }
    }
}