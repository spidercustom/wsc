using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using Rhino.Mocks;
using ScholarBridge.Business.Actions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class MessageActionServiceTests
    {
        private MessageActionService mas;
        private NullAction na;
        private IAction activateProvider;

        private MockRepository mocks;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            mas = new MessageActionService();
            na = new NullAction();
            activateProvider = mocks.StrictMock<IAction>();

            mas.Actions.Add(MessageType.None, na);
            mas.Actions.Add(MessageType.RequestIntermediaryApproval, activateProvider);
            mas.Actions.Add(MessageType.RequestProviderApproval, activateProvider);
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(activateProvider);
        }

        [Test]
        public void get_action_for_none()
        {
            var action = mas.GetAction(MessageType.None);
            Assert.That(action, Is.Not.Null);
            Assert.That(action, Is.TypeOf(typeof (NullAction)));

            Assert.That(action.SupportsApprove, Is.False);
            Assert.That(action.SupportsReject, Is.False);

            action.Approve(null, null, null);   // No-op tests
            action.Reject(null, null, null);
        }

        [Test]
        public void get_action_for_approvals()
        {
            var action1 = mas.GetAction(MessageType.RequestIntermediaryApproval);
            Assert.That(action1, Is.Not.Null);

            var action2 = mas.GetAction(MessageType.RequestProviderApproval);
            Assert.That(action2, Is.Not.Null);
        }

        [Test]
        public void can_approve()
        {
            var p = new Provider {AdminUser = new User()};
            var tp = new MailTemplateParams();
            var m = new OrganizationMessage {RelatedOrg = p, MessageTemplate = MessageType.RequestProviderApproval};

            Expect.Call(() => activateProvider.Approve(m, tp, p.AdminUser));
            mocks.ReplayAll();

            mas.Approve(m, tp, p.AdminUser);
            mocks.VerifyAll();
        }

        [Test]
        public void can_reject()
        {
            var p = new Provider {AdminUser = new User()};
            var tp = new MailTemplateParams();
            var m = new OrganizationMessage {RelatedOrg = p, MessageTemplate = MessageType.RequestProviderApproval};

            Expect.Call(() => activateProvider.Reject(m, tp, p.AdminUser));
            mocks.ReplayAll();

            mas.Reject(m, tp, p.AdminUser);
            mocks.VerifyAll();
        }
    }
}