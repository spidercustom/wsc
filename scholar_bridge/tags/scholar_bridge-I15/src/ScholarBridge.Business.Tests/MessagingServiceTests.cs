﻿using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class MessagingServiceTests
    {
        private MockRepository mocks;
        private MessagingService messagingService;
        public IMailerService mailerService;
        public IMessageService messageService;
        public IRoleDAL roleService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            mailerService = mocks.StrictMock<IMailerService>();
            messageService = mocks.StrictMock<IMessageService>();
            roleService = mocks.StrictMock<IRoleDAL>();
            messagingService = new MessagingService
                                   {
                                       MailerService = mailerService,
                                       MessageService = messageService,
                                       RoleService = roleService,
                                       AdminEmail = "test@example.com",
                                       DefaultFromEmail = "test@example.com"
                                   };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(mailerService);
            mocks.BackToRecord(messageService);
            mocks.BackToRecord(roleService);
        }

        [Test]
        public void can_send_mail_to_admin()
        {
            var u = new User {Username = "NewUser", Email = "test@example.com"};
            var p = new Provider {Name = "TestProvider", AdminUser = u};
            var templateParams = new MailTemplateParams();
            var message = new OrganizationMessage
                              {
                                  MessageTemplate = MessageType.RequestProviderApproval,
                                  From = new MessageAddress {Organization = p}
                              };

            var tm = new TemplatedMail("Foo\nbar baz");

            Expect.Call(mailerService.MailForMessageType(MessageType.RequestProviderApproval)).Return(tm);
            Expect.Call(roleService.FindByName(Role.WSC_ADMIN_ROLE)).Return(new Role { Name = Role.WSC_ADMIN_ROLE });
            Expect.Call(() => messageService.SendMessage(message));
            Expect.Call(
                () =>
                mailerService.SendMail(tm, templateParams, messagingService.DefaultFromEmail,
                                       messagingService.AdminEmail));
            mocks.ReplayAll();

            messagingService.SendMessageToAdmin(message, templateParams, true);
            mocks.VerifyAll();

            Assert.IsNotNull(message.To);
        }

        [Test]
        public void can_send_mail_from_admin()
        {
            var u = new User {Username = "NewUser", Email = "test@example.com"};
            var p = new Provider {Name = "TestProvider", AdminUser = u};
            var templateParams = new MailTemplateParams();
            var message = new OrganizationMessage
                              {
                                  MessageTemplate = MessageType.RequestProviderApproval,
                                  To = new MessageAddress {Organization = p}
                              };

            var tm = new TemplatedMail("Foo\nbar baz");

            Expect.Call(mailerService.MailForMessageType(MessageType.RequestProviderApproval)).Return(tm);
            Expect.Call(roleService.FindByName(Role.WSC_ADMIN_ROLE)).Return(new Role { Name = Role.WSC_ADMIN_ROLE });
            Expect.Call(() => messageService.SendMessage(message));
            Expect.Call(
                () =>
                mailerService.SendMail(tm, templateParams, messagingService.DefaultFromEmail, "test@example.com"));
            mocks.ReplayAll();

            messagingService.SendMessageFromAdmin(message, templateParams, true);
            mocks.VerifyAll();

            Assert.IsNotNull(message.From);
        }
    }
}