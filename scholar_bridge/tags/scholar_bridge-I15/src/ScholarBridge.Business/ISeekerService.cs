using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface ISeekerService
    {
        void SaveNew(Seeker seeker);
        void Update(Seeker context);

    	Seeker FindByUser(User user);

        void Activate(Seeker seeker);
    }
}