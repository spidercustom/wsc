using System.Collections.Generic;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class MessageActionService : IMessageActionService
    {
        private IDictionary<MessageType, IAction> actions = new Dictionary<MessageType, IAction>();

        public IDictionary<MessageType, IAction> Actions
        {
            get { return actions; }
            set { actions = value; }
        }

        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
            GetAction(message.MessageTemplate).Approve(message, templateParams, approver);
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
            GetAction(message.MessageTemplate).Reject(message, templateParams, approver);
        }

        public IAction GetAction(MessageType messageAction)
        {
            if (!Actions.ContainsKey(messageAction))
                return NullAction.Instance;

            return Actions[messageAction];
        }
    }
}