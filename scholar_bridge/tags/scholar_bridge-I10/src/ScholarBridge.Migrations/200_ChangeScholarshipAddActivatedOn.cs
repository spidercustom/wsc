﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(200)]
    public class ChangeScholarshipAddActivatedOn : Migration
    {
        private const string TABLE_NAME = "SBScholarship";
        private static readonly Column ActivatedOnColumn = new Column(
            "ActivatedOn",
            DbType.DateTime,
            ColumnProperty.Null);

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, ActivatedOnColumn);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, ActivatedOnColumn.Name);
        }
    }
}