using System;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(166)]
    public class UpdateSupportValues2 : Migration
    {
        private const string TABLE_NAME = "SBSupportLUT";
        private static readonly string[] INSERT_COLUMNS = { "SBSupport", "Description", "SupportType", "LastUpdateBy" };

        public override void Up()
        {
            Database.Update(TABLE_NAME, new[] { "SBSupport" }, new[] { "One-time Emergency Support" }, "SBSupport='Emergency funding'");
            Database.Update(TABLE_NAME, new[] { "SBSupport" }, new[] { "Support for Child Care" }, "SBSupport='Child-care support'");

            var adminId = HECBStandardDataHelper.GetAdminUserId(Database).ToString();
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "Support to Study Abroad", "", "Traditional", adminId });

            DeleteData("Scholarships that would support study abroad");
            DeleteData("Support for expenses only");
        }

        private void DeleteData(string sbSupportVal)
        {
            // Remove the related data
            var id = Database.SelectScalar("SBSupportIndex", TABLE_NAME, String.Format("SBSupport='{0}'", sbSupportVal));
            if (null != id)
            {
                Database.Delete("SBScholarshipSupportRT", "SBSupportIndex", id.ToString());
                Database.Delete("SBSeekerSupportRT", "SBSupportIndex", id.ToString());
            }

            Database.Delete(TABLE_NAME, "SBSupport", sbSupportVal);
        }
        
        public override void Down()
        {
            var adminId = HECBStandardDataHelper.GetAdminUserId(Database).ToString();
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "Support for expenses only", "", "Traditional", adminId });
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "Scholarships that would support study abroad", "", "Traditional", adminId });
         
            DeleteData("Support to Study Abroad");
        }
    }
}