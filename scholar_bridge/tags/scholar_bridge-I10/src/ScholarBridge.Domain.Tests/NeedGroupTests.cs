using NUnit.Framework;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class NeedGroupTests
    {

        [Test]
        public void MinPercentFormattedHandlesZero()
        {
            var n = new NeedGap {MinPercent = 0};
            Assert.AreEqual("0%", n.MinPercentFormatted);
        }

        [Test]
        public void MaxPercentFormattedHandlesZero()
        {
            var n = new NeedGap { MaxPercent = 0 };
            Assert.AreEqual("0%", n.MaxPercentFormatted);
        }

        [Test]
        public void MinPercentFormattedHandlesNonZero()
        {
            var n = new NeedGap { MinPercent = 0.4f };
            Assert.AreEqual("40%", n.MinPercentFormatted);
        }

        [Test]
        public void MaxPercentFormattedHandlesNonZero()
        {
            var n = new NeedGap { MaxPercent = 0.5f };
            Assert.AreEqual("50%", n.MaxPercentFormatted);
        }

        [Test]
        public void MinPercentFormattedHandlesOne()
        {
            var n = new NeedGap { MinPercent = 1f };
            Assert.AreEqual("100%", n.MinPercentFormatted);
        }

        [Test]
        public void MaxPercentFormattedHandlesOne()
        {
            var n = new NeedGap { MaxPercent = 1f };
            Assert.AreEqual("100%", n.MaxPercentFormatted);
        }
    }
}