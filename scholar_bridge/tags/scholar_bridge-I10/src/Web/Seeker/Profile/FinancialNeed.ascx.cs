﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class FinancialNeed : WizardStepUserControlBase<Domain.Seeker>
    {
        Domain.Seeker _seekerInContext;
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        public IGenericLookupDAL<NeedGap> NeedGapDAL { get; set; }
        Domain.Seeker SeekerInContext
        {
            get
            {
                if (_seekerInContext == null)
                    _seekerInContext = Container.GetDomainObject();
                return _seekerInContext;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context");

            if (!Page.IsPostBack)
            {

                NeedGaps.DataBind();
                TypesOfSupport.DataBind();

                PopulateScreen();
            }
        }
        private void PopulateScreen()
        {
            PopulateCheckBoxes(NeedGaps, NeedGapDAL.FindAll());
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());
            if (null != SeekerInContext.Need)
            {
                Fafsa.Checked = SeekerInContext.Need.Fafsa;
                UserDerived.Checked = SeekerInContext.Need.UserDerived;
                MinimumSeekerNeed.Amount = SeekerInContext.Need.MinimumSeekerNeed;
                MaximumSeekerNeed.Amount = SeekerInContext.Need.MaximumSeekerNeed;
                NeedGaps.Items.SelectItems(SeekerInContext.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != SeekerInContext.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(SeekerInContext.SupportedSituation.TypesOfSupport, ts => ts.Id.ToString());
            }
        }

        public override void PopulateObjects()
        {
            // Need
            if (null == SeekerInContext.Need)
                SeekerInContext.Need = new DefinitionOfNeed();
            SeekerInContext.Need.Fafsa = Fafsa.Checked;
            SeekerInContext.Need.UserDerived = UserDerived.Checked;
            SeekerInContext.Need.MinimumSeekerNeed = MinimumSeekerNeed.Amount;
            SeekerInContext.Need.MaximumSeekerNeed = MaximumSeekerNeed.Amount;

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => (object)item);
            SeekerInContext.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            SeekerInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (SeekerInContext.Stage < SeekerStages.FinancialNeed)
                SeekerInContext.Stage = SeekerStages.FinancialNeed;
        }


        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
       
        #region WizardStepUserControlBase methods

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            SeekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            SeekerService.Update(SeekerInContext);
        }

        public override bool WasSuspendedFrom(Domain.Seeker @object)
        {
            return @object.Stage == SeekerStages.FinancialNeed;
        }

        public override bool CanResume(Domain.Seeker @object)
        {
            return @object.Stage >= SeekerStages.FinancialNeed;
        }
        #endregion
	}
}