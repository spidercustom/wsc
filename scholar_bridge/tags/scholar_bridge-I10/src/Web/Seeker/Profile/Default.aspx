﻿<%@ Page Language="C#" MasterPageFile="~/Seeker/Seeker.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Profile.Default"  Title="MyProfile" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="~/Seeker/Profile/Basics.ascx" tagname="Basics" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AboutMe.ascx" tagname="AboutMe" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/AcademicInformation.ascx" tagname="AcademicInfo" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/Activities.ascx" tagname="Activities" tagprefix="sb" %>
<%@ Register src="~/Seeker/Profile/FinancialNeed.ascx" tagname="FinancialNeed" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div id="confirmSaving" title="Confirm saving" style="display:none">
        You have change the data. Would you like to save the changes?
    </div>
    <div ID="confirmSavingActivatedProfile" title="Confirm saving" style="display:none">
        <asp:Literal ID="ConfirmSaveIfActivatedLabel" runat="server" />
    </div>
    <br />
    <asp:button ID="ActivateButton" ToolTip="Save & activate the profile" OnClick="ActivateButton_Click" runat="server" Text="Activate Profile"/>
    <br />

    <div class="tabs" id="BuildSeekerProfileWizardTab">
        <ul class="linkarea">
            <li><a  href="#tab"><span>Basics</span></a></li>
            <li><a  href="#tab"><span>About Me</span></a></li>
            <li><a href="#tab"><span>My Academic Info</span></a></li>
            <li><a href="#tab"><span>My Activities & Interests</span></a></li>
            <li><a href="#tab"><span>My Financial Need</span></a></li>
        </ul>
         
        <div id="tab">
            <asp:MultiView  ID="BuildSeekerProfileWizard" 
                            runat="server"
                            OnActiveViewChanged="BuildSeekerProfileWizard_ActiveStepChanged">
              <asp:View ID="BasicInfoStep" runat="server">
                    <sb:Basics ID="Basics1" runat="server" />
              </asp:View>
              <asp:View ID="AboutMeStep" runat="server">
                    <sb:AboutMe ID="AboutMe1" runat="server" />
              </asp:View>
              <asp:View ID="AcademicStep" runat="server">
                    <sb:AcademicInfo ID="AcademicInfo1" runat="server" />
              </asp:View>
              <asp:View ID="ActivitiesStep" runat="server">
                    <sb:Activities ID="Activities1" runat="server"  />
              </asp:View>
              <asp:View ID="FinancialNeed" runat="server">
                    <sb:FinancialNeed ID="FinancialNeed1" runat="server" />
              </asp:View>
            </asp:MultiView>
        </div>
        <sbCommon:jQueryTabIntegrator id="buildSeekerProfileWizardTabIntegrator" 
                                runat="server" 
                                CausesValidation="true"
                                MultiViewControlID="BuildSeekerProfileWizard"
                                TabControlClientID="BuildSeekerProfileWizardTab" />

    </div>
    
    <div id="ActivationValidationErrors" class="issueList" title="Profile activation" style="display:none">
        <asp:Panel ID="BasicTabPageErrorPanel" runat="server">
            <h3>Basic</h3>
            <ul>
                <li id="FirstNameValidationError" runat="server">First Name is Required</li>
                <li id="LastNameValidationError" runat="server">Last Name is Required</li>
                <li id="StreetLine1ValidationError" runat="server">Street must be specified and must be less than 50 characters</li>
                <li id="CityValidationError" runat="server">City must be specified</li>
                <li id="StateValidationError" runat="server">State must be specified</li>
                <li id="PostalCodeValidationError" runat="server">Postal Code must be at-least 5 characters and must be less than 10 characters</li>
            </ul>
        </asp:Panel>
        <asp:Panel ID="MyAcademicInfoTabPageErrorPanel" runat="server">
            <h3>My Academic Info</h3>
            <ul>
                <li id="StudentTypeValidationError" runat="server">What type of student are you currently?</li>
                <li id="SchoolTypeValidationError" runat="server">What type of school are you considering?</li>
                <li id="AcademicProgramValidationError" runat="server">Academic Program is required</li>
            </ul>
        </asp:Panel>
        <asp:Panel ID="MyFinancialNeedTabPageErrorPanel" runat="server">
            <h3>My Financial Need</h3>
            <ul>
                <li id="SupportTypeValidationError" runat="server">Specify at-least one support type</li>
            </ul>
        </asp:Panel>
        <br />
        <p>
            All issues specified in above list must to be fixed for profile that is activating or is active.
        </p>
    </div>
    <div class="controlPanel">
        <fieldset>
            <sbCommon:SaveConfirmButton ID="PreviousButton" ToolTip="Save & navigate to the prior tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" runat="server" OnClick="PreviousButton_Click" Text="Previous"/>
            <sbCommon:SaveConfirmButton ID="NextButton" ToolTip="Save & navigate to the next tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" OnClick="NextButton_Click" runat="server" Text="Next" />
            <sbCommon:SaveConfirmButton ID="SaveButton" ToolTip="Save all work on this tab" NeedsConfirmation="false" ConfirmMessageDivID="confirmSaving" runat="server" OnClick="SaveButton_Click" Text="Save"/>
            <sbCommon:SaveConfirmButton ToolTip="Exit the profile update and build." ID="ExitButton" OnClick="ExitButton_Click" runat="server" ConfirmMessageDivID="confirmSaving" Text="Exit" />
        </fieldset>
    </div>
</asp:Content>
