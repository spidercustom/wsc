﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Matches.Show" Title="Seeker | My Matches | Show" %>
<%@ Register src="~/Common/ShowScholarship.ascx" tagname="ShowScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<sb:ScholarshipTitleStripe id="scholarshipTitleStripeControl" runat="server" />
<sb:ShowScholarship id="showScholarship" runat="server" />
</asp:Content>
