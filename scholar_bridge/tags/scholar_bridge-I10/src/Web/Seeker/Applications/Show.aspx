﻿<%@ Page Language="C#" MasterPageFile="~/Seeker/Seeker.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Applications.Show" Title="Application | Show" %>
<%@ Register src="~/Common/ShowApplication.ascx" tagname="ShowApplication" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" />

<sb:ShowApplication id="showApplication" runat="server" 
    LinkTo="~/Seeker/BuildApplication/Default.aspx" 
    ScholarshipLinkTo="~/Seeker/Scholarships/Show.aspx"/>
</asp:Content>