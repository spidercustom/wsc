﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Public.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Scholarships.Public" Title="Scholarships | Show" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div>
<div class="viewLabel">Scholarship Name:</div> <div class="viewValue"><asp:Literal ID="scholarshipName" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Provider Name:</div> <div class="viewValue"><asp:Literal ID="providerName" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Intermediary Name:</div> <div class="viewValue"><asp:Literal ID="intermediaryName" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Donor Name:</div> <div class="viewValue"><asp:Literal ID="donorName" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Mission Statement:</div> <div class="viewValue"><asp:Literal ID="missionStatement" runat="server" /></div>
<br />
</div>
<br />
<div>
<div >Value Proposition to be defined by WSC:</div> 
<br />
</div>
<div>
<div >Sign-up to apply to this and other scholarships at <a href="<%= ResolveUrl("~/Seeker/Register.aspx") %>">www.TheWashBoard.org</a>  </div>
<br />
</div>
<div >If you are already a registered user? <a href="<%= ResolveUrl("~/Seeker/Default.aspx") %>">Click to log in</a>  </div>
<br />
</div>

</div>
</asp:Content>
