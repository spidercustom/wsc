﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain.Lookup;
namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class Activities : WizardStepUserControlBase<Domain.Application>
    {
        Domain.Application _applicationInContext;
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Application applicationInContext
        {
            get
            {
                if (_applicationInContext == null)
                    _applicationInContext = Container.GetDomainObject();
                return _applicationInContext;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (applicationInContext == null)
                throw new InvalidOperationException("There is no application in context");

            if (!Page.IsPostBack)
            {
                
                WorkHoursControl.DataBind();
                ServiceHoursControl.DataBind();
                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {

            AcademicAreasControlDialogButton.Keys = applicationInContext.AcademicAreas.CommaSeparatedIds();
            AcademicAreasOtherControl.Text = applicationInContext.AcademicAreaOther;
            
            CareersControlDialogButton.Keys = applicationInContext.Careers.CommaSeparatedIds();
            CareersOtherControl.Text = applicationInContext.CareerOther;
            
            CommunityServiceControlDialogButton.Keys = applicationInContext.CommunityServices.CommaSeparatedIds();
            CommunityServiceOtherControl.Text = applicationInContext.CommunityServiceOther;

            OrganizationsControlDialogButton.Keys = applicationInContext.MatchOrganizations.CommaSeparatedIds();
            OrganizationsOtherControl.Text = applicationInContext.MatchOrganizationOther;

            AffiliationTypesControlDialogButton.Keys = applicationInContext.AffiliationTypes.CommaSeparatedIds();
            AffiliationTypesOtherControl.Text = applicationInContext.AffiliationTypeOther;

            SeekerHobbiesControlDialogButton.Keys = applicationInContext.Hobbies.CommaSeparatedIds();
            SeekerHobbiesOtherControl.Text = applicationInContext.HobbyOther;

            SportsControlDialogButton.Keys = applicationInContext.Sports.CommaSeparatedIds();
            SportsOtherControl.Text = applicationInContext.SportOther;

            ClubsControlDialogButton.Keys = applicationInContext.Clubs.CommaSeparatedIds();
            ClubsOtherControl.Text = applicationInContext.ClubOther;

            WorkTypeControlDialogButton.Keys = applicationInContext.WorkTypes.CommaSeparatedIds();
            
            WorkHoursControl.SelectedValues = applicationInContext.WorkHours.Cast<ILookup>().ToList();

            ServiceTypeControlDialogButton.Keys = applicationInContext.ServiceTypes.CommaSeparatedIds();

            ServiceHoursControl.SelectedValues = applicationInContext.ServiceHours.Cast<ILookup>().ToList();
        }

        public override void PopulateObjects()
        {
            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, applicationInContext.AcademicAreas);
            applicationInContext.AcademicAreaOther = AcademicAreasOtherControl.Text;

            PopulateList(CareersContainerControl, CareersControlDialogButton, applicationInContext.Careers);
            applicationInContext.CareerOther = CareersOtherControl.Text;

            PopulateList(CommunityServiceContainerControl, CommunityServiceControlDialogButton, applicationInContext.CommunityServices);
            applicationInContext.CommunityServiceOther = CommunityServiceOtherControl.Text;

            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, applicationInContext.MatchOrganizations);
            applicationInContext.MatchOrganizationOther = OrganizationsOtherControl.Text;

            PopulateList(AffiliationTypesContainerControl, AffiliationTypesControlDialogButton, applicationInContext.AffiliationTypes);
            applicationInContext.AffiliationTypeOther = AffiliationTypesOtherControl.Text;

            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, applicationInContext.Hobbies);
            applicationInContext.HobbyOther = SeekerHobbiesOtherControl.Text;

            PopulateList(SportsContainerControl, SportsControlDialogButton, applicationInContext.Sports);
            applicationInContext.SportOther = SportsOtherControl.Text;

            PopulateList(ClubsContainerControl, ClubsControlDialogButton, applicationInContext.Clubs);
            applicationInContext.ClubOther = ClubsOtherControl.Text;

            PopulateList(WorkTypeContainerControl, WorkTypeControlDialogButton, applicationInContext.WorkTypes);
            
            PopulateList(WorkHoursContainerControl, WorkHoursControl, applicationInContext.WorkHours);
            
            PopulateList(ServiceTypeContainerControl, ServiceTypeControlDialogButton, applicationInContext.ServiceTypes);
            PopulateList(ServiceHoursContainerControl, ServiceHoursControl, applicationInContext.ServiceHours);
            
            applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (applicationInContext.Stage < ApplicationStages.Activities)
                applicationInContext.Stage = ApplicationStages.Activities;
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        private static bool Validate(PlaceHolder applicabilityControl, LookupDialog dialogButton)
        {
            return true;
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
            PopulateObjects();
            ApplicationService.Update(applicationInContext);
		}

		public override bool WasSuspendedFrom(Domain.Application @object)
		{
		    return @object.Stage == ApplicationStages.Activities;
		}

		public override bool CanResume(Domain.Application @object)
		{
            return @object.Stage >= ApplicationStages.Activities;
		}
		#endregion
	}
}