﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ApproveIntermediary.aspx.cs" Inherits="ScholarBridge.Web.Admin.ApproveIntermediary" Title="Admin | Approve Intermediary" %>

<%@ Register TagPrefix="sb" TagName="ShowOrg" Src="~/Admin/ShowOrg.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Pending Intermediary</h3>

    <sb:ShowOrg id="showOrg" runat="server" />

    <br />

    <asp:Button ID="approveButton" visible="false" runat="server" Text="Approve" onclick="approveButton_Click" />
    <asp:Button ID="rejectButton" visible="false" runat="server" Text="Deny" onclick="rejectButton_Click" />
</asp:Content>
