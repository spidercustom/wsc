﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin
{
    public partial class ApproveIntermediary : Page
    {
        public IIntermediaryService IntermediaryService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Intermediary currentIntermediary;

        public int CurrentProviderId
        {
            get { return Int32.Parse(Request["id"]); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            currentIntermediary = IntermediaryService.FindById(CurrentProviderId);
            showOrg.Organization = currentIntermediary;
        }

        protected void approveButton_Click(object sender, EventArgs e)
        {
            currentIntermediary.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            IntermediaryService.Approve(currentIntermediary);
            ReturnToList();
        }

        protected void rejectButton_Click(object sender, EventArgs e)
        {
            currentIntermediary.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            IntermediaryService.Reject(currentIntermediary);
            ReturnToList();
        }

        private void ReturnToList()
        {
            Response.Redirect("~/Admin/PendingIntermediaries.aspx");
        }
    }
}
