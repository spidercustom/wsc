﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArticleEntry.ascx.cs" Inherits="ScholarBridge.Web.Admin.PressRoom.ArticleEntry" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>             
<asp:Label ID="errorMessage" runat="server" Text="Article not found." Visible="false" CssClass="errorMessage"/>
<label for="PostedDateControl">Date (M/D/YYYY)</label>
<br />
<sb:CalendarControl ID="PostedDateControl" runat="server" ></sb:CalendarControl>
<elv:PropertyProxyValidator ID="PostedDateControlValidator" runat="server" ControlToValidate="PostedDateControl" 
PropertyName="PostedDate" SourceTypeName="ScholarBridge.Domain.Article" ErrorMessage="should be correct date format." />
<br />
<br />
<label for="TitleControl">Title:</label>
<br />
<asp:TextBox ID="TitleControl" runat="server" TextMode="MultiLine" Rows="3" Width="600px" Height="50px"></asp:TextBox>
<asp:RequiredFieldValidator ID="TitleControlRequiredValidator" runat="server" ControlToValidate="TitleControl"
 ErrorMessage="should not be empty."></asp:RequiredFieldValidator>
<elv:PropertyProxyValidator ID="TitleControlValidator" runat="server" ControlToValidate="TitleControl" 
PropertyName="Title" SourceTypeName="ScholarBridge.Domain.Article" ErrorMessage="should not exceed 250 characters." />
<br />
<br />
<label for="BodyControl">Press Release Text :</label>
<asp:Label ID="Label1" runat="server" Width="600px">(text beginning with http:// and https:// will be made into a hyperlink up to the first space)</asp:Label> 

<br />
<asp:TextBox ID="BodyControl" runat="server" TextMode="MultiLine" Rows="10" Width="600px" Height="300px"></asp:TextBox>
<asp:RequiredFieldValidator ID="BodyControlRequiredValidator" runat="server" ControlToValidate="BodyControl"
 ErrorMessage="should not be empty."></asp:RequiredFieldValidator>
<elv:PropertyProxyValidator ID="BodyControlValidator" runat="server" ControlToValidate="BodyControl" 
PropertyName="Body" SourceTypeName="ScholarBridge.Domain.Article" ErrorMessage="should not exceed 4000 characters."  />
<br />

<asp:Button ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click" />
<asp:Button ID="cancelButton" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelButton_Click" />