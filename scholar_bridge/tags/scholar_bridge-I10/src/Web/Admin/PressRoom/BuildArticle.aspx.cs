﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin.PressRoom
{
    public partial class BuildArticle : System.Web.UI.Page
    {
        public IArticleService ArticleService { get; set; }
        public IUserContext UserContext { get; set; }
        private const string SUCCESS_MESSAGE = "Article saved.";
        private const string DEFAULT_PAGE = "~/Admin/PressRoom/Default.aspx";
        private int ArticleId
        {
            get
            {
                int articleId;
                if (!Int32.TryParse(Request.Params["id"], out articleId))
                    throw new ArgumentException("Cannot understand value of parameter id");
                return articleId;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            if (Request.QueryString["id"] == null)
            {
                ArticleEntry1.ArticleInContext = new Article() {PostedDate = DateTime.Today};
            }
            else
            {
                var article = ArticleService.GetById(ArticleId);
                if (article==null)
                    throw new ArgumentNullException("article");
                ArticleEntry1.ArticleInContext = article;


            }
        }

        public void ArticleEntry1_OnFormSaved(Article article)
        {
            SuccessMessageLabel.SetMessage(SUCCESS_MESSAGE);
            Response.Redirect(ResolveUrl(DEFAULT_PAGE));
        }
        public void ArticleEntry1_OnFormanceled()
        {
            Response.Redirect(ResolveUrl(DEFAULT_PAGE));

        }
    }
}
