﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Activate : WizardStepUserControlBase<Scholarship>
    {
        private const string SCHOLARSHIP_VALIDATION_ERRORS = "ScholarshipValidationErrors";
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (! Page.IsPostBack)
                PopulateScreen();
        }

        private void PopulateScreen()
        {
            if (null != ScholarshipInContext.AdditionalQuestions &&
                ScholarshipInContext.AdditionalQuestions.Count > 0)
            {
            }
        }

        public override void PopulateObjects()
        {
            
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.RequestedActivation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.RequestedActivation;
        }

        public override bool ValidateStep()
        {
            var result = Validation.Validate(ScholarshipInContext);
            return result.IsValid;
        }

        protected void confirmRequestActivationBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (e.DialogResult)
            {
                if (!ValidateSchoalarship())
                    return;

                if (ScholarshipInContext.CanSubmitForActivation())
                {
                    ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ScholarshipService.RequestActivation(ScholarshipInContext);
                    SuccessMessageLabel.SetMessage("Scholarship submitted for activation.");
                    Response.Redirect("~/Provider/Scholarships/Default.aspx#pending-activated-tab");
                }
                else
                {
                    ClientSideDialogs.ShowAlert("<p>Scholarship is either already in activation process or is already activated/denied</p>", "Cannot Send Activation Request!");
                }
            }
        }

        private bool ValidateSchoalarship()
        {
            //TODO: This can be better written if we can move all validation to Domain,
            // right now main, issue is how to better to move collection empty validation conditionally
            // that depends on options selected in "Selection Criteria" tab.
            var isValid = true;
            var stepIndex = 0;
            var failedValidations = new List<IValidator>();
            Container.Steps.ForEach(o => 
                {
                    Container.ActiveStepIndex = stepIndex;
                    Page.Validate();
                    o.ValidateStep();
                    isValid &= Page.IsValid;
                    for (var validationIndex = 0; validationIndex < Page.Validators.Count; validationIndex++)
                    {
                        var validation = Page.Validators[validationIndex];
                        if (!validation.IsValid)
                            failedValidations.Add(validation);
                    }
                    stepIndex++;
                });

            IssueListControl.DataSource = failedValidations;
            IssueListControl.DataBind();
            ClientSideDialogs.ShowDivAsDialog(SCHOLARSHIP_VALIDATION_ERRORS);
            return isValid;
        }
    }
}
