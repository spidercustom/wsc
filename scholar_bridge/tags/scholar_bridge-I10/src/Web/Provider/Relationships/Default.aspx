﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
    Inherits="ScholarBridge.Web.Provider.Relationships.Default" Title="Provider | Relationships" %>

<%@ Register TagPrefix="sb" TagName="ProviderRelationshipList" Src="~/Common/ProviderRelationshipList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div>
        <h3>Provider Relationships</h3>
        <ul class="pageNav">
            <li><asp:HyperLink ID="addRequestLinkTop" runat="server" NavigateUrl="~/Provider/Relationships/Create.aspx">Add Request</asp:HyperLink></li>
        </ul>
        <sb:ProviderRelationshipList ID="providerRelationshiplist" runat="server" InactivateLinkTo="~/Provider/Relationships/Inactivate.aspx" />
    </div>

</asp:Content>
