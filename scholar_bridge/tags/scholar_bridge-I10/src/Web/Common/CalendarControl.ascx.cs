﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    [ValidationProperty("SelectedDate")]
    public partial class CalendarControl : UserControl
    {
        
        public DateTime? SelectedDate
        {
            get
            {
                DateTime result;
                return  DateTime.TryParse(datepicker.Text,out result) ? result:  new DateTime? ();
            }
            set {
                SetText(value);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetText(SelectedDate);
        }

        protected override void OnPreRender(EventArgs e)
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (page != null)
            {
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), datepicker.ClientID + "_script", GetJS());
            }
            base.OnPreRender(e);
        }

        private void SetText(DateTime? value)
        {
            datepicker.Text = value.HasValue ? value.Value.ToString("d") : "";
        }

        private string GetJS()
        {
            return "$(function() { $('#" + datepicker.ClientID + "').attr('ReadOnly', 'true'); $('#" + datepicker.ClientID + "').datepicker({ showButtonPanel: true, showOn: 'both', buttonImage: '/images/calendar.gif', buttonImageOnly: true }); });";
        }
    }
}