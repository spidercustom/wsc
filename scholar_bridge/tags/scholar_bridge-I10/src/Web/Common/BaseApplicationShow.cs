using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public abstract class BaseApplicationShow : UserControl
    {
        public Application ApplicationToView { get; set; }
        public string LinkTo { get; set; }

        public abstract ApplicationStages Stage { get; }

        protected virtual void SetupEditLinks(HtmlGenericControl linkarea1, HtmlGenericControl linkarea2)
        {
//            linkarea1.Visible = CanEdit();
//            linkarea2.Visible = CanEdit();

            string navurl = string.Format("{0}?aid={1}&resumefrom={2}", LinkTo, ApplicationToView.Id, (int)Stage - 1);
//            ((HyperLink) linkarea1.FindControl("linkTop")).NavigateUrl = navurl;
//            ((HyperLink) linkarea2.FindControl("linkBottom")).NavigateUrl = navurl;
        }

        protected bool CanEdit()
        {
            return !String.IsNullOrEmpty(LinkTo) && ApplicationToView.CanEdit() &&
                Roles.IsUserInRole(Role.SEEKER_ROLE);
        }
    }
}