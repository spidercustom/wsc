﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class OrgUserList : UserControl
    {

        public IList<User> Users { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            orgUsers.DataSource = Users;
            orgUsers.DataBind();
        }

        protected void orgUsers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var link = (HyperLink)e.Item.FindControl("linktoUser");
                link.NavigateUrl = LinkTo + "?id=" + ((User)e.Item.DataItem).Id;
            }
        }
    }
}