﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class AllScholarshipApplicants : UserControl
    {
        public IList<Application> Applications { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            lstApplicants.DataSource = Applications.OrderBy(a => a.ApplicantName.LastName);
            lstApplicants.DataBind();
        }
    }
}