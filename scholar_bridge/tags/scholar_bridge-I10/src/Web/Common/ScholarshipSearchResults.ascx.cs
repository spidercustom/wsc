﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipSearchResults : UserControl
    {
        private IList<Scholarship> _Scholarships;
        public string LinkTo { get; set; }
        public IUserContext UserContext { get; set; }
        public IMatchService MatchService { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        private const string SCHOLARSHIP_VIEW_TEMPLATE = "/Seeker/Scholarships/Show.aspx";
        public IList<Scholarship> Scholarships
        {
            get { return _Scholarships; }
            set { _Scholarships = value;
                Bind();
            }
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            lbladdto.Visible = (!(UserContext.CurrentSeeker == null));
            Bind();
        }

        protected void Bind()
        {
            if (!(Scholarships == null))
            {
                lstScholarships.DataSource = (from s in Scholarships orderby s.Name select s).ToList();
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship) ((ListViewDataItem) e.Item).DataItem);
                var link = (LinkButton)e.Item.FindControl("linkBtn");
                if(!(UserContext.CurrentSeeker == null))
                {
                    var ub =new UrlBuilder(Request.Url.AbsoluteUri) {Path = ResolveUrl(SCHOLARSHIP_VIEW_TEMPLATE)};
                    ub.QueryString.Clear();
                    var url=  ub.ToString() + "?id=" + scholarship.Id + "&print=true";
                    link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
                }

                else 
                    link.Enabled = false;
    
                var provider = (Literal)e.Item.FindControl("lblProvider");
                provider.Text = String.IsNullOrEmpty(scholarship.Provider.Name) ? scholarship.Intermediary.Name : scholarship.Provider.Name;
            }
        }
        
        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }

        
    }
}