﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="ScholarBridge.Web.ForgotPassword" Title="Forgot Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:PasswordRecovery ID="passwordRecovery" runat="server" 
        UserNameInstructionText="Enter your Email to receive a new password." 
        UserNameLabelText="Email:" 
        UserNameRequiredErrorMessage="Email is required." 
        onsendingmail="passwordRecovery_SendingMail">
        <SuccessTemplate>
            <h3>Your password has been sent to you.</h3>
            <asp:HyperLink ID="loginLnk" runat="server" NavigateUrl="~/Login.aspx">Return to Login page</asp:HyperLink>
        </SuccessTemplate>
    </asp:PasswordRecovery>

</asp:Content>
