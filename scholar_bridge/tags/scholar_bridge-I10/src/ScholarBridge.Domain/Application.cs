using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Domain
{
    public class Application : CriteriaCountBase
    {
        public const string APPLICATION_SUMBISSION_RULESET = "Application-Submission";

        private SeekerAddress address;


        public Application()
        {
            InitializeMembers();
        }

        public Application(Match m) : this()
        {
            Scholarship = m.Scholarship;
            Seeker = m.Seeker;
            
            InitializeMembers();
        }

        private void InitializeMembers()
        {
            ApplicantName = new PersonName();
            Attachments = new List<Attachment>();
            Ethnicities = new List<Ethnicity>();
            Religions = new List<Religion>();
            Sports = new List<Sport>();

            AcademicAreas = new List<AcademicArea>();
            Careers = new List<Career>();
            CommunityServices = new List<CommunityService>();
            Hobbies = new List<SeekerHobby>();
            Clubs = new List<Club>();
            MatchOrganizations = new List<SeekerMatchOrganization>();
            AffiliationTypes = new List<AffiliationType>();
            WorkTypes = new List<WorkType>();
            WorkHours = new List<WorkHour>();
            ServiceTypes = new List<ServiceType>();
            ServiceHours = new List<ServiceHour>();
            Address = new SeekerAddress();
            Need = new DefinitionOfNeed();
            SupportedSituation = new SupportedSituation();
            QuestionAnswers = new List<ApplicationQuestionAnswer>(); 
        }

        public virtual int Id { get; set; }
        public virtual Scholarship Scholarship { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual ApplicationStages Stage { get; set; }

        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? SubmittedDate { get; set; }
        public virtual bool Finalist { get; set; }
        public virtual AwardStatuses AwardStatus { get; set; }

        public virtual PersonName ApplicantName { get; set; }
        public virtual string Email { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual PhoneNumber MobilePhone { get; set; }
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? DateOfBirth { get; set; }

        public virtual County County { get; set; }

        public virtual SeekerAddress Address
        {
            get
            {
                if (address == null)
                    address = new SeekerAddress();
                return address;
            }
            set
            {
                address = value;
            }
        }

        public virtual bool FirstGeneration { get; set; }

        public virtual Genders Gender { get; set; }

        public virtual CurrentSchool CurrentSchool { get; set; }

        [StringLengthValidator(0, 1500)]
        public virtual string PersonalStatement { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyChallenge { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyGift { get; set; }

        public virtual SeekerAcademics SeekerAcademics { get; set; }

        [StringLengthValidator(0, 100)]
        public virtual string Words { get; set; }
        [StringLengthValidator(0, 100)]
        public virtual string Skills { get; set; }

        public virtual IList<Ethnicity> Ethnicities { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string EthnicityOther { get; set; }

        public virtual IList<Religion> Religions { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ReligionOther { get; set; }

        public virtual IList<Sport> Sports { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string SportOther { get; set; }

        public virtual IList<AcademicArea> AcademicAreas { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string AcademicAreaOther { get; set; }

        public virtual IList<Career> Careers { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CareerOther { get; set; }

        public virtual IList<CommunityService> CommunityServices { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CommunityServiceOther { get; set; }

        public virtual IList<SeekerHobby> Hobbies { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string HobbyOther { get; set; }

        public virtual IList<Club> Clubs { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ClubOther { get; set; }

        public virtual IList<SeekerMatchOrganization> MatchOrganizations { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string MatchOrganizationOther { get; set; }

        public virtual IList<AffiliationType> AffiliationTypes { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string AffiliationTypeOther { get; set; }

        public virtual IList<WorkType> WorkTypes { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string WorkTypeOther { get; set; }

        public virtual IList<WorkHour> WorkHours { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string WorkHourOther { get; set; }

        public virtual IList<ServiceType> ServiceTypes { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ServiceTypeOther { get; set; }

        public virtual IList<ServiceHour> ServiceHours { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ServiceHourOther { get; set; }

        public virtual DefinitionOfNeed Need { get; set; }
        public virtual SupportedSituation SupportedSituation { get; set; }

        public virtual IList<Attachment> Attachments { get; protected set; }
        public virtual IList<ApplicationQuestionAnswer> QuestionAnswers { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual bool HasAttachments { get { return Attachments != null && Attachments.Count > 0; } }
        public virtual void CopyFromSeekerandScholarship()
        {
            var seeker = this.Seeker;
            if (seeker == null)
                throw new  NullReferenceException("Seeker");

            var scholarship = this.Scholarship;

            if (scholarship == null)
                throw new  NullReferenceException("Scholarship");

            this.ApplicantName = seeker.Name;
            //
            this.AcademicAreaOther = seeker.AcademicAreaOther;
            this.AcademicAreas = new List<AcademicArea>(seeker.AcademicAreas);
            this.Address = seeker.Address;
            this.AffiliationTypeOther = seeker.AffiliationTypeOther;
            this.AffiliationTypes =new List<AffiliationType>(seeker.AffiliationTypes);
            this.ApplicantName = seeker.Name;
            this.CareerOther = seeker.CareerOther;
            this.Careers =new List<Career>( seeker.Careers);
            this.ClubOther = seeker.ClubOther;
            this.Clubs =new List<Club>(seeker.Clubs);
            this.CommunityServiceOther = seeker.CommunityServiceOther;
            this.CommunityServices =new List<CommunityService>( seeker.CommunityServices);
            this.County = seeker.County;
            this.CurrentSchool = seeker.CurrentSchool;
            this.DateOfBirth = seeker.DateOfBirth;
            this.Email = seeker.User.Email;
            this.Ethnicities = new List<Ethnicity>(seeker.Ethnicities);
            this.EthnicityOther = seeker.EthnicityOther;
            this.FirstGeneration = seeker.FirstGeneration;
            this.Gender = seeker.Gender;
            this.Hobbies = new List<SeekerHobby>(seeker.Hobbies);
            this.HobbyOther = seeker.HobbyOther;
            this.MatchOrganizations = new List<SeekerMatchOrganization>(seeker.MatchOrganizations);
            this.MatchOrganizationOther = seeker.MatchOrganizationOther;
            this.MobilePhone = seeker.MobilePhone;
            this.MyChallenge = seeker.MyChallenge;
            this.MyGift = seeker.MyGift;
            this.Need =(DefinitionOfNeed) seeker.Need.Clone();
                
                
            this.PersonalStatement = seeker.PersonalStatement;
            this.Phone = seeker.Phone;
            this.ReligionOther = seeker.ReligionOther;
            this.Religions = new List<Religion>(seeker.Religions);
            this.SeekerAcademics =(SeekerAcademics) seeker.SeekerAcademics.Clone();
            this.ServiceHourOther = seeker.ServiceHourOther;
            this.ServiceHours = new List<ServiceHour>(seeker.ServiceHours);
            this.ServiceTypeOther = seeker.ServiceTypeOther;
            this.ServiceTypes = new List<ServiceType>(seeker.ServiceTypes); 
            
            this.Skills = seeker.Skills;
            this.SportOther = seeker.SportOther;
            this.Sports=new List<Sport>(seeker.Sports);
            this.Words = seeker.Words; 
            this.WorkHourOther = seeker.WorkHourOther;
            this.WorkHours = new List<WorkHour>(seeker.WorkHours); 
            this.WorkTypeOther = seeker.WorkTypeOther;
            this.WorkTypes = new List<WorkType>(seeker.WorkTypes);
       

            //copy questions from scholarship
            foreach (ScholarshipQuestion q in scholarship.AdditionalQuestions)
            {

                var qans = new ApplicationQuestionAnswer()
                               {
                                   Question = q,
                                   Application = this,
                                   AnswerText = "",
                                   LastUpdate = new ActivityStamp(seeker.User)


                               };
                this.QuestionAnswers.Add(qans);
            }
        }

        public virtual void Submit()
        {
            if (Stage != ApplicationStages.Submitted)
            {
                Stage = ApplicationStages.Submitted;
                SubmittedDate = DateTime.Now; 
            }
        }

        public virtual bool CanEdit()
        {
            return Stage != ApplicationStages.Submitted;
        }

        public virtual ValidationResults ValidateSubmission()
        {
            var results = Validate(APPLICATION_SUMBISSION_RULESET);
            //FIXME: We need to return validation results as applicable
            return results;
        }

        private ValidationResults Validate(string ruleSet)
        {
            var validator = ValidationFactory.CreateValidator<Application >(ruleSet);
            return validator.Validate(this);
        }
    }
}