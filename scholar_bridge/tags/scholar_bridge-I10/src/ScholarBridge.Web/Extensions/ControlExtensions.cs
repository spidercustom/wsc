﻿using System;
using System.Web.UI;
using System.Linq;

namespace ScholarBridge.Web.Extensions
{
    public static class ControlExtensions
    {
        public static void SetVisibilityDependent(this Control @base, Control mate)
        {
            if (null == @base) throw new ArgumentNullException("base");
            if (null == mate) throw new ArgumentNullException("mate");
            mate.PreRender += (sender, e) => mate.Visible = @base.Visible;
        }

        public static void SetVisibilityDependencies(this Control dependent, bool negate, params Control[] dependsOn)
        {
            SetVisibilityDependencies(dependent, false, negate, dependsOn);
        }

        public static void SetVisibilityDependencies(this Control dependent, params Control[] dependsOn)
        {
            SetVisibilityDependencies(dependent, false, false, dependsOn);
        }
        
        public static void SetVisibilityDependencies(this Control dependent, bool useAnding, bool negate, params Control[] dependsOn)
        {
            if (dependent == null) throw new ArgumentNullException("dependent");
            if (dependsOn == null) throw new ArgumentNullException("dependsOn");

            if (useAnding)
                dependent.Visible = dependsOn.All(o => o.Visible);
            else
                dependent.Visible = dependsOn.Any(o => o.Visible);

            if (negate)
                dependent.Visible = !dependent.Visible;
        }
    }
}
