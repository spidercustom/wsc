﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(81)]
    public class SeekerMatchOrganization : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "SeekerMatchOrganization";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
