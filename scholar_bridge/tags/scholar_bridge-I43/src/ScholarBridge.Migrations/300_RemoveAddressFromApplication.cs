using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(300)]
    public class RemoveAddressFromApplication : Migration
    {
        private const string TABLE_NAME = "SBApplication";
        private const string FK_COUNTY = "FK_SBApplication_CountyIndex";
        private const string FK_CITY = "FK_SBApplication_CityIndex";
        public override void Down()
        {
            
        }

        public override void Up()
        {

            Database.RemoveForeignKey(TABLE_NAME, FK_CITY);
            Database.RemoveForeignKey(TABLE_NAME, FK_COUNTY);

            Database.RemoveColumn(TABLE_NAME, "FirstName");
            Database.RemoveColumn(TABLE_NAME, "MiddleName");
            Database.RemoveColumn(TABLE_NAME, "LastName");
            Database.RemoveColumn(TABLE_NAME, "Email");
            Database.RemoveColumn(TABLE_NAME, "PhoneNumber");
            Database.RemoveColumn(TABLE_NAME, "MobilePhoneNumber");

            Database.RemoveColumn(TABLE_NAME, "AddressStreet1");
            Database.RemoveColumn(TABLE_NAME, "AddressStreet2");
            Database.RemoveColumn(TABLE_NAME, "AddressState");
            Database.RemoveColumn(TABLE_NAME, "AddressPostalCode");
            Database.RemoveColumn(TABLE_NAME, "CityIndex");
            Database.RemoveColumn(TABLE_NAME, "CountyIndex");
            Database.RemoveColumn(TABLE_NAME, "AddressCity");
            Database.RemoveColumn(TABLE_NAME, "AddressCounty");
        }

      
    }
}