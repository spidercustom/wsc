using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class AdditionalRequirementDALTest : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public AdditionalRequirementDAL AdditionalRequirementDAL { get; set; }

        // private avoids injection issues for spring tests
        private AdditionalRequirement AddedReq { get; set; }

        protected override void OnSetUpInTransaction()
        {
            var u = UserDALTest.InsertUser(UserDAL, "test@example.com");
            AddedReq = AdditionalRequirementDAL.Insert(new AdditionalRequirement
                                                           {
                                                               Name = "Test",
                                                               Description = "Desc",
                                                               LastUpdate = new ActivityStamp(u)
                                                           });
        }

        [Test]
        public void CanGetById()
        {
            var foundAr = AdditionalRequirementDAL.FindById(AddedReq.GetIdAsInteger());
            Assert.IsNotNull(foundAr);
            Assert.AreEqual(AddedReq.Name, foundAr.Name);
            Assert.AreEqual(AddedReq.Description, foundAr.Description);
        }

        [Test]
        public void CanGetAll()
        {
            var additionalReqs = AdditionalRequirementDAL.FindAll();
            Assert.IsNotNull(additionalReqs);
            CollectionAssert.IsNotEmpty(additionalReqs);
            CollectionAssert.AllItemsAreNotNull(additionalReqs);
        }

        [Test]
        public void CanGetAllById()
        {
            var additionalReqs = AdditionalRequirementDAL.FindAll();
            var ids = new List<object>
                          {
                              additionalReqs[0].Id,
                              additionalReqs[1].Id,
                          };

            var found = AdditionalRequirementDAL.FindAll(ids);

            Assert.IsNotNull(found);
            Assert.AreEqual(2, found.Count);
            CollectionAssert.IsNotEmpty(found);
            CollectionAssert.AllItemsAreNotNull(found);
        }

        [Test]
        public void CanUpdate()
        {

            AddedReq.Name = "Foo";
            AdditionalRequirementDAL.Update(AddedReq);

            var foundAr = AdditionalRequirementDAL.FindById(AddedReq.GetIdAsInteger());
            Assert.IsNotNull(foundAr);
            Assert.AreEqual("Foo", foundAr.Name);
            Assert.AreEqual(AddedReq.Description, foundAr.Description);
        }
    }
}