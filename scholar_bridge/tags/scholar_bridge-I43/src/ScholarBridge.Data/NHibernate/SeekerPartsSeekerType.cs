using NHibernate.Type;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>SeekerType</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class SeekerPartsSeekerType : EnumStringType
    {
        public SeekerPartsSeekerType()
            : base(typeof(ScholarBridge.Domain.SeekerType), 255)
        {
        }
    }
}