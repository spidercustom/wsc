﻿using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;
using Spider.Common.Core.DAL;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Data
{
    public interface IApplicationQuestionAnswerDAL : IDAL<ApplicationQuestionAnswer>
    {
        ApplicationQuestionAnswer FindById(int id);
        ApplicationQuestionAnswer FindByApplicationandQuestion(Application application,ScholarshipQuestion question );
        IList<ApplicationQuestionAnswer> FindByApplication(Application application);
        ApplicationQuestionAnswer Save(ApplicationQuestionAnswer applicationQuestionAnswer);
      
        
    }
}