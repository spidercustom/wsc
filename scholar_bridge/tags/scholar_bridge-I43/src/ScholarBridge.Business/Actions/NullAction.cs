using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class NullAction : IAction
    {
        public readonly static NullAction Instance = new NullAction();

        public bool SupportsApprove { get { return false; } }

        public bool SupportsReject { get { return false; } }
        public string LastStatus { get; set; }
        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
        }
    }
}