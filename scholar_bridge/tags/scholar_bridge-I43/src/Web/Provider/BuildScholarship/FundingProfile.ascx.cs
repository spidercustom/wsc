﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class FundingProfile : WizardStepUserControlBase<Scholarship>
    {
        
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public ISupportDAL SupportDAL { get; set; }
        public IGenericLookupDAL<TermOfSupport> TermOfSupportDAL { get; set; }

        private bool ImpersonationInProgress
        {
            get { return ImpersonateUser.IsImpersonationInProgress(Session); }
        }
        private Scholarship ScholarshipInContext
    	{
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        private void DisableControls()
        {
            FAFSARequiredControl.Enabled = false;
            FAFSANotRequiredControl.Enabled = false;
            FAFSAEFCRequiredControl.Enabled = false;
            FAFSAEFCNotRequiredControl.Enabled = false;
            ApplicantNeedRequiredControl.Enabled = false;
            ApplicantNeedNotRequiredControl.Enabled = false;
        
        }

        private void PopulateScreen()
        {
            if (ScholarshipInContext.IsActivated() && !ImpersonationInProgress)
                DisableControls();
            PopulateScreenFinacialNeed();
        }

        private void PopulateScreenFinacialNeed()
        {
            FAFSARequiredControl.Checked = ScholarshipInContext.IsFAFSARequired ?? false;
            FAFSANotRequiredControl.Checked = ScholarshipInContext.IsFAFSARequired.HasValue && !ScholarshipInContext.IsFAFSARequired.Value;
            FAFSAEFCRequiredControl.Checked = ScholarshipInContext.IsFAFSAEFCRequired ?? false;
            FAFSAEFCNotRequiredControl.Checked = ScholarshipInContext.IsFAFSAEFCRequired.HasValue && !ScholarshipInContext.IsFAFSAEFCRequired.Value;
            ApplicantNeedRequiredControl.Checked = ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired ?? false;
            
            ApplicantNeedNotRequiredControl.Checked = ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired.HasValue && !ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired.Value;
        }

        public override void PopulateObjects()
        {
            PopulateObjectsFinacialNeed();

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStages.NotActivated;
        }

       

        private void PopulateObjectsFinacialNeed()
        {
            ScholarshipInContext.IsFAFSARequired = null;
            if (FAFSARequiredControl.Checked)
                ScholarshipInContext.IsFAFSARequired = true;
            if (FAFSANotRequiredControl.Checked)
                ScholarshipInContext.IsFAFSARequired = false;

            ScholarshipInContext.IsFAFSAEFCRequired = null;
            if (FAFSAEFCRequiredControl.Checked)
                ScholarshipInContext.IsFAFSAEFCRequired = true;
           
            if (FAFSAEFCNotRequiredControl.Checked)
                ScholarshipInContext.IsFAFSAEFCRequired = false;

            ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired = null;
			
			// add/remove attribute usage for 'Need' based on button checked
            if (ApplicantNeedRequiredControl.Checked)
            {
                ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired = true;

				if (!ScholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.Need))
				{
					ScholarshipInContext.FundingProfile.AttributesUsage.Add(new FundingProfileAttributeUsage(){Attribute = FundingProfileAttribute.Need, UsageType = ScholarshipAttributeUsageType.Minimum});
				}
            }
			else
            {
				if (ApplicantNeedNotRequiredControl.Checked)
					ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired = false;

            	FundingProfileAttributeUsage need =
            		ScholarshipInContext.FundingProfile.FindAttributeUsage(FundingProfileAttribute.Need);
				if (need != null)
				{
					ScholarshipInContext.FundingProfile.AttributesUsage.Remove(need);
				}
            }
			// SupportedSituation is always required so we'll always add it
			if (!ScholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.SupportedSituation))
			{
				ScholarshipInContext.FundingProfile.AttributesUsage.Add(new FundingProfileAttributeUsage() { Attribute = FundingProfileAttribute.SupportedSituation, UsageType = ScholarshipAttributeUsageType.Minimum });
			}
			
            

        }

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int)WizardStepName.MatchCriteria);
		}

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public decimal GetMoneyValue(TextBox tb, PropertyProxyValidator validator)
        {
            decimal amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (! Decimal.TryParse(tb.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public int GetIntValue(TextBox tb, PropertyProxyValidator validator)
        {
            int amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (!Int32.TryParse(tb.Text, NumberStyles.Integer | NumberStyles.AllowThousands, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public void PopulateListControl(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }

        #endregion
    }
}