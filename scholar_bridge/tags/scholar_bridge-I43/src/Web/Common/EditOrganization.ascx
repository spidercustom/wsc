﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditOrganization.ascx.cs" Inherits="ScholarBridge.Web.Common.EditOrganization" %>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %> 
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<div class="form-iceland-container">
    <div class="form-iceland">
        <label for="Name">Name:</label>
        <asp:TextBox ID="Name" runat="server" Enabled="false"/>
        <elv:PropertyProxyValidator ID="NameValidator" runat="server" ControlToValidate="Name" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Organization" />
        <br />
        <label for="TaxId">Tax Id (EIN):</label>
        <asp:TextBox ID="TaxId" runat="server" Enabled="false"/>
        <elv:PropertyProxyValidator ID="TaxIdValidator" runat="server" ControlToValidate="TaxId" PropertyName="TaxId" SourceTypeName="ScholarBridge.Domain.Organization" />
        <br />
        <label for="Website">Website:</label>
        <asp:TextBox ID="Website" runat="server"/>
        <elv:PropertyProxyValidator ID="WebsiteValidator" runat="server" ControlToValidate="Website" PropertyName="Website" SourceTypeName="ScholarBridge.Domain.Organization" />
         <br/>
        <label for="WebsiteButton">&nbsp;</label>
        <sbCommon:AnchorButton  ID="ValidateWebsite" runat="server" Text="Validate Website" OnClick="ValidateWebsite_Click" CausesValidation="false"/>    
        <sb:CoolTipInfo ID="CoolTipInfoWebsiteButton" runat="Server" Content="Clicking this button will open the website in a new window, please make sure that your browser allows popups." />
               
        <br/>
        <h4>Address</h4>
        <label for="AddressStreet">Address Line 1:</label>
        <asp:TextBox ID="AddressStreet" runat="server" Enabled="false"/>
        <elv:PropertyProxyValidator ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressStreet2">Address Line 2:</label>
        <asp:TextBox ID="AddressStreet2" runat="server" Enabled="false"/>
        <elv:PropertyProxyValidator ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressCity">City:</label>
        <asp:TextBox ID="AddressCity" runat="server" Enabled="false"/>
        <elv:PropertyProxyValidator ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressState">State:</label>
        <asp:DropDownList ID="AddressState" runat="server" Enabled="false"></asp:DropDownList>
        <elv:PropertyProxyValidator ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />
        <label for="AddressPostalCode">Postal Code:</label>
        <asp:TextBox ID="AddressPostalCode" runat="server" Enabled="false"></asp:TextBox>
        <elv:PropertyProxyValidator ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
        <br />

        <h4>Phones</h4>
        <label for="Phone">Phone:</label>
        <asp:TextBox ID="Phone" runat="server" Enabled="false"/>
        <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
        <br />
        <label for="Fax">Fax:</label>
        <asp:TextBox ID="Fax" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="FaxValidator" runat="server" ControlToValidate="Fax" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
        <br />
        <label for="OtherPhone">Other Phone:</label>
        <asp:TextBox ID="OtherPhone" runat="server"/>
        <elv:PropertyProxyValidator ID="OtherPhoneValidator" runat="server" ControlToValidate="OtherPhone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
        <br />
        <sbCommon:AnchorButton ID="saveBtn" runat="server" Text="Save" onclick="saveBtn_Click" /> 
        <br />
    </div>
    
 </div>
     
    
    
     

