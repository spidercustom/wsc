﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Seeker.BuildApplication;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationAcademicInfoShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                if (null != ApplicationToView.CurrentSchool)
                {
                    studentType.Text = ApplicationToView.CurrentSchool.LastAttended.GetDisplayName();
                    highSchool.Text = ApplicationToView.CurrentSchool.HighSchoolOther ??
                                      ApplicationToView.CurrentSchool.HighSchool.Name;
                    schoolDistrict.Text = ApplicationToView.CurrentSchool.SchoolDistrictOther ??
                                          ApplicationToView.CurrentSchool.SchoolDistrict.Name;
                    currentCollege.Text = ApplicationToView.CurrentSchool.CollegeOther ??
                                          ApplicationToView.CurrentSchool.College.Name;
                }

                if (null != ApplicationToView.SeekerAcademics)
                {
                    if (ApplicationToView.SeekerAcademics.GPA.HasValue)
                        gpa.Text = ApplicationToView.SeekerAcademics.GPA.Value.ToString("0.000");


                    //classRank.Text = ApplicationToView.SeekerAcademics.ClassRank.Name;

                    if (null != ApplicationToView.SeekerAcademics.SATScore)
                    {
                        var sat = ApplicationToView.SeekerAcademics.SATScore;
                        if (sat.CriticalReading.HasValue)
                            satReading.Text = sat.CriticalReading.Value.ToString();
                        if (sat.Writing.HasValue)
                            satWriting.Text = sat.Writing.Value.ToString();
                        if (sat.Mathematics.HasValue)
                            satMath.Text = sat.Mathematics.Value.ToString();
                    }

                    if (null != ApplicationToView.SeekerAcademics.ACTScore)
                    {
                        var act = ApplicationToView.SeekerAcademics.ACTScore;
                        if (act.Reading.HasValue)
                            actReading.Text = act.Reading.Value.ToString();
                        if (act.English.HasValue)
                            actEnglish.Text = act.English.Value.ToString();
                        if (act.Mathematics.HasValue)
                            actMath.Text = act.Mathematics.Value.ToString();
                        if (act.Science.HasValue)
                            actScience.Text = act.Science.Value.ToString();
                    }

                    honors.Text = ApplicationToView.SeekerAcademics.Honors ? "Yes" : "No";

                    //if (ApplicationToView.SeekerAcademics.APCredits.HasValue)
                    //    apCredits.Text = ApplicationToView.SeekerAcademics.APCredits.Value.ToString();
                    //if (ApplicationToView.SeekerAcademics.IBCredits.HasValue)
                    //    ibCredits.Text = ApplicationToView.SeekerAcademics.IBCredits.Value.ToString();

                    academicProgram.Text = String.Join(", ", ApplicationToView.SeekerAcademics.AcademicPrograms.GetDisplayNames());
                    enrollmentStatus.Text = String.Join(", ", ApplicationToView.SeekerAcademics.SeekerStatuses.GetDisplayNames());
                    lengthOfProgram.Text = String.Join(", ", ApplicationToView.SeekerAcademics.ProgramLengths.GetDisplayNames());
                    schoolsConsidering.Text = String.Join(", ", ApplicationToView.SeekerAcademics.SchoolTypes.GetDisplayNames());

                    collegesApplied.DataSource = ApplicationToView.SeekerAcademics.CollegesApplied;
                    collegesApplied.DataBind();
                    collegesAppliedOther.Text = ApplicationToView.SeekerAcademics.CollegesAppliedOther;

                    collegesAccepted.DataSource = ApplicationToView.SeekerAcademics.CollegesAccepted;
                    collegesAccepted.DataBind();
                    collegesAcceptedOther.Text = ApplicationToView.SeekerAcademics.CollegesAcceptedOther;
                }


                firstGeneration.Text = ApplicationToView.FirstGeneration ? "Yes" : "No";

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);

            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            FirstGenerationRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.FirstGeneration);

            StudentGroupRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.StudentGroup);
            SchoolDistrictsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.SchoolDistrict;
            HighSchoolsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.HighSchool;

            SchoolTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SchoolType);
            AcademicProgramsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.AcademicProgram);
            EnrollmentStatusesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SeekerStatus);
            LengthOfProgrammRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ProgramLength);
            CollegesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.College);


            GPARow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.GPA);
			bool hasSATReading = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SATCriticalReading);
			bool hasSATWriting = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SATWriting);
			bool hasSATMathematics = seeker.Attributes.HasAttribute(SeekerProfileAttribute.SATMathematics);
			satScoreRow.Visible = hasSATReading || hasSATMathematics || hasSATWriting;
			satCriticalReadingScoreRow.Visible = hasSATReading;
			satWritingScoreRow.Visible = hasSATWriting;
			satMathematicsScoreRow.Visible = hasSATMathematics;
			bool hasACTEnglish = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTEnglish);
			bool hasACTMathematics = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTMathematics);
			bool hasACTReading = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTReading);
        	bool hasACTScience = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ACTScience);
			actScoreRow.Visible = hasACTEnglish || hasACTMathematics || hasACTReading || hasACTScience;
			actEnglishScoreRow.Visible = hasACTEnglish;
			actMathematicsScoreRow.Visible = hasACTMathematics;
			actReadingScoreRow.Visible = hasACTReading;
			actScienceScoreRow.Visible = hasACTScience;
			ClassRankRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ClassRank);
            APCreditsEarnedRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.APCreditsEarned);
            IBCreditsEarnedRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.IBCreditsEarned);
            HonorsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Honor);
            ApplicantStudentPerformance.Visible = ApplicantStudentPerformance.Visible =
                GPARow.Visible ||
                satScoreRow.Visible ||
                actScoreRow.Visible ||
                ClassRankRow.Visible ||
                APCreditsEarnedRow.Visible ||
                IBCreditsEarnedRow.Visible ||
                HonorsRow.Visible;
        }

        public override int ResumeFrom
        {
            get { return WizardStepName.Academics.GetNumericValue(); }
        }
    }
}