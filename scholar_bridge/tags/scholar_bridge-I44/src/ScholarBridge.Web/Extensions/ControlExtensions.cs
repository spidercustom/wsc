﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Extensions
{
    public static class ControlExtensions
    {
        public static void VisibleIf(this Control control, Func<bool> constraint)
        {
            control.PreRender += delegate { control.Visible = constraint(); };
        }

        public static Control FindControlRecursive(this Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }
            return null;
        }

    }
}
