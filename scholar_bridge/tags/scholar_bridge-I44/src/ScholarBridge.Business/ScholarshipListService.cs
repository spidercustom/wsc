﻿using System;
using System.IO;
using System.Net;
using ScholarBridge.Data;


namespace ScholarBridge.Business
{
	public class ScholarshipListService : IScholarshipListService
	{
    	public const string SCHOLARSHIP_LIST_FILE_NAME = "Scholarships.xml";
		public IScholarshipDAL ScholarshipDAL { get; set; }

		// the following members are set via config
		public string ScholarshipListDirectory { get; set; }
		public bool ScholarshipListFTPEnabled { get; set; }
		public string ScholarshipListFTPSite { get; set; }
		public string ScholarshipListFTPDirectory { get; set; }
		public string ScholarshipListFTPLogon { get; set; }
		public string ScholarshipListFTPPassword { get; set; }

		public string GetScholarshipList()
		{
			string xml = ScholarshipDAL.GetScholarshipListXml();
			if (string.IsNullOrEmpty(xml))
				xml = "<doc></doc>"; // assure valid xml even if the list is empty.
			return xml;
		}

		public void GenerateAndStoreScholarshipList()
		{
			string scholarshipListXml = GetScholarshipList();
			string scholarshipListFilePath = ScholarshipListDirectory.EndsWith("\\")
			                                 	? ScholarshipListDirectory
			                                 	: ScholarshipListDirectory + "\\";
			scholarshipListFilePath += SCHOLARSHIP_LIST_FILE_NAME;
			File.WriteAllText(scholarshipListFilePath, scholarshipListXml);
			if (ScholarshipListFTPEnabled)
				PutScholarshipListViaFTP(scholarshipListFilePath);
		}

		private void PutScholarshipListViaFTP(string scholarshipListFilePath)
		{
			FileInfo fileInf = new FileInfo(scholarshipListFilePath);

			string uri = !ScholarshipListFTPSite.StartsWith("ftp://") 
				? "ftp://" + ScholarshipListFTPSite 
				: ScholarshipListFTPSite;

			if (!string.IsNullOrEmpty(ScholarshipListFTPDirectory) )
				uri += "/" + ScholarshipListFTPDirectory;

			uri += "/" + SCHOLARSHIP_LIST_FILE_NAME;

			// Create FtpWebRequest object from the Uri provided
			FtpWebRequest reqFtp = (FtpWebRequest)WebRequest.Create(new Uri(uri));

			// Provide the WebPermission Credintials
			reqFtp.Credentials = new NetworkCredential(ScholarshipListFTPLogon, ScholarshipListFTPPassword);

			// By default KeepAlive is true, where the control connection is not closed
			// after a command is executed.
			reqFtp.KeepAlive = false;

			// Specify the command to be executed.
			reqFtp.Method = WebRequestMethods.Ftp.UploadFile;

			// Specify the data transfer type.
			reqFtp.UseBinary = true;

			// Notify the server about the size of the uploaded file
			reqFtp.ContentLength = fileInf.Length;

			// The buffer size is set to 2kb
			const int buffLength = 2048;
			byte[] buff = new byte[buffLength];

			// Opens a file stream (System.IO.FileStream) to read the file to be uploaded
			using (FileStream fs = fileInf.OpenRead())
			{
			    // Stream to which the file to be upload is written
                using (Stream strm = reqFtp.GetRequestStream())
                {

                    // Read from the file stream 2kb at a time
                    int contentLen = fs.Read(buff, 0, buffLength);

                    // Till Stream content ends
                    while (contentLen != 0)
                    {
                        // Write Content from the file stream to the FTP Upload Stream
                        strm.Write(buff, 0, contentLen);
                        contentLen = fs.Read(buff, 0, buffLength);
                    }
                }
			}
	    }
	}
}
