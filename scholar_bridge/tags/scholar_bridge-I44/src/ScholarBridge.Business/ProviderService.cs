using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class ProviderService : OrganizationService<Provider>, IProviderService
    {
        protected override void AddRolesToNewUser(User user, bool isAdmin)
        {
            user.Roles.Add(RoleDAL.FindByName(Role.PROVIDER_ROLE));
            if (isAdmin)
                user.Roles.Add(RoleDAL.FindByName(Role.PROVIDER_ADMIN_ROLE));
        }

        protected override MessageType TemplateForApproval
        {
            get { return MessageType.ProviderApproved; }
        }

        protected override MessageType TemplateForRejection
        {
            get { return MessageType.ProviderRejected; }
        }
    }
}