using System;
using System.Collections.Generic;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class ApplicationService : IApplicationService
    {
        public IApplicationDAL ApplicationDAL { get; set; }
        public IMatchService MatchService { get; set; }
        public IMessagingService MessagingService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }

        public Application GetById(int id)
        {
            return ApplicationDAL.FindById(id);
        }

        public void SaveNew(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            MatchService.ApplyForMatch(application.Seeker, application.Scholarship.Id, application);
            ApplicationDAL.Insert(application);
        }

        public void SubmitApplication(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Calculate Min/Pref criteria and populate the object
            application.Submit();
            ApplicationDAL.Update(application);

            // Save submission message to seeker's sent box
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ApplicationSubmisionSuccess(application, templateParams);
            var msg = new Message
            {
                MessageTemplate = MessageType.ApplicationSubmisionSuccess,
                To = new MessageAddress { Organization = application.Scholarship.Provider },
                From = new MessageAddress {  User = application.Seeker.User  },
                LastUpdate = new ActivityStamp(application.Seeker.User)
            };

            MessagingService.SaveNewSentMessage(msg,templateParams); 

            // Send Acknowlegement message to seeker
            templateParams = new MailTemplateParams();
            TemplateParametersService.ApplicationAcknowledgement(application, templateParams);
            msg = new Message
            {
                MessageTemplate = MessageType.ApplicationAcknowledgement,
                To = new MessageAddress { User = application.Seeker.User },
                From = new MessageAddress { Organization = application.Scholarship.Provider },
                LastUpdate = new ActivityStamp(application.Seeker.User)
            };
            MessagingService.SendMessage(msg, templateParams, application.Seeker.User.IsRecieveEmails);

        }

        public IList<Application> FindAllSubmitted(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

			// scholarship is new so no app's can be attached yet
			if (scholarship.Stage == ScholarshipStage.None)
        		return new List<Application>();

			return ApplicationDAL.FindAllSubmitted(scholarship);
        }

		public int CountAllSavedButNotSubmitted()
		{
			return ApplicationDAL.CountAllStartedButNotSubmitted();	
		}

		public int CountAllSubmitted()
		{
			return ApplicationDAL.CountAllSubmitted();
		}

        public int CountAllSubmittedBySeeker(Seeker seeker)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            return ApplicationDAL.CountAllSubmittedBySeeker(seeker);
        }

        public IList<Application> FindAllSubmitted(Scholarship scholarship, string lastNameSearch)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.SearchSubmittedBySeeker(scholarship, lastNameSearch);
        }

        public IList<Application> FindAllFinalists(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.FindAllFinalists(scholarship);
        }

        public void Finalist(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Check if awarded or rejected and don't allow changes after that point?
            if (!application.Finalist)
            {
                application.Finalist = true;
                ApplicationDAL.Update(application);
            }
        }

        public void NotFinalist(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Check if awarded or rejected and don't allow changes after that point?
            if (application.Finalist)
            {
                application.Finalist = false;
                ApplicationDAL.Update(application);
            }
        }

        public void AwardScholarship(Application application, User awarder)
        {
            ChangeStatus(application, AwardStatus.Awarded,
                         () =>
                             {
                                 // Send message to seeker
                                 var templateParams = new MailTemplateParams();
                                 TemplateParametersService.AwardScholarship(application, templateParams);
                                 var msg = new Message
                                               {
                                                   MessageTemplate = MessageType.AwardScholarship,
                                                   To = new MessageAddress {User = application.Seeker.User},
                                                   From = new MessageAddress {Organization = application.Scholarship.Provider},
                                                   LastUpdate = new ActivityStamp(awarder)
                                               };
                                 MessagingService.SendMessage(msg, templateParams, application.Seeker.User.IsRecieveEmails);
                             });
        }

        public void OfferScholarship(Application application, User awarder)
        {
            ChangeStatus(application, AwardStatus.Offered,
                         () =>
                             {
                                 var templateParams = new MailTemplateParams();
                                 TemplateParametersService.OfferScholarship(application, templateParams);
                                 var msg = new Message
                                               {
                                                   MessageTemplate = MessageType.OfferScholarship,
                                                   To = new MessageAddress {User = application.Seeker.User},
                                                   From = new MessageAddress {Organization = application.Scholarship.Provider},
                                                   LastUpdate = new ActivityStamp(awarder)
                                               };
                                 MessagingService.SendMessage(msg, templateParams, application.Seeker.User.IsRecieveEmails);
                             });
        }

        public void NotAwardScholarship(Application application, User awarder)
        {
            ChangeStatus(application, AwardStatus.NotAwarded,
                         () =>
                             {
                                 // XXX: No email? 
                             });
        }

        private void ChangeStatus(Application application, AwardStatus status, Action sendEmail)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            if (application.AwardStatus != status)
            {
                application.AwardStatus = status;
                ApplicationDAL.Update(application);

                sendEmail();
            }
        }

        public void Update(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            ApplicationDAL.Update(application);
        }

		/// <summary>
		/// return all <see cref="Application"></see> for a given <see cref="Seeker"/>
		/// </summary>
		/// <param name="seeker"></param>
		/// <returns></returns>
		public IList<Application> FindBySeeker(Seeker seeker)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");

			return ApplicationDAL.FindAll(seeker);
		}

        public  int CountAllHavingAttachment(Attachment attachment)
        {
            if (null == attachment)
                throw new ArgumentNullException("attachment");

            return ApplicationDAL.CountAllHavingAttachment(attachment);
        }

		public Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship)
		{
			if (null == seeker)
				throw new ArgumentNullException("seeker");
			if (null == scholarship)
				throw new ArgumentNullException("scholarship");

			return ApplicationDAL.FindBySeekerandScholarship(seeker, scholarship);
		}

		public void Delete(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            MatchService.DisconnectApplication(application);
            ApplicationDAL.Delete(application);
        }
    }
}