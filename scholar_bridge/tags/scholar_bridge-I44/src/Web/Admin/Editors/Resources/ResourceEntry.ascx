﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceEntry.ascx.cs" Inherits="ScholarBridge.Web.Admin.Editors.Resources.ResourceEntry" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>  
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>           
<%@ Register assembly="Brettle.Web.NeatUpload" namespace="Brettle.Web.NeatUpload" tagprefix="Upload" %>
<asp:Label ID="errorMessage" runat="server" Text="Resource not found." Visible="false" CssClass="errorMessage"/>
<div class="form-iceland-container">
    <div class="form-iceland">
        <label for="titleControl">Resource Title:</label>
        <br />
        <asp:TextBox ID="titleControl" runat="server" TextMode="MultiLine" Rows="3" Width="600px" Height="50px"></asp:TextBox>
        
        <sb:CoolTipInfo Content="This will appear as title of an resource." runat="server" />
        <asp:RequiredFieldValidator ID="titleControlRequiredValidator" runat="server" ControlToValidate="titleControl"
         ErrorMessage="Should not be empty"></asp:RequiredFieldValidator>
        <elv:PropertyProxyValidator ID="titleControlValidator" runat="server" ControlToValidate="titleControl" 
        PropertyName="Title" SourceTypeName="ScholarBridge.Domain.Resource" ErrorMessage="should not exceed 250 characters." />
        <br />
        <br />
        <label for="displayOrderBox">Display Order :</label>
        <asp:TextBox ID="displayOrderBox" runat="server"></asp:TextBox>&nbsp;(0 - 99999)
        <asp:requiredfieldvalidator ID="displayOrderRequired" runat="server" ControlToValidate="displayOrderbox" ErrorMessage="Display Order is required"></asp:requiredfieldvalidator>
        <asp:RangeValidator
            ID="RangeValidator1" ControlToValidate="displayOrderbox" MaximumValue="99999" MinimumValue="0" Type="Integer" runat="server" ErrorMessage="Must be in range of 0 to 99999"></asp:RangeValidator><br />
        <label for="urlTextBox">Specify a URL:</label>
        <asp:TextBox ID="urlTextBox" runat="server"></asp:TextBox><br />
        <asp:panel ID="selectFilePanel" runat="server">
            &nbsp;&nbsp; - OR -<br />
            <label for="attachFile">Select a File: </label>
                <asp:FileUpload ID="attachFile" runat="server" /> <br />
             <sbCommon:AnchorButton ID="UploadFile" runat="server" Text="Attach File" CausesValidation="false" OnClick="UploadFile_Click" />
            <br />
            <asp:CustomValidator ID="attachmentValidator" runat="server" ErrorMessage="CustomValidator"></asp:CustomValidator>
        </asp:panel>
        <asp:panel ID="selectedFilePanel" runat="server" Visible="false">
            <br />
            <label for="attachedFile">Selected File: </label>
            <b><asp:label ID="attachedFile" runat="server"></asp:label></b>&nbsp;&nbsp;
             <sbCommon:AnchorButton ID="removeFileButton" runat="server" Text="Remove" OnClick="removeFile_Click" />
        </asp:panel>
        <br />
        <label for="descriptionControl">Resource Description:</label>
        <asp:Label ID="label1" runat="server" Width="600px">(text beginning with http:// and https:// will be made into a hyperlink up to the first space)</asp:Label> 

        <br />
        <asp:TextBox ID="descriptionControl" runat="server" TextMode="MultiLine" Rows="10" Width="600px" Height="300px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="descriptionControlRequiredValidator" runat="server" ControlToValidate="descriptionControl"
         ErrorMessage="Should not be empty."></asp:RequiredFieldValidator>
        <elv:PropertyProxyValidator ID="descriptionControlValidator" runat="server" ControlToValidate="descriptionControl" 
        PropertyName="Description" SourceTypeName="ScholarBridge.Domain.Resource" ErrorMessage="Should not exceed 4000 characters."  />
        <br />
        <br />
        <sbCommon:AnchorButton ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click"   />
        <sbCommon:AnchorButton ID="cancelButton" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelButton_Click" />
        <br />
        <br />
</div> 
</div>