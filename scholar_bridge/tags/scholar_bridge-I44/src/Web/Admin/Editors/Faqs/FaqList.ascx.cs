﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Admin.Editors.Faqs
{
	public partial class FaqList : System.Web.UI.UserControl
	{
		public IFaqService FaqService { get; set; }
		public IUserContext UserContext { get; set; }


		protected void Page_Load(object sender, EventArgs e)
		{
			MainMenuHelper.SetupActiveMenuKey(Page, MainMenuHelper.MainMenuKey.AdminFaqs);
			UserContext.EnsureUserIsInContext();
			if (!IsPostBack)
				Bind();
		}

		protected void Bind()
		{
			var faqs = FaqService.FindAll();
			lstFaqs.DataSource = faqs;
			lstFaqs.DataBind();
		}

		protected void lstFaqs_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var faq = ((Faq)((ListViewDataItem)e.Item).DataItem);

				var chk = (CheckBox)e.Item.FindControl("chkFaq");
				chk.Attributes.Add("value", faq.Id.ToString());

				var btnDelete = (ConfirmButton)e.Item.FindControl("SingleDeleteBtn");
				btnDelete.Attributes.Add("value", faq.Id.ToString());
				
                var btnEdit = (AnchorButton)e.Item.FindControl("SingleEditBtn");
				btnEdit.Attributes.Add("value", faq.Id.ToString());
			}
		}

		protected void lstFaqs_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
		{
			pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
			Bind();
		}

		protected void DeleteBtn_Click(object sender, ConfirmButtonClickEventArgs e)
		{
			if (!e.DialogResult) return;
			bool rebind = false;
			for (int i = 0; i < lstFaqs.Items.Count; i++)
			{
				var chk = (CheckBox)lstFaqs.Items[i].FindControl("chkFaq");
				if (chk.Checked)
				{
					var faqid = int.Parse(chk.Attributes["value"]);
					var faq = FaqService.GetById(faqid);
					if (!(faq == null))
						FaqService.Delete(faq);
					rebind = true;
				}
			}

			if (rebind)
				Bind();
		}

		protected void SingleDeleteBtn_Click(object sender, ConfirmButtonClickEventArgs e)
		{
			if (!e.DialogResult) return;
			var btn = (ConfirmButton)sender;
			var faqid = int.Parse(btn.Attributes["value"]);
			var faq = FaqService.GetById(faqid);
			if (!(faq == null))
				FaqService.Delete(faq);
			Bind();
		}

		protected void SingleEditBtn_Click(object sender, EventArgs e)
		{
			var btn = (AnchorButton)sender;
			var faqid = int.Parse(btn.Attributes["value"]);
			faqEntry.FaqID = faqid;
			ShowAddUpdatePanel();
		}

		protected void pager_PreRender(object sender, EventArgs e)
		{
			if (pager.TotalRowCount <= pager.PageSize)
			{
				pager.Visible = false;
			}
		}

		protected void createFaqLnk_Click(object sender, EventArgs e)
		{

			faqEntry.FaqID = null;
			ShowAddUpdatePanel();
		}

		private void ShowAddUpdatePanel()
		{
			faqListPanel.Visible = false;
			addUpdatePanel.Visible = true;
		}

		protected void FaqEntry1_OnFormSaved(Faq faq)
		{
			HideAddUpdatePanel();
			Bind();
		}

		private void HideAddUpdatePanel()
		{
			faqListPanel.Visible = true;
			addUpdatePanel.Visible = false;
		}

		protected void FaqEntry1_OnFormCanceled()
		{
			HideAddUpdatePanel();
		}
	}
}