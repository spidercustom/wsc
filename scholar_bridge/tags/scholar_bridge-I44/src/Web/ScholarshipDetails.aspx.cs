﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Xml.Linq;
using ScholarBridge.Business;

namespace ScholarBridge.Web
{
    public partial class ScholarshipDetails : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }

        protected string MetaDescription { get; set; }
        protected string MetaKeywords { get; set; }

        private LinkGenerator linkGenerator;
        protected LinkGenerator LinkGenerator
        {
            get
            {
                if (linkGenerator == null)
                    linkGenerator = new LinkGenerator();
                return linkGenerator;
            }
        }

        /// <summary>
        /// Depending on the IIS version, the scholarship key may come tacked onto the url (iis 5,6) or as a url data item
        /// 
        /// e.g
        /// IIS 6 - ~ScholarshipDetails.aspx/Provider+Name/2009-2010/My+Big+Fat+Scholarship
        /// IIS 7 - ~ScholarshipDetails.aspx?urlkey=Provider+Name/2009-2010/My+Big+Fat+Scholarship
        /// 
        /// On IIS 7 the url will appear extensionless to the user as:
        /// http://www.theWashBoard.org/ScholarshipDetails/Provider+Name/2009-2010/My+Big+Fat+Scholarship
        /// 
        /// </summary>
        protected string ScholarshipKey
        {
            get
            {
                string key;
                if (Request["urlkey"] == null)
                    key = Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.LastIndexOf(".aspx") + 5);
                else
                {
                    key = Request["urlkey"];
                }
                return Encode(Server.UrlDecode(key));
            }
        }

        protected int? ScholarshipID
        {
            get
            {
                int? id = new int?();
                if (Request["sid"] != null)
                    id = int.Parse(Request["sid"]);
                return id;
            }
        }

        private static string Encode(string key)
        {
            key = key.Replace(' ', '+').Replace("%20", "+");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < key.Length; i++)
            {
                string c = key.Substring(i, 1);
                Match m = Regex.Match(c, "([a-zA-Z0-9]|[/]|[+]|[()]|[*]|[!]|[-_])");
                if (m.Success)
                {
                    sb.Append(c);
                }
                else
                {
                    char[] singleChar = c.ToCharArray();
                    int value = Convert.ToInt32(singleChar[0]);
                    sb.Append(String.Format("%{0:X}", value));
                }
            }
            return sb.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "Scholarship details ";

            if (ScholarshipID.HasValue)
            {
                GetDataFromDB();
            }
            else
            {
                GetDataFromXML();
            }

            MetaDescription = string.Format("Scholarship '{0}' hosted by theWashBoard is offered by {1}", scholarshipNameLabel.Text, providerNameLabel.Text);
            MetaKeywords = string.Format("Scholarship,{0},{1}", scholarshipNameLabel.Text, providerNameLabel.Text);
        }

        private void GetDataFromDB()
        {
            var scholarship = ScholarshipService.GetById(ScholarshipID.Value);
            scholarshipNameLabel.Text = scholarship.Name;
            Page.Header.Title = "Scholarship details for " + scholarship.Name;
            providerNameLabel.Text = scholarship.Provider.Name;
            if (scholarship.Donor == null)
            {
                donorPanel.Visible = false;
            }
            else
            {
                if (string.IsNullOrEmpty(scholarship.Donor.Name))
                {
                    donorPanel.Visible = false;
                }
                else
                {
                    donorNameLabel.Text = scholarship.Donor.Name;
                }
            }
            academicYearLabel.Text = scholarship.AcademicYear.DisplayName;
            numberOfAwardsLabel.Text = scholarship.FundingParameters.MaximumNumberOfAwards.ToString("c");
            applicationStartDateLabel.Text = scholarship.ApplicationStartDate.Value.ToShortDateString();
            applicationDueDateLabel.Text = scholarship.ApplicationDueDate.Value.ToShortDateString();
            scholarshipAmountLabel.Text = scholarship.MaximumAmount.ToString("c");
            missionStatementLabel.Text = scholarship.MissionStatement;
        }

        private void GetDataFromXML()
        {
            XElement scholarshipDoc = XElement.Load(Server.MapPath("~/_ScholarshipData/scholarships.xml"));

            var query = from s in scholarshipDoc.Descendants("scholarship")
                        where s.Element("UrlKey").Value == ScholarshipKey
                        select new
                                   {
                                       Name = s.Element("Name") == null ? "" : s.Element("Name").Value,
                                       Provider =
                            s.Element("ProviderName") == null ? "" : s.Element("ProviderName").Value,
                                       Donor = s.Element("DonorName") == null ? "" : s.Element("DonorName").Value,
                                       AcademicYear =
                            s.Element("AcademicYear") == null ? "" : s.Element("AcademicYear").Value,
                                       NumberOfAwards =
                            s.Element("NumberOfAwards") == null ? "" : s.Element("NumberOfAwards").Value,
                                       ApplicationStartDate =
                            s.Element("ApplicationStartDate") == null ? null : s.Element("ApplicationStartDate").Value,
                                       ApplicationDueDate =
                            s.Element("ApplicationDueDate") == null ? "" : s.Element("ApplicationDueDate").Value,
                                       MaximumAmount =
                            s.Element("MaximumAmount") == null ? "" : s.Element("MaximumAmount").Value,
                                       MissionStatement =
                            s.Element("MissionStatement") == null ? "" : s.Element("MissionStatement").Value,
                                       ScholarshipId =
                            s.Element("SBScholarshipID") == null ? "0" : s.Element("SBScholarshipID").Value
                                   };

            if (query.Count() > 0)
            {
                var scholarship = query.First();

                if (UserContext.CurrentSeeker != null)
                {
                    Response.Redirect(ResolveUrl("~/Seeker/Scholarships/Show.aspx?print=true&id=" + scholarship.ScholarshipId));
                }
                scholarshipNameLabel.Text = scholarship.Name;
                Page.Header.Title = "Scholarship details for " + scholarship.Name;
                providerNameLabel.Text = scholarship.Provider;
                if (string.IsNullOrEmpty(scholarship.Donor))
                {
                    donorPanel.Visible = false;
                }
                else
                {
                    donorNameLabel.Text = scholarship.Donor;
                }
                academicYearLabel.Text = scholarship.AcademicYear;
                numberOfAwardsLabel.Text = scholarship.NumberOfAwards;
                applicationStartDateLabel.Text = DateTime.Parse(scholarship.ApplicationStartDate).ToShortDateString();
                applicationDueDateLabel.Text = DateTime.Parse(scholarship.ApplicationDueDate).ToShortDateString();
                scholarshipAmountLabel.Text = Decimal.Parse(scholarship.MaximumAmount).ToString("c");
                missionStatementLabel.Text = scholarship.MissionStatement;
            }
        }
    }
}