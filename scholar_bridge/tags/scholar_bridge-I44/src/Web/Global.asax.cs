﻿using System;
using System.Configuration;
using System.Web;
using Common.Logging;
using ScholarBridge.Web.Exceptions;

namespace ScholarBridge.Web
{
    public class Global : HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Global));

    	public const string ACTIVE_USER_COUNT_APP_KEY = "ActiveUsers";
    	public int ActiveUsers
    	{
    		get
    		{
				if (Application[ACTIVE_USER_COUNT_APP_KEY] == null)
				{
					Application.Lock();
					Application[ACTIVE_USER_COUNT_APP_KEY] = 0;
					Application.UnLock();
				}
				return (int)Application[ACTIVE_USER_COUNT_APP_KEY];
    		}
			protected set
			{
				Application.Lock();
				Application[ACTIVE_USER_COUNT_APP_KEY] = value;
				Application.UnLock();
			}
    	}

        protected void Application_Start(object sender, EventArgs e)
        {
            log.Info("**** Application starting ****");
        	ActiveUsers = 0;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        	ActiveUsers += 1;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
			// Check the web.config to see if the site is in maintenance mode
			if (ConfigurationManager.AppSettings["MaintenanceMode"] == "true")
			{
				HttpContext.Current.RewritePath("~/MaintenanceMode.aspx");
			}
		}

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Context.Error.GetBaseException();
            if (ex is NoUserIsInContextException || ex is NoProviderIsInContextException || ex is NoIntermediaryIsInContextException)
            {
                log.Warn("User context not setup properly", ex);
                Response.Redirect("~/login.aspx?ReturnUrl=" + Server.UrlEncode(Request.Url.PathAndQuery));
                Context.ClearError();
            }
            else if (ex.GetBaseException() is System.Web.HttpRequestValidationException)
            {
                
                Response.Redirect("~/RequestError.aspx");
                Context.ClearError();
            }
            else
            {
                Server.Transfer("~/Error.aspx");
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
			ActiveUsers -= 1;
		}

        protected void Application_End(object sender, EventArgs e)
        {
            log.Info("**** Application ending ****");
			ActiveUsers -= 1;
        }
    }
}