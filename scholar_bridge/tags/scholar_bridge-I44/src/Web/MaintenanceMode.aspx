﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceMode.aspx.cs"
    Inherits="ScholarBridge.Web.MaintenanceMode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>theWashBoard.org Maintenance Mode</title>
    <meta name="FORMAT" content="text/html" />
    <meta name="CHARSET" content="ISO-8859-1" />
    <meta name="DOCUMENTLANGUAGECODE" content="en" />
    <meta name="DOCUMENTCOUNTRYCODE" content="us" />
    <meta name="DC.LANGUAGE" scheme="rfc1766" content="en-us" />
    <meta name="COPYRIGHT" content="Copyright 2010 by Washington Scholarship Coalition" />
    <meta name="SECURITY" content="Public" />
    <meta name="ROBOTS" content="index,follow" />
    <meta name="GOOGLEBOT" content="index,follow" />
    <meta name="Description" content="Log in to theWashBoard.org" />
    <meta name="Keywords" content="washington, scholarship, matches, students, college, high school, school" />
    <meta name="Author" content="theWashBoard.org" />
    <!-- Base keywords here-->
    <link href="/styles/WSCStyles.CSS" rel="stylesheet" type="text/css" media="All" />
    <link href="/styles/global.css" rel="stylesheet" type="text/css" media="All" />
</head>
<body>
    <!--Page wrapper starts here-->
    <div id="EntirePageWrapper">
        <div id="HeaderWrapper">
            <img alt="" src="/images/LogoMainHomepage.gif" width="919px" height="151px" border="0"
                usemap="#Map" /><map id="map" name="Map"><area shape="rect" coords="37,9,236,133"
                    href="/" alt="theWashBoard.org" /></map><img alt="" src="/images/PicBottomOverAllHomepage.gif"
                        width="919px" height="497px" />
            <div class="LoginContainer">
                <div class="Horizontal">
                    <table id="Table1" cellspacing="0" cellpadding="10" border="0" style="border-collapse: collapse;">
                        <tr>
                            <td>
                                <div style="width: 350px; height: 110px; background-color: White; color: Red; text-align: center;
                                    font-size: larger; font-weight: bold;">
                                    <p>
                                        We're Sorry!</p>
                                    <p>
                                        theWashBoard.org is briefly unavailable due to system maintenance. Please check
                                        back in 15 minutes.</p>
                                    <p>
                                        Thank You.</p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!--Login Text ends here-->
        <div style='clear: both;'>
        </div>
        <!--This is the outer most wrapper 01 starts here-->
        <div id="ContentWrapper01">
            <div id="LeftPageShadow">
                <img alt="" src="/images/LeftContentShadow.gif" /></div>
            <div id="RightPageShadow">
                <img alt="" src="/images/RightContentShadow.gif" /></div>
            <!--This is content wrapper 02 starts here-->
            <div id="ContentWrapper02">
                <div style='clear: both;'>
                </div>
                <!--  <HR> -->
                <div style='clear: both;'>
                </div>
                <!--The Three column starts here-->
                <div id="BoxWrapper">
                    <div id="LeftBottomBox">
                        <img alt="" src="/images/HomepageBottomBox01_ForSeekers.gif" width="262px" height="41px" />
                        <img alt="" src="/images/OverallHomepageBottomBox01.gif" width="237px" height="86px" />
                        <p style="width: 237px">
                            Create a profile and let us do the rest. We will match you with scholarships you
                            are most likely to qualify for and applying online is easy.
                            <br />
                            <br />
                        </p>
                        <p class="IcoBoxArrow">
                            <img alt="Seeker Registeration" src="/images/Ico_GreenArrow.gif" width="14px" height="14px" />&nbsp;Seeker
                            Registration</p>
                    </div>
                    <div id="CenterBottomBox" style="font-size: 14px;">
                        <img alt="News and Announcements" src="/images/news_announcements.png" />
                        <p style="width: 237px; margin-top: .5em;">
                        </p>
                        <p style="width: 237px; margin-top: 1.25em; text-align: center;">
                            <span style="color: Red; font-size: larger; font-weight: bold;">System maintenance is
                                in progress.</span></p>
                    </div>
                    <div id="RightBottomBox">
                        <img alt="" src="/images/HomepageBottomBox03_ForProviders.gif" width="262px" height="41px" />
                        <img alt="" src="/images/OverallHomepageBottomBox03.gif" width="237px" height="86px" />
                        <p style="width: 237px">
                            Post your scholarship to reach a larger group of students. We make reviewing and
                            evaluating applications easy. Find the next recipients of your scholarship.</p>
                        <p class="IcoBoxArrow">
                            <img alt="" src="/images/Ico_GreenArrow.gif" width="14px" height="14px" />&nbsp;Provider
                            Registration</p>
                    </div>
                </div>
                <br />
                <div style='clear: both;'>
                </div>
                <div id="Footer">
                    <div style="float: right;">
                        <a href="http://www.facebook.com/pages/theWashBoardorg/208349032424">
                            <img src="/images/FaceBook_32x32.png" /></a> <a style="margin-left: 10px;" href="http://twitter.com/thewashboardorg">
                                <img src="/images/Twitter_32x32.png" /></a>
                    </div>
                    &nbsp;<div style="padding-top: 5px; clear: left;">
                        &copy; 2010 Washington Scholarship Coalition</div>
                </div>
            </div>
            <!--This is content wrapper 02 ends here-->
        </div>
        <!--This is the outer most wrapper 01 ends here-->
    </div>
    <!--Page wrapper ends here-->
</body>
</html>
