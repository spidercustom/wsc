﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipList : UserControl
    {
        private IList<Scholarship> _Scholarships;
        public string EditActionLink { get; set; }
        public string ViewActionLink { get; set; }
        private const string DEFAULT_SORTEXPRESSION = "Status";

        private const string DESCENDING_SORTDIRECTION = "DESC";
        private const string ASCENDING_SORTDIRECTION = "ASC";
        public IList<Scholarship> Scholarships
        {
            get { return _Scholarships; }
            set { _Scholarships = value;
               
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lstScholarships.Sort(DEFAULT_SORTEXPRESSION, SortDirection.Descending);
            }

        }
        public string CurrentSortExpression
        {
            get
            {
                if (ViewState["CurrentSortExpression"] == null)
                {
                    ViewState["CurrentSortExpression"] = DEFAULT_SORTEXPRESSION;
                }
                return (string)ViewState["CurrentSortExpression"];
            }
            set
            {
                ViewState["CurrentSortExpression"] = value;
            }

        }
        public string CurrentSortDirection
        {
            get
            {
                if (ViewState["CurrentSortDirection"] == null)
                {
                    ViewState["CurrentSortDirection"] = DESCENDING_SORTDIRECTION;
                }
                return (string)ViewState["CurrentSortDirection"];
            }
            set
            {
                ViewState["CurrentSortDirection"] = value;
            }

        }

        public void SetCurrentSortDirection()
        {
            CurrentSortDirection = CurrentSortDirection == ASCENDING_SORTDIRECTION
                                       ? DESCENDING_SORTDIRECTION
                                       : ASCENDING_SORTDIRECTION;

        }
        protected void Bind()
        {
            if (!(Scholarships == null))
            {
                lstScholarships.DataSource = Scholarships;
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship)((ListViewDataItem)e.Item).DataItem);
                var literal=(PlaceHolder)e.Item.FindControl("literalApplicants");
               
                if (scholarship.IsActivated())
                {
                    var link = new HyperLink();
                    link.Text = scholarship.ApplicantsCount.ToString();
                    link.Visible = true;
                    if (scholarship.ApplicantsCount > 0)
                    {
                        link.NavigateUrl = EditActionLink + "?resumefrom=6&id=" + scholarship.Id.ToString();
                        literal.Controls.Add(link);
                    }
                    else
                    {
                        literal.Controls.Add(new Label() { Text = scholarship.ApplicantsCount.ToString() }); 
                    }


                } else
                {
                    literal.Controls.Add(new Label() { Text = scholarship.ApplicantsCount.ToString() }); 
                }
            }
        }

        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }

        protected void pager_PreRender(object sender, EventArgs e)
        {

            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }

        // XXX: Is this used?
        protected void createScholarshipLnk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Provider/BuildScholarship");
        }

        protected void lstScholarships_Sorting(object sender, ListViewSortEventArgs eventArgs)
        {
            if (CurrentSortExpression == eventArgs.SortExpression)
            {
                SetCurrentSortDirection();
            }
            switch (eventArgs.SortExpression)
            {
                case "Status":
                    Scholarships = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Scholarships orderby l.Stage ascending select l).ToList()
                        : (from l in Scholarships orderby l.Stage descending select l).ToList();

                    break;
                case "Name":
                    Scholarships = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Scholarships orderby l.Name ascending select l).ToList()
                        : (from l in Scholarships orderby l.Name descending select l).ToList();

                    break;
                case "AcademicYear":
                    Scholarships = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Scholarships orderby l.AcademicYear.YearStart ascending select l).ToList()
                        : (from l in Scholarships orderby l.AcademicYear.YearStart descending select l).ToList();

                    break;
                case "ApplicationStartDate":
                    Scholarships = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Scholarships orderby l.ApplicationStartDate ascending select l).ToList()
                        : (from l in Scholarships orderby l.ApplicationStartDate descending select l).ToList();

                    break;
                case "ApplicationDueDate":
                    Scholarships = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Scholarships orderby l.ApplicationDueDate ascending select l).ToList()
                        : (from l in Scholarships orderby l.ApplicationDueDate descending select l).ToList();

                    break;
                case "NumberofApplicants":
                    Scholarships = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Scholarships orderby l.ApplicantsCount ascending select l).ToList()
                        : (from l in Scholarships orderby l.ApplicantsCount descending select l).ToList();

                    break;

            }
            CurrentSortExpression = eventArgs.SortExpression;
            Bind();
        }

    }
}