﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class SentMessageList : UserControl
    {
        public IUserContext UserContext { get; set; }
        public string LinkTo { get; set; }
        private const string DEFAULT_SORTEXPRESSION = "Date";

        public IList<Domain.Messaging.SentMessage> Messages { get; set; }
        private const string DESCENDING_SORTDIRECTION = "DESC";
        private const string ASCENDING_SORTDIRECTION = "ASC";
        public string CurrentSortExpression
        {
            get
            {
                if (ViewState["CurrentSortExpression"] == null)
                {
                    ViewState["CurrentSortExpression"] = DEFAULT_SORTEXPRESSION;
                }
                return (string)ViewState["CurrentSortExpression"];
            }
            set
            {
                ViewState["CurrentSortExpression"] = value;
            }

        }
        public string CurrentSortDirection
        {
            get
            {
                if (ViewState["CurrentSortDirection"] == null)
                {
                    ViewState["CurrentSortDirection"] = ASCENDING_SORTDIRECTION;
                }
                return (string)ViewState["CurrentSortDirection"];
            }
            set
            {
                ViewState["CurrentSortDirection"] = value;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            if (!IsPostBack)
                SortToDefault();
        }

        private void Bind()
        {
            if (!(Messages == null))
            {
                messageList.DataSource = Messages;
                messageList.DataBind();
            }

        }
        private void SortToDefault()
        {
            CurrentSortExpression = DEFAULT_SORTEXPRESSION;
            CurrentSortDirection = DESCENDING_SORTDIRECTION;
            PreserveSorting(DEFAULT_SORTEXPRESSION);
        }
         
        protected void messageList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }
        protected void pager_PreRender(object sender, EventArgs e)
        {

            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }

        protected void messageList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var message = (Domain.Messaging.SentMessage) ((ListViewDataItem) e.Item).DataItem;
                var date = (Label)e.Item.FindControl("lblDate");
                date.Text = message.Date.ToLocalTime().ToShortDateString();

                var link = (LinkButton)e.Item.FindControl("linkToMessage");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
                + "?sent=true&popup=true&id=" + (message.Id.ToString());
                link.Attributes.Add("onclick", String.Format("messageView('{0}'); return false;", url));
     
                link.Attributes.Add("class", "GreenLink");
            }
        }

        public void SetCurrentSortDirection()
        {
            CurrentSortDirection = CurrentSortDirection == ASCENDING_SORTDIRECTION
                                       ? DESCENDING_SORTDIRECTION
                                       : ASCENDING_SORTDIRECTION;

        }

        protected void messageList_Sorting(object sender, ListViewSortEventArgs eventArgs)
        {
            if (CurrentSortExpression == eventArgs.SortExpression)
                SetCurrentSortDirection();
            else
                CurrentSortDirection = ASCENDING_SORTDIRECTION;
            PreserveSorting(eventArgs.SortExpression);
        }

        public void PreserveSorting(string sortExpression)
        {
            if (String.IsNullOrEmpty(sortExpression))
                sortExpression = CurrentSortExpression;
            switch (sortExpression)
            {
                case "Subject":
                    Messages = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Messages orderby l.Subject ascending select l).ToList()
                        : (from l in Messages orderby l.Subject descending select l).ToList();

                    break;
               
                 case "To":
                    Messages = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (Messages.OrderBy(p => p.To.User == null ? "" : p.To.User.Name.NameFirstLast, true).ToList())
                        :
                    (Messages.OrderBy(p => p.To.User == null ? "" : p.To.User.Name.NameFirstLast, false).ToList());

                    break;
                case "From":
                    Messages = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (Messages.OrderBy(p => p.From.User == null ? "" : p.From.User.Name.NameFirstLast, true).ToList())
                        :
                    (Messages.OrderBy(p => p.From.User == null ? "" : p.From.User.Name.NameFirstLast, false).ToList());

                    break;

                case "Date":
                    Messages = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Messages orderby l.Date ascending select l).ToList()
                        : (from l in Messages orderby l.Date descending select l).ToList();

                    break;

            }
            CurrentSortExpression = sortExpression;
            Bind();
        }
    }
}