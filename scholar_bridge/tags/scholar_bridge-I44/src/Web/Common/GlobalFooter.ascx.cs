﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    public partial class GlobalFooter : UserControl
    {

        private LinkGenerator linkGenerator;
        protected LinkGenerator LinkGenerator
        {
            get
            {
                if (linkGenerator == null)
                    linkGenerator = new LinkGenerator();
                return linkGenerator;
            }
        }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Page.User.Identity.IsAuthenticated)
				loginLink.Visible = false;
			else
			{
				loginLink.HRef = LinkGenerator.GetFullLink("/default.aspx");
			}
		}
    }
}
