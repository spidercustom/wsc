﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipList.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipList" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<asp:ListView ID="lstScholarships" runat="server" 
    onpagepropertieschanging="lstScholarships_PagePropertiesChanging"
     OnItemDataBound="lstScholarships_ItemDataBound" OnSorting="lstScholarships_Sorting">
    <LayoutTemplate>
    <table class="sortableListView" cellpadding="0" cellspacing="0" width="90%">
            <thead>
            <tr>
                <th style="width:60px"><asp:LinkButton ID="linkButtonSortOnStatus" runat="server" CommandName="Sort" CommandArgument="Status" Text="Status" /></th>
                <th ><asp:LinkButton ID="linkButtonSortOnName" runat="server" CommandName="Sort" CommandArgument="Name" Text="Name" /></th>
                <th><asp:LinkButton ID="linkButtonSortOnAcademicYear" runat="server" CommandName="Sort" CommandArgument="AcademicYear" Text="Academic Year" /></th>
                <th><asp:LinkButton ID="linkButtonSortOnApplicationStartDate" runat="server" CommandName="Sort" CommandArgument="ApplicationStartDate" Text="Application Start Date" /></th>
                <th><asp:LinkButton ID="linkButtonSortOnApplicationDueDate" runat="server" CommandName="Sort" CommandArgument="ApplicationDueDate" Text="Application Due Date" /></th>
                <th><asp:LinkButton ID="linkButtonSortOnNumberofApplicants" runat="server" CommandName="Sort" CommandArgument="NumberofApplicants" Text="Number of Applicants" /></th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><%# ((ScholarshipStage)DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName()%></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server" NavigateUrl= <%#EditActionLink  +"?id="+ DataBinder.Eval(Container.DataItem, "Id") %>   ><%# DataBinder.Eval(Container.DataItem, "Name")%></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
         <td><asp:PlaceHolder id="literalApplicants"  runat="server" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><%# ((ScholarshipStage)DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName()%></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server" NavigateUrl= <%#EditActionLink  +"?id="+ DataBinder.Eval(Container.DataItem, "Id") %>   ><%# DataBinder.Eval(Container.DataItem, "Name")%></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
        <td><asp:PlaceHolder id="literalApplicants"  runat="server" /></td>
    
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager" PagedControlID="lstScholarships" PageSize="20"
        OnPreRender="pager_PreRender">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
</div>
