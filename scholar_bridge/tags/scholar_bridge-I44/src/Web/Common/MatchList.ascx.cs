﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using System.Collections.Generic;
using Spring.Context.Support;
 

namespace ScholarBridge.Web.Common
{
    public delegate void MatchIconHandler(Match match, ImageButton iconButton);

    public delegate void MatchActionButtonHandler(Match match, Button button);

    public partial class MatchList : UserControl
    {
       
        public string LinkTo { get; set; }
        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }

        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }
        public ListView List
        {
            get { return matchList; }
        }

        public MatchIconHandler IconConfigurator { get; set; }
        public MatchActionButtonHandler ActionButtonConfigurator { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            matchList.ItemCommand += matchList_ItemCommand;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MatchDataSource.SelectMethod = SelectMethod;
        }
        public string SelectMethod { get; set; }

        private void matchList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            Console.WriteLine(e.CommandArgument);
        }

        public IList<Match> GetMatchesForSeekerWithoutApplications(string sortExpression)
        {
            var matches = MatchService.GetMatchesForSeekerWithoutApplications(UserContext.CurrentSeeker);
            return SortMatches(sortExpression, matches);
        }

        public IList<Match> GetSavedButNotAppliedMatches(string sortExpression)
        {
            var matches = MatchService.GetSavedButNotAppliedMatches(UserContext.CurrentSeeker);
            return SortMatches(sortExpression, matches);
        }
        public IList<Match> SortMatches(string sortExpression,IList<Match> matches)
        {
            switch (sortExpression)
            {
                case "ScholarshipName":
                case "ScholarshipName ASC":
                    matches = (from m in matches orderby m.Scholarship.Name ascending select m).ToList();

                    break;

                case "ScholarshipName DESC":
                    matches = (from m in matches orderby m.Scholarship.Name descending select m).ToList();

                    break;

                case "Rank":
                case "Rank ASC":
                    matches = (from m in matches orderby m.Rank ascending select m).ToList();
                    break;

                case "Rank DESC":
                    matches = (from m in matches orderby m.Rank descending select m).ToList();
                    break;

                case "EligibilityCriteria":
                case "EligibilityCriteria ASC":
                    matches = (from m in matches orderby m.MinumumCriteriaString ascending select m).ToList();
                    break;

                case "EligibilityCriteria DESC":
                    matches = (from m in matches orderby m.MinumumCriteriaString descending select m).ToList();
                    break;

                case "PreferredCriteria":
                case "PreferredCriteria ASC":
                    matches = (from m in matches orderby m.PreferredCriteriaString ascending select m).ToList();
                    break;

                case "PreferredCriteria DESC":
                    matches = (from m in matches orderby m.PreferredCriteriaString descending select m).ToList();
                    break;
                case "ApplicationDueDate":
                case "ApplicationDueDate ASC":
                    matches = (from m in matches orderby m.Scholarship.ApplicationDueDate ascending select m).ToList();
                    break;

                case "ApplicationDueDate DESC":
                    matches = (from m in matches orderby m.Scholarship.ApplicationDueDate descending select m).ToList();
                    break;

                case "NumberOfAwards":
                case "NumberOfAwards ASC":
                    matches = (from m in matches orderby m.Scholarship.FundingParameters.MaximumNumberOfAwards ascending select m).ToList();
                    break;

                case "NumberOfAwards DESC":
                    matches = (from m in matches orderby m.Scholarship.FundingParameters.MaximumNumberOfAwards descending select m).ToList();
                    break;

                case "ScholarshipAmount":
                case "ScholarshipAmount ASC":
                    matches = (from m in matches orderby m.Scholarship.AmountRange ascending select m).ToList();
                    break;

                case "ScholarshipAmount DESC":
                    matches = (from m in matches orderby m.Scholarship.AmountRange descending select m).ToList();
                    break;

                case "Action":
                case "Action ASC":
                    matches = (from m in matches orderby m.MatchApplicationStatusString ascending select m).ToList();
                    break;

                case "Action DESC":
                    matches = (from m in matches orderby m.MatchApplicationStatusString descending select m).ToList();
                    break;
            }
            return matches;
        }

        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match) ((ListViewDataItem) e.Item).DataItem);
                
                var link = (LinkButton) e.Item.FindControl("linktoScholarship");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) {Path = ResolveUrl(LinkTo)}.ToString()
                          + "?id=" + match.Scholarship.Id + "&print=true";
                link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
            }
        }
        
        protected void pager_PreRender(object sender, EventArgs e)
        {
            var pager = (DataPager) sender;
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }
    }
}