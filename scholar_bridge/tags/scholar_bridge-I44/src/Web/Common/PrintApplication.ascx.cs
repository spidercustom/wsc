﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Web.Common
{
    public partial class PrintApplication : UserControl
    {
        public Application  ApplicationToView{ get; set; }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            scholarshipName.Text = ApplicationToView.Scholarship.Name;

            NameRow.Visible = SetRow(NameRow, ApplicationToView.Seeker.Name.NameFirstLast);
            EmailRow.Visible = SetRow(EmailRow, ApplicationToView.Seeker.User.Email);
            
            SetRow(GenderRow, ApplicationToView.Gender.ToString());
            SetRow(PersonalStatementRow, ApplicationToView.PersonalStatement);
            SetRow(MyGiftRow, ApplicationToView.MyGift);
            SetRow(FiveSkillsRow, ApplicationToView.Words);
            SetRow(FiveWordsRow, ApplicationToView.Skills);

            if (ApplicationToView.Seeker.Address != null)
            {
                string address = ApplicationToView.Seeker.Address.ToString().Replace("\r\n", "<br/>\r\n");
                if (ApplicationToView.Seeker.Address.State.Abbreviation == State.WASHINGTON_STATE_ID)
                {
                    if (ApplicationToView.Seeker.County != null) CountyRow.Visible = SetRow(CountyRow, ApplicationToView.Seeker.County.Name);
                    SetRow(AddressRow, address);
                }
                else
                {
                    CountyRow.Visible = SetRow(CountyRow, ApplicationToView.Seeker.AddressCounty);
                    address = address.Replace(ApplicationToView.Seeker.Address.GetCity, ApplicationToView.Seeker.AddressCity);
                    SetRow(AddressRow, address);
                }
            }
            else
                AddressRow.Visible = false;
            if (ApplicationToView.Seeker.Phone != null)
                SetRow(MobileRow, ApplicationToView.Seeker.Phone.Number);
            else
                PhoneRow.Visible = false;

            if (ApplicationToView.Seeker.MobilePhone != null)
                SetRow(MobileRow, ApplicationToView.Seeker.MobilePhone.Number);
            else
                MobileRow.Visible = false;

            if (ApplicationToView.DateOfBirth.HasValue)
                SetRow(BirthdayRow, ApplicationToView.DateOfBirth.Value.ToString("MM/dd/yyyy"));
            else BirthdayRow.Visible = false;

            //Religion
            string religionList = "";
            if (ApplicationToView.Religions != null)
                religionList = GetCommaSeperated((from x in ApplicationToView.Religions select x.Name).ToArray());

            if (!String.IsNullOrEmpty(ApplicationToView.ReligionOther))
                religionList = (religionList == "") ? ApplicationToView.ReligionOther : religionList + ", " + ApplicationToView.ReligionOther;

            if (religionList != "")
                SetRow(ReligionRow, religionList);

            //Heritage
            string heritageList = "";
            if (ApplicationToView.Ethnicities != null)
                heritageList = GetCommaSeperated((from x in ApplicationToView.Ethnicities select x.Name).ToArray());
            
            if (!String.IsNullOrEmpty(ApplicationToView.ReligionOther))
                heritageList = (heritageList == "") ? ApplicationToView.EthnicityOther : heritageList + ", " + ApplicationToView.EthnicityOther;
            
            if (heritageList != "")
                SetRow(HeritageRow, heritageList);

            // Current School
            if (ApplicationToView.CurrentSchool != null) {
                SetRow(TypeofStudent, GetCommaSeperated(ApplicationToView.CurrentSchool.LastAttended.GetDisplayNames()));

                if (ApplicationToView.CurrentSchool.HighSchool != null)
                {
                    SetRow(CurrentHighSchool, ApplicationToView.CurrentSchool.HighSchool.Name);

                    if (ApplicationToView.CurrentSchool.HighSchool.District != null)
                        SetRow(CurrentSchoolDistrict, ApplicationToView.CurrentSchool.HighSchool.District.Name);
                }
            }
            
            // Academic Performance
            if (ApplicationToView.SeekerAcademics != null)
            {
                if (ApplicationToView.SeekerAcademics.GPA.HasValue)
                    SetRow(CurrentGPA, ApplicationToView.SeekerAcademics.GPA.Value.ToString("0.000"));

                if (ApplicationToView.SeekerAcademics.ClassRank != null)
                    SetRow(CurrentClassRank, ApplicationToView.SeekerAcademics.ClassRank.Name);

                if (ApplicationToView.SeekerAcademics.SATScore != null)
                {
                    if (ApplicationToView.SeekerAcademics.SATScore.Commulative.HasValue)
                        SetRow(SATCumulative, ApplicationToView.SeekerAcademics.SATScore.Commulative.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.SATScore.CriticalReading.HasValue)
                        SetRow(SATCriticalReading, ApplicationToView.SeekerAcademics.SATScore.CriticalReading.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.SATScore.Writing.HasValue)
                        SetRow(SATWriting, ApplicationToView.SeekerAcademics.SATScore.Writing.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.SATScore.Mathematics.HasValue)
                        SetRow(SATMathematics, ApplicationToView.SeekerAcademics.SATScore.Mathematics.Value.ToString());
                }

                if (ApplicationToView.SeekerAcademics.ACTScore != null)
                {
                    if (ApplicationToView.SeekerAcademics.ACTScore.Commulative.HasValue)
                        SetRow(ACTCumulative, ApplicationToView.SeekerAcademics.ACTScore.Commulative.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.ACTScore.Reading.HasValue)
                        SetRow(ACTReading, ApplicationToView.SeekerAcademics.ACTScore.Reading.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.ACTScore.English.HasValue)
                        SetRow(ACTEnglish, ApplicationToView.SeekerAcademics.ACTScore.English.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.ACTScore.Mathematics.HasValue)
                        SetRow(ACTMathematics, ApplicationToView.SeekerAcademics.ACTScore.Mathematics.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.ACTScore.Science.HasValue)
                        SetRow(ACTScience, ApplicationToView.SeekerAcademics.ACTScore.Science.Value.ToString());
                }

                if (ApplicationToView.SeekerAcademics.Honors)
                    SetRow(HonorsRow, ApplicationToView.SeekerAcademics.HonorsDetail);

                if (ApplicationToView.SeekerAcademics.APCredits)
                    SetRow(APCreditsRow, ApplicationToView.SeekerAcademics.APCreditsDetail);

                if (ApplicationToView.SeekerAcademics.IBCredits)
                    SetRow(IBCreditsRow, ApplicationToView.SeekerAcademics.IBCreditsDetail);

                //My Academic Info College
                if (ApplicationToView.SeekerAcademics.CollegesApplied != null)
                    SetRow(ConsideringSchoolTypesRow, GetCommaSeperated((from x in ApplicationToView.SeekerAcademics.CollegesApplied select x.Name).ToArray()));
                 
                SetRow(LengthofProgramsRow, GetCommaSeperated(ApplicationToView.SeekerAcademics.ProgramLengths.GetDisplayNames() ));
                SetRow(AcademicProgramsRow, GetCommaSeperated(ApplicationToView.SeekerAcademics.AcademicPrograms.GetDisplayNames()));
                SetRow(EnrollmentStatusRow, GetCommaSeperated(ApplicationToView.SeekerAcademics.SeekerStatuses.GetDisplayNames()));
            }

           
            if (ApplicationToView.FirstGeneration)
                SetRow(FirstGenerationRow, "Yes");
           
            // Type of College
            if (ApplicationToView.CurrentSchool != null)
            {
                 SetRow(TypeofCollegeStudentRow, GetCommaSeperated(ApplicationToView.CurrentSchool.TypeOfCollegeStudent.GetDisplayNames()));

                if (ApplicationToView.CurrentSchool.College != null)
                    SetRow(CurrentCollegeRow, ApplicationToView.CurrentSchool.College.Name);

                if (!string.IsNullOrEmpty(ApplicationToView.CurrentSchool.CollegeOther))
                    SetRow(CurrentCollegeRow, ApplicationToView.CurrentSchool.CollegeOther);
            }

            if (ApplicationToView.SeekerAcademics != null)
            {
                ArrayList collegeLocationPreferences = new ArrayList();
                if (ApplicationToView.SeekerAcademics.IsCollegeAppliedInWashington)
                    collegeLocationPreferences.Add("In Washington");
                if (ApplicationToView.SeekerAcademics.IsCollegeAppliedOutOfState )
                    collegeLocationPreferences.Add("Out of State");

                if (collegeLocationPreferences.Count > 0)
                    SetRow(CollegeLocationPreferenceRow, string.Join(", ", (string[])collegeLocationPreferences.ToArray(typeof(string))));

                if (ApplicationToView.SeekerAcademics.CollegesApplied != null)
                    SetRow(CollegesRow, GetCommaSeperated((from x in ApplicationToView.SeekerAcademics.CollegesApplied select x.Name).ToArray()));
             }

            //Activities and Interests
            string fieldsOfStudy = CombineArrayAndString((from x in ApplicationToView.AcademicAreas select x.Name).ToArray(), ApplicationToView.AcademicAreaOther);
            FieldsOfStudyRow.Visible = SetRow(FieldsOfStudyRow, fieldsOfStudy);
            string careers = CombineArrayAndString((from x in ApplicationToView.Careers select x.Name).ToArray(), ApplicationToView.CareerOther);
            CareersRow.Visible = SetRow(CareersRow, careers);

            //Organization and Company Affiliations
            string organizationAffiliation = CombineArrayAndString((from x in ApplicationToView.MatchOrganizations select x.Name).ToArray(), ApplicationToView.MatchOrganizationOther);
            OrganizationsRow.Visible = SetRow(OrganizationsRow, organizationAffiliation);
            string companyAffiliation = CombineArrayAndString((from x in ApplicationToView.Companies select x.Name).ToArray(), ApplicationToView.CompanyOther);
            CompaniesRow.Visible = SetRow(CompaniesRow, companyAffiliation);

            //Groups
            string hobbiesList = CombineArrayAndString((from x in ApplicationToView.Hobbies select x.Name).ToArray(), ApplicationToView.HobbyOther);
            HobbiesRow.Visible = SetRow(HobbiesRow, hobbiesList);

            string sportsList = CombineArrayAndString((from x in ApplicationToView.Sports select x.Name).ToArray(), ApplicationToView.SportOther);
            SportsRow.Visible = SetRow(SportsRow, sportsList);

            string clubsList = CombineArrayAndString((from x in ApplicationToView.Clubs select x.Name).ToArray(), ApplicationToView.ClubOther);
            ClubsRow.Visible = SetRow(ClubsRow, clubsList);
            
            //Work and Volunteering
            if (ApplicationToView.IsWorking)
                SetRow(WorkingRow, (string.IsNullOrEmpty(ApplicationToView.WorkHistory)?"Yes":"Yes. " + ApplicationToView.WorkHistory));
            else
                WorkingRow.Visible = false;

            if (ApplicationToView.IsService)
                SetRow(VolunteeringRow, (string.IsNullOrEmpty(ApplicationToView.ServiceHistory) ? "Yes" : "Yes. " + ApplicationToView.ServiceHistory));
            else
                VolunteeringRow.Visible = false;
            
            //Financial Need
            FinancialChallangeRow.Visible = SetRow(FinancialChallangeRow, ApplicationToView.MyChallenge);

            if (ApplicationToView.SupportedSituation != null && ApplicationToView.SupportedSituation.TypesOfSupport != null)
            {
                string typesOfSupport = CombineArrayAndString((from x in ApplicationToView.SupportedSituation.TypesOfSupport select x.Name).ToArray(), "");
                SupportTypesRow.Visible = SetRow(SupportTypesRow, typesOfSupport);
            }

            FAFSACompletedRow.Visible = SetRow(FAFSACompletedRow, ApplicationToView.HasFAFSACompleted.HasValue ? "Yes" : "No");
            FAFSAEFCRow.Visible = SetRow(FAFSAEFCRow, ApplicationToView.ExpectedFamilyContribution.HasValue ? "Yes" : "No");

            // Additional Requirements
            if (ApplicationToView.Scholarship.AdditionalRequirements != null)
            {
                if (ApplicationToView.Scholarship.AdditionalRequirements.Count>0)
                {
                    foreach (AdditionalRequirement item in ApplicationToView.Scholarship.AdditionalRequirements)
                    {
                        AddRowToTableSmallTextOnly(AddtionalRequirementsTable, item.Name);
                    }
                }
            }

            if (ApplicationToView.QuestionAnswers != null)
            {
                if (ApplicationToView.QuestionAnswers.Count>0)
                {
                    foreach (ApplicationQuestionAnswer item in ApplicationToView.QuestionAnswers)
                    {
                        AddRowToTable(QuestionsTable, item.Question.QuestionText, item.AnswerText);
                    }
                }
            } 

            if (ApplicationToView.Attachments != null)
            {
                if (ApplicationToView.Attachments.Count>0)
                {
                    applicationAttachments.Attachments = ApplicationToView.Attachments;
                    applicationAttachments.View = FileList.FileListView.List;
                }
            }
        }

        private string CombineArrayAndString(Array array, string text)
        {
            ArrayList tempList = new ArrayList();
            if (array != null)
                tempList.AddRange(array);

            if (text != null)
                tempList.Add(text);

            return string.Join(", ", (string[]) tempList.ToArray(typeof (string)));
        }

        private bool SetRow(HtmlGenericControl container, string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }
            var item = new HtmlGenericControl("DIV") {InnerHtml = text};
            container.Controls.Add(item);
            return true;
        }

        private void AddRowToTable(HtmlTable table, string caption, string text)
        {
            var row = new HtmlTableRow();
            var cell = new HtmlTableCell();
            var label = new Label {Text = caption, CssClass = "captionMedium"};
            cell.Controls.Add(label);
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            label = new Label {Text = text, CssClass = "smallText"};
            cell.Controls.Add(label);

            row.Cells.Add(cell);
            table.Rows.Add(row);
        }

        private void AddRowToTableSmallTextOnly(HtmlTable table, string text)
        {
            var row = new HtmlTableRow();
            var cell = new HtmlTableCell();
            var label = new Label {Text = text, CssClass = "smallText"};
            cell.Controls.Add(label);
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            label = new Label {Text = ""};
            cell.Controls.Add(label);

            row.Cells.Add(cell);
            table.Rows.Add(row);
        }

        private string GetCommaSeperated(string[] array)
        {
            return string.Join(", ", array);
        }
    }
}