﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintScholarship.ascx.cs"  Inherits="ScholarBridge.Web.Common.PrintScholarship" %>
<%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>

<h1><asp:Literal  ID="scholarshipName"  runat="server" /></h1>

<div style="clear:both; padding-top:20px;">This scholarship is offered by <b><asp:Literal ID="ContactName" runat="server" /></b>, the application <asp:Literal ID="ApplicationDueOn" runat="server" />. The application process <asp:Literal ID="ScheduleApplicationStart" runat="server" />.</div>
<h2>Scholarship Details</h2>
<div class="attribute"><div>Description:</div><div><asp:Literal ID="MissionStatement" runat="server" /></div></div>
<div class="attribute"><div>Academic Year:</div><div><asp:Literal ID="AcademicYear" runat="server" /></div></div>
<div class="attribute"><div>Application Due:</div><div><asp:Literal ID="ApplicationDueOn2" runat="server" /></div></div>
<div class="attribute"><div>Anticipated Award Date:</div><div><asp:Literal  ID="ScheduleApplicationAwardOn" runat="server" /></div></div>
<div class="attribute"><div>Award Amount: </div><div><asp:Literal ID="FundingAwardAmount" runat="server" /> (A total of <asp:Literal ID="FundingTotalFunds" runat="server" /> available).</div></div>
<div class="attribute"><div>Awards Available:</div> <div><asp:Literal ID="FundingNumberOfAwards" runat="server" /></div></div>
<div class="attribute" id="RenewDiv" runat="server"><div>Renewable:</div><div><asp:Literal ID="Renewable" runat="server" /></div></div>
<div class="attribute" id="ReapplyDiv" runat="server"><div>Reapply:</div><div><asp:Literal ID="Reapply" runat="server" /></div></div>
<div class="attribute"><div>Support Period:</div><div><asp:Literal ID="FundingSupportPeriod" runat="server" /></div></div>
    
<h2>Eligibility Requirements</h2>
<div runat="server" id="minimumEligibilityDiv">  
    <div class="attribute"><div>Types of Students:</div><div><asp:Literal ID="EligibilityTypeOfStudent" runat="server" /></div></div>
    <div class="attribute"><div>Situations Funded:</div><div><asp:Literal ID="EligibilitySupportedSituation" runat="server" /></div></div>
    <div class="attribute"><div>Colleges:</div><div><asp:Literal ID="EligibilityColleges" runat="server" /></div></div>
    <div class="attribute"><div>School Types:</div><div><asp:Literal ID="EligibilitySchoolType" runat="server" /></div></div>
    <div class="attribute"><div>Academic Programs:</div><div><asp:Literal ID="EligibilityAcademicPrograms" runat="server" /></div></div>
    <div class="attribute"><div>Enrollment Status:</div><div><asp:Literal ID="EligibilityEnrollmentStatus" runat="server" /></div></div>
    <div runat="server" id="MinimumTable" ></div>
</div>

<div id="ApplicationDetails" runat="server">
    <h2>Application Details</h2>
    <div class="attribute" runat="server" id="ApplyAtRow"><div>Apply at:</div><div><asp:Literal ID="ApplyAtUrl" runat="server" /></div></div>
    <div class="attribute" runat="server" id="FinancialNeedRow"><div>Financial Requirements:</div></div>
    <div class="attribute" runat="server" id="AdditionalRequirementsRow"><div>Include the Following:</div></div>
    <div class="attribute" runat="server" id="AdditionalQuestionsRow" ><div>Additional Questions:</div></div>       
    <div class="attribute" runat="server" id="FormsTable"><div>Additional Forms:</div>
        <div>
            <sb:FileList id="scholarshipAttachments" runat="server" />
        </div>
    </div>  

</div>

<div runat="server" id="preferenceDiv">  
    <h2>Scholarship Preferences</h2>
    <div runat="server" id="PreferenceTable" ></div>
</div> 

<h2>Contact Information</h2>
<div runat="server" id="scheduleDiv"> 
    <div class="attribute"><div>Name:</div><div><asp:Literal ID="ContactName2" runat="server" /></div></div>
    <div class="attribute" runat="server" id="DonorRow"><div>Donor:</div><div><asp:Literal ID="Donor" runat="server" /></div></div>
    <div class="attribute" runat="server" id="WebsiteRow"><div>Website: </div><div><asp:Literal ID="ContactWebsite" runat="server" /></div></div>
    <div class="attribute"><div>Address: </div><div><asp:Literal ID="ContactAddress" runat="server" /></div></div>
    <div class="attribute" runat="server" id="PhoneRow"><div>Phone Number: </div><div><asp:Literal ID="ContactPhone" runat="server" /></div></div>
</div> 



<div id="Footer">
    <p>TheWashBoard.org is a free web-based tool for Washington students seeking college scholarships. We make scholarship searching simple.</p> 
    <p>Create a profile and review your scholarship matches. Register Today at <a class="GreenLink" href="http://www.theWashBoard.org">theWashBoard.org</a></p>
    <p>Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</p>
</div>