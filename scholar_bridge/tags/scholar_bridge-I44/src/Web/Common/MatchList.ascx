﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>

<%@ Import Namespace="ScholarBridge.Domain.Auth"%>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<asp:ObjectDataSource ID="MatchDataSource" runat="server"
    SelectMethod="GetMatchesForSeekerWithoutApplications" TypeName="ScholarBridge.Web.Common.MatchList"    
      SortParameterName="sortExpression"    >
</asp:ObjectDataSource>
<asp:ListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    DataKeyNames="Id" DataSourceID="MatchDataSource"   >
    <LayoutTemplate>
     
    <table class="sortableListView" >
    
            <thead>
            <tr>
                <th></th>
              
                <th   id="th1"  style="width:250px;"> <asp:LinkButton ID="linkButtonSortOnScholarshipName" runat="server" CommandName="Sort" CommandArgument="ScholarshipName" Text="Scholarship Name" /></th>
                <th  id="th2"   onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Rank', 'Matches are ranked based on your profile and the number of minimum eligibility and preferred criteria you meet. Minimum requirements make up 2/3 of the rank, while preferences count for 1/3.', '')">
                <asp:LinkButton ID="linkButtonSortOnRank" runat="server" CommandName="Sort" CommandArgument="Rank" Text="Rank" /></th>
                <th   id="th3"   onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Eligibility Criteria', 'The number of minimum eligibility requirements that match your profile and are defined in the Scholarship.', '')">
                <asp:LinkButton ID="inkButtonSortOnEligibilityCriteria" runat="server" CommandName="Sort" CommandArgument="EligibilityCriteria" Text="Eligibility Criteria" /></th>
                <th  id="th4"   onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Preferred Criteria', 'The number of preferred criteria that match your profile and are defined in the scholarship.', '')">
                <asp:LinkButton ID="inkButtonSortOnPreferredCriteria" runat="server" CommandName="Sort" CommandArgument="PreferredCriteria" Text="Preferred Criteria" /></th>
                <th  id="th5"   onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Application Due Date', 'The date the scholarship application is due.', '')">
                <asp:LinkButton ID="inkButtonSortOnApplicationDueDate" runat="server" CommandName="Sort" CommandArgument="ApplicationDueDate" Text="Application Due Date" /></th>
                <th   id="th6"   onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Number of Awards', 'The anticipated number of scholarship awards available.', '')">
                <asp:LinkButton ID="lnkSortOnNumberOfAwards" runat="server" CommandName="Sort" CommandArgument="NumberOfAwards" Text="# of Awards" /></th>
                <th   id="th7"   onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Scholarship Amount', 'The anticipated amount of the scholarship award.', '')">
                <asp:LinkButton ID="lnkSortOnScholarshipAmount" runat="server" CommandName="Sort" CommandArgument="ScholarshipAmount" Text="Scholarship Amount" /></th>
                <th   id="th8"   onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Action', 'Scholarship Applications are saved on your My Applications tab. If a scholarship is closed, you can save it to your scholarships of interest and apply next year.', '')">
                <asp:LinkButton ID="nkSortOnAction" runat="server" CommandName="Sort" CommandArgument="Action" Text="Action" /></th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    <div class="pager">
    <asp:DataPager runat="server" ID="pager"   PageSize="20"  OnPreRender="pager_PreRender"  >
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
    </div> 
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:ImageButton id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png"/></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:##}%")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="ListButton" Width="70px" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:ImageButton  id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png" /></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:##}%")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="ListButton" Width="70px" /></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships saved to <b>My Scholarships of Interest.</b> Click <asp:Image ID="Image4" ImageUrl="~/images/star_empty.png" runat="server"/> to save.</p>
    </EmptyDataTemplate>
</asp:ListView>

