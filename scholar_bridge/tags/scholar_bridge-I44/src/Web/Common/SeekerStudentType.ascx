﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerStudentType.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerStudentType" %>

<script type="text/javascript">
$(document).ready(function() {
    $("input[name='ctl00$bodyContentPlaceHolder$AcademicInfo1$CurrentStudentGroupControl$studentGroup']").click(
        function() {
            var checkedvalue = $(this).val(); 
            $.each(["HighSchoolSeniorYear", "AdultReturningYear", "TransferYear"], function(i, name) {
                $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + name).attr("disabled","disabled");
            });
            $("#ctl00_bodyContentPlaceHolder_AcademicInfo1_CurrentStudentGroupControl_" + checkedvalue + "Year").removeAttr("disabled");
        });
    });
</script>

<style type="text/css">
    .radioSelect
    {
        width: 200px;
        text-align: left;
        height: 18px;
    }
    
    .fix-size-label-container > label
    {
        width:200px !important;
        display: inline-table;
        vertical-align:top;
        padding: 0px;
        margin: 5px 5px 10px 5px; 
    }
</style>

<div class="control-set fix-size-label-container">
    <asp:RadioButton runat="server" ID="MiddleSchool" GroupName="studentGroup"  /><label>Middle School</label>
    <br />
    <asp:RadioButton runat="server" ID="HighSchoolFreshMan" GroupName="studentGroup" /><label>High School Freshman</label>
    <br />
    <asp:RadioButton runat="server" ID="HighSchoolSophomore" GroupName="studentGroup"  /><label>High School Sophomore</label>
    <br />
    <asp:RadioButton runat="server" ID="HighSchoolJunior" GroupName="studentGroup" /><label>High School Junior</label>
    <br />
    <asp:RadioButton runat="server" ID="HighSchoolSenior" GroupName="studentGroup" /><label>High School Senior</label>
    <br />
    
    <asp:RadioButton runat="server" ID="HighSchoolGraduate" GroupName="studentGroup"/><label>High School Graduate</label>
    <br />
    
   
</div>
