﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterOrganization.ascx.cs"
    Inherits="ScholarBridge.Web.Common.RegisterOrganization" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
    <%@ Register Src="~/Common/CaptchaControl.ascx" tagname="CaptchaControl" tagprefix="sb" %>
    <%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<style type="text/css">
        .createpassword-label-size
        {
            width: 290px !important;
            font-style:italic !important;
            font-weight:normal !important;
        }
        
    </style>
<br />
    <div id="ValidationErrorPopup" title="" style="display: none">
     
        <asp:Image ID="ValidationErrorPopupimage" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/cooltipicon_errorbig.png"/> 
        Error on this page, please correct before leaving the page.
        
    </div>
    <h1><asp:Literal ID="pageTitle" runat="server" Text="Organization Regisration"/></h1>
<asp:MultiView runat="server" ID="CreateUserWizard" ActiveViewIndex="0">
    <asp:View ID="DataEntryView" runat="server">
    <div class="form-iceland-container">
    <div class="form-iceland">
        
        <p><span class="requiredAttributeIndicator">*</span> Indicates a required field.</p>
        <h2>Administrator Information</h2>
        <p class="GeneralBodyText">As your organization's administrator, you will be responsible for: 
        <ul>
            <li class="GeneralBodyText">Maintaining your organization's information.</li>
            <li class="GeneralBodyText">Adding and removing other users at your organization.</li>
            <li class="GeneralBodyText">Establishing relationships in the system with providers or intermediaries.</li>
        </ul>
        </p>
        
                    <label for="FirstName">
                        First Name:<span class="requiredAttributeIndicator">*</span></label>
                    <asp:TextBox ID="FirstName" runat="server" Columns="40" MaxLength="40"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="FirstNameRequired" runat="server"
                        ControlToValidate="FirstName" ErrorMessage="First Name is required." ToolTip="First Name must be entered."
                        EnableClientScript="true"></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="firstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName"
                        SourceTypeName="ScholarBridge.Domain.PersonName" />
                <br/>
                    <label for="MiddleName">
                        Middle Name:</label>
                    <asp:TextBox ID="MiddleName" runat="server" Columns="40" MaxLength="40"></asp:TextBox>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName"
                        SourceTypeName="ScholarBridge.Domain.PersonName" />
                <br/>
                    <label for="LastName">
                        Last Name:<span class="requiredAttributeIndicator">*</span></label>
                    <asp:TextBox ID="LastName" runat="server" Columns="40" MaxLength="40"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="LastNameRequired" runat="server"
                        ControlToValidate="LastName" ErrorMessage="Last Name is required." ToolTip="Last Name must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName"
                        SourceTypeName="ScholarBridge.Domain.PersonName" />
                <br/>
                    <label for="UserName">
                        Email Address:<span class="requiredAttributeIndicator">*</span></label>
                    <asp:TextBox ID="UserName" runat="server" Columns="40" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="EmailAddressRequired" runat="server"
                        ControlToValidate="UserName" ErrorMessage="Email address is required." ToolTip="Email address must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email"
                        SourceTypeName="ScholarBridge.Domain.Auth.User" />
                    <asp:CustomValidator ID="EmailCustomValidator" runat="server"></asp:CustomValidator>
                    <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
                <br/>
                    <label for="ConfirmEmail">
                        Confirm Email Address:<span class="requiredAttributeIndicator">*</span></label>
                  <asp:TextBox ID="ConfirmEmail" runat="server" Columns="40" MaxLength="50"></asp:TextBox>
                  
                    <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredConfirmEmailValidator"
                        runat="server" ControlToValidate="ConfirmEmail" ErrorMessage="Confirm Email is required."
                        ToolTip="Confirm Email is required."></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail"
                        ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address." />
                
                <br />
                    <label for="Password">
                        Create Password:<span class="requiredAttributeIndicator">*</span></label>
                   <label class="createpassword-label-size">Must be at least 7 characters with at least one special character or number.</label>
                <br />
                    <label  >&nbsp;</label>
                 <asp:TextBox ID="Password" onBlur="$('#COOLTIP').hide();" onFocus="CoolTip_Show('ctl00_bodyContentPlaceHolder_registerOrg_CoolTipInfo1_image', 'Information!', 'Your password must be at least 7 characters with at least one special character or number. Example: p@sword', '')" runat="server" TextMode="Password" Columns="40" MaxLength="20"></asp:TextBox>
                 <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password"
                        SourceTypeName="ScholarBridge.Domain.Auth.User" />
                    <asp:RequiredFieldValidator Display="Dynamic" ID="passwordRequired" runat="server"
                        ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required."></asp:RequiredFieldValidator>
                  <sb:CoolTipInfo ID="CoolTipInfo1" runat="Server" Content="Your password must be at least 7 characters with at least one special character or number. Example: p@ssword" />
                       
                  <br />
                  
                    <label for="ConfirmPassword">
                        Confirm Password:<span class="requiredAttributeIndicator">*</span></label>
                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" Columns="40"
                        MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="ConfirmPasswordRequired" runat="server"
                        ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required."
                        ToolTip="Confirm Password is required."></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                        ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."></asp:CompareValidator>
                <br />
                    <h2>Organization Information</h2>

                <br/>
                    <label for="Name">
                        Name:<span class="requiredAttributeIndicator">*</span></label>
                    <asp:TextBox ID="Name" runat="server" Columns="40" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="OrgNameRequired" runat="server"
                        ControlToValidate="Name" ErrorMessage="Organization name is required." ToolTip="Organization name must be selected."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="NameValidator" runat="server" ControlToValidate="Name" PropertyName="Name"
                        SourceTypeName="ScholarBridge.Domain.Organization" />
                <br/>
                    <label for="TaxId">
                        Tax Id (EIN):<span class="requiredAttributeIndicator">*</span></label>
                    <asp:TextBox ID="TaxId" runat="server" Columns="40" MaxLength="10" CssClass="ein"></asp:TextBox>
                    <sb:CoolTipInfo ID="CoolTipTaxId" runat="Server" Content="Your Tax ID (EIN) is used, in part, to verify that you are offering scholarships with charitable intent and are one of the following: a tax exempt entity under Section 501(c) of the Internal Revenue Code, are affiliated with a 501(c) entity, are an accredited higher education institution in the State of Washington that meets our criteria." />
               
                    <asp:RequiredFieldValidator Display="Dynamic" ID="TaxIDRequired" runat="server" ControlToValidate="TaxId"
                        ErrorMessage="Tax ID is required." ToolTip="Tax ID must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="TaxIdValidator" runat="server" ControlToValidate="TaxId" PropertyName="TaxId"
                        SourceTypeName="ScholarBridge.Domain.Organization" />
                <br/>
                    <label for="Website">
                        Website:</label>
                    <asp:TextBox ID="Website" runat="server" Columns="40" MaxLength="128"></asp:TextBox>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="WebsiteValidator" runat="server" ControlToValidate="Website" PropertyName="Website"
                        SourceTypeName="ScholarBridge.Domain.Organization" />
                    <asp:customvalidator ID="WebSiteCustomValidator" runat="server"></asp:customvalidator>
                <br/>
                    <label for="AddressStreet">Address Line 1:<span class="requiredAttributeIndicator">*</span></label>
                   <asp:TextBox ID="AddressStreet" runat="server" Columns="40" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="AddressStreetRequired" runat="server"
                        ControlToValidate="AddressStreet" ErrorMessage="Street Address is required."
                        ToolTip="Street address must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet"
                        PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                 <br/>
                    <label for="AddressStreet2">Address Line 2:</label>
                    <asp:TextBox ID="AddressStreet2" runat="server" Columns="40" MaxLength="50"></asp:TextBox>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2"
                        PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                <br/>
                    <label for="AddressCity">
                        City:<span class="requiredAttributeIndicator">*</span></label>
                    <asp:TextBox ID="AddressCity" runat="server" Columns="40" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="CityRequired" runat="server" ControlToValidate="AddressCity"
                        ErrorMessage="City is required." ToolTip="City must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City"
                        SourceTypeName="ScholarBridge.Domain.Contact.Address" />
                <br/>
                    <label for="AddressState">
                        State:<span class="requiredAttributeIndicator">*</span></label>
                    <asp:DropDownList ID="AddressState" runat="server"  Columns="40" >
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="StateRequired" runat="server" ControlToValidate="AddressState"
                        ErrorMessage="State is required." ToolTip="State must be selected."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State"
                        SourceTypeName="ScholarBridge.Domain.Contact.Address" />
  
                <br/>
                    <label for="AddressPostalCode">
                        Postal Code:<span class="requiredAttributeIndicator">*</span></label>
                   <asp:TextBox ID="AddressPostalCode" runat="server" Columns="12" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="PostalCodeRequired" runat="server"
                        ControlToValidate="AddressPostalCode" ErrorMessage="Postal Code is required."
                        ToolTip="Postal code must be entered."></asp:RequiredFieldValidator>
                    <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode"
                        PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />

            <br/>
            <!-- <h3>Phone Numbers</h3> -->
            <label for="Phone">Phone:<span class="requiredAttributeIndicator">*</span></label>
            <asp:TextBox ID="Phone" runat="server" Columns="12" MaxLength="12" CssClass="phone"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number"
                        SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
                    <asp:RequiredFieldValidator Display="Dynamic" ID="phoneRequired" runat="server" ControlToValidate="Phone"
                        ErrorMessage="Phone number is required." ToolTip="Phone number must be entered."></asp:RequiredFieldValidator>
            <br/>
            <label for="Fax">Fax:</label>
            <asp:TextBox ID="Fax" runat="server" Columns="12" MaxLength="12" CssClass="phone"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="FaxValidator" runat="server" ControlToValidate="Fax" PropertyName="Number"
                        SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
             <br/>
                    <label for="OtherPhone">Other Phone:</label>
            <asp:TextBox ID="OtherPhone" runat="server" Columns="12" MaxLength="12" CssClass="phone"></asp:TextBox>
            <elv:PropertyProxyValidator ValidationGroup="CreateUserWizard1" Display="Dynamic"
                        ID="OtherPhoneValidator" runat="server" ControlToValidate="OtherPhone" PropertyName="Number"
                        SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
            <br /><br /> 
           <p class="GeneralBodyText">By registering as a Provider or Intermediary with theWashBoard.org, you agree that you are offering scholarships with charitable intent and your scholarships will benefit either  residents of the State of Washington, or (ii) students attending an accredited Washington higher education institution.</p>
            <p class="GeneralBodyText">To see the full provider and intermediary Terms of Use, please see the Terms Applicable to Scholarship Intermediaries and Providers section of the <A class="GreenLink" href='<%= LinkGenerator.GetFullLink("/Terms.aspx") %>'>Terms of Use</A></p>                        

            <sbCommon:AnchorButton ID="RegisterButton" runat="server" Text="Next" OnClick="RegisterButton_Click" CausesValidation="true" />
            <br />
            <p class="GeneralBodyText">Please add noreply@theWashBoard.org to your safe senders list to avoid emails being identified as junk email.</p>
   
      </div>
    </div>
    </asp:View>
    <asp:View ID="CaptchaView" runat="server">
        <div    runat="server">
            
            <h2>Word Verification</h2>
            <p class="GeneralBodyText">By entering this code you help us prevent spam and fake registrations. This code can be typed completely in lower case.</p>
            
            <br /><sb:CaptchaControl ID="CaptchaControl1" runat="server" /><br /> 
            
            <br />
            <p class="GeneralBodyText">By registering as a Provider or Intermediary with theWashBoard.org, you agree that you are offering scholarships with charitable intent and your scholarships will benefit either  residents of the State of Washington, or (ii) students attending an accredited Washington higher education institution.</p>
            <p class="GeneralBodyText">To see the full provider and intermediary Terms of Use, please see the Terms Applicable to Scholarship Intermediaries and Providers section of the <A class="GreenLink" href='<%= LinkGenerator.GetFullLink("/Terms.aspx") %>'>Terms of Use</A></p>                        

            <sbCommon:AnchorButton ID="CompleteRegistration" runat="server" Text="Complete Registration" OnClick="CompleteRegistration_Click" CausesValidation="true" />
            <br />
             <br />
            <p class="GeneralBodyText">Please add noreply@theWashBoard.org to your safe senders list to avoid emails being identified as junk email.</p>
   
        </div>

         
    </asp:View>
    <asp:View ID="CompletionView" runat="server">
        <div id="emailSuccess" runat="server">
             <h2>Thanks for registering with us</h2>
            <p>An email has been sent to the email address you provided with instructions to complete the registration process. Please follow the instructions in the e-mail to enable your account.</p>
                
        </div>

        <p id="emailFail" runat="server" visible="false">
            There was a problem sending a confirmation email so no account was created. Please contact us for assistance.
        </p>
    </asp:View>
</asp:MultiView>
