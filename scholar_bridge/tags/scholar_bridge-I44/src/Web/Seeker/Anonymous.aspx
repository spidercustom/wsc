﻿<%@ Page Title="Seeker" Language="C#" AutoEventWireup="true" CodeBehind="Anonymous.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Anonymous" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>
<%@ Register Src="~/Common/Login.ascx" TagName="Login" TagPrefix="sb" %>
<%@ Register Src="~/Common/MainMenu.ascx" TagName="MainMenu" TagPrefix="sb" %>
<%@ Register Src="~/Common/ScholarshipSearchBox.ascx" TagName="ScholarshipSearchBox" TagPrefix="sb" %>
<%@ Register Src="~/Common/GlobalFooter.ascx" TagName="GlobalFooter" TagPrefix="sb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>theWashBoard.org Seeker Home</title>
    <meta name="FORMAT" content="text/html" />
    <meta name="CHARSET" content="ISO-8859-1" />
    <meta name="DOCUMENTLANGUAGECODE" content="en" />
    <meta name="DOCUMENTCOUNTRYCODE" content="us" />
    <meta name="DC.LANGUAGE" scheme="rfc1766" content="en-us" />
    <meta name="COPYRIGHT" content="Copyright (c) 2009 by Washington Scholarship Coalition" />
    <meta name="SECURITY" content="Public" />
    <meta name="ROBOTS" content="index,follow" />
    <meta name="GOOGLEBOT" content="index,follow" />
    <meta name="Description" content="theWashBoard.org Provider Home " />
    <meta name="Keywords" content="washington, scholarship, matches, students, college, high school, school" />
    <meta name="Author" content="theWashBoard.org" />

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">google.load("jquery", "1.3");</script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.7.1.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tablesorter.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.pager.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tinysort.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskedinput.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.cookie.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.watermark-2.0.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/superfish.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/site.js") %>"></script>

    <script type="text/javascript">
        var LoginUrl = '<%= LinkGenerator.GetFullLink("default.aspx")  %>'
    </script>

    <link href="<%= ResolveUrl("~/styles/WSCStyles.CSS") %>" rel="stylesheet" type="text/css" media="All" />
    <link href="<%= ResolveUrl("~/styles/global.css") %>" rel="stylesheet" type="text/css" media="All"/> 


</head>
<body>
    <form id="aspnetForm" runat="server">
    <div class="main-wrapper">
        <div id="Header">
            <div id="Search"><sb:ScholarshipSearchBox ID="ScholarshipSearchBox" runat="server" /></div>
            <a href="<%= ResolveUrl("~/") %>"><img src="<%= ResolveUrl("~/images/LogoTheWashBoard.gif") %>"></a>
            <div id="Navigation"><sb:MainMenu runat="server" /></div>
        </div>
        <div id="PageImage">
            <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
        </div>
        <div id="PageContent-Wrapper">
            <div id="PageContent">
                <div id="LoginForm"><sb:Login ID="loginForm" runat="server" /></div>
                <img alt="Smarter Scholarship Matches" src="<%= ResolveUrl("~/images/PgTitle_SmarterScholarshipMatches.gif") %>" width="340px" height="54px" />
                <p class="hookline">theWashBoard.org is your free online resource for finding scholarships that match your needs. Create a profile and let us do the rest.</p>

                <br class="clear" />
                <div class="three-column">
                    <div>
                        <a href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">
                            <img alt="Register" src="<%= ResolveUrl("~/images/BottomBox01_StepOne.gif") %>"/>
                        </a>
                        <a href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">
                            <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox01.gif") %>"/>
                        </a>
                        <p>It's easy! Enter your email address and create a password and we will send you a confirmation email. Click the confirmation link and you are ready to log in!</p>
                        <a class="circle-icon-link" href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">Register</a>
                    </div>
                    <div>
                        <img alt="Create your profile" src="<%= ResolveUrl("~/images/BottomBox02_StepTwo.gif") %>"/>
                        <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox02.gif") %>"/>
                        <p>Create a profile and let us do the rest. We will match you with scholarships you are most likely to qualify for.</p>
                    </div>
                    <div>
                        <img alt="Review your matches" src="<%= ResolveUrl("~/images/BottomBox03_StepThree.gif") %>" />
                        <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox03.gif") %>" />
                        <p>Your scholarship matches are ranked based on the number of eligibility criteria that match your profile. Start reviewing scholarships you are most likely to qualify for.</p>
                    </div>
                </div>
                
                <br class="clear" />
               <sb:GlobalFooter ID="GlobalFooter" runat="server" />
            </div>
        </div>
    </div>
    </form>
    <sb:SuccessMessageLabel ID="SuccessMessageLabel" runat="server"></sb:SuccessMessageLabel>    
</body>
</html>
