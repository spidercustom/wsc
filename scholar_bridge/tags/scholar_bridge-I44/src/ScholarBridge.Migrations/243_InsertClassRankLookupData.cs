﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(243)]
    public class InsertClassRankLookupData : Migration
	{
        private const string tableName= "SBClassRankLUT";
		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
			string[] columns = GetInsertColumns();

            Database.Insert(tableName, columns, new[] { "Top 2%", "Top 2%", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Top 5%", "Top 5%", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Top 10%", "Top 10%", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Valedictorian", "Valedictorian", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Salutatorian", "Salutatorian", "0", adminId.ToString(), DateTime.Now.ToString() });
		}

		public override void Down()
		{
            Database.ExecuteNonQuery("DELETE FROM " + tableName);
			
		}

		private string[] GetInsertColumns()
		{
			Column[] columns = Database.GetColumns(tableName);
			return new string[]
			       	{
			       		columns[1].Name,
						columns[2].Name,
						"Deprecated",
                        "LastUpdateBy",
                        "LastUpdateDate"
					};
		}
	}
}
