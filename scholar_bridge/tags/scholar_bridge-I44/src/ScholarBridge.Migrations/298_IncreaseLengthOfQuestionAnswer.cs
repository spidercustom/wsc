using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(298)]
	public class IncreaseLengthOfQuestionAnswer : Migration
    {
		private const string TABLE_NAME = "SBApplicationQuestionAnswer";
		private const string ANSWER_TEXT = "AnswerText";

        public override void Up()
        {
			IncreaseFieldLengths();
		}

    	public override void Down()
    	{
			DecreaseFieldLengths();
		}

		private void IncreaseFieldLengths()
		{
			Database.ExecuteNonQuery("alter table " + TABLE_NAME +  " alter column " + ANSWER_TEXT + " nvarchar(max) null");
		}

		private void DecreaseFieldLengths()
    	{
			TruncateData();
			Database.ExecuteNonQuery("alter table " + TABLE_NAME +  " alter column " + ANSWER_TEXT + " nvarchar(200) null");
    	}

    	private void TruncateData()
		{
			Database.ExecuteNonQuery("Update " + TABLE_NAME + " set " + ANSWER_TEXT + " = SUBSTRING( " + ANSWER_TEXT + ", 1, 200) ");
		}
    }
}