﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(285)]
    public class AddDisplayContactToScholarship : Migration
    {
		private const string TABLE_NAME = "SBScholarship";
        private const string COLUMN_NAME = "DisplayContactFor";

        public override void Up()
        {
			Database.AddColumn(TABLE_NAME, COLUMN_NAME, DbType.String,50,ColumnProperty.Null);
            SetContactDisplayToProvider();
            Database.ChangeColumn(TABLE_NAME, new Column(COLUMN_NAME, DbType.String, 50, ColumnProperty.NotNull));
        }

        public override void Down()
        {
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
        }

		/// <summary>
        /// set all ContactDisplay to 'Provider' type for now.
		/// </summary>
        private void SetContactDisplayToProvider()
		{
			Database.ExecuteNonQuery(@"
update SBScholarship
set DisplayContactFor = 'Provider'
");
		}

    }
}