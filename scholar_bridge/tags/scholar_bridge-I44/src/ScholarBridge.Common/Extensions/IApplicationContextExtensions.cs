﻿using System;
using Spring.Context;

namespace ScholarBridge.Common.Extensions
{
    public static class IApplicationContextExtensions
    {
        public static T GetObject<T>(this IApplicationContext context, string key) where T : class
        {
            var retrivedObject = context.GetObject(key);
            if (null == retrivedObject)
                throw new ArgumentException(string.Format("Cannot retrieve object by key {0}", key));
            var result = retrivedObject as T;
            if (null == result)
                throw new ArgumentException(string.Format("Cannot cast object retrieved by key {0} to ILookupDAL", key));

            return result;
        }
    }
}