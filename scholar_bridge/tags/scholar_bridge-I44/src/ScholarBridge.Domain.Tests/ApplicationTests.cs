using Microsoft.Practices.EnterpriseLibrary.Validation;
using NUnit.Framework;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;
using ActScore=ScholarBridge.Domain.ScholarshipParts.ActScore;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class ApplicationTests
    {

        [Test]
        public void can_edit_if_not_submitted()
        {
            var a = new Application(ApplicationType.Internal);
            Assert.IsTrue(a.CanEdit());

            a.Stage = ApplicationStage.NotActivated;
            Assert.IsTrue(a.CanEdit());

            a.Stage = ApplicationStage.Submitted;
            Assert.IsFalse(a.CanEdit());
        }


		[Test]
		public void submit_sets_values()
		{
			var a = new Application(ApplicationType.Internal);
			a.Submit();
			Assert.AreEqual(ApplicationStage.Submitted, a.Stage);
			Assert.IsTrue(a.SubmittedDate.HasValue);
		}


		[Test]
		public void submission_validation_passes_on_valid_application()
		{
			var a = CreateValidApplication();
			ValidationResults results = a.ValidateSubmission();
			Assert.AreEqual(0, results.Count);
		}

		[Test]
		public void submission_validation_AcademicArea()
		{
			var a = CreateValidApplication();

			ScholarshipAttributeUsage<SeekerProfileAttribute> attributeUsage = new ScholarshipAttributeUsage<SeekerProfileAttribute>();
			attributeUsage.Attribute = SeekerProfileAttribute.AcademicArea;
			attributeUsage.UsageType = ScholarshipAttributeUsageType.Minimum;

			a.Scholarship.SeekerProfileCriteria.AcademicAreas.Add(new AcademicArea() { Name = "Art History" });
			a.Scholarship.SeekerProfileCriteria.Attributes.Add(attributeUsage);

			ValidationResults results = a.ValidateSubmission();

			Assert.AreEqual(1, results.Count);
			foreach (var result in results)
			{
				Assert.AreEqual("AcademicArea", result.Key);
			}
		}

		[Test]
		public void submission_validation_AcademicProgram()
		{
			var a = CreateValidApplication();

			ScholarshipAttributeUsage<SeekerProfileAttribute> attributeUsage = new ScholarshipAttributeUsage<SeekerProfileAttribute>();
			attributeUsage.Attribute = SeekerProfileAttribute.AcademicProgram;
			attributeUsage.UsageType = ScholarshipAttributeUsageType.Minimum;

			a.SeekerAcademics = new SeekerAcademics();
			a.Scholarship.SeekerProfileCriteria.AcademicPrograms = AcademicPrograms.Undergraduate;
			a.Scholarship.SeekerProfileCriteria.Attributes.Add(attributeUsage);

			ValidationResults results = a.ValidateSubmission();

			Assert.AreEqual(1, results.Count);
			foreach (var result in results)
			{
				Assert.AreEqual("AcademicProgram", result.Key);
			}
		}

		[Test]
		public void submission_validation_ACT()
		{
			var a = CreateValidApplication();

			ScholarshipAttributeUsage<SeekerProfileAttribute> attributeUsage = new ScholarshipAttributeUsage<SeekerProfileAttribute>();
			attributeUsage.Attribute = SeekerProfileAttribute.ACTEnglish;
			attributeUsage.UsageType = ScholarshipAttributeUsageType.Minimum;
			a.Scholarship.SeekerProfileCriteria.Attributes.Add(attributeUsage);

			a.SeekerAcademics = new SeekerAcademics();
			a.SeekerAcademics.ACTScore = new SeekerParts.ActScore();
			a.Scholarship.SeekerProfileCriteria.ACTScore = new ActScore() {English = new RangeCondition<int>() {Minimum = 20, Maximum = 36},
			                                                               Mathematics= new RangeCondition<int>(){Maximum = 36, Minimum = 25},
			                                                               Reading = new RangeCondition<int>(){Maximum = 36, Minimum = 20},
																		   Science = new RangeCondition<int>() { Maximum = 36, Minimum = 20 }
			};

			ValidationResults results = a.ValidateSubmission();

			Assert.AreEqual(1, results.Count);
			foreach (var result in results)
			{
				Assert.AreEqual("ACTEnglish", result.Key);
			}

			a.SeekerAcademics.ACTScore.English = 15;
			results = a.ValidateSubmission();

			Assert.AreEqual(1, results.Count);
			foreach (var result in results)
			{
				Assert.AreEqual("ACTEnglish", result.Key);
			}

			a.SeekerAcademics.ACTScore.English = 26;
			results = a.ValidateSubmission();

			Assert.AreEqual(0, results.Count);

		}

		private Application CreateValidApplication()
		{
			Application a = new Application(ApplicationType.Internal);
			a.Seeker = new Seeker();
			a.Seeker.User = new User(){Name = new PersonName(){FirstName="Tesla", LastName = "Tester", MiddleName=string.Empty}}; 
			a.Seeker.Address = new SeekerAddress
			            	{
			            		State = new State {Abbreviation = State.WASHINGTON_STATE_ID, Name = "Washington"},
								City = new City { Name = "Tacoma", State = new State() { Abbreviation = State.WASHINGTON_STATE_ID, Name = "Washington" } }
			            	};
			a.Seeker.Address.Street = "1234 A";
			a.Seeker.Address.PostalCode = "98210";
			a.Scholarship = ScholarshipTests.CreateValidScholarship();
			a.Scholarship.FundingProfile.SupportedSituation = new SupportedSituation();
			a.SupportedSituation = new SupportedSituation();
			a.SupportedSituation.TypesOfSupport.Add(new Support(){Name="Tuition"});
			a.Scholarship.FundingProfile.SupportedSituation = a.SupportedSituation;
			return a;
		}


    }
}