using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class SeekerDALTest : TestBase
    {
        public SeekerDAL SeekerDAL { get; set; }
        public UserDAL UserDAL { get; set; }
		public LookupDAL<Ethnicity> EthnicityDAL { get; set; }
		public LookupDAL<Religion> ReligionDAL { get; set; }
        public LookupDAL<AcademicArea> AcademicAreaDAL { get; set; }
        public LookupDAL<Career> CareerDAL { get; set; }
        public LookupDAL<CommunityService> CommunityServiceDAL { get; set; }
        public LookupDAL<SeekerMatchOrganization> SeekerMatchOrganizationDAL { get; set; }
        public LookupDAL<Company> CompanyDAL { get; set; }
        public LookupDAL<SeekerHobby>  SeekerHobbyDAL { get; set; }
        public LookupDAL<Sport> SportDAL { get; set; }
        public LookupDAL<Club> ClubDAL { get; set; }
		public StateDAL StateDAL { get; set; }

		private User user;
        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
        }

        [Test]
        public void can_create_seeker()
        {
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);

            Assert.IsNotNull(seeker.User);
            Assert.AreEqual("TestFirst TestLast", seeker.Name.ToString());
        }

		[Test]
		public void can_find_seeker_by_user()
		{
			InsertSeeker(SeekerDAL, user, StateDAL);

			var seeker2 = SeekerDAL.FindByUser(user);
			Assert.IsNotNull(seeker2);
		}

		[Test]
		public void can_count_registered_seekers()
		{
			int currCount = SeekerDAL.CountRegisteredSeekers();
			InsertSeeker(SeekerDAL, user, StateDAL);
			User user1 = UserDALTest.InsertUser(UserDAL, "foo@bar1.com");
			User user2 = UserDALTest.InsertUser(UserDAL, "foo@bar2.com");

			user1.IsActive = true;
			UserDAL.Update(user1);
			user2.IsActive = false;
			UserDAL.Update(user2);

			InsertSeeker(SeekerDAL, user1, StateDAL);
			InsertSeeker(SeekerDAL, user2, StateDAL);

			Assert.IsFalse(user.IsActive);

			Assert.AreEqual(currCount + 1, SeekerDAL.CountRegisteredSeekers());
		}

		[Test]
		public void can_count_seekers_with_profiles()
		{
			int currCount = SeekerDAL.CountSeekersWithActiveProfiles();
			User user1 = UserDALTest.InsertUser(UserDAL, "foo@bar1.com");
			User user2 = UserDALTest.InsertUser(UserDAL, "foo@bar2.com");

			Seeker seeker0 = InsertSeeker(SeekerDAL, user, StateDAL);
			Seeker seeker1 = InsertSeeker(SeekerDAL, user1, StateDAL);
			Seeker seeker2 = InsertSeeker(SeekerDAL, user2, StateDAL);

			seeker1.ProfileActivatedOn = seeker2.ProfileActivatedOn = DateTime.Now;

			Assert.IsNull(seeker0.ProfileActivatedOn);

			SeekerDAL.Update(seeker1);
			SeekerDAL.Update(seeker2);

			Assert.AreEqual(currCount + 2, SeekerDAL.CountSeekersWithActiveProfiles());
		}


		#region Seeker ethnicity tests
		[Test]
		public void can_associate_seeker_with_ethnicities()
		{
			Seeker seeker = GetSeekerWithEthnicities();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Ethnicities);
			Assert.AreEqual(2, found.Ethnicities.Count);
		}

		[Test]
		public void can_remove_ethnicities_from_seeker()
		{
			Seeker seeker = GetSeekerWithEthnicities();
			Assert.AreEqual(2, seeker.Ethnicities.Count);

			seeker.Ethnicities.RemoveAt(0);
			SeekerDAL.Update(seeker);

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Ethnicities);
			Assert.AreEqual(1, found.Ethnicities.Count);
		}
		#endregion

		#region Seeker religion tests
		[Test]
		public void can_associate_seeker_with_religions()
		{
			Seeker seeker = GetSeekerWithReligions();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Religions);
			Assert.AreEqual(2, found.Religions.Count);
		}

		[Test]
		public void can_remove_Religions_from_seeker()
		{
			Seeker seeker = GetSeekerWithReligions();
			Assert.AreEqual(2, seeker.Religions.Count);

			seeker.Religions.RemoveAt(0);
			SeekerDAL.Update(seeker);

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Religions);
			Assert.AreEqual(1, found.Religions.Count);
		}
		#endregion
        #region Seeker AcademicAreas tests
        [Test]
        public void can_associate_seeker_with_AcademicAreas()
        {
            Seeker seeker = GetSeekerWithAcademicAreas();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AcademicAreas);
            Assert.AreEqual(2, found.AcademicAreas.Count);
        }

        [Test]
        public void can_remove_AcademicAreas_from_seeker()
        {
            Seeker seeker = GetSeekerWithAcademicAreas();
            Assert.AreEqual(2, seeker.AcademicAreas.Count);

            seeker.AcademicAreas.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.AcademicAreas);
            Assert.AreEqual(1, found.AcademicAreas.Count);
        }
        #endregion
        #region Seeker Careers tests
        [Test]
        public void can_associate_seeker_with_Careers()
        {
            Seeker seeker = GetSeekerWithCareers();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Careers);
            Assert.AreEqual(2, found.Careers.Count);
        }

        [Test]
        public void can_remove_Careers_from_seeker()
        {
            Seeker seeker = GetSeekerWithCareers();
            Assert.AreEqual(2, seeker.Careers.Count);

            seeker.Careers.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Careers);
            Assert.AreEqual(1, found.Careers.Count);
        }
        #endregion
        #region Seeker CommunityServices tests
        [Test]
        public void can_associate_seeker_with_CommunityServices()
        {
            Seeker seeker = GetSeekerWithCommunityServices();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.CommunityServices);
            Assert.AreEqual(2, found.CommunityServices.Count);
        }

        [Test]
        public void can_remove_CommunityServices_from_seeker()
        {
            Seeker seeker = GetSeekerWithCommunityServices ();
            Assert.AreEqual(2, seeker.CommunityServices.Count);

            seeker.CommunityServices.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.CommunityServices);
            Assert.AreEqual(1, found.CommunityServices.Count);
        }
        #endregion
        #region Seeker SeekerMatchOrganizations tests
        [Test]
        public void can_associate_seeker_with_SeekerMatchOrganizations()
        {
            Seeker seeker = GetSeekerWithSeekerMatchOrganizations();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.MatchOrganizations);
            Assert.AreEqual(2, found.MatchOrganizations.Count);
        }

        [Test]
        public void can_remove_SeekerMatchOrganizations_from_seeker()
        {
            Seeker seeker = GetSeekerWithSeekerMatchOrganizations();
            Assert.AreEqual(2, seeker.MatchOrganizations.Count);

            seeker.MatchOrganizations.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.MatchOrganizations);
            Assert.AreEqual(1, found.MatchOrganizations.Count);
        }
        #endregion
        #region Seeker AffiliationTypes tests
        [Test]
        public void can_associate_seeker_with_Companies()
        {
            Seeker seeker = GetSeekerWithAffiliationTypes();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Companies);
            Assert.AreEqual(2, found.Companies.Count);
        }

        [Test]
        public void can_remove_Companies_from_seeker()
        {
            Seeker seeker = GetSeekerWithAffiliationTypes();
            Assert.AreEqual(2, seeker.Companies.Count);

            seeker.Companies.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Companies);
            Assert.AreEqual(1, found.Companies.Count);
        }
        #endregion
        #region Seeker Hobbies tests
        [Test]
        public void can_associate_seeker_with_Hobbies()
        {
            Seeker seeker = GetSeekerWithHobbies();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Hobbies);
            Assert.AreEqual(2, found.Hobbies.Count);
        }

        [Test]
        public void can_remove_Hobbies_from_seeker()
        {
            Seeker seeker = GetSeekerWithHobbies();
            Assert.AreEqual(2, seeker.Hobbies.Count);

            seeker.Hobbies.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Hobbies);
            Assert.AreEqual(1, found.Hobbies.Count);
        }

        #endregion

        #region Seeker Sports tests
        [Test]
        public void can_associate_seeker_with_Sports()
        {
            Seeker seeker = GetSeekerWithSports();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Sports);
            Assert.AreEqual(2, found.Sports.Count);
        }

        [Test]
        public void can_remove_Sports_from_seeker()
        {
            Seeker seeker = GetSeekerWithSports();
            Assert.AreEqual(2, seeker.Sports.Count);

            seeker.Sports.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Sports);
            Assert.AreEqual(1, found.Sports.Count);
        }
        #endregion

        #region Seeker Clubs tests
        [Test]
        public void can_associate_seeker_with_Clubs()
        {
            Seeker seeker = GetSeekerWithClubs();

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Clubs);
            Assert.AreEqual(2, found.Clubs.Count);
        }

        [Test]
        public void can_remove_Clubs_from_seeker()
        {
            Seeker seeker = GetSeekerWithClubs();
            Assert.AreEqual(2, seeker.Clubs.Count);

            seeker.Clubs.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Clubs);
            Assert.AreEqual(1, found.Clubs.Count);
        }
        #endregion
         

        #region Seeker Attachments tests
		[Test]
		public void can_associate_seeker_with_Attachments()
		{
			Seeker seeker = GetSeekerWithAttachments();

			var found = SeekerDAL.FindByUser(seeker.User);
			Assert.IsNotNull(found);
			Assert.IsNotNull(found.Attachments);
			Assert.AreEqual(2, found.Attachments.Count);
		}

		[Test]
        public void can_remove_Attachments_from_seeker()
        {
            Seeker seeker = GetSeekerWithAttachments();
            Assert.AreEqual(2, seeker.Attachments.Count);

            seeker.Attachments.RemoveAt(0);
            SeekerDAL.Update(seeker);

            var found = SeekerDAL.FindByUser(seeker.User);
            Assert.IsNotNull(found);
            Assert.IsNotNull(found.Attachments);
            Assert.AreEqual(1, found.Attachments.Count);
        }
        #endregion


		private Seeker GetSeekerWithEthnicities()
		{
			var s1 = EthnicityDAL.Insert(new Ethnicity { Name = "Martian", LastUpdate = new ActivityStamp(user) });
			var s2 = EthnicityDAL.Insert(new Ethnicity { Name = "Lunar", LastUpdate = new ActivityStamp(user) });
			var s3 = EthnicityDAL.Insert(new Ethnicity { Name = "Asteroidian", LastUpdate = new ActivityStamp(user) });

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.Ethnicities.Add(s1);
			seeker.Ethnicities.Add(s3);

			SeekerDAL.Update(seeker);
			return seeker;
		}

		private Seeker GetSeekerWithReligions()
		{
			var s1 = ReligionDAL.Insert(new Religion { Name = "Astrology", LastUpdate = new ActivityStamp(user) });
			var s2 = ReligionDAL.Insert(new Religion { Name = "Darwinism", LastUpdate = new ActivityStamp(user) });
			var s3 = ReligionDAL.Insert(new Religion { Name = "Athiesm", LastUpdate = new ActivityStamp(user) });

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.Religions.Add(s1);
			seeker.Religions.Add(s3);

			SeekerDAL.Update(seeker);
			return seeker;
		}

        private Seeker GetSeekerWithAcademicAreas()
        {
            var s1 = AcademicAreaDAL.Insert(new AcademicArea { Name = "Test Academic1", LastUpdate = new ActivityStamp(user) });
            var s2 = AcademicAreaDAL.Insert(new AcademicArea { Name = "Test Academic2", LastUpdate = new ActivityStamp(user) });
         
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.AcademicAreas.Add(s1);
            seeker.AcademicAreas.Add(s2);
           
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithCareers()
        {
            var s1 = CareerDAL.Insert(new Career { Name = "Test Career1", LastUpdate = new ActivityStamp(user) });
            var s2 = CareerDAL.Insert(new Career { Name = "Test Career2", LastUpdate = new ActivityStamp(user) });
          
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Careers.Add(s1);
            seeker.Careers.Add(s2);
         
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithCommunityServices()
        {
            var s1 =  CommunityServiceDAL.Insert(new  CommunityService { Name = "Test  CommunityService1", LastUpdate = new ActivityStamp(user) });
            var s2 =  CommunityServiceDAL.Insert(new  CommunityService { Name = "Test  CommunityService2", LastUpdate = new ActivityStamp(user) });
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker. CommunityServices.Add(s1);
            seeker. CommunityServices.Add(s2);
          
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithSeekerMatchOrganizations()
        {
            var s1 = SeekerMatchOrganizationDAL.Insert(new SeekerMatchOrganization { Name = "Test SeekerMatchOrganization1", LastUpdate = new ActivityStamp(user) });
            var s2 = SeekerMatchOrganizationDAL.Insert(new SeekerMatchOrganization { Name = "Test SeekerMatchOrganization2", LastUpdate = new ActivityStamp(user) });
         
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.MatchOrganizations.Add(s1);
            seeker.MatchOrganizations.Add(s2);
         
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithAffiliationTypes()
        {
            var s1 = CompanyDAL.Insert(new Company { Name = "Test Company1", LastUpdate = new ActivityStamp(user) });
            var s2 = CompanyDAL.Insert(new Company { Name = "Test Company2", LastUpdate = new ActivityStamp(user) });
      
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Companies.Add(s1);
            seeker.Companies.Add(s2);
        
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithHobbies()
        {
            var s1 = SeekerHobbyDAL.Insert(new SeekerHobby { Name = "Test SeekerHobby1", LastUpdate = new ActivityStamp(user) });
            var s2 = SeekerHobbyDAL.Insert(new SeekerHobby { Name = "Test SeekerHobby2", LastUpdate = new ActivityStamp(user) });
       
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Hobbies.Add(s1);
            seeker.Hobbies.Add(s2);
         
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithSports()
        {
            var s1 = SportDAL.Insert(new Sport { Name = "Test Sport1", LastUpdate = new ActivityStamp(user) });
            var s2 = SportDAL.Insert(new Sport { Name = "Test Sport2", LastUpdate = new ActivityStamp(user) });
       
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Sports.Add(s1);
            seeker.Sports.Add(s2);
       
            SeekerDAL.Update(seeker);
            return seeker;
        }

        private Seeker GetSeekerWithClubs()
        {
            var s1 = ClubDAL.Insert(new Club { Name = "Test Club1", LastUpdate = new ActivityStamp(user) });
            var s2 = ClubDAL.Insert(new Club { Name = "Test Club2", LastUpdate = new ActivityStamp(user) });
       
            var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
            seeker.Clubs.Add(s1);
            seeker.Clubs.Add(s2);
        
            SeekerDAL.Update(seeker);
            return seeker;
        }
		 
		private Seeker GetSeekerWithAttachments()
		{
			var a1 = new Attachment()
			{
				Bytes = 100,
				Comment = "test file a1",
				LastUpdate = new ActivityStamp(user),
				MimeType = "Text/Plain",
				Name = "tempfilea1.txt",
				UniqueName = Guid.NewGuid().ToString()
			};
			var a2 = new Attachment()
			{
				Bytes = 100,
				Comment = "test file a2",
				LastUpdate = new ActivityStamp(user),
				MimeType = "Text/Plain",
				Name = "tempfilea2.txt",
				UniqueName = Guid.NewGuid().ToString()
			};

			var seeker = InsertSeeker(SeekerDAL, user, StateDAL);
			seeker.Attachments.Add(a1);
			seeker.Attachments.Add(a2);

			SeekerDAL.Update(seeker);
			return seeker;
		}


		public static Seeker InsertSeeker(ISeekerDAL seekerDAL, User user, StateDAL stateDAL)
        {
            var wi = stateDAL.FindByAbbreviation("WI");
		    var seeker = new Seeker
		                     {
		                         User = user,
		                         PersonalStatement = "I like to do stuff",
		                         MyGift = "I read minds",
		                         Gender = Genders.Male,
		                         EthnicityOther = "Plutarian",
		                         ReligionOther = "Anarchy",
		                         AcademicAreaOther = "AcademicOther",
		                         CareerOther = "Test CareerOther",
		                         CommunityServiceOther = "Test CommunityOther",
		                         MatchOrganizationOther = "Test MatchOrganization",
		                         CompanyOther   = "Test AffliiationOther",
		                         HobbyOther = "Test HobbiersOther",
		                         SportOther = "Test SportsOther",
		                         ClubOther = "Test ClubOther",
		                         WorkHistory = "Test WorkHistory",
		                         ServiceHistory = "Test ServiceHistory",
                                 IsWorking = true,
		                         IsService = true,
								 SeekerType = SeekerType.Student,
                                 SeekerAcademics = new SeekerAcademics { GPA = 3 },
                                 LastUpdate = new ActivityStamp(user),
								 Address = new SeekerAddress {City = null, PostalCode="99999-9999", State= wi, Street= "Here", Street2 = "There"},
                                 County=null,
                                 DateOfBirth = DateTime.Now.AddYears(-18),
                                 Phone = new PhoneNumber("414-123-4567"),
                                 MobilePhone = new PhoneNumber("414-123-4568")
                               };

            seeker = seekerDAL.Insert(seeker);

            Assert.IsNotNull(seeker);
            Assert.AreNotEqual(0, seeker.Id);

            return seeker;
        }
    }
}