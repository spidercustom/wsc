using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c> AwardStatuses</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class AwardStatusesType : EnumStringType
    {
        public AwardStatusesType()
            : base(typeof(AwardStatus), 15)
        {
        }
    }
}