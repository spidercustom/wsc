using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IApplicationDAL : IDAL<Application>
    {
        Application FindById(int id);

        IList<Application> FindAllSubmitted(Scholarship scholarship);
        IList<Application> FindAll(Seeker seeker);
        IList<Application> SearchSubmittedBySeeker(Scholarship scholarship, string lastName);
        IList<Application> FindAllFinalists(Scholarship scholarship);

        Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship);
        int CountAllHavingAttachment(Attachment  attachment);
        int CountAllSubmittedBySeeker(Seeker seeker);
		int CountAllSubmitted();
		int CountAllStartedButNotSubmitted();
	}
}