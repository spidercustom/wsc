﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ISupportDAL : IGenericLookupDAL<Support>
    {
        IList<Support> FindAll(SupportType supportType);
    }
}