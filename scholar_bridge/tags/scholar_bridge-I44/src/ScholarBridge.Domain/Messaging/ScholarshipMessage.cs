namespace ScholarBridge.Domain.Messaging
{
    public class ScholarshipMessage : Message
    {
        public virtual Scholarship RelatedScholarship { get; set; }
    }
}