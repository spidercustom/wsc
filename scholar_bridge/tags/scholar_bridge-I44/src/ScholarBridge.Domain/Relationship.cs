﻿using System;
namespace ScholarBridge.Domain
{
    
    public class Relationship
    {
        public virtual int Id { get; set; }
        public virtual Provider Provider { get; set; }
        public virtual Intermediary Intermediary { get; set; }
        public virtual RelationshipRequester Requester { get; set; }
        public virtual RelationshipStatus Status { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }
        public virtual DateTime? ActivatedOn { get; set; }
        public virtual DateTime? InActivatedOn { get; set; }
        public virtual DateTime? RequestedOn { get; set; }

        public virtual Organization ToOrg
        {
            get
            {
                if (Requester == RelationshipRequester.Provider)
                    return Intermediary;
                return Provider;
            }
        }

        public virtual Organization FromOrg
        {
            get
            {
                if (Requester == RelationshipRequester.Provider)
                    return Provider;
                return Intermediary;
            }
        }
    }
}
