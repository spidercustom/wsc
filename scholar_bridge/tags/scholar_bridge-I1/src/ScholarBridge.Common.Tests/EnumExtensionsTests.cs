﻿using NUnit.Framework;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class EnumExtensionsTests
    {
        [Ignore] //Class is not fully functional so removing test case
        public void get_dispaynames_test()
        {
            string[] monthString = EnumExtensions.GetDisplayNames(typeof (DaysOfMonth));
            Assert.AreEqual(12, monthString.Length);
        }
    }
}
