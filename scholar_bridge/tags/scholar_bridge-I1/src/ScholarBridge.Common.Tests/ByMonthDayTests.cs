﻿using System;
using NUnit.Framework;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class ByMonthDayTests
    {
        [Test]
        public void can_do_to_string()
        {
            var byMonthDay = new ByMonthDay {Month = MonthOfYear.January, DayOfMonth = DaysOfMonth.First};

            Assert.AreEqual("1st January", byMonthDay.ToString());
        }

        [Test]
        public void validate_month_change_changes_day()
        {
            var byMonthDay = new ByMonthDay {Month = MonthOfYear.January, DayOfMonth = DaysOfMonth.ThirtyFirst};

            byMonthDay.Month = MonthOfYear.April;
            Assert.AreEqual(DaysOfMonth.Thirtieth, byMonthDay.DayOfMonth);

            byMonthDay.Month = MonthOfYear.February;
            Assert.AreEqual(DaysOfMonth.TwentyEighth, byMonthDay.DayOfMonth);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void validate_setting_thirtieth_throws_exception_for_month_having_twenty_eight_days()
        {
#pragma warning disable 168
            var byMonthDay = new ByMonthDay {Month = MonthOfYear.February, DayOfMonth = DaysOfMonth.Thirtieth};
#pragma warning restore 168
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void validate_setting_thirty_first_throws_exception_for_month_having_thrity_days()
        {
#pragma warning disable 168
            var byMonthDay = new ByMonthDay {Month = MonthOfYear.April, DayOfMonth = DaysOfMonth.ThirtyFirst};
#pragma warning restore 168
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void validate_setting_thirty_first_throws_exception_for_month_having_twenty_eight_days()
        {
#pragma warning disable 168
            var byMonthDay = new ByMonthDay {Month = MonthOfYear.February, DayOfMonth = DaysOfMonth.ThirtyFirst};
#pragma warning restore 168
        }

        [Test]
        public void test_equality_operator()
        {
            var firstObject = new ByMonthDay();
            var secondObject = new ByMonthDay();

            Assert.IsFalse(firstObject == null);
            Assert.IsFalse(null == secondObject);
            Assert.IsTrue(firstObject == secondObject);
            Assert.IsTrue(secondObject == firstObject);
            Assert.IsFalse(firstObject != secondObject);
            Assert.IsFalse(secondObject != firstObject);

            firstObject.DayOfMonth = DaysOfMonth.Second;

            Assert.IsFalse(firstObject == secondObject);
            Assert.IsFalse(secondObject == firstObject);

            firstObject.DayOfMonth = DaysOfMonth.First;
            firstObject.Month = MonthOfYear.February;
            Assert.IsFalse(firstObject == secondObject);
            Assert.IsFalse(secondObject == firstObject);

            firstObject.DayOfMonth = DaysOfMonth.Second;
            firstObject.Month = MonthOfYear.February;
            Assert.IsFalse(firstObject == secondObject);
            Assert.IsFalse(secondObject == firstObject);
        }

        [Test]
        public void test_greater_than_less_than_operator()
        {
            var firstObject = new ByMonthDay { Month = MonthOfYear.January, DayOfMonth = DaysOfMonth.Second};
            var secondObject = new ByMonthDay {Month = MonthOfYear.January, DayOfMonth = DaysOfMonth.First};

            Assert.IsTrue(firstObject > null);
            Assert.IsFalse(firstObject < null);
            Assert.IsFalse(null > secondObject);
            Assert.IsTrue(null < secondObject);

            Assert.IsTrue(firstObject > secondObject);
            Assert.IsTrue(secondObject < firstObject);
        }

    }
}