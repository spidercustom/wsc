﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ScholarBridge.Common
{
    //TODO: Not fully functional, do not use. Impliment this and remove methods from individual enums
    public static class EnumExtensions
    {
        //Oh! I do I make static extension method
        public static string[] GetDisplayNames(Type @enumType)
        {
            var fieldInfos = enumType.GetFields();

            var displayNames = new List<string>();
            Array.ForEach(fieldInfos,
                          fieldInfo =>
                              {
                                  string displayName = GetDisplayNameFromFieldInfo(fieldInfo);
                                  displayNames.Add(displayName);
                              });

            return displayNames.ToArray();
        }

        private static string GetDisplayNameFromFieldInfo(FieldInfo info)
        {
            if (info == null) throw new ArgumentNullException("info");

            var attributes = info.GetCustomAttributes(typeof(EnumElementDisplayNameAttribute), false);
            string result = null;
            Array.ForEach(attributes,
                attribute =>
                    {
                        var displayNameAttribute =
                            attribute as EnumElementDisplayNameAttribute;
                        result = displayNameAttribute != null ? displayNameAttribute.DisplayName : info.Name;
                    });

            return result;
        }

        public static string GetDisplayName(this Enum @enum, object enumElement)
        {
            if (@enum == null) throw new ArgumentNullException("enum");
            if (enumElement == null) throw new ArgumentNullException("enumElement");

            Type enumType = @enum.GetType();
            //FieldInfo enumElementFieldInfo = enumType.GetField(enumElement);

            //return GetDisplayNameFromFieldInfo(enumElementFieldInfo);
            return null;
        }

        //TODO: Move is in here
    }
}
