﻿using System;

namespace ScholarBridge.Common
{
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field)]
    public class EnumElementDisplayNameAttribute : Attribute
    {
        public string DisplayName { get; set; }  
        public EnumElementDisplayNameAttribute(string displayName)
        {
            DisplayName = displayName;
        }
    }
}
