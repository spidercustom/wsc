﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScholarBridge.Common.DayOfYear
{
    // TODO: Move to common
    public static class MonthOfYearExtensions
    {
        public static bool IsIn(this MonthOfYear month, params MonthOfYear[] values)
        {
            return Array.IndexOf(values, month) > -1;
        }

        //Oh, in extension methods how can I have static method extension?
        public static MonthOfYear[] GetMonthsHavingThirtyOneDays()
        {
            return new MonthOfYear[] {
                MonthOfYear.January,
                MonthOfYear.March,
                MonthOfYear.May,
                MonthOfYear.July,
                MonthOfYear.August,
                MonthOfYear.October,
                MonthOfYear.December
            };
        }

        //Oh, in extension methods how can I have static method extension?
        public static MonthOfYear[] GetMonthsHavingThirtyDays()
        {
            return new MonthOfYear[] {
                MonthOfYear.April,
                MonthOfYear.June,
                MonthOfYear.September,
                MonthOfYear.November
            };
        }
    }
}
