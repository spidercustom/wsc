﻿using System;

namespace ScholarBridge.Common.DayOfYear
{
    // TODO: Move to common
    public static class DaysOfMonthExtensions
    {
        public static bool IsIn(this DaysOfMonth dayOfMonth, params DaysOfMonth[] list)
        {
            return Array.IndexOf(list, dayOfMonth) > -1;
        }

        public static string ToNumericValueString(this DaysOfMonth daysOfMonth)
        {
            switch (daysOfMonth)
            {
                case DaysOfMonth.First:
                    return "1st";
                case DaysOfMonth.Second:
                    return "2nd";
                case DaysOfMonth.Third:
                    return "3rd";
                case DaysOfMonth.Fourth:
                    return "4th";
                case DaysOfMonth.Fifth:
                    return "5th";
                case DaysOfMonth.Sixth:
                    return "6th";
                case DaysOfMonth.Seventh:
                    return "7th";
                case DaysOfMonth.Eighth:
                    return "8th";
                case DaysOfMonth.Ninth:
                    return "9th";
                case DaysOfMonth.Tenth:
                    return "10th";
                case DaysOfMonth.Eleventh:
                    return "11th";
                case DaysOfMonth.Twelfth:
                    return "12th";
                case DaysOfMonth.Thirteenth:
                    return "13th";
                case DaysOfMonth.Fourtheenth:
                    return "14th";
                case DaysOfMonth.Fifteenth:
                    return "15th";
                case DaysOfMonth.Sixteenth:
                    return "16th";
                case DaysOfMonth.Seventeenth:
                    return "17th";
                case DaysOfMonth.Eighteenth:
                    return "18th";
                case DaysOfMonth.Nineteenth:
                    return "19th";
                case DaysOfMonth.Twentieth:
                    return "20th";
                case DaysOfMonth.TwentyFirst:
                    return "21st";
                case DaysOfMonth.TwentySecond:
                    return "22nd";
                case DaysOfMonth.TwentyThird:
                    return "23rd";
                case DaysOfMonth.TwentyFourth:
                    return "24th";
                case DaysOfMonth.TwentyFifth:
                    return "25th";
                case DaysOfMonth.TwentySixth:
                    return "26th";
                case DaysOfMonth.TwentySeventh:
                    return "27th";
                case DaysOfMonth.TwentyEighth:
                    return "28th";
                case DaysOfMonth.TwentyNinth:
                    return "29th";
                case DaysOfMonth.Thirtieth:
                    return "30th";
                case DaysOfMonth.ThirtyFirst:
                    return "31st";
                default:
                    throw new NotSupportedException(string.Format("enum value {0} not supported", daysOfMonth.ToString()));
            }
        }
    }
}
