﻿using System;
using System.Text;

namespace ScholarBridge.Common.DayOfYear
{
    public class ByWeekDay : RelativeDayOfYear
    {
        #region operators
        public static bool operator ==(ByWeekDay left, ByWeekDay right)
        {
            if (ReferenceEquals(left, right))
                return true;
            if (ReferenceEquals(null, left))
                return false;
            if (ReferenceEquals(null, right))
                return false;
            return left.Month == right.Month && 
                left.WeekDayOccurrence == right.WeekDayOccurrence &&
                left.DayOfWeek == right.DayOfWeek;
        }

        public static bool operator !=(ByWeekDay left, ByWeekDay right)
        {
            return !(left == right);
        }

        public static bool operator >(ByWeekDay left, ByWeekDay right)
        {
            if (ReferenceEquals(left, right))
                return false;
            if (ReferenceEquals(null, left))
                return false;
            if (ReferenceEquals(null, right))
                return true;
            return left.Month > right.Month ||
                   (left.Month == right.Month && left.WeekDayOccurrence > right.WeekDayOccurrence) ||
                   (left.Month == right.Month && left.WeekDayOccurrence == right.WeekDayOccurrence && left.DayOfWeek > right.DayOfWeek) ;
        }

        public static bool operator <(ByWeekDay left, ByWeekDay right)
        {
            if (ReferenceEquals(left, right))
                return false;
            if (ReferenceEquals(null, left))
                return true;
            if (ReferenceEquals(null, right))
                return false;
            return left.Month < right.Month ||
                   (left.Month == right.Month && left.WeekDayOccurrence < right.WeekDayOccurrence) ||
                   (left.Month == right.Month && left.WeekDayOccurrence == right.WeekDayOccurrence && left.DayOfWeek < right.DayOfWeek);
        }

        public static bool operator >=(ByWeekDay left, ByWeekDay right)
        {
            return (left > right || left == right);
        }

        public static bool operator <=(ByWeekDay left, ByWeekDay right)
        {
            return (left < right || left == right);
        }

        #endregion


        public virtual WeekDayOccurrencesInMonth WeekDayOccurrence { get; set; }
        public virtual DayOfWeek DayOfWeek { get; set; }
        public virtual MonthOfYear Month { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(WeekDayOccurrence.ToNumericValueString());
            builder.Append(" ");
            builder.Append(DayOfWeek.ToString());
            builder.Append(" in ");
            builder.Append(Month.ToString());

            return builder.ToString();
        }

        public bool Equals(ByWeekDay obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj.WeekDayOccurrence, WeekDayOccurrence) && Equals(obj.DayOfWeek, DayOfWeek) && Equals(obj.Month, Month);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (ByWeekDay)) return false;
            return Equals((ByWeekDay) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = WeekDayOccurrence.GetHashCode();
                result = (result*397) ^ DayOfWeek.GetHashCode();
                result = (result*397) ^ Month.GetHashCode();
                return result;
            }
        }

        public DateTime CreateDateTime(int year)
        {
            throw new NotImplementedException();
        }

        public void CopyValuesTo(ByWeekDay to)
        {
            if (to == null) throw new ArgumentNullException("to");
            to.Month = Month;
            to.DayOfWeek = DayOfWeek;
            to.WeekDayOccurrence = WeekDayOccurrence;
        }
    }
}
