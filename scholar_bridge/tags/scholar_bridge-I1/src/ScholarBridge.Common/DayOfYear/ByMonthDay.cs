﻿using System;
using System.Text;

namespace ScholarBridge.Common.DayOfYear
{
    public class ByMonthDay : RelativeDayOfYear
    {
        #region operators
        public static bool operator ==(ByMonthDay left, ByMonthDay right)
        {
            if (ReferenceEquals(left, right))
                return true;
            if (ReferenceEquals(null, left))
                return false;
            if (ReferenceEquals(null, right))
                return false;
            
            if (right == null) throw new ArgumentNullException("right");
            return left.Month.Equals(right.Month) && left.DayOfMonth == right.DayOfMonth;
        }

        public static bool operator !=(ByMonthDay left, ByMonthDay right)
        {
            return !(left == right);
        }

        public static bool operator >(ByMonthDay left, ByMonthDay right)
        {
            if (ReferenceEquals(left, right))
                return false;
            if (ReferenceEquals(null, left))
                return false;
            if (ReferenceEquals(null, right))
                return true;
            return left.Month > right.Month ||
                   (left.Month == right.Month && left.DayOfMonth > right.DayOfMonth);
        }

        public static bool operator <(ByMonthDay left, ByMonthDay right)
        {
            if (ReferenceEquals(left, right))
                return false;
            if (ReferenceEquals(null, left))
                return true;
            if (ReferenceEquals(null, right))
                return false;
            return left.Month < right.Month ||
                   (left.Month == right.Month && left.DayOfMonth < right.DayOfMonth);
        }

        public static bool operator >=(ByMonthDay left, ByMonthDay right)
        {
            return (left > right || left == right);
        }

        public static bool operator <=(ByMonthDay left, ByMonthDay right)
        {
            return (left < right || left == right);
        }

        #endregion

        private DaysOfMonth dayOfMonth;
        private MonthOfYear month;

        public virtual MonthOfYear Month
        {
            get { return month; }
            set
            {
                if (value.IsIn(MonthOfYearExtensions.GetMonthsHavingThirtyDays()) &&
                    dayOfMonth == DaysOfMonth.ThirtyFirst)
                {
                    dayOfMonth = DaysOfMonth.Thirtieth;
                }
                if (value == MonthOfYear.February &&
                    dayOfMonth > DaysOfMonth.TwentyEighth)
                {
                    dayOfMonth = DaysOfMonth.TwentyEighth;
                }
                month = value;
            }
        }

        public virtual DaysOfMonth DayOfMonth
        {
            get { return dayOfMonth; }
            set
            {
                if (DaysOfMonth.ThirtyFirst == value &&
                    Month.IsIn(MonthOfYearExtensions.GetMonthsHavingThirtyDays()))
                    throw new ArgumentException("Incorrect day for the month");
                if (DaysOfMonth.TwentyEighth < value &&
                    MonthOfYear.February == Month)
                    throw new ArgumentException("Incorrect day for the month");

                dayOfMonth = value;
            }
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(DayOfMonth.ToNumericValueString());
            builder.Append(" ");
            builder.Append(Month.ToString());

            return builder.ToString();
        }

        public bool Equals(ByMonthDay obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj.dayOfMonth, dayOfMonth) && Equals(obj.month, month);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (ByMonthDay)) return false;
            return Equals((ByMonthDay) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (dayOfMonth.GetHashCode()*397) ^ month.GetHashCode();
            }
        }

        public DateTime CreateDateTime(int year)
        {
            throw new NotImplementedException();
        }

        public void CopyValuesTo(ByMonthDay to)
        {
            if (to == null) throw new ArgumentNullException("to");

            to.Month = Month;
            to.DayOfMonth = DayOfMonth;
        }
    }
}