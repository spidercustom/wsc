using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public class TemplatedMailerService : MailerService, ITemplatedMailerService
    {
        private const string VARIABLE_TAG = "##";
        private const string VARIABLE_FORMAT = VARIABLE_TAG + "{0}" + VARIABLE_TAG;

        public void SendMail(MailTemplate template, Dictionary<string, string> templateVariables, MailTemplateParams templateParams)
        {
            SendMail(template, templateParams.TemplateDirectory, templateVariables, templateParams.From, templateParams.To);   
        }

        /// <summary>
        /// Send a templated Mail based on the parameters passed.
        /// </summary>
        /// <param name="template">The template to be used.</param>
        /// <param name="templateDir">The directory that should contain the templates.</param>
        /// <param name="templateVariables">The template variables to replace.</param>
        /// <param name="mailAddressFrom">The mail address from.</param>
        /// <param name="mailAddressTo">The mail address to.</param>
        public void SendMail(MailTemplate template, string templateDir, Dictionary<string, string> templateVariables, string mailAddressFrom, string mailAddressTo)
        {
            string pathToTemplate = Path.Combine(templateDir, template.TemplateName());
            if (!File.Exists(pathToTemplate))
            {
                throw new FileNotFoundException("Email Template does not exists.");
            }

            using (var reader = new StreamReader(pathToTemplate))
            {
                string subject = reader.ReadLine();
                string body = ReplaceTemplateVariables(new StringBuilder(reader.ReadToEnd()), templateVariables);
                var message = new MailMessage(mailAddressFrom, mailAddressTo)
                {
                    Subject = subject,
                    Body = body
                };
                SendMail(message);
            }
        }

        public string ReplaceTemplateVariables(StringBuilder bodyContent, Dictionary<string, string> templateVariables)
        {
            foreach (KeyValuePair<string, string> item in templateVariables)
            {
                string key = string.Format(VARIABLE_FORMAT, item.Key);
                bodyContent.Replace(key, item.Value);
            }
            return bodyContent.ToString();
        }
    }
}