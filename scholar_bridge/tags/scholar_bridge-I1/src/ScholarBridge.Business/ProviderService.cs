using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public class ProviderService : IProviderService
    {
        public IProviderDAL ProviderDAL { get; set; }
        public IUserDAL UserDAL { get; set; }
        public IRoleDAL RoleDAL { get; set; }
        public ITemplatedMailerService MailerService { get; set; }

        public Provider Get(int id)
        {
            return ProviderDAL.FindById(id);
        }

        public void SaveNew(Provider provider, User user)
        {
            if (null == provider)
            {
                throw new ArgumentNullException("provider", "Provider can not be null");
            }

            user.Roles.Add(RoleDAL.FindByName(Role.PROVIDER_ROLE));
            UserDAL.Update(user);
            provider.AdminUser = user;
            ProviderDAL.Insert(provider);
        }

        public void Update(Provider provider)
        {
            if (null == provider)
            {
                throw new ArgumentNullException("provider", "Provider can not be null");
            }

            ProviderDAL.Update(provider);
        }

        public IList<Provider> FindPendingProviders()
        {
            return ProviderDAL.FindAllPending();
        }

        public void ApproveProvider(Provider provider, MailTemplateParams templateParams)
        {
            if (null == provider)
            {
                throw new ArgumentNullException("provider", "Provider can not be null");
            }
            provider.ApprovalStatus = ProviderStatus.Approved;

            if (null != provider.AdminUser)
            {
                provider.AdminUser.IsApproved = true;
            }
            ProviderDAL.Update(provider);

            //Send email to admin user that this provider has been approved
            MailerService.SendMail(MailTemplate.ProviderApproval, BuildApprovalMailReplacements(provider.AdminUser, templateParams), templateParams);
        }

        public Dictionary<string, string> BuildApprovalMailReplacements(User user, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()}
                           };

            return templateParams.MergeVariablesInto(dict);
        }

        public void RejectProvider(Provider provider, MailTemplateParams templateParams)
        {
            if (null == provider)
            {
                throw new ArgumentNullException("provider", "Provider can not be null");
            }
            provider.ApprovalStatus = ProviderStatus.Rejected;
            ProviderDAL.Update(provider);

            //Send email to admin user that this provider has been rejected
            MailerService.SendMail(MailTemplate.ProviderRejection, BuildRejectionMailReplacements(provider.AdminUser, templateParams), templateParams);
        }

        public Dictionary<string, string> BuildRejectionMailReplacements(User user, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()}
                           };

            return templateParams.MergeVariablesInto(dict);
        }
    }
}