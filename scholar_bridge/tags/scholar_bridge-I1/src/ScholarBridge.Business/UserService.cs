﻿using System;
using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public class UserService : IUserService 
    {
        public IUserDAL UserDAL { get; set; }
        public IProviderDAL ProviderDAL { get; set; }
        public ITemplatedMailerService MailerService { get; set; }

        public void ActivateUser(User user, MailTemplateParams templateParams)
        {
            if (null == user)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            if (!user.IsActive)
            {
                user.IsActive = true;
                UserDAL.Update(user);

                MailerService.SendMail(MailTemplate.ProviderApprovalEmailToHECBAdmin, BuildActivationMailReplacements(user, templateParams), templateParams);
            }
        }

        public Dictionary<string,string> BuildActivationMailReplacements(User user, MailTemplateParams templateParams)
        {
            Provider provider = ProviderDAL.FindByUser(user);
            var dict = new Dictionary<string, string>
                           {
                               {"FirstName", user.Name.FirstName},
                               {"MiddleName", user.Name.MiddleName},
                               {"LastName", user.Name.LastName},
                               {"Email", user.Email},
                               {"ProviderName", provider.Name},
                               {"ProviderTax", provider.TaxId},
                               {"ProviderWebsite", provider.Website},
                               {"AddressStreet", (provider.Address == null) ? "" : provider.Address.Street},
                               {"AddressStreet2", (provider.Address == null) ? "" : provider.Address.Street2},
                               {"City", (provider.Address == null) ? "" : provider.Address.City},
                               {"State", (provider.Address == null) ? "" : provider.Address.State.Name},
                               {"PostalCode", (provider.Address == null) ? "" : provider.Address.PostalCode},
                               {"Phone", (provider.Phone == null) ? "" : provider.Phone.Number},
                               {"Fax", (provider.Fax == null) ? "" : provider.Fax.Number},
                               {"OtherPhone", (provider.OtherPhone == null) ? "" : provider.OtherPhone.Number}
                           };

            return templateParams.MergeVariablesInto(dict);
        }
    }
}
