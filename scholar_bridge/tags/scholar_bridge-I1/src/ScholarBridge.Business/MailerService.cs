using System;
using System.Net;
using System.Net.Mail;
using ScholarBridge.Business.Exceptions;

namespace ScholarBridge.Business
{
    public class MailerService : IMailerService
    {
        public void SendMail(string from, string to, string subject, string body)
        {
            SendMail(new MailMessage(from, to, subject, body));
        }

        public void SendMail(MailMessage message)
        {
            if (null == message)
            {
                throw new ArgumentNullException("message", "Mail message cannot be null.");
            }

            //validate TO addresses
            if (IsValidMessage(message))
            {
                // Include credentials if the server requires them.
                var client = new SmtpClient
                                 {
                                     Credentials = CredentialCache.DefaultNetworkCredentials
                                 };
                try
                {
                    client.Send(message);
                }
                catch (SmtpFailedRecipientsException ex)
                {
                    for (int i = 0; i < ex.InnerExceptions.Length; i++)
                    {
                        SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                        if (status == SmtpStatusCode.MailboxBusy || status == SmtpStatusCode.MailboxUnavailable)
                        {
                            // Delivery failed - retrying in 5 seconds
                            System.Threading.Thread.Sleep(5000);
                            client.Send(message);
                        }
                        else
                        {
                            //rethrow execption
                            throw;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the specified message is a valid message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>
        /// 	<c>true</c> if specified message is a valid message; otherwise, throws an InvalidEmailMessageException
        /// </returns>
        public bool IsValidMessage(MailMessage message)
        {
            // we don't need to validate email address added in message.from or to or bccc
            // as only a valid email can be added to them while creating the message object else
            // MailMessage object itself fires and invalid mail address exception

            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            if (String.IsNullOrEmpty(message.From.Address))  //checks if From email address is Specified
            {
                throw new InvalidEmailMessageException("Invalid From email address");
            }

            if (message.To.Count == 0) //checks atleast one reciepent is added to the message
            {
                throw new InvalidEmailMessageException("Message Reciepents contain invalid email address");
            }

            if (String.IsNullOrEmpty(message.Subject)) //checks if message contains a subject
            {
                throw new InvalidEmailMessageException("Message subject is empty");
            }

            if (String.IsNullOrEmpty(message.Body)) //checks if message contails a body
            {
                throw new InvalidEmailMessageException("Message body is empty");
            }

            return true;
        }
    }
}