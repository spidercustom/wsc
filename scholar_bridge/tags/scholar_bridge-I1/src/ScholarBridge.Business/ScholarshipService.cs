﻿using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public class ScholarshipService : IScholarshipService
    {
        public IScholarshipDAL ScholarshipDAL { get; set; }
        public IScholarshipStageDAL ScholarshipStageDAL { get; set; }

        #region IScholarshipService Members

        public Scholarship GetById(int id)
        {
            return ScholarshipDAL.FindById(id);
        }

        public IList<Scholarship> GetByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ProviderStatus.Approved)
                throw new Exceptions.ProviderNotApprovedException();
            return ScholarshipDAL.FindByProvider(provider);
        }

        public Scholarship Save(Scholarship scholarship)
        {
            if (scholarship == null)
                throw new ArgumentNullException("scholarship");
            if (scholarship.Provider == null)
                throw new ArgumentNullException("scholarship.Provider");
            if (scholarship.Provider.ApprovalStatus != ProviderStatus.Approved)
                throw new Exceptions.ProviderNotApprovedException();
            if (scholarship.MinimumAmount < 0)
                throw new ArgumentException("Minimum amount cannot be less than zero");
            if (scholarship.MaximumAmount < 1)
                throw new ArgumentException("Maximum amount cannot be less than one");
            if (scholarship.MinimumAmount > scholarship.MaximumAmount)
                throw new ArgumentException("Minimum amount cannot be greate than maximum amount");

            return ScholarshipDAL.Save(scholarship);
        }

        public Scholarship FindByNameAndProvider(string name, Provider provider)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");
            if (provider == null)
                throw new ArgumentNullException("provider");

            return ScholarshipDAL.FindByNameAndProvider(name, provider);
        }

        public bool ScholarshipNameExists(string name, Provider provider)
        {
            return null != FindByNameAndProvider(name, provider);
        }

        public ScholarshipStage GetStageById(ScholarshipStages id)
        {
            return ScholarshipStageDAL.FindById(id);
        }

        public IList<ScholarshipStage> GetAllStages()
        {
            return ScholarshipStageDAL.GetAll();
        }

        #endregion
    }
}
