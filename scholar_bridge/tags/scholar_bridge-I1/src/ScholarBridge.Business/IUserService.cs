﻿using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface  IUserService
    {
        void ActivateUser(User user, MailTemplateParams templateParams);
    }
}
