using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface ITemplatedMailerService : IMailerService
    {
        void SendMail(MailTemplate template, Dictionary<string, string> templateVariables,
                      MailTemplateParams templateParams);

        void SendMail(MailTemplate template, string templateDir, Dictionary<string, string> templateVariables,
                      string mailAddressFrom, string mailAddressTo);
    }
}