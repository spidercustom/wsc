using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IProviderService
    {
        Provider Get(int id);

        void SaveNew(Provider provider, User user);
        void Update(Provider provider);

        IList<Provider> FindPendingProviders();

        void ApproveProvider(Provider provider, MailTemplateParams templateParams);
        void RejectProvider(Provider provider, MailTemplateParams templateParams);
    }
}