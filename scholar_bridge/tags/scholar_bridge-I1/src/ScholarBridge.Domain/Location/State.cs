﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain.Location
{
    [Serializable]
    public class State
    {
        [StringLengthValidator(2,2)]
        public virtual string Abbreviation { get; set; }

        [StringLengthValidator(0, 30)]
        public virtual string Name { get; set; }
    }
}