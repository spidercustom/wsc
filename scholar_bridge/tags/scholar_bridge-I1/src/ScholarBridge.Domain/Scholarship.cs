﻿using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain
{
    public class Scholarship
    {
        public Scholarship()
        {
            AdditionalRequirements = new List<AdditionalRequirement>();
            Need = new DefinitionOfNeed();
            SupportedSituation = new SupportedSituation();
            FundingParameters = new FundingParameters();
            Donor = new ScholarshipDonor();
        }

        public virtual int Id { get; set; }

        [NotNullValidator]
        [StringLengthValidator(1, 50)]
        public virtual string Name { get; set; }

        [StringLengthValidator(0, 2000)]
        public virtual string MissionStatement { get; set; }

        [NotNullValidator]
        public virtual ScholarshipScheduleBase Schedule { get; set; }

        public virtual ScholarshipDonor Donor { get; set; }

        public virtual Provider Provider { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual ScholarshipStage Stage { get; set; }

        [NotNullValidator]
        public virtual decimal MinimumAmount { get; set; }

        [NotNullValidator]
        [PropertyComparisonValidator("MinimumAmount", ComparisonOperator.GreaterThanEqual)]
        public virtual decimal MaximumAmount { get; set; }

        public virtual DefinitionOfNeed Need { get; set; }

        public virtual SupportedSituation SupportedSituation { get; set; }

        public virtual FundingParameters FundingParameters { get; set; }

        public virtual IList<AdditionalRequirement> AdditionalRequirements { get; protected set; }

        public virtual void ResetAdditionalRequirements(IList<AdditionalRequirement> additionalReqs)
        {
            AdditionalRequirements.Clear();
            foreach (var ar in additionalReqs)
            {
                AdditionalRequirements.Add(ar);
            }
        }
    }
}
