﻿namespace ScholarBridge.Domain.Auth
{
    public class Role
    {
        public const string ADMIN_ROLE = "Admin";
        public const string WSC_ADMIN_ROLE = "WSCAdmin";
        public const string PROVIDER_ROLE = "Provider";
        public const string INTERMEDIARY_ROLE = "Intermediary";
        public const string SEEKER_ROLE = "Seeker";

//        public virtual IList<Privilege> Privileges { get; set; }
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }
    }
}