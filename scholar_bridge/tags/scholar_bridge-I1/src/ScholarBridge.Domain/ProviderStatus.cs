namespace ScholarBridge.Domain
{
    public enum ProviderStatus
    {
        None,
        PendingApproval,
        Approved,
        Rejected
    }

    /// <summary>
    /// Useful methods for dealing with <c>ProviderStatus</c>. Really a workaround for
    /// enums being simple integer values in .NET.
    /// </summary>
    public static class ProviderStatusExtension
    {
        /// <summary>
        /// Create a simple English name for the Provider Status.
        /// </summary>
        /// <param name="status">The ProviderStatus that you want the display name for.</param>
        /// <returns>A human readable version of the ProviderStatus.</returns>
        public static string ToDisplayName(this ProviderStatus status)
        {
            switch (status)
            {
                case ProviderStatus.PendingApproval:
                    return "Pending Approval";
                case ProviderStatus.Approved:
                    return "Approved";
                case ProviderStatus.Rejected:
                    return "Rejected";
                default:
                    return "None";
            }
        }
    }
}