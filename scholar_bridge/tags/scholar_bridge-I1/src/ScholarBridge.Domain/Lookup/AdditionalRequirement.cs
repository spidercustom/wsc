using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain.Lookup
{
    public class AdditionalRequirement
    {
        public virtual int Id { get; set; }

        [NotNullValidator]
        public virtual string Name { get; set; }
        
        public virtual string Description { get; set; }
        
        public virtual ActivityStamp LastUpdate { get; set; }
    }
}