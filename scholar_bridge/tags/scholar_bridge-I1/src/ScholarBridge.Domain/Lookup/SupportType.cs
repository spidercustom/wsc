namespace ScholarBridge.Domain.Lookup
{
    public enum SupportType
    {
        None,
        Emergency,
        Traditional
    }
}