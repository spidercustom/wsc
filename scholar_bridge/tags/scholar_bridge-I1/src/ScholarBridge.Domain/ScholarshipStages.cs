﻿
namespace ScholarBridge.Domain
{
    public enum ScholarshipStages
    {
        GeneralInformation,
        SeekerProfile,
        FundingProfile,
        PreviewCandidatePopulation,
        BuildScholarshipMatchLogic,
        BuildScholarshipRenewalCriteria,
        ActiveScholarship
    }
}
