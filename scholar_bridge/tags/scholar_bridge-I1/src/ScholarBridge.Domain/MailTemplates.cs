﻿using System;

namespace ScholarBridge.Domain
{
    public enum MailTemplate
    {
        // a mail template enum should be the firstname of email template file
        // for example if ProverDenied is enum then system will look for
        // providerdenied.mail file.
        ProviderConfirmationLink,
        ProviderRejection,
        ProviderApproval,
        ProviderApprovalEmailToHECBAdmin,
        ForgotPassword
    }

    public static class MailTemplateExtensions
    {
        public static string TemplateName(this MailTemplate mailTemplate)
        {
            return string.Format("{0}.mail", Enum.GetName(typeof (MailTemplate), mailTemplate));
        }
    }
}