using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingParameters
    {
        public virtual decimal AnnualSupportAmount { get; set; }

        public virtual int MinimumNumberOfAwards { get; set; }

        [PropertyComparisonValidator("MinimumNumberOfAwards", ComparisonOperator.GreaterThan)]
        public virtual int MaximumNumberOfAwards { get; set; }
    }
}