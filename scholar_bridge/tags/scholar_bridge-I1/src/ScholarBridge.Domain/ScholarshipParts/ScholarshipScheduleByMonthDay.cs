﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipScheduleByMonthDay : ScholarshipScheduleBase
    {
        public ScholarshipScheduleByMonthDay()
        {
            StartFrom = new ByMonthDay();
            DueOn = new ByMonthDay();
            AwardOn = new ByMonthDay();
        }

        //TODO: Find out better way to move start, due and award here
        [NotNullValidator]
        public virtual ByMonthDay StartFrom { get; set; }

        [NotNullValidator]
        [PropertyComparisonValidator("StartFrom", ComparisonOperator.GreaterThanEqual)]
        public virtual ByMonthDay DueOn { get; set; }


        [NotNullValidator]
        [PropertyComparisonValidator("DueOn", ComparisonOperator.GreaterThanEqual)]
        public virtual ByMonthDay AwardOn { get; set; }
    }
}
