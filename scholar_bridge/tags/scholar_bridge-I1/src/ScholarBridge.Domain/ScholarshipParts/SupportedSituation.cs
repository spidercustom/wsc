using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class SupportedSituation
    {

        public SupportedSituation()
        {
            TypesOfSupport = new List<Support>();
        }

        public virtual bool Emergency { get; set; }

        public virtual bool Traditional { get; set; }

        public virtual IList<Support> TypesOfSupport { get; protected set; }

        public virtual void ResetTypesOfSupport(IList<Support> supports)
        {
            TypesOfSupport.Clear();
            foreach (var ar in supports)
            {
                TypesOfSupport.Add(ar);
            }
        }
    }
}