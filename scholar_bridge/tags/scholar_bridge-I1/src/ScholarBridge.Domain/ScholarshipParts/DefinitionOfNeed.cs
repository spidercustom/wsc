using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class DefinitionOfNeed
    {
        public DefinitionOfNeed()
        {
            NeedGaps = new List<NeedGap>();
        }

        public virtual bool Fafsa { get; set; }

        public virtual bool UserDerived { get; set; }

        public virtual decimal MinimumSeekerNeed { get; set; }

        [PropertyComparisonValidator("MinimumSeekerNeed", ComparisonOperator.GreaterThan)]
        public virtual decimal MaximumSeekerNeed { get; set; }

        public virtual IList<NeedGap> NeedGaps { get; protected set; }

        public virtual void ResetNeedGaps(IList<NeedGap> needGaps)
        {
            NeedGaps.Clear();
            foreach (var ar in needGaps)
            {
                NeedGaps.Add(ar);
            }
        }
    }
}