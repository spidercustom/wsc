﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Contact;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipDonor
    {
        public ScholarshipDonor()
        {
            Name = new PersonName();
            Address = new Address();
            Phone = new PhoneNumber();
        }

        public virtual PersonName Name { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual Address Address { get; set; }

        [EmailValidator]
        public virtual string Email { get; set; }
    }
}
