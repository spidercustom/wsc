﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Domain
{
    public class Provider
    {

        private User adminUser;

        public Provider()
        {
            ApprovalStatus = ProviderStatus.PendingApproval;
            Users = new List<User>();
        }

        public virtual int Id { get; set; }

        public virtual IList<User> Users { get; protected set; }

        public virtual User AdminUser {
            get { return adminUser; }
            set
            {
                // If the user is added as an Admin user also add them to the UserList if they don't exist
                if (null != value && ! Users.Any(u => u.Id == value.Id))
                {
                    Users.Add(value);
                }
                adminUser = value;
            } 
        }

        [NotNullValidator]
        [StringLengthValidator(1, 128)]
        public virtual string Name { get; set; }

        public virtual Address Address { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual PhoneNumber Fax { get; set; }
        public virtual PhoneNumber OtherPhone { get; set; }

        [StringLengthValidator(1, 128)]
        public virtual string Website { get; set; }

        [NotNullValidator]
        [StringLengthValidator(9, 10)]
        public virtual string TaxId { get; set; }

        public virtual ProviderStatus ApprovalStatus { get; set; }
        public virtual string AdminNotes { get; protected set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        /// <summary>
        /// Append notes to the end of the AdminNotes.
        /// The user passed in will be atrributed.
        /// </summary>
        /// <param name="userAppendingNotes">The user to attribute the notes to.</param>
        /// <param name="notes">The test to add.</param>
        public virtual void AppendAdminNotes(User userAppendingNotes, string notes)
        {
            if (null == userAppendingNotes)
                throw new ArgumentNullException("userAppendingNotes", "Must supply a user to attribute these notes to.");
            if (null == notes)
                throw new ArgumentNullException("notes", "Must supply notes.");

            var sb = new StringBuilder(AdminNotes);
            sb.Append("-- ").Append(userAppendingNotes.Name).Append("\t")
                .Append(DateTime.Now.ToShortDateString()).Append(" ")
                .Append(DateTime.Now.ToShortTimeString()).AppendLine(" --");
            sb.AppendLine(notes).AppendLine();

            AdminNotes = sb.ToString();
        }
    }
}
