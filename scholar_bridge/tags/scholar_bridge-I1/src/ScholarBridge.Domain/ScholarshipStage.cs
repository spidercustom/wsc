﻿
namespace ScholarBridge.Domain
{
    public class ScholarshipStage
    {
        public virtual ScholarshipStages Id { get; set; }
        public virtual string Name { get; set; }
    }
}
