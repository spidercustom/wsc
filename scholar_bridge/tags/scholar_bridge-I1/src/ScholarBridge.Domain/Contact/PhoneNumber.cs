﻿using System;
using System.Text.RegularExpressions;

namespace ScholarBridge.Domain.Contact
{
    public class PhoneNumber
    {
        private string number;
        private readonly Regex regex = new Regex("[^0-9]");

        public PhoneNumber()
        {
        }

        public PhoneNumber(string number)
        {
            Number = number;
        }

        public virtual string Number
        {
            get { return number; }
            set
            {
                if (string.IsNullOrEmpty(value)) return;

                // strip out everything but the numbers
                number = regex.Replace(value, "");

                // trim the beginning 1
                if (number.StartsWith("1"))
                {
                    number = number.Substring(1);
                }
            }
        }

        public virtual string FormattedPhoneNumber
        {
            get
            {
                return String.IsNullOrEmpty(number)
                           ? null
                           : string.Format("{0}-{1}-{2}",
                                           Number.Substring(0, 3),
                                           Number.Substring(3, 3),
                                           Number.Substring(6));
            }
        }

        public override string ToString()
        {
            return FormattedPhoneNumber;
        }
    }
}