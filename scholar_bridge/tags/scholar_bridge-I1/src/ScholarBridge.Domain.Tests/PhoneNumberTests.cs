using NUnit.Framework;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class PhoneNumberTests
    {
        [Test]
        public void NullPhoneNumberParsed()
        {
            var p = new PhoneNumber(null);
            Assert.AreEqual(null, p.Number);
        }

        [Test]
        public void PhoneNumberParsed()
        {
            var p = new PhoneNumber("(414) 290-8031");
            Assert.AreEqual("4142908031", p.Number);
        }

        [Test]
        public void PhoneNumberWithOneParsed()
        {
            var p = new PhoneNumber("1 (414) 290-8031");
            Assert.AreEqual("4142908031", p.Number);
        }

        [Test]
        public void PhoneNumberFormatted()
        {
            var p = new PhoneNumber("(414) 290-8031");
            Assert.AreEqual("414-290-8031", p.FormattedPhoneNumber);
            Assert.AreEqual(p.FormattedPhoneNumber, p.ToString());
        }

        [Test]
        public void PhoneNumberFormattedNull()
        {
            var p = new PhoneNumber();
            Assert.IsNull(p.FormattedPhoneNumber);
        }

    }
}