using System;
using NUnit.Framework;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Domain.Tests
{

    [TestFixture]
    public class ProviderTests
    {
        
        [Test]
        public void AddingAdminUserAddsUserToUsersCollection()
        {
            var p = new Provider();

            Assert.AreEqual(0, p.Users.Count);
            p.AdminUser = new User { Id = 1};

            CollectionAssert.IsNotEmpty(p.Users);
            Assert.AreEqual(1, p.Users.Count);

            p.AdminUser = new User { Id = 1 };
            Assert.AreEqual(1, p.Users.Count);
        }

        [Test]
        public void CanAppendAdminNotesToProvider()
        {
            var p = new Provider();
            var u = new User
                        {
                            Name =
                                {
                                    FirstName = "TestFirst",
                                    LastName = "TestLast"
                                }
                        };

            p.AppendAdminNotes(u, "This is a test note.");
            StringAssert.Contains("-- TestFirst TestLast", p.AdminNotes);
            StringAssert.Contains("This is a test note.", p.AdminNotes);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CanNotAppendAdminNotesWithoutNotes()
        {
            var p = new Provider();
            p.AppendAdminNotes(new User(), null);

            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CanNotAppendAdminNotesWithoutUser()
        {
            var p = new Provider();
            p.AppendAdminNotes(null, "This is a test note.");

            Assert.Fail();
        }
    }
}