﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class FundingProfile : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public IAdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        public INeedGapDAL NeedGapDAL { get; set; }

        private Scholarship scholarshipInContext;

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            scholarshipInContext = Container.GetDomainObject();
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        private void PopulateScreen()
        {
            PopulateCheckBoxes(AdditionalRequirements, AdditionalRequirementDAL.FindAll());
            PopulateCheckBoxes(NeedGaps, NeedGapDAL.FindAll());
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());

            scholarshipNameLbl.Text = scholarshipInContext.Name;
            AdditionalRequirements.Items.SelectItems(scholarshipInContext.AdditionalRequirements, ar => ar.Id.ToString());

            if (null != scholarshipInContext.Need)
            {
                Fafsa.Checked = scholarshipInContext.Need.Fafsa;
                UserDerived.Checked = scholarshipInContext.Need.UserDerived;
                MinimumSeekerNeed.Text = scholarshipInContext.Need.MinimumSeekerNeed.ToString();
                MaximumSeekerNeed.Text = scholarshipInContext.Need.MaximumSeekerNeed.ToString();
                NeedGaps.Items.SelectItems(scholarshipInContext.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != scholarshipInContext.SupportedSituation)
            {
                Emergency.Checked = scholarshipInContext.SupportedSituation.Emergency;
                Traditional.Checked = scholarshipInContext.SupportedSituation.Traditional;

                TypesOfSupport.Items.SelectItems(scholarshipInContext.SupportedSituation.TypesOfSupport,
                                                 ts => ts.Id.ToString());
            }

            if (null != scholarshipInContext.FundingParameters)
            {
                AnnualSupportAmount.Text = scholarshipInContext.FundingParameters.AnnualSupportAmount.ToString();
                MinimumNumberOfAwards.Text = scholarshipInContext.FundingParameters.MinimumNumberOfAwards.ToString();
                MaximumNumberOfAwards.Text = scholarshipInContext.FundingParameters.MaximumNumberOfAwards.ToString();
            }
        }

        private void PopulateObjects()
        {
            var selectedAdditionalReqs = AdditionalRequirements.Items.SelectedItems(item => Int32.Parse(item));
            scholarshipInContext.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll(selectedAdditionalReqs));

            // Need
            if (null == scholarshipInContext.Need)
                scholarshipInContext.Need = new DefinitionOfNeed();
            scholarshipInContext.Need.Fafsa = Fafsa.Checked;
            scholarshipInContext.Need.UserDerived = UserDerived.Checked;
            scholarshipInContext.Need.MinimumSeekerNeed = GetMoneyValue(MinimumSeekerNeed, MinimumSeekerNeedValidator);
            scholarshipInContext.Need.MaximumSeekerNeed = GetMoneyValue(MaximumSeekerNeed, MaximumSeekerNeedValidator);

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => Int32.Parse(item));
            scholarshipInContext.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            scholarshipInContext.SupportedSituation.Emergency = Emergency.Checked;
            scholarshipInContext.SupportedSituation.Traditional = Traditional.Checked;

            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => Int32.Parse(item));
            scholarshipInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            // FundingParameters
            scholarshipInContext.FundingParameters.AnnualSupportAmount = GetMoneyValue(AnnualSupportAmount,
                                                                                       AnnualSupportAmountValidator);
            scholarshipInContext.FundingParameters.MinimumNumberOfAwards = GetIntValue(MinimumNumberOfAwards,
                                                                                       MinimumNumberOfAwardsValidator);
            scholarshipInContext.FundingParameters.MaximumNumberOfAwards = GetIntValue(MaximumNumberOfAwards,
                                                                                       MaximumNumberOfAwardsValidator);

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage == null || scholarshipInContext.Stage.Id < ScholarshipStages.FundingProfile)
                scholarshipInContext.Stage = ScholarshipService.GetStageById(ScholarshipStages.FundingProfile);
        }

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage.Id == ScholarshipStages.SeekerProfile;
        }

        public override int StepIndex
        {
            get { return 3; }
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public decimal GetMoneyValue(TextBox tb, PropertyProxyValidator validator)
        {
            decimal amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (! Decimal.TryParse(tb.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public int GetIntValue(TextBox tb, PropertyProxyValidator validator)
        {
            int amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (!Int32.TryParse(tb.Text, NumberStyles.Integer | NumberStyles.AllowThousands, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }

        #endregion
    }
}