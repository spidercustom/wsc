﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfo.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.GeneralInfo" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="ByMonthControl" Src="~/Common/DayByMonth.ascx" %>
<%@ Register TagPrefix="sb" TagName="ByWeekControl" Src="~/Common/DayByWeek.ascx" %>

<h3>Build Scholarship – General Information</h3>

<label for="ScholarshipNameControl">Scholarship Name:</label>
<span class="requiredAttributeIndicator">*</span>
<asp:TextBox ID="ScholarshipNameControl" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="ScholarshipNameValidator" runat="server" ControlToValidate="ScholarshipNameControl" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
<br />

<p class="additionInfoText">
  The mission statement can contain what you want the scholarship seekers to see. Note: This is not a narrative version of criteria but rather the inspiration that motivated the scholarship founder. It is limited to 250 words to provide focus.
</p>

<label for="MissionStatementControl">Mission Statement:</label>
<asp:TextBox ID="MissionStatementControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="MissionStatementValidator" runat="server" ControlToValidate="MissionStatementControl" PropertyName="MissionStatement" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
<br />

<fieldset>
  <legend>Schedule</legend>
  <asp:Button ID="ScheduleByWeekButton" runat="server" Text="By Week" 
    onclick="ScheduleByWeekButton_Click" CausesValidation="false" />
  <asp:Button ID="ScheduleByMonthButton" runat="server" Text="By Month" 
    onclick="ScheduleByMonthButton_Click" CausesValidation="false" />
  <br />
  
  <asp:MultiView ID="Schedule" runat="server" ActiveViewIndex="0">
    <asp:View ID="ScheduleByWeek" runat="server">
      <h4>Defining schedule by week</h4>
    
      <label for="StartFromByWeekControl">Application Start Date:</label>      
      <sb:ByWeekControl id="StartFromByWeekControl" runat="server" />
      <br />
      
      <label for="DueOnByWeekControl">Application Due Date:</label>
      <sb:ByWeekControl id="DueOnByWeekControl" runat="server" />
      <br />

      <label for="AwardOnByWeekControl">Scholarship Award Date:</label>
      <sb:ByWeekControl id="AwardOnByWeekControl" runat="server" />
      <br />
    </asp:View>
    <asp:View ID="ScheduleByMonth" runat="server">
      <p>Defining schedule by month</p>
    
      <label for="StartDate">Application Start Date:</label>      
      <sb:ByMonthControl id="StartFromByMonthControl" runat="server" />
      <br />
      
      <label for="DueDate">Application Due Date:</label>
      <sb:ByMonthControl id="DueOnByMonthControl" runat="server" />
      <br />

      <label for="AwardDate">Scholarship Award Date:</label>
      <sb:ByMonthControl id="AwardOnByMonthControl" runat="server" />
      <br />
    </asp:View>
  </asp:MultiView>
</fieldset>

<fieldset >
  <legend>Scholarship Award Amount</legend>
  
  <label for="MaximumAmount">Maximum Scholarship Award Amount:</label>
  <span class="requiredAttributeIndicator">*</span>
  <asp:TextBox ID="MaximumAmount" runat="server"></asp:TextBox>
  <elv:PropertyProxyValidator ID="MaximumAmountValidator" runat="server" ControlToValidate="MaximumAmount" PropertyName="MaximumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
  <br />

  <label for="MinimumAmount">Minimum Scholarship Award Amount:</label>
  <span class="requiredAttributeIndicator">*</span>
  <asp:TextBox ID="MinimumAmount" runat="server"></asp:TextBox>
  <elv:PropertyProxyValidator ID="MinimumAmountValidator" runat="server" ControlToValidate="MinimumAmount" PropertyName="MinimumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
  <br />
</fieldset>

<fieldset>
  <legend>Donor</legend>
    
    <h4>Name</h4>
    <label for="FirstName">First Name:</label>
    <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
    <br />
    
    <label for="MiddleName">Middle Name:</label>
    <asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
    <br />
    
    <label for="LastName">Last Name:</label>
    <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
    <br />
  
    <h4>Contact Informations</h4>
    <label for="AddressStreet">Street:</label>
    <asp:TextBox ID="AddressStreet" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressStreet2">Street:</label>
    <asp:TextBox ID="AddressStreet2" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressCity">City:</label>
    <asp:TextBox ID="AddressCity" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressState">State:</label>
    <asp:DropDownList ID="AddressState" runat="server"></asp:DropDownList>
    <elv:PropertyProxyValidator ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressPostalCode">Postal Code:</label>
    <asp:TextBox ID="AddressPostalCode" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="Phone">Phone:</label>
    <asp:TextBox ID="Phone" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
    <br />

    <label for="EmailAddress">Email Address:</label>
    <asp:TextBox ID="EmailAddress" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="EmailAddressValidator" runat="server" ControlToValidate="EmailAddress" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
    <br />

</fieldset>