﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Web.Wizards;
using Spring.Context;
using Spring.Context.Support;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Default : System.Web.UI.Page, IWizardStepsContainer<Domain.Scholarship>
    {
        private const string SCHOLARSHIP_ID = "sid";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            if (provider.ApprovalStatus != ScholarBridge.Domain.ProviderStatus.Approved)
                throw new Business.Exceptions.ProviderNotApprovedException();

            foreach (var step in Steps)
            {
                step.Container = this;
            }

            if (!IsPostBack)
            {
                Domain.Scholarship scholarshipToEdit = GetScholarshipToEdit();
                if (scholarshipToEdit != null)
                {
                    if (!scholarshipToEdit.Provider.Id.Equals(UserContext.CurrentProvider.Id))
                        throw new InvalidOperationException("Scholarship do not belong to provider in context");
                    
                    ResumeWizard();
                }
            }
        }


        protected void SaveAndExitButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && ValidateStep())
            {
                Save();
                Response.Redirect("../");
            }
        }

        public Domain.Scholarship GetScholarshipToEdit()
        {
            string scholarshipIdString = ScholarshipIdString;

            if (string.IsNullOrEmpty(scholarshipIdString)) //if not found so far return null
                return null;

            int scholarshipId = -1;
            if (!Int32.TryParse(scholarshipIdString, out scholarshipId))
                throw new ArgumentException("Cannot understand value of parameter scholarship");
            Domain.Scholarship result = ScholarshipService.GetById(scholarshipId);
            return result;
        }

        private string ScholarshipIdString
        {
            get
            {
                string scholarshipIdString = string.Empty;
                if (!string.IsNullOrEmpty(Request.Params[SCHOLARSHIP_ID])) //retrive from parameters
                    scholarshipIdString = Request.Params[SCHOLARSHIP_ID];

                if (string.IsNullOrEmpty(scholarshipIdString) && //if not in parameters
                    null != ViewState[SCHOLARSHIP_ID] ) //retrive from view state
                    scholarshipIdString = ViewState[SCHOLARSHIP_ID].ToString();
                return scholarshipIdString;
            }
            set
            {
                ViewState[SCHOLARSHIP_ID] = value;
            }
        }

        protected void BuildScholarshipWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Page.IsValid && ValidateStep())
            {
                Save();
            }
            else if (e.NextStepIndex > e.CurrentStepIndex)
            {
                e.Cancel = true;
            }
        }

        private bool ValidateStep()
        {
            IWizardStepControl<Domain.Scholarship> wizardStepControl = WizardStepControlQueries.FindFirstWizardStepControl<Domain.Scholarship>(BuildScholarshipWizard.ActiveStep.Controls);
            if (wizardStepControl != null)
            {
                return wizardStepControl.ValidateStep();
            }

            return true;
        }

        private void Save()
        {
            //TODO: Fix this: Not sure Why do I need to write IsValid logic here
            if (Page.IsValid && ValidateStep())
            {
                IWizardStepControl<Domain.Scholarship> wizardStepControl = WizardStepControlQueries.FindFirstWizardStepControl<Domain.Scholarship>(BuildScholarshipWizard.ActiveStep.Controls);
                if (wizardStepControl != null)
                {
                    wizardStepControl.Save();
                    if (wizardStepControl.ChangeNextStepIndex())
                        BuildScholarshipWizard.ActiveStepIndex = wizardStepControl.GetChangedNextStepIndex();
                }
                ScholarshipIdString = GetDomainObject().Id.ToString();
            }
        }

        #region IWizardStepsContainer<Scholarship> Members
        //TODO: Can be moved to WizardStepsContainerPageBase
        public void ResumeWizard()
        {
            for (int stepIndex = 0; stepIndex < Steps.Length; stepIndex++)
            {
                IWizardStepControl<Domain.Scholarship> stepControl = Steps[stepIndex];
                if (stepControl.CanResume(GetDomainObject()))
                {
                    BuildScholarshipWizard.ActiveStepIndex = stepControl.StepIndex;
                    break;
                }
            }
        }
        //TODO: Can be moved to WizardStepsContainerPageBase
        public IWizardStepControl<Domain.Scholarship>[] Steps
        {
            get
            {
                IWizardStepControl<Domain.Scholarship>[] stepControls = WizardStepControlQueries.FindWizardStepControls<Domain.Scholarship>(BuildScholarshipWizard.WizardSteps);
                return stepControls;
            }
        }
        //TODO: Can be moved to WizardStepsContainerPageBase
        Domain.Scholarship scholarshipInContext;
        public Domain.Scholarship GetDomainObject()
        {
            if (scholarshipInContext == null)
            {
                scholarshipInContext = GetScholarshipToEdit();
                if (scholarshipInContext == null)
                {
                    scholarshipInContext = new Domain.Scholarship();
                    scholarshipInContext.Provider = UserContext.CurrentProvider;
                }
            }
            return scholarshipInContext;
        }

        #endregion
    }
}
