﻿using System;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class SeekerProfile : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
        }

        private void PopulateObjects()
        {
            Domain.Scholarship scholarshipInContext = Container.GetDomainObject();
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage == null || scholarshipInContext.Stage.Id <ScholarshipStages.SeekerProfile)
                scholarshipInContext.Stage = ScholarshipService.GetStageById(ScholarshipStages.SeekerProfile);
        }

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            Domain.Scholarship scholarshipInContext = Container.GetDomainObject();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage.Id == ScholarshipStages.SeekerProfile;
        }

        public override int StepIndex
        {
            get { return 2; }
        }

        public override bool ValidateStep()
        {
            return true;
        }
        #endregion
    }
}