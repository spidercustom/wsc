﻿using System;
using System.Web;
using ScholarBridge.Common;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Provider
{
    public partial class ConfirmProvider : System.Web.UI.Page
    {
        public IUserService UserService { get; set; }
        public IUserDAL UserDAL { get; set; }
        public IProviderDAL ProviderDAL { get; set; }

        public string Key
        {
            get { return Request.QueryString["key"]; }
        }

        public string DecryptedKey
        {
            get { return CryptoHelper.Decrypt(Key); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (String.IsNullOrEmpty(Key) || !IsValidKey(Key))
                {
                    SetError();
                    return;
                }

                User u = UserDAL.FindByUsername(DecryptedKey);
                if (null == u)
                {
                    SetError();
                    return;
                }
                var mailParams = ConfigHelper.GetMailParams(ConfigHelper.GetHECBAdminEmail());
                mailParams.MergeVariables.Add("Link", BuildLinkToApproval());
                UserService.ActivateUser(u, mailParams);

                lblStatus.Text =
                    "Thank you for your request. Your email has been validated.  You will receive an email once you are setup in the system.  The validation process may take up to 1 week.";
            }
        }

        private static bool IsValidKey(string key)
        {
            return key.StartsWith("enc");
        }

        public void SetError()
        {
            lblStatus.Text = "The link you have followed is not valid.";
        }

        public static string BuildLinkToApproval()
        {
            var builder = new UriBuilder(HttpContext.Current.Request.Url)
                              {
                                  Path = "Admin/PendingProviders.aspx"
                              };

            return builder.Uri.AbsoluteUri;
        }
    }
}