﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ScholarBridge.Web.Login" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:Login ID="Login1" runat="server" UserNameLabelText="Email Address" PasswordRecoveryUrl="~/ForgotPassword.aspx" PasswordRecoveryText="Forgot Your Password?">
    </asp:Login>
</asp:Content>
