﻿$(document).ready(function() {
    // convert print divs to clickable things that cause a print to occur
    $(".print").mouseup(function() {
        window.print();
    }).hover(
        function() { $(this).attr("style", "cursor: pointer;"); },
        function() { $(this).attr("style", "cursor: default;"); }
    );
});