﻿using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using System.Web;
using ScholarBridge.Common;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Web.Config;
using System.Collections.Generic;

namespace ScholarBridge.Web
{
    public partial class RegisterProvider : System.Web.UI.Page
    {

        public IProviderService ProviderService { get; set; }
        public IUserDAL UserService { get; set; }
        public IStateDAL StateService { get; set; }
        public ITemplatedMailerService MailerService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (! Page.IsPostBack)
            {
                var states = (DropDownList)CreateUserWizardStep1.ContentTemplateContainer.FindControl("AddressState");
                states.DataSource = StateService.FindAll();
                states.DataTextField = "Name";
                states.DataValueField = "Abbreviation";
                states.DataBind();
                states.Items.Insert(0, new ListItem("- Select One -", ""));
                //turn on automatic mail sending by setting up from address
                CreateUserWizard1.MailDefinition.From = ConfigHelper.GetSmtpSection().From;
            }
        }

        protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
        {
            CreateUserWizard cuw = (CreateUserWizard)sender;
            cuw.Email = cuw.UserName; 
        }

        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            User u = GetCurrentUser(sender);
            u.Name.FirstName = GetTextBoxValue(CreateUserWizardStep1, "FirstName");
            u.Name.MiddleName = GetTextBoxValue(CreateUserWizardStep1, "MiddleName");
            u.Name.LastName = GetTextBoxValue(CreateUserWizardStep1, "LastName");
            
            var provider = new Domain.Provider
                               {
                                   Name = GetTextBoxValue(CreateUserWizardStep1, "Name"),
                                   TaxId = GetTextBoxValue(CreateUserWizardStep1, "TaxId"),
                                   Website = GetTextBoxValue(CreateUserWizardStep1, "Website"),
                                   LastUpdate = new ActivityStamp(u),
                                   Address = GetAddress(CreateUserWizardStep1),
                                   Phone = GetPhone(CreateUserWizardStep1, "Phone"),
                                   Fax = GetPhone(CreateUserWizardStep1, "Fax"),
                                   OtherPhone = GetPhone(CreateUserWizardStep1, "OtherPhone")
                               };

            ProviderService.SaveNew(provider, u);
        }

        private User GetCurrentUser(object sender)
        {
            var cuw = (CreateUserWizard)sender;
            return UserService.FindByUsername(cuw.Email);
        }

        private static PhoneNumber GetPhone(TemplatedWizardStep step2, string name)
        {
            string phone = GetTextBoxValue(step2, name);
            if (String.IsNullOrEmpty(phone))
                return null;

            return new PhoneNumber(phone);
        }

        private Address GetAddress(TemplatedWizardStep step2)
        {
            string street = GetTextBoxValue(step2, "AddressStreet");
            string street2 = GetTextBoxValue(step2, "AddressStreet2");
            string city = GetTextBoxValue(step2, "AddressCity");
            string postalCode = GetTextBoxValue(step2, "AddressPostalCode");
            string state = GetDropDownValue(step2, "AddressState");

            return new Address
                       {
                           Street = street,
                           Street2 = street2,
                           City = city,
                           PostalCode = postalCode,
                           State = StateService.FindByAbbreviation(state)
                       };
        }

        private static string GetTextBoxValue(TemplatedWizardStep step, string name)
        {
            return GetTextBoxFromStep(step, name).Text;
        }

        private static string GetDropDownValue(TemplatedWizardStep step, string name)
        {
            return ((DropDownList)step.ContentTemplateContainer.FindControl(name)).SelectedValue;
        }

        private static TextBox GetTextBoxFromStep(TemplatedWizardStep step, string name)
        {
            return (TextBox)step.ContentTemplateContainer.FindControl(name);
        }

        protected void CreateUserWizard1_CreateUserError(object sender, CreateUserErrorEventArgs e)
        {
            switch (e.CreateUserError)
            {
                case MembershipCreateStatus.DuplicateUserName:
                case MembershipCreateStatus.DuplicateEmail:
                    var valid = (PropertyProxyValidator)
                        CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserNameValidator");
                    valid.Text = "EmailAddress is already in use, please choose another one.";
                    valid.IsValid = false;
                    break;
            }
        }

        protected void CreateUserWizard1_SendingMail(object sender, MailMessageEventArgs e)
        {
            User u = GetCurrentUser(sender);
            string link = BuildConfirmationLinkForUser(u);

            var dict = new Dictionary<string,string> {{"Name", u.Name.ToString()}, {"Link", link}};

            MailerService.SendMail(MailTemplate.ProviderConfirmationLink, dict, ConfigHelper.GetMailParams(u.Email));

            // We handled sending it ourselves, so cancel
            e.Cancel = true;
        }

        private string BuildConfirmationLinkForUser(User u)
        {
            var builder = new UriBuilder(HttpContext.Current.Request.Url)
                              {
                                  Path = "Provider/ConfirmProvider.aspx",
                                  Query = String.Format("key={0}", HttpUtility.UrlEncode(CryptoHelper.Encrypt(u.Email)))
                              };

            return builder.Uri.AbsoluteUri;
        }
    }
}
