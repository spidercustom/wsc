﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="RegisterProvider.aspx.cs"
    Inherits="ScholarBridge.Web.RegisterProvider" Title="Untitled Page" %>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" 
        DisableCreatedUser="True"
        OnCreatedUser="CreateUserWizard1_CreatedUser"
        OnCreatingUser="CreateUserWizard1_CreatingUser"
        CreateUserButtonText="Register" 
        InvalidPasswordErrorMessage="Password requires 1 capital letter &amp; 1 numeric or special character with an 8 character minimum" 
        oncreateusererror="CreateUserWizard1_CreateUserError" 
        onsendingmail="CreateUserWizard1_SendingMail">
        <WizardSteps>
            <asp:CreateUserWizardStep  ID="CreateUserWizardStep1" runat="server">
                <ContentTemplate>
                
                    <h3>User Information</h3>
                    <label for="FirstName">First Name:</label>
                    <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="MiddleName">Middle Name:</label>
                    <asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"  ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="LastName">Last Name:</label>
                    <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="UserName">Email Address:</label>
                    <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                    <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
                    <br />
                    
                    <label for="Password">Password:</label>
                    <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="ConfirmPassword">Confirm Password:</label>
                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                        ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."
                        ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                        ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                        ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                    <br />

                    <h3>ScholarShip Provider</h3>
                    <label for="Name">Name:</label>
                    <asp:TextBox ID="Name" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="NameValidator" runat="server" ControlToValidate="Name" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Provider" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="TaxId">Tax Id (EIN):</label>
                    <asp:TextBox ID="TaxId" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="TaxIdValidator" runat="server" ControlToValidate="TaxId" PropertyName="TaxId" SourceTypeName="ScholarBridge.Domain.Provider" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="Website">Website:</label>
                    <asp:TextBox ID="Website" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="WebsiteValidator" runat="server" ControlToValidate="Website" PropertyName="Website" SourceTypeName="ScholarBridge.Domain.Provider" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    
                    <h4>Address</h4>
                    <label for="AddressStreet">Street:</label>
                    <asp:TextBox ID="AddressStreet" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="AddressStreet2">Street:</label>
                    <asp:TextBox ID="AddressStreet2" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="AddressCity">City:</label>
                    <asp:TextBox ID="AddressCity" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="AddressState">State:</label>
                    <asp:DropDownList ID="AddressState" runat="server"></asp:DropDownList>
                    <elv:PropertyProxyValidator ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="AddressPostalCode">Postal Code:</label>
                    <asp:TextBox ID="AddressPostalCode" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    
                    <h4>Phones</h4>
                    <label for="Phone">Phone:</label>
                    <asp:TextBox ID="Phone" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="Fax">Fax:</label>
                    <asp:TextBox ID="Fax" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="FaxValidator" runat="server" ControlToValidate="Fax" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" ValidationGroup="CreateUserWizard1"/>
                    <br />
                    <label for="OtherPhone">Other Phone:</label>
                    <asp:TextBox ID="OtherPhone" runat="server"></asp:TextBox>
                    <elv:PropertyProxyValidator ID="OtherPhoneValidator" runat="server" ControlToValidate="OtherPhone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" ValidationGroup="CreateUserWizard1"/>
                    <br />
                </ContentTemplate>
            </asp:CreateUserWizardStep>
            
            <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                <ContentTemplate>
                    <h2>Thanks for registering with us</h2>
                    <p>Now wait for an email to be sent to the email address you specified with instructions
                    to enable your account and login.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </ContentTemplate>
            </asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>
</asp:Content>
