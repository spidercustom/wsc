﻿using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        public ITemplatedMailerService MailerService { get; set; }

        protected void passwordRecovery_SendingMail(object sender, MailMessageEventArgs e)
        {
            //create message from template
            var templateParams = ConfigHelper.GetMailParams(e.Message.To[0].Address);
            templateParams.MergeVariables.Add("Password", e.Message.Body);

            MailerService.SendMail(MailTemplate.ForgotPassword,templateParams.MergeVariables, templateParams);
            // We handled sending it ourselves, so cancel
            e.Cancel = true;
        }
    }
}
