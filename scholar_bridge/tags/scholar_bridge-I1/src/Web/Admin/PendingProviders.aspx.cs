﻿using System;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Admin
{
    public partial class PendingProviders : System.Web.UI.Page
    {
        public IProviderService ProviderService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            pendingProviders.DataSource = ProviderService.FindPendingProviders();
            pendingProviders.DataBind();
        }
    }
}
