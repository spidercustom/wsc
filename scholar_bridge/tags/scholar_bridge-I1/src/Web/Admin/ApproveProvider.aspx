﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ApproveProvider.aspx.cs" Inherits="ScholarBridge.Web.Admin.ApproveProvider" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Approve/Reject Pending Provider</h3>

    <label for="statusLabel">Status:</label>
    <asp:Label ID="statusLabel" runat="server" />
    <br />

    <label for="nameLabel">Name:</label>
    <asp:Label ID="nameLabel" runat="server" />
    <br />

    <label for="taxIdLabel">TaxId (EIN):</label>
    <asp:Label ID="taxIdLabel" runat="server" />
    <br />
    
    <label for="websiteLabel">Website:</label>
    <asp:HyperLink ID="websiteLabel" runat="server"></asp:HyperLink>
    <br />
    
    <label for="addressLabel">Address:</label>    
    <asp:Label ID="addressLabel" runat="server" />
    <br />
    
    <label for="phoneLabel">Phone:</label>  
    <asp:Label ID="phoneLabel" runat="server" />
    <br />
    
    <label for="faxLabel">Fax:</label>  
    <asp:Label ID="faxLabel" runat="server" />
    <br />
    
    <label for="otherPhoneLabel">Other Phone:</label>  
    <asp:Label ID="otherPhoneLabel" runat="server" />
    <br />

    <label for="existingNotesTb">Administrative Notes:</label>  
    <asp:TextBox ID="existingNotesTb" runat="server" Enabled="false" TextMode="MultiLine" Columns="300" Rows="10"></asp:TextBox><br />
    <label for="existingNotesTb">Add Notes:</label>  
    <asp:TextBox ID="newNotesTb" runat="server" TextMode="MultiLine" Columns="300" Rows="5"></asp:TextBox><br />
    <asp:Button ID="addNotes" runat="server" Text="Add Note" onclick="addNotes_Click"  />
    <br />

    <asp:Button ID="approveButton" runat="server" Text="Approve" onclick="approveButton_Click" />
    <asp:Button ID="rejectButton" runat="server" Text="Deny" onclick="rejectButton_Click" />
</asp:Content>
