﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PendingProviders.aspx.cs" Inherits="ScholarBridge.Web.Admin.PendingProviders" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Scholarship Provider Pending Approval</h3>
    <asp:Repeater ID="pendingProviders" runat="server">
        <HeaderTemplate>
        <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>EIN</th>
            </tr>
        </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="row">
                <td><asp:HyperLink id="linktoProvider" runat="server" NavigateUrl='<%# "~/Admin/ApproveProvider.aspx?id=" + DataBinder.Eval(Container.DataItem, "Id") %>'><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:HyperLink> </as</td>
                <td><%# DataBinder.Eval(Container.DataItem, "TaxId") %></td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="altrow">
                <td><asp:HyperLink id="linktoProvider" runat="server" NavigateUrl='<%# "~/Admin/ApproveProvider.aspx?id=" + DataBinder.Eval(Container.DataItem, "Id") %>'><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:HyperLink> </as</td>
                <td><%# DataBinder.Eval(Container.DataItem, "TaxId") %></td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
        </table>
        </FooterTemplate>
    </asp:Repeater>

</asp:Content>

