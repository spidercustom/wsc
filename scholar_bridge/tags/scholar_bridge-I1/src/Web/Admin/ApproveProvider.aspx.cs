﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Admin
{
    public partial class ApproveProvider : System.Web.UI.Page
    {
        public IProviderService ProviderService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Provider currentProvider;

        public int CurrentProviderId
        {
            get { return Int32.Parse(Request["id"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            currentProvider = ProviderService.Get(CurrentProviderId);

            if (! Page.IsPostBack)
            {
                statusLabel.Text = currentProvider.ApprovalStatus.ToDisplayName();
                nameLabel.Text = currentProvider.Name;
                taxIdLabel.Text = currentProvider.TaxId;
                
                websiteLabel.Text = currentProvider.Website;
                websiteLabel.NavigateUrl = currentProvider.Website;

                addressLabel.Text = currentProvider.Address == null ? null : currentProvider.Address.ToString();
                phoneLabel.Text = currentProvider.Phone == null ? null : currentProvider.Phone.ToString();
                faxLabel.Text = currentProvider.Fax == null ? null : currentProvider.Fax.ToString();
                otherPhoneLabel.Text = currentProvider.OtherPhone == null ? null : currentProvider.OtherPhone.ToString();
                existingNotesTb.Text = currentProvider.AdminNotes;
            }
        }

        protected void approveButton_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var mailParams = ConfigHelper.GetMailParams(currentProvider.AdminUser.Email  );
            ProviderService.ApproveProvider(currentProvider,mailParams );
            ReturnToList();
        }

        protected void rejectButton_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            var mailParams = ConfigHelper.GetMailParams(currentProvider.AdminUser.Email);
            ProviderService.RejectProvider(currentProvider,mailParams );
            ReturnToList();
        }

        protected void addNotes_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            currentProvider.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            ProviderService.Update(currentProvider);
            newNotesTb.Text = null;
            existingNotesTb.Text = currentProvider.AdminNotes;
        }

        private void ReturnToList()
        {
            Response.Redirect("~/Admin/PendingProviders.aspx");
        }
    }
}
