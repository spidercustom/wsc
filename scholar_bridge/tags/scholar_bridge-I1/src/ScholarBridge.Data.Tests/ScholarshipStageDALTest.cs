﻿using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipStageDALTest : TestBase
    {
        public IScholarshipStageDAL ScholarshipStageDAL { get; set; }
        [Test]
        public void Test_Find_By_Id()
        {
            ScholarshipStage stage = ScholarshipStageDAL.FindById(ScholarshipStages.GeneralInformation);
            Assert.IsNotNull(stage);
            Assert.AreEqual(ScholarshipStages.GeneralInformation, stage.Id);
        }
        [Test]
        public void Test_Get_All()
        {
            IList<ScholarshipStage> stages = ScholarshipStageDAL.GetAll();
            Assert.IsNotNull(stages);
            Assert.AreEqual(7, stages.Count);
        }
    }
}
