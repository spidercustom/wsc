using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class ScholarshipAssociationsTests : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }
        public ScholarshipStageDAL ScholarshipStageDAL { get; set; }

        public AdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
        public SupportDAL SupportDAL { get; set; }
        public NeedGapDAL NeedGapDAL { get; set; }

        [Test]
        public void CanAssociateAdditionalReqsWithScholarship()
        {
            var u = UserDAL.FindByUsername("admin");
            var p = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "test", u);
            var s = InsertScholarship(ScholarshipDAL, ScholarshipStageDAL, p, u);

            Assert.IsNotNull(s);

            s.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void CanAssociateNeedGapsWithScholarship()
        {
            var u = UserDAL.FindByUsername("admin");
            var p = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "test", u);
            var s = InsertScholarship(ScholarshipDAL, ScholarshipStageDAL, p, u);

            Assert.IsNotNull(s);

            s.Need.ResetNeedGaps(NeedGapDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void CanAssociateSupportsWithScholarship()
        {
            var u = UserDAL.FindByUsername("admin");
            var p = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "test", u);
            var s = InsertScholarship(ScholarshipDAL, ScholarshipStageDAL, p, u);

            Assert.IsNotNull(s);

            s.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        public static Scholarship InsertScholarship(ScholarshipDAL scholarshipDal, ScholarshipStageDAL stageDal, Provider provider, User user)
        {
            var s = new Scholarship
                                     {
                                         Name = "Test Scholarship 1",
                                         MissionStatement = @"Long big text to describe mission",
                                         Schedule = ScholarshipDALTest.CreateScheduleByMonthDay(),
                                         Provider = provider,
                                         LastUpdate = new ActivityStamp(user),
                                         Stage = stageDal.FindById(ScholarshipStages.GeneralInformation)
                                     };

            return scholarshipDal.Save(s);
        }
    }
}