using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class RoleDALTest : TestBase
    {
        public RoleDAL RoleDAL { get; set; }
        public UserDAL UserDAL { get; set; }

        [Test]
        public void CanCreateRole()
        {
            InsertRole(RoleDAL, UserDAL, "TestRole");
        }

        [Test]
        public void CanGetRoleById()
        {
            var role = InsertRole(RoleDAL, UserDAL, "TestRole");
            var foundRole = RoleDAL.FindById(role.Id);

            Assert.IsNotNull(foundRole);
            Assert.AreEqual(role.Name, foundRole.Name);
        }

        [Test]
        public void CanGetRoleByName()
        {
            var role = InsertRole(RoleDAL, UserDAL, "TestRole");
            var foundRole = RoleDAL.FindByName(role.Name);

            Assert.IsNotNull(foundRole);
            Assert.AreEqual(role.Name, foundRole.Name);
        }

        public static Role InsertRole(IRoleDAL roleDAL, IUserDAL userDAL, string name)
        {
            var user = UserDALTest.InsertUser(userDAL, "RoleTest", "test@spiderlogic.com");
            return InsertRoleWithUser(roleDAL, name, user);
        }

        public static Role InsertRoleWithUser(IRoleDAL roleDAL, string name, User user)
        {
            var role = new Role
                           {
                               Name = name,
                               LastUpdate = new ActivityStamp(user)
                           };

            var createdRole = roleDAL.Insert(role);

            Assert.IsNotNull(createdRole);
            Assert.AreNotEqual(0, createdRole.Id);

            return createdRole;
        }
    }
}