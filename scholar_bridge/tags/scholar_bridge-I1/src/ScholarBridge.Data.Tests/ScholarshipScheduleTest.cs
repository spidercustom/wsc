﻿using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipScheduleTest : TestBase
    {
        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }
        public ScholarshipStageDAL ScholarshipStageDAL { get; set; }

        private User user;
        private Provider provider;
        private ScholarshipStage stage;
        public override void SetUp()
        {
            base.SetUp();
            user = UserDALTest.InsertUser(UserDAL, "ProviderUser", "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            stage = ScholarshipStageDAL.FindById(ScholarshipStages.GeneralInformation);
        }

        [Test]
        public void check_schedule_bymonth_persist_and_can_be_retrived()
        {
            var scholarship = ScholarshipDALTest.CreateTestObject(user, provider, stage);
            scholarship.Schedule = ScholarshipDALTest.CreateScheduleByMonthDay(); ;
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            
            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.AreEqual(typeof(ScholarshipScheduleByMonthDay), retrivedScholarship.Schedule.GetType());
        }

        [Test]
        public void check_schedule_byweek_persist_and_can_be_retrived()
        {
            var scholarship = ScholarshipDALTest.CreateTestObject(user, provider, stage);
            scholarship.Schedule = ScholarshipDALTest.CreateScheduleByMonthDay(); ;
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Schedule.Id, retrivedScholarship.Schedule.Id);
            Assert.AreEqual(typeof(ScholarshipScheduleByMonthDay), retrivedScholarship.Schedule.GetType());
        }

    }
}
