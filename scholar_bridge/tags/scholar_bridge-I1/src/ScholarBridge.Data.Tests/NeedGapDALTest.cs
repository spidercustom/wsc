using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class NeedGapDALTest : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public NeedGapDAL NeedGapDAL { get; set; }

        // private prevents injection from the spring test
        private NeedGap TestNeedGap { get; set; }

        [SetUp]
        public new void SetUp()
        {
            base.SetUp();
            var u = UserDALTest.InsertUser(UserDAL, "Test", "test@example.com");
            TestNeedGap = NeedGapDAL.Insert(new NeedGap()
                                                {
                                                    Name = "Test Need Gap",
                                                    Description = "Desc",
                                                    MinPercent = 0,
                                                    MaxPercent = 0.5f,
                                                    LastUpdate = new ActivityStamp(u)
                                                });

        }

        [Test]
        public void CanGetById()
        {
            var needGap = NeedGapDAL.FindById(TestNeedGap.Id);
            Assert.IsNotNull(needGap);
            Assert.AreEqual(TestNeedGap.Name, needGap.Name);
            Assert.AreEqual(TestNeedGap.Description, needGap.Description);
        }

        [Test]
        public void CanGetAllById()
        {
            var needGaps = NeedGapDAL.FindAll();
            var ids = new List<int>
                          {
                              needGaps[0].Id,
                              needGaps[1].Id,
                          };

            var found = NeedGapDAL.FindAll(ids);

            Assert.IsNotNull(found);
            Assert.AreEqual(2, found.Count);
            CollectionAssert.IsNotEmpty(found);
            CollectionAssert.AllItemsAreNotNull(found);
        }

        [Test]
        public void CanGetByAll()
        {
            var needGaps = NeedGapDAL.FindAll();
            Assert.IsNotNull(needGaps);
            CollectionAssert.IsNotEmpty(needGaps);
            CollectionAssert.AllItemsAreNotNull(needGaps);
        }
    }
}