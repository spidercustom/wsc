﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface INeedGapDAL : IDAL<NeedGap>
    {
        NeedGap FindById(int id);

        IList<NeedGap> FindAll();
        IList<NeedGap> FindAll(IList<int> ids);
    }
}