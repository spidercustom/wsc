using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>ProviderStatus</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class ProviderStatusType : EnumStringType
    {
        public ProviderStatusType()
            : base(typeof(ProviderStatus), 30)
        {
        }
    }
}