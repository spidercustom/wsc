﻿using System.Collections.Generic;
using ScholarBridge.Domain.Location;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class StateDAL : AbstractDAL<State>, IStateDAL
    {
        public State FindByAbbreviation(string abbreviation)
        {
            return UniqueResult("Abbreviation", abbreviation);
        }

        public IList<State> FindAll()
        {
            return FindAll("Abbreviation");
        }
    }
}