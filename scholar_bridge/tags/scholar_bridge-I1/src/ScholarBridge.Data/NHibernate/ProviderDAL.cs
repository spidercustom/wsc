using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class ProviderDAL : AbstractDAL<Provider>, IProviderDAL
    {
        public IList<Provider> FindAllPending()
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("ApprovalStatus", ProviderStatus.PendingApproval))
                .List<Provider>();
        }

        public Provider FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public Provider FindByUser(User user)
        {
            return CreateCriteria()
                .CreateCriteria("Users")
                .Add(Restrictions.Eq("Id", user.Id)).UniqueResult<Provider>();
        }
    }
}