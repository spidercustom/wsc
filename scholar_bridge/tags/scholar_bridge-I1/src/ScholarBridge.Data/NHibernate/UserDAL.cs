﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class UserDAL : AbstractDAL<User>, IUserDAL
    {        
        public User FindByEmail(string email)
        {
            return CreateCriteria().Add(Restrictions.Eq("Email", email)).UniqueResult<User>();
        }

        public User FindByUsername(string email)
        {
            return CreateCriteria().Add(Restrictions.Eq("Username", email)).UniqueResult<User>();
        }

        public User FindByUsernameAndPassword(string username, string password)
        {
            ICriteria criteria = CreateCriteria();
            criteria.Add(Restrictions.Eq("Username", username));
            criteria.Add(Restrictions.Eq("Password", password));
            return criteria.UniqueResult<User>();
        }

        public IList<User> FindAll(int pageIndex, int pageSize)
        {
            ICriteria criteria = CreateCriteria();
            criteria.SetFirstResult(pageSize * pageIndex);
            criteria.SetMaxResults(pageSize);
            return criteria.List<User>();
        }

        public IList<User> FindByEmail(string email, int pageIndex, int pageSize)
        {
            email = email.Replace('*', '%');
            email = email.Replace('?', '_');
            ICriteria criteria = CreateCriteria();
            criteria.Add(Restrictions.Like("Email", email).IgnoreCase());
            criteria.SetFirstResult(pageSize * pageIndex);
            criteria.SetMaxResults(pageSize);
            return criteria.List<User>();
        }

        public IList<User> FindByRoleName(string roleName)
        {
            return CreateCriteria().CreateCriteria("Roles").Add(Restrictions.Eq("Name", roleName)).List<User>();
        }

        public IList<User> FindByRoleNameAndUsernameWildcard(string roleName, string usernameToMatch)
        {
            ICriteria criteria = CreateCriteria();
            criteria.CreateCriteria("Roles").Add(Restrictions.Eq("Name", roleName));
            criteria.Add(Restrictions.Like("Username", usernameToMatch, MatchMode.Anywhere));
            return criteria.List<User>();
        }

        public int FindCountOfUsersOnline()
        {
            ICriteria criteria = CreateCriteria();
            criteria.Add(Restrictions.Eq("IsOnline", true));
            IList<User> onlineUsers = criteria.List<User>();

            return onlineUsers.Count;
        }
    }
}