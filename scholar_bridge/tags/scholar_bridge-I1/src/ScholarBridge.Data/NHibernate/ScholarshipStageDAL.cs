﻿using System;
using System.Collections.Generic;
using Spider.Common.Core.DAL;
using ScholarBridge.Domain;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class ScholarshipStageDAL : AbstractDAL<ScholarshipStage>, IScholarshipStageDAL
    {
        public ScholarshipStage FindById(ScholarshipStages stage)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", stage)).UniqueResult<ScholarshipStage>();
        }

        public IList<ScholarshipStage> GetAll()
        {
            return FindAll("Id");
        }

        
    }
}
