﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class NeedGapDAL : AbstractDAL<NeedGap>, INeedGapDAL
    {
        public NeedGap FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public IList<NeedGap> FindAll()
        {
            return FindAll("Name");
        }

        public IList<NeedGap> FindAll(IList<int> ids)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Id", ids.ToArray()))
                .List<NeedGap>();
        }
    }
}