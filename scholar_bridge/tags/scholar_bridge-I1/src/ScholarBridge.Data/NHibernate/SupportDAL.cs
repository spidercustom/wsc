﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class SupportDAL : AbstractDAL<Support>, ISupportDAL
    {
        public Support FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public IList<Support> FindAll()
        {
            return FindAll("Name");
        }

        public IList<Support> FindAll(SupportType supportType)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("SupportType", supportType))
                .List<Support>();
        }

        public IList<Support> FindAll(IList<int> ids)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Id", ids.ToArray()))
                .List<Support>();
        }
    }
}