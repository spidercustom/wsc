﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class AdditionalRequirementDAL : AbstractDAL<AdditionalRequirement>, IAdditionalRequirementDAL
    {
        public AdditionalRequirement FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public IList<AdditionalRequirement> FindAll()
        {
            return FindAll("Name");
        }

        public IList<AdditionalRequirement> FindAll(IList<int> ids)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Id", ids.ToArray()))
                .List<AdditionalRequirement>();
        }
    }
}