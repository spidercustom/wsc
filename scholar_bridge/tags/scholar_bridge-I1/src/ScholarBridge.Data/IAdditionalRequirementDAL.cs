﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IAdditionalRequirementDAL : IDAL<AdditionalRequirement>
    {
        IList<AdditionalRequirement> FindAll();
        IList<AdditionalRequirement> FindAll(IList<int> ids);

    }
}