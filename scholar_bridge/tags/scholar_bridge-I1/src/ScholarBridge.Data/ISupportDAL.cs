﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface ISupportDAL : IDAL<Support>
    {
        Support FindById(int id);

        IList<Support> FindAll();
        IList<Support> FindAll(SupportType supportType);
        IList<Support> FindAll(IList<int> ids);
    }
}