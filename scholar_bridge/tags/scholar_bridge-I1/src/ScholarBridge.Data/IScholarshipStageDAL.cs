﻿using ScholarBridge.Domain;
using System.Collections.Generic;

namespace ScholarBridge.Data
{
    public interface IScholarshipStageDAL
    {
        ScholarshipStage FindById(ScholarshipStages stage);
        IList<ScholarshipStage> GetAll();
    }
}
