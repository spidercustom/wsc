using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IProviderDAL : IDAL<Provider>
    {
        Provider FindById(int id);
        Provider FindByUser(User user);
        IList<Provider> FindAllPending();
    }
}