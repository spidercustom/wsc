using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(7)]
    public class ChangeUserQAndA : Migration
    {
        /// <summary>
        /// Making Question and Answer nullable
        /// </summary>
        public override void Up()
        {
            Database.ChangeColumn(AddUsers.TABLE_NAME, new Column("Question", DbType.String, 255, ColumnProperty.Null));
            Database.ChangeColumn(AddUsers.TABLE_NAME, new Column("Answer", DbType.String, 255, ColumnProperty.Null));
        }

        public override void Down()
        {
            Database.ChangeColumn(AddUsers.TABLE_NAME, new Column("Question", DbType.String, 255, ColumnProperty.NotNull));
            Database.ChangeColumn(AddUsers.TABLE_NAME, new Column("Answer", DbType.String, 255, ColumnProperty.NotNull));
        }
    }
}