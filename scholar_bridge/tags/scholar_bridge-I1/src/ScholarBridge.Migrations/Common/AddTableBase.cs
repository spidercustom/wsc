﻿using System;
using Migrator.Framework;
using System.Collections.Generic;

namespace ScholarBridge.Migrations.Common
{
    public abstract class AddTableBase : Migration
    {

        public const string TABLE_PREFIX = "SB_";
        public const string LAST_UPDATE_DATE_NAME = "LastUpdateDate";
        public const string LAST_UPDATED_BY_NAME = "LastUpdatedBy";

        public abstract string TableName { get ;}
        public abstract string PrimaryKeyColumn { get; }
        public abstract Column[] CreateColumns();
        public abstract ForeignKey[] CreateForeignKeys();

        private Column[] columns;
        public virtual Column[] Columns
        {
            get
            {
                if (columns == null)
                    columns = CreateColumns();
                return columns;
            }
        }

        ForeignKey[] foreignKeys;
        public virtual ForeignKey[] ForeignKeys
        {
            get
            {
                if (foreignKeys == null)
                    foreignKeys = CreateForeignKeys();
                return foreignKeys;
            }
        }

        public ForeignKey CreateForeignKey(string name, string foreignColumn, AddTableBase primaryTable)
        {
            return new ForeignKey(name, this, foreignColumn, primaryTable);
        }

        public ForeignKey CreateForeignKey(string foreignColumn, AddTableBase primaryTable)
        {
            return new ForeignKey(
                string.Format("FK_{0}_{1}", TableName, foreignColumn),
                this, foreignColumn, primaryTable);
        }

        public ForeignKey CreateForeignKey(string foreignColumn, string primaryTable, string primaryColumn)
        {
            return new ForeignKey(
                string.Format("FK_{0}_{1}", TableName, foreignColumn),
                this, foreignColumn, primaryTable, primaryColumn);
        }

        public virtual void AddTable()
        {
            Database.AddTable(
                TableName, Columns);
        }

        public virtual void RemoveTable()
        {
            Database.RemoveTable(this.TableName);
        }

        public virtual void AddForeignKeys()
        {
            if (this.ForeignKeys == null)
                return;
            for (int index = 0; index < this.ForeignKeys.Length; index++)
            {
                ForeignKey key = this.ForeignKeys[index];
                Database.AddForeignKey(key.Name, key.ForeignTable, key.ForeignColumn, key.PrimaryTable, key.PrimaryColumn);
            }
        }

        public virtual void RemoveForeignKeys()
        {
            if (this.ForeignKeys == null)
                return;
            for (int index = this.ForeignKeys.Length - 1; index > -1; index--)
            {
                ForeignKey key = this.ForeignKeys[index];
                Database.RemoveForeignKey(key.ForeignTable, key.Name);
            }
        }

        public override void Up()
        {
            AddTable();
            AddForeignKeys();
            AddDefaultData();
        }

        public virtual void AddDefaultData()
        {
        }

        public override void Down()
        {
            RemoveForeignKeys();
            RemoveTable();
        }

        protected void InsertAllFieldValue(string[] values)
        {
            if (values.Length != Columns.Length)
                throw new ArgumentException("field and value count do not match");

            string[] columnNames = PrepareColumnNameArray();
            Database.Insert(TableName, columnNames, values);
        }

        private string[] cachedColumnNames = null;
        private string[] PrepareColumnNameArray()
        {
            if (cachedColumnNames != null)
                return cachedColumnNames;
            List<string> columnNames = new List<string>();
            foreach (Column column in Columns)
            {
                columnNames.Add(column.Name);
            }
            cachedColumnNames = columnNames.ToArray();
            return cachedColumnNames;
        }
    }
}
