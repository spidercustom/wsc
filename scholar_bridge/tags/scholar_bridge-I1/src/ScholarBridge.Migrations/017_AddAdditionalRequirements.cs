using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(17)]
    public class AddAdditionalRequirement : Migration
    {
        public const string TABLE_NAME = "SB_AdditionalRequirement";
        public const string PRIMARY_KEY_COLUMN = "Id";

        public static readonly string[] COLUMNS = {
                                                      PRIMARY_KEY_COLUMN, "Name", "Description", "LastUpdatedBy",
                                                      "LastUpdateDate"
                                                  };

        protected static readonly string FK_LAST_UPDATED_BY = string.Format("FK_{0}_LastUpdatedBy", TABLE_NAME);

        public static readonly string[] INSERT_COLUMNS = { "Name", "Description", "LastUpdatedBy" };

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                              new Column(COLUMNS[1], DbType.String, 50, ColumnProperty.NotNull),
                              new Column(COLUMNS[2], DbType.String, 250, ColumnProperty.NotNull),
                              new Column(COLUMNS[3], DbType.Int32, ColumnProperty.NotNull),
                              new Column(COLUMNS[4], DbType.DateTime, ColumnProperty.Null, "(getdate())")
                );

            Database.AddForeignKey(FK_LAST_UPDATED_BY, TABLE_NAME, "LastUpdatedBy", AddUsers.TABLE_NAME,
                                   AddUsers.PRIMARY_KEY_COLUMN);

            int adminId = DataHelper.GetAdminUserId(Database);
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "Interviews", "An in person interview.", adminId.ToString() });
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "Transcripts", "Transcripts one or more previous schools.", adminId.ToString() });
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "Additional Essay(s)", "Additional written responses.", adminId.ToString() });
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "Letters of Recommendation", "Recommendation letters.", adminId.ToString() });
        }

        public override void Down()
        {
            Database.ExecuteNonQuery("DELETE FROM " + TABLE_NAME);
            Database.RemoveForeignKey(TABLE_NAME, FK_LAST_UPDATED_BY);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}