﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;
using System.Data;

namespace ScholarBridge.Migrations
{
    [Migration(13)]
    public class ScholarshipAddSaveContext : AddTableBase
    {
        public const string TABLE_NAME = "SB_ScholarshipStage";
        public const string PRIMARY_KEY_COLUMN = "Id";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return null;
        }


        public override Column[] CreateColumns()
        {
            return new Column[]
            {
                new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKey),
                new Column("Name", DbType.String, 50, ColumnProperty.NotNull), 
            };
        }

        //TODO: check if there is better/easy way to do this
        public override void AddDefaultData()
        {
            InsertAllFieldValue(new string[] { "0", "General Information" });
            InsertAllFieldValue(new string[] { "1", "Seeker Profile" });
            InsertAllFieldValue(new string[] { "2", "Funding Profile" });
            InsertAllFieldValue(new string[] { "3", "Preview Candidate Population" });
            InsertAllFieldValue(new string[] { "4", "Build Scholarship Match Logic" });
            InsertAllFieldValue(new string[] { "5", "Build Scholarship Renewal Criteria" });
            InsertAllFieldValue(new string[] { "6", "Active Scholarship" });
        }

    }
}
