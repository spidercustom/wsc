using System.Security.Cryptography;
using System.Text;

namespace ScholarBridge.Web.Security
{
    /// <summary>
    /// Helper class to generate a machine key for use in the application's config file to 
    /// override the default key used by the provider.
    /// </summary>
    public static class KeyCreator
    {

        /// <summary>
        /// Creates a key based on the indicated size.
        /// </summary>
        /// <param name="keySize">size of the key.</param>
        /// <returns>Generated key of the specified length.</returns>
        public static string CreateKey(int keySize)
        {
            var rng = new RNGCryptoServiceProvider();
            var randomData = new byte[keySize];
            rng.GetBytes(randomData);
            return ToHexString(randomData);
        }

        /// <summary>
        /// Converts the given byte array to a hex string representation.
        /// </summary>
        /// <param name="randomData">the data to convert.</param>
        /// <returns>Hexadecimal string representation of the given byte array./</returns>
        public static string ToHexString(byte[] randomData)
        {
            var hexString = new StringBuilder(64);
            for (int counter = 0; counter < randomData.Length; counter++)
            {
                hexString.Append(string.Format("{0:X2}", randomData[counter]));
            }
            return hexString.ToString();
        }
    }
}