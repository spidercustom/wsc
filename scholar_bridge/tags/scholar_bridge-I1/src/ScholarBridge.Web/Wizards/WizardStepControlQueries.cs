﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace ScholarBridge.Web.Wizards
{
    /// <summary>
    /// Common queries to find out IWizardStepControl
    /// </summary>
    public static class WizardStepControlQueries
    {
        public static IWizardStepControl<T> FindFirstWizardStepControl<T>(ControlCollection controls)
        {
            var wizardStepControls =
                from Control control in controls
                where
                    control is IWizardStepControl<T>
                select control;

            if (wizardStepControls == null)
                return null;

            if (wizardStepControls.Count() == 0)
                return null;

            return wizardStepControls.First() as IWizardStepControl<T>;
        }

        public static IWizardStepControl<T>[] FindWizardStepControls<T>(WizardStepCollection steps)
        {
            List<IWizardStepControl<T>> result = new List<IWizardStepControl<T>>();
            foreach (WizardStepBase step in steps)
            {
                IWizardStepControl<T> wizardStepControl = FindFirstWizardStepControl<T>(step.Controls);
                if (null != wizardStepControl)
                    result.Add(wizardStepControl);
            }
            return result.ToArray();
        }
    }
}
