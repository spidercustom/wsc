﻿
namespace ScholarBridge.Web.Wizards
{
    /// <summary>
    /// This will host the wizard navigation logic and their steps
    /// I would think that, in idea case we should not need any or very few code in 
    /// Provider/BuildScholarship/Default.aspx and Provider/BuildScholarship/GeneralInfo.ascx
    /// related to navigation and wizard specific
    /// </summary>
    public interface IWizardStepsContainer<T>
    {
        IWizardStepControl<T>[] Steps { get; }
        T GetDomainObject();
    }
}
