﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScholarBridge.Web.Exceptions
{
    [global::System.Serializable]
    public class NoProviderIsInContextException : ApplicationException //TODO: Do we need ScholarBridge exception type?
    {
      public NoProviderIsInContextException() { }
      public NoProviderIsInContextException( string message ) : base( message ) { }
      public NoProviderIsInContextException( string message, Exception inner ) : base( message, inner ) { }
      protected NoProviderIsInContextException( 
	    System.Runtime.Serialization.SerializationInfo info, 
	    System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { }
    }
}
