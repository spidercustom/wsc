﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Web.UI;
using System.Collections.Specialized;
using System.Globalization;

namespace ScholarBridge.Web
{
    [
    DefaultProperty("Amount"),
    ToolboxData("<{0}:AmountBox runat=server></{0}:AmountBox>")
    ]
    public class AmountBox : TextBox , IPostBackDataHandler
    {

        #region IPostBackDataHandler Members

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            string amountString = postCollection[postDataKey];
            decimal amount = 0;
            if (!string.IsNullOrEmpty(amountString) && 
                Decimal.TryParse(amountString, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amount)
                )
            {
                Amount = amount;
                return true;
            }
            return false;
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            OnAmountChanged(EventArgs.Empty);
        }

        #endregion

        public decimal Amount { get; set; }
        public override string Text
        {
            get
            {
                string result = this.Amount.ToString("c");
                return result;
            }
        }

        public event EventHandler AmountChanged;
        protected void OnAmountChanged(EventArgs e)
        {
            if (null != AmountChanged)
                AmountChanged(this, e);
        }
    }
}
