using System;
using NUnit.Framework;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Domain;
using Rhino.Mocks;
using ScholarBridge.Data;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class ScholarshipServiceTests
    {
        private MockRepository mocks;
        private IScholarshipDAL scholarshipDAL;
        private IScholarshipStageDAL scholarshipStageDAL;

        private ScholarshipService scholarshipService ;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            scholarshipDAL = mocks.StrictMock<IScholarshipDAL>();
            scholarshipStageDAL = mocks.StrictMock<IScholarshipStageDAL>();
            scholarshipService = new ScholarshipService { ScholarshipDAL = scholarshipDAL, ScholarshipStageDAL = scholarshipStageDAL };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(scholarshipDAL);
            mocks.BackToRecord(scholarshipStageDAL);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetByProviderThrowsExceptionIfNullPassed()
        {
            scholarshipService.GetByProvider(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void GetByProviderThrowsExceptionIfProviderNotApproved()
        {
            scholarshipService.GetByProvider(new Provider { ApprovalStatus = ProviderStatus.PendingApproval });
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveThrowsExceptionIfNullPassed()
        {
            scholarshipService.Save(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SaveThrowsExceptionIfNoProviderAssociated()
        {
            var scholarship = new Scholarship {Provider = null};
            scholarshipService.Save(scholarship);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ProviderNotApprovedException))]
        public void SaveThrowsExceptionIfProviderNotApproved()
        {
            var scholarship = new Scholarship { Provider = new Provider { ApprovalStatus = ProviderStatus.PendingApproval} };
            scholarshipService.Save(scholarship);
            Assert.Fail();
        }

        [Test]
        public void verify_scholarship_exists_by_name_and_provider_returns_true()
        {
            var name = "Scholarship-1";
            var provider = new Provider();
            var scholarship = new Scholarship();
            Expect.Call(scholarshipDAL.FindByNameAndProvider(name, provider)).Return(scholarship);
            mocks.ReplayAll();

            bool exists = scholarshipService.ScholarshipNameExists(name, provider);
            mocks.VerifyAll();

            Assert.IsTrue(exists);
        }

        [Test]
        public void verify_scholarship_exists_by_name_and_provider_returns_false()
        {
            var name = "Scholarship-1";
            var provider = new Provider();
            Expect.Call(scholarshipDAL.FindByNameAndProvider(name, provider)).Return(null);
            mocks.ReplayAll();

            bool exists = scholarshipService.ScholarshipNameExists(name, provider);
            mocks.VerifyAll();

            Assert.IsFalse(exists);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_by_name_and_provider_name_arg_exception()
        {
            scholarshipService.FindByNameAndProvider(string.Empty, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_by_name_and_provider_provider_arg_exception()
        {
            scholarshipService.FindByNameAndProvider("string", null);
        }
    
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_minimum_amount_less_than_zero()
        {
            scholarshipService.Save(new Scholarship{ MinimumAmount = -1});
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_maximum_amount_less_than_one()
        {
            scholarshipService.Save(new Scholarship { MaximumAmount = 0 });
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_maximum_amount_less_than_minimum()
        {
            scholarshipService.Save(new Scholarship { MinimumAmount = 2, MaximumAmount = 1 } );
        }

    }
}