﻿using System;
using System.Net.Mail;
using NUnit.Framework;
using ScholarBridge.Business.Exceptions;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class MailerServiceTests
    {

        private readonly MailerService mailerService = new MailerService();

        [Test]
        public void ValidatesGoodMessage()
        {
            var goodMessage = CreateGoodMessage();
            Assert.IsTrue(mailerService.IsValidMessage(goodMessage));
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SendMailThrowsExceptionIfNullPassed()
        {
            mailerService.SendMail(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ValidationThrowsExceptionIfMessageNull()
        {
            mailerService.IsValidMessage(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(InvalidEmailMessageException))]
        public void ValidationThrowsExceptionIfSubjectIsNull()
        {
            var goodMessage = CreateGoodMessage();
            goodMessage.Subject = null;

            mailerService.IsValidMessage(goodMessage);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(InvalidEmailMessageException))]
        public void ValidationThrowsExceptionIfBodyIsNull()
        {
            var goodMessage = CreateGoodMessage();
            goodMessage.Body = null;

            mailerService.IsValidMessage(goodMessage);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(InvalidEmailMessageException))]
        public void ValidationThrowsExceptionIfToHasNoEmails()
        {
            var goodMessage = CreateGoodMessage();
            goodMessage.To.Clear();

            mailerService.IsValidMessage(goodMessage);
            Assert.Fail();
        }

        [Test(), Ignore("Unable to test because it requires smtp server to be running")]
        public void SendGoodMail()
        {
            mailerService.SendMail("test@scholarbridge.com", "bsingh@spiderlogic.com", "Test message from SendGoodMail", "This is a test message");
        }

        private MailMessage CreateGoodMessage()
        {
            return new MailMessage("foo@bar.com", "bar@foo.com")
            {
                Subject = "Test Message",
                Body = "Hello"
            };
        }
    }
}