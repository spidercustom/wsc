﻿using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        private UserService userService;
        private IUserDAL userDAL;
        private IProviderDAL providerDAL;
        private ITemplatedMailerService mailerService;
        private MockRepository mocks;
        
        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            userDAL = mocks.StrictMock<IUserDAL>();
            providerDAL = mocks.StrictMock<IProviderDAL>();
            mailerService = mocks.StrictMock<ITemplatedMailerService>();
            userService = new UserService  { UserDAL = userDAL, ProviderDAL = providerDAL, MailerService = mailerService };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(userDAL);
            mocks.BackToRecord(providerDAL);
            mocks.BackToRecord(mailerService);
        }

        [Test]
        public void ActivateUser()
        {
             
            var user = new User() { Id = 5 };
            var provider = new Provider {Name = "Provider"};
            var templateParams = new MailTemplateParams {From = "test@example.com", To = "test@example.com", TemplateDirectory = "D:\\Temp"};

            Expect.Call(userDAL.Update(user)).Return(user);
            Expect.Call(providerDAL.FindByUser(user)).Return(provider);
            mailerService.SendMail(Arg<MailTemplate>.Is.Equal(MailTemplate.ProviderApprovalEmailToHECBAdmin),
                Arg<Dictionary<string, string>>.Is.Anything,
                Arg<MailTemplateParams>.Is.Equal(templateParams));

            mocks.ReplayAll();
            
            userService.ActivateUser(user, templateParams);
            Assert.IsTrue(user.IsActive);
            
            mocks.VerifyAll();
        }
    }
}
