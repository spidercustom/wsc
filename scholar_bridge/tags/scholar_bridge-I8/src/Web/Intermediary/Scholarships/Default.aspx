<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Scholarships.Default" Title="Intermediary | Scholarships" %>

<%@ Register TagPrefix="sb" TagName="ScholarshipList" Src="~/Common/ScholarshipList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div>
    <label >Select Organization:</label><asp:DropDownList ID="cboOrganization"  
        runat="server" 
        onselectedindexchanged="cboOrganization_SelectedIndexChanged" 
        AutoPostBack="True"></asp:DropDownList>
</div>
<div class="tabs">
    <ul>
        <li><a href="#notactivated-scholarships-tab"><span>Not Activated</span></a></li>
        <li><a href="#pending-activated-tab"><span>Pending Activation</span></a></li>
        <li><a href="#activated-scholarship-tab"><span>Activated</span></a></li>
        <li><a href="#awarded-scholarship-tab"><span>Awarded</span></a></li>
    </ul>
    <div id="notactivated-scholarships-tab">
       <h3>Not Activated Scholarships</h3>
        <sb:ScholarshipList id="scholarshipNotActivatedList" runat="server" 
            LinkTo="~/Provider/BuildScholarship/Default.aspx" />
    </div>
    <div id="pending-activated-tab">
        <h3>Activated Scholarships</h3>
        <sb:ScholarshipList id="scholarshipPendingList" runat="server" 
            LinkTo="~/Intermediary/Scholarships/Show.aspx" />
    </div>
    <div id="activated-scholarship-tab">
        <h3>Activated Scholarships</h3>
        <sb:ScholarshipList id="scholarshipActivatedList" runat="server" 
            LinkTo="~/Intermediary/Scholarships/Show.aspx" />
    </div>
    <div id="awarded-scholarship-tab">
        <h3>Awarded Scholarships</h3>
        <sb:ScholarshipList id="scholarshipAwardedList" runat="server" 
            LinkTo="~/Intermediary/Scholarships/Show.aspx" />
    </div>
</div>


    
</asp:Content>
