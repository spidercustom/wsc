﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Scholarships.Show" EnableViewState="true" %>
<%@ Register src="~/Common/ShowScholarship.ascx" tagname="ShowScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server"/>

<div id="linkarea">
<sbCommon:ConfirmButton ID="deleteConfirmBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
        Text="Delete Scholarship"  runat="server" onclick="deleteConfirmBtn_Click" />
        
<sbCommon:ConfirmButton ID="confirmRequestActivationBtn" ConfirmMessageDivID ="requestActivationConfirmDiv" 
        Text="Request Activation"  runat="server" onclick="confirmRequestActivationBtn_Click" />
        <br />
        <br />
        
<p>[<asp:HyperLink ID="linkCopy" runat="server">Copy</asp:HyperLink>]</p> 
</div>

<div id="deleteConfirmDiv" title="Confirm delete" style="display:none">
    Deleting the scholarship will remove it from the Scholarships Not Activated tab.
    <br /> <br />Are you sure want to delete?
  </div>   
  <div id="requestActivationConfirmDiv" title="Request Activation" style="display:none">
    Requesting activation will send a message to an authorized agent to activate the scholarship. 
    You can expect the scholarship to be activated in approximately X number of days. 
    Once you request activation, you will not be able to edit the scholarship criteria.
    <br /> <br />
    Are you sure want to continue?
  </div>   

<sb:ShowScholarship id="showScholarship" runat="server" LinkTo="~/Provider/BuildScholarship/Default.aspx"
    ApplicationLinkTo="~/Intermediary/Scholarships/ShowApplication.aspx" />


        
  <asp:ScriptManager ID="ConfirmButtoncriptmanager" runat="server"></asp:ScriptManager>

</asp:Content>