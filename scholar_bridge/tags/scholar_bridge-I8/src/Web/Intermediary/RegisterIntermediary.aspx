﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="RegisterIntermediary.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.RegisterIntermediary"  Title="Register Intermediary" %>

<%@ Register TagPrefix="sb" TagName="RegisterOrganization" Src="~/Common/RegisterOrganization.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:RegisterOrganization id="registerOrg" runat="server" />
</asp:Content>
