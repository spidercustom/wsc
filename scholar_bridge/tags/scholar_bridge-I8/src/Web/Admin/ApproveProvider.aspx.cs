﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin
{
    public partial class ApproveProvider : Page
    {
        public IProviderService ProviderService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Provider currentProvider;

        public int CurrentProviderId
        {
            get { return Int32.Parse(Request["id"]); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            currentProvider = ProviderService.FindById(CurrentProviderId);
            showOrg.Organization = currentProvider;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
			if (currentProvider.AdminNotes != null)
        		notesLiteral.Text = currentProvider.AdminNotes.ToStringTableFormat();
        }

        protected void approveButton_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.Approve(currentProvider);
            ReturnToList();
        }

        protected void rejectButton_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.Reject(currentProvider);
            ReturnToList();
        }

        protected void addNotes_Click(object sender, EventArgs e)
        {
            currentProvider.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            currentProvider.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            ProviderService.Update(currentProvider);
            newNotesTb.Text = null;
        }

        private void ReturnToList()
        {
            Response.Redirect("~/Admin/PendingProviders.aspx");
        }
    }
}
