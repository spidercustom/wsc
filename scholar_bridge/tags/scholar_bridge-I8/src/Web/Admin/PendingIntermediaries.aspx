﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PendingIntermediaries.aspx.cs" Inherits="ScholarBridge.Web.Admin.PendingIntermediaries" Title="Admin | Pending Intermediaries" %>

<%@ Register TagPrefix="sb" TagName="PendingOrgs" Src="~/Admin/PendingOrgs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Scholarship Intermediaries Pending Approval</h3>
    <sb:PendingOrgs id="pendingIntermediaries" runat="server" LinkTo="~/Admin/ApproveIntermediary.aspx" />

</asp:Content>

