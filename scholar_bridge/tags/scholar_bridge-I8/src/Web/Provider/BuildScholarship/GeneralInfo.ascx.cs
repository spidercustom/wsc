﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class GeneralInfo : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IStateDAL StateService { get; set; }
        public IGenericLookupDAL<TermOfSupport> TermOfSupportDAL { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");


            // FIXME: add calendar support
            if (!IsPostBack)
            {
                AddressState.DataSource = StateService.FindAll();
                AddressState.DataTextField = "Name";
                AddressState.DataValueField = "Abbreviation";
                AddressState.DataBind();
                AddressState.Items.Insert(0, new ListItem("- Select One -", ""));

                ScholarshipAcademicYearControl.DataSource = DisplayAcademicYears();
                ScholarshipAcademicYearControl.DataTextField = "DisplayName";
                ScholarshipAcademicYearControl.DataValueField = "Year";
                ScholarshipAcademicYearControl.DataBind();

                PopulateListControl(TermsOfSupportControl, TermOfSupportDAL.FindAll().OrderBy(o => o.Id));
                TermsOfSupportControl.SelectedValue = TermOfSupport.ACADEMIC_YEAR_ID.ToString();

                PopulateScreen();
            }
        }

        private IEnumerable<AcademicYear> DisplayAcademicYears()
        {
            var midyear = ScholarshipInContext.AcademicYear ?? AcademicYear.CurrentScholarshipYear;
            return Enumerable.Range(-2, 6).Select(year => new AcademicYear(year + midyear.Year));
        }

        public override void PopulateObjects()
        {
            ScholarshipInContext.Name = ScholarshipNameControl.Text;
            ScholarshipInContext.AcademicYear = new AcademicYear(Int32.Parse(ScholarshipAcademicYearControl.SelectedValue));
            ScholarshipInContext.MissionStatement = MissionStatementControl.Text;
            ScholarshipInContext.ProgramGuidelines = ProgramGuidelinesControl.Text;
            ScholarshipInContext.Intermediary = IntermediarySelected.SelectedValue;
            ScholarshipInContext.Provider = ProviderSelected.SelectedValue;

            PopulateFundingObjects();
            PopulateScheduleObjects();
            PopulateDonorObject();

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.GeneralInformation)
                ScholarshipInContext.Stage = ScholarshipStages.GeneralInformation;
        }

        private void PopulateScheduleObjects()
        {
            ScholarshipInContext.ApplicationStartDate = calApplicationStartDate.SelectedDate;
            ScholarshipInContext.ApplicationDueDate = calApplicationDueDate.SelectedDate;
            ScholarshipInContext.AwardDate = calAwardDate.SelectedDate;
        }

        private void PopulateFundingObjects()
        {
            ScholarshipInContext.FundingParameters.AnnualSupportAmount = AnnualSupportAmount.Amount;
            ScholarshipInContext.FundingParameters.MinimumNumberOfAwards = (int)MinimumNumberOfAwards.Amount;
            ScholarshipInContext.FundingParameters.MaximumNumberOfAwards = (int)MaximumNumberOfAwards.Amount;

            var selectedTermOfSupportId = Int32.Parse(TermsOfSupportControl.SelectedValue);
            var selectedTermOfSupport = TermOfSupportDAL.FindById(selectedTermOfSupportId);
            ScholarshipInContext.FundingParameters.TermOfSupport = selectedTermOfSupport;

            ScholarshipInContext.MinimumAmount = MinimumAmount.Amount;
            ScholarshipInContext.MaximumAmount = MaximumAmount.Amount;
        }

        private void PopulateDonorObject()
        {
            ScholarshipInContext.Donor.Name = DonorName.Text;

            ScholarshipInContext.Donor.Address.Street = AddressStreet.Text;
            ScholarshipInContext.Donor.Address.Street2 = AddressStreet2.Text;
            ScholarshipInContext.Donor.Address.City = AddressCity.Text;
            ScholarshipInContext.Donor.Address.PostalCode = AddressPostalCode.Text;
            var state = StateService.FindByAbbreviation(AddressState.SelectedValue);
            ScholarshipInContext.Donor.Address.State = state;

            ScholarshipInContext.Donor.Phone = new PhoneNumber(Phone.Text);
            ScholarshipInContext.Donor.Email = EmailAddress.Text;
        }
        
        private void PopulateScreen()
        {
            IntermediarySelected.SelectedValue = ScholarshipInContext.Intermediary;
            ProviderSelected.SelectedValue = ScholarshipInContext.Provider;
            ScholarshipNameControl.Text = ScholarshipInContext.Name;
            int academicYear = null == ScholarshipInContext.AcademicYear
                ? AcademicYear.CurrentScholarshipYear.Year
                : ScholarshipInContext.AcademicYear.Year;
            ScholarshipAcademicYearControl.SelectedValue = academicYear.ToString();
            MissionStatementControl.Text = ScholarshipInContext.MissionStatement;
            ProgramGuidelinesControl.Text = ScholarshipInContext.ProgramGuidelines;

            PopulateScheduleScreen();
            PopulateFundingScreen();
            PopulateDonorScreen();
        }

        private void PopulateScheduleScreen()
        {
            calApplicationStartDate.SelectedDate = ScholarshipInContext.ApplicationStartDate;
            calApplicationDueDate.SelectedDate = ScholarshipInContext.ApplicationDueDate;
            calAwardDate.SelectedDate = ScholarshipInContext.AwardDate;
        }

        private void PopulateFundingScreen()
        {
            var fundingParameters = ScholarshipInContext.FundingParameters;
            if (null != fundingParameters)
            {
                AnnualSupportAmount.Amount = fundingParameters.AnnualSupportAmount;
                MinimumNumberOfAwards.Amount = fundingParameters.MinimumNumberOfAwards;
                MaximumNumberOfAwards.Amount = fundingParameters.MaximumNumberOfAwards;
                if (null != fundingParameters.TermOfSupport)
                    TermsOfSupportControl.SelectedValue = fundingParameters.TermOfSupport.Id.ToString();
            }
            MaximumAmount.Amount = ScholarshipInContext.MaximumAmount;
            MinimumAmount.Amount = ScholarshipInContext.MinimumAmount;
        }

        private void PopulateDonorScreen()
        {
            DonorName.Text = ScholarshipInContext.Donor.Name;

            AddressStreet.Text = ScholarshipInContext.Donor.Address.Street;
            AddressStreet2.Text = ScholarshipInContext.Donor.Address.Street2;
            AddressCity.Text = ScholarshipInContext.Donor.Address.City;
            AddressPostalCode.Text = ScholarshipInContext.Donor.Address.PostalCode;
            if (null != ScholarshipInContext.Donor.Address.State)
                AddressState.SelectedValue = ScholarshipInContext.Donor.Address.State.Abbreviation;
            if (null != ScholarshipInContext.Donor.Phone)
                Phone.Text = ScholarshipInContext.Donor.Phone.Number;
            EmailAddress.Text = ScholarshipInContext.Donor.Email;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.GeneralInformation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.GeneralInformation;
        }

        public override bool ValidateStep()
        {
            bool result = true;

            var foundScholarship = ScholarshipService.ScholarshipExists(ProviderSelected.SelectedValue, ScholarshipNameControl.Text, Int32.Parse(ScholarshipAcademicYearControl.SelectedValue));
            if (null != foundScholarship && foundScholarship.Id != ScholarshipInContext.Id)
            {
                result &= false;
                ScholarshipNameValidator.IsValid = false;
                ScholarshipNameValidator.Text = "Scholarship in the selected Academic Year with this Name already exists";
            }
            return result;
        }

        public void PopulateListControl(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }

        protected void providerValidator_OnValueConvert(object sender, ValueConvertEventArgs e)
        {
            if (null != ProviderSelected.SelectedValue)
            {
                e.ConvertedValue = ProviderSelected.SelectedValue.Name;
            }
            else
            {
                e.ConversionErrorMessage = "Provider must be specified.";
                e.ConvertedValue = null;
            }
        }
    }
}
