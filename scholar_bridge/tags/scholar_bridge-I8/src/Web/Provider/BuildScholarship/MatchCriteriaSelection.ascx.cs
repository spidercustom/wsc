﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;


namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class MatchCriteriaSelection : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                SeekerStudentTypeControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.StudentGroup,
                    SeekerProfileAttribute.SchoolType,
                    SeekerProfileAttribute.AcademicProgram,
                    SeekerProfileAttribute.SeekerStatus,
                    SeekerProfileAttribute.ProgramLength,
                    SeekerProfileAttribute.College);
                SeekerStudentTypeControl.RepeaterControl.DataBind();

                SeekerDemographicsPersonalControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.FirstGeneration,
                    SeekerProfileAttribute.Ethnicity,
                    SeekerProfileAttribute.Religion,
                    SeekerProfileAttribute.Gender);
                SeekerDemographicsPersonalControl.RepeaterControl.DataBind();

                SeekerDemographicsGeographicControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SeekerGeographicsLocation);
                SeekerDemographicsGeographicControl.RepeaterControl.DataBind();

                SeekerInterestsControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.AcademicArea,
                    SeekerProfileAttribute.Career,
                    SeekerProfileAttribute.CommunityService,
                    SeekerProfileAttribute.OrganizationAndAffiliationType);
                SeekerInterestsControl.RepeaterControl.DataBind();

                SeekerActivitiesControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.SeekerHobby,
                    SeekerProfileAttribute.Sport,
                    SeekerProfileAttribute.Club,
                    SeekerProfileAttribute.WorkType,
                    SeekerProfileAttribute.WorkHour,
                    SeekerProfileAttribute.ServiceType,
                    SeekerProfileAttribute.ServiceHour);
                SeekerActivitiesControl.RepeaterControl.DataBind();

                SeekerStudentPerformanceControl.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(
                    SeekerProfileAttribute.GPA,
                    SeekerProfileAttribute.ClassRank,
                    SeekerProfileAttribute.SAT,
                    SeekerProfileAttribute.ACT,
                    SeekerProfileAttribute.Honor,
                    SeekerProfileAttribute.APCreditsEarned,
                    SeekerProfileAttribute.IBCreditsEarned);
                SeekerStudentPerformanceControl.RepeaterControl.DataBind();
                
                FundingSpecifications.RepeaterControl.DataSource = typeof(FundingProfileAttribute).GetKeyValue();
                FundingSpecifications.RepeaterControl.DataBind();

                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {
            PopulateSeekerProfileCriteriaAttributesScreen();
            PopulateFundingAttributesScreen();
        }

        private void PopulateSeekerProfileCriteriaAttributesScreen()
        {
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerStudentTypeControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerDemographicsPersonalControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerDemographicsGeographicControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerInterestsControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerActivitiesControl);
            PopulateSeekerProfileCriteriaSectionAttributesScreen(SeekerStudentPerformanceControl);
        }

        public void PopulateSeekerProfileCriteriaSectionAttributesScreen(AttributeSelectionControl attributeSelectionControl)
        {
            attributeSelectionControl.
               ForEachRepeaterItem(usageButtonGroup =>
               {
                   var attribute = (SeekerProfileAttribute)usageButtonGroup.AttributeEnumInt;

                   var attributeUsage = ScholarshipInContext.SeekerProfileCriteria.AttributesUsage.
                       FirstOrDefault(o => o.Attribute == attribute);

                   usageButtonGroup.Value =
                       null == attributeUsage ?
                       ScholarshipAttributeUsageType.NotUsed :
                       attributeUsage.UsageType;
               });
        }

        private void PopulateFundingAttributesScreen()
        {
            FundingSpecifications.
				ForEachRepeaterItem(usageButtonGroup =>
				                    	{
											var attribute = (FundingProfileAttribute) usageButtonGroup.AttributeEnumInt;

											var attributeUsage = ScholarshipInContext.FundingProfile.AttributesUsage.
												FirstOrDefault(o => o.Attribute == attribute);

											usageButtonGroup.Value = 
												null == attributeUsage ? 
												ScholarshipAttributeUsageType.NotUsed : 
												attributeUsage.UsageType;
				                    	});

        }

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override void PopulateObjects()
        {
            PopulateSeekerProfileCriteriaAttributesObject();
            PopulateFundingAttributesObject();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.MatchCriteriaSelection)
                ScholarshipInContext.Stage = ScholarshipStages.MatchCriteriaSelection;
        }

        private void PopulateFundingAttributesObject()
        {
            ScholarshipInContext.FundingProfile.AttributesUsage.Clear();
            
            foreach (RepeaterItem repeaterItem in FundingSpecifications.RepeaterControl.Items)
            {
            	AttributeSelectionControl.AttributeUsageButtonGroup buttonGroup = BuildButtonGroup(repeaterItem);
				if (!buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
                {

					var attribute = (FundingProfileAttribute)buttonGroup.AttributeEnumInt;
                    var attributeUsage = new FundingProfileAttributeUsage
                                             {
                                                 Attribute = attribute,
												 UsageType = buttonGroup.Value
                                             };
                    ScholarshipInContext.FundingProfile.AttributesUsage.Add(attributeUsage);
                }
            }
        }

        private void PopulateSeekerProfileCriteriaAttributesObject()
        {
            ScholarshipInContext.SeekerProfileCriteria.AttributesUsage.Clear();
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerStudentTypeControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerDemographicsPersonalControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerDemographicsGeographicControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerInterestsControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerActivitiesControl);
            PopulateSeekerProfileCriteriaSectionAttributesObject(SeekerStudentPerformanceControl); 
        }

        private void PopulateSeekerProfileCriteriaSectionAttributesObject(AttributeSelectionControl attributeSelectionControl)
        {
            foreach (RepeaterItem repeaterItem in attributeSelectionControl.RepeaterControl.Items)
            {
                AttributeSelectionControl.AttributeUsageButtonGroup buttonGroup = BuildButtonGroup(repeaterItem);
                if (!buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
                {
                    var attribute = (SeekerProfileAttribute)buttonGroup.AttributeEnumInt;
                    var attributeUsage = new SeekerProfileAttributeUsage
                                             {
                                                 Attribute = attribute,
                                                 UsageType = buttonGroup.Value
                                             };
                    ScholarshipInContext.SeekerProfileCriteria.AttributesUsage.Add(attributeUsage);
                }
            }
        }

        private static AttributeSelectionControl.AttributeUsageButtonGroup BuildButtonGroup(Control repeaterItem)
		{
			var usageMinimumRB = (RadioButton)repeaterItem.FindControl("Minimum");
			var usagePreferenceRB = (RadioButton)repeaterItem.FindControl("Preference");
			var usageNotUsedRB = (RadioButton)repeaterItem.FindControl("Minimum");
			var attributeControl = (HtmlInputHidden)repeaterItem.FindControl("KeyControl");
			return new AttributeSelectionControl.AttributeUsageButtonGroup(Int32.Parse(attributeControl.Value), usageMinimumRB, usagePreferenceRB, usageNotUsedRB);
		}

		public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.MatchCriteriaSelection;
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.MatchCriteriaSelection;
        }
    }
}