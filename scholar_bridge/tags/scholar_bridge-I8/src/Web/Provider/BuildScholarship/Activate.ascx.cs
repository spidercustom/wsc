﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Activate : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (! Page.IsPostBack)
                PopulateScreen();
        }

        private void PopulateScreen()
        {
            if (null != ScholarshipInContext.AdditionalQuestions &&
                ScholarshipInContext.AdditionalQuestions.Count > 0)
            {
            }
        }

        public override void PopulateObjects()
        {
            
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.RequestedActivation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.RequestedActivation;
        }

        public override bool ValidateStep()
        {
            var result = Validation.Validate(ScholarshipInContext);
            return result.IsValid;
        }

        protected void confirmRequestActivationBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (e.DialogResult)
            {
                ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                if (ScholarshipInContext.CanSubmitForActivation())
                {
                    ScholarshipService.RequestActivation(ScholarshipInContext);
                    SuccessMessageLabel.SetMessage("Scholarship submitted for activation.");
                    Response.Redirect("~/Provider/Scholarships/Default.aspx#pending-activated-tab");
                }
                else
                {
                    ClientSideDialogs.ShowAlert("<p>Scholarship is either already in activation process or is already actived/rejected</p>", "Cannot Send Activation Request!");
                }
            }
        }
    }
}
