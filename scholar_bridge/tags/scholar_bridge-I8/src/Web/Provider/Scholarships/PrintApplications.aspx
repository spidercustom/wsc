﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PrintApplications.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.PrintApplications" Title="Applications | Print" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
$(document).ready(function() {
    $("#<%= printApplications.ClientID %>").change(function() {
        var radios = "#<%= applicationChoice.ClientID %> :input";
        if ($(this).is(':checked')) {
            $(radios).removeAttr('disabled');
        } else {
            $(radios).attr('disabled', true);
        }  
    });
});
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PrintViewHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<asp:Panel ID="printChoice" runat="server">
    <input type="hidden" name="print" value="true" /><br />
    <asp:CheckBox ID="printList" runat="server" Text="Print list of Applicants" /><br />
    <asp:CheckBox ID="printApplications" runat="server" Text="Print Applications" Checked="true"/>
    <asp:RadioButtonList ID="applicationChoice" runat="server">
        <asp:ListItem  Text="Finalists" Value="finalists" Selected="True" />
        <asp:ListItem  Text="All" Value="all" />
    </asp:RadioButtonList>
    </ul>
    <asp:Button ID="printBtn" runat="server" Text="Print" OnClick="print_OnClick"/>
</asp:Panel>

<asp:Panel ID="printContent" runat="server" Visible="false">

</asp:Panel>

</asp:Content>
