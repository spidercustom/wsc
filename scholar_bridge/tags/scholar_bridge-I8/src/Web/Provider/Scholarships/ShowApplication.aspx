﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ShowApplication.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.ShowApplication" Title="Scholarship | Application | Show" %>
<%@ Register src="~/Common/ShowApplication.ascx" tagname="ShowApplication" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" />

<sb:ShowApplication id="showApplication" runat="server" 
    LinkTo="~/Seeker/BuildApplication/Default.aspx" 
    ScholarshipLinkTo="~/Provider/Scholarships/Show.aspx"/>
</asp:Content>
