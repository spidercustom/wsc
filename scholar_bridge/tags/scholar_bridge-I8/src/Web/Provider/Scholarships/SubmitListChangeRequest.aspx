<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SubmitListChangeRequest.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.SubmitListChangeRequest" Title="Submit List Change" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<%@ Register src="../../Common/ListChangeRequest.ascx" tagname="ListChangeRequest" tagprefix="uc1" %>
             
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<uc1:ListChangeRequest ID="ListChangeRequest1" runat="server" />
 
<asp:Button ID="saveBtn" runat="server" Text="Save"  
        onclick="saveBtn_Click" /> 
<asp:Button ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" 
        onclick="cancelBtn_Click" />
    
</asp:Content>
