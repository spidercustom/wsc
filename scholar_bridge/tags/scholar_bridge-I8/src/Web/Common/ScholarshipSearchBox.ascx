﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipSearchBox.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipSearchBox" %>
<asp:Panel ID="pnlSearch" runat="server"  class="searchbox" 
    DefaultButton="SearchBtn" >

<asp:TextBox ID="txtSearch" runat="server" Width="200px" CssClass="ui-corner-all" Font-Italic="true" ></asp:TextBox>
   <asp:ImageButton ID="SearchBtn" CssClass="ui-corner-all" runat="server" 
        ImageUrl="~/images/magnifier.png" AlternateText="Go" ImageAlign="Middle" 
        Height="24px" Width="24px" onclick="SearchBtn_Click"  />
</asp:Panel>
