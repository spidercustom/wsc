﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    
    public partial class SeekerProfileProgress: UserControl
    {
         
        public UserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            var seeker = UserContext.CurrentSeeker;
            if (!(seeker == null))
            {

            var percent = seeker.GetPercentComplete();
            
                lblPercent.Text = string.Format("  Your profile is {0}% complete", percent.ToString());
                var page = HttpContext.Current.CurrentHandler as Page;
                if (page != null)
                    page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), this.ClientID + "_script",GetJS(percent));
           
            } else
            {
                this.Visible = false;

            }
        }
         
        private string GetJS(int percent)
        {
            var script = "$(function() {$('#progressbar').progressbar({value: "+percent.ToString()+ "});});";
            return script;
        }
        
       
    }
}