﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowAllApplications.ascx.cs" Inherits="ScholarBridge.Web.Common.ShowAllApplications" %>
<%@ Register src="~/Common/ShowApplication.ascx" tagname="ShowApplication" tagprefix="sb" %>

<asp:Repeater ID="allApplications" runat="server">
<ItemTemplate>
    <sb:ShowApplication id="applicationShow" runat="server" ApplicationToView="<%# Container.DataItem %>" />
</ItemTemplate>
<SeparatorTemplate><br /></SeparatorTemplate>
</asp:Repeater>
