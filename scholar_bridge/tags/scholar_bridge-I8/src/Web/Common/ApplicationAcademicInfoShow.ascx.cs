﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationAcademicInfoShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                if (null != ApplicationToView.CurrentSchool)
                {
                    studentType.Text = ApplicationToView.CurrentSchool.LastAttended.GetDisplayName();
                    highSchool.Text = ApplicationToView.CurrentSchool.HighSchoolOther ??
                                      ApplicationToView.CurrentSchool.HighSchool.Name;
                    schoolDistrict.Text = ApplicationToView.CurrentSchool.SchoolDistrictOther ??
                                          ApplicationToView.CurrentSchool.SchoolDistrict.Name;
                    currentCollege.Text = ApplicationToView.CurrentSchool.CollegeOther ??
                                          ApplicationToView.CurrentSchool.College.Name;
                }

                if (null != ApplicationToView.SeekerAcademics)
                {
                    if (ApplicationToView.SeekerAcademics.GPA.HasValue)
                        gpa.Text = ApplicationToView.SeekerAcademics.GPA.Value.ToString("0.0000");

                    if (ApplicationToView.SeekerAcademics.ClassRank.HasValue)
                        classRank.Text = ApplicationToView.SeekerAcademics.ClassRank.Value.ToString();

                    if (null != ApplicationToView.SeekerAcademics.SATScore)
                    {
                        var sat = ApplicationToView.SeekerAcademics.SATScore;
                        if (sat.CriticalReading.HasValue)
                            satReading.Text = sat.CriticalReading.Value.ToString();
                        if (sat.Writing.HasValue)
                            satWriting.Text = sat.Writing.Value.ToString();
                        if (sat.Mathematics.HasValue)
                            satMath.Text = sat.Mathematics.Value.ToString();
                    }

                    if (null != ApplicationToView.SeekerAcademics.ACTScore)
                    {
                        var act = ApplicationToView.SeekerAcademics.ACTScore;
                        if (act.Reading.HasValue)
                            actReading.Text = act.Reading.Value.ToString();
                        if (act.English.HasValue)
                            actEnglish.Text = act.English.Value.ToString();
                        if (act.Mathematics.HasValue)
                            actMath.Text = act.Mathematics.Value.ToString();
                        if (act.Science.HasValue)
                            actScience.Text = act.Science.Value.ToString();
                    }

                    honors.Text = ApplicationToView.SeekerAcademics.Honors ? "Yes" : "No";

                    if (ApplicationToView.SeekerAcademics.APCredits.HasValue)
                        apCredits.Text = ApplicationToView.SeekerAcademics.APCredits.Value.ToString();
                    if (ApplicationToView.SeekerAcademics.IBCredits.HasValue)
                        ibCredits.Text = ApplicationToView.SeekerAcademics.IBCredits.Value.ToString();

                    academicProgram.Text = String.Join(", ", ApplicationToView.SeekerAcademics.AcademicPrograms.GetDisplayNames());
                    enrollmentStatus.Text = String.Join(", ", ApplicationToView.SeekerAcademics.SeekerStatuses.GetDisplayNames());
                    lengthOfProgram.Text = String.Join(", ", ApplicationToView.SeekerAcademics.ProgramLengths.GetDisplayNames());
                    schoolsConsidering.Text = String.Join(", ", ApplicationToView.SeekerAcademics.SchoolTypes.GetDisplayNames());

                    collegesApplied.DataSource = ApplicationToView.SeekerAcademics.CollegesApplied;
                    collegesApplied.DataBind();
                    collegesAppliedOther.Text = ApplicationToView.SeekerAcademics.CollegesAppliedOther;

                    collegesAccepted.DataSource = ApplicationToView.SeekerAcademics.CollegesAccepted;
                    collegesAccepted.DataBind();
                    collegesAcceptedOther.Text = ApplicationToView.SeekerAcademics.CollegesAcceptedOther;
                }


                firstGeneration.Text = ApplicationToView.FirstGeneration ? "Yes" : "No";

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);

            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            FirstGenerationRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.FirstGeneration);

            StudentGroupRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.StudentGroup);
            SchoolDistrictsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.SchoolDistrict;
            HighSchoolsRow.Visible = seeker.MatchLocationCriteria.StateDependentLocation == StateDependentLocation.HighSchool;

            SchoolTypesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SchoolType);
            AcademicProgramsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.AcademicProgram);
            EnrollmentStatusesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SeekerStatus);
            LengthOfProgrammRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ProgramLength);
            CollegesRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.College);


            GPARow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.GPA);
            SATScoreRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SAT);
            ACTScoreRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ACT);
            ClassRankRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.ClassRank);
            APCreditsEarnedRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.APCreditsEarned);
            IBCreditsEarnedRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.IBCreditsEarned);
            HonorsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Honor);
            ApplicantStudentPerformance.Visible = ApplicantStudentPerformance.Visible =
                GPARow.Visible ||
                SATScoreRow.Visible ||
                ACTScoreRow.Visible ||
                ClassRankRow.Visible ||
                APCreditsEarnedRow.Visible ||
                IBCreditsEarnedRow.Visible ||
                HonorsRow.Visible;
        }

        public override ApplicationStages Stage
        {
            get { return ApplicationStages.AcademicInfo; }
        }
    }
}