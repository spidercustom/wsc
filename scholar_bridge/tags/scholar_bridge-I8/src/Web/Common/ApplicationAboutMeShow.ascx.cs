﻿using System;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationAboutMeShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {

                personalStatement.Text = LineBreakFormat(ApplicationToView.PersonalStatement);
                myChallenge.Text = LineBreakFormat(ApplicationToView.MyChallenge);
                myGift.Text = LineBreakFormat(ApplicationToView.MyGift);

                fiveWords.DataSource = ApplicationToView.Words;
                fiveWords.DataBind();

                fiveSkills.DataSource = ApplicationToView.Skills;
                fiveSkills.DataBind();

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);
            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            FiveWordsRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SeekerVerbalizingWord);
            FiveSkillRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.SeekerSkill);
        }

        private static string LineBreakFormat(string value)
        {
            if (! String.IsNullOrEmpty(value))
            {
                value.Replace("\r\n\r\n", "<br/>\r\n");
            }

            return value;
        }

        public override ApplicationStages Stage
        {
            get { return ApplicationStages.AboutMe; }
        }
    }
}