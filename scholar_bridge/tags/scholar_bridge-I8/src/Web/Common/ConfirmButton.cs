﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ConfirmButton runat=server></{0}:ConfirmButton>")]
    public class ConfirmButton : WebControl, IPostBackEventHandler
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue(true)]
        [Localizable(true)]
        public bool NeedsConfirmation
        {
            get
            {
                var nc = ViewState["NeedsConfirmation"];
                if (null == nc)
                    return true;
                return (bool)nc;
            }

            set
            {
                ViewState["NeedsConfirmation"] = value;
            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue("")]
        public string ConfirmMessageDivID
        {
            get
            {
                var s = (String)ViewState["ConfirmMessageDivID"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["ConfirmMessageDivID"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptInclude(GetType(), "DEFAULT", ResolveUrl("~/js/ConfirmButton.js"));
            base.OnPreRender(e);
        }

        private const string ON_CLICK_SCRIPT_TEMPLATE = "javascript:ConfirmYesNoDialog('{0}', '{1}');";
        protected override void Render(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(ConfirmMessageDivID))
                throw new ArgumentNullException("ConfirmMessageDivID property cannot be left empty");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);

            if (NeedsConfirmation)
                writer.AddAttribute(HtmlTextWriterAttribute.Onclick, ON_CLICK_SCRIPT_TEMPLATE.Build(ConfirmMessageDivID, UniqueID));

            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
            writer.WriteLine();
        }

        public event EventHandler<ConfirmButtonClickEventArgs> Click;

        public void RaisePostBackEvent(string eventArgument)
        {
            if (null != Click)
            {
                var result = bool.Parse(eventArgument);
                var args = new ConfirmButtonClickEventArgs { DialogResult = result };
                Click(this, args);
            }
        }
    }

    public class ConfirmButtonClickEventArgs : EventArgs
    {
        public bool DialogResult { get; set; }    
    }
}
