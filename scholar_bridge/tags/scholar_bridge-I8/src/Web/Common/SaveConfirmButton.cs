﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Common
{
	/// <summary>
	/// This control renders as a button which checks (client-side) to see if the data on the page has changed and, if so,
	/// prompts the user to save.
	/// 
	/// We're using this button for a second purpose.  asp:button doesn't go through standard post-back in Safari
	/// so we can't catch the name of the button in the DataChange js.  Using this control DOES go thru standard 
	/// postback, so we can catch the control name and skip prompting.  For this usage, we set the NeedsConfirmation
	/// flag to false which causes the button to go straight to post-back.
	/// </summary>
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:SaveConfirmButton runat=server></{0}:SaveConfirmButton>")]
    public class SaveConfirmButton : WebControl, IPostBackEventHandler
    {
		/// <summary>
		/// Using this property to determine if the confirm pop-up is required.
		/// 
		/// </summary>
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue(true)]
        [Localizable(true)]
        public bool NeedsConfirmation
        {
            get
            {
                var nc = ViewState["NeedsConfirmation"];
                if (null == nc)
                    return true;
                return (bool)nc;
            }

            set
            {
                ViewState["NeedsConfirmation"] = value;
            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var s = (String)ViewState["Text"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        [Category("Appearance")]
        [DefaultValue("")]
        public string ConfirmMessageDivID
        {
            get
            {
                var s = (String)ViewState["ConfirmMessageDivID"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["ConfirmMessageDivID"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptInclude(GetType(), "DEFAULT", ResolveUrl("~/js/SaveConfirmButton.js"));
            base.OnPreRender(e);
        }

		private const string ON_CLICK_SCRIPT_TEMPLATE = "javascript:SavingYesNoCancelDialog('{0}', '{1}', '{2}');";
		private const string ON_CLICK_SCRIPT_TEMPLATE_SAVE_ONLY = "javascript:SaveAndContinue('{0}');";
		protected override void Render(HtmlTextWriter writer)
        {
			if (string.IsNullOrEmpty(ConfirmMessageDivID) && NeedsConfirmation)
                throw new ArgumentException("ConfirmMessageDivID property cannot be left empty if NeedsConfirmation is true");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "button");
			writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);
			if (!string.IsNullOrEmpty(ToolTip))
				writer.AddAttribute(HtmlTextWriterAttribute.Title, ToolTip);

            if (NeedsConfirmation)
                writer.AddAttribute(HtmlTextWriterAttribute.Onclick, ON_CLICK_SCRIPT_TEMPLATE.Build(ConfirmMessageDivID, UniqueID, ValidateEvenWhenNoChange));
            else
            {
            	writer.AddAttribute(HtmlTextWriterAttribute.Onclick, ON_CLICK_SCRIPT_TEMPLATE_SAVE_ONLY.Build(UniqueID));
            }
            
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
            writer.WriteLine();
        }

        public bool ValidateEvenWhenNoChange { get; set; }

        public event EventHandler<SaveConfirmButtonClickEventArgs> Click;

        public void RaisePostBackEvent(string eventArgument)
        {
            if (null != Click)
            {
                Page.Validate();
                var saveAndContinue = bool.Parse(eventArgument);
                var args = new SaveConfirmButtonClickEventArgs {SaveBeforeContinue = saveAndContinue};
                Click(this, args);
            }
        }
    }

    public class SaveConfirmButtonClickEventArgs : EventArgs
    {
        public bool SaveBeforeContinue { get; set; }    
    }
}
