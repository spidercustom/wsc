﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class Login : UserControl
    {
        public IUserService UserService { get; set; }

        private System.Web.UI.WebControls.Login LoginControl
        {
            get { return ((System.Web.UI.WebControls.Login) loginView.FindControl("Login1")); }
        }

        private string Username
        {
            get { return LoginControl.UserName; }
        }

        private void SetDestinationUrl(string url)
        {
            LoginControl.DestinationPageUrl = url;
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            if (null == Request["ReturnUrl"])
            {
                SetDestinationUrl(DestinationForUser(Username));
            }
        }

        public string DestinationForUser(string username)
        {
            var roles = new List<string>(Roles.GetRolesForUser(username));
            if (roles.Any(r => Role.PROVIDER_ROLE.Equals(r)))
            {
                return "~/Provider/";
            }
            if (roles.Any(r => Role.INTERMEDIARY_ROLE.Equals(r)))
            {
                return "~/Intermediary/";
            }
            if (roles.Any(r => Role.WSC_ADMIN_ROLE.Equals(r)))
            {
                return "~/Message/";
            }
            if (roles.Any(r => Role.SEEKER_ROLE.Equals(r)))
            {
                return "~/Seeker/";
            }

            return "~/";
        }

        protected void Login1_LoginError(object sender, EventArgs e)
        {
            var u = UserService.FindByEmail(Username);
            if (null != u)
            {
                // Just want to check a couple of properties to see why the user can't login
                // Otherwise just use the default error
                if (! u.IsApproved)
                {
                    LoginControl.FailureText = "You have not yet been approved by the administrators of the site. <br/> You will receive an email when you are approved and can login.";
                }
            }
        }
    }
}
