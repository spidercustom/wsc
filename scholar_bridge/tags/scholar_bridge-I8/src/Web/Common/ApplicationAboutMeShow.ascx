﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationAboutMeShow.ascx.cs" Inherits="ScholarBridge.Web.Common.ApplicationAboutMeShow" %>

<div>
<div class="viewLabel">Personal Statement:</div> <div class="viewValue"><asp:Literal ID="personalStatement" runat="server" /></div>
<br />
</div>

<div>
<div class="viewLabel">My Challenge:</div> <div class="viewValue"><asp:Literal ID="myChallenge" runat="server" /></div>
<br />
</div>

<div>
<div class="viewLabel">My Gift:</div> <div class="viewValue"><asp:Literal ID="myGift" runat="server" /></div>
<br />
</div>

<div id="FiveWordsRow" runat="server">
<div class="viewLabel">5 Words:</div>
<div class="viewValue">
<asp:Repeater ID="fiveWords" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<br />
</div>

<div id="FiveSkillRow" runat="server">
<div class="viewLabel">5 Skills:</div>
<div class="viewValue">
<asp:Repeater ID="fiveSkills" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<br />
</div>

<br />
