﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipSearchBox : UserControl
    {
        private const string RESULTS_PAGE = "~/Seeker/Scholarships/SearchResults.aspx?val={0}";

        public string Criteria { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (page != null)
            {
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), txtSearch.ClientID + "_script", GetJS());
            }
            if (!IsPostBack)
                txtSearch.Text = Criteria;
        }

        private string GetJS()
        {
            return "$(function() {searchBoxFocus('" + txtSearch.ClientID + "'); });";
        }

        protected void SearchBtn_Click(object sender, ImageClickEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            string value = txtSearch.Text;
            Response.Redirect(ResolveUrl(RESULTS_PAGE.Build(value)));
        }
    }
}