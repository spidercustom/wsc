﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ShowAllApplications : UserControl
    {
        public IList<Application> Applications { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            allApplications.DataSource = Applications;
            allApplications.DataBind();
        }
    }
}