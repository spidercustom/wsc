﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipSearchResults : UserControl
    {
        private IList<Scholarship> _Scholarships;
        public string LinkTo { get; set; }
        public IUserContext UserContext { get; set; }
        public IMatchService MatchService { get; set; }
        public IScholarshipService ScholarshipService { get; set; }

        public IList<Scholarship> Scholarships
        {
            get { return _Scholarships; }
            set { _Scholarships = value;
                Bind();
            }
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            lbladdto.Visible = (!(UserContext.CurrentSeeker == null));
            Bind();
        }

        protected void Bind()
        {
            if (!(Scholarships == null))
            {
                lstScholarships.DataSource = (from s in Scholarships orderby s.Name select s).ToList();
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship) ((ListViewDataItem) e.Item).DataItem);

                var link = (LinkButton)e.Item.FindControl("linkBtn");
                if(!(UserContext.CurrentSeeker == null))
                    link.CommandArgument = scholarship.Id.ToString();

                else 
                    link.Enabled = false;
    
                var provider = (Literal)e.Item.FindControl("lblProvider");
                provider.Text = String.IsNullOrEmpty(scholarship.Provider.Name) ? scholarship.Intermediary.Name : scholarship.Provider.Name;
            }
        }
        
        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }

        protected void linkBtn_Click(object sender, EventArgs e)
        {
            var link = (LinkButton) sender;
            var scholarshipId = int.Parse(link.CommandArgument);
            var scholarship = ScholarshipService.GetById(scholarshipId);
            var seeker = UserContext.CurrentSeeker;
            var match = MatchService.GetMatch(seeker, scholarshipId);

            if (!(match == null))
            {
                ClientSideDialogs.ShowAlert(string.Format("Scholarship {0} is already added to My Scholarships Tab.", scholarship.Name), "Add to My Scholarship Tab.");
                return;
            }


            match = new Match
                        {
                            Seeker = seeker,
                            Scholarship = scholarship,
                            MatchStatus = MatchStatus.Saved,
                            LastUpdate = new ActivityStamp(UserContext.CurrentUser),
                        };
            MatchService.InsertCustomMatch(match);
            ClientSideDialogs.ShowAlert("Scholarship added to My Scholarships Tab.", "Add to My Scholarship Tab.");
        }
    }
}