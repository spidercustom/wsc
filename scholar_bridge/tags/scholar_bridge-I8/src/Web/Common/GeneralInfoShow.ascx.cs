﻿using System;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class GeneralInfoShow : BaseScholarshipShow
    {
        private const string NOT_ACTIVATED_TEXT = "&lt;not activated yet&gt;";
        private const string MONTH_FULL_NAME_FORMAT = "MMMM";
        private const string CURRENCY_FORMAT = "c";
        private const string TEXT_FOR_UNKNOWN = "&lt;not defined&gt;";
        private const string DEFAULT_INTERMEDIARY_NAME = "HSEB";
        private const string RANGE_OF_VALUE_FORMAT = "{0} - {1}";
        public override ScholarshipStages Stage { get { return ScholarshipStages.GeneralInformation; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //setup links
            SetupEditLinks(linkarea1, linkarea2);

            lblName.Text = ScholarshipToView.DisplayName;
            lblAcademicYear.Text = ScholarshipToView.AcademicYear.ToString();

            lblScholarshipActivationDate.Text = ScholarshipToView.ActivatedOn.HasValue
                                                    ? ScholarshipToView.ActivatedOn.Value.ToString("d")
                                                    : NOT_ACTIVATED_TEXT;
            lblMission.Text = ScholarshipToView.MissionStatement;
            lblProgramGuidelines.Text = ScholarshipToView.ProgramGuidelines;
            lblAppStart.Text = ScholarshipToView.ApplicationStartDate.HasValue
                                   ? ScholarshipToView.ApplicationStartDate.Value.ToOrdinalDayString(MONTH_FULL_NAME_FORMAT)
                                   : TEXT_FOR_UNKNOWN;
            lblAppDue.Text = ScholarshipToView.ApplicationDueDate.HasValue
                                 ? ScholarshipToView.ApplicationDueDate.Value.ToOrdinalDayString(MONTH_FULL_NAME_FORMAT)
                                 : TEXT_FOR_UNKNOWN;
            lblAwardOn.Text = ScholarshipToView.AwardDate.HasValue
                                  ? ScholarshipToView.AwardDate.Value.ToOrdinalDayString(MONTH_FULL_NAME_FORMAT)
                                  : TEXT_FOR_UNKNOWN;

            lblAnnualSupportAmount.Text = ScholarshipToView.FundingParameters.AnnualSupportAmount.ToString(CURRENCY_FORMAT);
            lblNumberOfAwards.Text = string.Format(RANGE_OF_VALUE_FORMAT,
                                                   ScholarshipToView.FundingParameters.MinimumNumberOfAwards,
                                                   ScholarshipToView.FundingParameters.MinimumNumberOfAwards);
            lblTermOfSupport.Text = ScholarshipToView.FundingParameters.TermOfSupport == null
                                        ? TEXT_FOR_UNKNOWN
                                        : ScholarshipToView.FundingParameters.TermOfSupport.Name;
            lblAwardAmount.Text = string.Format(RANGE_OF_VALUE_FORMAT, 
                                                ScholarshipToView.MinimumAmount.ToString(CURRENCY_FORMAT),
                                                ScholarshipToView.MaximumAmount.ToString(CURRENCY_FORMAT));
            //view donor info
            lblDonor.Text = GetDonorDisplayString(ScholarshipToView.Donor);
            BindContactInformation();
            Page.DataBind();
        }

        private void BindContactInformation()
        {

            lblProviderWebSite.Text = ScholarshipToView.Provider.Website;
            lblProviderWebSite.NavigateUrl = ScholarshipToView.Provider.Website;

            lblProviderName.Text = ScholarshipToView.Provider.Name;
            lblAddressLine1.Text = ScholarshipToView.Provider.Address.Street;
            
            if (!String.IsNullOrEmpty(ScholarshipToView.Provider.Address.Street2))
                lblAddressLine2.Text = ScholarshipToView.Provider.Address.Street2;
            else
                lblAddressLine2.Visible = false;

            lblCityStateZip.Text = ScholarshipToView.Provider.Address.GetCityStateZip();
            if (null != ScholarshipToView.Provider.Phone)
                lblPhone.Text = ScholarshipToView.Provider.Phone.FormattedPhoneNumber;
            if (null == ScholarshipToView.Intermediary)
                lblIntermediaryName.Text = DEFAULT_INTERMEDIARY_NAME;
            else
                lblIntermediaryName.Text = ScholarshipToView.Intermediary.Name;
        }

        private static string GetDonorDisplayString(ScholarshipDonor donor)
        {
            var sb = new StringBuilder();
            if (! String.IsNullOrEmpty(donor.Name))
            {
                sb.Append(donor.Name);
                if (!String.IsNullOrEmpty(donor.Address.ToString()))
                    sb.Append(", ");
            }
            if (!String.IsNullOrEmpty(donor.Address.ToString()))
                sb.Append(donor.Address.ToString());

            if (null != donor.Phone && !string.IsNullOrEmpty(donor.Phone.Number))
                sb.AppendFormat(" Phone: {0}", donor.Phone.Number);

            if (!string.IsNullOrEmpty(donor.Email))
                sb.AppendFormat(" Email: {0}", donor.Email);

            return sb.ToString();
        }

        protected override void SetupEditLinks(HtmlGenericControl linkarea1, HtmlGenericControl linkarea2)
        {
            base.SetupEditLinks(linkarea1, linkarea2);

            editProgramGuidelinesLnk.NavigateUrl = "~/Provider/Scholarships/EditProgramGuidelines.aspx?id=" + ScholarshipToView.Id;
            EditControlContainer.Visible = CanEditGuidelines();
        }

        private bool CanEditGuidelines()
        {
            return !ScholarshipToView.CanEdit() &&
                   (Roles.IsUserInRole(Role.PROVIDER_ROLE) || Roles.IsUserInRole(Role.INTERMEDIARY_ROLE));
        }
    }
}
