﻿using System;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class AdditionalCriteriaShow : BaseScholarshipShow
    {
        public override ScholarshipStages Stage { get { return ScholarshipStages.AdditionalCriteria; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);

            if (!Page.IsPostBack)
            {
                AdditionalRequirementsRepeater.DataSource = ScholarshipToView.AdditionalRequirements;
                AdditionalRequirementsRepeater.DataBind();
                RequirementsFooterRow.Visible = ScholarshipToView.AdditionalRequirements.Count <= 0;

                AdditionalCriteriaRepeater.DataSource = ScholarshipToView.AdditionalQuestions;
                AdditionalCriteriaRepeater.DataBind();
            }

            scholarshipAttachments.Attachments = ScholarshipToView.Attachments;
            scholarshipAttachments.View = Page.IsInPrintView()
                                              ? FileList.FileListView.List
                                              : FileList.FileListView.Detail;
        }
    }
}