﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Basics" %>
<%@ Register TagPrefix="sb" Src="~/Common/LookupItemCheckboxList.ascx" TagName="LookupItemCheckboxList" %>
<%@ Register Tagprefix="sb" tagname="LookupDialog" src="~/Common/Lookup/LookupDialog.ascx"   %>    
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<h3>My Profile – Basics</h3>
<fieldset>
    <legend>My Name:</legend>
    <label for="FirstNameBox">First:</label>
    <asp:TextBox ID="FirstNameBox" runat="server" Columns="20" MaxLength="40" />
    <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstNameBox" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
    <br />
    <label for="MidNameBox">Middle:</label>
    <asp:TextBox ID="MidNameBox" runat="server" Columns="10" MaxLength="40"/>
    <elv:PropertyProxyValidator ID="MidNameValidator" runat="server" ControlToValidate="MidNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
    <br />
    <label for="LastNameBox">Last:</label>
    <asp:TextBox ID="LastNameBox" runat="server" Columns="20" MaxLength="40"/>
    <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
</fieldset>
<br />

<fieldset>
    <legend>Email</legend>
    <label for="EmailBox">Email:</label>
    <asp:Literal ID="EmailBox" runat="server" />
    <asp:HyperLink ID="LinkUpdateEmail" runat="server" NavigateUrl="~/Seeker/Users/ChangeEmail.aspx"   Text=" Update Email Address" />
    <br />
    <label for="UseEmailBox">Email Notifications</label>
    <asp:CheckBox ID="UseEmailBox" runat="server" Text="check box to receive email updates" Width="400px" />
    <p class="additionInfoText" >Notifications are sent for updates on the Scholarships saved to your "My Scholarships" tab. These updates include reminders of applications due dates and responses to submitted applications. These notifications will be posted on the Seeker In box tab. If you also would like to receive an e-mail of these notifications, Please check the above email notifications check box.</p>
</fieldset>
<br />

<fieldset>
    <legend>My Permanent Address:</legend>
    <label for="AddressLine1Box">Street Line 1:</label>
    <asp:TextBox ID="AddressLine1Box" runat="server" Columns="35" MaxLength="50"/>
    <elv:PropertyProxyValidator ID="AddressLine1Validator" runat="server" ControlToValidate="AddressLine1Box" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
    <br />
    <label for="AddressLine2Box">Street Line 2:</label>
    <asp:TextBox ID="AddressLine2Box" runat="server" Columns="35" MaxLength="50"/>
    <elv:PropertyProxyValidator ID="AddressLine2Validator" runat="server" ControlToValidate="AddressLine2Box" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
    <br />
    <label for="StateDropDown">State:</label>
    <asp:DropDownList ID="StateDropDown" runat="server"></asp:DropDownList>
    <br />
    <label for="CityControl">City:</label>
    <asp:TextBox ID="CityControl" runat="server" Columns="25" ReadOnly="true"/>
    <sb:LookupDialog ID="CityControlDialogButton" runat="server" BuddyControl="CityControl" ItemSource="CityDAL" Title="City" SelectionLimit="1"/>
    <br />
    <label for="CountyControl">County:</label>
    <asp:TextBox ID="CountyControl" runat="server" Columns="25" ReadOnly="true"/>
    <sb:LookupDialog ID="CountyControlDialogButton" runat="server" BuddyControl="CountyControl" ItemSource="CountyDAL" Title="County" SelectionLimit="1" />
    <br />
    <label for="ZipBox">Postal Code:</label>
    <asp:TextBox ID="ZipBox" runat="server" Columns="10" MaxLength="10"/>
    <elv:PropertyProxyValidator ID="ZipValidator" runat="server" ControlToValidate="ZipBox" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
    <br />
    <label for="PhoneBox">Phone:</label>
    <asp:TextBox ID="PhoneBox" runat="server" Columns="35" CssClass="phone"/>
    <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="PhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
        <br />
    <label for="MobilePhoneBox">Mobile Phone:</label>
    <asp:TextBox ID="MobilePhoneBox" runat="server" Columns="35" CssClass="phone"/>
    <elv:PropertyProxyValidator ID="MobilePhoneValidator" runat="server" ControlToValidate="MobilePhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
</fieldset>
<br />

<fieldset>
    <label for="DobControl">Birthdate</label>
    <asp:TextBox ID="DobControl" runat="server" CssClass="date"/>
    <elv:PropertyProxyValidator ID="DobValidator" runat="server" ControlToValidate="DobControl" PropertyName="DateOfBirth" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    <asp:RangeValidator ID="dobRangeValid" runat="server" ControlToValidate="DobControl" MinimumValue="1/1/1900" Type="Date" Display="Dynamic" ErrorMessage="How old are you?" />
<br />

    <label for="GenderButtonList">My Gender:</label>
    <asp:RadioButtonList ID="GenderButtonList" runat="server">
        <asp:ListItem Text="Male" Value="1"></asp:ListItem>
        <asp:ListItem Text="Female" Value="2"></asp:ListItem>
    </asp:RadioButtonList>
</fieldset>
<br />
<fieldset>
    <label for="ReligionCheckboxList">My Religion/Faith:</label>
    <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" />
    <label for="OtherReligion">Other Religion?:</label>
    <asp:TextBox ID="OtherReligion" runat="server"/>  
    <elv:PropertyProxyValidator ID="OtherReligionValidator" runat="server" ControlToValidate="OtherReligion" PropertyName="ReligionOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>

<br />

    <label for="EthnicityControl">My Heritage:</label>
    <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" />
    <label for="OtherEthnicity">Other Ethnicity?</label>
    <asp:TextBox ID="OtherEthnicity" runat="server"/>
    <elv:PropertyProxyValidator ID="OtherEthnicityValidator" runat="server" ControlToValidate="OtherEthnicity" PropertyName="EthnicityOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>
</fieldset>



