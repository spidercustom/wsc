﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class AcademicInformation : WizardStepUserControlBase<Domain.Application>
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Application application;
        Domain.Application ApplicationInContext
        {
            get
            {
                if (application == null)
                    application = Container.GetDomainObject();
                if (application == null)
                    throw new InvalidOperationException("There is no application in context");
                return application;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ApplicationInContext == null)
                throw new InvalidOperationException("There is no application in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
		}

        private void PopulateScreen()
        {
            FirstGenerationControl.Checked = ApplicationInContext.FirstGeneration;
            if (null != ApplicationInContext.CurrentSchool)
            {
                CurrentStudentGroupControl.StudentGroup = ApplicationInContext.CurrentSchool.LastAttended;
                CurrentStudentGroupControl.Years = ApplicationInContext.CurrentSchool.YearsAttended;

                if (null != ApplicationInContext.CurrentSchool.College)
                    CollegeControlDialogButton.Keys = ApplicationInContext.CurrentSchool.College.Id.ToString();
                CollegeOtherControl.Text = ApplicationInContext.CurrentSchool.CollegeOther;

                if (null != ApplicationInContext.CurrentSchool.HighSchool)
                    HighSchoolControlDialogButton.Keys = ApplicationInContext.CurrentSchool.HighSchool.Id.ToString();
                HighSchoolOtherControl.Text = ApplicationInContext.CurrentSchool.HighSchoolOther;

                if (null != ApplicationInContext.CurrentSchool.SchoolDistrict)
                    SchoolDistrictControlLookupDialogButton.Keys = ApplicationInContext.CurrentSchool.SchoolDistrict.Id.ToString();
                SchoolDistrictOtherControl.Text = ApplicationInContext.CurrentSchool.SchoolDistrictOther;
            }


            if (null != ApplicationInContext.SeekerAcademics)
            {
                CollegesAppliedControlDialogButton.Keys = ApplicationInContext.SeekerAcademics.CollegesApplied.CommaSeparatedIds();
                CollegesAppliedOtherControl.Text = ApplicationInContext.SeekerAcademics.CollegesAppliedOther;

                CollegesAcceptedControlDialogButton.Keys = ApplicationInContext.SeekerAcademics.CollegesAccepted.CommaSeparatedIds();
                CollegesAcceptedOtherControl.Text = ApplicationInContext.SeekerAcademics.CollegesAcceptedOther;

                if (ApplicationInContext.SeekerAcademics.GPA.HasValue)
                    GPAControl.Amount = (decimal)ApplicationInContext.SeekerAcademics.GPA.Value;

                if (ApplicationInContext.SeekerAcademics.ClassRank.HasValue)
                    ClassRankControl.Amount = ApplicationInContext.SeekerAcademics.ClassRank.Value;

                SchoolTypeControl.SelectedValues = (int)ApplicationInContext.SeekerAcademics.SchoolTypes;
                AcademicProgramControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.AcademicPrograms;
                SeekerStatusControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.SeekerStatuses;
                ProgramLengthControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.ProgramLengths;
                
                if (null != ApplicationInContext.SeekerAcademics.SATScore)
                {
                    SATReadingControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.CriticalReading.Value;
                    SATWritingControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Writing.Value;
                    SATMathControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Mathematics.Value;
                }
                if (null != ApplicationInContext.SeekerAcademics.ACTScore)
                {
                    ACTEnglishControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.English.Value;
                    ACTMathControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Mathematics.Value;
                    ACTReadingControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Reading.Value;
                    ACTScienceControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Science.Value;
                }

                APCreditsControl.Amount = ApplicationInContext.SeekerAcademics.APCredits ?? 0;
                IBCreditsControl.Amount = ApplicationInContext.SeekerAcademics.IBCredits ?? 0;
                HonorsControl.Checked = ApplicationInContext.SeekerAcademics.Honors;
            }
        }

	    #region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

        public override void Save()
        {
            PopulateObjects();
            ApplicationService.Update(ApplicationInContext);
        }

        public override void PopulateObjects()
        {
            ApplicationInContext.FirstGeneration = FirstGenerationControl.Checked;
          
            if (null == ApplicationInContext.SeekerAcademics)
            {
                ApplicationInContext.SeekerAcademics = new SeekerAcademics();
            }

            CollegesAppliedControlDialogButton.PopulateListFromSelection(ApplicationInContext.SeekerAcademics.CollegesApplied);
            ApplicationInContext.SeekerAcademics.CollegesAppliedOther = CollegesAppliedOtherControl.Text;

            CollegesAcceptedControlDialogButton.PopulateListFromSelection(ApplicationInContext.SeekerAcademics.CollegesAccepted);
            ApplicationInContext.SeekerAcademics.CollegesAcceptedOther = CollegesAcceptedOtherControl.Text;

            ApplicationInContext.SeekerAcademics.Honors = HonorsControl.Checked;
            ApplicationInContext.SeekerAcademics.GPA = (int) GPAControl.Amount;
            ApplicationInContext.SeekerAcademics.APCredits = (int) APCreditsControl.Amount;
            ApplicationInContext.SeekerAcademics.IBCredits = (int) IBCreditsControl.Amount;
            ApplicationInContext.SeekerAcademics.ClassRank = (int) ClassRankControl.Amount;

            if (null == ApplicationInContext.SeekerAcademics.SATScore)
            {
                ApplicationInContext.SeekerAcademics.SATScore = new SatScore();
            }

            ApplicationInContext.SeekerAcademics.SATScore.CriticalReading = (int) SATReadingControl.Amount;
            ApplicationInContext.SeekerAcademics.SATScore.Writing = (int) SATWritingControl.Amount;
            ApplicationInContext.SeekerAcademics.SATScore.Mathematics = (int) SATMathControl.Amount;

            if (null == ApplicationInContext.SeekerAcademics.ACTScore)
            {
                ApplicationInContext.SeekerAcademics.ACTScore = new ActScore();
            }

            ApplicationInContext.SeekerAcademics.ACTScore.English = (int) ACTEnglishControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Mathematics = (int) ACTMathControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Reading = (int) ACTReadingControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Science = (int) ACTScienceControl.Amount;

            if (null == ApplicationInContext.CurrentSchool)
            {
                ApplicationInContext.CurrentSchool = new CurrentSchool();
            }
            ApplicationInContext.CurrentSchool.LastAttended = CurrentStudentGroupControl.StudentGroup;
            ApplicationInContext.CurrentSchool.YearsAttended = CurrentStudentGroupControl.Years;

            ApplicationInContext.CurrentSchool.College = (College)CollegeControlDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.CollegeOther = CollegeOtherControl.Text;

            ApplicationInContext.CurrentSchool.HighSchool = (HighSchool)HighSchoolControlDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.HighSchoolOther = HighSchoolOtherControl.Text;

            ApplicationInContext.CurrentSchool.SchoolDistrict = (SchoolDistrict)SchoolDistrictControlLookupDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.SchoolDistrictOther = SchoolDistrictOtherControl.Text;

            ApplicationInContext.SeekerAcademics.SchoolTypes = (SchoolTypes) SchoolTypeControl.SelectedValues;
            ApplicationInContext.SeekerAcademics.AcademicPrograms = (AcademicPrograms) AcademicProgramControl.SelectedValue;
            ApplicationInContext.SeekerAcademics.SeekerStatuses = (SeekerStatuses)SeekerStatusControl.SelectedValue;
            ApplicationInContext.SeekerAcademics.ProgramLengths = (ProgramLengths) ProgramLengthControl.SelectedValue;
            ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
        }

	    public override bool WasSuspendedFrom(Domain.Application @object)
        {
            return @object.Stage == ApplicationStages.AcademicInfo;
        }

        public override bool CanResume(Domain.Application @object)
        {
            return @object.Stage >= ApplicationStages.AcademicInfo;
        }
		#endregion

    }
}