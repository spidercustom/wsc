using NUnit.Framework;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class SeekerTests
    {
        [Test]
        public void CalculatesProfleCompletePercentage()
        {
            var seeker = new Seeker()
                             {
                                 Words = "XXX"
                             };
            var percent = seeker.GetPercentComplete();
            var expected = percent + 1;
            seeker.ClubOther = "CLUBS";
            percent = seeker.GetPercentComplete();
            
            Assert.AreNotEqual(expected, percent);
        }
    }
}
