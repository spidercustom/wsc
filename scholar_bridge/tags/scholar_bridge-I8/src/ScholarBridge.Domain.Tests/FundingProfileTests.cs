﻿using System;
using NUnit.Framework;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class FundingProfileTests
    {

        private FundingProfileAttributeUsage CreateMinimumUsageTypeAttribute(FundingProfileAttribute attribute)
        {
            return new FundingProfileAttributeUsage
            {
                Attribute = attribute,
                UsageType = ScholarshipAttributeUsageType.Minimum
            };
        }

        [Test]
        public void test_has_attribute()
        {
            var fundingProfile = new FundingProfile();
            Assert.IsFalse(fundingProfile.HasAttributeUsage(FundingProfileAttribute.Need));
            fundingProfile.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.Need));

            Assert.IsTrue(fundingProfile.HasAttributeUsage(FundingProfileAttribute.Need));
        }

        [Test]
        public void test_clone_implimentation_exists()
        {
            var fundingProfile = new FundingProfile();
            fundingProfile.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.Need));

            var clonedCriteria = (FundingProfile)fundingProfile.Clone();
            Assert.AreEqual(fundingProfile.AttributesUsage.Count, clonedCriteria.AttributesUsage.Count);
            Assert.AreEqual(fundingProfile.AttributesUsage[0].Attribute, clonedCriteria.AttributesUsage[0].Attribute);
            Assert.AreEqual(fundingProfile.AttributesUsage[0].UsageType, clonedCriteria.AttributesUsage[0].UsageType);
        }

        [Test]
        public void remove_attribute()
        {
            var criteria = new FundingProfile();
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.Need));
            Assert.AreEqual(1, criteria.AttributesUsage.Count);
            criteria.RemoveAttributeUsage(FundingProfileAttribute.SupportedSituation);
            Assert.AreEqual(1, criteria.AttributesUsage.Count);
            criteria.RemoveAttributeUsage(FundingProfileAttribute.Need);
            Assert.AreEqual(0, criteria.AttributesUsage.Count);
        }

        [Test]
        public void find_attribute_usage()
        {
            var criteria = new FundingProfile();
            Assert.IsNull(criteria.FindAttributeUsage(FundingProfileAttribute.Need));
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.Need));

            Assert.IsNull(criteria.FindAttributeUsage(FundingProfileAttribute.SupportedSituation));
            Assert.IsNotNull(criteria.FindAttributeUsage(FundingProfileAttribute.Need));
        }

        [Test]
        public void get_attribute_usage_type()
        {
            var criteria = new FundingProfile();
            Assert.AreEqual(ScholarshipAttributeUsageType.NotUsed, criteria.GetAttributeUsageType(FundingProfileAttribute.Need));

            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(FundingProfileAttribute.Need));
            Assert.AreEqual(ScholarshipAttributeUsageType.NotUsed, criteria.GetAttributeUsageType(FundingProfileAttribute.SupportedSituation));
            Assert.AreEqual(ScholarshipAttributeUsageType.Minimum, criteria.GetAttributeUsageType(FundingProfileAttribute.Need));
        }

        [Test]
        public void test_cloning()
        {
            var fundingProfile = new FundingProfile();
            PopulateFundingProfile(fundingProfile);
            var clonable = (ICloneable)fundingProfile;
            var cloned = (FundingProfile)clonable.Clone();

            Assert.IsNotNull(cloned.Need);
            Assert.IsNotNull(cloned.SupportedSituation);
            CollectionAssert.AreEqual(fundingProfile.AttributesUsage, cloned.AttributesUsage);
            Assert.AreEqual(fundingProfile.Need.Fafsa, cloned.Need.Fafsa);
            Assert.AreEqual(fundingProfile.Need.MaximumSeekerNeed, cloned.Need.MaximumSeekerNeed);
            Assert.AreEqual(fundingProfile.Need.MinimumSeekerNeed, cloned.Need.MinimumSeekerNeed);
            Assert.AreEqual(fundingProfile.Need.UserDerived, cloned.Need.UserDerived);
            CollectionAssert.AreEqual(fundingProfile.Need.NeedGaps, cloned.Need.NeedGaps);
            CollectionAssert.AreEqual(fundingProfile.SupportedSituation.TypesOfSupport, cloned.SupportedSituation.TypesOfSupport);
            
        }

        public static void PopulateFundingProfile(FundingProfile fundingProfile)
        {
            fundingProfile.AttributesUsage.Add(new FundingProfileAttributeUsage());
            fundingProfile.Need.NeedGaps.Add(new NeedGap());
            fundingProfile.SupportedSituation.TypesOfSupport.Add(new Support());
        }
    }
}
