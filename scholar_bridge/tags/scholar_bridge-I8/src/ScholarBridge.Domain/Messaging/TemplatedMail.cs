using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ScholarBridge.Domain.Messaging
{
    public class TemplatedMail
    {
        private const string VARIABLE_TAG = "##";
        private const string VARIABLE_FORMAT = VARIABLE_TAG + "{0}" + VARIABLE_TAG;

        public TemplatedMail() {}

        public TemplatedMail(string rawText)
        {
            using (var reader = new StringReader(rawText))
            {
                ReadValues(reader);
            }
        }

        public void ReadValues(TextReader reader)
        {
            Subject = reader.ReadLine();
            RawBody = reader.ReadToEnd();
        }

        public string Subject { get; set; }
        public string RawBody { get; set; }

        public string Body(Dictionary<string, string> templateVariables)
        {
            var bodyContent = new StringBuilder(RawBody);
            foreach (KeyValuePair<string, string> item in templateVariables)
            {
                string key = string.Format(VARIABLE_FORMAT, item.Key);
                bodyContent.Replace(key, item.Value);
            }
            return bodyContent.ToString();
        }
    }
}