﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public class SeekerProfileAttributeUsage
    {
        public virtual SeekerProfileAttribute Attribute { get; set; }
        public virtual ScholarshipAttributeUsageType UsageType { get; set; }
    }
}
