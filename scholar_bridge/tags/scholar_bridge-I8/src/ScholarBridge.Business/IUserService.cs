﻿using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IUserService
    {
        User FindByEmail(string email);

		void Insert(User user);

		void Update(User user);

		void Delete(User user);

        void ActivateUser(User user);

        void ActivateSeekerUser(User user);

        void ActivateProviderUser(User user);

        void ActivateIntermediaryUser(User user);

        void ResetUsername(User user, string newEmail);

        void SendConfirmationEmail(User user, bool requiresResetPassword);
        void UpdateEmailAddress(User user,string newEmail);
        void SendEmailAddressChangeVerificationEmail(User user);
        void ConfirmEmailAddressVerification(User user);
    }
}
