﻿using System;
using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class UserDAL :  AbstractDAL<User>, IUserDAL
    {
        private static readonly SimpleExpression NOT_DELETED = Restrictions.Eq("IsDeleted", false);

        public User FindByEmail(string email)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Email", email))
                .UniqueResult<User>();
        }

        public User FindByUsername(string email)
        {
            return CreateCriteria()
                .SetCacheable(true)
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Username", email))
                .UniqueResult<User>();
        }

        public User FindByUsernameAndPassword(string username, string password)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Username", username))
                .Add(Restrictions.Eq("Password", password))
                .UniqueResult<User>();
        }

        public IList<User> FindAll(int pageIndex, int pageSize)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .SetFirstResult(pageSize*pageIndex)
                .SetMaxResults(pageSize)
                .List<User>();
        }

        public IList<User> FindByEmail(string email, int pageIndex, int pageSize)
        {
            email = email.Replace('*', '%');
            email = email.Replace('?', '_');
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Like("Email", email).IgnoreCase())
                .SetFirstResult(pageSize*pageIndex)
                .SetMaxResults(pageSize)
                .List<User>();
        }

        public IList<User> FindByRoleName(string roleName)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .CreateCriteria("Roles")
                .Add(Restrictions.Eq("Name", roleName))
                .List<User>();
        }

        public IList<User> FindByRoleNameAndUsernameWildcard(string roleName, string usernameToMatch)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Like("Username", usernameToMatch, MatchMode.Anywhere))
                .CreateCriteria("Roles").Add(Restrictions.Eq("Name", roleName))
                .List<User>();
        }

        public IList<User> FindNonConfirmedUsers(int daysOld)
        {
            var beforeDate = DateTime.Today.AddDays(-daysOld);
            return FindNonConfirmedUsers(beforeDate);
        }

        public IList<User> FindNonConfirmedUsers(DateTime beforeDate)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("IsActive", false))
                .Add(Restrictions.Le("LastUpdate.On", beforeDate))
                .List<User>();
        }

        public int FindCountOfUsersOnline()
        {
            return  CreateCriteria()
                .Add(Restrictions.Eq("IsOnline", true))
                .SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }

        public User FindById(int id)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Id", id))
                .UniqueResult<User>();
        }
    }
}