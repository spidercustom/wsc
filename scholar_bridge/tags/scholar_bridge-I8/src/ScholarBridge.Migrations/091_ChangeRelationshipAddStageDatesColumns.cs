﻿using Migrator.Framework;
using System.Data;

namespace ScholarBridge.Migrations
{
    [Migration(91)]
    public class ChangeRelationshipAddStageDatesColumns: Migration
    {
        private const string FK_SBRelationship_SBRelationshipStageLUT = "FK_SBRelationship_SBRelationshipStageLUT";

        public override void Up()
        {
            Database.AddColumn(AddRelationship.TABLE_NAME, "ActivatedOn", DbType.DateTime, ColumnProperty.Null);
            Database.AddColumn(AddRelationship.TABLE_NAME, "InActivatedOn", DbType.DateTime, ColumnProperty.Null);
            Database.AddColumn(AddRelationship.TABLE_NAME, "RequestedOn", DbType.DateTime, ColumnProperty.Null);
            Database.RenameColumn(AddRelationship.TABLE_NAME, "LastUpdatedOn", "LastUpdateDate");
            Database.RenameColumn(AddRelationship.TABLE_NAME, "LastUpdatedBy", "LastUpdateBy");
            Database.RenameColumn(AddRelationship.TABLE_NAME,"Stage",AddRelationshipStage.PRIMARY_KEY_COLUMN);

            Database.ChangeColumn(AddRelationship.TABLE_NAME, new Column(AddRelationshipStage.PRIMARY_KEY_COLUMN, DbType.Int32,ColumnProperty.NotNull));

            Database.AddForeignKey(FK_SBRelationship_SBRelationshipStageLUT, AddRelationship.TABLE_NAME,
                                   AddRelationshipStage.PRIMARY_KEY_COLUMN, AddRelationshipStage.TABLE_NAME, AddRelationshipStage.PRIMARY_KEY_COLUMN);

            
        }

        public override void Down()
        {
            Database.RemoveColumn(AddRelationship.TABLE_NAME, "ActivatedOn");
            Database.RemoveColumn(AddRelationship.TABLE_NAME, "InActivatedOn");
            Database.RemoveColumn(AddRelationship.TABLE_NAME, "RequestedOn");
            Database.RenameColumn(AddRelationship.TABLE_NAME, AddRelationshipStage.PRIMARY_KEY_COLUMN,"Stage" );
            Database.ChangeColumn(AddRelationship.TABLE_NAME, new Column("Stage", DbType.String,30, ColumnProperty.NotNull));

            Database.RemoveForeignKey(AddRelationship.TABLE_NAME, FK_SBRelationship_SBRelationshipStageLUT);


        }

    }
}
