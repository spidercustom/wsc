﻿
using Migrator.Framework;
namespace ScholarBridge.Migrations
{
    [Migration(15)]
    public class ChangeScholarshipAddStage : Migration
    {
        public const string STAGE_COLUMN_NAME = "Stage";
        protected static readonly string FK_SCHOLARSHIP_STAGE = string.Format("FK_{0}_ScholarshipStage", AddScholarship.TABLE_NAME);
        public override void Up()
        {
            Database.AddColumn(AddScholarship.TABLE_NAME, STAGE_COLUMN_NAME, System.Data.DbType.Int32, ColumnProperty.Null);
            Database.Update(AddScholarship.TABLE_NAME, new []{ "Stage" }, new []{"0"});
            Database.AddForeignKey(FK_SCHOLARSHIP_STAGE, AddScholarship.TABLE_NAME, STAGE_COLUMN_NAME, AddScholarshipStages.TABLE_NAME, AddScholarshipStages.PRIMARY_KEY_COLUMN);
        }

        public override void Down()
        {
            Database.RemoveForeignKey(AddScholarship.TABLE_NAME, FK_SCHOLARSHIP_STAGE);
            Database.RemoveColumn(AddScholarship.TABLE_NAME, STAGE_COLUMN_NAME);
        }        
    }
}
