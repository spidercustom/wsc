using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(182)]
    public class AddApplicationSeekerHobbyRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationSeekerHobbyRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSeekerHobbyLUT"; }
        }
    }
}