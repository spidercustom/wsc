using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using Rhino.Mocks;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class ScholarshipTests
    {
		private MockRepository mocks;

		[SetUp]
		public void Setup()
		{
			mocks = new MockRepository();
		}

		[TearDown]
		public void Teardown()
		{
			mocks.VerifyAll();
		}

       

        [Test]
        public void verify_clone_implementation_exists()
        {
            var s = CreateValidScholarship();
            PopulateScholarship(s);

			mocks.ReplayAll();

            var cloned = (Scholarship)s.Clone("c:/temp");
            Assert.AreEqual(0, cloned.Id);
            Assert.AreEqual(AcademicYear.CurrentScholarshipYear.Year, cloned.AcademicYear.Year);
            Assert.IsTrue(cloned.CanSubmitForActivation());
            Assert.IsNull(cloned.LastUpdate);
            Assert.AreEqual(ScholarshipStage.None, cloned.Stage);
            Assert.IsNotNull(cloned.SeekerProfileCriteria);
            Assert.IsNotNull(cloned.FundingProfile);
        }

        [Test]
        public void validate_activated_and_rejected_and_request_for_activation()
        {
            var s = new Scholarship();

            for (var stageIndex = ScholarshipStage.None.GetNumericValue(); 
                stageIndex < ScholarshipStage.Activated.GetNumericValue(); stageIndex++)
            {
                s.Stage = (ScholarshipStage) stageIndex;
                Assert.IsFalse(s.IsActivated());
                Assert.IsFalse(s.IsRejected());
                Assert.IsTrue(s.CanSubmitForActivation());
            }

            s.Stage = ScholarshipStage.Activated;
            Assert.IsTrue(s.IsActivated());
            Assert.IsFalse(s.IsRejected());
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStage.Awarded;
            Assert.IsTrue(s.IsActivated());
            Assert.IsFalse(s.IsRejected());
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStage.Rejected;
            Assert.IsFalse(s.IsActivated());
            Assert.IsTrue(s.IsRejected());
            Assert.IsTrue(s.CanSubmitForActivation());
        }

        private void PopulateScholarship(Scholarship s)
        {
            s.AdditionalRequirements.Add(new AdditionalRequirement());
            var sq = mocks.StrictMock<ScholarshipQuestion>();
            Expect.Call(sq.Clone()).Return(new ScholarshipQuestion());
            s.AdditionalQuestions.Add(sq);

        	var attachment = mocks.StrictMock<Attachment>();
			Expect.Call(attachment.Clone("c:/temp")).Return(new Attachment());
            s.Attachments.Add(attachment);

            SeekerProfileCriteriaTest.PopulateSeekerProfile(s.SeekerProfileCriteria);
            FundingProfileTests.PopulateFundingProfile(s.FundingProfile);
        }

        [Test]
        public void can_edit_if_no_stage_set()
        {
            var s = new Scholarship();
            Assert.IsTrue(s.CanEdit());
        }

         

        public static Scholarship CreateValidScholarship()
        {
            var s = new Scholarship
                        {
                            Name = "Foo",
                            MissionStatement = "Accomplish things",
                            ProgramGuidelines = "Give to people in need",
                            Provider = new Provider { Name = "Foo Provider", ApprovalStatus = ApprovalStatus.Approved },
                            ApplicationStartDate = DateTime.Today,
                            ApplicationDueDate = DateTime.Today.AddMonths(1),
                            AwardDate = DateTime.Today.AddMonths(2),
                            MinimumAmount = 0,
                            MaximumAmount = 100
                        };

            return s;
        }

        [Test]
        public void display_name_with_no_donor_gives_name()
        {
            var s = new Scholarship { Name = "Foo" };
            Assert.AreEqual("Foo", s.DisplayName);
        }

        [Test]
        public void display_name_with_donor_but_no_name_gives_name()
        {
            var s = new Scholarship { Name = "Foo", Donor = new ScholarshipDonor() };
            Assert.AreEqual("Foo", s.DisplayName);
        }

        [Test]
        public void display_name_with_donor_gives_name_with_donor_name()
        {
            var s = new Scholarship { Name = "Foo", Donor = new ScholarshipDonor { Name = "First Last"} };
            Assert.AreEqual("Foo - First Last", s.DisplayName);
        }

        [Test]
        public void can_append_admin_notes_to_scholarship()
        {
            var scholarship = new Scholarship();
            var u = new User { Name = { FirstName = "TestFirst", LastName = "TestLast" } };
            scholarship.AppendAdminNotes(u, "This is a test note.");
            StringAssert.Contains("-- TestFirst TestLast", scholarship.AdminNotes.ToStringTableFormat());
            StringAssert.Contains("This is a test note.", scholarship.AdminNotes.NotesXml);
        }

        [Test]
        public void can_generate_an_amount_string()
        {
            var s = new Scholarship { MinimumAmount = 100.00m, MaximumAmount = 3000.00m };
            Assert.AreEqual("$100 to $3,000", s.AmountRange);
            s.MinimumAmount = 0;
            Assert.AreEqual("$0 to $3,000", s.AmountRange);
        }

        [Test]
        public void is_award_period_closed_false_if_date_is_null()
        {
            var scholarship = new Scholarship();
            Assert.IsFalse(scholarship.IsAwardPeriodClosed);
        }

        [Test]
        public void is_award_period_closed_false_if_date_is_tomorrow()
        {
            var scholarship = new Scholarship { AwardPeriodClosed = DateTime.Now.AddDays(1) };
            Assert.IsFalse(scholarship.IsAwardPeriodClosed);
        }

		[Test]
		public void is_award_period_closed_true_if_date_is_yesterday()
		{
			var scholarship = new Scholarship { AwardPeriodClosed = DateTime.Now.AddDays(-1) };
			Assert.IsTrue(scholarship.IsAwardPeriodClosed);
		}

        [Test]
        public void canStartApplication_false_if_date_is_tommrow_and_activated()
        {
            var scholarship = new Scholarship {Stage = ScholarshipStage.Activated, ApplicationStartDate = DateTime.Now.AddDays(1) };
            Assert.IsFalse(scholarship.CanStartApplication );
        }
        
        [Test]
        public void canStartApplication_true_if_date_is_today_and_activated()
        {
            var scholarship = new Scholarship { Stage = ScholarshipStage.Activated, ApplicationStartDate = DateTime.Now };
            Assert.IsTrue(scholarship.CanStartApplication);
        }

        [Test]
        public void canStartApplication_false_if_date_is_yesterday_and_activated()
        {
            var scholarship = new Scholarship { Stage = ScholarshipStage.Activated, ApplicationStartDate = DateTime.Now.AddDays(-1) };
            Assert.IsTrue(scholarship.CanStartApplication);
        }

        [Test]
        public void canStartApplication_false_if_date_is_yesterday_andNotactivated()
        {
            var scholarship = new Scholarship { Stage = ScholarshipStage.NotActivated, ApplicationStartDate = DateTime.Now.AddDays(-1) };
            Assert.IsFalse(scholarship.CanStartApplication);
        }

        [Test]
        public void is_belong_returns_false_if_null()
        {
            var s = new Scholarship();
            Assert.IsFalse(s.IsBelongToOrganization(null));
        }

        [Test]
        public void is_belong_returns_false_if_not_match_provider()
        {
            var s = new Scholarship {Provider = new Provider {Id = 1}, Intermediary = new Intermediary {Id = 2}};
            Assert.IsFalse(s.IsBelongToOrganization(new Provider { Id = 2}));

            s.Provider = null;
            Assert.IsFalse(s.IsBelongToOrganization(new Provider { Id = 1 }));
        }

        [Test]
        public void is_belong_returns_false_if_not_match_intermediary()
        {
            var s = new Scholarship { Provider = new Provider { Id = 1 }, Intermediary = new Intermediary { Id = 2 } };
            Assert.IsFalse(s.IsBelongToOrganization(new Intermediary { Id = 1 }));


            s.Intermediary = null;
            Assert.IsFalse(s.IsBelongToOrganization(new Intermediary { Id = 2 }));
        }

        [Test]
        public void is_belong_returns_true_if_match_provider()
        {
            var s = new Scholarship { Provider = new Provider { Id = 1 }, Intermediary = new Intermediary { Id = 2 } };
            Assert.IsTrue(s.IsBelongToOrganization(new Provider { Id = 1 }));
        }

        [Test]
        public void is_belong_returns_true_if_match_intermediary()
        {
            var s = new Scholarship { Provider = new Provider { Id = 1 }, Intermediary = new Intermediary { Id = 2 } };
            Assert.IsTrue(s.IsBelongToOrganization(new Intermediary { Id = 2 }));
        }
	}
}