using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class SeekerTests
    {
        [Test]
        public void CalculatesProfleCompletePercentage()
        {
            var seeker = new Seeker()
                             {
                                 Words = "XXX"
                             };
            var percent = seeker.GetPercentComplete();
            var expected = percent + 1;
            seeker.ClubOther = "CLUBS";
            percent = seeker.GetPercentComplete();
            
            Assert.AreNotEqual(expected, percent);
        }

        [Test]
        public void empty_seeker_validation()
        {
            var seeker = new Seeker();
            seeker.ValidateActivation();
            Assert.That(seeker.ValidateActivation().IsValid, Is.False);
        }

        [Test]
        public void empty_seeker_with_current_school_validation()
        {
            var seeker = new Seeker {CurrentSchool = new CurrentSchool()};
            seeker.ValidateActivation();
            Assert.That(seeker.ValidateActivation().IsValid, Is.False);
        }

        [Test]
        public void empty_seeker_with_seeker_academics_validation()
        {
            var seeker = new Seeker { SeekerAcademics = new SeekerAcademics() };
            seeker.ValidateActivation();
            Assert.That(seeker.ValidateActivation().IsValid, Is.False);
        }

        [Test]
        public void empty_seeker_with_situation_supported()
        {
            var seeker = new Seeker { SupportedSituation = new SupportedSituation() };
            seeker.ValidateActivation();
            Assert.That(seeker.ValidateActivation().IsValid, Is.False);
        }

        [Test]
        public void fill_required_for_activation_and_validate()
        {
            var seeker = CreateValidSeeker();

            var results = seeker.ValidateActivation();
            Assert.That(results.IsValid, Is.True);
        }

        [Test]
        public void name_missing_test()
        {
            var seeker = CreateValidSeeker();
            seeker.User = null;
            var results = seeker.ValidateActivation();
            Assert.That(results.IsValid, Is.False);
        }

        [Test]
        public void address_missing_test()
        {
            var seeker = CreateValidSeeker();
            seeker.Address = null;
            var results = seeker.ValidateActivation();
            Assert.That(results.IsValid, Is.False);
        }         

        public static Seeker CreateValidSeeker()
        {
            var seeker = new Seeker
                             {
                                 User = new User
                                            {
                                                Name = new PersonName
                                                           {
                                                               FirstName = "seeker",
                                                               MiddleName = "god-father",
                                                               LastName = "ms"
                                                           }
                                            }, 
                                 Address = new SeekerAddress
                                               {
                                                   Street = "street address - 1",
                                                   Street2 = "street address - 2",
                                                   CityInState = new City { Name = "Tacoma" },
												   CountyInState = new County() {Name = "Pierce"},
                                                   State = new State { Abbreviation = "WA" },
                                                   PostalCode = "41101"
                                               },
                                 CurrentSchool = new CurrentSchool
                                                     {
                                                         LastAttended = StudentGroups.Graduate,
                                                         TypeOfCollegeStudent=StudentGroups.AdultReturning
                                                     },
                                 SeekerAcademics = new SeekerAcademics
                                                       {
                                                           SchoolTypes = SchoolTypes.Seminary,
                                                           AcademicPrograms = AcademicPrograms.AdvancedDegree
                                                       },
                             };
            seeker.SupportedSituation.TypesOfSupport.Add(new Support());
            return seeker;
        }
    }
}
