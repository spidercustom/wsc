﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Extensions
{
    public static class ListControlExtensions
    {
        public static void EnumBind(this ListControl control, params Enum[] enumElements)
        {
            control.DataSource = EnumExtensions.GetKeyValue(enumElements);
            control.DataTextField = "Value";
            control.DataValueField = "Key";
            control.DataBind();
        }
    }
}
