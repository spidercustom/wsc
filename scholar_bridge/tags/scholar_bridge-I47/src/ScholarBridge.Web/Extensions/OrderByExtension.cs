﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace ScholarBridge.Web.Extensions
{
	public static class OrderByExtension
	{

		public static IOrderedEnumerable<T> OrderBy<T>(this IEnumerable<T> list, Func<T, object> keySelector, bool orderAscending)
		{
			return orderAscending ? list.OrderBy(keySelector) : list.OrderByDescending(keySelector);
		}

		/// <summary>
		/// Will try to access the property with the name specified in 
		/// <see cref="propertyName"/>, and will return null if that property does not exist.
		/// </summary>
		/// <param name="obj">The object to access</param>
		/// <param name="propertyName">Name of the property to access</param>
		/// <returns></returns>
		public static object GetValue(this object obj, string propertyName)
		{
			return GetValue(obj, propertyName, null);
		}

		/// <summary>
		/// Will try to access the property with the name specified in 
		/// <see cref="propertyName"/>, and will return null if that property does not exist.
		/// </summary>
		/// <param name="obj">The object to access</param>
		/// <param name="propertyName">Name of the property to access</param>
		/// <param name="format">If provided, will return a string with this format applied.</param>
		/// <returns></returns>
		public static object GetValue(this object obj, string propertyName, string format)
		{
			try
			{
				var o = DataBinder.Eval(obj, propertyName);
				return string.IsNullOrEmpty(format) ? o : string.Format(format, o);
			}
			catch (HttpException)
			{
				return null;
			}
		}
	}
}
