using System;
using System.Web;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Exceptions;

namespace ScholarBridge.Web
{
    public class UserContext : IUserContext
    {
        public IUserDAL UserService { get; set; }
        public IProviderDAL ProviderService { get; set; }
        public IIntermediaryDAL IntermediaryService { get; set; }
        public ISeekerDAL SeekerService { get; set; }

        public User CurrentUser
        {
            get
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    return null;
                string[] names = HttpContext.Current.User.Identity.Name.Split('!');
                return UserService.FindByUsername(names[0]);
            }
        }

        public Organization CurrentOrganization
        {
            get
            {
                var org = CurrentProvider;
                if (null != org)
                    return org;
                return CurrentIntermediary;
            }
        }

        public Provider CurrentProvider
        {
            get
            {
                var u = CurrentUser;
                if (null == u)
                    return null;

                return ProviderService.FindByUser(u);
            }
        }

        public Intermediary CurrentIntermediary
        {
            get
            {
                var u = CurrentUser;
                if (null == u)
                    return null;

                return IntermediaryService.FindByUser(u);
            }
        }

        public Seeker CurrentSeeker
        {
            get
            {
                var u = CurrentUser;
                if (null == u)
                    return null;
                return SeekerService.FindByUser(u);
            }
        }

        public void EnsureUserIsInContext()
        {
            if (null == CurrentUser)
                throw new NoUserIsInContextException();
        }

        public void EnsureProviderIsInContext()
        {
            if (null == CurrentProvider)
                throw new NoProviderIsInContextException();
        }

        public void EnsureIntermediaryIsInContext()
        {
            if (null == CurrentIntermediary)
                throw new NoIntermediaryIsInContextException();
        }

        public void EnsureOrganizationIsInContext()
        {
            if (null == CurrentOrganization)
                throw new NoOrganizationIsInContextException();

            if (CurrentOrganization.ApprovalStatus != ApprovalStatus.Approved)
            {
                if (CurrentOrganization is Provider)
                    throw new Business.Exceptions.ProviderNotApprovedException();

                throw new Business.Exceptions.IntermediaryNotApprovedException();
            }

        }

        public void EnsureSeekerIsInContext()
        {
            if (null == CurrentSeeker)
                throw new NoSeekerIsInContextException();
        }

        public void EnsureApplicationBelongsToOrganization(Application application)
        {
            EnsureOrganizationIsInContext();

            if (!(application.Scholarship.Provider.Id.Equals(CurrentOrganization.Id) || (application.Scholarship.Intermediary != null && application.Scholarship.Intermediary.Id.Equals(CurrentOrganization.Id))))
            {
                if (CurrentOrganization is Provider)
                    throw new InvalidOperationException("Application does not belong to provider in context");

                throw new InvalidOperationException("Application does not belong to intermediary in context");
            }

        }

        public bool ScholarshipBelongsToOrganization(Scholarship scholarship)
        {
            EnsureOrganizationIsInContext();

            if (scholarship.Provider.Id.Equals(CurrentOrganization.Id) || scholarship.Intermediary.Id.Equals(CurrentOrganization.Id))
                return true;

            return false;


        }
    }
}