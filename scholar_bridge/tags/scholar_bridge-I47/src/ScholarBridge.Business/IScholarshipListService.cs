﻿

namespace ScholarBridge.Business
{
	public interface IScholarshipListService
	{
		void GenerateAndStoreScholarshipList();
	}
}
