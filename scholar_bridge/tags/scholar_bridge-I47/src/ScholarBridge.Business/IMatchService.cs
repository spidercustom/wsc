using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface IMatchService
    {
        Match GetMatch(Seeker seeker, int scholarshipId);

        void SaveMatch(Seeker seeker, int scholarshipId);
        void UnSaveMatch(Seeker seeker, int scholarshipId);

        void InsertCustomMatch(Match match);

		IList<Match> GetMatchesForSeekerWithoutApplications(int startIndex, int rowCount, string sortExpression, Seeker seeker, MatchFilterCriteria criteria);
		int CountMatchesForSeekerWithoutApplications(Seeker seeker, MatchFilterCriteria criteria);
		IList<Match> GetSavedMatches(Scholarship scholarship);
        IList<Match> GetSavedButNotAppliedMatches( Seeker seeker);

        void ApplyForMatch(Seeker seeker, int scholarshipId, Application application);

    	void UpdateMatches(Seeker seeker);

        /// <summary>
        /// Disconnect an application from any matches it is associated with.
        /// This would be helpful prior to deleting an application for example.
        /// </summary>
        /// <param name="application"></param>
        void DisconnectApplication(Application application);
    }
}