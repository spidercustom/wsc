﻿using System.Collections.Generic;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IUserService
    {
        User FindByEmail(string email);
        IList<User> FindByEmail(string email, int pageIndex, int pageSize);
        User FindByUsername(string userName);
        IList<User> SearchByUserName(string partialUserName, int pageIndex, int pageSize);
        IList<User> FindAll(string sortProperty);
        IList<User> FindAll(int pageIndex, int pageSize);
        
        int FindCountOfUsersOnline();

        void Insert(User user);
        void Update(User user);
        void Delete(User user);

        void ActivateUser(User user);
        void ActivateSeekerUser(User user);
        void ActivateProviderUser(User user);
        void ActivateIntermediaryUser(User user);

        void ResetUsername(User user, string newEmail);

        void SendConfirmationEmail(User user, bool requiresResetPassword);
        void UpdateEmailAddress(User user,string newEmail);
        void ConfirmEmailAddressVerification(User user);

        void SendSeekerRegistrationInterestToAdmin(string email);
    }
}
