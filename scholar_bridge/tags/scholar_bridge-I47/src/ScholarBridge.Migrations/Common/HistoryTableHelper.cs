﻿using System;
using Migrator.Framework;

namespace ScholarBridge.Migrations.Common
{
	class HistoryTableHelper
	{
		public const string NEED_HISTORY_TABLE_NAME = "SBNeedHistoryTables";
		/// <summary>
		/// Add a HISTORY table to the specified input table
		/// </summary>
		/// <param name="database"></param>
		/// <param name="tableName"></param>
		public static void AddHistoryToTable(ITransformationProvider database, string tableName)
		{

			int adminUserId = HECBStandardDataHelper.GetAdminUserId(database);
			string[] insertColumns = new[] { AddHistoryTableProcedures.Columns[1], AddHistoryTableProcedures.Columns[2] };
			database.Insert(NEED_HISTORY_TABLE_NAME, insertColumns, new[] { tableName, adminUserId.ToString() });
			database.ExecuteNonQuery("exec SPCreateHistoryTables");
			RebuildHistoryTriggersForTable(database, tableName);
		}

		/// <summary>
		/// Remove the HISTORY table from the specified input table
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="tableName"></param>
		public static void RemoveHistoryFromTable(ITransformationProvider provider, string tableName)
		{
			// delete the history table
			string historyTableName = tableName + "HISTORY";
			if (provider.TableExists(historyTableName))
				provider.RemoveTable(historyTableName);

			// remove the triggers
			provider.ExecuteNonQuery(
				"IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Delete" + tableName + "]')) " +
				"     DROP TRIGGER [dbo].[Delete" + tableName + "]"
				);

			provider.ExecuteNonQuery(
				"IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Update" + tableName + "]')) " +
				"     DROP TRIGGER [dbo].[Update" + tableName + "]"
				);

		}

		/// <summary>
		/// Rebuild the Update/Delete triggers on a table that will populate the HISTORY table
		/// </summary>
		/// <param name="database"></param>
		/// <param name="tableName"></param>
		public static void RebuildHistoryTriggersForTable(ITransformationProvider database, string tableName)
		{
			database.ExecuteNonQuery("exec SPRebuildHistoryTriggerForTable @t='" + tableName + "'");
			
		}
	}
}
