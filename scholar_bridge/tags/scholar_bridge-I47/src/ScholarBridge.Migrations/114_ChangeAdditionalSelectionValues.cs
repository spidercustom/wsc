using System;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(114)]
    public class ChangeAdditionalSelectionValues : Migration
    {
        public const string TABLE_NAME = "SBAdditionalRequirementLUT";
        public const string COLUMN = "SBAdditionalRequirement";
        public override void Up()
        {
            int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
            Database.Update(TABLE_NAME, new[] {COLUMN}, new[] {"Interview Required"}, String.Format("{0}='{1}'", COLUMN, "Interviews"));
            Database.Update(TABLE_NAME, new[] { COLUMN }, new[] { "Essay(s)" }, String.Format("{0}='{1}'", COLUMN, "Additional Essay(s)"));
            Database.Update(TABLE_NAME, new[] { COLUMN }, new[] { "Transcripts - High School" },  String.Format("{0}='{1}'", COLUMN, "Transcripts"));
            Database.Insert(TABLE_NAME, new[] { COLUMN, "Description", "LastUpdateBy" }, new[] { "Transcripts - College", "Previous college transcripts", adminId.ToString() });
        }

        public override void Down()
        {
            Database.Update(TABLE_NAME, new[] { COLUMN }, new[] { "Interviews" }, String.Format("{0}='{1}'", COLUMN, "Interview Required"));
            Database.Update(TABLE_NAME, new[] { COLUMN }, new[] { "Additional Essay(s)" }, String.Format("{0}='{1}'", COLUMN, "Essay(s)"));
            Database.Update(TABLE_NAME, new[] { COLUMN }, new[] { "Transcripts" }, String.Format("{0}='{1}'", COLUMN, "Transcripts - High School"));
            Database.Delete(TABLE_NAME, COLUMN, "Transcripts - College");
        }
    }
}