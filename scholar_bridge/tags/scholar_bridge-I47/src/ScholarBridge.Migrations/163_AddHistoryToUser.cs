﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(163)]
	public class AddHistoryToUserAndSeeker : Migration
	{
		private readonly string[] _tablesNeedingHistory = new [] { "SBUser" };

		public override void Up()
		{
			foreach (var tableName in _tablesNeedingHistory)
			{
				HistoryTableHelper.AddHistoryToTable(Database, tableName);
			}
		}

		public override void Down()
		{
			foreach (var tableName in _tablesNeedingHistory)
			{
				HistoryTableHelper.RemoveHistoryFromTable(Database, tableName);
			}
		}

	}
}
