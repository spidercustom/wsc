﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(271)]
    public class UpdateReligionLookupData : Migration
	{
		private const string SBReligionLUT = "SBReligionLUT";
        private const String JUDAISAM = "Judaism";
        private const String ISLAMIC = "Islamic";
		
		public override void Up()
		{
            Database.ExecuteNonQuery("update " + SBReligionLUT +
                                     @" set Religion = 'Muslim', Description = 'Muslim'
										where Religion = '" +ISLAMIC+"'" );
            Database.ExecuteNonQuery("update " + SBReligionLUT +
                                     @" set Religion = 'Jewish', Description = 'Jewish'
										where Religion = '" + JUDAISAM + "'");
		}

		public override void Down()
		{
            Database.ExecuteNonQuery("update " + SBReligionLUT +
                                    @" set Religion = 'Islamic', Description = 'Islamic'
										where Religion = 'Muslim'");
            Database.ExecuteNonQuery("update " + SBReligionLUT +
                                     @" set Religion = 'Judaism', Description = 'Judaism'
										where Religion = 'Jewish'");
		}

		
	}
}
