﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(144)]
    public class RenameCommunityInvolvementAndCauseLUT : Migration
    {
        public const string NEW_LOOKUP_TABLE_ID_COLUMN_NAME = "SBCommunityServiceIndex";
        public const string OLD_LOOKUP_TABLE_ID_COLUMN_NAME = "SBCommunityInvolvementCauseIndex";

        private const string NEW_LOOKUP_TABLE_NAME_COLUMN_NAME = "CommunityService";
        private const string OLD_LOOKUP_TABLE_NAME_COLUMN_NAME = "CommunityInvolvementCause";

        private const string NEW_LOOKUP_TABLE_NAME = "SBCommunityServiceLUT";
        private const string OLD_LOOKUP_TABLE_NAME = "SBCommunityInvolvementCauseLUT";
        

        public override void Up()
        {
            Database.RenameTable(OLD_LOOKUP_TABLE_NAME, NEW_LOOKUP_TABLE_NAME);
            Database.RenameColumn(NEW_LOOKUP_TABLE_NAME, OLD_LOOKUP_TABLE_ID_COLUMN_NAME, NEW_LOOKUP_TABLE_ID_COLUMN_NAME);
            Database.RenameColumn(NEW_LOOKUP_TABLE_NAME, OLD_LOOKUP_TABLE_NAME_COLUMN_NAME, NEW_LOOKUP_TABLE_NAME_COLUMN_NAME);
        }

        public override void Down()
        {
            Database.RenameColumn(NEW_LOOKUP_TABLE_NAME, NEW_LOOKUP_TABLE_ID_COLUMN_NAME, OLD_LOOKUP_TABLE_ID_COLUMN_NAME);
            Database.RenameColumn(NEW_LOOKUP_TABLE_NAME, NEW_LOOKUP_TABLE_NAME_COLUMN_NAME, OLD_LOOKUP_TABLE_NAME_COLUMN_NAME);
            Database.RenameTable(NEW_LOOKUP_TABLE_NAME, OLD_LOOKUP_TABLE_NAME);
        }
    }
}