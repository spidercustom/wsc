﻿using System;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(258)]
	public class AddAdditionalAcademicAreas : Migration
	{
		private string LOOKUP_TABLE = "SBAcademicAreaLUT";

		private string[] COLUMNS = new []
		                           	{
		                           		"AcademicArea", "Description", "Deprecated", "LastUpdateBy", "LastUpdateDate"
		                           	};

		private const string REMOVE_KEY = "Science and Engineering";

		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			foreach (var value in LOOKUP_VALUES)
			{
				AddLookup(value, adminId);	
			}

			try
			{
				Database.Delete(LOOKUP_TABLE, COLUMNS[0], REMOVE_KEY);
			}
			catch (Exception)
			{
				// do nothing in-case the lookup value is already attached 
			}

		}

		public override void Down()
		{
			foreach (var value in LOOKUP_VALUES)
			{
				try
				{
					Database.Delete(LOOKUP_TABLE, COLUMNS[0], value);
				}
				catch (Exception)
				{
					// do nothing in-case the lookup value is already attached 
				}
			}
			AddLookup(REMOVE_KEY, HECBStandardDataHelper.GetAdminUserId(Database));
		}

		private void AddLookup(string lookupValue, int adminId)
		{
			int hitCount = (int)Database.ExecuteScalar("select count(*) from " + LOOKUP_TABLE + " where " + COLUMNS[0] + " = '" + lookupValue + "'");
			if (hitCount == 0)
				Database.Insert(LOOKUP_TABLE, COLUMNS,
								new[] { lookupValue, lookupValue, "0", adminId.ToString(), DateTime.Now.ToString() }
					);
		}

		private string[] LOOKUP_VALUES = new []
		                                	{
												"Agriculture",
												"Architecture and design",
												"Art, Performing or Visual",
												"Cultural & ethnic studies",
												"Engineering",
												"Environmental studies & Forestry",
												"Health Sciences  ",
												"History",
												"Language and Linguistics",
												"Literature",
												"Military Sciences",
												"Natural Sciences",
												"Philosophy",
												"Physical Education, Health and Recreation",
												"Public affairs",
												"Social Sciences ",
												"Social work",
												"Transportation",
												"Vocational & Trades, Apprenticeships"
		                                	};
	}
}
