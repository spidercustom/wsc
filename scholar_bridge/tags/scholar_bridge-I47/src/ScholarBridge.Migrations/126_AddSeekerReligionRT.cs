﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(126)]
	public class AddSeekerReligionRT : AddRelationTableBase
	{
		protected override string FirstTableName
		{
			get { return "SBSeeker"; }
		}

		protected override string SecondTableName
		{
			get { return "SBReligionLUT"; }
		}
	}
}
