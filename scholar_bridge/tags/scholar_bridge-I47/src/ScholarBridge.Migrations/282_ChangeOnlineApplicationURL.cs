using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(282)]
    public class ChangeOnlineApplicationURL : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        
        private const string COLUMNA = "OnlineApplicationUrl";


        public override void Up()
        {
            Database.ChangeColumn(TABLE_NAME, new Column(COLUMNA, DbType.String, 150, ColumnProperty.Null));
        }

        public override void Down()
        {
            Database.ChangeColumn(TABLE_NAME, new Column(COLUMNA, DbType.String, 100, ColumnProperty.Null));
        }
    }
}