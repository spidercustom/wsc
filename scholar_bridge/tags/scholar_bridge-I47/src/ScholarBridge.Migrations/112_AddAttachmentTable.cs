using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(112)]
    public class AddAttachmentTable : AddTableBase
    {
        public const string TABLE_NAME = "SBAttachment";
        public const string PRIMARY_KEY_COLUMN = "SBAttachmentId";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("FileName", DbType.String, 100, ColumnProperty.NotNull),
                           new Column("MimeType", DbType.String, 100, ColumnProperty.NotNull),
                           new Column("UniqueName", DbType.String, 36, ColumnProperty.NotNull),
                           new Column("Comment", DbType.String, 250),
                           new Column("SizeInBytes", DbType.Double, ColumnProperty.NotNull),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())")
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return null;
        }
    }
}