﻿using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Data
{
    // XXX: This is not being used?
    public interface IApplicationQuestionAnswerDAL : IDAL<ApplicationQuestionAnswer>
    {
        ApplicationQuestionAnswer FindById(int id);
        ApplicationQuestionAnswer FindByApplicationandQuestion(Application application,ScholarshipQuestion question );
        IList<ApplicationQuestionAnswer> FindByApplication(Application application);
        ApplicationQuestionAnswer Save(ApplicationQuestionAnswer applicationQuestionAnswer);
    }
}