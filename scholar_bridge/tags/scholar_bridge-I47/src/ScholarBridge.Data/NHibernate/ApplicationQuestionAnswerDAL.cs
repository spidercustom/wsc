using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.NHibernate
{
    public class ApplicationQuestionAnswerDAL : AbstractDAL<ApplicationQuestionAnswer>, IApplicationQuestionAnswerDAL
    {
        public ApplicationQuestionAnswer FindByApplicationandQuestion(Application application, ScholarshipQuestion question)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))
                .Add(Restrictions.Eq("Question", question))
                .UniqueResult<ApplicationQuestionAnswer>();
        }

        public IList<ApplicationQuestionAnswer> FindByApplication(Application application)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))
                .List<ApplicationQuestionAnswer>();
        }
    }
}