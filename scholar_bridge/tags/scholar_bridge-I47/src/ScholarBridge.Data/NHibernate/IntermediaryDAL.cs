using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.NHibernate
{
    public class IntermediaryDAL : AbstractDAL<Intermediary>, IIntermediaryDAL
    {
		public IList<Intermediary> FindAllPending()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("ApprovalStatus", ApprovalStatus.PendingApproval))
				.List<Intermediary>();
		}

		/// <summary>
		/// Return all Approved Intermediaries
		/// </summary>
		/// <returns></returns>
		public IList<Intermediary> FindAllActive()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("ApprovalStatus", ApprovalStatus.Approved))
				.List<Intermediary>();
		}

        public Intermediary FindByUser(User user)
        {
            return CreateCriteria()
                .CreateCriteria("Users")
                .Add(Restrictions.Eq("Id", user.Id))
                .SetCacheable(true)
                .UniqueResult<Intermediary>();
        }

        public User FindUserInOrg(Intermediary organization, int userId)
        {
            // XXX: Couldn't figure out how to do this with Criteria
            var query = Session.CreateQuery(@"select user from Intermediary as intermediary
inner join intermediary.Users as user
where intermediary.Id=:intermediaryId AND user.id=:userId");

            query.SetInt32("intermediaryId", organization.Id);
            query.SetInt32("userId", userId);
            return query.UniqueResult<User>();
        }
    }
}