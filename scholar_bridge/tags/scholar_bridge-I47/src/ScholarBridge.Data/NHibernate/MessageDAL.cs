using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.NHibernate
{
    public class MessageDAL : MessageDALBase<Message>, IMessageDAL
    {
        public Message FindById(User user, IList<Role> roles, Organization organization, int id)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            crit.Add(Restrictions.Eq("Id", id));
            return crit.UniqueResult<Message>();
        }

        /// <summary>
        /// The match is if any of the user, roles or organization information matches what is passed
        /// null values in the database means 'do not limit by that field'.
        /// </summary>
        /// <example>
        /// User is set in the db, then only to that user.
        /// Organization is set, then all users in the organization.
        /// Organization and Role are set, then only to those users in the Organization with that role.
        /// </example>
        /// <remarks>
        /// User/Role or User/Organization combo don't make sense.
        /// </remarks>
         
        public IList<Message> FindAll(int startIndex, int rowCount, string sortExpression, MessageAction action, User user, IList<Role> roles, Organization organization)
        {
            var sql = "select {{cat.*}} from SBMessageView  {{cat}} where ( ToUserId={0} Or ToRoleId in ({1}) Or ToOrganizationId={2} ) AND  ActionTaken ='{3}'  order by ";
            var userId = user == null ? 0 : user.Id;
            var organizationId = organization == null ? 0 : organization.Id;
            var roleId = "0";
            if (roles !=null)
            foreach (var r in roles) { roleId = roleId + "," + r.Id; }

            sql = sql.Build(userId, roleId, organizationId,Enum.GetName(typeof (MessageAction ), action));
            var orderby = GetOrderBy(sortExpression);

            return Session.CreateSQLQuery((sql + orderby), "cat", typeof(Message))
                .SetFirstResult(startIndex).SetMaxResults(rowCount).List<Message>();
       }
        public int CountAll(  MessageAction action, User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
                crit.Add(Restrictions.Eq("ActionTaken", action));
                return crit.SetProjection(
                  Projections.CountDistinct("Id")).UniqueResult<int>();


        }

        private string GetOrderBy (  string sortExpression)
        {
            var orderby = "";
            switch (sortExpression)
            {
                case "Subject":
                case "Subject ASC":

                    orderby = "Subject ASC";
                    break;

                case "Subject DESC":
                    orderby = "Subject DESC";
                    break;

                case "SenderName":
                case "SenderName ASC":
                    
                    orderby = "MessageSenderName ASC";
                    break;

                case "SenderName DESC":
                    orderby = "MessageSenderName DESC";
                    break;

                case "OrganizationName":
                case "OrganizationName ASC":

                    orderby = "MessageOrganizationName ASC";
                    break;

                case "OrganizationName DESC":

                    orderby = "MessageOrganizationName DESC";
                    break;

                case "Date":
                case "Date ASC":

                    orderby = "SentDate ASC";
                    break;

                case "Date DESC":
                    orderby = "SentDate DESC";
                    break;
                

                default:
                    orderby = "SentDate DESC";
                    break;
                
            }
            return orderby;
        }

        public IList<Message> FindAllForInbox(int startIndex, int rowCount, string sortExpression, User user, IList<Role> roles, Organization organization)
    	{
            var sql = "select {{cat.*}} from SBMessageView  {{cat}} where ( ToUserId={0} Or ToRoleId in ({1}) Or ToOrganizationId={2} )    AND ActionTaken <> '{3}' AND MessageTemplate <> '{4}' AND IsArchived=0  order by ";
            var userId = user == null ? 0 : user.Id;
            var organizationId = organization == null ? 0 : organization.Id;
            var roleId = "0";
            if (roles != null)
            foreach (var r in roles) { roleId = roleId + "," + r.Id; }

            sql = sql.Build(userId, roleId, organizationId, Enum.GetName(typeof(MessageAction), MessageAction.Deny), Enum.GetName(typeof(MessageType), MessageType.ScholarshipApproved));
            var orderby = GetOrderBy(sortExpression);

            return Session.CreateSQLQuery((sql + orderby), "cat", typeof(Message))
                .SetFirstResult(startIndex).SetMaxResults(rowCount).List<Message>();

		}

        public int CountAllForInbox( User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            
            crit.Add(Restrictions.Not(CreateCriteriaForMessageQuery("MessageTemplate", MessageType.ScholarshipApproved)));
            crit.Add(Restrictions.Not(CreateCriteriaForMessageQuery("IsArchived", true)));
            crit.Add(Restrictions.Not(CreateCriteriaForMessageQuery("ActionTaken", MessageAction.Deny)));
            return crit.SetProjection(
                Projections.CountDistinct("Id")).UniqueResult<int>();
        }

        public IList<Message> FindAllArchived(int startIndex, int rowCount, string sortExpression, User user, IList<Role> roles, Organization organization)
        {
            var sql = "select {{cat.*}} from SBMessageView  {{cat}} where ( ToUserId={0} Or ToRoleId in ({1}) Or ToOrganizationId={2} ) AND IsArchived=1  order by ";
            var roleId = "0";
            if (roles != null)
            foreach (var r in roles) { roleId = roleId + "," + r.Id; }

            sql = sql.Build(user == null ? 0 : user.Id, roleId, organization == null ? 0 : organization.Id);
            var orderby = GetOrderBy(sortExpression);

            return Session.CreateSQLQuery((sql + orderby), "cat", typeof(Message))
                .SetFirstResult(startIndex).SetMaxResults(rowCount).List<Message>();

        }
        public int CountAllArchived(User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);

            return crit.Add(Restrictions.Eq("IsArchived", true)).SetProjection(
                Projections.CountDistinct("Id")).UniqueResult<int>();
             
        }

        public IList<Message> FindAllByMessageType(int startIndex, int rowCount, string sortExpression, MessageType type, User user, IList<Role> roles, Organization organization)
    	{
             var sql = "select {{cat.*}} from SBMessageView  {{cat}} where ( ToUserId={0} Or ToRoleId in ({1}) Or ToOrganizationId={2} ) AND  MessageTemplate='{3}' order by ";
            var roleId = "0";
            if (roles != null)
            foreach (var r in roles) { roleId = roleId + "," + r.Id; }

            sql = sql.Build(user == null ? 0 : user.Id, roleId, organization == null ? 0 : organization.Id,Enum.GetName(typeof(MessageType),type));
            var orderby = GetOrderBy(sortExpression);

            return Session.CreateSQLQuery((sql + orderby), "cat", typeof(Message))
                .SetFirstResult(startIndex).SetMaxResults(rowCount).List<Message>();
    	}
        public int CountAllByMessageType(MessageType type, User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);

            return crit.Add(CreateCriteriaForMessageQuery("MessageTemplate", type)).SetProjection(
                Projections.CountDistinct("Id")).UniqueResult<int>();
        }

    	public int CountUnread(User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            return crit.Add(Restrictions.Eq("IsRead", false)).SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }
        public void DeleteRelated(Scholarship scholarship)
        {
            Session.Delete("from ScholarshipMessage sm where sm.RelatedScholarship=:scholarship", scholarship, NHibernateUtil.Entity(typeof(Scholarship)));
        }

        private ICriteria BuildFindAllCriteria(User user, IEnumerable<Role> roles, Organization organization)
        {
            var crit = CreateCriteria();
            crit.Add(CreateCriteriaForMessageQuery("To.User", user));
            crit.Add(CreateInCriteriaForMessageQuery("To.Role", roles));
			crit.Add(CreateCriteriaForMessageQuery("To.Organization", organization));
			return crit;
        }
    }
}