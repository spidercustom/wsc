using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.NHibernate
{
    public class SentMessageDAL : MessageDALBase<SentMessage>, ISentMessageDAL
    {
        public SentMessage FindById(User user, IList<Role> roles, Organization organization, int id)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            crit.Add(Restrictions.Eq("Id", id));
            return crit.UniqueResult<SentMessage>();
        }

        /// <summary>
        /// The match is if any of the user, roles or organization information matches what is passed
        /// null values in the database means 'do not limit by that field'.
        /// </summary>
        /// <example>
        /// User is set in the db, then only to that user.
        /// Organization is set, then all users in the organization.
        /// Organization and Role are set, then only to those users in the Organization with that role.
        /// </example>
        /// <remarks>
        /// User/Role or User/Organization combo don't make sense.
        /// </remarks>
        
        public IList<SentMessage> FindAll(int startIndex, int rowCount, string sortExpression, User user, IList<Role> roles, Organization organization)
        {
            var sql = "select {{cat.*}} from SBSentMessageView  {{cat}} where FromUserId={0} Or FromRoleId in ({1}) Or FromOrganizationId={2} order by ";
            var userId = user == null ? 0 : user.Id;
            var organizationId = organization == null ? 0 : organization.Id;
            var roleId = "0"; 
            if (roles !=null)
            foreach (var r in roles ){roleId = roleId + "," + r.Id;}
             
            sql = sql.Build(userId, roleId, organizationId);
            var orderby = "";
             

            switch (sortExpression)
            {
                case "Subject":
                case "Subject ASC":
                    
                    orderby = "Subject ASC";
                    break;

                case "Subject DESC":
                    orderby = "Subject DESC";
                    break;

                case "RecipientName":
                case "RecipientName ASC":
                    orderby = "MessageRecipientName ASC";
                    break;

                case "RecipientName DESC":
                    orderby = "MessageRecipientName DESC";
                    break;

                case "OrganizationName":
                case "OrganizationName ASC":
                    orderby = "MessageOrganizationName ASC";
                    break;

                case "OrganizationName DESC":
                    orderby = "MessageOrganizationName DESC";
                    break;

                case "Date":
                case "Date ASC":

                    orderby = "SentDate ASC";
                    break;

                case "Date DESC":
                    orderby = "SentDate DESC";
                    break;
                default:
                    orderby = "SentDate DESC";
                    break;
            }
                return Session.CreateSQLQuery((sql + orderby), "cat", typeof(SentMessage))
                    .SetFirstResult(startIndex).SetMaxResults(rowCount).List<SentMessage>();
        }

        public int CountAll(  User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            return crit.SetProjection(
                  Projections.CountDistinct("Id")).UniqueResult<int>();


        }

        private ICriteria BuildFindAllCriteria(User user, IEnumerable<Role> roles, Organization organization)
        {
            var crit = CreateCriteria();
            crit.Add(CreateCriteriaForMessageQuery("From.User", user));
            crit.Add(CreateInCriteriaForMessageQuery("From.Role", roles));
            crit.Add(CreateCriteriaForMessageQuery("From.Organization", organization));
            return crit;
        }
    }
}