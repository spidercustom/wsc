using NUnit.Framework;

namespace ScholarBridge.Common.Tests
{

    [TestFixture]
    public class UrlBuilderTests
    {

        [Test]
        public void can_build_url()
        {
            var ub = new UrlBuilder("http://www.zorched.net");
            Assert.AreEqual("http://www.zorched.net/", ub.ToString());

            ub.Port = 8080;
            Assert.AreEqual("http://www.zorched.net:8080/", ub.ToString());

            ub.Path = "2009/04/26/scala-and-adding-new-syntax/";
            ub.Port = 80;
            Assert.AreEqual("http://www.zorched.net/2009/04/26/scala-and-adding-new-syntax/", ub.ToString());
        }

        [Test]
        public void can_build_url_scheme_host()
        {
            var ub = new UrlBuilder("http", "www.zorched.net");
            Assert.AreEqual("http://www.zorched.net/", ub.ToString());
        }

        [Test]
        public void can_build_url_scheme_host_port()
        {
            var ub = new UrlBuilder("http", "www.zorched.net", 80);
            Assert.AreEqual("http://www.zorched.net/", ub.ToString());
        }

        [Test]
        public void can_build_url_scheme_host_port_path()
        {
            var ub = new UrlBuilder("http", "www.zorched.net", 80, "2009/04/26/scala-and-adding-new-syntax/");
            Assert.AreEqual("http://www.zorched.net/2009/04/26/scala-and-adding-new-syntax/", ub.ToString());
        }

        [Test]
        public void can_add_query_params()
        {
            var ub = new UrlBuilder("http://www.zorched.net");
            Assert.AreEqual("http://www.zorched.net/", ub.ToString());

            ub.QueryString.Add("test", "true");
            Assert.AreEqual("http://www.zorched.net/?test=true", ub.ToString());

            ub.QueryString.Add("bar", "1");
            Assert.AreEqual("http://www.zorched.net/?test=true&bar=1", ub.ToString());
        }
    }
}