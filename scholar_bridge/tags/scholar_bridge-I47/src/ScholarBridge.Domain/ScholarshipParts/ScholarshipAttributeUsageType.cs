﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public enum ScholarshipAttributeUsageType
    {
        NotUsed = 0,
        Preference = 1,
        Minimum = 2
    }
}
