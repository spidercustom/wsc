﻿
namespace ScholarBridge.Domain.ScholarshipParts
{
    public enum OrganizationContactDisplay
    {
        Provider,
        Intermediary,
    }
}
