﻿
using System;

namespace ScholarBridge.Domain
{
	[Serializable]
	public enum MatchFilterCriteria
	{
		Match100Percent,
		ScholarshipsOfInterest,
		CollegeSelections,
		NewSinceLastLogin,
		ActiveScholarshipsOnly,
		All
	}
}
