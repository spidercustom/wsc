﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeSelectionControl.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.AttributeSelectionControl" %>
<asp:Repeater ID="AttributesControl" runat="server">
  <HeaderTemplate>
    <table width="100%">
      <thead>
        <tr>
          <th style="width:60px"><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" />Minimum</th>
          <th style="width:60px"><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" />Preference</th>
          <th style="width:60px">Not Used</th>
          <th>Attribute</th>
        </tr>
      </thead>
      <tbody>
  </HeaderTemplate>
  <ItemTemplate>
    <tr class="row">
        <td align="center"><asp:RadioButton ID="Minimum" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="Preference" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="NotUsed" runat="server" GroupName="UsageSelector" /></td>
        <td align="left">
            <asp:Literal ID="attributeName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Value") %>'></asp:Literal>
            <input type="hidden" runat="server" id="KeyControl" value='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />
        </td>
    </tr>
  </ItemTemplate>
  <AlternatingItemTemplate>
    <tr class="altrow">
        <td align="center"><asp:RadioButton ID="Minimum" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="Preference" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="NotUsed" runat="server" GroupName="UsageSelector" /></td>
        <td align="left">
            <asp:Literal ID="attributeName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Value") %>'></asp:Literal>
            <input type="hidden" runat="server" id="KeyControl" value='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />
        </td>
    </tr>
  </AlternatingItemTemplate>
  <FooterTemplate>
      </tbody>
    </table>
  </FooterTemplate>
</asp:Repeater>