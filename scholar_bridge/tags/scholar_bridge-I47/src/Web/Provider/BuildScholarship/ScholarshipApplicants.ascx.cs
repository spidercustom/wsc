﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Logging;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using Spring.Context.Support;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class ScholarshipApplicants : WizardStepUserControlBase<Scholarship>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ScholarshipApplicants));
        private   const string CHECKBOX_SCRIPT_TEMPLATE ="javascript:getElementById('{0}').value='{1}'";

        public IScholarshipService ScholarshipService
        {
            get { return (IScholarshipService)ContextRegistry.GetContext().GetObject("ScholarshipService"); }
        }

        public IApplicationService ApplicationService
        {
            get { return (IApplicationService)ContextRegistry.GetContext().GetObject("ApplicationService"); }
        }
        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }
        public IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        private const string DEFAULT_SORTEXPRESSION = "DateSubmitted DESC";
        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        public string LinkTo { get; set; }

        public string EmptyMessage
    	{
    		get {
    			return !Page.IsPostBack ? "No applicants for this scholarship at this time" : "No applicants were selected by the search criteria.";
    			}
    	}

        

        public int ApplicationCount
        {
            get { return ScholarshipInContext.ApplicantsCount; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (! Page.IsPostBack)
            {
                var ub = new UrlBuilder(Request.Url.AbsoluteUri) { PageName = "PrintApplications.aspx" };
                printWindow.OnClientClick = String.Format("printApplications('{0}'); return false;", ub.ToString());

                PopulateScreen();

				if (Page is SBBasePage)
				{
					((SBBasePage)Page).BypassPromptIds.AddRange(
						new[]	{
									"appFinalistCheckBox",
                                    "application","downloadAllFinalistBtn","downloadAllBtn",
                                    "ctl00_bodyContentPlaceHolder_ScholarshipApplicants1_lstApplicants_ColumnHeaderFinalist_btnFinalist"  ,
                                    "ctl00_bodyContentPlaceHolder_ScholarshipApplicants1_lstApplicants_ColumnHeaderAwardStatus_btnAwardStatus",   
                                    "ctl00_bodyContentPlaceHolder_ScholarshipApplicants1_lstApplicants_ColumnHeaderApplicantName_btnApplicantName" ,
                                    "ctl00_bodyContentPlaceHolder_ScholarshipApplicants1_lstApplicants_ColumnHeaderDateSubmitted_btnDateSubmitted" ,
                                    "ctl00_bodyContentPlaceHolder_ScholarshipApplicants1_lstApplicants_ColumnHeaderEligibilityCriteria_btnEligibilityCriteria" ,
                                    "ctl00_bodyContentPlaceHolder_ScholarshipApplicants1_lstApplicants_ColumnHeaderPreferredCriteria_btnPreferredCriteria" ,
                                    "ctl00$bodyContentPlaceHolder$ScholarshipApplicants1$lstApplicants$ColumnHeaderFinalist$btnFinalist",
                                    "ctl00$bodyContentPlaceHolder$ScholarshipApplicants1$lstApplicants$ColumnHeaderAwardStatus$btnAwardStatus",
                                    "ctl00$bodyContentPlaceHolder$ScholarshipApplicants1$lstApplicants$ColumnHeaderApplicantName$btnApplicantName",
                                    "ctl00$bodyContentPlaceHolder$ScholarshipApplicants1$lstApplicants$ColumnHeaderDateSubmitted$btnDateSubmitted",
                                    "ctl00$bodyContentPlaceHolder$ScholarshipApplicants1$lstApplicants$ColumnHeaderEligibilityCriteria$btnEligibilityCriteria",
                                    "ctl00$bodyContentPlaceHolder$ScholarshipApplicants1$lstApplicants$ColumnHeaderPreferredCriteria$btnPreferredCriteria"
								});
				}
            }
            else
            {
                // workaround to see which checkbox has been clicked 
                // events for these checkboxes are not firing due to nested user controls
                HandleFinalistCheckboxes();
                HandleAwardStatusChanges();
            }
        }

        private void PopulateScreen()
        {
             
        }


		protected void lstApplicants_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var minimum = (Label)e.Item.FindControl("lblMin");
                var preference = (Label)e.Item.FindControl("lblPref");
                
                var app = ((Application)((ListViewDataItem)e.Item).DataItem);

                var match = MatchService.GetMatch(app.Seeker, app.Scholarship.Id );
                if (null!=match)
                {
                    minimum.Text = match.MinumumCriteriaString;
                    preference.Text = match.PreferredCriteriaString;
                }
                BindFinalistCheckbox(e, app);
                 
                BindAttachmentsButton(e, app);
                BindAwardStatusDdl(e, app);
            }
        }

        private void HandleFinalistCheckboxes()
        {
            if (String.IsNullOrEmpty(hiddenfinalist.Value))
                return;
            downloadAllFinalistBtn.Enabled = false;
            foreach (ListViewDataItem  dt in lstApplicants.Items )
            {
                var cb = (CheckBox) dt.FindControl("appFinalistCheckBox");
                var appId = Int32.Parse(cb.InputAttributes["value"]);
                if (appId.ToString() == hiddenfinalist.Value)
                {
                    var application = ApplicationService.GetById(appId);
                    application.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    if (cb.Checked)
                    {
                        ApplicationService.Finalist(application);
                    }
                    else
                    {
                        ApplicationService.NotFinalist(application);
                    }
                }
                if (cb.Checked )
                    downloadAllFinalistBtn.Enabled = true;
            }
            hiddenfinalist.Value = "";
            searchApplicants_Click(null, null);
        }

        private void HandleAwardStatusChanges()
        {

            if (String.IsNullOrEmpty(hiddenawardstatus.Value))
                return;

            foreach (ListViewDataItem dt in lstApplicants.Items)
            {
                var ddl = (DropDownList)dt.FindControl("awardStatusDdl");
                var parts = ddl.SelectedValue.Split('_');
                var appId = Int32.Parse(parts[0]);
                var newStatus = (AwardStatus)Enum.Parse(typeof(AwardStatus), parts[1]);

                if (appId.ToString() == hiddenawardstatus.Value)
                {
                    var application = ApplicationService.GetById(appId);
                    application.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    switch (newStatus)
                    {
                        case AwardStatus.Awarded:
                            ApplicationService.AwardScholarship(application, UserContext.CurrentUser);
                            break;
                        case AwardStatus.NotAwarded:
                            ApplicationService.NotAwardScholarship(application, UserContext.CurrentUser);
                            break;
                        case AwardStatus.Offered:
                            ApplicationService.OfferScholarship(application, UserContext.CurrentUser);
                            break;
                        default:
                            application.AwardStatus = newStatus;
                            ApplicationService.Update(application);
                            break;
                    }
                }
            }

            hiddenawardstatus.Value = "";
            searchApplicants_Click(null, null);
        }

        protected void searchApplicants_Click(object sender, EventArgs e)
        {
            lstApplicants.DataBind();
        }

        protected void clearSearchBtn_Click(object sender, EventArgs e)
        {
            searchApplicantsTxt.Text = null;
            lstApplicants.DataBind();
        }

        protected void downloadAllBtn_OnClick(object sender, EventArgs e)
        {
            var Applications = GetAllApplicants();
            
            if (log.IsInfoEnabled) log.Info(String.Format("Downloading all application attachments for Scholarship id: '{0}'", ScholarshipInContext.Id));
            DownloadZip(Applications);
        }

        protected void downloadAllFinalistBtn_OnClick(object sender, EventArgs e)
        {
            var Applications = GetAllApplicants();

            if (log.IsInfoEnabled) log.Info(String.Format("Downloading all finalist application attachments for Scholarship id: '{0}'", ScholarshipInContext.Id));
            DownloadZip(Applications.Where(a => a.Finalist));
        }

        protected void saveAwardPeriodDate_Click(object sender, EventArgs e)
        {
            if (awardPeriodCloseDate.SelectedDate.HasValue)
            {
                ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                ScholarshipService.CloseAwardPeriod(ScholarshipInContext, awardPeriodCloseDate.SelectedDate.Value);
            }
            searchApplicants_Click(null, null);
        }


        public IList<Application> GetApplicants(string sortExpression, int startRowIndex, int maximumRows, string search,int scholarshipId)
        {
            if (string.IsNullOrEmpty(sortExpression))
                sortExpression = DEFAULT_SORTEXPRESSION;
            var scholarship = ScholarshipService.GetById(scholarshipId);
            IList<Application> results = new  List<Application>();
            if (null != scholarship)
            {
                results= String.IsNullOrEmpty(search)
                    ? ApplicationService.FindAllSubmitted(startRowIndex,maximumRows,sortExpression, scholarship)
                    : ApplicationService.FindAllSubmitted(startRowIndex, maximumRows, sortExpression, scholarship, search);
            }

            return results;
         }

         public int CountApplicants(  string search,int scholarshipId)
         {
             var scholarship = ScholarshipService.GetById(scholarshipId);
             var result = 0;
             if (null != scholarship)
             {
                 result = String.IsNullOrEmpty(search)
                     ? ApplicationService.CountAllSubmitted(  scholarship)
                     : ApplicationService.CountAllSubmitted(  scholarship, search);
             }
             return result;
         }

         protected void ApplicantsDataSource_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
         {
             
             e.InputParameters["search"] = searchApplicantsTxt.Text;
             e.InputParameters["scholarshipId"] = ScholarshipInContext.Id;
              
         }

         protected void ApplicantsDataSource_OnSelected(object source, ObjectDataSourceStatusEventArgs e)
         {
             var applications = (IList<Application>) e.ReturnValue;
             if (ScholarshipInContext.AwardPeriodClosed.HasValue)
                 awardPeriodCloseDate.SelectedDate = ScholarshipInContext.AwardPeriodClosed.Value;
             saveAwardPeriodDate.NeedsConfirmation = !(null != applications && applications.Any(a => a.AwardStatus == AwardStatus.Awarded));
             if (null == applications || applications.Count == 0)
             {
                 downloadAllBtn.Enabled = false;
                 downloadAllFinalistBtn.Enabled = false;
             }
             else if (0 == applications.Count(a => a.Finalist))
             {
                 downloadAllFinalistBtn.Enabled = false;
             }
         }

         public IList<Application> GetAllApplicants()
         {
              
             IList<Application> results = new List<Application>();
             if (null != ScholarshipInContext)
                 results = ApplicationService.FindAllSubmitted(ScholarshipInContext);
             return results;
         }
         

        private void BindFinalistCheckbox(ListViewItemEventArgs e, Application app)
        {
            var cb = (CheckBox)e.Item.FindControl("appFinalistCheckBox");
            cb.InputAttributes.Add("value", app.Id.ToString());
            cb.Checked = app.Finalist;
            cb.Attributes.Add("onclick", CHECKBOX_SCRIPT_TEMPLATE.Build(hiddenfinalist.ClientID,  app.Id.ToString()) );
            // FIXME: When an app is awarded or denied or something, we probably want to disable this.
            // cb.Enabled = app.Stage != ApplicationStages.Awarded;
        }

        

        private void BindAttachmentsButton(ListViewItemEventArgs e, Application app)
        {
            var btn = (Button)e.Item.FindControl("attachmentsBtn");
            if (app.HasAttachments)
            {
                btn.CommandArgument = app.Id.ToString();
                var count = app.Attachments.Count;
                btn.Text = count == 1 ? "1 file" : String.Format("{0} files", count);
				var url = String.Format("{0}?id={1}", ConfigHelper.GetPageURLViaProxy(ResolveUrl("~/Common/ApplicationFiles.aspx")), app.Id);
				btn.Attributes.Add("onclick", String.Format("applicationAttachments('{0}'); return false;", url));
            }
            else
            {
                btn.Text = "None";
                btn.Enabled = false;
            }
        }

        private void BindAwardStatusDdl(ListViewItemEventArgs e, Application application)
        {
            var ddl = (DropDownList)e.Item.FindControl("awardStatusDdl");
            ddl.DataSource = GetStatusValues(application);
            ddl.DataValueField = "Value";
            ddl.DataTextField = "Key";
            ddl.DataBind();
            ddl.Items[0].Text = "- Select One -";

            if (application.AwardStatus != AwardStatus.None)
            {
                ddl.SelectedValue = BuildAwardStatusValue(application);
            }

            ddl.Attributes.Add("onchange", CHECKBOX_SCRIPT_TEMPLATE.Build(hiddenawardstatus.ClientID, application.Id.ToString()));
        }

        

        private Dictionary<string, string> GetStatusValues(Application application)
        {
            // Convert the value into format <application id>_<awardstatus>
            var newValues = new Dictionary<string, string>();
            var t = typeof (AwardStatus);
            foreach (var name in Enum.GetNames(t))
            {
                var displayName = ((AwardStatus) Enum.Parse(t, name)).GetDisplayName();
                newValues[displayName] = BuildAwardStatusValue(application, name);
            }

            return newValues;
        }

        private string BuildAwardStatusValue(Application app)
        {
            return BuildAwardStatusValue(app, app.AwardStatus.ToString());
        }

        private string BuildAwardStatusValue(Application app, string status)
        {
            return String.Format("{0}_{1}", app.Id, status);
        }

         
        protected void pager_PreRender(object sender, EventArgs e)
        {
            var pager = (DataPager) sender;
            pager.Visible = pager.TotalRowCount > pager.PageSize;
        }
        private void DownloadZip(IEnumerable<Application> applications)
        {
            var mf = new MultipleFiles();
            foreach (var application in applications)
            {
                if (application.HasAttachments)
                {
                    var attachments = application.Attachments;
                    var directory = application.Seeker.Name.NameFirstLast;
                    attachments.ForEach(a =>
                                            {
                                                var fullPath = a.GetFullPath(ConfigHelper.GetAttachmentsDirectory());
                                                if (log.IsDebugEnabled)
                                                    log.Debug(String.Format("Adding file to zip: {0}", fullPath));
                                                
                                                mf.AddFile(Path.Combine(directory, a.Name), fullPath);
                                            });
                }
            }

            String file = null;
            try
            {
                file = Path.GetTempFileName();
                if (log.IsDebugEnabled) log.Debug(String.Format("Creating zip file: {0}", file));

                mf.CreateZip(file);

                if (log.IsDebugEnabled) log.Debug("Zip file created");

                FileHelper.SendZipFile(Response, file);
            }
            finally
            {
                if (null != file)
                {
                    try { File.Delete(file); } catch { }
                }
            }
        }
        #region IWizardStepControl implementation
        public override void Save()
        {
             
        }

        public override void PopulateObjects()
        {
        }

        public override bool ValidateStep()
        {
            var result = Validation.Validate(ScholarshipInContext);
            return result.IsValid;
        }


        #endregion
        
    }
}