﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class FundingProfile : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        private bool ImpersonationInProgress
        {
            get { return ImpersonateUser.IsImpersonationInProgress(Session); }
        }
        private Scholarship ScholarshipInContext
    	{
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        private void DisableControls()
        {
            FAFSARequiredRadioList.Enabled = false;
            FAFSAEFCRequiredRadioList.Enabled = false;
        	ApplicantNeedRequiredRadioList.Enabled = false;
        
        }

        private void PopulateScreen()
        {
            if (ScholarshipInContext.IsActivated() && !ImpersonationInProgress)
                DisableControls();
            PopulateScreenFinacialNeed();
        }

        private void PopulateScreenFinacialNeed()
        {
			if (ScholarshipInContext.IsFAFSARequired.HasValue)
				FAFSARequiredRadioList.SelectedIndex = ScholarshipInContext.IsFAFSARequired.Value ? 0 : 1;
			else
				FAFSARequiredRadioList.SelectedIndex = -1;

			if (ScholarshipInContext.IsFAFSAEFCRequired.HasValue)
				FAFSAEFCRequiredRadioList.SelectedIndex = ScholarshipInContext.IsFAFSAEFCRequired.Value ? 0 : 1;
			else
				FAFSAEFCRequiredRadioList.SelectedIndex = -1;

			if (ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired.HasValue)
				ApplicantNeedRequiredRadioList.SelectedIndex = ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired.Value ? 0 : 1;
			else
				ApplicantNeedRequiredRadioList.SelectedIndex = -1;
		}

        public override void PopulateObjects()
        {
            PopulateObjectsFinacialNeed();

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStage.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStage.NotActivated;
        }

        private void PopulateObjectsFinacialNeed()
        {
            ScholarshipInContext.IsFAFSARequired = null;
			switch (FAFSARequiredRadioList.SelectedIndex)
			{
				case 0:
					ScholarshipInContext.IsFAFSARequired = true;
					break;
				case 1:
					ScholarshipInContext.IsFAFSARequired = false;
					break;
			}

            ScholarshipInContext.IsFAFSAEFCRequired = null;
			switch (FAFSAEFCRequiredRadioList.SelectedIndex)
			{
				case 0:
					ScholarshipInContext.IsFAFSAEFCRequired = true;
					break;
				case 1:
					ScholarshipInContext.IsFAFSAEFCRequired = false;
					break;
			}

            ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired = null;

			switch (ApplicantNeedRequiredRadioList.SelectedIndex)
			{
				case 0:
					ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired = true;
					break;
				case 1:
					ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired = false;
					break;
			}

			// add/remove attribute usage for 'Need' based on button checked
			if (ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired.HasValue && ScholarshipInContext.IsApplicantDemonstrateFinancialNeedRequired.Value)
			{
				if (!ScholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.Need))
				{
					ScholarshipInContext.FundingProfile.AttributesUsage.Add(new FundingProfileAttributeUsage {Attribute = FundingProfileAttribute.Need, UsageType = ScholarshipAttributeUsageType.Minimum});
				}
			}
			else
			{
        		FundingProfileAttributeUsage need =
        			ScholarshipInContext.FundingProfile.FindAttributeUsage(FundingProfileAttribute.Need);
				if (need != null)
				{
					ScholarshipInContext.FundingProfile.AttributesUsage.Remove(need);
				}
			}

			// SupportedSituation is always required so we'll always add it
			if (!ScholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.SupportedSituation))
			{
				ScholarshipInContext.FundingProfile.AttributesUsage.Add(new FundingProfileAttributeUsage { Attribute = FundingProfileAttribute.SupportedSituation, UsageType = ScholarshipAttributeUsageType.Minimum });
			}
        }

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int)WizardStepName.MatchCriteria);
		}

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public decimal GetMoneyValue(TextBox tb, PropertyProxyValidator validator)
        {
            decimal amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (! Decimal.TryParse(tb.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public int GetIntValue(TextBox tb, PropertyProxyValidator validator)
        {
            int amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (!Int32.TryParse(tb.Text, NumberStyles.Integer | NumberStyles.AllowThousands, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public void PopulateListControl(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }

        #endregion
    }
}