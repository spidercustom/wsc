﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SentMessageList.ascx.cs" Inherits="ScholarBridge.Web.Common.SentMessageList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:ObjectDataSource ID="MessageDataSource" runat="server"
    SelectMethod="GetSentMessages" TypeName="ScholarBridge.Web.Common.SentMessageList"    
      SortParameterName="sortExpression"   
      SelectCountMethod="CountSentMessages"  EnablePaging="true"  MaximumRowsParameterName="maximumRows" 
      
         >
         
</asp:ObjectDataSource>
<sbCommon:SortableListView ID="messageList" runat="server" 
    onitemdatabound="messageList_ItemDataBound"   DataKeyNames="Id" DataSourceID="MessageDataSource" SortExpressionDefault="Date"
			SortDirectionDefault="Descending">
    <LayoutTemplate>
        <table  class="sortableListView" >
            <thead>
                <tr>
                    <th><sbCommon:SortableListViewColumnHeader Key="Subject" Text="Subject"  ID="SortableListViewColumnHeader1" runat="server"  /></th>
                    <th><sbCommon:SortableListViewColumnHeader Key="RecipientName" Text="To"  ID="SortableListViewColumnHeader3" runat="server"  /></th>
                    <th><sbCommon:SortableListViewColumnHeader Key="OrganizationName" Text="Organization Name"  ID="SortableListViewColumnHeader2" runat="server"  /></th>
                    <th><sbCommon:SortableListViewColumnHeader Key="Date" Text="Date"  ID="SortableListViewColumnHeader4" runat="server"  /></th>
                     
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>    
        <div class="pager">
    <asp:DataPager runat="server" ID="pager" PagedControlID="messageList" PageSize="20" >
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false"  />
            <sbCommon:CustomNumericPagerField  VisiblePageNumberCount="25"
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
    <br />
</div> 

        </LayoutTemplate>
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton id="linkToMessage"  Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
            <td><%# DataBinder.Eval(Container.DataItem, "RecipientName")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "OrganizationName")%></td>
            <td><asp:Label ID="lblDate" runat="server"></asp:Label></td>
             
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton id="linkToMessage"  Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
            <td><%# DataBinder.Eval(Container.DataItem, "RecipientName")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "OrganizationName")%></td>
            <td><asp:Label ID="lblDate" runat="server"></asp:Label></td>
            
        </tr>
    </AlternatingItemTemplate>
</sbCommon:SortableListView>
