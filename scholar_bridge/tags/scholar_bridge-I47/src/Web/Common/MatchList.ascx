﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>

<%@ Import Namespace="ScholarBridge.Domain.Auth"%>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<asp:ObjectDataSource ID="MatchDataSource" runat="server" 
    SelectMethod="GetMatchesForSeekerWithoutApplications" 
    TypeName="ScholarBridge.Web.Common.MatchList"    
    SortParameterName="sortExpression"  
    SelectCountMethod="CountMatchesForSeekerWithoutApplications"  EnablePaging="true"  MaximumRowsParameterName="maximumRows">
</asp:ObjectDataSource>

<div id="FilterButtons">
    <span style="font-weight:bold; font-size:13px; color:black;">Filter: </span>
    <asp:button CssClass="button" ID="hundredPercentButton" runat="server" Text="100% Match" onclick="hundredPercentButton_Click" />
    <asp:button CssClass="button" ID="scholarshipsOfInterestButton" runat="server" Text="Scholarships of Interest" onclick="scholarshipsOfInterestButton_Click" />
    <asp:button CssClass="button" ID="collegeMatchButton" runat="server" Text="College Matches" onclick="collegeMatchButton_Click" />
    <asp:button CssClass="button" ID="newMatchesButton" runat="server" Text="New Matches" onclick="newMatchesButton_Click" />
    <asp:button CssClass="button" ID="activeOnlyButton" runat="server" Text="Active Scholarships Only" onclick="activeOnlyButton_Click" />
    <asp:button CssClass="button" ID="allMatchesButton" runat="server" Text="All Matches" onclick="allMatchesButton_Click" />
</div>

<asp:hiddenfield runat="server" ID="matchCriteriaIndex" Value='<%= CRITERIA_MATCH_100_PERCENT %>' />

<sbCommon:SortableListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    DataKeyNames="Id" DataSourceID="MatchDataSource" 
    SortExpressionDefault="Rank"
	SortDirectionDefault="Descending"   >
    <LayoutTemplate>
    <style type="text/css">
        #FilterButtons{text-align:center;}
        #FilterButtons input.button{margin:0px 3px; padding:0px 4px;}
        table.MatchList td, th{padding:2px 3px; margin:0px; border-bottom:1px solid #efefef;}
        table.MatchList thead{background-color:#eee;}
        table.MatchList tr{display:table-row; border-bottom:1px solid #eee;}
        table.MatchList th a{color:Black;}
        table.MatchList .button{font-size:11px; font-weight:normal;}
    </style>
    <table class="MatchList" style="margin-top:20px;">
            <thead>
            <tr>
                <th></th>
                <th style="width:250px;"><sbCommon:SortableListViewColumnHeader Key="ScholarshipName" Text="Scholarship Name"  ID="SortableListViewColumnHeader1" runat="server"  /></th>
                <th style="width:50px" class="tip" title="Matches are ranked based on your profile and the number of minimum eligibility and preferred criteria you meet. Minimum requirements make up 2/3 of the rank, while preferences count for 1/3.">
                    <sbCommon:SortableListViewColumnHeader Key="Rank" Text="Rank"  ID="SortableListViewColumnHeader2" runat="server"  />
                </th>
                <th class="tip" title="The number of minimum eligibility requirements that match your profile and are defined in the Scholarship.">
                    <sbCommon:SortableListViewColumnHeader Key="EligibilityCriteria" Text="Eligibility Criteria"  ID="SortableListViewColumnHeader3" runat="server"  />
                </th>
                <th class="tip" title="The number of preferred criteria that match your profile and are defined in the scholarship.">
                  <sbCommon:SortableListViewColumnHeader Key="PreferredCriteria" Text="Preferred Criteria"  ID="SortableListViewColumnHeader4" runat="server"  />
               </th>
                <th class="tip" title="The date the scholarship application is due.">
                    <sbCommon:SortableListViewColumnHeader Key="ApplicationDueDate" Text="Application Due Date"  ID="SortableListViewColumnHeader5" runat="server"  />
                </th>
                <th class="tip" title="The anticipated number of scholarship awards available.">
                    <sbCommon:SortableListViewColumnHeader Key="NumberOfAwards" Text="# of Awards"  ID="SortableListViewColumnHeader6" runat="server"  />
                </th>
                <th class="tip" title="The anticipated amount of the scholarship award.">
                    <sbCommon:SortableListViewColumnHeader Key="ScholarshipAmount" Text="Scholarship Amount"  ID="SortableListViewColumnHeader7" runat="server"  />
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    <div class="pager">
    <asp:DataPager runat="server" ID="pager"   OnPreRender="pager_PreRender"  PageSize="20">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField  VisiblePageNumberCount="25"
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
    </div> 
    </LayoutTemplate>
          
    <ItemTemplate>
        <tr class="row">
            <td><asp:ImageButton id="IconButton" runat="server" CommandName="iconClicked"  ImageUrl="~/images/tag.png"/></td>
            <td><asp:LinkButton id="linktoScholarship" runat="server" >
                    <%# ((ScholarBridge.Domain.Match)Container.DataItem).CreateDate != null ? (((ScholarBridge.Domain.Match)Container.DataItem).CreateDate.Value  > DateTime.Now.AddHours(-24) ? "<b><i>New!</i></b> " : "") : ""%>
                    <%# Eval("ScholarshipName")%>
                </asp:LinkButton>
            </td>
            <td><%# Eval("Rank", "{0:##}%")%></td>
            <td><%# Eval("MinumumCriteriaString")%></td>
            <td><%# Eval("PreferredCriteriaString")%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
            <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="button" Width="70px" /></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:ImageButton  id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png" /></td>
            <td><asp:LinkButton id="linktoScholarship" runat="server" >
                    <%# ((ScholarBridge.Domain.Match)Container.DataItem).CreateDate != null ? (((ScholarBridge.Domain.Match)Container.DataItem).CreateDate.Value  > DateTime.Now.AddHours(-24) ? "<b><i>New!</i></b> " : "") : ""%>
                    <%# Eval("ScholarshipName")%>
                </asp:LinkButton>
            </td>
            <td><%# Eval("Rank", "{0:##}%")%></td>
            <td><%# Eval("MinumumCriteriaString")%></td>
            <td><%# Eval("PreferredCriteriaString")%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
            <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="button" Width="70px" /></td>
        </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>The system could not find any scholarship matches for you. If you haven't already, please complete your profile; otherwise, try clearing out any filters you've placed by clicking on "All Matches" above.</p>
    </EmptyDataTemplate>
</sbCommon:SortableListView>

