﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipSearchBox.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipSearchBox" %>
<div id="Search">
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="SearchBtn">
    <asp:TextBox ID="txtSearch" runat="server"  />
    <asp:Button Cssclass="button" ID="SearchBtn" runat="server" OnClick="SearchBtn_Click" />
</asp:Panel>
</div>
 