﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerStudentType.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerStudentType" %>

<div>
    <asp:RadioButton Text="Middle School" runat="server" ID="MiddleSchool" GroupName="studentGroup"  /><br />
    <asp:RadioButton Text="High School Freshman" runat="server" ID="HighSchoolFreshMan" GroupName="studentGroup" /><br />
    <asp:RadioButton Text="High School Sophomore" runat="server" ID="HighSchoolSophomore" GroupName="studentGroup" /><br />
    <asp:RadioButton Text="High School Junior" runat="server" ID="HighSchoolJunior" GroupName="studentGroup" /><br />
    <asp:RadioButton Text="High School Senior" runat="server" ID="HighSchoolSenior" GroupName="studentGroup" /><br />
    <asp:RadioButton Text="High School Graduate" runat="server" ID="HighSchoolGraduate" GroupName="studentGroup"/><br />
</div>
