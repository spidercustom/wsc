﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationAdditionalRequirementsShow.ascx.cs" Inherits="ScholarBridge.Web.Common.ApplicationAdditionalRequirementsShow" %>
<%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>

<div id="AddRequirementsBlock" runat="server">
<div class="viewLabel">Additional Requirements:</div> 
<div class="viewValue">
<asp:Repeater ID="additionalRequirements" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<br />
</div>

<div id="QABlock" runat="server">
<div class="viewLabel">Questions & Answers:</div> 
<div class="viewValue">
<asp:Repeater ID="questions" runat="server">
    <ItemTemplate>
    <h4><%# Eval("Question.QuestionText") %></h4>
    <%# Eval("AnswerText")%> <br />
    </ItemTemplate>
</asp:Repeater>
</div>
<br />
</div>

<div id="AttachmentsBlock" runat="server">
<h4>Application Attachments</h4>
<sb:FileList id="applicationAttachments" runat="server" />
</div>
<br />
