﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using System.Collections.Generic;
using Spring.Context.Support;


namespace ScholarBridge.Web.Common
{
	public delegate void MatchIconHandler(Match match, ImageButton iconButton);

	public delegate void MatchActionButtonHandler(Match match, Button button);

	public partial class MatchList : UserControl
	{
		protected const int CRITERIA_ACTIVE_SCHOLARSHIPS_ONLY = 1;
		protected const int CRITERIA_ALL = 2;
		protected const int CRITERIA_COLLEGE_SELECTIONS = 3;
		protected const int CRITERIA_MATCH_100_PERCENT = 4;
		protected const int CRITERIA_NEW_SINCE_LAST_LOGIN = 5;
		protected const int CRITERIA_SCHOLARSHIPS_OF_INTEREST = 6;

		private int CurrentFilterCriteriaIndex
		{
			get
			{
				if (Context.Session["CurrentFilterCriteriaIndex"] == null)
				{
					return CRITERIA_MATCH_100_PERCENT; //default
				}
				return int.Parse((string)Context.Session["CurrentFilterCriteriaIndex"]);
			}
			set
			{
				Context.Session["CurrentFilterCriteriaIndex"] = value.ToString();
			}
		}
		private MatchFilterCriteria CurrentFilterCriteria
		{
			get
			{
				switch (CurrentFilterCriteriaIndex)
				{
					case 1:
						return MatchFilterCriteria.ActiveScholarshipsOnly;
					case 2:
						return MatchFilterCriteria.All;
					case 3:
						return MatchFilterCriteria.CollegeSelections;
					case 4:
						return MatchFilterCriteria.Match100Percent;
					case 5:
						return MatchFilterCriteria.NewSinceLastLogin;
					case 6:
						return MatchFilterCriteria.ScholarshipsOfInterest;
					default:
						return MatchFilterCriteria.Match100Percent;
				}
			}
		}

		private const string DEFAULT_SORTEXPRESSION = "Rank DESC";
		public string LinkTo { get; set; }


		public IMatchService MatchService
		{
			get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
		}

		protected virtual IUserContext UserContext
		{
			get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
		}
		public ListView List
		{
			get { return matchList; }
		}

		public MatchIconHandler IconConfigurator { get; set; }
		public MatchActionButtonHandler ActionButtonConfigurator { get; set; }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			matchList.ItemCommand += matchList_ItemCommand;

		}

		private void matchList_ItemCommand(object sender, ListViewCommandEventArgs e)
		{
			Console.WriteLine(e.CommandArgument);
		}

		public IList<Match> GetMatchesForSeekerWithoutApplications(string sortExpression, int startRowIndex, int maximumRows)
		{
			if (string.IsNullOrEmpty(sortExpression))
				sortExpression = DEFAULT_SORTEXPRESSION;

			return MatchService.GetMatchesForSeekerWithoutApplications(startRowIndex, maximumRows, sortExpression, UserContext.CurrentSeeker, CurrentFilterCriteria);

		}

		public int CountMatchesForSeekerWithoutApplications()
		{
			var count = MatchService.CountMatchesForSeekerWithoutApplications(UserContext.CurrentSeeker, CurrentFilterCriteria);
			return count;
		}

		protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var match = ((Match)((ListViewDataItem)e.Item).DataItem);

				var link = (LinkButton)e.Item.FindControl("linktoScholarship");
				var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
						  + "?id=" + match.Scholarship.Id + "&print=true";
				link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
			}
		}

		protected void pager_PreRender(object sender, EventArgs e)
		{
			var pager = (DataPager)sender;
			pager.Visible = pager.TotalRowCount > pager.PageSize;
		}

		protected void hundredPercentButton_Click(object sender, EventArgs e)
		{
			CurrentFilterCriteriaIndex = CRITERIA_MATCH_100_PERCENT;
			matchList.DataBind();
		}

		protected void scholarshipsOfInterestButton_Click(object sender, EventArgs e)
		{
			CurrentFilterCriteriaIndex = CRITERIA_SCHOLARSHIPS_OF_INTEREST;
			matchList.DataBind();
		}

		protected void collegeMatchButton_Click(object sender, EventArgs e)
		{
			CurrentFilterCriteriaIndex = CRITERIA_COLLEGE_SELECTIONS;
			matchList.DataBind();
		}

		protected void newMatchesButton_Click(object sender, EventArgs e)
		{
			CurrentFilterCriteriaIndex = CRITERIA_NEW_SINCE_LAST_LOGIN;
			matchList.DataBind();
		}

		protected void activeOnlyButton_Click(object sender, EventArgs e)
		{
			CurrentFilterCriteriaIndex = CRITERIA_ACTIVE_SCHOLARSHIPS_ONLY;
			matchList.DataBind();
		}

		protected void allMatchesButton_Click(object sender, EventArgs e)
		{
			CurrentFilterCriteriaIndex = CRITERIA_ALL;
			matchList.DataBind();
		}
	}
}