﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupTableList.ascx.cs" Inherits="ScholarBridge.Web.Admin.Editors.Lookups.LookupTableList" %>

<label for="tableDropDown">Select a table:&nbsp;&nbsp;</label>
<asp:dropdownlist 
    ID="tableDropDown" 
    runat="server" 
    AutoPostBack="true"
    DataSourceID="lookupTableListSource" 
    DataTextField="Name"
    DataValueField="AssemblyQualifiedName"
    AppendDataBoundItems="true"
>
    <asp:ListItem Text="Select a Table" Value=""></asp:ListItem>
</asp:dropdownlist>
<asp:GridView 
    ID="gvLookupTables" 
    AutoGenerateColumns="False"
    DataSourceID="lookupTableDataSource"
    DataKeyNames="Id"
    ShowFooter="true"
    runat="server"
     AllowPaging="true"
     AllowSorting="true"
     PageSize="25"
     >
    <Columns>
        <asp:BoundField DataField="Id" Visible="false" /> 
        <asp:TemplateField HeaderText="Name" SortExpression="Name">
            <EditItemTemplate>
                <asp:TextBox ID="NameBox" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                <asp:requiredfieldvalidator ID="nameRequired" ValidationGroup="UpdateTableItem"  ControlToValidate="NameBox" runat="server" EnableClientScript="true" ErrorMessage="*Required" Display="Dynamic"></asp:requiredfieldvalidator>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
            </ItemTemplate>
            <footertemplate>
                <asp:TextBox ID="NameAddBox" runat="server"></asp:TextBox>
                <asp:requiredfieldvalidator ID="NameAddRequired" ValidationGroup="AddTableItem" ControlToValidate="NameAddBox" runat="server" EnableClientScript="true" ErrorMessage="*Required" Display="Dynamic"></asp:requiredfieldvalidator>
            </footertemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Description" SortExpression="Description" >
            <EditItemTemplate>
                <asp:TextBox ID="DescriptionBox" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
            </ItemTemplate>
            <footertemplate>
                <asp:TextBox ID="DescriptionAddBox" runat="server"></asp:TextBox>
            </footertemplate>
            
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Deprecated" SortExpression="Deprecated">
            <EditItemTemplate>
                <asp:dropdownlist 
                        ID="DeprecatedBoolList" 
                        SelectedValue='<%# Bind("Deprecated") %>'
                        runat="server">
                    <asp:ListItem Text="False" Value="False"></asp:ListItem>
                    <asp:ListItem Text="True" Value="True"></asp:ListItem>
                </asp:dropdownlist>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Deprecated") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Last Update" SortExpression="On">
            <ItemTemplate>
                <asp:Label ToolTip='<%# Eval("LastUpdate.By.Name") %>' ID="Label1" runat="server" Text='<%# Eval("LastUpdate.On") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ShowHeader="False">
            <EditItemTemplate>
                <asp:LinkButton ID="LinkButton1" runat="server" 
                    ValidationGroup="UpdateTableItem"
                    CausesValidation="True" 
                    CommandName="Update" Text="Update"></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                    CommandName="Cancel" Text="Cancel"></asp:LinkButton>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="LinkButton1" runat="server" 
                    CausesValidation="False" 
                    CommandName="Edit" Text="Edit"></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                    CommandName="Delete" Text="Deprecate"></asp:LinkButton>
            </ItemTemplate>
            <footertemplate>
                <asp:LinkButton ID="AddItemButton" ValidationGroup="AddTableItem"  runat="server" 
                    CausesValidation="true" 
                    CommandName="AddNew" Text="Add New" OnClick="AddLookupTableData">
                </asp:LinkButton>
            
            </footertemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:ObjectDataSource 
    ID="lookupTableListSource" 
    TypeName="ScholarBridge.Web.Admin.Editors.Lookups.LookupTableList"
    OnObjectCreating="dataSource_ObjectCreating"
    SelectMethod="GetLookupTableList"
    runat="server">
</asp:ObjectDataSource>
<asp:ObjectDataSource 
    ID="lookupTableDataSource" 
    TypeName="ScholarBridge.Web.Admin.Editors.Lookups.LookupTableList"
    OnObjectCreating="dataSource_ObjectCreating"
    SortParameterName="SortColumn"
    SelectMethod="GetLookupTableData"
    UpdateMethod="UpdateLookupTableData"
    DeleteMethod="DeleteLookupTableData"
    runat="server">
	<SelectParameters>
		<asp:ControlParameter ControlID="tableDropDown" PropertyName="SelectedValue" Name="qualifiedClassName" Type="string" />
	</SelectParameters>
</asp:ObjectDataSource>

