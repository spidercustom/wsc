﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="DisplayOrganization.aspx.cs" Inherits="ScholarBridge.Web.Admin.DisplayOrganization" Title="Admin | Display Organization" %>

<%@ Register TagPrefix="sb" TagName="ShowOrg" Src="~/Admin/ShowOrg.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderGeneralImage.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderGeneralImage.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <h2><asp:label ID="orgName" runat="server"></asp:label></h2>
    <sb:ShowOrg id="showOrg" runat="server" />

</asp:Content>
