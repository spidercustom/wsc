﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Basics.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Basics" %>
<%@ Register TagPrefix="elv"  Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" %>
<%@ Register TagPrefix="sbCommon" Assembly="Web" Namespace="ScholarBridge.Web.Common" %>
<%@ Register TagPrefix="sb" TagName="LookupItemCheckboxList" Src="~/Common/LookupItemCheckboxList.ascx" %>
<%@ Register Tagprefix="sb" tagname="LookupDialog" src="~/Common/Lookup/LookupDialog.ascx"   %>    
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register TagPrefix="sb" TagName="Tip" Src="~/Common/Tip.ascx" %>  

<script type="text/javascript" language="javascript"> 
	function CheckWashingtonState(selfId, cityDropDownId, countyDropDownId, cityTextBoxId, countyTextBoxId ) {
	    if (selfId == undefined) {
	        return false;
	    }
	    
	    var atext = $("#" + selfId).find(':selected').text();
	    if (atext == "Washington") {
	        hideElementById(cityTextBoxId);
	        hideElementById(countyTextBoxId);
	        showElementById(cityDropDownId);
	        showElementById(countyDropDownId);
	    } else {
	        showElementById(cityTextBoxId);
	        showElementById(countyTextBoxId);
	        hideElementById(cityDropDownId);
	        hideElementById(countyDropDownId);
	    }
	}
</script>

<div class="two-column with_divider">
    <div class="question_list">
        <h2>My Contact Information</h2>
        <div>
            <label>First Name</label>
            <asp:TextBox ID="FirstNameBox" runat="server" MaxLength="40"/>
            <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstNameBox" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
        </div>
        <div>
            <label>Middle Name</label>
            <asp:TextBox ID="MidNameBox" runat="server" MaxLength="40"/>
            <elv:PropertyProxyValidator ID="MidNameValidator" runat="server" ControlToValidate="MidNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>
        </div>
        <div>
            <label>Last Name</label>
            <asp:TextBox ID="LastNameBox" runat="server" MaxLength="40"/>
            <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastNameBox" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"/>       
        </div>
        
        <hr />
        
        <div>
            <label>E-mail<br /></label>
            <span><asp:Literal ID="EmailBox" runat="server" /> <asp:HyperLink NavigateUrl="~/Profile/" runat="server">(update?)</asp:HyperLink></span>
        </div>
        <div>
            <label for="UseEmailBox"><span class="tip" title="Scholarship status updates and application due date reminders will be sent to the Messages tab. Click this box if you would also like these messages sent to your email box.">Email Notifications</span></label>
            <asp:CheckBox ID="UseEmailBox" runat="server" Text="" Width="50px" />
        </div>
        
        <hr />
        
        <div>
            <label><span class="tip" title="We require your address inorder to match you with Scholarships offered in your area.">Address Line 1</span></label>
            <asp:TextBox ID="AddressLine1Box" runat="server" Columns="30" MaxLength="50"/>
            <asp:requiredfieldvalidator ID="AddressLine1Required" Display="Dynamic" runat="server" ControlToValidate="AddressLine1Box" ErrorMessage="Address line 1 is required"></asp:requiredfieldvalidator>
            <elv:PropertyProxyValidator ID="AddressLine1Validator" runat="server" ControlToValidate="AddressLine1Box" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        </div>
        <div>
            <label>Address Line 2</label>
            <asp:TextBox ID="AddressLine2Box" runat="server" Columns="30" MaxLength="50"/>
            <elv:PropertyProxyValidator ID="AddressLine2Validator" runat="server" ControlToValidate="AddressLine2Box" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        </div>
        <div>
            <label>State</label>
            <asp:DropDownList ID="StateDropDown" runat="server"></asp:DropDownList>
            <asp:requiredfieldvalidator ControlToValidate="StateDropDown" Display="Dynamic" runat="server" ID="stateRequired" ErrorMessage="Address State is required"></asp:requiredfieldvalidator>
        </div>
        <div>
            <label>County</label>
            <asp:panel ID="CountyDropDownPanel" runat="server">
                <asp:DropDownList ID="CountyDropDown" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="- Select One -" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:requiredfieldvalidator ControlToValidate="CountyDropDown" Display="Dynamic" runat="server" ID="countySelectRequired" ErrorMessage="County selection is required"></asp:requiredfieldvalidator>
            </asp:panel>
            <asp:panel ID="countyTextPanel" runat="server">
                <asp:TextBox ID="CountyTextBox" runat="server" Columns="30" MaxLength="50"/>
                <asp:requiredfieldvalidator ControlToValidate="CountyTextBox" Display="Dynamic" runat="server" ID="countyRequired" ErrorMessage="Address County is required"></asp:requiredfieldvalidator>
            </asp:panel>        
        </div>
        <div>
            <label>City</label>
            <asp:Panel ID="CityDropDownPanel" runat="server">
                <asp:DropDownList ID="CityDropDown" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="- Select One -" Value=""></asp:ListItem>
                </asp:DropDownList> 
                <asp:requiredfieldvalidator ControlToValidate="CityDropDown" Display="Dynamic" runat="server" ID="citySelectRequired" ErrorMessage="City selection is required"></asp:requiredfieldvalidator>
            </asp:Panel>
            <asp:panel ID="cityTextPanel" runat="server">
                <asp:TextBox ID="CityTextBox" runat="server" Columns="30" MaxLength="50"/>
                <asp:requiredfieldvalidator ControlToValidate="CityTextBox" Display="Dynamic" runat="server" ID="cityRequired" ErrorMessage="Address City is required"></asp:requiredfieldvalidator>
            </asp:panel>
        </div>
        <div>
            <label for="ZipBox">Postal Code</label>
            <asp:TextBox ID="ZipBox" runat="server" Columns="10" MaxLength="10"/>
            <asp:requiredfieldvalidator ControlToValidate="ZipBox" Display="Dynamic" runat="server" ID="postalCodeRequired" ErrorMessage="Address Postal Code is required"></asp:requiredfieldvalidator>
            <elv:PropertyProxyValidator ID="ZipValidator" runat="server" ControlToValidate="ZipBox" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.AddressBase"/>
        </div>
        
        <hr />
        
        <div>
            <label for="PhoneBox">Home Phone</label>
            <asp:TextBox ID="PhoneBox" runat="server" Columns="12" CssClass="phone"/>
            <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="PhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
        </div>
        <div>
            <label for="MobilePhoneBox">Mobile Phone</label>
            <asp:TextBox ID="MobilePhoneBox" runat="server" Columns="12" CssClass="phone"/>
            <elv:PropertyProxyValidator ID="MobilePhoneValidator" runat="server" ControlToValidate="MobilePhoneBox" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber"/>
        </div>
    </div>
    <div class="question_list">
        <h2>My Background Information</h2>
        <div>
            <label>Birthdate</label>
            <asp:TextBox ID="DobControl" runat="server" CssClass="date"/>
            <elv:PropertyProxyValidator ID="DobValidator" runat="server" ControlToValidate="DobControl" PropertyName="DateOfBirth" SourceTypeName="ScholarBridge.Domain.Seeker"/>
            <asp:RangeValidator ID="dobRangeValid" runat="server" ControlToValidate="DobControl" MinimumValue="1/1/1900" Type="Date" Display="Dynamic" ErrorMessage="How old are you?" />
        </div>
        <div>
            <label><span class="tip" title="Some Scholarships include gender in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for.">Gender</span></label>
            <asp:RadioButtonList ID="GenderButtonList" runat="server">
                <asp:ListItem Text="Male" Value="2" />
                <asp:ListItem Text="Female" Value="4" />
                <asp:ListItem Text="I prefer not to answer" Value="10" />
            </asp:RadioButtonList>
        </div>
        <div>
            <label><span class="tip" title="Some Scholarships include religion in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for.">
                        Religion / Faith
                   </span>
            </label>
            <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="ReligionCheckboxList" runat="server" LookupServiceSpringContainerKey="ReligionDAL" CheckBoxListWidth="210px" />
            <label></label>
            <div>
                <label>Other: </label>
                <asp:TextBox ID="OtherReligion" runat="server"/>  
                <elv:PropertyProxyValidator ID="OtherReligionValidator" runat="server" ControlToValidate="OtherReligion" PropertyName="ReligionOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>
            </div>
        </div>
        <div>
            <label><span class="tip" title="Some Scholarships include ethnic heritage in their eligibility requirements. By providing this information, you increase your chances of being matched to a scholarship you qualify for.">Ethnicity / Heritage</span></label>
            <sb:LookupItemCheckboxList AllBoxesUnchecked="true" AllNoneButtonsInvisible="true" ID="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" CheckBoxListWidth="210px"/>
            <label></label>
            <div>
                <label>Other: </label>
                <asp:TextBox ID="OtherEthnicity" runat="server"/>
                <elv:PropertyProxyValidator ID="OtherEthnicityValidator" runat="server" ControlToValidate="OtherEthnicity" PropertyName="EthnicityOther" SourceTypeName="ScholarBridge.Domain.Seeker"/>
            </div>
        </div>
        <div>
            <label><span class="tip" title="Is English your Second Language or are you an English Language Learner?">ESL/ELL</span></label>
            <asp:RadioButtonList id="ESLELLSelection" runat="server">
                <asp:ListItem Text="Yes" Value="True" />
                <asp:ListItem Text="No" Value="False" />
            </asp:RadioButtonList>
        </div>
    </div>
</div>