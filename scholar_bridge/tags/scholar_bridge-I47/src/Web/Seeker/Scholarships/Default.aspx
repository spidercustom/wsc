﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" EnableEventValidation="false"
     Inherits="ScholarBridge.Web.Seeker.Scholarships.Default"  Title="Seeker | My Scholarships" %>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopMyApplications.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomMyApplications.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<!--Left floated content area starts here-->
                <div id="HomeContentLeft">

                  <Img src="<%= ResolveUrl("~/images/PgTitle_MyApplications.gif") %>" width="211px" height="54px">
                  <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerLoggedIn.gif") %>" width="513px" height="96px">
                  </div>
                  <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <div id="HomeContentRight">
                 <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                 </div>
                <BR><BR>

                 <div id="Clear"></div>
                 
<h3>List of My Applications</h3>
<asp:ObjectDataSource ID="ApplicationDataSource" runat="server"
    SelectMethod="GetApplications" TypeName="ScholarBridge.Web.Seeker.Scholarships.Default"    
      SortParameterName="sortExpression" SelectCountMethod="CountApplications"  EnablePaging="true" >
</asp:ObjectDataSource>

<sbCommon:SortableListView ID="applicationList" runat="server" 
    onitemdatabound="applicationList_ItemDataBound" 
    DataKeyNames="Id" DataSourceID="ApplicationDataSource" SortExpressionDefault="DateSubmitted"
			SortDirectionDefault="Descending">
    <LayoutTemplate>
        <table class="sortableListView">
                <thead>
                <tr>
                    <th>
                        <sbCommon:SortableListViewColumnHeader Key="ScholarshipName" Text="Scholarship Name"  ID="SortableListViewColumnHeader1" runat="server"  />
                   </th>
                    <th>
                        <sbCommon:SortableListViewColumnHeader Key="ApplicationDueDate" Text="Application Due Date"  ID="SortableListViewColumnHeader2" runat="server"  />
                    </th>
                    <th> 
                        <sbCommon:SortableListViewColumnHeader Key="NumberOfAwards" Text="# of Awards"  ID="SortableListViewColumnHeader3" runat="server"  />
                    </th>
                    <th>
                       <sbCommon:SortableListViewColumnHeader Key="ScholarshipAmount" Text="Amount $"  ID="SortableListViewColumnHeader4" runat="server"  />
                    </th>
                    <th>
                        <sbCommon:SortableListViewColumnHeader Key="Status" Text="Status"  ID="SortableListViewColumnHeader5" runat="server"  />
                    </th>
                    <th></th>
                    <th>
                        <sbCommon:SortableListViewColumnHeader Key="DateSubmitted" Text="Date Submitted"  ID="SortableListViewColumnHeader6" runat="server"  />
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
            
        </table>
            <div class="pager">
                <asp:DataPager runat="server" ID="pager"   PageSize="20"  OnPreRender="pager_PreRender"  >
                    <Fields>
                        <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                            ShowNextPageButton="false" ShowLastPageButton="false" />
                        <sbCommon:CustomNumericPagerField
                            CurrentPageLabelCssClass="pagerlabel"
                            NextPreviousButtonCssClass="pagerlink"
                            PagingPageLabelCssClass="pagingPageLabel"
                            NumericButtonCssClass="pagerlink"/>
                        <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                            ShowNextPageButton="true" ShowLastPageButton="false" />
                    </Fields>
                </asp:DataPager>
            </div> 
    </LayoutTemplate>
          
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton id="linkToScholarship" runat="server" ><%# Eval("Scholarship.Name")%></asp:LinkButton></td>
            <td><%# ((Application)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((Application)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0}", ((Application)Container.DataItem).Scholarship.AmountRange)%></td>
            <td><%# Eval("ApplicationStatus") %></td>
            <td><asp:Button ID="defaultActionButton" runat="server"  CssClass="ListButton" Width="60px"  /></td>
            <td><asp:Label ID="submittedDateLabel" runat="server" /></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton id="linkToScholarship" runat="server" ><%# Eval("Scholarship.Name")%></asp:LinkButton></td>
            <td><%# ((Application)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((Application)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0}", ((Application)Container.DataItem).Scholarship.AmountRange)%></td>
            <td><%# Eval("ApplicationStatus")%></td>
            <td><asp:Button ID="defaultActionButton" runat="server"  CssClass="ListButton" Width="60px"/></td>
            <td><asp:Label ID="submittedDateLabel" runat="server" /></td>
        </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
        <p>You have no scholarship applications at this time.</p>
    </EmptyDataTemplate>
</sbCommon:SortableListView>



</asp:Content>
