﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Matches.Default"  Title="Seeker | My Matches" %>

<%@ Register src="~/Common/MatchList.ascx" tagname="MatchList" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="My Matches" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="167" width="873" src="<%= ResolveUrl("~/images/header/my_matches.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <div id="ProfileCompleteness">
        <sb:SeekerProfileProgress ID="ProfileProgess" runat="server" ShowGotoProfile="false"/>
    </div>
    <h1><img src="<%= ResolveUrl("~/images/PgTitle_MyMatches.gif") %>" width="399px" height="54px"></h1>
    <p class="hookline">
        The following matches are based on your profile and the scholarship requirements. A more complete profile will result in better scholarship matches and may show more scholarships that you qualify for.
    </p>
    
    <div class="clear"></div>
    <sb:MatchList id="qualifyList" runat="server"  OnMatchAction="list_OnMatchAction" />
</asp:Content>
