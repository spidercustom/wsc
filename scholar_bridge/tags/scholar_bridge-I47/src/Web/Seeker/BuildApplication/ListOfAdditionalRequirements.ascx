﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListOfAdditionalRequirements.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.ListOfAdditionalRequirements" %>

<asp:ListView ID="lstRequirements" runat="server">
    <LayoutTemplate>
        <ul><asp:PlaceHolder id="itemPlaceholder" runat="server" /></ul>
    </LayoutTemplate>
          
    <ItemTemplate>
        <li><%# DataBinder.Eval(Container.DataItem, "Name")%></li>
    </ItemTemplate>
   
    <EmptyDataTemplate>
        <p>There are no additional requirements.</p>
    </EmptyDataTemplate>
</asp:ListView>
