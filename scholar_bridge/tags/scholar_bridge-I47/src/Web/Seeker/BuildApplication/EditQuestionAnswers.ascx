﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditQuestionAnswers.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.EditQuestionAnswers" %>

<asp:ListView ID="lstQA" runat="server" onitemdatabound="lstQA_ItemDataBound"  >
    <LayoutTemplate>
    <div class="question_list">
        <asp:PlaceHolder id="itemPlaceholder" runat="server" />
    </div>
    </LayoutTemplate>
          
    <ItemTemplate>
        <div>
            <label class="full">
                <%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Question"), "QuestionText")%>
                <span class="requirement_indicator" runat="server" id="requiredIndicator">(REQUIRED) </span> 
            </label>
            <asp:TextBox ID="txtAnswer" runat="server" TextMode="MultiLine" Rows="4"/>
        </div>
    </ItemTemplate>
   
    <EmptyDataTemplate>
    <p>There are no additional questions.</p>
    </EmptyDataTemplate>
</asp:ListView>


