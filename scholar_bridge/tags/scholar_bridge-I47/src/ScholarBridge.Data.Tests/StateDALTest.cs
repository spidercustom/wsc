using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class StateDALTest : TestBase
    {
        public StateDAL StateDAL { get; set; }
        
        [Test]
        public void CanGetByAbbreviation()
        {
            var s = StateDAL.FindByAbbreviation("WI");
            Assert.IsNotNull(s);
            Assert.AreEqual("WI", s.Abbreviation);
            Assert.AreEqual("Wisconsin", s.Name);
        }

        [Test]
        public void CanGetByAll()
        {
            var states = StateDAL.FindAll();
            Assert.IsNotNull(states);
            CollectionAssert.IsNotEmpty(states);
            CollectionAssert.AllItemsAreNotNull(states);
            foreach (var s in states)
            {
                Assert.IsNotNull(s.Abbreviation);
                Assert.IsNotNull(s.Name);
            }
        }

        [Test]
        public void can_behave_as_lookup()
        {
            var s = (ILookup) StateDAL.FindByAbbreviation("WI");
            Assert.AreEqual("WI", s.Id);
            Assert.AreEqual("WI", s.GetIdAsString());
            Assert.AreEqual("Wisconsin", s.Name);
            Assert.AreEqual("Wisconsin", s.Description);
        }
    }
}