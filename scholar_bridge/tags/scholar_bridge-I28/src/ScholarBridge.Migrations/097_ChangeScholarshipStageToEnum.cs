﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(97)]
    public class ChangeScholarshipStageToEnum : Migration
    {
        public const string TABLE_NAME = "SBScholarshipStageLUT";
        public const string PRIMARY_KEY_COLUMN = "SBScholarshipStageIndex";

        private readonly string[] columns = new[] { PRIMARY_KEY_COLUMN, "SBScholarshipStage" };

        public override void Up()
        {
            Database.RemoveForeignKey("SBScholarship", "FK_SB_Scholarship_ScholarshipStage");
            Database.RemoveTable(TABLE_NAME);

            Database.RemoveColumn("SBScholarship", "SBStageIndex");
            Database.AddColumn("SBScholarship", new Column("Stage", DbType.String, 50, ColumnProperty.NotNull, "('None')"));
        }

        public override void Down()
        {
            Database.AddTable(TABLE_NAME, 
                new Column(columns[0], DbType.Int32, ColumnProperty.PrimaryKey),
                new Column(columns[1], DbType.String, 50, ColumnProperty.NotNull)
            );

            Database.RemoveColumn("SBScholarship", "Stage");
            Database.AddColumn("SBScholarship", new Column("SBStageIndex", DbType.Int32, ColumnProperty.Null));
            Database.AddForeignKey("FK_SB_Scholarship_ScholarshipStage", "SBScholarship", "SBStageIndex", TABLE_NAME, PRIMARY_KEY_COLUMN);

            Database.Insert(TABLE_NAME, columns, new[] { "0", "General Information" });
            Database.Insert(TABLE_NAME, columns, new[] { "1", "Seeker Profile" });
            Database.Insert(TABLE_NAME, columns, new[] { "2", "Funding Profile" });
            Database.Insert(TABLE_NAME, columns, new[] { "3", "Preview Candidate Population" });
            Database.Insert(TABLE_NAME, columns, new[] { "4", "Build Scholarship Match Logic" });
            Database.Insert(TABLE_NAME, columns, new[] { "5", "Build Scholarship Renewal Criteria" });
            Database.Insert(TABLE_NAME, columns, new[] { "6", "Requested Activation" });
            Database.Insert(TABLE_NAME, columns, new[] { "7", "Activated" });
            Database.Insert(TABLE_NAME, columns, new[] { "8", "Rejected" });
            Database.Insert(TABLE_NAME, columns, new[] { "9", "Awarded Scholarship" });
        }
    }
}
