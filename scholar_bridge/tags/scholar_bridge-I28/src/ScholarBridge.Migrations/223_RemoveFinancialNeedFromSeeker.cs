﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(223)]
    public class RemoveFinancialNeedFromSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public readonly string[]  COLUMNS
            = { "Fafsa", "UserDerived", "MinimumSeekerNeed", "MaximumSeekerNeed" };

        public override void Up()
        {
            foreach (var col in COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }

           

             
        }

        public override void Down()
        {

            Database.AddColumn(TABLE_NAME, COLUMNS[0], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, COLUMNS[1], DbType.Boolean, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, COLUMNS[2], DbType.Double, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, COLUMNS[3], DbType.Double, ColumnProperty.Null);
            
           
        }
    }
}