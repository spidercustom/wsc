﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(239)]
    public class ChangeScholarshipFinancialNeedFields : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        public override void Up()
        {
            Database.RemoveColumn(TABLE_NAME, "IsFamilyMembersInHouseHoldRequired");
            Database.RemoveColumn(TABLE_NAME, "IsFamilyDependentsAttendingCollegeRequired");
            Database.RemoveColumn(TABLE_NAME, "IsFamilyIncomeRequired");
            Database.AddColumn(TABLE_NAME, "IsApplicantDemonstrateFinancialNeedRequired", DbType.Boolean);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "IsApplicantDemonstrateFinancialNeedRequired");

            Database.AddColumn(TABLE_NAME, "IsFamilyMembersInHouseHoldRequired", DbType.Boolean);
            Database.AddColumn(TABLE_NAME, "IsFamilyDependentsAttendingCollegeRequired", DbType.Boolean);
            Database.AddColumn(TABLE_NAME, "IsFamilyIncomeRequired", DbType.Boolean);
        }
    }
}