﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(149)]
    public class AddCurrentSchoolToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";
        private const string FK_COLLEGE = "FK_SBSeeker_CurrentCollege";
        private const string FK_HIGHSCHOOL = "FK_SBSeeker_CurrentHighSchool";

        public readonly string[] NEW_COLUMNS
            = { "LastAttended", "YearsAttended", "CurrentCollegeIndex", "CurrentHighSchoolIndex" };

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.String, 50, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[2], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[3], DbType.Int32, ColumnProperty.Null);

            Database.AddForeignKey(FK_COLLEGE, TABLE_NAME, "CurrentCollegeIndex", "SBCollegeLUT", "SBCollegeIndex");
            Database.AddForeignKey(FK_HIGHSCHOOL, TABLE_NAME, "CurrentHighSchoolIndex", "SBHighSchoolLUT", "SBHighSchoolIndex");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_COLLEGE);
            Database.RemoveForeignKey(TABLE_NAME, FK_HIGHSCHOOL);
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}