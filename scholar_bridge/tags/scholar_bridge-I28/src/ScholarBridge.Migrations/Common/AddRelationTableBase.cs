﻿using System.Data;
using System.Text;
using Migrator.Framework;

namespace ScholarBridge.Migrations.Common
{
    public abstract class AddRelationTableBase : AddTableBase
    {
        protected abstract string FirstTableName
        { get; }

        protected abstract string SecondTableName
        { get; }        

        public override string TableName
        {
            get
            {
                var result =  new StringBuilder();
                result.Append(TABLE_PREFIX);
                result.Append(GetPlainTableName(FirstTableName));
                result.Append(GetPlainTableName(SecondTableName));
                result.Append(KnownSuffixes[SufixType.Relation]);
                return result.ToString();
            }
        }

        public override string PrimaryKeyColumn
        {
            get { throw new System.NotImplementedException(); }
        }

        public override Column[] CreateColumns()
        {
            var firstTablePrimaryKey = PrepareTablePrimaryKeyColumnName(FirstTableName);
            var secondTablePrimaryKey = PrepareTablePrimaryKeyColumnName(SecondTableName);
            return new[]
                       {
                           new Column(firstTablePrimaryKey, DbType.Int32, ColumnProperty.PrimaryKey), 
                           new Column(secondTablePrimaryKey, DbType.Int32, ColumnProperty.PrimaryKey), 
                       };
        }

        private const string FOREIGN_KEY_CONSTRAINT_NAME_TEMPLATE = "FK_{0}_{1}";
        public override ForeignKey[] CreateForeignKeys()
        {
            var firstTableForeignKey = new ForeignKey
            {
               Name =
                   string.Format(FOREIGN_KEY_CONSTRAINT_NAME_TEMPLATE, TableName,
                                 FirstTableName),
               PrimaryTable = FirstTableName,
               PrimaryColumn = Columns[0].Name,
               ForeignTable = TableName,
               ForeignColumn = Columns[0].Name
            };

            var secondTableForeignKey = new ForeignKey
            {
                Name =
                    string.Format(FOREIGN_KEY_CONSTRAINT_NAME_TEMPLATE, TableName,
                                  SecondTableName),
                PrimaryTable = SecondTableName,
                PrimaryColumn = Columns[1].Name,
                ForeignTable = TableName,
                ForeignColumn = Columns[1].Name
            };

            return new[] { firstTableForeignKey, secondTableForeignKey };
        }
    }
}
