﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(83)]
    public class ChangeScholarshipAddSeekerPerformance : Migration
    {
        private readonly Column[] newColumns =
            new[]
        {
             new Column("MatchCriteriaGPAGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaGPALessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaClassRankGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaClassRankLessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaSATWritingGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaSATWritingLessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaSATCriticalReadingGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaSATCriticalReadingLessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaSATMathematicsGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaSATMathematicsLessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreEnglishGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreEnglishLessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreMathematicsGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreMathematicsLessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreReadingGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreReadingLessThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreScienceGreaterThan", DbType.Int32, ColumnProperty.Null), 
             new Column("MatchCriteriaACTScoreScienceLessThan", DbType.Int32, ColumnProperty.Null), 
        };
        public override void Up()
        {
            Array.ForEach(newColumns,
                column => Database.AddColumn("SBScholarship", column));
        }

        public override void Down()
        {
            Array.ForEach(newColumns,
                column => Database.RemoveColumn("SBScholarship", column.Name));
        }

    }
}
