using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(158)]
    public class AddLastMatchColumnToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";
        private const string COLUMN = "LastMatch";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, COLUMN, DbType.DateTime, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMN);
        }
    }
}