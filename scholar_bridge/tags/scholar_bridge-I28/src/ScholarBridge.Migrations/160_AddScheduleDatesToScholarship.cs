using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(160)]
    public class AddScheduleDatesToScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";
        
        private const string APPLICATION_STARTDATE = "ApplicationStartDate";
        private const string APPLICATION_DUEDATE = "ApplicationDueDate";
        private const string AWARD_DATE = "AwardDate";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, APPLICATION_STARTDATE, DbType.DateTime, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, APPLICATION_DUEDATE, DbType.DateTime, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, AWARD_DATE, DbType.DateTime, ColumnProperty.Null);
            Database.RemoveTable("SBScholarshipSchedule");
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, APPLICATION_STARTDATE);
            Database.RemoveColumn(TABLE_NAME, APPLICATION_DUEDATE);
            Database.RemoveColumn(TABLE_NAME, AWARD_DATE);

        }
    }
}