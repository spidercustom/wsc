using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(198)]
    public class AddApplicationFinalist : Migration
    {
        public const string TABLE_NAME = "SBApplication";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("Finalist", DbType.Boolean, ColumnProperty.NotNull, "'false'"));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "Finalist");
        }
    }
}