﻿
using NHibernate.Type;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class RelationshipStatusType : EnumStringType
    {
        public RelationshipStatusType()
            : base(typeof(RelationshipStatus), 30)
        {
        }
    }
}