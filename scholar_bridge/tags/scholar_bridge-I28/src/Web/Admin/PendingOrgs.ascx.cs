﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Admin
{
    public partial class PendingOrgs : System.Web.UI.UserControl
    {

		public IUserService UserService { get; set; }
		public IList<Organization> Organizations { get; set; }
		public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
			if (UserService == null)
				UserService = new UserService();
			pendingOrgsListView.DataSource = Organizations;
			pendingOrgsListView.DataBind();
            pager.VisibleIf(() => pager.PageSize < pager.TotalRowCount);
            pendingOrgsListView.PagePropertiesChanged += (s1, e1) => pendingOrgsListView.DataBind();
        }

		protected void pendingOrgsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var org = ((Organization)((ListViewDataItem)e.Item).DataItem);
				var btnResend = (ConfirmButton)e.Item.FindControl("resendCnfBtn");

				btnResend.Attributes.Add("value", org.AdminUser.Email);
			}
		}

		protected void resendCnfBtn_Click(object sender, EventArgs e)
		{
			var btn = (ConfirmButton)sender;
			var adminEmail = btn.Attributes["value"];
			User adminUser = UserService.FindByEmail(adminEmail);
			UserService.SendConfirmationEmail(adminUser, false);
			SuccessMessageLabel.SetMessage("Email confirmation message successfully resent to " + adminEmail);
			Response.Redirect(ResolveUrl(Request.Url.PathAndQuery));

		}

    }
}