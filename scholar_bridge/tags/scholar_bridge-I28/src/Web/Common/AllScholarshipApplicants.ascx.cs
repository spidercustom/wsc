﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class AllScholarshipApplicants : UserControl
    {
        public IList<Application> Applications { get; set; }
		public string EmptyMessage
		{
			get
			{
				return !Page.IsPostBack ? "No applicants for this scholarship at this time" : "No applicants were selected by the search criteria.";
			}
		}


        protected void Page_Load(object sender, EventArgs e)
        {
			lstApplicants.DataSource = Applications.OrderBy(a => a.ApplicantName.LastName);
            lstApplicants.DataBind();
        }
    }
}