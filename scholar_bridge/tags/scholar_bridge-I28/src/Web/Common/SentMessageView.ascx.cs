﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Web.Common
{
    public partial class SentMessageView : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }

        public string ListUrl { get; set; }

        public int MessageId { get { return Int32.Parse(Request["id"]); } }

        public SentMessage CurrentMessage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            CurrentMessage = MessageService.FindSentMessage(UserContext.CurrentUser, UserContext.CurrentOrganization,
                                                        MessageId);
            if (null != CurrentMessage)
            {
                if (! Page.IsPostBack)
                {
                    subjectLbl.Text = CurrentMessage.Subject;
                    fromLbl.Text = CurrentMessage.From.EmailAddress().BuildLinks();
                    toLbl.Text = CurrentMessage.To.EmailAddress().BuildLinks();
                    dateLbl.Text = CurrentMessage.Date.ToLocalTime().ToShortDateString();
                    contentLbl.Text = CurrentMessage.Content.BuildLinks();
                }
            }
        }
    }
}