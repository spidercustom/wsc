﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Validation;

namespace ScholarBridge.Web.Common
{
    public partial class SeekerProfileActivationValidation : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ActivationValidationErrors.Visible = false;
        }

        public bool ValidateActivationAndPopulateErrors(Domain.Seeker seekerProfile)
        {
            ActivationValidationErrors.Visible = true;
            if (!Page.IsValid && Page is Seeker.Profile.Default)
                return false;

            var results = seekerProfile.ValidateActivation();
            if (!results.IsValid)
            {
                PopulateActivationErrors(results);
                ClientSideDialogs.ShowDivAsDialog(ActivationValidationErrors.ClientID);
            }
            return results.IsValid;
        }

        public void PopulateActivationErrors(IEnumerable<ValidationResult> results)
        {
            ActivationValidationErrors.Visible = true;
            FirstNameValidationError.Visible = results.Any(o => o.Key == "FirstName");
            LastNameValidationError.Visible = results.Any(o => o.Key == "LastName");
            StreetLine1ValidationError.Visible = results.Any(o => o.Key == "Street");
            CityValidationError.Visible = results.Any(o => o.Key == "City");
            StateValidationError.Visible = results.Any(o => o.Key == "State");
            PostalCodeValidationError.Visible = results.Any(o => o.Key == "PostalCode");
            StudentTypeValidationError.Visible = results.Any(o => o.Key == "LastAttended");
            SchoolTypeValidationError.Visible = results.Any(o => o.Key == "SchoolTypes");
            AcademicProgramValidationError.Visible = results.Any(o => o.Key == "AcademicPrograms");
            SupportTypeValidationError.Visible = results.Any(o => o.Key == "TypesOfSupport");

            BasicTabPageErrorPanel.Visible =
                FirstNameValidationError.Visible |
                LastNameValidationError.Visible |
                StreetLine1ValidationError.Visible |
                CityValidationError.Visible |
                StateValidationError.Visible |
                PostalCodeValidationError.Visible;

            MyAcademicInfoTabPageErrorPanel.Visible =
                StudentTypeValidationError.Visible |
                SchoolTypeValidationError.Visible |
                AcademicProgramValidationError.Visible;

            MyFinancialNeedTabPageErrorPanel.Visible = SupportTypeValidationError.Visible;
        }
    }
}