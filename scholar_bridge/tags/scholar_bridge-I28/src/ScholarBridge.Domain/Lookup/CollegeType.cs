﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum CollegeType
    {
        // Zero won't work because zero is none and you can't tell between that
        // and another value. 0 + 1 == 1
        [DisplayName("Any")]
        Any = 1,
        [DisplayName("In Washington State")]
        InWashingTonState = 2,
        [DisplayName("Specify")]
        Specify = 4
    }
}
