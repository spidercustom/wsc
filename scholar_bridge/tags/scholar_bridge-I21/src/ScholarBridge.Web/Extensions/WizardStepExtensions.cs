﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Extensions
{
    public static class WizardStepExtensions
    {
        public static IWizardStepControl<T> FindFirstWizardStepControl<T>(this ControlCollection controls)
        {
            return FindFirstWizardStepControl<T>(controls, false, -1);
        }

        public static IWizardStepControl<T> FindFirstWizardStepControl<T>(this ControlCollection controls, int placeHolderStepIndex)
        {
            return FindFirstWizardStepControl<T>(controls, true, placeHolderStepIndex);
        }

        private static IWizardStepControl<T> FindFirstWizardStepControl<T>(IEnumerable controls, bool returnPlaceHolderIfNotFound, int placeHolderStepIndex)
        {
            var wizardStepControls =
                from Control control in controls
                where
                    control is IWizardStepControl<T>
                select control;

            if (wizardStepControls.Count() == 0)
            {
                return returnPlaceHolderIfNotFound ? new PlaceHolderStep<T>() : null;
            }
            return wizardStepControls.First() as IWizardStepControl<T>;
        }

        public static IWizardStepControl<T>[] FindWizardStepControls<T>(this ViewCollection steps)
        {
            var result = new List<IWizardStepControl<T>>();
            for (var index = 0; index < steps.Count; index++)
            {
                var step = steps[index];
                var wizardStepControl = FindFirstWizardStepControl<T>(step.Controls, true, index);
                result.Add(wizardStepControl);
            }
            return result.ToArray();
        }
    }
}
