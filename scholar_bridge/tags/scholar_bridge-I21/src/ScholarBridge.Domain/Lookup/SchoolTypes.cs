﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SchoolTypes
    {
        [DisplayName("Community College")]
        CommunityCollege = 1,
        [DisplayName("Technical/Vocational")]
        TechnicalORVocational = 2,
        [DisplayName("Public University")]
        PublicUniversity = 4,
        [DisplayName("Private not-for-profit / Independent College")]
        PrivateUniversity = 8
    }
}
