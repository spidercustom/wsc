﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchCriteriaSelection.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.MatchCriteriaSelection" %>
<%@ Register TagPrefix="sb" TagName="AttributeSelectionControl" Src="AttributeSelectionControl.ascx" %>
<p><br /></p>

<h3>Build Scholarship – Selection Criteria</h3>

<h4>Applicant Student Type</h4>
<sb:AttributeSelectionControl id="SeekerStudentTypeControl" runat="server" />
<br />

<h4>Applicant Demographics - Personal</h4>
<sb:AttributeSelectionControl id="SeekerDemographicsPersonalControl" runat="server" />
<br />

<h4>Applicant Demographics - Geographic</h4>
<sb:AttributeSelectionControl id="SeekerDemographicsGeographicControl" runat="server" />
<br />

<h4>Applicant Interests</h4>
<sb:AttributeSelectionControl id="SeekerInterestsControl" runat="server" />
<br />

<h4>Applicant Activities</h4>
<sb:AttributeSelectionControl id="SeekerActivitiesControl" runat="server" />
<br />

<h4>Applicant Student Performance</h4>
<sb:AttributeSelectionControl id="SeekerStudentPerformanceControl" runat="server" />
<br />
