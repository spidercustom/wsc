﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;
using ScholarBridge.Domain.Extensions;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class SeekerProfile : WizardStepUserControlBase<Scholarship>
    {
        private const string GPA_VALUE_FORMAT = "0.000";
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context"); 

            if (!IsPostBack)
            {
                EthnicityControl.DataBind();
                ReligionControl.DataBind();
                WorkHoursControl.DataBind();
                ServiceHoursControl.DataBind();

                PopulateScreen();
            }
        }

        public override void Activated()
        {
            base.Activated();
            SetControlsVisibility();
            SetupAttributeUsageTypeIcons();
            PopulateScreen();
        }

        #region Visibilty of the controls
        private void SetupAttributeUsageTypeIcons()
        {
            SeekerProfileCriteria criteria = ScholarshipInContext.SeekerProfileCriteria;

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(StudentGroupUsageTypeIconControl,
                           criteria.Attributes.GetUsageType(SeekerProfileAttribute.StudentGroup));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SchoolTypeUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SchoolType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicProgramUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.AcademicProgram));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerStatusUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SeekerStatus));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ProgramLengthUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ProgramLength));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CollegesUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.College));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FirstGenerUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.FirstGeneration));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(EthnicityUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Ethnicity));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ReligionUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Religion));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GendersUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Gender));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicAreasUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.AcademicArea));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CareersUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Career));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CommunityServicesUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.CommunityService));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(OrganizationsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.OrganizationAndAffiliationType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AffiliationTypesUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.OrganizationAndAffiliationType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerHobbiesUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SeekerHobby));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SportsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Sport));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClubsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Club));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkTypeUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.WorkType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkHoursUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.WorkHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceTypeUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ServiceType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceHoursUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ServiceHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GPAUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.GPA));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClassRankUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ClassRank));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SATScoreUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SAT));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ACTScoreUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.ACT));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(HonorsUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.Honor));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(APCUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.APCreditsEarned));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(IBCUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.IBCreditsEarned));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerDemographicsGeographicUsageTypeIconControl,
                            criteria.Attributes.GetUsageType(SeekerProfileAttribute.SeekerGeographicsLocation));
            
        }

        private void SetControlsVisibility()
        {
            SeekerInterestsControlContainer.Visible = true;
            SeekerTypeContainerControl.Visible = true;
            SeekerDemographicsPersonalContainerControl.Visible = true;
            SeekerDemographicsGeographicContainerControl.Visible = true;
            SeekerInterestsControlContainer.Visible = true;
            SeekerActivitiesControlContainer.Visible = true;
            SeekerPerformanceControlContainer.Visible = true;

            SeekerProfileCriteria criteria = ScholarshipInContext.SeekerProfileCriteria;
            
            StudentGroupContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.StudentGroup);
            SchoolTypeContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.SchoolType);
            AcademicProgramContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.AcademicProgram);
            SeekerStatusContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.SeekerStatus);
            ProgramLengthContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.ProgramLength);
            CollegesContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.College);
            FirstGenerationContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.FirstGeneration);
            EthnicityContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.Ethnicity);
            ReligionContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.Religion);
            GendersContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.Gender);
            LocationSelectorContainer.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.SeekerGeographicsLocation); 
            AcademicAreasContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.AcademicArea);
            CareersContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.Career);
            CommunityServicesContainerControl.Visible =
                criteria.Attributes.HasAttribute(SeekerProfileAttribute.CommunityService);
            OrganizationsContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.OrganizationAndAffiliationType);
            AffiliationTypesContainerControl.Visible =
                criteria.Attributes.HasAttribute(SeekerProfileAttribute.OrganizationAndAffiliationType);
            HobbiesContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.SeekerHobby);
            SportsContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.Sport);
            ClubsContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.Club);
            WorkTypeContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.WorkType);
            WorkHoursContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.WorkHour);
            ServiceTypeContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.ServiceType);
            ServiceHoursContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.ServiceHour);
            GPAControlContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.GPA);
            ClassRankContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.ClassRank);
            SATContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.SAT);
            ACTContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.ACT);
            HonorsContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.Honor);
            APCreditContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.APCreditsEarned);
            IBCreditContainerControl.Visible = criteria.Attributes.HasAttribute(SeekerProfileAttribute.IBCreditsEarned);
            
            SetDependentVisibility();
        }

        private void SetDependentVisibility()
        {
            SeekerTypeContainerControl.Visible =
                StudentGroupControl.Visible |
                SchoolTypeControl.Visible |
                AcademicProgramControl.Visible |
                SeekerStatusControl.Visible |
                ProgramLengthControl.Visible |
                CollegesContainerControl.Visible;
            

            SeekerDemographicsPersonalContainerControl.Visible =
                FirstGenerationContainerControl.Visible |
                EthnicityContainerControl.Visible |
                ReligionContainerControl.Visible |
                GendersContainerControl.Visible;

            SeekerDemographicsGeographicContainerControl.Visible = LocationSelectorContainer.Visible;
            
            SeekerInterestsControlContainer.Visible =
                AcademicAreasContainerControl.Visible |
                CareersContainerControl.Visible |
                CommunityServicesContainerControl.Visible |
                OrganizationsContainerControl.Visible |
                AffiliationTypesContainerControl.Visible;

            SeekerActivitiesControlContainer.Visible =
                HobbiesContainerControl.Visible |
                SportsContainerControl.Visible |
                ClubsContainerControl.Visible |
                WorkTypeContainerControl.Visible |
                WorkHoursContainerControl.Visible |
                ServiceTypeContainerControl.Visible |
                ServiceHoursContainerControl.Visible;

            SeekerPerformanceControlContainer.Visible =
                GPAControlContainerControl.Visible |
                ClassRankContainerControl.Visible |
                SATContainerControl.Visible |
                ACTContainerControl.Visible |
                HonorsControl.Visible |
                APCreditControl.Visible;


            NoSelectionContainerControl.Visible = !
                (SeekerTypeContainerControl.Visible |
                SeekerDemographicsPersonalContainerControl.Visible |
                SeekerDemographicsGeographicContainerControl.Visible |
                SeekerInterestsControlContainer.Visible |
                SeekerActivitiesControlContainer.Visible |
                SeekerPerformanceControlContainer.Visible);
        }
        #endregion

        private void PopulateScreen()
        {
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;

            StudentGroupControl.SelectedValues = (int)seekerProfileCriteria.StudentGroups;
            SchoolTypeControl.SelectedValues = (int)seekerProfileCriteria.SchoolTypes;
            AcademicProgramControl.SelectedValues= (int)seekerProfileCriteria.AcademicPrograms;
            SeekerStatusControl.SelectedValues = (int)seekerProfileCriteria.SeekerStatuses;
            ProgramLengthControl.SelectedValues =(int)seekerProfileCriteria.ProgramLengths;
            CollegesControlDialogButton.Keys = seekerProfileCriteria.Colleges.CommaSeparatedIds();
            FirstGenerationControl.Checked = seekerProfileCriteria.FirstGeneration;
            EthnicityControl.SelectedValues = seekerProfileCriteria.Ethnicities.Cast<ILookup>().ToList();
            ReligionControl.SelectedValues = seekerProfileCriteria.Religions.Cast<ILookup>().ToList();
            GendersControl.SelectedValues = (int)seekerProfileCriteria.Genders;
            LocationSelectorControl.PopulateScreen(seekerProfileCriteria);
            AcademicAreasControlDialogButton.Keys = seekerProfileCriteria.AcademicAreas.CommaSeparatedIds();
            CareersControlDialogButton.Keys = seekerProfileCriteria.Careers.CommaSeparatedIds();
            CommunityServicesControlDialogButton.Keys = seekerProfileCriteria.CommunityServices.CommaSeparatedIds();
            OrganizationsControlDialogButton.Keys = seekerProfileCriteria.Organizations.CommaSeparatedIds();
            AffiliationTypesControlDialogButton.Keys = seekerProfileCriteria.AffiliationTypes.CommaSeparatedIds();
            SeekerHobbiesControlDialogButton.Keys = seekerProfileCriteria.Hobbies.CommaSeparatedIds();
            SportsControlDialogButton.Keys = seekerProfileCriteria.Sports.CommaSeparatedIds();
            ClubsControlDialogButton.Keys = seekerProfileCriteria.Clubs.CommaSeparatedIds();
            WorkTypeControlDialogButton.Keys = seekerProfileCriteria.WorkTypes.CommaSeparatedIds();
            WorkHoursControl.SelectedValues = seekerProfileCriteria.WorkHours.Cast<ILookup>().ToList();
            ServiceTypeControlDialogButton.Keys = seekerProfileCriteria.ServiceTypes.CommaSeparatedIds();
            ServiceHoursControl.SelectedValues = seekerProfileCriteria.ServiceHours.Cast<ILookup>().ToList();

            PopulateGPAControls(seekerProfileCriteria);

            if (null != seekerProfileCriteria.ClassRank)
                ClassRankControl.PopulateFromRangeCondition(seekerProfileCriteria.ClassRank);

            if (null != seekerProfileCriteria.SATScore)
            {
                SATWritingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Writing);
                SATCriticalReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.CriticalReading);
                SATMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Mathematics);
            }

            if (null != seekerProfileCriteria.ACTScore)
            {
                ACTEnglishControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.English);
                ACTMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Mathematics);
                ACTReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Reading);
                ACTScienceControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Science);
            }
            HonorsControl.Checked = seekerProfileCriteria.Honors;
            if (seekerProfileCriteria.APCreditsEarned.HasValue)
                APCreditControl.Amount = seekerProfileCriteria.APCreditsEarned.Value;
            if (seekerProfileCriteria.IBCreditsEarned.HasValue)
                IBCreditControl.Amount = seekerProfileCriteria.IBCreditsEarned.Value;
        }

        private void PopulateGPAControls(SeekerProfileCriteria seekerProfileCriteria)
        {
            /* see comment below marked as WSC-352 */
            if (null != seekerProfileCriteria.GPA)
            {
                GPAGreaterThanControl.Amount = (decimal) seekerProfileCriteria.GPA.Minimum;
                if (null == seekerProfileCriteria.GPA.Maximum)
                    GPALessThanControl.Text = string.Empty;
                else
                    GPALessThanControl.Text = seekerProfileCriteria.GPA.Maximum.Value.ToString(GPA_VALUE_FORMAT);
            }
            else
            {
                GPAGreaterThanControl.Amount = 0m;
                GPALessThanControl.Text = string.Empty;
            }
        }

        public override void PopulateObjects()
        {
            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            
            seekerProfileCriteria.StudentGroups = (StudentGroups) StudentGroupControl.SelectedValues;
            seekerProfileCriteria.SchoolTypes = (SchoolTypes) SchoolTypeControl.SelectedValues;
            seekerProfileCriteria.AcademicPrograms =
                (AcademicPrograms) AcademicProgramControl.SelectedValues;
            seekerProfileCriteria.SeekerStatuses = (SeekerStatuses) SeekerStatusControl.SelectedValues;
            seekerProfileCriteria.ProgramLengths =
                (ProgramLengths) ProgramLengthControl.SelectedValues;
            PopulateList(CollegesContainerControl, CollegesControlDialogButton, seekerProfileCriteria.Colleges);
            seekerProfileCriteria.FirstGeneration = FirstGenerationControl.Checked;
            PopulateList(EthnicityContainerControl, EthnicityControl, seekerProfileCriteria.Ethnicities);
            PopulateList(ReligionContainerControl, ReligionControl, seekerProfileCriteria.Religions);
            seekerProfileCriteria.Genders = GendersContainerControl.Visible
                                              ? (Genders) GendersControl.SelectedValues
                                              : 0;

            LocationSelectorControl.PopulateObject(seekerProfileCriteria);

            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, seekerProfileCriteria.AcademicAreas);
            PopulateList(CareersContainerControl, CareersControlDialogButton, seekerProfileCriteria.Careers);
            PopulateList(CommunityServicesContainerControl, CommunityServicesControlDialogButton, seekerProfileCriteria.CommunityServices);
            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, seekerProfileCriteria.Organizations);
            PopulateList(AffiliationTypesContainerControl, AffiliationTypesControlDialogButton, seekerProfileCriteria.AffiliationTypes);
            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, seekerProfileCriteria.Hobbies);
            PopulateList(SportsContainerControl, SportsControlDialogButton, seekerProfileCriteria.Sports);
            PopulateList(ClubsContainerControl, ClubsControlDialogButton, seekerProfileCriteria.Clubs);
            PopulateList(WorkTypeContainerControl, WorkTypeControlDialogButton, seekerProfileCriteria.WorkTypes);
            PopulateList(WorkHoursContainerControl, WorkHoursControl, seekerProfileCriteria.WorkHours);
            PopulateList(ServiceTypeContainerControl, ServiceTypeControlDialogButton, seekerProfileCriteria.ServiceTypes);
            PopulateList(ServiceHoursContainerControl, ServiceHoursControl, seekerProfileCriteria.ServiceHours);

            PopulateGPAObjects();
            
            seekerProfileCriteria.ClassRank = ClassRankControl.CreateIntegerRangeCondition();

            if (SATContainerControl.Visible)
            {
                seekerProfileCriteria.SATScore = new SatScore();
                seekerProfileCriteria.SATScore.Writing = SATWritingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.CriticalReading = SATCriticalReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.Mathematics = SATMathematicsControl.CreateIntegerRangeCondition();
            }
            else
            {
                seekerProfileCriteria.SATScore = null;
            }

            if (ACTContainerControl.Visible)
            {
                seekerProfileCriteria.ACTScore = new ActScore();
                seekerProfileCriteria.ACTScore.English = ACTEnglishControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Mathematics = ACTMathematicsControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Reading = ACTReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Science = ACTScienceControl.CreateIntegerRangeCondition();
            }
            else
            {
                seekerProfileCriteria.ACTScore = null;
            }
            seekerProfileCriteria.Honors = HonorsControl.Checked;
            if (string.IsNullOrEmpty(APCreditControl.Text))
                seekerProfileCriteria.APCreditsEarned = null;
            else
                seekerProfileCriteria.APCreditsEarned = Convert.ToInt32(APCreditControl.Amount);

            if (string.IsNullOrEmpty(IBCreditControl.Text))
                seekerProfileCriteria.IBCreditsEarned = null;
            else
                seekerProfileCriteria.IBCreditsEarned = Convert.ToInt32(IBCreditControl.Amount);

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage <ScholarshipStages.NotActivated)
                ScholarshipInContext.Stage = ScholarshipStages.NotActivated;
        }

        private void PopulateGPAObjects()
        {
            var minimum = (double)GPAGreaterThanControl.Amount;
            var maximum = GPALessThanControl.Text.CreateNullableDouble();

            var seekerProfileCriteria = ScholarshipInContext.SeekerProfileCriteria;
            seekerProfileCriteria.GPA = new RangeCondition<double?>(minimum, maximum);
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool ValidateStep()
        {
            return true;
        }

        #endregion

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int) WizardStepName.MatchCriteria);
        }
    }
}

/* In ref: WSC-352 and .aspx of GPA 
    Per WSC-352, GPA Less than Field is expected to be null and number control that we use do not support blank (null)
    value. So, for that, GPA is not implimented as like other fields. JavaScript code for third party control
    has been modified to support this situation.
*/