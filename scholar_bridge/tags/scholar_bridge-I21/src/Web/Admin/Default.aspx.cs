﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
namespace ScholarBridge.Web.Admin
{
    public partial class Default : Page
	{
		#region Properties
		public IProviderService ProviderService {get;set;}
		public IIntermediaryService IntermediaryService { get; set; }
		public IApplicationService ApplicationService { get; set; }
		public ISeekerService SeekerService { get; set; }


		private IList<Domain.Provider> ApprovedProviders
		{
			get
			{
				if (Session["ApprovedProviders"] == null)
				{
					Session["ApprovedProviders"] = ProviderService.FindActiveOrganizations();
				}
				return (IList<Domain.Provider>)Session["ApprovedProviders"];
			}
			set
			{
				Session["ApprovedProviders"] = value;
			}

		}

		private IList<Domain.Intermediary> ApprovedIntermediaries
		{
			get
			{
				if (Session["ApprovedIntermediaries"] == null)
				{
					Session["ApprovedIntermediaries"] = IntermediaryService.FindActiveOrganizations();
				}
				return (IList<Domain.Intermediary>)Session["ApprovedIntermediaries"];
			}
			set
			{
				Session["ApprovedIntermediaries"] = value;
			}

		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
        {
			if (!Page.IsPostBack)
			{
				InitializeObjects();
				LoadStatistics();
			}
		}

		#region Private Methods
		private void LoadStatistics()
    	{
    		providerCountLabel.Text = ApprovedProviders.Count.ToString();
    		intermediaryCountLabel.Text = ApprovedIntermediaries.Count.ToString();
			registeredSeekersLabel.Text = SeekerService.CountActivatedSeekers().ToString();
			activeProfilesLabel.Text = SeekerService.CountSeekersWithActiveProfiles().ToString();
			appsStartedLabel.Text = ApplicationService.CountAllSavedButNotSubmitted().ToString();
			appsSubmittedLabel.Text = ApplicationService.CountAllSubmitted().ToString();

			providersHidden.Value = "true";
    		intermediariesHidden.Value = "true";
    	}

    	private void InitializeObjects()
		{
			ApprovedProviders = null;
			ApprovedIntermediaries = null;
		}
		protected virtual void SetResponseHeadersForExcel(GridView gridView)
		{
			Response.Clear();

			Response.AddHeader("content-disposition", "attachment; filename=FileName.xls");

			Response.Charset = "";

			gridView.AllowPaging = false;
			gridView.DataBind();

			// If you want the option to open the Excel file without saving than

			// comment out the line below

			// Response.Cache.SetCacheability(HttpCacheability.NoCache);

			Response.ContentType = "application/vnd.xls";

			System.IO.StringWriter stringWrite = new System.IO.StringWriter();

			HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

			gridView.RenderControl(htmlWrite);

			Response.Write(stringWrite.ToString());

			Response.End();
			gridView.AllowPaging = true;
		}


		#endregion

		#region datasource methods

		public IList<Domain.Provider> SelectProviders()
		{
			return ApprovedProviders;
		}
		public IList<Domain.Intermediary> SelectIntermediaries()
		{
			return ApprovedIntermediaries;
		}
		#endregion

		#region event handlers

		protected void providerDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
    		e.ObjectInstance = this;
		}

    	protected void intermediaryDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    	{
			e.ObjectInstance = this;
		}

		protected void saveProvidersToExcel_Click(object sender, EventArgs e)
		{
			SetResponseHeadersForExcel(providerGrid);
		}

    	protected void saveIntermediariesToExcel_Click(object sender, EventArgs e)
    	{
			SetResponseHeadersForExcel(intermediaryGrid);
    	}

		/// <summary>
		/// this empty override prevents asp from killing the GridView render to excel
		/// </summary>
		/// <param name="control"></param>
		public override void VerifyRenderingInServerForm(Control control)
		{
			// Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
		}

		#endregion
	}
}
