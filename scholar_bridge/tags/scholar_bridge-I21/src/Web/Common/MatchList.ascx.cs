﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using System.Collections.Generic;

namespace ScholarBridge.Web.Common
{
    
    public delegate void MatchIconHandler(Match match, ImageButton iconButton);
    public delegate void MatchActionButtonHandler(Match match, Button button);
    
    public partial class MatchList : UserControl
    {
        public string LinkTo { get; set; }
        public MatchIconHandler IconConfigurator { get; set; }
        public MatchActionButtonHandler ActionButtonConfigurator { get; set;}

        protected void Page_Load(object sender, EventArgs e)
        {
            BindMatches();
        }

        public IList<Match> Matches { get; set; }
        public void BindMatches()
        {
            matchList.DataSource = Matches;
            matchList.DataBind();
        }

        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match)((ListViewDataItem)e.Item).DataItem);

                var link = (LinkButton)e.Item.FindControl("linktoScholarship");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) {Path = ResolveUrl(LinkTo)}.ToString()
                + "?id=" + match.Scholarship.Id + "&print=true";
                link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url  ));

                var iconButton = (ImageButton) e.Item.FindControl("IconButton");
                if (null == IconConfigurator)
                {
                    iconButton.Visible = false;
                }
                else
                {
                    iconButton.CommandArgument = match.Scholarship.Id.ToString();
                    IconConfigurator(match, iconButton);
                }

                var btn = (Button)e.Item.FindControl("matchBtn");
                if (null == ActionButtonConfigurator)
                {
                    btn.Visible = false;
                }
                else
                {
                    btn.CommandArgument = match.Scholarship.Id.ToString();
                    ActionButtonConfigurator(match, btn);
                }
            }
        }

        
        public int PageSize
        {
            get { return pager.PageSize; }
            set { pager.PageSize = value; }
        }


        protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }

        protected void matchList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindMatches();
        }
    }
}