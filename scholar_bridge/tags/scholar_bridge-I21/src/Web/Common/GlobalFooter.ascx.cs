﻿using System;
using System.Web;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class GlobalFooter : UserControl
    {

        private LinkGenerator linkGenerator;
        protected LinkGenerator LinkGenerator
        {
            get
            {
                if (linkGenerator == null)
                    linkGenerator = new LinkGenerator();
                return linkGenerator;
            }
        }

       
    }
}

