﻿using System;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipListViewOptions : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }

        public event EventHandler UpdateView;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {
            if (UserContext.CurrentProvider != null)
            {
                var provider = UserContext.CurrentProvider;
                var orgs =
                    (from i in RelationshipService.GetByProvider(provider)
                     orderby i.Intermediary.Name
                     select new { i.Intermediary.Id, i.Intermediary.Name }).ToList();

                orgs.Insert(0, new { Id = 0, Name = "- ALL -" });

                cboOrganization.DataValueField = "Id";
                cboOrganization.DataTextField = "Name";
                cboOrganization.DataSource = orgs;
                cboOrganization.DataBind();
            }
            if (UserContext.CurrentIntermediary != null)
            {
                var intermediary = UserContext.CurrentIntermediary;
                var orgs =
                    (from i in RelationshipService.GetByIntermediary(intermediary)
                     orderby i.Provider.Name
                     select new { i.Provider.Id, i.Provider.Name }).ToList();

                orgs.Insert(0, new { Id = 0, Name = "- ALL -" });

                cboOrganization.DataValueField = "Id";
                cboOrganization.DataTextField = "Name";
                cboOrganization.DataSource = orgs;
                cboOrganization.DataBind();
            }
            ScholarshipStatusCheckboxes.EnumBind(
                ScholarshipStages.NotActivated,
                ScholarshipStages.Activated,
                ScholarshipStages.Awarded);
                
            ScholarshipStatusCheckboxes.Items.SelectAll();
        }

        protected void UpdateViewBtn_Click(object sender, EventArgs e)
        {
            if (UpdateView != null)
                UpdateView(this, EventArgs.Empty);
        }

        public ScholarshipStages[] GetSelectedStages()
        {
            if (ScholarshipStatusCheckboxes.Items.Count == 0)
                PopulateScreen();
            
            return ScholarshipStatusCheckboxes.Items.SelectedItems(o => (ScholarshipStages)Convert.ToInt32(o)).ToArray();
        }

        public int GetSelectedOrganizationId()
        {
            if (string.IsNullOrEmpty(cboOrganization.SelectedValue))
                return 0;
            return Convert.ToInt32(cboOrganization.SelectedValue);
        }
    }
}