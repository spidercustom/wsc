﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain.Lookup;
using Spring.Context.Support;
using System.Web.UI.WebControls;
using JSHelper = ScholarBridge.Web.Common.Lookup.LookupDialogJSHelper;

namespace ScholarBridge.Web.Common.Lookup
{
    public partial class LookupDialog : UserControl
    {
        public const string SEPARATOR_STRING = ", ";

        public string ItemSource { get; set; }
        public string Title { get; set; }
        public string BuddyControl { get; set; }
        public string OtherControl { get; set; }
        public string OtherControlLabel { get; set; }
        public string DataPageUrl { get; set; }
        public string SelectionLimit { get; set; }

        public string Keys
        {
            get { return hiddenids.Value; }
            set
            {
                hiddenids.Value = value;
                if (!String.IsNullOrEmpty(BuddyControl))
                {
                    SetBuddyControlValues();
                }
            }
        }
        
        public string UserDataClientID
        {
            get { return HiddenUserData.ClientID; }
        }

        public string UserData
        {
            get { return HiddenUserData.Value; }
            set { HiddenUserData.Value = value; }
        }

        private void SetBuddyControlValues()
        {
            var items = GetSelectedLookupItems();
            var buddy = (TextBox) FindControlRecursive(Page, BuddyControl);
            if (buddy != null)
                buddy.Text = string.Join(", ", (from i in items select i.Name).ToArray());
        }

        public string[] GetKeyStrings()
        {
            if (string.IsNullOrEmpty(hiddenids.Value))
                return new string[] {};
            return hiddenids.Value.Split(new[] {SEPARATOR_STRING}, StringSplitOptions.RemoveEmptyEntries);
        }


        public ILookup GetSelectedLookupItem()
        {
            var items = GetSelectedLookupItems();
            if (null != items && items.Count > 0)
                return items[0];
            return null;
        }

        public IList<ILookup> GetSelectedLookupItems()
        {
            var keysAsStrings = GetKeyStrings();
            if (null == keysAsStrings)
                return new List<ILookup>();
            IList<ILookup> selectedLookupItems = FindAll(keysAsStrings, ItemSource);
            return selectedLookupItems;
        }

        public void PopulateListFromSelection<T>(IList<T> list)
        {
            list.Clear();
            IEnumerable<T> selectedItems =
                GetSelectedLookupItems().Cast<T>();
            selectedItems.ForEach(o => list.Add(o));
        }

        private static IList<ILookup> FindAll(IEnumerable<object> ids, string dalSpringKey)
        {
            if (null == ids)
                return new List<ILookup>();
            var idsList = ids.ToList();
            if (idsList.Count == 0)
                return new List<ILookup>();
            var dal = ContextRegistry.GetContext().GetObject<ILookupDAL>(dalSpringKey);
            var result = dal.FindAll(idsList);

            return result;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //validate required properties
            if (string.IsNullOrEmpty(BuddyControl))
                throw new NullReferenceException("Propery Buddy Control must have a valid value");
            if (string.IsNullOrEmpty(ItemSource))
                throw new NullReferenceException("Propery ItemSource must have a valid value");

            int limit;
            if (int.TryParse(SelectionLimit, out limit) == false)
                SelectionLimit = "0";

            DataPageUrl = TemplateSourceDirectory + "/LookupSourceProcesser.aspx";

            CreateDynamicControls();
            SetBuddyControlValues();
        }

        private void CreateDynamicControls()
        {
            var div = CreateDivforDialog();
            var uc = CreateLookupListControl();

            div.Controls.Add(uc);
            Controls.Add(div);

            var dialogId = div.ClientID;
            var availableDivId = uc.FindControl("lookuplist_available").ClientID;
            var selectiondivId = uc.FindControl("lookuplist_selections").ClientID;
            var pagerdivId = uc.FindControl("pager").ClientID;
            var dataUrl = DataPageUrl + "?LookupSource=" + ItemSource;
            var buddyId = ClientID.Replace(ID, BuddyControl); //txtResults.ClientID;
            var hiddenID = hiddenids.ClientID;
            var searchButtonId = uc.FindControl("btnSearch").ClientID;
            var searchBoxId = uc.FindControl("txtSearch").ClientID;
            var ajaxprogressId = uc.FindControl("ajaxprogress").ClientID;
            var rightdivId = uc.FindControl("lookuplist_right").ClientID;
            var otherboxdivId = uc.FindControl("lookuplist_otherbox").ClientID;
            var othertextboxId = uc.FindControl("txtOther").ClientID;

            var otherControlID = !String.IsNullOrEmpty(OtherControl) ? ClientID.Replace(ID, OtherControl) : "";
            var otherLabelId = OtherControlLabel;
            if (!String.IsNullOrEmpty(OtherControlLabel))
            {
                var ctrl = uc.FindControl(OtherControlLabel);
                if (!(ctrl == null))
                    otherLabelId = ctrl.ClientID;


            }
            btnLookup.Attributes.Add("onclick",
                                     JSHelper.BuildShowDialogJS(dialogId, availableDivId, selectiondivId, pagerdivId,
                                                                dataUrl, hiddenID, searchButtonId, searchBoxId,
                                                                ajaxprogressId, SelectionLimit, UserDataClientID));

            // add documentreadyscript
            Page.ClientScript.RegisterClientScriptBlock(
                typeof (Page), string.Format("JS_{0}", ClientID),
                JSHelper.BuildCreateDialogJS(dialogId, buddyId, hiddenID,otherControlID,rightdivId,otherboxdivId,othertextboxId,otherLabelId));
        }

        private HtmlGenericControl CreateDivforDialog()
        {
            var div = new HtmlGenericControl("div") {ID = "dialog"};
            div.Attributes.Add("class", "lookupdialog");
            div.Attributes.Add("title", Title);
            return div;
        }

        private LookupList CreateLookupListControl()
        {
            var uc = (LookupList) LoadControl(TemplateSourceDirectory + "/LookupList.ascx");
            uc.ID = "lookuplist";
            uc.Attributes.Add("runat", "server");
            uc.HiddenIds = FindControl("hiddenids").ClientID;
            
            return uc;
        }

        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }
    }
}