﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Default"  Title="Seeker" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<%@ Register src="~/Common/ScholarshipSearchBox.ascx" tagname="ScholarshipSearchBox" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<!--Left floated content area starts here-->
                 <div id="HomeContentLeft">

                  <Img src="<%= ResolveUrl("~/images/PgTitle_SmarterScholarshipMatches.gif") %>" width="340px" height="54px">
                  <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerLoggedIn.gif") %>" width="513px" height="96px">
                  <!-- <P class="HighlightedTextMain">Now that you’re logged in we can begin helping you find the scholarships best suited for you. Simply complete your profile and let us do the rest, or if you have, review your scholarships in My Matches. </P> -->
                 <A href="<%= ResolveUrl("~/Seeker/Profile") %>"><img src="<%= ResolveUrl("~/images/Btn_GotoMyProfile.gif") %>" width="144px" height="32px"></A>
                 </div>
                  <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <div id="HomeContentRight">
                    <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" ShowGotoProfile="false" />
                 </div>

                 <!--Right Floated content area ends here-->
                 <BR><BR>

                 <div id="Clear"></div>
                 <HR>



                <div style='clear: both;'></div>


                 <!--The Three column starts here-->
                 <div id="BoxWrapper">
                      <div id="LeftBottomBox">
                           <A href="<%= ResolveUrl("~/Seeker/Profile") %>"><Img src="<%= ResolveUrl("~/images/BottomBox01_DontForget.gif") %>" width="262px" height="51px"></A>
                           <A href="<%= ResolveUrl("~/Seeker/Profile") %>"><Img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox01.gif") %>" width="237px" height="86px"></A>

                           <P>Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                           <p class="IcoBoxArrow"><A href="<%= ResolveUrl("~/Seeker/Profile") %>"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px">&nbsp;Go to My Profile</A></P>

                      </div>
                      <div id="CenterBottomBox">
                            <A href="<%= ResolveUrl("~/Seeker/Matches") %>"><img src="<%= ResolveUrl("~/images/BottomBox02_WhatsNew.gif") %>" width="262px" height="51px"></A>
                            <A href="<%= ResolveUrl("~/Seeker/Matches") %>"><Img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox02.gif") %>" width="237px" height="86px"></A>
                            <P>Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                            <p class="IcoBoxArrow"><A href="<%= ResolveUrl("~/Seeker/Matches") %>"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px">&nbsp;Go to My Matches</A></P>
                      </div>
                      <div id="RightBottomBox">
                            <A href="<%= ResolveUrl("~/Seeker/Scholarships") %>"><img src="<%= ResolveUrl("~/images/BottomBox03_ApplyToday.gif") %>" width="262px" height="51px"></A>
                            <A href="<%= ResolveUrl("~/Seeker/Scholarships") %>"><Img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox03.gif") %>" width="237px" height="86px"></A>
                            <P>Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus, congue et sagittis id, sollicitudin id elit.</P>
                            <p class="IcoBoxArrow"><A href="<%= ResolveUrl("~/Seeker/Scholarships") %>"><img src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px">&nbsp;Go to My Applications</A></P>
                      </div>



                 </div> <BR>    
    
</asp:Content>
