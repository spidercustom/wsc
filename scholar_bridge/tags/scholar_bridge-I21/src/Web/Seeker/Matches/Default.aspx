﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Matches.Default"  Title="Seeker | My Matches" %>

<%@ Register src="~/Common/MatchList.ascx" tagname="MatchList" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerProfileProgress.ascx" tagname="SeekerProfileProgress" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopMyMatches.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomMyMatches.gif" Width="918px" Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
                <!--Left floated content area starts here-->
                <div id="HomeContentLeft">

                  <Img src="<%= ResolveUrl("~/images/PgTitle_MyMatches.gif") %>" width="399px" height="54px">
                  <img src="<%= ResolveUrl("~/images/EmphasisedMyMatchesPage.gif") %>" width="513px" height="96px">
                  <P class="ListDescriptionText">The Required Criteria column determines if you qualify for a scholarship. The
                  Preferred Criteria column shows the number of additional selection criteria 
                  that you meet. Click on the scholarship name to see the criteria.</P>
                 </div>
                  <!--Left floated content area ends here-->

                 <!--Right Floated content area starts here-->
                 <div id="HomeContentRight">
                 <sb:SeekerProfileProgress Id="ProfileProgess" runat="server" />
                 </div>
                <BR>

                 <div id="Clear"></div>




    <ul style="display:inline">
        <li class="left-part">
            <h3>My Scholarships of interest</h3>
        </li>
        <li class="right-part">
            <ul>
                <li>
                    <asp:Image ImageUrl="~/images/matched_scholarship.png" runat="server"/>
                    <span>Matched scholarship not yet saved to <strong>My Scholarships</strong> list</span>
                </li>
                <li>
                    <asp:Image ImageUrl="~/images/saved_scholarship.png" runat="server"/>
                    <span>Scholarship is saved to <strong>My Scholarships</strong> list</span>
                </li>
                <li>
                    <asp:Image ImageUrl="~/images/applied-scholarship.png" runat="server"/>
                    <span>Application is created for scholarship</span>
                </li>
            </ul>
        </li>
    </ul>
    <br />
    
    <sb:MatchList id="savedList" runat="server" OnMatchAction="list_OnMatchAction" PageSize="4" />
    <br />
<hr />
<br />
    <h3>My Matches</h3>
    <p>Scholarships you qualify for:</p>
    <sb:MatchList id="qualifyList" runat="server" OnMatchAction="list_OnMatchAction" />

</asp:Content>
