﻿<%@ Page Title="Provider" Language="C#" AutoEventWireup="true" CodeBehind="Anonymous.aspx.cs"
    Inherits="ScholarBridge.Web.Seeker.Anonymous" %>

<%@ Register Src="~/Common/ScholarshipSearchBox.ascx" TagName="ScholarshipSearchBox"
    TagPrefix="sb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<%@ Register Src="~/Common/GlobalFooter.ascx" TagName="GlobalFooter" TagPrefix="sb" %>
<%@ Register Src="~/Common/ProviderMenu.ascx" TagName="ProviderMenu" TagPrefix="sb" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>The WashBoard.org - Seeker's Home</title>
    <meta name="FORMAT" content="text/html"/>
    <meta name="CHARSET" content="ISO-8859-1"/>
    <meta name="DOCUMENTLANGUAGECODE" content="en"/>
    <meta name="DOCUMENTCOUNTRYCODE" content="us"//>
    <meta name="DC.LANGUAGE" scheme="rfc1766" content="en-us"/>
    <meta name="COPYRIGHT" content="Copyright (c) 2009 by Washington Scholarship Coalition"/>
    <meta name="SECURITY" content="Public"/>
    <meta name="ROBOTS" content="index,follow"/>
    <meta name="GOOGLEBOT" content="index,follow"/>
    <meta name="Description" content="The WashBoard.org Provider Home "/>
    <meta name="Keywords" content=""/>
    <meta name="Author" content="The WashBoard.org"/>
    <!-- Base keywords here-->

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
        google.load("jquery", "1.3");
    </script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.7.1.min.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tablesorter.min.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.pager.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tinysort.min.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskedinput.min.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.cookie.min.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.watermark-2.0.min.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/superfish.js") %>"></script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/site.js") %>"></script>

    <%--<link rel="stylesheet" href="<%= ResolveUrl("~/styles/main.css") %>" type="text/css" />--%>
    <link href="<%= ResolveUrl("~/styles/WSCStyles.CSS") %>" rel="stylesheet" type="text/css"
        media="All" />
</head>
<body>
    <form id="aspnetForm" runat="server">
    <!--Page wrapper starts here-->
    <div id="EntirePageWrapper">
        <div id="HeaderWrapper">
            <div id="Logo">
                <img alt="" src="<%= ResolveUrl("~/images/LeftShadow_Logo.gif") %>" width="22px"
                    height="137px" /><a href="<%= ResolveUrl("~/") %>"><img alt="" src="<%= ResolveUrl("~/images/LogoTheWashBoard.gif") %>"
                        width="234px" height="137px" /></a></div>
            <div id="successMessage">
                <sb:SuccessMessageLabel ID="SuccessMessageLabel2" runat="server"></sb:SuccessMessageLabel></div>
            <div id="LogoRight">
                <img alt="" src="<%= ResolveUrl("~/images/LogoRight.gif") %>" width="640px" height="137px" /><img
                    alt="" src="<%= ResolveUrl("~/images/RightShadow_Logo.gif") %>" width="22px"
                    height="137px;" /></div>
        </div>
        <!--Welcome text starts here-->
        <!-- <div id="WelcomeTextContainer">
          <div class="WelcomeText">Welcome back Brad!!</div>

     </div>   -->
        <!--Welcome text ends here-->
        <!--topmenu starts here-->
        <div id="MenuContainer">
            <a href="#" onclick="loginalert();">
                <img alt="" src="<%= ResolveUrl("~/images/NavSeeker_MyProfileInactive.gif") %>" width="124px"
                    height="36px" />
            </a>
            <img alt="" src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px" />
            <a href="#" onclick="loginalert();">
                <img alt="" src="<%= ResolveUrl("~/images/NavSeeker_MyMatchesInactive.gif") %>" width="113px"
                    height="36px" />
            </a>
            <img alt="" src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px" />
            <a href="#" onclick="loginalert();">
                <img alt="" src="<%= ResolveUrl("~/images/NavSeeker_MyApplicationsInactive.gif") %>"
                    width="141px" height="36px" />
            </a>
            <img alt="" src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px" />
            <a href="#" onclick="loginalert();">
                <img alt="" src="<%= ResolveUrl("~/images/NavSeeker_MySettingsInactive.gif") %>"
                    width="111px" height="36px" />
            </a>
            <img alt="" src="<%= ResolveUrl("~/images/NavSeparator.gif") %>" width="2px" height="36px" /><a
                href="#" onclick="loginalert();"><img alt="" src="<%= ResolveUrl("~/images/NavSeeker_MyMegsInactive.gif") %>"
                    width="126px" height="36px" /></a>
        </div>
        <!--Global nav links start here-->
        <div id="GlobaNavLinksContainerSeeker">
            <div class="GlobalNavLinks">
                <sb:ScholarshipSearchBox runat="server" />
            </div>
        </div>
        <!--Branding picture starts here-->
        <div>
            <img alt="" src="<%= ResolveUrl("~/images/PicTopSeekerHome.gif") %>" width="918px"
                height="15px" /></div>
        <div>
            <img alt="" src="<%= ResolveUrl("~/images/PicBottomSeekerHome.gif") %>" width="918px"
                height="265px" /></div>
        <!--Branding picture ends here-->
        <!--This is the outer most wrapper 01 starts here-->
        <div id="ContentWrapper01">
            <div id="LeftPageShadow">
                <img alt="" src="<%= ResolveUrl("~/images/LeftContentShadow.gif") %>" /></div>
            <div id="RightPageShadow">
                <img alt="" src="<%= ResolveUrl("~/images/RightContentShadow.gif") %>" /></div>
            <!--This is content wrapper 02 starts here-->
            <div id="ContentWrapper02">
                <!--Left floated content area starts here-->
                <div id="HomeContentLeft">
                    <img alt="" src="<%= ResolveUrl("~/images/PgTitle_SmarterScholarshipMatches.gif") %>"
                        width="340px" height="54px" />
                    <img alt="" src="<%= ResolveUrl("~/images/EmphasisedTextSeekerUnlogged.gif") %>"
                        width="513px" height="96px" />
                </div>
                <!--Left floated content area ends here-->
                <!--Right Floated content area starts here-->
                <div id="HomeContentRight">
                    <div class="LoginContainer">
                        <div class="Vertical">
                            <sb:Login ID="loginForm" runat="server" />
                        </div>
                    </div>
                </div>
                <!--Right Floated content area ends here-->
                <br/>
                <br/>
                <div id="DIV1">
                </div>
                <div style='clear: both;'>
                </div>
                <!--The Three column starts here-->
                <div id="BoxWrapper">
                    <div id="LeftBottomBox">
                        <a href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">
                            <img alt="" src="<%= ResolveUrl("~/images/BottomBox01_StepOne.gif") %>" width="262px"
                                height="51px" /></a> <a href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">
                                    <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox01.gif") %>" width="237px"
                                        height="86px" /></a>
                        <p style="width: 237px">
                            Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus,
                            congue et sagittis id, sollicitudin id elit.</p>
                        <p class="IcoBoxArrow">
                            <a href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">
                                <img alt="" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" class="GreenArrow" />&nbsp;Register</a></p>
                    </div>
                    <div id="CenterBottomBox">
                        <a href="#" onclick="loginalert();">
                            <img alt="" src="<%= ResolveUrl("~/images/BottomBox02_StepTwo.gif") %>" width="262px"
                                height="51px" /></a> <a href="#" onclick="loginalert();">
                                    <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox02.gif") %>" width="237px"
                                        height="86px" /></a>
                        <p style="width: 237px">
                            Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus,
                            congue et sagittis id, sollicitudin id elit.</p>
                        <p class="IcoBoxArrow">
                            <a href="#" onclick="loginalert();">
                                <img alt="" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" class="GreenArrow" />&nbsp;Create
                                your profile</a></p>
                    </div>
                    <div id="RightBottomBox">
                        <a href="#" onclick="loginalert();">
                            <img alt="" src="<%= ResolveUrl("~/images/BottomBox03_StepThree.gif") %>" width="262px"
                                height="51px" /></a> <a href="#" onclick="loginalert();">
                                    <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox03.gif") %>" width="237px"
                                        height="86px" /></a>
                        <p style="width: 237px">
                            Lorem ipsum dolor sitamet, consectetur adipiscing elit. Vestibulum arcu lectus,
                            congue et sagittis id, sollicitudin id elit.</p>
                        <p class="IcoBoxArrow">
                            <a href="#" onclick="loginalert();">
                                <img alt="" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" class="GreenArrow" />&nbsp;Go
                                to my matches </a>
                        </p>
                    </div>
                </div>
                <br/>
                <div id="Clear">
                </div>
                <hr/>
                <div id="Footer">
                    <sb:GlobalFooter ID="GlobalFooter1" runat="server" />
                </div>
            </div>
            <!--This is content wrapper 02 ends here-->
        </div>
        <!--This is the outer most wrapper 01 ends here-->
    </div>
    <!--Page wrapper ends here-->
    </form>
</body>
</html>
