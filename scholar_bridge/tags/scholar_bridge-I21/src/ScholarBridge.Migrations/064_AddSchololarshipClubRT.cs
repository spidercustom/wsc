﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(64)]
    public class AddSchololarshipClubRT : AddRelationTableBase
    {
        protected override string FirstTableName
        {
            get { return "SBScholarship"; }
        }

        protected override string SecondTableName
        {
            get { return "SBClubLUT"; }
        }
    }
}
