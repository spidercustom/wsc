﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(199)]
    public class ChangeMatchRelateApplication : Migration
    {
        private const string TABLE_NAME = "SBMatch";
        private const string FK_APPLICATION = "FK_SBMatch_SBApplication";
        private const string APPLICATION_PRIMARY_KEY_COLUMN = "SBApplicationId";
        private const string APPLICATION_TABLE_NAME = "SBApplication";

        private static readonly Column ApplicationRelationColumn = new Column(
            APPLICATION_PRIMARY_KEY_COLUMN,
            DbType.Int32,
            ColumnProperty.Null
            );
 
        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, ApplicationRelationColumn);
            Database.AddForeignKey(FK_APPLICATION, 
                TABLE_NAME, 
                APPLICATION_PRIMARY_KEY_COLUMN, 
                APPLICATION_TABLE_NAME, 
                APPLICATION_PRIMARY_KEY_COLUMN);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, APPLICATION_PRIMARY_KEY_COLUMN);
        }
    }
}