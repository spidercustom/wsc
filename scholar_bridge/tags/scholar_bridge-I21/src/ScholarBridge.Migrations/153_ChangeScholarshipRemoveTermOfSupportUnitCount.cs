﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(153)]
    public class ChangeScholarshipRemoveTermOfSupportUnitCount : Migration
    {
        public override void Up()
        {
            Database.RemoveColumn(
                ChangeScholarshipFundingParameterTermsOfSupport.SCHOLARSHIP_TABLE_NAME,
                ChangeScholarshipFundingParameterTermsOfSupport.TERM_OF_SUPPORT_COUNT);
        }

        public override void Down()
        {
            Database.AddColumn(
                ChangeScholarshipFundingParameterTermsOfSupport.SCHOLARSHIP_TABLE_NAME,
                ChangeScholarshipFundingParameterTermsOfSupport.TERM_OF_SUPPORT_COUNT, 
                DbType.Int16);
        }
    }
}