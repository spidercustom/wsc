using System;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class UserDALTest : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public RoleDAL RoleDAL { get; set; }

        [Test]
        public void CanCreateUser()
        {
            var user = InsertUser(UserDAL, "test@spiderlogic.com");
            Assert.IsNotNull(user);
            Assert.IsNotNull(user.Name);
            Assert.IsNotNull(user.Name);
        }

        [Test]
        public void NameCreatedSuccessfully()
        {
            var user = InsertUser(UserDAL, "test@spiderlogic.com");
            Assert.IsNotNull(user);
            Assert.IsNotNull(user.Name.FirstName);
            Assert.IsNotNull(user.Name.LastName);

            Assert.AreEqual("TestFirst", user.Name.FirstName);
            Assert.AreEqual("TestLast", user.Name.LastName);
        }

        [Test]
        public void CanUpdateName()
        {
            var user = new User
            {
                Username = "test",
                Password = "@abc123K",
                Email = "foo@bar.com",
                Question = "Who am I?",
                Answer = "A test",
            };

            user = InsertUser(UserDAL, user);
            Assert.IsNotNull(user);

            user.Name.FirstName = "TestFirst";
            user.Name.LastName = "TestLast";
            UserDAL.Update(user);

            var foundUser = UserDAL.FindByUsername(user.Username);

            Assert.IsNotNull(foundUser);
            Assert.IsNotNull(foundUser.Name);
            Assert.AreEqual("TestFirst", foundUser.Name.FirstName);
            Assert.AreEqual("TestLast", foundUser.Name.LastName);
        }

        [Test]
        public void CanGetUserByEmail()
        {
            var user = InsertUser(UserDAL, "test@spiderlogic.com");

            var foundUser = UserDAL.FindByEmail("test@spiderlogic.com");
            Assert.IsNotNull(foundUser);
            Assert.AreEqual(user.Username, foundUser.Username);
            Assert.AreEqual(user.Email, foundUser.Email);
        }

        [Test]
        public void CanGetUserByUsername()
        {
            var user = InsertUser(UserDAL, "test@spiderlogic.com");

            var foundUser = UserDAL.FindByUsername("test@spiderlogic.com");
            Assert.IsNotNull(foundUser);
            Assert.AreEqual(user.Username, foundUser.Username);
            Assert.AreEqual(user.Email, foundUser.Email);
        }

        [Test]
        public void can_not_get_deleted_user_by_username()
        {
            var user = InsertUser(UserDAL, "test@spiderlogic.com");

            var foundUser = UserDAL.FindByUsername("test@spiderlogic.com");
            Assert.IsNotNull(foundUser);
            Assert.AreEqual(user.Username, foundUser.Username);
            Assert.AreEqual(user.Email, foundUser.Email);

            UserDAL.Delete(foundUser);

            Assert.IsNull(UserDAL.FindByUsername("test@spiderlogic.com"));
        }

        [Test]
        public void CanGetAllUsersByEmail()
        {
            var user = InsertUser(UserDAL, "test@neverindb.com");

            var foundUsers = UserDAL.FindByEmail("*neverindb.com", 0, 10);
            Assert.IsNotNull(foundUsers);
            CollectionAssert.IsNotEmpty(foundUsers);
            CollectionAssert.AllItemsAreNotNull(foundUsers);
            Assert.AreEqual(user.Username, foundUsers[0].Username);
            Assert.AreEqual(user.Email, foundUsers[0].Email);
        }

        [Test]
        public void CanGetUserByUsernameAndPassword()
        {
            var user = InsertUser(UserDAL, "test@spiderlogic.com");

            var foundUser = UserDAL.FindByUsernameAndPassword("test@spiderlogic.com", "@abc123K");
            Assert.IsNotNull(foundUser);
            Assert.AreEqual(user.Username, foundUser.Username);
            Assert.AreEqual(user.Email, foundUser.Email);
        }

        [Test]
        public void CanFindAll()
        {
            var initialCount = UserDAL.FindAll(0, 1000).Count;

            InsertUser(UserDAL, "test@spiderlogic.com");

			var foundUsers = UserDAL.FindAll(0, 1000);
            Assert.IsNotNull(foundUsers);
            Assert.AreEqual(initialCount + 1, foundUsers.Count);
        }

        [Test]
        public void find_all_filters_deleted_users()
        {
			var initialCount = UserDAL.FindAll(0, 1000).Count;

            var user = InsertUser(UserDAL, "test@spiderlogic.com");

			var foundUsers = UserDAL.FindAll(0, 1000);
            Assert.IsNotNull(foundUsers);
            Assert.AreEqual(initialCount + 1, foundUsers.Count);

            UserDAL.Delete(user);

			var foundUsers2 = UserDAL.FindAll(0, 1000);
            Assert.AreEqual(initialCount, foundUsers2.Count);
        }

        [Test]
        public void CanFindAllByWildCard()
        {
            var initialCount = UserDAL.FindByRoleNameAndUsernameWildcard("TestRole", "Test").Count;

            InsertUserWithRole();

            var foundUsers = UserDAL.FindByRoleNameAndUsernameWildcard("TestRole", "Test");
            Assert.IsNotNull(foundUsers);
            Assert.AreEqual(initialCount + 1, foundUsers.Count);

            var notFoundUsers = UserDAL.FindByRoleNameAndUsernameWildcard("Bad", "Test");
            Assert.IsNotNull(notFoundUsers);
            Assert.AreEqual(0, notFoundUsers.Count);
        }


        [Test]
        public void can_find_all_by_role_name()
        {
            var initialCount = UserDAL.FindByRoleName("TestRole").Count;

            InsertUserWithRole();

            var foundUsers = UserDAL.FindByRoleName("TestRole");
            Assert.IsNotNull(foundUsers);
            Assert.AreEqual(initialCount + 1, foundUsers.Count);

            var notFoundUsers = UserDAL.FindByRoleName("Bad");
            Assert.IsNotNull(notFoundUsers);
            Assert.AreEqual(0, notFoundUsers.Count);
        }

        [Test]
        public void can_find_nonactivated_users()
        {
            var initialAll = UserDAL.FindAll("Username").Count;
            var initialInactive = UserDAL.FindNonConfirmedUsers(5).Count;
            
            var activeUser = new User
                                 {
                                     Username = "activated@example.com",
                                     Password = "@abc123K",
                                     Question = "Who am I?",
                                     Answer = "A test",
                                     Email = "activated@example.com",
                                     Name = new PersonName {FirstName = "TestFirst", LastName = "TestLast"},
                                     IsActive = true,
                                     LastUpdate = new ActivityStamp
                                                      {
                                                          On = DateTime.Now.AddDays(-10)
                                                      }
                                 };
            var inactiveUser = new User
                                   {
                                       Username = "inactive@example.com",
                                       Password = "@abc123K",
                                       Question = "Who am I?",
                                       Answer = "A test",
                                       Email = "activated@example.com",
                                       Name = new PersonName {FirstName = "TestFirst", LastName = "TestLast"},
                                       IsActive = false,
                                       LastUpdate = new ActivityStamp
                                                        {
                                                            On = DateTime.Now.AddDays(-10)
                                                        }
                                   };

            var newInactiveUser = new User
                                      {
                                          Username = "new_inactive@example.com",
                                          Password = "@abc123K",
                                          Question = "Who am I?",
                                          Answer = "A test",
                                          Email = "activated@example.com",
                                          Name = new PersonName {FirstName = "TestFirst", LastName = "TestLast"},
                                          IsActive = false,
                                          LastUpdate = new ActivityStamp
                                                           {
                                                               On = DateTime.Now.AddDays(-2)
                                                           }
                                      };

            UserDAL.Insert(activeUser);
            UserDAL.Insert(inactiveUser);
            UserDAL.Insert(newInactiveUser);

            Assert.That(UserDAL.FindAll("Username").Count, Is.EqualTo(initialAll + 3));
            Assert.That(UserDAL.FindNonConfirmedUsers(5).Count, Is.EqualTo(initialInactive + 1));
        }

        [Test]
        public void delete_performs_soft_delete()
        {
            var user = InsertUser(UserDAL, "test@example.com");
            UserDAL.Delete(user);

            var foundUser = UserDAL.FindById(user.Id);
            Assert.IsNotNull(foundUser);
            Assert.IsTrue(foundUser.IsDeleted);
        }

        private void InsertUserWithRole()
        {
            var user = InsertUser(UserDAL, "test@spiderlogic.com");
            var role = RoleDALTest.InsertRoleWithUser(RoleDAL, "TestRole", user);
            user.Roles.Add(role);
            UserDAL.Update(user);
        }

        public static User InsertUser(IUserDAL userDAL, string email)
        {
        	var user = new User
        	           	{
    	           			Username = email,
    	           			Password = "@abc123K",
    	           			Question = "Who am I?",
    	           			Answer = "A test",
							Email=email,
							Name = new PersonName { FirstName = "TestFirst", LastName = "TestLast" }
                       };

            return InsertUser(userDAL, user);
        }

        private static User InsertUser(IUserDAL userDAL, User user)
        {
            var createdUser = userDAL.Insert(user);

            Assert.IsNotNull(createdUser);
            Assert.AreNotEqual(0, createdUser.Id);

            return createdUser;
        }
    }
}