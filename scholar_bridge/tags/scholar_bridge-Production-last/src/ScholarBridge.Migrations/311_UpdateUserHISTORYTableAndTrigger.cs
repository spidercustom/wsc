﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(311)]
	public class UpdateUserHistoryTableAndTrigger : Migration
	{
		public override void Up()
		{
			Database.AddColumn("SBUserHISTORY", new Column("EmailWaitingforVerification", DbType.String, 50, ColumnProperty.Null));
			Database.AddColumn("SBUserHISTORY", new Column("IsRecieveEmails", DbType.Boolean, ColumnProperty.Null));
			UpdateTriggerUP();
		}


		public override void Down()
		{
			UpdateTriggerDOWN();
			Database.RemoveColumn("SBUserHISTORY", "EmailWaitingforVerification");
			Database.RemoveColumn("SBUserHISTORY", "IsRecieveEmails");
		}

		private void UpdateTriggerUP()
		{
			Database.ExecuteNonQuery(
				@"
ALTER TRIGGER [dbo].[DeleteSBUser] ON [dbo].[SBUser] 
	AFTER DELETE
	AS
	BEGIN
	
		-- Check for updated rows, if none then exit
		IF @@ROWCOUNT = 0
			RETURN
	
		-- Disable ^x rows affected^ messages for history table inserts
		SET NOCOUNT ON
	
		-- Copy modified record(s) into SBUserHistory table
	
		INSERT dbo.SBUserHISTORY 
			(SBUserID,Username,Password,Email,
				Question,Answer,IsApproved,IsLockedOut,
				IsActive,IsDeleted,IsPasswordReset,IsOnLine,
				CreationDate,LastActivityDate,LastLoginDate,
				LastLockedOutDate,LastPasswordChangeDate,PasswordFormat,
				PasswordSalt,Comments,FailedPasswordAttemptCount,
				FailedPasswordAttemptWindowStart,FailedPasswordAnswerAttemptCount,
				FailedPasswordAnswerAttemptWindowStart,LastUpdateBy,LastUpdateDate,
				FirstName,MiddleName,LastName,PhoneNumber,FaxNumber,OtherPhoneNumber,
				EmailWaitingforVerification, IsRecieveEmails )
		SELECT 
			D.SBUserID,D.Username,D.Password,D.Email,
			D.Question,D.Answer,D.IsApproved,D.IsLockedOut,D.IsActive,D.IsDeleted,D.IsPasswordReset,D.IsOnLine,
			D.CreationDate,D.LastActivityDate,D.LastLoginDate,
			D.LastLockedOutDate,D.LastPasswordChangeDate,D.PasswordFormat,
			D.PasswordSalt,D.Comments,D.FailedPasswordAttemptCount,
			D.FailedPasswordAttemptWindowStart,D.FailedPasswordAnswerAttemptCount,
			D.FailedPasswordAnswerAttemptWindowStart,D.LastUpdateBy,D.LastUpdateDate,
			D.FirstName,D.MiddleName,D.LastName,D.PhoneNumber,D.FaxNumber,D.OtherPhoneNumber,
			D.EmailWaitingforVerification, D.IsRecieveEmails 
		FROM Deleted D
	END
	
			");

			Database.ExecuteNonQuery(
				@"
ALTER TRIGGER [dbo].[UpdateSBUser] ON [dbo].[SBUser] 
	AFTER UPDATE
	AS
	BEGIN
	
		-- Check for updated rows, if none then exit
		IF @@ROWCOUNT = 0
			RETURN
	
		-- Disable ^x rows affected^ messages for history table inserts
		SET NOCOUNT ON
	
		-- Copy modified record(s) into SBUserHistory table
	
		INSERT dbo.SBUserHISTORY 
			(SBUserID,Username,Password,Email,Question,Answer,IsApproved,IsLockedOut,IsActive,IsDeleted,IsPasswordReset,IsOnLine,CreationDate,LastActivityDate,LastLoginDate,LastLockedOutDate,LastPasswordChangeDate,PasswordFormat,PasswordSalt,Comments,FailedPasswordAttemptCount,FailedPasswordAttemptWindowStart,FailedPasswordAnswerAttemptCount,FailedPasswordAnswerAttemptWindowStart,LastUpdateBy,LastUpdateDate,FirstName,MiddleName,LastName,PhoneNumber,FaxNumber,OtherPhoneNumber)
		SELECT 
			D.SBUserID,D.Username,D.Password,D.Email,D.Question,D.Answer,D.IsApproved,D.IsLockedOut,D.IsActive,D.IsDeleted,D.IsPasswordReset,D.IsOnLine,D.CreationDate,D.LastActivityDate,D.LastLoginDate,D.LastLockedOutDate,D.LastPasswordChangeDate,D.PasswordFormat,D.PasswordSalt,D.Comments,D.FailedPasswordAttemptCount,D.FailedPasswordAttemptWindowStart,D.FailedPasswordAnswerAttemptCount,D.FailedPasswordAnswerAttemptWindowStart,D.LastUpdateBy,D.LastUpdateDate,D.FirstName,D.MiddleName,D.LastName,D.PhoneNumber,D.FaxNumber,D.OtherPhoneNumber
		FROM Deleted D
		INNER JOIN Inserted I 
		ON 	(D.SBUserID = I.SBUserID)
		WHERE	(D.Username <> I.Username)
	OR (D.Password <> I.Password)
	OR (D.Email <> I.Email)
	OR (D.Question <> I.Question
			OR	(D.Question IS NULL AND I.Question IS NOT NULL)
			OR	(D.Question IS NOT NULL AND I.Question IS NULL))
	OR (D.Answer <> I.Answer
			OR	(D.Answer IS NULL AND I.Answer IS NOT NULL)
			OR	(D.Answer IS NOT NULL AND I.Answer IS NULL))
	OR (D.IsApproved <> I.IsApproved
			OR	(D.IsApproved IS NULL AND I.IsApproved IS NOT NULL)
			OR	(D.IsApproved IS NOT NULL AND I.IsApproved IS NULL))
	OR (D.IsLockedOut <> I.IsLockedOut
			OR	(D.IsLockedOut IS NULL AND I.IsLockedOut IS NOT NULL)
			OR	(D.IsLockedOut IS NOT NULL AND I.IsLockedOut IS NULL))
	OR (D.IsActive <> I.IsActive
			OR	(D.IsActive IS NULL AND I.IsActive IS NOT NULL)
			OR	(D.IsActive IS NOT NULL AND I.IsActive IS NULL))
	OR (D.IsDeleted <> I.IsDeleted
			OR	(D.IsDeleted IS NULL AND I.IsDeleted IS NOT NULL)
			OR	(D.IsDeleted IS NOT NULL AND I.IsDeleted IS NULL))
	OR (D.IsPasswordReset <> I.IsPasswordReset
			OR	(D.IsPasswordReset IS NULL AND I.IsPasswordReset IS NOT NULL)
			OR	(D.IsPasswordReset IS NOT NULL AND I.IsPasswordReset IS NULL))
	OR (D.IsOnLine <> I.IsOnLine
			OR	(D.IsOnLine IS NULL AND I.IsOnLine IS NOT NULL)
			OR	(D.IsOnLine IS NOT NULL AND I.IsOnLine IS NULL))
	OR (D.CreationDate <> I.CreationDate
			OR	(D.CreationDate IS NULL AND I.CreationDate IS NOT NULL)
			OR	(D.CreationDate IS NOT NULL AND I.CreationDate IS NULL))
	OR (D.LastActivityDate <> I.LastActivityDate
			OR	(D.LastActivityDate IS NULL AND I.LastActivityDate IS NOT NULL)
			OR	(D.LastActivityDate IS NOT NULL AND I.LastActivityDate IS NULL))
	OR (D.LastLoginDate <> I.LastLoginDate
			OR	(D.LastLoginDate IS NULL AND I.LastLoginDate IS NOT NULL)
			OR	(D.LastLoginDate IS NOT NULL AND I.LastLoginDate IS NULL))
	OR (D.LastLockedOutDate <> I.LastLockedOutDate
			OR	(D.LastLockedOutDate IS NULL AND I.LastLockedOutDate IS NOT NULL)
			OR	(D.LastLockedOutDate IS NOT NULL AND I.LastLockedOutDate IS NULL))
	OR (D.LastPasswordChangeDate <> I.LastPasswordChangeDate
			OR	(D.LastPasswordChangeDate IS NULL AND I.LastPasswordChangeDate IS NOT NULL)
			OR	(D.LastPasswordChangeDate IS NOT NULL AND I.LastPasswordChangeDate IS NULL))
	OR (D.PasswordFormat <> I.PasswordFormat
			OR	(D.PasswordFormat IS NULL AND I.PasswordFormat IS NOT NULL)
			OR	(D.PasswordFormat IS NOT NULL AND I.PasswordFormat IS NULL))
	OR (D.PasswordSalt <> I.PasswordSalt
			OR	(D.PasswordSalt IS NULL AND I.PasswordSalt IS NOT NULL)
			OR	(D.PasswordSalt IS NOT NULL AND I.PasswordSalt IS NULL))
	OR (D.Comments <> I.Comments
			OR	(D.Comments IS NULL AND I.Comments IS NOT NULL)
			OR	(D.Comments IS NOT NULL AND I.Comments IS NULL))
	OR (D.FailedPasswordAttemptCount <> I.FailedPasswordAttemptCount
			OR	(D.FailedPasswordAttemptCount IS NULL AND I.FailedPasswordAttemptCount IS NOT NULL)
			OR	(D.FailedPasswordAttemptCount IS NOT NULL AND I.FailedPasswordAttemptCount IS NULL))
	OR (D.FailedPasswordAttemptWindowStart <> I.FailedPasswordAttemptWindowStart
			OR	(D.FailedPasswordAttemptWindowStart IS NULL AND I.FailedPasswordAttemptWindowStart IS NOT NULL)
			OR	(D.FailedPasswordAttemptWindowStart IS NOT NULL AND I.FailedPasswordAttemptWindowStart IS NULL))
	OR (D.FailedPasswordAnswerAttemptCount <> I.FailedPasswordAnswerAttemptCount
			OR	(D.FailedPasswordAnswerAttemptCount IS NULL AND I.FailedPasswordAnswerAttemptCount IS NOT NULL)
			OR	(D.FailedPasswordAnswerAttemptCount IS NOT NULL AND I.FailedPasswordAnswerAttemptCount IS NULL))
	OR (D.FailedPasswordAnswerAttemptWindowStart <> I.FailedPasswordAnswerAttemptWindowStart
			OR	(D.FailedPasswordAnswerAttemptWindowStart IS NULL AND I.FailedPasswordAnswerAttemptWindowStart IS NOT NULL)
			OR	(D.FailedPasswordAnswerAttemptWindowStart IS NOT NULL AND I.FailedPasswordAnswerAttemptWindowStart IS NULL))
	OR (D.FirstName <> I.FirstName
			OR	(D.FirstName IS NULL AND I.FirstName IS NOT NULL)
			OR	(D.FirstName IS NOT NULL AND I.FirstName IS NULL))
	OR (D.MiddleName <> I.MiddleName
			OR	(D.MiddleName IS NULL AND I.MiddleName IS NOT NULL)
			OR	(D.MiddleName IS NOT NULL AND I.MiddleName IS NULL))
	OR (D.LastName <> I.LastName
			OR	(D.LastName IS NULL AND I.LastName IS NOT NULL)
			OR	(D.LastName IS NOT NULL AND I.LastName IS NULL))
	OR (D.PhoneNumber <> I.PhoneNumber
			OR	(D.PhoneNumber IS NULL AND I.PhoneNumber IS NOT NULL)
			OR	(D.PhoneNumber IS NOT NULL AND I.PhoneNumber IS NULL))
	OR (D.FaxNumber <> I.FaxNumber
			OR	(D.FaxNumber IS NULL AND I.FaxNumber IS NOT NULL)
			OR	(D.FaxNumber IS NOT NULL AND I.FaxNumber IS NULL))
	OR (D.OtherPhoneNumber <> I.OtherPhoneNumber
			OR	(D.OtherPhoneNumber IS NULL AND I.OtherPhoneNumber IS NOT NULL)
			OR	(D.OtherPhoneNumber IS NOT NULL AND I.OtherPhoneNumber IS NULL))
	OR (D.EmailWaitingforVerification <> I.EmailWaitingforVerification
			OR	(D.EmailWaitingforVerification IS NULL AND I.EmailWaitingforVerification IS NOT NULL)
			OR	(D.EmailWaitingforVerification IS NOT NULL AND I.EmailWaitingforVerification IS NULL))
	OR (D.IsRecieveEmails <> I.IsRecieveEmails
			OR	(D.IsRecieveEmails IS NULL AND I.IsRecieveEmails IS NOT NULL)
			OR	(D.IsRecieveEmails IS NOT NULL AND I.IsRecieveEmails IS NULL))
	
		-- Update LastUpdateDate if not explicitly set
		IF NOT UPDATE(LastUpdateDate)
		BEGIN
			UPDATE dbo.SBUser
			SET LastUpdateDate = GETDATE()
			FROM Inserted I
			INNER JOIN dbo.SBUser A 
			ON (I.SBUserID = A.SBUserID)
		END
	END
	

"
				);
		}

		private void UpdateTriggerDOWN()
		{
			Database.ExecuteNonQuery(
				@"
ALTER TRIGGER [dbo].[DeleteSBUser] ON [dbo].[SBUser] 
	AFTER DELETE
	AS
	BEGIN
	
		-- Check for updated rows, if none then exit
		IF @@ROWCOUNT = 0
			RETURN
	
		-- Disable ^x rows affected^ messages for history table inserts
		SET NOCOUNT ON
	
		-- Copy modified record(s) into SBUserHistory table
	
		INSERT dbo.SBUserHISTORY 
			(SBUserID,Username,Password,Email,Question,Answer,IsApproved,IsLockedOut,IsActive,IsDeleted,IsPasswordReset,IsOnLine,CreationDate,LastActivityDate,LastLoginDate,LastLockedOutDate,LastPasswordChangeDate,PasswordFormat,PasswordSalt,Comments,FailedPasswordAttemptCount,FailedPasswordAttemptWindowStart,FailedPasswordAnswerAttemptCount,FailedPasswordAnswerAttemptWindowStart,LastUpdateBy,LastUpdateDate,FirstName,MiddleName,LastName,PhoneNumber,FaxNumber,OtherPhoneNumber)
		SELECT 
			D.SBUserID,D.Username,D.Password,D.Email,D.Question,D.Answer,D.IsApproved,D.IsLockedOut,D.IsActive,D.IsDeleted,D.IsPasswordReset,D.IsOnLine,D.CreationDate,D.LastActivityDate,D.LastLoginDate,D.LastLockedOutDate,D.LastPasswordChangeDate,D.PasswordFormat,D.PasswordSalt,D.Comments,D.FailedPasswordAttemptCount,D.FailedPasswordAttemptWindowStart,D.FailedPasswordAnswerAttemptCount,D.FailedPasswordAnswerAttemptWindowStart,D.LastUpdateBy,D.LastUpdateDate,D.FirstName,D.MiddleName,D.LastName,D.PhoneNumber,D.FaxNumber,D.OtherPhoneNumber
		FROM Deleted D
	END
				");
			Database.ExecuteNonQuery(
				@"
ALTER TRIGGER [dbo].[UpdateSBUser] ON [dbo].[SBUser] 
	AFTER UPDATE
	AS
	BEGIN
	
		-- Check for updated rows, if none then exit
		IF @@ROWCOUNT = 0
			RETURN
	
		-- Disable ^x rows affected^ messages for history table inserts
		SET NOCOUNT ON
	
		-- Copy modified record(s) into SBUserHistory table
	
		INSERT dbo.SBUserHISTORY 
			(SBUserID,Username,Password,Email,Question,Answer,IsApproved,IsLockedOut,IsActive,IsDeleted,IsPasswordReset,IsOnLine,CreationDate,LastActivityDate,LastLoginDate,LastLockedOutDate,LastPasswordChangeDate,PasswordFormat,PasswordSalt,Comments,FailedPasswordAttemptCount,FailedPasswordAttemptWindowStart,FailedPasswordAnswerAttemptCount,FailedPasswordAnswerAttemptWindowStart,LastUpdateBy,LastUpdateDate,FirstName,MiddleName,LastName,PhoneNumber,FaxNumber,OtherPhoneNumber)
		SELECT 
			D.SBUserID,D.Username,D.Password,D.Email,D.Question,D.Answer,D.IsApproved,D.IsLockedOut,D.IsActive,D.IsDeleted,D.IsPasswordReset,D.IsOnLine,D.CreationDate,D.LastActivityDate,D.LastLoginDate,D.LastLockedOutDate,D.LastPasswordChangeDate,D.PasswordFormat,D.PasswordSalt,D.Comments,D.FailedPasswordAttemptCount,D.FailedPasswordAttemptWindowStart,D.FailedPasswordAnswerAttemptCount,D.FailedPasswordAnswerAttemptWindowStart,D.LastUpdateBy,D.LastUpdateDate,D.FirstName,D.MiddleName,D.LastName,D.PhoneNumber,D.FaxNumber,D.OtherPhoneNumber
		FROM Deleted D
		INNER JOIN Inserted I 
		ON 	(D.SBUserID = I.SBUserID)
		WHERE	(D.Username <> I.Username)
	OR (D.Password <> I.Password)
	OR (D.Email <> I.Email)
	OR (D.Question <> I.Question
			OR	(D.Question IS NULL AND I.Question IS NOT NULL)
			OR	(D.Question IS NOT NULL AND I.Question IS NULL))
	OR (D.Answer <> I.Answer
			OR	(D.Answer IS NULL AND I.Answer IS NOT NULL)
			OR	(D.Answer IS NOT NULL AND I.Answer IS NULL))
	OR (D.IsApproved <> I.IsApproved
			OR	(D.IsApproved IS NULL AND I.IsApproved IS NOT NULL)
			OR	(D.IsApproved IS NOT NULL AND I.IsApproved IS NULL))
	OR (D.IsLockedOut <> I.IsLockedOut
			OR	(D.IsLockedOut IS NULL AND I.IsLockedOut IS NOT NULL)
			OR	(D.IsLockedOut IS NOT NULL AND I.IsLockedOut IS NULL))
	OR (D.IsActive <> I.IsActive
			OR	(D.IsActive IS NULL AND I.IsActive IS NOT NULL)
			OR	(D.IsActive IS NOT NULL AND I.IsActive IS NULL))
	OR (D.IsDeleted <> I.IsDeleted
			OR	(D.IsDeleted IS NULL AND I.IsDeleted IS NOT NULL)
			OR	(D.IsDeleted IS NOT NULL AND I.IsDeleted IS NULL))
	OR (D.IsPasswordReset <> I.IsPasswordReset
			OR	(D.IsPasswordReset IS NULL AND I.IsPasswordReset IS NOT NULL)
			OR	(D.IsPasswordReset IS NOT NULL AND I.IsPasswordReset IS NULL))
	OR (D.IsOnLine <> I.IsOnLine
			OR	(D.IsOnLine IS NULL AND I.IsOnLine IS NOT NULL)
			OR	(D.IsOnLine IS NOT NULL AND I.IsOnLine IS NULL))
	OR (D.CreationDate <> I.CreationDate
			OR	(D.CreationDate IS NULL AND I.CreationDate IS NOT NULL)
			OR	(D.CreationDate IS NOT NULL AND I.CreationDate IS NULL))
	OR (D.LastActivityDate <> I.LastActivityDate
			OR	(D.LastActivityDate IS NULL AND I.LastActivityDate IS NOT NULL)
			OR	(D.LastActivityDate IS NOT NULL AND I.LastActivityDate IS NULL))
	OR (D.LastLoginDate <> I.LastLoginDate
			OR	(D.LastLoginDate IS NULL AND I.LastLoginDate IS NOT NULL)
			OR	(D.LastLoginDate IS NOT NULL AND I.LastLoginDate IS NULL))
	OR (D.LastLockedOutDate <> I.LastLockedOutDate
			OR	(D.LastLockedOutDate IS NULL AND I.LastLockedOutDate IS NOT NULL)
			OR	(D.LastLockedOutDate IS NOT NULL AND I.LastLockedOutDate IS NULL))
	OR (D.LastPasswordChangeDate <> I.LastPasswordChangeDate
			OR	(D.LastPasswordChangeDate IS NULL AND I.LastPasswordChangeDate IS NOT NULL)
			OR	(D.LastPasswordChangeDate IS NOT NULL AND I.LastPasswordChangeDate IS NULL))
	OR (D.PasswordFormat <> I.PasswordFormat
			OR	(D.PasswordFormat IS NULL AND I.PasswordFormat IS NOT NULL)
			OR	(D.PasswordFormat IS NOT NULL AND I.PasswordFormat IS NULL))
	OR (D.PasswordSalt <> I.PasswordSalt
			OR	(D.PasswordSalt IS NULL AND I.PasswordSalt IS NOT NULL)
			OR	(D.PasswordSalt IS NOT NULL AND I.PasswordSalt IS NULL))
	OR (D.Comments <> I.Comments
			OR	(D.Comments IS NULL AND I.Comments IS NOT NULL)
			OR	(D.Comments IS NOT NULL AND I.Comments IS NULL))
	OR (D.FailedPasswordAttemptCount <> I.FailedPasswordAttemptCount
			OR	(D.FailedPasswordAttemptCount IS NULL AND I.FailedPasswordAttemptCount IS NOT NULL)
			OR	(D.FailedPasswordAttemptCount IS NOT NULL AND I.FailedPasswordAttemptCount IS NULL))
	OR (D.FailedPasswordAttemptWindowStart <> I.FailedPasswordAttemptWindowStart
			OR	(D.FailedPasswordAttemptWindowStart IS NULL AND I.FailedPasswordAttemptWindowStart IS NOT NULL)
			OR	(D.FailedPasswordAttemptWindowStart IS NOT NULL AND I.FailedPasswordAttemptWindowStart IS NULL))
	OR (D.FailedPasswordAnswerAttemptCount <> I.FailedPasswordAnswerAttemptCount
			OR	(D.FailedPasswordAnswerAttemptCount IS NULL AND I.FailedPasswordAnswerAttemptCount IS NOT NULL)
			OR	(D.FailedPasswordAnswerAttemptCount IS NOT NULL AND I.FailedPasswordAnswerAttemptCount IS NULL))
	OR (D.FailedPasswordAnswerAttemptWindowStart <> I.FailedPasswordAnswerAttemptWindowStart
			OR	(D.FailedPasswordAnswerAttemptWindowStart IS NULL AND I.FailedPasswordAnswerAttemptWindowStart IS NOT NULL)
			OR	(D.FailedPasswordAnswerAttemptWindowStart IS NOT NULL AND I.FailedPasswordAnswerAttemptWindowStart IS NULL))
	OR (D.FirstName <> I.FirstName
			OR	(D.FirstName IS NULL AND I.FirstName IS NOT NULL)
			OR	(D.FirstName IS NOT NULL AND I.FirstName IS NULL))
	OR (D.MiddleName <> I.MiddleName
			OR	(D.MiddleName IS NULL AND I.MiddleName IS NOT NULL)
			OR	(D.MiddleName IS NOT NULL AND I.MiddleName IS NULL))
	OR (D.LastName <> I.LastName
			OR	(D.LastName IS NULL AND I.LastName IS NOT NULL)
			OR	(D.LastName IS NOT NULL AND I.LastName IS NULL))
	OR (D.PhoneNumber <> I.PhoneNumber
			OR	(D.PhoneNumber IS NULL AND I.PhoneNumber IS NOT NULL)
			OR	(D.PhoneNumber IS NOT NULL AND I.PhoneNumber IS NULL))
	OR (D.FaxNumber <> I.FaxNumber
			OR	(D.FaxNumber IS NULL AND I.FaxNumber IS NOT NULL)
			OR	(D.FaxNumber IS NOT NULL AND I.FaxNumber IS NULL))
	OR (D.OtherPhoneNumber <> I.OtherPhoneNumber
			OR	(D.OtherPhoneNumber IS NULL AND I.OtherPhoneNumber IS NOT NULL)
			OR	(D.OtherPhoneNumber IS NOT NULL AND I.OtherPhoneNumber IS NULL))
	
		-- Update LastUpdateDate if not explicitly set
		IF NOT UPDATE(LastUpdateDate)
		BEGIN
			UPDATE dbo.SBUser
			SET LastUpdateDate = GETDATE()
			FROM Inserted I
			INNER JOIN dbo.SBUser A 
			ON (I.SBUserID = A.SBUserID)
		END
	END
	

"
				);
		}
	}
}
