using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(186)]
    public class AddApplicationWorkTypeRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationWorkTypeRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBWorkTypeLUT"; }
        }
    }
}