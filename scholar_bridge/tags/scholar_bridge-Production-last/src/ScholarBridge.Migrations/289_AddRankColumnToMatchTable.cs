using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(289)]
	public class AddRankColumnToMatchTable : Migration
    {
		private const string TABLE_NAME = "SBMatch";
		private const string COLUMN_NAME = "Rank";

		public override void Up()
		{
			Database.AddColumn(TABLE_NAME, COLUMN_NAME, DbType.Decimal, 8, ColumnProperty.NotNull, "0");
			ComputeInitialRanks();
        }

		public override void Down()
		{
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
		}

		private void ComputeInitialRanks()
		{
			Database.ExecuteNonQuery(@"
with percentages (matchid, minimumPercentage, preferredPercentage)
as
(
	select sbmatchid,
			minimumPercentage = case when ScholarshipMinimumCriteriaCount = 0 
									then 1.000 
									else cast(SeekerMinimumCriteriaCount as decimal(8,3)) /  
										cast(ScholarshipMinimumCriteriaCount as decimal(8,3)) 
								end ,
			preferredPercentage = case when ScholarshipPreferredCriteriaCount = 0 
									then 1.000 
									else cast(SeekerPreferredCriteriaCount as decimal(8,3)) /  
										cast(ScholarshipPreferredCriteriaCount as decimal(8,3)) 
								end 
	from SBMatch
)
update SBMatch
set [Rank] = round((select 33.0 * preferredPercentage + 67.0 * minimumPercentage 
					from percentages where percentages.matchid = SBMatch.SBMatchId), 2)
				
");
		}

    }
}