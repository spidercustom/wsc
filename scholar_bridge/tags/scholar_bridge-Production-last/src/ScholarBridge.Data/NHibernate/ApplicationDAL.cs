using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class ApplicationDAL : AbstractDAL<Application>, IApplicationDAL
    {
        public IList<Application> FindAllSubmitted(Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
                .List<Application>();
        }

        public IList<Application> FindAllFinalists(Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Finalist", true))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
                .List<Application>();
        }
       

		/// <summary>
		/// Count all Submitted applications site-wide for active scholarships
		/// </summary>
		/// <returns></returns>
		public int CountAllSubmitted()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

		/// <summary>
		/// Count all Submitted applications site-wide for active scholarships
		/// </summary>
		/// <returns></returns>
		public int CountAllStartedButNotSubmitted()
		{
			return CreateCriteria()
				.Add(Restrictions.Not(Restrictions.Eq("Stage", ApplicationStage.Submitted)))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

        public int CountAllSubmittedBySeeker(Seeker seeker)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker ))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted))
                .SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }

        public int CountAllHavingAttachment(Attachment attachment)
        {
            var sql = @"select count(*) as attachmentcount from SBApplicationAttachmentRT where SBApplicationAttachmentRT.SBAttachmentID in 
            (select SBAttachment.SBAttachmentId from SBAttachment where SBAttachment.UniqueName ='{0}') ".Build(attachment.UniqueName);
            
			ISQLQuery query = Session.CreateSQLQuery(sql).AddScalar("attachmentcount", NHibernateUtil.Int32);
    	    return  (int) query.UniqueResult();
    	 
        }
         

        public IList<Application> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .List<Application>();
        }

        public IList<Application> SearchSubmittedBySeeker(Scholarship scholarship, string lastName)
        {
            var crit= CreateCriteria()
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.Eq("Stage", ApplicationStage.Submitted));

             var crit1=crit.CreateCriteria("Seeker");
            return crit1.CreateCriteria("User")
                .Add(Restrictions.Like("Name.LastName", string.Format("%{0}%", lastName)))
                .List<Application>();
        }

        public Application FindBySeekerandScholarship(Seeker seeker,Scholarship scholarship)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .UniqueResult<Application>();
        }
    }
}