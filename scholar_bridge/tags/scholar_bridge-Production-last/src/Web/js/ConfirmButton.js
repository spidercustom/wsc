﻿function ConfirmYesNoDialogContinue(controlId) {
    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlId, 'True', false, '', '', false, true));
}

function ConfirmYesNoDialog(divId, controlId) {
    $("#" + divId).dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        buttons: {
            "No": function() {
                $(this).dialog("destroy");
            },
            "Yes": function() {
                $(this).dialog("destroy");
                ConfirmYesNoDialogContinue(controlId);
            }

        }
        ,
        close: function() {
                $(this).dialog("destroy");
                
            }
    });
}