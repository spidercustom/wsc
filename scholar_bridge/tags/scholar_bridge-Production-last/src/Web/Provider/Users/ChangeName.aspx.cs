﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Provider.Users
{
    public partial class ChangeName : Page
    {
        public IUserContext UserContext { get; set; }
        public IProviderService ProviderService { get; set; }

        public int UserId { get { return Int32.Parse(Request["id"]); } }
        public User CurrentUser { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            CurrentUser = ProviderService.FindUserInOrg(UserContext.CurrentProvider, UserId);
            editUserName.CurrentUser = CurrentUser;
        }

        protected void editUserName_OnUserSaved(User user)
        {
            SuccessMessageLabel.SetMessage("User's name has been changed.");
            PopupHelper.CloseSelf(false);
            PopupHelper.RefreshParent(ResolveUrl("~/Provider/Admin/#user-tab"));
        }

        protected void editUserName_OnFormCanceled()
        {
            PopupHelper.CloseSelf(false );
        }
    }
}
