﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Anonymous.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Anonymous" Title="Seeker Home" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>
<%@ Register Src="~/Common/Login.ascx" TagName="Login" TagPrefix="sb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Seeker Home" />
    
    <script type="text/javascript">
        var LoginUrl = '<%= LinkGenerator.GetFullLink("default.aspx")  %>'
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Body" Runat="Server">
    <div id="LoginForm"><sb:Login ID="loginForm" runat="server" /></div>
    <h2><img alt="Smarter Scholarship Matches" src="<%= ResolveUrl("~/images/titles/smarter_scholarship_matches.png") %>" width="332px" height="22px"/></h2>
    <p class="hookline">theWashBoard.org is your free online resource for finding scholarships that match your needs. Create a profile and let us do the rest.</p>
    
    <br class="clear" />
    <hr style="margin:20px 0px;" />
    
    <div class="three-column">
        <div>
            <a href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">
                <img alt="Register" src="<%= ResolveUrl("~/images/BottomBox01_StepOne.gif") %>"/>
            </a>
            <a href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">
                <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox01.gif") %>"/>
            </a>
            <p>It's easy! Enter your email address and create a password and we will send you a confirmation email. Click the confirmation link and you are ready to log in!</p>
            <a class="circle-icon-link" href="<%= LinkGenerator.GetFullLink("/Seeker/Register.aspx") %>">Register</a>
        </div>
        <div>
            <img alt="Create your profile" src="<%= ResolveUrl("~/images/BottomBox02_StepTwo.gif") %>"/>
            <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox02.gif") %>"/>
            <p>Create a profile and let us do the rest. We will match you with scholarships you are most likely to qualify for.</p>
        </div>
        <div>
            <img alt="Review your matches" src="<%= ResolveUrl("~/images/BottomBox03_StepThree.gif") %>" />
            <img alt="" src="<%= ResolveUrl("~/images/SeekerBottomBox03.gif") %>" />
            <p>Your scholarship matches are ranked based on the number of eligibility criteria that match your profile. Start reviewing scholarships you are most likely to qualify for.</p>
        </div>
    </div>
    
   <sb:SuccessMessageLabel ID="SuccessMessageLabel" runat="server"></sb:SuccessMessageLabel>  
</asp:Content>
