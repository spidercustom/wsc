﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceList.ascx.cs" Inherits="ScholarBridge.Web.Admin.Editors.Resources.ResourceList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register src="~/Admin/Editors/Resources/ResourceEntry.ascx" tagname="ResourceEntry" tagprefix="sb" %>

<h3>Resource Editor</h3>

<asp:panel ID='resourceListPanel' runat="server">

    <div>
        <asp:ListView ID="lstResources" runat="server" 
            OnItemDataBound="lstResources_ItemDataBound" 
            onpagepropertieschanging="lstResources_PagePropertiesChanging" >
            <LayoutTemplate>
             <br />
                 <sbCommon:AnchorButton CausesValidation="false" ID="createResourceLnkTop" runat="server" OnClick="createResourceLnk_Click" Text="Add Resource" /> 
     
            <br />
            <table class="sortableTable">
                    <thead>
                    <tr>
                        <th>Select</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Title</th>
                        <th>URL/FileName</th>
                        <th>Order</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder id="itemPlaceholder" runat="server" />
                </tbody>
            </table>
            
            </LayoutTemplate>
                  
            <ItemTemplate>
            <tr class="row">
                <td><asp:CheckBox ID="chkResource" runat="server" /></td>
                <td><sbCommon:AnchorButton ID="SingleEditBtn" CausesValidation="false" Text="Edit" runat="server" onclick="SingleEditBtn_Click" /></td>
                <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" CausesValidation="false" ConfirmMessageDivID ="deleteConfirmDiv" 
                    Text="Delete"  runat="server" OnClickConfirm="SingleDeleteBtn_Click"   Width="60px" /></td>
                <td><asp:HyperLink id="linktoResource" Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
                <td><%# Eval("URL") == null ? Eval("AttachedFile.Name") : (string)Eval("URL") == string.Empty ? Eval("AttachedFile.Name") : Eval("URL")%></td>
                <td><%# Eval("DisplayOrder") %></td>
            </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
            <tr class="altrow">
                <td><asp:CheckBox ID="chkResource" runat="server" /></td>
                <td><sbCommon:AnchorButton ID="SingleEditBtn" CausesValidation="false"  Text="Edit" runat="server" onclick="SingleEditBtn_Click" /></td>
                <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" CausesValidation="false"  ConfirmMessageDivID ="deleteConfirmDiv" 
                    Text="Delete"  runat="server" OnClickConfirm="SingleDeleteBtn_Click" Width="60px"  /></td>
                <td><asp:HyperLink id="linktoResource"  Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Title") %></asp:HyperLink></td>
                <td><%# Eval("URL") == null ? Eval("AttachedFile.Name") : (string)Eval("URL") == string.Empty ? Eval("AttachedFile.Name") : Eval("URL")%></td>
                <td><%# Eval("DisplayOrder") %></td>
            </tr>
            </AlternatingItemTemplate>
            <EmptyDataTemplate>
            <p>There are noResources, </p>
            
                <sbCommon:AnchorButton ID="createResourceLnk" runat="server" OnClick="createResourceLnk_Click" Text="Add Resource" /> 
     
            </EmptyDataTemplate>
        </asp:ListView>

        <div class="pager">
            <asp:DataPager runat="server" ID="pager" 
                PagedControlID="lstResources" 
                PageSize="50" 
                onprerender="pager_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                        ShowNextPageButton="false" ShowLastPageButton="false" />
                    <sbCommon:CustomNumericPagerField
                        CurrentPageLabelCssClass="pagerlabel"
                        NextPreviousButtonCssClass="pagerlink"
                        PagingPageLabelCssClass="pagingPageLabel"
                        NumericButtonCssClass="pagerlink"/>
                    <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                        ShowNextPageButton="true" ShowLastPageButton="false" />
                </Fields>
            </asp:DataPager>
            <br />
        </div> 
        <div>
            <sbCommon:ConfirmButton ID="DeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
                Text="Delete Selected"  runat="server" OnClickConfirm="DeleteBtn_Click" Width="130px" />
        </div>
        <div id="deleteConfirmDiv" title="Confirm Delete" style="display:none">
                Are you sure want to delete?
        </div> 
        <asp:ScriptManager ID="scriptManager" runat="server" ></asp:ScriptManager>  
     
     </div>
 
</asp:panel>

<asp:panel ID="addUpdatePanel" runat="server" Visible="false">

    <sb:ResourceEntry ID="resourceEntry" 
                    runat="server"  
                    OnFormSaved="ResourceEntry1_OnFormSaved" 
                    OnFormCanceled ="ResourceEntry1_OnFormCanceled" />
    
</asp:panel>
