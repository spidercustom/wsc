﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs" Inherits="ScholarBridge.Web.Common.UserDetails" %>

<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>

<asp:Image ID="personImage" runat="server" ImageUrl="~/images/person.png" class="profileImage"/>
<br />
 <br />

  <asp:Label ID="nameLbl" runat="server" /><br />
    <asp:Label ID="emailAddressLbl" runat="server" />
     <br />
      
     <asp:Label ID="Label1" runat="server" Text="Phone:" />
     <asp:Literal ID="phoneLbl" runat="server" />
    <br />
     
     <asp:Label ID="Label2" runat="server" Text="Fax: " />
    <asp:Literal ID="faxLbl" runat="server" />
    
    <br />
 
     <asp:Label ID="Label3" runat="server" Text="Other Phone:" />
       <asp:Literal ID="otherPhoneLbl" runat="server" />
    <br />
    
     <asp:Label ID="Label4" runat="server" Text="Member Since:" />
     <asp:Literal ID="memberSinceLbl" runat="server" />
     <br />
      
     <asp:Label ID="Label5" runat="server" Text="Last Login: " />
     <asp:Literal ID="lastLoginLbl" runat="server" />
           

