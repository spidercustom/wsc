﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ScholarBridge.Web.Common.Login" %>
<div id="Login">
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login ID="Login1" runat="server" onloggedin="Login1_LoggedIn" onloginerror="Login1_LoginError">
     <LayoutTemplate>
        <div id="ValidationErrorPopup" style="display: none">
            <asp:Image ID="ValidationErrorPopupimage" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/cooltipicon_errorbig.png"/> 
            <asp:Literal ID="FailureDialogText" runat="server"> Invalid email or password. Please try again.</asp:Literal>
        </div>
        <asp:panel ID="loginPanel" DefaultButton="Login" runat="server">
            <h2>Sign In <span id="or-create-account" style="font-size:12px;">or <asp:HyperLink id="registerlink" runat="server" Text="Register Today!" /></span></h2>
            <asp:Textbox id="UserName" text="Email Address" runat="server" />
            <asp:TextBox TextMode="Password"  id="Password" text="password" runat="server" />
            <asp:Button Cssclass="button" ID="Login" CommandName="Login" runat="server" />
            <br />
            <asp:HyperLink id="forgotPassword" runat="server" Text="Forgot your password?" NavigateUrl="#" />
        </asp:panel>
     </LayoutTemplate>
    </asp:Login>
    </AnonymousTemplate>    
</asp:LoginView>
</div>