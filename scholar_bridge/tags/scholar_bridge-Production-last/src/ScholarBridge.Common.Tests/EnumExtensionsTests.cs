﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class EnumExtensionsTests
    {
        public enum test_enum_1
        {
            element1, 
            element2
        }

        public enum test_enum_2
        {
            element1, 
            [DisplayName("Element2")]
            element2
        }

        public enum test_enum_3
        {
            [DisplayName("Element1")]
            element1,
            [DisplayName("Element2")]
            element2
        }

        [Test] 
        public void get_dispaynames_for_enum_having_some_display_name_attribute()
        {
            var e1 = test_enum_2.element1;
            Assert.AreEqual(1, e1.GetDisplayNames().Length);
            Assert.AreEqual("element1", e1.GetDisplayNames()[0]);

            var e2 = test_enum_2.element1 | test_enum_2.element2;
            Assert.AreEqual(2, e2.GetDisplayNames().Length);
            Assert.AreEqual("Element2", e2.GetDisplayNames()[1]);
        }


        [Test]
        public void get_display_name_from_enum_object_without_display_name_attribute()
        {
            test_enum_1 enumObject = test_enum_1.element1;
            Assert.AreEqual("element1", enumObject.GetDisplayName());
        }

        [Test]
        public void get_display_name_from_enum_object_with_display_name_attribute()
        {
            test_enum_2 enumObject = test_enum_2.element2;
            Assert.AreEqual("Element2", enumObject.GetDisplayName());
        }

        public enum IsInTestEnum
        {
            Element1,
            Element2,
            Element3
        }

        [Test]
        public void test_is_in_return_true()
        {
            var find = IsInTestEnum.Element3;
            Assert.IsTrue(find.IsIn(IsInTestEnum.Element1, IsInTestEnum.Element3));
        }
    
        [Test]
        public void test_is_in_return_false()
        {
            var find = IsInTestEnum.Element3;
            Assert.IsFalse(find.IsIn(IsInTestEnum.Element1, IsInTestEnum.Element2));
        }

        [Test]
        public void can_get_enum_as_dictionary()
        {
            Dictionary<Int32, String> dictionary = typeof (test_enum_3).GetKeyValue();
            Assert.AreEqual(2, dictionary.Keys.Count);
            Assert.AreEqual(dictionary[0], "Element1");
            Assert.AreEqual(dictionary[1], "Element2");
        }

        [Test]
        public void can_get_enum_as_dictionary_with_exclusion()
        {
            Dictionary<Int32, String> dictionary = typeof(test_enum_3).GetKeyValue(new[] { 1 });
            Assert.AreEqual(1, dictionary.Keys.Count);
        }

        [Test]
        public void can_get_numeric_value()
        {
            Assert.AreEqual(0, test_enum_3.element1.GetNumericValue());
            Assert.AreEqual("0", test_enum_3.element1.NumericValueString());
        }

        [Test]
        public void can_get_key_values()
        {
            var dictionary = EnumExtensions.GetKeyValue(test_enum_3.element1, test_enum_3.element2);
            Assert.AreEqual(dictionary[0], "Element1");
            Assert.AreEqual(dictionary[1], "Element2");
        }


        [Flags]
        public enum flag_enum_display_name
        {
            [DisplayName("Element1")]
            element1,
            [DisplayName("Element2")]
            element2
        }


        [Test]
        public void can_get_display_names()
        {
            flag_enum_display_name value = flag_enum_display_name.element1;
            string[] names = value.GetDisplayNames();
            Assert.AreEqual(1, names.Length);
            Assert.AreEqual("Element1", names[0]);

            value |= flag_enum_display_name.element2;
            names = value.GetDisplayNames();
            Assert.AreEqual(2, names.Length);
            Assert.AreEqual("Element1", names[0]);
            Assert.AreEqual("Element2", names[1]);
        }

        [Test]
        public void can_get_display_names_joined()
        {
            flag_enum_display_name value = flag_enum_display_name.element1;
            string names = value.GetDisplayNames(", ");
            Assert.AreEqual("Element1", names);

            value |= flag_enum_display_name.element2;
            names = value.GetDisplayNames(", ");
            Assert.AreEqual("Element1, Element2", names);
        }
    }
}
