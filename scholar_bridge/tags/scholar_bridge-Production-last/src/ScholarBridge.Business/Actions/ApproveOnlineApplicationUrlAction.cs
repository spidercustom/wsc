using System;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public class ApproveOnlineApplicationUrlAction : IAction
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IMessageService MessageService { get; set; }
        public string LastStatus { get; set; }
        public bool SupportsApprove { get { return true; } }

        public bool SupportsReject { get { return true; } }

        public void Approve(Message message, MailTemplateParams templateParams, User approver)
        {
            if (! (message is  ScholarshipMessage))
                throw new ArgumentException("Message must be an ScholarshipMessage");

            var scholarship = ((ScholarshipMessage )message).RelatedScholarship;

            scholarship.LastUpdate = new ActivityStamp(approver);
            ScholarshipService.ApproveOnlineApplicationUrl(scholarship,approver,message.From.User );

            message.ActionTaken = MessageAction.Approve;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Scholarship Online Application Url Approved";
        }

        public void Reject(Message message, MailTemplateParams templateParams, User approver)
        {
            if (!(message is ScholarshipMessage))
                throw new ArgumentException("Message must be an ScholarshipMessage");

            var scholarship = ((ScholarshipMessage)message).RelatedScholarship;

            scholarship.LastUpdate = new ActivityStamp(approver);
            ScholarshipService.RejectOnlineApplicationUrl(scholarship, approver, message.From.User);

            message.ActionTaken = MessageAction.Deny;
            message.LastUpdate = new ActivityStamp(approver);
            MessageService.ArchiveMessage(message);
            LastStatus = "Scholarship Online Application Url Rejected";
        }
    }
}