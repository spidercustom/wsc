﻿using System;
using System.IO;
using System.Net;

namespace ScholarBridge.Business
{
	/// <summary>
	/// for purposes of theWashBoard app, anything that is put on the public web server will go to
	/// the same (only one) server directory.
	/// </summary>
	public class FtpService : IFtpService
	{
		// the following members are set from config through DI
		public bool FTPEnabled { get; set; }
		public string FTPSite { get; set; }
		public string FTPDirectory { get; set; }
		public string FTPLogon { get; set; }
		public string FTPPassword { get; set; }

		public void PutFileToPublicServer(string fileName, string localFilePath)
		{
			FileInfo fileInf = new FileInfo(localFilePath);

			string uri = !FTPSite.StartsWith("ftp://")
				? "ftp://" + FTPSite
				: FTPSite;

			if (!string.IsNullOrEmpty(FTPDirectory))
				uri += "/" + FTPDirectory;

			uri += "/" + fileName;

			// Create FtpWebRequest object from the Uri provided
			FtpWebRequest reqFtp = (FtpWebRequest)WebRequest.Create(new Uri(uri));

			// Provide the WebPermission Credintials
			reqFtp.Credentials = new NetworkCredential(FTPLogon, FTPPassword);

			// By default KeepAlive is true, where the control connection is not closed
			// after a command is executed.
			reqFtp.KeepAlive = false;

			// Specify the command to be executed.
			reqFtp.Method = WebRequestMethods.Ftp.UploadFile;

			// Specify the data transfer type.
			reqFtp.UseBinary = true;

			// Notify the server about the size of the uploaded file
			reqFtp.ContentLength = fileInf.Length;

			// The buffer size is set to 2kb
			const int buffLength = 2048;
			byte[] buff = new byte[buffLength];

			// Opens a file stream (System.IO.FileStream) to read the file to be uploaded
			using (FileStream fs = fileInf.OpenRead())
			{
				// Stream to which the file to be upload is written
				using (Stream strm = reqFtp.GetRequestStream())
				{

					// Read from the file stream 2kb at a time
					int contentLen = fs.Read(buff, 0, buffLength);

					// Till Stream content ends
					while (contentLen != 0)
					{
						// Write Content from the file stream to the FTP Upload Stream
						strm.Write(buff, 0, contentLen);
						contentLen = fs.Read(buff, 0, buffLength);
					}
				}
			}
		}
	}
}
