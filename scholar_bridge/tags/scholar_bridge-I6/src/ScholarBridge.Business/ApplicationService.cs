using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public class ApplicationService : IApplicationService
    {
        public IApplicationDAL ApplicationDAL { get; set; }
        public IMatchService MatchService { get; set; }

        public Application GetById(int id)
        {
            return ApplicationDAL.FindById(id);
        }

        public void SaveNew(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            MatchService.ApplyForMatch(application.Seeker, application.Scholarship.Id, application);
            ApplicationDAL.Insert(application);
        }

        public Application Save(Application application)
        {
            if (application == null)
                throw new ArgumentNullException("application");
            
            return ApplicationDAL.Save(application);
        }
        public void SubmitApplication(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Calculate Min/Pref criteria and populate the object
            application.Submit();
            ApplicationDAL.Update(application);
        }

        public IList<Application> FindAllSubmitted(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.FindAllSubmitted(scholarship);
        }

        public int CountAllSubmitted(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.CountAllSubmitted(scholarship);
        }
        public int CountAllSubmittedBySeeker(Seeker seeker)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            return ApplicationDAL.CountAllSubmittedBySeeker(seeker);
        }

        public IList<Application> FindAllSubmitted(Scholarship scholarship, string lastNameSearch)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.SearchSubmittedBySeeker(scholarship, lastNameSearch);
        }

        public IList<Application> FindAllFinalists(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.FindAllFinalists(scholarship);
        }


        public void Finalist(int applicationId)
        {
            Finalist(ApplicationDAL.FindById(applicationId));
        }

        public void Finalist(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Check if awarded or rejected and don't allow changes after that point?

            if (!application.Finalist)
            {
                application.Finalist = true;
                ApplicationDAL.Update(application);
            }
        }

        public void NotFinalist(int applicationId)
        {
            NotFinalist(ApplicationDAL.FindById(applicationId));
        }

        public void NotFinalist(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Check if awarded or rejected and don't allow changes after that point?

            if (application.Finalist)
            {
                application.Finalist = false;
                ApplicationDAL.Update(application);
            }
        }

        public void Update(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            ApplicationDAL.Update(application);
        }
        public Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship)
        {
            if (null ==seeker)
                throw new ArgumentNullException("seeker");
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");
            return ApplicationDAL.FindBySeekerandScholarship(seeker, scholarship);

        }
        public void Delete(Application application)
        {
            ApplicationDAL.Delete(application);

        }
    }
}