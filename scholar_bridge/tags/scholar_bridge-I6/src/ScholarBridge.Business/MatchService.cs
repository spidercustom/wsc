using System;
using System.Collections.Generic;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public class MatchService : IMatchService
    {
        private static readonly MatchStatus[] SAVED_AND_APPLIED_STATUS = new[] { MatchStatus.Saved, MatchStatus.Applied };
        public IMatchDAL MatchDAL { get; set; }
        public ISeekerDAL SeekerDAL { get; set; }

        public Match GetMatch(Seeker seeker, int scholarshipId)
        {
            return MatchDAL.Find(seeker, scholarshipId);
        }
        
        public void SaveMatch(Seeker seeker, int scholarshipId)
        {
            ChangeStatus(seeker, scholarshipId, MatchStatus.Saved);
        }

        public void UnSaveMatch(Seeker seeker, int scholarshipId)
        {
            ChangeStatus(seeker, scholarshipId, MatchStatus.New);
        }

        public void ApplyForMatch(Seeker seeker, int scholarshipId, Application application)
        {
            var match = GetMatch(seeker, scholarshipId);
            match.Application = application;
            match.MatchStatus = MatchStatus.Applied;
            MatchDAL.Update(match);

            return;
        }

        private void ChangeStatus(Seeker seeker, int scholarshipId, MatchStatus newStatus)
        {
            var match = MatchDAL.Find(seeker, scholarshipId);
            if (null != match)
            {
                match.MatchStatus = newStatus;
                match.LastUpdate = new ActivityStamp(seeker.User);
                MatchDAL.Update(match);
            }
        }

        public IList<Match> GetMatchesForSeeker(Seeker seeker)
        {
            if (! seeker.LastMatch.HasValue || seeker.LastMatch.Value < seeker.LastUpdate.On)
            {
                MatchDAL.UpdateMatches(seeker);
                seeker.LastMatch = DateTime.Now;
                SeekerDAL.Update(seeker);
            }
            return MatchDAL.FindAll(seeker);
        }

        public IList<Match> GetSavedMatches(Seeker seeker)
        {
            return MatchDAL.FindAll(seeker, SAVED_AND_APPLIED_STATUS);
        }
    }
}