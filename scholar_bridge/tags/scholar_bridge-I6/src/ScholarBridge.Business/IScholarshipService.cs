﻿using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IScholarshipService
    {
        Scholarship GetById(int id);
        
          
        Scholarship Save(Scholarship scholarship);
        void Delete(Scholarship scholarship);

        Scholarship ScholarshipExists(Provider provider, string name, int year);
        IList<Scholarship> GetByProvider(Provider provider);
        IList<Scholarship> GetByProvider(Provider provider, ScholarshipStages stage);
        
        IList<Scholarship> GetByProviderNotActivated(Provider provider);
        IList<Scholarship> GetByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages stage);
        IList<Scholarship> GetNotActivatedByOrganizations(Provider provider, Intermediary intermediary);

        IList<Scholarship> GetByIntermediary(Intermediary intermediary);
        IList<Scholarship> GetByIntermediary(Intermediary intermediary, ScholarshipStages stage);
        IList<Scholarship> GetByIntermediaryNotActivated(Intermediary intermediary);
        
        void RequestActivation(Scholarship scholarship);
        void Approve(Scholarship scholarship, User approver);
        void Reject(Scholarship scholarship, User approver);


        Scholarship CopyScholarship(Scholarship copyFrom);
    }
}
