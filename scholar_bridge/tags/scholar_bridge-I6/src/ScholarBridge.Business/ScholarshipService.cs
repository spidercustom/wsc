﻿using System;
using System.Collections.Generic;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class ScholarshipService : IScholarshipService
    {
        private const int MAXIMUM_COPY_NAME_ALLOWED = 10;
        private const string COPY_NAME_TEMPLATE = "Copy ({0}) of {1}";

        public IScholarshipDAL ScholarshipDAL { get; set; }
        public IMatchDAL MatchDal { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }
        public IMessagingService MessagingService { get; set; }
        public IRoleDAL RoleService { get; set; }

        public string AttachmentsDirectory { get; set; }

        public Scholarship GetById(int id)
        {
            return ScholarshipDAL.FindById(id);
        }

        public IList<Scholarship> GetByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            return ScholarshipDAL.FindByProvider(provider);
        }

        public Scholarship Save(Scholarship scholarship)
        {
            if (scholarship == null)
                throw new ArgumentNullException("scholarship");
            if (scholarship.Provider == null)
                throw new ArgumentNullException("scholarship.Provider");
            if (scholarship.Provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.Save(scholarship);
        }

        public void Delete(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");
            if (! scholarship.CanEdit())
                throw new ArgumentException("You can only delete an editable scholarship");

            // Rarely should there be messages sent to a deletable Scholarship, but just in case
            MessagingService.DeleteRelatedMessages(scholarship);
        	foreach (var attachment in scholarship.Attachments)
        	{
                attachment.RemoveFile(AttachmentsDirectory);
        	}
            ScholarshipDAL.Delete(scholarship);
        }

        public Scholarship ScholarshipExists(Provider provider, string name, int year)
        {
            if (null == provider)
                throw new ArgumentNullException("provider");
            if (null == name)
                throw new ArgumentNullException("name");

            return ScholarshipDAL.FindByBusinessKey(provider, name, year);
        }

        public IList<Scholarship> GetByProvider(Provider provider, ScholarshipStages stage)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.FindByProvider(provider, stage);
        }
       

        public IList<Scholarship> GetByProviderNotActivated(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.FindByProviderNotActivated(provider);

        }
        public IList<Scholarship> GetByIntermediary(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");
            if (intermediary.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            return ScholarshipDAL.FindByIntermediary(intermediary);
        }
       
        public IList<Scholarship> GetByIntermediary(Intermediary intermediary, ScholarshipStages stage)
            {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindByIntermediary(intermediary, stage);
        }
        public IList<Scholarship> GetByIntermediaryNotActivated(Intermediary intermediary)
           {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");
            return ScholarshipDAL.FindByIntermediaryNotActivated(intermediary);

        }

        public IList<Scholarship> GetByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages stage)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindByOrganizations(provider, intermediary, stage);
        }

        public IList<Scholarship> GetNotActivatedByOrganizations(Provider provider, Intermediary intermediary)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindNotActivatedByOrganizations(provider, intermediary);
        }

        /// <summary>
        /// Handles a null Intermediary which means the WSC Admin acts as the intermediary
        /// </summary>
        /// <param name="scholarship"></param>
        /// <returns></returns>
        public MessageAddress ActivationHandlerForScholarship(Scholarship scholarship)
        {
            if (null == scholarship.Intermediary)
            {
                return new MessageAddress { Role = RoleService.FindByName(Role.WSC_ADMIN_ROLE) };
            }

            return new MessageAddress {Organization = scholarship.Intermediary};
        }

        public void RequestActivation(Scholarship scholarship)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }
            if (! scholarship.CanSubmitForActivation())
            {
                throw new ArgumentException("Scholarship not ready for submitting.");
            }

            scholarship.Stage = ScholarshipStages.RequestedActivation;
            ScholarshipDAL.Update(scholarship);

            // Send message to scholarship provider that the scholarship has been approved.
            var templateParams = new MailTemplateParams();
            TemplateParametersService.RequestScholarshipActivation(scholarship, templateParams);
            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.RequestScholarshipActivation,
                              RelatedScholarship = scholarship,
                              To = ActivationHandlerForScholarship(scholarship),
                              From = new MessageAddress { Organization = scholarship.Provider },
                              LastUpdate = scholarship.LastUpdate
                           };
            MessagingService.SendMessage(msg, templateParams, true);
        }

        public void Approve(Scholarship scholarship, User approver)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }
            if (scholarship.Stage != ScholarshipStages.RequestedActivation)
            {
                throw new ArgumentException("Scholarship must be in the RequestedActivation stage.");
            }

            scholarship.Stage = ScholarshipStages.Activated;
            scholarship.ActivatedOn = DateTime.Now;
            ScholarshipDAL.Update(scholarship);
            MatchDal.UpdateMatches(scholarship);

            // Send message to scholarship provider that the scholarship has been approved.
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipApproved(scholarship, templateParams);
            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.ScholarshipApproved,
                              RelatedScholarship = scholarship,
                              To = new MessageAddress { Organization = scholarship.Provider },
                              From = ActivationHandlerForScholarship(scholarship),
                              LastUpdate = new ActivityStamp(approver)
                          };
            MessagingService.SendMessage(msg, templateParams, true);
        }

        public void Reject(Scholarship scholarship, User approver)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }
            if (scholarship.Stage != ScholarshipStages.RequestedActivation)
            {
                throw new ArgumentException("Scholarship must be in the RequestedActivation stage.");
            }

            scholarship.Stage = ScholarshipStages.Rejected;
            ScholarshipDAL.Update(scholarship);

            // Send message to scholarship provider that the scholarship has been rejected.
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipRejected(scholarship, templateParams);
            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.ScholarshipRejected,
                              RelatedScholarship = scholarship,
                              To = new MessageAddress { Organization = scholarship.Provider },
                              From = ActivationHandlerForScholarship(scholarship),
                              LastUpdate = new ActivityStamp(approver)
                          };
            MessagingService.SendMessage(msg, templateParams, true);
        }

        public Scholarship CopyScholarship(Scholarship copyFrom)
        {
            var copiedScholarship = (Scholarship) copyFrom.Clone(AttachmentsDirectory);
            for (int copyIndex = 1; copyIndex < MAXIMUM_COPY_NAME_ALLOWED; copyIndex ++)
            {
                var newName = COPY_NAME_TEMPLATE.Build(copyIndex, copiedScholarship.Name);
                var foundScholarship = ScholarshipExists(copiedScholarship.Provider, newName, copiedScholarship.AcademicYear.Year);
                bool copyNameExists = null != foundScholarship;
                if (!copyNameExists)
                {
                    copiedScholarship.Name = newName;
                    return copiedScholarship;
                }
            }

            throw new TooManyScholarshipCopyExistsException();
        }
    }
}
