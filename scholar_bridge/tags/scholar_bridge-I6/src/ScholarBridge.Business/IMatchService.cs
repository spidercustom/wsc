using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface IMatchService
    {
        Match GetMatch(Seeker seeker, int scholarshipId);

        void SaveMatch(Seeker seeker, int scholarshipId);
        void UnSaveMatch(Seeker seeker, int scholarshipId);

        IList<Match> GetMatchesForSeeker(Seeker seeker);

        IList<Match> GetSavedMatches(Seeker seeker);

        void ApplyForMatch(Seeker seeker, int scholarshipId, Application application);
    }
}