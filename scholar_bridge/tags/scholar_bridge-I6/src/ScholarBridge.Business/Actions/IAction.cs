using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Actions
{
    public interface IAction
    {
        bool SupportsApprove { get; }

        bool SupportsReject { get; }

        void Approve(Message message, MailTemplateParams templateParams, User approver);

        void Reject(Message message, MailTemplateParams templateParams, User approver); 
    }
}