﻿using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Validation;

namespace ScholarBridge.Common.Validation
{
    public class ListIsNotEmptyValidation : Validator<ICollection>
    {
        public ListIsNotEmptyValidation() : base(null, null) {}

        public ListIsNotEmptyValidation(string messageTemplate, string tag) : base(messageTemplate, tag)
        {
        }

        protected override string DefaultMessageTemplate
        {
            get { return "Specify at least one"; }
        }

        protected override void DoValidate(ICollection objectToValidate, object currentTarget, string key, ValidationResults validationResults)
        {
            var isEmpty = null == objectToValidate || 0 == objectToValidate.Count;
            if (isEmpty)
            {
                LogValidationResult(validationResults, string.Format(MessageTemplate, key), currentTarget,
                                    key);
            }
        }
    }
}
