﻿using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Tests.Wizards
{
    [TestFixture]
    public class WizardStepContainerCommonTests
    {
        private MockRepository mocks;
        private IWizardStepsContainer<Scholarship> container;
        private IWizardStepControl<Scholarship>[] dummySteps;
        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            container = mocks.StrictMock<IWizardStepsContainer<Scholarship>>();
            var dummyStep = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            dummySteps = new[] {dummyStep, dummyStep, dummyStep};
        }

        [Test]
        public void test_go_prior_from_step_index_one()
        {
            Expect.Call(container.ActiveStepIndex).Return(1);
            Expect.Call(() => container.Goto(0));
            mocks.ReplayAll();

            WizardStepContainerCommon.GoPrior(container);

        }

        [Test]
        public void test_go_prior_from_step_index_zero()
        {
            Expect.Call(container.ActiveStepIndex).Return(0);
            Expect.Call(() => container.Goto(0));
            mocks.ReplayAll();

            WizardStepContainerCommon.GoPrior(container);
        }

        [Test]
        public void test_go_next_from_step_index_last()
        {
            Expect.Call(container.Steps).Return(dummySteps);
            Expect.Call(container.ActiveStepIndex).Return(2);
            Expect.Call(() => container.Goto(2));
            mocks.ReplayAll();

            WizardStepContainerCommon.GoNext(container);
        }

        [Test]
        public void test_go_next_from_step_index_last_minus_one()
        {
            Expect.Call(container.Steps).Return(dummySteps);
            Expect.Call(container.ActiveStepIndex).Return(1);
            Expect.Call(() => container.Goto(2));
            mocks.ReplayAll();

            WizardStepContainerCommon.GoNext(container);
        }

        [Test]
        public void test_goto_with_index_in_range()
        {
            Expect.Call(container.Steps).Return(dummySteps);
            Expect.Call(container.ActiveStepIndex).SetPropertyWithArgument(0);

            Expect.Call(container.Steps).Return(dummySteps);
            Expect.Call(container.ActiveStepIndex).SetPropertyWithArgument(1);

            Expect.Call(container.Steps).Return(dummySteps);
            Expect.Call(container.ActiveStepIndex).SetPropertyWithArgument(2);
            mocks.ReplayAll();

            WizardStepContainerCommon.Goto(container, 0);
            WizardStepContainerCommon.Goto(container, 1);
            WizardStepContainerCommon.Goto(container, 2);
        }

        [Test]
        public void test_goto_with_outbound_index()
        {
            Expect.Call(container.Steps).Return(dummySteps);
            Expect.Call(container.ActiveStepIndex).SetPropertyWithArgument(0);

            Expect.Call(container.Steps).Return(dummySteps);
            Expect.Call(container.ActiveStepIndex).SetPropertyWithArgument(2);

            mocks.ReplayAll();

            WizardStepContainerCommon.Goto(container, -1);
            WizardStepContainerCommon.Goto(container, 3);
        }
        
        [Test]
        public void test_step_activated_notification_call()
        {
            var stepUnderTest = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            Expect.Call(container.Steps).Return(new[] {stepUnderTest});
            Expect.Call(container.ActiveStepIndex).Return(0);
            Expect.Call(stepUnderTest.Activated);

            mocks.ReplayAll();

            WizardStepContainerCommon.NotifyStepActivated(container);
        }

        [Test]
        public void test_step_validation_call()
        {
            var stepUnderTest = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            Expect.Call(container.Steps).Return(new[] { stepUnderTest });
            Expect.Call(container.ActiveStepIndex).Return(0);
            Expect.Call(stepUnderTest.ValidateStep()).Return(false);

            mocks.ReplayAll();

            WizardStepContainerCommon.ValidateStep(container);
        }
        [Test]
        public void test_step_save_call()
        {
            var stepUnderTest = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            Expect.Call(container.Steps).Return(new[] { stepUnderTest });
            Expect.Call(container.ActiveStepIndex).Return(0);
            Expect.Call(stepUnderTest.Save);

            mocks.ReplayAll();

            WizardStepContainerCommon.SaveActiveStep(container);
        }

        [Test]
        public void test_step_populate_objects()
        {
            var stepUnderTest = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            Expect.Call(container.Steps).Return(new[] { stepUnderTest });
            Expect.Call(container.ActiveStepIndex).Return(0);
            Expect.Call(stepUnderTest.PopulateObjects);

            mocks.ReplayAll();

            WizardStepContainerCommon.PopulateObjectsFromActiveStep(container);
        }

        [Test]
        public void test_resume_from_case_resume_from_first_step()
        {
            var stepUnderTest1 = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            var stepUnderTest2 = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            var steps = new[] { stepUnderTest1, stepUnderTest2 };
            var scholarship = new Scholarship();

            Expect.Call(container.Steps).Return(steps);
            Expect.Call(container.GetDomainObject()).Return(scholarship);

            Expect.Call(stepUnderTest1.WasSuspendedFrom(scholarship)).Return(true);
            Expect.Call(() => container.Goto(0));

            mocks.ReplayAll();

            WizardStepContainerCommon.ResumeWizard(container);
        }


        [Test]
        public void test_resume_from_case_resume_from_second_step()
        {
            var stepUnderTest1 = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            var stepUnderTest2 = mocks.StrictMock<IWizardStepControl<Scholarship>>();
            var steps = new[] { stepUnderTest1, stepUnderTest2};
            var scholarship = new Scholarship();
            
            Expect.Call(container.Steps).Return(steps);
            Expect.Call(container.GetDomainObject()).Return(scholarship);

            Expect.Call(stepUnderTest1.WasSuspendedFrom(scholarship)).Return(false);            
            Expect.Call(stepUnderTest2.WasSuspendedFrom(scholarship)).Return(true);

            Expect.Call(() => container.Goto(1));

            mocks.ReplayAll();

            WizardStepContainerCommon.ResumeWizard(container);
        }


        [TearDown]
        public void Teardown()
        {
            container.BackToRecord();
        }
    }
}
