using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using NUnit.Framework;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using Rhino.Mocks;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class ScholarshipTests
    {
		private MockRepository mocks;

		[SetUp]
		public void Setup()
		{
			mocks = new MockRepository();
		}

		[TearDown]
		public void Teardown()
		{
			mocks.VerifyAll();
		}

		[Test]
        public void scholarship_min_value_must_be_greater_than_zero()
        {
            var s = CreateValidScholarship();
            s.MinimumAmount = -1;
            var results = Validation.Validate(s);

            Assert.IsFalse(results.IsValid);
            Assert.AreEqual(1, results.Count);

            s.MinimumAmount = 0;
            var results2 = Validation.Validate(s);
            Assert.IsTrue(results2.IsValid);

        }

        [Test] 
        public void scholarship_max_value_must_be_greater_than_one()
        {
            var s = CreateValidScholarship();
            s.MaximumAmount = 0;
            var results = Validation.Validate(s);

            Assert.IsFalse(results.IsValid);
            Assert.AreEqual(1, results.Count);

            s.MaximumAmount = 1;
            var results2 = Validation.Validate(s);
            Assert.IsTrue(results2.IsValid);
        }

        [Test]
        public void verify_clone_implementation_exists()
        {
            var s = CreateValidScholarship();
            PopulateScholarship(s);

			mocks.ReplayAll();

            var cloned = (Scholarship)s.Clone("c:/temp");
            Assert.AreEqual(0, cloned.Id);
            Assert.AreEqual(AcademicYear.CurrentScholarshipYear.Year, cloned.AcademicYear.Year);
            Assert.AreEqual(0, cloned.CompletedStages.Count);
            Assert.IsNull(cloned.LastUpdate);
            Assert.AreEqual(ScholarshipStages.None, cloned.Stage);
            Assert.IsNotNull(cloned.SeekerProfileCriteria);
            Assert.IsNotNull(cloned.FundingProfile);
        }

        private void PopulateScholarship(Scholarship s)
        {
            s.AdditionalRequirements.Add(new AdditionalRequirement());
            var sq = mocks.StrictMock<ScholarshipQuestion>();
            Expect.Call(sq.Clone()).Return(new ScholarshipQuestion());
            s.AdditionalQuestions.Add(sq);

        	var attachment = mocks.StrictMock<Attachment>();
			Expect.Call(attachment.Clone("c:/temp")).Return(new Attachment());
            s.Attachments.Add(attachment);

            SeekerProfileCriteriaTest.PopulateSeekerProfile(s.SeekerProfileCriteria);
            FundingProfileTests.PopulateFundingProfile(s.FundingProfile);
        }

        [Test]
        public void cant_submit_for_activation_if_no_stage_set()
        {
            var s = new Scholarship();
            Assert.IsFalse(s.CanSubmitForActivation());
        }

        [Test]
        public void can_not_submit_for_activation_if_anystage_is_incomplete()
        {
            var s = new Scholarship { Stage = ScholarshipStages.GeneralInformation };
            s.CompletedStages = new List<ScholarshipStages>();
            s.CompletedStages.Add(ScholarshipStages.GeneralInformation);
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.SeekerProfile;
            s.CompletedStages.Add(ScholarshipStages.SeekerProfile);
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.FundingProfile;
            s.CompletedStages.Add(ScholarshipStages.FundingProfile);
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.PreviewCandidatePopulation;
            s.CompletedStages.Add(ScholarshipStages.PreviewCandidatePopulation);
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.BuildScholarshipMatchLogic;
            s.CompletedStages.Add(ScholarshipStages.BuildScholarshipMatchLogic);
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.BuildScholarshipRenewalCriteria;
            s.CompletedStages.Add(ScholarshipStages.BuildScholarshipRenewalCriteria);
            Assert.IsFalse(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.RequestedActivation;
            Assert.IsFalse(s.CanSubmitForActivation());
        }

        [Test]
        public void can_edit_if_no_stage_set()
        {
            var s = new Scholarship();
            Assert.IsTrue(s.CanEdit());
        }

        [Test]
        public void cant_edit_if_submitted_or_later()
        {
            var s = new Scholarship { Stage = ScholarshipStages.RequestedActivation };
            Assert.IsFalse(s.CanEdit());

            s.Stage = ScholarshipStages.Activated;
            Assert.IsFalse(s.CanEdit());

            s.Stage = ScholarshipStages.Awarded;
            Assert.IsFalse(s.CanEdit());

            s.Stage = ScholarshipStages.Rejected;
            Assert.IsTrue(s.CanEdit());
        }

        private static Scholarship CreateValidScholarship()
        {
            var s = new Scholarship
                        {
                            Name = "Foo",
                            MissionStatement = "Accomplish things",
                            ProgramGuidelines = "Give to people in need",
                            Provider = new Provider { Name = "Foo Provider", ApprovalStatus = ApprovalStatus.Approved },
                            ApplicationStartDate = DateTime.Today,
                            ApplicationDueDate = DateTime.Today.AddMonths(1),
                            AwardDate = DateTime.Today.AddMonths(2),
                            MinimumAmount = 0,
                            MaximumAmount = 100
                        };

            return s;
        }

        [Test]
        public void stage_completion_tests()
        {
            Scholarship scholarship = new Scholarship();
            Assert.IsNotNull(scholarship.CompletedStages);

            scholarship.MarkStageCompletion(ScholarshipStages.GeneralInformation, true);
            Assert.AreEqual(1, scholarship.CompletedStages.Count);
            Assert.AreEqual(ScholarshipStages.GeneralInformation, scholarship.CompletedStages[0]);

            Assert.IsTrue(scholarship.IsStageCompleted(ScholarshipStages.GeneralInformation));
            Assert.IsFalse(scholarship.IsStageCompleted(ScholarshipStages.SeekerProfile));

            scholarship.MarkStageCompletion(ScholarshipStages.GeneralInformation, false);
            Assert.AreEqual(0, scholarship.CompletedStages.Count);
            Assert.IsFalse(scholarship.IsStageCompleted(ScholarshipStages.GeneralInformation));
        }

        [Test]
        public void display_name_with_no_donor_gives_name()
        {
            var s = new Scholarship { Name = "Foo" };
            Assert.AreEqual("Foo", s.DisplayName);
        }

        [Test]
        public void display_name_with_donor_but_no_name_gives_name()
        {
            var s = new Scholarship { Name = "Foo", Donor = new ScholarshipDonor() };
            Assert.AreEqual("Foo", s.DisplayName);
        }

        [Test]
        public void display_name_with_donor_gives_name_with_donor_name()
        {
            var s = new Scholarship { Name = "Foo", Donor = new ScholarshipDonor { Name = new PersonName { FirstName = "First", LastName = "Last" } } };
            Assert.AreEqual("Foo - First Last", s.DisplayName);
        }

        [Test]
        public void can_append_admin_notes_to_scholarship()
        {
            var scholarship = new Scholarship();
            var u = new User { Name = { FirstName = "TestFirst", LastName = "TestLast" } };
            scholarship.AppendAdminNotes(u, "This is a test note.");
            StringAssert.Contains("-- TestFirst TestLast", scholarship.AdminNotes.ToStringTableFormat());
            StringAssert.Contains("This is a test note.", scholarship.AdminNotes.NotesXml);
        }

        [Test]
        public void can_generate_an_amount_string()
        {
            var s = new Scholarship { MinimumAmount = 100.00m, MaximumAmount = 3000.00m };
            Assert.AreEqual("$100 to $3,000", s.AmountRange);
            s.MinimumAmount = 0;
            Assert.AreEqual("$0 to $3,000", s.AmountRange);
        }
    }
}