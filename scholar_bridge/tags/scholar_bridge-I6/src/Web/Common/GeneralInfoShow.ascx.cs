﻿using System;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class GeneralInfoShow : BaseScholarshipShow
    {
        private const string NOT_ACTIVATED_TEXT = "&lt;not activated yet&gt;";
        private const string MONTH_FULL_NAME_FORMAT = "MMMM";
        private const string CURRENCY_FORMAT = "c";
        private const string UNKNOWN_TEXT = "&lt;Unknown&gt;";
        private const string DEFAULT_INTERMEDIARY_NAME = "HSEB";
        public override ScholarshipStages Stage { get { return ScholarshipStages.GeneralInformation; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //setup links
            SetupEditLinks(linkarea1, linkarea2);

            lblName.Text = ScholarshipToView.DisplayName;
            lblAcademicYear.Text = ScholarshipToView.AcademicYear.ToString();

            lblScholarshipActivationDate.Text = ScholarshipToView.ActivatedOn.HasValue
                                                    ? ScholarshipToView.ActivatedOn.Value.ToString("d")
                                                    : NOT_ACTIVATED_TEXT;
            lblMission.Text = ScholarshipToView.MissionStatement;
            lblProgramGuidelines.Text = ScholarshipToView.ProgramGuidelines;
            lblAppStart.Text = ScholarshipToView.ApplicationStartDate.HasValue
                                   ? ScholarshipToView.ApplicationStartDate.Value.ToOrdinalDayString(MONTH_FULL_NAME_FORMAT)
                                   : UNKNOWN_TEXT;
            lblAppDue.Text = ScholarshipToView.ApplicationDueDate.HasValue
                                 ? ScholarshipToView.ApplicationDueDate.Value.ToOrdinalDayString(MONTH_FULL_NAME_FORMAT)
                                 : UNKNOWN_TEXT;
            lblAwardOn.Text = ScholarshipToView.AwardDate.HasValue
                                  ? ScholarshipToView.AwardDate.Value.ToOrdinalDayString(MONTH_FULL_NAME_FORMAT)
                                  : UNKNOWN_TEXT;

            lblAwardAmount.Text = string.Format("{0} - {1}", ScholarshipToView.MinimumAmount.ToString(CURRENCY_FORMAT ),
                                                ScholarshipToView.MaximumAmount.ToString(CURRENCY_FORMAT));
            //view donor info
            lblDonor.Text = GetDonorDisplayString(ScholarshipToView.Donor);
            BindContactInformation();
            Page.DataBind();
        }

        private void BindContactInformation()
        {
            lblProviderName.Text = ScholarshipToView.Provider.Name;
            lblProviderWebSite.Text = ScholarshipToView.Provider.Website;
            lblAddressLine1.Text = ScholarshipToView.Provider.Address.Street;
            lblAddressLine2.Text = ScholarshipToView.Provider.Address.Street2;
            lblCity.Text = ScholarshipToView.Provider.Address.City;
            lblState.Text = ScholarshipToView.Provider.Address.State.Abbreviation;
            lblPostCode.Text = ScholarshipToView.Provider.Address.PostalCode;
            if (null != ScholarshipToView.Provider.Phone)
                lblPhone.Text = ScholarshipToView.Provider.Phone.FormattedPhoneNumber;
            if (null == ScholarshipToView.Intermediary)
                lblIntermediaryName.Text = DEFAULT_INTERMEDIARY_NAME;
            else
                lblIntermediaryName.Text = ScholarshipToView.Intermediary.Name;
        }

        private static string GetDonorDisplayString(ScholarshipDonor donor)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("{0}, {1}", donor.Name, donor.Address);
            
            if (null != donor.Phone && !string.IsNullOrEmpty(donor.Phone.Number))
                sb.AppendFormat(" Phone : {0}", donor.Phone.Number);

            if (!string.IsNullOrEmpty(donor.Email))
                sb.AppendFormat(" Email : {0}", donor.Email);
            return sb.ToString();
        }

        protected override void SetupEditLinks(HtmlGenericControl linkarea1, HtmlGenericControl linkarea2)
        {
            base.SetupEditLinks(linkarea1, linkarea2);

            editProgramGuidelinesLnk.NavigateUrl = "~/Provider/Scholarships/EditProgramGuidelines.aspx?id=" + ScholarshipToView.Id;
            EditControlContainer.Visible = CanEditGuidelines();
        }

        private bool CanEditGuidelines()
        {
            return !ScholarshipToView.CanEdit() &&
                   (Roles.IsUserInRole(Role.PROVIDER_ROLE) || Roles.IsUserInRole(Role.INTERMEDIARY_ROLE));
        }
    }
}
