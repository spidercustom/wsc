﻿<%@ Import Namespace="ScholarBridge.Web.Extensions"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.GeneralInfoShow" %>

<div id="recordinfo">
<h3>General Information</h3>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>

<table class="viewonlyTable">
    <tr>
        <th>Scholarship Name:</th>
        <td ><asp:Label  ID="lblName" runat="server"  /></td>
    </tr>
    <tr>
        <th>Academic Year:</th>
        <td ><asp:Label  ID="lblAcademicYear" runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Date Scholarship activated:</th>
        <td><asp:Label ID="lblScholarshipActivationDate" runat="server"/></td>
    </tr>
    <tr>
        <th scope="row">Donor:</th>
        <td><asp:Label ID="lblDonor"  runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Mission Statement:</th>
        <td><asp:Label ID="lblMission"  runat="server"  /></td>
    </tr>
    <tr class="exclude-in-print">
        <th scope="row">Program Guidelines: 
            <span id="EditControlContainer" runat="server">
                [<asp:HyperLink ID="editProgramGuidelinesLnk" runat="server">Edit</asp:HyperLink>]
            </span>    
        </th>
        <td><asp:Label ID="lblProgramGuidelines"  runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Schedule</th>
        <td>
            <table>
                <tbody>
                    <tr>
                        <th>Application Start On:</th>
                        <td><asp:Label  ID="lblAppStart" runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Application Due On:</th>
                        <td><asp:Label ID="lblAppDue"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Award On:</th>
                        <td><asp:Label ID="lblAwardOn"  runat="server"  /></td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <th scope="row">Award Amount:</th>
        <td><asp:Label ID="lblAwardAmount" runat="server"  /></td>
    </tr>
    <tr>
        <th scope="row">Number of Awards:</th>
        <td><asp:Label ID="Label1" runat="server"  /></td>
    </tr>
</table>

<h4>Contact Information </h4>
<table class="viewonlyTable">
    <tr>
        <th>Provider Name:</th>
        <td><asp:Label  ID="lblProviderName" runat="server"  /></td>
    </tr>
    <tr>
        <th>Intermediary Name:</th>
        <td><asp:Label  ID="lblIntermediaryName" runat="server"  /></td>
    </tr>
    <tr>
        <th>Web site:</th>
        <td><asp:Label  ID="lblProviderWebSite" runat="server"  /></td>
    </tr>
    <tr>
        <th>Address:</th>
        <td>
            <asp:Label  ID="lblAddressLine1" runat="server"  /><br />
            <asp:Label  ID="lblAddressLine2" runat="server"  /><br />
            <asp:Label  ID="lblCity" runat="server"  /><br />
            <asp:Label  ID="lblState" runat="server"  /><br />
            <asp:Label  ID="lblPostCode" runat="server"  /><br />
            <asp:Label  ID="lblPhone" runat="server"  /><br />
        </td>
    </tr>
</table>

</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
