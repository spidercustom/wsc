﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using System.Collections.Generic;

namespace ScholarBridge.Web.Common
{
    public delegate void MatchAction(int scholarshipId);

    public partial class MatchList : UserControl
    {
        public IList<Match> Matches { get; set; }
        public string LinkTo { get; set; }

        public string ActionText { get; set; }
        public event MatchAction MatchAction;
        public Func<Match, bool> IsActionEnabled { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindMatches();
            }
        }

        private void BindMatches()
        {
            matchList.DataSource = Matches;
            matchList.DataBind();
        }

        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match)((ListViewDataItem)e.Item).DataItem);
                var link = (HyperLink)e.Item.FindControl("linktoScholarship");
                link.NavigateUrl = LinkTo + "?id=" + match.Scholarship.Id;

                var btn = (Button)e.Item.FindControl("matchBtn");
                btn.CommandArgument = match.Scholarship.Id.ToString();
                btn.Text = ActionText;

                if (null == MatchAction || !IsActionEnabled(match))
                {
                    btn.Enabled = false;
                }
            }
        }

        protected void matchBtn_OnCommand(object sender, CommandEventArgs e)
        {
            var id = Int32.Parse((string)e.CommandArgument);
            if (null!=  MatchAction)
            {
                MatchAction(id);
            }
            BindMatches();
        }

        protected void matchList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindMatches();
        }
    }
}