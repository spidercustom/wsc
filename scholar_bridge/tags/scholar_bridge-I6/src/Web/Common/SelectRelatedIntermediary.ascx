﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectRelatedIntermediary.ascx.cs" Inherits="ScholarBridge.Web.Common.SelectRelatedIntermediary" %>
<asp:DropDownList ID="intermediaryDDL" runat="server" 
    onselectedindexchanged="intermediaryDDL_SelectedIndexChanged"  AutoPostBack="true" />
<asp:Label class="noteBene" ID="hecbMessage" runat="server" Text="Place keeper text for WSC defined message about the HECB acting as the intermediary in ScholarBridge if they select their organization as an intermediary" Visible="false" />