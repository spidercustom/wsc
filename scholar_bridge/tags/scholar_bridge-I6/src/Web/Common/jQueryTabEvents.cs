﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("TabControlClientID")]
    [ToolboxData("<{0}:jQueryTabEvents runat=server></{0}:jQueryTabEvents>")]
    public class jQueryTabEvents : WebControl, IPostBackEventHandler
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string TabControlClientID
        {
            get
            {
                var s = (String)ViewState["TabControlClientID"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["TabControlClientID"] = value;
            }
        }

        private MultiView GetAssociatedMultiView()
        {
            if (string.IsNullOrEmpty(MultiViewControlID))
                return null;
            return NamingContainer.FindControl(MultiViewControlID) as MultiView;
        }

        public string MultiViewControlID { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            var postBackScript = Page.ClientScript.GetPostBackEventReference(this, TAB_SELECT);

            var builder = new StringBuilder();
            builder.AppendFormat(TabSelectEventTemplate, TabControlClientID, ActiveTabIndex, postBackScript);
            builder.Replace(",'select'", ",'select:' + ui.index.toString()");

            Page.ClientScript.RegisterClientScriptBlock(GetType(), ClientID + "key", builder.ToString(), true);
            base.OnPreRender(e);
        }

        private const string TabSelectEventTemplate =
@"
$(document).ready(function() {{
    $('#{0}').tabs('select', {1});
    $('#{0}').bind('tabsselect', function(event, ui) {{
        {2}
    }});
}});
";

        private int activeTabIndex;
        public int ActiveTabIndex
        {
            get
            {
                var associatedMultiView = GetAssociatedMultiView();
                if (null == associatedMultiView)
                    return activeTabIndex;
                return associatedMultiView.ActiveViewIndex;
            }
            set
            {
                var associatedMultiView = GetAssociatedMultiView();
                if (null != associatedMultiView)
                    associatedMultiView.ActiveViewIndex = value;
                
                activeTabIndex = value;
            }
        }

        #region IPostBackEventHandler Members
        private const string TAB_SELECT = "select";
        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument.StartsWith(TAB_SELECT))
            {
                var parameters = eventArgument.Split(':');
                ActiveTabIndex = Int32.Parse(parameters[1]);
                if (null != TabChanged)
                {
                    TabChanged(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler TabChanged;

        #endregion
    }
}
