﻿using System;
using ScholarBridge.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class PrintView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PrintViewContainer.Visible = Page.IsInPrintView();
            ScreenViewContainer.Visible = !PrintViewContainer.Visible;

            printViewLink.HRef = BuildPrintViewUrl();
        }

        private string BuildPrintViewUrl()
        {
            var uriBuilder = new UrlBuilder(Request.Url.AbsoluteUri);
            if (! uriBuilder.QueryString.ContainsKey("print"))
            {
                uriBuilder.QueryString.Add("print", "true");
            }
            return uriBuilder.ToString();
        }
    }
}