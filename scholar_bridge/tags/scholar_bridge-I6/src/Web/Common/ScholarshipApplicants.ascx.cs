﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Logging;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipApplicants : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ScholarshipApplicants));

        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        public Scholarship Scholarship { get; set; }
        public string LinkTo { get; set; }

        private IList<Application> Applications { get; set; }

        public int ApplicationCount
        {
            get { return ApplicationService.CountAllSubmitted(Scholarship); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var ub = new UrlBuilder(Request.Url.AbsoluteUri) { PageName = "PrintApplications.aspx" };
            printWindow.Attributes.Add("onclick", String.Format("printApplications('{0}'); return false;", ub.ToString()));

            if (! Page.IsPostBack)
            {
                Applications = GetApplications(null);
                BindApplications();
            }
        }

        private void BindApplications()
        {
            lstApplicants.DataSource = Applications;
            lstApplicants.DataBind();

            if (null == Applications || Applications.Count == 0)
            {
                downloadAllBtn.Enabled = false;
                downloadAllFinalistBtn.Enabled = false;
            }
            else if (0 == Applications.Count(a => a.Finalist))
            {
                downloadAllFinalistBtn.Enabled = false;
            }
        }

        private IList<Application> GetApplications(string search)
        {
            if (null != Scholarship)
            {
                return String.IsNullOrEmpty(search) 
                    ? ApplicationService.FindAllSubmitted(Scholarship) 
                    : ApplicationService.FindAllSubmitted(Scholarship, search);
            }

            return null;
        }

        protected void lstApplicants_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var app = ((Application)((ListViewDataItem)e.Item).DataItem);
                BindFinalistCheckbox(e, app);
                BindApplicationLink(e, app);
                BindAttachmentsButton(e, app);
            }
        }

        protected void appFinalistCheckBox_OnCheckChanged(object sender, EventArgs e)
        {
            var cb = (CheckBox)sender;
            var appId = Int32.Parse(cb.InputAttributes["value"]);
            if (cb.Checked)
            {
                ApplicationService.Finalist(appId);
            }
            else
            {
                ApplicationService.NotFinalist(appId);
            }
            searchApplicants_Click(null, null);
        }

        private void BindFinalistCheckbox(ListViewItemEventArgs e, Application app)
        {
            var cb = (CheckBox)e.Item.FindControl("appFinalistCheckBox");
            cb.InputAttributes.Add("value", app.Id.ToString());
            cb.Checked = app.Finalist;
            // FIXME: When an app is awarded or denied or something, we probably want to disable this.
            // cb.Enabled = app.Stage != ApplicationStages.Awarded;
        }

        private void BindApplicationLink(ListViewItemEventArgs e, Application app)
        {
            var link = (HyperLink)e.Item.FindControl("linkToApplication");
            link.NavigateUrl = LinkTo + "?id=" + app.Id;
        }

        private void BindAttachmentsButton(ListViewItemEventArgs e, Application app)
        {
            var btn = (Button)e.Item.FindControl("attachmentsBtn");
            if (app.HasAttachments)
            {
                btn.CommandArgument = app.Id.ToString();
                var count = app.Attachments.Count;
                btn.Text = count == 1 ? "1 file" : String.Format("{0} files", count);
                var url = String.Format("{0}?id={1}", ResolveUrl("~/Common/ApplicationFiles.aspx"), app.Id);
                btn.Attributes.Add("onclick", String.Format("applicationAttachments('{0}'); return false;", url));
            }
            else
            {
                btn.Text = "None";
                btn.Enabled = false;
            }
        }

        protected void searchApplicants_Click(object sender, EventArgs e)
        {
            Applications = GetApplications(searchApplicantsTxt.Text);
            BindApplications();
            ReturnToTab();
        }

        protected void clearSearchBtn_Click(object sender, EventArgs e)
        {
            searchApplicantsTxt.Text = null;
            Applications = GetApplications(null);
            BindApplications();
            ReturnToTab();
        }

        private void ReturnToTab()
        {
            // This is a neat hack to get the page to reload into the proper tab
            Page.ClientScript.RegisterStartupScript(GetType(), "anchor", "location.hash = '#applicants-tab';", true);
        }

        protected void downloadAllBtn_OnClick(object sender, EventArgs e)
        {
            Applications = GetApplications(null);

            if (log.IsInfoEnabled) log.Info(String.Format("Downloading all application attachments for Scholarship id: '{0}'", Scholarship.Id));
            DownloadZip(Applications);
        }

        protected void downloadAllFinalistBtn_OnClick(object sender, EventArgs e)
        {
            Applications = GetApplications(null);

            if (log.IsInfoEnabled) log.Info(String.Format("Downloading all finalist application attachments for Scholarship id: '{0}'", Scholarship.Id));
            DownloadZip(Applications.Where(a => a.Finalist));
        }

        private void DownloadZip(IEnumerable<Application> applications)
        {
            var mf = new MultipleFiles();
            foreach (var application in applications)
            {
                if (application.HasAttachments)
                {
                    var attachments = application.Attachments;
                    var directory = application.ApplicantName.NameFirstLast;
                    attachments.ForEach(a =>
                                            {
                                                var fullPath = a.GetFullPath(ConfigHelper.GetAttachmentsDirectory());
                                                if (log.IsDebugEnabled)
                                                    log.Debug(String.Format("Adding file to zip: {0}", fullPath));

                                                mf.AddFile(Path.Combine(directory, a.Name), fullPath);
                                            });
                }
            }

            String file = null;
            try
            {
                file = Path.GetTempFileName();
                if (log.IsDebugEnabled) log.Debug(String.Format("Creating zip file: {0}", file));

                mf.CreateZip(file);

                if (log.IsDebugEnabled) log.Debug("Zip file created");

                FileHelper.SendZipFile(Response, file);
            }
            finally
            {
                if (null != file)
                {
                    try { File.Delete(file); } catch { }
                }
            }
        }
    }
}