﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{
    public partial class CaptchaControl : System.Web.UI.UserControl
    {
        public string ValidationGroup { get; set; }
        protected void Page_Load(object sender, EventArgs e)

        {
            if (!IsPostBack || ImageHipChallenge1.Words.Count==0)
            {
                HipValidator1.ValidationGroup = ValidationGroup;
                ImageHipChallenge1.Words.Add(CreateRandomNumber(5));
            }

        }

        private string CreateRandomNumber(int CodeLength)
        {
            string dictionary = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
            string[] dictArray = dictionary.Split(',');
            string randomCode = "";
            int temp = -1;

            Random ObjRand = new Random();
            for (int i = 0; i < CodeLength; i++)
            {
                if (temp != -1)
                {
                    ObjRand = new Random(i * temp * ((int)DateTime.Now.Ticks));
                }
                int t = ObjRand.Next(20);
                if (temp == t)
                {
                    return CreateRandomNumber(CodeLength);
                }
                temp = t;
                randomCode += dictArray[t];
            }
            return randomCode;
        }

        protected void trynewButton_Click(object sender, EventArgs e)
        {
            ImageHipChallenge1.Words.Add(CreateRandomNumber(5));
        } 
    }
}

