﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs" Inherits="ScholarBridge.Web.Common.UserDetails" %>

<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>

<asp:Image ID="personImage" runat="server" ImageUrl="~/images/person.png" class="profileImage"/>

<div id="memberInfo">
    <asp:Label ID="nameLbl" runat="server" /><br />
    <asp:Label ID="emailAddressLbl" runat="server" />
    <p>
    <br />
    
    <table>
        <tr>
            <td>
                <small>Phone:</small> 
            </td>
            <td>
                <small><asp:Label ID="phoneLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Fax:</small> 
            </td>
            <td>
                <small><asp:Label ID="faxLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Other Phone:</small> 
            </td>
            <td>
                <small><asp:Label ID="otherPhoneLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Member Since:&nbsp;&nbsp;</small> 
            &nbsp;</td>
            <td>
                <small><asp:Label ID="memberSinceLbl" runat="server" /></small>
            </td>
        </tr>
        <tr>
            <td>
                <small>Last Login:</small> 
            </td>
            <td>
                <small><asp:Label ID="lastLoginLbl" runat="server" /></small>
            </td>
        </tr>
    </table>
    </p>
</div>