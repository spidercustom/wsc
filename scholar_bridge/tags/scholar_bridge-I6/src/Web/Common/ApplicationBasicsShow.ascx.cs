﻿using System;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationBasicsShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                applicantName.Text = ApplicationToView.ApplicantName.NameFirstLast;
                applicantEmail.Text = ApplicationToView.Email;
                if (ApplicationToView.DateOfBirth.HasValue)
                    dob.Text = ApplicationToView.DateOfBirth.Value.ToString("MM/dd/yyyy");
                if (null != ApplicationToView.Phone)
                    phoneNumber.Text = ApplicationToView.Phone.Number;
                if (null != ApplicationToView.MobilePhone)
                    mobileNumber.Text = ApplicationToView.MobilePhone.Number;
                gender.Text = ApplicationToView.Gender.ToString();

                if (null != ApplicationToView.Address)
                    address.Text = ApplicationToView.Address.ToString().Replace("\r\n", "<br/>\r\n");

                if (null != ApplicationToView.County)
                    county.Text = ApplicationToView.County.Name;

                religions.DataSource = ApplicationToView.Religions;
                religions.DataBind();
                religionsOther.Text = ApplicationToView.ReligionOther;

                heritages.DataSource = ApplicationToView.Ethnicities;
                heritages.DataBind();
                heritagesOther.Text = ApplicationToView.EthnicityOther;

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);
            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {
            EthnicityRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Ethnicity);
            ReligionRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Religion);
            GendersRow.Visible = seeker.HasAttributeUsage(SeekerProfileAttribute.Gender);
        }

        public override ApplicationStages Stage
        {
            get { return ApplicationStages.Basics; }
        }
    }
}