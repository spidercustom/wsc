﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SentMessageView.ascx.cs" Inherits="ScholarBridge.Web.Common.SentMessageView" %>

<div class="message">
    <h2><asp:Label ID="subjectLbl" runat="server"/></h2>
    <h3><asp:Label ID="fromLbl" runat="server" /></h3>
    <span id="sent"><asp:Label ID="dateLbl" runat="server" /></span>
    
    <pre>
    <asp:Label ID="contentLbl" runat="server"/>
    </pre>
</div>
