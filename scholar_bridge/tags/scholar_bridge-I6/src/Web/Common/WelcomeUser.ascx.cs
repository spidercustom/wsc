﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    public partial class WelcomeUser : UserControl
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var currentUser = UserContext.CurrentUser;
            if (null != currentUser)
            {
                if (null == currentUser.Name)
                    LoginFirstLastName.Text = currentUser.Username;
                else
                    LoginFirstLastName.Text = UserContext.CurrentUser.Name.NameFirstLast;
            }
        }
    }
}

