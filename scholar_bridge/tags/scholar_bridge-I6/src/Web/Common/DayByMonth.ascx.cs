﻿using System;
using ScholarBridge.Common.DayOfYear;

namespace ScholarBridge.Web.Common
{
    public partial class DayByMonth : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private ByMonthDay weekMonth;
        public ByMonthDay WeekMonth
        {
            get
            {
                if (null == weekMonth)
                    weekMonth = new ByMonthDay();

                weekMonth.DayOfMonth = (DaysOfMonth)DayOfMonthControl.SelectedIndex;
                weekMonth.Month = (MonthOfYear)MonthControl.SelectedIndex;

                return weekMonth;
            }
            set
            {
                weekMonth = value;

                DayOfMonthControl.SelectedIndex = (int)weekMonth.DayOfMonth;
                MonthControl.SelectedIndex = (int)weekMonth.Month;
            }
        }

    }
}