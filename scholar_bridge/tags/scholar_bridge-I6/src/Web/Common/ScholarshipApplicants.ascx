﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipApplicants.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipApplicants" %>

<div class="search" class="exclude-in-print">
<label for="searchApplicantsTxt">Search by Applicant Last Name</label>
<asp:TextBox ID="searchApplicantsTxt" runat="server" title="Last Name"/> 
<asp:Button ID="searchApplicants" runat="server" Text="Search" 
    onclick="searchApplicants_Click" /> <asp:Button ID="clearSearchBtn" 
    runat="server" Text="Clear Search" onclick="clearSearchBtn_Click" />
<br />
</div>

<asp:Button ID="printWindow" runat="server" Text="Print" />

<asp:ListView ID="lstApplicants" runat="server" onitemdatabound="lstApplicants_ItemDataBound" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th>Finalist</th>
                <th>Applicant Name</th>
                <th>Date Submitted</th>
                <th>Required Criteria</th>
                <th>Preferred Criteria</th>
                <th>Attachments</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><asp:CheckBox ID="appFinalistCheckBox" runat="server" OnCheckedChanged="appFinalistCheckBox_OnCheckChanged" AutoPostBack="true"/></td>
        <td><asp:HyperLink id="linkToApplication" runat="server"><%# Eval("ApplicantName.NameLastFirst")%></asp:HyperLink></td>
        <td><%# Eval("SubmittedDate", "{0:MM/dd/yyyy}")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><asp:Button ID="attachmentsBtn" runat="server" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><asp:CheckBox ID="appFinalistCheckBox" runat="server" OnCheckedChanged="appFinalistCheckBox_OnCheckChanged" AutoPostBack="true"/></td>
        <td><asp:HyperLink id="linkToApplication" runat="server"><%# Eval("ApplicantName.NameLastFirst")%></asp:HyperLink></td>
        <td><%# Eval("SubmittedDate", "{0:MM/dd/yyyy}")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><asp:Button ID="attachmentsBtn" runat="server" /></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no Applicants at this time</p>
    </EmptyDataTemplate>
</asp:ListView>

<asp:LinkButton ID="downloadAllBtn" runat="server" Text="Download All Attachments" OnClick="downloadAllBtn_OnClick" CssClass="exclude-in-print"/>
<asp:LinkButton ID="downloadAllFinalistBtn" runat="server" Text="Download All Finalist Attachments" OnClick="downloadAllFinalistBtn_OnClick"  CssClass="exclude-in-print"/>


<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="lstApplicants" PageSize="25">
        <Fields>
            <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel"  NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
            <asp:NextPreviousPagerField />
        </Fields>
    </asp:DataPager>
</div> 