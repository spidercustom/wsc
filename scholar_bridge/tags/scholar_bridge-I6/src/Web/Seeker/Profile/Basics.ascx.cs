﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Basics : WizardStepUserControlBase<Domain.Seeker>
    {
		public ISeekerService SeekerService { get; set; }
		public IStateDAL StateService { get; set; }
		Domain.Seeker _seekerInContext;
		Domain.Seeker SeekerInContext
		{
			get
			{
				if (_seekerInContext == null)
					_seekerInContext = Container.GetDomainObject();
				return _seekerInContext;
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			if (SeekerInContext == null)
				throw new InvalidOperationException("There is no seeker in context");

			if (!Page.IsPostBack)
			{
				PopulateScreen();
			    DisableNameBoxesIfActive();
			}
		}

        private void DisableNameBoxesIfActive()
        {
            var seeker = SeekerInContext;
            if (null != seeker && seeker.Stage == SeekerStages.Published)
            {
                FirstNameBox.Enabled = false;
                MidNameBox.Enabled = false;
                LastNameBox.Enabled = false;
            }
        }

		private void PopulateScreen()
		{
			StateDropDown.DataSource = StateService.FindAll();
			StateDropDown.DataTextField = "Name";
			StateDropDown.DataValueField = "Abbreviation";
			StateDropDown.DataBind();
			StateDropDown.Items.Insert(0, new ListItem("- Select One -", ""));
            EmailBox.Text = !String.IsNullOrEmpty(SeekerInContext.User.EmailWaitingforVerification) ? string.Format("{0} - Email waiting for verification ({1})", SeekerInContext.User.Email, SeekerInContext.User.EmailWaitingforVerification)
                : SeekerInContext.User.Email;
		    UseEmailBox.Checked = SeekerInContext.User.IsRecieveEmails;
			LastNameBox.Text = SeekerInContext.User.Name.LastName;
			MidNameBox.Text = SeekerInContext.User.Name.MiddleName;
			FirstNameBox.Text = SeekerInContext.User.Name.FirstName;
			AddressLine1Box.Text = SeekerInContext.Address.Street;
			AddressLine2Box.Text = SeekerInContext.Address.Street2;
			if (null != SeekerInContext.Address.State)
				StateDropDown.SelectedValue = SeekerInContext.Address.State.Abbreviation;

		    CountyControlDialogButton.Keys = SeekerInContext.County == null ? "" : SeekerInContext.County.Id.ToString();
            CityControlDialogButton.Keys = SeekerInContext.Address.City == null ? "" : SeekerInContext.Address.City.Id.ToString();
			ZipBox.Text = SeekerInContext.Address.PostalCode;
            
            PhoneBox.Text = SeekerInContext.Phone == null ? "" : SeekerInContext.Phone.FormattedPhoneNumber;
            MobilePhoneBox.Text = SeekerInContext.MobilePhone == null ? "" : SeekerInContext.MobilePhone.FormattedPhoneNumber;

            if (SeekerInContext.DateOfBirth.HasValue)
		        DobControl.Text = SeekerInContext.DateOfBirth.Value.ToString("MM/dd/yyyy");

			ReligionCheckboxList.SelectedValues = SeekerInContext.Religions.Cast<ILookup>().ToList();
			EthnicityControl.SelectedValues = SeekerInContext.Ethnicities.Cast<ILookup>().ToList();
			OtherReligion.Text = SeekerInContext.ReligionOther;
			GenderButtonList.SelectedIndex = (SeekerInContext.Gender.Equals(Genders.Male) ? 0 : (SeekerInContext.Gender.Equals(Genders.Female) ? 1 : -1));
			OtherEthnicity.Text = SeekerInContext.EthnicityOther;
		}
		
		public override void PopulateObjects()
		{
			SeekerInContext.User.Name.LastName = LastNameBox.Text;
			SeekerInContext.User.Name.MiddleName = MidNameBox.Text;
			SeekerInContext.User.Name.FirstName = FirstNameBox.Text;
			SeekerInContext.Address.Street = AddressLine1Box.Text;
			SeekerInContext.Address.Street2 = AddressLine2Box.Text;
			SeekerInContext.Address.State = StateDropDown.SelectedValue == string.Empty 
															? null 
															: StateService.FindByAbbreviation(StateDropDown.SelectedValue);
		    SeekerInContext.County = (County) CountyControlDialogButton.GetSelectedLookupItem();
		    SeekerInContext.Address.City = (City) CityControlDialogButton.GetSelectedLookupItem();

			SeekerInContext.Address.PostalCode = ZipBox.Text;
            SeekerInContext.Phone = new PhoneNumber(PhoneBox.Text);
            SeekerInContext.MobilePhone = new PhoneNumber(MobilePhoneBox.Text);
		    SeekerInContext.User.IsRecieveEmails = UseEmailBox.Checked;
		    DateTime dob;
		    if (DateTime.TryParse(DobControl.Text, out dob))
		    {
		        SeekerInContext.DateOfBirth = dob;
		    }
            else
		    {
                SeekerInContext.DateOfBirth = null;
		    }

			PopulateList(EthnicityControl, SeekerInContext.Ethnicities);
			PopulateList(ReligionCheckboxList, SeekerInContext.Religions);
			SeekerInContext.Gender = GenderButtonList.SelectedValue == string.Empty ? Genders.Unspecified : (Genders) int.Parse(GenderButtonList.SelectedValue);
			SeekerInContext.EthnicityOther = OtherEthnicity.Text;
			SeekerInContext.ReligionOther = OtherReligion.Text;
		}

		private static void PopulateList<T>(LookupItemCheckboxList checkboxList, IList<T> list)
		{
			checkboxList.PopulateListFromSelectedValues(list);
		}

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
			PopulateObjects();
			SeekerService.Update(SeekerInContext);
		}

        public override bool WasSuspendedFrom(Domain.Seeker @object)
        {
            return @object.Stage == SeekerStages.Basics;
        }

        public override bool CanResume(Domain.Seeker @object)
        {
            return @object.Stage >= SeekerStages.Basics;
        }

		public override bool IsCompleted
		{
			get { return true; }
			set { }
		}
		#endregion
	}
}