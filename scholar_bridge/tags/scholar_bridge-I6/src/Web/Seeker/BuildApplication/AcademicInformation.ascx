﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicInformation.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.AcademicInformation" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerStudentType.ascx" tagname="SeekerStudentType" tagprefix="sb" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>

<label for="CurrentStudentGroupControl">What type of student are you currently?:</label>
<br />
<sb:SeekerStudentType id="CurrentStudentGroupControl" runat="server" />
<br />

<label id="HighSchoolLabelControl" for="HighSchoolControl">High School you are currently attending:</label>
<asp:TextBox ID="HighSchoolControl" ReadOnly="true" runat="server"></asp:TextBox>
<sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl" 
    OtherControl="HighSchoolOtherControl" OtherControlLabel="HighSchoolOtherLabelControl" ItemSource="HighSchoolDAL" Title="High School Selection" SelectionLimit="1" /><br />
<label id="HighSchoolOtherLabelControl" for="HighSchoolOtherControl">Other:</label>
<asp:TextBox CssClass="othercontroltextarea" ID="HighSchoolOtherControl" runat="server" />
<br />
<asp:PlaceHolder ID="SchoolDistrictContainerControl" runat="server">
<label id="SchoolDistrictLabelControl" for="SchoolDistrictControl">School District:</label>
<asp:TextBox ID="SchoolDistrictControl" ReadOnly="true" runat="server"></asp:TextBox>
<sb:LookupDialog ID="SchoolDistrictControlLookupDialogButton" runat="server" BuddyControl="SchoolDistrictControl" ItemSource="SchoolDistrictDAL" Title="School District Selection" SelectionLimit="1" 
    OtherControl="SchoolDistrictOtherControl" OtherControlLabel="SchoolDistrictOtherLabelControl"/><br />
<label id="SchoolDistrictOtherLabelControl" for="SchoolDistrictOtherControl">Other:</label>
<asp:TextBox CssClass="othercontroltextarea" ID="SchoolDistrictOtherControl" runat="server" />
<br />
</asp:PlaceHolder>

<h3>High School Academic Performance</h3>

<label for="GPAControl">GPA:</label>
<sandTrap:NumberBox ID="GPAControl" runat="server" Precision="0" MinAmount="0" MaxAmount="5"/>
<elv:PropertyProxyValidator ID="GPAValidator" runat="server" ControlToValidate="GPAControl" PropertyName="GPA" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
<br />
<label for="ClassRankControl">Class Rank:</label>
<sandTrap:NumberBox ID="ClassRankControl" runat="server" Precision="0" />
<elv:PropertyProxyValidator ID="ClassRankValidator" runat="server" ControlToValidate="ClassRankControl" PropertyName="ClassRank" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
<br />

<fieldset>
<h4>SAT</h4>
<label for="SATWritingControl">Writing:</label>
<sandTrap:NumberBox ID="SATWritingControl" runat="server" Precision="0" MinAmount="0"/>
<elv:PropertyProxyValidator ID="SATWritingControlValidator" runat="server" ControlToValidate="ClassRankControl" PropertyName="Writing" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
<br />
<label for="SATReadingControl">Critical Reading:</label>
<sandTrap:NumberBox ID="SATReadingControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="SATReadingValidator" runat="server" ControlToValidate="SATReadingControl" PropertyName="CriticalReading" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
<br />
<label for="SATMathControl">Mathematics:</label>
<sandTrap:NumberBox ID="SATMathControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="SATMathValidator" runat="server" ControlToValidate="SATMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
<br />
</fieldset>

<fieldset>
<h4>ACT</h4>
<label for="ACTEnglishControl">English:</label>
<sandTrap:NumberBox ID="ACTEnglishControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="ACTEnglishValidator" runat="server" ControlToValidate="ACTEnglishControl" PropertyName="English" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
<br />
<label for="ACTReadingControl">Reading:</label>
<sandTrap:NumberBox ID="ACTReadingControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="ACTReadingValidator" runat="server" ControlToValidate="ACTReadingControl" PropertyName="Reading" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />

<br />
<label for="ACTMathControl">Mathematics:</label>
<sandTrap:NumberBox ID="ACTMathControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="ACTMathValidator" runat="server" ControlToValidate="ACTMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />

<br />
<label for="ACTScienceControl">Science:</label>
<sandTrap:NumberBox ID="ACTScienceControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="ACTScienceValidator" runat="server" ControlToValidate="ACTScienceControl" PropertyName="Science" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
<br />
</fieldset>

<label for="HonorsControl">Honors:</label>
<asp:CheckBox ID="HonorsControl" Text="" runat="server" />
<br />

<label for="APCreditsControl">AP Credits:</label>
<sandTrap:NumberBox ID="APCreditsControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="APCreditsValidator" runat="server" ControlToValidate="APCreditsControl" PropertyName="APCredits" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />

<br />
<label for="IBCreditsControl">IB Credits:</label>
<sandTrap:NumberBox ID="IBCreditsControl" runat="server" Precision="0"  MinAmount="0"/>
<elv:PropertyProxyValidator ID="IBCreditsValidator" runat="server" ControlToValidate="IBCreditsControl" PropertyName="IBCredits" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
<br />

<label for="CollegeControl">College you are currently attending:</label>
<asp:TextBox ID="CollegeControl" ReadOnly="true" runat="server"></asp:TextBox>
<sb:LookupDialog ID="CollegeControlDialogButton" runat="server" BuddyControl="CollegeControl" OtherControl="CollegeOtherControl" OtherControlLabel="CollegeOtherLabelControl" ItemSource="CollegeDAL" Title="College Selection" SelectionLimit="1" /><br />
<label id="CollegeOtherLabelControl" for="CollegeOtherControl">Other:</label>
<asp:TextBox CssClass="othercontroltextarea" ID="CollegeOtherControl" runat="server" />
<br />

<label for="SchoolTypeControl">What types of schools are you considering?</label>
<sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
<br />

<label for="AcademicProgramControl">Academic program:</label>
<sb:EnumRadioButtonList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
<br />

<label for="SeekerStatusControl">Enrollment status?:</label>
<sb:EnumRadioButtonList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
<br />

<label for="ProgramLengthControl">Length of program:</label>
<sb:EnumRadioButtonList id="ProgramLengthControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.ProgramLengths, ScholarBridge.Domain" />
<br />
  
<label for="FirstGenerationControl">First Generation:</label>
<asp:Image ID="FirstGenerUsageTypeIconControl" runat="server" />
<asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
<br />

<label for="CollegesAppliedLabelControl">List of Colleges I’m considering:</label>
<asp:TextBox ID="CollegesAppliedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
<sb:LookupDialog ID="CollegesAppliedControlDialogButton" runat="server" BuddyControl="CollegesAppliedLabelControl" OtherControl="CollegesAppliedOtherControl" ItemSource="CollegeDAL" Title="College Selection"/><br />
<asp:TextBox CssClass="othercontroltextarea" ID="CollegesAppliedOtherControl" runat="server" />
<br />

<label for="CollegesAcceptedLabelControl">List of Colleges I’m accepted at:</label>
<asp:TextBox ID="CollegesAcceptedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
<sb:LookupDialog ID="CollegesAcceptedControlDialogButton" runat="server" BuddyControl="CollegesAcceptedLabelControl" OtherControl="CollegesAcceptedOtherControl" ItemSource="CollegeDAL" Title="College Selection"/><br />
<asp:TextBox CssClass="othercontroltextarea" ID="CollegesAcceptedOtherControl" runat="server" />
<br />