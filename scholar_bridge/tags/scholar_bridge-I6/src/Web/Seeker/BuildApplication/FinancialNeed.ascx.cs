﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class FinancialNeed : WizardStepUserControlBase<Domain.Application>
    {
        Domain.Application _applicationInContext;
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        public INeedGapDAL NeedGapDAL { get; set; }
        Domain.Application applicationInContext
        {
            get
            {
                if (_applicationInContext == null)
                    _applicationInContext = Container.GetDomainObject();
                return _applicationInContext;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (applicationInContext == null)
                throw new InvalidOperationException("There is no application in context");

            if (!Page.IsPostBack)
            {

                NeedGaps.DataBind();
                TypesOfSupport.DataBind();

                PopulateScreen();
            }
        }
        private void PopulateScreen()
        {
            PopulateCheckBoxes(NeedGaps, NeedGapDAL.FindAll());
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());
            if (null != applicationInContext.Need)
            {
                Fafsa.Checked = applicationInContext.Need.Fafsa;
                UserDerived.Checked = applicationInContext.Need.UserDerived;
                MinimumSeekerNeed.Amount = applicationInContext.Need.MinimumSeekerNeed;
                MaximumSeekerNeed.Amount = applicationInContext.Need.MaximumSeekerNeed;
                NeedGaps.Items.SelectItems(applicationInContext.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != applicationInContext.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(applicationInContext.SupportedSituation.TypesOfSupport, ts => ts.Id.ToString());
            }
        }

        public override void PopulateObjects()
        {
            // Need
            if (null == applicationInContext.Need)
                applicationInContext.Need = new DefinitionOfNeed();
            applicationInContext.Need.Fafsa = Fafsa.Checked;
            applicationInContext.Need.UserDerived = UserDerived.Checked;
            applicationInContext.Need.MinimumSeekerNeed = MinimumSeekerNeed.Amount;
            applicationInContext.Need.MaximumSeekerNeed = MaximumSeekerNeed.Amount;

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => (object)item);
            applicationInContext.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            applicationInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (applicationInContext.Stage < ApplicationStages.FinancialNeed)
                applicationInContext.Stage = ApplicationStages.FinancialNeed;
        }


        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
       
        #region WizardStepUserControlBase methods

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            ApplicationService.Save(applicationInContext);
        }

        public override bool WasSuspendedFrom(Domain.Application @object)
        {
            return @object.Stage == ApplicationStages.FinancialNeed;
        }

        public override bool CanResume(Domain.Application @object)
        {
            return @object.Stage >= ApplicationStages.FinancialNeed;
        }

        public override bool IsCompleted
        {
            get { return true; }
            set { }
        }
        #endregion
	}
}