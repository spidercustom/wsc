﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class Default : SBBasePage, IWizardStepsContainer<Domain.Application>
    {
        #region consts
        private const string SUBMISSION_VALIDATION_ERRORS_DIV_ID = "SubmissionValidationErrors";

	    private const string SUBMISSION_PAGE = "~/Seeker/Applications/Submit.aspx?aid={0}";
        private const string DELETE_PAGE = "~/Seeker/Applications/Delete.aspx?aid={0}";
        private const string DEFAULT_PAGE = "~/Seeker/Applications/Show.aspx?aid={0}";
        public const string APPLICATION_ID = "aid";
	    public const string SCHOLARSHIP_ID = "sid";
        #endregion

        #region properties

        public IApplicationService ApplicationService { get; set; }
		public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
	    
        private int CurrentApplicationId
        {
            get
            {
                int applicationId = this.GetIntegerPageParameterValue(ViewState, APPLICATION_ID, -1);
                return applicationId;
            }
            set
            {
                ViewState[APPLICATION_ID] = value;
            }
        }
		#endregion

		#region Page Lifecycle Events
		protected void Page_Load(object sender, EventArgs e)
        {
			Steps.ForEach(step => step.Container = this);

			if (!IsPostBack)
			{
                RemoveSubmitButtonIfAlreadySubmitted();

			    CheckForDataChanges = true;
                var applicationToEdit = GetCurrentApplication();
				BypassPromptIds.AddRange(
					new[]	{
								"PreviousButton",
								"NextButton",
								"SaveButton",
                                "SubmitButton",
                                "DeleteButton"
							});

				
                if (applicationToEdit != null)
                {
                    if (applicationToEdit.Seeker != UserContext.CurrentSeeker)
                        throw new InvalidOperationException("Application does not belong to seeker in context");

                    if (applicationToEdit.Stage == ApplicationStages.Submitted)
                    {
                        SuccessMessageLabel.SetMessage("Dear User, The application has already been submitted hence it can't be edited");
                        Response.Redirect(ResolveUrl(DEFAULT_PAGE.Build(applicationToEdit.Id)));
                    }
                    
                
			    ActiveStepIndex = WizardStepName.Basics.GetNumericValue();
                }
			}
		}
        public Application GetCurrentApplication()
        {
            return CurrentApplicationId > 0 ? ApplicationService.GetById(CurrentApplicationId) : null;
        }
        private void RemoveSubmitButtonIfAlreadySubmitted()
	    {
	        var application = GetDomainObject();
            if (null != application && application.Stage == ApplicationStages.Submitted)
            {
                SubmitButton.Visible = false;

                const string msg =
                    "You have submitted your applicatoin information. Once saved, the changes will replace the previous information in your application. Do you want to update your application with these changes?";
                SaveButton.Attributes.Add("onclick", String.Format("return confirmSaveDialog('{0}', 'Save Changes?')", msg));
            }
	    }

	    protected void Page_PreRender(object sender, EventArgs e)
		{
			PreviousButton.Enabled = ActiveStepIndex != 0;
			NextButton.Enabled = ActiveStepIndex != Steps.Length - 1;
		}

		#endregion

		#region Control event handlers

		protected void BuildApplicationWizard_ActiveStepChanged(object sender, EventArgs e)
    	{
			WizardStepContainerCommon.NotifyStepActivated(this);
		}

        protected void PreviousButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
    	{
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
    		    GoPrior();
		}

        protected void NextButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
    	{
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
    		    GoNext();
    	}
        
        
        protected void SubmitButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            
            WizardStepContainerCommon.PopulateObjectsFromActiveStep(this);
            if (ValidateSubmitted())
            {
                Save();
                ClientSideDialogs.ShowDivAsYesNoDialog("confirmSubmit", ResolveUrl(SUBMISSION_PAGE.Build(applicationInContext.Id)));
            }
        }

        protected void ExitButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
            {
                Response.Redirect("../");
            }
        }

		#endregion

		#region Private Methods

        Domain.Application applicationInContext;
        public Domain.Application GetDomainObject()
        {
            if (applicationInContext == null)
            {
               
                applicationInContext = GetCurrentApplication();
                if (applicationInContext==null)
                {
                    var currentScholarship = GetScholarshipInContext();
                    if (currentScholarship == null)
                        throw new InvalidOperationException("No Scholarship in context.");

                    applicationInContext = ApplicationService.FindBySeekerandScholarship(UserContext.CurrentSeeker,
                                                                                         currentScholarship);

                    //Create New one
                    if (applicationInContext == null)
                    {
                        applicationInContext = new Domain.Application
                        {
                            //intialize default values here
                            Seeker = UserContext.CurrentSeeker,
                            Scholarship = currentScholarship,


                        };
                        applicationInContext.CopyFromSeekerandScholarship();
                        applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                        ApplicationService.Save(applicationInContext);
                        CurrentApplicationId = applicationInContext.Id;

                    }
                    else {CurrentApplicationId = applicationInContext.Id; }

                }
            }
            
            return applicationInContext;
        }

        public Scholarship GetScholarshipInContext()
        {
            int scholarshipId = this.GetIntegerPageParameterValue(ViewState, SCHOLARSHIP_ID, -1);
            return scholarshipId > 0 ? ScholarshipService.GetById(scholarshipId) : null;
        }
        

	    private bool ValidateStep()
		{
			return WizardStepContainerCommon.ValidateStep(this);
		}

        private bool Save()
		{
            var application = GetDomainObject();
            if (!Page.IsValid || !ValidateStep())
                return false;
            if (application.Stage == ApplicationStages.Submitted && !ValidateSubmitted())
                return false;
            application.LastUpdate=new ActivityStamp(UserContext.CurrentUser);
            WizardStepContainerCommon.SaveActiveStep(this);
            Dirty = false;
	        return true;
		}

        private bool ValidateSubmitted()
        {
            var application = GetDomainObject();

            var results = application.ValidateSubmission();
            if (results.IsValid)
                return true;
                
            IssueListControl.DataSource = results;
            IssueListControl.DataBind();
            ClientSideDialogs.ShowDivAsDialog(SUBMISSION_VALIDATION_ERRORS_DIV_ID);
            return false;
        }

		#endregion

		#region IWizardStepsContainer<Application> Members

        public IWizardStepControl<Domain.Application>[] Steps
		{
			get
			{
                IWizardStepControl<Domain.Application>[] stepControls =BuildApplicationWizard.Views.FindWizardStepControls<Domain.Application>();
				return stepControls;
			}
		}

        

		public void GoPrior()
		{
			WizardStepContainerCommon.GoPrior(this);
		}

		public void GoNext()
		{
			WizardStepContainerCommon.GoNext(this);
		}

		public void Goto(int index)
		{
			WizardStepContainerCommon.Goto(this, index);
		}

		public int ActiveStepIndex
		{
			get { return BuildApplicationWizard.ActiveViewIndex; }
			set { BuildApplicationWizard.ActiveViewIndex = value; }
		}

		#endregion

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            Save();
        }
        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            ClientSideDialogs.ShowDivAsYesNoDialog("confirmDelete", ResolveUrl(DELETE_PAGE.Build(applicationInContext.Id)));
        }
	}
}