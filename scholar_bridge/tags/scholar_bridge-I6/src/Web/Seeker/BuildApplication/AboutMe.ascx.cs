﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
    public partial class AboutMe : WizardStepUserControlBase<Domain.Application>
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Application application;
        Domain.Application ApplicationInContext
        {
            get
            {
                if (application == null)
                    application = Container.GetDomainObject();
                if (application == null)
                    throw new InvalidOperationException("There is no application in context");
                return application;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ApplicationInContext == null)
                throw new InvalidOperationException("There is no application in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
		}

        private void PopulateScreen()
        {
            PersonalStatementControl.Text = ApplicationInContext.PersonalStatement;
            MyGiftControl.Text = ApplicationInContext.MyGift;
            MyChallengeControl.Text = ApplicationInContext.MyChallenge;
            FiveWordsControlDialogButton.Keys = ApplicationInContext.Words.CommaSeparatedIds();
            FiveSkillsControlDialogButton.Keys = ApplicationInContext.Skills.CommaSeparatedIds();
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
		    PopulateObjects();
            ApplicationService.Save(ApplicationInContext);
		}

        public override void PopulateObjects()
        {
            ApplicationInContext.PersonalStatement = PersonalStatementControl.Text;
            ApplicationInContext.MyGift = MyGiftControl.Text;
            ApplicationInContext.MyChallenge = MyChallengeControl.Text;

            FiveWordsControlDialogButton.PopulateListFromSelection(ApplicationInContext.Words);
            FiveSkillsControlDialogButton.PopulateListFromSelection(ApplicationInContext.Skills);
            ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
        }

        public override bool WasSuspendedFrom(Domain.Application @object)
		{
            return @object.Stage == ApplicationStages.AboutMe;
		}

        public override bool CanResume(Domain.Application @object)
		{
            return @object.Stage >= ApplicationStages.AboutMe;
		}

		public override bool IsCompleted
		{
			get { return true; }
			set {  }
		}
		#endregion

	}
}