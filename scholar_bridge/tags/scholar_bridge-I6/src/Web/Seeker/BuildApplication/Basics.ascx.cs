﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class Basics : WizardStepUserControlBase<Domain.Application>
    {
		public IApplicationService ApplicationService { get; set; }
		public IStateDAL StateService { get; set; }
        public IUserContext UserContext { get; set; }
		Domain.Application _applicationInContext;
		Domain.Application ApplicationInContext
		{
			get
			{
				if (_applicationInContext == null)
					_applicationInContext = Container.GetDomainObject();
				return _applicationInContext;
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			if (ApplicationInContext == null)
				throw new InvalidOperationException("There is no application in context");

			if (!Page.IsPostBack)
			{
				PopulateScreen();
			    DisableNameBoxesIfActive();
			}
		}

        private void DisableNameBoxesIfActive()
        {
            var application = ApplicationInContext;
            if (null != application && application.Stage == ApplicationStages.Submitted)
            {
                FirstNameBox.Enabled = false;
                MidNameBox.Enabled = false;
                LastNameBox.Enabled = false;
            }
        }

		private void PopulateScreen()
		{
		    StateDropDown.DataSource = StateService.FindAll();
		    StateDropDown.DataTextField = "Name";
		    StateDropDown.DataValueField = "Abbreviation";
		    StateDropDown.DataBind();
		    StateDropDown.Items.Insert(0, new ListItem("- Select One -", ""));
		    if (null != ApplicationInContext.ApplicantName)
		    {
		    
		        LastNameBox.Text = ApplicationInContext.ApplicantName.LastName;
		        MidNameBox.Text = ApplicationInContext.ApplicantName.MiddleName;
		        FirstNameBox.Text = ApplicationInContext.ApplicantName.FirstName;
		    }

	        EmailBox.Text = ApplicationInContext.Email;
            ConfirmEmailBox.Text = ApplicationInContext.Email;
			AddressLine1Box.Text = ApplicationInContext.Address.Street;
			AddressLine2Box.Text = ApplicationInContext.Address.Street2;
			if (null != ApplicationInContext.Address.State)
				StateDropDown.SelectedValue = ApplicationInContext.Address.State.Abbreviation;

		    CountyControlDialogButton.Keys = ApplicationInContext.County == null ? "" : ApplicationInContext.County.Id.ToString();
            CityControlDialogButton.Keys = ApplicationInContext.Address.City == null ? "" : ApplicationInContext.Address.City.Id.ToString();
			ZipBox.Text = ApplicationInContext.Address.PostalCode;
            
            PhoneBox.Text = ApplicationInContext.Phone == null ? "" : ApplicationInContext.Phone.FormattedPhoneNumber;
            MobilePhoneBox.Text = ApplicationInContext.MobilePhone == null ? "" : ApplicationInContext.MobilePhone.FormattedPhoneNumber;

            if (ApplicationInContext.DateOfBirth.HasValue)
		        DobControl.Text = ApplicationInContext.DateOfBirth.Value.ToString("MM/dd/yyyy");

			ReligionCheckboxList.SelectedValues = ApplicationInContext.Religions.Cast<ILookup>().ToList();
			EthnicityControl.SelectedValues = ApplicationInContext.Ethnicities.Cast<ILookup>().ToList();
			OtherReligion.Text = ApplicationInContext.ReligionOther;
			GenderButtonList.SelectedIndex = (ApplicationInContext.Gender.Equals(Genders.Male) ? 0 : (ApplicationInContext.Gender.Equals(Genders.Female) ? 1 : -1));
			OtherEthnicity.Text = ApplicationInContext.EthnicityOther;
		}
		
		public override void PopulateObjects()
		{
            ApplicationInContext.ApplicantName.LastName = LastNameBox.Text;
            ApplicationInContext.ApplicantName.MiddleName = MidNameBox.Text;
            ApplicationInContext.ApplicantName.FirstName = FirstNameBox.Text;
            ApplicationInContext.Email = EmailBox.Text;
			ApplicationInContext.Address.Street = AddressLine1Box.Text;
			ApplicationInContext.Address.Street2 = AddressLine2Box.Text;
			ApplicationInContext.Address.State = StateDropDown.SelectedValue == string.Empty 
															? null 
															: StateService.FindByAbbreviation(StateDropDown.SelectedValue);
		    ApplicationInContext.County = (County) CountyControlDialogButton.GetSelectedLookupItem();
		    ApplicationInContext.Address.City = (City) CityControlDialogButton.GetSelectedLookupItem();

			ApplicationInContext.Address.PostalCode = ZipBox.Text;
            ApplicationInContext.Phone = new PhoneNumber(PhoneBox.Text);
            ApplicationInContext.MobilePhone = new PhoneNumber(MobilePhoneBox.Text);

		    DateTime dob;
		    if (DateTime.TryParse(DobControl.Text, out dob))
		    {
		        ApplicationInContext.DateOfBirth = dob;
		    }
            else
		    {
                ApplicationInContext.DateOfBirth = null;
		    }

			PopulateList(EthnicityControl, ApplicationInContext.Ethnicities);
			PopulateList(ReligionCheckboxList, ApplicationInContext.Religions);
			ApplicationInContext.Gender = GenderButtonList.SelectedValue == string.Empty ? Genders.Unspecified : (Genders) int.Parse(GenderButtonList.SelectedValue);
			ApplicationInContext.EthnicityOther = OtherEthnicity.Text;
			ApplicationInContext.ReligionOther = OtherReligion.Text;
            ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
		}

		private static void PopulateList<T>(LookupItemCheckboxList checkboxList, IList<T> list)
		{
			checkboxList.PopulateListFromSelectedValues(list);
		}

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
			PopulateObjects();
			ApplicationService.Save(ApplicationInContext);
		}

        public override bool WasSuspendedFrom(Domain.Application @object)
        {
            return @object.Stage == ApplicationStages.Basics;
        }

        public override bool CanResume(Domain.Application @object)
        {
            return @object.Stage >= ApplicationStages.Basics;
        }

		public override bool IsCompleted
		{
			get { return true; }
			set { }
		}
		#endregion
	}
}