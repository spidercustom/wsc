﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Web.Seeker.Applications
{
    public partial class Submit : Page
    {
        public IUserContext UserContext { get; set; }
        public IApplicationService ApplicationService { get; set; }
        private const string DEFAULT_PAGEURL = "~/Seeker/Applications/Show.aspx?aid={0}";
        private const string SUBMISSION_SUCCESS_NOTE = @"Congratulations, you have successfully Submitted the application. Wishing you all the best for positive interations with scholarship providers.";
	   
        private int ApplicationId
        {
            get
            {
                int applicationId;
                if (!Int32.TryParse(Request.Params["aid"], out applicationId))
                    throw new ArgumentException("Cannot understand value of parameter aid");
                return applicationId;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();

            var application = ApplicationService.GetById(ApplicationId);
            if (application == null)
                throw new ArgumentNullException("application");
           
            if (!(application.Seeker==UserContext.CurrentSeeker))
                    throw new InvalidOperationException("Application doesn't belong to seeker in context.");



            ApplicationService.SubmitApplication(application);
             SuccessMessageLabel.SetMessage(SUBMISSION_SUCCESS_NOTE);
            Response.Redirect(DEFAULT_PAGEURL.Build(application.Id));

        }
    }
}
