﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Seeker.Scholarships
{
	public partial class Default : SBBasePage
	{
	    private const string SCHOLARSHIP_VIEW_TEMPLATE = "~/Seeker/Matches/Show.aspx?id={0}";
	    public IMatchService MatchService { get; set; }
        public IUserContext UserContext { get; set; }

        private Domain.Seeker currentSeeker;
	    private IList<Match> matches;
	    

	    public IList<Match> Matches
	    {
	        get
	        {
                if (matches == null)
                {
                    if (currentSeeker == null)
                        throw new InvalidOperationException("There is no seeker in context");
                    matches = MatchService.GetSavedMatches(currentSeeker);
                }
	            return matches;
	        }
	    }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureSeekerIsInContext();
            currentSeeker = UserContext.CurrentSeeker;

            if (!Page.IsPostBack)
            {
                BindMyScholarships();
            }
        }

        private void BindMyScholarships()
        {
            myScholarhipList.DataSource = Matches;
            myScholarhipList.DataBind();
        }

        protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.UnSaveMatch(currentSeeker, scholarshipId);
        }


        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match)((ListViewDataItem)e.Item).DataItem);
                var link = (HyperLink)e.Item.FindControl("linkToScholarship");
                link.NavigateUrl = SCHOLARSHIP_VIEW_TEMPLATE.Build(match.Scholarship.Id);

                var btn = (Button)e.Item.FindControl("matchBtn");
                ApplyMatchAction(btn, match);
            }
        }


        protected void matchBtn_OnCommand(object sender, CommandEventArgs e)
        {
            var id = Int32.Parse((string)e.CommandArgument);
            var match = Matches.Single(o => o.Id == id);
            if (null == match)
                throw new InvalidOperationException("Cannot find match in cached collection");

            ExecuteDefaultAction(match);

            BindMyScholarships();
        }

        protected void matchList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            BindMyScholarships();
        }

        #region Match Actions

        private static void ApplyMatchAction(Button btn, Match match)
        {
            var matchAction = GetDefaultAction(match);
            btn.Visible = !(matchAction is BlankAction);
            btn.CommandArgument = match.Id.ToString();
            btn.Text = matchAction.Text;
        }

        private static MatchAction GetDefaultAction(Match match)
        {
            switch (match.MatchApplicationStatus)
            {
                case MatchApplicationStatus.Unkonwn:
                    return BlankAction.Instance;
                case MatchApplicationStatus.NotApplied:
                    return BuildApplicationAction.Instance;
                case MatchApplicationStatus.Appling:
                    return EditApplicationAction.Instance;
                case MatchApplicationStatus.Applied:
                    return ViewApplicationAction.Instance;
                case MatchApplicationStatus.BeingConsidered:
                    return ViewApplicationAction.Instance;
                case MatchApplicationStatus.Closed:
                    return BlankAction.Instance;
                case MatchApplicationStatus.Awarded:
                    return ViewApplicationAction.Instance;
                default:
                    throw new NotSupportedException();
            }
        }

        private static void ExecuteDefaultAction(Match match)
        {
            GetDefaultAction(match).Execute(match);
        }

        
        abstract class MatchAction
        {
            public string Text { get; protected set; }
            public Action<Match> Execute { get; protected set; }
        }

        abstract class OpenUrlAction : MatchAction
        {
            protected OpenUrlAction(string text)
            {
                Text = text;
                Execute = o => HttpContext.Current.Response.Redirect(ConstructUrl(o));
            }

            protected abstract string ConstructUrl(Match match);
        }

        class EditApplicationAction : OpenUrlAction
        {
            public static readonly EditApplicationAction Instance = new EditApplicationAction();
            private const string EDIT_APPLICATION_URL_TEMPLATE = "~/Seeker/BuildApplication/Default.aspx?aid={0}";

            public EditApplicationAction() : base("Edit") {}

            protected override string ConstructUrl(Match match)
            {
                return EDIT_APPLICATION_URL_TEMPLATE.Build(match.Application.Id);
            }
        }

        class BuildApplicationAction : OpenUrlAction
        {
            public static readonly BuildApplicationAction Instance = new BuildApplicationAction();
            private const string BUILD_APPLICATION_URL_TEMPLATE = "~/Seeker/BuildApplication/Default.aspx?sid={0}";

            public BuildApplicationAction() : base("Apply") {}

            protected override string ConstructUrl(Match match)
            {
                return BUILD_APPLICATION_URL_TEMPLATE.Build(match.Scholarship.Id);
            }
        }

        class ViewApplicationAction : OpenUrlAction
        {
            public static readonly ViewApplicationAction Instance = new ViewApplicationAction();
            private const string VIEW_APPLICATION_URL_TEMPLATE = "~/Seeker/Applications/Show.aspx?aid={0}";

            public ViewApplicationAction() : base("View") {}

            protected override string ConstructUrl(Match match)
            {
                return VIEW_APPLICATION_URL_TEMPLATE.Build(match.Application.Id);
            }
        }

        sealed class BlankAction : MatchAction
        {
            public static readonly MatchAction Instance = new BlankAction();
            
            private BlankAction()
            {
                Text = String.Empty;
                Execute = NoActionMethod;
            }

            private static void NoActionMethod(Match match) {}
        }
        #endregion
    }
}