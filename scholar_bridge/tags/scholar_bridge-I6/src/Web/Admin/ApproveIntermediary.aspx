﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ApproveIntermediary.aspx.cs" Inherits="ScholarBridge.Web.Admin.ApproveIntermediary" Title="Admin | Approve Intermediary" %>

<%@ Register TagPrefix="sb" TagName="ShowOrg" Src="~/Admin/ShowOrg.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Approve/Reject Pending Intermediary</h3>

    <sb:ShowOrg id="showOrg" runat="server" />

    <table>
        <tr>
            <td>
                <label for="existingNotesTb">Administrative Notes:</label>  
            </td>
            <td>
                <asp:panel ID="existingNotesPanel" runat="server" ScrollBars="Vertical" CssClass="adminNoteDisplay">
                    <asp:Literal ID="notesLiteral" runat="server"></asp:Literal>
                </asp:panel>
            </td>
        </tr>
        <tr>
            <td>
                <label for="existingNotesTb">Add Notes:</label>  
            </td>
            <td>
                <asp:TextBox ID="newNotesTb" CssClass="adminNoteEntry"  runat="server" TextMode="MultiLine" Columns="300" Rows="5"></asp:TextBox><br />
            </td>        
        </tr>
        <tr>
            <td>
            
            </td>
            <td>
                <asp:Button ID="addNotes" runat="server" Text="Add Note" onclick="addNotes_Click"  />
            </td>
        </tr>
    </table>
    <br />

    <asp:Button ID="approveButton" runat="server" Text="Approve" onclick="approveButton_Click" />
    <asp:Button ID="rejectButton" runat="server" Text="Deny" onclick="rejectButton_Click" />
</asp:Content>
