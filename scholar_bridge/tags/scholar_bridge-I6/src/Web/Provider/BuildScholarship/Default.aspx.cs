﻿using System;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain;
using ScholarBridge.Business;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Default : SBBasePage, IWizardStepsContainer<Scholarship>
    {
        public const string SCHOLARSHIP_ID = "sid";

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            Steps.ForEach(step => step.Container = this);

            if (!IsPostBack)
            {
                CheckForDataChanges = true;

                bool copyFromParameterExists = WizardStepContainerCommon.CopyFromParameterExists();
                bool resumeFromParameterExists = WizardStepContainerCommon.ResumeFromParameterExists();
                var scholarshipToEdit = GetCurrentScholarship();

                BypassPromptIds.AddRange(
                    new[]	{
								"PreviousButton",                            
								"NextButton",                            
								"SaveButton",
                                "ExitButton",
                                "StepCompletedCheckBox"
							});

                ActiveStepIndex = WizardStepName.GeneralInformation.GetNumericValue();
                if (copyFromParameterExists)
                {
                    BeginCopyFrom();
                }
                else if (scholarshipToEdit != null)
                {
                    if (!scholarshipToEdit.IsBelongToOrganization(UserContext.CurrentOrganization))
                        throw new InvalidOperationException("Scholarship do not belong to provider in context");

                    if (!scholarshipToEdit.CanEdit())
                    {
                        Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + scholarshipToEdit.Id);
                    }

                    if (resumeFromParameterExists)
                    {
                        ResumeFromStep();
                    }
                    else
                    {
                        ResumeWizard();
                    }
                    PopulateStageCompletionMarkers();
                }
            }
            
        }

        protected void PreviousButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
            {
                GoPrior();
                PopulateStageCompletionMarkers();
            }
        }

        protected void NextButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
            {
                GoNext();
                PopulateStageCompletionMarkers();
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void ExitButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
        {
            if (!e.SaveBeforeContinue || (e.SaveBeforeContinue && Save()))
            {
                Response.Redirect("../Scholarships");
            }
        }

        protected void BuildScholarshipWizard_ActiveStepChanged(object sender, EventArgs e)
        {
            WizardStepContainerCommon.NotifyStepActivated(this);
            PopulateStageCompletionMarkers();
        }

        protected void StepCompletedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var activeStep = GetActiveWizardStepControl();
            activeStep.IsCompleted = StepCompletedCheckBox.Checked;
            Dirty = false;
            PopulateStageCompletionMarkers();
        }

        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            //because its here, do not need to call this in save and load
            ScholarshipTitleStripeControl.UpdateView(GetDomainObject());
            base.OnPreRender(e);
        }

        protected bool CanEdit()
        {
            var scholarship = GetCurrentScholarship();
            return null == scholarship || scholarship.CanEdit();
        }


        public Scholarship GetCurrentScholarship()
        {
            return CurrentScholarshipId > 0 ? ScholarshipService.GetById(CurrentScholarshipId) : null;
        }

        private int CurrentScholarshipId
        {
            get
            {
                int scholarshipId = this.GetIntegerPageParameterValue(ViewState, SCHOLARSHIP_ID, -1);
                return scholarshipId;
            }
            set
            {
                ViewState[SCHOLARSHIP_ID] = value;
            }
        }

        private bool ValidateStep()
        {
            return WizardStepContainerCommon.ValidateStep(this);
        }

        private bool Save()
        {
            if (Page.IsValid && ValidateStep() && CanEdit())
            {
                WizardStepContainerCommon.SaveActiveStep(this);
                CurrentScholarshipId = GetDomainObject().Id;
                Dirty = false;

                return true;
            }

            return false;
        }

        private void BeginCopyFrom()
        {
            var copyFromId = WizardStepContainerCommon.GetCopyFromId();
            var copyFrom = ScholarshipService.GetById(copyFromId);

			scholarshipInContext = ScholarshipService.CopyScholarship(copyFrom);
            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(scholarshipInContext);
        	CurrentScholarshipId = scholarshipInContext.Id;
        }

        #region IWizardStepsContainer<Scholarship> Members
        public void ResumeWizard()
        {
            WizardStepContainerCommon.ResumeWizard(this);
        }

        private void ResumeFromStep()
        {
            WizardStepContainerCommon.ResumeFromStep(this);
        }

        public IWizardStepControl<Scholarship>[] Steps
        {
            get
            {
                var stepControls = BuildScholarshipWizard.Views.FindWizardStepControls<Scholarship>();
                return stepControls;
            }
        }

        Scholarship scholarshipInContext;
        public Scholarship GetDomainObject()
        {
            if (scholarshipInContext == null)
            {
                scholarshipInContext = GetCurrentScholarship() ?? new Scholarship
                                                                      {
                                                                          ApplicationStartDate = DateTime.Today,
                                                                          ApplicationDueDate = DateTime.Today,
                                                                          AwardDate = DateTime.Today
                                                                      } ;
            }
            return scholarshipInContext;
        }

        public void GoPrior()
        {
            WizardStepContainerCommon.GoPrior(this);
        }

        public void GoNext()
        {
            WizardStepContainerCommon.GoNext(this);
        }

        public void Goto(int index)
        {
            WizardStepContainerCommon.Goto(this, index);
        }

        public int ActiveStepIndex
        {
            get { return BuildScholarshipWizard.ActiveViewIndex; }
            set { BuildScholarshipWizard.ActiveViewIndex = value; }
        }

        #endregion

        private void PopulateStageCompletionMarkers()
        {
            Scholarship scholarship = GetDomainObject();
            GeneralInformationCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.GeneralInformation);
            MatchCriteraCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.MatchCriteriaSelection);
            SeekerProfileCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.SeekerProfile);
            FundingProfileCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.FundingProfile);
            AdditionalCriteriaCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.AdditionalCriteria);
            ActivateCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.Activated);
            IWizardStepControl<Scholarship> step = GetActiveWizardStepControl();
            if (null != step)
                StepCompletedCheckBox.Checked = step.IsCompleted;
        }

        protected IWizardStepControl<Scholarship> GetActiveWizardStepControl()
        {
            View view = BuildScholarshipWizard.GetActiveView();
            if (null != view)
            {
                var wizardStepControl = view.Controls.FindFirstWizardStepControl<Scholarship>();
                return wizardStepControl;
            }
            return new PlaceHolderStep<Scholarship>();
        }
    }
}
