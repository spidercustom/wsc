﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralInfo.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.GeneralInfo" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="ByMonthControl" Src="~/Common/DayByMonth.ascx" %>
<%@ Register TagPrefix="sb" TagName="ByWeekControl" Src="~/Common/DayByWeek.ascx" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedIntermediary" Src="~/Common/SelectRelatedIntermediary.ascx" %>
<%@ Register TagPrefix="sb" TagName="SelectRelatedProvider" Src="~/Common/SelectRelatedProvider.ascx" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CalendarControl" Src="~/Common/CalendarControl.ascx" %>

<h3>Build Scholarship – General Information</h3>

<label for="ScholarshipNameControl">Scholarship Name:</label>
<span class="requiredAttributeIndicator">*</span>
<asp:TextBox ID="ScholarshipNameControl" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="ScholarshipNameValidator" runat="server" ControlToValidate="ScholarshipNameControl" PropertyName="Name" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
<br />

<label for="ScholarshipAcademicYearControl">Scholarship Academic Year:</label>
<span class="requiredAttributeIndicator">*</span>
<asp:DropDownList ID="ScholarshipAcademicYearControl" runat="server" />
<elv:PropertyProxyValidator ID="ScholarshipAcademicYearValidator" runat="server" ControlToValidate="ScholarshipAcademicYearControl" PropertyName="AcademicYear" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
<br />

<p class="additionInfoText">
  The mission statement can contain what you want the scholarship seekers to see. Note: This is not a narrative version of criteria but rather the inspiration that motivated the scholarship founder. It is limited to 250 words to provide focus.
</p>

<label for="MissionStatementControl">Mission Statement:</label>
<asp:TextBox ID="MissionStatementControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="MissionStatementValidator" runat="server" ControlToValidate="MissionStatementControl" PropertyName="MissionStatement" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
<br />

<p class="additionInfoText">
Please enter the guidelines to be followed in the administration and award of this scholarship program. These guidelines should be based on the desire of the donor, and need to be reviewed annually (2000 character limit).
</p>

<label for="ProgramGuidelinesControl">Program Guidelines:</label>
<asp:TextBox ID="ProgramGuidelinesControl" runat="server" TextMode="MultiLine"></asp:TextBox>
<elv:PropertyProxyValidator ID="ProgramGuidelinesValidator" runat="server" ControlToValidate="ProgramGuidelinesControl" PropertyName="ProgramGuidelines" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
<br />

<fieldset>
  <legend>Organizations</legend>
  <label for="IntermediarySelected">Scholarship Intermediary:</label>
  <sb:SelectRelatedIntermediary id="IntermediarySelected" runat="server" />
  <br />
  <label for="ProviderSelected">Scholarship Provider:</label>
  <sb:SelectRelatedProvider id="ProviderSelected" runat="server" />
</fieldset>

<fieldset>
  <legend>Schedule</legend>
  <br />
  <label for="calAppStart">Application Start Date</label>
  <sb:CalendarControl ID="calApplicationStartDate" runat="server" ></sb:CalendarControl>
  <elv:PropertyProxyValidator ID="ApplicationStartDateValidator" runat="server" ControlToValidate="calApplicationStartDate" PropertyName="ApplicationStartDate" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
  <br />
  <label for="calApplicationDueDate"  >Application Due Date</label>
  <sb:CalendarControl ID="calApplicationDueDate" runat="server"></sb:CalendarControl>
   <elv:PropertyProxyValidator ID="ApplicationDueDateValidator" runat="server" ControlToValidate="calApplicationDueDate" PropertyName="ApplicationDueDate" SourceTypeName="ScholarBridge.Domain.Scholarship"/>

  <br />
  <label for="calAwardDate">Scholarship Notification/Award Date:</label>
  <sb:CalendarControl ID="calAwardDate"  runat="server"></sb:CalendarControl>
   <elv:PropertyProxyValidator ID="AwardDateValidator" runat="server" ControlToValidate="calAwardDate" PropertyName="AwardDate" SourceTypeName="ScholarBridge.Domain.Scholarship"/>

  <br />
</fieldset>

<fieldset >
  <legend>Scholarship Award Amount</legend>
  
  <label for="MaximumAmount">Maximum Scholarship Award Amount:</label>
  <span class="requiredAttributeIndicator">*</span>
  <sandTrap:CurrencyBox ID="MaximumAmount" runat="server" Precision="0" />
  <elv:PropertyProxyValidator ID="MaximumAmountValidator" runat="server" ControlToValidate="MaximumAmount" PropertyName="MaximumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
  <br />

  <label for="MinimumAmount">Minimum Scholarship Award Amount:</label>
  <span class="requiredAttributeIndicator">*</span>
  <sandTrap:CurrencyBox ID="MinimumAmount" runat="server"  Precision="0" />
  <elv:PropertyProxyValidator ID="MinimumAmountValidator" runat="server" ControlToValidate="MinimumAmount" PropertyName="MinimumAmount" SourceTypeName="ScholarBridge.Domain.Scholarship"/>
  <br />
</fieldset>

<fieldset>
  <h4>Donor Information</h4>
    
    <label for="FirstName">First Name:</label>
    <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
    <br />
    
    <label for="MiddleName">Middle Name:</label>
    <asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
    <br />
    
    <label for="LastName">Last Name:</label>
    <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
    <br />
  
    <label for="AddressStreet">Street:</label>
    <asp:TextBox ID="AddressStreet" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressStreetValidator" runat="server" ControlToValidate="AddressStreet" PropertyName="Street" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressStreet2">Street:</label>
    <asp:TextBox ID="AddressStreet2" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressStreet2Validator" runat="server" ControlToValidate="AddressStreet2" PropertyName="Street2" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressCity">City:</label>
    <asp:TextBox ID="AddressCity" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressCityValidator" runat="server" ControlToValidate="AddressCity" PropertyName="City" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressState">State:</label>
    <asp:DropDownList ID="AddressState" runat="server"></asp:DropDownList>
    <elv:PropertyProxyValidator ID="AddressStateValidator" runat="server" ControlToValidate="AddressState" PropertyName="State" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="AddressPostalCode">Postal Code:</label>
    <asp:TextBox ID="AddressPostalCode" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="AddressPostalCodeValidator" runat="server" ControlToValidate="AddressPostalCode" PropertyName="PostalCode" SourceTypeName="ScholarBridge.Domain.Contact.Address" />
    <br />
    
    <label for="Phone">Phone:</label>
    <asp:TextBox ID="Phone" runat="server" CssClass="phone"/>
    <elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="Phone" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
    <br />

    <label for="EmailAddress">Email Address:</label>
    <asp:TextBox ID="EmailAddress" runat="server"></asp:TextBox>
    <elv:PropertyProxyValidator ID="EmailAddressValidator" runat="server" ControlToValidate="EmailAddress" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipDonor" />
    <br />

</fieldset>