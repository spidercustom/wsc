﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class GeneralInfo : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IStateDAL StateService { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");


            //add calendar support



            if (!Page.IsPostBack)
            {
                AddressState.DataSource = StateService.FindAll();
                AddressState.DataTextField = "Name";
                AddressState.DataValueField = "Abbreviation";
                AddressState.DataBind();
                AddressState.Items.Insert(0, new ListItem("- Select One -", ""));

                ScholarshipAcademicYearControl.DataSource = DisplayAcademicYears();
                ScholarshipAcademicYearControl.DataTextField = "DisplayName";
                ScholarshipAcademicYearControl.DataValueField = "Year";
                ScholarshipAcademicYearControl.DataBind();
            }

            if (!IsPostBack)
                PopulateScreen();
        }

        private IEnumerable<AcademicYear> DisplayAcademicYears()
        {
            var midyear = ScholarshipInContext.AcademicYear ?? AcademicYear.CurrentScholarshipYear;
            return Enumerable.Range(-2, 6).Select(year => new AcademicYear(year + midyear.Year));
        }

        public override void PopulateObjects()
        {
            ScholarshipInContext.Name = ScholarshipNameControl.Text;
            ScholarshipInContext.AcademicYear = new AcademicYear(Int32.Parse(ScholarshipAcademicYearControl.SelectedValue));
            ScholarshipInContext.MissionStatement = MissionStatementControl.Text;
            ScholarshipInContext.ProgramGuidelines = ProgramGuidelinesControl.Text;
            ScholarshipInContext.Intermediary = IntermediarySelected.SelectedValue;
            ScholarshipInContext.Provider = ProviderSelected.SelectedValue;
            ScholarshipInContext.MinimumAmount = MinimumAmount.Amount;
            ScholarshipInContext.MaximumAmount = MaximumAmount.Amount;
            ScholarshipInContext.ApplicationStartDate = calApplicationStartDate.SelectedDate;
            ScholarshipInContext.ApplicationDueDate = calApplicationDueDate.SelectedDate;
            ScholarshipInContext.AwardDate = calAwardDate.SelectedDate;
            PopulateDonorObject();

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.GeneralInformation)
                ScholarshipInContext.Stage = ScholarshipStages.GeneralInformation;
        }

        private void PopulateDonorObject()
        {
            ScholarshipInContext.Donor.Name.FirstName = FirstName.Text;
            ScholarshipInContext.Donor.Name.MiddleName = MiddleName.Text;
            ScholarshipInContext.Donor.Name.LastName = LastName.Text;

            ScholarshipInContext.Donor.Address.Street = AddressStreet.Text;
            ScholarshipInContext.Donor.Address.Street2 = AddressStreet2.Text;
            ScholarshipInContext.Donor.Address.City = AddressCity.Text;
            ScholarshipInContext.Donor.Address.PostalCode = AddressPostalCode.Text;
            var state = StateService.FindByAbbreviation(AddressState.SelectedValue);
            ScholarshipInContext.Donor.Address.State = state;

            ScholarshipInContext.Donor.Phone = new PhoneNumber(Phone.Text);
            ScholarshipInContext.Donor.Email = EmailAddress.Text;
        }
        
        private void PopulateScreen()
        {
            IntermediarySelected.SelectedValue = ScholarshipInContext.Intermediary;
            ProviderSelected.SelectedValue = ScholarshipInContext.Provider;
            ScholarshipNameControl.Text = ScholarshipInContext.Name;

            int academicYear = null == ScholarshipInContext.AcademicYear
                ? AcademicYear.CurrentScholarshipYear.Year
                : ScholarshipInContext.AcademicYear.Year;

            ScholarshipAcademicYearControl.SelectedValue = academicYear.ToString();
            MissionStatementControl.Text = ScholarshipInContext.MissionStatement;
            ProgramGuidelinesControl.Text = ScholarshipInContext.ProgramGuidelines;

            calApplicationStartDate.SelectedDate = ScholarshipInContext.ApplicationStartDate;
            calApplicationDueDate.SelectedDate = ScholarshipInContext.ApplicationDueDate;
            calAwardDate.SelectedDate = ScholarshipInContext.AwardDate;
            MaximumAmount.Amount = ScholarshipInContext.MaximumAmount;
            MinimumAmount.Amount = ScholarshipInContext.MinimumAmount;
            PopulateDonorScreen();
        }

        private void PopulateDonorScreen()
        {
            FirstName.Text = ScholarshipInContext.Donor.Name.FirstName;
            MiddleName.Text = ScholarshipInContext.Donor.Name.MiddleName;
            LastName.Text = ScholarshipInContext.Donor.Name.LastName;

            AddressStreet.Text = ScholarshipInContext.Donor.Address.Street;
            AddressStreet2.Text = ScholarshipInContext.Donor.Address.Street2;
            AddressCity.Text = ScholarshipInContext.Donor.Address.City;
            AddressPostalCode.Text = ScholarshipInContext.Donor.Address.PostalCode;
            if (null != ScholarshipInContext.Donor.Address.State)
                AddressState.SelectedValue = ScholarshipInContext.Donor.Address.State.Abbreviation;
            if (null != ScholarshipInContext.Donor.Phone)
                Phone.Text = ScholarshipInContext.Donor.Phone.Number;
            EmailAddress.Text = ScholarshipInContext.Donor.Email;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.GeneralInformation;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.GeneralInformation;
        }

        public override bool IsCompleted
        {
            get { return ScholarshipInContext.IsStageCompleted(ScholarshipStages.GeneralInformation); }
            set
            {
                ScholarshipInContext.MarkStageCompletion(ScholarshipStages.GeneralInformation, value);
                ScholarshipService.Save(ScholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            bool result = true;

            var foundScholarship = ScholarshipService.ScholarshipExists(ProviderSelected.SelectedValue, ScholarshipNameControl.Text, Int32.Parse(ScholarshipAcademicYearControl.SelectedValue));
            if (null != foundScholarship && foundScholarship.Id != ScholarshipInContext.Id)
            {
                result &= false;
                ScholarshipNameValidator.IsValid = false;
                ScholarshipNameValidator.Text = "Scholarship in the selected Academic Year with this Name already exists";
            }
            return result;
        }

       
    }
}
