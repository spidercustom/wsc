﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchCriteriaSelection.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.MatchCriteriaSelection" %>
<%@ Register TagPrefix="sb" TagName="AttributeSelectionControl" Src="AttributeSelectionControl.ascx" %>
<p><br /></p>

<h3>Build Scholarship – Selection Criteria</h3>

<h4>Seeker's Personality</h4>
<sb:AttributeSelectionControl id="SeekerPersonalityControl" runat="server" />
<br />

<h4>Seeker Student Type</h4>
<sb:AttributeSelectionControl id="SeekerStudentTypeControl" runat="server" />
<br />

<h4>Seeker Demographics - Personal</h4>
<sb:AttributeSelectionControl id="SeekerDemographicsPersonalControl" runat="server" />
<br />

<h4>Seeker Demographics - Geographic</h4>
<sb:AttributeSelectionControl id="SeekerDemographicsGeographicControl" runat="server" />
<br />

<h4>Seeker Interests</h4>
<sb:AttributeSelectionControl id="SeekerInterestsControl" runat="server" />
<br />

<h4>Seeker Activities</h4>
<sb:AttributeSelectionControl id="SeekerActivitiesControl" runat="server" />
<br />

<h4>Seeker Student Performance</h4>
<sb:AttributeSelectionControl id="SeekerStudentPerformanceControl" runat="server" />
<br />

<h3>Build Scholarship – Funding Specifications</h3>
<sb:AttributeSelectionControl id="FundingSpecifications" runat="server" />
