﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true"
  CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.Default" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="GeneralInfo.ascx" tagname="GeneralInfo" tagprefix="sb" %>
<%@ Register src="SeekerProfile.ascx" tagname="SeekerProfile" tagprefix="sb" %>
<%@ Register src="FundingProfile.ascx" tagname="FundingProfile" tagprefix="sb" %>
<%@ Register src="MatchCriteriaSelection.ascx" tagname="MatchCriteriaSelection" tagprefix="sb" %>
<%@ Register src="AdditionalCriteria.ascx" tagname="AdditionalCriteria" tagprefix="sb" %>
<%@ Register src="Activate.ascx" tagname="Activate" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%= ResolveUrl("~/js/lookupdialog.js") %>" type="text/javascript"></script>
    <link href="<%= ResolveUrl("~/styles/lookupdialog.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
  <div class="controlPanel">
    <asp:CheckBox ID="StepCompletedCheckBox" runat="server" Text="Complete" 
      AutoPostBack="True" oncheckedchanged="StepCompletedCheckBox_CheckedChanged" />

    <sbCommon:SaveConfirmButton ID="PreviousButton" runat="server" ConfirmMessageDivID="confirmSaving" OnClick="PreviousButton_Click" Text="Previous"/>
    <sbCommon:SaveConfirmButton ID="NextButton" runat="server" ConfirmMessageDivID="confirmSaving" OnClick="NextButton_Click" Text="Next" />
    <asp:Button ID="SaveButton" runat="server" OnClick="SaveButton_Click" Text="Save"/>
    <sbCommon:SaveConfirmButton ID="ExitButton" runat="server" ConfirmMessageDivID="confirmSaving" OnClick="ExitButton_Click" Text="Exit" />
  </div>
  <div id="confirmSaving" title="Confirm saving" style="display:none">
    You have change the data. Would you like to save the changes?
  </div>
  <br />
  
  <p><sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HasPrintView="false" /></p>

  <div class="tabs" id="BuildScholarshipWizardTab">
    <ul>
      <li>
        <a href="#tab">
          <asp:Image ID="GeneralInformationCompletedControl" runat="server" ImageUrl="~/images/tick.png" AlternateText="Commpleted" Visible="false" />
          <span>General</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <asp:Image ID="MatchCriteraCompletedControl" runat="server" ImageUrl="~/images/tick.png" AlternateText="Commpleted"  Visible="false"/>
          <span>Selection Criteria</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <asp:Image ID="SeekerProfileCompletedControl" runat="server" ImageUrl="~/images/tick.png" AlternateText="Commpleted" Visible="false" />
          <span>Seeker</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <asp:Image ID="FundingProfileCompletedControl" runat="server" ImageUrl="~/images/tick.png" AlternateText="Commpleted" Visible="false" />
          <span>Funding</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <asp:Image ID="AdditionalCriteriaCompletedControl" runat="server" ImageUrl="~/images/tick.png" AlternateText="Commpleted" Visible="false" />
          <span>+Requirements</span>
        </a>
      </li>
      <li>
        <a href="#tab">
          <asp:Image ID="ActivateCompletedControl" runat="server" ImageUrl="~/images/tick.png" AlternateText="Commpleted" Visible="false" />
          <span>Activate</span>
        </a>
      </li>
    </ul>
    
    <div id="tab">

      <asp:MultiView ID="BuildScholarshipWizard" runat="server"
        OnActiveViewChanged="BuildScholarshipWizard_ActiveStepChanged">
          <asp:View ID="GeneralInfoStep" runat="server">
            <sb:GeneralInfo ID="generalInfo" runat="server" />
          </asp:View>

          <asp:View ID="MatchCriteriaSelectionStep" runat="server">
            <sb:MatchCriteriaSelection ID="matchCriteriaSelection" runat="server" />
          </asp:View>
          
          <asp:View ID="SeekerProfileStep" runat="server" >
            <div class="controlPanel">
              <input id="SeekerProfileStepListChangeButton" type="button" style="float:right" value="List Change" onclick="javascript:window.open('<%= ResolveUrl("~/provider/scholarships/SubmitListChangeRequest.aspx") %>');" />
            </div><br />
            <sb:SeekerProfile ID="seekerProfile" runat="server" />
          </asp:View>
          
          <asp:View ID="FundingProfileStep" runat="server">
            <div class="controlPanel" >
              <input id="FundingProfileStepListChangeButton" type="button" style="float:right" value="List Change" onclick="javascript:window.open('<%= ResolveUrl("~/provider/scholarships/SubmitListChangeRequest.aspx") %>');" />
            </div><br />
            <sb:FundingProfile ID="fundingProfile" runat="server" />
          </asp:View>
          
          <asp:View ID="CriteriaStep" runat="server" >
            <sb:AdditionalCriteria ID="additionalCriteria" runat="server" />
          </asp:View>
          
          <asp:View ID="ActivateScholarshipStep" runat="server">
            <sb:Activate ID="ActivateStep" runat="server" />
          </asp:View>
      </asp:MultiView>
      
      <sbCommon:jQueryTabEvents id="BuildScholarshipWizardTabEvents" runat="server" MultiViewControlID="BuildScholarshipWizard"
            TabControlClientID="BuildScholarshipWizardTab" />
    </div>
  </div>
</asp:Content>
