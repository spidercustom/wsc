﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;
using System.Linq;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class FundingProfile : WizardStepUserControlBase<Scholarship>
    {
        
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public ISupportDAL SupportDAL { get; set; }
        public INeedGapDAL NeedGapDAL { get; set; }
        public ITermOfSupportDAL TermOfSupportDAL { get; set; }

    	private Scholarship ScholarshipInContext
    	{
            get { return Container.GetDomainObject(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        public override void Activated()
        {
            base.Activated();
            SetControlsVisibility();
            SetupAttributeUsageTypeIcons();
        }

        private void SetupAttributeUsageTypeIcons()
        {
            Domain.ScholarshipParts.FundingProfile fundingProfile = ScholarshipInContext.FundingProfile;

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(NeedUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.Need));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SupportSituationUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.SupportedSituation));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FundingParametersUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.FundingParameters));
        }

        private void SetControlsVisibility()
        {
            NeedContainerControl.Visible =
                ScholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.Need);
            SupportSituationContainerControl.Visible =
                ScholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.SupportedSituation);
            FundingParametersContainerControl.Visible =
                ScholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.FundingParameters);


            NoSelectionContainerControl.Visible = !
                                                  (NeedContainerControl.Visible |
                                                   SupportSituationContainerControl.Visible |
                                                   FundingParametersContainerControl.Visible);
        }

        private void PopulateScreen()
        {
            PopulateListControl(NeedGaps, NeedGapDAL.FindAll());
            PopulateListControl(TypesOfSupport, SupportDAL.FindAll());
            PopulateListControl(TermsOfSupportControl, TermOfSupportDAL.FindAll().OrderBy(o => o.Id));
            TermsOfSupportControl.SelectedValue = TermOfSupport.ACADEMIC_YEAR_ID.ToString();

            if (null != ScholarshipInContext.FundingProfile.Need)
            {
                Fafsa.Checked = ScholarshipInContext.FundingProfile.Need.Fafsa;
                UserDerived.Checked = ScholarshipInContext.FundingProfile.Need.UserDerived;
                MinimumSeekerNeed.Amount = ScholarshipInContext.FundingProfile.Need.MinimumSeekerNeed;
                MaximumSeekerNeed.Amount = ScholarshipInContext.FundingProfile.Need.MaximumSeekerNeed;
                NeedGaps.Items.SelectItems(ScholarshipInContext.FundingProfile.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != ScholarshipInContext.FundingProfile.SupportedSituation)
            {
                TypesOfSupport.Items.SelectItems(ScholarshipInContext.FundingProfile.SupportedSituation.TypesOfSupport,
                                                 ts => ts.Id.ToString());
            }

            var fundingParameters = ScholarshipInContext.FundingProfile.FundingParameters;
            if (null != fundingParameters)
            {
                AnnualSupportAmount.Amount = fundingParameters.AnnualSupportAmount;
                MinimumNumberOfAwards.Amount = fundingParameters.MinimumNumberOfAwards;
                MaximumNumberOfAwards.Amount = fundingParameters.MaximumNumberOfAwards;
                if (null != fundingParameters.TermOfSupport)
                    TermsOfSupportControl.SelectedValue = fundingParameters.TermOfSupport.Id.ToString();
            }
        }

        public override void PopulateObjects()
        {
            // Need
            if (null == ScholarshipInContext.FundingProfile.Need)
                ScholarshipInContext.FundingProfile.Need = new DefinitionOfNeed();
            ScholarshipInContext.FundingProfile.Need.Fafsa = Fafsa.Checked;
            ScholarshipInContext.FundingProfile.Need.UserDerived = UserDerived.Checked;
            ScholarshipInContext.FundingProfile.Need.MinimumSeekerNeed = MinimumSeekerNeed.Amount;
            ScholarshipInContext.FundingProfile.Need.MaximumSeekerNeed = MaximumSeekerNeed.Amount;

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => (object)item);
            ScholarshipInContext.FundingProfile.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            ScholarshipInContext.FundingProfile.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            // FundingParameters
            ScholarshipInContext.FundingProfile.FundingParameters.AnnualSupportAmount = AnnualSupportAmount.Amount;
            ScholarshipInContext.FundingProfile.FundingParameters.MinimumNumberOfAwards = (int) MinimumNumberOfAwards.Amount;
            ScholarshipInContext.FundingProfile.FundingParameters.MaximumNumberOfAwards = (int) MaximumNumberOfAwards.Amount;

            var selectedTermOfSupportId = Int32.Parse(TermsOfSupportControl.SelectedValue);
            var selectedTermOfSupport = TermOfSupportDAL.FindById(selectedTermOfSupportId);
            ScholarshipInContext.FundingProfile.FundingParameters.TermOfSupport = selectedTermOfSupport;

            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (ScholarshipInContext.Stage < ScholarshipStages.FundingProfile)
                ScholarshipInContext.Stage = ScholarshipStages.FundingProfile;
        }

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int)WizardStepName.MatchCriteria);
		}

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.FundingProfile;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.FundingProfile;
        }

        public override bool IsCompleted
        {
            get { return ScholarshipInContext.IsStageCompleted(ScholarshipStages.FundingProfile); }
            set
            {
                ScholarshipInContext.MarkStageCompletion(ScholarshipStages.FundingProfile, value);
                ScholarshipService.Save(ScholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public decimal GetMoneyValue(TextBox tb, PropertyProxyValidator validator)
        {
            decimal amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (! Decimal.TryParse(tb.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public int GetIntValue(TextBox tb, PropertyProxyValidator validator)
        {
            int amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (!Int32.TryParse(tb.Text, NumberStyles.Integer | NumberStyles.AllowThousands, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public void PopulateListControl(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }

        #endregion
    }
}