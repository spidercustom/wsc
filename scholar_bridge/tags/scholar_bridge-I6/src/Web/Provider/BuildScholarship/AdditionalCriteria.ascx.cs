﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Brettle.Web.NeatUpload;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class AdditionalCriteria : WizardStepUserControlBase<Scholarship>
	{
		#region DI Properties
		public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        public IAdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
		#endregion

		#region Properties
		private List<Question> Questions
    	{
    		get
    		{
    		    if (ViewState["Questions"] == null)
    		    {
    		        ViewState["Questions"]
    		            = ScholarshipInContext.AdditionalQuestions
    		                .Select(q => new Question
    		                                 {
    		                                     DisplayOrder = q.DisplayOrder, 
                                                 QuestionText = q.QuestionText
    		                                 }).ToList();
    		    }
    		    return (List<Question>)ViewState["Questions"];
    		}
    	}

		private Scholarship ScholarshipInContext
		{
            get { return Container.GetDomainObject(); }
		}

		#endregion

		#region Page Events
		protected void Page_Init(object sender, EventArgs e)
		{
		}


		public void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
            {
                var tooBig = Session["UploadTooBig"];
                if (null != tooBig)
                {
                    fileTooBigLbl.Visible = true;
                    Session.Remove("UploadTooBig");
                }

                PopulateScreen();
				SetButtonsToSkip();
            }
		}

		#endregion

		#region Private Methods

		private void SetButtonsToSkip()
		{
			if (Page is IChangeCheckingPage)
			{
			    var ccPage = (IChangeCheckingPage) Page;
			    ccPage.BypassPromptIds.AddRange(new[]
			                                        {
			                                            "updateQuestionButton",
			                                            "cancelQuestionUpdateButton",
			                                            "editQuestionButton",
			                                            "deleteQuestionButton",
			                                            "addQuestionButton",
			                                            "addEmptyQuestionButton",
			                                            "AttachFile"
			                                        });
			}
		}

		private void PopulateScreen()
        {
            PopulateCheckBoxes(AdditionalRequirements, AdditionalRequirementDAL.FindAll());
            AdditionalRequirements.Items.SelectItems(ScholarshipInContext.AdditionalRequirements, ar => ar.Id.ToString());
            BindAttachedFiles();
        }

        private void BindAttachedFiles()
        {
            attachedFiles.DataSource = ScholarshipInContext.Attachments;
            attachedFiles.DataBind();
        }

        public override void PopulateObjects()
        {
            var selectedAdditionalReqs = AdditionalRequirements.Items.SelectedItems(item => (object)item);
            ScholarshipInContext.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll(selectedAdditionalReqs));

            int qNum = CreateOrUpdateQuestions();
            RemoveDeletedQuestions(qNum);

            if (ScholarshipInContext.Stage < ScholarshipStages.AdditionalCriteria)
                ScholarshipInContext.Stage = ScholarshipStages.AdditionalCriteria;

        }

        private int CreateOrUpdateQuestions()
        {
            var user = UserContext.CurrentUser;

            int qNum = 0;
            foreach (var q in Questions)
            {
            	string questionText = q.QuestionText;
                if (! String.IsNullOrEmpty(questionText))
                {
                    ScholarshipQuestion question;
                    qNum++;
                    if (qNum > ScholarshipInContext.AdditionalQuestions.Count)
                    {
                        question = new ScholarshipQuestion();
                        ScholarshipInContext.AddAdditionalQuestion(question);
                    }
                    else
                    {
                        question = ScholarshipInContext.AdditionalQuestions[qNum - 1];
                    }
                    question.QuestionText = questionText;
                    question.DisplayOrder = qNum;
                    question.LastUpdate = new ActivityStamp(user);
                }
            }
            return qNum;
        }

        private void RemoveDeletedQuestions(int qNum)
        {
            if (qNum < ScholarshipInContext.AdditionalQuestions.Count)
            {
                for (int del = ScholarshipInContext.AdditionalQuestions.Count; del > qNum; del--)
                    ScholarshipInContext.AdditionalQuestions.RemoveAt(del - 1);
            }
		}

		private static void PopulateCheckBoxes(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
		}

		#endregion

		#region Wizard Control Override Methods
		public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.AdditionalCriteria;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.AdditionalCriteria;
        }

        public override bool IsCompleted
        {
            get { return ScholarshipInContext.IsStageCompleted(ScholarshipStages.AdditionalCriteria); }
            set
            {
                ScholarshipInContext.MarkStageCompletion(ScholarshipStages.AdditionalCriteria, value);
                ScholarshipService.Save(ScholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            return true;
		}
        #endregion

		#region Inner Classes

		public class QuestionDataProvider
		{
			private readonly IList<Question> _questions;
			public QuestionDataProvider(IList<Question> questions)
			{
				_questions = questions;
			}
			/// <summary>
			/// method to select all questions for display & edit
			/// </summary>
			/// <returns></returns>
			public IList<Question> Select()
			{
				return _questions;
			}

			/// <summary>
			/// Method used by the GridView to update a given questions text (or remove it).
			/// </summary>
			/// <param name="DisplayOrder"></param>
			/// <param name="QuestionText"></param>
			public void Update(int DisplayOrder, string QuestionText)
			{
				UpdateQuestion(DisplayOrder, QuestionText);
			}

			/// <summary>
			/// Updating may also remove a question if the text goes to string.empty.
			/// </summary>
			/// <param name="DisplayOrder"></param>
			/// <param name="QuestionText"></param>
			private void UpdateQuestion(int DisplayOrder, string QuestionText)
			{
				int removeIndex = -1;
				bool remove = false;
				foreach (var question in _questions)
				{
					removeIndex++;
					if (question.DisplayOrder == DisplayOrder)
					{
						if (QuestionText == null || QuestionText.Trim() == string.Empty)
							remove = true;
						else
							question.QuestionText = QuestionText;

						break;
					}
				}
				if (remove)
				{
					_questions.RemoveAt(removeIndex);
					int i = 0;
					foreach (var q in _questions)
					{
						q.DisplayOrder = ++i;
					}
				}
			}
		}

		[Serializable]
		public class Question
		{
			public int DisplayOrder
			{
				get;
				set;
			}
			public string QuestionText
			{
				get;
				set;
			}
		}
		#endregion

		#region control event handlers
		protected void addQuestionButton_Click(object sender, EventArgs e)
		{
            var questionText = questionsGrid.FooterRow.FindControl("questionAddTextBox") as TextBox;
            AddQuestion(questionText);
		}

		protected void addEmptyQuestionButton_Click(object sender, EventArgs e)
		{
            var questionText = questionsGrid.Controls[0].Controls[0].FindControl("questionEmptyAddTextBox") as TextBox;
            AddQuestion(questionText);
		}

        private void AddQuestion(ITextControl questionText)
        {
            if (questionText.Text != null && questionText.Text.Trim() != string.Empty)
            {
                Questions.Add(new Question { DisplayOrder = Questions.Count + 1, QuestionText = questionText.Text });
                questionsGrid.DataBind();
            }
        }

        protected void UploadFile_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && AttachFile.HasFile)
            {

                //check if a valid filename is being uploaded
                if (String.IsNullOrEmpty(AttachFile.FileName))
                {
                    AttachmentCommentsValidator.IsValid = false;
                    AttachmentCommentsValidator.Text = "Please choose a valid file to attach and try again.";
                    return;
                }

                var attachment = CreateAttachment();
                try
                {
                    AttachFile.MoveTo(attachment.GetFullPath(ConfigHelper.GetAttachmentsDirectory()), MoveToOptions.Overwrite);
                    ScholarshipInContext.Attachments.Add(attachment);
                    ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ScholarshipService.Save(ScholarshipInContext);
                } 
                catch (Exception)
                {
                    attachment.RemoveFile(ConfigHelper.GetAttachmentsDirectory());
                    AttachmentCommentsValidator.IsValid = false;
                    AttachmentCommentsValidator.Text = "Could not upload the file. Please try again.";
                    // FIXME: Display error to user
                    if (ScholarshipInContext.Attachments.Contains(attachment))
                        ScholarshipInContext.Attachments.RemoveAt(ScholarshipInContext.Attachments.Count -1);
                }
                AttachmentComments.Text = null;
                BindAttachedFiles();
            }
        }

        private Attachment CreateAttachment()
        {
            string mimeType = AttachFile.ContentType;
            if (String.IsNullOrEmpty(mimeType))
                mimeType = "application/octet-stream";
            var attachment = new Attachment
                                 {
                                     Name = Path.GetFileName(AttachFile.FileName),
                                     Comment = AttachmentComments.Text,
                                     Bytes = AttachFile.ContentLength,
                                     MimeType = mimeType,
                                     LastUpdate = new ActivityStamp(UserContext.CurrentUser)
                                 };
            attachment.GenerateUniqueName();
            return attachment;
        }

		protected void attachedFiles_OnItemDeleting(object sender, ListViewDeleteEventArgs e)
		{
            if (ScholarshipInContext.Attachments.Count > e.ItemIndex)
            {
                var attachment = ScholarshipInContext.Attachments[e.ItemIndex];
                if (null != attachment)
                {
                    ScholarshipInContext.Attachments.RemoveAt(e.ItemIndex);

                    ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ScholarshipService.Save(ScholarshipInContext);

                	attachment.RemoveFile(ConfigHelper.GetAttachmentsDirectory());
                }
            }
		    BindAttachedFiles();
		}

		protected void QuestionSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
		{
			e.ObjectInstance = new QuestionDataProvider(Questions);
		}
		#endregion

	}
}
