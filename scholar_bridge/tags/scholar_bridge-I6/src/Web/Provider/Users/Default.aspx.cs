﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Provider.Users
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;

            orgUserList.Users = provider.ActiveUsers;
            orgInactiveUserList.Users = provider.DeletedUsers;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
        	if (!User.IsInRole(Role.PROVIDER_ADMIN_ROLE))
        	{
        		createUserLnk.Visible = false;
        	}
        }

    }
}