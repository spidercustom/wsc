﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="ScholarBridge.Web.Provider.Relationships.Create" Title="Provider | Relationships | Create " %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<p>Place keeper for WSC provided text explaining the ScholarBridge process for establishing a relationship including contacting the organization before submitting this request</p>    
<br />
<h3>Authorize Intermediary to Build New Scholarships?</h3>
<br />
<label for="orgList" >Select Organization</label>
<asp:DropDownList ID="orgList" runat="server"></asp:DropDownList>
<asp:RequiredFieldValidator runat="server" ID="validateorganization" ControlToValidate="orgList" />
<br />
<asp:Button ID="saveBtn" runat="server" Text="Yes"  
        onclick="saveBtn_Click" UseSubmitBehavior="False"  /> 
<asp:Button ID="cancelBtn" runat="server" Text="No" CausesValidation="false" 
        onclick="cancelBtn_Click" />
</asp:Content>
