using System;
using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ApplicationServiceTests
    {
        private MockRepository mocks;
        private ApplicationService applicationService;
        private IApplicationDAL applicationDal;
        private IMatchService matchService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            applicationDal = mocks.StrictMock<IApplicationDAL>();
            matchService = mocks.StrictMock<IMatchService>();

            applicationService = new ApplicationService
                                {
                                    ApplicationDAL = applicationDal,
                                    MatchService = matchService
                                };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(applicationDal);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void submit_throws_exception_if_null_passed()
        {
            applicationService.SubmitApplication(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_new_throws_exception_if_null_passed()
        {
            applicationService.SaveNew(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void update_throws_exception_if_null_passed()
        {
            applicationService.Update(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void finalist_throws_exception_if_null_passed()
        {
            applicationService.Finalist(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void findbyseekerandscholarship_throws_exception_if_null_seeker_passed()
        {
            applicationService.FindBySeekerandScholarship(null, new Scholarship());
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void findbyseekerandscholarship_throws_exception_if_null_scholarship_passed()
        {
            applicationService.FindBySeekerandScholarship(new Seeker(), null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void notfinalist_throws_exception_if_null_passed()
        {
            applicationService.NotFinalist(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void count_throws_exception_if_null_passed()
        {
            applicationService.CountAllSubmitted(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void countSubmittedbyseeker_throws_exception_if_null_passed()
        {
            applicationService.CountAllSubmittedBySeeker(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_all_finalistsr_throws_exception_if_null_passed()
        {
            applicationService.FindAllFinalists(null);
            Assert.Fail();
        }

        [Test]
        public void submit_application_updates_status_and_date()
        {
            var app = new Application
                             {
                                Seeker = new Seeker(),
                                Scholarship = new Scholarship()
                             };
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.SubmitApplication(app);

            mocks.VerifyAll();
            Assert.AreEqual(ApplicationStages.Submitted, app.Stage);
            Assert.IsTrue(app.SubmittedDate.HasValue);
        }

        [Test]
        public void save_new_calls_insert()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };
            Expect.Call(applicationDal.Insert(app)).Return(app);
            Expect.Call(() => matchService.ApplyForMatch(app.Seeker, app.Scholarship.Id, app));
            mocks.ReplayAll();

            applicationService.SaveNew(app);

            mocks.VerifyAll();
        }
        
        [Test]
        public void finalist_when_not_finalist_updates_application()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.Finalist(app);

            mocks.VerifyAll();
            Assert.IsTrue(app.Finalist);
        }

        [Test]
        public void finalist_by_id_when_not_finalist_updates_application()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };
            Expect.Call(applicationDal.FindById(1)).Return(app);
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.Finalist(1);

            mocks.VerifyAll();
            Assert.IsTrue(app.Finalist);
        }

        [Test]
        public void finalist_when_finalist_does_not_update_application()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
                Finalist = true
            };
            mocks.ReplayAll();

            applicationService.Finalist(app);

            mocks.VerifyAll();
            Assert.IsTrue(app.Finalist);
        }

        [Test]
        public void notfinalist_when_finalist_updates_application()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
                Finalist = true
            };
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.NotFinalist(app);

            mocks.VerifyAll();
            Assert.IsFalse(app.Finalist);
        }

        [Test]
        public void not_finalist_by_id_when_not_finalist_updates_application()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
                Finalist = true
            };
            Expect.Call(applicationDal.FindById(1)).Return(app);
            Expect.Call(applicationDal.Update(app)).Return(app);
            mocks.ReplayAll();

            applicationService.NotFinalist(1);

            mocks.VerifyAll();
            Assert.IsFalse(app.Finalist);
        }

        [Test]
        public void not_finalist_when_not_finalist_does_not_update_application()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            mocks.ReplayAll();

            applicationService.NotFinalist(app);

            mocks.VerifyAll();
            Assert.IsFalse(app.Finalist);
        }

        [Test]
        public void count_delegates_to_dal()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            Expect.Call(applicationDal.CountAllSubmitted(app.Scholarship)).Return(2);
            mocks.ReplayAll();

            applicationService.CountAllSubmitted(app.Scholarship);

            mocks.VerifyAll();
        }

        [Test]
        public void countSubmittedBySeeker_delegates_to_dal()
        {
            var app = new Application
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship(),
            };
            Expect.Call(applicationDal.CountAllSubmittedBySeeker(app.Seeker)).Return(2);
            mocks.ReplayAll();

            applicationService.CountAllSubmittedBySeeker(app.Seeker);

            mocks.VerifyAll();
        }

        [Test]
        public void find_all_finalists_delegates_to_dal()
        {

            var s = new Scholarship();
            var apps = new List<Application>();
            Expect.Call(applicationDal.FindAllFinalists(s)).Return(apps);
            mocks.ReplayAll();

            applicationService.FindAllFinalists(s);

            mocks.VerifyAll();
        }
    }
}