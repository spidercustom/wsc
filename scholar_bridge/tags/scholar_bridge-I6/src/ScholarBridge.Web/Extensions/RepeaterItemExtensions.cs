using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Extensions
{
    public static class RepeaterItemExtensions
    {
        public static bool IsRow(this RepeaterItem item)
        {
            return item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem;
        }
    }
}