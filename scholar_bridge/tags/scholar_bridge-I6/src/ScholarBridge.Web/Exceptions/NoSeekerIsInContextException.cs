﻿using System;

namespace ScholarBridge.Web.Exceptions
{
    [global::System.Serializable]
    public class NoSeekerIsInContextException : ApplicationException //TODO: Do we need ScholarBridge exception type?
    {
        public NoSeekerIsInContextException() { }
        public NoSeekerIsInContextException(string message) : base(message) { }
        public NoSeekerIsInContextException(string message, Exception inner) : base(message, inner) { }
        protected NoSeekerIsInContextException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
