﻿
using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    public enum ScholarshipStages
    {
        None,
        [DisplayName("General information")]
        GeneralInformation,
        [DisplayName("Selection criteria")]
        MatchCriteriaSelection,
        [DisplayName("Seeker profile criteria")]
        SeekerProfile,
        [DisplayName("Funding profile criteria")]
        FundingProfile,
		[DisplayName("+Requirements")]
        AdditionalCriteria,
        PreviewCandidatePopulation,
        BuildScholarshipMatchLogic,
        BuildScholarshipRenewalCriteria,
        [DisplayName("Activation requested")]
        RequestedActivation,
        Activated,
        Rejected,
        Awarded
    }


    
}
