using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace ScholarBridge.Domain.Lookup
{
    /// <summary>
    /// Specifiys a range of the percentage of remaining need.
    /// </summary>
    /// <remarks>
    /// MinPercent and MaxPercent are represented as a float value of 0 to 1 inclusive.
    /// </remarks>
    public class NeedGap : LookupBase
    {
        private const string PERCENT_FORMAT = "#0%";

        [RangeValidator(0, RangeBoundaryType.Inclusive, 1, RangeBoundaryType.Inclusive)]
        public virtual float MinPercent { get; set; }

        [RangeValidator(0, RangeBoundaryType.Inclusive, 1, RangeBoundaryType.Inclusive)]
        [PropertyComparisonValidator("MinPercent", ComparisonOperator.GreaterThan)]
        public virtual float MaxPercent { get; set; }

        public virtual string MinPercentFormatted
        {
            get { return MinPercent.ToString(PERCENT_FORMAT); }
        }

        public virtual string MaxPercentFormatted
        {
            get { return MaxPercent.ToString(PERCENT_FORMAT); }
        }
    }
}