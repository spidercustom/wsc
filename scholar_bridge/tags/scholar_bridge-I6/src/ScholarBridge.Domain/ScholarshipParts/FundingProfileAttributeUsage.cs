﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingProfileAttributeUsage
    {
        public virtual FundingProfileAttribute Attribute { get; set; }
        public virtual ScholarshipAttributeUsageType UsageType { get; set; }
    }
}
