﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Contact;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipDonor : ICloneable
    {
        public ScholarshipDonor()
        {
            Name = new PersonName();
            Address = new Address();
            Phone = new PhoneNumber();
        }

        public virtual PersonName Name { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual Address Address { get; set; }

        // ASP.NET passes an empty string from a form in the PropertyProxyValidator
        // The empty string fails the EmailValidator (where a null would have passed since it's not checked)
        // which means we have to do this for any optional, but validated fields to get PropertyProxyValidator
        // to work (and we would have to have TextBox.Text check for empty string and convert them to nulls on data binding out of the form)
        // Yes, this is insane.
        [ValidatorComposition(CompositionType.Or, MessageTemplate = "The email address '{0}' is invalid.")]
        [StringLengthValidator(0, 0)]
        [EmailValidator]
        public virtual string Email { get; set; }

        public virtual object Clone()
        {
            return MemberwiseClone();
        }
    }
}
