using System;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class AcademicYear
    {
        public AcademicYear() { }
        public AcademicYear(int year) 
        {
            Year = year; 
        }

        /// <summary>
        /// Year is the year that the school year ends.
        /// This is the same as the Class of N or Fiscal Year X, they both relate to the end date.
        /// </summary>
        /// <example>
        /// AcademicYear 2009 - 2010: Class of 2010
        /// </eexample>
        public virtual int Year { get; set; }

        public virtual int YearStart
        {
            get { return Year - 1; }
        }

        public virtual int YearEnd
        {
            get { return Year; }
        }

        public virtual string DisplayName { get { return string.Format("{0} - {1}", YearStart, YearEnd); } }

        public override string ToString() 
        {
            return DisplayName;
        }

        public static AcademicYear CurrentScholarshipYear
        {
            get { return ScholarshipYearForDate(DateTime.Now); }
        }

        /// <summary>
        /// For this date, what AcedemicYear is currently active
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static AcademicYear YearForDate(DateTime date)
        {
            var currentMonth = date.Month;
            var currentYear = date.Year;
            if (currentMonth >= 9 && currentMonth <= 12)
            {
                return new AcademicYear { Year = currentYear + 1 };
            }
            return new AcademicYear { Year = currentYear };
        }

        /// <summary>
        /// For this date, what AcademicYear are scholarships being collected for
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static AcademicYear ScholarshipYearForDate(DateTime date)
        {
            var currentMonth = date.Month;
            var currentYear = date.Year;
            if (currentMonth >= 1 && currentMonth <= 8)
            {
                return new AcademicYear {Year = currentYear + 1};
            }
            return new AcademicYear {Year = currentYear + 2};
        }

        public override bool Equals(object obj)
        {
            var ay = obj as AcademicYear;
            return Equals(ay);
        }

        public bool Equals(AcademicYear obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.Year == Year;
        }

        public override int GetHashCode()
        {
            return Year;
        }
    }
}