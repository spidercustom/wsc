﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(145)]
    public class RenameCommunityInvolvementAndCauseRT : Migration
    {
        #region scholarship
        private const string NEW_SCHOLARSHIP_RT_TABLE_NAME = "SBScholarshipCommunityServiceRT";
        private const string OLD_SCHOLARSHIP_RT_TABLE_NAME = "SBScholarshipCommunityInvolvementCauseRT";
        #endregion

        #region seeker
        private const string NEW_SEEKER_RT_TABLE_NAME = "SBSeekerCommunityServiceRT";
        private const string OLD_SEEKER_RT_TABLE_NAME = "SBSeekerCommunityInvolvementCauseRT";
        #endregion

        public override void Up()
        {
            Database.RenameTable(OLD_SCHOLARSHIP_RT_TABLE_NAME, NEW_SCHOLARSHIP_RT_TABLE_NAME);
            Database.RenameColumn(
                NEW_SCHOLARSHIP_RT_TABLE_NAME, 
                RenameCommunityInvolvementAndCauseLUT.OLD_LOOKUP_TABLE_ID_COLUMN_NAME,
                RenameCommunityInvolvementAndCauseLUT.NEW_LOOKUP_TABLE_ID_COLUMN_NAME);

            Database.RenameTable(OLD_SEEKER_RT_TABLE_NAME, NEW_SEEKER_RT_TABLE_NAME);
            Database.RenameColumn(
                NEW_SEEKER_RT_TABLE_NAME,
                RenameCommunityInvolvementAndCauseLUT.OLD_LOOKUP_TABLE_ID_COLUMN_NAME,
                RenameCommunityInvolvementAndCauseLUT.NEW_LOOKUP_TABLE_ID_COLUMN_NAME);
        }

        public override void Down()
        {
            Database.RenameColumn(
                NEW_SCHOLARSHIP_RT_TABLE_NAME,
                RenameCommunityInvolvementAndCauseLUT.NEW_LOOKUP_TABLE_ID_COLUMN_NAME,
                RenameCommunityInvolvementAndCauseLUT.OLD_LOOKUP_TABLE_ID_COLUMN_NAME);
            Database.RenameTable(NEW_SCHOLARSHIP_RT_TABLE_NAME, OLD_SCHOLARSHIP_RT_TABLE_NAME);

            Database.RenameColumn(
                NEW_SEEKER_RT_TABLE_NAME,
                RenameCommunityInvolvementAndCauseLUT.NEW_LOOKUP_TABLE_ID_COLUMN_NAME,
                RenameCommunityInvolvementAndCauseLUT.OLD_LOOKUP_TABLE_ID_COLUMN_NAME);
            Database.RenameTable(NEW_SEEKER_RT_TABLE_NAME, OLD_SEEKER_RT_TABLE_NAME);
        }
    }
}