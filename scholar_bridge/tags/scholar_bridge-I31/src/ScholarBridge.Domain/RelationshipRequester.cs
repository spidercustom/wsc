﻿
namespace ScholarBridge.Domain
{
    public enum RelationshipRequester
    {
        Provider,
        Intermediary
    }
}
