<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="SubmitListChangeRequest.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.SubmitListChangeRequest" Title="Submit List Change" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<%@ Register src="~/Common/ListChangeRequest.ascx" tagname="ListChangeRequest" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>            
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<sb:ListChangeRequest ID="ListChangeRequest1" runat="server" />
 
<sbCommon:AnchorButton ID="saveBtn" runat="server" Text="Submit"  
        onclick="saveBtn_Click" /> 
<sbCommon:AnchorButton ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" 
        onclick="cancelBtn_Click" />           
</asp:Content>
