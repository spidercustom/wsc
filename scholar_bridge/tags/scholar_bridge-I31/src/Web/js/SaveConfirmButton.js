﻿function SaveAndContinue(controlId, causesValidation, validationGroup) {
    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlId, 'True', causesValidation, validationGroup, '', false, true));
}

function CancelSavingAndContinue(controlId, causesValidation, validationGroup) {
    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlId, 'False', causesValidation, validationGroup, '', false, true));
}

function SavingYesNoCancelDialog(divId, controlId, validateEvenWhenNoChange, causesValidation, validationGroup) {
    if (checkForChanges()) {
        $("#" + divId).dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            buttons: {
                "Yes": function() {
                    $(this).dialog("destroy");
                    SaveAndContinue(controlId, causesValidation, validationGroup);
                },
                "No": function() {
                    $(this).dialog("destroy");
                    CancelSavingAndContinue(controlId, causesValidation, validationGroup);
                },
                "Cancel": function() {
                    $(this).dialog("destroy");
                }
            }
        });
    }
    else {
        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(controlId, validateEvenWhenNoChange, causesValidation, validationGroup, '', false, true));
    };
}