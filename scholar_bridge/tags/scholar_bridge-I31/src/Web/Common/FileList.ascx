﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileList.ascx.cs" Inherits="ScholarBridge.Web.Common.FileList" %>


    <asp:MultiView ID="ViewContainer" runat="server" ActiveViewIndex="0">
        <asp:View ID="DetailView" runat="server">
            <table class="viewonlyTable">
                <thead>
                    <tr>
                        <th>File</th>
                        <th>Size</th>
                        <th>Type</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                <asp:Repeater ID="DetailViewRepeater" runat="server" >
                    <ItemTemplate>
                        <tr>
                            <td><asp:LinkButton ID="downloadAttachmentBtn" runat="server" Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("Id") %>'
                                OnCommand="downloadAttachmentBtn_OnCommand"/></td>
                            <td><%# Eval("DisplaySize") %></td>
                            <td><%# Eval("MimeType") %></td>
                            <td><%# Eval("Comment")%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                </tbody>
            </table>
        </asp:View>
        <asp:View ID="ListView" runat="server">
            <table class="viewonlyTable">
            <asp:Repeater ID="ListViewRepeater" runat="server" >
                <ItemTemplate>
                    <tr>
                        <th>Attachments:</th>
                        <td>
                            <asp:LinkButton ID="downloadAttachmentBtn" runat="server" Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("Id") %>'
                            OnCommand="downloadAttachmentBtn_OnCommand"/>
                        </td>
                    </tr>                            
                    <tr>                            
                        <th>Comments:</th>
                        <td><%# Eval("Comment")%></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </table>
        </asp:View>
    </asp:MultiView>
