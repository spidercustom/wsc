﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditScholarshipSchedule.ascx.cs" Inherits="ScholarBridge.Web.Common.EditScholarshipSchedule" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
 <%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>            
<%@ Register src="CalendarControl.ascx" tagname="CalendarControl" tagprefix="uc1" %>
<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<div class="form-iceland-container">
    <div class="form-iceland">
     <P class="form-section-title">Schedule</P>
     
        <label for="calAppStart">Application Start Date</label>
        <uc1:calendarcontrol ID="calApplicationStartDate" runat="server">
        </uc1:calendarcontrol>
        <sb:CoolTipInfo ID="CoolTipInfo6" runat="Server" Content="This date notifies seekers that you are accepting scholarship applications for the current academic year. If scholarship applications are accepted year round, then use today’s date. Applications can be submitted as soon as the scholarship has been activated." />  
        <elv:PropertyProxyValidator ID="ApplicationStartDateValidator" runat="server" ControlToValidate="calApplicationStartDate"
            PropertyName="ApplicationStartDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        
    
        <br />
        <label for="calApplicationDueDate">
            Application Due Date</label>
       <uc1:calendarcontrol ID="calApplicationDueDate" runat="server">
        </uc1:calendarcontrol>
        <sb:CoolTipInfo ID="CoolTipInfo7" runat="Server" Content="This is the date the scholarship applications are due.  Applications will not be accepted on-line after this date. Once the scholarship is activated, applications are notified that they have until this date to submit applications. The date can only be extended after scholarship is activated." />  
            <elv:PropertyProxyValidator ID="ApplicationDueDateValidator" runat="server" ControlToValidate="calApplicationDueDate"
            PropertyName="ApplicationDueDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
        <asp:CustomValidator ID="DueDateCustomValidator" runat="server" ErrorMessage="Application due date can't be earlier than current due date." ControlToValidate="calApplicationDueDate"  OnServerValidate="DueDateValidator_OnServerValidate"/>
       
         <br />
        
        <label for="calAwardDate">
            Anticipated Award Date:</label>
      
        <uc1:calendarcontrol ID="calAwardDate" runat="server"></uc1:calendarcontrol>
        <sb:CoolTipInfo ID="CoolTipInfo8" runat="Server" Content="This is the date the provider anticipates notifying scholarship awardees. For information only." />  
     <elv:PropertyProxyValidator ID="AwardDateValidator" runat="server" ControlToValidate="calAwardDate"
            PropertyName="AwardDate" SourceTypeName="ScholarBridge.Domain.Scholarship" />
     
        <br />
        <br />
    <sbCommon:AnchorButton ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click"  />
    <sbCommon:AnchorButton ID="cancelButton" runat="server" Text="Cancel" onclick="cancelButton_Click" />
    <br />
    </div>
    </div> 

 