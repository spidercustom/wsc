﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    public partial class MainMenu : UserControl
    {
        
        public IUserContext UserContext { get; set; }
        private string ContextUrl
        {
            get { return Request.Url.AbsoluteUri ?? String.Empty; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserContext.CurrentUser ==null)
            {
                SetMenuForAnonymousUser();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            MainMenuHelper.ApplyActiveMenuImage(this);
        }

        private void SetMenuForAnonymousUser()
        {
            SeekerPanel.Visible = false;
            ProviderPanel.Visible = false;
            IntermediaryPanel.Visible = false;
            if (ContextUrl.Contains("/Provider/"))
            {
                ProviderPanel.Visible = true;
            }
            else if (ContextUrl.Contains("/Intermediary/"))
            {
                IntermediaryPanel.Visible = true;
            }
            else
            {
                SeekerPanel.Visible = true;
            }
        }
    }
}

