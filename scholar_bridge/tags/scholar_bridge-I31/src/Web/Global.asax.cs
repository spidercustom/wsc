﻿using System;
using System.Web;
using Common.Logging;
using ScholarBridge.Web.Exceptions;

namespace ScholarBridge.Web
{
    public class Global : HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Global));

        protected void Application_Start(object sender, EventArgs e)
        {
            log.Info("**** Application starting ****");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            log.Debug("******************** New Page ********************");
            log.Debug(Context.Request.Path);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Context.Error.GetBaseException();
            if (ex is NoUserIsInContextException || ex is NoProviderIsInContextException || ex is NoIntermediaryIsInContextException)
            {
                log.Warn("User context not setup properly", ex);
                Response.Redirect("~/login.aspx?ReturnUrl=" + Server.UrlEncode(Request.Url.PathAndQuery));
                Context.ClearError();
            }
            else if (ex.GetBaseException() is System.Web.HttpRequestValidationException)
            {
                
                Response.Redirect("~/RequestError.aspx");
                Context.ClearError();
            }
            else
            {
                Server.Transfer("~/Error.aspx");
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            log.Info("**** Application ending ****");
        }
    }
}