﻿using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class RelationshipDALTest : TestBase
    {
        public RelationshipDAL RelationshipDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public IntermediaryDAL IntermediaryDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        private User user;
        private Provider provider;
        private Intermediary intermediary;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            intermediary = IntermediaryDALTest.InsertIntermediary(IntermediaryDAL, StateDAL, "TestIntermediary", user);
        }

        [Test]
        public void find_by_provider()
        {
            var newRelationship = InsertRelationship(CreateTestObject());

            Relationship relationship = RelationshipDAL.FindByProvider(newRelationship.Provider)[0];
            Assert.IsNotNull(relationship);

            var relationshipList = RelationshipDAL.FindByProvider(new Provider(){Id=12,Name="Nonexistingprovider"});
            Assert.AreEqual(0, relationshipList.Count);
        }

        [Test]
        public void find_by_intermediary()
        {
            var newRelationship = InsertRelationship(CreateTestObject());

            Relationship relationship = RelationshipDAL.FindByIntermediary(newRelationship.Intermediary)[0];
            Assert.IsNotNull(relationship);

            var relationshipList = RelationshipDAL.FindByIntermediary(new Intermediary() { Id = 12, Name = "NonexistingIntermediary" });
            Assert.AreEqual(0, relationshipList.Count);
        }

        [Test]
        public void find_by_provider_and_intermediary()
        {
            var newRelationship = InsertRelationship(CreateTestObject());

            var relationship = RelationshipDAL.FindByProviderandIntermediary(newRelationship.Provider,newRelationship.Intermediary);
            Assert.IsNotNull(relationship);

            var relationshipnonexistent = RelationshipDAL.FindByProviderandIntermediary(newRelationship.Provider, new Intermediary() { Id = 12, Name = "NonexistingIntermediary" });
            Assert.IsNull(relationshipnonexistent);
        }


        [Test]
        public void can_find_by_id()
        {
            var newRelationship = InsertRelationship(CreateTestObject());
            Relationship retrivedRelationship = RelationshipDAL.FindById(newRelationship.Id);
            Assert.AreEqual(newRelationship.Id, retrivedRelationship.Id);
        }

        [Test]
        public void can_create_Relationship()
        {
            var newRelationship = InsertRelationship(CreateTestObject());
            Assert.IsNotNull(newRelationship);
            Assert.AreNotEqual(0, newRelationship.Id);
        }

        [Test]
        public void can_update_Relationship()
        {
            var newRelationship = InsertRelationship(CreateTestObject());
            Assert.IsNotNull(newRelationship);
            Assert.AreNotEqual(0, newRelationship.Id);

            newRelationship.Status = RelationshipStatus.InActive;
            
            RelationshipDAL.Save(newRelationship);

            var foundRelationship = RelationshipDAL.FindById(newRelationship.Id);

            Assert.IsNotNull(foundRelationship);

            Assert.IsTrue(foundRelationship.Status==RelationshipStatus.InActive);
        }

        [Test]
        public void find_intermediaries_without_relationship()
        {
            var newRelationship = InsertRelationship(CreateTestObject());   // Handle the intermediary created on setup
            var initialCount = RelationshipDAL.FindNotRelatedByProvider(provider).Count;    // Now count the non-related

            var newIntermediary = IntermediaryDALTest.InsertIntermediary(IntermediaryDAL, StateDAL, "New Intermediary",user);   // Add a related

            var notRelated = RelationshipDAL.FindNotRelatedByProvider(provider);
            Assert.AreEqual(initialCount + 1, notRelated.Count);    // Should be initial non-related + the one added
        }

        [Test]
        public void find_providers_without_relationship()
        {
            var newRelationship = InsertRelationship(CreateTestObject());   // Handle the intermediary created on setup
            var initialCount = RelationshipDAL.FindNotRelatedByIntermediary(intermediary).Count;    // Now count the non-related

            var newProvider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "New Provider", user);   // Add a related

            var notRelated = RelationshipDAL.FindNotRelatedByIntermediary(intermediary);
            Assert.AreEqual(initialCount + 1, notRelated.Count);    // Should be initial non-related + the one added
        }

        public Relationship CreateTestObject()
        {
            return CreateTestObject(user, provider, intermediary, RelationshipStatus.Pending);
        }

        public static Relationship CreateTestObject(User user, Provider provider,Intermediary intermediary, RelationshipStatus status)
        {
            var result = new Relationship
            {
               
                Provider = provider,
                Intermediary = intermediary,
                Requester = RelationshipRequester.Provider,
                LastUpdate = new ActivityStamp(user),
                Status= status,
                RequestedOn = DateTime.Today
                 
            };
            return result;
        }

        public Relationship InsertRelationship(Relationship relationship)
        {
            return RelationshipDAL.Save(relationship);
        }
    }
}
