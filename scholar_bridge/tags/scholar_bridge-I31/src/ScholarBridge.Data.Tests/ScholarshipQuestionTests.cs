﻿using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
	[TestFixture]
	public class ScholarshipQuestionTests : TestBase
	{

		public ProviderDAL ProviderDAL { get; set; }
		public UserDAL UserDAL { get; set; }
		public StateDAL StateDAL { get; set; }
		public ScholarshipDAL ScholarshipDAL { get; set; }

		private User user;
		private Provider provider;
		private ScholarshipStages stage;
		private Scholarship scholarship;

		protected override void OnSetUpInTransaction()
		{
			user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
			provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
			stage = ScholarshipStages.NotActivated;
			scholarship = ScholarshipDAL.Insert(ScholarshipDALTest.CreateTestObject(user, provider, stage));
		}

		[Test]
		public void can_create_scholarship_question()
		{
			InsertScholarshipQuestion(CreateTestObject());

            var s = ScholarshipDAL.FindById(scholarship.Id);
			Assert.IsNotNull(s.AdditionalQuestions);
            Assert.AreNotEqual(0, s.AdditionalQuestions[0].Id);
		}

		[Test]
		public void can_find_questions_by_scholarship()
		{
			var scholarshipQuestion = CreateTestObject();
			var scholarshipQuestion2 = CreateTestObject();
			scholarshipQuestion2.QuestionText = "What is truth?";
			InsertScholarshipQuestion(scholarshipQuestion);
			InsertScholarshipQuestion(scholarshipQuestion2);

		    var s = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(s);
            Assert.IsNotNull(s.AdditionalQuestions);
            Assert.AreEqual(2, s.AdditionalQuestions.Count);
		}

        [Test]
        public void can_remove_questions_from_scholarship()
        {
            var scholarshipQuestion = CreateTestObject();
            var scholarshipQuestion2 = CreateTestObject();
            scholarshipQuestion2.QuestionText = "What is truth?";
            InsertScholarshipQuestion(scholarshipQuestion);
            InsertScholarshipQuestion(scholarshipQuestion2);

            var s = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(s);
            Assert.IsNotNull(s.AdditionalQuestions);
            Assert.AreEqual(2, s.AdditionalQuestions.Count);

            s.RemoveAdditionalQuestion(0);
            s.RemoveAdditionalQuestion(0);
            ScholarshipDAL.Update(scholarship);

            var s2 = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(s2);
            Assert.IsNotNull(s2.AdditionalQuestions);
            Assert.AreEqual(0, s2.AdditionalQuestions.Count);
        }

		public void AssertScholarshipQuestionsAreSame(ScholarshipQuestion expected, ScholarshipQuestion actual)
		{
			Assert.AreEqual(expected.Id, actual.Id);
		}

		public ScholarshipQuestion CreateTestObject()
		{
			return CreateTestObject(scholarship, user);
		}

		public static ScholarshipQuestion CreateTestObject(Scholarship scholarship, User user)
		{
			var result = new ScholarshipQuestion
			             	{
			             		QuestionText = "What is your favorite color?",
			             		DisplayOrder = 1,
								LastUpdate = new ActivityStamp(user)
							};
			return result;
		}


		public void InsertScholarshipQuestion(ScholarshipQuestion scholarshipQuestion)
		{
            scholarship.AddAdditionalQuestion(scholarshipQuestion);
		    ScholarshipDAL.Update(scholarship);
		}
	}
}