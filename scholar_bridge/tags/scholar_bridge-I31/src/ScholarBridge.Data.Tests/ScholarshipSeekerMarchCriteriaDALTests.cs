﻿using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipSeekerMarchCriteriaDALTests : TestBase
    {
        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        private User user;
        private Provider provider;
        private ScholarshipStages stage;
        private Scholarship scholarship;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            stage = ScholarshipStages.NotActivated;

            scholarship = ScholarshipDALTest.CreateTestObject(user, provider, stage);
        }

        [Test]
        public void test_enums_gets_save_and_load()
        {
            const StudentGroups studentGroups = StudentGroups.AdultFirstTime | StudentGroups.HighSchoolSenior;
            const SchoolTypes schoolTypes = SchoolTypes.PublicUniversityOrCollege| SchoolTypes.Seminary;
            const AcademicPrograms academicPrograms = AcademicPrograms.AdvancedDegree | AcademicPrograms.Undergraduate;
            const SeekerStatuses seekerStatuses = SeekerStatuses.PartTime | SeekerStatuses.FullTime;
            const ProgramLengths programLengths = ProgramLengths.FourYears | ProgramLengths.TwoYears;
            const Genders selectedGenders = Genders.Male | Genders.Female;

            scholarship.SeekerProfileCriteria.StudentGroups = studentGroups;
            scholarship.SeekerProfileCriteria.SchoolTypes = schoolTypes;
            scholarship.SeekerProfileCriteria.AcademicPrograms = academicPrograms;
            scholarship.SeekerProfileCriteria.SeekerStatuses = seekerStatuses;
            scholarship.SeekerProfileCriteria.ProgramLengths = programLengths;
            scholarship.SeekerProfileCriteria.Genders = selectedGenders;

            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            
            Assert.AreEqual(selectedGenders, scholarship.SeekerProfileCriteria.Genders);
            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.AreEqual(studentGroups, scholarship.SeekerProfileCriteria.StudentGroups);
            Assert.AreEqual(schoolTypes,scholarship.SeekerProfileCriteria.SchoolTypes);
            Assert.AreEqual(academicPrograms,scholarship.SeekerProfileCriteria.AcademicPrograms);
            Assert.AreEqual(seekerStatuses,scholarship.SeekerProfileCriteria.SeekerStatuses);
            Assert.AreEqual(programLengths,scholarship.SeekerProfileCriteria.ProgramLengths);
            Assert.AreEqual(selectedGenders, scholarship.SeekerProfileCriteria.Genders);
        }

        [Test]
        public void test_hobbies_gets_saved_and_load()
        {
            var hobby = CreateLookupObjectOfType<SeekerHobby>("Walking and chewing gum");

            scholarship.SeekerProfileCriteria.Hobbies.Add(hobby);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Hobbies);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.Hobbies.Count);
        }

        [Test]
        public void test_College_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<College>("College");

            scholarship.SeekerProfileCriteria.Colleges.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Colleges);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.Colleges.Count);
        }

        [Test]
        public void test_Ethnicity_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<Ethnicity>("Ethnicity");

            scholarship.SeekerProfileCriteria.Ethnicities.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Ethnicities);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.Ethnicities.Count);
        }

        [Test]
        public void test_Religion_gets_saved_and_load()
        {
            var word = CreateLookupObjectOfType<Religion>("Religion");

            scholarship.SeekerProfileCriteria.Religions.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Religions);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.Religions.Count);
        }

        [Test]
        public void test_County_gets_saved_and_load()
        {
            var WA = StateDAL.FindByAbbreviation("WA");
            var word = CreateLookupObjectOfType<County>("County", o => o.State = WA);

            scholarship.SeekerProfileCriteria.Counties.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Counties);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.Counties.Count);
        }

        [Test]
        public void test_City_gets_saved_and_load()
        {
            var WA = StateDAL.FindByAbbreviation("WA");
            var word = CreateLookupObjectOfType<City>("City", o => o.State = WA);

            scholarship.SeekerProfileCriteria.Cities.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Cities);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.Cities.Count);
        }

        [Test]
        public void test_SchoolDistrict_gets_saved_and_load()
        {
            var WA = StateDAL.FindByAbbreviation("WA");
            var word = CreateLookupObjectOfType<SchoolDistrict>("SchoolDistrict", o => o.State = WA);


            scholarship.SeekerProfileCriteria.SchoolDistricts.Add(word);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.SchoolDistricts);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.SchoolDistricts.Count);
        }

        private T CreateLookupObjectOfType<T>(string name) where T : ILookup
        {
            return CreateLookupObjectOfType<T>(name, null);
        }

        private T CreateLookupObjectOfType<T>(string name, Action<T> beforeInsert) where T : ILookup
        {
            var word = LookupTestBase<LookupDAL<T>, T>.CreateLookupObject(name, user);
            var dal = LookupTestBase<LookupDAL<T>, T>.RetrieveDAL();
            if (null != beforeInsert)
                beforeInsert(word);
            dal.Insert(word);

            return word;
        }

        [Test]
        public void test_organization_gets_saved_and_load()
        {
            var organization = LookupTestBase<LookupDAL<SeekerMatchOrganization>, SeekerMatchOrganization>.CreateLookupObject("org-1", user);
            var dal = LookupTestBase<LookupDAL<SeekerMatchOrganization>, SeekerMatchOrganization>.RetrieveDAL();
            dal.Insert(organization);

            scholarship.SeekerProfileCriteria.Organizations.Add(organization);

            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.AreEqual(scholarship.Id, retrivedScholarship.Id);
            Assert.IsNotNull(scholarship.SeekerProfileCriteria.Organizations);
            Assert.AreEqual(1, scholarship.SeekerProfileCriteria.Organizations.Count);
        }


        [Test]
        public void test_gpa_is_null_when_not_set()
        {
            Assert.IsNull(scholarship.SeekerProfileCriteria.GPA);
            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNull(retrivedScholarship.SeekerProfileCriteria.GPA);
        }

        [Test]
        public void test_gpa_is_not_null_when_set()
        {
            scholarship.SeekerProfileCriteria.GPA  = new RangeCondition<double?> {Minimum = 10d, Maximum = 100d};
            scholarship = ScholarshipDAL.Save(scholarship);
            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.GPA);
            AssertRangeEqual(scholarship.SeekerProfileCriteria.GPA, retrivedScholarship.SeekerProfileCriteria.GPA);
        }

        public static void AssertRangeEqual(RangeCondition<double?> expected, RangeCondition<double?> actual)
        {
            Assert.AreEqual(expected.Minimum, actual.Minimum);
            Assert.AreEqual(expected.Maximum, actual.Maximum);
        }

        public static void AssertRangeEqual(RangeCondition<double> expected, RangeCondition<double> actual)
        {
            Assert.AreEqual(expected.Minimum, actual.Minimum);
            Assert.AreEqual(expected.Maximum, actual.Maximum);
        }

        public static void AssertRangeEqual(RangeCondition<int> expected, RangeCondition<int> actual)
        {
            Assert.AreEqual(expected.Minimum, actual.Minimum);
            Assert.AreEqual(expected.Maximum, actual.Maximum);
        }

        [Test]
        public void test_sat_is_null_when_not_set()
        {
            Assert.IsNull(scholarship.SeekerProfileCriteria.SATScore);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNull(retrivedScholarship.SeekerProfileCriteria.SATScore);
        }

        [Test]
        public void test_sat_is_retrived_when_set()
        {
            Assert.IsNull(scholarship.SeekerProfileCriteria.SATScore);
            scholarship.SeekerProfileCriteria.SATScore = new SatScore();
            Assert.IsNotNull(scholarship.SeekerProfileCriteria.SATScore.Writing);
            Assert.IsNotNull(scholarship.SeekerProfileCriteria.SATScore.Mathematics);
            Assert.IsNotNull(scholarship.SeekerProfileCriteria.SATScore.CriticalReading);
            scholarship.SeekerProfileCriteria.SATScore.Writing.Minimum = 1;
            scholarship.SeekerProfileCriteria.SATScore.Writing.Maximum = 2;
            scholarship.SeekerProfileCriteria.SATScore.Mathematics.Minimum = 3;
            scholarship.SeekerProfileCriteria.SATScore.Mathematics.Maximum = 4;
            scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Minimum = 5;
            scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Maximum = 6;

            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.SATScore);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.SATScore.Writing);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.SATScore.Mathematics);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.SATScore.CriticalReading);
            Assert.AreEqual(1, scholarship.SeekerProfileCriteria.SATScore.Writing.Minimum);
            Assert.AreEqual(2, scholarship.SeekerProfileCriteria.SATScore.Writing.Maximum);
            Assert.AreEqual(3, scholarship.SeekerProfileCriteria.SATScore.Mathematics.Minimum);
            Assert.AreEqual(4, scholarship.SeekerProfileCriteria.SATScore.Mathematics.Maximum);
            Assert.AreEqual(5, scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Minimum);
            Assert.AreEqual(6, scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Maximum);
        }

        [Test]
        public void match_criteria_attribute_save_and_retrieve()
        {
            Assert.IsNotNull(scholarship.SeekerProfileCriteria.Attributes);
            Assert.AreEqual(0, scholarship.SeekerProfileCriteria.Attributes.Count);
            var criteriaAttribute1 = new SeekerProfileAttributeUsage
                                         {
                                             Attribute = SeekerProfileAttribute.SeekerVerbalizingWord,
                                             UsageType = ScholarshipAttributeUsageType.Minimum
                                         };
            scholarship.SeekerProfileCriteria.Attributes.Add(criteriaAttribute1);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Attributes);
            Assert.AreEqual(1, retrivedScholarship.SeekerProfileCriteria.Attributes.Count);
            Assert.AreEqual(criteriaAttribute1.Attribute, SeekerProfileAttribute.SeekerVerbalizingWord);
            Assert.AreEqual(criteriaAttribute1.UsageType, ScholarshipAttributeUsageType.Minimum);

            scholarship.SeekerProfileCriteria.Attributes.Remove(SeekerProfileAttribute.SeekerVerbalizingWord);
            scholarship = ScholarshipDAL.Save(scholarship);
            retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(retrivedScholarship.SeekerProfileCriteria.Attributes);
            Assert.AreEqual(0, retrivedScholarship.SeekerProfileCriteria.Attributes.Count);
        }

        [Test]
        public void funding_profile_attribute_save_and_retrieve()
        {
            Assert.IsNotNull(scholarship.FundingProfile.AttributesUsage);
            Assert.AreEqual(0, scholarship.FundingProfile.AttributesUsage.Count);
            var criteriaAttribute1 = new FundingProfileAttributeUsage
            {
                Attribute = FundingProfileAttribute.Need,
                UsageType = ScholarshipAttributeUsageType.Minimum
            };
            scholarship.FundingProfile.AttributesUsage.Add(criteriaAttribute1);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            Assert.IsNotNull(retrivedScholarship.FundingProfile.AttributesUsage);
            Assert.AreEqual(1, retrivedScholarship.FundingProfile.AttributesUsage.Count);
            Assert.AreEqual(criteriaAttribute1.Attribute, FundingProfileAttribute.Need);
            Assert.AreEqual(criteriaAttribute1.UsageType, ScholarshipAttributeUsageType.Minimum);

            scholarship.FundingProfile.AttributesUsage.Clear();
            scholarship = ScholarshipDAL.Save(scholarship);
            retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(retrivedScholarship.FundingProfile.AttributesUsage);
            Assert.AreEqual(0, retrivedScholarship.FundingProfile.AttributesUsage.Count);
        }



    }
}
