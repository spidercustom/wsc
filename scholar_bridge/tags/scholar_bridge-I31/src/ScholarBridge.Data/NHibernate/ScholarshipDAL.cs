﻿using System;
using System.Collections.Generic;
using NHibernate;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class ScholarshipDAL : AbstractDAL<Scholarship>, IScholarshipDAL
    {
       
        #region IScholarshipDAL Members
        public Scholarship FindById(int id)
        {
            return CreateCriteria().Add(Restrictions.Eq("Id", id)).UniqueResult<Scholarship>();
        }

        public Scholarship FindByBusinessKey(Provider provider, string name, int year)
        {
            return CreateCriteria()
               .Add(Restrictions.Eq("Name", name))
               .Add(Restrictions.Eq("Provider", provider))
               .Add(Restrictions.Eq("AcademicYear.Year", year))
               .UniqueResult<Scholarship>();
        }
        

        public Scholarship Save(Scholarship scholarship)
        {
            return scholarship.Id < 1 ?
                Insert(scholarship) :
                Update(scholarship);
        }

        public IList<Scholarship> FindByProvider(Provider provider)
        {
            return CreateCriteria().Add(Restrictions.Eq("Provider", provider)).List<Scholarship>(); 
        }

        public IList<Scholarship> FindByProvider(Provider provider, ScholarshipStages stage)
        {
            return FindByProvider(provider, new[] { stage });
        }

        public IList<Scholarship> FindByProviderNotActivated(Provider provider)
        {
            return FindByProvider(provider, GetNonActivatedStages());
        }

        public IList<Scholarship> FindByProvider(Provider provider, ScholarshipStages[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Provider", provider))
                .AddOrder(Order.Asc("Stage"))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .AddOrder(Order.Asc("Stage"))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages stage)
        {
            return FindByOrganizations(provider, intermediary, new[] { stage });
        }

        public IList<Scholarship> FindNotActivatedByOrganizations(Provider provider, Intermediary intermediary)
        {
            return FindByOrganizations(provider, intermediary, GetNonActivatedStages());
        }

        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStages[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>(); 
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStages stage)
        {
            return FindByIntermediary(intermediary, new[] {stage});
        }


        public IList<Scholarship> FindByIntermediaryNotActivated(Intermediary intermediary)
        {
            return FindByIntermediary(intermediary, GetNonActivatedStages());
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStages[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindAllClosedSince(DateTime date)
        {
            return CreateCriteria()
                .Add(Restrictions.IsNotNull("AwardPeriodClosed"))
                .Add(Restrictions.Between("AwardPeriodClosed", date, DateTime.Now))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindAllDueOn(DateTime date)
        {
            return CreateCriteria()
                .Add(Restrictions.IsNotNull("ApplicationDueDate"))
                .Add(Restrictions.Between("ApplicationDueDate", date, date.AddDays(1).AddSeconds(-1)))
                .List<Scholarship>();
        }


        public IList<Scholarship> FindByStage(ScholarshipStages stage)
        {
            return CreateCriteria()
                 .Add(Restrictions.Eq( "Stage", stage))
                 .List<Scholarship>();
        }

        #endregion
        public static ScholarshipStages[] GetNonActivatedStages()
        {
            return new[]
                       {
                           ScholarshipStages.None,
                           ScholarshipStages.NotActivated,
                           ScholarshipStages.Rejected
                       };
        }

        public void Flush()
        {
            var cur = Session.FlushMode;
            try
            {
                Session.FlushMode = FlushMode.Never;
                Session.Flush();
            }
            finally
            {
                Session.FlushMode = cur;   
            }
        }
    }
}
