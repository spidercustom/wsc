using NHibernate.Type;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>ApprovalStatus</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class SupportTypeType : EnumStringType
    {
        public SupportTypeType()
            : base(typeof(SupportType), 30)
        {
        }
    }
}