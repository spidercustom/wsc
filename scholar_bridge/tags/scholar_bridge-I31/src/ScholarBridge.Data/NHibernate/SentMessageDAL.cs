using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.NHibernate
{
    public class SentMessageDAL : MessageDALBase<SentMessage>, ISentMessageDAL
    {
        public SentMessage FindById(User user, IList<Role> roles, Organization organization, int id)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            crit.Add(Restrictions.Eq("Id", id));
            return crit.UniqueResult<SentMessage>();
        }

        /// <summary>
        /// The match is if any of the user, roles or organization information matches what is passed
        /// null values in the database means 'do not limit by that field'.
        /// </summary>
        /// <example>
        /// User is set in the db, then only to that user.
        /// Organization is set, then all users in the organization.
        /// Organization and Role are set, then only to those users in the Organization with that role.
        /// </example>
        /// <remarks>
        /// User/Role or User/Organization combo don't make sense.
        /// </remarks>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <param name="organization"></param>
        /// <returns></returns>
        public IList<SentMessage> FindAll(User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            return crit.List<SentMessage>();
        }

        private ICriteria BuildFindAllCriteria(User user, IEnumerable<Role> roles, Organization organization)
        {
            var crit = CreateCriteria();
            crit.Add(CreateCriteriaForMessageQuery("From.User", user));
            crit.Add(CreateInCriteriaForMessageQuery("From.Role", roles));
            crit.Add(CreateCriteriaForMessageQuery("From.Organization", organization));
            return crit;
        }
    }
}