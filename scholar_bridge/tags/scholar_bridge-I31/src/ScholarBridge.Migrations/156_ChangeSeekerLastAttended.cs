using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(156)]
    public class AddMatchCriteriaAttributeLUT : Migration
    {
        private const string TABLE_NAME = "SBSeeker";
        private const string COLUMN_NAME = "LastAttended";

        public override void Up()
        {
            // Hopefully there is no interesting data in this column yet
            Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
            Database.AddColumn(TABLE_NAME, new Column(COLUMN_NAME, DbType.Int32, ColumnProperty.Null));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
            Database.ChangeColumn(TABLE_NAME, new Column(COLUMN_NAME, DbType.String, 50, ColumnProperty.Null));
        }
    }
}