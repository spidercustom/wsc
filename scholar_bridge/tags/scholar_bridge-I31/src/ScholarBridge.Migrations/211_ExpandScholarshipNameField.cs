﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(211)]
    public class ExpandScholarshipNameField : Migration
    {
		private const string TABLE = "SBScholarship";
		private const string COL_NAME = "Name";

        public override void Up()
        {
            Column nameColumn = Database.GetColumnByName(TABLE, COL_NAME);

        	nameColumn.Size = 100;
			Database.ChangeColumn(TABLE, nameColumn);
        }

        public override void Down()
        {
        	Database.ExecuteNonQuery("update SBScholarship set name = Substring(Name, 1, 50)");
            Column nameColumn = Database.GetColumnByName(TABLE, COL_NAME);

        	nameColumn.Size = 50;
			Database.ChangeColumn(TABLE, nameColumn);
        }
    }
}
