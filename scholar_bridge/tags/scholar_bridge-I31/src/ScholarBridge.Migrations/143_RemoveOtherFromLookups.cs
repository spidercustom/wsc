﻿using System;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	/// <summary>
	/// remove 'Other' from the lookup table entries...
	/// Don't bother putting a 'Down' method in...
	/// </summary>
	[Migration(143)]
	public class RemoveOtherFromLookups : Migration
	{

		public static string[] TABLE_NAMES = new[]
		                                     	{
		                                     		"SBAcademicAreaLUT", // 0
		                                     		"SBAdditionalRequirementLUT", // 1
		                                     		"SBAffiliationTypeLUT", // 2
		                                     		"SBCareerLUT", // 3
		                                     		"SBCommunityInvolvementCauseLUT", // 4
		                                     		"SBClubLUT", // 5
		                                     		"SBEthnicityLUT", // 6
		                                     		"SBSeekerMatchOrganizationLUT", // 7
		                                     		"SBReligionLUT", // 8
		                                     		"SBSeekerHobbyLUT", // 9
		                                     		"SBSeekerSkillLUT", // 10
		                                     		"SBSeekerVerbalizingWordLUT", // 11
		                                     		"SBServiceTypeLUT", // 12
		                                     		"SBSportLUT", // 13
		                                     		"SBWorkTypeLUT" // 14
		                                     	};

		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			// remove the 'Other' selections from the lookups.
			foreach (var table in TABLE_NAMES)
			{
				Database.ExecuteNonQuery("delete from " + table +  " where Description like 'Other %'");
			}

			// add some additional values per Jeff N

			string tableName = TABLE_NAMES[11]; //SBSeekerVerbalizingWordLUT
			string[] columns = GetInsertColumns(tableName);
			InsertIfNotPresent(tableName, columns, new[] { "Determined", "Determined", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Open-minded", "Open-minded", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Integrity", "Integrity", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Optimistic", "Optimistic", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = TABLE_NAMES[4]; //SBCommunityInvolvementCauseLUT;
			columns = GetInsertColumns(tableName);
			InsertIfNotPresent(tableName, columns, new[] { "Animal Shelter", "Animal Shelter", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Book Drive", "Book Drive", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Camp Counselor", "Camp Counselor", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Church/Synagogue/Mosque/Temple", "Church/Synagogue/Mosque/Temple", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Community Center", "Community Center", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Environment - preservation, restoration, etc.", "Environment - preservation, restoration, etc.", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Food Bank/Soup kitchen", "Food Bank/Soup kitchen", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Homeless Shelter", "Homeless Shelter", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Library", "Library", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Mentoring", "Mentoring", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Senior Center/Retirement Community", "Senior Center/Retirement Community", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Teacher's Assistant (un-paid)", "Teacher's Assistant (un-paid)", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Tutoring (un-paid)/Reading with Children", "Tutoring (un-paid)/Reading with Children", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = TABLE_NAMES[7]; //SBSeekerMatchOrganizationLUT;
			columns = GetInsertColumns(tableName);
			InsertIfNotPresent(tableName, columns, new[] { "Church/ synagogue/ mosque/ temple", "Church/ synagogue/ mosque/ temple", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Seattle Metropolitan Credit Union", "Seattle Metropolitan Credit Union", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Sorority/ Fraternity – specific", "Sorority/ Fraternity – specific", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Service Club: Rotary", "Service Club: Rotary", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Service Club: Kiwanis", "Service Club: Kiwanis", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Service Club: Elks", "Service Club: Elks", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Service Club: Lions", "Service Club: Lions", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Service Club: Knights of Columbus", "Service Club: Knights of Columbus", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Service Club: Daughters of the American Revolution", "Service Club: Daughters of the American Revolution", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Service Club: USO", "Service Club: USO", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = TABLE_NAMES[2]; //SBAffiliationTypeLUT;
			columns = GetInsertColumns(tableName);
			InsertIfNotPresent(tableName, columns, new[] { "Child of employee", "Child of employee", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Employee", "Employee", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = TABLE_NAMES[9]; //SBSeekerHobbyLUT;
			columns = GetInsertColumns(tableName);
			InsertIfNotPresent(tableName, columns, new[] { "Dancing", "Dancing", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Exercise", "Exercise", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Gardening", "Gardening", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Horesback riding", "Horesback riding", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Martial Arts", "Martial Arts", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Music", "Music", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Rapping/Singing", "Rapping/Singing", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Scrap booking", "Scrap booking", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Snow boarding", "Snow boarding", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Snow skiing", "Snow skiing", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Travel", "Travel", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Wake boarding", "Wake boarding", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Water skiing", "Water skiing", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = TABLE_NAMES[13]; //SBSportLUT;
			columns = GetInsertColumns(tableName);
			InsertIfNotPresent(tableName, columns, new[] { "Cheerleader/drill team", "Cheerleader/drill team", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Cross Country", "Cross Country", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Golf", "Golf", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Hockey", "Hockey", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Lacrosse", "Lacrosse", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Swim", "Swim", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Water Polo", "Water Polo", "0", adminId.ToString(), DateTime.Now.ToString() });

			tableName = TABLE_NAMES[5]; //SBClubLUT;
			columns = GetInsertColumns(tableName);
			InsertIfNotPresent(tableName, columns, new[] { "4H", "4H", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Campfire", "Campfire", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Dance", "Dance", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "DECA", "DECA", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Debate", "Debate", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "FHA", "FHA", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Knowledge Bowl", "Knowledge Bowl", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Leadership", "Leadership (ASB, Student Government, Student Senate)", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Music", "Music (jazz ensemble, orchestra, marching band, voal, etc.)", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "PSTA", "PSTA", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Theater", "Theater", "0", adminId.ToString(), DateTime.Now.ToString() });
			InsertIfNotPresent(tableName, columns, new[] { "Visual Arts", "Visual Arts", "0", adminId.ToString(), DateTime.Now.ToString() });
		}

		public override void Down()
		{
			// do nothing...
		}

		private void InsertIfNotPresent(string tableName, string[] columns, string[] values)
		{
			string searchString = values[0].Replace("'", "''");
			int readCount = (int)Database.SelectScalar("count(*)", tableName, columns[0] + " = '" + searchString + "'");

			if (readCount == 0)
				Database.Insert(tableName, columns, values);
		}

		private string[] GetInsertColumns(string tableName)
		{
			Column[] columns = Database.GetColumns(tableName);
			return new []
			       	{
			       		columns[1].Name,
						columns[2].Name,
						"Deprecated",
                        "LastUpdateBy",
                        "LastUpdateDate"
					};
		}

	}

}
