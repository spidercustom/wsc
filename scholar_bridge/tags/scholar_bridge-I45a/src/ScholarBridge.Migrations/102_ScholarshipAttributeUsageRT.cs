﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(102)]
    public class ScholarshipAttributeUsageRT : Migration
    {
        public const string TABLE_NAME = "SBScholarshipAttributeUsageRT";
        private readonly Column[] Columns = new[]
                {
                    new Column("SBScholarshipID", DbType.Int32, ColumnProperty.PrimaryKey), 
                    new Column("SBAttributeIndex", DbType.Int32, ColumnProperty.PrimaryKey),
                    new Column("SBUsageTypeIndex", DbType.Int32)
                };

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                Columns);
        }

        public override void Down()
        {
            Database.RemoveTable(TABLE_NAME);
        }

    }
}
