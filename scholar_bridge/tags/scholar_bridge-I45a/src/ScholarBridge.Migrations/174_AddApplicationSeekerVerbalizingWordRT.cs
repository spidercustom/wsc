using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(174)]
    public class AddApplicationSeekerVerbalizingWordRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationSeekerVerbalizingWordRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBSeekerVerbalizingWordLUT"; }
        }
    }
}