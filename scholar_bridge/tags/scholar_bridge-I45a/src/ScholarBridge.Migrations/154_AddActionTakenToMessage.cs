using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(154)]
    public class AddActionTakenToMessage : Migration
    {
        public const string TABLE = "SBMessage";
        public const string COLUMN = "ActionTaken";

        public override void Up()
        {
            Database.AddColumn(TABLE, new Column(COLUMN, DbType.String, 10, ColumnProperty.Null, "'None'"));
            Database.Update(TABLE, new[] {COLUMN}, new[] {"None"});
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE, COLUMN);
        }
    }
}