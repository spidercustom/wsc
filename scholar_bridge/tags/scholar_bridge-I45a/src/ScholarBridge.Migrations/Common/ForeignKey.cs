﻿
namespace ScholarBridge.Migrations.Common
{
    public class ForeignKey
    {
        public ForeignKey()
        {

        }

        public ForeignKey(string name, AddTableBase foreignTable, string foreignColumn, string primaryTable, string primaryColumn)
        {
            this.Name = name;
            this.ForeignTable = foreignTable.TableName;
            this.ForeignColumn = foreignColumn;
            this.PrimaryTable = primaryTable;
            this.PrimaryColumn = primaryColumn;
        }


        public ForeignKey(string name, AddTableBase foreignTable, string foreignColumn, AddTableBase primaryTable)
        {
            this.Name = name;
            this.ForeignTable = foreignTable.TableName;
            this.ForeignColumn = foreignColumn;
            this.PrimaryTable = primaryTable.TableName;
            this.PrimaryColumn = primaryTable.PrimaryKeyColumn;
        }

        public string Name { get; set; }

        public string PrimaryTable { get; set; }
        public string PrimaryColumn { get; set; }

        public string ForeignTable { get; set; }
        public string ForeignColumn { get; set; }
    }
}
