using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(12)]
    public class AddAdminNotesToProvider : Migration
    {
        private const string COLUMN_NAME = "AdminNotes";

        public override void Up()
        {
            Database.AddColumn(AddProvider.TABLE_NAME, COLUMN_NAME, DbType.String, Int32.MaxValue, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(AddProvider.TABLE_NAME, COLUMN_NAME);
        }
    }
}