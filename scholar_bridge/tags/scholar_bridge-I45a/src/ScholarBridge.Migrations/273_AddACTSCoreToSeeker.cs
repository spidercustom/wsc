﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(273)]
    public class AddACTSCoreToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public readonly string[] NEW_COLUMNS
            = { "ACTCommulative", "SATCommulative" };

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Int32, ColumnProperty.Null);
        }

        public override void Down()
        {
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}