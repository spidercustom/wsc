using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(267)]
    public class AddOnlineApplicationToScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        private const string COLUMNA = "IsUseOnlineApplication";
        private const string COLUMNB = "IsOnlineApplicationApproved";
        private const string COLUMNC = "OnlineApplicationUrl";


        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, COLUMNA, DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, COLUMNB, DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, COLUMNC, DbType.String,100 , ColumnProperty.Null);
            
             
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMNA);
            Database.RemoveColumn(TABLE_NAME, COLUMNB);
            Database.RemoveColumn(TABLE_NAME, COLUMNC);

        }
    }
}