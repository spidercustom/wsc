﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(229)]
    public class RemoveScholarshipNeedGaps : Migration
    {
        public override void Up()
        {
            Database.RemoveTable("SBScholarshipNeedGapRT");
            Database.RemoveTable("SBNeedGapLUT");
            
        }

        public override void Down()
        {
           
            new AddNeedGap().Up();
            new AddScholarshipNeedGap().Up();
        }
    }
}
