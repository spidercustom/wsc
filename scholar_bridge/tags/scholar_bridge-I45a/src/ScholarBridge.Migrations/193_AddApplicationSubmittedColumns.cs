using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(193)]
    public class AddApplicationSubmittedColumns : Migration
    {
        public const string TABLE_NAME = "SBApplication";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, "SubmittedDate", DbType.DateTime, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, "SeekerPreferredCriteriaCount", DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, "SeekerMinimumCriteriaCount", DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, "ScholarshipPreferredCriteriaCount", DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, "ScholarshipMinimumCriteriaCount", DbType.Int32, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "SubmittedDate");

            Database.RemoveColumn(TABLE_NAME, "SeekerPreferredCriteriaCount");
            Database.RemoveColumn(TABLE_NAME, "SeekerMinimumCriteriaCount");
            Database.RemoveColumn(TABLE_NAME, "ScholarshipPreferredCriteriaCount");
            Database.RemoveColumn(TABLE_NAME, "ScholarshipMinimumCriteriaCount");
        }
    }
}