﻿using System;

namespace ScholarBridge.Common
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class DisplayNameAttribute : Attribute
    {
        public string DisplayName { get; set; }  
        public DisplayNameAttribute(string displayName)
        {
            DisplayName = displayName;
        }
    }
}
