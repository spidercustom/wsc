using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data.NHibernate
{
    public class MessageDAL : MessageDALBase<Message>, IMessageDAL
    {
        public Message FindById(User user, IList<Role> roles, Organization organization, int id)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            crit.Add(Restrictions.Eq("Id", id));
            return crit.UniqueResult<Message>();
        }

        /// <summary>
        /// The match is if any of the user, roles or organization information matches what is passed
        /// null values in the database means 'do not limit by that field'.
        /// </summary>
        /// <example>
        /// User is set in the db, then only to that user.
        /// Organization is set, then all users in the organization.
        /// Organization and Role are set, then only to those users in the Organization with that role.
        /// </example>
        /// <remarks>
        /// User/Role or User/Organization combo don't make sense.
        /// </remarks>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <param name="organization"></param>
        /// <returns></returns>
        public IList<Message> FindAll(MessageAction action, User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            crit.Add(Restrictions.Eq("ActionTaken", action));
            return crit.List<Message>();
        }

    	public IList<Message> FindAllForInbox(User user, IList<Role> roles, Organization organization)
    	{
			var crit = BuildFindAllCriteria(user, roles, organization);
			crit.Add(Restrictions.Not(CreateCriteriaForMessageQuery("MessageTemplate", MessageType.ScholarshipApproved)));
			crit.Add(Restrictions.Not(CreateCriteriaForMessageQuery("IsArchived", true)));
			crit.Add(Restrictions.Not(CreateCriteriaForMessageQuery("ActionTaken", MessageAction.Deny)));
			return crit.List<Message>();
		}

    	public IList<Message> FindAllArchived(User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            crit.Add(Restrictions.Eq("IsArchived", true));
            return crit.List<Message>();
        }

    	public IList<Message> FindAllByMessageType(MessageType type, User user, IList<Role> roles, Organization organization)
    	{
            var crit = BuildFindAllCriteria(user, roles, organization);
			crit.Add(CreateCriteriaForMessageQuery("MessageTemplate", type));
            return crit.List<Message>();
    	}

    	public int CountUnread(User user, IList<Role> roles, Organization organization)
        {
            var crit = BuildFindAllCriteria(user, roles, organization);
            return crit.Add(Restrictions.Eq("IsRead", false)).SetProjection(Projections.CountDistinct("Id"))
                .UniqueResult<int>();
        }
        public void DeleteRelated(Scholarship scholarship)
        {
            Session.Delete("from ScholarshipMessage sm where sm.RelatedScholarship=:scholarship", scholarship, NHibernateUtil.Entity(typeof(Scholarship)));
        }

        private ICriteria BuildFindAllCriteria(User user, IEnumerable<Role> roles, Organization organization)
        {
            var crit = CreateCriteria();
            crit.Add(CreateCriteriaForMessageQuery("To.User", user));
            crit.Add(CreateInCriteriaForMessageQuery("To.Role", roles));
			crit.Add(CreateCriteriaForMessageQuery("To.Organization", organization));
			return crit;
        }
    }
}