﻿using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Data
{
    public interface IMessageDAL : IDAL<Message>
    {
        /// <summary>
        /// Ensures user has access to the Message by passing relavent context
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <param name="organization"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Message FindById(User user, IList<Role> roles, Organization organization, int id);

        IList<Message> FindAll(MessageAction action, User user, IList<Role> roles, Organization organization);
		IList<Message> FindAllForInbox(User user, IList<Role> roles, Organization organization);
		IList<Message> FindAllArchived(User user, IList<Role> roles, Organization organization);
		IList<Message> FindAllByMessageType(MessageType type, User user, IList<Role> roles, Organization organization);
		int CountUnread(User user, IList<Role> roles, Organization organization);

        void DeleteRelated(Scholarship scholarship);
    }
}