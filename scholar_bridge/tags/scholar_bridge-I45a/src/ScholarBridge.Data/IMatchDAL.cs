using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IMatchDAL : IDAL<Match>
    {
        IList<Match> FindAll(Application application);
        IList<Match> FindAll(Seeker seeker, MatchStatus status);
        IList<Match> FindAll(Scholarship scholarship, MatchStatus[] status);
		IList<Match> FindAllCurrentMatches(Seeker seeker, bool excludeMatchesWithApplications);

        Match Find(Seeker seeker, int scholarshipId);

        void UpdateMatches(Seeker seeker);
        void UpdateMatches(Scholarship newScholarship);
    }
}