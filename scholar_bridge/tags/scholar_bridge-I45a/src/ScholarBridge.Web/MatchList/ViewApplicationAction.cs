using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    class ViewApplicationAction : OpenUrlAction
    {
		private const string VIEW_APPLICATION_URL_TEMPLATE = "/Seeker/Applications/Show.aspx?aid={0}";

        public ViewApplicationAction() : base("View")
        {
        	IsShowInPopup = true;
        }

        protected override string ConstructUrl(Match match)
        {
			IsShowInPopup = true;
            if (match.Application != null)
                return LinkGenerator.GetFullLinkStatic(VIEW_APPLICATION_URL_TEMPLATE.Build(match.Application.Id));
            
            return "";
        }

    	public override string GetActionUrl(Match match)
    	{
    		return ConstructUrl(match);
    	}
    }
}