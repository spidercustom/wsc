﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin.Editors.PressRoom 
{

    public partial class ArticleEntry : UserControl
    {
        public delegate void ArticleSaved(Article article);
        public delegate void ArticleCanceled();

        public event ArticleSaved FormSaved;
        public event ArticleCanceled FormCanceled;

        public IArticleService ArticleService { get; set; }

		public int? ArticleID
		{
			get
			{
				if (ViewState["ArticleID"] == null)
					return new int?();
				return (int?)ViewState["ArticleID"];
			}
			set
			{
				ViewState["ArticleID"] = value;
				articleInContext = null;
				SetPageValues();
			}
		}

		private Article articleInContext;
		private Article ArticleInContext
		{
			get
			{
				if (articleInContext == null)
				{
					articleInContext = !ArticleID.HasValue
						? new Article()
						: ArticleService.GetById(ArticleID.Value);
				}
				return articleInContext;
			}
		}

		public IUserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (null == ArticleInContext)
            {
                errorMessage.Visible = true;
                return;
            }

            PostedDateControl.SelectedDate = ArticleInContext.PostedDate;
            TitleControl.Text = ArticleInContext.Title;
            BodyControl.Text = ArticleInContext.Body;
        }

		private void SetPageValues()
		{
			if (ArticleID.HasValue)
			{
				PostedDateControl.SelectedDate = ArticleInContext.PostedDate;
				TitleControl.Text = ArticleInContext.Title;
				BodyControl.Text = ArticleInContext.Body;
			}
			else
			{
				PostedDateControl.SelectedDate = null;
				TitleControl.Text = string.Empty;
				BodyControl.Text = string.Empty;
				
			}
		}

        protected void saveButton_Click(object sender, EventArgs e)
        {   Page.Validate();
            if (!Page.IsValid) return;
            ArticleInContext.PostedDate = PostedDateControl.SelectedDate.Value;
            ArticleInContext.Title = TitleControl.Text;
            ArticleInContext.Body = BodyControl.Text;
            ArticleInContext.LastUpdate=new ActivityStamp(UserContext.CurrentUser);
            ArticleService.Save(ArticleInContext);

            if (null != FormSaved)
            {
                FormSaved(ArticleInContext);
            }
        }

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}