<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Register" Title="Seeker Registration" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register Src="~/Common/CaptchaControl.ascx" tagname="CaptchaControl" tagprefix="sb" %>
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/Tip.ascx" tagname="Tip" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Seeker Registration" />
    
    <link href="<%= ResolveUrl("~/styles/form.css") %>" rel="stylesheet" type="text/css" media="All"/> 

    <script type="text/javascript">
        $(document).ready(function() {
            $(".tip").tooltip({
                position: "center right",
                effect: "slide",
                direction: "right",
                bounce: true,
                tip: '#TooltipContainer'
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h1><asp:Literal ID="pageTitle" runat="server" Text="Seeker Registration"/></h1>
    <asp:MultiView runat="server" ID="CreateUserWizard" ActiveViewIndex="0">
        <asp:View ID="CaptchaView" runat="server">
            <p class="hookline">First, let's make sure you're human (or a very smart robot). This verification helps us prevent automated registerations.</p>
            <sb:CaptchaControl ID="CaptchaControl1" runat="server" />
            <sb:AnchorButton ID="RegisterButton" runat="server"  Text="Continue to registration" onclick="RegisterButton_Click"/>
        </asp:View>
        <asp:View ID="DataEntryView" runat="server"> 
                    <p class="hookline">Please fill out this form to complete your registration. You can also ensure our e-mails don't end up in your spam folder by adding noreply@theWashBoard.org to your safe senders list.</p>
                    <div class="question_list">
                        <div class="multi-line">
                            <label>What will you be using theWashBoard.org as?<sb:Tip ID="Tip1" runat="server" content="Only &quot;Students&quot; are able to apply for scholarships. Select one of the other roles if you are not a student but want to create a profile, see matches, or demonstrate the benefits of theWashBoard.org" /></label>
                            <asp:RadioButtonList ID="seekerTypeList" runat="server"/>
                            <asp:requiredfieldvalidator ID="seekerTypeRequiredValidator" runat="server" ErrorMessage="Please select an option" ControlToValidate="seekerTypeList" CssClass="error_nofitication"></asp:requiredfieldvalidator>                            
                        </div>
                        <div>
                            <label>First Name:</label>
                            <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="firstNameRequired" ControlToValidate="FirstName" runat="server" Display="Dynamic" ErrorMessage="First name is required"></asp:RequiredFieldValidator>
                            <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                        </div>
                        <div>
                            <label>Last Name:</label>
                            <asp:TextBox ID="LastName"  runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="lastNameRequired" ControlToValidate="LastName" Display="Dynamic" runat="server" ErrorMessage="Last name is required" ></asp:RequiredFieldValidator>
                            <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                        </div>
                        <div>
                            <label>Email Address:</label>
                            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="emailRequired" ControlToValidate="UserName" Display="Dynamic" runat="server" ErrorMessage="Email address is required"></asp:RequiredFieldValidator>
                            <elv:PropertyProxyValidator ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                            <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
                        </div>
                        <div>
                            <label>Confirm Email Address:</label>
                            <asp:TextBox ID="ConfirmEmail" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ErrorMessage="Email confirmation is required" ID="confirmEmailRequired" ControlToValidate="ConfirmEmail" runat="server" ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address."/>
                        </div>
                        <div>
                            <label>Password:</label>
                            <asp:TextBox ID="Password" runat="server" TextMode="Password" ToolTip="Your password must be at least 7 characters with at least one special character or number, Example: p@sword" CssClass="tip"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="passwordRequired" Display="Dynamic"  ControlToValidate="Password" runat="server" ErrorMessage="A password is required and must be at least 7 characters with at least one special character or number."></asp:RequiredFieldValidator>
                            <elv:PropertyProxyValidator ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                        </div>
                        <div>
                            <label>Confirm Password:</label>
                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" Display="Dynamic" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Please re-enter your new password here."></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                        </div>
                        <div>
                            <label>&nbsp;</label>
                            <sb:AnchorButton ID="CompleteRegistration" runat="server" Text="Complete registration" OnClick="CompleteRegistration_Click" CausesValidation="true" />
                        </div>
                    </div>                    
        </asp:View>
        <asp:View ID="CompletionView" runat="server">
            <p class="hookline">Thank you for registering! An email has been sent to the email address you provided with instructions to complete the registration process. Please follow the instructions in the e-mail to enable your account.</p>
            <sb:AnchorButton ID="okNavigateToLogin" runat="server" Text="Continue to sign in page" OnClick="okNavigateToLogin_Click"/>
        </asp:View>           
    </asp:MultiView>
</asp:Content>
