﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain.Lookup;
namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class Activities : WizardStepUserControlBase<Domain.Application>
    {
        Domain.Application _applicationInContext;
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Application applicationInContext
        {
            get
            {
                if (_applicationInContext == null)
                    _applicationInContext = Container.GetDomainObject();
                return _applicationInContext;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (applicationInContext == null)
                throw new InvalidOperationException("There is no application in context");

            if (!Page.IsPostBack)
            {
                
                 
                PopulateScreen();
            }
            else
            {
                SetWorkingControlState();
                SetVolunteeringControlState();
            }
        }

        private void PopulateScreen()
        {

            AcademicAreasControlDialogButton.Keys = applicationInContext.AcademicAreas.CommaSeparatedIds();
            AcademicAreasOtherControl.Text = applicationInContext.AcademicAreaOther;

            CareersControlDialogButton.Keys = applicationInContext.Careers.CommaSeparatedIds();
            CareersOtherControl.Text = applicationInContext.CareerOther;

            OrganizationsControlDialogButton.Keys = applicationInContext.MatchOrganizations.CommaSeparatedIds();
            OrganizationsOtherControl.Text = applicationInContext.MatchOrganizationOther;

            AffiliationTypesControlDialogButton.Keys = applicationInContext.Companies.CommaSeparatedIds();
            AffiliationTypesOtherControl.Text = applicationInContext.CompanyOther;

            SeekerHobbiesControlDialogButton.Keys = applicationInContext.Hobbies.CommaSeparatedIds();
            SeekerHobbiesOtherControl.Text = applicationInContext.HobbyOther;

            SportsControlDialogButton.Keys = applicationInContext.Sports.CommaSeparatedIds();
            SportsOtherControl.Text = applicationInContext.SportOther;

            ClubsControlDialogButton.Keys = applicationInContext.Clubs.CommaSeparatedIds();
            ClubsOtherControl.Text = applicationInContext.ClubOther;

            WorkingControl.Checked = applicationInContext.IsWorking;
            WorkHistoryControl.Text = applicationInContext.WorkHistory;

            VolunteeringControl.Checked = applicationInContext.IsService;
            ServiceHistoryControl.Text = applicationInContext.ServiceHistory;

            WorkingControl.Attributes.Add("onclick", WorkHistoryControl.ClientID + ".disabled = ! " + WorkingControl.ClientID + ".checked;");
            VolunteeringControl.Attributes.Add("onclick", ServiceHistoryControl.ClientID + ".disabled = ! " + VolunteeringControl.ClientID + ".checked;");

            SetWorkingControlState();
            SetVolunteeringControlState();
        }

        public void SetWorkingControlState()
        {
            WorkHistoryControl.Enabled = WorkingControl.Checked;
        }

        public void SetVolunteeringControlState()
        {
            ServiceHistoryControl.Enabled = VolunteeringControl.Checked;
        }

        public override void PopulateObjects()
        {
            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, applicationInContext.AcademicAreas);
            applicationInContext.AcademicAreaOther = AcademicAreasOtherControl.Text;

            PopulateList(CareersContainerControl, CareersControlDialogButton, applicationInContext.Careers);
            applicationInContext.CareerOther = CareersOtherControl.Text;

            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, applicationInContext.MatchOrganizations);
            applicationInContext.MatchOrganizationOther = OrganizationsOtherControl.Text;

            PopulateList(AffiliationTypesContainerControl, AffiliationTypesControlDialogButton, applicationInContext.Companies);
            applicationInContext.CompanyOther = AffiliationTypesOtherControl.Text;

            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, applicationInContext.Hobbies);
            applicationInContext.HobbyOther = SeekerHobbiesOtherControl.Text;

            PopulateList(SportsContainerControl, SportsControlDialogButton, applicationInContext.Sports);
            applicationInContext.SportOther = SportsOtherControl.Text;

            PopulateList(ClubsContainerControl, ClubsControlDialogButton, applicationInContext.Clubs);
            applicationInContext.ClubOther = ClubsOtherControl.Text;

            applicationInContext.IsWorking = WorkingControl.Checked;
            applicationInContext.WorkHistory = WorkingControl.Checked ? WorkHistoryControl.Text : "";

            applicationInContext.IsService = VolunteeringControl.Checked;
            applicationInContext.ServiceHistory = VolunteeringControl.Checked ? ServiceHistoryControl.Text : "";


            
            applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (applicationInContext.Stage < ApplicationStage.NotActivated)
                applicationInContext.Stage = ApplicationStage.NotActivated;
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        private static bool Validate(PlaceHolder applicabilityControl, LookupDialog dialogButton)
        {
            return true;
        }

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
            PopulateObjects();
            ApplicationService.Update(applicationInContext);
		}
		#endregion
	}
}