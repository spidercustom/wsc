﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="ScholarBridge.Web.Help" Title="Help" %>
<%@ Import Namespace="ScholarBridge.Domain"%>
<%@ Register src="Common/FaqList.ascx" tagname="FaqList" tagprefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Help" />
    <style type="text/css">
       h4{font-size:12px; border-bottom:1px solid #ccc; margin-top:25px; margin-bottom:10px;}
       h4 > a {color:Gray;}
       div.question{font-weight:bold; margin-top:10px;}
    </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Help</h2>

    <p>Support Hours: 8:00 am – 5:00 pm PST Monday thru Friday <br />
        Call 888-535-0747, option #8 or <A href='/ContactUs.aspx'>contact us online.</A>  
    </p>

    <h3>Frequently Asked Questions</h3>
    <ul style="padding-bottom:10px;">
        <li>
            <a href="#generalFAQ">General Questions</a>
        </li>
        <li>
            <a href="#seekerFAQ">Seeker-related Questions</a>
        </li>
        <li>
            <a href="#providerFAQ">Provider-related Questions</a>
        </li>
    </ul>

    <h4><a name="generalFAQ">General Questions</a></h4>
    <uc2:FaqList ID="generalFaqList" runat="server"/>

    <h4><a name="seekerFAQ">Seeker-related Questions</a></h4>
    <uc2:FaqList ID="seekerFaqList" runat="server"/>
        
    <h4><a name="providerFAQ">Provider-related Questions</a></h4>
    <uc2:FaqList ID="providerFaqList" runat="server"/>

</asp:Content>
