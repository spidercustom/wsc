﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageList.ascx.cs" Inherits="ScholarBridge.Web.Common.MessageList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<asp:ListView ID="messageList" runat="server" 
    onitemdatabound="messageList_ItemDataBound">
    <LayoutTemplate>
        <table class="sortableTable" cellpadding="0" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Subject</th>
                    <th>From</th>
                    <th>Organization Name</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton ID="linkToMessage" runat="server"   ><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
            <td><asp:Label ID="lblFrom" runat="server"></asp:Label></td>
            <td><asp:Label ID="lblOrg" runat="server"></asp:Label> </td>
            <td><asp:Label ID="lblDate" runat="server"></asp:Label></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton ID="linkToMessage" runat="server"   ><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
             <td><asp:Label ID="lblFrom" runat="server"></asp:Label></td>
            <td><asp:Label ID="lblOrg" runat="server"></asp:Label> </td>
             <td><asp:Label ID="lblDate" runat="server"></asp:Label></td>
        </tr>
    </AlternatingItemTemplate>
</asp:ListView>
<div class="pager">
    <asp:DataPager runat="server" ID="pager" PagedControlID="messageList" PageSize="20">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
    <br />
</div> 
