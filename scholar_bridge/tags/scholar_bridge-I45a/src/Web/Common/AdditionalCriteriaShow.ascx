﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteriaShow.ascx.cs" Inherits="ScholarBridge.Web.Common.AdditionalCriteriaShow" %>

<%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>


<p class="form-section-title section-level-1">Additional Scholarship Application Requirements</p>
<strong>Applicant must provide the following requested additional information</strong>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>

<p class="form-section-title section-level-2">Additional Requirements:</p><span>Please include the following with your application</span> 
<table class="viewonlyTable">
    <tbody>
        <asp:Repeater ID="AdditionalRequirementsRepeater" runat="server">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Name")%></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <tr runat="server" id="RequirementsFooterRow">
            <td>No additional requirements listed for this scholarship.</td>
        </tr>
    </tbody>
</table>

<hr />
<p class="form-section-title section-level-2">Scholarship Questions:</p><span>Please answer the following questions</span>
<table class="viewonlyTable">
    <asp:Repeater ID="AdditionalCriteriaRepeater" runat="server">
        <ItemTemplate>
            <tr>
                <td><%# Eval("DisplayOrder")%>.</td>
                <td><%# Eval("QuestionText")%></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</table>

<hr />
<p class="form-section-title section-level-2">Additional Application Forms:</p>
<sb:FileList id="scholarshipAttachments" runat="server" />

<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
