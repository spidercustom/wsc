﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class SentMessageList : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }

        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            var messages = MessageService.FindAllSent(UserContext.CurrentUser, UserContext.CurrentOrganization);
            messageList.DataSource = (from m in messages orderby m.Date descending select m).ToList();
            messageList.DataBind();
            pager.VisibleIf(() => pager.PageSize < pager.TotalRowCount);
            messageList.PagePropertiesChanged += (s1, e1) => messageList.DataBind();
        }

        protected void messageList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var message = (Domain.Messaging.SentMessage)((ListViewDataItem)e.Item).DataItem;
                var date = (Label)e.Item.FindControl("lblDate");
                date.Text = message.Date.ToLocalTime().ToShortDateString();
 
               var link = (LinkButton)e.Item.FindControl("linkToMessage");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
                + "?sent=true&popup=true&id=" + (message.Id.ToString());
                link.Attributes.Add("onclick", String.Format("messageView('{0}'); return false;", url));
                link.Attributes.Add("class", "GreenLink");
            }
        }
    }
}