﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationActivitiesShow.ascx.cs" Inherits="ScholarBridge.Web.Common.ApplicationActivitiesShow" %>

<h3 id="AcademicIntBlock" runat="server">Academic Interests</h3>
<div id="AcademicAreasRow" runat="server">
<div class="viewLabel">Academic Areas:</div> 
<div class="viewValue">
<asp:Repeater ID="academicAreas" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="academicAreasOther" runat="server" /></div>
<br />
</div>

<div id="CareersRow" runat="server">
<div class="viewLabel">Careers:</div> 
<div class="viewValue">
<asp:Repeater ID="careers" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="careersOther" runat="server" /></div>
<br />
</div>

<h4 id="GroupsBlock" runat="server">What other groups or interests are you involved in?</h4>

<div id="HobbiesRow" runat="server">
<div class="viewLabel">Hobbies:</div> 
<div class="viewValue">
<asp:Repeater ID="hobbies" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="hobbiesOther" runat="server" /></div>
<br />
</div>

<div id="SportsRow" runat="server">
<div class="viewLabel">Sports:</div> 
<div class="viewValue">
<asp:Repeater ID="sports" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="sportsOther" runat="server" /></div>
<br />
</div>

<div id="ClubsRow" runat="server">
<div class="viewLabel">Clubs:</div> 
<div class="viewValue">
<asp:Repeater ID="clubs" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="clubsOther" runat="server" /></div>
<br />
</div>

<h4 id="OrgAffilBlock" runat="server">Are you or any family members affiliated with specific Organizations?</h4>
<div id="OrganizationsRow" runat="server">
<div class="viewLabel">Organizations:</div> 
<div class="viewValue">
<asp:Repeater ID="organizations" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="organizationsOther" runat="server" /></div>
<br />
</div>

<div id="AffiliationTypesRow" runat="server">
<div class="viewLabel">Affiliation Type:</div> 
<div class="viewValue">
<asp:Repeater ID="affiliations" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="affiliationsOther" runat="server" /></div>
<br />
</div>

<h4 id="WorkServiceBlock" runat="server">Work/Service</h4>
<div id="WorkTypesRow" runat="server">
<div class="viewLabel">Work Type:</div> 
<div class="viewValue">
<asp:Repeater ID="workTypes" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="workTypesOther" runat="server" /></div>
<br />
</div>

<div id="WorkHoursRow" runat="server">
<div class="viewLabel">Work Hours:</div> 
<div class="viewValue">
<asp:Repeater ID="workHours" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="workHoursOther" runat="server" /></div>
<br />
</div>

<div id="ServiceTypesRow" runat="server">
<div class="viewLabel">Service Type:</div> 
<div class="viewValue">
<asp:Repeater ID="serviceTypes" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="serviceTypesOther" runat="server" /></div>
<br />
</div>

<div id="ServiceHoursRow" runat="server">
<div class="viewLabel">Service Hours:</div> 
<div class="viewValue">
<asp:Repeater ID="serviceHours" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue"><asp:Literal ID="serviceHoursOther" runat="server" /></div>
<br />
</div>
<br />
