using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface IArticleService
    {
        Article GetById(int id);

        Article Save(Article article);
        void Delete(Article article);

        IList<Article> FindAll();
    }
}