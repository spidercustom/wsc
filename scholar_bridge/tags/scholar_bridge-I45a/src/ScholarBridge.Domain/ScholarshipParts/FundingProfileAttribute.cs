﻿using ScholarBridge.Common;

namespace ScholarBridge.Domain.ScholarshipParts
{
	/// <summary>
	/// Note - Important!!!: 
	/// 
	/// Database views of scholarship seeker match depends up
	/// integer representation of enum elements. If someone changes/adds/removes these
	/// they must also change database views for the matching algorithm to work
	/// correctly.
	/// 
	/// The associated database views are:
	/// SBMatchBools
	/// SBMatchEnums
	/// SBMatchList
	/// SBMatchRanges
	/// </summary>
	public enum FundingProfileAttribute
    {
        Need = 1,
        [DisplayName("Situations the Scholarship will Fund")]
        SupportedSituation = 2,
    }
}
