﻿using System;

namespace ScholarBridge.Domain.Messaging
{
    public enum MessageType
    {
        // a mail template enum should be the firstname of email template file
        // for example if ProverDenied is enum then system will look for
        // providerdenied.mail file.
        None,
        [PreserveFormatting(true)]
        AwardScholarship,
		[PreserveFormatting(true)]
		ConfirmationLink,
		[PreserveFormatting(true)]
        ForgotPassword,
        [PreserveFormatting(true)]
        IntermediaryApproved,
        [PreserveFormatting(true)]
        IntermediaryRejected,
        [PreserveFormatting(true)]
        OfferScholarship,
        [PreserveFormatting(true)]
        ProviderRejected,
        [PreserveFormatting(true)]
        ProviderApproved,
        [PreserveFormatting(true)]
        RequestIntermediaryApproval,
        [PreserveFormatting(true)]
        RequestProviderApproval,
        [PreserveFormatting(true)]
        RelationshipCreateRequest,
        [PreserveFormatting(true)]
        RelationshipInactivated,
        [PreserveFormatting(true)]
        ScholarshipApproved,
        [PreserveFormatting(true)]
        ScholarshipRejected,
        [PreserveFormatting(true)]
        ScholarshipClosed,
        [PreserveFormatting(true)]
        RelationshipRequestApproved,
        [PreserveFormatting(true)]
        RelationshipRequestRejected,
        [PreserveFormatting(false)]
        ListChangeRequest,
        [PreserveFormatting(true)]
        EmailAddressChangeVerificationLink,
        [PreserveFormatting(true)]
        ApplicationDue,
        [PreserveFormatting(true)]
        ApplicationSubmisionSuccess,
        [PreserveFormatting(true)]
        ApplicationAcknowledgement,
        [PreserveFormatting(true)]
        ScholarshipSendToFriend,
        [PreserveFormatting(true)]
        SeekerInterestedToRegister,
        [PreserveFormatting(false)]
		ContactUsSupport,
        [PreserveFormatting(false)]
		ContactUsProblem,
        [PreserveFormatting(false)]
		ContactUsSuggestion,
        [PreserveFormatting(true)]
        RequestOnlineApplicationApproval,
        [PreserveFormatting(true)]
        ScholarshipOnlineApplicationUrlApproved,
		[PreserveFormatting(true)]
		ScholarshipOnlineApplicationUrlRejected,
        [PreserveFormatting(true)]
        ChangeUserNameEmailLink
         
    }

    public static class MessageTypeExtensions
    {
        public static string TemplateName(this MessageType messageType)
        {
            return string.Format("{0}.mail", Enum.GetName(typeof (MessageType), messageType));
        }
    }
}