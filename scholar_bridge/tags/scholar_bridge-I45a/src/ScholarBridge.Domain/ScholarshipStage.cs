﻿
using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    public enum ScholarshipStage
    {
        None,
        [DisplayName("Not Activated")]
        NotActivated,
        Activated,
        Rejected,
        Awarded
    }
}
