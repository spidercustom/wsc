using NUnit.Framework;
using ScholarBridge.Web.Security;

namespace ScholarBridge.Web.Tests
{

    [TestFixture]
    public class KeyCreatorTests
    {
        [Test]
        public void can_create_key()
        {
            var key = KeyCreator.CreateKey(10);
            Assert.IsNotNull(key);
            Assert.AreEqual(20, key.Length);    // to hex doubles the size
        }
    }
}