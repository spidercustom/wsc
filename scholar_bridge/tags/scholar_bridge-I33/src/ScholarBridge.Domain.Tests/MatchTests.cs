using System;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class MatchTests
    {
        [Test]
        public void scholarship_name_null_by_default()
        {
            var m = new Match();
            Assert.IsNull(m.ScholarshipName);
        }

        [Test]
        public void scholarship_name_same_as_scholarship()
        {
            var s = new Scholarship {Name = "Test"};
            var m = new Match {Scholarship = s};
            Assert.AreEqual(s.Name, m.ScholarshipName);
        }

        [Test]
        public void criteria_percents_handle_zero()
        {
            var m = new Match();
            Assert.AreEqual(1, m.MinimumCriteriaPercent());
            Assert.AreEqual(1, m.PreferredCriteriaPercent());
        }

        [Test]
        public void criteria_percents_handle_val()
        {
            var m = new Match
                        {
                            SeekerMinimumCriteriaCount = 2,
                            ScholarshipMinimumCriteriaCount = 4,
                            SeekerPreferredCriteriaCount = 2,
                            ScholarshipPreferredCriteriaCount = 10
                        };
            Assert.AreEqual(.5, m.MinimumCriteriaPercent(), 0.001);
            Assert.AreEqual(.2, m.PreferredCriteriaPercent(), 0.001);
        }

        [Test]
        public void criteria_percents_to_strings()
        {
            var m = new Match
            {
                SeekerMinimumCriteriaCount = 2,
                ScholarshipMinimumCriteriaCount = 4,
                SeekerPreferredCriteriaCount = 2,
                ScholarshipPreferredCriteriaCount = 10
            };
            Assert.AreEqual("2 of 4", m.MinumumCriteriaString);
            Assert.AreEqual("2 of 10", m.PreferredCriteriaString);
        }

        [Test]
        public void validate_not_applied_state_when_match_is_new()
        {
            var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(1));
            Assert.AreEqual(MatchApplicationStatus.NotApplied, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.NotApplied.GetDisplayName(), match.MatchApplicationStatusString);
        }

        [Test]
        public void validate_not_applied_state_when_match_is_saved()
        {
            var match = CreateMatch(MatchStatus.Saved, DateTime.Today.AddDays(1));
            Assert.AreEqual(MatchApplicationStatus.NotApplied, match.MatchApplicationStatus);
        }

        [Test]
        public void validate_applying_state_when_applied_but_not_submitted()
        {
            var match = CreateMatch(MatchStatus.Applied, DateTime.Today.AddDays(1));
            match.Application = CreateApplication(ApplicationStages.NotActivated, match.Scholarship);
            Assert.AreEqual(MatchApplicationStatus.Applying, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.Applying.GetDisplayName(), match.MatchApplicationStatusString);
        }
        
        [Test]
        public void validate_applied_state_when_application_is_submitted()
        {
            var match = CreateMatch(MatchStatus.Applied, DateTime.Today.AddDays(1));
            match.Application = CreateApplication(ApplicationStages.Submitted, match.Scholarship);
            Assert.AreEqual(MatchApplicationStatus.Applied, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.Applied.GetDisplayName(), match.MatchApplicationStatusString);
        }

        [Test]
        public void validate_being_consider_state_when_submitted_and_due_date_passed()
        {
            var match = CreateMatch(MatchStatus.Applied, DateTime.Today.AddDays(-1));
            match.Application = CreateApplication(ApplicationStages.Submitted, match.Scholarship);
            Assert.AreEqual(MatchApplicationStatus.BeingConsidered, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.BeingConsidered.GetDisplayName(), match.MatchApplicationStatusString);
        }

        [Test]
        public void validate_closed_state_when_not_applied_and_due_date_passed()
        {
            var match = CreateMatch(MatchStatus.Saved, DateTime.Today.AddDays(-1));
            Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
            Assert.AreEqual(MatchApplicationStatus.Closed.GetDisplayName(), match.MatchApplicationStatusString);
        }

		[Test]
		public void validate_closed_state_when_not_submitted_and_due_date_passed()
		{
			var match = CreateMatch(MatchStatus.Saved, DateTime.Today.AddDays(-1));
			match.Application = CreateApplication(ApplicationStages.NotActivated, match.Scholarship);
			Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
		}

		[Test]
		public void validate_closed_state_when_new_and_due_date_passed()
		{
			var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(-1));
			match.Application = CreateApplication(ApplicationStages.NotActivated, match.Scholarship);
			Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
		}

		[Test]
		public void validate_notapplied_state_when_new_and_on_due_date()
		{
			var match = CreateMatch(MatchStatus.New, DateTime.Today.AddDays(-1));
			match.Application = CreateApplication(ApplicationStages.NotActivated, match.Scholarship);
			Assert.AreEqual(MatchApplicationStatus.Closed, match.MatchApplicationStatus);
		}

		private static Application CreateApplication(ApplicationStages stage, Scholarship scholarship)
		{
			return new Application
			       	{
			       		Stage = stage,
			       		Scholarship = scholarship
			};
		}

		private static Match CreateMatch(MatchStatus status, DateTime scholarshipDueDate)
        {
            return CreateMatch(status, scholarshipDueDate, scholarshipDueDate.AddDays(1));
        }

        private static Match CreateMatch(MatchStatus status, DateTime scholarshipDueDate, DateTime scholarshipAwardDate)
        {
            return new Match
                       {
                           MatchStatus = status,
                           Scholarship = new Scholarship
                                             {
                                                 ApplicationDueDate = scholarshipDueDate,
                                                 AwardDate = scholarshipAwardDate
                                             }
                       };
        }





    }
}