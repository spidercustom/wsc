﻿using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
	public interface IResourceService
	{
		Resource GetById(int id);

		Resource Save(Resource resource);
		void Delete(Resource resource);

		IList<Resource> FindAll();
	}
}