﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(20)]
    public class ChangeScholarshipAddFundingProfile : Migration
    {
        public readonly string[] NEW_COLUMNS
            = { "Fafsa", "UserDerived", "MinimumSeekerNeed", "MaximumSeekerNeed",
              "Emergency", "Traditional",
              "MinimumAnnualAmount", "MaximumAnnualAmount", "MinimumNumberOfAwards", "MaximumNumberOfAwards"  };

        public override void Up()
        {
            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[0], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[1], DbType.Boolean, ColumnProperty.Null);

            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[2], DbType.Double, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[3], DbType.Double, ColumnProperty.Null);

            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[4], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[5], DbType.Boolean, ColumnProperty.Null);

            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[6], DbType.Double, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[7], DbType.Double, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[8], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, NEW_COLUMNS[9], DbType.Int32, ColumnProperty.Null);
        }

        public override void Down()
        {
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(AddScholarship.TABLE_NAME, col);
            }
        }

    }
}
