﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(162)]
	public class AddHistoryTableProcedures : Migration
	{
		public const string TABLE_NAME = "SBNeedHistoryTables";
		public const string PRIMARY_KEY_COLUMN = "SBNeedHistoryTableID";
		protected static readonly string FkLastUpdatedBy = string.Format("FK_{0}_LastUpdatedBy", TABLE_NAME);

		public static readonly string[] Columns = new[] { PRIMARY_KEY_COLUMN, 
								"TableName",
                                "LastUpdatedBy", 
								"LastUpdateDate"
                                };

		public override void Up()
        {
            int columnCounter = 0;
            Database.AddTable(TABLE_NAME,
								new Column(Columns[columnCounter++], DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity), /*Id*/
								new Column(Columns[columnCounter++], DbType.String, 50, ColumnProperty.NotNull), /*TableName*/
								new Column(Columns[columnCounter++], DbType.Int32, ColumnProperty.NotNull), /*LastUpdatedBy*/
								new Column(Columns[columnCounter], DbType.DateTime, ColumnProperty.Null, "(getdate())") /*LastUpdateDate*/
                              
                );

			Database.AddForeignKey(FkLastUpdatedBy, TABLE_NAME, "LastUpdatedBy", "SBUser", "SBUserId");

			//int adminUserId = DataHelper.GetAdminUserId(Database);
			//string[] insertColumns = new [] {	Columns[1], Columns[2]};
			//Database.Insert(TABLE_NAME, insertColumns, new [] { "SBUser", adminUserId.ToString() });
			//Database.Insert(TABLE_NAME, insertColumns, new [] { "SBSeeker", adminUserId.ToString() });

			Database.ExecuteNonQuery(_dropSpRebuildHistoryTriggers);
			Database.ExecuteNonQuery(_dropSpRebuildHistoryTriggerForTable);
			Database.ExecuteNonQuery(_dropSpCreateHistoryTables);
			Database.ExecuteNonQuery(_spRebuildHistoryTriggers);
			Database.ExecuteNonQuery(_spCreateHistoryTables);
			Database.ExecuteNonQuery(_spRebuildHistoryTriggerForTable);
		}

		public override void Down()
		{
			Database.RemoveForeignKey(TABLE_NAME, FkLastUpdatedBy);
			Database.RemoveTable(TABLE_NAME);
			Database.ExecuteNonQuery(_dropSpRebuildHistoryTriggers);
			Database.ExecuteNonQuery(_dropSpRebuildHistoryTriggerForTable);
			Database.ExecuteNonQuery(_dropSpCreateHistoryTables);
		}

		private const string _dropSpRebuildHistoryTriggerForTable =
@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPRebuildHistoryTriggerForTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPRebuildHistoryTriggerForTable]
";
		private const string _dropSpRebuildHistoryTriggers =
@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPRebuildHistoryTriggers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPRebuildHistoryTriggers]
";
		private const string _dropSpCreateHistoryTables =
@"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPCreateHistoryTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPCreateHistoryTables]
";
		private const string _spRebuildHistoryTriggers =
@"
create procedure [dbo].[SPRebuildHistoryTriggers]
-- Creates update and delete triggers to maintain HISTORY tables
as
BEGIN

	declare HistoryTableCursor cursor for 
	select replace(TABLE_NAME,'HISTORY','') as table_name from INFORMATION_SCHEMA.TABLES where TABLE_NAME like '%HISTORY'
	
	declare @t varchar(255)
	
	open HistoryTableCursor
	fetch next from HistoryTableCursor into @t
	
	while (@@fetch_status = 0)
	BEGIN
		exec SPRebuildHistoryTriggerForTable @t
		fetch next from HistoryTableCursor into @t
	END -- end of cursor loop
	close HistoryTableCursor
	deallocate HistoryTableCursor
END
";
		private const string _spRebuildHistoryTriggerForTable =
@"
CREATE   procedure [dbo].[SPRebuildHistoryTriggerForTable] @t varchar(255)
as
BEGIN
	declare @dropUpdateTrigger nvarchar(max), @dropDeleteTrigger nvarchar(max),
		@insertClause nvarchar(max), @selectClause nvarchar(max), 
		@joinClause nvarchar(max), @whereClause nvarchar(max),
		@dateUpdateJoinCondition nvarchar(max)
	select @insertClause = '', @selectClause  = '', @joinClause  = '', @whereClause = '', @dateUpdateJoinCondition = ''
	
	-- SQL to drop the triggers if they exists
	set @dropUpdateTrigger = 'if exists (select * from dbo.sysobjects where id = object_id(''Update'+@t+''') and OBJECTPROPERTY(id, N''IsTrigger'') = 1)
	drop trigger [dbo].[Update'+@t+']
	
	'
	
	set @dropDeleteTrigger = 'if exists (select * from dbo.sysobjects where id = object_id(''Delete'+@t+''') and OBJECTPROPERTY(id, N''IsTrigger'') = 1)
	drop trigger [dbo].[Delete'+@t+']
	
	'
	
	-- Build up insert and select clauses from all columns in table
	select 
	@insertClause = @insertClause + case when @insertClause != '' then ',' else '' end 
		+ COLUMN_NAME,
	@selectClause = @selectClause + case when @selectClause != '' then ',' else '' end
		+ 'D.' + COLUMN_NAME
	from INFORMATION_SCHEMA.COLUMNS cols 
	where TABLE_NAME = @t and DATA_TYPE != 'text'
	
	-- Build up where clause from non-PK, non-LastUpdate* columns 
	select
	--@whereClause1 = case when len(@whereClause) > 3800 then @whereClause else @whereClause1 end,
	--@whereClause = case when len(@whereClause) > 3800 then '' else @whereClause end,
	@whereClause  = @whereClause + case when @whereClause != '' then char(10) + '	OR ' else '' end 
		+ '(D.' + cols.COLUMN_NAME + ' <> I.' + cols.COLUMN_NAME + 
		case when cols.IS_NULLABLE = 'YES' then '
			OR	(D.' + cols.COLUMN_NAME + ' IS NULL AND I.' + cols.COLUMN_NAME + ' IS NOT NULL)
			OR	(D.' + cols.COLUMN_NAME + ' IS NOT NULL AND I.' + cols.COLUMN_NAME + ' IS NULL))'
		else ')' end
	from INFORMATION_SCHEMA.COLUMNS cols 
	left outer join (INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kc 
			on tc.CONSTRAINT_NAME = kc.CONSTRAINT_NAME 
			and tc.TABLE_NAME = kc.TABLE_NAME)
		on tc.TABLE_NAME = cols.TABLE_NAME and kc.COLUMN_NAME = cols.COLUMN_NAME and tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
	where cols.TABLE_NAME = @t and tc.TABLE_NAME is null and cols.COLUMN_NAME not in ('LastUpdateBy', 'LastUpdateDate')
		and cols.DATA_TYPE != 'text'
	
	
	-- Build join clauses from primary key(s)
	select @joinClause = @joinClause + case when @joinClause != '' then char(10) + '		AND	' else '' end
		+ 'D.' + COLUMN_NAME + ' = I.' + COLUMN_NAME,
	@dateUpdateJoinCondition = @dateUpdateJoinCondition + case when @dateUpdateJoinCondition != '' then char(10) + '		AND	' else '' end
		+ 'I.' + COLUMN_NAME + ' = A.' + COLUMN_NAME
	from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
	inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kc on tc.CONSTRAINT_NAME = kc.CONSTRAINT_NAME and tc.TABLE_NAME = kc.TABLE_NAME
	where tc.TABLE_NAME = @T and tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
	order by ORDINAL_POSITION
	
	
	-- Finish building clauses
	set @insertClause = '
		INSERT dbo.'+@t+'HISTORY 
			(' + @insertClause + ')'
	set @selectClause = '
		SELECT 
			' + @selectClause 
	set @joinClause = '
		FROM Deleted D
		INNER JOIN Inserted I 
		ON 	(' + @joinClause + ')'
	if (len(@whereClause) > 0)
		set @whereClause = '
		WHERE	' + @whereClause
	
	declare @updatePreamble nvarchar(max), @updatePostamble nvarchar(max),
		@deletePreamble nvarchar(max), @deletePostamble nvarchar(max)
	
	-- Beginning of SQL to create the update trigger
	set @updatePreamble = 'CREATE TRIGGER Update' + @t + ' ON dbo.' + @t + ' 
	AFTER UPDATE
	AS
	BEGIN
	
		-- Check for updated rows, if none then exit
		IF @@ROWCOUNT = 0
			RETURN
	
		-- Disable ^x rows affected^ messages for history table inserts
		SET NOCOUNT ON
	
		-- Copy modified record(s) into ' + @t + 'History table
	'
	
	-- Tail end of the update trigger
	set @updatePostamble = '
	
		-- Update LastUpdateDate if not explicitly set
		IF NOT UPDATE(LastUpdateDate)
		BEGIN
			UPDATE dbo.'+@t+'
			SET LastUpdateDate = GETDATE()
			FROM Inserted I
			INNER JOIN dbo.'+@t+' A 
			ON (' + @dateUpdateJoinCondition + ')
		END
	END
	'
	
	-- Beginning of SQL to create the delete trigger
	set @deletePreamble = 'CREATE TRIGGER Delete' + @t + ' ON dbo.' + @t + ' 
	AFTER DELETE
	AS
	BEGIN
	
		-- Check for updated rows, if none then exit
		IF @@ROWCOUNT = 0
			RETURN
	
		-- Disable ^x rows affected^ messages for history table inserts
		SET NOCOUNT ON
	
		-- Copy modified record(s) into ' + @t + 'History table
	'
	
	-- Tail end of the delete trigger
	set @deletePostamble = '
		FROM Deleted D
	END
	'
	
	-- Run SQL to drop and create update trigger
	exec sp_executesql @dropUpdateTrigger
	
	print ' whereclause 2'	
	exec ('PRINT N''' + @whereClause + '''')
	exec ('exec sp_executesql N''' + @updatePreamble + @insertClause + @selectClause + @joinClause + @whereClause + @updatePostamble + '''')
	print 'Created update trigger for ' +@t
	
	-- Run SQL to drop and create delete trigger
	exec sp_executesql @dropDeleteTrigger
	exec ('exec sp_executesql N''' + @deletePreamble + @insertClause + @selectClause + @deletePostamble + '''')
	print 'Created delete trigger for ' +@t
END

";

		private const string _spCreateHistoryTables =
@"
CREATE   procedure [dbo].[SPCreateHistoryTables]
as
BEGIN
	--
	-- Generates any missing history tables for the Common DB
	--
	-- Created by David Mitchell 09/29/2004
	-- modified by Greg Martin 08/25/2008

	set nocount on

	declare @query nvarchar(max)
	declare @tablename varchar(50)

	set @query = ''

	declare histgen cursor for 
		select orig.name
		from sysobjects orig
		where orig.xtype = 'U'
		and orig.name not like '%LUT'
		and orig.name not like '%DRAFT'
		and orig.name not like '%HISTORY'
		and not exists (select name from sysobjects where name = orig.name + 'HISTORY')
		and orig.name in (select TableName from SBNeedHistoryTables)
		and orig.name not in ('TrackSSNs')
		and orig.name not like '[_]%'
		and orig.name not like 'sys%'

	open histgen
	fetch next from histgen into @tablename
	WHILE @@FETCH_STATUS = 0
	BEGIN

		set @query = 'create table dbo.' + @tablename + 'HISTORY ('
		
		select @query = @query + ' [' + c.name + '] ' + t.name
		+ case when t.variable = 1 or t.name = 'char' then '(' + cast(c.length as varchar(10)) +  ')' else '' end
		+ case when c.xtype in (106) then '(' + cast(c.xprec as varchar(10)) + ',' + cast(c.xscale as varchar(10)) + ')' else '' end 
		+ case when c.isnullable = 1 then ' NULL' else ' NOT NULL' end
		+ ','
		from syscolumns c
		inner join systypes t on c.xtype = t.xtype
		where c.id = object_id(@tableName)  and t.name <> 'sysname'
		order by c.colorder
		
		declare @reverseQuery nVarChar(max)
		set @reverseQuery = reverse(@query)
		if substring(@reverseQuery, 1, 1) = ',' 
		begin
			set @query = substring(@query, 1, len(@query) - 1)
		end
		set @query = @query + ')'
		
		--print @query	
		exec sp_executesql @query

		fetch next from histgen into @tablename
	END
	CLOSE histgen
	DEALLOCATE histgen

END
";
	}
}