using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using ScholarBridge.Common;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web
{
    public class FileHelper
    {
        public static void CreateAndSendZipFile(HttpResponse response, IEnumerable<Attachment> attachments, string directory)
        {
            var mf = new MultipleFiles();
            attachments.ForEach(a => mf.AddFile(Path.Combine(directory, a.Name), a.GetFullPath(ConfigHelper.GetAttachmentsDirectory())));
            String file = null;
            try
            {
                file = mf.CreateZip(Path.GetTempFileName());
                SendZipFile(response, file);
            }
            finally
            {
                if (null != file)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch
                    {
                    }
                }
            }
        }

        public static void SendFile(HttpResponse response, string fileName, string mimeType, string filePath)
        {
            response.ClearHeaders();
            response.AddHeader("Content-Type", mimeType);
            response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", fileName));

            response.WriteFile(filePath);
            response.Flush();
            response.Close();
        }

        public static void SendZipFile(HttpResponse response, string file)
        {
            SendFile(response, "attachments.zip", "application/zip", file);
        }
    }
}