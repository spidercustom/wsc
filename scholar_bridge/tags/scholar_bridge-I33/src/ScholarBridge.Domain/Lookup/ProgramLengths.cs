﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum ProgramLengths
    {
        [DisplayName("1 year")]
        OneYear = 1,
        [DisplayName("2 years")]
        TwoYears = 2,
        [DisplayName("4 years")]
        FourYears = 4
    }
}
