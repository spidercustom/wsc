using System;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Domain
{
    public class Match : CriteriaCountBase, ISoftDeletable
    {
        public virtual int Id { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual Scholarship Scholarship { get; set; }

        public virtual Application Application { get; set; }

        public virtual MatchStatus MatchStatus { get; set; }
		public virtual bool AddedViaMatch { get; set; }
		public virtual bool IsDeleted { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual string ScholarshipName
        {
            get { return null == Scholarship ? null : Scholarship.DisplayName; }
        }

		/// <summary>
		/// Essentially Match status is <see cref="ApplicationStatus"/> with a couple extra statuses
		/// </summary>
        public virtual MatchApplicationStatus MatchApplicationStatus
        {
            get
            {
                if (Scholarship.Stage==ScholarshipStages.Awarded || Scholarship.Stage==ScholarshipStages.Awarded) 
                    return MatchApplicationStatus.Closed;
                
                var result = MatchApplicationStatus.Unknown;
                 
                if (Application==null ) 
                   {
                  		DateTime applicationDueDate = Scholarship.ApplicationDueDate.Value.AddDays(1).AddSeconds(-1);
                        if (DateTime.Today< Scholarship.ApplicationStartDate) result = MatchApplicationStatus.NotApplied;

                        if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate,applicationDueDate))
                            result = MatchApplicationStatus.NotApplied;

                        if (DateTime.Today >= applicationDueDate) result = MatchApplicationStatus.Closed;

                        if (Scholarship.AwardPeriodClosed.HasValue) result = MatchApplicationStatus.Closed;
                    } 

                else
                    {
						switch (Application.ApplicationStatus)
						{
							case ApplicationStatus.Applying:
								result = MatchApplicationStatus.Applying;
								break;
							case ApplicationStatus.Applied:
								result = MatchApplicationStatus.Applied;
								break;
							case ApplicationStatus.Awarded:
								result = MatchApplicationStatus.Awarded;
								break;
							case ApplicationStatus.BeingConsidered:
								result = MatchApplicationStatus.BeingConsidered;
								break;
							case ApplicationStatus.Closed:
								result = MatchApplicationStatus.Closed;
								break;
							case ApplicationStatus.Offered:
								result = MatchApplicationStatus.Offered;
								break;
						}
                    }

                return result; 
            }
        }

        public virtual string MatchApplicationStatusString
        {
            get { return MatchApplicationStatus.GetDisplayName(); }   
        }
    }
}