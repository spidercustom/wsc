﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipSearch.ascx.cs" Inherits="ScholarBridge.Web.Admin.ScholarshipSearch" %>
<table style="border: solid 1px black;">
    <tr>
        <td>
            Status:</td>
        <td>
            <asp:DropDownList ID="ddlScholarshipStatus" runat="server">
                <asp:ListItem Text="" Value=""></asp:ListItem>
                <asp:ListItem Text="Activated" Value="Activated"></asp:ListItem>
                <asp:ListItem Text="Not Activated" Value="NotActivated"></asp:ListItem>
                <asp:ListItem Text="Closed" Value="Closed"></asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            Provider:</td>
        <td>
            <asp:DropDownList ID="ddlProvider" runat="server" AppendDataBoundItems="true" 
                DataSourceID="providerDropDownSource" DataTextField="Name" DataValueField="Id">
                <asp:ListItem Text="" Value="-1"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Name (contains):
        </td>
        <td>
            <asp:TextBox ID="txtNameSearch" runat="server"></asp:TextBox>
        </td>
        <td>
            Intermediary:</td>
        <td>
            <asp:DropDownList ID="ddlIntermediary" runat="server" 
                AppendDataBoundItems="true" DataSourceID="intermediaryDropDownSource" 
                DataTextField="Name" DataValueField="Id">
                <asp:ListItem Text="" Value="-1"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Button ID="btnSearchScholarships" runat="server" 
                OnClick="btnSearchScholarships_Click" Text="Search Scholarships" 
                CausesValidation="False" />
        </td>
    </tr>
</table>
<asp:GridView ID="gvScholarshipList" runat="server" AutoGenerateColumns="False">
    <Columns>
        <asp:TemplateField HeaderText="Scholarship Name" SortExpression="Name">
            <ItemTemplate>
                <asp:Label ID="lblScholarshipName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Provider" SortExpression="Provider">
            <ItemTemplate>
                <asp:Label ID="label1" runat="server" Text='<%# Eval("Provider.Name") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="AcademicYear" HeaderText="Year" SortExpression="AcademicYear" />
        <asp:BoundField DataField="Stage" HeaderText="Stage" SortExpression="Stage" />
        <asp:BoundField DataField="ApplicationDueDate" HeaderText="App Due Date" DataFormatString="{0:d}" SortExpression="ApplicationDueDate" />
        <asp:BoundField DataField="AwardDate" HeaderText="Award Date" DataFormatString="{0:d}" SortExpression="AwardDate"/>
    </Columns>
</asp:GridView>

<asp:ObjectDataSource ID="providerDropDownSource" 
    runat="server" 
    SelectMethod="SelectProvidersForDropDown"
    TypeName="ScholarBridge.Web.Admin.ScholarshipSearch"
    OnObjectCreating="providerDropDownSource_ObjectCreating">
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="intermediaryDropDownSource" 
    runat="server" 
    SelectMethod="SelectIntermediariesForDropDown"
    TypeName="ScholarBridge.Web.Admin.ScholarshipSearch"
    OnObjectCreating="intermediaryDropDownSource_ObjectCreating">
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="scholarshipDataSource" 
    runat="server" 
    SelectMethod="SelectScholarships"
    TypeName="ScholarBridge.Web.Admin.ScholarshipSearch"
    OnObjectCreating="scholarshipDataSource_ObjectCreating">
</asp:ObjectDataSource>




