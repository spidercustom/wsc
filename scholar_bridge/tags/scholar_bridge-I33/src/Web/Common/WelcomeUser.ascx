﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeUser.ascx.cs" Inherits="ScholarBridge.Web.Common.WelcomeUser" %>
<!--Welcome text starts here-->
     <div id="WelcomeTextContainer">
        <div class="WelcomeText">Welcome back
        <%--<asp:HyperLink ID="profileLnk" runat="server" NavigateUrl="~/Profile/Default.aspx">--%>
        <asp:Literal ID="LoginFirstLastName" runat="server" />!                   
        <%--</asp:HyperLink>--%>
         </div>
    </div>
<!--Organization name starts here-->
 <div id="OrganizationNameContainer">
      <div class="OrganizationName"><asp:Literal ID="OrgName" runat="server" /></div>
 </div>
 <!--Organization name ends here-->
