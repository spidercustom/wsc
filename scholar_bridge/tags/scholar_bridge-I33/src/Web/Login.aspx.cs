﻿using System;
using System.Web;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MainMenuHelper.ResetActiveMenu(this);
            
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
            	string[] names = HttpContext.Current.User.Identity.Name.Split('!');
                Response.Redirect(loginForm.DestinationForUser(names[0]));
            }
        }

       
    }
}
