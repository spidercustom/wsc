using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Domain
{
    public class Application : CriteriaCountBase
    {
        public const string APPLICATION_SUMBISSION_RULESET = "Application-Submission";

        private SeekerAddress address;

		#region C'tors

		public Application()
        {
            InitializeMembers();
        }

        public Application(Match m) : this()
        {
            Scholarship = m.Scholarship;
            Seeker = m.Seeker;
            
            InitializeMembers();
        }

		#endregion


		private void InitializeMembers()
        {
            ApplicantName = new PersonName();
            Attachments = new List<Attachment>();
            Ethnicities = new List<Ethnicity>();
            Religions = new List<Religion>();
            Sports = new List<Sport>();

            AcademicAreas = new List<AcademicArea>();
            Careers = new List<Career>();
            CommunityServices = new List<CommunityService>();
            Hobbies = new List<SeekerHobby>();
            Clubs = new List<Club>();
            MatchOrganizations = new List<SeekerMatchOrganization>();
            Companies = new List<Company>();
            Address = new SeekerAddress();
            SupportedSituation = new SupportedSituation();
            QuestionAnswers = new List<ApplicationQuestionAnswer>(); 
        }

        public virtual int Id { get; set; }
        public virtual Scholarship Scholarship { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual ApplicationStages Stage { get; set; }

        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should be between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? SubmittedDate { get; set; }
        public virtual bool Finalist { get; set; }
        public virtual AwardStatuses AwardStatus { get; set; }

        public virtual PersonName ApplicantName { get; set; }
        public virtual string Email { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual PhoneNumber MobilePhone { get; set; }
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should be between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? DateOfBirth { get; set; }

        public virtual County County { get; set; }

        public virtual SeekerAddress Address
        {
            get
            {
                if (address == null)
                    address = new SeekerAddress();
                return address;
            }
            set
            {
                address = value;
            }
        }

        public virtual bool FirstGeneration { get; set; }

        public virtual Genders Gender { get; set; }

        public virtual CurrentSchool CurrentSchool { get; set; }

        [StringLengthValidator(0, 1500)]
        public virtual string PersonalStatement { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyChallenge { get; set; }

        [StringLengthValidator(0, 150)]
        public virtual string MyGift { get; set; }

        public virtual SeekerAcademics SeekerAcademics { get; set; }

        [StringLengthValidator(0, 100)]
        public virtual string Words { get; set; }
        [StringLengthValidator(0, 100)]
        public virtual string Skills { get; set; }

        public virtual IList<Ethnicity> Ethnicities { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string EthnicityOther { get; set; }

        public virtual IList<Religion> Religions { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ReligionOther { get; set; }

        public virtual IList<Sport> Sports { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string SportOther { get; set; }

        public virtual IList<AcademicArea> AcademicAreas { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string AcademicAreaOther { get; set; }

        public virtual IList<Career> Careers { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CareerOther { get; set; }

        public virtual IList<CommunityService> CommunityServices { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CommunityServiceOther { get; set; }

        public virtual IList<SeekerHobby> Hobbies { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string HobbyOther { get; set; }

        public virtual IList<Club> Clubs { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ClubOther { get; set; }

        public virtual IList<SeekerMatchOrganization> MatchOrganizations { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string MatchOrganizationOther { get; set; }

        public virtual IList<Company> Companies { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string CompanyOther { get; set; }

        public virtual bool IsWorking { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string WorkHistory { get; set; }

        public virtual bool IsService { get; set; }
        [StringLengthValidator(0, 250)]
        public virtual string ServiceHistory { get; set; }
        [StringLengthValidator(0, 50)]
        public virtual string AddressCity { get; set; }
        [StringLengthValidator(0, 50)]
        public virtual string AddressCounty { get; set; }
        public virtual SupportedSituation SupportedSituation { get; set; }
        
        public virtual bool? HasFAFSACompleted { get; set; }
        public virtual decimal? ExpectedFamilyContribution { get; set; }

        public virtual IList<Attachment> Attachments { get; protected set; }
        public virtual IList<ApplicationQuestionAnswer> QuestionAnswers { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual bool HasAttachments { get { return Attachments != null && Attachments.Count > 0; } }
        public virtual void CopyFromSeekerandScholarship()
        {
            var seeker = Seeker;
            if (seeker == null)
                throw new  NullReferenceException("Seeker");

            var scholarship = Scholarship;

            if (scholarship == null)
                throw new  NullReferenceException("Scholarship");

            ApplicantName = seeker.Name;
            //
            AcademicAreaOther = seeker.AcademicAreaOther;
            AcademicAreas = new List<AcademicArea>(seeker.AcademicAreas);
            Address = seeker.Address;
            CompanyOther = seeker.CompanyOther;
            Companies =new List<Company>(seeker.Companies);
            ApplicantName = seeker.Name;
            CareerOther = seeker.CareerOther;
            Careers =new List<Career>( seeker.Careers);
            ClubOther = seeker.ClubOther;
            Clubs =new List<Club>(seeker.Clubs);
            CommunityServiceOther = seeker.CommunityServiceOther;
            CommunityServices =new List<CommunityService>( seeker.CommunityServices);
            County = seeker.County;
            CurrentSchool = seeker.CurrentSchool;
            DateOfBirth = seeker.DateOfBirth;
            Email = seeker.User.Email;
            Ethnicities = new List<Ethnicity>(seeker.Ethnicities);
            EthnicityOther = seeker.EthnicityOther;
            FirstGeneration = seeker.FirstGeneration;
            Gender = seeker.Gender;
            Hobbies = new List<SeekerHobby>(seeker.Hobbies);
            HobbyOther = seeker.HobbyOther;
            MatchOrganizations = new List<SeekerMatchOrganization>(seeker.MatchOrganizations);
            MatchOrganizationOther = seeker.MatchOrganizationOther;
            MobilePhone = seeker.MobilePhone;
            MyChallenge = seeker.MyChallenge;
            MyGift = seeker.MyGift;
            ExpectedFamilyContribution = seeker.ExpectedFamilyContribution;
            HasFAFSACompleted = seeker.HasFAFSACompleted;
            PersonalStatement = seeker.PersonalStatement;
            Phone = seeker.Phone;
            ReligionOther = seeker.ReligionOther;
            Religions = new List<Religion>(seeker.Religions);
            SeekerAcademics =(SeekerAcademics) seeker.SeekerAcademics.Clone();
            //SupportedSituation=new SupportedSituation();
            SupportedSituation.ResetTypesOfSupport(seeker.SupportedSituation.TypesOfSupport); 
            Skills = seeker.Skills;
            SportOther = seeker.SportOther;
            Sports=new List<Sport>(seeker.Sports);
            Words = seeker.Words;
            ServiceHistory = seeker.ServiceHistory;
            WorkHistory = seeker.WorkHistory;
            IsService = seeker.IsService;
            IsWorking = seeker.IsWorking;
            AddressCity = seeker.AddressCity;
            AddressCity = seeker.AddressCounty;
       

            //copy questions from scholarship
            foreach (ScholarshipQuestion q in scholarship.AdditionalQuestions)
            {

                var qans = new ApplicationQuestionAnswer
                           	{
                                   Question = q,
                                   Application = this,
                                   AnswerText = "",
                                   LastUpdate = new ActivityStamp(seeker.User)


                               };
                QuestionAnswers.Add(qans);
            }
        }

		public virtual ApplicationStatus ApplicationStatus
		{
			get
			{
				DateTime applicationDueDate = Scholarship.ApplicationDueDate.Value.AddDays(1).AddSeconds(-1);
				if (Scholarship.Stage == ScholarshipStages.Awarded || Scholarship.Stage == ScholarshipStages.Awarded)
					return ApplicationStatus.Closed;

				var result = ApplicationStatus.Unknown;

				if (Stage == ApplicationStages.Submitted)
				{

					if (DateTime.Today < Scholarship.ApplicationStartDate) result = ApplicationStatus.Applied;

					if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate, applicationDueDate))
						result = ApplicationStatus.Applied;

					if (DateTime.Today >= applicationDueDate) result = ApplicationStatus.BeingConsidered;
					if (Scholarship.AwardPeriodClosed.HasValue) result = ApplicationStatus.BeingConsidered;
				}

				else
				{
					if (DateTime.Today < Scholarship.ApplicationStartDate) result = ApplicationStatus.Applying;

					if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate, applicationDueDate))
						result = ApplicationStatus.Applying;

					if (DateTime.Today >= applicationDueDate) result = ApplicationStatus.Closed;

					if (Scholarship.AwardPeriodClosed.HasValue) result = ApplicationStatus.Closed;
				}

				return result;
			}
		}



        public virtual void Submit()
        {
            if (Stage != ApplicationStages.Submitted)
            {
                Stage = ApplicationStages.Submitted;
                SubmittedDate = DateTime.Now; 
            }
        }

        public virtual bool CanEdit()
        {
            return Stage != ApplicationStages.Submitted;
        }

        public virtual ValidationResults ValidateSubmission()
        {
            var results = Validate(APPLICATION_SUMBISSION_RULESET);
            //FIXME: We need to return validation results as applicable
            return results;
        }

        private ValidationResults Validate(string ruleSet)
        {
            var validator = ValidationFactory.CreateValidator<Application >(ruleSet);
            return validator.Validate(this);
        }
    }
}