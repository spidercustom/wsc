using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(146)]
    public class AddSeekerAcceptedCollegesRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBSeekerCollegeAcceptedRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCollegeLUT"; }
        }
    }
}