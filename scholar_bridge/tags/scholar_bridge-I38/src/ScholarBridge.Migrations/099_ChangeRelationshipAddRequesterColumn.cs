﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
 
    [Migration(99)]
    public class ChangeRelationshipAddRequesterColumn : Migration
    {
        public const string FK_SBRelationship_SBRelationshipStageLUT = "FK_SBRelationship_SBRelationshipStageLUT";
        private readonly string[] columns = new[] { "SBRelationshipStageIndex", "SBRelationshipStage" };
    public override void Up()
        {
            Database.AddColumn(AddRelationship.TABLE_NAME, "Requester", DbType.String,30, ColumnProperty.NotNull);
            Database.RenameColumn(AddRelationship.TABLE_NAME,  "ApprovalStatus","Status");
            Database.RemoveForeignKey(AddRelationship.TABLE_NAME, FK_SBRelationship_SBRelationshipStageLUT);
            Database.RemoveColumn(AddRelationship.TABLE_NAME, "SBRelationshipStageIndex");
            Database.RemoveTable(AddRelationshipStage.TABLE_NAME); 
        }

        public override void Down()
        {
            Database.AddTable(AddRelationshipStage.TABLE_NAME,
                new Column(columns[0], DbType.Int32, ColumnProperty.PrimaryKey),
                new Column(columns[1], DbType.String, 50, ColumnProperty.NotNull)
            );


            Database.AddColumn(AddRelationship.TABLE_NAME, new Column(AddRelationshipStage.PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.Null));
            Database.AddForeignKey(FK_SBRelationship_SBRelationshipStageLUT, AddRelationship.TABLE_NAME,
                AddRelationshipStage.PRIMARY_KEY_COLUMN, AddRelationshipStage.TABLE_NAME, AddRelationshipStage.PRIMARY_KEY_COLUMN);

            Database.Insert(AddRelationship.TABLE_NAME, columns, new[] { "0", "Pending" });
            Database.Insert(AddRelationship.TABLE_NAME, columns, new[] { "1", "Active" });
            Database.Insert(AddRelationship.TABLE_NAME, columns, new[] { "2", "InActive" });

            Database.RemoveColumn(AddRelationship.TABLE_NAME, "Requester");
            Database.RenameColumn(AddRelationship.TABLE_NAME, "Status", "ApprovalStatus");
            
        }
    }
}
