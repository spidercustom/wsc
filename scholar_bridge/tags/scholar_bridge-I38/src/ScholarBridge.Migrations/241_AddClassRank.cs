﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;
using System.Data;

namespace ScholarBridge.Migrations
{
    [Migration(241)]
    public class AddClassRank : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "ClassRank";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
