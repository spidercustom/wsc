﻿using System;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class AdminScholarshipNotes : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

         

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //if (ScholarshipInContext.AdminNotes != null)
            //    notesLiteral.Text = ScholarshipInContext.AdminNotes.ToStringTableFormat();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ScholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        private void PopulateScreen()
        {
            if (ScholarshipInContext.AdminNotes != null)
                notesLiteral.Text = ScholarshipInContext.AdminNotes.ToStringTableFormat();
        }

        protected void addNotes_Click(object sender, EventArgs e)
        {
            
            PopulateObjects();
            PopulateScreen();
        	newNotesTb.Text = string.Empty;
        }

         

        #region IWizardStepControl implementation
        public override void Save()
        {
            PopulateObjects();
            
        }

        public override void PopulateObjects()
        {
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipInContext.AppendAdminNotes(UserContext.CurrentUser, newNotesTb.Text);
            ScholarshipService.Save(ScholarshipInContext);
        }

        public override bool ValidateStep()
        {

            return true;
        }

        
        #endregion
    }
}