﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Web.Common
{
    public partial class PrintApplication : UserControl
    {
        public Application  ApplicationToView{ get; set; }
       
        protected void Page_Load(object sender, EventArgs e)
        {
             
            scholarshipName.Text = ApplicationToView.Scholarship.Name ;
            PersonalInfoName.Text = ApplicationToView.ApplicantName.NameFirstLast;
            PersonalInfoEmail.Text = ApplicationToView.Email;
             if (null != ApplicationToView.Address)
                    PersonalInfoAddress.Text = ApplicationToView.Address.ToString().Replace("\r\n", "<br/>\r\n");
            if (null != ApplicationToView.County)
                    PersonalInfoCounty.Text = ApplicationToView.AddressCounty;
            
            if (null != ApplicationToView.Phone)
                    PersonalInfoPhone.Text = ApplicationToView.Phone.Number;
                if (null != ApplicationToView.MobilePhone)
                    PersonalInfoMobile.Text = ApplicationToView.MobilePhone.Number;

                if (ApplicationToView.DateOfBirth.HasValue)
                    PersonalInfoBirthDay.Text  = ApplicationToView.DateOfBirth.Value.ToString("MM/dd/yyyy");
                
            PersonalInfoGender.Text = ApplicationToView.Gender.ToString();
            PersonalInfoStatement.Text = ApplicationToView.PersonalStatement;
            PersonalInfoMyChallenge.Text = ApplicationToView.MyChallenge;
            PersonalInfoMyGift.Text = ApplicationToView.MyGift;
            PersonalInfoFiveWords.Text = ApplicationToView.Words;
            PersonalInfoFiveSkills.Text = ApplicationToView.Skills;
            //Religion

             if (null !=ApplicationToView.Religions )
             {
                
             AddRowToTable(ReligionTable,"My Religion/Faith:",
                 GetCommaSeperated((from x in ApplicationToView.Religions select x.Name).ToArray()));

             }
            
            if ( !String.IsNullOrEmpty( ApplicationToView.ReligionOther ))
             {
                AddRowToTable(ReligionTable,"Other Religion:",ApplicationToView.ReligionOther);

             }
            //My Heritage:
            
            if (null != ApplicationToView.Ethnicities)
            {
                AddRowToTable(HeritageTable, "My Heritage:",
                    GetCommaSeperated((from x in ApplicationToView.Ethnicities select x.Name).ToArray()));
            }

            if (!String.IsNullOrEmpty(ApplicationToView.EthnicityOther))
            {
                AddRowToTable(HeritageTable , "Other Ethnicity:", ApplicationToView.EthnicityOther);

            }
            HeritageTable.Visible = HeritageTable.Rows.Count > 0;
            //Academic Infor

            //higschool
            if (null != ApplicationToView.CurrentSchool)
            {
                if (null != ApplicationToView.CurrentSchool.LastAttended)
                {
                    AddRowToTable(AcademicInfoHighSchoolTable, "What type of student are you currently?",
                        GetCommaSeperated(ApplicationToView.CurrentSchool.LastAttended.GetDisplayNames()));
                }

                if (null != ApplicationToView.CurrentSchool.HighSchool)
                {
                    AddRowToTable(AcademicInfoHighSchoolTable, "High School you are currently attending or graduate from:",
                          ApplicationToView.CurrentSchool.HighSchool.Name);
                    if (null != ApplicationToView.CurrentSchool.HighSchool.District)
                    {
                        AddRowToTable(AcademicInfoHighSchoolTable, "School District:",
                                      ApplicationToView.CurrentSchool.HighSchool.District.Name);
                    }
                }

            }
            

            if (null != ApplicationToView.SeekerAcademics)
            {
                

                if (ApplicationToView.SeekerAcademics.GPA.HasValue)
                    AddRowToTable(AcademicPeformanceTable , "GPA:",
                                  ApplicationToView.SeekerAcademics.GPA.Value.ToString("0.000"));

                if (null != ApplicationToView.SeekerAcademics.ClassRank )
                {
                    AddRowToTable(AcademicPeformanceTable, "Class Rank:",
                        ApplicationToView.SeekerAcademics.ClassRank.Name);
                }

                if (null != ApplicationToView.SeekerAcademics.SATScore)
                {
                    var sat = ApplicationToView.SeekerAcademics.SATScore;

                    if (sat.Commulative.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Cumulative:", sat.Commulative.Value.ToString());

                    if (sat.CriticalReading.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Critical Reading:", sat.CriticalReading.Value.ToString());

                    if (sat.Writing.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Writing:", sat.Writing.Value.ToString());

                    if (sat.Mathematics.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Mathematics:", sat.Mathematics.Value.ToString());
                }

                if (null != ApplicationToView.SeekerAcademics.ACTScore)
                {
                    var act = ApplicationToView.SeekerAcademics.ACTScore;
                    if (act.Commulative.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Cumulative:", act.Commulative.Value.ToString());

                    if (act.Reading.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Reading:", act.Reading.Value.ToString());

                    if (act.English.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "English:", act.English.Value.ToString());

                    if (act.Mathematics.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Mathematics:", act.Mathematics.Value.ToString());

                    if (act.Science.HasValue)
                        AddRowToTable(AcademicPeformanceTable, "Science:", act.Science.Value.ToString());

                    if (ApplicationToView.SeekerAcademics.Honors)
                    {
                        AddRowToTable(AcademicPeformanceTable, "Honors:",
                                      ApplicationToView.SeekerAcademics.HonorsDetail);
                    }
                    if (ApplicationToView.SeekerAcademics.APCredits)
                    {
                        AddRowToTable(AcademicPeformanceTable, "AP Credits:",
                                      ApplicationToView.SeekerAcademics.APCreditsDetail);
                    }
                    if (ApplicationToView.SeekerAcademics.IBCredits)
                    {
                        AddRowToTable(AcademicPeformanceTable, "IB Credits:",
                                      ApplicationToView.SeekerAcademics.IBCreditsDetail);
                    }
                }


                //My Academic Info College
                        if (null != ApplicationToView.SeekerAcademics.CollegesApplied)
                        {
                            AddRowToTable(AcademicInfoCollegeTable, "What types of schools are you considering?",
                                GetCommaSeperated((from x in ApplicationToView.SeekerAcademics.CollegesApplied select x.Name).ToArray()));
                        }

                         
                        if (null != ApplicationToView.SeekerAcademics.ProgramLengths)
                        {
                            AddRowToTable(AcademicInfoCollegeTable, "Length of program:",
                                GetCommaSeperated(ApplicationToView.SeekerAcademics.ProgramLengths.GetDisplayNames() ));
                        }

                        if (null != ApplicationToView.SeekerAcademics.AcademicPrograms)
                        {
                            AddRowToTable(AcademicInfoCollegeTable, "Academic program:",
                                GetCommaSeperated(ApplicationToView.SeekerAcademics.AcademicPrograms.GetDisplayNames()));
                        }

                        if (null != ApplicationToView.SeekerAcademics.SeekerStatuses)
                        {
                            AddRowToTable(AcademicInfoCollegeTable, "Enrollment status?",
                                GetCommaSeperated(ApplicationToView.SeekerAcademics.SeekerStatuses.GetDisplayNames()));
                        }
            }
            //first Generation
            if (ApplicationToView.FirstGeneration)
            {
                AddRowToTable(AcademicInfoCollegeTable, "First Generation in Your Family to Attend College:",
                              "Yes");
            }

            //type of college
            if (null != ApplicationToView.CurrentSchool)
            {
                if (null != ApplicationToView.CurrentSchool.TypeOfCollegeStudent)
                {
                    AddRowToTable(AcademicInfoCollegeTable, "What type of college student are you?",
                                  GetCommaSeperated(ApplicationToView.CurrentSchool.TypeOfCollegeStudent.GetDisplayNames()));
                }

                if (null != ApplicationToView.CurrentSchool.College)
                {
                    AddRowToTable(AcademicInfoCollegeTable, "College you are currently attending:",
                                  ApplicationToView.CurrentSchool.College.Name );

                    if (  ! string.IsNullOrEmpty( ApplicationToView.CurrentSchool.CollegeOther ))
                    {
                        AddRowToTable(AcademicInfoCollegeTable, "College Other:",
                                  ApplicationToView.CurrentSchool.CollegeOther);
                     }
                }
            }

            if (null != ApplicationToView.SeekerAcademics)
            {
                var college = ApplicationToView.SeekerAcademics.IsCollegeAppliedInWashington ? "In Washington" : "";

                if (ApplicationToView.SeekerAcademics.IsCollegeAppliedOutOfState )
                {
                    if (ApplicationToView.SeekerAcademics.IsCollegeAppliedInWashington)
                        college += ",";
                    
                    college += "Out of State";
                }

                if ((college.Length > 0) || (null != ApplicationToView.SeekerAcademics.CollegesApplied))
                {
                    AddRowToTable(AcademicInfoCollegeTable, "Colleges You are considering:",college);
                }

                if (null != ApplicationToView.SeekerAcademics.CollegesApplied)
                {
                    AddRowToTable(AcademicInfoCollegeTable, " Colleges:",
                    GetCommaSeperated((from x in ApplicationToView.SeekerAcademics.CollegesApplied select x.Name).ToArray()));
                }
             }

            //activities and interests
            //What are your academic interests?
            var showInterests = false;
            if (null != ApplicationToView.AcademicAreas)
            {
                 if (  ApplicationToView.AcademicAreas.Count>0)
                 {
                     showInterests = true;
                     AddRowToTable(InterestsTable, "Field of Study:",
                                   GetCommaSeperated((from x in ApplicationToView.AcademicAreas select x.Name).ToArray()));
                 }
            }
            if ( ! string.IsNullOrEmpty( ApplicationToView.AcademicAreaOther))
            {
                AddRowToTable(InterestsTable, "Field of Study:",ApplicationToView.AcademicAreaOther );
                showInterests = true;
            }
            if (null != ApplicationToView.Careers)
            {
                if (  ApplicationToView.Careers.Count>0)
                {
                    AddRowToTable(InterestsTable, "Careers:",
                                  GetCommaSeperated((from x in ApplicationToView.Careers select x.Name).ToArray()));
                    showInterests = true;
                }
            }
            if (! string.IsNullOrEmpty( ApplicationToView.CareerOther))
            {
                AddRowToTable(InterestsTable, "Careers:", ApplicationToView.CareerOther );
                showInterests = true;
            }
            InterestsTable.Visible = showInterests;
            //Are you or any family members affiliated with specific Organizations Or Companies?

            if (null != ApplicationToView.MatchOrganizations)
            {
                if ( ApplicationToView.MatchOrganizations.Count>0)
                {
                    AddRowToTable(OrganizationsTable, "Organizations:",
                                  GetCommaSeperated(
                                      (from x in ApplicationToView.MatchOrganizations select x.Name).ToArray()));
                }
            }
            if (! string.IsNullOrEmpty(  ApplicationToView.MatchOrganizationOther))
            {
                AddRowToTable(OrganizationsTable, "Organizations:",ApplicationToView.MatchOrganizationOther);
            }

            if (null != ApplicationToView.Companies)
            {
                if (ApplicationToView.Companies.Count>0)
                {
                    AddRowToTableSmallTextOnly(OrganizationsTable,
                                  GetCommaSeperated((from x in ApplicationToView.Companies select x.Name).ToArray()));
                }
            }
            if (! string.IsNullOrEmpty(  ApplicationToView.CompanyOther))

            {
                AddRowToTableSmallTextOnly(OrganizationsTable,  ApplicationToView.CompanyOther);
            }
            //What other groups or interests are you involved in?</

            var showGroups = false;
            if (null != ApplicationToView.Hobbies)
            {
                if ( ApplicationToView.Hobbies.Count>0)
                {
                    AddRowToTable(GroupsInvolvedTable, "Hobbies:",
                                  GetCommaSeperated((from x in ApplicationToView.Hobbies select x.Name).ToArray()));
                    showGroups = true;
                }
            }
            if (! string.IsNullOrEmpty( ApplicationToView.HobbyOther))
            {
                AddRowToTable(GroupsInvolvedTable, "Hobbies:", ApplicationToView.HobbyOther);
                showGroups = true;
            }

            if (null != ApplicationToView.Sports)
            {
                if (  ApplicationToView.Sports.Count >0)
                {
                    AddRowToTable(GroupsInvolvedTable, "Sports:",
                                  GetCommaSeperated((from x in ApplicationToView.Sports select x.Name).ToArray()));
                    showGroups = true;
                }
            }
            if (! string.IsNullOrEmpty(  ApplicationToView.SportOther))
            {
                AddRowToTable(GroupsInvolvedTable, "Sports:", ApplicationToView.SportOther);
                showGroups = true;
            }

            if (null != ApplicationToView.Clubs)
            {
                if ( ApplicationToView.Clubs.Count>0)
                {
                    AddRowToTable(GroupsInvolvedTable, "Clubs:",
                                  GetCommaSeperated((from x in ApplicationToView.Clubs select x.Name).ToArray()));
                    showGroups = true;
                }
            }
            if (! string.IsNullOrEmpty(  ApplicationToView.ClubOther))
            {
                AddRowToTable(GroupsInvolvedTable, "Clubs:", ApplicationToView.ClubOther);
                showGroups = true;
            }
            GroupsInvolvedTable.Visible = showGroups;
            //Are you working? Volunteering or Community Service?
            bool showServices = false;
            if ( ApplicationToView.IsWorking)
            {
                showServices = true;
                AddRowToTable(ServicesTable, "Working?", "Yes");
                if (!string.IsNullOrEmpty(ApplicationToView.WorkHistory))
                {
                AddRowToTable(ServicesTable, "Work History:", ApplicationToView.WorkHistory);
                }
            }
            

            if (ApplicationToView.IsService)
            {
                showServices = true;
                AddRowToTable(ServicesTable, "Volunteering?", "Yes");
                if (!string.IsNullOrEmpty(ApplicationToView.ServiceHistory))
                {
                    AddRowToTable(ServicesTable, "Service History:", ApplicationToView.ServiceHistory);
                }
            }
            ServicesTable.Visible = showServices;
            //finanical need
            var showFinancialNeed = false;
            if (!String.IsNullOrEmpty( ApplicationToView.MyChallenge ))
            {
                AddRowToTable(FinancialNeedTable, "Describe your financial Challenge:", ApplicationToView.MyChallenge);
                showFinancialNeed = true;
            }

            if (null!= ApplicationToView.SupportedSituation)
            {
                if (null != ApplicationToView.SupportedSituation.TypesOfSupport)
                {
                    if ( ApplicationToView.SupportedSituation.TypesOfSupport.Count>0 )
                    {
                        AddRowToTable(FinancialNeedTable, "Types of Support:",
                                      ApplicationToView.MyChallenge);

                        AddRowToTable(FinancialNeedTable, "Types of Support:",
                            GetCommaSeperated((from x in ApplicationToView.SupportedSituation.TypesOfSupport select x.Name).ToArray()));
                        showFinancialNeed = true;
                    }
                }
            }
            FinancialNeedTable.Visible = showFinancialNeed;
            var showFafsa = false;
            if  (ApplicationToView.HasFAFSACompleted.HasValue)
            {
                AddRowToTable(FafsaTable, "Have you completed the FAFSA?", ApplicationToView.HasFAFSACompleted.Value ? "Yes" : "No");
                showFafsa = true;
            }
            if (ApplicationToView.ExpectedFamilyContribution.HasValue )
            {
                AddRowToTable(FafsaTable, "What is your FAFSA defined EFC (Expected Family Contribution)?",
                              ApplicationToView.ExpectedFamilyContribution.Value.ToString("0.000"));
                showFafsa = true;

            }
            FafsaTable.Visible = showFafsa;

            //additional requirements
            if (ApplicationToView.Scholarship.AdditionalRequirements != null)
            {
                if (ApplicationToView.Scholarship.AdditionalRequirements.Count>0)
                {
                    foreach (AdditionalRequirement item in ApplicationToView.Scholarship.AdditionalRequirements)
                    {
                        AddRowToTableSmallTextOnly(AddtionalRequirementsTable, item.Name);
                    }
                }
                else
                {
                    AddtionalRequirementsTable.Visible = false;
                }
            }
            

            if (ApplicationToView.QuestionAnswers != null)
            {
                if (ApplicationToView.QuestionAnswers.Count>0)
                {
                    foreach (ApplicationQuestionAnswer item in ApplicationToView.QuestionAnswers)
                    {
                        AddRowToTable(QuestionsTable, item.Question.QuestionText, item.AnswerText);
                    }
                }
                else
                {
                    QuestionsTable.Visible = false;
                }
            } 
            if (ApplicationToView.Attachments != null)
            {
                if (ApplicationToView.Attachments.Count>0)
                {
                    applicationAttachments.Attachments = ApplicationToView.Attachments;
                    applicationAttachments.View = FileList.FileListView.List;
                }
                else
                {
                    applicationAttachments.Visible = false;
                    FormsTable.Visible = false;
                }
            }
           
        }

         private void AddRowToTable(HtmlTable table, string caption, string text)
         {
             var row = new HtmlTableRow();
             var cell = new HtmlTableCell();
             var label = new Label();
             label.Text = caption;
             label.CssClass = "captionMedium";
             cell.Controls.Add(label);
             row.Cells.Add(cell);

             cell = new HtmlTableCell();
             label = new Label();
             label.Text = text;
             label.CssClass = "smallText";
             cell.Controls.Add(label);


             row.Cells.Add(cell);
             table.Rows.Add(row);
         }
         private void AddRowToTableSmallTextOnly(HtmlTable table,  string text)
         {
             var row = new HtmlTableRow();
             var cell = new HtmlTableCell();
             var label = new Label();
             label.Text = text;
             label.CssClass = "smallText";
             cell.Controls.Add(label);
             row.Cells.Add(cell);

             cell = new HtmlTableCell();
             label = new Label();
             label.Text = "";
             
             cell.Controls.Add(label);


             row.Cells.Add(cell);
             table.Rows.Add(row);
         }
        private string GetCommaSeperated(string[] array)
        {
            return string.Join(", ", array);
        }
    }
}