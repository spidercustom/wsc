﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserForm.ascx.cs" Inherits="ScholarBridge.Web.Common.UserForm" %>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %> 
<asp:Label ID="errorMessage" runat="server" ForeColor="Red" Visible="false" CssClass="errorMessage"/>
<div class="form-iceland-container">
    <div class="form-iceland">
<table>
    <tr>
        <td>
            <label for="FirstName">First Name:</label>
        </td>
        <td>
            <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
            <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" />
        </td>
    </tr>
    <tr>
        <td>
            <label for="MiddleName">Middle Name:</label>
        </td>
        <td>
            <asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
            <elv:PropertyProxyValidator ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName" />
        </td>
    </tr>
    <tr>
        <td>
            <label for="LastName">Last Name:</label>
        </td>
        <td>
            <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
            <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" />
        </td>
    </tr>
    <tr>
        <td>
            <label for="UserName">Email Address:</label>
        </td>
        <td>
            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
            <elv:PropertyProxyValidator ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" />
            <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <label for="ConfirmEmail">Confirm Email Address:</label>
        </td>
        <td>
            <asp:TextBox ID="ConfirmEmail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="ConfirmEmailRequiredValidator" ControlToValidate="ConfirmEmail" runat="server" ErrorMessage="Confirm Email Address must be sepcified." />
            <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" 
                ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address." />
        </td>
    </tr>
    <tr>
        <td>
            <label for="PhoneNumber">Phone Number:</label>
        </td>
        <td>
            <asp:TextBox ID="PhoneNumber" runat="server"></asp:TextBox>
            <elv:PropertyProxyValidator ID="PropertyProxyValidator3" runat="server" ControlToValidate="PhoneNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
        </td>
    </tr>
    <tr>
        <td>
            <label for="FaxNumber">Fax Number:</label>
        </td>
        <td>
            <asp:TextBox ID="FaxNumber" runat="server"></asp:TextBox>
            <elv:PropertyProxyValidator ID="PropertyProxyValidator1" runat="server" ControlToValidate="FaxNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
       </td>
    </tr>
    <tr>
        <td>
            <label for="OtherPhoneNumber">Other Phone:</label>
        </td>
        <td>
            <asp:TextBox ID="OtherPhoneNumber" runat="server"></asp:TextBox>
            <elv:PropertyProxyValidator ID="PropertyProxyValidator2" runat="server" ControlToValidate="OtherPhoneNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
        </td>
    </tr>
</table>
<br />
<br />
<sbCommon:AnchorButton ID="saveBtn" runat="server" Text="Save" onclick="saveBtn_Click"  /> 
<sbCommon:AnchorButton ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelBtn_Click" />
</div>
</div>