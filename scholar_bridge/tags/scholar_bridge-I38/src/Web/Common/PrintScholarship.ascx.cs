﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{
    public partial class PrintScholarship : UserControl
    {
        public Scholarship Scholarship { get; set; }
       
        protected void Page_Load(object sender, EventArgs e)
        {
			scholarshipName.Text = Scholarship.Name;
            ApplicationDueOn.Text = Scholarship.ApplicationDueDate.Value.Date.ToString("MM/dd/yyyy");
            AcademicYear.Text = Scholarship.AcademicYear.YearStart + " - " + Scholarship.AcademicYear.YearEnd;
            MissionStatement.Text = Scholarship.MissionStatement;
            if (Scholarship.Donor != null)
                Donor.Text = Scholarship.Donor.Name + ", " + Scholarship.Donor.Profile;

            if (Donor.Text.Length == 0 || Donor.Text == ", ")
                DonorRow.Visible = false;

            ScheduleApplicationStart.Text = Scholarship.ApplicationStartDate.Value.Date.ToString("MM/dd/yyyy");
            ScheduleApplicationDue.Text = Scholarship.ApplicationDueDate.Value.Date.ToString("MM/dd/yyyy");
            ScheduleApplicationAwardOn.Text = Scholarship.AwardDate.Value.Date.ToString("MM/dd/yyyy");
          
            if ( Scholarship.FundingParameters !=null)
            {
                FundingNumberOfAwards.Text =  Scholarship.FundingParameters.MinimumNumberOfAwards.ToString() + " - " +
                                             Scholarship.FundingParameters.MaximumNumberOfAwards.ToString();
                FundingTotalFunds.Text =  Scholarship.FundingParameters.AnnualSupportAmount.ToString("c0");
            }

            FundingAwardAmount.Text =  Scholarship.MinimumAmount.ToString("c0") + " - " +
                                          Scholarship.MaximumAmount.ToString("c0");

            FundingSupportPeriod.Text = Scholarship.AcademicYear.YearStart + " - " + Scholarship.AcademicYear.YearEnd;

            var org = Scholarship.DisplayContactFor == OrganizationContactDisplay.Intermediary ? (Organization)Scholarship.Intermediary : (Organization)Scholarship.Provider;
            if (org != null)
            {
                ContactName.Text = org.Name;
                ContactWebsite.Text = org.Website;
                ContactAddress.Text = org.Address.ToString();
                ContactPhone.Text = org.Phone == null ? "" : org.Phone.FormattedPhoneNumber;
                 
            }
            if (ContactWebsite.Text.Length == 0)
                WebsiteRow.Visible = false;

            Renewable.Text = Scholarship.Renewable ? "Yes" : "No";
            if (!string.IsNullOrEmpty(Scholarship.RenewableGuidelines))
                RenewableGuideLines.Text = Scholarship.RenewableGuidelines;
            else
                RenewableGuidelinesRow.Visible = false;
            Reapply.Text = Scholarship.Reapply ? "Yes" : "No";
            EligibilityTypeOfStudent.Text = Scholarship.SeekerProfileCriteria.StudentGroups.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            EligibilitySupportedSituation.Text =
                GetCommaSeperated(
                    (from x in Scholarship.FundingProfile.SupportedSituation.TypesOfSupport select x.Name).ToArray());
            if (Scholarship.SeekerProfileCriteria.CollegeType == CollegeType.Specify)
            {
                EligibilityColleges.Text =
                    GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Colleges select x.Name).ToArray());
            }
            else
            {
                EligibilityColleges.Text = Scholarship.SeekerProfileCriteria.CollegeType.GetDisplayName();
                
            }
            EligibilitySchoolType.Text =Scholarship.SeekerProfileCriteria.SchoolTypes.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            EligibilityAcademicPrograms.Text = Scholarship.SeekerProfileCriteria.AcademicPrograms.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            EligibilityEnrollmentStatus.Text = Scholarship.SeekerProfileCriteria.SeekerStatuses.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR);
            //donor and website should not show the heading if no details
            //FirstGeneration
            var attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.FirstGeneration);

            if (attributeUsage != null)
            {
                if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                {
                    AddAttirbute(attributeUsage, "Should be first generation");
                }
            }
            //Ethinicity
              attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Ethnicity);

            if (attributeUsage != null)
            {
                if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                {
                    AddAttirbute(attributeUsage,
                                 GetCommaSeperated(
                                     (from x in Scholarship.SeekerProfileCriteria.Ethnicities select x.Name).ToArray()));
                }
            }

            //Religion
             attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Religion );

            if (attributeUsage != null)
            {
                if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                {
                    AddAttirbute(attributeUsage,
                                 GetCommaSeperated(
                                     (from x in Scholarship.SeekerProfileCriteria.Religions select x.Name).ToArray()));
                }
            }

            //Genders
            attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Gender);

            if (attributeUsage != null)
            {
                if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                {
                    AddAttirbute(attributeUsage,Scholarship.SeekerProfileCriteria.Genders.GetDisplayNames(EnumExtensions.COMMA_SEPEARATOR));
                }
            }


            //State
            attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.State);

            if (attributeUsage != null)
            {
                if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                {
                    AddAttirbute(attributeUsage, Scholarship.SeekerProfileCriteria.State.Name);
                    ;
                }

                if (Scholarship.SeekerProfileCriteria.State.Abbreviation == "WA")
                {

                    //HighSchools
                    attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.HighSchool);

                    if (attributeUsage != null)
                    {
                        if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                        {
                            AddAttirbute(attributeUsage,
                                         GetCommaSeperated(
                                             (from x in Scholarship.SeekerProfileCriteria.HighSchools select x.Name).
                                                 ToArray()));
                        }
                    }

                    //SchoolDistrict
                    attributeUsage =
                        Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.SchoolDistrict);

                    if (attributeUsage != null)
                    {
                        if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                        {
                            AddAttirbute(attributeUsage,
                                         GetCommaSeperated(
                                             (from x in Scholarship.SeekerProfileCriteria.SchoolDistricts select x.Name)
                                                 .ToArray()));
                        }
                    }

                    //City
                    attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.City);

                    if (attributeUsage != null)
                    {
                        if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                        {
                            AddAttirbute(attributeUsage,
                                         GetCommaSeperated(
                                             (from x in Scholarship.SeekerProfileCriteria.Cities select x.Name).ToArray()));
                        }
                    }

                    //County
                    attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.County);

                    if (attributeUsage != null)
                    {
                        if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                        {
                            AddAttirbute(attributeUsage,
                                         GetCommaSeperated(
                                             (from x in Scholarship.SeekerProfileCriteria.Counties select x.Name).
                                                 ToArray()));
                        }
                    }

                }

            }
            //Field of study
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.AcademicArea);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                     GetCommaSeperated(
                                         (from x in Scholarship.SeekerProfileCriteria.AcademicAreas select x.Name).ToArray()));
                    }
                }

                //Careers
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Career);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                     GetCommaSeperated(
                                         (from x in Scholarship.SeekerProfileCriteria.Careers select x.Name).ToArray()));
                    }
                }

                //Organizations
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Organization );

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                     GetCommaSeperated(
                                         (from x in Scholarship.SeekerProfileCriteria.Organizations select x.Name).ToArray()));
                    }
                }

                //Company
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Company);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                     GetCommaSeperated(
                                         (from x in Scholarship.SeekerProfileCriteria.Companies select x.Name).ToArray()));
                    }
                }


                //Hobbies
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.SeekerHobby);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                     GetCommaSeperated(
                                         (from x in Scholarship.SeekerProfileCriteria.Hobbies select x.Name).ToArray()));
                    }
                }

                //Sports
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Sport);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                     GetCommaSeperated(
                                         (from x in Scholarship.SeekerProfileCriteria.Sports select x.Name).ToArray()));
                    }
                }

                //Clubs
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Club);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                     GetCommaSeperated(
                                         (from x in Scholarship.SeekerProfileCriteria.Clubs select x.Name).ToArray()));
                    }
                }


                //Applicant Working
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.WorkType);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,"Yes");
                    }
                }

                //Applicant Service
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.ServiceType);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,"Yes");
                    }
                }

                //GPA
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.GPA);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var gpastring = "";
                        if (Scholarship.SeekerProfileCriteria.GPA.Maximum.HasValue && Scholarship.SeekerProfileCriteria.GPA.Maximum != Scholarship.MAX_GPA)
                            gpastring = string.Format("{0} - {1}", Scholarship.SeekerProfileCriteria.GPA.Minimum.Value.ToString("0.000"), Scholarship.SeekerProfileCriteria.GPA.Maximum.Value.ToString("0.000"));
                        else
                            gpastring = string.Format("{0} or Greater", Scholarship.SeekerProfileCriteria.GPA.Minimum.Value.ToString("0.000"));
                        AddAttirbute(attributeUsage, gpastring);
                    }
                }

                //SATWriting
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.SATWriting);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var str = "";
                        if (Scholarship.SeekerProfileCriteria.SATScore != null)
                        {
                            str = string.Format("{0} - {1} ",
                                                                    Scholarship.SeekerProfileCriteria.SATScore.Writing.Minimum,
                                                                    Scholarship.SeekerProfileCriteria.SATScore.Writing.Maximum);
                        }

                        AddAttirbute(attributeUsage, str);
                    }
                }

                //SATCriticalReading
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.SATCriticalReading);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var str = "";
                        if (Scholarship.SeekerProfileCriteria.SATScore != null)
                        {
                            str = string.Format("{0} - {1} ",
                                                  Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Minimum,
                                                  Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Maximum);
                        }

                        AddAttirbute(attributeUsage, str);
                    }
                }

                //SATMathematics
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.SATMathematics);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var str = "";
                        if (Scholarship.SeekerProfileCriteria.SATScore != null)
                        {
                            str = string.Format("{0} - {1} ",
                                                  Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Minimum,
                                                  Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Maximum);
                        }

                        AddAttirbute(attributeUsage, str);
                    }
                }

                //ACT English
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.ACTEnglish);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var str = "";
                        if (Scholarship.SeekerProfileCriteria.ACTScore != null)
                        {
                            str = string.Format("{0} - {1} ",
                                                Scholarship.SeekerProfileCriteria.ACTScore.English.Minimum,
                                                Scholarship.SeekerProfileCriteria.ACTScore.English.Maximum);

                        }

                        AddAttirbute(attributeUsage, str);
                    }
                }

                //ACT Mathematics
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.ACTMathematics);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var str = "";
                        if (Scholarship.SeekerProfileCriteria.ACTScore != null)
                        {
                            str = string.Format("{0} - {1} ",
                                          Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Minimum,
                                          Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Maximum);

                        }

                        AddAttirbute(attributeUsage, str);
                    }
                }

                //ACT Reading
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.ACTReading);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var str = "";
                        if (Scholarship.SeekerProfileCriteria.ACTScore != null)
                        {
                            str = string.Format("{0} - {1} ",
                                          Scholarship.SeekerProfileCriteria.ACTScore.Reading.Minimum,
                                          Scholarship.SeekerProfileCriteria.ACTScore.Reading.Maximum);
                        }

                        AddAttirbute(attributeUsage, str);
                    }
                }


                //ACT Science
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.ACTScience);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {

                        var str = "";
                        if (Scholarship.SeekerProfileCriteria.ACTScore != null)
                        {
                            str = string.Format("{0} - {1} ",
                                          Scholarship.SeekerProfileCriteria.ACTScore.Science.Minimum,
                                          Scholarship.SeekerProfileCriteria.ACTScore.Science.Maximum);
                        }

                        AddAttirbute(attributeUsage, str);
                    }
                }

                //Class Rank
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.ClassRank);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage,
                                      GetCommaSeperated(
                                          (from x in Scholarship.SeekerProfileCriteria.ClassRanks select x.Name).ToArray()));
                    }
                }

                //Academic Honors
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.Honor);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage, "Honors should be awarded");
                    }
                }
                //AP Credits
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.APCreditsEarned);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage, Scholarship.SeekerProfileCriteria.APCreditsEarned.ToString());
                    }
                }

                //IB Credits
                attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(SeekerProfileAttribute.IBCreditsEarned);

                if (attributeUsage != null)
                {
                    if (attributeUsage.UsageType != ScholarshipAttributeUsageType.NotUsed)
                    {
                        AddAttirbute(attributeUsage, Scholarship.SeekerProfileCriteria.IBCreditsEarned.ToString());
                    }
                }

            var showFinancialNeed = false;
            if (Scholarship.IsApplicantDemonstrateFinancialNeedRequired.HasValue )
            {
                if (Scholarship.IsApplicantDemonstrateFinancialNeedRequired.Value == true)
                {
                    AddRowToTableSmallTextOnly(FinancialNeedTable, "Applicant must demonstrate Financial Need");
                    showFinancialNeed = true;
                }
            }
            if (Scholarship.IsFAFSARequired.HasValue)
            {
                if (Scholarship.IsFAFSARequired.Value == true)
                {
                    AddRowToTableSmallTextOnly(FinancialNeedTable, "Applicant required to complete FAFSA");
                    showFinancialNeed = true;
                }
            }

            if (Scholarship.IsFAFSAEFCRequired.HasValue)
            {
                if (Scholarship.IsFAFSAEFCRequired.Value == true)
                {
                    AddRowToTableSmallTextOnly(FinancialNeedTable,
                                               "Applicant required to provide FAFSA defined EFC (expected Family Contribution)");
                }
            }
            FinancialNeedTable.Visible = showFinancialNeed;

            if (Scholarship.AdditionalRequirements!=null)
            {
                foreach (AdditionalRequirement item in Scholarship.AdditionalRequirements)
                {
                    AddRowToTableSmallTextOnly(AddtionalRequiremnetsTable, item.Name);
                }
            }

            if (Scholarship.AdditionalQuestions != null)
            {
                foreach (ScholarshipQuestion item in Scholarship.AdditionalQuestions)
                {
                    AddRowToTableSmallTextOnly(QuestionsTable, item.QuestionText);
                }
            }
            if (Scholarship.Attachments != null)
            {
                if (Scholarship.Attachments.Count > 0)
                {
                    scholarshipAttachments.Attachments = Scholarship.Attachments;
                    scholarshipAttachments.View = FileList.FileListView.List;
                } else
                {
                    scholarshipAttachments.Visible = false;
                     FormsTable.Visible = false;
                }
            } else
            {
                scholarshipAttachments.Visible = false;
                FormsTable.Visible = false;
            }
            if (Scholarship.IsUseOnlineApplication)
            {
                ApplyAtUrl.Text = Scholarship.OnlineApplicationUrl;
            }
            
            else{
                ApplyAtTable.Visible = false;
            }
        }
         private void AddAttirbute(ScholarshipAttributeUsage<SeekerProfileAttribute> attributeUsage,string values )
         {
             var row = new HtmlTableRow();
             var cell = new HtmlTableCell();
             var label = new Label();
             label.Text  = attributeUsage.Attribute.GetDisplayName()+":";
             label.CssClass = "captionMedium";
             cell.Controls.Add(label);
             row.Cells.Add(cell);

             cell = new HtmlTableCell();
             label = new Label();
             label.Text = values;
             label.CssClass = "smallText";
             cell.Controls.Add(label);


             row.Cells.Add(cell);

             if (attributeUsage.UsageType == ScholarshipAttributeUsageType.Minimum)

                 MinimumTable.Rows.Add(row);
             else

                 PreferenceTable.Rows.Add(row);

         }
         private void AddRowToTable(HtmlTable table, string caption, string text)
         {
             var row = new HtmlTableRow();
             var cell = new HtmlTableCell();
             var label = new Label();
             label.Text = caption;
             label.CssClass = "captionMedium";
             cell.Controls.Add(label);
             row.Cells.Add(cell);

             cell = new HtmlTableCell();
             label = new Label();
             label.Text = text;
             label.CssClass = "smallText";
             cell.Controls.Add(label);


             row.Cells.Add(cell);
             table.Rows.Add(row);
         }
         private void AddRowToTableSmallTextOnly(HtmlTable table,  string text)
         {
             var row = new HtmlTableRow();
             var cell = new HtmlTableCell();
             var label = new Label();
             label.Text = text;
             label.CssClass = "smallText";
             cell.Controls.Add(label);
             row.Cells.Add(cell);

             cell = new HtmlTableCell();
             label = new Label();
             label.Text = "";
             
             cell.Controls.Add(label);


             row.Cells.Add(cell);
             table.Rows.Add(row);
         }
        private string GetCommaSeperated(string[] array)
        {
            return string.Join(", ", array);
        }
    }
}