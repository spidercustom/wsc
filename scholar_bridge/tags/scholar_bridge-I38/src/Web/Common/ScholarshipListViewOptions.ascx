﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipListViewOptions.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipListViewOptions" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<style type="text/css">
    #ViewOptionsContainer 
    {
    	width:70%;
    	border: thin solid #d0d0d0;
    }            
    
    #ViewOptionsControlsContainer
    {
    	width:70%;
    }
    
    #UpdateOptionsContainer
    {
    	margin-left:10px;
    	width:130px;
    }
    
    #BuildNewScholarshipContainer
    {
    	padding-top:30px;
    	padding-left:10px;
    	width:22%;
    }

    .large-labels > label 
    {
        width: 165px;
    }
    
    .organization-control
    {
    	width:245px;
    }
</style>
<div class="form-iceland-container">
    <div id="ViewOptionsContainer" class="form-iceland ui-corner-all" >
        <p class="FormHeader">View options</p>
        <div id="ViewOptionsControlsContainer" class="form-iceland large-labels">
            <label class="label">View Scholarship By Status:</label>
            <asp:CheckBoxList ID="ScholarshipStatusCheckboxes" runat="server" CssClass="control-set"
                RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="False" />
            <br />        
            
            <label class="label">Select Organization:</label>
            <asp:DropDownList ID="cboOrganization" runat="server" CssClass="organization-control" />
        </div>
        <div id="UpdateOptionsContainer" class="form-iceland">
            <sbCommon:AnchorButton ID="UpdateViewBtn" runat="server" Text="Update View" onclick="UpdateViewBtn_Click" />
        </div>
    </div>
    <div id="BuildNewScholarshipContainer" class="form-iceland">
        <sbCommon:AnchorButton ID="BuildNewScholarshipButton" runat="server" Text="Create new Scholarship" NavigationUrl="~/Provider/BuildScholarship" />
    </div>
    <br/>
</div>
<div id="Clear"></div>