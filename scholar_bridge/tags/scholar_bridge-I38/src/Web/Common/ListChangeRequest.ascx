﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListChangeRequest.ascx.cs" Inherits="ScholarBridge.Web.Common.ListChangeRequest" %>

<style type="text/css">
    
    .large-labels > label 
    {
        width: 300px;
        padding-right:10px;
    }
   
</style>

<h2>Submit Request for Scholarship Criteria</h2>
<h3>Use this form to submit a request to have new criteria values added to the system. An e-mail will be sent to the System Administrator and a copy will be sent to your "Our Messages" Sent tab. Requests will be responded to within 5 business days.</h3>
<h3> You can also add Scholarship Specific Questions on the +Requirements tab.</h3>

<p  style="font-size:12px;"><span  style="font-weight:bold;">From : </span><asp:Label ID="lblFrom" runat="server"    /></p>


<div class="form-iceland-container"  >
    <div class="form-iceland large-labels" >
        <label for="cboListType">Choose the field you would like to submit a change to:</label>
        <asp:DropDownList runat="server" ID="cboListType"  Width="207px"></asp:DropDownList>
         <br />
         <label for="txtValue">Enter the new Value you would like to see listed in the selection criteria you chose (200 character limit)</label>
          <asp:TextBox runat="server" ID="txtValue" MaxLength="200" Width="460px" />
            <asp:RequiredFieldValidator ID="ValueRequiredValidator" runat="server" ControlToValidate="txtValue" ErrorMessage="Value cannot be empty."></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator EnableClientScript="true" runat="server" ID="valueValidator" ControlToValidate="txtValue" ValidationExpression="^.{1,200}$" ErrorMessage="Value should not exceed 200 character limit."></asp:RegularExpressionValidator>
         <br />
        
        <label for="txtReason">Reason for Change: Please describe if this is a new value or a change to an existing list value and why you are requesting this change (500 Character limit).</label>
            <asp:TextBox runat="server" ID="txtReason" MaxLength="100" Rows="5"  Height="69px" Width="460px" TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="reasonRequiredValidtor" runat="server" ControlToValidate="txtReason" ErrorMessage="Reason cannot be empty."></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator EnableClientScript="true" runat="server" ID="reasonValidator" ControlToValidate="txtReason" ValidationExpression="^.{1,500}$" ErrorMessage="Reason should not exceed 500 character limit."></asp:RegularExpressionValidator>
        <br />
    </div>
</div>     

