﻿using System;

namespace ScholarBridge.Web.Exceptions
{
    [global::System.Serializable]
    public class NoIntermediaryIsInContextException : ApplicationException //TODO: Do we need ScholarBridge exception type?
    {
      public NoIntermediaryIsInContextException() { }
      public NoIntermediaryIsInContextException( string message ) : base( message ) { }
      public NoIntermediaryIsInContextException( string message, Exception inner ) : base( message, inner ) { }
      protected NoIntermediaryIsInContextException( 
	    System.Runtime.Serialization.SerializationInfo info, 
	    System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { }
    }
}
