
build.proj
-------------------------------------------------------------------------------
  Commonly used targes and their properties
  -----------------------------------------------------------------------------
  
    * Migrate: Will build or update the database elements. Need to create 
    database before running it.

    Releated properties:
    ---------------------------------------------------------------------------
    DbServer: Server on which database is hosted. Default is "spider-database"
    DbUser: Database user name. Default is "scholarBridgeUser"
    DbPass: Database user name's password. Default is "scholarBridgeUser"
    DbCatalog: Database name. Default "ScholarBridge"
    ===========================================================================

	* Test: Compiles and runs all unit tests. Needs database setup before 
	running tests. 

	Releated properties:
	---------------------------------------------------------------------------
	(none as of now)
	also see related properties of Migrate task.
	===========================================================================
  
	* SetupVirtual: Builds the web and project on which web depends. It creates
	pre-compiled web-site folder in file system as well as create virtual
	directory in IIS.
  
	Releated properties:
	---------------------------------------------------------------------------
	WebVirtDirectoryName: IIS virtual directory name. Default is 
	"ScholarBridge"
	===========================================================================
  
  
  