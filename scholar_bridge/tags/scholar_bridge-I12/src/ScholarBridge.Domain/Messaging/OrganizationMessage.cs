namespace ScholarBridge.Domain.Messaging
{
    public class OrganizationMessage : Message
    {
        public virtual Organization RelatedOrg { get; set; }
    }
}