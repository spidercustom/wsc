using System;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Domain
{
    public class Match : CriteriaCountBase, ISoftDeletable
    {
        public virtual int Id { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual Scholarship Scholarship { get; set; }

        public virtual Application Application { get; set; }

        public virtual MatchStatus MatchStatus { get; set; }
        public virtual bool IsDeleted { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual string ScholarshipName
        {
            get { return null == Scholarship ? null : Scholarship.DisplayName; }
        }

        public virtual MatchApplicationStatus MatchApplicationStatus
        {
            get
            {
                var result = MatchApplicationStatus.Unkonwn;
                if (MatchStatus.IsIn(MatchStatus.New, MatchStatus.Saved))
                {
                    result = DateTime.Today > Scholarship.ApplicationDueDate ? 
                        MatchApplicationStatus.Closed : MatchApplicationStatus.NotApplied;
                }

                if (MatchStatus == MatchStatus.Applied && null != Application)
                {
                    result = MatchApplicationStatus.Appling;

                    if (!Application.Stage.Equals(ApplicationStages.Submitted) && 
                        DateTime.Today > Scholarship.ApplicationDueDate)
                        result = MatchApplicationStatus.Closed;

                    if (Application.Stage.Equals(ApplicationStages.Submitted))
                    {
                        if (DateTime.Today.IsBetween(Scholarship.ApplicationDueDate, Scholarship.AwardDate))
                            result = MatchApplicationStatus.BeingConsidered;
                        else
                            result = MatchApplicationStatus.Applied;
                    }

                    if (DateTime.Today > Scholarship.AwardDate && !Application.AwardStatus.IsIn(AwardStatuses.Offered, AwardStatuses.Awarded))
                        result = MatchApplicationStatus.Closed;

                    if (Application.AwardStatus == AwardStatuses.Offered)
                        result = MatchApplicationStatus.Offerred;

                    if (Application.AwardStatus == AwardStatuses.Awarded)
                        result = MatchApplicationStatus.Awarded;

                    return result;
                }

                return result; 
            }
        }

        public virtual string MatchApplicationStatusString
        {
            get { return MatchApplicationStatus.GetDisplayName(); }   
        }
    }
}