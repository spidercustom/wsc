﻿using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class RoleDAL : AbstractDAL<Role>, IRoleDAL
    {
        public Role FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public Role FindByName(string name)
        {
            return UniqueResult("Name", name);
        }
    }
}