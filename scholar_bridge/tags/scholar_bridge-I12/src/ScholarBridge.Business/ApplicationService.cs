using System;
using System.Collections.Generic;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class ApplicationService : IApplicationService
    {
        public IApplicationDAL ApplicationDAL { get; set; }
        public IMatchService MatchService { get; set; }
        public IMessagingService MessagingService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }

        public Application GetById(int id)
        {
            return ApplicationDAL.FindById(id);
        }

        public void SaveNew(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            MatchService.ApplyForMatch(application.Seeker, application.Scholarship.Id, application);
            ApplicationDAL.Insert(application);
        }

        public Application Save(Application application)
        {
            if (application == null)
                throw new ArgumentNullException("application");
            
            return ApplicationDAL.Save(application);
        }

        public void SubmitApplication(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Calculate Min/Pref criteria and populate the object
            application.Submit();
            ApplicationDAL.Update(application);

            // Save submission message to seeker's sent box
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ApplicationSubmisionSuccess(application, templateParams);
            var msg = new Message
            {
                MessageTemplate = MessageType.ApplicationSubmisionSuccess,
                To = new MessageAddress { Organization = application.Scholarship.Provider },
                From = new MessageAddress {  User = application.Seeker.User  },
                LastUpdate = new ActivityStamp(application.Seeker.User)
            };

            MessagingService.SaveNewSentMessage(msg,templateParams); 

            // Send Acknowlegement message to seeker
            templateParams = new MailTemplateParams();
            TemplateParametersService.ApplicationAcknowledgement(application, templateParams);
            msg = new Message
            {
                MessageTemplate = MessageType.ApplicationAcknowledgement,
                To = new MessageAddress { User = application.Seeker.User },
                From = new MessageAddress { Organization = application.Scholarship.Provider },
                LastUpdate = new ActivityStamp(application.Seeker.User)
            };
            MessagingService.SendMessage(msg, templateParams, application.Seeker.User.IsRecieveEmails);

        }

        public IList<Application> FindAllSubmitted(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.FindAllSubmitted(scholarship);
        }

        public int CountAllSubmitted(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.CountAllSubmitted(scholarship);
        }

        public int CountAllSubmittedBySeeker(Seeker seeker)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");

            return ApplicationDAL.CountAllSubmittedBySeeker(seeker);
        }

        public IList<Application> FindAllSubmitted(Scholarship scholarship, string lastNameSearch)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.SearchSubmittedBySeeker(scholarship, lastNameSearch);
        }

        public IList<Application> FindAllFinalists(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");

            return ApplicationDAL.FindAllFinalists(scholarship);
        }


        public void Finalist(int applicationId)
        {
            Finalist(ApplicationDAL.FindById(applicationId));
        }

        public void Finalist(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Check if awarded or rejected and don't allow changes after that point?

            if (!application.Finalist)
            {
                application.Finalist = true;
                ApplicationDAL.Update(application);
            }
        }

        public void NotFinalist(int applicationId)
        {
            NotFinalist(ApplicationDAL.FindById(applicationId));
        }

        public void NotFinalist(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            // FIXME: Check if awarded or rejected and don't allow changes after that point?

            if (application.Finalist)
            {
                application.Finalist = false;
                ApplicationDAL.Update(application);
            }
        }

        public void AwardScholarship(Application application, User awarder)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            if (application.AwardStatus != AwardStatuses.Awarded)
            {
                application.AwardStatus = AwardStatuses.Awarded;
                ApplicationDAL.Update(application);

                // Send message to seeker
                var templateParams = new MailTemplateParams();
                TemplateParametersService.AwardScholarship(application, templateParams);
                var msg = new Message
                {
                    MessageTemplate = MessageType.AwardScholarship,
                    To = new MessageAddress { User = application.Seeker.User },
                    From = new MessageAddress { Organization = application.Scholarship.Provider },
                    LastUpdate = new ActivityStamp(awarder)
                };
                MessagingService.SendMessage(msg, templateParams, application.Seeker.User.IsRecieveEmails);
            }
        }

        public void OfferScholarship(Application application, User awarder)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            if (application.AwardStatus != AwardStatuses.Offered)
            {
                application.AwardStatus = AwardStatuses.Offered;
                ApplicationDAL.Update(application);

                // Send message to seeker
                var templateParams = new MailTemplateParams();
                TemplateParametersService.OfferScholarship(application, templateParams);
                var msg = new Message
                {
                    MessageTemplate = MessageType.OfferScholarship,
                    To = new MessageAddress { User = application.Seeker.User },
                    From = new MessageAddress { Organization = application.Scholarship.Provider },
                    LastUpdate = new ActivityStamp(awarder)
                };
                MessagingService.SendMessage(msg, templateParams, application.Seeker.User.IsRecieveEmails);
            }
        }

        public void NotAwardScholarship(Application application, User awarder)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            if (application.AwardStatus != AwardStatuses.NotAwarded)
            {
                application.AwardStatus = AwardStatuses.NotAwarded;
                ApplicationDAL.Update(application);

                // XXX: No email?
            }
        }

        public void Update(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            ApplicationDAL.Update(application);
        }
        
        public Application FindBySeekerandScholarship(Seeker seeker, Scholarship scholarship)
        {
            if (null == seeker)
                throw new ArgumentNullException("seeker");
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");
            return ApplicationDAL.FindBySeekerandScholarship(seeker, scholarship);
        }

        public void Delete(Application application)
        {
            if (null == application)
                throw new ArgumentNullException("application");

            MatchService.DisconnectApplication(application);
            ApplicationDAL.Delete(application);
        }
    }
}