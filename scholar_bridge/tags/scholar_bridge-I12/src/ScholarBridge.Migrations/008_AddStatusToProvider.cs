using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(8)]
    public class AddStatusToProvider : Migration
    {
        private const string COLUMN_NAME = "ApprovalStatus";

        public override void Up()
        {
            Database.AddColumn(AddProvider.TABLE_NAME, COLUMN_NAME, DbType.String, 30, ColumnProperty.Null);
            Database.Update(AddProvider.TABLE_NAME, new []{COLUMN_NAME}, new []{"Approved"});
            Database.ChangeColumn(AddProvider.TABLE_NAME, new Column(COLUMN_NAME, DbType.String, 30, ColumnProperty.NotNull));
        }

        public override void Down()
        {
            Database.RemoveColumn(AddProvider.TABLE_NAME, COLUMN_NAME);
        }
    }
}