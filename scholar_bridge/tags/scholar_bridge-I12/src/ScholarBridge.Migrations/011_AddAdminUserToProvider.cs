using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(11)]
    public class AddAdminUserToProvider : Migration
    {
        private const string COLUMN_NAME = "AdminUser";
        protected static readonly string FK_ADMIN_USER = string.Format("FK_{0}_AdminUser", AddProvider.TABLE_NAME);

        public override void Up()
        {
            Database.AddColumn(AddProvider.TABLE_NAME, COLUMN_NAME, DbType.Int32, ColumnProperty.Null);
            Database.AddForeignKey(FK_ADMIN_USER, AddProvider.TABLE_NAME, COLUMN_NAME, AddUsers.TABLE_NAME, AddUsers.PRIMARY_KEY_COLUMN);
        }

        public override void Down()
        {
            Database.RemoveColumn(AddProvider.TABLE_NAME, COLUMN_NAME);
        }
    }
}