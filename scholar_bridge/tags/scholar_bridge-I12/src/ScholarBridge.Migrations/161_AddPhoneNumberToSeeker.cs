using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(161)]
    public class AddPhoneNumberToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        private const string PHONE_NUMBER = "PhoneNumber";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, PHONE_NUMBER, DbType.String,10, ColumnProperty.Null);
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, PHONE_NUMBER);
        }
    }
}