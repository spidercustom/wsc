﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
             
<div>
<h4>What is your financial need?</h4>
<br />
    <asp:PlaceHolder ID="NeedContainerControl" runat="server">
      <h5>Need</h5>
      <asp:CheckBox ID="Fafsa" runat="server" Text="FAFSA Definition of Need" TextAlign="Left"/>
      <asp:CheckBox ID="UserDerived" runat="server" Text="User Derived Definition of Need" TextAlign="Left"/>
      <br />
       <br />
      <label for="MinimumSeekerNeed">Minimum Need:</label>
      <br />
      <sandTrap:CurrencyBox ID="MinimumSeekerNeed" runat="server" Precision="0" />
      <elv:PropertyProxyValidator ID="MinimumSeekerNeedValidator" runat="server" ControlToValidate="MinimumSeekerNeed" PropertyName="MinimumSeekerNeed"  SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
      <br />
      <label for="MaximumSeekerNeed">Maximum Need:</label>
      <br />
      <sandTrap:CurrencyBox ID="MaximumSeekerNeed" runat="server" Precision="0" />
      <elv:PropertyProxyValidator ID="MaximumSeekerNeedValidator" runat="server" ControlToValidate="MaximumSeekerNeed" PropertyName="MaximumSeekerNeed" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
      <br />
      <label for="NeedGaps">Need Gap Threshold:</label>
      <br />
      <asp:CheckBoxList ID="NeedGaps" runat="server" TextAlign="Left"/>
      <br />
    </asp:PlaceHolder>
    <br />
    
    <asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
        <h5>Types of Support</h5>
        <asp:CheckBoxList ID="TypesOfSupport" runat="server" TextAlign="Left"/>
        <br />
    </asp:PlaceHolder>
    <asp:ScriptManager ID="scriptmanager1" runat="server" ></asp:ScriptManager>
</div>