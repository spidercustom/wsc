﻿using System;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Seeker.Scholarships
{
    public partial class SearchResults : SBBasePage
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        private string Criteria
        {
            get
            {
                if (string.IsNullOrEmpty(Request.Params["val"]))
                    throw new ArgumentException("Cannot understand value of parameter criteria");
                return Request.Params["val"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ScholarshipSearchBox1.Criteria = Criteria;
                var scholarships = ScholarshipService.GetBySearchCriteria(Criteria);
                ScholarshipSearchResults1.Scholarships = scholarships;
            }
        }

        protected void BackBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/Seeker"));
        }
    }
}
