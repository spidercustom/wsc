﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class ListChangeRequest : UserControl
    {
        public string From { get; set; }

        public string ListType
        {
            get { return cboListType.Text; }
        }

        public string Reason
        {
            get { return txtReason.Text; }
        }

        public string NewValue
        {
            get { return txtValue.Text; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblFrom.Text = From;
                List<string> list =
                    (from i in typeof (SeekerProfileAttribute).GetKeyValue() select i.Value).ToList();
                List<string> fundingSpecifications =
                    (from i in typeof (FundingProfileAttribute).GetKeyValue() select i.Value).ToList();


                list.AddRange(fundingSpecifications);
                list.Sort();
                cboListType.DataSource = list;
                cboListType.DataBind();
            }
        }
    }
}