﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationAcademicInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.ApplicationAcademicInfoShow" %>

<div id="StudentGroupRow" runat="server">
<div class="viewLabel">Type of Student:</div> <div class="viewValue"><asp:Literal ID="studentType" runat="server" /></div>
<br />
</div>

<div id="HighSchoolsRow" runat="server">
<div class="viewLabel">High School:</div> <div class="viewValue"><asp:Literal ID="highSchool" runat="server" /></div>
<br />
</div>

<div id="SchoolDistrictsRow" runat="server">
<div class="viewLabel">School District:</div> <div class="viewValue"><asp:Literal ID="schoolDistrict" runat="server" /></div>
<br />
</div>


<h3 id="ApplicantStudentPerformance" runat="server">High School Academic Performance</h3>
<div id="GPARow" runat="server">
<div class="viewLabel">GPA:</div> <div class="viewValue"><asp:Literal ID="gpa" runat="server" /></div>
<br />
</div>

<div id="ClassRankRow" runat="server">
<div class="viewLabel">Class Rank:</div> <div class="viewValue"><asp:Literal ID="classRank" runat="server" /></div>
<br />
</div>

<div id="SATScoreRow" runat="server">
    <h4>SAT</h4>
    <div>
    <div class="viewLabel">Writing:</div> <div class="viewValue"><asp:Literal ID="satWriting" runat="server" /></div>
    </div>
    <br />
    <div>
    <div class="viewLabel">Critical Reading:</div> <div class="viewValue"><asp:Literal ID="satReading" runat="server" /></div>
    </div>
    <br />
    <div>
    <div class="viewLabel">Mathematics:</div> <div class="viewValue"><asp:Literal ID="satMath" runat="server" /></div>
    </div>
    <br />
</div>

<div id="ACTScoreRow" runat="server">
    <h4>ACT</h4>
    <div>
    <div class="viewLabel">English:</div> <div class="viewValue"><asp:Literal ID="actEnglish" runat="server" /></div>
    </div>
    <br />
    <div>
    <div class="viewLabel">Reading:</div> <div class="viewValue"><asp:Literal ID="actReading" runat="server" /></div>
    </div>
    <br />
    <div>
    <div class="viewLabel">Mathematics:</div> <div class="viewValue"><asp:Literal ID="actMath" runat="server" /></div>
    </div>
    <br />
    <div>
    <div class="viewLabel">Science:</div> <div class="viewValue"><asp:Literal ID="actScience" runat="server" /></div>
    </div>
    <br />
</div>

<div id="HonorsRow" runat="server">
<div class="viewLabel">Honors:</div> <div class="viewValue"><asp:Literal ID="honors" runat="server" /></div>
<br />
</div>

<div id="APCreditsEarnedRow" runat="server">
<div class="viewLabel">AP Credits:</div> <div class="viewValue"><asp:Literal ID="apCredits" runat="server" /></div>
<br />
</div>

<div id="IBCreditsEarnedRow" runat="server">
<div class="viewLabel">IB Credits:</div> <div class="viewValue"><asp:Literal ID="ibCredits" runat="server" /></div>
<br />
</div>

<div>
<div class="viewLabel">College:</div> <div class="viewValue"><asp:Literal ID="currentCollege" runat="server" /></div>
<br />
</div>

<div id="SchoolTypesRow" runat="server">
<div class="viewLabel">Schools Considering:</div> <div class="viewValue"><asp:Literal ID="schoolsConsidering" runat="server" /></div>
<br />
</div>

<div id="AcademicProgramsRow" runat="server">
<div class="viewLabel">Academic Program:</div> <div class="viewValue"><asp:Literal ID="academicProgram" runat="server" /></div>
<br />
</div>

<div id="EnrollmentStatusesRow" runat="server">
<div class="viewLabel">Enrollment Status:</div> <div class="viewValue"><asp:Literal ID="enrollmentStatus" runat="server" /></div>
<br />
</div>

<div id="LengthOfProgrammRow" runat="server">
<div class="viewLabel">Length of Program:</div> <div class="viewValue"><asp:Literal ID="lengthOfProgram" runat="server" /></div>
<br />
</div>

<div id="FirstGenerationRow" runat="server">
<div class="viewLabel">First Generation:</div> <div class="viewValue"><asp:Literal ID="firstGeneration" runat="server" /></div>
<br />
</div>


<div id="CollegesRow" runat="server">
    <div>
    <div class="viewLabel">Colleges Applied:</div> 
    <div class="viewValue">
    <asp:Repeater ID="collegesApplied" runat="server">
        <HeaderTemplate><ul></HeaderTemplate>
        <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>
    </div>
    <div class="viewValue">
        <asp:Literal ID="collegesAppliedOther" runat="server" />
    </div>
    <br />
    </div>
    
    <div>
    <div class="viewLabel">Colleges Accepted:</div> 
    <div class="viewValue">
    <asp:Repeater ID="collegesAccepted" runat="server">
        <HeaderTemplate><ul></HeaderTemplate>
        <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
        <FooterTemplate></ul></FooterTemplate>
    </asp:Repeater>
    </div>
    <div class="viewValue">
        <asp:Literal ID="collegesAcceptedOther" runat="server" />
    </div>
    <br />
    </div>
</div>
<br />