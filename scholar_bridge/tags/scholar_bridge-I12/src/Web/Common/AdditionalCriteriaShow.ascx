﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteriaShow.ascx.cs" Inherits="ScholarBridge.Web.Common.AdditionalCriteriaShow" %>

<%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>


<h3>Additional Scholarship Application Requirements</h3>
<strong>Applicant must provide the following requested additional information</strong>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>

<h4>Additional Requirements:</h4><span>Please include the following with your application</span> 
<table class="viewonlyTable">
    <tbody>
        <asp:Repeater ID="AdditionalRequirementsRepeater" runat="server">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Name")%></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <tr runat="server" id="RequirementsFooterRow">
            <td>No additional requirements listed for this scholarship.</td>
        </tr>
    </tbody>
</table>

<h4>Scholarship Questions:</h4><span>Please answer the following questions</span>
<table class="viewonlyTable">
    <asp:Repeater ID="AdditionalCriteriaRepeater" runat="server">
        <ItemTemplate>
            <tr>
                <td><%# Eval("DisplayOrder")%>.</td>
                <td><%# Eval("QuestionText")%></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</table>

<h4>Additional Application Forms:</h4>
<sb:FileList id="scholarshipAttachments" runat="server" />

<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
