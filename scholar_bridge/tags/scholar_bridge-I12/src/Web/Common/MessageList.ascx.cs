﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class MessageList : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }
        public IUserService UserService { get; set; }

        public bool Archived { get; set; }
        public MessageAction MessageAction { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            IList<Domain.Messaging.Message> messages = null;
            if (Archived)
            {
                messages = MessageService.FindAllArchived(UserContext.CurrentUser, UserContext.CurrentOrganization);
            }
            else if (MessageAction != MessageAction.None)
            {
                messages = MessageService.FindAll(MessageAction, UserContext.CurrentUser, UserContext.CurrentOrganization);
            }
            else
            {
                messages = MessageService.FindAll(UserContext.CurrentUser, UserContext.CurrentOrganization);
            }
            messageList.DataSource = (from m in messages orderby m.Date descending select m).ToList();
            messageList.DataBind();
        }

        protected void messageList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var link = (LinkButton)e.Item.FindControl("linkToMessage");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
                + "?id=" + +((Domain.Messaging.Message)e.Item.DataItem).Id;
                link.Attributes.Add("onclick", String.Format("messageView('{0}'); return false;", url));

                var org = (Label)e.Item.FindControl("lblOrg");
                var from = (Label)e.Item.FindControl("lblFrom");
                var message = ((Domain.Messaging.Message) e.Item.DataItem);
                
                if (!(message.From.Organization ==null))
                  org.Text=  message.From.Organization.Name;

                var user = UserService.FindByEmail(message.From.EmailAddress());
                if (!(user == null))
                    from.Text = user.Name.NameFirstLast;

            }
        }
    }
}