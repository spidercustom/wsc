﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class SentMessageList : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }

        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
           
            var messages =  MessageService.FindAllSent(UserContext.CurrentUser, UserContext.CurrentOrganization);
            messageList.DataSource = (from m in messages orderby m.Date descending select m).ToList();
            messageList.DataBind();
        }

        protected void messageList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var link = (LinkButton)e.Item.FindControl("linkToMessage");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
                + "?sent=true&id=" + +((Domain.Messaging.SentMessage)e.Item.DataItem).Id;
                link.Attributes.Add("onclick", String.Format("messageView('{0}'); return false;", url));

            }
        }
    }
}