﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerProfileProgress.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerProfileProgress" %>
<%@ Register TagPrefix="sb" TagName="SeekerActivation" Src="~/Common/SeekerProfileActivation.ascx" %>
<h3>Profile Status</h3>
 
<div style="width:195px; height:12px;" id="progressbar"  class="ui-progressbar ui-widget ui-widget-content ui-corner-all"></div>  
 
<div>
<asp:Label ID="lblPercent"  runat="server" Text="" />
<br />
<a href="<%= ResolveUrl("~/Seeker/Profile/") %>"> Edit Profile</a>
<br />
<asp:Label ID="lblStatus"  runat="server" Text="" /> 
<br />
<sb:SeekerActivation ID="SeekerProfileActivationControl" runat="server" />
<br />
<asp:Label ID="lblLastUpdate"  runat="server" Text="" /> 
</div>






