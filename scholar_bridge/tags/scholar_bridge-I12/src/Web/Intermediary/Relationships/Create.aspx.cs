﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Intermediary.Relationships
{
    public partial class Create: Page
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }
        
        private const string DEFAULT_PAGEURL = "~/Intermediary/Relationships/Default.aspx";
        
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                var providers = RelationshipService.FindAvailableProviders(UserContext.CurrentIntermediary);
                if (providers.Count == 0)
                {
                    SuccessMessageLabel.SetMessage("No provider is available to add relationship request");
                    Response.Redirect(DEFAULT_PAGEURL);
                }
                orgList.DataSource = providers;
                orgList.DataTextField = "Name";
                orgList.DataValueField = "Id";
                orgList.DataBind();
            }
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect(DEFAULT_PAGEURL);
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            var intermediary = UserContext.CurrentIntermediary;
            int selectedId = int.Parse(orgList.SelectedValue);
            var provider = ProviderService.FindById(selectedId);
            var relationship = new Relationship()
                                  {
                                      Provider=provider,
                                      Intermediary=intermediary,
                                      Requester = RelationshipRequester.Intermediary,
                                      LastUpdate=new ActivityStamp(UserContext.CurrentUser),
                                  };
            RelationshipService.CreateRequest(relationship);
            SuccessMessageLabel.SetMessage("Request Created Successfully.");
            Response.Redirect(DEFAULT_PAGEURL);
            
        }

    }
}
