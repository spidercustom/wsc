<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Default" Title="Provider | Scholarships" %>

<%@ Register TagPrefix="sb" TagName="ScholarshipList" Src="~/Common/ScholarshipList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <div id="ScholarshipStatusFilterContainer">
        <label>View Scholarship By Status:</label>
        <div class="resetSizing">
            <asp:CheckBoxList ID="ScholarshipStatusCheckboxes" runat="server"
                RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="False" />
                <asp:Button ID="UpdateViewBtn" runat="server" Text="Update View" 
                onclick="UpdateViewBtn_Click" />
        </div>
    </div>

    <div>
        <label>Select Organization:</label><asp:DropDownList ID="cboOrganization"  
            runat="server" 
            onselectedindexchanged="cboOrganization_SelectedIndexChanged" 
            AutoPostBack="True"></asp:DropDownList>
    </div>

    <sb:ScholarshipList id="scholarshipsList" runat="server" 
        ViewActionLink="~/Provider/Scholarships/Show.aspx"
        EditActionLink="~/Provider/BuildScholarship/Default.aspx"  />
</asp:Content>
