﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SeekerStatuses
    {
        [DisplayName("Full-time")]
        FullTime = 1,
        [DisplayName("Part-time")]
        PartTime = 2,
        [DisplayName("Less than part-time")]
        LessThanPartTime = 4
    }
}
