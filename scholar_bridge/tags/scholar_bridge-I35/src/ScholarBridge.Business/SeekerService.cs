using System;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public class SeekerService : ISeekerService
    {
        public ISeekerDAL SeekerDAL { get; set; }
        public IRoleDAL RoleService { get; set; }
        public IUserService UserService { get; set; }

        public void SaveNew(Seeker seeker)
        {
            if (null == seeker)
            {
                throw new ArgumentNullException("seeker", "Seeker can not be null");
            }
			if (seeker.User.Id < 1)
			{
				UserService.Insert(seeker.User);
			}

            seeker.User.Roles.Add(RoleService.FindByName(Role.SEEKER_ROLE));
            UserService.Update(seeker.User);
			seeker.LastUpdate = new ActivityStamp(seeker.User);
            SeekerDAL.Insert(seeker);

            UserService.SendConfirmationEmail(seeker.User, false);
        }

        public void Update(Seeker seeker)
        {
            seeker.LastUpdate = new ActivityStamp(seeker.User);
            SeekerDAL.Update(seeker);
        }

    	public Seeker FindByUser(User user)
    	{
    		return SeekerDAL.FindByUser(user);
    	}

        public void Activate(Seeker seeker)
        {
            if (seeker == null) throw new ArgumentNullException("seeker");
            if (seeker.IsPublished())
                return;

            var validationResults = seeker.ValidateActivation();
            if (null != validationResults && !validationResults.IsValid)
                throw new ValidationErrorsException(validationResults);
            seeker.Stage = SeekerStages.Published;
            seeker.ProfileActivatedOn = DateTime.Now;
            Update(seeker);
        }

    	public int CountActivatedSeekers()
    	{
    		return SeekerDAL.CountRegisteredSeekers();
    	}

    	public int CountSeekersWithActiveProfiles()
    	{
    		return SeekerDAL.CountSeekersWithActiveProfiles();
    	}
    }
}
