﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.FundingProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
 <style type="text/css">
     .large-labels > label 
    {
        width: 460px;
    }

</style>
            
<h3>Build Scholarship – Financial Need</h3>
<p>Indicate with these fields if the applicant must demonstrate financial need and what information they may be asked to provide.</p>
<div class="form-iceland-container">
    <div class="form-iceland large-labels">
        <asp:PlaceHolder ID="FAFSAContainer" runat="server">
            <p class="form-section-title">Financial Information </p>
            <label>Applicant must demonstrate Financial Need?</label> 
            <div class="control-set">
                <asp:RadioButton ID="ApplicantNeedRequiredControl" Text="Yes" runat="server" GroupName="ApplicantNeedRequired" />
                <asp:RadioButton ID="ApplicantNeedNotRequiredControl" Text="No" runat="server" GroupName="ApplicantNeedRequired"/>
            </div>
             <sb:CoolTipInfo ID="CoolTipInfo1" Content="Use to indicate if applicant must demonstrate Financial Need. Applicants will be required to answer the question 'What is your financial need? Describe your challenge?'" runat="server" />
       
            <br />
            <label>Applicant required to complete FAFSA?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FAFSARequiredControl" Text="Yes" runat="server" GroupName="FAFSARequired" />
                <asp:RadioButton ID="FAFSANotRequiredControl" Text="No" runat="server" GroupName="FAFSARequired"/>
            </div>
             <sb:CoolTipInfo ID="CoolTipInfo2" Content="Use to indicate if applicant must complete a FAFSA. For more info on FAFSA, see the link on the Resources page." runat="server" />
   
            <br />
      
            <label>Applicant required to provide FAFSA defined EFC (expected Family Contribution)?</label> 
            <div class="control-set">
                <asp:RadioButton ID="FAFSAEFCRequiredControl" Text="Yes" runat="server" GroupName="FAFSAEFCRequired" />
                <asp:RadioButton ID="FAFSAEFCNotRequiredControl" Text="No" runat="server" GroupName="FAFSAEFCRequired"/>
            </div>
               <sb:CoolTipInfo ID="CoolTipInfo3" Content="Use to indicate if applicant must provide their FAFSA defined EFC. For more info on FAFSA, see the link on the Resources page." runat="server" />
       
            <br />
        </asp:PlaceHolder> 
        
        <p>If you have additional questions, worksheets, or financial information forms that the applicant must complete, you can indicate these on the next tab, +Requirements.</p>
        
       </div>
       </div>
