﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activities.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.Activities" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>

<div class="form-iceland-container">
    <div class="form-iceland two-columns">

        <p class="form-section-title">What are your academic interests?</p>
        <br />    
        <asp:PlaceHolder ID="AcademicAreasContainerControl" runat="server">
            <label id="AcademicAreasLabelControl" for="AcademicAreasControl">Academic Areas:</label>
            <sb:LookupDialog ID="AcademicAreasControlDialogButton" runat="server" BuddyControl="AcademicAreasControl" 
                OtherControl="AcademicAreasOtherControl" OtherControlLabel="AcademicAreasOtherControlLabel" ItemSource="AcademicAreaDAL" Title="Academic Areas"/>
            
            <asp:TextBox ID="AcademicAreasControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
           <br />
            <label id="AcademicAreasOtherControlLabel" for="AcademicAreasOtherControl" class="othercontrollabel">Others </label>
            <asp:TextBox CssClass="othercontroltextarea" ID="AcademicAreasOtherControl" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="CareersContainerControl" runat="server">
            <label id="CareersLabelControl" for="CareersControl">Careers:</label>
            <sb:LookupDialog ID="CareersControlDialogButton" runat="server" BuddyControl="CareersControl"  OtherControl="CareersOtherControl"
            OtherControlLabel="CareersOtherControlLabel"  ItemSource="CareerDAL" Title="Career Selection"/>
            <asp:TextBox ID="CareersControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="CareersOtherControlLabel" for="CareersOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="CareersOtherControl"  TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
        <hr /> 
        <p class="form-section-title">What other groups or interests are you involved in?</p><br />
        <asp:PlaceHolder ID="CommunityServiceContainerControl" runat="server">
            <label id="CommunityServiceLabelControl" for="CommunityServiceControl">Community Service/Involvement:</label>
            <sb:LookupDialog ID="CommunityServiceControlDialogButton" runat="server" OtherControl="CommunityServiceOtherControl" BuddyControl="CommunityServiceControl" 
             OtherControlLabel="CommunityServiceOtherControlLabel" ItemSource="CommunityServiceDAL" Title="Community Involvement Cause Selection"/>
            <asp:TextBox ID="CommunityServiceControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="CommunityServiceOtherControlLabel" for="CommunityServiceOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="CommunityServiceOtherControl"  TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="HobbiesContainerControl" runat="server">
            <label id="SeekerHobbiesLabelControl" for="SeekerHobbiesControl">Hobbies:</label>
            <sb:LookupDialog ID="SeekerHobbiesControlDialogButton" runat="server" OtherControl="SeekerHobbiesOtherControl" BuddyControl="SeekerHobbiesControl" 
            OtherControlLabel="SeekerHobbiesOtherControlLabel"  ItemSource="SeekerHobbyDAL" Title="Hobby Selection"/>
            <asp:TextBox ID="SeekerHobbiesControl" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="SeekerHobbiesOtherControlLabel" for="SeekerHobbiesOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="SeekerHobbiesOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
  
        <asp:PlaceHolder ID="SportsContainerControl" runat="server">
            <label id="SportsLabelControl" for="SportsControl">Sports:</label>
            <sb:LookupDialog ID="SportsControlDialogButton" runat="server" OtherControl="SportsOtherControl" BuddyControl="SportsControl" 
            OtherControlLabel="SportsOtherControlLabel" ItemSource="SportDAL" Title="Sport Participation Selection"/>
            <asp:TextBox ID="SportsControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="SportsOtherControlLabel" for="SportsOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="SportsOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="ClubsContainerControl" runat="server">
            <label id="ClubsLabelControl" for="ClubsControl">Clubs:</label>
            <sb:LookupDialog ID="ClubsControlDialogButton" runat="server" OtherControl="ClubsOtherControl" BuddyControl="ClubsControl" 
            OtherControlLabel="ClubsOtherControlLabel" ItemSource="ClubDAL" Title="Club Participation Selection"/>
            <asp:TextBox ID="ClubsControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="ClubsOtherControlLabel" for="ClubsOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="ClubsOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>
            <br />
        </asp:PlaceHolder>
    </div>
    <div class="vertical-devider"></div>
    <div class="form-iceland two-columns">
        <p class="form-section-title">Are you or any family members affiliated with specific Organizations?</p>  <br />
        <asp:PlaceHolder ID="OrganizationsContainerControl" runat="server">
            <label id="OrganizationsLabelControl" for="OrganizationsControl">Organizations:</label>
            <sb:LookupDialog ID="OrganizationsControlDialogButton" runat="server"  OtherControl="OrganizationsOtherControl" BuddyControl="OrganizationsControl" 
            OtherControlLabel="OrganizationsOtherControlLabel" ItemSource="SeekerMatchOrganizationDAL" Title="Organization Affiliation Selection"/>
            <asp:TextBox ID="OrganizationsControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="OrganizationsOtherControlLabel" for="OrganizationsOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="OrganizationsOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>    
            <br />
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="AffiliationTypesContainerControl" runat="server">
            <label id="AffiliationTypesLabelControl" for="AffiliationTypesControl">Affiliation types:</label>
            <sb:LookupDialog ID="AffiliationTypesControlDialogButton" runat="server" OtherControl="AffiliationTypesOtherControl" BuddyControl="AffiliationTypesControl" 
            OtherControlLabel="AffiliationTypesOtherControlLabel" ItemSource="AffiliationTypeDAL" Title="Affiliation Type Selection"/>
            <asp:TextBox ID="AffiliationTypesControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
            <label id="AffiliationTypesOtherControlLabel" for="AffiliationTypesOtherControl" class="othercontrollabel">Others:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="AffiliationTypesOtherControl" ReadOnly="true" TextMode="MultiLine" runat="server" ></asp:TextBox>    
            <br />
        </asp:PlaceHolder>
        <hr />
        <p class="form-section-title">Work/Service</p>  
        <asp:PlaceHolder ID="WorkTypeContainerControl" runat="server">
            <label id="WorkTypeLabelControl" for="WorkTypeControl">Work Type:</label>
            <sb:LookupDialog ID="WorkTypeControlDialogButton" runat="server" BuddyControl="WorkTypeControl" 
            ItemSource="WorkTypeDAL" Title="Work Type Selection"/>
            <asp:TextBox ID="WorkTypeControl"  TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="WorkHoursContainerControl" runat="server">
            <label id="WorkHoursLabelControl" for="WorkHoursControl">Work Hours:</label>
            <sb:LookupItemCheckboxList id="WorkHoursControl" runat="server" LookupServiceSpringContainerKey="WorkHourDAL"/>
            <br />
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="ServiceTypeContainerControl" runat="server">
            <label id="ServiceTypeLabelControl" for="ServiceTypeControl">Service Type:</label>
            <sb:LookupDialog ID="ServiceTypeControlDialogButton" runat="server" BuddyControl="ServiceTypeControl" ItemSource="ServiceTypeDAL" Title="Service Type Selection"/>
            <asp:TextBox ID="ServiceTypeControl" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <br />
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="ServiceHoursContainerControl" runat="server">
            <label id="ServiceHoursLabelControl" for="ServiceHoursControl">Service hours:</label>
            <sb:LookupItemCheckboxList id="ServiceHoursControl" runat="server" LookupServiceSpringContainerKey="ServiceHourDAL"/>
            <br />
        </asp:PlaceHolder>
    </div>
</div>