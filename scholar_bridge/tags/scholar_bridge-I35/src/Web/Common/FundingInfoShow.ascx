﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.FundingInfoShow" %>

<div class="recordinfo">
<p class="form-section-title section-level-1">Financial Need</p>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>     



<br />
<asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
    <p class="vertical-indent-level-1">
        No Scholarship Match Criteria Selected. Please go to the "Selection Criteria" tab while editing scholarship, 
        to choose fields to be used for Scholarship selection
    </p>
</asp:PlaceHolder>
 

<asp:PlaceHolder ID="FundingNeedsContainer" runat="server">
    <table class="viewonlyTable">
        <tr>
            <th>Applicant must demonstrate Financial Need?:</th>
            <td><asp:Literal ID="lblDemonstrateFinancialNeed" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <th>Applicant required to complete FAFSA?:</th>
            <td><asp:Literal ID="lblFafsaRequired" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <th>Applicant required to provide FAFSA defined EFC (expected Family Contribution)?:</th>
            <td><asp:Literal ID="lblEFCRequired" runat="server"></asp:Literal></td>
        </tr>
    </table>
    
    
</asp:PlaceHolder>
<br />
<span id="EditFinancialNeedContainer" runat="server" Visible="false">
                [<asp:HyperLink ID="editLink" runat="server" Visible="false">Edit</asp:HyperLink>]
            </span>   
</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
