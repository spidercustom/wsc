﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CaptchaControl.ascx.cs"
    Inherits="ScholarBridge.Web.Common.CaptchaControl" %>
<%@ Register Assembly="Hip" Namespace="Msdn.Web.UI.WebControls" TagPrefix="msdn" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<asp:ScriptManager ID="captchascriptmanager" runat="server" />
<div class="captchaControl">
<asp:UpdatePanel ID="captchupdatepanel" runat="server" >
<ContentTemplate>
    <label for="txtCaptcha">
        Type the characters shown below:<span class="requiredAttributeIndicator">*</span></label>
    <asp:TextBox ID="txtCaptcha" runat="server" Width="180px" onkeyup="ToLower(this)"/>
    <msdn:HipValidator ID="HipValidator1" runat="server" ControlToValidate="txtCaptcha"
        ErrorMessage="Please enter exact word as shown below" HipChallenge="ImageHipChallenge1" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCaptcha"
        ErrorMessage="Please enter exact word as shown below" />
    <br />
    <br />
    <msdn:ImageHipChallenge ID="ImageHipChallenge1" runat="server" Height="100px" Width="280px" />
    <br />
    <br />
    <sbCommon:AnchorButton ID="trynewButton" runat="server" Text="Try another word." OnClick="trynewButton_Click"
        CausesValidation="false" />
    <br />
    <br />
    
</ContentTemplate>        
</asp:UpdatePanel>        
</div>
