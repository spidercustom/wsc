﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("TabControlClientID")]
    [ToolboxData("<{0}:jQueryTabIntegrator runat=server></{0}:jQueryTabIntegrator>")]
    public class jQueryTabIntegrator : WebControl, IPostBackEventHandler
    {
        private const string DISALBE_TAB_SCRIPT_TEMPLATE =
            @"$('#BuildScholarshipWizardTab').tabs('option', 'disabled', {0});";

        private const string DISABLE_TABS = "DISABLE_TABS";
		
    	public bool CausesValidation
    	{
			get
			{
				return ViewState["CausesValidation"] == null ? false : (bool)ViewState["CausesValidation"];
			}
			set
			{
				ViewState["CausesValidation"] = value;
			}
		}
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string TabControlClientID
        {
            get
            {
                var s = (String)ViewState["TabControlClientID"];
                return (s ?? String.Empty);
            }

            set
            {
                ViewState["TabControlClientID"] = value;
            }
        }

        private MultiView GetAssociatedMultiView()
        {
            if (string.IsNullOrEmpty(MultiViewControlID))
                return null;
            return NamingContainer.FindControl(MultiViewControlID) as MultiView;
        }

        public string MultiViewControlID { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            var postBackScript = Page.ClientScript.GetPostBackEventReference(this, TAB_SELECT);

            var builder = new StringBuilder();
            builder.AppendFormat(TabSelectEventTemplate, TabControlClientID, ActiveTabIndex, postBackScript);
            builder.Replace(",'select'", ",'select:' + ui.index.toString()");

            Page.ClientScript.RegisterClientScriptBlock(GetType(), ClientID + "key", builder.ToString(), true);

            Page.ClientScript.RegisterOnDocumentReadyBlock(GetType(), "DISABLE_TABS",
                DISALBE_TAB_SCRIPT_TEMPLATE.Build(CreateDisabledTabIndicesArrayString()));

            base.OnPreRender(e);
        }

        public int[] DisabledTabIndices
        {
            get
            {
                var indices = (int[]) ViewState[DISABLE_TABS] ?? new int[0];
                return indices;
            }
            set
            {
                ViewState[DISABLE_TABS] = value;
            }
        }

        private string CreateDisabledTabIndicesArrayString()
        {
            if (DisabledTabIndices.Length == 0)
                return "[]";

            var indicesStringList = new List<string>();
            DisabledTabIndices.ForEach(o => indicesStringList.Add(o.ToString()));

            var indicesCsv = string.Join(",", indicesStringList.ToArray());
            return string.Format("[{0}]", indicesCsv);
        }

        private const string TabSelectEventTemplate =
@"
$(document).ready(function() {{
    $('#{0}').tabs('select', {1});
    $('#{0}').bind('tabsselect', function(event, ui) {{
        {2}
    }});
}});
";

        private int activeTabIndex;
        public int ActiveTabIndex
        {
            get
            {
                var associatedMultiView = GetAssociatedMultiView();
                if (null == associatedMultiView)
                    return activeTabIndex;
                return associatedMultiView.ActiveViewIndex;
            }
            set
            {
                
                //special case of activate tab
                
                var associatedMultiView = GetAssociatedMultiView();
                if (null != associatedMultiView)
                {
                    associatedMultiView.ActiveViewIndex =value ;
                }
                activeTabIndex = value;
            }
        }

        #region IPostBackEventHandler Members
        private const string TAB_SELECT = "select";
        public void RaisePostBackEvent(string eventArgument)
        {
			if (CausesValidation)
			{
				if (Page.IsPostBack)
					Page.Validate();
				if (!Page.IsValid)
				{
					// don't raise the event, just stay on the current tab.
					return;
				}
			}	

            if (eventArgument.StartsWith(TAB_SELECT))
            {
				if (null != TabChanging)
				{
					TabChanging(this, EventArgs.Empty);
				}
            	var parameters = eventArgument.Split(':');
                ActiveTabIndex = Int32.Parse(parameters[1]);
                if (null != TabChanged)
                {
                    TabChanged(this, EventArgs.Empty);
                }
            }
        }

		public event EventHandler TabChanging;
		public event EventHandler TabChanged;

        #endregion
    }
}
