﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplicationBasicsShow.ascx.cs" Inherits="ScholarBridge.Web.Common.ApplicationBasicsShow" %>

<div>
<div class="viewLabel">Name:</div> <div class="viewValue"><asp:Literal ID="applicantName" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Email:</div> <div class="viewValue"><asp:Literal ID="applicantEmail" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Address:</div> <div class="viewValue"><asp:Literal ID="address" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">County:</div> <div class="viewValue"><asp:Literal ID="county" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Phone:</div> <div class="viewValue"><asp:Literal ID="phoneNumber" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Mobile:</div> <div class="viewValue"><asp:Literal ID="mobileNumber" runat="server" /></div>
<br />
</div>
<div>
<div class="viewLabel">Birthday:</div> <div class="viewValue"><asp:Literal ID="dob" runat="server" /></div>
<br />
</div>
<div id="GendersRow" runat="server">
<div class="viewLabel">Gender:</div> <div class="viewValue"><asp:Literal ID="gender" runat="server" /></div>
<br />
</div>

<div id="ReligionRow" runat="server">
<div class="viewLabel">Religion/Faith:</div>
<div class="viewValue">
<asp:Repeater ID="religions" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue">
<asp:Literal ID="religionsOther" runat="server" />
</div>
<br />
</div>

<div id="EthnicityRow" runat="server">
<div class="viewLabel">Heritage:</div>
<div class="viewValue">
<asp:Repeater ID="heritages" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate><li><%# Eval("Name") %></li></ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<div class="viewValue">
<asp:Literal ID="heritagesOther" runat="server" />
</div>
<br />
</div>
<br />