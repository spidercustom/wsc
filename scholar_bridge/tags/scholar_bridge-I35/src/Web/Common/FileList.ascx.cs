﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Common
{
    public partial class FileList : UserControl
    {
        public IList<Attachment> Attachments { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateActiveView();
        }

        private void PopulateActiveView()
        {
            var activeViewRepeater = GetActiveViewRepeater();
            activeViewRepeater.DataSource = Attachments;
            activeViewRepeater.DataBind();
        }

        private Repeater GetActiveViewRepeater()
        {
            switch (View)
            {
                case FileListView.Detail:
                    return DetailViewRepeater;
                case FileListView.List:
                    return ListViewRepeater;
                default:
                    throw new NotSupportedException();
            }
        }

        protected void downloadAttachmentBtn_OnCommand(object sender, CommandEventArgs e)
        {
            var id = Int32.Parse((string)e.CommandArgument);
            var attachment = Attachments.First(a => a.Id == id);
            FileHelper.SendFile(Response, attachment.Name, attachment.MimeType, attachment.GetFullPath(ConfigHelper.GetAttachmentsDirectory()));
        }

        public FileListView View
        {
            get { return (FileListView)ViewContainer.ActiveViewIndex; }
            set
            {
                ViewContainer.ActiveViewIndex = (int) value;
                PopulateActiveView();
            }
        }

        public enum FileListView
        {
            Detail,
            List
        }
    }
}