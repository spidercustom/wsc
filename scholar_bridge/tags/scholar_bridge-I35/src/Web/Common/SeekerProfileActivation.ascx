﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerProfileActivation.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerProfileActivation" %>
<%@ Register TagPrefix="sb" TagName="SeekerProfileActivationValidationErrors" Src="~/Common/SeekerProfileActivationValidation.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<sbCommon:AnchorButton ID="btnActivateProfile" Text="Activate Profile" runat="server" OnClick="btnActivateProfile_Click" ToolTip="Save & activate the profile" />
<sb:SeekerProfileActivationValidationErrors id="seekerProfileActivationValidationErrors" runat="server" />