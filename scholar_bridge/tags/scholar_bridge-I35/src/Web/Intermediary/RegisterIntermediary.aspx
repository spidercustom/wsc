﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="RegisterIntermediary.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.RegisterIntermediary"  Title="Register Intermediary" %>

<%@ Register TagPrefix="sb" TagName="RegisterOrganization" Src="~/Common/RegisterOrganization.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
     <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopOrgRegistration.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomOrgRegistration.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:RegisterOrganization id="registerOrg" runat="server"  Title="Intermediary Organization Registration" />
</asp:Content>
