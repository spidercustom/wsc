﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IHighSchoolDAL : IGenericLookupDAL<HighSchool>
    {
        IList<HighSchool> FindByState(string stateAbbreviation);
    }
}
