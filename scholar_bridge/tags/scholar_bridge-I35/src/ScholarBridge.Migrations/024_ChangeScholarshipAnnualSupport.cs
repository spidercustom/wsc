﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(24)]
    public class ChangeScholarshipAnnualSupport : Migration
    {

        public override void Up()
        {
            Database.AddColumn(AddScholarship.TABLE_NAME, "AnnualSupportAmount", DbType.Double, ColumnProperty.Null);
            Database.ExecuteNonQuery("UPDATE " + AddScholarship.TABLE_NAME +" SET AnnualSupportAmount=MaximumAnnualAmount");
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "MaximumAnnualAmount");
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "MinimumAnnualAmount");

        }

        public override void Down()
        {
            Database.RemoveColumn(AddScholarship.TABLE_NAME, "AnnualSupportAmount");
            Database.AddColumn(AddScholarship.TABLE_NAME, "MinimumAnnualAmount", DbType.Double, ColumnProperty.Null);
            Database.AddColumn(AddScholarship.TABLE_NAME, "MaximumAnnualAmount", DbType.Double, ColumnProperty.Null);
        }
    }
}
