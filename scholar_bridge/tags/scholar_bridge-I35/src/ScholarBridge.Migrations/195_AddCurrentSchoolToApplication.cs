﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(195)]
    public class AddCurrentSchoolToApplication : Migration
    {
        private const string TABLE_NAME = "SBApplication";
        private const string FK_COLLEGE = "FK_Application_CurrentCollege";
        private const string FK_HIGHSCHOOL = "FK_Application_CurrentHighSchool";
        private const string FK_SCHOOLDISTRICT = "FK_Application_SchoolDistrict";

        public readonly string[] NEW_COLUMNS
            = { "LastAttended", "YearsAttended", 
                  "CurrentCollegeIndex", 
                  "CurrentHighSchoolIndex",
                  "CurrentCollegeOther",
                  "CurrentHighSchoolOther",
                  "CurrentSchoolDistrictOther",
                  "CollegesAppliedOther" ,
                  "CollegesAcceptedOther",
                  "SchoolDistrictIndex"
              };

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.String, 50, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[2], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[3], DbType.Int32, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[4], DbType.String, 250, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[5], DbType.String, 250, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[6], DbType.String, 250, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[7], DbType.String, 250, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[8], DbType.String, 250, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[9], DbType.Int32, ColumnProperty.Null);

            Database.AddForeignKey(FK_COLLEGE, TABLE_NAME, "CurrentCollegeIndex", "SBCollegeLUT", "SBCollegeIndex");
            Database.AddForeignKey(FK_HIGHSCHOOL, TABLE_NAME, "CurrentHighSchoolIndex", "SBHighSchoolLUT", "SBHighSchoolIndex");
            Database.AddForeignKey(FK_SCHOOLDISTRICT, TABLE_NAME, "SchoolDistrictIndex", "SBSchoolDistrictLUT", "SBSchoolDistrictIndex");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_COLLEGE);
            Database.RemoveForeignKey(TABLE_NAME, FK_HIGHSCHOOL);
            Database.RemoveForeignKey(TABLE_NAME, FK_SCHOOLDISTRICT);
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}