﻿
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(205)]
    public class ApplicationAwardStatusColumns : Migration
    {
        private const string TABLE_NAME = "SBApplication";
        private const string COL1 = "AwardStatus";
        private const string COL2 = "AwardPeriodClosed";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, COL1, DbType.String, 15, ColumnProperty.NotNull, "'None'");
            Database.AddColumn(TABLE_NAME, COL2, DbType.DateTime, ColumnProperty.Null);
		}

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COL1);
            Database.RemoveColumn(TABLE_NAME, COL2);
		}
    }
}