using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class TemplatedMailTests
    {
     
        [Test]
        public void templated_mail_parses_subject_and_body()
        {
            var tm = BuildTemplatedMail();
            Assert.That(tm.RawSubject, Is.EqualTo("Subject"));
            Assert.That(tm.RawBody, Is.EqualTo("##FOO## ##BAR## ##BAZ##"));
        }

        [Test]
        public void templated_mail_replaces_values()
        {
            var tm = BuildTemplatedMail();
            var vars = new Dictionary<string, string>
                           {
                               {"FOO", "Foo"},
                               {"BAR", "Bar"},
                               {"BAZ", "Baz"}
                           };
            Assert.That(tm.RawSubject, Is.EqualTo("Subject"));
            Assert.That(tm.Body(vars), Is.EqualTo("Foo Bar Baz"));
        }

        private static TemplatedMail BuildTemplatedMail()
        {
            var raw = @"Subject
##FOO## ##BAR## ##BAZ##";
            return new TemplatedMail(raw);
        }
    }
}