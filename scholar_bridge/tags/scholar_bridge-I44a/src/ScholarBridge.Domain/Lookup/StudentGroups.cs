﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum StudentGroups
    {//need to re-order because of sorting while its going to be bound with some control.
        [DisplayName("Middle School")]
        [DisplayOrder(1)]
        MiddleSchool = 1,


        [DisplayName("High School Senior")]
        [DisplayOrder(5)]
        HighSchoolSenior = 2,


        [DisplayName("High School Graduate")]
        [DisplayOrder(6)]
        HighSchoolGraduate = 4,


        [DisplayName("Adult Returning")]
        [DisplayOrder(7)]
        AdultReturning = 8,


        [DisplayName("Adult First-time")]
        [DisplayOrder(8)]
        AdultFirstTime = 16,


        [DisplayName("Transfer")]
        [DisplayOrder(9)]
        Transfer = 32,


        [DisplayName("Undergraduate")]
        [DisplayOrder(10)]
        Undergraduate = 64,


        [DisplayName("Graduate")]
        [DisplayOrder(11)]
        Graduate = 128,


        [DisplayName("High School Freshman")]
        [DisplayOrder(2)]
        HighSchoolFreshMan = 256,


        [DisplayName("High School Sophomore")]
        [DisplayOrder(3)]
        HighSchoolSophomore = 512,


        [DisplayName("High School Junior")]
        [DisplayOrder(4)]
        HighSchoolJunior = 1024
        

    }
}
