﻿using System;
using ScholarBridge.Common;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum AcademicPrograms
    {
        [DisplayName("Undergraduate")]
        [DisplayOrder(1)]
        Undergraduate = 1,
        [DisplayName("Advanced Degree")]
        [DisplayOrder(2)]
        AdvancedDegree = 2,
        [DisplayName("Continuing Ed/Non-Degree Seeking")]
        [DisplayOrder(3)]
        ContinuingEdNonDegreeSeeking = 4
    }
}
