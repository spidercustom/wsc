﻿using System;

namespace ScholarBridge.Domain.Messaging
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true )]
    public class PreserveFormattingAttribute : Attribute
    {
        public bool PreserveFormatting { get; set; }
        public PreserveFormattingAttribute(bool preserveFormatting)
        {
            PreserveFormatting = preserveFormatting;
        }
    }
}
