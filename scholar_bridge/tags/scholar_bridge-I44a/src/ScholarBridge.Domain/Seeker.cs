using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.Resources;
using ScholarBridge.Domain.SeekerParts;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain
{
	/// <summary>
	/// Indicates what type of Seeker has signed up.
	/// All but 'Student' are considered Influencer's.  Influencers are able to act like 
	/// students but cannot apply for scholarships.
	/// </summary>
	public enum SeekerType
	{
		Student,
		Parent,
		[DisplayName("Counselor/Teacher")]
		CounselorTeacher,
		[DisplayName("Financial Aid Professional")]
		FinancialAidProfessional 
	}

	/// <summary>
	/// Seeker: Person seeking financial aid
	/// </summary>
    public class Seeker
    {
        public const string PROFILE_ACTIVATION_RULESET = "Profile-Activation";
        
        public Seeker()
        {
        	InitializeMembers();
        }

		private void InitializeMembers()
		{
			Ethnicities = new List<Ethnicity>();
			Religions = new List<Religion>();
            Sports = new  List<Sport>();

            AcademicAreas = new  List<AcademicArea>();
            Careers =new List<Career>();
            Hobbies =new List<SeekerHobby>();
            Clubs = new List<Club>();
            MatchOrganizations =new List<SeekerMatchOrganization>();
            Companies  =new List<Company>();
            
			Address = new SeekerAddress();            
            SupportedSituation=new SupportedSituation();
			Attachments = new List<Attachment>();
		}

        public virtual int Id { get; set; }
		public virtual User User { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual PhoneNumber MobilePhone { get; set; }
        public virtual DateTime? DateOfBirth { get; set; }

		[StringLengthValidator(0, 100, MessageTemplateResourceName = "NamedStringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual County County
		{
			get; set;
		}

		public virtual DateTime? ProfileActivatedOn { get; set; }
		public virtual DateTime? ProfileLastUpdateDate { get; set; }
		public virtual SeekerType SeekerType { get; set; }
		private SeekerAddress _address;

        public virtual SeekerAddress Address 
		{ 
			get
			{
				if (_address == null)
                    _address = new SeekerAddress();
				return _address;
			}
			 set
			 {
			 	_address = value;
			 }
		}

        public virtual PersonName Name { get { return null == User ? null : User.Name; } }

        public virtual bool FirstGeneration { get; set; }

        public virtual Genders Gender { get; set; }

        public virtual SeekerStage Stage { get; set; }

        public virtual CurrentSchool CurrentSchool { get; set; }

        [StringLengthValidator(0, 1500, MessageTemplate = "Your Personal Statement must be 1500 characters or less.")]
        public virtual string PersonalStatement { get; set; }

		[StringLengthValidator(0, 1500, MessageTemplate = "Your Financial Challenge statement must be 1500 characters or less.")]
        public virtual string MyChallenge { get; set; }

        [StringLengthValidator(0, 150, MessageTemplate = "Your Gift statement must be 150 characters or less")]
        public virtual string MyGift { get; set; }

        public virtual SeekerAcademics SeekerAcademics { get; set; }

		[StringLengthValidator(0, 100, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string Words { get; set; }
		[StringLengthValidator(0, 100, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string Skills { get; set; }

		public virtual IList<Ethnicity> Ethnicities { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string EthnicityOther { get; set; }

        public virtual IList<Religion> Religions { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string ReligionOther { get; set; }
        
        public virtual IList<Sport> Sports { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string SportOther { get; set; }

        public virtual IList<AcademicArea> AcademicAreas { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string AcademicAreaOther { get; set; }

        public virtual IList<Career> Careers { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string CareerOther { get; set; }

        public virtual IList<SeekerHobby> Hobbies { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string HobbyOther { get; set; }

        public virtual IList<Club> Clubs { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string ClubOther { get; set; }

        public virtual IList<SeekerMatchOrganization> MatchOrganizations { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string MatchOrganizationOther { get; set; }

        public virtual IList<Company> Companies { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string CompanyOther { get; set; }

        public virtual bool  IsWorking { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string WorkHistory { get; set; }

        public virtual bool IsService { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string ServiceHistory { get; set; }

		public virtual bool IsEslEll { get; set; }

		[StringLengthValidator(0, 50, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string AddressCity { get; set; }
		[StringLengthValidator(0, 50, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
        public virtual string AddressCounty { get; set; }

        public virtual SupportedSituation SupportedSituation { get; set; }

		public virtual IList<Attachment> Attachments { get; protected set; }

        public virtual DateTime? LastMatch { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual bool? HasFAFSACompleted { get; set; }
        public virtual decimal? ExpectedFamilyContribution { get; set; }
        
		/// <summary>
		/// The only thing that we're currently validating that must be entered are name and address.
		/// </summary>
		/// <returns></returns>
        public virtual ValidationResults ValidateActivation()
        {
            var results = Validate(PROFILE_ACTIVATION_RULESET);

            var name = Name ?? new PersonName();
            results.AddAllResults(name.ValidateAsRequired());

			// FIXME: Address validation should work for both in-state and out-of-state.
			var address = Address ?? new SeekerAddress();
			if (address.State == null || address.State.Abbreviation == State.WASHINGTON_STATE_ID)
				results.AddAllResults(address.ValidateAsRequired());

            return results;
        }

        private ValidationResults Validate(string ruleSet)
        {
            var validator = ValidationFactory.CreateValidator<Seeker>(ruleSet);
            return validator.Validate(this);
        }

        // FIXME: There HAS to  be a better way of doing this
        public virtual int GetPercentComplete()
        {
           //Calculate Percentage based on seeker object fields
          var completedCount = 0;
          var allCount = 0;
          if (!(Name == null))
          {
              allCount++;
              if (!String.IsNullOrEmpty(Name.NameFirstLast))
                  completedCount++;
          }

          if (Address != null)
          {
              allCount++;
              if (!String.IsNullOrEmpty(Address.GetCity))
                  completedCount++;

              allCount++;
              if (!String.IsNullOrEmpty(Address.Street))
                  completedCount++;

              allCount++;
              if (!(Address.State == null))
                  completedCount++;

              allCount++;
              if (!(Address.PostalCode == null))
                  completedCount++;

          }
          allCount++;
          if (!String.IsNullOrEmpty(AddressCounty))
              completedCount++;
          allCount++;
          if (!String.IsNullOrEmpty(AddressCity ))
              completedCount++;

          
          allCount++;
          if (!(DateOfBirth == null))
              completedCount++;

          if (!(Phone == null))
          {
              allCount++;
              if (!String.IsNullOrEmpty(Phone.Number))
                  completedCount++;

          }

          allCount++;
          if (!(MobilePhone == null))
              completedCount++;

            allCount++;
             if (!String.IsNullOrEmpty(EthnicityOther) || Ethnicities.Count > 0)
              completedCount++;
          
            allCount++;
            if (!String.IsNullOrEmpty(ReligionOther) || Religions.Count > 0)
              completedCount++;


            allCount++;
            if (!String.IsNullOrEmpty(PersonalStatement))
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(MyGift))
                completedCount++;
            
            allCount++;
            if (!String.IsNullOrEmpty(Words))
                completedCount++;
            
            allCount++;
            if (!String.IsNullOrEmpty(Skills))
                completedCount++;

            allCount++;
            if (!(CurrentSchool == null))
                completedCount++;

            if (!(SeekerAcademics == null))
            {
                allCount++;
                if (!String.IsNullOrEmpty(SeekerAcademics.CollegesAcceptedOther) || SeekerAcademics.CollegesAccepted.Count > 0)
                    completedCount++;


                allCount++;
                if (!String.IsNullOrEmpty(SeekerAcademics.CollegesAppliedOther) || SeekerAcademics.CollegesApplied.Count > 0)
                    completedCount++;

                allCount++;
                if (!(SeekerAcademics.GPA == null))
                    completedCount++;
                
                allCount++;
                if (!(SeekerAcademics.ClassRank == null))
                    completedCount++;


                if (!(SeekerAcademics.ACTScore == null))
                {
                    allCount++;
                    if (!(SeekerAcademics.ACTScore.English == null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.ACTScore.Mathematics == null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.ACTScore.Reading == null))
                        completedCount++;
                    allCount++;
                    if (!(SeekerAcademics.ACTScore.Science == null))
                        completedCount++;
                   
                    allCount++;
                    if (!(SeekerAcademics.ACTScore.Commulative == null))
                        completedCount++;
                }

                if (!(SeekerAcademics.SATScore == null))
                {
                    allCount++;
                    if (!(SeekerAcademics.SATScore.CriticalReading == null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.SATScore.Mathematics == null))
                        completedCount++;

                    allCount++;
                    if (!(SeekerAcademics.SATScore.Writing == null))
                        completedCount++;
                    allCount++;
                    if (!(SeekerAcademics.SATScore.Commulative == null))
                        completedCount++;
                }
            }

            allCount++; 
            if (!String.IsNullOrEmpty( AcademicAreaOther ) || AcademicAreas.Count>0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(CareerOther) || Careers.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(MatchOrganizationOther) || MatchOrganizations.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(CompanyOther) || Companies.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(HobbyOther) || Hobbies.Count > 0)
                completedCount++;

            allCount++;
            if (!String.IsNullOrEmpty(SportOther) || Sports.Count > 0)
                completedCount++;
            allCount++;
            if (!String.IsNullOrEmpty(ClubOther) || Clubs.Count > 0)
                completedCount++;


            allCount++;
            if ( !(HasFAFSACompleted==null ))
                completedCount++;
            
            allCount++;
            if (!(ExpectedFamilyContribution == null))
                completedCount++;

            allCount++;
            if (!(SupportedSituation.TypesOfSupport == null))
            {
                if (SupportedSituation.TypesOfSupport.Count >0)
                completedCount++;
            }
            double percent = completedCount * 100;
            percent = percent/allCount;
            return Convert.ToInt32(percent);
        }

        public virtual bool IsPublished()
        {
            return true;
        }
    }
}