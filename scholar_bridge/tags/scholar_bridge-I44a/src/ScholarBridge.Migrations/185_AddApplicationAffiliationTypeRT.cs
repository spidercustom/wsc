using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(185)]
    public class AddApplicationAffiliationTypeRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationAffiliationTypeRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBAffiliationTypeLUT"; }
        }
    }
}