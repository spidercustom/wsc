﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(269)]
    public class AddIsServiceToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, new Column("IsService", DbType.Boolean,ColumnProperty.Null));
            Database.AddColumn(TABLE_NAME, new Column("IsWorking", DbType.Boolean, ColumnProperty.Null));
            Database.AddColumn(TABLE_NAME, new Column("WorkHistory", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("ServiceHistory", DbType.String, 250));
            Database.AddColumn(TABLE_NAME, new Column("CompanyOther", DbType.String, 250));
        }

        public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, "IsService");
            Database.RemoveColumn(TABLE_NAME, "IsWorking");
            Database.RemoveColumn(TABLE_NAME, "WorkHistory");
            Database.RemoveColumn(TABLE_NAME, "ServiceHistory");
        }
    }
}