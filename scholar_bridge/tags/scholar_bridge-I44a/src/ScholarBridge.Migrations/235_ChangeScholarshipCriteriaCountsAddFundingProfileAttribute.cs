﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(235)]
    public class ChangeScholarshipCriteriaCountsAddFundingProfileAttribute : Migration
    {
        public override void Up()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBScholarshipCriteriaCounts]");
            Database.ExecuteNonQuery(CHANGED_VIEW);
        }

        public override void Down()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBScholarshipCriteriaCounts]");
            Database.ExecuteNonQuery(PRIOR_VIEW);
        }

        public const string CHANGED_VIEW = @"
            CREATE VIEW [dbo].[SBScholarshipCriteriaCounts]
            AS

	        SELECT SBScholarshipID, [1] as prefCrit, [2] as minCrit
	        FROM
	        (
		        SELECT SBScholarshipID, SBUsageTypeIndex 
		        FROM SBScholarshipMatchCriteriaAttributeUsageRT

		        union all
        	
		        SELECT SBScholarshipID, SBUsageTypeIndex 
		        FROM SBScholarshipFundingProfileAttributeUsageRT

	        ) AS SourceTable
	        PIVOT
	        (
		        COUNT(SBUsageTypeIndex)
		        FOR SBUsageTypeIndex IN ([1], [2])
	        ) AS PivotTable";


        private const string PRIOR_VIEW =
        @"CREATE VIEW [dbo].[SBScholarshipCriteriaCounts]
        AS
        SELECT SBScholarshipID, [1] as prefCrit, [2] as minCrit
        FROM
        (SELECT SBScholarshipID, SBUsageTypeIndex 
            FROM SBScholarshipMatchCriteriaAttributeUsageRT) AS SourceTable
        PIVOT
        (
        COUNT(SBUsageTypeIndex)
        FOR SBUsageTypeIndex IN ([1], [2])
        ) AS PivotTable";

    }
}