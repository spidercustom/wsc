using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(247)]
    public class AddScholarshipCompanyRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBScholarshipCompanyRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBScholarship"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCompanyLUT"; }
        }
    }
}