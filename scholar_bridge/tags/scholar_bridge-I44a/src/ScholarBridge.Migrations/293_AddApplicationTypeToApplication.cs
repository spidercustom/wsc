﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(293)]
    public class AddApplicationTypeToApplication : Migration
    {
        private const string TABLE_NAME = "SBApplication";
        private const string COLUMN_NAME = "ApplicationType";

        public override void Up()
        {
			Database.AddColumn(TABLE_NAME, COLUMN_NAME, DbType.String);
            SetApplicationTypeToInternal();
			Column typeColumn = Database.GetColumnByName(TABLE_NAME, COLUMN_NAME);
        	typeColumn.ColumnProperty = ColumnProperty.NotNull;
        	typeColumn.Type = DbType.String;
			Database.ChangeColumn(TABLE_NAME, typeColumn);
        }

        public override void Down()
        {
			Database.RemoveColumn(TABLE_NAME, COLUMN_NAME);
        }

		/// <summary>
        /// set all Application to 'Internal' type for now.
		/// </summary>
        private void SetApplicationTypeToInternal()
		{
			Database.ExecuteNonQuery(@"
update SBApplication
set ApplicationType = 'Internal'
");
		}

    }
}