﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(231)]
    public class ChangeScholarshipAddAvailabilityFields : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        public readonly string[] COLUMNS
           = { "IsAvailableToWAResidents", "IsAvailableForWACollegeAttendees"};
		public override void Up()
		{
			Database.AddColumn(TABLE_NAME, COLUMNS[0], DbType.Boolean);
			Database.AddColumn(TABLE_NAME, COLUMNS[1], DbType.Boolean);
			Database.ExecuteNonQuery("delete from SBMessage where MessageTemplate = 'RequestScholarshipActivation'");
		}

    	public override void Down()
        {
            Database.RemoveColumn(TABLE_NAME, COLUMNS[0]);
            Database.RemoveColumn(TABLE_NAME, COLUMNS[1]);
        }
    }
}