using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(14)]
    public class AddRolesData : Migration
    {
        private readonly string[] COLUMNS = new[] {"Name", "LastUpdatedBy"};
        private readonly string[] ROLE_NAMES = new[] { "Admin", "WSCAdmin", "Provider", "Intermediary", "Seeker" };

        public override void Up()
        {
            Database.Insert(AddUsers.TABLE_NAME, 
                new[] { "Username", "Password", "Email", "PasswordFormat", "IsActive", "IsApproved" },
                new[] { "admin", "sb@dmin123", "admin@scholarbridge.com", "2", "true", "true" });

            Database.Insert(AddUsers.TABLE_NAME,
                new[] { "Username", "Password", "Email", "PasswordFormat", "IsActive", "IsApproved" },
                new[] { "wscadmin", "sb@dmin123", "admin@scholarbridge.com", "2", "true", "true" });

            int adminId = DataHelper.GetAdminUserId(Database);
            int wscAdminId = DataHelper.GetUserId(Database, "wscadmin");

            Database.Insert(AddRoles.TABLE_NAME, COLUMNS, new[] { ROLE_NAMES[0], adminId.ToString() });
            Database.Insert(AddRoles.TABLE_NAME, COLUMNS, new[] { ROLE_NAMES[1], adminId.ToString()  });
            Database.Insert(AddRoles.TABLE_NAME, COLUMNS, new[] { ROLE_NAMES[2], adminId.ToString()  });
            Database.Insert(AddRoles.TABLE_NAME, COLUMNS, new[] { ROLE_NAMES[3], adminId.ToString()  });
            Database.Insert(AddRoles.TABLE_NAME, COLUMNS, new[] { ROLE_NAMES[4], adminId.ToString()  });

            int adminRoleId = DataHelper.GetRoleId(Database, "Admin");
            int wscAdminRoleId = DataHelper.GetRoleId(Database, "WSCAdmin");
            Database.Insert(AddUserRoles.TABLE_NAME, new[] { "UserId", "RoleId" }, new[] { adminId.ToString(), adminRoleId.ToString() });
            Database.Insert(AddUserRoles.TABLE_NAME, new[] { "UserId", "RoleId" }, new[] { wscAdminId.ToString(), wscAdminRoleId.ToString() }); 
        }

        public override void Down()
        {
            Database.Delete(AddUserRoles.TABLE_NAME, "1", "1"); // Delete all

            Database.Delete(AddRoles.TABLE_NAME, "Name", ROLE_NAMES[0]);
            Database.Delete(AddRoles.TABLE_NAME, "Name", ROLE_NAMES[1]);
            Database.Delete(AddRoles.TABLE_NAME, "Name", ROLE_NAMES[2]);
            Database.Delete(AddRoles.TABLE_NAME, "Name", ROLE_NAMES[3]);
            Database.Delete(AddRoles.TABLE_NAME, "Name", ROLE_NAMES[4]);

            // These are used in the LastUpdateBy which will be hard/impossible to remove.
            // Database.Delete(AddUsers.TABLE_NAME, "Username", "admin");
            // Database.Delete(AddUsers.TABLE_NAME, "Username", "wscadmin");
        }
    }
}