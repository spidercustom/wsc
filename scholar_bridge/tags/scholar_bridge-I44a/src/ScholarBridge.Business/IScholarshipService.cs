﻿using System;
using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IScholarshipService
    {
        Scholarship GetById(int id);

		Scholarship Save(Scholarship scholarship);
        void Delete(Scholarship scholarship);

        Scholarship ScholarshipExists(Provider provider, string name, int year);

        IList<Scholarship> GetByProvider(Provider provider, ScholarshipStage[] stage);

        IList<Scholarship> GetByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStage[] stages);

        IList<Scholarship> GetByIntermediary(Intermediary intermediary, ScholarshipStage[] stages);

        void Activate(Scholarship scholarship, User approver);

        Scholarship CopyScholarship(Scholarship copyFrom);

        void CloseAwardPeriod(Scholarship scholarship, DateTime time);
        IList<Scholarship> GetBySearchCriteria(string criteria);

        void NotifySeekersClosedSince(DateTime date);
        void NotifySeekersScholarshipDue(int inDays);
        void SendToFriend(Scholarship scholarship, string userComments, string toEmail, User sender, string domainName);
        void RequestOnlineApplicationUrlApproval(Scholarship scholarship, Organization organization, User user);
		int CountAllActivated();
		int CountAllNotActivated();
		int CountAllClosed();
    	int CountAllRejected();

    	IList<Scholarship> GetAllScholarships();
        void ApproveOnlineApplicationUrl(Scholarship scholarship, User approver, User organizationUser);
        void RejectOnlineApplicationUrl(Scholarship scholarship, User rejector, User organizationUser);
    }
}
