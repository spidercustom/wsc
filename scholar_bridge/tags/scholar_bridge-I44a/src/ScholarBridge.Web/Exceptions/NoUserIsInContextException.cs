﻿using System;

namespace ScholarBridge.Web.Exceptions
{
    [global::System.Serializable]
    public class NoUserIsInContextException : ApplicationException //TODO: Do we need ScholarBridge exception type?
    {
        public NoUserIsInContextException() { }
        public NoUserIsInContextException(string message) : base(message) { }
        public NoUserIsInContextException(string message, Exception inner) : base(message, inner) { }
        protected NoUserIsInContextException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
