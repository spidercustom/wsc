﻿<%@ Page Language="C#" MasterPageFile="~/Simple.Master" AutoEventWireup="true" CodeBehind="ScholarshipDetails.aspx.cs" Inherits="ScholarBridge.Web.ScholarshipDetails" Title="Scholarship" %>
<asp:Content ID="metaContent" ContentPlaceHolderID="seoMeta" runat="server">
    <meta name="Description" content="<%= MetaDescription %>">
    <meta name="Keywords" content="<%= MetaKeywords %>">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .form-iceland span
        {
	        float:left;
	        display: block;  
	        margin: 5px 0px 10px 5px; 
	        font-family: Arial, Helvetica, sans-serif;
	        color: black;
	        font-size: 11px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <br />
    <br />
    <div class="form-iceland-container">
        <div class="form-iceland">
            <label for="scholarshipNameLabel">Scholarship Name:</label><asp:label Font-Bold="true" Font-Size="Larger" ID="scholarshipNameLabel" runat="server"></asp:label>
            <br />
            <label for="providerNameLabel">Provider Name:</label><asp:label ID="providerNameLabel" runat="server"></asp:label>
            <br />
            <asp:panel ID="donorPanel" runat="server">
                <label for="donorNameLabel">Donor Name:</label><asp:label ID="donorNameLabel" runat="server"></asp:label>
                <br />
            </asp:panel>
            <label for="missionStatementLabel">Description -</label>
            <br />
            <asp:label ID="missionStatementLabel" runat="server"></asp:label>
            <br />
            <br />
            <label for="academicYearLabel">Academic Year:</label><asp:label ID="academicYearLabel" runat="server"></asp:label>
            <br />
            <label for="scholarshipAmountLabel">Scholarship Amount:</label><asp:label ID="scholarshipAmountLabel" runat="server"></asp:label>
            <br />
            <label for="numberOfAwardsLabel">Number of Awards:</label><asp:label ID="numberOfAwardsLabel" runat="server"></asp:label>
            <br />
            <label for="applicationStartDateLabel">Application Start Date:</label><asp:label ID="applicationStartDateLabel" runat="server"></asp:label>
            <br />
            <label for="applicationDueDateLabel">Application Due Date:</label><asp:label Font-Bold="true" ID="applicationDueDateLabel" runat="server"></asp:label>
            <br />
            <br />

            <div>
                <div >TheWashBoard.org is a free web-based tool for Washington students seeking college scholarships.  We make 
                 scholarship searching simple.  Create a profile and review your scholarship matches.  Register to apply for this and other scholarships at <a href="http://www.theWashBoard.org" runat="server" >theWashBoard</a>.    
                </div>
                <br />
            </div>
        </div>
    </div>
    
</asp:Content>


