﻿<%@ Import Namespace="ScholarBridge.Web"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationAdminPageHeader.ascx.cs" Inherits="ScholarBridge.Web.Common.OrganizationAdminPageHeader" %>
 <div  >
<p class="HighlightedTextMedium">Only the <span style="font-weight: bold;">Organization Administrator</span> can edit these pages. All other users can only view.</p>

<p class="HighlightedTextMedium">Your <span style="font-weight: bold;">Organization Administrator</span> is responsible for:</p>

<p class="HighlightedTextMedium"><span style="font-weight: bold;">• Organization information</span>. Editable fields include the link to your website and alternative phone numbers. Updates to other Organization Information, including changing your Organization Administrator, must be submitted to <A id="supportlink" class="GreenLink" runat="server"  href="#">System Support</a></p>

<p class="HighlightedTextMedium"><span style="font-weight: bold;">• Users</span>. Add and remove users at your organization. Update user information.</p>

<p class="HighlightedTextMedium"><span style="font-weight: bold;">• Relationships</span>. Establish and maintain relationships with other Provider and Intermediary Organizations in theWashBoard.org.</p> 
</div>