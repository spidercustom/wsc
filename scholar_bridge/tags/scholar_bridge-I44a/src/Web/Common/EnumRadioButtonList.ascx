﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EnumRadioButtonList.ascx.cs" Inherits="ScholarBridge.Web.Common.EnumRadioButtonList" %>
<div class="control-set">
    <asp:RadioButtonList ID="RadioButtonList" runat="server">
    </asp:RadioButtonList>
    <br />
    <asp:CustomValidator ID="SelectNoneValidator" runat="server" 
      ErrorMessage="Select atleast one" 
      onservervalidate="SelectNoneValidator_ServerValidate"></asp:CustomValidator>
    <br />
</div>