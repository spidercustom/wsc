﻿using System;
using System.Linq;
using System.Collections;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class PrintScholarship : UserControl
    {
        public Scholarship Scholarship { get; set; }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            preferenceDiv.Visible = false;

			scholarshipName.Text = Scholarship.Name;
            ApplicationDueOn.Text = (DateTime.Now > Scholarship.ApplicationDueDate.Value.Date) ? "was due " : "is due ";
            ApplicationDueOn.Text += "<b>" + Scholarship.ApplicationDueDate.Value.Date.ToString("MM/dd/yyyy") + "</b>";
            ApplicationDueOn2.Text = Scholarship.ApplicationDueDate.Value.Date.ToString("MM/dd/yyyy");
            AcademicYear.Text = Scholarship.AcademicYear.YearStart + " - " + Scholarship.AcademicYear.YearEnd;
            MissionStatement.Text = Scholarship.MissionStatement;
            ScheduleApplicationStart.Text = (DateTime.Now > Scholarship.ApplicationStartDate.Value.Date) ? "started on " : "will begin on " ;
            ScheduleApplicationStart.Text += Scholarship.ApplicationStartDate.Value.Date.ToString("MM/dd/yyyy");
            ScheduleApplicationAwardOn.Text = Scholarship.AwardDate.Value.Date.ToString("MM/dd/yyyy");

            if (Scholarship.Donor == null || string.IsNullOrEmpty(Scholarship.Donor.Name))
                DonorRow.Visible = false;
            else
                Donor.Text = Scholarship.Donor.Name + ", " + Scholarship.Donor.Profile;

            if (Scholarship.FundingParameters != null)
            {
                FundingNumberOfAwards.Text =  Scholarship.FundingParameters.MinimumNumberOfAwards.ToString() + " - " +
                                             Scholarship.FundingParameters.MaximumNumberOfAwards.ToString();
                FundingTotalFunds.Text =  Scholarship.FundingParameters.AnnualSupportAmount.ToString("c0");
            }

            FundingAwardAmount.Text =  Scholarship.MinimumAmount.ToString("c0") + " - " + Scholarship.MaximumAmount.ToString("c0");
            FundingSupportPeriod.Text = Scholarship.AcademicYear.YearStart + " - " + Scholarship.AcademicYear.YearEnd;

            var org = Scholarship.DisplayContactFor == OrganizationContactDisplay.Intermediary ? 
                (Organization)Scholarship.Intermediary : 
                (Organization)Scholarship.Provider;

            if (org != null) // Why is this checked? Doesn't org have to be not null for a scholarship to display?
            {
                ContactName.Text = org.Name;
                ContactName2.Text = org.Name;
                ContactAddress.Text = org.Address.ToString();
                if(string.IsNullOrEmpty(org.Website))
                    WebsiteRow.Visible = false;
                else
                    ContactWebsite.Text = org.Website;

                if (org.Phone == null)
                    PhoneRow.Visible = false;
                else
                    ContactPhone.Text = org.Phone.FormattedPhoneNumber;
            }

            if (Scholarship.Renewable)
                Renewable.Text = string.IsNullOrEmpty(Scholarship.RenewableGuidelines) ? "Yes" : "Yes. " + Scholarship.RenewableGuidelines;
            else
                RenewDiv.Visible = false;

            if (Scholarship.Reapply)
                Reapply.Text = "Yes";
            else
                ReapplyDiv.Visible = false;

            EligibilityTypeOfStudent.Text = Scholarship.SeekerProfileCriteria.StudentGroups.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            EligibilitySupportedSituation.Text =
                GetCommaSeperated(
                    (from x in Scholarship.FundingProfile.SupportedSituation.TypesOfSupport select x.Name).ToArray());

            if (Scholarship.SeekerProfileCriteria.CollegeType == CollegeType.Specify)
                EligibilityColleges.Text =
                    GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Colleges select x.Name).ToArray());
            else
                EligibilityColleges.Text = Scholarship.SeekerProfileCriteria.CollegeType.GetDisplayName();
                
            
            EligibilitySchoolType.Text = Scholarship.SeekerProfileCriteria.SchoolTypes.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            EligibilityAcademicPrograms.Text = Scholarship.SeekerProfileCriteria.AcademicPrograms.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);
            EligibilityEnrollmentStatus.Text = Scholarship.SeekerProfileCriteria.SeekerStatuses.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR);

            AddAttribute(SeekerProfileAttribute.FirstGeneration, "Should be first generation");
            AddAttribute(SeekerProfileAttribute.Ethnicity, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Ethnicities select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Religion, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Religions select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Gender, Scholarship.SeekerProfileCriteria.Genders.GetDisplayNames(EnumExtensions.COMMA_SEPARATOR));

            if (Scholarship.SeekerProfileCriteria.State != null && AddAttribute(SeekerProfileAttribute.State, Scholarship.SeekerProfileCriteria.State.Name) && Scholarship.SeekerProfileCriteria.State.Abbreviation == "WA")
            {
                AddAttribute(SeekerProfileAttribute.HighSchool, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.HighSchools select x.Name).ToArray()));
                AddAttribute(SeekerProfileAttribute.SchoolDistrict, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.SchoolDistricts select x.Name).ToArray()));
                AddAttribute(SeekerProfileAttribute.City, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Cities select x.Name).ToArray()));
                AddAttribute(SeekerProfileAttribute.County, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Counties select x.Name).ToArray()));
            }
            AddAttribute(SeekerProfileAttribute.AcademicArea, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.AcademicAreas select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Career, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Careers select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Organization, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Organizations select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Company, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Companies select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.SeekerHobby, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Hobbies select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Sport, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Sports select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Club, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.Clubs select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.WorkType, "Provide Information");
            AddAttribute(SeekerProfileAttribute.ServiceType, "Provide Information");

            if (Scholarship.SeekerProfileCriteria.GPA != null)
                AddAttribute(SeekerProfileAttribute.GPA, 
                    (Scholarship.SeekerProfileCriteria.GPA.Maximum.HasValue && Scholarship.SeekerProfileCriteria.GPA.Maximum != Scholarship.MAX_GPA)? 
                        string.Format("{0} - {1}", Scholarship.SeekerProfileCriteria.GPA.Minimum.Value.ToString("0.000"), Scholarship.SeekerProfileCriteria.GPA.Maximum.Value.ToString("0.000")) : 
                        string.Format("{0} or Greater", Scholarship.SeekerProfileCriteria.GPA.Minimum.Value.ToString("0.000")));
            if (Scholarship.SeekerProfileCriteria.SATScore != null)
            {
                AddAttribute(SeekerProfileAttribute.SATWriting, string.Format("{0} - {1} ", Scholarship.SeekerProfileCriteria.SATScore.Writing.Minimum, Scholarship.SeekerProfileCriteria.SATScore.Writing.Maximum));
                AddAttribute(SeekerProfileAttribute.SATCriticalReading, string.Format("{0} - {1} ", Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Minimum, Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Maximum));
                AddAttribute(SeekerProfileAttribute.SATMathematics, string.Format("{0} - {1} ", Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Minimum, Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Maximum));
            }
            if (Scholarship.SeekerProfileCriteria.ACTScore != null)
            {
                AddAttribute(SeekerProfileAttribute.ACTEnglish, string.Format("{0} - {1} ", Scholarship.SeekerProfileCriteria.ACTScore.English.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.English.Maximum));
                AddAttribute(SeekerProfileAttribute.ACTMathematics, string.Format("{0} - {1} ", Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Maximum));
                AddAttribute(SeekerProfileAttribute.ACTReading, string.Format("{0} - {1} ", Scholarship.SeekerProfileCriteria.ACTScore.Reading.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.Reading.Maximum));
                AddAttribute(SeekerProfileAttribute.ACTScience, string.Format("{0} - {1} ", Scholarship.SeekerProfileCriteria.ACTScore.Science.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.Science.Maximum));
            }
            AddAttribute(SeekerProfileAttribute.ClassRank, GetCommaSeperated((from x in Scholarship.SeekerProfileCriteria.ClassRanks select x.Name).ToArray()));
            AddAttribute(SeekerProfileAttribute.Honor, "Provide information");
            AddAttribute(SeekerProfileAttribute.APCreditsEarned, "Provide information");
            AddAttribute(SeekerProfileAttribute.IBCreditsEarned, "Provide information");

            ApplicationDetails.Visible = false;
            ArrayList financialRequirements = new ArrayList();
            if (Scholarship.IsApplicantDemonstrateFinancialNeedRequired.HasValue && Scholarship.IsApplicantDemonstrateFinancialNeedRequired.Value == true)
                financialRequirements.Add("Must demonstrate Financial Need");
            
            if (Scholarship.IsFAFSARequired.HasValue && Scholarship.IsFAFSARequired.Value == true)
                financialRequirements.Add("Completed FAFSA");

            if (Scholarship.IsFAFSAEFCRequired.HasValue && Scholarship.IsFAFSAEFCRequired.Value == true)
                financialRequirements.Add("Expected Family Contribution (defined by FAFSA)");

            if (financialRequirements.Count > 0)
            {
                AddToContainer(FinancialNeedRow, string.Join(", ", (string[])financialRequirements.ToArray(typeof(string))));
                ApplicationDetails.Visible = true;
            } 
            else
                FinancialNeedRow.Visible = false;

            ArrayList additionalRequirements = new ArrayList();
            if (Scholarship.AdditionalRequirements != null)
                foreach (AdditionalRequirement item in Scholarship.AdditionalRequirements)
                    additionalRequirements.Add(item.Name);
            if (additionalRequirements.Count > 0)
            {
                ApplicationDetails.Visible = true;
                AddToContainer(AdditionalRequirementsRow, "<ul><li>" + string.Join("</li><li>", (string[])additionalRequirements.ToArray(typeof(string))) + "</li></ul>");
            }
            else
                AdditionalRequirementsRow.Visible = false;

            ArrayList additionalQuestions = new ArrayList();
            if (Scholarship.AdditionalQuestions != null)
                foreach (ScholarshipQuestion item in Scholarship.AdditionalQuestions)
                    additionalQuestions.Add(item.QuestionText);
            if (additionalQuestions.Count > 0)
            {
                ApplicationDetails.Visible = true;
                AddToContainer(AdditionalQuestionsRow, "<ul><li>" + string.Join("</li><li>", (string[])additionalQuestions.ToArray(typeof(string))) + "</li></ul>");
            }
            else
                AdditionalQuestionsRow.Visible = false;

            if (Scholarship.Attachments != null && Scholarship.Attachments.Count > 0)
            {
                scholarshipAttachments.Attachments = Scholarship.Attachments;
                scholarshipAttachments.View = FileList.FileListView.List;
                ApplicationDetails.Visible = true;
            } else {
                scholarshipAttachments.Visible = false;
                FormsTable.Visible = false;
            }

            if (Scholarship.IsUseOnlineApplication)
            {
                ApplyAtUrl.Text = "<a href=\""+Scholarship.OnlineApplicationUrl+"\">"+Scholarship.OnlineApplicationUrl+"</a>";
                ApplicationDetails.Visible = true;
            }
            else
                ApplyAtRow.Visible = false;
        }
         
        private bool AddAttribute(SeekerProfileAttribute profileAttribute,string values )
         {
            var attributeUsage = Scholarship.SeekerProfileCriteria.Attributes.Find(profileAttribute);
            if (attributeUsage == null || attributeUsage.UsageType == ScholarshipAttributeUsageType.NotUsed) 
                return false;

            var criteriaItem = new HtmlGenericControl("DIV");
            var criteriaTitle = new HtmlGenericControl("DIV");
            var criteria = new HtmlGenericControl("DIV");
            criteriaItem.Attributes.Add("class", "attribute");
            criteriaTitle.InnerHtml = attributeUsage.Attribute.GetDisplayName() + ":";
            criteria.InnerHtml = values;
            criteriaItem.Controls.Add(criteriaTitle);
            criteriaItem.Controls.Add(criteria);

            if (attributeUsage.UsageType == ScholarshipAttributeUsageType.Minimum)
            {
                MinimumTable.Controls.Add(criteriaItem);
            }
            else
            {
                PreferenceTable.Controls.Add(criteriaItem);
                preferenceDiv.Visible = true;
            }
            return true;
         }

        private void AddToContainer(HtmlGenericControl container, string text)
         {
             var item = new HtmlGenericControl("DIV");
            
             item.InnerHtml = text;
             container.Controls.Add(item);
         }

        private string GetCommaSeperated(string[] array)
        {
            return string.Join(", ", array);
        }
    }
}