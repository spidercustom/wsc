﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Profile
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }

        private const string PASSWORD_ERROR_MESSAGE=
            "Password must be at least 7 characters with at least one special character or number.";
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.UserSettings);
            editUserName.CurrentUser = UserContext.CurrentUser;
            editUserEmail.CurrentUser = UserContext.CurrentUser;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                seekertypediv.Visible = false;
                if (UserContext.CurrentSeeker != null)
                {
                    seekertypediv.Visible = true;
                    seekerType.Text = UserContext.CurrentSeeker.SeekerType.GetDisplayName();
                }
            }
        }

        protected void editUserName_OnUserSaved(User user)
        {
            SuccessMessageLabel.SetMessage("Your information has been updated");
            Response.Redirect("~/Profile/");
        }

        protected void ChangePassword1_ChangedPassword(object sender, EventArgs e)
        {
            SuccessMessageLabel.SetMessage("Your password has been changed.");
        }

        protected void ChangePassword1_Error(object sender, EventArgs e)
        {
            ChangePassword1.ChangePasswordFailureText = "";
            var validator = (CustomValidator) ChangePassword1.ChangePasswordTemplateContainer.FindControl("NewPassowrdCustomValidator");
            validator.IsValid = false;
            validator.ErrorMessage = PASSWORD_ERROR_MESSAGE;
            ClientSideDialogs.ShowDivAsDialog("ChangePasswordValidationErrorPopup");
        }
    }
}