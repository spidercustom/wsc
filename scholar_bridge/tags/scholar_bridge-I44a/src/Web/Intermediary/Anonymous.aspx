﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Anonymous.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Anonymous" Title="Intermediary Home" %>
<%@ Register Assembly="ScholarBridge.Web" Namespace="ScholarBridge.Web" TagPrefix="sb" %>
<%@ Register Src="~/Common/Login.ascx" TagName="Login" TagPrefix="sb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Intermediary Home" />
    
    <script type="text/javascript">
        var LoginUrl = '<%= LinkGenerator.GetFullLink("default.aspx")  %>'
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/provide_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Body" Runat="Server">
    <div id="LoginForm"><sb:Login ID="loginForm" runat="server" /></div>
    <img alt="Smarter Scholarship Matches" src="<%= ResolveUrl("~/images/PgTitle_SmarterScholarshipMatches.gif") %>" />
    <p class="hookline">TheWashBoard.org makes scholarship searching simple. In one stop, students can search and apply for vetted scholarships specific to their academic interests, college or university, or other criteria.</p>

    <br class="clear" />
    <div class="three-column">
        <div>
            <a href="<%= LinkGenerator.GetFullLink("/Intermediary/RegisterIntermediary.aspx") %>">
                <img alt="Register" src="<%= ResolveUrl("~/images/BottomBox01_CreateOrg.gif") %>"/>
            </a>
            <a href="<%= LinkGenerator.GetFullLink("/Intermediary/RegisterIntermediary.aspx") %>">
                <img alt="" src="<%= ResolveUrl("~/images/ProviderBottomBox01.gif") %>"/>
            </a>
            <p>It’s easy! Enter your Organization's information to setup an account.</p>
            <a class="circle-icon-link" href="<%= LinkGenerator.GetFullLink("/Intermediary/RegisterIntermediary.aspx") %>">Register</a>
        </div>
        <div>
            <img alt="Build scholarships" src="<%= ResolveUrl("~/images/BottomBox02_BuildScholarships.gif") %>"/>
            <img alt="" src="<%= ResolveUrl("~/images/ProviderBottomBox02.gif") %>"/>
            <p> We guide you through a simple step by step process to define your scholarships unique criteria and application requirements.</p>
        </div>
        <div>
            <img alt="Manage applications" src="<%= ResolveUrl("~/images/BottomBox03_ManageApplications.gif") %>" />
            <img alt="" src="<%= ResolveUrl("~/images/ProviderBottomBox03.gif") %>" />
            <p> We make reviewing and evaluating applications easy. Review online, or download applicant information.</p>
        </div>
    </div>
    
   <sb:SuccessMessageLabel ID="SuccessMessageLabel1" runat="server"></sb:SuccessMessageLabel>  
</asp:Content>
