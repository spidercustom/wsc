﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Web.Provider.Applications
{
    public partial class Show : Page
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        public Application ApplicationToView { get; set; }

        private int ApplicationId
        {
            get
            {
                int applicationId;
                if (!Int32.TryParse(Request.Params["aid"], out applicationId))
                    throw new ArgumentException("Cannot understand value of parameter aid");
                return applicationId;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsInPrintView())
                Response.Redirect("~/Provider/Applications/show.aspx?aid=" + ApplicationId.ToString() + "&print=true");

            ApplicationToView = ApplicationService.GetById(ApplicationId);

            if (ApplicationToView != null)
            {
                UserContext.EnsureApplicationBelongsToOrganization(ApplicationToView);

                if (ApplicationToView.ApplicationType == ApplicationType.External)
                {
                    var control = (PrintExternalApplication)LoadControl("~/Common/PrintExternalApplication.ascx");
                    control.ApplicationToView = ApplicationToView;

                    ApplicationPlaceHolder.Controls.Add(control); 
                }
                else
                {
                    var control = (PrintApplication)LoadControl("~/Common/PrintApplication.ascx");
                    control.ApplicationToView = ApplicationToView;
                    ApplicationPlaceHolder.Controls.Add(control); 
                    
                }
            }
        }
    }
}
