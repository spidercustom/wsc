﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Provider.Scholarships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IScholarshipService ScholarshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.ProviderScholarships); 
            UserContext.EnsureProviderIsInContext();
            FillScholarships();
        }

        private void FillScholarships()
        {
            var provider = UserContext.CurrentProvider;

            if (scholarshipsListViewOptions.GetSelectedOrganizationId() == 0)
            {
                scholarshipsList.Scholarships = ScholarshipService.GetByProvider(provider,scholarshipsListViewOptions.GetSelectedStages());
                scholarshipsList.PreserveSorting("");
            }
            else
            {
                var intermediary = IntermediaryService.FindById(scholarshipsListViewOptions.GetSelectedOrganizationId());
                scholarshipsList.Scholarships = ScholarshipService.GetByOrganizations(provider, intermediary, scholarshipsListViewOptions.GetSelectedStages());
                scholarshipsList.PreserveSorting("");
            }
        }

        protected void UpdateViewBtn_Click(object sender, EventArgs e)
        {
            FillScholarships();
        }
    }
}