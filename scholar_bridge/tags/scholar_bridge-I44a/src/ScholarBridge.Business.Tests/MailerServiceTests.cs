﻿using System;
using System.IO;
using System.Net.Mail;
using NUnit.Framework;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class MailerServiceTests
    {
        private readonly MailerService mailerService = new MailerService { TemplateDirectory = "C:/tmp" };

        [Test]
        public void ValidatesGoodMessage()
        {
            var goodMessage = CreateGoodMessage();
            Assert.IsTrue(mailerService.IsValidMessage(goodMessage));
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SendMailThrowsExceptionIfNullPassed()
        {
            mailerService.SendMail(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ValidationThrowsExceptionIfMessageNull()
        {
            mailerService.IsValidMessage(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(InvalidEmailMessageException))]
        public void ValidationThrowsExceptionIfSubjectIsNull()
        {
            var goodMessage = CreateGoodMessage();
            goodMessage.Subject = null;

            mailerService.IsValidMessage(goodMessage);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(InvalidEmailMessageException))]
        public void ValidationThrowsExceptionIfBodyIsNull()
        {
            var goodMessage = CreateGoodMessage();
            goodMessage.Body = null;

            mailerService.IsValidMessage(goodMessage);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(InvalidEmailMessageException))]
        public void ValidationThrowsExceptionIfToHasNoEmails()
        {
            var goodMessage = CreateGoodMessage();
            goodMessage.To.Clear();

            mailerService.IsValidMessage(goodMessage);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ThrowsExceptionIfTemplateCanNotBeFound()
        {
            var templateParams = new MailTemplateParams();
            mailerService.SendMail(MessageType.ForgotPassword, templateParams, "test@example.com", "test@example.com");
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ThrowsExceptionIfTemplateCanNotBeFound2()
        {
            var templateParams = new MailTemplateParams
            {
                To = "test@example.com",
                From = "test@example.com"
            };
            mailerService.SendMail(MessageType.ForgotPassword, templateParams);
            Assert.Fail();
        }

        [Test(), Ignore("Unable to test because it requires smtp server to be running")]
        public void SendGoodMail()
        {
            mailerService.SendMail("test@scholarbridge.com", "bsingh@spiderlogic.com", "Test message from SendGoodMail", "This is a test message");
        }

        private static MailMessage CreateGoodMessage()
        {
            return new MailMessage("foo@bar.com", "bar@foo.com")
            {
                Subject = "Test Message",
                Body = "Hello"
            };
        }
    }
}