﻿using System;
using System.Collections;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using Spring.Context.Support;

namespace ScholarBridge.Data.Tests
{
    public class LookupTestBase<TDal, TEntity> : TestBase
        where TDal : IGenericLookupDAL<TEntity>
        where TEntity : ILookup
    {
        public UserDAL UserDAL { get; set; }

        private User User { get; set; }
        protected TDal DAL { get; set; }

        protected override void OnSetUpInTransaction()
        {
            DAL = RetrieveDAL();
            User = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
        }

        public static TDal RetrieveDAL()
        {
            var foundObjects = ContextRegistry.GetContext().GetObjectsOfType(typeof(TDal));
            if (null == foundObjects ||  0.Equals(foundObjects.Count))
                throw new ArgumentException("Cannot find object of type {0} in spring container", typeof(TDal).FullName);

            //What those days were, lines of code to retrive value from collection, 
            //We can probably live without putting ifs
            IEnumerator objectsEnumerable = foundObjects.Values.GetEnumerator();
            if (null == objectsEnumerable)
                throw new InvalidOperationException("Cannot retrive enumerator");
            if (!objectsEnumerable.MoveNext())
                throw new InvalidOperationException("Unexpectedly cannot move to next");
            return (TDal) objectsEnumerable.Current;
        }

        [Test]
        public void test_save_and_load()
        {
            var @entity = CreateLookupObject("Test Object");
            var original = DAL.Insert(@entity);
            var retrived = DAL.FindById(original.GetIdAsInteger());
            AssertLookup(original, retrived);
        }

        [Test]
        public void find_all_filters_deprecated()
        {
            var initialCount = DAL.FindAll().Count;
            var entity1 = CreateLookupObject("Test 1");
            var entity2 = CreateLookupObject("Test 2");
            var entity3 = CreateLookupObject("Deprecated");
            entity3.Deprecated = true;

            DAL.Insert(entity1);
            DAL.Insert(entity2);
            DAL.Insert(entity3);

            var found = DAL.FindAll();
            Assert.That(found.Count, Is.EqualTo(initialCount + 2));
        }

        [Test]
        public void can_get_all()
        {
            var entities = DAL.FindAll();
            Assert.IsNotNull(entities);
        }

        [Test]
        public void delete_marks_as_deprecated()
        {
            var entity = CreateLookupObject("Test 1");
            var original = DAL.Insert(entity);
            Assert.That(original.Deprecated, Is.False);
            DAL.Delete(original);

            var afterDelete = DAL.FindById(original.GetIdAsInteger());
            Assert.That(afterDelete, Is.Not.Null);
            Assert.That(afterDelete.Deprecated, Is.True);
        }

        private static void AssertLookup(ILookup expected, ILookup actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Description, actual.Description);
            Assert.AreEqual(expected.LastUpdate.By.Username, actual.LastUpdate.By.Username);
            Assert.AreEqual(expected.LastUpdate.On, actual.LastUpdate.On);
        }

        protected virtual TEntity CreateLookupObject(string name)
        {
            var @object = (TEntity) Activator.CreateInstance(typeof(TEntity));
            if (@object == null)
                throw new InvalidOperationException(string.Format("Either type {0} is not of type ILookup or cannot create instance of it", typeof(TEntity).FullName));
            @object.Name = name;
            @object.Description = "test-description";
            @object.LastUpdate = new ActivityStamp(User);
            return @object;
        }

        public static TEntity CreateLookupObject(string name, User user)
        {
            var @object = (TEntity)Activator.CreateInstance(typeof(TEntity));
            if (@object == null)
                throw new InvalidOperationException(string.Format("Either type {0} is not of type ILookup or cannot create instance of it", typeof(TEntity).FullName));
            @object.Name = name;
            @object.Description = "test-description";
            @object.LastUpdate = new ActivityStamp(user);
            return @object;
        }

    }
}
