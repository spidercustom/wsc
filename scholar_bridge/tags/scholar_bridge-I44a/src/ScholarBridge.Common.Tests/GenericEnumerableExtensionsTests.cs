using System.Collections.Generic;
using System.Collections;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Common.Tests
{

    [TestFixture]
    public class GenericEnumerableExtensionsTests
    {

        [Test]
        public void can_run_for_each()
        {
            int i = 0;

            IEnumerable<Foo> foos = new List<Foo> {new Foo(), new Foo()};
            IGenericEnumerableExtensions.ForEach(foos, f => f.Name = "Foo" + i ++);
            var fooEnum = foos.GetEnumerator();

            Assert.IsTrue(fooEnum.MoveNext());
            Assert.AreEqual("Foo0", fooEnum.Current.Name);
            Assert.IsTrue(fooEnum.MoveNext());
            Assert.AreEqual("Foo1", fooEnum.Current.Name);
        }

        [Test]
        public void can_run_for_each_non_generic()
        {
            int i = 0;

            IEnumerable foos = new ArrayList { new Foo(), new Foo() };
            IEnumerableExtensions.ForEach(foos, (Foo f) => f.Name = "Foo" + i++);
            var fooEnum  = foos.GetEnumerator();
            
            Assert.IsTrue(fooEnum.MoveNext());
            Assert.AreEqual("Foo0", ((Foo) fooEnum.Current).Name);
            Assert.IsTrue(fooEnum.MoveNext());
            Assert.AreEqual("Foo1", ((Foo)fooEnum.Current).Name);
        }

        private class Foo
        {
            public string Name { get; set; }
        }
    }
}