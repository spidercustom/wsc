using System;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum SupportType
    {
        None = 1,
        Emergency = 2,
        Traditional = 4
    }
}