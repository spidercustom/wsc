﻿using ScholarBridge.Domain.Location;

namespace ScholarBridge.Domain.Lookup
{
    public class SchoolDistrict : LookupBase
    {
        public virtual State State { get; set; }
    }
}
