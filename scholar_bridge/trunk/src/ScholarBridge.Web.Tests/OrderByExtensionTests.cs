﻿using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Tests
{
	[TestFixture]
	public class OrderByExtensionTests
	{
		[Test]
		public void GetValueTest()
		{
			string s = "string of length 19";
			Assert.AreEqual(s.Length, s.GetValue("Length"));//will get the value of the property provided

			string formatString = "this is no longer a string of length {0}";
			Assert.AreEqual(string.Format(formatString, s.Length), s.GetValue("Length", formatString));//you can provide a format string and it'll return a formatted string

			object o = s;
			Assert.AreEqual(s.Length, o.GetValue("Length"));

			Assert.IsNull(o.GetValue("Some Property Name that doesn't exist"));//if the property doesn't exist, null is returned

			o = null;
			Assert.IsNull(o.GetValue("Anything"));//this can be called on null objects, which is pretty sweet
		}
		[Test]
		public void OrderByTest()
		{
			var numbers = GetNumberList();

			var sortedNumbers = numbers.OrderBy(n => n.NumberValue, true).ToList();

			Assert.AreEqual(numbers[0].NumberValue, sortedNumbers[1].NumberValue);
			Assert.AreEqual(numbers[3].NumberValue, sortedNumbers[2].NumberValue);


			sortedNumbers = numbers.OrderBy(n => n.NumberName, true).ToList();

			Assert.AreEqual("four", sortedNumbers[0].NumberName);
			Assert.AreEqual("three", sortedNumbers[2].NumberName);
		}

		private static IList<Number> GetNumberList()
		{
			return new List<Number>
				{	new Number("two", "2"),
					new Number("one", "1"),
					new Number("four", "4"),
					new Number("three", "3")};
		}
		private class Number
		{
			public string NumberName { get; set;} 
			public string NumberValue { get; set;}

			public Number(string name, string value)
			{
				NumberName = name;
				NumberValue = value;
			}
		}
	}
}
