﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutMe.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.AboutMe" %>
<%@ Register TagPrefix="elv"  Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" %>
<%@ Register TagPrefix="sbCommon" Assembly="Web" Namespace="ScholarBridge.Web.Common" %>
<%@ Register TagPrefix="Upload" Assembly="Brettle.Web.NeatUpload" Namespace="Brettle.Web.NeatUpload" %>
<%@ Register TagPrefix="sb" TagName="LookupDialog" src="~/Common/Lookup/LookupDialog.ascx"  %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register TagPrefix="sb" TagName="Tip" Src="~/Common/Tip.ascx" %>  

<h2>About me</h2>
<p>This is your chance to let Scholarship providers know who you are.  Add a personal statement, share your talents and gifts, and attach a writing sample or pictures. Providers want to know something about the person who is going to receive their scholarships. </p>
<div class="question_list">
    <div class="full">
        <label>
            <span class="tip" title="In 1,500 characters or less, say something about yourself, what you hope to achieve by going to college, what it would mean to you to receive a scholarship.">
                Personal Statement:
            </span>
        </label>
        <asp:TextBox ID="PersonalStatementControl" runat="server" TextMode="MultiLine" Rows="12"></asp:TextBox>
        <elv:PropertyProxyValidator ID="PersonalStatementValidator" runat="server" ControlToValidate="PersonalStatementControl" PropertyName="PersonalStatement" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <div class="full">
        <label>
            <span class="tip" title="Are you creative? a leader? This is a great way to briefly let scholarship providers know something about you. This is not used in the Scholarship match but will be included on your application.">
                5 Words That Describe Me:
            </span>
        </label>
        <asp:TextBox ID="FiveWordsControl" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="FiveWordsValidator" runat="server" ControlToValidate="FiveWordsControl" PropertyName="Words" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <div class="full">
        <label>
            <span class="tip" title="Are you a problem solver? imaginative? a great communicator? This is a great way to briefly let scholarship providers know your skills. This is not used in the Scholarship match but will be included on your application.">
                5 Skills I Have
            </span>
        </label>
        <asp:TextBox ID="FiveSkillsControl" runat="server"></asp:TextBox>
        <elv:PropertyProxyValidator ID="FiveSkillsValidator" runat="server" ControlToValidate="FiveSkillsControl" PropertyName="Skills" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <div class="full">
        <label>My Talents & Gifts</label>
        <asp:TextBox ID="MyGiftControl" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
        <elv:PropertyProxyValidator ID="MyGiftValidator" runat="server" ControlToValidate="MyGiftControl" PropertyName="MyGift" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
</div>

<div>
    <h2>Attachments (Transcripts, Letters of Recommendation, Award Letters, etc.)</h2>
    <p>These documents may be attached to any scholarship application that you fill out. The maximum file size limit is 10 MB.</p>
    <div style="display: none;">
        <Upload:ProgressBar id="AttachFileProgressBar" runat="server" inline="true" />
    </div>
    <div class="question_list">
        <div>
            <label>Select File: </label>
            <Upload:InputFile ID="AttachFile" runat="server" />
            <asp:Label ID="fileTooBigLbl" runat="server" Visible="false" CssClass="error" Text="The file you tried to upload is too large. It should be less than 10MB." />
        </div>
        <div>
            <label>
                <span class="tip" title="Add a descriptive comment with the file being uploaded.">
                    File Description
                </span>
            </label>
            <asp:TextBox ID="AttachmentComments" runat="server" />    
            <elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments" PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment"/>
        </div>
        <div>
            <label></label>
            <div class="tip" title="Including files with applications will attach the file to each scholarship application you create. You will be able to add and remove files from the application before you submit it.">
                <asp:checkbox runat="server" ID="includeCheckBox" />
                <label >
                    Automatically include in applications.
                </label>
            </div>
        </div>
        <div>
            <label></label>
            <sbCommon:AnchorButton ID="UploadFile" runat="server" Text="Attach File" onclick="UploadFile_Click" />
        </div>
        <div>
            <label><Upload:ProgressBar id="progressBarId" runat="server" inline="false" Url="~/NeatUpload/SmoothProgress.aspx" /></label>
        </div>
    </div>

    <asp:ListView ID="attachedFiles" runat="server" OnItemDeleting="attachedFiles_OnItemDeleting" onitemcommand="attachedFiles_ItemCommand">
        <LayoutTemplate>
        <table class="sortableTable">
            <thead>
                <tr>
                    <th style="width:50px;">&nbsp;</th>
                    <th style="width:200px;">File</th>
                    <th style="width:70px;">Size</th>
                    <th>Comments</th>
                    <th style="width:80px;">
                        <span class="tip" title="Including files with applications will attach the file to each scholarship application you create. You will be able to add and remove files from the application before you submit it.">
                            Included with applications
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:LinkButton ID="deleteAttachmentBtn"   runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("Id")%>' /></td>
                <td>
                    <asp:LinkButton ID="downloadButton" CommandName="Download" CausesValidation="false" CommandArgument='<%# Eval("Id") %>' runat="server" Text='<%# Bind("Name") %>' ToolTip='<%# Eval("Comment") %>'>
                    </asp:LinkButton>            
                </td>
                <td><%# Eval("DisplaySize") %></td>
                <td><%# Eval("Comment") %></td>
                <td align="center"><asp:checkbox runat="server" ID="includeWithAppsCheck" Checked='<%# Eval("IncludeWithApplications") %>'/></td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate></EmptyDataTemplate>
    </asp:ListView>
</div>