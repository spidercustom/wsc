﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register TagPrefix="sb" TagName="Tip" Src="~/Common/Tip.ascx" %>  

<h2>My Financial Need</h2>
<div class="question_list">
    <div class="full">
        <label>
            <span class="tip" title="Describe your financial need in your own words. Let providers know the challenges you are facing in paying for college. Examples would include employment status, size of family, and other financial obligations.">
                Describe your financial challenge:
            </span>
        </label>
        <asp:TextBox ID="MyChallengeControl" runat="server" TextMode="MultiLine"  Rows="6"></asp:TextBox>
        <elv:PropertyProxyValidator ID="MyChallengeValidator" runat="server" ControlToValidate="MyChallengeControl" PropertyName="MyChallenge" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
</div>
<hr />
<div class="two-column with_divider">
    <div class="question_list">
        <div>
            <label>
                <span class="tip" title="The FAFSA isn’t just used by schools to determine your eligibility for grants, student loans, and other federal aid. Many scholarship providers will also use the FAFSA to evaluate your need for scholarship aid. While not all scholarships require you to complete the FAFSA, we encourage all students seeking aid to apply.">
                    Have you completed the <a  href="http://www.fafsa.ed.gov" target="_blank">FAFSA</a>?
                </span>
            </label>
            <div>
                <asp:RadioButton ID="FAFSACompletedControl" Text="Yes" runat="server" GroupName="FAFSAFilled" /><br />
                <asp:RadioButton ID="FAFSANotCompletedControl" Text="No" runat="server" GroupName="FAFSAFilled"/>
            </div>
        </div>
        <div>
            <label>What is your FAFSA defined EFC (Expected Family Contribution)?</label> 
            <sandTrap:NumberBox ID="ExpectedFamilyContributionControl" runat="server" Precision="0" Enabled="false"/>
        </div>
    </div>
    <div class="question_list">
        <div>
            <label>
                <span class="tip" title="Some scholarships specify what the funds can be used for. Check all that apply for your situation to ensure you get matched with scholarships that will fund your needs.">
                    Types of Support
                </span>
            </label>
            <asp:CheckBoxList ID="TypesOfSupport" runat="server"/>
        </div>
    </div>
</div>

