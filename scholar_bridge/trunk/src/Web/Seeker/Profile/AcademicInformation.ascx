﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicInformation.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.AcademicInformation" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemRadioButtonList.ascx" tagname="LookupItemRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerStudentType.ascx" tagname="SeekerStudentType" tagprefix="sb" %>
<%@ Register src="~/Common/SeekerCollegeType.ascx" tagname="SeekerCollegeType" tagprefix="sb" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register TagPrefix="sb" TagName="Tip" Src="~/Common/Tip.ascx" %>  

<script type="text/javascript">
	function UpdateSATComulative(satWritingId, satReadingId, satMathId,satHiddenComulativeId, satComulativeId) {
	    
	    if (satComulativeId == undefined) {
	        return false;
	    }
	    
	    var sum = 0;
	    
	    //add only if the value is number
        if (!isNaN($("#" + satWritingId).val()) && $("#" + satWritingId).val().length != 0) {
        sum += parseFloat($("#" + satWritingId).val());
        }

        if (!isNaN($("#" + satReadingId).val()) && $("#" + satReadingId).val().length != 0) {
            sum += parseFloat($("#" + satReadingId).val());
        }

        if (!isNaN($("#" + satMathId).val()) && $("#" + satMathId).val().length != 0) {
            sum += parseFloat($("#" + satMathId).val());
        }

        if (sum > 0) {
            disableElementById(satComulativeId); $("#" + satComulativeId).val(sum); $("#" + satHiddenComulativeId).val(sum);
        } else {
            enableElementById(satComulativeId); $("#" + satHiddenComulativeId).val(''); 
        }
	}

    function UpdateACTComulative(actEnglishId, actReadingId, actMathId, actScienceId,actHiddenComulativeId, actComulativeId) {
        if (actComulativeId == undefined) {
            return false;
        }

        var sum = 0;

        //add only if the value is number
        if (!isNaN($("#" + actEnglishId).val()) && $("#" + actEnglishId).val().length != 0) {
            sum += parseFloat($("#" + actEnglishId).val());
        }

        if (!isNaN($("#" + actReadingId).val()) && $("#" + actReadingId).val().length != 0) {
            sum += parseFloat($("#" + actReadingId).val());
        }

        if (!isNaN($("#" + actMathId).val()) && $("#" + actMathId).val().length != 0) {
            sum += parseFloat($("#" + actMathId).val());
        }

        if (!isNaN($("#" + actScienceId).val()) && $("#" + actScienceId).val().length != 0) {
            sum += parseFloat($("#" + actScienceId).val());
        }

        if (sum > 0) {
            $("#" + actComulativeId).val(sum); $("#" + actHiddenComulativeId).val(sum); disableElementById(actComulativeId);
        } else {
            enableElementById(actComulativeId); $("#" + actHiddenComulativeId).val(''); 
        }
    }
</script>
<div class="two-column with_divider">
    <div class="question_list">
        <h2>Secondary Education</h2>
        <div>
            <label class="full">
                <span title="Be sure to update your student information to find all the scholarships that you can apply for." class="tip">
                    What type of student are you currently?
                </span>
            </label>
            <sb:SeekerStudentType id="CurrentStudentGroupControl" runat="server" />
        </div>

        <div>
            <label class="full">Which high school did you attend or are attending?</label>
            <label style="margin-top:0px;"><span><sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl" OtherControl="HighSchoolOtherControl" OtherControlLabel="OtherHighSchool" ItemSource="HighSchoolDAL" Title="High School Selection" SelectionLimit="1" /></span></label>
            <asp:TextBox ID="HighSchoolControl" ReadOnly="true" runat="server"></asp:TextBox>
        </div>
        <div id="OtherHighSchool">
            <label>Other:</label>
            <asp:TextBox ID="HighSchoolOtherControl" runat="server" />
        </div>

        <div>
            <label class="full">Which school district is this school in?</label>
            <label style="margin-top:0px;"><span><sb:LookupDialog ID="SchoolDistrictControlLookupDialogButton" runat="server" BuddyControl="SchoolDistrictControl" ItemSource="SchoolDistrictDAL" Title="School District Selection" SelectionLimit="1" OtherControl="SchoolDistrictOtherControl" OtherControlLabel="OtherSchoolDistrict"/></span></label>
            <asp:TextBox ID="SchoolDistrictControl" ReadOnly="true" runat="server"></asp:TextBox>
            
        </div>
        <div id="OtherSchoolDistrict">
            <label>Other:</label>
            <asp:TextBox ID="SchoolDistrictOtherControl" runat="server" />
        </div>
        
        <hr />
        <h2>Higher Education</h2>
        <div>
            <label class="full">
                <span class="tip" title="Be sure to update your student information to find all the scholarships that you can apply for.">
                    What type of college student are you currently?
                </span>
            </label>
            <sb:SeekerCollegeType id="SeekerStudentCollegeTypeControl" runat="server" />
        </div>
        <hr />
        <div>
            <label class="full">Which, if any, college are you currently attending?</label>
            <label style="margin-top:0px;"><span><sb:LookupDialog ID="CollegeControlDialogButton" runat="server" BuddyControl="CollegeControl" OtherControl="CollegeOtherControl" OtherControlLabel="OtherCurrentCollege" ItemSource="CollegeDAL" Title="College Selection" SelectionLimit="1" /></span></label>
            <asp:TextBox ID="CollegeControl" ReadOnly="true" runat="server"></asp:TextBox>
        </div>
        <div id="OtherCurrentCollege">
            <label>Other:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="CollegeOtherControl" runat="server" />
        </div>
        <div>
            <label>Academic program</label>
            <sb:EnumRadioButtonList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
        </div>
        <div>
            <label>Enrollment status</label>
            <sb:EnumRadioButtonList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
        </div>
        <div>
            <label>
                <span class="tip" title="Indicate whether you are in the first generation in your family to attend college in the US.">
                    First-generation student
                </span>
            </label>
            <asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
        </div>
        <hr />
        <div>
            <label class="full">
                <span class="tip" title="Please indicate any colleges that you are considering attending to ensure you are matched with scholarships that are for use at those schools.">
                    What types of schools are you considering?
                </span>
            </label>
            <sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
        </div>
        <div>
            <label class="full">Where would you like to attend college?</label>
            <div>
                <asp:CheckBox ID="InWashingtonCheckbox" Text="In Washington" runat="server" /><br />
                <asp:CheckBox ID="OutOfStateCheckbox" Text="Out of State" runat="server" />
            </div>
        </div>        
        <div>
            <label class="full">Which colleges are you considering?</label>
            <label style="margin-top:0px"><span><sb:LookupDialog ID="CollegesAppliedControlDialogButton" runat="server" BuddyControl="CollegesAppliedLabelControl" OtherControl="CollegesAppliedOtherControl" OtherControlLabel="CollegesConsidered" ItemSource="CollegeDAL" Title="College Selection"/></span></label>
            <asp:TextBox ID="CollegesAppliedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
        <div id="CollegesConsidered">
            <label>Other:</label>
            <asp:TextBox CssClass="othercontroltextarea" ID="CollegesAppliedOtherControl" runat="server" />
        </div>
    </div>
    <div class="question_list">
        <h2>Academic Performance</h2>
        <div>
            <label>GPA:</label>
            <sandTrap:NumberBox ID="GPAControl" CssClass="short" runat="server"  Precision="3"  MinAmount="0" MaxAmount="5"/>
            <elv:PropertyProxyValidator ID="GPAValidator" runat="server" ControlToValidate="GPAControl" PropertyName="GPA" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
        </div>
        <div>
            <label>Class Rank:</label>
            <sb:LookupItemRadioButtonList id="ClassRankControl" runat="server" RadioButtonListWidth="250px" MultiColumn="true" LookupServiceSpringContainerKey="ClassRankDAL" ></sb:LookupItemRadioButtonList>
            <asp:CustomValidator ID="ClassRankControlValidator" runat="server"  ErrorMessage="Please select class rank."    />
        </div>
        <hr />
        <div>
             <label>
                <span class="tip" title="Only individual SAT score for each category are used in scholarship matching. If you only know your cumulative score, please enter it but you are encouraged to find and enter the scores for each category.">
                    <b>SAT Score</b>
                </span>
             </label>
        </div>
        <div>
            <label>Writing</label>
            <sandTrap:NumberBox ID="SATWritingControl" CssClass="short"  runat="server" Precision="0" MinAmount="0" MaxAmount="800" />
            <elv:PropertyProxyValidator ID="SATWritingControlValidator" runat="server" ControlToValidate="SATWritingControl" PropertyName="Writing" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
            <asp:CustomValidator ID="SATWritingCustomValidator" runat="server" ControlToValidate="SATWritingControl" ErrorMessage="Value entered must be 200-800"   OnServerValidate="SATWritingCustomValidator_OnServerValidate" />
        </div>
        <div>
            <label>Critical Reading</label>
            <sandTrap:NumberBox ID="SATReadingControl" CssClass="short" runat="server" Precision="0"   MinAmount="0" MaxAmount="800"/>
            <elv:PropertyProxyValidator ID="SATReadingValidator" runat="server" ControlToValidate="SATReadingControl" PropertyName="CriticalReading" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
             <asp:CustomValidator ID="SATReadingCustomValidator" runat="server" ControlToValidate="SATReadingControl" ErrorMessage="Value entered must be 200-800"   OnServerValidate="SATReadingCustomValidator_OnServerValidate" />
        </div>
        <div>
            <label>Mathematics</label>
            <sandTrap:NumberBox ID="SATMathControl" CssClass="short" runat="server" Precision="0"   MinAmount="0" MaxAmount="800" />
            <elv:PropertyProxyValidator ID="SATMathValidator" runat="server" ControlToValidate="SATMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
            <asp:CustomValidator ID="SATMathCustomValidator" runat="server" ControlToValidate="SATMathControl" ErrorMessage="Value entered must be 200-800"   OnServerValidate="SATMathCustomValidator_OnServerValidate" />
        </div>
        <div>
            <label>
                <span class="tip" title="Enter cumulative amount, or enter scores for each category and the cumulative total will be calculated.">
                    Cumulative
                </span>
            </label>
            <sandTrap:NumberBox ID="SATScoreCommulativeControl" CssClass="short"  runat="server" Precision="0"  MinAmount="600" MaxAmount="2400"/>
            <elv:PropertyProxyValidator ID="PropertyProxyValidator1" runat="server" ControlToValidate="SATScoreCommulativeControl" PropertyName="Commulative" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
            <asp:CustomValidator ID="SATCommulativeCustomValidator" runat="server" ControlToValidate="SATScoreCommulativeControl" ErrorMessage="Value entered must be 600-2400"   OnServerValidate="SATCommulativeCustomValidator_OnServerValidate" />
            <asp:HiddenField ID="hiddenSATCommulative" runat="server" />
        </div>
        <hr />
        <div>
            <label>
                <span class="tip" title="Only individual ACT score for each category are used in scholarship matching. If you only know your cumulative score, please enter it but you are encouraged to find and enter the scores for each category.">
                    <b>ACT Score</b>
                </span>
            </label>
        </div>
        <div>
            <label>English</label>
            <sandTrap:NumberBox ID="ACTEnglishControl" CssClass="short" runat="server" Precision="0" MinAmount="0" MaxAmount="36" />
            <elv:PropertyProxyValidator ID="ACTEnglishValidator" runat="server" ControlToValidate="ACTEnglishControl" PropertyName="English" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
            <asp:CustomValidator ID="ACTEnglishCustomValidator" runat="server" ControlToValidate="ACTEnglishControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTEnglishCustomValidator_OnServerValidate" />
        </div>
        <div>
            <label>Reading</label>
            <sandTrap:NumberBox ID="ACTReadingControl" CssClass="short" runat="server" Precision="0"   MinAmount="0" MaxAmount="36"/>
            <elv:PropertyProxyValidator ID="ACTReadingValidator" runat="server" ControlToValidate="ACTReadingControl" PropertyName="Reading" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
            <asp:CustomValidator ID="ACTReadingCustomValidator" runat="server" ControlToValidate="ACTReadingControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTReadingCustomValidator_OnServerValidate" />
        </div>
        <div>
            <label>Mathematics</label>
            <sandTrap:NumberBox ID="ACTMathControl" CssClass="short" runat="server" Precision="0"  MinAmount="0" MaxAmount="36"/>
            <elv:PropertyProxyValidator ID="ACTMathValidator" runat="server" ControlToValidate="ACTMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
             <asp:CustomValidator ID="ACTMathCustomValidator" runat="server" ControlToValidate="ACTMathControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTMathCustomValidator_OnServerValidate" />
        </div>
        <div>
            <label>Science</label>
            <sandTrap:NumberBox ID="ACTScienceControl" CssClass="short" runat="server" Precision="0"  MinAmount="0" MaxAmount="36"/>
            <elv:PropertyProxyValidator ID="ACTScienceValidator" runat="server" ControlToValidate="ACTScienceControl" PropertyName="Science" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
             <asp:CustomValidator ID="ACTScienceCustomValidator" runat="server" ControlToValidate="ACTScienceControl" ErrorMessage="Value entered must be 0-36."   OnServerValidate="ACTScienceCustomValidator_OnServerValidate" />
        </div>
        <div>
            <label>
                <span class="tip" title="Enter cumulative amount, or enter scores for each category and the cumulative total will be calculated.">
                    Cumulative
                </span>
            </label>
            <sandTrap:NumberBox ID="ACTScoreCommulativeControl" CssClass="short" runat="server" Precision="0"   MinAmount="0" MaxAmount="144"/>
            <elv:PropertyProxyValidator ID="ACTScoreCommulativeValidator" runat="server" ControlToValidate="ACTScoreCommulativeControl" PropertyName="Commulative" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
            <asp:CustomValidator ID="ACTCommulativeCustomValidator" runat="server" ControlToValidate="ACTScoreCommulativeControl" ErrorMessage="Value entered must be 0-144."   OnServerValidate="ACTCommulativeCustomValidator_OnServerValidate" />
            <asp:HiddenField ID="hiddenACTCommulative" runat="server" />
        </div>
        <hr />
        <div>
            <label>
                <span class="tip" title="Have you received honors awards, participated in honors classes or programs? Please provide brief details.">
                    Honors
                </span>
            </label>
            <div>
                <asp:CheckBox ID="HonorsCheckboxControl" Text="" runat="server" /><br />
                <asp:TextBox TextMode="MultiLine" ID="HonorsTextControl" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="honorsTextValidator" runat="server" ControlToValidate="HonorsTextControl" PropertyName="HonorsDetail" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
            </div>
        </div>
        <div>
            <label>
                <span class="tip" title="Have you taken advance placement courses or earned advanced placement credits? Please provide brief details.">
                    AP Credits
                </span>
            </label>
            <div>
                <asp:CheckBox ID="APCreditsCheckBoxControl" Text="" runat="server" /><br />
                <asp:TextBox TextMode="MultiLine" ID="APCreditsTextControl" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="APCreditsTextValidator" runat="server" ControlToValidate="APCreditsTextControl" PropertyName="APCreditsDetail" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
            </div>
        </div>
        <div>
        </div>
        <div>
            <label>
                <span class="tip" title="Have you participated in an International Baccalaureate program? Please provide brief details.">
                    IB Credits
                </span>
             </label>
             <div>
                <asp:CheckBox ID="IBCreditsCheckBoxControl" Text="" runat="server"  /><br />
                <asp:TextBox TextMode="MultiLine" ID="IBCreditsTextBoxControl" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="IBCreditsTextValidator" runat="server" ControlToValidate="IBCreditsTextBoxControl" PropertyName="IBCreditsDetail" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
             </div>
        </div>
        <div>
            <label>
               <span class="tip" title="Have you participated in the Running Start program? Please provide brief details.">
                   Running Start
               </span>
            </label>
            <div>
                <asp:CheckBox ID="RunningStartCheckBoxControl" Text="" runat="server"/><br />
                <asp:TextBox TextMode="MultiLine" ID="RunningStartTextBoxControl" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="RunningStartTextValidator" runat="server" ControlToValidate="RunningStartTextBoxControl" PropertyName="RunningStartDetail" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
            </div>
        </div>
    </div>
</div>
