﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteria.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.AdditionalCriteria" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet" TagPrefix="elv" %>
<%@ Register src="EditQuestionAnswers.ascx" tagname="EditQuestionAnswers" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="ListOfAdditionalRequirements.ascx" tagname="ListOfAdditionalRequirements" tagprefix="sb" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register TagPrefix="sb" TagName="Tip" Src="~/Common/Tip.ascx" %>  


<asp:panel CssClass="section" runat="server" ID="RequirementsforApplication">
    <h2>Additional Requirements for this Application</h2>
    <sb:ListOfAdditionalRequirements ID="AdditionalRequirementsList" runat="server" />
</asp:panel>

<asp:panel CssClass="question_list section" runat="server" ID="InstructionsandQuestionsPanel">
    <h2>Additional Instructions and Questions</h2>
    <sb:EditQuestionAnswers ID="QAEditor" runat="server" />
</asp:panel>

<asp:panel CssClass="section" ID="scholarshipFilesPanel" runat="server">
    <h2>Scholarship Documents</h2>
    <asp:ListView ID="scholarshipFiles" runat="server" OnItemCommand="scholarshipFiles_ItemCommand">
        <LayoutTemplate >
            <table class="sortableTable">
                <thead>
                    <tr>
                        <th style="width:200px;">File</th>
                        <th style="width:70px;">Size</th>
                        <th>Instructions</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        
        <ItemTemplate>
            <tr class="row">
                <td>
                    <asp:LinkButton ID="deleteAttachmentBtn" CssClass="ListButton"  runat="server" Text='<%# Eval("Name") %>' CommandName="Download" CommandArgument='<%# Eval("Id")%>' />
                </td>
                <td>
                    <%# Eval("DisplaySize") %>
                </td>
                <td style="line-height:normal;">
                    <%# Eval("Comment") %>
                </td>
            </tr>
        </ItemTemplate>
         
        <EmptyDataTemplate>
        </EmptyDataTemplate>
    </asp:ListView>
</asp:panel>

<div CssClass="section">
    <h2>Attachments (Transcripts, Letters of Recommendation, Award Letters, etc.)</h2>
    <p>These documents may be attached to any scholarship application that you fill out.</p>
    
    <div class="question_list">
        <div>
            <label>Select File: </label>
            <asp:FileUpload ID="AttachFile" runat="server" />
            <asp:Label ID="fileTooBigLbl" runat="server" Visible="false" CssClass="error" Text="The file you tried to upload is too large. It should be less than 10MB." />
        </div>
        <div>
            <label>
                <span class="tip" title="Add a descriptive comment with the file being uploaded.">
                    File Description
                </span>
            </label>
            <asp:TextBox ID="AttachmentComments" runat="server" />    
            <elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments" PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment"/>
        </div>
        <div>
            <label></label>
            <sbCommon:AnchorButton ID="UploadFile" runat="server" Text="Attach File" onclick="UploadFile_Click" />
        </div>
    </div>

    <asp:ListView ID="attachedFiles" runat="server" OnItemDeleting="attachedFiles_OnItemDeleting">
        <LayoutTemplate>
        <table class="sortableTable">
            <thead>
                <tr>
                    <th style="width:50px;">&nbsp;</th>
                    <th style="width:200px;">File</th>
                    <th style="width:70px;">Size</th>
                    <th>Comments</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:LinkButton ID="deleteAttachmentBtn"   runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("Id")%>' /></td>
                <td>
                    <asp:LinkButton ID="downloadButton" CommandName="Download" CausesValidation="false" CommandArgument='<%# Eval("Id") %>' runat="server" Text='<%# Bind("Name") %>' ToolTip='<%# Eval("Comment") %>'>
                    </asp:LinkButton>            
                </td>
                <td><%# Eval("DisplaySize") %></td>
                <td><%# Eval("Comment") %></td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate></EmptyDataTemplate>
    </asp:ListView>
</div>
