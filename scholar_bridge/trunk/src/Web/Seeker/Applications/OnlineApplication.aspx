﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="OnlineApplication.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Applications.OnlineApplication" Title="Seeker | Application | Online Application" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<div class="subsection">
<p class="form-section-title-small">This Scholarship provider requires that you apply for their scholarship on their website. Click "OK" and you will be directed to this Scholarship Providers online scholarship application. Please ensure that your browser's popup blocker allows popups from theWashBoard.org.</p>
 
<br />
<sbCommon:AnchorButton ID="saveBtn" runat="server" Text="OK" onclick="saveBtn_Click" CausesValidation="false"   /> 
<sbCommon:AnchorButton ID="cancelBtn" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelBtn_Click" />    
</div>
</asp:Content>
