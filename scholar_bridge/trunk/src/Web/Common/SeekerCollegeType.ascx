﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerCollegeType.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerCollegeType" %>
<div>
    <asp:RadioButton Text="Undergraduate" runat="server" ID="Undergraduate" GroupName="studentGroup"/><br />
    <asp:RadioButton Text="Graduate" runat="server" ID="Graduate" GroupName="studentGroup"/><br />
    <asp:RadioButton Text="Transfer" runat="server" ID="Transfer" GroupName="studentGroup"/><br />
    <asp:RadioButton Text="Adult, First Time" runat="server" ID="AdultFirstTime" GroupName="studentGroup" /><br />
    <asp:RadioButton Text="Adult, Returning" runat="server" ID="AdultReturning" GroupName="studentGroup"/><br />
    <asp:RadioButton Text="I am currently not in college" runat="server" ID="RadioButton1" GroupName="studentGroup"/>  
</div>
