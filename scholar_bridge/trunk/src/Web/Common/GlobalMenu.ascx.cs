﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    public partial class GlobalMenu : UserControl
    {
        public IUserContext UserContext { get; set; }
        public string Referrer
        {
            get { return Request.ServerVariables["HTTP_REFERER"] ?? String.Empty; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (UserContext.CurrentUser == null)
            {
                SetMenuForAnonymousUser();
            }
        }
        private void SetMenuForAnonymousUser()
        {
			if (Referrer.Contains("/Seeker/") || Referrer.Contains("/Provider/") || Referrer.Contains("/Intermediary/"))
            {
				AnonymousPanel.Visible = true;
            }
        }
    }
}

