﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>

<%@ Import Namespace="ScholarBridge.Domain.Auth"%>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<asp:ObjectDataSource ID="MatchDataSource" runat="server" 
    SelectMethod="GetMatchesForSeekerWithoutApplications" 
    TypeName="ScholarBridge.Web.Common.MatchList"    
    SortParameterName="sortExpression"  
    SelectCountMethod="CountMatchesForSeekerWithoutApplications"  EnablePaging="true"  MaximumRowsParameterName="maximumRows">
</asp:ObjectDataSource>
<style type="text/css">
    #FilterArea{text-align:center;}
    #FilterArea span{margin:0px 13px; border-bottom:0px; cursor:pointer;}
    
    #FilterArea span:hover input:first-child, 
    #FilterArea span.active input:first-child, 
    #FilterArea span input:first-child + input{display:inline;}
    
    #FilterArea span:hover input:first-child + input, 
    #FilterArea span.active input:first-child + input, 
    #FilterArea span input{display:none;}

    table.MatchList td, th{padding:2px 3px; margin:0px; border-bottom:1px solid #efefef;}
    table.MatchList thead{background-color:#eee;}
    table.MatchList tr{display:table-row; border-bottom:1px solid #eee;}
    table.MatchList th a{color:Black;}
    table.MatchList .button{font-size:11px; font-weight:normal;}
	span.new {color:blue; font-weight:bold; font-style:italic;}
</style>
<div id="FilterArea">
    <span runat="server" id="PerfectMatches" class="tip" title="Display only scholarships where your profile meets all the minimum scholarship eligibility requirements.">
        <asp:ImageButton runat="server" ImageUrl="/images/icon/perfect-matches-active.png" OnClick="hundredPercentButton_Click" />
        <asp:ImageButton runat="server" ImageUrl="/images/icon/perfect-matches-inactive.png" OnClick="hundredPercentButton_Click" />
    </span>
    <span runat="server" id="ScholarshipsofInterest" class="tip" title="Display only the scholarships that you have selected or starred as a Scholarship of Interest. These are saved as your personal My Scholarships of Interest.">
        <asp:ImageButton runat="server" ImageUrl="/images/icon/scholarships-of-interest-active.png" OnClick="scholarshipsOfInterestButton_Click" />
        <asp:ImageButton runat="server" ImageUrl="/images/icon/scholarships-of-interest-inactive.png" OnClick="scholarshipsOfInterestButton_Click" />
    </span>
    <span runat="server" id="CollegeMatches" class="tip" title="Display only scholarships for the specific Colleges that I have listed in My Profile in the 'Colleges I am considering' field.">
        <asp:ImageButton runat="server" ImageUrl="/images/icon/college-matches-active.png" OnClick="collegeMatchButton_Click" />
        <asp:ImageButton runat="server" ImageUrl="/images/icon/college-matches-inactive.png" OnClick="collegeMatchButton_Click" />
    </span>
    <span runat="server" id="NewMatches" class="tip" title="Display only scholarships that are new to 'My Matches'. These have been added since you last logged in, or last updated your profile.">
        <asp:ImageButton runat="server" ImageUrl="/images/icon/new-matches-active.png" OnClick="newMatchesButton_Click" />
        <asp:ImageButton runat="server" ImageUrl="/images/icon/new-matches-inactive.png" OnClick="newMatchesButton_Click" />
    </span>
    <span runat="server" id="ActiveScholarships" class="tip" title="Display only scholarships that are currently accepting applications.">
        <asp:ImageButton runat="server" ImageUrl="/images/icon/active-scholarships-active.png" OnClick="activeOnlyButton_Click" />
        <asp:ImageButton runat="server" ImageUrl="/images/icon/active-scholarships-inactive.png" OnClick="activeOnlyButton_Click" />
    </span>
    <span runat="server" id="AllMatches" class="tip" title="Display all scholarships that match, at minimum, a single eligibility requirement with your profile. You may not qualify for these scholarships but this will give you a broader view of the scholarships available.">
        <asp:ImageButton runat="server" ImageUrl="/images/icon/all-matches-active.png" OnClick="allMatchesButton_Click" />
        <asp:ImageButton runat="server" ImageUrl="/images/icon/all-matches-inactive.png" OnClick="allMatchesButton_Click" />
    </span>
</div>

<asp:hiddenfield runat="server" ID="matchCriteriaIndex" Value='<%= CRITERIA_MATCH_100_PERCENT %>' />

<sbCommon:SortableListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    DataKeyNames="Id" DataSourceID="MatchDataSource" 
    SortExpressionDefault="Rank"
	SortDirectionDefault="Descending"   >
    <LayoutTemplate>
    <table class="MatchList" style="margin-top:20px;">
            <thead>
            <tr>
                <th></th>
                <th style="width:250px;"><sbCommon:SortableListViewColumnHeader Key="ScholarshipName" Text="Scholarship Name"  ID="SortableListViewColumnHeader1" runat="server"  /></th>
                <th style="width:50px" class="tip" title="Matches are ranked based on your profile and the number of minimum eligibility and preferred criteria you meet. Minimum requirements make up 2/3 of the rank, while preferences count for 1/3.">
                    <sbCommon:SortableListViewColumnHeader Key="Rank" Text="Rank"  ID="SortableListViewColumnHeader2" runat="server"  />
                </th>
                <th class="tip" title="The number of minimum eligibility requirements that match your profile and are defined in the Scholarship.">
                    <sbCommon:SortableListViewColumnHeader Key="EligibilityCriteria" Text="Eligibility Criteria"  ID="SortableListViewColumnHeader3" runat="server"  />
                </th>
                <th class="tip" title="The number of preferred criteria that match your profile and are defined in the scholarship.">
                  <sbCommon:SortableListViewColumnHeader Key="PreferredCriteria" Text="Preferred Criteria"  ID="SortableListViewColumnHeader4" runat="server"  />
               </th>
                <th class="tip" title="The date the scholarship application is due.">
                    <sbCommon:SortableListViewColumnHeader Key="ApplicationDueDate" Text="Application Due Date"  ID="SortableListViewColumnHeader5" runat="server"  />
                </th>
                <th class="tip" title="The anticipated number of scholarship awards available.">
                    <sbCommon:SortableListViewColumnHeader Key="NumberOfAwards" Text="# of Awards"  ID="SortableListViewColumnHeader6" runat="server"  />
                </th>
                <th class="tip" title="The anticipated amount of the scholarship award.">
                    <sbCommon:SortableListViewColumnHeader Key="ScholarshipAmount" Text="Scholarship Amount"  ID="SortableListViewColumnHeader7" runat="server"  />
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    <div class="pager">
    <asp:DataPager runat="server" ID="pager"   OnPreRender="pager_PreRender"  PageSize="20">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField  VisiblePageNumberCount="25"
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
    </div> 
    </LayoutTemplate>
          
    <ItemTemplate>
        <tr class="row">
            <td><asp:ImageButton id="IconButton" runat="server" CommandName="iconClicked"  ImageUrl="~/images/tag.png"/></td>
            <td><asp:LinkButton id="linktoScholarship" runat="server" >
                    <%# ((ScholarBridge.Domain.Match)Container.DataItem).CreateDate != null ? (((ScholarBridge.Domain.Match)Container.DataItem).CreateDate.Value  > DateTime.Now.AddHours(-24) ? "<span class='new'>New!</span> " : "") : ""%>
                    <%# Eval("ScholarshipName")%>
                </asp:LinkButton>
            </td>
            <td><%# Eval("Rank", "{0:##}%")%></td>
            <td><%# Eval("MinumumCriteriaString")%></td>
            <td><%# Eval("PreferredCriteriaString")%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
            <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="button" Width="70px" /></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:ImageButton  id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png" /></td>
            <td><asp:LinkButton id="linktoScholarship" runat="server" >
                    <%# ((ScholarBridge.Domain.Match)Container.DataItem).CreateDate != null ? (((ScholarBridge.Domain.Match)Container.DataItem).CreateDate.Value > DateTime.Now.AddHours(-24) ? "<span class='new'>New!</span> " : "") : ""%>
                    <%# Eval("ScholarshipName")%>
                </asp:LinkButton>
            </td>
            <td><%# Eval("Rank", "{0:##}%")%></td>
            <td><%# Eval("MinumumCriteriaString")%></td>
            <td><%# Eval("PreferredCriteriaString")%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
            <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
            <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
            <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="button" Width="70px" /></td>
        </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>The system could not find any scholarship matches for you. If you haven't already, please complete your profile; otherwise, try clearing out any filters you've placed by clicking on "All Matches" above.</p>
    </EmptyDataTemplate>
</sbCommon:SortableListView>

