﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ScholarshipList : UserControl
    {



        private IList<Scholarship> _Scholarships;
        public string EditActionLink { get; set; }
        public string ViewActionLink { get; set; }


        public IList<Scholarship> Scholarships
        {
            get { return _Scholarships; }
            set
            {
                _Scholarships = value;
                Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void Bind()
        {
            if (!(Scholarships == null))
            {
                lstScholarships.DataSource = Scholarships;// (from s in Scholarships orderby s.Stage, s.Name select s).ToList();
                lstScholarships.DataBind();
            }
        }

        protected void lstScholarships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var scholarship = ((Scholarship)((ListViewDataItem)e.Item).DataItem);
                var literal = (PlaceHolder)e.Item.FindControl("literalApplicants");

                if (scholarship.IsActivated())
                {
                    var link = new HyperLink();
                    link.Text = scholarship.ApplicantsCount.ToString();
                    link.Visible = true;
                    if (scholarship.ApplicantsCount > 0)
                    {
                        link.NavigateUrl = EditActionLink + "?resumefrom=6&id=" + scholarship.Id.ToString();
                        literal.Controls.Add(link);
                    }
                    else
                    {
                        literal.Controls.Add(new Label() { Text = scholarship.ApplicantsCount.ToString() });
                    }


                }
                else
                {
                    literal.Controls.Add(new Label() { Text = scholarship.ApplicantsCount.ToString() });
                }
            }
        }



        protected void lstScholarships_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pager.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            Bind();
        }
        protected void pager_PreRender(object sender, EventArgs e)
        {
            if (pager.TotalRowCount <= pager.PageSize)
            {
                pager.Visible = false;
            }
        }

        protected void createScholarshipLnk_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Provider/BuildScholarship");
        }
    }
}