﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImpersonateUser.ascx.cs" Inherits="ScholarBridge.Web.Common.ImpersonateUser" %>
<p>
	Email address of user to impersonate:
	<asp:TextBox id="userToFind" runat="server"></asp:TextBox>
	<asp:Button id="searchButton" runat="server" Text="Search" onclick="SearchButton_Click" CausesValidation="false"></asp:Button>
</p>
<asp:DataList Runat="server" 
                OnItemCommand="PreviousUsersList_ItemCommand"
                ID="previousUsersList" 
                Visible="False">
	<ItemTemplate>
		<asp:Label Runat="server" ID="Username" Text='<%# Eval("Username") %>' Visible="false">
		</asp:Label>
		<asp:LinkButton ID="LinkButton1" Runat="server">
		<%# Eval("Name") + " - " %>
		<%# Eval("Username") %>
		</asp:LinkButton>
	</ItemTemplate>
</asp:DataList>
<p>
	<asp:ListBox id="listBox1" runat="server" Visible="False"></asp:ListBox>
	<asp:Button id="impersonateButton" runat="server" Text="Impersonate" Visible="False" onclick="ImpersonateButton_Click"></asp:Button>
	<asp:RequiredFieldValidator ID="vldListBox1" runat="server"  ControlToValidate="listBox1" ErrorMessage="Please select a user"></asp:RequiredFieldValidator>
</p>