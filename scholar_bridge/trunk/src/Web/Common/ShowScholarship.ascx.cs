﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ShowScholarship : UserControl
    {
        public string LinkTo { get; set; }
        public string ApplicationLinkTo { get; set; }
        public Scholarship Scholarship { get; set; }
        public string LinkToEditProgramGuideLines { get; set; }
        public string LinkToEditSchedule { get; set; }
        public string LinkToEditFinancialNeed { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            GeneralInfoShow1.ScholarshipToView = Scholarship;
            GeneralInfoShow1.LinkTo = LinkTo;
            GeneralInfoShow1.LinkToEditProgramGuidelines = LinkToEditProgramGuideLines;
            GeneralInfoShow1.LinkToEditSchedule = LinkToEditSchedule;
            SeekerInfoShow1.ScholarshipToView = Scholarship;
            SeekerInfoShow1.LinkTo = LinkTo;

			scholarshipName.Text = Scholarship.Name;
            FundingInfoShow1.ScholarshipToView = Scholarship;
            FundingInfoShow1.LinkTo = LinkTo;
            FundingInfoShow1.LinkToEditFinancialNeed = LinkToEditFinancialNeed;

            AdditionalCriteriaShow1.ScholarshipToView = Scholarship;
            AdditionalCriteriaShow1.LinkTo = LinkTo;

        }
    }
}