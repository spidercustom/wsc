﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Common
{
	public partial class ContactUs : UserControl
	{
		public IMessageService MessageService { get; set; }
		public UserContext UserContext { get; set; }

		public delegate void ContactUsMessageSent();
		public delegate void ContactUsMessageCanceled();

        // XXX: Do we need these events?
		public event ContactUsMessageSent FormSent;
		public event ContactUsMessageCanceled FormCanceled;

		public enum ContactUsMessageType
		{
			Support,
			Problem,
			Suggestion
		}

		public ContactUsMessageType MessageType
		{
			get
			{
				if (ViewState["MessageType"] == null)
					return ContactUsMessageType.Support;

				return (ContactUsMessageType) ViewState["MessageType"];
			}
			set
			{
				ViewState["MessageType"] = value;
			}
		}

		public bool ResetMessage
		{
			get; set;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				sendButton.Text = "Send";
				cancelButton.Text = "Cancel";
				// if the user isn't logged in, then require their email address
				if (Page.User.Identity.IsAuthenticated)
				{
					fromBox.Visible = false;
					fromLabel.Visible = true;
					fromLabel.Text = Page.User.Identity.Name;
					fromBoxValidator.Enabled = false;
					emailValidator.Enabled = false;
				}
				else
				{
					fromBox.Visible = true;
					fromLabel.Visible = false;
				}
			}
		}
		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (ResetMessage)
				comments.Text = string.Empty;

			switch (MessageType)
			{
				case ContactUsMessageType.Support:
					toLabel.Text = "theWashBoard.org - SupportTeam";
					subject.Text = "Support/Help";
					titleLabel.Text = "Get some help using theWashBoard.org";
					break;
				case ContactUsMessageType.Problem:
					toLabel.Text = "theWashBoard.org - Administrator";
					subject.Text = "Problem or Issue";
					titleLabel.Text = "Report a problem or issue with theWashBoard.org";
					break;
				case ContactUsMessageType.Suggestion:
					toLabel.Text = "theWashBoard.org - Development Group";
					subject.Text = "Comments and Suggestions";
					titleLabel.Text = "Send your suggestions or comments to theWashBoard.org";
					break;
			}
		}

        protected void Comments_OnServerValidate(object source, ServerValidateEventArgs args)
        {
            if (comments.Text.Length > 1000)
            {
                CommentsCustomValidator.IsValid = false;
                args.IsValid = false;
            }
        }

		protected void sendButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
				return;

			string emailAddress = Page.User.Identity.IsAuthenticated ? Page.User.Identity.Name : fromBox.Text;

			switch (MessageType)
			{
				case ContactUsMessageType.Problem:
					MessageService.ContactUsProblem(emailAddress, UserContext.CurrentUser, comments.Text);
					break;
				case ContactUsMessageType.Suggestion:
					MessageService.ContactUsSuggestion(emailAddress, UserContext.CurrentUser, comments.Text);
					break;
				case ContactUsMessageType.Support:
					MessageService.ContactUsSupport(emailAddress, UserContext.CurrentUser, comments.Text);
					break;
			}
			SuccessMessageLabel.SetMessage("Thank you.  Your message has been sent successfully.");

			if (null != FormSent)
			{
				FormSent();
			}
		}

		protected void cancelButton_Click(object sender, EventArgs e)
		{
			if (null != FormCanceled)
			{
				FormCanceled();
			}
		}
	}
}