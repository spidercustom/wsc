﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Web.Extensions;
using Spring.Context.Support;

namespace ScholarBridge.Web.Common
{
    public partial class SentMessageList : UserControl
    {
        
        private const string DEFAULT_SORTEXPRESSION = "Date DESC";

        public string LinkTo { get; set; }

        public IMessageService MessageService
        {
            get { return (IMessageService)ContextRegistry.GetContext().GetObject("MessageService"); }
        }
        public IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }

        public IUserService UserService
        {
            get { return (IUserService)ContextRegistry.GetContext().GetObject("UserService"); }
        }

        public IList<Domain.Messaging.SentMessage> GetSentMessages(string sortExpression, int startRowIndex, int maximumRows)
        {
            if (string.IsNullOrEmpty(sortExpression))
                sortExpression = DEFAULT_SORTEXPRESSION;
            return MessageService.FindAllSent(startRowIndex, maximumRows, sortExpression, UserContext.CurrentUser, UserContext.CurrentOrganization);
        }
        public int CountSentMessages(  )
        {
             
            return MessageService.CountAllSent(UserContext.CurrentUser, UserContext.CurrentOrganization);
        }
        protected void messageList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var message = (Domain.Messaging.SentMessage)((ListViewDataItem)e.Item).DataItem;
                var date = (Label)e.Item.FindControl("lblDate");
                date.Text = message.Date.ToLocalTime().ToShortDateString();
 
               var link = (LinkButton)e.Item.FindControl("linkToMessage");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
                + "?sent=true&popup=true&id=" + (message.Id.ToString());
                link.Attributes.Add("onclick", String.Format("messageView('{0}'); return false;", url));
                link.Attributes.Add("class", "GreenLink");
            }
        }
    }
}