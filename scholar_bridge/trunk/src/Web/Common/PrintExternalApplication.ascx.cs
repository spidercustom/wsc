﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class PrintExternalApplication : UserControl
    {
        public Application  ApplicationToView{ get; set; }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Url.Text = ApplicationToView.Scholarship.OnlineApplicationUrl;

            PersonalInfoName.Text = ApplicationToView.Seeker.Name.NameFirstLast;
            PersonalInfoEmail.Text = ApplicationToView.Seeker.User.Email;
             if (null != ApplicationToView.Seeker.Address)
                 PersonalInfoAddress.Text = ApplicationToView.Seeker.Address.ToString().Replace("\r\n", "<br/>\r\n");
        }
    }
}