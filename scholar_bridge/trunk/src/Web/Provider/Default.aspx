﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Provider.Default" Title="Provider" %>
<%@ Register Src="~/Common/SeekerProfileProgress.ascx" TagName="SeekerProfileProgress" TagPrefix="sb" %>
<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Seeker Home" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="270" width="873" src="<%= ResolveUrl("~/images/header/provide_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <div style="float:right; width:300px; margin-left:20px;">
        <sb:SeekerProfileProgress ID="ProfileProgess" runat="server" ShowGotoProfile="false" />
    </div>
    <h2><img alt="Smarter Scholarship Matches" src="<%= ResolveUrl("~/images/titles/Smarter-Scholarship-Matches.gif") %>" width="332px" height="22px"/></h2>
    <p class="hookline">TheWashBoard.org makes scholarship searching simple. In one stop, students can search and apply for vetted scholarships specific to their academic interests, college or university, or other criteria.</p>
        
   
    <div class="clear"></div>
    <hr style="margin:20px 0px;" />
    
    <div class="three-column">
        <div>
            <a href="<%= ResolveUrl("~/Provider/BuildScholarship") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox01_BuildScholarships.gif") %>" width="262px" height="51px"/>
                <img src="<%= ResolveUrl("~/images/ProviderBottomBox01.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">
                We guide you through a simple step by step process to 
                define your scholarships unique criteria and application requirements.</p>
            <a class="circle-icon-link" href="<%= ResolveUrl("~/Provider/BuildScholarship") %>">Start  Building</a>
        </div>
        <div>
            <a href="<%= ResolveUrl("~/Provider/Scholarships") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox02_ManageApplications.gif") %>" width="262px" height="51px"/>
                <img src="<%= ResolveUrl("~/images/ProviderBottomBox02.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">
                We make reviewing and evaluating applications easy.&nbsp; 
                Review on line, or download applicant information. </p>
            <a class="circle-icon-link" href="<%= ResolveUrl("~/Provider/Scholarships") %>">Start Reviewing</a>
        </div>
        <div>
            <a href="<%= ResolveUrl("~/Provider/Scholarships") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox03_AwardScholarship.gif") %>" width="262px" height="51px"/>
                <img src="<%= ResolveUrl("~/images/ProviderBottomBox03.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">
                Designate finalists for further consideration.&nbsp; 
                Extend offers and notify applicants when scholarship is awarded.</p>
            <a class="circle-icon-link" href="<%= ResolveUrl("~/Provider/Scholarships") %>">View Applicants</a>
        </div>    
   </div>
</asp:Content>
