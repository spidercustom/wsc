﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Activate : WizardStepUserControlBase<Scholarship>
	{
		#region Properties

		bool InScholarshipValidation
		{
			get; set;
		}

		protected bool UserIsAdmin
    	{
    		get
    		{
    			return UserContext.CurrentUser.IsInRole(Role.INTERMEDIARY_ADMIN_ROLE) ||
    			       UserContext.CurrentUser.IsInRole(Role.PROVIDER_ADMIN_ROLE) ||
    			       UserContext.CurrentUser.IsInRole(Role.WSC_ADMIN_ROLE);
    		}
    	}
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
		}
		#endregion

		#region Page lifecycle events

		protected void Page_Load(object sender, EventArgs e)
        {
			SetPreviewURL();
			if (!UserIsAdmin)
    			confirmActivationBtn.Visible = false;

            if (!Page.IsPostBack)
            {
            	SetButtonsToSkip();
            }
		}

    	public void EvaluateScholarshipValidations()
    	{
    		if (!ValidSchoalarship())
    		{
    			scholarshipValidationErrors.Visible = true;
    			confirmActivationBtn.Enabled = false;
    			certifyAvailabilityDiv.Visible = false;
    		}
    		else
    		{
    			scholarshipValidationErrors.Visible = false;
    			confirmActivationBtn.Enabled = true;
				certifyAvailabilityDiv.Visible = true;
				if (!UserIsAdmin)
    			{
    				ShowAdministratorsDiv();
    				certifyAvailabilityDiv.Visible = false;
    			}
    		}
    	}

    	#endregion

		#region IWizardStepControl implementation
		public override void Save()
        {
            PopulateObjects();
            ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ScholarshipService.Save(ScholarshipInContext);
        }

    	public override void PopulateObjects()
    	{
    	}

    	public override bool ValidateStep()
        {
            var result = Validation.Validate(ScholarshipInContext);
            return result.IsValid;
		}

		public override void Activated()
		{
			if (!InScholarshipValidation)
			{
				InScholarshipValidation = true;
				base.Activated();
				EvaluateScholarshipValidations();
				InScholarshipValidation = false;
			}
		}
		#endregion

		#region Private Methods
		private void SetPreviewURL()
		{
			string windowOpenerClickString = string.Format("window.open('{0}?id={1}&print=true', null); return false",
						  UserContext.CurrentOrganization  ==(Organization) UserContext.CurrentProvider
							? LinkGenerator.GetFullLinkStatic("/Provider/Scholarships/PrintScholarship.aspx")
                            : LinkGenerator.GetFullLinkStatic("/Intermediary/Scholarships/PrintScholarship.aspx"),
						  ScholarshipInContext.Id);
			previewButton.OnClientClick = windowOpenerClickString;
		}

		private void SetButtonsToSkip()
		{
			if (Page is IChangeCheckingPage)
			{
				var ccPage = (IChangeCheckingPage)Page;
				ccPage.BypassPromptIds.AddRange(new[] { confirmActivationBtn.ID });
			}
		}

		/// <summary>
		/// If the user is not an admin, show a div that contains a message stating that an
		/// admin must perform the activate and show the admin name.
		/// </summary>
		private void ShowAdministratorsDiv()
		{
			Dictionary<string, string> adminDictionary = new Dictionary<string, string>
                                         	{
                                         		{
                                         			"Provider",
                                         			ScholarshipInContext.Provider.AdminUser.Name.NameFirstLast +
                                                    ", " + ScholarshipInContext.Provider.Name
                                         		}
                                         	};

			if (ScholarshipInContext.Intermediary != null)
			{
				adminDictionary.Add("Intermediary", 
										ScholarshipInContext.Intermediary.AdminUser.Name.NameFirstLast +
										", " + ScholarshipInContext.Intermediary.Name);
			}
			adminRepeater.DataSource = adminDictionary;
			adminRepeater.DataBind();
			showAdministratorsDiv.Visible = true;
		}

		private bool ValidSchoalarship()
		{
			// TODO: This can be better written if we can move all validation to Domain,
			// right now main, issue is how to better to move collection empty validation conditionally
			// that depends on options selected in "Selection Criteria" tab.
			var isValid = true;
			var stepIndex = 0;
			var failedValidations = new List<IValidator>();
			Container.Steps.ForEach(o =>
			{
				Container.ActiveStepIndex = stepIndex;
				Page.Validate();
				o.ValidateStep();
				isValid &= Page.IsValid;
				for (var validationIndex = 0; validationIndex < Page.Validators.Count; validationIndex++)
				{
					var validation = Page.Validators[validationIndex];
					if (!validation.IsValid)
						failedValidations.Add(validation);
				}
				stepIndex++;
			});
            

			IssueListControl.DataSource = failedValidations;
			IssueListControl.DataBind();
			return isValid;
		}

		#endregion

		#region Control Event Handlers
		protected void confirmActivationBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
			bool itemSelected = false;
			foreach (ListItem item in availabilityCheckBoxList.Items)
			{
				if (item.Selected)
				{
					itemSelected = true;
					break;
				}
			}
			if (!itemSelected)
			{
				availabilityCheckCustomValidator.IsValid = false;
				return;
			}
			if (e.DialogResult)
            {
                if (!ValidSchoalarship())
                    return;

                if (ScholarshipInContext.CanSubmitForActivation())
                {
					foreach (ListItem item in availabilityCheckBoxList.Items)
					{
							if (item.Value == "residents")
								ScholarshipInContext.IsAvailableToWAResidents = item.Selected;
							if (item.Value == "students")
								ScholarshipInContext.IsAvailableForWACollegeAttendees = item.Selected;
					}
                    ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    ScholarshipService.Activate(ScholarshipInContext, UserContext.CurrentUser);
                    SuccessMessageLabel.SetMessage("Scholarship activated successfully.");
                    Response.Redirect("~/Provider/Scholarships/Default.aspx");
                }
                else
                {
                    ClientSideDialogs.ShowAlert("<p>Scholarship is already activated</p>", "Cannot activate!");
                }
            }
        }

		#endregion
	}
}
