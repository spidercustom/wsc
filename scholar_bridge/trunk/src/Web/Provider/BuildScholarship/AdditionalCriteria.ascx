﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteria.ascx.cs"
    Inherits="ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
    TagPrefix="elv" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<h3>Build Scholarship – Additional Requirements</h3>
<p>
    Please indicate here what additional materials and information the applicant needs
    to provide. Each question can be 500 characters maximum.</p>

<div class="form-iceland-container">
    <div class="form-iceland">
        <p class="form-section-title">Additional Requirements with Scholarship Application</p>
        <div class="control-set">
            <asp:CheckBoxList ID="AdditionalRequirements" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow" />
        </div>
        <br />
        <hr />

        <p class="form-section-title">Scholarship Specific Instructions and Questions</p>
            <p>Use this section to provide additional instructions to scholarship applicants. Enter questions they need to answer, or provide clarifying information about scholarship eligibility and application procedures.</p>
    
        <table width="90%" class="control-set">
            <tr>
                <td>
                    <asp:GridView ID="questionsGrid" 
                        runat="server" 
                        datakeynames="DisplayOrder"
                        ShowFooter="true" 
                        AutoGenerateColumns="False"
                        DataSourceID="QuestionSource" >
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <th colspan="2">
                                        
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox Width="400" MaxLength="500"  Wrap="true" Rows="2" TextMode="MultiLine"
                                            ID="questionEmptyAddTextBox" runat="server"></asp:TextBox>
                                         <elv:PropertyProxyValidator ID="firstQuestionValidator" runat="server" ControlToValidate="questionEmptyAddTextBox"
                                            PropertyName="QuestionText" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipQuestion" />
                                    </td>
                                    <td>
                                     <asp:CheckBox Width="100" Text="Required?" ID="questionEmptyAddrequiredCheckBox" runat="server" Checked='<%# Bind("IsRequired") %>'></asp:CheckBox>
                            
                                    </td>
                                    <td>
                                        <sbCommon:AnchorButton CausesValidation="true" ID="addEmptyQuestionButton" runat="server" Text="Add" OnClick="addEmptyQuestionButton_Click" />
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <EditItemTemplate>
                                    <asp:Label ID="displayOrderEditLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="questionOrderHidden" Value='<%# Bind("DisplayOrder") %>' />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Literal ID="displayOrderLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>' /><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Questions">
                                <EditItemTemplate>
                                    <asp:TextBox Width="400" MaxLength="500"   Wrap="true" Rows="2" TextMode="MultiLine"
                                        ID="questionTextBox" runat="server" Text='<%# Bind("QuestionText") %>'></asp:TextBox>
                                     <elv:PropertyProxyValidator ID="updateQuestionValidator" runat="server" ControlToValidate="questionTextBox"
                                        PropertyName="QuestionText" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipQuestion" />
                                </EditItemTemplate>
                                
                                <ItemTemplate>
                                    <asp:Label ID="questionTextLabel"  width="300px" runat="server" Text='<%# Bind("QuestionText") %>' /><br />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <br />
                                    <asp:TextBox Width="400" MaxLength="500"   Wrap="true" Rows="2" TextMode="MultiLine"
                                        ID="questionAddTextBox" runat="server"></asp:TextBox>
                                     <elv:PropertyProxyValidator ID="newQuestionValidator" runat="server" ControlToValidate="questionAddTextBox"
                                        PropertyName="QuestionText" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.ScholarshipQuestion" />
                              
                               </FooterTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Required?">
                                <EditItemTemplate>
                                    <asp:CheckBox Width="100" Text="Required?" ID="requiredCheckBox" runat="server" Checked='<%# Bind("IsRequired") %>'></asp:CheckBox>
                                  </EditItemTemplate>
                                
                                <ItemTemplate>
                                      <asp:CheckBox Width="100" Text="Required?" ID="requiredCheckBoxReadOnly"  Enabled="false" runat="server" Checked='<%# Bind("IsRequired") %>'></asp:CheckBox>
                                 <br />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <br />
                                    <asp:CheckBox Width="100" Text="Required?" ID="questionAddrequiredCheckBox" runat="server" Checked='<%# Bind("IsRequired") %>'></asp:CheckBox>
                        
                               </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <sbCommon:AnchorButton ID="updateQuestionButton" runat="server" CausesValidation="True"
                                        CommandName="Update" Text="Update" CommandArgument='<%# Bind("DisplayOrder") %>'></sbCommon:AnchorButton>
                                    <sbCommon:AnchorButton ID="cancelQuestionUpdateButton" runat="server" CausesValidation="False"
                                        CommandName="Cancel" Text="Cancel"></sbCommon:AnchorButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <sbCommon:AnchorButton ID="editQuestionButton" runat="server" CausesValidation="False" CommandName="Edit"
                                        Text="Edit"></sbCommon:AnchorButton>
                                        <sbCommon:AnchorButton ID="deleteQuestionButton" runat="server" CausesValidation="False"
                                        CommandName="Delete" Text="Delete" CommandArgument='<%# Bind("QuestionText") %>'></sbCommon:AnchorButton>
                                  
                                </ItemTemplate>
                                <FooterTemplate>
                                    <sbCommon:AnchorButton ID="addQuestionButton" runat="server" Text="Add" OnClick="addQuestionButton_Click" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="QuestionSource" runat="server" 
                        SelectMethod="Select" UpdateMethod="Update" DeleteMethod="Delete" 
                        TypeName="ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria+QuestionDataProvider"
                        OnDeleted="QuestionSource_Deleted"
                        OnObjectCreating="QuestionSource_ObjectCreating" >
                        <deleteparameters>
                            <asp:parameter name="DisplayOrder" type="Int32" />
                        </deleteparameters>
   
                    </asp:ObjectDataSource>
                </td>
            </tr>
        </table>
        
        <br />
        <hr />

        <script type="text/javascript">
            $(document).ready(function() {

                var inlineProgressBar = NeatUploadPB.prototype.Bars['<%= AttachFileProgressBar.ClientID %>'];
                var origDisplay = inlineProgressBar.Display;
                inlineProgressBar.Display = function() {
                    var elem = document.getElementById(this.ClientID);
                    elem.parentNode.style.display = "block";
                    origDisplay.call(this);
                }
                inlineProgressBar.EvalOnClose = "NeatUploadMainWindow.document.getElementById('" + inlineProgressBar.ClientID + "').parentNode.style.display = \"none\";";
            });
        </script>

        <p class="form-section-title">Upload Scholarship Forms</p>
        
        <p>These forms will be available to scholarship seekers to download.</p>
        <div style="display: none;">
            <Upload:ProgressBar ID="AttachFileProgressBar" runat="server" Inline="true" />
        </div>
        
        <label for="AttachFile">Select File:</label>
         <Upload:InputFile ID="AttachFile" runat="server"    Size="46" />
         <span class ="FileUploadContainer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <sbCommon:AnchorButton ID="UploadFile" runat="server" Text="Attach File"  OnClick="UploadFile_Click"  />
         </span>
        <br />
        <p>Please enter instructions for applicant. This will accompany the form.</p>
        <label for="AttachmentComments">Instructions for applicant:</label>
        <asp:TextBox ID="AttachmentComments" runat="server" Columns="60"/>
        <elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments"
            PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment" />
               
        <br />
        <asp:Label ID="fileTooBigLbl" runat="server" Visible="false" CssClass="error" Text="The file you tried to upload is too large. It should be less than 10MB." />
        <br />
        
        <asp:ListView ID="attachedFiles" runat="server" OnItemDeleting="attachedFiles_OnItemDeleting" >
            
            <LayoutTemplate >
            
                <table class="sortableTable">
                    <thead>
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                File
                            </th>
                            <th>
                                Size
                            </th>
                            <th>
                                 Instructions For Applicants
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                    </tbody>
                </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr class="row">
                    <td>
                        <asp:LinkButton ID="deleteAttachmentBtn" CssClass="ListButton"  runat="server" Text="Delete" CommandName="Delete"
                            CommandArgument='<%# Eval("Id")%>' />
                    </td>
                    <td>
                        <%# Eval("Name") %>
                    </td>
                    <td>
                        <%# Eval("DisplaySize") %>
                    </td>
                    <td>
                        <%# Eval("Comment") %>
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
            <table class="sortableTable">
                    <thead>
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                File
                            </th>
                            <th>
                                Size
                            </th>
                            <th>
                                 Instructions For Applicants
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </EmptyDataTemplate>
        </asp:ListView>
        
        <br />
        <hr />
        <p class="form-section-title">Require Application Materials Mailed to Provider</p>
        <div class="control-set">
            <p>If you require applicants to mail in physical copies of their scholarship application and additional materials, check the box below. Applicants, when they click apply, will be shown a message that they are required to mail in their application and all materials to the scholarship Provider.</p>
            <span> <asp:CheckBox ID="UsePostalApplication" runat="server"  Text="" /> I require all scholarship application materials be mailed to require the contact address indicated on the scholarship. </span>
           
        </div>
        <br /><br />
        <p class="form-section-title">Use Provider On-line Application</p>
        <div class="control-set">
            <p>If you have an existing on-line application form, theWashboard.org can direct applicants to your site. When the applicant clicks <b>Apply</b>, the WashBoard.org will open a new browser window to your specified site and list the applicants name on the Applicants tab of your scholarship.</p>
            <span> <asp:CheckBox ID="UseOnlineApplication" runat="server"  Text="" /> I have an existing on-line scholarship application and request theWashBoard.org post a link to the following site: </span>
           
           <asp:TextBox ID="OnlineApplicationControl" runat="server"  style="width:520px !important;" ></asp:TextBox>
         <sb:CoolTipInfo ID="CoolTipInfoOnlineApplicationControl" runat="Server"  Content="All online application links are verified by theWashBoard.org system admin to ensure accuracy. You will receive a message as soon as your link is verified." />  
         <asp:CustomValidator ID="OnlineApplicationValidator"   runat="server"  />
            <br />
            <br />
          <sbCommon:AnchorButton ID="SubmitOnlineApplication" runat="server" Text="Submit" OnClick= "SubmitOnlineApplication_Click" CausesValidation="false" />
        </div>
        
    </div>
</div>