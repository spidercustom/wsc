﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{

    public class LookupDAL<T> : AbstractDAL<T>, ILookupDAL, IGenericLookupDAL<T> where T : ILookup
    {
        private static readonly SimpleExpression NOT_DEPRECATED = Restrictions.Eq("Deprecated", false);

        ILookup ILookupDAL.FindById(object id)
        {
            return FindById(id);
        }

        IList<ILookup> ILookupDAL.FindAll(IList<object> ids)
        {
            return new List<ILookup>(FindAll(ids).Cast<ILookup>());
        }

        IList<ILookup> ILookupDAL.FindAll()
        {
            return new List<ILookup>(FindAll(false).Cast<ILookup>());
        }

        public T FindById(object id)
        {
            return UniqueResult("Id",  id);
        }
        
		public IList<T> FindAll(bool includeDeprecated)
		{
			ICriteria criterion = 
				CreateCriteria()
					.AddOrder(Order.Asc("Name"))
					.SetCacheable(true)
					.SetCacheRegion("Lookup");

			if (!includeDeprecated)
				criterion = criterion.Add(NOT_DEPRECATED);

			return criterion.List<T>();
		}

        public IList<T> FindAll()
        {
        	return FindAll(false);
        }

        public IList<T> FindAll(IList<object> ids)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Id", ids.ToArray()))
                .List<T>();
        }

        public  virtual List<KeyValuePair<string, string>> GetLookupItems(string userData)
        {
            var list = FindAll();
            return list.Select(o => new KeyValuePair<string, string>(o.Id.ToString(), o.Name)).ToList();
        }
    }
}
