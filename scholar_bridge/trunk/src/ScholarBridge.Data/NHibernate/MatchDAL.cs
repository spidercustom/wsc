using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class MatchDAL : AbstractDAL<Match>, IMatchDAL
    {
        private static readonly SimpleExpression NOT_DELETED = Restrictions.Eq("IsDeleted", false);

        public IList<Match> FindAll(Application application)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))

                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus status)
        {
            return FindAll(seeker, new[] {status});
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.In("MatchStatus", status))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }

    	public IList<Match> FindAll(Scholarship scholarship, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.In("MatchStatus", status))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }
		public int CountAllCurrentMatches(Seeker seeker, bool excludeMatchesWithApplications, MatchFilterCriteria criteria)
		{
			var query = CreateCriteria()
				.Add(NOT_DELETED)
				.Add(Restrictions.Eq("Seeker", seeker))
				.Add(Restrictions.Eq("AddedViaMatch", true));
			if (excludeMatchesWithApplications)
				query.Add(Restrictions.IsNull("Application"));
			SetFilterCriteria(query, criteria);
			query.SetProjection(Projections.CountDistinct("Id"));
			return query.UniqueResult<int>();

		}

		public IList<Match> FindAllCurrentMatches(int startIndex, 
												int rowCount, 
												string sortExpression, 
												Seeker seeker, 
												bool excludeMatchesWithApplications, 
												MatchFilterCriteria criteria)
        {

			var query = CreateCriteria().SetFirstResult(startIndex).SetMaxResults(rowCount)
				.Add(NOT_DELETED)
				.Add(Restrictions.Eq("Seeker", seeker))
				.Add(Restrictions.Eq("AddedViaMatch", true));
			if (excludeMatchesWithApplications)
				query.Add(Restrictions.IsNull("Application"));
            SetFilterCriteria(query, criteria);
            query = CreateSort(sortExpression, query, criteria);


            return query.List<Match>();
        }

    	private static void SetFilterCriteria(ICriteria query, MatchFilterCriteria filterCriteria)
    	{
    		switch (filterCriteria)
    		{
    			case MatchFilterCriteria.Match100Percent:
    				query.Add(Restrictions.EqProperty("SeekerMinimumCriteriaCount", "ScholarshipMinimumCriteriaCount"));
    				break;
				case MatchFilterCriteria.ActiveScholarshipsOnly:
                    //See JIRA WSC-1448
    				break;
				case MatchFilterCriteria.NewSinceLastLogin:
    				query.Add(Restrictions.Ge("CreateDate", DateTime.Now.AddDays(-1)));
    				break;
				case MatchFilterCriteria.CollegeSelections:
					query.Add(Restrictions.Eq("CollegeMatchFlag", true));
					break;
				case MatchFilterCriteria.ScholarshipsOfInterest:
					query.Add(Restrictions.Eq("MatchStatus", MatchStatus.Saved));
					break;
				case MatchFilterCriteria.All:
					break;
				default:
					throw new ArgumentException("Unsupported MatchFilterCriteria: " + filterCriteria);
    		}
    	}

        private static ICriteria CreateSort(string sortExpression, ICriteria query, MatchFilterCriteria filterCriteria)
		{
			switch (sortExpression)
			{
				case "ScholarshipName":
				case "ScholarshipName ASC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly){
                        query.CreateCriteria("Scholarship").AddOrder(new Order("Name", true))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    } else {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("Name", true));
                    }

                    break;

				case "ScholarshipName DESC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("Name", false))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    }
                    else
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("Name", false));
                    }

                    break;

				case "Rank":
				case "Rank ASC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)  query.CreateCriteria("Scholarship").Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));

					query.AddOrder(new Order("Rank", true));
					break;

				case "Rank DESC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly) query.CreateCriteria("Scholarship").Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    query.AddOrder(new Order("Rank", false));
					break;

				case "EligibilityCriteria":
				case "EligibilityCriteria ASC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly) query.CreateCriteria("Scholarship").Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    query.AddOrder(new Order("MinimumCriteriaPercent", true));
					query.AddOrder(new Order("ScholarshipMinimumCriteriaCount", true));

					break;

				case "EligibilityCriteria DESC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly) query.CreateCriteria("Scholarship").Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    query.AddOrder(new Order("MinimumCriteriaPercent", false));
					query.AddOrder(new Order("ScholarshipMinimumCriteriaCount", false));
					break;

				case "PreferredCriteria":
				case "PreferredCriteria ASC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly) query.CreateCriteria("Scholarship").Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
					query.AddOrder(new Order("PreferredCriteriaPercent", true));
					query.AddOrder(new Order("ScholarshipPreferredCriteriaCount", true));


					break;

				case "PreferredCriteria DESC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly) query.CreateCriteria("Scholarship").Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    query.AddOrder(new Order("PreferredCriteriaPercent", false));
					query.AddOrder(new Order("ScholarshipPreferredCriteriaCount", false));
					break;
				case "ApplicationDueDate":
				case "ApplicationDueDate ASC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("ApplicationDueDate", true))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    }
                    else
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("ApplicationDueDate", true));
                    }
					break;

				case "ApplicationDueDate DESC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("ApplicationDueDate", false))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    }
                    else
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("ApplicationDueDate", false));
                    }
					break;

				case "NumberOfAwards":
				case "NumberOfAwards ASC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", true))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    }
                    else
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", true));
                    }
					break;

				case "NumberOfAwards DESC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", false))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    }
                    else
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", false));
                    }
                    break;

				case "ScholarshipAmount":
				case "ScholarshipAmount ASC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("MaximumAmount", true))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    }
                    else
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("MaximumAmount", true));
                    } 
					break;

				case "ScholarshipAmount DESC":
                    if (filterCriteria == MatchFilterCriteria.ActiveScholarshipsOnly)
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("MaximumAmount", false))
                             .Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now));
                    }
                    else
                    {
                        query.CreateCriteria("Scholarship")
                             .AddOrder(new Order("MaximumAmount", false));
                    }
					break;

				default:
					throw new NotSupportedException(string.Format("SortExpression {0} not supported", sortExpression));
			}
			return query;
		}

		public Match Find(Seeker seeker, int scholarshipId)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship.Id", scholarshipId))
                .UniqueResult<Match>();
        }

		public void UpdateMatches(Seeker seeker)
		{
			IQuery query = Session.CreateSQLQuery("exec dbo.BuildSeekerMatches @SeekerId=:seekerId,@UpdateUserId=:userId");
			query.SetInt32("seekerId", seeker.Id);
			query.SetInt32("userId", seeker.User.Id);
			query.ExecuteUpdate();
		}

		#region Method Overrides
		public new Match Insert(Match match)
		{
			match.ComputeRank();
			return base.Insert(match);
		}

		public new Match Update(Match match)
		{
			match.ComputeRank();
			return base.Update(match);
		}
		#endregion

    }
}
