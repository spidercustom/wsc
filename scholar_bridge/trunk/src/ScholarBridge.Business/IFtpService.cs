﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScholarBridge.Business
{
	public interface IFtpService
	{
		bool FTPEnabled { get; set; }
		void PutFileToPublicServer(string remoteFileName, string localFilePath);
	}
}
