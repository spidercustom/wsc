﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
	public class FaqService : IFaqService
	{
		public IFaqDAL FaqDAL { get; set; }

		public Faq GetById(int id)
		{
			return FaqDAL.FindById(id);
		}

		public Faq Save(Faq faq)
		{
			if (faq == null)
				throw new ArgumentNullException("faq");

			return FaqDAL.Save(faq);
		}

		public IList<Faq> FindAll()
		{
			return (from a in FaqDAL.FindAll("Question") 
					orderby a.FaqType, a.Question
					ascending select a).ToList();
		}

		public IList<Faq> FindAll(FaqType type)
		{
            // FIXME: filter and order in the database
			return (from a in FaqDAL.FindAll("Question") 
					where a.FaqType == type
					orderby a.Question
					ascending select a).ToList();
		}

		public void Delete(Faq faq)
		{
			if (null == faq)
				throw new ArgumentNullException("faq");

			FaqDAL.Delete(faq);
		}
	}
}