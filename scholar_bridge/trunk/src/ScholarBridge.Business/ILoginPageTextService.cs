﻿

namespace ScholarBridge.Business
{
	public interface ILoginPageTextService
	{
		string GetLoginPageText();
		void PutLoginPageText(string loginPageText);
	}
}
