using System;
using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class MatchServiceTests
    {
        private MockRepository mocks;
        private MatchService matchService;
        private IMatchDAL matchDAL;
        private ISeekerDAL seekerDAL;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            matchDAL = mocks.StrictMock<IMatchDAL>();
            seekerDAL = mocks.StrictMock<ISeekerDAL>();
            matchService = new MatchService
                               {
                                   MatchDAL = matchDAL,
                                   SeekerDAL = seekerDAL
                               };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(matchDAL);
            mocks.BackToRecord(seekerDAL);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void apply_for_match_throws_exception_if_null_passed()
        {
            matchService.ApplyForMatch(null, 0, null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void change_status_throws_exception_if_null_passed()
        {
            matchService.ChangeStatus(null, 0, MatchStatus.New);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_saved_but_not_applied_matches_seeker_throws_exception_if_null_passed()
        {
            matchService.GetSavedButNotAppliedMatches(null);
            Assert.Fail();
        }


        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_saved_matches_scholarship_throws_exception_if_null_passed()
        {
            Scholarship s = null;
            matchService.GetSavedMatches(s);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void disconnect_application_throws_exception_if_null_passed()
        {
            matchService.DisconnectApplication(null);
            Assert.Fail();
        }

        [Test]
        public void get_saved_but_not_applied_mataches_calls_correct_dal_function()
        {
            var seeker = new Seeker();
            Expect.Call(matchDAL.FindAll(seeker, MatchStatus.Saved)).Return(new List<Match>());
            mocks.ReplayAll();

            matchService.GetSavedButNotAppliedMatches(seeker);
            mocks.VerifyAll();
        }

        [Test]
        public void save_match_changes_match_status()
        {
            var seeker = new Seeker { LastUpdate = new ActivityStamp { On = DateTime.Now } };
            var match = new Match {Id = 1};
            Expect.Call(matchDAL.Find(seeker, 1)).Return(match);
            Expect.Call(matchDAL.Update(match)).Return(match);
            mocks.ReplayAll();

            matchService.SaveMatch(seeker, 1);
            mocks.VerifyAll();
        }

        [Test]
        public void unsave_match_changes_match_status()
        {
            var seeker = new Seeker { LastUpdate = new ActivityStamp { On = DateTime.Now } };
            var match = new Match { Id = 1 };
            Expect.Call(matchDAL.Find(seeker, 1)).Return(match);
            Expect.Call(matchDAL.Update(match)).Return(match);
            mocks.ReplayAll();

            matchService.UnSaveMatch(seeker, 1);
            mocks.VerifyAll();
        }

        [Test]
        public  void apply_for_match()
        {
            var seeker = new Seeker();
            var match = new Match();
            const int scholarshipId = 1;
            var application = new Application(ApplicationType.Internal);

            Expect.Call(matchDAL.Find(seeker, scholarshipId)).Return(match);
            Expect.Call(matchDAL.Update(match)).Return(match);
            mocks.ReplayAll();

            matchService.ApplyForMatch(seeker, scholarshipId, application);
            Assert.AreEqual(application, match.Application);
            Assert.AreEqual(MatchStatus.Submitted, match.MatchStatus);
        }

        [Test]
        public void disconnect_application_not_connected_to_matches()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };

            Expect.Call(matchDAL.FindAll(app)).Return(null);
            mocks.ReplayAll();

            matchService.DisconnectApplication(app);
            mocks.VerifyAll();
        }

        [Test]
        public void disconnect_application_connected_to_matches()
        {
            var app = new Application(ApplicationType.Internal)
            {
                Seeker = new Seeker(),
                Scholarship = new Scholarship()
            };

            var m = new Match {Application = app, Seeker = app.Seeker, Scholarship = app.Scholarship};
            var matches = new List<Match> {m};

            Expect.Call(matchDAL.FindAll(app)).Return(matches);
            Expect.Call(matchDAL.Update(m)).Return(m);
            mocks.ReplayAll();

            matchService.DisconnectApplication(app);
            mocks.VerifyAll();

            Assert.IsNull(m.Application);
            Assert.AreEqual(MatchStatus.Saved, m.MatchStatus);
        }
    }
}