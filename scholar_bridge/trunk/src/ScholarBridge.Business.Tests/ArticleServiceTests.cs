using System;
using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class ArticleServiceTests
    {
        private ArticleService articleService;
        private MockRepository mocks;
        private IArticleDAL articleDAL;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            articleDAL = mocks.StrictMock<IArticleDAL>();
            articleService = new ArticleService()
            {
                ArticleDAL = articleDAL
            };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(articleDAL);
        }

        [Test]
        public void should_get_article_by_id()
        {
            Expect.Call(articleDAL.FindById(1)).Return(new Article {Id = 1});

            mocks.ReplayAll();

            Article f = articleService.GetById(1);
            Assert.IsNotNull(f);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void should_throw_exception_on_save_of_null()
        {
            articleService.Save(null);
            Assert.Fail();
        }

        [Test]
        public void should_save_article()
        {
            var a = new Article {Id = 1};
            Expect.Call(articleDAL.Save(a)).Return(a);

            mocks.ReplayAll();

            articleService.Save(a);
            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void should_throw_exception_on_delete_of_null()
        {
            articleService.Delete(null);
            Assert.Fail();
        }

        [Test]
        public void should_delete_article()
        {
            var a = new Article { Id = 1 };
            Expect.Call(() => articleDAL.Delete(a));

            mocks.ReplayAll();

            articleService.Delete(a);
            mocks.VerifyAll();
        }

        [Test]
        public void should_find_all_when_empty()
        {
            Expect.Call(articleDAL.FindAll("PostedDate")).Return(new List<Article>());

            mocks.ReplayAll();

            IList<Article> found = articleService.FindAll();
            mocks.VerifyAll();
        }

        [Test]
        public void should_find_all_when_found()
        {
            Expect.Call(articleDAL.FindAll("PostedDate")).Return(new List<Article> { new Article { Title = "A" }, new Article { Title = "B"} });

            mocks.ReplayAll();

            IList<Article> found = articleService.FindAll();
            Assert.AreEqual(2, found.Count);
            Assert.AreEqual("A", found[0].Title);
            mocks.VerifyAll();
        }
    }
}