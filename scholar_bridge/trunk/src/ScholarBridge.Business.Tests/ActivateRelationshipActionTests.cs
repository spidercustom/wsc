﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Actions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ActivateRelationshipActionTests
    {
        private MockRepository mocks;
        private ActivateRelationshipAction activateRelationship;
        private IRelationshipService relationshipService;
        private IMessageService messageService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            relationshipService = mocks.StrictMock<IRelationshipService>();
            messageService = mocks.StrictMock<IMessageService>();
            activateRelationship = new ActivateRelationshipAction
            {
                RelationshipService =relationshipService,
                MessageService = messageService
            };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(relationshipService);
            mocks.BackToRecord(messageService);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void approve_with_wrong_message_type_throws_exception()
        {
            activateRelationship.Approve(new Message(), null, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void reject_with_wrong_message_type_throws_exception()
        {
            activateRelationship.Reject(new Message(), null, null);
        }

        [Test]
        public void approve_with_relationship()
        {
            var r = new Relationship
                        {
                            Status = RelationshipStatus.Pending,
                            Provider = new Provider() {AdminUser = new User() {Id  = 1}},
                            Intermediary = new Intermediary() { AdminUser = new User() { Id = 3 } },
                            Requester=RelationshipRequester.Provider
                            

            };
            var m = new RelationshipMessage
                        {
                            Subject  = "ApproveMessage",
                            From=new MessageAddress(){Organization=r.Provider},
                            To = new MessageAddress() { Organization = r.Intermediary }

                        };
            Expect.Call(relationshipService.GetByOrganizations(r.Provider, r.Intermediary)).Return(r);
            Expect.Call(() => relationshipService.Approve(r));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateRelationship.Approve(m, null, r.Provider.AdminUser);

            mocks.VerifyAll();
        }

        [Test]
        public void reject_with_relationship()
        {
            var r = new Relationship
            {
                Status = RelationshipStatus.Pending,
                Provider = new Provider() { AdminUser = new User() { Id = 1 } },
                Intermediary = new Intermediary() { AdminUser = new User() { Id = 3 } },
                Requester = RelationshipRequester.Provider


            };
            var m = new RelationshipMessage
            {
                Subject = "RejectMessage",
                From = new MessageAddress() { Organization = r.Provider },
                To = new MessageAddress() { Organization = r.Intermediary }

            };
            Expect.Call(relationshipService.GetByOrganizations(r.Provider, r.Intermediary)).Return(r);
            Expect.Call(() => relationshipService.Reject(r));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateRelationship.Reject(m, null, r.Provider.AdminUser);

            mocks.VerifyAll();
        }
    }
}
