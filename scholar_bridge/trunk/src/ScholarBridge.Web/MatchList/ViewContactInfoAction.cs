using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.MatchList
{
    class ViewContactInfoAction : OpenUrlAction
    {
        private const string VIEW_CONTACT_INFO_URL_TEMPLATE = "~/Seeker/Scholarships/Show.aspx?id={0}#generalinfo-tab";

        public ViewContactInfoAction() : base("Contact") { }

        protected override string ConstructUrl(Match match)
        {
            return VIEW_CONTACT_INFO_URL_TEMPLATE.Build(match.Scholarship.Id);
        }

		public override string GetActionUrl(Match match)
		{
			return ConstructUrl(match);
		}
    }
}