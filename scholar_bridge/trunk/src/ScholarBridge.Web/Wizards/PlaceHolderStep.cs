﻿namespace ScholarBridge.Web.Wizards
{
    public class PlaceHolderStep<T> : IWizardStepControl<T>
    {
        #region IWizardStepControl<T> Members

        public bool ValidateStep()
        {
            return true;
        }

        public void PopulateObjects() {}

        public void Save() {}

        public bool ChangeNextStepIndex()
        {
            return false;
        }

        public int GetChangedNextStepIndex() { return 0; }

        public bool WasSuspendedFrom(T @object)
        {
            return false;
        }

        public bool CanResume(T @object)
        {
            return false;
        }

        public bool IsCompleted { get; set; }

        public IWizardStepsContainer<T> Container { get; set; } 

        public void Activated() {}
        
        #endregion
    }
}
