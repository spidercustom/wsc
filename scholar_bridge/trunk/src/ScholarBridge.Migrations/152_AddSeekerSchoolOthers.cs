﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(152)]
    public class AddSeekerSchoolOthers : Migration
	{
		private const string SEEKER_TABLE_NAME = "SBSeeker";
        private readonly string[] COLUMNS =
            new[] { "CurrentHighSchoolOther", "CurrentCollegeOther", "CollegesAppliedOther", "CollegesAcceptedOther" };

		public override void Up()
		{
            foreach (var c in COLUMNS)
            {
                Database.AddColumn(SEEKER_TABLE_NAME, new Column(c, DbType.String, 250, ColumnProperty.Null));
            }
		}

		public override void Down()
		{
		    foreach (var c in COLUMNS)
		    {
		        Database.RemoveColumn(SEEKER_TABLE_NAME, c);
		    }
		}
	}
}