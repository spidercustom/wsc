﻿using System.Collections.Generic;
using System.Data;
using Migrator.Framework;
using Spider.Common.Extensions.System.Collections;

namespace ScholarBridge.Migrations
{
    [Migration(56)]
    public class AddDeprecatedColumnToOtherLookups : Migration
    {
        private readonly List<string> TABLES = new List<string>
                                                   {
                                                       "SBAdditionalRequirementLUT", 
                                                       "SBSupportLUT", 
                                                       "SBNeedGapLUT"
                                                   };

        public override void Up()
        {
            var column = new Column("Deprecated", DbType.Boolean, ColumnProperty.NotNull, 0);
            TABLES.Each(t => Database.AddColumn(t, column));
        }

        public override void Down()
        {
            TABLES.Each(t => Database.RemoveColumn(t, "Deprecated"));
        }
    }
}