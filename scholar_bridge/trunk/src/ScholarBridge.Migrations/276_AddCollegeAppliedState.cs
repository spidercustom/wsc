﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(276)]
    public class AddCollegeAppliedState : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public readonly string[] NEW_COLUMNS
            = { "IsCollegeAppliedInWashington", "IsCollegeAppliedOutOfState" };

        public override void Up()
        {
      

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Boolean, ColumnProperty.Null);
           
        }

        public override void Down()
        {
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}