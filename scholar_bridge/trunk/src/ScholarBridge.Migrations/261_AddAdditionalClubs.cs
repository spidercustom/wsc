﻿using System;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(261)]
	public class AddAdditionalClubs : Migration
	{
		private string LOOKUP_TABLE = "SBClubLUT";

		private string[] COLUMNS = new []
		                           	{
		                           		"Club", "Description", "Deprecated", "LastUpdateBy", "LastUpdateDate"
		                           	};

		public override void Up()
		{
			int adminId = HECBStandardDataHelper.GetAdminUserId(Database);

			foreach (var value in LOOKUP_VALUES)
			{
				AddLookup(value, adminId);	
			}

			Database.Update(LOOKUP_TABLE, new [] {COLUMNS[0], COLUMNS[1]},
			                new [] {"Dance/Drill Club", "Dance/Drill Club"}, "Club = 'Dance'");
		}

		public override void Down()
		{
			foreach (var value in LOOKUP_VALUES)
			{
				try
				{
					Database.Delete(LOOKUP_TABLE, COLUMNS[0], value);
				}
				catch (Exception)
				{
					// do nothing in-case the lookup value is already attached 
				}
			}
			Database.Update(LOOKUP_TABLE, new [] { COLUMNS[0], COLUMNS[1] },
							new [] { "Dance", "Dance" }, "Club = 'Dance/Drill Club'");
		}

		private void AddLookup(string lookupValue, int adminId)
		{
			int hitCount = (int)Database.ExecuteScalar("select count(*) from " + LOOKUP_TABLE + " where " + COLUMNS[0] + " = '" + lookupValue + "'");
			if (hitCount == 0)
				Database.Insert(LOOKUP_TABLE, COLUMNS,
								new[] { lookupValue, lookupValue, "0", adminId.ToString(), DateTime.Now.ToString() }
					);
		}

		private string[] LOOKUP_VALUES = new []
		                                	{
												"Art Club",
												"AWANA",
												"Boys and Girls Club of America",
												"Church Youth Groups",
												"Ecology Club",
												"FBLA",
												"FFA",
												"Gardening Club",
												"Gem & Mineral Club",
												"Junior Achievement",
												"Key Club/Builders Club",
												"Museum Club",
												"National Honor Society/Jr Honor Society",
												"Spiral Scouts International",
												"Student Council/Leadership",
												"Upward Sports Club"
		                                	};
	}
}
