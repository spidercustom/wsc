﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListOfAdditionalRequirements.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.ListOfAdditionalRequirements" %>

<asp:ListView ID="lstRequirements" runat="server">
     <LayoutTemplate>
    <table>
    <asp:PlaceHolder id="itemPlaceholder" runat="server" />
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr>
        
        <td><%# DataBinder.Eval(Container.DataItem, "Name")%></td>
        
     </tr>
    </ItemTemplate>
   
    <EmptyDataTemplate>
    <p>There is no additional requirements </p>
    
    </EmptyDataTemplate>
</asp:ListView>


