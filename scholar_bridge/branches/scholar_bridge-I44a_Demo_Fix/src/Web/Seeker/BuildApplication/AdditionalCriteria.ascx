﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteria.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.AdditionalCriteria" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

<%@ Register src="EditQuestionAnswers.ascx" tagname="EditQuestionAnswers" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register src="ListOfAdditionalRequirements.ascx" tagname="ListOfAdditionalRequirements" tagprefix="sb" %>

<div class="form-iceland">
    <p class="form-section-title">Additional Requirements with Scholarship Application</p>
    <sb:ListOfAdditionalRequirements ID="AdditionalRequirementsList" runat="server" />
    <hr />

    <p class="form-section-title">Please complete the following instructions and questions: </p>
    <p><span class="requiredAttributeIndicator" >*</span>=required</p>
    <sb:EditQuestionAnswers ID="QAEditor" runat="server" />
    <br />
    <hr />

    <asp:panel ID="scholarshipFilesPanel" runat="server">
    <p class="form-section-title">Scholarship Application Documents</p>
        <asp:ListView ID="scholarshipFiles" runat="server" OnItemCommand="scholarshipFiles_ItemCommand">
            
            <LayoutTemplate >
            
                <table class="sortableTable">
                    <thead>
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                File
                            </th>
                            <th>
                                Size
                            </th>
                            <th>
                                Instructions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                    </tbody>
                </table>
            </LayoutTemplate>
            
            <ItemTemplate>
                <tr class="row">
                    <td>
                        <asp:LinkButton ID="deleteAttachmentBtn" CssClass="ListButton"  runat="server" Text="Download" CommandName="Download"
                            CommandArgument='<%# Eval("Id")%>' />
                    </td>
                    <td>
                        <%# Eval("Name") %>
                    </td>
                    <td>
                        <%# Eval("DisplaySize") %>
                    </td>
                    <td>
                        <%# Eval("Comment") %>
                    </td>
                </tr>
            </ItemTemplate>
             
            <EmptyDataTemplate>
            <table class="sortableTable">
                    <thead>
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                File
                            </th>
                            <th>
                                Size
                            </th>
                            <th>
                                Instructions For Applicants
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </EmptyDataTemplate>
            
        </asp:ListView>
    <br />
    <hr />
    </asp:panel>


    <p class="form-section-title">Upload files</p>
    <label for="AttachFile">Select File:</label>
    <asp:FileUpload ID="AttachFile" runat="server" />
    <br />
    
    <label for="AttachmentComments">Comments:</label>
    <asp:TextBox ID="AttachmentComments" runat="server" />
    <elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments" PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment"/>
    <br />
    
    <sbCommon:AnchorButton ID="UploadFile" runat="server" Text="Attach File" onclick="UploadFile_Click" />
    <br />
    
    <asp:ListView ID="attachedFiles" runat="server" OnItemDeleting="attachedFiles_OnItemDeleting">
        <LayoutTemplate>
        <table class="sortableTable">
            <thead>
                <tr>
                    <th style="width:50px;">&nbsp;</th>
                    <th style="width:300px;">File</th>
                    <th style="width:70px;">Size</th>
                    <th>Comments</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:LinkButton ID="deleteAttachmentBtn" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("Id")%>' /></td>
                <td><%# Eval("Name") %></td>
                <td><%# Eval("DisplaySize") %></td>
                <td><%# Eval("Comment")%></td>
            </tr>
        </ItemTemplate>
    </asp:ListView>

</div>