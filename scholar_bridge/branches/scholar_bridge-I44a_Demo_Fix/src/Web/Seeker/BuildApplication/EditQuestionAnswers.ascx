﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditQuestionAnswers.ascx.cs" Inherits="ScholarBridge.Web.Seeker.BuildApplication.EditQuestionAnswers" %>

<asp:ListView ID="lstQA" runat="server" 
    onitemdatabound="lstQA_ItemDataBound"  >
    <LayoutTemplate>
    
    <table width="100%">
    <asp:PlaceHolder id="itemPlaceholder" runat="server" />
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
        <tr><td>
            <label style="width:550px">
                <%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Question"), "QuestionText")%>
                 <span class="requiredAttributeIndicator" runat="server" id="requiredIndicator">*</span> 
            
            </label>
            
            <br />
            <asp:TextBox ID="txtAnswer" runat="server" TextMode="MultiLine" Rows="2" Width="550px" Height="50px"/>
           
            
            <br />
        </td></tr>
    </ItemTemplate>
   
    <EmptyDataTemplate>
    <p>There are no additional questions </p>
    
    </EmptyDataTemplate>
</asp:ListView>


