﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.MatchList;
using ScholarBridge.Domain.ApplicationParts;
using Spring.Context.Support;

namespace ScholarBridge.Web.Seeker.Scholarships
{
	public partial class Default : SBBasePage
	{
        private const string SCHOLARSHIP_VIEW_TEMPLATE = "~/Seeker/Scholarships/Show.aspx";
        public const string SESSION_APPLICATION_ID_KEY = "InitialApplicationIdToShow";

		public IApplicationService ApplicationService
        {
            get { return (IApplicationService)ContextRegistry.GetContext().GetObject("ApplicationService"); }
        }

        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }
         

        
        private readonly ApplicationListActionHelper actionHelper = new ApplicationListActionHelper();
       
		public int? InitialApplicationIdToShow
		{
			get
			{
				if (Session[SESSION_APPLICATION_ID_KEY] == null)
					return new int?();
				return (int?)Session[SESSION_APPLICATION_ID_KEY];
			}
			set
			{
				if (value == null)
					Session.Remove(SESSION_APPLICATION_ID_KEY);
				else
					Session[SESSION_APPLICATION_ID_KEY] = value;
			}
		}

		 

		protected override void OnInitComplete(EventArgs e)
        {
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.SeekerApplications); 
            UserContext.EnsureSeekerIsInContext();
           
            actionHelper.SetupListView(applicationList);
            base.OnInitComplete(e);
        }

        
		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				CheckForApplicationToShow();
			}			
		}

		/// <summary>
		/// if an application ID is passed in session, then popup the Application.Show page in 
		/// a new window.
		/// </summary>
		private void CheckForApplicationToShow()
		{
			if (InitialApplicationIdToShow.HasValue)
			{
				string jscript = "<script type='text/javascript'>";
				jscript += "printApplications('" +
				           LinkGenerator.GetFullLinkStatic("/seeker/applications/show.aspx?aid="
				                                           + InitialApplicationIdToShow.Value + "&print=true")
				           + "')";
				jscript += "</script>";
				const string showApp = "showApplication";
				if (!Page.ClientScript.IsClientScriptBlockRegistered(showApp))
					Page.ClientScript.RegisterStartupScript(Page.GetType(), showApp, jscript);
				InitialApplicationIdToShow = null;
			}
		}



        protected void applicationList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
				var application = ((Application)((ListViewDataItem)e.Item).DataItem);
                SetScholarshipViewLink(application, e);
            }
        }

		private void SetScholarshipViewLink(Application application, ListViewItemEventArgs e)
		{
			var link = (LinkButton)e.Item.FindControl("linkToScholarship");
			var url = new UrlBuilder(AbsolutePathWithoutQueryString()) { Path = ResolveUrl(SCHOLARSHIP_VIEW_TEMPLATE) }.ToString()
			          + "?id=" + application.Scholarship.Id + "&print=true";
			link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
           
			var submittedDateLabel = (Label)e.Item.FindControl("submittedDateLabel");
            //in case of online application we only need to show blank submitted date.
            if (application.ApplicationType == ApplicationType.External)
            {
                submittedDateLabel.Text = "&nbsp;";
            }
            else
            {
                submittedDateLabel.Text = application.SubmittedDate == null
                                              ? "&nbsp;"
                                              : application.SubmittedDate.Value.ToShortDateString();
            }
		}

		private string AbsolutePathWithoutQueryString()
		{
			string[] absolutePathParts = Request.Url.AbsoluteUri.Split('?');
			return absolutePathParts[0];
		}


        public IList<Application> GetApplications(string sortExpression)
        {
            var currentSeeker = UserContext.CurrentSeeker;
            if (currentSeeker == null)
                throw new InvalidOperationException("There is no seeker in context");
            var applications = ApplicationService.FindBySeeker(currentSeeker);
            return SortApplications(sortExpression, applications);
        }
        public IList<Application> SortApplications(string sortExpression, IList<Application> list)
        {
            switch (sortExpression)
            {
                case "ScholarshipName":
                case "ScholarshipName ASC":
                    list = (from l in list orderby l.Scholarship.Name ascending select l).ToList();

                    break;

                case "ScholarshipName DESC":
                    list = (from l in list orderby l.Scholarship.Name descending select l).ToList();
                    break;
               
                case "ApplicationDueDate":
                case "ApplicationDueDate ASC":
                    list = (from l in list orderby l.Scholarship.ApplicationDueDate ascending select l).ToList();
                    break;

                case "ApplicationDueDate DESC":
                    list = (from l in list orderby l.Scholarship.ApplicationDueDate descending select l).ToList();
                    break;

                case "NumberOfAwards":
                case "NumberOfAwards ASC":
                    list = (from l in list orderby l.Scholarship.FundingParameters.MaximumNumberOfAwards ascending select l).ToList();
                     break;

                case "NumberOfAwards DESC":
                    list = (from l in list orderby l.Scholarship.FundingParameters.MaximumNumberOfAwards descending select l).ToList();
                    break;

                case "ScholarshipAmount":
                case "ScholarshipAmount ASC":
                    list = (from l in list orderby l.Scholarship.AmountRange ascending select l).ToList();
                    break;

                case "ScholarshipAmount DESC":
                    list = (from l in list orderby l.Scholarship.AmountRange descending select l).ToList();
                    break;

                case "Status":
                case "Status ASC":
                    list = (from l in list orderby l.ApplicationStatus ascending select l).ToList();
                    break;

                case "Status DESC":
                    list = (from l in list orderby l.ApplicationStatus descending select l).ToList();
                    break;

                case "Action":
                case "Action ASC":
                    list = (from l in list orderby l.ApplicationStatus ascending select l).ToList();
                    break;

                case "Action DESC":
                    list = (from l in list orderby l.ApplicationStatus descending select l).ToList();
                    break;

                case "DateSubmitted":
                case "DateSubmitted ASC":
                    list = (from l in list orderby l.SubmittedDate ascending select l).ToList();
                    break;

                case "DateSubmitted DESC":
                    list = (from l in list orderby l.SubmittedDate descending select l).ToList();
                    break;

            }
            return list;
        }


	    protected void pager_PreRender(object sender, EventArgs e)
        {
            var pager=(DataPager) sender;
            pager.Visible = pager.TotalRowCount > pager.PageSize;
        }
	}
}