﻿using System;


namespace ScholarBridge.Web.Admin.Editors
{
    public partial class Default : System.Web.UI.Page
    {
                 
        protected void Page_Load(object sender, EventArgs e)
        {
			if (!Page.IsPostBack)
				adminEditorIntegrator.ActiveTabIndex = 0;
        }

        protected void Bind()
        {
        }

		protected void pressRoomLink_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/admin/PressRoom");
		}
    }
}
