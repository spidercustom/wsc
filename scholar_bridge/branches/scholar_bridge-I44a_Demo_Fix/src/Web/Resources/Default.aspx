﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Resources.Default" Title="Resources" %>
<%@ Register src="~/Resources/ResourceList.ascx" tagname="ResourceList" tagprefix="sb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Terms and Conditions" />
    <style type="text/css">
        #PageContent hr{margin:15px auto;}
        #PageContent p{margin-top:5px;}
        #PageContent p.hookline{margin-top:10px;}
        #PageContent p.hookline + hr{border-color:White;}
        #PageContent div.item-title{font-size:12px;}
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Resources</h2>
    <p class="hookline">Some interesting and useful sites and documents to aid you in your scholarship search. </p>
    <sb:ResourceList ID="resourceList1" runat="server"/>
</asp:Content>
