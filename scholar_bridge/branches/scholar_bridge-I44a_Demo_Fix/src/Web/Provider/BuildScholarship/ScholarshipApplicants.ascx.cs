﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Logging;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Web.Config;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class ScholarshipApplicants : WizardStepUserControlBase<Scholarship>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ScholarshipApplicants));
        private   const string CHECKBOX_SCRIPT_TEMPLATE ="javascript:getElementById('{0}').value='{1}'";

		public IMatchService MatchService { get; set; }
		public IApplicationService ApplicationService { get; set; }
		public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        private const string DEFAULT_SORTEXPRESSION = "DateSubmitted";

        private const string DESCENDING_SORTDIRECTION = "DESC";
        private const string ASCENDING_SORTDIRECTION = "ASC";


        Scholarship ScholarshipInContext
        {
            get { return Container.GetDomainObject(); }
        }

        public string LinkTo { get; set; }

        
        public string CurrentSortExpression
        {
            get
            {
                if (ViewState["CurrentSortExpression"] == null)
                {
                    ViewState["CurrentSortExpression"] = DEFAULT_SORTEXPRESSION;
                }
                return (string)ViewState["CurrentSortExpression"];
            }
            set
            {
                ViewState["CurrentSortExpression"] = value;
            }

        }
        public string CurrentSortDirection
        {
            get
            {
                if (ViewState["CurrentSortDirection"] == null)
                {
                    ViewState["CurrentSortDirection"] = DESCENDING_SORTDIRECTION;
                }
                return (string)ViewState["CurrentSortDirection"];
            }
            set
            {
                ViewState["CurrentSortDirection"] = value;
            }

        }

        public void SetCurrentSortDirection()
        {
            CurrentSortDirection = CurrentSortDirection == ASCENDING_SORTDIRECTION
                                       ? DESCENDING_SORTDIRECTION
                                       : ASCENDING_SORTDIRECTION;

        }

        protected void lstApplicants_Sorting (object sender, ListViewSortEventArgs eventArgs)
        {
            Applications=GetApplications(null);
             
            if (CurrentSortExpression ==eventArgs.SortExpression )
            {
                SetCurrentSortDirection();
            }
            switch (eventArgs.SortExpression)
            {
                case "Finalist":
                    Applications =CurrentSortDirection ==ASCENDING_SORTDIRECTION ?
                        (from l in Applications orderby l.Finalist  ascending  select l).ToList()
                        : (from l in Applications orderby l.Finalist descending select l).ToList();

                    break;

                case "AwardStatus":

                    Applications = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                        (from l in Applications orderby l.AwardStatus ascending select l).ToList()
                        : (from l in Applications orderby l.AwardStatus descending select l).ToList();
                    break;

                case "ApplicantName":
                    Applications = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                           (from l in Applications orderby l.Seeker.Name.NameLastFirst ascending select l).ToList()
                           : (from l in Applications orderby l.Seeker.Name.NameLastFirst descending select l).ToList();
                    break;

                case "DateSubmitted":
                    Applications = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                           (from l in Applications orderby l.SubmittedDate ascending select l).ToList()
                           : (from l in Applications orderby l.SubmittedDate descending select l).ToList();
                    break;

                case "RequiredCriteria":
                    Applications = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                           (from l in Applications orderby l.MinumumCriteriaString ascending select l).ToList()
                           : (from l in Applications orderby l.MinumumCriteriaString descending select l).ToList();
                    break;

                case "PreferredCriteria":
                    Applications = CurrentSortDirection == ASCENDING_SORTDIRECTION ?
                           (from l in Applications orderby l.PreferredCriteriaString ascending select l).ToList()
                           : (from l in Applications orderby l.PreferredCriteriaString descending select l).ToList();
                    break;
            }
            CurrentSortExpression = eventArgs.SortExpression;
             BindApplications();
        }

        public string EmptyMessage
    	{
    		get {
    			return !Page.IsPostBack ? "No applicants for this scholarship at this time" : "No applicants were selected by the search criteria.";
    			}
    	}

        private IList<Application> Applications { get; set; }

        public int ApplicationCount
        {
            get { return ScholarshipInContext.ApplicantsCount; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (! Page.IsPostBack)
            {
                var ub = new UrlBuilder(Request.Url.AbsoluteUri) { PageName = "PrintApplications.aspx" };
                printWindow.OnClientClick = String.Format("printApplications('{0}'); return false;", ub.ToString());

                PopulateScreen();

				if (Page is SBBasePage)
				{
					((SBBasePage)Page).BypassPromptIds.AddRange(
						new[]	{
									"appFinalistCheckBox",
                                    "application"
								});
				}
            }
            else
            {
                // workaround to see which checkbox has been clicked 
                // events for these checkboxes are not firing due to nested user controls
                HandleFinalistCheckboxes();
                HandleAwardStatusChanges();
            }
        }

        private void PopulateScreen()
        {
            Applications = GetApplications(null);
            BindApplications();
        }


		protected void lstApplicants_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var app = ((Application)((ListViewDataItem)e.Item).DataItem);
                BindFinalistCheckbox(e, app);
                BindApplicationLink(e, app);
                BindAttachmentsButton(e, app);
                BindAwardStatusDdl(e, app);
            }
        }

        private void HandleFinalistCheckboxes()
        {
            if (String.IsNullOrEmpty(hiddenfinalist.Value))
                return;

            foreach (ListViewDataItem  dt in lstApplicants.Items )
            {
                var cb = (CheckBox) dt.FindControl("appFinalistCheckBox");
                var appId = Int32.Parse(cb.InputAttributes["value"]);
                if (appId.ToString() == hiddenfinalist.Value)
                {
                    var application = ApplicationService.GetById(appId);
                    application.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    if (cb.Checked)
                    {
                        ApplicationService.Finalist(application);
                    }
                    else
                    {
                        ApplicationService.NotFinalist(application);
                    }
                }
            }
            hiddenfinalist.Value = "";
            searchApplicants_Click(null, null);
        }

        private void HandleAwardStatusChanges()
        {

            if (String.IsNullOrEmpty(hiddenawardstatus.Value))
                return;

            foreach (ListViewDataItem dt in lstApplicants.Items)
            {
                var ddl = (DropDownList)dt.FindControl("awardStatusDdl");
                var parts = ddl.SelectedValue.Split('_');
                var appId = Int32.Parse(parts[0]);
                var newStatus = (AwardStatus)Enum.Parse(typeof(AwardStatus), parts[1]);

                if (appId.ToString() == hiddenawardstatus.Value)
                {
                    var application = ApplicationService.GetById(appId);
                    application.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                    switch (newStatus)
                    {
                        case AwardStatus.Awarded:
                            ApplicationService.AwardScholarship(application, UserContext.CurrentUser);
                            break;
                        case AwardStatus.NotAwarded:
                            ApplicationService.NotAwardScholarship(application, UserContext.CurrentUser);
                            break;
                        case AwardStatus.Offered:
                            ApplicationService.OfferScholarship(application, UserContext.CurrentUser);
                            break;
                        default:
                            application.AwardStatus = newStatus;
                            ApplicationService.Update(application);
                            break;
                    }
                }
            }

            hiddenawardstatus.Value = "";
            searchApplicants_Click(null, null);
        }

        protected void searchApplicants_Click(object sender, EventArgs e)
        {
            Applications = GetApplications(searchApplicantsTxt.Text);
            BindApplications();
        }

        protected void clearSearchBtn_Click(object sender, EventArgs e)
        {
            searchApplicantsTxt.Text = null;
            Applications = GetApplications(null);
            BindApplications();
        }

        protected void downloadAllBtn_OnClick(object sender, EventArgs e)
        {
            Applications = GetApplications(null);

            if (log.IsInfoEnabled) log.Info(String.Format("Downloading all application attachments for Scholarship id: '{0}'", ScholarshipInContext.Id));
            DownloadZip(Applications);
        }

        protected void downloadAllFinalistBtn_OnClick(object sender, EventArgs e)
        {
            Applications = GetApplications(null);

            if (log.IsInfoEnabled) log.Info(String.Format("Downloading all finalist application attachments for Scholarship id: '{0}'", ScholarshipInContext.Id));
            DownloadZip(Applications.Where(a => a.Finalist));
        }

        protected void saveAwardPeriodDate_Click(object sender, EventArgs e)
        {
            if (awardPeriodCloseDate.SelectedDate.HasValue)
            {
                ScholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                ScholarshipService.CloseAwardPeriod(ScholarshipInContext, awardPeriodCloseDate.SelectedDate.Value);
            }
            searchApplicants_Click(null, null);
        }

        private void BindApplications()
        {
            if (ScholarshipInContext.AwardPeriodClosed.HasValue)
                awardPeriodCloseDate.SelectedDate = ScholarshipInContext.AwardPeriodClosed.Value;
            saveAwardPeriodDate.NeedsConfirmation = ! (null != Applications && Applications.Any(a => a.AwardStatus == AwardStatus.Awarded));

			if (Applications != null)
        		foreach (var app in Applications)
        		{
        			Match appMatch = MatchService.GetMatch(app.Seeker, app.Scholarship.Id);
					if (appMatch != null)
					{
						app.SeekerPreferredCriteriaCount = appMatch.SeekerPreferredCriteriaCount;
						app.SeekerMinimumCriteriaCount = appMatch.SeekerMinimumCriteriaCount;
						app.ScholarshipMinimumCriteriaCount = appMatch.ScholarshipMinimumCriteriaCount;
						app.ScholarshipPreferredCriteriaCount = appMatch.ScholarshipPreferredCriteriaCount;
					}
        		}

            lstApplicants.DataSource = Applications;
            lstApplicants.DataBind();

			if (null == Applications || Applications.Count == 0)
            {
                downloadAllBtn.Enabled = false;
                downloadAllFinalistBtn.Enabled = false;
            }
            else if (0 == Applications.Count(a => a.Finalist))
            {
                downloadAllFinalistBtn.Enabled = false;
            }
        }

        private void BindFinalistCheckbox(ListViewItemEventArgs e, Application app)
        {
            var cb = (CheckBox)e.Item.FindControl("appFinalistCheckBox");
            cb.InputAttributes.Add("value", app.Id.ToString());
            cb.Checked = app.Finalist;
            cb.Attributes.Add("onclick", CHECKBOX_SCRIPT_TEMPLATE.Build(hiddenfinalist.ClientID,  app.Id.ToString()) );
            // FIXME: When an app is awarded or denied or something, we probably want to disable this.
            // cb.Enabled = app.Stage != ApplicationStages.Awarded;
        }

        private void BindApplicationLink(ListViewItemEventArgs e, Application app)
        {
			//var link = (HyperLink)e.Item.FindControl("linkToApplication");
			//link.NavigateUrl = LinkTo + "?id=" + app.Id;
        }

        private void BindAttachmentsButton(ListViewItemEventArgs e, Application app)
        {
            var btn = (Button)e.Item.FindControl("attachmentsBtn");
            if (app.HasAttachments)
            {
                btn.CommandArgument = app.Id.ToString();
                var count = app.Attachments.Count;
                btn.Text = count == 1 ? "1 file" : String.Format("{0} files", count);
				var url = String.Format("{0}?id={1}", ConfigHelper.GetPageURLViaProxy(ResolveUrl("~/Common/ApplicationFiles.aspx")), app.Id);
				btn.Attributes.Add("onclick", String.Format("applicationAttachments('{0}'); return false;", url));
            }
            else
            {
                btn.Text = "None";
                btn.Enabled = false;
            }
        }

        private void BindAwardStatusDdl(ListViewItemEventArgs e, Application application)
        {
            var ddl = (DropDownList)e.Item.FindControl("awardStatusDdl");
            ddl.DataSource = GetStatusValues(application);
            ddl.DataValueField = "Value";
            ddl.DataTextField = "Key";
            ddl.DataBind();
            ddl.Items[0].Text = "- Select One -";

            if (application.AwardStatus != AwardStatus.None)
            {
                ddl.SelectedValue = BuildAwardStatusValue(application);
            }

            ddl.Attributes.Add("onchange", CHECKBOX_SCRIPT_TEMPLATE.Build(hiddenawardstatus.ClientID, application.Id.ToString()));
        }

        private IList<Application> GetApplications(string search)
        {
            if (null != ScholarshipInContext)
            {
                return String.IsNullOrEmpty(search)
                    ? ApplicationService.FindAllSubmitted(ScholarshipInContext)
                    : ApplicationService.FindAllSubmitted(ScholarshipInContext, search);
            }

            return null;
        }

        private Dictionary<string, string> GetStatusValues(Application application)
        {
            // Convert the value into format <application id>_<awardstatus>
            var newValues = new Dictionary<string, string>();
            var t = typeof (AwardStatus);
            foreach (var name in Enum.GetNames(t))
            {
                var displayName = ((AwardStatus) Enum.Parse(t, name)).GetDisplayName();
                newValues[displayName] = BuildAwardStatusValue(application, name);
            }

            return newValues;
        }

        private string BuildAwardStatusValue(Application app)
        {
            return BuildAwardStatusValue(app, app.AwardStatus.ToString());
        }

        private string BuildAwardStatusValue(Application app, string status)
        {
            return String.Format("{0}_{1}", app.Id, status);
        }

         
        protected void pager_PreRender(object sender, EventArgs e)
        {
            pager.Visible = pager.TotalRowCount > pager.PageSize;
        }
        private void DownloadZip(IEnumerable<Application> applications)
        {
            var mf = new MultipleFiles();
            foreach (var application in applications)
            {
                if (application.HasAttachments)
                {
                    var attachments = application.Attachments;
                    var directory = application.Seeker.Name.NameFirstLast;
                    attachments.ForEach(a =>
                                            {
                                                var fullPath = a.GetFullPath(ConfigHelper.GetAttachmentsDirectory());
                                                if (log.IsDebugEnabled)
                                                    log.Debug(String.Format("Adding file to zip: {0}", fullPath));

                                                mf.AddFile(Path.Combine(directory, a.Name), fullPath);
                                            });
                }
            }

            String file = null;
            try
            {
                file = Path.GetTempFileName();
                if (log.IsDebugEnabled) log.Debug(String.Format("Creating zip file: {0}", file));

                mf.CreateZip(file);

                if (log.IsDebugEnabled) log.Debug("Zip file created");

                FileHelper.SendZipFile(Response, file);
            }
            finally
            {
                if (null != file)
                {
                    try { File.Delete(file); } catch { }
                }
            }
        }
        #region IWizardStepControl implementation
        public override void Save()
        {
             
        }

        public override void PopulateObjects()
        {
        }

        public override bool ValidateStep()
        {
            var result = Validation.Validate(ScholarshipInContext);
            return result.IsValid;
        }


        #endregion
        
    }
}