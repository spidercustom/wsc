﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchCriteriaSelection.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.MatchCriteriaSelection" %>
<%@ Register TagPrefix="sb" TagName="AttributeSelectionControl" Src="AttributeSelectionControl.ascx" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>
<style type="text/css">
        .large-label-size
        {
            width: 250px !important;
        }
        
    </style>
<br />
<span><span class="requiredAttributeIndicator">*</span> = required</span><br />
<h2>Build Scholarship - Applicant's Profile</h2>
<p>Are there restrictions on who can apply for the scholarship? The school, type of program, or expenses the scholarship can be used for? Indicate with these fields what type of student can apply for the scholarship and what the scholarship can be used for. All are selected by default.</p>
<div class="form-iceland-container">
    <div class="form-iceland">
        <div class="form-iceland-container">
                <div class="form-iceland two-columns">
                    <p class="form-section-title-small">Type of Student Eligible to Apply</p>
                      <asp:PlaceHolder ID="StudentGroupContainerControl" runat="server">
                      <label id="StudentGroupLabelControl" for="StudentGroupControl">Student Groups:<span class="requiredAttributeIndicator">*</span></label>
                        <sb:FlagEnumCheckBoxList id="StudentGroupControl" runat="server" ShowSelectAllButton="True" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.StudentGroups, ScholarBridge.Domain" ></sb:FlagEnumCheckBoxList>
                        <sb:CoolTipInfo ID="CoolTipInfoStudentGroupControl" Content="Select all that apply. Will match with applicants answer to 'What type of student are you currently?'  Use to restrict who can apply." runat="server" />
                        <asp:CustomValidator ID="StudentGroupValidator"  OnServerValidate="StudentGroupValidator_OnServerValidate" runat="server"  ErrorMessage="Select at least one Student Group."    />
                     <br />
                      </asp:PlaceHolder>
                      
                      <br />
                      <p class="form-section-title-small">College and School type Scholarship can be used for</p>
                      <asp:PlaceHolder ID="CollegesContainerControl" runat="server">
                        <label id="CollegesControlLabel1" for="CollegesRadioButtons">Colleges:<span class="requiredAttributeIndicator">*</span></label>
                         <sb:CoolTipInfo ID="CoolTipInfoCollegesControl" Content="Use to indicate if the scholarship can only be used at specific colleges., Use to restrict who can apply." runat="server" />
                         <asp:CustomValidator ID="CollegesControlValidator"   OnServerValidate="CollegesControlValidator_OnServerValidate" runat="server"   ErrorMessage="Select at least one College."    />
  
                        <br />
                        <asp:RadioButton ID="CollegesRadioButtonAny" runat="server" GroupName="CollegesRadioButton" Text="Any" />
                        <br />
                        <asp:RadioButton ID="CollegesRadioButtonWashington" runat="server"    GroupName="CollegesRadioButton" Text="Washington" />
                       
                         <br />
                          <asp:RadioButton ID="CollegesRadioButtonOutOfState" runat="server"    GroupName="CollegesRadioButton" Text="Out of State" />
                        <br />
                        <br />
                        <asp:RadioButton ID="CollegesRadioButtonSpecify" runat="server"    GroupName="CollegesRadioButton" Text="Specify" />
                        <label id="CollegesControlLabel2" for="CollegesControl">&nbsp;</label>
                       
                        <sb:LookupDialog ID="CollegesControlDialogButton"   runat="server" BuddyControl="CollegesControl" ItemSource="CollegeDAL" Title="College Selection"/>
                        <asp:TextBox ID="CollegesControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="25"  runat="server"></asp:TextBox>
                       <br />
                      </asp:PlaceHolder>
                      <br />
                      <asp:PlaceHolder ID="SchoolTypeContainerControl" runat="server">
                      <label id="SchoolTypeControlLabel" for="SchoolTypeControl">School types:<span class="requiredAttributeIndicator">*</span></label>
                       <sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server"  ShowSelectAllButton="True" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
                       <sb:CoolTipInfo ID="CoolTipInfoSchoolTypeControl" Content="Select all that apply. Will match with applicants answer to 'What types of schools are you considering?' Use to restrict the type of school the scholarship can be used for." runat="server" />
                        <asp:CustomValidator ID="SchoolTypeControlValidator" OnServerValidate="SchoolTypeControlValidator_OnServerValidate" runat="server" ErrorMessage="Select at least one School Type."    />
                      <br />
                      </asp:PlaceHolder>
                </div>

                <div class="vertical-devider"></div>
                <div class="form-iceland two-columns">
                      <p class="form-section-title-small">Program or Expenses Scholarship can be used for</p>
                      <asp:PlaceHolder ID="AcademicProgramContainerControl" runat="server">
                      <label id="AcademicProgramControlLabel" for="AcademicProgramControl">Academic Programs:<span class="requiredAttributeIndicator">*</span></label>
                       <sb:CoolTipInfo ID="CoolTipInfoAcademicProgramControl" Content="Select all that apply. Use to indicate the type of program the scholarship can be used for." runat="server" />
                        <asp:CustomValidator ID="AcademicProgramControlValidator"   OnServerValidate="AcademicProgramControlValidator_OnServerValidate" runat="server"   ErrorMessage="Select at least one Academic Program."    />

                        <br /><sb:FlagEnumCheckBoxList id="AcademicProgramControl" runat="server"  ShowSelectAllButton="True" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
  
                        <br />
                      </asp:PlaceHolder>
                      <br />
                      
                      <asp:PlaceHolder ID="SeekerStatusContainerControl" runat="server">
                      <label id="SeekerStatusControlLabel" for="SeekerStatusControl">Enrollment Status:<span class="requiredAttributeIndicator">*</span></label>
                        <sb:CoolTipInfo ID="CoolTipInfoSeekerStatusControl" Content="Select all that apply. Use to indicate the level of enrollment the scholarship can be used for." runat="server" />
                       <asp:CustomValidator ID="SeekerStatusControlValidator"  OnServerValidate="SeekerStatusControlValidator_OnServerValidate" runat="server"  ErrorMessage="Select at least one Enrollment Status."    />
 
                       <br /><sb:FlagEnumCheckBoxList id="SeekerStatusControl" runat="server"  ShowSelectAllButton="True" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
                        
                      <br />
                      </asp:PlaceHolder>
                      
                      <br />
                      
                      <asp:PlaceHolder ID="TypesOfSupportContainerControl" runat="server">
                      <label id="TypesOfSupportControlLabel"  class ="large-label-size" for="TypesOfSupportControl">Situations the Scholarship will fund:<span class="requiredAttributeIndicator">*</span></label>
                        <sb:CoolTipInfo ID="CoolTipInfoTypesOfSupport" Content="Select all that apply. Use to indicate what expenses the scholarship can be used to cover." runat="server" />
                        <asp:CustomValidator ID="TypesOfSupportControlValidator"  OnServerValidate="TypesOfSupportControlValidator_OnServerValidate" runat="server" ErrorMessage="Select at least one Situations the Scholarship will fund."    />
                    <br />
                        <div class="control-set">
                            <asp:CheckBoxList ID="TypesOfSupport" runat="server"   RepeatDirection="Vertical" RepeatLayout="Flow"  Width="300px"/>
                            <br /><br />
                             <input type="button"  class="ListButton ui-corner-all" ID="SelectAllTypesOfSupporButton"  runat="server" value="Select All"   />
                        </div>
                        
                       
                        <br />
                      </asp:PlaceHolder>
                      <br />
                      
                      
                </div>
        </div>      
        <br />
    </div> 
</div>  

         