﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Common;

namespace ScholarBridge.Web.Provider.Admin
{
	public partial class Default : Page
	{
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IProviderService ProviderService { get; set; }

        private LinkGenerator linkGenerator;
        private LinkGenerator LinkGenerator
        {
            get
            {
                if (linkGenerator == null)
                    linkGenerator = new LinkGenerator();
                return linkGenerator;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.ProviderAdmin); 
            var provider = UserContext.CurrentProvider;
            
                editOrg.Organization = UserContext.CurrentProvider;

            relationshiplist.Relationships = RelationshipService.GetByProvider(provider);
            orgUserList.Users = provider.Users;
            adminDetails.UserToView = UserContext.CurrentOrganization.AdminUser;
            adminDetails1.UserToView = UserContext.CurrentOrganization.AdminUser;
            createUserLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", LinkGenerator.GetFullLink("Provider/Users/Create.aspx?popup=true"));
            addRequestLink.OnClientClick = String.Format("showinpopup('{0}'); return false;", LinkGenerator.GetFullLink("Provider/Relationships/Create.aspx?popup=true"));             
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!User.IsInRole(Role.PROVIDER_ADMIN_ROLE))
            {
                createUserLink.Visible = false;
                editOrg.ViewOnlyMode = true;
            }
        }
        protected void editOrg_OnOrganizationSaved(Organization org)
        {
            org.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ProviderService.Update((Domain.Provider)org);
            SuccessMessageLabel.SetMessage("Your organization details were updated.");
            Response.Redirect("~/Provider/Admin");
        }
	}
}
