﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintExternalApplication.ascx.cs"
    Inherits="ScholarBridge.Web.Common.PrintExternalApplication" %>
    <%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>

<br /><br />
<div class="subsection-big">
   <br />
   <br />
   <p class="sectionHeader">This person was directed to your Application site:<asp:Label ID="Url" runat="server" /> </p>
   <br />
    <table class="viewonlyTable">
        <tr>
            <td><label class="captionMedium">Name:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoName" runat="server" ></asp:Label></td>
        </tr>
         <tr runat="server" id="Tr1">
            <td><label class="captionMedium">Email:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoEmail" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td><label class="captionMedium">Address:</label></td><td><asp:Label CssClass="smallText" ID="PersonalInfoAddress" runat="server" ></asp:Label></td>
        </tr>
    </table>
<br />
<br /> 
        
<%-- <p>TheWashBoard.org is a free web-based tool for Washington students seeking college scholarships. We make scholarship searching simple.</p> 
 <p>Create a profile and review your scholarship matches. Register Today at <a class="GreenLink" href=http://www.theWashBoard.org>theWashBoard.org</a></p>--%>
 <br />
 <p class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</p>
</div>
