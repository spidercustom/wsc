﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Seeker.BuildApplication;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationActivitiesShow : BaseApplicationShow
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != ApplicationToView)
            {
                academicAreas.DataSource = ApplicationToView.AcademicAreas;
                academicAreas.DataBind();
                academicAreasOther.Text = ApplicationToView.AcademicAreaOther;

                careers.DataSource = ApplicationToView.Careers;
                careers.DataBind();
                careersOther.Text = ApplicationToView.CareerOther;

                hobbies.DataSource = ApplicationToView.Hobbies;
                hobbies.DataBind();
                hobbiesOther.Text = ApplicationToView.HobbyOther;

                sports.DataSource = ApplicationToView.Sports;
                sports.DataBind();
                sportsOther.Text = ApplicationToView.SportOther;

                clubs.DataSource = ApplicationToView.Clubs;
                clubs.DataBind();
                clubsOther.Text = ApplicationToView.ClubOther;

                organizations.DataSource = ApplicationToView.MatchOrganizations;
                organizations.DataBind();
                organizationsOther.Text = ApplicationToView.MatchOrganizationOther;

                SetupVisibility(ApplicationToView.Scholarship.SeekerProfileCriteria);
            }
        }

        private void SetupVisibility(SeekerProfileCriteria seeker)
        {

            AcademicAreasRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.AcademicArea);
            CareersRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Career);
            AcademicIntBlock.Visible =
                AcademicAreasRow.Visible ||
                CareersRow.Visible;
            
            OrganizationsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Organization);
            AffiliationTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Company);
            OrgAffilBlock.Visible =
                OrganizationsRow.Visible ||
                AffiliationTypesRow.Visible;

            HobbiesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Honor);
            SportsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Sport);
            ClubsRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.Club);
            GroupsBlock.Visible =
                HobbiesRow.Visible ||
                SportsRow.Visible ||
                ClubsRow.Visible;

            WorkTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.WorkType);
            WorkHoursRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.WorkHour);
            ServiceTypesRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ServiceType);
            ServiceHoursRow.Visible = seeker.Attributes.HasAttribute(SeekerProfileAttribute.ServiceHour);
            WorkServiceBlock.Visible =
                WorkTypesRow.Visible ||
                WorkHoursRow.Visible ||
                ServiceTypesRow.Visible ||
                ServiceHoursRow.Visible;
        }

        public override int ResumeFrom
        {
            get { return WizardStepName.Activities.GetNumericValue(); }
        }
    }
}