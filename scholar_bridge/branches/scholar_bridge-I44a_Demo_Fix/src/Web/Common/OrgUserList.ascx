﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgUserList.ascx.cs" Inherits="ScholarBridge.Web.Common.OrgUserList" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>

<asp:Repeater ID="orgUsers" runat="server" 
    onitemdatabound="orgUsers_ItemDataBound">
    <HeaderTemplate>
    <table class="sortableTable">
        <thead>
            <tr>
                <th>Email</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Confirmed Email?</th>
                <th>Creation Date</th>
                <th>Last Login</th>
                <th>Status</th>
               
            </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton id="linkeditUser" runat="server"><%# DataBinder.Eval(Container.DataItem, "Email") %></asp:LinkButton ></td>
            <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Name"), "FirstName") %></td>
            <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Name"), "LastName")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "IsActive") %></td>
            <td><%# DataBinder.Eval(Container.DataItem, "CreationDate", "{0:MM/dd/yyyy}")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "LastLoginDate", "{0:MM/dd/yyyy}")%></td>
            <td><asp:Label ID="lblStatus" runat="server" /></td>
            
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton id="linkeditUser" runat="server"><%# DataBinder.Eval(Container.DataItem, "Email") %></asp:LinkButton ></td>
            <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Name"), "FirstName")%></td>
            <td><%# DataBinder.Eval(DataBinder.Eval(Container.DataItem, "Name"), "LastName")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "IsActive")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "CreationDate", "{0:MM/dd/yyyy}")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "LastLoginDate", "{0:MM/dd/yyyy}")%></td>
             <td><asp:Label ID="lblStatus" runat="server" /></td>
             
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
        </tbody>
    </table>
    </FooterTemplate>
</asp:Repeater>