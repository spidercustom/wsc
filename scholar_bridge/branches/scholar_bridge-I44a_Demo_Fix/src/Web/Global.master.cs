﻿using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web
{
    public partial class GlobalMasterPage : MasterPage
    {
        public string ImpersonatingUser
        {
            get
            {
                return Session[ImpersonateUser.IMPERSONATING_USER_SESSION_VARIABLE_NAME] as string;
            }
            set
            {
                Session[ImpersonateUser.IMPERSONATING_USER_SESSION_VARIABLE_NAME] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "theWashBoard.org | " + Page.Header.Title;

            if (!Page.IsPostBack)
            {
                string[] names = Page.User.Identity.Name.Split('!');
                if (names.Length > 1)
                {
                    ImpersonatingUser = names[1];
                    FormsAuthentication.SetAuthCookie(names[0], false);

                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (string.IsNullOrEmpty(ImpersonatingUser))
                    ImpersonationPlaceholder.Visible = false;
            }
        }

        protected void stopImpersonationButton_Command(object sender, CommandEventArgs e)
        {
            FormsAuthentication.SignOut();

            string redirUrl = FormsAuthentication.GetRedirectUrl(ImpersonatingUser, false) + Page.Request.PathInfo;
            if (Page.Request.QueryString.HasKeys())
            {
                redirUrl += "?" + Page.Request.QueryString;
            }

            FormsAuthentication.SetAuthCookie(ImpersonatingUser, false);
            AbandonSessionAndRedirect(redirUrl);
        }

        public void AbandonSessionAndRedirect(string path)
        {
            Session.Abandon();
            Response.Redirect(path, true);
        }

    }
}
