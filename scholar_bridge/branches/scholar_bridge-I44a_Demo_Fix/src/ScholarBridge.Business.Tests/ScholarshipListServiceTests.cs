using System.IO;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Data;

namespace ScholarBridge.Business.Tests
{

    [TestFixture]
    public class ScholarshipListServiceTests
    {
        private MockRepository mocks;
        private ScholarshipListService scholarshipListService;
		private IScholarshipDAL scholarshipDAL;

    	private string scholarshipListFilePath = ".\\" + ScholarshipListService.SCHOLARSHIP_LIST_FILE_NAME;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            scholarshipDAL = mocks.StrictMock<IScholarshipDAL>();
        	scholarshipListService = new ScholarshipListService
        	                         	{
        	                         		ScholarshipDAL = scholarshipDAL,
        	                         		ScholarshipListDirectory = ".",
        	                         		ScholarshipListFTPEnabled = false,
        	                         		ScholarshipListFTPLogon = "",
        	                         		ScholarshipListFTPDirectory = "",
											ScholarshipListFTPPassword = "",
											ScholarshipListFTPSite = ""
                                     };
        }

        [TearDown]
        public void Teardown()
        {
			mocks.BackToRecord(scholarshipDAL);
			if (File.Exists(scholarshipListFilePath))
				File.Delete(scholarshipListFilePath);
        }

        [Test]
        public void generate_scholarship_list()
        {
			Expect.Call(scholarshipDAL.GetScholarshipListXml()).Return(scholarshipListXML);
			mocks.ReplayAll();

        	var exists = scholarshipListService.GetScholarshipList();
			Assert.IsNotNull(exists);
		}

		[Test]
		public void save_scholarship_list()
		{
			Expect.Call(scholarshipDAL.GetScholarshipListXml()).Return(null);
			mocks.ReplayAll();

			scholarshipListService.GenerateAndStoreScholarshipList();
			Assert.IsTrue(File.Exists(scholarshipListFilePath));
		}

		[Test]
		[Ignore("Use this test to manually verify the FTP send of the scholarship list.  Needs an available ftp server.")]
		public void save_scholarship_list_with_ftp()
		{
			Expect.Call(scholarshipDAL.GetScholarshipListXml()).Return(scholarshipListXML);
			mocks.ReplayAll();

			scholarshipListService.ScholarshipListFTPDirectory = "_ScholarshipData";
			scholarshipListService.ScholarshipListFTPEnabled = true;
			scholarshipListService.ScholarshipListFTPLogon = "daver";
			scholarshipListService.ScholarshipListFTPPassword = "daver01";
			scholarshipListService.ScholarshipListFTPSite = "ftp://68.179.192.76";
			scholarshipListService.GenerateAndStoreScholarshipList();
			Assert.IsTrue(File.Exists(scholarshipListFilePath));
		}

    	private const string scholarshipListXML =
    		@"
<doc>
  <scholarship>
    <UrlKey>/Daves+Test+Org/2008-2009/My+First+Big+Scholarship</UrlKey>
    <Name>My First Big Scholarship</Name>
    <AcademicYear>2008-2009</AcademicYear>
    <ApplicationStartDate>2010-04-01T00:00:00</ApplicationStartDate>
    <ApplicationDueDate>2010-05-01T00:00:00</ApplicationDueDate>
    <MaximumAmount>6000.0000</MaximumAmount>
    <ProviderName>Daves Test Org</ProviderName>
    <IntermediaryName>Daves Test Intermediary</IntermediaryName>
    <DonorName></DonorName>
    <MissionStatement>Give lots of money to lots of deserving students.x</MissionStatement>
    <SBScholarshipID>37</SBScholarshipID>
  </scholarship>
  <scholarship>
    <UrlKey>/Daves+Test+Org/2008-2009/Daves+Second+Scholarship</UrlKey>
    <Name>Daves Second Scholarship</Name>
    <AcademicYear>2008-2009</AcademicYear>
    <ApplicationStartDate>2009-04-01T00:00:00</ApplicationStartDate>
    <ApplicationDueDate>2009-09-27T00:00:00</ApplicationDueDate>
    <MaximumAmount>5000.0000</MaximumAmount>
    <ProviderName>Daves Test Org</ProviderName>
    <DonorName>BiggyBucks</DonorName>
    <MissionStatement>This is helping hand to underpriviliged computer geeks.   </MissionStatement>
    <SBScholarshipID>38</SBScholarshipID>
  </scholarship>
</doc>";
    }
}
