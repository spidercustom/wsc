﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class RelationshipServiceTests
    {
        private RelationshipService relationshipService;
        private MockRepository mocks;
        private IRelationshipDAL relationshipDAL;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            relationshipDAL = mocks.StrictMock<IRelationshipDAL>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();
            relationshipService = new RelationshipService
                                      {
                                          RelationshipDAL = relationshipDAL,
                                          MessagingService = messagingService,
                                          TemplateParametersService = templateParametersService
                                      };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(relationshipDAL);
            mocks.BackToRecord(messagingService);
            mocks.BackToRecord(templateParametersService);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void approve_throws_exception_if_null_passed()
        {
            relationshipService.Approve(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void get_active_by_organization_throws_exception_if_null_passed()
        {
            relationshipService.GetActiveByOrganization(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void reject_throws_exception_if_null_passed()
        {
            relationshipService.Reject(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void inactivate_relationship_throws_exception_if_null_relationship_passed()
        {
            relationshipService.InActivate(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void createrequest_throws_exception_if_null_relationship_passed()
        {
            relationshipService.CreateRequest(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetByProviderThrowsExceptionIfNullPassed()
        {
            relationshipService.GetByProvider(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetByIntermediaryThrowsExceptionIfNullPassed()
        {
            relationshipService.GetByIntermediary(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_available_intermediaries_throws_exception_if_null()
        {
            relationshipService.FindAvailableIntermediaries(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_available_providers_throws_exception_if_null()
        {
            relationshipService.FindAvailableProviders(null);
            Assert.Fail();
        }

        [Test]
        public void inactivate_relationship_submitter_provider()
        {
            var relationship = CreateValidRelationship();

            Expect.Call(relationshipDAL.Save(relationship)).Return(relationship);
            Expect.Call(() => templateParametersService.RelationshipInActivated(
                                  Arg<Organization>.Is.Equal(relationship.Provider),
                                  Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(
                                  Arg<Message>.Is.Anything,
                                  Arg<MailTemplateParams>.Is.Anything,
                                  Arg<bool>.Is.Anything));

            mocks.ReplayAll();

            relationshipService.InActivate(relationship);
            Assert.IsTrue(relationship.Status == RelationshipStatus.InActive);

            mocks.VerifyAll();
        }

        [Test]
        public void inactivate_relationship_submitter_intermediary()
        {
            var relationship = CreateValidRelationship();
            relationship.Requester = RelationshipRequester.Intermediary;

            Expect.Call(relationshipDAL.Save(relationship)).Return(relationship);
            Expect.Call(() => templateParametersService.RelationshipInActivated(
                                  Arg<Organization>.Is.Equal(relationship.Intermediary),
                                  Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(
                                  Arg<Message>.Is.Anything,
                                  Arg<MailTemplateParams>.Is.Anything,
                                  Arg<bool>.Is.Anything));
            mocks.ReplayAll();

            relationshipService.InActivate(relationship);
            Assert.IsTrue(relationship.Status == RelationshipStatus.InActive);

            mocks.VerifyAll();
        }

        [Test]
        public void create_relationship_request_submitter_provider()
        {
            var relationship = CreateValidRelationship();

            Expect.Call(relationshipDAL.FindByProviderandIntermediary(relationship.Provider, relationship.Intermediary)).Return(null);
            Expect.Call(relationshipDAL.Save(relationship)).Return(relationship);
            Expect.Call(() => templateParametersService.RelationshipRequestCreated(
                                  Arg<Organization>.Is.Equal(relationship.Provider),
                                  Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(() => messagingService.SendMessage(
                                  Arg<Message>.Is.Anything, 
                                  Arg<MailTemplateParams>.Is.Anything, 
                                  Arg<bool>.Is.Anything));

            mocks.ReplayAll();

            relationshipService.CreateRequest(relationship);
            Assert.IsTrue(relationship.Status == RelationshipStatus.Pending);

            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof(DuplicateRelationshipException))]
        public void create_request_throws_exception_if_already_related()
        {
            var relationship = CreateValidRelationship();
            Expect.Call(relationshipDAL.FindByProviderandIntermediary(relationship.Provider, relationship.Intermediary)).Return(relationship);

            mocks.ReplayAll();

            relationshipService.CreateRequest(relationship);

            Assert.Fail();
        }

        private static Relationship CreateValidRelationship()
        {
            var provider = new Provider
                           {
                               Name = "Provider",
                               AdminUser = new User {Id = 3}
                           };
            var intermediary = new Intermediary
                                   {
                                       Name = "intermediary",
                                       AdminUser = new User {Id = 1}
                                   };
            return new Relationship
                       {
                           Id = 1,
                           Provider = provider,
                           Intermediary = intermediary,
                           Status = RelationshipStatus.Active
                       };
        }
    }
}