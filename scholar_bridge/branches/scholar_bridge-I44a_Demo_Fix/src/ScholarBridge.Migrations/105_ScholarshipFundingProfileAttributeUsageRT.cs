﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(105)]
    public class ScholarshipFundingProfileAttributeUsageRT : Migration
    {
        private const string TABLE_NAME = "SBScholarshipFundingProfileAttributeUsageRT";
        private readonly Column[] Columns = new[]
                {
                    new Column("SBScholarshipID", DbType.Int32, ColumnProperty.PrimaryKey), 
                    new Column("SBFundingProfileAttributeIndex", DbType.Int32, ColumnProperty.PrimaryKey),
                    new Column("SBUsageTypeIndex", DbType.Int32)
                };

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                Columns);
        }

        public override void Down()
        {
            Database.RemoveTable(TABLE_NAME);
        }

    }
}
