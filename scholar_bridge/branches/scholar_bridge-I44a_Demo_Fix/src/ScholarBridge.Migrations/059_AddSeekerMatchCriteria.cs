﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(59)]
    public class AddSeekerMatchCriteria : Migration
    {
        readonly Column[] newColumns = new[]
        {
           new Column("MatchCriteriaStudentGroups", DbType.Int32, ColumnProperty.NotNull, 0), 
           new Column("MatchCriteriaSchoolTypes", DbType.Int32, ColumnProperty.NotNull, 0), 
           new Column("MatchCriteriaAcademicPrograms", DbType.Int32, ColumnProperty.NotNull, 0), 
           new Column("MatchCriteriaSeekerStatuses", DbType.Int32, ColumnProperty.NotNull, 0), 
           new Column("MatchCriteriaProgramLengths", DbType.Int32, ColumnProperty.NotNull, 0), 
           new Column("MatchCriteriaGenders", DbType.Int32, ColumnProperty.NotNull, 0), 
           new Column("MatchCriteriaFirstGeneration", DbType.Boolean, ColumnProperty.NotNull, false), 
           new Column("MatchCriteriaHonors", DbType.Boolean, ColumnProperty.NotNull, false), 
           new Column("MatchCriteriaAPCreditsEarned", DbType.Int32, ColumnProperty.NotNull, 0), 
        };

        public override void Up()
        {
            foreach (var column in newColumns)
            {
                Database.AddColumn("SBScholarship", column);
            }
        }

        public override void Down()
        {
            foreach (var column in newColumns)
            {
                Database.RemoveColumn("SBScholarship", column.Name);
            }
        }
    }
}
