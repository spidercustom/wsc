using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(180)]
    public class AddApplicationCareerRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBApplicationCareerRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBApplication"; }
        }

        protected override string SecondTableName
        {
            get { return "SBCareerLUT"; }
        }
    }
}