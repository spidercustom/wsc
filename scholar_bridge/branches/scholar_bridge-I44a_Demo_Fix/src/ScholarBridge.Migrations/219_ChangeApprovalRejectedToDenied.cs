﻿using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(219)]
    public class _219_ChangeApprovalRejectedToDenied : Migration
    {
        public override void Up()
        {
        	Database.ExecuteNonQuery(
        		"update SBOrganization set ApprovalStatus = 'Denied' where ApprovalStatus = 'Rejected'");
        }

        public override void Down()
        {
        	Database.ExecuteNonQuery(
				"update SBOrganization set ApprovalStatus = 'Rejected' where ApprovalStatus = 'Denied'");
        }
    }
}