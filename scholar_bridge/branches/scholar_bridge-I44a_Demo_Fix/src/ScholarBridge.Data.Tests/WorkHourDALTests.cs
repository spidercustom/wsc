﻿using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class WorkHourDALTests : LookupTestBase<LookupDAL<WorkHour>, WorkHour>
    {
    }
}