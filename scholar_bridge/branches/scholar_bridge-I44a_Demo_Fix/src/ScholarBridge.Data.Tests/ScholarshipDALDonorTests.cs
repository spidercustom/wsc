﻿using System;
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipDALDonorTests : TestBase
    {
        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        private User user;
        private Provider provider;
        private ScholarshipStage stage;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
            stage = ScholarshipStage.NotActivated;
        }

        [Test]
        public void validate_donor_can_be_saved_and_retrived()
        {
            var scholarship = ScholarshipDALTest.CreateTestObject(user, provider, stage);
            PopulateDonor(scholarship.Donor);
            scholarship = ScholarshipDAL.Save(scholarship);

            var retrivedScholarship = ScholarshipDAL.FindById(scholarship.Id);
            AssertScholarshipDonor(scholarship.Donor, retrivedScholarship.Donor);
        }

        private static void AssertScholarshipDonor(ScholarshipDonor expected, ScholarshipDonor actual)
        {
            if (expected == null) throw new ArgumentNullException("expected");
            if (actual == null) throw new ArgumentNullException("actual");
            //TODO: Implement Equals?
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Profile , actual.Profile );
             
        }

        public ScholarshipDonor PopulateDonor(ScholarshipDonor donor)
        {
            if (donor == null) throw new ArgumentNullException("donor");
            return PopulateDonor(StateDAL, donor);
        }

        public static ScholarshipDonor PopulateDonor(StateDAL stateDAL, ScholarshipDonor donor)
        {
            if (stateDAL == null) throw new ArgumentNullException("stateDAL");
            if (donor == null) throw new ArgumentNullException("donor");

            var washington = stateDAL.FindByAbbreviation("WA");
            donor.Name = "ScholarshipDonor Name";
            donor.Profile = "profile1";
             
            return donor;
        }
    }
}
