﻿using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{

	[TestFixture]
	public class FaqDALTests : TestBase
	{

		public FaqDAL FaqDAL { get; set; }
		public UserDAL UserDAL { get; set; }

		private User user;
		protected override void OnSetUpInTransaction()
		{
			user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
		}

		[Test]
		public void can_insert_faq()
		{
			var faq = InsertFaq(FaqDAL, user);

			Assert.IsNotNull(faq);
			Assert.IsTrue(faq.Id > 0);
		}

		[Test]
		public void can_delete_faqs()
		{
			var faq = InsertFaq(FaqDAL, user);
			FaqDAL.Delete(faq);
			var found = FaqDAL.FindById(faq.Id);
			Assert.IsNull(found);
		}

		[Test]
		public void can_find_all_faqs()
		{
			var all = FaqDAL.FindAll("Question");
			var currentCount = all.Count;
			InsertFaq(FaqDAL, user);

			InsertFaq(FaqDAL, user);

			all = FaqDAL.FindAll("Question");
			Assert.IsNotNull(all);
			Assert.AreEqual(currentCount + 2, all.Count);
		}

		public static Faq InsertFaq(IFaqDAL faqDal, User user)
		{
			var faq = new Faq
			          	{
			          		Question = "what is life?",
			          		Answer = "anti-entropy.",
			          		FaqType = FaqType.Seeker,
							LastUpdate = new ActivityStamp(user)
						};
			faq = faqDal.Insert(faq);
			return faq;
		}
	}
}
