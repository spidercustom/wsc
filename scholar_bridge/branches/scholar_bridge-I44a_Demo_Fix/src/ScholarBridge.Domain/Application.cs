using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.Resources;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Domain
{
    public class Application : CriteriaCountBase
    {
		public const string APPLICATION_SUMBISSION_RULESET = "Application-Submission";

		#region C'tors

        public Application(ApplicationType t)
        {
            ApplicationType = t;
            InitializeMembers();
        }
        public Application()
        {
            ApplicationType = ApplicationType.Internal;
            InitializeMembers();
        }

        public Application(Match m,ApplicationType t) : this(t)
        {
            Scholarship = m.Scholarship;
            Seeker = m.Seeker;
            
            InitializeMembers();
        }

		#endregion

        private void InitializeMembers()
        {
           
            
            Attachments = new List<Attachment>();
            Ethnicities = new List<Ethnicity>();
            Religions = new List<Religion>();
            Sports = new List<Sport>();

            AcademicAreas = new List<AcademicArea>();
            Careers = new List<Career>();
            Hobbies = new List<SeekerHobby>();
            Clubs = new List<Club>();
            MatchOrganizations = new List<SeekerMatchOrganization>();
            Companies = new List<Company>();
             
            SupportedSituation = new SupportedSituation();
            QuestionAnswers = new List<ApplicationQuestionAnswer>(); 
        }

        public virtual int Id { get; set; }
        public virtual Scholarship Scholarship { get; set; }
        public virtual Seeker Seeker { get; set; }
        public virtual ApplicationStage Stage { get; set; }
        public virtual ApplicationType ApplicationType { get; set; }
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should be between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? SubmittedDate { get; set; }
        public virtual bool Finalist { get; set; }
        public virtual AwardStatus AwardStatus { get; set; }
         
        
        [DateTimeRangeValidator("1753-01-01T00:00:00", RangeBoundaryType.Inclusive, "9999-12-31T00:00:00", RangeBoundaryType.Inclusive, MessageTemplate = "Date should be between Jan 1, 1753 to Dec 31, 9999")]
        public virtual DateTime? DateOfBirth { get; set; }

        public virtual bool FirstGeneration { get; set; }

        public virtual Genders Gender { get; set; }

        public virtual CurrentSchool CurrentSchool { get; set; }

		[StringLengthValidator(0, 1500, MessageTemplate = "Your Personal Statement must be 1500 characters or less.")]
		public virtual string PersonalStatement { get; set; }

		[StringLengthValidator(0, 1500, MessageTemplate = "Your Financial Challenge statement must be 1500 characters or less.")]
		public virtual string MyChallenge { get; set; }

		[StringLengthValidator(0, 150, MessageTemplate = "Your Gift statement must be 150 characters or less")]
		public virtual string MyGift { get; set; }

        public virtual SeekerAcademics SeekerAcademics { get; set; }

		[StringLengthValidator(0, 100, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Words { get; set; }
		[StringLengthValidator(0, 100, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string Skills { get; set; }

        public virtual IList<Ethnicity> Ethnicities { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string EthnicityOther { get; set; }

        public virtual IList<Religion> Religions { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string ReligionOther { get; set; }

        public virtual IList<Sport> Sports { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string SportOther { get; set; }

        public virtual IList<AcademicArea> AcademicAreas { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string AcademicAreaOther { get; set; }

        public virtual IList<Career> Careers { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string CareerOther { get; set; }

        public virtual IList<SeekerHobby> Hobbies { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string HobbyOther { get; set; }

        public virtual IList<Club> Clubs { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string ClubOther { get; set; }

        public virtual IList<SeekerMatchOrganization> MatchOrganizations { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string MatchOrganizationOther { get; set; }

        public virtual IList<Company> Companies { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string CompanyOther { get; set; }

		public virtual bool IsEslEll { get; set; }

		public virtual bool IsWorking { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string WorkHistory { get; set; }

        public virtual bool IsService { get; set; }
		[StringLengthValidator(0, 250, MessageTemplateResourceName = "StringLengthLessThanMessage", MessageTemplateResourceType = typeof(MessageTextResource))]
		public virtual string ServiceHistory { get; set; }
		
        public virtual SupportedSituation SupportedSituation { get; set; }
        
        public virtual bool? HasFAFSACompleted { get; set; }
        public virtual decimal? ExpectedFamilyContribution { get; set; }

        public virtual IList<Attachment> Attachments { get; protected set; }
        public virtual IList<ApplicationQuestionAnswer> QuestionAnswers { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual bool HasAttachments { get { return Attachments != null && Attachments.Count > 0; } }
        public virtual void CopyFromSeekerandScholarship()
        {
            var seeker = Seeker;
            if (seeker == null)
                throw new  NullReferenceException("Seeker");

            var scholarship = Scholarship;

            if (scholarship == null)
                throw new  NullReferenceException("Scholarship");

            
            
            AcademicAreaOther = seeker.AcademicAreaOther;
            AcademicAreas = new List<AcademicArea>(seeker.AcademicAreas);
             
            CompanyOther = seeker.CompanyOther;
            Companies =new List<Company>(seeker.Companies);
            
            CareerOther = seeker.CareerOther;
            Careers =new List<Career>( seeker.Careers);
            ClubOther = seeker.ClubOther;
            Clubs =new List<Club>(seeker.Clubs);
            
            CurrentSchool = seeker.CurrentSchool;
            DateOfBirth = seeker.DateOfBirth;
             
            Ethnicities = new List<Ethnicity>(seeker.Ethnicities);
            EthnicityOther = seeker.EthnicityOther;
            FirstGeneration = seeker.FirstGeneration;
            Gender = seeker.Gender;
            Hobbies = new List<SeekerHobby>(seeker.Hobbies);
            HobbyOther = seeker.HobbyOther;
            MatchOrganizations = new List<SeekerMatchOrganization>(seeker.MatchOrganizations);
            MatchOrganizationOther = seeker.MatchOrganizationOther;
             
            MyChallenge = seeker.MyChallenge;
            MyGift = seeker.MyGift;
            ExpectedFamilyContribution = seeker.ExpectedFamilyContribution;
            HasFAFSACompleted = seeker.HasFAFSACompleted;
            PersonalStatement = seeker.PersonalStatement;
             
            ReligionOther = seeker.ReligionOther;
            Religions = new List<Religion>(seeker.Religions);
            SeekerAcademics =(SeekerAcademics) seeker.SeekerAcademics.Clone();
            SupportedSituation.ResetTypesOfSupport(seeker.SupportedSituation.TypesOfSupport); 
            Skills = seeker.Skills;
            SportOther = seeker.SportOther;
            Sports=new List<Sport>(seeker.Sports);
            Words = seeker.Words;
            ServiceHistory = seeker.ServiceHistory;
            WorkHistory = seeker.WorkHistory;
            IsService = seeker.IsService;
            IsWorking = seeker.IsWorking;
        	IsEslEll = seeker.IsEslEll;
            
       
            //copy attachments

            foreach (Attachment a in seeker.Attachments )
            {
                 if (a.IncludeWithApplications)
                 {
                    var attachment = new Attachment
                                     	{

                                             Bytes = a.Bytes,
                                             Comment = a.Comment,
                                             MimeType = a.MimeType,
                                             Name = a.Name,
                                             UniqueName = a.UniqueName,
                                             LastUpdate=new ActivityStamp(seeker.User)
                                         };

                    Attachments.Add(attachment);
                 }
            }
            //copy questions from scholarship
            foreach (ScholarshipQuestion q in scholarship.AdditionalQuestions)
            {

                var qans = new ApplicationQuestionAnswer
                           	{
                                   Question = q,
                                   Application = this,
                                   AnswerText = "",
                                   LastUpdate = new ActivityStamp(seeker.User)


                               };
                QuestionAnswers.Add(qans);
            }
        }

		public virtual ApplicationStatus ApplicationStatus
		{
			get
			{
                if (ApplicationType == ApplicationType.External)
                    return ApplicationStatus.ApplyatProviderSite;

                DateTime applicationDueDate = Scholarship.ApplicationDueDate.Value.AddDays(1).AddSeconds(-1);
				if (Scholarship.Stage == ScholarshipStage.Awarded || Scholarship.Stage == ScholarshipStage.Awarded)
					return ApplicationStatus.Closed;

				var result = ApplicationStatus.Unknown;

				if (Stage == ApplicationStage.Submitted)
				{

					if (DateTime.Today < Scholarship.ApplicationStartDate) result = ApplicationStatus.Submitted;

					if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate, applicationDueDate))
						result = ApplicationStatus.Submitted;

					if (DateTime.Today >= applicationDueDate) result = ApplicationStatus.BeingConsidered;
					if (Scholarship.AwardPeriodClosed.HasValue) result = ApplicationStatus.BeingConsidered;
				}

				else
				{
					if (DateTime.Today < Scholarship.ApplicationStartDate) result = ApplicationStatus.Applying;

					if (DateTime.Today.IsBetween(Scholarship.ApplicationStartDate, applicationDueDate))
						result = ApplicationStatus.Applying;

					if (DateTime.Today >= applicationDueDate) result = ApplicationStatus.Closed;

					if (Scholarship.AwardPeriodClosed.HasValue) result = ApplicationStatus.Closed;
				}

				return result;
			}
		}



        public virtual void Submit()
        {
            if (Stage != ApplicationStage.Submitted)
            {
                Stage = ApplicationStage.Submitted;
                SubmittedDate = DateTime.Now; 
            }
        }

        public virtual bool CanEdit()
        {
            return Stage != ApplicationStage.Submitted;
        }

        public virtual ValidationResults ValidateSubmission()
        {
        	var results = Validate(APPLICATION_SUMBISSION_RULESET);
			 
            
			CheckForRequiredFields(results);
            return results;
        }

    	private void CheckForRequiredFields(ValidationResults results)
    	{
			 
                     

    		ValidationResults nameResults = Seeker.Name.ValidateAsRequired();
			if (nameResults.Count > 0)
				results.AddResult(CreateResult("Name", "Your name is not complete.  Please complete your profile before submitting applications."));

            if (Seeker.Address != null)
            {
                ValidationResults addressResults = Seeker.Address.ValidateAsRequired();
                if (addressResults.Count > 0)
                    results.AddResult(CreateResult("Address",
                                                   "Your address is not complete.  Please complete your profile before submitting applications."));
            }else
            {
                    results.AddResult(CreateResult("Address",
                                                   "Your address is not complete.  Please complete your profile before submitting applications."));
            }


    	    //validate for required questions
            foreach (ApplicationQuestionAnswer  q in QuestionAnswers)
            {
                if (q.Question.IsRequired )
                {
                    if (q.AnswerText.Length ==0)
						results.AddResult(CreateResult(q.Question.Id.ToString(), "Additional Requirement Question requires an answer:  " + q.Question.QuestionText)
								);

                }
            }


    	    foreach (ScholarshipAttributeUsage<SeekerProfileAttribute> attribute in Scholarship.SeekerProfileCriteria.Attributes)
    		{
    			if (attribute.UsageType == ScholarshipAttributeUsageType.Minimum)
    			{
					switch (attribute.Attribute)
					{
						
						case SeekerProfileAttribute.AcademicArea:
							if (!MatchFoundInScholarshipList(AcademicAreas.ToArray(), Scholarship.SeekerProfileCriteria.AcademicAreas.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.AcademicArea), 
													string.Format("To qualify you must select one of the following Academic Areas: {0}",
							                                    ConcatinateListValues(Scholarship.SeekerProfileCriteria.AcademicAreas.ToArray())))
								);
							break;

						case SeekerProfileAttribute.AcademicProgram:
							if ((SeekerAcademics.AcademicPrograms.GetNumericValue() & Scholarship.SeekerProfileCriteria.AcademicPrograms.GetNumericValue()) == 0)
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.AcademicProgram),
													string.Format("To qualify your intended academic program must be one of the following: {0}", ConcatinateEnumValues(Scholarship.SeekerProfileCriteria.AcademicPrograms)))
								);
							break;

						case SeekerProfileAttribute.ACTEnglish:
							if (SeekerAcademics.ACTScore == null || SeekerAcademics.ACTScore.English == null || SeekerAcademics.ACTScore.English == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.ACTEnglish),
																		"ACT English Score"));
							else 
								if (SeekerAcademics.ACTScore.English > Scholarship.SeekerProfileCriteria.ACTScore.English.Maximum ||
									SeekerAcademics.ACTScore.English < Scholarship.SeekerProfileCriteria.ACTScore.English.Minimum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.ACTEnglish),
													string.Format("To qualify your ACT English score must be between {0} and {1}", Scholarship.SeekerProfileCriteria.ACTScore.English.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.English.Maximum))
									);
							break;

						case SeekerProfileAttribute.ACTMathematics:
							if (SeekerAcademics.ACTScore == null || SeekerAcademics.ACTScore.Mathematics == null || SeekerAcademics.ACTScore.Mathematics == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.ACTMathematics), "ACT Mathematics Score"));
							else
								if (SeekerAcademics.ACTScore.Mathematics > Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Maximum ||
									SeekerAcademics.ACTScore.Mathematics < Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Minimum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.ACTMathematics),
													string.Format("To qualify your ACT Mathematics score must be between {0} and {1}", Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.Mathematics.Maximum))
									);
							break;

						case SeekerProfileAttribute.ACTReading:
							if (SeekerAcademics.ACTScore == null || SeekerAcademics.ACTScore.Reading == null || SeekerAcademics.ACTScore.Reading == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.ACTReading), "ACT Reading Score"));
							else
								if (SeekerAcademics.ACTScore.Reading > Scholarship.SeekerProfileCriteria.ACTScore.Reading.Maximum ||
									SeekerAcademics.ACTScore.Reading < Scholarship.SeekerProfileCriteria.ACTScore.Reading.Minimum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.ACTReading),
													string.Format("To qualify your ACT Reading score must be between {0} and {1}", Scholarship.SeekerProfileCriteria.ACTScore.Reading.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.Reading.Maximum))
									);
							break;

						case SeekerProfileAttribute.ACTScience:
							if (SeekerAcademics.ACTScore == null || SeekerAcademics.ACTScore.Science == null || SeekerAcademics.ACTScore.Science == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.ACTScience), "ACT Science Score"));
							else
								if (SeekerAcademics.ACTScore.Science > Scholarship.SeekerProfileCriteria.ACTScore.Science.Maximum ||
									SeekerAcademics.ACTScore.Science < Scholarship.SeekerProfileCriteria.ACTScore.Science.Minimum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.ACTScience),
													string.Format("To qualify your ACT Science score must be between {0} and {1}", Scholarship.SeekerProfileCriteria.ACTScore.Science.Minimum, Scholarship.SeekerProfileCriteria.ACTScore.Science.Maximum))
									);
							break;

						case SeekerProfileAttribute.APCreditsEarned:
							if (SeekerAcademics.APCredits)
							{
								if (string.IsNullOrEmpty(SeekerAcademics.APCreditsDetail))
									results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.APCreditsEarned), "AP Credits Detail Information"));
							}
							else
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.APCreditsEarned), "AP Credits Information"));
							break;

						case SeekerProfileAttribute.Career:
							if (!MatchFoundInScholarshipList(Careers.ToArray(), Scholarship.SeekerProfileCriteria.Careers.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.Career),
													string.Format("To qualify you must select one of the following Careers: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Careers.ToArray())))
								);
							break;

						case SeekerProfileAttribute.City:
                            if (string.IsNullOrEmpty(Seeker.Address.GetCity) && string.IsNullOrEmpty(Seeker.AddressCity))
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.City), "Your Current City"));
							else
								if (!Scholarship.SeekerProfileCriteria.Cities.Contains(Seeker.Address.City))
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.City),
													string.Format("This scholarship is specific to residents of the following cities: {0}",
																	ConcatinateListValues(Scholarship.SeekerProfileCriteria.Cities.ToArray())))
									);
							break;

						case SeekerProfileAttribute.ClassRank:
							if (SeekerAcademics.ClassRank == null)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.ClassRank), "Your HS Class Rank"));
							else
								if (!Scholarship.SeekerProfileCriteria.ClassRanks.Contains(SeekerAcademics.ClassRank))
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.ClassRank),
													string.Format("This scholarship is specific to HS students with the following class rank(s): {0}",
																	ConcatinateListValues(Scholarship.SeekerProfileCriteria.ClassRanks.ToArray())))
									);
							break;

						case SeekerProfileAttribute.Club:
							if (!MatchFoundInScholarshipList(Clubs.ToArray(), Scholarship.SeekerProfileCriteria.Clubs.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.Club),
													string.Format("To qualify you must participate in one of following Clubs: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Clubs.ToArray())))
								);
							break;

						case SeekerProfileAttribute.College:
							if ((SeekerAcademics.CollegesApplied == null || SeekerAcademics.CollegesApplied.Count == 0) && string.IsNullOrEmpty(SeekerAcademics.CollegesAppliedOther))
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.College), "Your intended College"));
							break;

						 

						case SeekerProfileAttribute.Company:
							if (!MatchFoundInScholarshipList(Companies.ToArray(), Scholarship.SeekerProfileCriteria.Companies.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.Company),
													string.Format("To qualify you must be associated with one of following Companies: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Companies.ToArray())))
								);
							break;

						case SeekerProfileAttribute.County:

							if (Seeker.County == null)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.County), "Your County of residence"));
							else
								if (!Scholarship.SeekerProfileCriteria.Counties.Contains(Seeker.County))
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.County),
													string.Format("This scholarship is specific to residents of the following counties: {0}",
																	ConcatinateListValues(Scholarship.SeekerProfileCriteria.Counties.ToArray())))
									);
							break;

						case SeekerProfileAttribute.EslEll:
							if (!IsEslEll)
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.EslEll),
													"To qualify English must be a second language or you must be an English Language Learner.")
								);
							break;

						case SeekerProfileAttribute.Ethnicity:
							if (!MatchFoundInScholarshipList(Ethnicities.ToArray(), Scholarship.SeekerProfileCriteria.Ethnicities.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.Ethnicity),
													string.Format("To qualify you must be of one of the following Ethnicities: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Ethnicities.ToArray())))
								);
							break;

						case SeekerProfileAttribute.FirstGeneration:
							if (!FirstGeneration)
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.FirstGeneration),
													"To qualify you must be the first generation in your family to go to college")
								);
							break;

						case SeekerProfileAttribute.Gender:
							if (Gender.GetNumericValue() < 2)
								results.AddResult(
									CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.Gender), "Gender (Male or Female")
								);
							else
								if ((Gender.GetNumericValue() & Scholarship.SeekerProfileCriteria.Genders.GetNumericValue()) == 0)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.Gender),
													string.Format("To qualify you must be {0}", ConcatinateEnumValues(Scholarship.SeekerProfileCriteria.Genders)))
									);
							break;

						case SeekerProfileAttribute.GPA:
							if (!SeekerAcademics.GPA.HasValue || SeekerAcademics.GPA.Value == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.GPA), "Your GPA"));
							else
							{
								if (SeekerAcademics.GPA < Scholarship.SeekerProfileCriteria.GPA.Minimum || SeekerAcademics.GPA > Scholarship.SeekerProfileCriteria.GPA.Maximum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.GPA),
													string.Format("To qualify your GPA must be between {0} and {1}", Scholarship.SeekerProfileCriteria.GPA.Minimum, Scholarship.SeekerProfileCriteria.GPA.Maximum))
									);
							}
							break;

						case SeekerProfileAttribute.HighSchool:
							if (CurrentSchool == null || CurrentSchool.HighSchool == null && string.IsNullOrEmpty(CurrentSchool.HighSchoolOther))
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.HighSchool), "Your High School of Graduation"));
							else
							{
								if (!Scholarship.SeekerProfileCriteria.HighSchools.Contains(CurrentSchool.HighSchool))
									CreateResult(GetAttributeName(SeekerProfileAttribute.HighSchool),
													string.Format(
													"To qualify you must be or have been a student at the following high school(s): {0}",
													ConcatinateListValues(Scholarship.SeekerProfileCriteria.HighSchools.ToArray()))
										);
							}
							break;

						case SeekerProfileAttribute.Honor:
							if (!SeekerAcademics.Honors)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.Honor), "Honors Classes"));
							else
							{
								if (string.IsNullOrEmpty(SeekerAcademics.HonorsDetail))
									CreateResult(GetAttributeName(SeekerProfileAttribute.Honor),
													string.Format(
													"To qualify you must describe your Honors Classes")
										);

							}
							break;

						case SeekerProfileAttribute.IBCreditsEarned:
							if (SeekerAcademics.IBCredits)
							{
								if (string.IsNullOrEmpty(SeekerAcademics.IBCreditsDetail))
									results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.IBCreditsEarned), "IB Credits Detail Information"));
							}
							else
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.IBCreditsEarned), "IB Credits Information"));
							break;

						case SeekerProfileAttribute.Organization:
							if (!MatchFoundInScholarshipList(MatchOrganizations.ToArray(), Scholarship.SeekerProfileCriteria.Organizations.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.Organization),
													string.Format("To qualify you must be associated with one of the following Organizations: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Organizations.ToArray())))
								);
							break;

						case SeekerProfileAttribute.Religion:
							if (!MatchFoundInScholarshipList(Religions.ToArray(), Scholarship.SeekerProfileCriteria.Religions.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.Religion),
													string.Format("To qualify you must practice one of the following Religions: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Religions.ToArray())))
								);
							break;

						case SeekerProfileAttribute.RunningStart:
							if (SeekerAcademics.RunningStart)
							{
								if (string.IsNullOrEmpty(SeekerAcademics.RunningStartDetail))
									results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.RunningStart), "Running Start Detail Information"));
							}
							else
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.RunningStart), "Running Start Information"));
							break;

						case SeekerProfileAttribute.SATCriticalReading:
							if (SeekerAcademics.SATScore == null || SeekerAcademics.SATScore.CriticalReading == null || SeekerAcademics.SATScore.CriticalReading == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.SATCriticalReading), "SAT Critical Reading Score"));
							else
								if (SeekerAcademics.SATScore.CriticalReading > Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Maximum ||
									SeekerAcademics.SATScore.CriticalReading < Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Minimum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.SATCriticalReading),
													string.Format("To qualify your SAT Critical Reading score must be between {0} and {1}",
															Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Minimum,
															Scholarship.SeekerProfileCriteria.SATScore.CriticalReading.Maximum))
									);
							break;

						case SeekerProfileAttribute.SATMathematics:
							if (SeekerAcademics.SATScore == null || SeekerAcademics.SATScore.Mathematics == null || SeekerAcademics.SATScore.Mathematics == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.SATMathematics), "SAT Mathematics Score"));
							else
								if (SeekerAcademics.SATScore.Mathematics > Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Maximum ||
									SeekerAcademics.SATScore.Mathematics < Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Minimum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.SATMathematics),
													string.Format("To qualify your SAT Mathematics score must be between {0} and {1}",
															Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Minimum,
															Scholarship.SeekerProfileCriteria.SATScore.Mathematics.Maximum))
									);
							break;

						case SeekerProfileAttribute.SATWriting:
							if (SeekerAcademics.SATScore == null || SeekerAcademics.SATScore.Writing == null || SeekerAcademics.SATScore.Writing == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.SATWriting), "SAT Writing Score"));
							else
								if (SeekerAcademics.SATScore.Writing > Scholarship.SeekerProfileCriteria.SATScore.Writing.Maximum ||
									SeekerAcademics.SATScore.Writing < Scholarship.SeekerProfileCriteria.SATScore.Writing.Minimum)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.SATWriting),
													string.Format("To qualify your SAT Writing score must be between {0} and {1}",
															Scholarship.SeekerProfileCriteria.SATScore.Writing.Minimum,
															Scholarship.SeekerProfileCriteria.SATScore.Writing.Maximum))
									);
							break;

						case SeekerProfileAttribute.SchoolType:
							if (SeekerAcademics.SchoolTypes.GetNumericValue() < 1)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.SchoolType), "School Type You are Considering"));
							else
								if ((SeekerAcademics.SchoolTypes.GetNumericValue() & Scholarship.SeekerProfileCriteria.SchoolTypes.GetNumericValue()) == 0)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.SchoolType),
													string.Format("To qualify you must select one of the following School Types: {0}", ConcatinateEnumValues(Scholarship.SeekerProfileCriteria.SchoolTypes)))
									);
							break;

						case SeekerProfileAttribute.SeekerHobby:
							if (!MatchFoundInScholarshipList(Hobbies.ToArray(), Scholarship.SeekerProfileCriteria.Hobbies.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.SeekerHobby),
													string.Format("To qualify you must participate one of the following Hobbies: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Hobbies.ToArray())))
								);
							break;

						// Seeker skill is no longer used in match
						//*
						//case SeekerProfileAttribute.SeekerSkill:
						//    // not validated yet
						//    break;

						case SeekerProfileAttribute.SeekerStatus:
							if (SeekerAcademics.SeekerStatuses.GetDisplayNames().Length == 0)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.SeekerStatus), "Current Enrollment Status"));
							else
								if ((SeekerAcademics.SeekerStatuses.GetNumericValue() & Scholarship.SeekerProfileCriteria.SeekerStatuses.GetNumericValue()) == 0)
									results.AddResult(
										CreateResult(GetAttributeName(SeekerProfileAttribute.SeekerStatus),
													string.Format("To qualify your status must be one of the following: {0}", ConcatinateEnumValues(Scholarship.SeekerProfileCriteria.SeekerStatuses)))
									);
							break;

						//* 5 words not currently used in match
						//case SeekerProfileAttribute.SeekerVerbalizingWord:
						// //not used in match
						//    break;

						case SeekerProfileAttribute.ServiceType:
							if (!IsService)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.ServiceType), "Service/Volunteering"));
							else
							{
								if (string.IsNullOrEmpty(ServiceHistory))
									CreateResult(GetAttributeName(SeekerProfileAttribute.ServiceType),
													string.Format(
													"To qualify you must describe your Service/Volunteering involvement.")
										);

							}
							break;

						case SeekerProfileAttribute.Sport:
							if (!MatchFoundInScholarshipList(Sports.ToArray(), Scholarship.SeekerProfileCriteria.Sports.ToArray()))
								results.AddResult(
									CreateResult(GetAttributeName(SeekerProfileAttribute.Sport),
													string.Format("To qualify you must participate one of the following Sports: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.Sports.ToArray())))
								);
							break;

						case SeekerProfileAttribute.State:
                            if (Seeker.Address.State == null)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.State), "State Selection"));
							else
							{
								if (!Scholarship.SeekerProfileCriteria.States.Contains(Seeker.Address.State))
								{
									CreateResult(GetAttributeName(SeekerProfileAttribute.State),
													string.Format("To qualify you must reside in one of the following States: {0}",
																ConcatinateListValues(Scholarship.SeekerProfileCriteria.States.ToArray()))
									);
								}
							}
							break;

						case SeekerProfileAttribute.WorkType:
							if (!IsWorking)
								results.AddResult(CreateRequiredResult(GetAttributeName(SeekerProfileAttribute.WorkType), "Work/Employment"));
							else
							{
								if (string.IsNullOrEmpty(ServiceHistory))
									CreateResult(GetAttributeName(SeekerProfileAttribute.WorkType),
													string.Format(
													"To qualify you must describe your Work/Employment history.")
										);

							}
							break;

						default:
							break;
					}
				}
    		}
			foreach (FundingProfileAttributeUsage attribute in Scholarship.FundingProfile.AttributesUsage)
			{
				if (attribute.UsageType == ScholarshipAttributeUsageType.Minimum)
				{
					switch (attribute.Attribute)
					{
						case FundingProfileAttribute.Need:
							if (string.IsNullOrEmpty(MyChallenge))
								results.AddResult(CreateRequiredResult("FinancialNeed", "Your Financial Need Statement"));
							break;
						default:
							break;
					}
				}
			}
			if (SupportedSituation.TypesOfSupport.Count < 1)
				results.AddResult(CreateRequiredResult("TypeOfSupport", "Type of Support Needed"));
			else
			{
				if (!MatchFoundInScholarshipList(SupportedSituation.TypesOfSupport.ToArray(), Scholarship.FundingProfile.SupportedSituation.TypesOfSupport.ToArray()))
					results.AddResult(
						CreateResult("TypesOfSupport",
										string.Format("To qualify your need must in the following types of support: {0}",
													ConcatinateListValues(Scholarship.FundingProfile.SupportedSituation.TypesOfSupport.ToArray())))
					);
			}

    	}

		private static string GetAttributeName(SeekerProfileAttribute seekerProfileAttribute)
		{
			return Enum.GetName(typeof(SeekerProfileAttribute), seekerProfileAttribute);
		}

		private static string ConcatinateEnumValues(Enum enumFlagField)
		{

			string[] flagValues = enumFlagField.GetDisplayNames();
			string items = string.Empty;
			foreach (var item in flagValues)
			{
				if (items != string.Empty)
					items += ", ";
				items += item;
			}
			return items;
		}

    	private ValidationResult CreateResult(string key, string resultMessage)
    	{
    		return new ValidationResult(resultMessage, this, key, "", null);
    	}

		private static bool MatchFoundInScholarshipList(IEnumerable<ILookup> seekerList, IEnumerable<ILookup> scholarshipList)
		{
			bool matchFound = false;
			foreach (var seekerItem in seekerList)
			{
				if (scholarshipList.Contains(seekerItem))
				{
					matchFound = true;
					break;
				}
			}
			return matchFound;
		}

    	private static string ConcatinateListValues(IEnumerable<ILookup> iList)
		{
			string items = string.Empty;
			foreach (var item in iList)
			{
				if (items != string.Empty)
					items += ", ";
				items += item.Name;
			}
			return items;
		}

		private ValidationResult CreateRequiredResult(string key, string requiredAttribute)
		{
			return new ValidationResult(string.Format("{0} is required for this scholarship", requiredAttribute), this, key, "", null);
		}

    	private ValidationResults Validate(string ruleSet)
        {
            var validator = ValidationFactory.CreateValidator<Application >(ruleSet);
            return validator.Validate(this);
        }
    }
}