﻿using System.Web.UI;

namespace ScholarBridge.Web.Wizards
{
    public abstract class WizardStepUserControlBase<T> : UserControl, IWizardStepControl<T>
    {

        #region IWizardStepControl<T> Members
        public abstract bool ValidateStep();
        public abstract void Save();
        public abstract void PopulateObjects();

        public virtual IWizardStepsContainer<T> Container { get; set; }
        public virtual void Activated()
        {
        }
        #endregion
    }
}
