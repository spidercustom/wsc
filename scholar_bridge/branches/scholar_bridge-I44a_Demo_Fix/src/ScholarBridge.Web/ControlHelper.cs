﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web
{
    public class ControlHelper
    {
        public static void SetControlStateRecursive(Control c, bool state)
        {
            foreach (Control control in c.Controls)
            {
                if (control is WebControl)
                    (control as WebControl).Enabled = state;
                if (control.Controls.Count > 0)
                    SetControlStateRecursive(control, state);
            }
        }

        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }
    }
}
