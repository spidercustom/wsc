﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
	public interface ILookupTableService
	{
		IEnumerable GetLookupTableData(string qualifiedClassName);
		List<Type> GetLookupTableList();
		void UpdateLookupTableData(string tableName, User user, string name, string description, bool deprecated, int id);
		void DeleteLookupTableData(string tableName, int id);
        void InsertLookupTableData(string tableName, User user, string name, string description);
	}
}
