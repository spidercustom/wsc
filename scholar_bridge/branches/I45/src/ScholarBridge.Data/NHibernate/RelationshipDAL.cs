﻿using System.Collections.Generic;
using NHibernate.Criterion;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class RelationshipDAL : AbstractDAL<Relationship>, IRelationshipDAL
    {
        public IList<Relationship> FindByProvider(Provider provider)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Provider", provider))
                .List<Relationship>(); 
        }

        public IList<Intermediary> FindNotRelatedByProvider(Provider provider)
        {
            var query = Session.CreateQuery(@"from Intermediary as i 
where i not in (select r.Intermediary from Relationship as r where r.Provider=:provider)");
            query.SetEntity("provider", provider);
            return query.List<Intermediary>();
        }

        public IList<Provider> FindNotRelatedByIntermediary(Intermediary intermediary)
        {
            var query = Session.CreateQuery(@"from Provider as p
where p not in (select r.Provider from Relationship as r where r.Intermediary=:intermediary)");
            query.SetEntity("intermediary", intermediary);
            return query.List<Provider>();
        }

        public IList<Relationship> FindByIntermediary(Intermediary intermediary)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Relationship>(); 
        }

        public Relationship FindByProviderandIntermediary(Provider provider, Intermediary intermediary)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .UniqueResult<Relationship>();
        }
    }
}
