﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ILookupDAL
    {
        IList<ILookup> FindAll();
        IList<ILookup> FindAll(IList<object> ids);
        ILookup FindById(object id);
        List<KeyValuePair<string,string>> GetLookupItems(string UserData);
    }
}
