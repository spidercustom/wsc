﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.MatchList;

namespace ScholarBridge.Web.Seeker.Matches
{
	public partial class Default : SBBasePage
	{
        public IMatchService MatchService { get; set; }
		public IUserContext UserContext { get; set; }

		protected void Page_Load(object sender, EventArgs e)
        {
            MainMenuHelper.SetupActiveMenuKey(this, MainMenuHelper.MainMenuKey.SeekerMatches);
            UserContext.EnsureSeekerIsInContext();
            
        }

	    protected override void OnInitComplete(EventArgs e)
        {
            ConfigureLists();
            base.OnInitComplete(e);
        }

	    public void DoStarIconClick()
	    {
            Response.Redirect(Request.Url.AbsoluteUri);

	    }

	    private void ConfigureLists()
	    {
	        var iconHelper = new MatchListIconHelper();
            var actionHelper = new MatchListActionHelper();
          

            savedList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
	        iconHelper.SetupListView(savedList.List);
            iconHelper.PostIconClick= (DoStarIconClick);
            actionHelper.SetupListView(savedList.List);

	        qualifyList.LinkTo = "~/Seeker/Scholarships/Show.aspx";
            iconHelper.SetupListView(qualifyList.List);
            actionHelper.SetupListView(qualifyList.List);
            iconHelper.PostIconClick = (DoStarIconClick);
        }

         
	     

	    protected void list_OnMatchAction(int scholarshipId)
        {
            MatchService.SaveMatch(UserContext.CurrentSeeker, scholarshipId);
        }
	}
}