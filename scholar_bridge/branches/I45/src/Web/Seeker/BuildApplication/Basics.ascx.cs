﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class Basics : WizardStepUserControlBase<Application>
    {
		public IApplicationService ApplicationService { get; set; }
		public IStateDAL StateService { get; set; }
        public IUserContext UserContext { get; set; }
        public LookupDAL<City> CityDAL { get; set; }
        public LookupDAL<County> CountyDAL { get; set; }
        private Application _applicationInContext;

		Application ApplicationInContext
		{
			get
			{
				if (_applicationInContext == null)
					_applicationInContext = Container.GetDomainObject();
				return _applicationInContext;
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			if (ApplicationInContext == null)
				throw new InvalidOperationException("There is no application in context");

            dobRangeValid.MaximumValue = DateTime.Today.ToShortDateString();
			if (!Page.IsPostBack)
			{
				PopulateScreen();
			    
			}
             
		}

         

		private void PopulateScreen()
		{

            if (null != ApplicationInContext.Seeker.Name)
		    {
		    
		        LastNameControl.Text = ApplicationInContext.Seeker.Name.LastName;
                MidNameControl.Text = ApplicationInContext.Seeker.Name.MiddleName;
                FirstNameControl.Text = ApplicationInContext.Seeker.Name.FirstName;
		    }

		    EmailControl.Text = ApplicationInContext.Seeker.User.Email;

            AddressLine1Control.Text = ApplicationInContext.Seeker.Address.Street;
            AddressLine2Control.Text = ApplicationInContext.Seeker.Address.Street2;
            if (null != ApplicationInContext.Seeker.Address.State)
            {
                StateControl.Text = ApplicationInContext.Seeker.Address.State.Name;
                if (ApplicationInContext.Seeker.Address.State.Abbreviation == State.WASHINGTON_STATE_ID)
                {
                    CityControl.Text = ApplicationInContext.Seeker.Address.City.Name;
                    CountyControl.Text = ApplicationInContext.Seeker.County.Name;
                }
                else
                {
                    CityControl.Text = ApplicationInContext.Seeker.AddressCity;
                    CountyControl.Text = ApplicationInContext.Seeker.AddressCounty;
                }
            }

            ZipControl.Text = ApplicationInContext.Seeker.Address.PostalCode;

            PhoneControl.Text = ApplicationInContext.Seeker.Phone == null ? "" : ApplicationInContext.Seeker.Phone.FormattedPhoneNumber;
            MobilePhoneControl.Text = ApplicationInContext.Seeker.MobilePhone == null ? "" : ApplicationInContext.Seeker.MobilePhone.FormattedPhoneNumber;

            if (ApplicationInContext.DateOfBirth.HasValue)
		        DobControl.Text = ApplicationInContext.DateOfBirth.Value.ToString("MM/dd/yyyy");

			ReligionCheckboxList.SelectedValues = ApplicationInContext.Religions.Cast<ILookup>().ToList();
			EthnicityControl.SelectedValues = ApplicationInContext.Ethnicities.Cast<ILookup>().ToList();
			OtherReligion.Text = ApplicationInContext.ReligionOther;
			GenderButtonList.SelectedIndex = (ApplicationInContext.Gender.Equals(Genders.Male) ? 0 : (ApplicationInContext.Gender.Equals(Genders.Female) ? 1 : -1));
			OtherEthnicity.Text = ApplicationInContext.EthnicityOther;

            ESLELLCheckBoxControl.Checked = ApplicationInContext.IsEslEll;
		}
		 
		 
		public override void PopulateObjects()
		{
           
             
			 
             

		    DateTime dob;
		    if (DateTime.TryParse(DobControl.Text, out dob))
		    {
		        ApplicationInContext.DateOfBirth = dob;
		    }
            else
		    {
                ApplicationInContext.DateOfBirth = null;
		    }

			PopulateList(EthnicityControl, ApplicationInContext.Ethnicities);
			PopulateList(ReligionCheckboxList, ApplicationInContext.Religions);
			ApplicationInContext.Gender = GenderButtonList.SelectedValue == string.Empty ? Genders.Unspecified : (Genders) int.Parse(GenderButtonList.SelectedValue);
			ApplicationInContext.EthnicityOther = OtherEthnicity.Text;
			ApplicationInContext.ReligionOther = OtherReligion.Text;
            ApplicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            ApplicationInContext.IsEslEll = ESLELLCheckBoxControl.Checked;
		}

		private static void PopulateList<T>(LookupItemCheckboxList checkboxList, IList<T> list)
		{
			checkboxList.PopulateListFromSelectedValues(list);
		}

		#region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

		public override void Save()
		{
			PopulateObjects();
			ApplicationService.Update(ApplicationInContext);
		}
		#endregion
	}
}