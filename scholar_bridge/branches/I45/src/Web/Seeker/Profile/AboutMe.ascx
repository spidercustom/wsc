﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutMe.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.AboutMe" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload"
             Assembly="Brettle.Web.NeatUpload" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<div class="form-iceland">
    <p class="form-section-title">About me</p>
    <p>This is your chance to let Scholarship providers know who you are.  Add a personal statement, share your talents and gifts, and attach a writing sample or pictures. Providers want to know something about the person who is going to receive their scholarships. </p><br />
    <div>
    <label for="PersonalStatementControl">Personal Statement:</label>
    <asp:TextBox ID="PersonalStatementControl" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
    <sb:CoolTipInfo ID="CoolTipInfo2" Content="Optional. Use to say something about yourself, what you hope to achieve by going to college, what it would mean to you to receive a scholarship." runat="server" />
    <elv:PropertyProxyValidator ID="PersonalStatementValidator" runat="server" ControlToValidate="PersonalStatementControl" PropertyName="PersonalStatement" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <br />

    <div>
    <label for="MyGiftControl">My Gift:</label>
    <asp:TextBox ID="MyGiftControl" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
    <sb:CoolTipInfo ID="CoolTipInfo1" Content="Optional. Use to describe your talents and gifts." runat="server" />
    <elv:PropertyProxyValidator ID="MyGiftValidator" runat="server" ControlToValidate="MyGiftControl" PropertyName="MyGift" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div><br />

    <div>
    <label id="FiveWordsLabelControl" for="FiveWordsControl">5 Words That Describe Me:</label>
    <asp:TextBox ID="FiveWordsControl" TextMode="MultiLine" runat="server" Width="400px"></asp:TextBox>
    <sb:CoolTipInfo ID="CoolTipInfo3" Content="Optional. Are you creative? a leader? This is a great way to briefly let scholarship providers know something about you. This is not used in the Scholarship match but will be included on your application." runat="server" />
    <elv:PropertyProxyValidator ID="FiveWordsValidator" runat="server" ControlToValidate="FiveWordsControl" PropertyName="Words" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div>
    <br />

    <div>
    <label id="FiveSkillsLabelControl" for="FiveSkillsControl">5 Skills I Have:</label>
    <asp:TextBox ID="FiveSkillsControl" TextMode="MultiLine" runat="server" Width="400px"></asp:TextBox>
    <sb:CoolTipInfo ID="CoolTipInfo4" Content="Optional. Are you a problem solver? imaginative? a great communicator? This is a great way to briefly let scholarship providers know your skills. This is not used in the Scholarship match but will be included on your application." runat="server" />
    <elv:PropertyProxyValidator ID="FiveSkillsValidator" runat="server" ControlToValidate="FiveSkillsControl" PropertyName="Skills" SourceTypeName="ScholarBridge.Domain.Seeker"/>
    </div><br />
    <hr />

    <p class="form-section-title">Attach documents (Transcripts, Letters of Recommendation, Award Letters, etc.)</p>
    <p>These documents may be attached to any scholarship application that you fill out.</p>

    <div style="display: none;">
    <Upload:ProgressBar id="AttachFileProgressBar" runat="server" inline="true" />
    </div>
    <br />
    <label for="AttachFile">Select File:</label>
    <Upload:InputFile ID="AttachFile" runat="server" />
    <br />
    <label for="AttachmentComments">Comments:</label>
    <asp:TextBox ID="AttachmentComments" runat="server" />    
    <sb:CoolTipInfo ID="CoolTipInfo5" Content="Optional. Add a descriptive comment with the file being uploaded." runat="server" />

    <elv:PropertyProxyValidator ID="AttachmentCommentsValidator" runat="server" ControlToValidate="AttachmentComments" PropertyName="Comment" SourceTypeName="ScholarBridge.Domain.Attachment"/>
    <asp:Label ID="fileTooBigLbl" runat="server" Visible="false" CssClass="error" Text="The file you tried to upload is too large. It should be less than 10MB." />
    <br />
    <label for="IncludeCheckBox" class="hidden">*Include with Applications:</label>
    <asp:checkbox runat="server" ID="includeCheckBox" Visible="false"  />
    <br />
    <sbCommon:AnchorButton ID="UploadFile" runat="server" Text="Attach File" onclick="UploadFile_Click" />

    <asp:ListView ID="attachedFiles" runat="server" 
        OnItemDeleting="attachedFiles_OnItemDeleting" 
        onitemcommand="attachedFiles_ItemCommand">
        <LayoutTemplate>
        <table class="sortableTable">
            <thead>
                <tr>
                    <th style="width:50px;">&nbsp;</th>
                    <th style="width:200px;">File</th>
                    <th style="width:70px;">Size</th>
                    <th>Comments</th>
                    <th style="width:125px;">Include with Applications*</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5">
                    *Including files with Applications will attach the file to each scholarship application you create. You will be able to add and remove files from the application before you submit it.
                </td>
            </tr>
            </tfoot>
        </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:LinkButton ID="deleteAttachmentBtn"   runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("Id")%>' /></td>
                <td>
                    <asp:LinkButton ID="downloadButton" CommandName="Download" CausesValidation="false" CommandArgument='<%# Eval("Id") %>' runat="server" Text='<%# Bind("Name") %>' ToolTip='<%# Eval("Comment") %>'>
                    </asp:LinkButton>            
                </td>
                <td><%# Eval("DisplaySize") %></td>
                <td><%# Eval("Comment") %></td>
                <td align="center"><asp:checkbox runat="server" ID="includeWithAppsCheck" Checked='<%# Eval("IncludeWithApplications") %>'/></td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
                <table class="sortableTable">
                   <thead>
                    <tr>
                        <th style="width:50px;">&nbsp;</th>
                        <th style="width:200px;">File</th>
                        <th style="width:70px;">Size</th>
                        <th>Comments</th>
                        <th style="width:125px;">Include with Applications*</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
          </EmptyDataTemplate>
    </asp:ListView>
</div>