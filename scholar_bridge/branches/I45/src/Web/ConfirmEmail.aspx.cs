﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web
{
    public partial class ConfirmEmail : Page
    {
        public IUserService UserService { get; set; }

        public string Key
        {
            get { return Request.QueryString["key"]; }
        }

        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Key) || !IsValidKey(Key))
            {
                SetError();
                return;
            }

            bool needsResetPassword;
            string userName;
        	try
        	{
				userName = GetEmailAddress(Key, out needsResetPassword);
        	}
        	catch (System.FormatException ex)
        	{
                SetError2();
                return;
        	}
            CurrentUser = UserService.FindByEmail(userName);
            if (null == CurrentUser)
            {
                SetError();
                return;
            }

            if (!Page.IsPostBack)
            {
                
                if (needsResetPassword)
                {
                    lblStatus.Text =
                        @"<b>Please create a password to use to log in.</b><br />
						<p class='blueNote'>Your password must be at least 7 characters with at least one special character or number. Example: p@ssworD</p><br />";
                    setPassword.Visible = true;
                    return;
                }

                if (CurrentUser.IsActive && CurrentUser.IsApproved)
                {
                    SuccessMessageLabel.SetMessage("Your email is validated.  You may now login.");
                    if (CurrentUser.IsInRole(Role.INTERMEDIARY_ROLE))
                        Response.Redirect("~/Intermediary/anonymous.aspx");
                    else if (CurrentUser.IsInRole(Role.PROVIDER_ROLE))
                        Response.Redirect("~/Provider/anonymous.aspx");
                    else
                        Response.Redirect("~/Seeker/");
                }
                if (!CurrentUser.IsActive)
                    ConfirmUser(CurrentUser);
                else
                    lblStatus.Text = "Your email was already validated, cannot validate already confirmed email";
            }
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                setPassword.Visible = false;

                CurrentUser.Password = HashPassword(Password.Text, CurrentUser.PasswordSalt);
                CurrentUser.PasswordFormat = (int) MembershipPasswordFormat.Hashed;
                ConfirmUser(CurrentUser);
            }
        }

        private void ConfirmUser(User user)
        {
            // XXX: If this gets any more complicated we should pull this into a helper class.
            user.LastUpdate = new ActivityStamp(user);
            if (user.IsInRole(Role.PROVIDER_ROLE) && ! user.IsApproved)
            {
                ConfirmProvider(user);
            }
            else if (user.IsInRole(Role.INTERMEDIARY_ROLE) && !user.IsApproved)
            {
                ConfirmIntermediary(user);
            }
            else if ((user.IsInRole(Role.SEEKER_ROLE) ||(user.IsInRole(Role.INFLUENCER_ROLE)))  && !user.IsApproved)
            {
                ConfirmSeeker(user);
            }
            else
            {
                ConfirmNormalUser(user);
            }
        }

        private void ConfirmSeeker(User u)
        {
            UserService.ActivateSeekerUser(u);
            SuccessMessageLabel.SetMessage("Your email has been validated.  You may now login.");
            Response.Redirect("~/Seeker/");
        }

        private void ConfirmNormalUser(User u)
        {
            UserService.ActivateUser(u);
            SuccessMessageLabel.SetMessage("Your email has been validated.  You may now login.");
            if (u.IsInRole(Role.INTERMEDIARY_ROLE))
                Response.Redirect("~/Intermediary/anonymous.aspx");
            else
            {
                Response.Redirect("~/Provider/anonymous.aspx");
            }
        }

        private void ConfirmProvider(User u)
        {
            UserService.ActivateProviderUser(u);
            ShowApprovalDialog("/Provider/anonymous.aspx");
        }

        private void ConfirmIntermediary(User u)
        {
            UserService.ActivateIntermediaryUser(u);
            ShowApprovalDialog("/Intermediary/anonymous.aspx"); 
        }

        private static bool IsValidKey(string key)
        {
            return key.StartsWith("enc");
        }

        private void ShowApprovalDialog(string redirectTo)
        {
            LinkGenerator linkGenerator = new LinkGenerator();
            
            // Gets the executing web page

            var page = HttpContext.Current.CurrentHandler as Page;
            const string divId = "ApprovalDialogAlert";
            // Checks if the handler is a Page and that the script isn't allready on the Page
            if (null != page && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), divId))
            {
                var script = "EmailApprovalAlert('{0}','{1}');".Build(linkGenerator.GetFullLink("/ContactUs.aspx"),linkGenerator.GetFullLink(redirectTo));
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), divId, script);
            }
        }

		public void SetError()
		{
			lblStatus.Text = "The link you have followed is not valid.";
		}

		public void SetError2()
		{
			lblStatus.Text = "The link that you entered contains invalid characters.  If you manually entered or used 'cut and paste' to copy the link from the confirmation email, make sure that you did not cut the link too short or add additional characters.";
		}

        private const string RESET = "reset=true";

        public string GetEmailAddress(string encryptedParam, out bool resetPassword)
        {
            var decrypted = CryptoHelper.Decrypt(encryptedParam);
            if (decrypted.Contains("&"))
            {
                var parts = decrypted.Split(new[] {"&"}, StringSplitOptions.RemoveEmptyEntries);
                resetPassword = 2 == parts.Length && RESET == parts[1];
                return parts[0];
            }

            resetPassword = false;
            return decrypted;
        }

        public string HashPassword(string password, string salt)
        {
            var hash = new HMACSHA1 {Key = HexToByte(salt)};
            return Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
        }

        private byte[] HexToByte(string hexString)
        {
            var bytes = new byte[hexString.Length/2 + 1];
            for (var i = 0; i <= hexString.Length/2 - 1; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i*2, 2), 16);
            }
            return bytes;
        }
    }
}