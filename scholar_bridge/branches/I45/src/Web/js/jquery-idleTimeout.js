//######
//## This work is licensed under the Creative Commons Attribution-Share Alike 3.0 
//## United States License. To view a copy of this license, 
//## visit http://creativecommons.org/licenses/by-sa/3.0/us/ or send a letter 
//## to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.
//######

(function($) {
    $.fn.idleTimeout = function(options) {
        var defaults = {
            inactivity: 780000, //13 mins
            noconfirm: 120000, //2 mins
            sessionAlive: false,
            redirect_url: '/login.aspx',
            click_reset: true,
            alive_url: '',
            logout_url: '/logout.aspx'
        }

        //##############################
        //## Private Variables
        //##############################
        var opts = $.extend(defaults, options);
        var liveTimeout, confTimeout, sessionTimeout;
        var modal = "<div id='modal_pop'><p>This page is about to time out. Please click OK or refresh this page to continue.</p></div>";
        var loggedoutPopup = "<div id='loggedout_pop'><p>Your Session has timed out.</p></div>";

        //##############################
        //## Private Functions
        //##############################
        var start_liveTimeout = function() {
            clearTimeout(liveTimeout);
            clearTimeout(confTimeout);
            liveTimeout = setTimeout(logout, opts.inactivity);

            if (opts.sessionAlive) {
                clearTimeout(sessionTimeout);
                sessionTimeout = setTimeout(keep_session, opts.sessionAlive);
            }
        }

        var logout = function() {

            confTimeout = setTimeout(redirect, opts.noconfirm);
            $(modal).dialog({
                buttons: { "OK": function() {
                    $(this).dialog('close');
                    stay_logged_in();
                }
                },
                modal: true,
                title: ' '
            });

        }

        var redirect = function() {
            BP_bOnBeforeUnloadFired = true;


            if (opts.logout_url) {

                $.ajax({
                    type: "GET", //POST
                    url: opts.logout_url,
                    data: "", // Set Method Params

                    success: function(msg, status) {
                        if ($('#modal_pop').dialog("isOpen")) $('#modal_pop').dialog("close");

                        $(loggedoutPopup).dialog({
                            buttons: { "Close": function() {
                                $(this).dialog('close');
                                
                            }
                            },
                            modal: true,
                            title: '',
                            close: function(msg, status) { window.location.href = opts.redirect_url; }

                        });

                    }
                });

            }

        }

        var stay_logged_in = function(el) {
            start_liveTimeout();
            if (opts.alive_url) {
                $.get(opts.alive_url);
            }
        }

        var keep_session = function() {
            $.get(opts.alive_url);
            clearTimeout(sessionTimeout);
            sessionTimeout = setTimeout(keep_session, opts.sessionAlive);
        }

        //###############################
        //Build & Return the instance of the item as a plugin
        // This is basically your construct.
        //###############################
        return this.each(function() {
            obj = $(this);
            start_liveTimeout();
            if (opts.click_reset) {
                $(document).bind('click', start_liveTimeout);
            }
            if (opts.sessionAlive) {
                keep_session();
            }
        });

    };
})(jQuery);

$(document).ready(function() {

    /** Jquery Session Time out Popup **/
$(document).ready(function() {
    $(document).idleTimeout();
});

});