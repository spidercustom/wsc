﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
    Inherits="ScholarBridge.Web.Profile.Default" Title="Profile" %>

<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="EditUserName" Src="~/Common/EditUserName.ascx" %>
<%@ Register TagPrefix="sb" TagName="EditOrganizationUserEmail" Src="~/Common/EditOrganizationUserEmail.ascx" %>
<%@ Register Src="~/Common/SeekerProfileProgress.ascx" TagName="SeekerProfileProgress"
    TagPrefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            $("#pnlchangepassword").keypress(function(e) {

                if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                    $("#pnlchangepassword > a[id$='_ChangePasswordPushButton']").click();
                    return true;
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div>
        <asp:Image ID="Image1" ImageUrl="~/images/PicTopMySettings.gif" Width="918px" Height="15px"
            runat="server" /></div>
    <div>
        <asp:Image ID="Image2" ImageUrl="~/images/PicBottomMySettings.gif" Width="918px"
            Height="169px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:LoginView ID="loginView2" runat="server">
        <RoleGroups>
            <asp:RoleGroup Roles="Seeker, Influencer">
                <ContentTemplate>
                    <!--Left floated content area starts here-->
                    <div id="HomeContentLeft">
                        <img src="<%= ResolveUrl("~/images/PgTitle_MySettings.gif") %>" width="149px" height="54px">
                        <img src="<%= ResolveUrl("~/images/EmphasisedTextSeekerLoggedIn.gif") %>" width="513px"
                            height="96px">
                    </div>
                    <!--Left floated content area ends here-->
                    <!--Right Floated content area starts here-->
                    <div id="HomeContentRight">
                        <sb:SeekerProfileProgress ID="ProfileProgess" runat="server" />
                    </div>
                    <br>
                    <br>
                    <div id="Clear">
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
            <asp:RoleGroup Roles="Intermediary, Intermediary Admin, Provider, Provider Admin">
                <ContentTemplate>
                    <img src="<%= ResolveUrl("~/images/PgTitle_MySettings.gif") %>" width="149px" height="54px">
                    <br>
                    <br>
                    <div id="Clear">
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
    <div id="seekertypediv" runat="server">
        <p class="FormHeader">
            Seeker Type: <b>
                <asp:label ID="seekerType" runat="server"></asp:label></b></p>
    </div>
    <div>
        <p class="FormHeader">
            Change User Details</p>
        <br />
        <sb:EditUserName ID="editUserName" runat="server" OnUserSaved="editUserName_OnUserSaved" />
    </div>
    <div>
        <p class="FormHeader">
            Change Email Address</p>
        <br />
        <sb:EditOrganizationUserEmail runat="server" ID="editUserEmail" />
        <br />
        <hr>
        <br />
    </div>
    <div>
        <p class="FormHeader">
            Change Password</p>
        <br />
        <asp:ChangePassword ID="ChangePassword1" runat="server" OnChangePasswordError="ChangePassword1_Error"
            SuccessPageUrl="~/Profile/Default.aspx" OnChangedPassword="ChangePassword1_ChangedPassword"
            BorderPadding="4" ChangePasswordButtonType="Image" ChangePasswordButtonImageUrl="~/images/btn_save.gif"
            CancelButtonStyle-CssClass="hidden" OnChangingPassword="ChangePassword1_OnChangingPassword">
            <ChangePasswordTemplate>
                <div class="form-iceland-container">
                    <div id="pnlchangepassword" class="form-iceland">
                        <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Current Password:</asp:Label>
                        <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                            ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="CurrentPassowordCustomValidator" runat="server" ControlToValidate="CurrentPassword"   ValidationGroup="ChangePassword1"  />
                        <br />
                        <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                        <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <sb:CoolTipInfo ID="CoolTipInfoPassword" runat="Server" Content="Your password must be at least 7 characters with at least one special character or number. Example: p@sword" />
                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                            ErrorMessage="New Password is required." ToolTip="New Password is required."
                            ValidationGroup="ChangePassword1">*Required</asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="NewPassowrdCustomValidator" runat="server" ControlToValidate="NewPassword" />
                        <br />
                        <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm Password:</asp:Label>
                        <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                            ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                            ValidationGroup="ChangePassword1">*Required</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                            ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                            ValidationGroup="ChangePassword1"></asp:CompareValidator>
                        <br />
                        <br />
                        <sbCommon:AnchorButton ID="ChangePasswordPushButton" runat="server" Text="Save" CommandName="ChangePassword"
                            ValidationGroup="ChangePassword1" />
                        <asp:Label ID="FailureText" Style="color: Red;" runat="server" EnableViewState="False"></asp:Label>
                    </div>
                </div>
            </ChangePasswordTemplate>
        </asp:ChangePassword>
    </div>
    <div id="ChangePasswordValidationErrorPopup"    title="Change Password Failed!" style="display: none">
        <asp:Image ID="ValidationErrorPopupimage" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/cooltipicon_errorbig.png" />
        <asp:Label ID="ChangePasswordValidationErrorPopupLabel" runat="server" Text="Password must be at least 7 characters with at least one special character or number." />
    </div>
</asp:Content>
