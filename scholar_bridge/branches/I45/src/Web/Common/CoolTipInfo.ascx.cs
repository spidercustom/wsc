﻿using System;
using System.Web;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
   
    public partial class CoolTipInfo : UserControl
    {
        public string Content { get; set; }
        public string ContentDiv { get; set; }
        public string Header { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Header))
                Header = "Information!";

           image.Attributes.Add("header",HttpUtility.HtmlEncode(Header  ));
           image.Attributes.Add("content", HttpUtility.HtmlEncode(Content));
           image.Attributes.Add("contentdivid", HttpUtility.HtmlEncode(ContentDiv));
           image.Attributes.Add("title", "");
        }
    }
}