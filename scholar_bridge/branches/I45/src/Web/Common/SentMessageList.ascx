﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SentMessageList.ascx.cs" Inherits="ScholarBridge.Web.Common.SentMessageList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<asp:ListView ID="messageList" runat="server" 
    onitemdatabound="messageList_ItemDataBound" OnSorting="messageList_Sorting"  OnPagePropertiesChanging="messageList_PagePropertiesChanging" >
    <LayoutTemplate>
        <table class="sortableListView">
            <thead>
                <tr>
                    <th><asp:LinkButton ID="linkButtonSortOnSubject" runat="server" CommandName="Sort" CommandArgument="Subject" Text="Subject" /></th>
                    <th><asp:LinkButton ID="linkButtonSortOnFrom" runat="server" CommandName="Sort" CommandArgument="To" Text="To" /></th>
                    <th><asp:LinkButton ID="linkButtonOrganizationName" runat="server" CommandName="Sort" CommandArgument="From" Text="From" /></th>
                    <th><asp:LinkButton ID="linkButtonDate" runat="server" CommandName="Sort" CommandArgument="Date" Text="Date" /></th>
           
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>    </LayoutTemplate>
    <ItemTemplate>
        <tr class="row">
            <td><asp:LinkButton id="linkToMessage"  Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
            <td><%# DataBinder.Eval(Container.DataItem, "To")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "From") %></td>
            <td><asp:Label ID="lblDate" runat="server"></asp:Label></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:LinkButton id="linkToMessage"  Target="_blank" runat="server"><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:LinkButton></td>
            <td><%# DataBinder.Eval(Container.DataItem, "To")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "From") %></td>
            <td><asp:Label ID="lblDate" runat="server"></asp:Label></td>
        </tr>
    </AlternatingItemTemplate>
</asp:ListView>
<div class="pager">
    <asp:DataPager runat="server" ID="pager" PagedControlID="messageList" PageSize="20">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
    <br />
</div> 
