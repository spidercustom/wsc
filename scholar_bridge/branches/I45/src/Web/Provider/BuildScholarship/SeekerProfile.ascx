﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.SeekerProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>
<%@ Register src="~/Common/NumberRangeConditionControl.ascx" tagname="NumberRangeCondition" tagprefix="sb" %>
<%@ Register Assembly="Web"
             Namespace="ScholarBridge.Web.Common"
             TagPrefix="sbCommon" %>

<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>
<%@ Register TagPrefix="sb" TagName="SingleAttributeSelectionControl" Src="SingleAttributeSelectionControl.ascx" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<script type="text/javascript">
	$(function() {
	$("#accordion").accordion({
            active: false,
	        autoHeight: false ,
	        icons: {
    			header: "ui-icon-circle-arrow-e",
    			headerSelected: "ui-icon-circle-arrow-s"
   				
			    }
	    });
	});

	function CheckWashingtonState(selfId, clientIds, notusedRadioIDs, minimumRadioIDs, preferenceRadioIDs, buddyIDs) {

	    if (selfId == undefined) {
	        //alert('i am undefined');
	        return false;
	    }
	    var controls = clientIds.split(",");
	    

	    var atext = $("#" + selfId).find(':selected').text();
	    
	    var controls = clientIds.split(",");
	    if (controls.length > 0) {
	        for (var i = 0; i < controls.length; i++) {
	           
	            if (atext == "Washington") {
	                //enableElementById(controls[i]);

	            } else {
	             
	                disableElementById(controls[i]);
	            }
	        
	        }
	    }

	    var controls = notusedRadioIDs.split(",");
	    if (controls.length > 0) {
	        for (var i = 0; i < controls.length; i++) {

	            if (atext == "Washington") {
	                enableElementById(controls[i]);
	                

	            } else {
	            disableElementById(controls[i]);
	            $("#" + controls[i]).attr("checked", true);
	            $("#" + controls[i]).click();
	            }

	        }
	    }
	    var controls = minimumRadioIDs.split(",");
	    if (controls.length > 0) {
	        for (var i = 0; i < controls.length; i++) {

	            if (atext == "Washington") {
	                enableElementById(controls[i]);
	              
	                

	            } else {
	                disableElementById(controls[i]);

	            }

	        }
	    }
	    
	    var controls = preferenceRadioIDs.split(",");
	    if (controls.length > 0) {
	        for (var i = 0; i < controls.length; i++) {

	            if (atext == "Washington") {
	                enableElementById(controls[i]);


	            } else {
	                disableElementById(controls[i]);

	            }

	        }
	    }


	    if (atext != "Washington") {

	        var controls = buddyIDs.split(",");
	        if (controls.length > 0) {
	            for (var i = 0; i < controls.length; i++) {
	                $("#" + controls[i]).val('');
	            }
	        }
	    
	    }
	                 
	    
	} 
	</script>
<div class="form-iceland-container">
    <div class="form-iceland largeleft-columns">
    <h2>Build Scholarship – Applicant's Profile</h2>
    <p>What criteria will you use to evaluate applicants and make an award?
       Use these fields to define what is a <b>Required Minimum</b> and what will be <b>Given Preference</b>. 
       This will be used to match your scholarhsip with qualified applicants.
    </p>
    <p><span style="font-weight: bold;">Required Minimum</span> will set a match threshold. Applicants will not be able to submit an application unless they meet ALL Required minimum criteria.
       <span style="font-weight: bold;">Given Preference</span> will indicate any additional evaluation criteria you use. Applicants will be able to submit applications without matching on preference criteria.
    </p>
    <p>Expand the section. Select Required Minimum or Given Preference from the columns on the left for the fields you want to use.
    Then select the criteria values from the options on the right.
    </p>
    </div>
     <div class="form-iceland smallright-columns">
         <div id="ListChangeContainer" class="ui-corner-all" >
         <p>Not finding the criteria you're looking for?</p>
        <div style="width:120px; height:29px; margin:0 auto;"><sbCommon:AnchorButton id="SeekerProfileStepListChangeButton"  runat="server" Text="Request Criteria" OnClientClick="javascript:window.open('../scholarships/SubmitListChangeRequest.aspx');" /></div>
        </div>
    </div>
</div>                            

<div class="form-iceland-container">
     <div class="form-iceland">
        <div id="accordion">
	        <h3><a href="#" ><span class="form-section-title">Applicant Student Performance<asp:CustomValidator ID="PerformanceCustomValidator" runat="server" OnServerValidate="PerformanceCustomValidator_OnServerValidate" ErrorMessage="This section contains following errors:" /></span><br /><span>Do you use student academic performance to evaluate applicants? Student Performance includes GPA, SAT, and Honors.</span><hr /></a></h3>
	        <div>
		            <asp:PlaceHolder ID="SeekerPerformanceControlContainer" runat="server">
                        <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                           
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionGPAControl" 
                                runat="server" DisplayHeader="true" BuddyControl="GPAContainerControl"/>
                            </div>
                           <div class="form-iceland right-columns">
                           <br /><br />
                              <asp:Panel  ID="GPAContainerControl" Width="450px" runat="server">
                                    <!-- See code comment WSC-352 -->
                                   
                                     <label  class="attributeTitlelabel-small" >GPA (4.0=A) </label>  
                                    <div class="control-set">
                                        
                                           <label>Greater than or equal</label> 
                                            <sandTrap:NumberBox ID="GPAGreaterThanControl" Width="40px" runat="server" Precision="3" MinAmount="0" MaxAmount="5"/>
                                            <label for="GPALessThanControl">Less than or equal</label>
                                            
                                            <asp:TextBox ID="GPALessThanControl" runat="server"  Width="40px" style="text-align:right;margin-left:10px;" Precision="3" CanNull="true"
                                                minamount="0" maxamount="5" onfocus="FormatNumberAsDecimal(this)" onblur="FormatDecimalAsNumber(this)"/>
                                         
                                     </div>
                                <br />
                              </asp:Panel >
                              </div> 
                        </div>
                        
                        <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                            <br />
                            <br />
                            

                                <sb:SingleAttributeSelectionControl id="AttributeSelectionSATWritingControl" 
                                runat="server" DisplayHeader="false" BuddyControl="SATWritingContainerControl"/>
                            </div>
                             
                            <div class="form-iceland right-columns">
                               
                                <asp:Panel  ID="SATWritingContainerControl" runat="server">
                               
                                <label>SAT Score:</label>
                                 <br />
                                  
                                    <label  class="attributeTitlelabel subsection" >Writing</label>  
                                                         
                                    <div id="satWritingDiv" class="control-set" runat="Server">
    								
								    <sb:NumberRangeCondition id="SATWritingControl" runat="server"  Title="Writing"
								      MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
								    <br />
								    </div>
                                </asp:Panel > 
                            </div>
                        </div>   
					    <br />
                       <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionSATCriticalReadingControl" 
                                runat="server" DisplayHeader="false" BuddyControl="SATCriticalReadingContainerControl"/>
                            </div>
                            <div class="form-iceland right-columns">
                                <asp:Panel  ID="SATCriticalReadingContainerControl"  runat="server">
                                <label  class="attributeTitlelabel subsection" >Critical Reading</label>  
                                    <div id="satCriticalReadingDiv" class="control-set"  runat="Server">
                                             
                                            <sb:NumberRangeCondition id="SATCriticalReadingControl" runat="server" Title="Critical Reading"
                                              MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
                                            <br />
                                        </div>
                                </asp:Panel > 
                            </div>
                        </div>   
					    <br />					

                       <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionSATMathematicsControl" 
                                runat="server" DisplayHeader="false" BuddyControl="SATMathematicsContainerControl"/>
                            </div>
                            <div class="form-iceland right-columns">
                                <asp:Panel  ID="SATMathematicsContainerControl"  runat="server">
                                    <label  class="attributeTitlelabel subsection" >Mathematics</label>  
                                    <div id="satMathematicsDiv"  class="control-set"  runat="Server">
									    <sb:NumberRangeCondition id="SATMathematicsControl" runat="server"
									      MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
								    </div>
                                </asp:Panel > 
                            </div>
                        </div>      
                       
                       <br />
                        <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                             <br />
                            <br />
                            
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionACTEnglishControl" 
                                runat="server" DisplayHeader="false" BuddyControl="ACTEnglishContainerControl"/>
                            </div>
                            <div class="form-iceland right-columns">
                                <label>ACT Score:</label>
                                <br />
                                <asp:Panel  ID="ACTEnglishContainerControl"  runat="server">
                                    <label class="attributeTitlelabel subsection" >English</label>
                                    <div id="actEnglishDiv"  class="control-set" runat="Server">
                                        <sb:NumberRangeCondition id="ACTEnglishControl" runat="server"
                                          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                                        <br />  
								    </div>
                                </asp:Panel > 
                            </div>
                        </div>   
					    <br />
                        <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionACTMathematicsControl" 
                                runat="server" DisplayHeader="false" BuddyControl="ACTMathematicsContainerControl"/>
                            </div>
                            <div class="form-iceland right-columns">
                                <asp:Panel  ID="ACTMathematicsContainerControl"  runat="server">
                                    <label class="attributeTitlelabel subsection" >Mathematics</label>
                                    <div id="actMathematicsDiv"  class="control-set" runat="Server">
                                        <sb:NumberRangeCondition id="ACTMathematicsControl" runat="server"
                                          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                                        <br />
                                    </div>
                                </asp:Panel > 
                            </div>
                        </div>   
					    <br />
                        <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionACTReadingControl" 
                                runat="server" DisplayHeader="false" BuddyControl="ACTReadingContainerControl"/>
                            </div>
                            <div class="form-iceland right-columns">
                                <asp:Panel  ID="ACTReadingContainerControl"  runat="server">
                                <label class="attributeTitlelabel subsection" >Reading</label>
                                    <div id="actReadingDiv" class="control-set" runat="Server">
                                        <sb:NumberRangeCondition id="ACTReadingControl" runat="server"
                                          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                                        <br />
								    </div>
                                </asp:Panel > 
                            </div>
                        </div>   
				       <br />
                        <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionACTScienceControl" 
                                runat="server" DisplayHeader="false" BuddyControl="ACTScienceContainerControl"/>
                            </div>
                            <div class="form-iceland right-columns">
                                <asp:Panel  ID="ACTScienceContainerControl"  runat="server">
                                <label class="attributeTitlelabel subsection" >Science</label>
                                    <div id="actScienceDiv" class="control-set" runat="Server">
                                        <sb:NumberRangeCondition id="ACTScienceControl" runat="server"
                                          MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                                    </div>
                                </asp:Panel > 
                            </div>
                        </div>   
					    <br />
    				   
				       <br />
                        <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionClassRankControl" 
                                runat="server" DisplayHeader="false" BuddyControl="ClassRankContainerControl"/>
                            </div>
                            
                            <div class="form-iceland right-columns">
                              <asp:Panel  ID="ClassRankContainerControl" runat="server">
                              <label id="ClassRankControlLabel" for="ClassRankControl">Class Rank:</label>
                                 <asp:CustomValidator ID="ClassRankValidator" runat="server" ErrorMessage="Please select Class Rank." />
                
                                <sb:LookupItemCheckboxList id="ClassRankControl" runat="server" LookupServiceSpringContainerKey="ClassRankDAL" MultiColumn="true"/>
                                <br />
                              </asp:Panel >

                           </div>
                           
                    </div>
                    <br /><br />
				    <div class="form-iceland-container">
					    <div class="form-iceland left-columns">
						    <sb:SingleAttributeSelectionControl id="AttributeSelectionHonorsControl" 
						    runat="server" DisplayHeader="false" BuddyControl="HonorsContainerControl"/>
					    </div>
    				    <div class="form-iceland right-columns">
						      <label for="HonorsLabelControl">Academic Honors</label><sb:CoolTipInfo ID="CoolTipInfoHonorsControl" Content="Use to indicate if applicant has received honors awards, or participated in honors classes or honors programs. Applicants will be asked to provide details." runat="server" />
						      <asp:Panel  ID="HonorsContainerControl" runat="server" Visible="false">
							    <asp:Literal ID="HonorsControl" runat="server" />
							    <br />
						      </asp:Panel >
    						    
					      </div>
                    </div>
                    <br />  
                   
                    <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionAPCreditControl" 
                            runat="server" DisplayHeader="false" BuddyControl="APCreditContainerControl"/>
                        </div>
                        <div class="form-iceland right-columns">
                              <label for="APCreditControl">AP Credits Earned</label><sb:CoolTipInfo ID="CoolTipInfoAPCreditControl" Content="Use to indicate if applicant has earned Advanced Placement credit. Applicants will be asked to provide details." runat="server" />
                              <asp:Panel  ID="APCreditContainerControl" runat="server" Visible="false">
                                <asp:Literal ID="APCreditControl" runat="server" />
                                <br />
                              </asp:Panel >
                              </div>
                    </div>
                    <br />
                    <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionIBCreditControl" 
                            runat="server" DisplayHeader="false" BuddyControl="IBCreditContainerControl"/>
                        </div>
                        <div class="form-iceland right-columns">
                              <label for="IBCreditControl">IB Credits Earned</label><sb:CoolTipInfo ID="CoolTipInfoIBCreditControl" Content="Use to indicate if applicant has earned International Baccalaureate credit. Applicants will be asked to provide details." runat="server" />
                              <asp:Panel  ID="IBCreditContainerControl" runat="server" Visible="false">
                                <asp:Literal ID="IBCreditControl" runat="server" />
                               </asp:Panel >
                               </div>
                   </div>
                   <br />  
                    <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionRunningStartControl" runat="server" DisplayHeader="false" BuddyControl="RunningStartContainerControl"/>
                        </div>
                        <div class="form-iceland right-columns">
                              <label for="RunningStartControl">Running Start Student</label><sb:CoolTipInfo ID="CoolTipRunningStartControl" Content="Use to indicate if applicant has earned Running Start credit. Applicants will be asked to provide details." runat="server" />
                              <asp:Panel  ID="RunningStartContainerControl" runat="server" Visible="false">
                                <asp:Literal ID="RunningStartControl" runat="server" />
                               </asp:Panel >
                        </div>
                   </div>
                   <br />                                        
               </asp:PlaceHolder>
	        </div>
	        <h3><a href="#" ><span class="form-section-title">Applicant Demographics - Geographic <asp:CustomValidator ID="GeographicsCustomValidator" runat="server" OnServerValidate="GeographicsCustomValidator_OnServerValidate" ErrorMessage="This section contains following errors:" /></span><br /><span>Are applicants required to be from a specific area? Demographics - Geographic includes High School, City, and State.</span><hr /></a></h3>
	        <div>
		            <asp:PlaceHolder ID="SeekerDemographicsGeographicContainerControl" runat="server">
                        <div class="form-iceland-container">
                                <div class="form-iceland left-columns">
                                    <sb:SingleAttributeSelectionControl id="AttributeSelectionStateControl" 
                                    runat="server" DisplayHeader="true" BuddyControl="StateContainerControl"/>
                                </div>
                               <div class="form-iceland right-columns">
                                  <br /><br />
                                  <asp:Panel  ID="StateContainerControl" runat="server">
                                      <label for="StateControl">State:</label>
                                        <asp:DropDownList ID="StateControl" runat="server" onclick="CheckWashingtonState();"/>
                                        <sb:CoolTipInfo ID="CoolTipInfoStateControl" Content="Use to indicate if applicants must be residents of a specific state, All applicants are required to provide a permanent address." runat="server" />
             
                                        <br />
                                        <span >The following fields can only be used when Washington State is selected.</span>
                                        <br />
                                  </asp:Panel >
                               </div> 
                         </div> 
                        <br />
                        <div class="form-iceland-container">
                                <div class="form-iceland left-columns">
                                <br /><br />
                                    <sb:SingleAttributeSelectionControl id="AttributeSelectionHighSchoolControl" 
                                    runat="server" DisplayHeader="false" BuddyControl="HighSchoolContainerControl"/>
                                </div>
                               <div class="form-iceland right-columns">
                                  <br /><br />
                                  <asp:Panel  ID="HighSchoolContainerControl" runat="server">
                                      <label id="HighSchoolControlLabel" for="HighSchoolControl">High School:</label>
                                        <sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl" ItemSource="HighSchoolDAL" Title="High School Selection"/>
                                        <asp:TextBox ID="HighSchoolControl" ReadOnly="true" TextMode="MultiLine" Rows="5"  Columns="23" runat="server"></asp:TextBox>
                                        <sb:CoolTipInfo ID="CoolTipInfoHighSchool" Content="Use to indicate if applicant has to have attended or graduated from a specified High School." runat="server" />
                                        <asp:CustomValidator ID="HighSchoolValidator" runat="server" ErrorMessage="Please select High School." />
                            
                                 
                                        <br />
                                  </asp:Panel >
                               </div> 
                         </div> 
                        <br />
                        <div class="form-iceland-container">
                                <div class="form-iceland left-columns">
                                <br /><br />
                                    <sb:SingleAttributeSelectionControl id="AttributeSelectionSchoolDistrictControl" 
                                    runat="server" DisplayHeader="false" BuddyControl="SchoolDistrictContainerControl"/>
                                </div>
                               <div class="form-iceland right-columns">
                                  <br /><br />
                                  <asp:Panel  ID="SchoolDistrictContainerControl" runat="server">
                                      <label id="SchoolDistrictLabel" for="SchoolDistrictControl">School District:</label>
                                        <sb:LookupDialog ID="SchoolDistrictControlDialogButton" runat="server" BuddyControl="SchoolDistrictControl" ItemSource="SchoolDistrictDAL" Title="School District Selection"/>
                                        <asp:TextBox ID="SchoolDistrictControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                                        <sb:CoolTipInfo ID="CoolTipInfoSchoolDistrict" Content="Use to indicate if applicant has to have attended a specified School district." runat="server" />
                                        <asp:CustomValidator ID="SchoolDistrictValidator" runat="server" ErrorMessage="Please select School District." />
                            
                                        <br />
                                  </asp:Panel >
                               </div> 
                         </div> 
                        <br />
                        <div class="form-iceland-container">
                                <div class="form-iceland left-columns">
                                <br /><br />
                                    <sb:SingleAttributeSelectionControl id="AttributeSelectionCityControl" 
                                    runat="server" DisplayHeader="false" BuddyControl="CityContainerControl"/>
                                </div>
                               <div class="form-iceland right-columns">
                                  <br /><br />
                                  <asp:Panel  ID="CityContainerControl" runat="server">
                                      <label id="CityControlLabel" for="CityControl">City:</label>
                                        <sb:LookupDialog ID="CityControlDialogButton" runat="server" BuddyControl="CityControl" ItemSource="CityDAL" Title="City Selection"/>
                                        <asp:TextBox ID="CityControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                                        <sb:CoolTipInfo ID="CoolTipInfoCity" Content="Use to indicate if applicants must be a residents of a specific city." runat="server" />
                                          <asp:CustomValidator ID="CityValidator" runat="server" ErrorMessage="Please select City." />
                            
                                        <br />
                                  </asp:Panel >
                               </div> 
                         </div> 
                         <br />
                        <div class="form-iceland-container">
                                <div class="form-iceland left-columns">
                                <br /><br />
                                    <sb:SingleAttributeSelectionControl id="AttributeSelectionCountyControl" 
                                    runat="server" DisplayHeader="false" BuddyControl="CountyContainerControl"/>
                                </div>
                               <div class="form-iceland right-columns">
                                  <br /><br />
                                  <asp:Panel  ID="CountyContainerControl" runat="server">
                                      <label id="CountyControlLabel" for="CountyControl">County:</label>
                                        <sb:LookupDialog ID="CountyControlDialogButton" runat="server" BuddyControl="CountyControl" ItemSource="CountyDAL" Title="County Selection"/>
                                        <asp:TextBox ID="CountyControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                                         <sb:CoolTipInfo ID="CoolTipInfoCounty" Content="Use to indicate if applicants must be residents of a specific county." runat="server" />
                                        <asp:CustomValidator ID="CountyValidator" runat="server" ErrorMessage="Please select County." />
                            
                                        <br />
                                  </asp:Panel >
                               </div> 
                         </div> 
                    </asp:PlaceHolder>
	        </div>
	        <h3><a href="#" ><span class="form-section-title">Applicant Demographics - Personal <asp:CustomValidator ID="DemographicsCustomValidator" runat="server" OnServerValidate="DemographicsCustomValidator_OnServerValidate" ErrorMessage="This section contains following errors:" /></span><br /><span>Does your criteria include personal traits? Personal includes First Generation, Ethnicity and Religion.</span><hr /></a></h3>
	        <div>
		         <asp:PlaceHolder ID="SeekerDemographicsPersonalContainerControl" runat="server">
                   <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionFirstGenerationControl" 
                            runat="server" DisplayHeader="true" BuddyControl="FirstGenerationContainerControl"/>
                            </div>
                            <div class="form-iceland right-columns">
                            <br /><br />
                                <asp:Panel ID="FirstGenerationContainerControl" runat="server">
                                <label for="FirstGenerationControl" style="width:230px !important;">First Generation College Student</label>
                                
                                <asp:Literal ID="FirstGenerationControl" runat="server" />
                                <sb:CoolTipInfo ID="CoolTipInfo1" Content="Applicant is in the first generation of their family to attend college." runat="server" />
                                </asp:Panel >
                            </div>
                    </div>      
                    <br />
                      <div class="form-iceland-container">
                                <div class="form-iceland left-columns">
                                    <sb:SingleAttributeSelectionControl id="AttributeSelectionEthinicityControl" 
                                    runat="server" DisplayHeader="false" BuddyControl="EthnicityContainerControl"/>
                                </div>
                               <div class="form-iceland right-columns">
                                  <asp:Panel  ID="EthnicityContainerControl" runat="server">
                                  <asp:CustomValidator ID="EthinicityValidator" runat="server" ErrorMessage="Please select ethinicity." />
                                  <label id="EthnicityLabelControl" for="EthnicityControl">Ethnicity:</label>
                                    <sb:CoolTipInfo ID="CoolTipInfo4" Content="Use to indicate if scholarship has ethnic affiliation." runat="server" />                                  
                                    <sb:LookupItemCheckboxList id="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL" MultiColumn="true"/>
                                    <br />
                                  </asp:Panel >

                               </div>
                     </div>      
                    <br />
                    <br />
                    <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionReligionControl" 
                                runat="server" DisplayHeader="false" BuddyControl="ReligionContainerControl"/>
                            </div>
                           <div class="form-iceland right-columns">
                                <asp:Panel  ID="ReligionContainerControl" runat="server">
                                <asp:CustomValidator ID="ReligionValidator" runat="server" ErrorMessage="Please select religion." />
                                <label id="ReligionLabelControl" for="ReligionControl">Religion:</label>
                                <sb:CoolTipInfo ID="CoolTipInfo2" Content="Use to indicate if scholarship has religious affiliation." runat="server" />
                                
                                <sb:LookupItemCheckboxList id="ReligionControl" runat="server"  MultiColumn="true" LookupServiceSpringContainerKey="ReligionDAL"/>
                                <br />
                              </asp:Panel >
                            </div>

                    </div>      
                    <br />
                    <br />
                    <div class="form-iceland-container">
                            <div class="form-iceland left-columns">
                                <sb:SingleAttributeSelectionControl id="AttributeSelectionGendersControl" 
                                runat="server" DisplayHeader="false" BuddyControl="GendersContainerControl"/>
                            </div>
                           <div class="form-iceland right-columns">
                                <asp:Panel  ID="GendersContainerControl" runat="server">
                                <asp:CustomValidator ID="GendersValidator" runat="server" ErrorMessage="Please select Gender." />
                            
                                <label id="GendersLabelControl" for="GendersControl">Genders:</label>
                                <sb:CoolTipInfo ID="CoolTipInfo3" Content="Use to indicate if scholarship has gender affiliation." runat="server" />

                                <br />
                                <sb:FlagEnumCheckBoxList id="GendersControl" runat="server"  
                                 ExcludeKeys="1"    MultiColumn="true" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.Genders, ScholarBridge.Domain" />
                                </asp:Panel >
                           </div>
                    </div>   
                    
                   <br /> 
                    <br />
                    <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionESLELLControl" runat="server" DisplayHeader="false" BuddyControl="ESLELLContainerControl"/>
                        </div>
                        <div class="form-iceland right-columns">
                              <label for="ESLELLControl">ESL / ELL </label>
                              <sb:CoolTipInfo ID="CoolTipInfo6" Content="Use to indicate if applicant is an English Language Learner or considers English a Second Language." runat="server" />
                              <asp:Panel  ID="ESLELLContainerControl" runat="server" Visible="false">
                                <asp:Literal ID="ESLELLControl" runat="server" />
                              </asp:Panel >
                        </div>
                   </div>
                    
                                            
              </asp:PlaceHolder>
	        </div>
	        <h3><a href="#" ><span class="form-section-title">Applicant Interests <asp:CustomValidator ID="InterestsCustomValidator" runat="server" OnServerValidate="InterestsCustomValidator_OnServerValidate" ErrorMessage="This section contains following errors:" /></span><br /><span>Interests includes Field of Study and Careers. Also includes affiliations with Organizations and Companies.</span><hr /></a></h3>
	        <div>
		            <asp:PlaceHolder ID="SeekerInterestsControlContainer" runat="server">
              <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                           <sb:SingleAttributeSelectionControl id="AttributeSelectionAcademicAreasControl" 
                            runat="server" DisplayHeader="True" BuddyControl="AcademicAreasContainerControl"/>
                        </div>
                       <div class="form-iceland right-columns">
                          <br /><br />
                          <asp:Panel  ID="AcademicAreasContainerControl" runat="server">
                            <label id="AcademicAreasLabelControl" for="AcademicAreasControl">Field of Study:</label>
                            <sb:LookupDialog ID="AcademicAreasControlDialogButton" runat="server" BuddyControl="AcademicAreasControl" ItemSource="AcademicAreaDAL" Title="Field of Study Selection"/>
                            <asp:TextBox ID="AcademicAreasControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                            <sb:CoolTipInfo ID="CoolTipInfoAcademicAreasControl" Content="Use to indicate Major or academic area of interest." runat="server" />
                            <asp:CustomValidator ID="AcademicAreasValidator" runat="server" ErrorMessage="Please select Field of Study." />
                  
                            <asp:RequiredFieldValidator ID="AcademicAreasControlRequiredValidator" ControlToValidate="AcademicAreasControl" runat="server" ErrorMessage="Select at least one field of study"></asp:RequiredFieldValidator>
                            <br />
                          </asp:Panel >
                        </div> 
                </div> 
               <br />
              <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                        <br /><br />
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionCareersControl" 
                            runat="server" DisplayHeader="false" BuddyControl="CareersContainerControl"/>
                        </div>
                       <div class="form-iceland right-columns">
                          <br /><br />             
                          <asp:Panel  ID="CareersContainerControl" runat="server">
                            <label id="CareersLabelControl" for="CareersControl">Careers:</label>
                            <sb:LookupDialog ID="CareersControlDialogButton" runat="server" BuddyControl="CareersControl" ItemSource="CareerDAL" Title="Career Selection"/>
                            <asp:TextBox ID="CareersControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                            <sb:CoolTipInfo ID="CoolTipInfoCareersControl" Content="Use to indicate career objectives." runat="server" />
                             <asp:CustomValidator ID="CareersValidator" runat="server" ErrorMessage="Please select Careers." />
                  
                            <asp:RequiredFieldValidator ID="CareersControlRequiredValidator" ControlToValidate="CareersControl" runat="server" ErrorMessage="Select at least one career"></asp:RequiredFieldValidator>
                            <br />
                          </asp:Panel >
                        </div> 
                </div> 
              <br />
              <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                        <br /><br />
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionOrganizationsControl" 
                            runat="server" DisplayHeader="false" BuddyControl="OrganizationsContainerControl"/>
                        </div>
                       <div class="form-iceland right-columns">
                          <br />         
                          <asp:Panel  ID="OrganizationsContainerControl" runat="server">
                            <label id="OrganizationsLabelControl" for="OrganizationsControl">Organization:</label>
                            <sb:LookupDialog ID="OrganizationsControlDialogButton" runat="server" BuddyControl="OrganizationsControl" ItemSource="SeekerMatchOrganizationDAL" Title="Organization Affiliation Selection"/>
                            <asp:TextBox ID="OrganizationsControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                            <sb:CoolTipInfo ID="CoolTipInfoOrganizationsControl" Content="Use to indicate scholarship is available to members of an organization, service club, church, sorority or fraternity." runat="server" />
                             <asp:CustomValidator ID="OrganizationsValidator" runat="server" ErrorMessage="Please select Organizations." />
                  
                            <asp:RequiredFieldValidator ID="OrganizationsControlRequiredValidator" ControlToValidate="OrganizationsControl" runat="server" ErrorMessage="Select at least one organization"></asp:RequiredFieldValidator>
                            <br />
                          </asp:Panel >
                        </div> 
                </div> 
              <br />
              <div class="form-iceland-container">
                        <div class="form-iceland left-columns">
                        <br /><br />
                            <sb:SingleAttributeSelectionControl id="AttributeSelectionCompanyControl" 
                            runat="server" DisplayHeader="false" BuddyControl="CompanyContainerControl"/>
                        </div>
                       <div class="form-iceland right-columns">
                          <br /><br />         
                      <asp:Panel  ID="CompanyContainerControl" runat="server">
                        <label id="CompanyLabelControl" for="CompanyControl">Company:</label>
                       <sb:LookupDialog ID="CompanyControlDialogButton" runat="server" BuddyControl="CompanyControl" ItemSource="CompanyDAL" Title="Company Selection"/>
                        <asp:TextBox ID="CompanyControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                        <sb:CoolTipInfo ID="CoolTipInfoCompanyControl" Content="Use to indicate scholarship is available to employees, family members, or people affiliated with a company." runat="server" />
                          <asp:CustomValidator ID="CompanyValidator" runat="server" ErrorMessage="Please select Company." />
                  
                        <asp:RequiredFieldValidator ID="CompanyControlRequiredValidator" ControlToValidate="CompanyControl" runat="server" ErrorMessage="Select at least one company"></asp:RequiredFieldValidator>
                        <br />
                      </asp:Panel >
                    </div> 
                </div> 
              <br />          
            </asp:PlaceHolder>
	        </div>
	        <h3><a href="#" ><span class="form-section-title">Applicant Activities<asp:CustomValidator ID="ActivitiesCustomValidator" runat="server"  OnServerValidate="ActivitiesCustomValidator_OnServerValidate" ErrorMessage="This section contains following errors:" /></span><br /><span>Activities includes Hobbies, Sports and Clubs. Also includes Work or Service. </span><hr /></a></h3>
	        <div>
		    <asp:PlaceHolder ID="SeekerActivitiesControlContainer" runat="server">
              <div class="form-iceland-container">
                <div class="form-iceland left-columns">
                    <sb:SingleAttributeSelectionControl id="AttributeSelectionHobbiesControl" 
                    runat="server" DisplayHeader="true" BuddyControl="HobbiesContainerControl"/>
                </div>
               
               <div class="form-iceland right-columns">
               <br /><br />
                  <asp:Panel  ID="HobbiesContainerControl" runat="server">
                    <label id="SeekerHobbiesLabelControl" for="SeekerHobbiesControl">Hobbies:</label>
                    <sb:LookupDialog ID="SeekerHobbiesControlDialogButton" runat="server" BuddyControl="SeekerHobbiesControl" ItemSource="SeekerHobbyDAL" Title="Hobby Selection"/>
                    <asp:TextBox ID="SeekerHobbiesControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                    <sb:CoolTipInfo ID="CoolTipInfoSeekerHobbiesControl" Content="Use to indicate applicant hobbies." runat="server" />
                      <asp:CustomValidator ID="SeekerHobbiesValidator" runat="server" ErrorMessage="Please select Hobbies." />
                      <br />
                  </asp:Panel >
                  </div> 
                </div> 
              <br />
              <div class="form-iceland-container">
                <div class="form-iceland left-columns">
                    <sb:SingleAttributeSelectionControl id="AttributeSelectionSportsControl" 
                    runat="server" DisplayHeader="false" BuddyControl="SportsContainerControl"/>
                </div>
               
               <div class="form-iceland right-columns">
                  <asp:Panel  ID="SportsContainerControl" runat="server">
                    <label id="SportsLabelControl" for="SportsControl">Sports:</label>
                    <sb:LookupDialog ID="SportsControlDialogButton" runat="server" BuddyControl="SportsControl" ItemSource="SportDAL" Title="Sport Participation Selection"/>
                    <asp:TextBox ID="SportsControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                    <sb:CoolTipInfo ID="CoolTipInfoSportsControl" Content="Use to indicate sports the applicant participates in." runat="server" />
                      <asp:CustomValidator ID="SportsValidator" runat="server" ErrorMessage="Please select Sports." />
                    <br />
                  </asp:Panel >
                  </div>
               </div>     
              <br />
              <div class="form-iceland-container">
                <div class="form-iceland left-columns">
                    <sb:SingleAttributeSelectionControl id="AttributeSelectionClubsControl" 
                    runat="server" DisplayHeader="false" BuddyControl="ClubsContainerControl"/>
                </div>
               
               <div class="form-iceland right-columns">
                  <asp:Panel  ID="ClubsContainerControl" runat="server">
                    <label id="ClubsLabelControl" for="ClubsControl">Clubs:</label>
                   <sb:LookupDialog ID="ClubsControlDialogButton" runat="server" BuddyControl="ClubsControl" ItemSource="ClubDAL" Title="Club Participation Selection"/>
                    <asp:TextBox ID="ClubsControl" ReadOnly="true" TextMode="MultiLine"  Rows="5"  Columns="23" runat="server"></asp:TextBox>
                    <sb:CoolTipInfo ID="CoolTipInfoClubsControl" Content="Use to indicate applicant club membership." runat="server" />
                   <asp:CustomValidator ID="ClubsValidator" runat="server" ErrorMessage="Please select Clubs." />
                       <br />
                  </asp:Panel >
                </div>
               </div> 
              <br />
              <div class="form-iceland-container">
                <div class="form-iceland left-columns">
                    <sb:SingleAttributeSelectionControl id="AttributeSelectionWorkTypeControl" 
                    runat="server" DisplayHeader="false" BuddyControl="WorkTypeContainerControl"/>
                </div>
               
                    <div class="form-iceland right-columns">
                      <asp:Panel  ID="WorkTypeContainerControl" runat="server">
                        <label id="WorkTypeLabelControl" for="WorkTypeControl">Applicant Working:</label>
                        <asp:Literal ID="WorkTypeControl" runat="server" />
                        <sb:CoolTipInfo ID="CoolTipInfoWorkTypeControl" Content="Applicant will be asked to provide detail. Use this field to indicate if an applicant working is part of your selection criteria." runat="server" />
                        <br />
                      </asp:Panel >
                    </div> 
                </div>
              <br />
              
               <div class="form-iceland-container">
                    <div class="form-iceland left-columns">
                        <sb:SingleAttributeSelectionControl id="AttributeSelectionServiceTypeControl" 
                        runat="server" DisplayHeader="false" BuddyControl="ServiceTypeContainerControl"/>
                    </div>
                   
                        <div class="form-iceland right-columns">
                          <asp:Panel  ID="ServiceTypeContainerControl" runat="server">
                            <label id="ServiceTypeLabelControl" for="ServiceTypeControl">Applicant Service:</label>
                           <asp:Literal ID="ServiceTypeControl" runat="server" />
                            <sb:CoolTipInfo ID="CoolTipInfoServiceTypeControl" Content="Applicant will be asked to provide detail. Use this field to indicate if an applicant engaged in service is part of your selection criteria." runat="server" />
                            <br />
                          </asp:Panel >
                      </div> 
                </div>
              <br />                      
            </asp:PlaceHolder>
	        </div>
	      
        </div>
    </div>
</div>

