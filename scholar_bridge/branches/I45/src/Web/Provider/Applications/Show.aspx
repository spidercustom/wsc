﻿<%@ Page Language="C#" MasterPageFile="~/PrintView.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Provider.Applications.Show" Title="Application | Show" %>
<%@ Register src="~/Common/PrintApplication.ascx" tagname="PrintApplication" tagprefix="sb" %>
<%@ Register src="~/Common/PrintExternalApplication.ascx" tagname="PrintExternalApplication" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PrintViewPageHeader" runat="server">
   <div id="PreviewOptions" class="non-printable">
        <a class="button" onclick="window.print();"><span>Print This Page</span></a>
    </div>
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
 <%--<div runat="Server" visible="<% #!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this) %>" ><asp:Image   ID="Image1" ImageUrl="~/images/PicTopSeekerLoggedInHome.gif" Width="918px" Height="15px" runat="server" /></div>
    <div runat="Server" visible="<% #!ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this) %>"><asp:Image ID="Image2" ImageUrl="~/images/PicBottomSeekerLoggedInHome.gif" Width="918px" Height="265px" runat="server" /></div>
--%></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:ScholarshipTitleStripe id="ScholarshipTitleStripeControl" runat="server" HideInPrintView="true" />
    <br /><br /><br /><br />
    <asp:PlaceHolder ID="ApplicationPlaceHolder" runat="server" />
</asp:Content>