﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.PressRoom.Show" Title="Press Room" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Import Namespace="ScholarBridge.Web.Extensions" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Press Room" />
    <style type="text/css">
     </style>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Press Room</h2>
    <a style="float:right;" class="button" onclick="window.print();"><span>Print This Page</span></a>
    <asp:Label ID="errorMessage" runat="server" Text="Article not found." Visible="false" CssClass="errorMessage"/>

    <h3><asp:Literal ID="DateControl" runat="server" /> - <asp:Literal ID="TitleControl" runat="server" /></h3>

    <pre style="font-family:Arial; font-size:11px; margin:10px 0px;">
        <asp:Label ID="BodyControl" runat="server" CssClass="word_wrap"/>
    </pre>

    <div id="linkarea" class="hide-in-print" runat="server">
       <ul>
          <li><asp:HyperLink ID="PressRoomLnk" runat="server" NavigateUrl="~/PressRoom/">Return to Press Room</asp:HyperLink></li>
          <li><asp:HyperLink ID="HomeLnk" runat="server" NavigateUrl="~/">Home</asp:HyperLink></li>
      </ul>
    </div>
</asp:Content>
