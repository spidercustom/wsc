using System;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class CleanupDALTest : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public CleanupDAL CleanupDAL { get; set; }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void can_not_pass_null_user_to_remove_row()
        {
            CleanupDAL.RemoveUserRowFromDatabase(null);
            Assert.Fail();
        }

        [Test]
        public void can_find_nonactivated_users()
        {
			// run the remove to assure that the counts won't get screwed up later...
			CleanupDAL.RemoveNonConfirmedUsersFromDatabase(5);

			var initialAll = UserDAL.FindAll("Username").Count;
            var initialInactive = UserDAL.FindNonConfirmedUsers(5).Count;
            
            var activeUser = new User
                                 {
                                     Username = "activated@example.com",
                                     Password = "@abc123K",
                                     Question = "Who am I?",
                                     Answer = "A test",
                                     Email = "active@example.com",
                                     Name = new PersonName {FirstName = "TestFirst", LastName = "TestLast"},
                                     IsActive = true,
                                     LastUpdate = new ActivityStamp
                                                      {
                                                          On = DateTime.Now.AddDays(-10)
                                                      }
                                 };
            var inactiveUser = new User
                                   {
                                       Username = "inactive@example.com",
                                       Password = "@abc123K",
                                       Question = "Who am I?",
                                       Answer = "A test",
                                       Email = "inactive@example.com",
                                       Name = new PersonName {FirstName = "TestFirst", LastName = "TestLast"},
                                       IsActive = false,
                                       LastUpdate = new ActivityStamp
                                                        {
                                                            On = DateTime.Now.AddDays(-10)
                                                        }
                                   };

            var newInactiveUser = new User
                                      {
                                          Username = "new_inactive@example.com",
                                          Password = "@abc123K",
                                          Question = "Who am I?",
                                          Answer = "A test",
                                          Email = "new_inactive@example.com",
                                          Name = new PersonName {FirstName = "TestFirst", LastName = "TestLast"},
                                          IsActive = false,
                                          LastUpdate = new ActivityStamp
                                                           {
                                                               On = DateTime.Now.AddDays(-2)
                                                           }
                                      };

			// this emulates the user that changes email address (set to IsActive=false) but hasn't yet re-confirmed email address
			var inactiveUserThatIsApproved = new User
													{
														Username = "approved_inactive@example.com",
														Password = "@abc123K",
														Question = "Who am I?",
														Answer = "A test",
														Email = "new_inactive@example.com",
														Name = new PersonName { FirstName = "TestFirst", LastName = "TestLast" },
														IsActive = false,
														IsApproved = true,
														LastUpdate = new ActivityStamp
														{
															On = DateTime.Now.AddDays(-10)
														}
													};


            UserDAL.Insert(activeUser);
            UserDAL.Insert(inactiveUser);
			UserDAL.Insert(newInactiveUser);
			UserDAL.Insert(inactiveUserThatIsApproved);

            Assert.That(UserDAL.FindAll("Username").Count, Is.EqualTo(initialAll + 4));
            Assert.That(UserDAL.FindNonConfirmedUsers(5).Count, Is.EqualTo(initialInactive + 1));

            CleanupDAL.RemoveNonConfirmedUsersFromDatabase(5);

            Assert.That(UserDAL.FindAll("Username").Count, Is.EqualTo(initialAll + 3));
            Assert.That(UserDAL.FindNonConfirmedUsers(5).Count, Is.EqualTo(initialInactive));
        }

    }
}