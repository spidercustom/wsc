﻿using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class CountyDALTests : LookupTestBase<CountyDAL, County>
    {
        public StateDAL StateDAL { get; set; }

        protected override County CreateLookupObject(string name)
        {
            var WA = StateDAL.FindByAbbreviation("WA");
            County result = base.CreateLookupObject(name);
            result.State = WA;
            return result;
        }

        [Test]
        public void find_by_state()
        {
            var baseCount = DAL.FindByState("WA").Count;
            var newCountyInWA = CreateLookupObject("Test 1");
            DAL.Insert(newCountyInWA);

            var newList = DAL.FindByState("WA");
            Assert.AreEqual(baseCount + 1, newList.Count);
        }


        [Test]
        public void validate_get_lookup_item_redirections()
        {
            var baseWACount = DAL.FindByState("WA").Count;
            var baseAllCount = DAL.FindAll().Count;

            var newEntityInWA = CreateLookupObject("Test 1");
            DAL.Insert(newEntityInWA);

            var asLookupDAL = (IGenericLookupDAL<County>)DAL;
            Assert.AreEqual(baseAllCount + 1, asLookupDAL.GetLookupItems(null).Count);
            Assert.AreEqual(baseWACount + 1, asLookupDAL.GetLookupItems("WA").Count);
        }
    }
}