﻿using System;

namespace ScholarBridge.Web.Exceptions
{
    [global::System.Serializable]
    public class NoOrganizationIsInContextException : ApplicationException //TODO: Do we need ScholarBridge exception type?
    {
        public NoOrganizationIsInContextException() { }
        public NoOrganizationIsInContextException(string message) : base(message) { }
        public NoOrganizationIsInContextException(string message, Exception inner) : base(message, inner) { }
        protected NoOrganizationIsInContextException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
