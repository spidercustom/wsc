﻿using System;


namespace ScholarBridge.Business.Exceptions
{
    [global::System.Serializable]
    public class ProviderNotApprovedException : ApplicationException
    {
        public ProviderNotApprovedException()
            : base("Provider is not yet approved")
        { }
        protected ProviderNotApprovedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [global::System.Serializable]
    public class IntermediaryNotApprovedException : ApplicationException
    {
        public IntermediaryNotApprovedException()
            : base("Intermediary is not yet approved")
        { }
        protected IntermediaryNotApprovedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
