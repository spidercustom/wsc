﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(308)]
	public class AddEslEll : Migration
	{
		public override void Up()
		{
			Database.AddColumn("SBScholarship", new Column("MatchCriteriaEslEll", DbType.Boolean, ColumnProperty.Null));
			Database.AddColumn("SBSeeker", new Column("IsEslEll", DbType.Boolean, ColumnProperty.Null));
			Database.AddColumn("SBApplication", new Column("IsEslEll", DbType.Boolean, ColumnProperty.Null));
		}

		public override void Down()
		{
			Database.RemoveColumn("SBScholarship", "MatchCriteriaEslEll");
			Database.RemoveColumn("SBSeeker", "IsEslEll");
			Database.RemoveColumn("SBApplication", "IsEslEll");
		}
	}
}
