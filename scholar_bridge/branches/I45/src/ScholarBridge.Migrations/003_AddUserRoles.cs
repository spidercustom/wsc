﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(3)]
    public class AddUserRoles : Migration
    {
        public const string TABLE_NAME = "SB_UserRolesRT";

        protected static readonly string[] COLUMNS = new[] {"UserId", "RoleId"};
        protected static readonly string FK_ROLES = string.Format("FK_{0}_Roles", TABLE_NAME);
        protected static readonly string FK_USERS = string.Format("FK_{0}_Users", TABLE_NAME);


        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKey),
                              new Column(COLUMNS[1], DbType.Int32, ColumnProperty.PrimaryKey)
                );

            Database.AddForeignKey(FK_USERS, TABLE_NAME, COLUMNS[0], "SB_User", "Id");
            Database.AddForeignKey(FK_ROLES, TABLE_NAME, COLUMNS[1], "SB_Role", "Id");
        }

        public override void Down()
        {
            Database.RemoveForeignKey(TABLE_NAME, FK_ROLES);
            Database.RemoveForeignKey(TABLE_NAME, FK_USERS);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}