﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(52)]
    public class AddAcademicArea : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "AcademicArea";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
