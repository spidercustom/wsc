using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(242)]
    public class AddScholarshipClassRankRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBScholarshipClassRankRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBScholarship"; }
        }

        protected override string SecondTableName
        {
            get { return "SBClassRankLUT"; }
        }
    }
}