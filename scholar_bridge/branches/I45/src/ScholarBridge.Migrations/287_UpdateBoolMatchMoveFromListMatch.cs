﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(287)]
    public class UpdateBoolMatchFromListView : Migration
    {
        public override void Up()
        {
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchBools]");
			Database.ExecuteNonQuery(SB_MATCH_IN_BOOL);
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInEnum]");
			Database.ExecuteNonQuery(SB_MATCH_IN_ENUM);
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInList]");
			Database.ExecuteNonQuery(SB_MATCH_IN_LIST);
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBScholarshipCriteriaCounts]");
			Database.ExecuteNonQuery(SB_SCHOLARSHIP_CRITERIA_COUNTS);

		}

		public override void Down()
		{
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchBools]");
			Database.ExecuteNonQuery(AddMatchViews.SBMatchBools); //159
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInEnum]");
			Database.ExecuteNonQuery(UpdateMatchViewChangeLastAttendedToStudentGroup.SB_MATCH_IN_ENUM); //286
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchInList]");
			Database.ExecuteNonQuery(UpdateMatchViewAddSupportSituation.SB_MATCH_IN_LIST); //234
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBScholarshipCriteriaCounts]");
			Database.ExecuteNonQuery(ChangeScholarshipCriteriaCountsAddFundingProfileAttribute.CHANGED_VIEW); //235

		}
		#region New Criteria count view

    	public const string SB_SCHOLARSHIP_CRITERIA_COUNTS = @"
CREATE VIEW [dbo].[SBScholarshipCriteriaCounts]
AS

SELECT SBScholarshipID, [1] as prefCrit, [2] as minCrit
FROM
(
    SELECT SBScholarshipID, SBUsageTypeIndex 
    FROM SBScholarshipMatchCriteriaAttributeUsageRT

    union all

    SELECT SBScholarshipID, SBUsageTypeIndex 
    FROM SBScholarshipFundingProfileAttributeUsageRT

    union all
	-- determine if college is required
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship where SBScholarship.MatchCriteriaCollegeType > 1 -- 1=Any
		
    union all
	-- determine if School Types is required (no match if all selected)
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    where SBScholarship.MatchCriteriaSchoolTypes between 1 and 30 -- 31 = All school types
		
    union all
	-- determine if Seeker Status is required (no match if all selected)
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    where SBScholarship.MatchCriteriaSeekerStatuses between 1 and 6 -- 7 = All seeker statuses
		
    union all
	-- determine if Academic Program is required (no match if all selected)
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    where SBScholarship.MatchCriteriaAcademicPrograms between 1 and 6 -- 7 = All Academic Programs
		
    union all
	-- Student Groups is always requred so we'll just add a minimum requirement to the count
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
    
    union all
	-- SupportedSituation (Types of Support) is always requred so we'll just add a minimum requirement to the count
    SELECT	SBScholarshipID, 
			2 as SBUsageTypeIndex -- minimum
    FROM SBScholarship 
		
) AS SourceTable
PIVOT
(
    COUNT(SBUsageTypeIndex)
    FOR SBUsageTypeIndex IN ([1], [2])
) AS PivotTable
";
		#endregion

		#region new Bool view
		public const string SB_MATCH_IN_BOOL =
    @"
CREATE VIEW [dbo].[SBMatchBools]
AS

select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Honors' as criterion, 
	s.MatchCriteriaHonors as selected,
	seeker.SBSeekerId, seeker.Honors as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.Honors=s.MatchCriteriaHonors
where match.SBMatchCriteriaAttributeIndex=33

union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'FirstGeneration', 
	s.MatchCriteriaFirstGeneration as selected,
	seeker.SBSeekerId, seeker.FirstGeneration
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.FirstGeneration=s.MatchCriteriaFirstGeneration
where match.SBMatchCriteriaAttributeIndex=6

union all
-- college match situation 1 - provider selects wa schools and seeker selects wa school box or a specific wa school
-- *****************************************************************************************************
-- Note!!! On all college matches we do not look at the SBScholarshipMatchCriteriaAttributeUsageRT table
-- because this if entered by the provider will always be a required minimum
-- *****************************************************************************************************
select sbscholarshipid, AcademicYear, 2 as SBUsageTypeIndex, 'College' as criterion, 1 as selected, SBSeekerID, 1 as seekerValue  
from SBScholarship,
(
	select distinct sbseekerid from SBSeeker 
	where IsCollegeAppliedInWashington = 1
	or exists (select * from SBSeekerCollegeAppliedRT where SBSeekerId = SBSeeker.SBSeekerId)
) as WASeekers 
WHERE SBScholarship.MatchCriteriaCollegeType = 2 and Stage='Activated'

union all
-- college match situation 2 - provider selects Out of State (college type = 8) schools and seeker checks the OOS box
select sbscholarshipid, AcademicYear, 2 as SBUsageTypeIndex, 'College' as criterion, 1 as selected, SBSeekerID, 1 as seekerValue  
from SBScholarship s
left join SBSeeker on IsCollegeAppliedOutOfState = 1
WHERE s.MatchCriteriaCollegeType = 8  and s.Stage='Activated'

union all
select distinct * from
( 
	-- college match situation 3 - provider selects specific colleges and seeker has 1 or more specific matches 
	select distinct s.sbscholarshipid, s.AcademicYear, 2 as SBUsageTypeIndex, 
				'College' as criterion, 1 as selected, srt.SBSeekerID, 1 as seekerValue  
	from SBScholarship s
	inner join SBScholarshipCollegeRT rt on s.SBScholarshipId=rt.SBScholarshipId
	left join SBSeekerCollegeAppliedRT sRt on rt.SBCollegeIndex=sRt.SBCollegeIndex
	where s.MatchCriteriaCollegeType = 4  and s.Stage='Activated'
	
	union all
	-- college match situation 4 - seeker selected college types match implied types from Provider list
	select distinct s.sbscholarshipid, s.AcademicYear, 2 as SBUsageTypeIndex, 
			'College' as criterion, 1 as selected, SBSeeker.SBSeekerID, 1 as seekerValue  
	from SBScholarship s
	inner join
	(
		select distinct rt.SBScholarshipID, c.SchoolType from SBScholarshipCollegeRT rt 
		inner join SBCollegeLUT c on rt.SBCollegeIndex = c.SBCollegeIndex
		inner join SBScholarship s on s.SBScholarshipID = rt.SBScholarshipID
		where s.MatchCriteriaCollegeType = 4  and s.Stage='Activated'
	) ST on ST.SBScholarshipID = s.SBScholarshipID
	inner join sbseeker on (SBSeeker.SchoolTypes & ST.SchoolType) > 0  
) SeekersMatchingProviderImpliedSchoolTypes

union all
-- Note!  School Type is always a Minimum requirement if entered by the provider
--        so it is not necessary to look at SBScholarshipMatchCriteriaAttributeUsageRT.
select distinct * from 
(
	-- school type match #1:  where any one school type selected by the seeker matches any one selected by the provider
	select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, 'SchoolTypes' as criterion, 
		1 as selected, seeker.SBSeekerID, 1 as seekerValue   
	from SBScholarship s
	left join SBSeeker seeker on (seeker.SchoolTypes & s.MatchCriteriaSchoolTypes) > 0
	where	s.Stage='Activated' and  
			s.MatchCriteriaSchoolTypes between 1 and 30 -- 31 = all school types selected
			
	union all
	-- school type match #2:  where any one school type implied by the colleges selected by the seeker 
	-- matches any one school type selected by the provider
	select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, 'SchoolTypes' as criterion, 
		1 as selected, ST.SBSeekerID, 1 as seekerValue   
	from SBScholarship s
	inner join
	(
		select rt.SBSeekerID, sum(distinct c.SchoolType) as SchoolType from SBSeekerCollegeAppliedRT rt 
		inner join SBCollegeLUT c on rt.SBCollegeIndex = c.SBCollegeIndex
		group by rt.SBSeekerID
	) ST on (ST.SchoolType & s.MatchCriteriaSchoolTypes) > 0
	where s.Stage='Activated' 
	  and s.MatchCriteriaSchoolTypes between 1 and 30 -- 31 = all school types selected

) SchoolTypeMatches

union all
-- check for types of financial support matches
-- Note!  Financial support is always a Minimum requirement so it is not necessary to look at 
--        SBScholarshipMatchCriteriaAttributeUsageRT.
select distinct s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, 'SupportSituation' as criterion,
		1 as selected, sRt.SBSeekerID, 1 as seekerValue   
from SBScholarship s
inner join SBScholarshipSupportRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSupportRT sRt on rt.SBSupportIndex=sRt.SBSupportIndex
where s.Stage ='Activated'

union all
-- Any match with provider ClassRank will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ClassRank' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipClassRankRT rt on s.SBScholarshipId = rt.SBScholarshipId
left join SBSeeker sk on rt.SBClassRankIndex = sk.ClassRankIndex
where match.SBMatchCriteriaAttributeIndex=30

union all
-- Any (one or more) match with provider HighSchool will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'HighSchool' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage = 'Activated'
inner join SBScholarshipHighSchoolRT rt on s.SBScholarshipId = rt.SBScholarshipId
left join SBSeeker seeker on rt.SBHighSchoolIndex = seeker.CurrentHighSchoolIndex
where match.SBMatchCriteriaAttributeIndex = 13

union all
-- Any (one or more) match with provider SchoolDistrict will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SchoolDistrict' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSchoolDistrictRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker sk on rt.SBSchoolDistrictIndex=sk.SchoolDistrictIndex
where match.SBMatchCriteriaAttributeIndex=17

union all
-- Any (one or more) match with provider City will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'City' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.SBCityIndex=seeker.CityIndex
where match.SBMatchCriteriaAttributeIndex=3

union all
-- Any (one or more) match with provider County will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'County' as criterion,
		1 as selected, seeker.SBSeekerID, case when seeker.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCountyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.SBCountyIndex=seeker.CountyIndex
where match.SBMatchCriteriaAttributeIndex=8


union all
-- Any (one or more) match with provider Ethnicity will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Ethnicity' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipEthnicityRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerEthnicityRT sRt on rt.SBEthnicityIndex=sRt.SBEthnicityIndex
where match.SBMatchCriteriaAttributeIndex=9

union all
-- Any (one or more) match with provider Religion will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Religion' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipReligionRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerReligionRT sRt on rt.SBReligionIndex=sRt.SBReligionIndex
where match.SBMatchCriteriaAttributeIndex=16

union all
-- Any (one or more) match with provider AcademicArea will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'AcademicArea' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipAcademicAreaRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerAcademicAreaRT sRt on rt.SBAcademicAreaIndex=sRt.SBAcademicAreaIndex
where match.SBMatchCriteriaAttributeIndex=0

union all
-- Any (one or more) match with provider Careers will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Career' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCareerRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCareerRT sRt on rt.SBCareerIndex=sRt.SBCareerIndex
where match.SBMatchCriteriaAttributeIndex=2

union all
-- Any (one or more) match with provider Organizations will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'MatchOrganization' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerMatchOrganizationRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerMatchOrganizationRT sRt on rt.SBSeekerMatchOrganizationIndex=sRt.SBSeekerMatchOrganizationIndex
where match.SBMatchCriteriaAttributeIndex=14

union all
-- Any (one or more) match with provider Company will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Company' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipCompanyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerCompanyRT sRt on rt.SBCompanyIndex = sRt.SBCompanyIndex
where match.SBMatchCriteriaAttributeIndex=14

union all
-- Any (one or more) match with provider SeekerHobby will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerHobby' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSeekerHobbyRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSeekerHobbyRT sRt on rt.SBSeekerHobbyIndex=sRt.SBSeekerHobbyIndex
where match.SBMatchCriteriaAttributeIndex=19

union all
-- Any (one or more) match with provider Sport will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Sport' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipSportRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerSportRT sRt on rt.SBSportIndex=sRt.SBSportIndex
where match.SBMatchCriteriaAttributeIndex=25

union all
-- Any (one or more) match with provider Club(s) will be considered a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Club' as criterion,
		1 as selected, sRt.SBSeekerID, case when sRt.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipClubRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeekerClubRT sRt on rt.SBClubIndex=sRt.SBClubIndex
where match.SBMatchCriteriaAttributeIndex=4

union all
-- If the provider has specified either preferred or minimum for student service then we have a match
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerServing' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker sk on sk.IsService = 1
where match.SBMatchCriteriaAttributeIndex=24

union all
-- If the provider has specified either preferred or minimum for student service 
-- then we have a match 
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SeekerWorking' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker sk on sk.IsWorking = 1
where match.SBMatchCriteriaAttributeIndex=28

union all
-- If the provider has specified either preferred or minimum for financial need 
-- then we have a match if the seeker fills the financial need challenge statement
select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'FinancialNeed' as criterion,
		1 as selected, sk.SBSeekerID, case when sk.sbseekerid is null then 0 else 1 end as seekerValue   
from SBScholarshipFundingProfileAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker sk on sk.MyChallenge is not null and len(ltrim(rtrim(sk.MyChallenge))) > 0
where match.SBFundingProfileAttributeIndex=1
";
		#endregion

		#region new list view
		public const string SB_MATCH_IN_LIST = @"
CREATE VIEW [dbo].[SBMatchInList]
AS

select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'State'  as criterion,  
	rt.StateAbbreviation as idx, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
inner join SBScholarshipStateRT rt on s.SBScholarshipId=rt.SBScholarshipId
left join SBSeeker seeker on rt.StateAbbreviation=seeker.AddressState
where match.SBMatchCriteriaAttributeIndex=12

";
#endregion

		#region new enum view
		public const string SB_MATCH_IN_ENUM = @"
Create VIEW [dbo].[SBMatchInEnum]
AS

select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'StudentGroup' as criterion, 
	s.MatchCriteriaStudentGroups as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on
		(	( (seeker.LastAttended & s.MatchCriteriaStudentGroups) = seeker.LastAttended ) or
			( (seeker.TypeOfCollegeStudent & s.MatchCriteriaStudentGroups) = seeker.TypeOfCollegeStudent) )
where match.SBMatchCriteriaAttributeIndex=26

union all
-- if provider hasn't selected all academic programs then look for a match with seeker.  
-- if provider specifies specific academic programs then the match type is Minimum (2)
select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, 'AcademicPrograms', 
	s.MatchCriteriaAcademicPrograms as val, seeker.SBSeekerId 
from SBScholarship s 
left join SBSeeker seeker on (seeker.AcademicPrograms & s.MatchCriteriaAcademicPrograms) > 0 -- any match
where s.MatchCriteriaAcademicPrograms between 1 and 6 -- 7 = All Academic Programs 
  and s.Stage='Activated'

union all
-- if provider hasn't selected all Seeker Statuses then look for a match with seeker.  
-- if provider specifies specific Seeker Statuses then the match type is Minimum (2)
select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, 'SeekerStatuses', 
	s.MatchCriteriaSeekerStatuses as val, seeker.SBSeekerId
from SBScholarship s 
left join SBSeeker seeker on (seeker.SeekerStatuses & s.MatchCriteriaSeekerStatuses) > 0 -- any match
where s.MatchCriteriaSeekerStatuses between 1 and 6 -- 7 = all statuses selected
  and s.Stage='Activated'
  
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Gender', 
	s.MatchCriteriaGenders as val, seeker.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.Gender & s.MatchCriteriaGenders) = seeker.Gender
where match.SBMatchCriteriaAttributeIndex=10
";
		#endregion
	}
}