﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(168)]
    public class ChangeScholarshipChangeGPADatatType : Migration
    {
        private const string TABLE_NAME = "SBScholarship";
        private const string GPA_LESS_THAN = "MatchCriteriaGPALessThan";
        private const string GPA_GREATER_THAN = "MatchCriteriaGPAGreaterThan";

        public override void Up()
        {
            var greaterThanColumn = Database.GetColumnByName(TABLE_NAME, GPA_GREATER_THAN);
            var lessThanColumn = Database.GetColumnByName(TABLE_NAME, GPA_LESS_THAN);
            
            lessThanColumn.Type = greaterThanColumn.Type = DbType.Double;

            Database.ChangeColumn(TABLE_NAME, greaterThanColumn);
            Database.ChangeColumn(TABLE_NAME, lessThanColumn);
        }

        public override void Down()
        {
            var greaterThanColumn = Database.GetColumnByName(TABLE_NAME, GPA_GREATER_THAN);
            var lessThanColumn = Database.GetColumnByName(TABLE_NAME, GPA_LESS_THAN);

            lessThanColumn.Type = greaterThanColumn.Type = DbType.Int32;

            Database.ChangeColumn(TABLE_NAME, greaterThanColumn);
            Database.ChangeColumn(TABLE_NAME, lessThanColumn);
        }
    }
}