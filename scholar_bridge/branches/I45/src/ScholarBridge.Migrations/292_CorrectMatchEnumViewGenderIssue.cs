﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(292)]
	public class CorrectMatchEnumViewGenderIssue : Migration
    {
        public override void Up()
        {
			Database.ExecuteNonQuery("if exists (select * from sys.views where name = 'SBMatchInEnum') DROP VIEW [dbo].[SBMatchInEnum]");
			Database.ExecuteNonQuery(SB_MATCH_IN_ENUM_VIEW);

		}

		public override void Down()
		{
			Database.ExecuteNonQuery("if exists (select * from sys.views where name = 'SBMatchInEnum') DROP VIEW [dbo].[SBMatchInEnum]");
			Database.ExecuteNonQuery(UpdateBoolMatchFromListView.SB_MATCH_IN_ENUM); //287

		}
		#region New Criteria count view

    	public const string SB_MATCH_IN_ENUM_VIEW = @"
CREATE VIEW [dbo].[SBMatchInEnum]
AS

select distinct s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'StudentGroup' as criterion, 
	s.MatchCriteriaStudentGroups as val, seeker.SBSeekerId 
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on
		(	( (seeker.LastAttended & s.MatchCriteriaStudentGroups) = seeker.LastAttended ) or
			( (seeker.TypeOfCollegeStudent & s.MatchCriteriaStudentGroups) = seeker.TypeOfCollegeStudent) )
where match.SBMatchCriteriaAttributeIndex=26

union all
-- if provider hasn't selected all academic programs then look for a match with seeker.  
-- if provider specifies specific academic programs then the match type is Minimum (2)
select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, 'AcademicPrograms', 
	s.MatchCriteriaAcademicPrograms as val, seeker.SBSeekerId 
from SBScholarship s 
left join SBSeeker seeker on (seeker.AcademicPrograms & s.MatchCriteriaAcademicPrograms) > 0 -- any match
where s.MatchCriteriaAcademicPrograms between 1 and 6 -- 7 = All Academic Programs 
  and s.Stage='Activated'

union all
-- if provider hasn't selected all Seeker Statuses then look for a match with seeker.  
-- if provider specifies specific Seeker Statuses then the match type is Minimum (2)
select s.SBScholarshipId, s.AcademicYear, 2 as SBUsageTypeIndex, 'SeekerStatuses', 
	s.MatchCriteriaSeekerStatuses as val, seeker.SBSeekerId
from SBScholarship s 
left join SBSeeker seeker on (seeker.SeekerStatuses & s.MatchCriteriaSeekerStatuses) > 0 -- any match
where s.MatchCriteriaSeekerStatuses between 1 and 6 -- 7 = all statuses selected
  and s.Stage='Activated'
  
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'Gender', 
	s.MatchCriteriaGenders as val, seeker.SBSeekerId
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on (seeker.Gender & s.MatchCriteriaGenders) > 0
where match.SBMatchCriteriaAttributeIndex=10
";
		#endregion

	}
}