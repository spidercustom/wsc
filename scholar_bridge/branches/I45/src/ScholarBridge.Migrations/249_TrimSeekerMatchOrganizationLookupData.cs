﻿using System;
using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(249)]
    public class TrimSeekerMatchOrganizationLookupData : Migration
	{
        private const string tableName = "SBSeekerMatchOrganizationLUT";
        public override void Up()
        {
            Database.ExecuteNonQuery("DELETE FROM SBApplicationSeekerMatchOrganizationRT" );
            Database.ExecuteNonQuery("DELETE FROM SBScholarshipSeekerMatchOrganizationRT");
			Database.ExecuteNonQuery("DELETE FROM SBSeekerSeekerMatchOrganizationRT");
			Database.ExecuteNonQuery("DELETE FROM " + tableName + " where SeekerMatchOrganization='Boeing' ");
            Database.ExecuteNonQuery("DELETE FROM " + tableName + " where SeekerMatchOrganization='Seattle Metropolitan Credit Union' ");
        }

	    public override void Down()
		{
            
            int adminId = HECBStandardDataHelper.GetAdminUserId(Database);
            string[] columns = GetInsertColumns();
            Database.Insert(tableName, columns, new[] { "Boeing", "Boeing", "0", adminId.ToString(), DateTime.Now.ToString() });
            Database.Insert(tableName, columns, new[] { "Seattle Metropolitan Credit Union", "Seattle Metropolitan Credit Union", "0", adminId.ToString(), DateTime.Now.ToString() });
           
		}

		private string[] GetInsertColumns()
		{
			Column[] columns = Database.GetColumns(tableName);
			return new string[]
			       	{
			       		columns[1].Name,
						columns[2].Name,
						"Deprecated",
                        "LastUpdateBy",
                        "LastUpdateDate"
					};
		}
	}
}
