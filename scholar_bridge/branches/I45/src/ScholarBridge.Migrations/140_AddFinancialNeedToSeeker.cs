﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(140)]
    public class AddFinancialNeedToSeeker : Migration
    {
        private const string TABLE_NAME = "SBSeeker";

        public readonly string[] NEW_COLUMNS
            = { "Fafsa", "UserDerived", "MinimumSeekerNeed", "MaximumSeekerNeed",
              "Emergency", "Traditional" };

        public override void Up()
        {
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[0], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[1], DbType.Boolean, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[2], DbType.Double, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[3], DbType.Double, ColumnProperty.Null);

            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[4], DbType.Boolean, ColumnProperty.Null);
            Database.AddColumn(TABLE_NAME, NEW_COLUMNS[5], DbType.Boolean, ColumnProperty.Null);
        }

        public override void Down()
        {
            foreach (var col in NEW_COLUMNS)
            {
                Database.RemoveColumn(TABLE_NAME, col);
            }
        }
    }
}