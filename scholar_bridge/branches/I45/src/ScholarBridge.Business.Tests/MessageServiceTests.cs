using System;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using Rhino.Mocks;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class MessageServiceTests
    {
        private MockRepository mocks;
        private MessageService messageService;
        public IMessageDAL messageDAL;
        public ISentMessageDAL sentMessageDAL;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            messageDAL = mocks.StrictMock<IMessageDAL>();
            sentMessageDAL = mocks.StrictMock<ISentMessageDAL>();
            messageService = new MessageService
                                 {
                                     MessageDAL = messageDAL,
                                     SentMessageDAL = sentMessageDAL
                                 };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(messageDAL);
            mocks.BackToRecord(sentMessageDAL);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void send_message_throws_exception_if_null()
        {
            messageService.SendMessage(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void archive_message_throws_exception_if_null()
        {
            messageService.ArchiveMessage(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void read_message_throws_exception_if_null()
        {
            messageService.MarkMessageRead(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void delete_related_throws_exception_if_null()
        {
            messageService.DeletedRelatedMessages(null);
            Assert.Fail();
        }

        [Test]
        public void send_message_stores_message_and_sent()
        {
            var m = new Message { IsArchived = false };
            Expect.Call(messageDAL.Insert(m)).Return(m);
            Expect.Call(sentMessageDAL.Insert(Arg<SentMessage>.Matches(sm => sm.Subject == m.Subject))).Return(Arg<SentMessage>.Is.Anything);
            mocks.ReplayAll();

            messageService.SendMessage(m);

            mocks.VerifyAll();
        }

        [Test]
        public void message_archived()
        {
            var m = new Message { IsArchived = false };
            Expect.Call(messageDAL.Update(m)).Return(m);
            mocks.ReplayAll();
            messageService.ArchiveMessage(m);
            mocks.VerifyAll();
            Assert.That(m.IsArchived, Is.True);
        }

        [Test]
        public void archived_message_not_rearchived()
        {
            var m = new Message {IsArchived = true};
            mocks.ReplayAll();
            messageService.ArchiveMessage(m);
            mocks.VerifyAll();
        }

        [Test]
        public void message_read()
        {
            var m = new Message { IsRead = false };
            Expect.Call(messageDAL.Update(m)).Return(m);
            mocks.ReplayAll();
            messageService.MarkMessageRead(m);
            mocks.VerifyAll();
            Assert.That(m.IsRead, Is.True);
        }

        [Test]
        public void read_message_not_reread()
        {
            var m = new Message { IsRead = true };
            mocks.ReplayAll();
            messageService.MarkMessageRead(m);
            mocks.VerifyAll();
        }

        [Test]
        public void delete_related_delegates()
        {
            var s = new Scholarship();
            Expect.Call(() => messageDAL.DeleteRelated(s));
            mocks.ReplayAll();

            messageService.DeletedRelatedMessages(s);

            mocks.VerifyAll();
        }
    }
}