﻿using System;
using System.Text;

namespace ScholarBridge.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsBetween(this DateTime value, DateTime mininum, DateTime maximum)
        {
            return value >= mininum && value <= maximum;
        }

        public static bool IsBetween(this DateTime value, DateTime? mininum, DateTime? maximum)
        {
            return IsBetween(value, mininum ?? DateTime.MinValue, maximum ?? DateTime.MaxValue);
        }

        public static string ToOrdinalDayString(this DateTime value, string otherDatePartPattern)
        {
            var builder = new StringBuilder();
            builder.Append(value.Day.ToString());
            builder.Append(GetOrdinalSuffix(value.Day));
            if (!string.IsNullOrEmpty(otherDatePartPattern))
            {
                builder.Append(" ");
                builder.Append(value.ToString(otherDatePartPattern));
            }
            return builder.ToString();
        }

        //more generic, can be moved to somewhere else
        public static string GetOrdinalSuffix(int number)
        {
            int lastDigit = number % 10;
            if (lastDigit == 1)
                return "st";
            if (lastDigit == 2)
                return "nd";
            if (lastDigit == 3)
                return "rd";

            return "th";
        }
    }
}
