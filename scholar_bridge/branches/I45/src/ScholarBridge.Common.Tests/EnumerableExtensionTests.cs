using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Common.Tests
{

    [TestFixture]
    public class EnumerableExtensionTests
    {
        [Test]
        public void should_iterate_over_foreach()
        {
            int val = 0;
            IEnumerable e = new List<int> {1, 2, 3};
            e.ForEach((int i) => val += i);
            Assert.AreEqual(6, val);
        }
    }
}