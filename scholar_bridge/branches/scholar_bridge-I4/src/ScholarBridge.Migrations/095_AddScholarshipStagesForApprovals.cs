﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(95)]
    public class AddScholarshipStagesForApprovals : Migration
    {
        public const string TABLE_NAME = "SBScholarshipStageLUT";
        public const string PRIMARY_KEY_COLUMN = "SBScholarshipStageIndex";

        private readonly string[] columns = new[] { PRIMARY_KEY_COLUMN, "SBScholarshipStage" };

        public override void Up()
        {
            Database.Update(TABLE_NAME, columns, new[] { "6", "Requested Activation" }, "SBScholarshipStageIndex=6");
            Database.Update(TABLE_NAME, columns, new[] { "7", "Activated" }, "SBScholarshipStageIndex=7");
            Database.Update(TABLE_NAME, columns, new[] { "8", "Rejected" }, "SBScholarshipStageIndex=8");
            Database.Insert(TABLE_NAME, columns, new[] { "9", "Awarded Scholarship" });
        }

        public override void Down()
        {
            Database.Update(TABLE_NAME, columns, new[] { "6", "Active Scholarship" }, "SBScholarshipStageIndex=6");
            Database.Update(TABLE_NAME, columns, new[] { "7", "Activated Scholarship" }, "SBScholarshipStageIndex=7");
            Database.Update(TABLE_NAME, columns, new[] { "8", "Awarded Scholarship" }, "SBScholarshipStageIndex=8");
            Database.Delete(TABLE_NAME, PRIMARY_KEY_COLUMN, "9");
        }
    }
}
