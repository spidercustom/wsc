﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(111)]
    public class ScholarshipStageCompletionState : Migration
    {
        public const string TABLE_NAME = "SBScholarshipCompletedStageRT";
        public static readonly string FK_SCHOLARSHIP_ID = string.Format("FK_{0}_SBScholarshipID", TABLE_NAME);

        private readonly Column[] Columns = new[]
                {
                    new Column("SBScholarshipID", DbType.Int32, ColumnProperty.PrimaryKey), 
                    new Column("SBScholarshipStageName", DbType.String, 50, ColumnProperty.PrimaryKey)
                };

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                Columns);

            Database.AddForeignKey(FK_SCHOLARSHIP_ID, TABLE_NAME, Columns[0].Name, "SBScholarship", "SBScholarshipId");
        }

        public override void Down()
        {
            Database.RemoveTable(TABLE_NAME);
        }
    }
}