using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using NUnit.Framework;
using ScholarBridge.Common.DayOfYear;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class ScholarshipTests
    {
        [Test]
        public void scholarship_min_value_must_be_greater_than_zero()
        {
            var s = CreateValidScholarship();
            s.MinimumAmount = -1;
            var results = Validation.Validate(s);

            Assert.IsFalse(results.IsValid);
            Assert.AreEqual(1, results.Count);

            s.MinimumAmount = 0;
            var results2 = Validation.Validate(s);
            Assert.IsTrue(results2.IsValid);

        }

        [Test]
        public void scholarship_max_value_must_be_greater_than_one()
        {
            var s = CreateValidScholarship();
            s.MaximumAmount = 0;
            var results = Validation.Validate(s);

            Assert.IsFalse(results.IsValid);
            Assert.AreEqual(1, results.Count);

            s.MaximumAmount = 1;
            var results2 = Validation.Validate(s);
            Assert.IsTrue(results2.IsValid);
        }

        [Test]
        public void verify_clone_implimentation_exists()
        {
            var s = CreateValidScholarship();
            var cloneable = (ICloneable) s;
            var cloned = (Scholarship) cloneable.Clone();

            Assert.AreEqual(0, cloned.Id);
            Assert.AreEqual(0, cloned.Schedule.Id);
            
        }

        [Test]
        public void cant_submit_for_activation_if_no_stage_set()
        {
            var s = new Scholarship();
            Assert.IsFalse(s.CanSubmitForActivation());
        }

        [Test]
        public void can_submit_for_activation_if_not_yet_submitted()
        {
            var s = new Scholarship { Stage = ScholarshipStages.GeneralInformation };
            Assert.IsTrue(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.SeekerProfile;
            Assert.IsTrue(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.FundingProfile;
            Assert.IsTrue(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.PreviewCandidatePopulation;
            Assert.IsTrue(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.BuildScholarshipMatchLogic;
            Assert.IsTrue(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.BuildScholarshipRenewalCriteria;
            Assert.IsTrue(s.CanSubmitForActivation());

            s.Stage = ScholarshipStages.RequestedActivation;
            Assert.IsFalse(s.CanSubmitForActivation());
        }

        [Test]
        public void can_edit_if_no_stage_set()
        {
            var s = new Scholarship();
            Assert.IsTrue(s.CanEdit());
        }

        [Test]
        public void cant_edit_if_submitted_or_later()
        {
            var s = new Scholarship {Stage = ScholarshipStages.RequestedActivation};
            Assert.IsFalse(s.CanEdit());

            s.Stage = ScholarshipStages.Activated;
            Assert.IsFalse(s.CanEdit());

            s.Stage = ScholarshipStages.Awarded;
            Assert.IsFalse(s.CanEdit());

            s.Stage = ScholarshipStages.Rejected;
            Assert.IsTrue(s.CanEdit());
        }

        private static Scholarship CreateValidScholarship()
        {
            var s = new Scholarship
                        {
                            Name = "Foo",
                            MissionStatement = "Accomplish things",
                            ProgramGuidelines = "Give to people in need",
                            Schedule = new ScholarshipScheduleByMonthDay
                                           {
                                               StartFrom = new ByMonthDay(),
                                               AwardOn = new ByMonthDay(),
                                               DueOn = new ByMonthDay()
                                           },
                            Provider = new Provider { Name = "Foo Provider", ApprovalStatus = ApprovalStatus.Approved },
                            MinimumAmount = 0,
                            MaximumAmount = 100
                        };

            return s;
        }

        [Test]
        public void stage_completion_tests()
        {
            Scholarship scholarship = new Scholarship();
            Assert.IsNotNull(scholarship.CompletedStages);

            scholarship.MarkStageCompletion(ScholarshipStages.GeneralInformation, true);
            Assert.AreEqual(1, scholarship.CompletedStages.Count);
            Assert.AreEqual(ScholarshipStages.GeneralInformation, scholarship.CompletedStages[0]);

            Assert.IsTrue(scholarship.IsStageCompleted(ScholarshipStages.GeneralInformation));
            Assert.IsFalse(scholarship.IsStageCompleted(ScholarshipStages.SeekerProfile));

            scholarship.MarkStageCompletion(ScholarshipStages.GeneralInformation, false);
            Assert.AreEqual(0, scholarship.CompletedStages.Count);
            Assert.IsFalse(scholarship.IsStageCompleted(ScholarshipStages.GeneralInformation));
        }

        [Test]
        public void display_name_with_no_donor_gives_name()
        {
            var s = new Scholarship {Name = "Foo"};
            Assert.AreEqual("Foo", s.DisplayName);
        }

        [Test]
        public void display_name_with_donor_but_no_name_gives_name()
        {
            var s = new Scholarship { Name = "Foo", Donor = new ScholarshipDonor()};
            Assert.AreEqual("Foo", s.DisplayName);
        }

        [Test]
        public void display_name_with_donor_gives_name_with_donor_name()
        {
            var s = new Scholarship { Name = "Foo", Donor = new ScholarshipDonor { Name = new PersonName { FirstName = "First", LastName = "Last"}}};
            Assert.AreEqual("Foo (First Last)", s.DisplayName);
        }
    }
}