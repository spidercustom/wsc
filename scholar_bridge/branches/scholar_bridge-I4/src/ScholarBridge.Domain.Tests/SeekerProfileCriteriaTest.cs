﻿using NUnit.Framework;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class SeekerProfileCriteriaTest
    {
        private SeekerProfileAttributeUsage CreateMinimumUsageTypeAttribute(SeekerProfileAttribute attribute)
        {
            return new SeekerProfileAttributeUsage
            {
                Attribute = attribute,
                UsageType = ScholarshipAttributeUsageType.Minimum
            };
        }

        [Test]
        public void test_has_attribute()
        {
            var criteria = new SeekerProfileCriteria();
            Assert.IsFalse(criteria.HasAttributeUsage(SeekerProfileAttribute.AcademicArea));
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(SeekerProfileAttribute.AcademicArea));

            Assert.IsTrue(criteria.HasAttributeUsage(SeekerProfileAttribute.AcademicArea));
        }

        public void test_clone_implimentation_exists()
        {
            var criteria = new SeekerProfileCriteria();
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(SeekerProfileAttribute.AcademicArea));

            var clonedCriteria = (SeekerProfileCriteria)criteria.Clone();
            Assert.AreEqual(criteria.AttributesUsage.Count, clonedCriteria.AttributesUsage.Count);
            Assert.AreEqual(criteria.AttributesUsage[0].Attribute, clonedCriteria.AttributesUsage[0].Attribute);
            Assert.AreEqual(criteria.AttributesUsage[0].UsageType, clonedCriteria.AttributesUsage[0].UsageType);
        }

        [Test]
        public void remove_attribute()
        {
            var criteria = new SeekerProfileCriteria();
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(SeekerProfileAttribute.AcademicArea));
            Assert.AreEqual(1, criteria.AttributesUsage.Count);
            criteria.RemoveAttributeUsage(SeekerProfileAttribute.SeekerVerbalizingWord);
            Assert.AreEqual(1, criteria.AttributesUsage.Count);
            criteria.RemoveAttributeUsage(SeekerProfileAttribute.AcademicArea);
            Assert.AreEqual(0, criteria.AttributesUsage.Count);
        }

        [Test]
        public void find_attribute_usage()
        {
            var criteria = new SeekerProfileCriteria();
            Assert.IsNull(criteria.FindAttributeUsage(SeekerProfileAttribute.SeekerVerbalizingWord));
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(SeekerProfileAttribute.AcademicArea));
            
            Assert.IsNull(criteria.FindAttributeUsage(SeekerProfileAttribute.SeekerVerbalizingWord));
            Assert.IsNotNull(criteria.FindAttributeUsage(SeekerProfileAttribute.AcademicArea));
        }

        [Test]
        public void get_attribute_usage_type()
        {
            var criteria = new SeekerProfileCriteria();
            Assert.AreEqual(ScholarshipAttributeUsageType.NotUsed, criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerVerbalizingWord));
            
            criteria.AttributesUsage.Add(CreateMinimumUsageTypeAttribute(SeekerProfileAttribute.AcademicArea));
            Assert.AreEqual(ScholarshipAttributeUsageType.NotUsed, criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerVerbalizingWord));
            Assert.AreEqual(ScholarshipAttributeUsageType.Minimum, criteria.GetAttributeUsageType(SeekerProfileAttribute.AcademicArea));
        }

        [Test]
        public void validate_location_state()
        {
            var criteria = new SeekerProfileCriteria();
            var stateWA = new State {Abbreviation = "WA", Name = "Washington"};
            
            Assert.IsNull(criteria.State);
            criteria.States.Add(stateWA);
            Assert.IsNotNull(criteria.State);
        }

        [Test]
        public void validate_state_dependent_location()
        {
            var criteria = new SeekerProfileCriteria();
            
            Assert.AreEqual(StateDependentLocation.None, criteria.StateDependentLocation);
            Assert.IsNull(criteria.StateDependentLocations);

            criteria.Counties.Add(new County());
            Assert.AreEqual(StateDependentLocation.County, criteria.StateDependentLocation);
            Assert.IsNotNull(criteria.StateDependentLocations);
            Assert.That(criteria.StateDependentLocations[0] is County);
            
            criteria.Cities.Add(new City());
            Assert.AreEqual(StateDependentLocation.County, criteria.StateDependentLocation);

            criteria.Counties.Clear();
            Assert.AreEqual(StateDependentLocation.City, criteria.StateDependentLocation);
            Assert.IsNotNull(criteria.StateDependentLocations);
            Assert.That(criteria.StateDependentLocations[0] is City);

            criteria.SchoolDistricts.Add(new SchoolDistrict());
            Assert.AreEqual(StateDependentLocation.City, criteria.StateDependentLocation);

            criteria.Cities.Clear();
            Assert.AreEqual(StateDependentLocation.SchoolDistrict, criteria.StateDependentLocation);
            Assert.IsNotNull(criteria.StateDependentLocations);
            Assert.That(criteria.StateDependentLocations[0] is SchoolDistrict);

            criteria.HighSchools.Add(new HighSchool());
            Assert.AreEqual(StateDependentLocation.SchoolDistrict, criteria.StateDependentLocation);

            criteria.SchoolDistricts.Clear();
            Assert.AreEqual(StateDependentLocation.HighSchool, criteria.StateDependentLocation);
            Assert.IsNotNull(criteria.StateDependentLocations);
            Assert.That(criteria.StateDependentLocations[0] is HighSchool);
        
            criteria.HighSchools.Clear();
            Assert.AreEqual(StateDependentLocation.None, criteria.StateDependentLocation);
            Assert.IsNull(criteria.StateDependentLocations);
        }
    }
}
