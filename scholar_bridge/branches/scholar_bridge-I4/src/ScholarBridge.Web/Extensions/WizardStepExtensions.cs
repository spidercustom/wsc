﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Wizards;
using System.Web.UI;
using System.Linq;
using System.Collections.Generic;

namespace ScholarBridge.Web.Extensions
{
    public static class WizardStepExtensions
    {
        public static IWizardStepControl<T> FindFirstWizardStepControl<T>(this ControlCollection controls)
        {
            return FindFirstWizardStepControl<T>(controls, false, -1);
        }

        public static IWizardStepControl<T> FindFirstWizardStepControl<T>(this ControlCollection controls, int placeHolderStepIndex)
        {
            return FindFirstWizardStepControl<T>(controls, true, placeHolderStepIndex);
        }

        private static IWizardStepControl<T> FindFirstWizardStepControl<T>(ControlCollection controls, bool returnPlaceHolderIfNotFound, int placeHolderStepIndex)
        {
            var wizardStepControls =
                from Control control in controls
                where
                    control is IWizardStepControl<T>
                select control;

            if (wizardStepControls.Count() == 0)
            {
                if (returnPlaceHolderIfNotFound)
                    return new PlaceHolderStep<T>(placeHolderStepIndex);
                else
                    return null;
            }
            return wizardStepControls.First() as IWizardStepControl<T>;
        }

        public static IWizardStepControl<T>[] FindWizardStepControls<T>(this ViewCollection steps)
        {
            var result = new List<IWizardStepControl<T>>();
            for (int index = 0; index < steps.Count; index++)
            {
                View step = steps[index];
                var wizardStepControl = FindFirstWizardStepControl<T>(step.Controls, true, index);
                result.Add(wizardStepControl);
            }
            return result.ToArray();
        }
    }
}
