﻿using System;
using System.Web;

namespace ScholarBridge.Web.Wizards
{
    //TODO: Create unit test for this
    public static class WizardStepContainerCommon
    {
        public const string RESUME_FROM = "resumefrom";
        public const string COPY_FROM = "copyfrom";

        public static bool CopyFromParameterExists()
        {
            return Array.Exists(HttpContext.Current.Request.Params.AllKeys, key => COPY_FROM.Equals(key));
        }

        public static bool ResumeFromParameterExists()
        {
            return Array.Exists(HttpContext.Current.Request.Params.AllKeys, key => RESUME_FROM.Equals(key));
        }

        public static int GetCopyFromId()
        {
            string copyFromIdString = HttpContext.Current.Request.Params[COPY_FROM];
            int copyFromId;
            if (!Int32.TryParse(copyFromIdString, out copyFromId))
                throw new ArgumentException("Cannot understand value of parameter scholarship");

            return copyFromId;
        }

        public static void ResumeWizard<T>(IWizardStepsContainer<T> wizardContainer)
        {
            for (var stepIndex = 0; stepIndex < wizardContainer.Steps.Length; stepIndex++)
            {
                var stepControl = wizardContainer.Steps[stepIndex];
                if (stepControl.WasSuspendedFrom(wizardContainer.GetDomainObject()))
                {
                    wizardContainer.Goto(stepIndex);
                    break;
                }
            }
        }

        public static void ResumeFromStep<T>(IWizardStepsContainer<T> wizardContainer)
        {
            var resumeFromPageIndexString = HttpContext.Current.Request.Params[RESUME_FROM];
            var resumeFromPageIndex = Int32.Parse(resumeFromPageIndexString);

            var scholarship = wizardContainer.GetDomainObject();
            var resumeFromIndex = Math.Min(resumeFromPageIndex, wizardContainer.Steps.Length);
            for (; resumeFromIndex >= 1; resumeFromIndex--)
            {
                var stepControl = wizardContainer.Steps[resumeFromIndex - 1];
                if (stepControl.CanResume(scholarship))
                {
                    wizardContainer.Goto(resumeFromIndex);
                    return;
                }
            }
            wizardContainer.Goto(0);
        }

        public static void GoPrior<T>(IWizardStepsContainer<T> wizardContainer)
        {
            int validPriorStepIndex = Math.Max(0, wizardContainer.ActiveStepIndex - 1);
            wizardContainer.Goto(validPriorStepIndex);
        }

        public static void GoNext<T>(IWizardStepsContainer<T> wizardContainer)
        {
            int validNextStepIndex = Math.Min(
                wizardContainer.Steps.Length - 1,
                wizardContainer.ActiveStepIndex + 1);
            
            wizardContainer.Goto(validNextStepIndex); 
        }

        public static void Goto<T>(IWizardStepsContainer<T> wizardContainer, int index)
        {
            //correct the index if its going out of bound
            int validGotoStepIndex = Math.Min(wizardContainer.Steps.Length - 1, index);
            validGotoStepIndex = Math.Max(0, validGotoStepIndex);
            wizardContainer.ActiveStepIndex = validGotoStepIndex;
        }

        public static void NotifyStepActivated<T>(IWizardStepsContainer<T> wizardContainer)
        {
            IWizardStepControl<T> activeStep = wizardContainer.Steps[wizardContainer.ActiveStepIndex];
            activeStep.Activated();
        }

        public static bool ValidateStep<T>(IWizardStepsContainer<T> wizardContainer)
        {
            IWizardStepControl<T> activeStep = wizardContainer.Steps[wizardContainer.ActiveStepIndex];
            return activeStep.ValidateStep();
        }

        public static void SaveActiveStep<T>(IWizardStepsContainer<T> wizardContainer)
        {
            IWizardStepControl<T> activeStep = wizardContainer.Steps[wizardContainer.ActiveStepIndex];
            activeStep.Save();
        }

    }
}
