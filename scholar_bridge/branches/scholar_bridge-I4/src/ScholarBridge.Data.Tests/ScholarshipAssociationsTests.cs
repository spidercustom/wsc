using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{

    [TestFixture]
    public class ScholarshipAssociationsTests : TestBase
    {
        public UserDAL UserDAL { get; set; }
        public ProviderDAL ProviderDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        public AdditionalRequirementDAL AdditionalRequirementDAL { get; set; }
        public SupportDAL SupportDAL { get; set; }
        public NeedGapDAL NeedGapDAL { get; set; }
        public TermOfSupportDAL TermOfSupportDAL { get; set; }

        [Test]
        public void can_associate_additional_reqs_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.ResetAdditionalRequirements(AdditionalRequirementDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void can_associate_need_gaps_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.FundingProfile.Need.ResetNeedGaps(NeedGapDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        [Test]
        public void can_associate_support_with_scholarship()
        {
            Scholarship s = BuildScholarship();

            s.FundingProfile.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll());
            ScholarshipDAL.Update(s);
        }

        private const int TERMS_OF_SUPPORT_ACADEMIC_YEAR_ID = 3;
        [Test]
        public void can_set_and_retrieve_term_of_support()
        {
            Scholarship s = BuildScholarship();
            var academicYearSupport = TermOfSupportDAL.FindById(TERMS_OF_SUPPORT_ACADEMIC_YEAR_ID);
            Assert.IsNotNull(academicYearSupport, "Could not retrieve term of support 'academic year' for testing");
            
            s.FundingProfile.FundingParameters.TermOfSupport = academicYearSupport;
            s.FundingProfile.FundingParameters.TermOfSupportUnitCount = 1;

            ScholarshipDAL.Update(s);

            var retrievedScholarship = ScholarshipDAL.FindById(s.Id);
            Assert.IsNotNull(retrievedScholarship.FundingProfile.FundingParameters.TermOfSupport);
            Assert.AreEqual(TERMS_OF_SUPPORT_ACADEMIC_YEAR_ID, retrievedScholarship.FundingProfile.FundingParameters.TermOfSupport.Id);
            Assert.AreEqual(1, retrievedScholarship.FundingProfile.FundingParameters.TermOfSupportUnitCount);
        }

        private Scholarship BuildScholarship()
        {
        	var uList = UserDAL.FindAll(0, 1);
        	var u = uList[0];

            var p = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "test", u);
            var s = InsertScholarship(ScholarshipDAL, p, u);

            Assert.IsNotNull(s);
            return s;
        }

        public static Scholarship InsertScholarship(ScholarshipDAL scholarshipDal, Provider provider, User user)
        {
            var s = new Scholarship
                                     {
                                         Name = "Test Scholarship 1",
                                         AcademicYear = new AcademicYear(2009),
                                         MissionStatement = @"Long big text to describe mission",
                                         Schedule = ScholarshipDALTest.CreateScheduleByMonthDay(),
                                         Provider = provider,
                                         LastUpdate = new ActivityStamp(user),
                                         Stage = ScholarshipStages.GeneralInformation
                                     };

            return scholarshipDal.Save(s);
        }
    }
}