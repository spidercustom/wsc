﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScholarBridge.Domain;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface IRelationshipDAL : IDAL<Relationship>
    {
        Relationship FindById(int id);
        IList<Relationship> FindByProvider(Provider provider);
        IList<Relationship> FindByIntermediary(Intermediary intermediary);
        Relationship Save(Relationship relationship);
        Relationship FindByProviderandIntermediary(Provider provider, Intermediary intermediary);
    }
}
