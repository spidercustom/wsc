﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="ScholarBridge.Web.Error" Title="Application Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<h2>Oops!</h2>
<p>Something unexpected has occurred. Please go back and try again.</p>

<p id="errorMessage" runat="server"></p>
</asp:Content>
