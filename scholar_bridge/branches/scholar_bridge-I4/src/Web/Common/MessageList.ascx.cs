﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class MessageList : UserControl
    {
        public IUserContext UserContext { get; set; }
        public IMessageService MessageService { get; set; }

        public bool Archived { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserContext.EnsureUserIsInContext();

            IList<Domain.Messaging.Message> messages = null;
            if (Archived)
            {
                messages = MessageService.FindAllArchived(UserContext.CurrentUser, UserContext.CurrentOrganization);
            }
            else
            {
                messages = MessageService.FindAll(UserContext.CurrentUser, UserContext.CurrentOrganization);
            }
            messageList.DataSource = messages;
            messageList.DataBind();
        }

        protected void messageList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var link = (HyperLink)e.Item.FindControl("linkToMessage");
                link.NavigateUrl = LinkTo + "?id=" + ((Domain.Messaging.Message)e.Item.DataItem).Id;
            }
        }
    }
}