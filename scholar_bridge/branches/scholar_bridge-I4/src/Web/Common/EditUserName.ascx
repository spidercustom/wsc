﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditUserName.ascx.cs" Inherits="ScholarBridge.Web.Common.EditUserName" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<asp:Label ID="errorMessage" runat="server" Text="No user found." Visible="false" CssClass="errorMessage"/>
             
<label for="FirstName">First Name:</label>
<asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" />
<br />
<label for="MiddleName">Middle Name:</label>
<asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName" />
<br />
<label for="LastName">Last Name:</label>
<asp:TextBox ID="LastName" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" />
<br />
<label for="PhoneNumber">Phone Number:</label>
<asp:TextBox ID="PhoneNumber" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="PhoneValidator" runat="server" ControlToValidate="PhoneNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
<br />
<label for="FaxNumber">Fax Number:</label>
<asp:TextBox ID="FaxNumber" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="PropertyProxyValidator1" runat="server" ControlToValidate="FaxNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
<br />
<label for="OtherPhoneNumber">Other Phone:</label>
<asp:TextBox ID="OtherPhoneNumber" runat="server"></asp:TextBox>
<elv:PropertyProxyValidator ID="PropertyProxyValidator2" runat="server" ControlToValidate="OtherPhoneNumber" PropertyName="Number" SourceTypeName="ScholarBridge.Domain.Contact.PhoneNumber" />
<br />
<asp:Button ID="saveButton" runat="server" Text="Save" onclick="saveButton_Click" />
<asp:Button ID="cancelButton" runat="server" Text="Cancel" CausesValidation="false" onclick="cancelButton_Click" />