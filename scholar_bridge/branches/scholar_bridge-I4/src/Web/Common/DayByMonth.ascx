﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DayByMonth.ascx.cs" Inherits="ScholarBridge.Web.Common.DayByMonth" %>
<asp:DropDownList ID="DayOfMonthControl" runat="server" >
  <asp:ListItem Value="0">1st</asp:ListItem>
  <asp:ListItem Value="1">2nd</asp:ListItem>
  <asp:ListItem Value="2">3rd</asp:ListItem>
  <asp:ListItem Value="3">4th</asp:ListItem>
  <asp:ListItem Value="4">5th</asp:ListItem>
  <asp:ListItem Value="5">6th</asp:ListItem>
  <asp:ListItem Value="6">7th</asp:ListItem>
  <asp:ListItem Value="7">8th</asp:ListItem>
  <asp:ListItem Value="8">9th</asp:ListItem>
  <asp:ListItem Value="9">10th</asp:ListItem>
  <asp:ListItem Value="10">11th</asp:ListItem>
  <asp:ListItem Value="11">12th</asp:ListItem>
  <asp:ListItem Value="12">13th</asp:ListItem>
  <asp:ListItem Value="13">14th</asp:ListItem>
  <asp:ListItem Value="14">15th</asp:ListItem>
  <asp:ListItem Value="15">16th</asp:ListItem>
  <asp:ListItem Value="16">17th</asp:ListItem>
  <asp:ListItem Value="17">18th</asp:ListItem>
  <asp:ListItem Value="18">19th</asp:ListItem>
  <asp:ListItem Value="19">20th</asp:ListItem>
  <asp:ListItem Value="20">21st</asp:ListItem>
  <asp:ListItem Value="21">22nd</asp:ListItem>
  <asp:ListItem Value="22">23rd</asp:ListItem>
  <asp:ListItem Value="23">24th</asp:ListItem>
  <asp:ListItem Value="24">25th</asp:ListItem>
  <asp:ListItem Value="25">26th</asp:ListItem>
  <asp:ListItem Value="26">27th</asp:ListItem>
  <asp:ListItem Value="27">28th</asp:ListItem>
  <asp:ListItem Value="28">29th</asp:ListItem>
  <asp:ListItem Value="29">30th</asp:ListItem>
  <asp:ListItem Value="30">31st</asp:ListItem>
</asp:DropDownList>
<span>&nbsp;of&nbsp;</span>
<asp:DropDownList ID="MonthControl" runat="server" >
  <asp:ListItem Value="0">January</asp:ListItem>
  <asp:ListItem Value="1">February</asp:ListItem>
  <asp:ListItem Value="2">March</asp:ListItem>
  <asp:ListItem Value="3">April</asp:ListItem>
  <asp:ListItem Value="4">May</asp:ListItem>
  <asp:ListItem Value="5">June</asp:ListItem>
  <asp:ListItem Value="6">July</asp:ListItem>
  <asp:ListItem Value="7">August</asp:ListItem>
  <asp:ListItem Value="8">September</asp:ListItem>
  <asp:ListItem Value="9">October</asp:ListItem>
  <asp:ListItem Value="10">November</asp:ListItem>
  <asp:ListItem Value="11">December</asp:ListItem>
</asp:DropDownList>