﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web.Common
{
    [DefaultProperty("TabControlClientID")]
    [ToolboxData("<{0}:jQueryTabEvents runat=server></{0}:jQueryTabEvents>")]
    public class jQueryTabEvents : WebControl, IPostBackEventHandler
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string TabControlClientID
        {
            get
            {
                String s = (String)ViewState["TabControlClientID"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["TabControlClientID"] = value;
            }
        }

        private MultiView GetAssociatedMultiView()
        {
            if (string.IsNullOrEmpty(MultiViewControlID))
                return null;
            return NamingContainer.FindControl(MultiViewControlID) as MultiView;
        }

        public string MultiViewControlID { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            string postBackScript = Page.ClientScript.GetPostBackEventReference(this, TAB_SELECT);

            StringBuilder builder = new StringBuilder();
            builder.AppendFormat(TabSelectEventTemplate, TabControlClientID, ActiveTabIndex, postBackScript);
            builder.Replace(",'select'", ",'select:' + ui.index.toString()");

            Page.ClientScript.RegisterClientScriptBlock(GetType(), ClientID + "key", builder.ToString(), true);
            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {

            base.Render(writer);
        }

        private const string TabSelectEventTemplate =
@"
$(document).ready(function() {{
    $('#{0}').tabs('select', {1});
    $('#{0}').bind('tabsselect', function(event, ui) {{
        {2}
    }});
}});
";

        private int activeTabIndex;
        public int ActiveTabIndex
        {
            get
            {
                MultiView associatedMultiView = GetAssociatedMultiView();
                if (null == associatedMultiView)
                    return activeTabIndex;
                return associatedMultiView.ActiveViewIndex;
            }
            set
            {
                MultiView associatedMultiView = GetAssociatedMultiView();
                if (null != associatedMultiView)
                    associatedMultiView.ActiveViewIndex = value;
                
                activeTabIndex = value;
            }
        }

        #region IPostBackEventHandler Members
        private const string TAB_SELECT = "select";
        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument.StartsWith(TAB_SELECT))
            {
                string[] parameters = eventArgument.Split(':');
                ActiveTabIndex = Int32.Parse(parameters[1]);
                if (null != TabChanged)
                {
                    TabChanged(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler TabChanged;

        #endregion
    }
}
