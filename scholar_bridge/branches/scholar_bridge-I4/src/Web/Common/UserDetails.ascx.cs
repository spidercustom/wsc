﻿using System;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class UserDetails : UserControl
    {
        public User CurrentUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (null == CurrentUser)
                {
                    errorMessage.Visible = true;
                    return;
                }

                nameLbl.Text = CurrentUser.Name.NameFirstLast;
                emailAddressLbl.Text = CurrentUser.Email;

				phoneLbl.Text = GetPhone(CurrentUser.Phone);
            	faxLbl.Text = GetPhone(CurrentUser.Fax);
            	otherPhoneLbl.Text = GetPhone(CurrentUser.OtherPhone);
				
                SetNullableDate(memberSinceLbl, CurrentUser.CreationDate);
                SetNullableDate(lastLoginLbl, CurrentUser.LastLoginDate);
            }
        }

		private static string GetPhone(Domain.Contact.PhoneNumber phoneNumber)
		{
			return phoneNumber == null ? string.Empty : phoneNumber.FormattedPhoneNumber;
		}

        private static void SetNullableDate(ITextControl lbl, DateTime? date)
        {
            if (date.HasValue)
            {
                lbl.Text = date.Value.ToString("M/dd/yyyy HH:mm");
            }
        }
    }
}