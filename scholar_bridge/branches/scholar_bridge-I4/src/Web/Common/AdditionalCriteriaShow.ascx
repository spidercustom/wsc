﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteriaShow.ascx.cs" Inherits="ScholarBridge.Web.Common.AdditionalCriteriaShow" %>

<div id="recordinfo">
<h4>Additional Scholarship Application Criteria</h4>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>

<table class="viewonlyTable">
    <thead><tr><th>Additional Requirements</th></tr></thead>
    <tbody>
        <asp:Repeater ID="AdditionalRequirementsRepeater" runat="server">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Name")%></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        
    </tbody>
    <tfoot>
        <tr runat="server" id="RequirementsFooterRow">
            <td>No additional requirements listed for this scholarship.</td>
        </tr>
    </tfoot>
</table>

<table class="viewonlyTable">
    <thead><tr><th colspan="2">Scholarship Questions</th></tr></thead>
    <tbody>
    <asp:Repeater ID="AdditionalCriteriaRepeater" runat="server">
        <ItemTemplate>
            <tr>
                <td><%# Eval("DisplayOrder")%></td>
                <td><%# Eval("QuestionText")%></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    </tbody>
</table>

<table class="viewonlyTable">
    <thead>
        <tr><th colspan="4">Scholarship Attachments</th></tr>
        <tr>
            <th>File</th>
            <th>Size</th>
            <th>Type</th>
            <th>Comment</th>
        </tr>
    </thead>
    <tbody>
    <asp:Repeater ID="AttachedFiles" runat="server" >
        <ItemTemplate>
            <tr>
                <td><asp:LinkButton ID="downloadAttachmentBtn" 
                    runat="server" Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("Id") %>'
                    OnCommand="downloadAttachmentBtn_OnCommand"/></td>
                <td><%# Eval("DisplaySize") %></td>
                <td><%# Eval("MimeType") %></td>
                <td><%# Eval("Comment")%></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    </tbody>
</table>


</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
