﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public partial class Login : UserControl
    {
        public IUserContext UserContext { get; set; }

        private System.Web.UI.WebControls.Login LoginControl
        {
            get { return ((System.Web.UI.WebControls.Login) loginView.FindControl("Login1")); }
        }

        private string Username
        {
            get { return LoginControl.UserName; }
        }

        private void SetDestinationUrl(string url)
        {
            LoginControl.DestinationPageUrl = url;
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            if (null == Request["ReturnUrl"])
            {
                SetDestinationUrl(DestinationForUser(Username));

            }
        }

        public string DestinationForUser(string username)
        {
            var roles = new List<string>(Roles.GetRolesForUser(username));
            if (roles.Any(r => Role.PROVIDER_ROLE.Equals(r)))
            {
                return "~/Provider/";
            }
            else if (roles.Any(r => Role.INTERMEDIARY_ROLE.Equals(r)))
            {
                return "~/Intermediary/";
            }
            else if (roles.Any(r => Role.WSC_ADMIN_ROLE.Equals(r)))
            {
                return "~/Messages/";
            }
            else if (roles.Any(r => Role.SEEKER_ROLE.Equals(r)))
            {
                return "~/Seeker/";
            }

            return "~/";
        }
    }
}
