﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IntermediaryRelationshipList.ascx.cs" Inherits="ScholarBridge.Web.Common.IntermediaryRelationshipList" %>
<div id="yesnodialogdiv" title="Confirm Inactivate & Notify." style="display:none;">
    <p>Inactivating a relationship will apply to future scholarships. Continue?</p>
</div>

<asp:ListView ID="lstRelationships" runat="server" 
    OnItemDataBound="lstRelationships_ItemDataBound" 
    onpagepropertieschanging="lstRelationships_PagePropertiesChanging" 
    onitemcommand="lstRelationships_ItemCommand" >
    <LayoutTemplate>
    <table class="sortableTable">
            <thead>
            <tr>
                <th>Organization Name</th>
                <th>Org. Admin Name</th>
                <th>Org. Admin Email</th>
                <th>Phone Number</th>
                <th>Status</th>
                <th>Requested Date</th>
                <th>Activated Date</th>
                <th>InActivated Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
        <tr class="row">
            <td><asp:Label ID="lblName" runat="server" /></td>
            <td><asp:Label ID="lblAdminName" runat="server" /></td>
            <td><asp:Label ID="lblAdminEmail" runat="server" /></td>
            <td><asp:Label ID="lblPhone" runat="server" /></td>
            <td><asp:Label ID="lblStatus" runat="server" /></td>
            <td><%#DataBinder.Eval(Container.DataItem, "RequestedOn","{0:d}")%></td>
            <td><%#DataBinder.Eval(Container.DataItem, "ActivatedOn", "{0:d}")%></td>
            <td><%#DataBinder.Eval(Container.DataItem, "InActivatedOn", "{0:d}")%></td>
            <td><asp:HyperLink id="inactivateLink" Text="Inactivate & Notify"   runat="server"/></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:Label ID="lblName" runat="server" /></td>
            <td><asp:Label ID="lblAdminName" runat="server" /></td>
            <td><asp:Label ID="lblAdminEmail" runat="server" /></td>
            <td><asp:Label ID="lblPhone" runat="server" /></td>
            <td><asp:Label ID="lblStatus" runat="server" /></td>
            <td><%#DataBinder.Eval(Container.DataItem, "RequestedOn","{0:d}")%></td>
            <td><%#DataBinder.Eval(Container.DataItem, "ActivatedOn", "{0:d}")%></td>
            <td><%#DataBinder.Eval(Container.DataItem, "InActivatedOn", "{0:d}")%></td>
            <td><asp:HyperLink id="inactivateLink" Text="Inactivate & Notify"   runat="server"/></td>  
        </tr>
    </AlternatingItemTemplate>   
    <EmptyDataTemplate>
    <p>There are no Relationships, </p>
    </EmptyDataTemplate>
</asp:ListView>
<div class="pager">
    <asp:DataPager runat="server" ID="pager"  PagedControlID="lstRelationships" 
    PageSize="20" >
    <Fields>
      <asp:NumericPagerField ButtonCount="20" CurrentPageLabelCssClass="pagerlabel" 
            NextPreviousButtonCssClass="pagerlink" NumericButtonCssClass="pagerlink" />
        <asp:NextPreviousPagerField />
    </Fields>
    </asp:DataPager>
</div> 
