﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FundingInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.FundingInfoShow" %>

 <div id="recordinfo">
<h4>Funding Profile</h4>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>     

<table class="viewonlyTable">
    <tbody>
    <tr>
        <td>
            <table>
                <thead><tr><th>Need </th></tr></thead>
                <tbody>
                    <tr>
                        <th>Need Definitions</th>
                        <td ><asp:Label  ID="lblNeed" runat="server"  /></td>
                    </tr>
                    
                    <tr>
                        <th  scope="row">Seeker Need Amount</th>
                        <td><asp:Label ID="lblMinMaxNeed"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Need GAP Threshold </th>
                        <td><asp:Label ID="lblNeedGAP"  runat="server"  /></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>


</div>
<div id="linkarea2" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
