﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Intermediary.Users
{
    public partial class Show : Page
    {
        public IUserContext UserContext { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }

        public int UserId { get { return Int32.Parse(Request["id"]); } }
        public User CurrentUser { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureIntermediaryIsInContext();
            CurrentUser = IntermediaryService.FindUserInOrg(UserContext.CurrentIntermediary, UserId);
            userDetails.CurrentUser = CurrentUser;

            ToggleButtons();
        }

        private void ToggleButtons()
        {
            if (null == CurrentUser)
            {
                deleteBtn.Visible = false;
                reactivateBtn.Visible = false;
                return;
            }

            deleteBtn.Visible = !CurrentUser.IsDeleted;
            reactivateBtn.Visible = CurrentUser.IsDeleted;

            // Don't allow someone to delete themselves
            if (UserContext.CurrentUser.Id == UserId)
            {
                deleteBtn.Visible = false;
            }
        }

        protected void deleteBtn_Click(object sender, EventArgs e)
        {
            CurrentUser.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            IntermediaryService.DeleteUser(UserContext.CurrentIntermediary, CurrentUser);
            SuccessMessageLabel.SetMessage("User was deactivated.");
            Response.Redirect("~/Intermediary/Users/");
        }

        protected void reactivateBtn_Click(object sender, EventArgs e)
        {
            CurrentUser.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            IntermediaryService.ReactivateUser(UserContext.CurrentIntermediary, CurrentUser);
            SuccessMessageLabel.SetMessage("User was reactivated.");
            Response.Redirect("~/Intermediary/Users/");
        }
    }
}
