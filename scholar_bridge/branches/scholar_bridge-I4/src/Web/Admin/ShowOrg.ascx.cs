﻿using System;
using ScholarBridge.Domain;
using ScholarBridge.Common.Extensions;
namespace ScholarBridge.Web.Admin
{
    public partial class ShowOrg : System.Web.UI.UserControl
    {
        public Organization Organization { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (null != Organization)
                {
                    statusLabel.Text = Organization.ApprovalStatus.GetDisplayName();
                    adminConfirmLabel.Text = Organization.AdminUser.IsActive.ToString();
                    nameLabel.Text = Organization.Name;
                    taxIdLabel.Text = Organization.TaxId;

                    websiteLabel.Text = Organization.Website;
                    websiteLabel.NavigateUrl = Organization.Website;

                    addressLabel.Text = Organization.Address == null ? null : Organization.Address.ToString();
                    phoneLabel.Text = Organization.Phone == null ? null : Organization.Phone.ToString();
                    faxLabel.Text = Organization.Fax == null ? null : Organization.Fax.ToString();
                    otherPhoneLabel.Text = Organization.OtherPhone == null ? null : Organization.OtherPhone.ToString();
                    //                existingNotesTb.Text = Organization.AdminNotes;
                }
            }
        }
    }
}