﻿<%@ Page Title="Confirm Email" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="ConfirmEmail.aspx.cs" Inherits="ScholarBridge.Web.ConfirmEmail" %>

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label><br />
    
    <asp:Panel ID="setPassword" runat="server" Visible="false">
        <label for="Password">Password:</label>
        &nbsp;<asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="passwordIsRequired" runat="server" Display="Dynamic" ControlToValidate="Password" ErrorMessage="Password is required" ToolTip="Did you forget to enter a password?"></asp:RequiredFieldValidator>
        <elv:PropertyProxyValidator ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User"/>
        <br />
        <label for="ConfirmPassword">Confirm Password:</label>
        &nbsp;<asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" Display='Dynamic' ControlToValidate="ConfirmPassword"
            ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
            ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."></asp:CompareValidator>
        <br />
        
        <asp:Button ID="saveButton" runat="server" Text="Set Password" onclick="saveButton_Click" CausesValidation="true" />
    
    </asp:Panel>
    
</asp:Content>
