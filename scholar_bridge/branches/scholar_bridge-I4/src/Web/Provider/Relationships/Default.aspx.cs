﻿using System;
using System.Web.UI;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Provider.Relationships
{
    public partial class Default : Page
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
            var provider = UserContext.CurrentProvider;
            providerRelationshiplist.Relationships = RelationshipService.GetByProvider( provider);
        }
    }
}