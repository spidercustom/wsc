﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="ScholarBridge.Web.Provider.Users.Create" Title="Provider | Users | Create" %>

<%@ Register TagPrefix="sb" TagName="UserForm" Src="~/Common/UserForm.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <h3>Create User</h3>
    <sb:UserForm id="userForm" runat="server" OnUserSaved="userForm_OnUserSaved" OnFormCanceled="userForm_OnFormCanceled" />
</asp:Content>
