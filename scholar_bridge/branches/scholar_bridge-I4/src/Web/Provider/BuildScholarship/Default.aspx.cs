﻿using System;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Domain;
using ScholarBridge.Business;
using System.Web.UI.WebControls;
using ScholarBridge.Web.Extensions;
using System.Web.UI;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class Default : SBBasePage, IWizardStepsContainer<Scholarship>
    {
        public const string SCHOLARSHIP_ID = "sid";
        

        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            Steps.ForEach(step => step.Container = this);
            
            if (!IsPostBack)
            {
                CheckForDataChanges = true;

                bool copyFromParameterExists = WizardStepContainerCommon.CopyFromParameterExists();
                bool resumeFromParameterExists = WizardStepContainerCommon.ResumeFromParameterExists();
                var scholarshipToEdit = GetCurrentScholarship();

                BypassPromptIds.AddRange(
                    new[]	{
								"PreviousButton",                            
								"SaveAndNextButton",                            
								"SaveAndExitButton",
                                "StepCompletedCheckBox"
							});

                ActiveStepIndex = WizardStepName.GeneralInformation.GetNumericValue();
                if (copyFromParameterExists)
                {
                    BeginCopyFrom();
                }
                else if (scholarshipToEdit != null)
                {
                    // FIXME: Intermediary won't work
                    if (!scholarshipToEdit.IsBelongToOrganization(UserContext.CurrentOrganization))
                        throw new InvalidOperationException("Scholarship do not belong to provider in context");

                    if (!scholarshipToEdit.CanEdit())
                    {
                        Response.Redirect("~/Provider/Scholarships/Show.aspx?id=" + scholarshipToEdit.Id);
                    }

                    if (resumeFromParameterExists)
                    {
                        ResumeFromStep();
                    }
                    else
                    {
                        ResumeWizard();
                    }
                    PopulateStageCompletionMarkers();
                }
            }
        }

        protected void BuildScholarshipWizard_PreviousButtonClick(object sender, EventArgs e)
        {
            GoPrior();
        }


        protected void SaveAndExitButton_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                Response.Redirect("../");
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("../");
        }

        protected void BuildScholarshipWizard_NextButtonClick(object sender, EventArgs e)
        {
            GoNext();
            PopulateStageCompletionMarkers();
        }

        protected void BuildScholarshipWizard_SaveAndNextButtonClick(object sender, EventArgs e)
        {
            if (Save())
            {
                GoNext();
                PopulateStageCompletionMarkers();
            }
        }

        protected void BuildScholarshipWizard_ActiveStepChanged(object sender, EventArgs e)
        {
            WizardStepContainerCommon.NotifyStepActivated(this);
            PopulateStageCompletionMarkers();
        }

        protected void StepCompletedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            IWizardStepControl<Scholarship> activeStep = GetActiveWizardStepControl();
            activeStep.IsCompleted = StepCompletedCheckBox.Checked;
            Dirty = false;
            PopulateStageCompletionMarkers();
        }

        #endregion

        protected bool CanEdit()
        {
            var scholarship = GetCurrentScholarship();
            return null == scholarship || scholarship.CanEdit();
        }


        public Scholarship GetCurrentScholarship()
        {
            return CurrentScholarshipId > 0 ? ScholarshipService.GetById(CurrentScholarshipId) : null;
        }

        private int CurrentScholarshipId
        {
            get
            {
                int scholarshipId = this.GetIntegerPageParameterValue(ViewState, SCHOLARSHIP_ID, -1);
                return scholarshipId;
            }
            set
            {
                ViewState[SCHOLARSHIP_ID] = value;
            }
        }

        private bool ValidateStep()
        {
            return WizardStepContainerCommon.ValidateStep(this);
        }

        private bool Save()
        {
            if (Page.IsValid && ValidateStep() && CanEdit())
            {
                WizardStepContainerCommon.SaveActiveStep(this);
                CurrentScholarshipId = GetDomainObject().Id;
                Dirty = false;

                return true;
            }

            return false;
        }

        private void BeginCopyFrom()
        {
            int copyFromId = WizardStepContainerCommon.GetCopyFromId();
            Scholarship copyFrom = ScholarshipService.GetById(copyFromId);
            scholarshipInContext = (Scholarship)((ICloneable)copyFrom).Clone();
        }

        #region IWizardStepsContainer<Scholarship> Members
        public void ResumeWizard()
        {
            WizardStepContainerCommon.ResumeWizard(this);
        }

        private void ResumeFromStep()
        {
            WizardStepContainerCommon.ResumeFromStep(this);
        }

        public IWizardStepControl<Scholarship>[] Steps
        {
            get
            {
                IWizardStepControl<Scholarship>[] stepControls = BuildScholarshipWizard.Views.FindWizardStepControls<Scholarship>();
                return stepControls;
            }
        }

        Scholarship scholarshipInContext;
        public Scholarship GetDomainObject()
        {
            if (scholarshipInContext == null)
            {
                scholarshipInContext = GetCurrentScholarship() ;
                if (scholarshipInContext == null)
                {
                    scholarshipInContext = new Scholarship();
                }
            }
            return scholarshipInContext;
        }

        public void GoPrior()
        {
            WizardStepContainerCommon.GoPrior(this);
        }

        public void GoNext()
        {
            WizardStepContainerCommon.GoNext(this);
        }

        public void Goto(int index)
        {
            WizardStepContainerCommon.Goto(this, index);
        }

        public int ActiveStepIndex
        {
            get { return BuildScholarshipWizard.ActiveViewIndex; }
            set { BuildScholarshipWizard.ActiveViewIndex = value; }
        }

        #endregion

        private void PopulateStageCompletionMarkers()
        {
            Scholarship scholarship = GetDomainObject();
            GeneralInformationCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.GeneralInformation);
            MatchCriteraCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.MatchCriteriaSelection);
            SeekerProfileCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.SeekerProfile);
            FundingProfileCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.FundingProfile);
            AdditionalCriteriaCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.AdditionalCriteria);
            ActivateCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.Activated);
            AwardCompletedControl.Visible = scholarship.IsStageCompleted(ScholarshipStages.Awarded);
            IWizardStepControl<Scholarship> step = GetActiveWizardStepControl();
            if (null != step)
                StepCompletedCheckBox.Checked = step.IsCompleted;
        }

        protected IWizardStepControl<Scholarship> GetActiveWizardStepControl()
        {
            View view = BuildScholarshipWizard.GetActiveView();
            if (null != view)
            {
                var wizardStepControl = view.Controls.FindFirstWizardStepControl<Scholarship>();
                return wizardStepControl;
            }
            return new PlaceHolderStep<Scholarship>(BuildScholarshipWizard.ActiveViewIndex);
        }

    }
}
