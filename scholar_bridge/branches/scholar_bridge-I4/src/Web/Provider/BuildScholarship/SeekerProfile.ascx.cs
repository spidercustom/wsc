﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;
using ScholarBridge.Domain.Extensions;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class SeekerProfile : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        Scholarship _scholarshipInContext;
        Scholarship scholarshipInContext
        {
            get
            {
                if (_scholarshipInContext == null)
                    _scholarshipInContext = Container.GetDomainObject();
                if (_scholarshipInContext == null)
                    throw new InvalidOperationException("There is no scholarship in context");
                return _scholarshipInContext;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context"); 

            if (!IsPostBack)
            {
                EthnicityControl.DataBind();
                ReligionControl.DataBind();
                WorkHoursControl.DataBind();
                ServiceHoursControl.DataBind();

                PopulateScreen();
            }
        }

        #region Visibilty of the controls
        public override void Activated()
        {
            base.Activated();
            SetControlsVisibility();
            SetupAttributeUsageTypeIcons();
        }

        private void SetupAttributeUsageTypeIcons()
        {
            SeekerProfileCriteria criteria = scholarshipInContext.SeekerProfileCriteria;

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FiveWordsUsageTypeIconControl,
                           criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerVerbalizingWord));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FiveSkillsUsageTypeIconControl,
                           criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerSkill));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(StudentGroupUsageTypeIconControl,
                           criteria.GetAttributeUsageType(SeekerProfileAttribute.StudentGroup));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SchoolTypeUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SchoolType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicProgramUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.AcademicProgram));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerStatusUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerStatus));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ProgramLengthUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ProgramLength));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CollegesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.College));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FirstGenerUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.FirstGeneration));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(EthnicityUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Ethnicity));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ReligionUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Religion));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GendersUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Gender));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AcademicAreasUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.AcademicArea));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CareersUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Career));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(CommunityServicesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.CommunityService));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(OrganizationsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.OrganizationAndAffiliationType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(AffiliationTypesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.OrganizationAndAffiliationType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerHobbiesUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerHobby));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SportsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Sport));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClubsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Club));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkTypeUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.WorkType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(WorkHoursUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.WorkHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceTypeUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ServiceType));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ServiceHoursUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ServiceHour));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(GPAUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.GPA));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ClassRankUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ClassRank));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SATScoreUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SAT));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(ACTScoreUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.ACT));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(HonorsUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.Honor));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(APCUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.APCreditsEarned));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(IBCUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.IBCreditsEarned));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SeekerDemographicsGeographicUsageTypeIconControl,
                            criteria.GetAttributeUsageType(SeekerProfileAttribute.SeekerGeographicsLocation));
            
        }

        private void SetControlsVisibility()
        {
            SeekerPersonalityContainerControl.Visible = true;
            SeekerInterestsControlContainer.Visible = true;
            SeekerTypeContainerControl.Visible = true;
            SeekerDemographicsPersonalContainerControl.Visible = true;
            SeekerDemographicsGeographicContainerControl.Visible = true;
            SeekerInterestsControlContainer.Visible = true;
            SeekerActivitiesControlContainer.Visible = true;
            SeekerPerformanceControlContainer.Visible = true;

            SeekerProfileCriteria criteria = scholarshipInContext.SeekerProfileCriteria;
            
            FiveWordsContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.SeekerVerbalizingWord);
            FiveSkillsContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.SeekerSkill);
            StudentGroupContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.StudentGroup);
            SchoolTypeContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.SchoolType);
            AcademicProgramContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.AcademicProgram);
            SeekerStatusContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.SeekerStatus);
            ProgramLengthContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.ProgramLength);
            CollegesContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.College);
            FirstGenerationContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.FirstGeneration);
            EthnicityContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.Ethnicity);
            ReligionContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.Religion);
            GendersContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.Gender);
            LocationSelectorContainer.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.SeekerGeographicsLocation); 
            AcademicAreasContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.AcademicArea);
            CareersContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.Career);
            CommunityServicesContainerControl.Visible =
                criteria.HasAttributeUsage(SeekerProfileAttribute.CommunityService);
            OrganizationsContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.OrganizationAndAffiliationType);
            AffiliationTypesContainerControl.Visible =
                criteria.HasAttributeUsage(SeekerProfileAttribute.OrganizationAndAffiliationType);
            HobbiesContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.SeekerHobby);
            SportsContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.Sport);
            ClubsContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.Club);
            WorkTypeContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.WorkType);
            WorkHoursContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.WorkHour);
            ServiceTypeContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.ServiceType);
            ServiceHoursContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.ServiceHour);
            GPAControlContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.GPA);
            ClassRankContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.ClassRank);
            SATContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.SAT);
            ACTContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.ACT);
            HonorsContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.Honor);
            APCreditContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.APCreditsEarned);
            IBCreditContainerControl.Visible = criteria.HasAttributeUsage(SeekerProfileAttribute.IBCreditsEarned);
            
            SetDependentVisibility();
        }

        private void SetDependentVisibility()
        {
            SeekerPersonalityContainerControl.Visible = 
                FiveWordsContainerControl.Visible |
                FiveSkillsContainerControl.Visible;

            SeekerTypeContainerControl.Visible =
                StudentGroupControl.Visible |
                SchoolTypeControl.Visible |
                AcademicProgramControl.Visible |
                SeekerStatusControl.Visible |
                ProgramLengthControl.Visible |
                CollegesContainerControl.Visible;
            

            SeekerDemographicsPersonalContainerControl.Visible =
                FirstGenerationContainerControl.Visible |
                EthnicityContainerControl.Visible |
                ReligionContainerControl.Visible |
                GendersContainerControl.Visible;

            SeekerDemographicsGeographicContainerControl.Visible = LocationSelectorContainer.Visible;
            
            SeekerInterestsControlContainer.Visible =
                AcademicAreasContainerControl.Visible |
                CareersContainerControl.Visible |
                CommunityServicesContainerControl.Visible |
                OrganizationsContainerControl.Visible |
                AffiliationTypesContainerControl.Visible;

            SeekerActivitiesControlContainer.Visible =
                HobbiesContainerControl.Visible |
                SportsContainerControl.Visible |
                ClubsContainerControl.Visible |
                WorkTypeContainerControl.Visible |
                WorkHoursContainerControl.Visible |
                ServiceTypeContainerControl.Visible |
                ServiceHoursContainerControl.Visible;

            SeekerPerformanceControlContainer.Visible =
                GPAControlContainerControl.Visible |
                ClassRankContainerControl.Visible |
                SATContainerControl.Visible |
                ACTContainerControl.Visible |
                HonorsControl.Visible |
                APCreditControl.Visible;


            NoSelectionContainerControl.Visible = !
                (SeekerPersonalityContainerControl.Visible |
                SeekerTypeContainerControl.Visible |
                SeekerDemographicsPersonalContainerControl.Visible |
                SeekerDemographicsGeographicContainerControl.Visible |
                SeekerInterestsControlContainer.Visible |
                SeekerActivitiesControlContainer.Visible |
                SeekerPerformanceControlContainer.Visible);
        }
        #endregion

        private void PopulateScreen()
        {
            scholarshipNameLbl.Text = scholarshipInContext.Name;
            var seekerProfileCriteria = scholarshipInContext.SeekerProfileCriteria;

            FiveWordsControlDialogButton.Keys = seekerProfileCriteria.Words.CommaSeparatedIds();
            FiveSkillsControlDialogButton.Keys =  seekerProfileCriteria.Skills.CommaSeparatedIds();
            StudentGroupControl.SelectedValues = (int)seekerProfileCriteria.StudentGroups;
            SchoolTypeControl.SelectedValues = (int)seekerProfileCriteria.SchoolTypes;
            AcademicProgramControl.SelectedValues= (int)seekerProfileCriteria.AcademicPrograms;
            SeekerStatusControl.SelectedValues = (int)seekerProfileCriteria.SeekerStatuses;
            ProgramLengthControl.SelectedValues =(int)seekerProfileCriteria.ProgramLengths;
            CollegesControlDialogButton.Keys = seekerProfileCriteria.Colleges.CommaSeparatedIds();
            FirstGenerationControl.Checked = seekerProfileCriteria.FirstGeneration;
            EthnicityControl.SelectedValues = seekerProfileCriteria.Ethnicities.Cast<ILookup>().ToList();
            ReligionControl.SelectedValues = seekerProfileCriteria.Religions.Cast<ILookup>().ToList();
            GendersControl.SelectedValues = (int)seekerProfileCriteria.Genders;
            LocationSelectorControl.PopulateScreen(seekerProfileCriteria);
            AcademicAreasControlDialogButton.Keys = seekerProfileCriteria.AcademicAreas.CommaSeparatedIds();
            CareersControlDialogButton.Keys = seekerProfileCriteria.Careers.CommaSeparatedIds();
            CommunityServicesControlDialogButton.Keys = seekerProfileCriteria.CommunityServices.CommaSeparatedIds();
            OrganizationsControlDialogButton.Keys = seekerProfileCriteria.Organizations.CommaSeparatedIds();
            AffiliationTypesControlDialogButton.Keys = seekerProfileCriteria.AffiliationTypes.CommaSeparatedIds();
            SeekerHobbiesControlDialogButton.Keys = seekerProfileCriteria.Hobbies.CommaSeparatedIds();
            SportsControlDialogButton.Keys = seekerProfileCriteria.Sports.CommaSeparatedIds();
            ClubsControlDialogButton.Keys = seekerProfileCriteria.Clubs.CommaSeparatedIds();
            WorkTypeControlDialogButton.Keys = seekerProfileCriteria.WorkTypes.CommaSeparatedIds();
            WorkHoursControl.SelectedValues = seekerProfileCriteria.WorkHours.Cast<ILookup>().ToList();
            ServiceTypeControlDialogButton.Keys = seekerProfileCriteria.ServiceTypes.CommaSeparatedIds();
            ServiceHoursControl.SelectedValues = seekerProfileCriteria.ServiceHours.Cast<ILookup>().ToList();

            if (null != seekerProfileCriteria.GPA)
                GPAControl.PopulateFromRangeCondition(seekerProfileCriteria.GPA);

            if (null != seekerProfileCriteria.ClassRank)
                ClassRankControl.PopulateFromRangeCondition(seekerProfileCriteria.ClassRank);

            if (null != seekerProfileCriteria.SATScore)
            {
                SATWritingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Writing);
                SATCriticalReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.CriticalReading);
                SATMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.SATScore.Mathematics);
            }

            if (null != seekerProfileCriteria.ACTScore)
            {
                ACTEnglishControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.English);
                ACTMathematicsControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Mathematics);
                ACTReadingControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Reading);
                ACTScienceControl.PopulateFromRangeCondition(seekerProfileCriteria.ACTScore.Science);
            }
            HonorsControl.Checked = seekerProfileCriteria.Honors;
            APCreditControl.Text = 
                seekerProfileCriteria.APCreditsEarned.HasValue ?
                seekerProfileCriteria.APCreditsEarned.Value.ToString() :
                string.Empty;
            
            IBCreditControl.Text = 
                seekerProfileCriteria.IBCreditsEarned.HasValue ?
                seekerProfileCriteria.IBCreditsEarned.Value.ToString() :
                string.Empty;
        }

        private void PopulateObjects()
        {
            var seekerProfileCriteria = scholarshipInContext.SeekerProfileCriteria;
            
            PopulateList(FiveWordsContainerControl, FiveWordsControlDialogButton, seekerProfileCriteria.Words);
            PopulateList(FiveSkillsContainerControl, FiveSkillsControlDialogButton, seekerProfileCriteria.Skills);
            seekerProfileCriteria.StudentGroups = (StudentGroups) StudentGroupControl.SelectedValues;
            seekerProfileCriteria.AcademicPrograms =
                (AcademicPrograms) AcademicProgramControl.SelectedValues;
            seekerProfileCriteria.SeekerStatuses = (SeekerStatuses) SeekerStatusControl.SelectedValues;
            seekerProfileCriteria.ProgramLengths =
                (ProgramLengths) ProgramLengthControl.SelectedValues;
            PopulateList(CollegesContainerControl, CollegesControlDialogButton, seekerProfileCriteria.Colleges);
            seekerProfileCriteria.FirstGeneration = FirstGenerationControl.Checked;
            PopulateList(EthnicityContainerControl, EthnicityControl, seekerProfileCriteria.Ethnicities);
            PopulateList(ReligionContainerControl, ReligionControl, seekerProfileCriteria.Religions);
            seekerProfileCriteria.Genders = GendersContainerControl.Visible
                                              ? (Genders) GendersControl.SelectedValues
                                              : 0;

            LocationSelectorControl.PopulateObject(seekerProfileCriteria);

            PopulateList(AcademicAreasContainerControl, AcademicAreasControlDialogButton, seekerProfileCriteria.AcademicAreas);
            PopulateList(CareersContainerControl, CareersControlDialogButton, seekerProfileCriteria.Careers);
            PopulateList(CommunityServicesContainerControl, CommunityServicesControlDialogButton, seekerProfileCriteria.CommunityServices);
            PopulateList(OrganizationsContainerControl, OrganizationsControlDialogButton, seekerProfileCriteria.Organizations);
            PopulateList(AffiliationTypesContainerControl, AffiliationTypesControlDialogButton, seekerProfileCriteria.AffiliationTypes);
            PopulateList(HobbiesContainerControl, SeekerHobbiesControlDialogButton, seekerProfileCriteria.Hobbies);
            PopulateList(SportsContainerControl, SportsControlDialogButton, seekerProfileCriteria.Sports);
            PopulateList(ClubsContainerControl, ClubsControlDialogButton, seekerProfileCriteria.Clubs);
            PopulateList(WorkTypeContainerControl, WorkTypeControlDialogButton, seekerProfileCriteria.WorkTypes);
            PopulateList(WorkHoursContainerControl, WorkHoursControl, seekerProfileCriteria.WorkHours);
            PopulateList(ServiceTypeContainerControl, ServiceTypeControlDialogButton, seekerProfileCriteria.ServiceTypes);
            PopulateList(ServiceHoursContainerControl, ServiceHoursControl, seekerProfileCriteria.ServiceHours);
            
            seekerProfileCriteria.GPA = GPAControl.CreateIntegerRangeCondition();
            seekerProfileCriteria.ClassRank = ClassRankControl.CreateIntegerRangeCondition();

            if (!SATContainerControl.Visible)
            {
                seekerProfileCriteria.SATScore = new SatScore();
                seekerProfileCriteria.SATScore.Writing = SATWritingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.CriticalReading = SATCriticalReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.SATScore.Mathematics = SATMathematicsControl.CreateIntegerRangeCondition();
            }
            else
            {
                seekerProfileCriteria.SATScore = null;
            }

            if (!ACTContainerControl.Visible)
            {
                seekerProfileCriteria.ACTScore = new ActScore();
                seekerProfileCriteria.ACTScore.English = ACTEnglishControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Mathematics = ACTMathematicsControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Reading = ACTReadingControl.CreateIntegerRangeCondition();
                seekerProfileCriteria.ACTScore.Science = ACTScienceControl.CreateIntegerRangeCondition();
            }
            else
            {
                seekerProfileCriteria.ACTScore = null;
            }
            seekerProfileCriteria.Honors = HonorsControl.Checked;
            if (string.IsNullOrEmpty(APCreditControl.Text))
                seekerProfileCriteria.APCreditsEarned = null;
            else
                seekerProfileCriteria.APCreditsEarned = Int32.Parse(APCreditControl.Text);

            if (string.IsNullOrEmpty(IBCreditControl.Text))
                seekerProfileCriteria.IBCreditsEarned = null;
            else
                seekerProfileCriteria.IBCreditsEarned = Int32.Parse(IBCreditControl.Text);

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage <ScholarshipStages.SeekerProfile)
                scholarshipInContext.Stage = ScholarshipStages.SeekerProfile;
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupItemCheckboxList checkboxList, IList<T> list)
        {
            if (containerControl.Visible)
                checkboxList.PopulateListFromSelectedValues(list);
            else
                list.Clear();
        }

        private static void PopulateList<T>(PlaceHolder containerControl, LookupDialog lookupDialog, IList<T> list)
        {
            if (containerControl.Visible)
                lookupDialog.PopulateListFromSelection(list);
            else
                list.Clear();
        }

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.SeekerProfile;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.SeekerProfile;
        }

        public override bool IsCompleted
        {
            get { return scholarshipInContext.IsStageCompleted(ScholarshipStages.SeekerProfile); }
            set
            {
                scholarshipInContext.MarkStageCompletion(ScholarshipStages.SeekerProfile, value);
                ScholarshipService.Save(scholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            return true;
        }

        #endregion

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int) WizardStepName.MatchCriteria);
        }
    }
}