﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;
using System.Linq;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class FundingProfile : WizardStepUserControlBase<Scholarship>
    {
        private const string TERMS_OF_SUPPORT_ACADEMIC_YEAR_ID_STRING = "3";
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public ISupportDAL SupportDAL { get; set; }
        public INeedGapDAL NeedGapDAL { get; set; }
        public ITermOfSupportDAL TermOfSupportDAL { get; set; }

		private Scholarship _scholarshipInContext;
    	private Scholarship scholarshipInContext
    	{
    		get
    		{
    			if (_scholarshipInContext == null)
    				_scholarshipInContext = Container.GetDomainObject();
    			return _scholarshipInContext;
    		}
    	}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        public override void Activated()
        {
            base.Activated();
            SetControlsVisibility();
            SetupAttributeUsageTypeIcons();
        }

        private void SetupAttributeUsageTypeIcons()
        {
            Domain.ScholarshipParts.FundingProfile fundingProfile = scholarshipInContext.FundingProfile;

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(NeedUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.Need));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(SupportSituationUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.SupportedSituation));

            CriteriaUsageTypeIconHelper.SetupAttributeUsageTypeIcon(FundingParametersUsageTypeIconControl,
                           fundingProfile.GetAttributeUsageType(FundingProfileAttribute.FundingParameters));
        }

        private void SetControlsVisibility()
        {
            NeedContainerControl.Visible =
                scholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.Need);
            SupportSituationContainerControl.Visible =
                scholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.SupportedSituation);
            FundingParametersContainerControl.Visible =
                scholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.FundingParameters);


            NoSelectionContainerControl.Visible = !
                                                  (NeedContainerControl.Visible |
                                                   SupportSituationContainerControl.Visible |
                                                   FundingParametersContainerControl.Visible);
        }

        private void PopulateScreen()
        {
            PopulateListControl(NeedGaps, NeedGapDAL.FindAll());
            PopulateListControl(TypesOfSupport, SupportDAL.FindAll());
            PopulateListControl(TermsOfSupportTermUnitControl, TermOfSupportDAL.FindAll().OrderBy(o => o.Id));
            TermsOfSupportTermUnitControl.SelectedValue = TERMS_OF_SUPPORT_ACADEMIC_YEAR_ID_STRING;

            scholarshipNameLbl.Text = scholarshipInContext.Name;

            if (null != scholarshipInContext.FundingProfile.Need)
            {
                Fafsa.Checked = scholarshipInContext.FundingProfile.Need.Fafsa;
                UserDerived.Checked = scholarshipInContext.FundingProfile.Need.UserDerived;
                MinimumSeekerNeed.Amount = scholarshipInContext.FundingProfile.Need.MinimumSeekerNeed;
                MaximumSeekerNeed.Amount = scholarshipInContext.FundingProfile.Need.MaximumSeekerNeed;
                NeedGaps.Items.SelectItems(scholarshipInContext.FundingProfile.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != scholarshipInContext.FundingProfile.SupportedSituation)
            {
                Emergency.Checked = scholarshipInContext.FundingProfile.SupportedSituation.Emergency;
                Traditional.Checked = scholarshipInContext.FundingProfile.SupportedSituation.Traditional;

                TypesOfSupport.Items.SelectItems(scholarshipInContext.FundingProfile.SupportedSituation.TypesOfSupport,
                                                 ts => ts.Id.ToString());
            }

            var fundingParameters = scholarshipInContext.FundingProfile.FundingParameters;
            if (null != fundingParameters)
            {
                AnnualSupportAmount.Amount = fundingParameters.AnnualSupportAmount;
                MinimumNumberOfAwards.Amount = fundingParameters.MinimumNumberOfAwards;
                MaximumNumberOfAwards.Amount = fundingParameters.MaximumNumberOfAwards;
                TermsOfSupportUnitCountControl.Amount = fundingParameters.TermOfSupportUnitCount;
                if (null != fundingParameters.TermOfSupport)
                    TermsOfSupportTermUnitControl.SelectedValue = fundingParameters.TermOfSupport.Id.ToString();
            }
        }

        private void PopulateObjects()
        {
            // Need
            if (null == scholarshipInContext.FundingProfile.Need)
                scholarshipInContext.FundingProfile.Need = new DefinitionOfNeed();
            scholarshipInContext.FundingProfile.Need.Fafsa = Fafsa.Checked;
            scholarshipInContext.FundingProfile.Need.UserDerived = UserDerived.Checked;
            scholarshipInContext.FundingProfile.Need.MinimumSeekerNeed = MinimumSeekerNeed.Amount;
            scholarshipInContext.FundingProfile.Need.MaximumSeekerNeed = MaximumSeekerNeed.Amount;

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => (object)item);
            scholarshipInContext.FundingProfile.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            scholarshipInContext.FundingProfile.SupportedSituation.Emergency = Emergency.Checked;
            scholarshipInContext.FundingProfile.SupportedSituation.Traditional = Traditional.Checked;

            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            scholarshipInContext.FundingProfile.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            // FundingParameters
            
 
            
            scholarshipInContext.FundingProfile.FundingParameters.AnnualSupportAmount = AnnualSupportAmount.Amount;
            scholarshipInContext.FundingProfile.FundingParameters.MinimumNumberOfAwards = (int) MinimumNumberOfAwards.Amount;
            scholarshipInContext.FundingProfile.FundingParameters.MaximumNumberOfAwards = (int) MaximumNumberOfAwards.Amount;

            var selectedTermsOfSupport = Int32.Parse(TermsOfSupportTermUnitControl.SelectedValue);
            var selectedTermOfSupport = TermOfSupportDAL.FindById(selectedTermsOfSupport);
            var termOfSupportCount = (int)TermsOfSupportUnitCountControl.Amount;

            scholarshipInContext.FundingProfile.FundingParameters.TermOfSupportUnitCount = termOfSupportCount;
            scholarshipInContext.FundingProfile.FundingParameters.TermOfSupport = selectedTermOfSupport;

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage < ScholarshipStages.FundingProfile)
                scholarshipInContext.Stage = ScholarshipStages.FundingProfile;
        }

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
            Container.Goto((int)WizardStepName.MatchCriteria);
		}

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.FundingProfile;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.FundingProfile;
        }

        public override bool IsCompleted
        {
            get { return scholarshipInContext.IsStageCompleted(ScholarshipStages.FundingProfile); }
            set
            {
                scholarshipInContext.MarkStageCompletion(ScholarshipStages.FundingProfile, value);
                ScholarshipService.Save(scholarshipInContext);
            }
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public decimal GetMoneyValue(TextBox tb, PropertyProxyValidator validator)
        {
            decimal amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (! Decimal.TryParse(tb.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public int GetIntValue(TextBox tb, PropertyProxyValidator validator)
        {
            int amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (!Int32.TryParse(tb.Text, NumberStyles.Integer | NumberStyles.AllowThousands, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public void PopulateListControl(ListControl ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }

        #endregion
    }
}