﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="RegisterProvider.aspx.cs"
    Inherits="ScholarBridge.Web.Provider.RegisterProvider" Title="Register Provider" %>

<%@ Register TagPrefix="sb" TagName="RegisterOrganization" Src="~/Common/RegisterOrganization.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <sb:RegisterOrganization id="registerOrg" runat="server" />
</asp:Content>
