﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Business;
using ScholarBridge.Data;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Common.Lookup;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class FinancialNeed : WizardStepUserControlBase<Domain.Seeker>
    {
        Domain.Seeker _seekerInContext;
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }
        public ISupportDAL SupportDAL { get; set; }
        public INeedGapDAL NeedGapDAL { get; set; }
        Domain.Seeker seekerInContext
        {
            get
            {
                if (_seekerInContext == null)
                    _seekerInContext = Container.GetDomainObject();
                return _seekerInContext;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (seekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context");

            if (!Page.IsPostBack)
            {

                NeedGaps.DataBind();
                TypesOfSupport.DataBind();

                PopulateScreen();
            }
        }
        private void PopulateScreen()
        {
            PopulateCheckBoxes(NeedGaps, NeedGapDAL.FindAll());
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());
            if (null != seekerInContext.Need)
            {
                Fafsa.Checked = seekerInContext.Need.Fafsa;
                UserDerived.Checked = seekerInContext.Need.UserDerived;
                MinimumSeekerNeed.Text = seekerInContext.Need.MinimumSeekerNeed.ToString();
                MaximumSeekerNeed.Text = seekerInContext.Need.MaximumSeekerNeed.ToString();
                NeedGaps.Items.SelectItems(seekerInContext.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != seekerInContext.SupportedSituation)
            {
                Emergency.Checked = seekerInContext.SupportedSituation.Emergency;
                Traditional.Checked = seekerInContext.SupportedSituation.Traditional;

                TypesOfSupport.Items.SelectItems(seekerInContext.SupportedSituation.TypesOfSupport,
                                                 ts => ts.Id.ToString());
            }

        }

        private void PopulateObjects()
        {
            // Need
            if (null == seekerInContext.Need)
                seekerInContext.Need = new DefinitionOfNeed();
            seekerInContext.Need.Fafsa = Fafsa.Checked;
            seekerInContext.Need.UserDerived = UserDerived.Checked;
            seekerInContext.Need.MinimumSeekerNeed = GetMoneyValue(MinimumSeekerNeed, MinimumSeekerNeedValidator);
            seekerInContext.Need.MaximumSeekerNeed = GetMoneyValue(MaximumSeekerNeed, MaximumSeekerNeedValidator);

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => (object)item);
            seekerInContext.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            seekerInContext.SupportedSituation.Emergency = Emergency.Checked;
            seekerInContext.SupportedSituation.Traditional = Traditional.Checked;

            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            seekerInContext.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            

            seekerInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (seekerInContext.Stage < SeekerStages.FinancialNeed)
                seekerInContext.Stage = SeekerStages.FinancialNeed;
        }


        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }
        public decimal GetMoneyValue(TextBox tb, PropertyProxyValidator validator)
        {
            decimal amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (!Decimal.TryParse(tb.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }
       
        private static bool Validate(PlaceHolder applicabilityControl, LookupDialog dialogButton)
        {
            return true;
        }
        #region WizardStepUserControlBase methods

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            SeekerService.Update(seekerInContext);
        }

        public override bool WasSuspendedFrom(Domain.Seeker @object)
        {
            return @object.Stage == SeekerStages.FinancialNeed;
        }

        public override bool CanResume(Domain.Seeker @object)
        {
            return @object.Stage >= SeekerStages.FinancialNeed;
        }

        public override bool IsCompleted
        {
            get { return true; }
            set { }
        }
        #endregion
	}
}