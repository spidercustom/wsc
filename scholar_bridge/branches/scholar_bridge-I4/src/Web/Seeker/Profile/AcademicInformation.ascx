﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AcademicInformation.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.AcademicInformation" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/EnumRadioButtonList.ascx" tagname="EnumRadioButtonList" tagprefix="sb" %>


<label for="CurrentStudentGroupControl">What type of student are you currently?:</label>
<sb:EnumRadioButtonList id="CurrentStudentGroupControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.StudentGroups, ScholarBridge.Domain"/>
<br />

<label id="HighSchoolLabelControl" for="HighSchoolControl">High School you are currently attending:</label>
<asp:TextBox ID="HighSchoolControl" ReadOnly="true" runat="server"></asp:TextBox>
<sb:LookupDialog ID="HighSchoolControlDialogButton" runat="server" BuddyControl="HighSchoolControl" ItemSource="HighSchoolDAL" Title="High School Selection" SelectionLimit="1" />
<br />

<h3>High School Academic Performance</h3>

<label for="GPAControl">GPA:</label>
<asp:TextBox ID="GPAControl" runat="server" />
<elv:PropertyProxyValidator ID="GPAValidator" runat="server" ControlToValidate="GPAControl" PropertyName="GPA" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
<br />
<label for="ClassRankControl">Class Rank:</label>
<asp:TextBox ID="ClassRankControl" runat="server" />
<elv:PropertyProxyValidator ID="ClassRankValidator" runat="server" ControlToValidate="ClassRankControl" PropertyName="ClassRank" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
<br />

<fieldset>
<h4>SAT</h4>
<label for="SATWritingControl">Writing:</label>
<asp:TextBox ID="SATWritingControl" runat="server" />
<elv:PropertyProxyValidator ID="SATWritingControlValidator" runat="server" ControlToValidate="ClassRankControl" PropertyName="Writing" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
<br />
<label for="SATReadingControl">Critical Reading:</label>
<asp:TextBox ID="SATReadingControl" runat="server" />
<elv:PropertyProxyValidator ID="SATReadingValidator" runat="server" ControlToValidate="SATReadingControl" PropertyName="CriticalReading" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
<br />
<label for="SATMathControl">Mathematics:</label>
<asp:TextBox ID="SATMathControl" runat="server" />
<elv:PropertyProxyValidator ID="SATMathValidator" runat="server" ControlToValidate="SATMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.SatScore" />
<br />
</fieldset>

<fieldset>
<h4>ACT</h4>
<label for="ACTEnglishControl">English:</label>
<asp:TextBox ID="ACTEnglishControl" runat="server" />
<elv:PropertyProxyValidator ID="ACTEnglishValidator" runat="server" ControlToValidate="ACTEnglishControl" PropertyName="English" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
<br />
<label for="ACTReadingControl">Reading:</label>
<asp:TextBox ID="ACTReadingControl" runat="server" />
<elv:PropertyProxyValidator ID="ACTReadingValidator" runat="server" ControlToValidate="ACTReadingControl" PropertyName="Reading" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />

<br />
<label for="ACTMathControl">Mathematics:</label>
<asp:TextBox ID="ACTMathControl" runat="server" />
<elv:PropertyProxyValidator ID="ACTMathValidator" runat="server" ControlToValidate="ACTMathControl" PropertyName="Mathematics" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />

<br />
<label for="ACTScienceControl">Science:</label>
<asp:TextBox ID="ACTScienceControl" runat="server" />
<elv:PropertyProxyValidator ID="ACTScienceValidator" runat="server" ControlToValidate="ACTScienceControl" PropertyName="Science" SourceTypeName="ScholarBridge.Domain.SeekerParts.ActScore" />
<br />
</fieldset>

<label for="HonorsControl">Honors:</label>
<asp:CheckBox ID="HonorsControl" Text="" runat="server" />
<br />

<label for="APCreditsControl">AP Credits:</label>
<asp:TextBox ID="APCreditsControl" runat="server" />
<elv:PropertyProxyValidator ID="APCreditsValidator" runat="server" ControlToValidate="APCreditsControl" PropertyName="APCredits" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />

<br />
<label for="IBCreditsControl">IB Credits:</label>
<asp:TextBox ID="IBCreditsControl" runat="server" />
<elv:PropertyProxyValidator ID="IBCreditsValidator" runat="server" ControlToValidate="IBCreditsControl" PropertyName="IBCredits" SourceTypeName="ScholarBridge.Domain.SeekerParts.SeekerAcademics" />
<br />

<label for="CollegeControl">College you are currently attending:</label>
<asp:TextBox ID="CollegeControl" ReadOnly="true" runat="server"></asp:TextBox>
<sb:LookupDialog ID="CollegeControlDialogButton" runat="server" BuddyControl="CollegeControl" ItemSource="CollegeDAL" Title="College Selection" SelectionLimit="1" />
<br />

<label for="SchoolTypeControl">What types of schools are you considering?</label>
<sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
<br />

<label for="AcademicProgramControl">Academic program:</label>
<sb:EnumRadioButtonList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
<br />

<label for="SeekerStatusControl">Enrollment status?:</label>
<sb:EnumRadioButtonList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
<br />

<label for="ProgramLengthControl">Length of program:</label>
<sb:EnumRadioButtonList id="ProgramLengthControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.ProgramLengths, ScholarBridge.Domain" />
<br />
  
<label for="FirstGenerationControl">First Generation:</label>
<asp:Image ID="FirstGenerUsageTypeIconControl" runat="server" />
<asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
<br />

<label for="CollegesAppliedLabelControl">List of Colleges I’m considering:</label>
<asp:TextBox ID="CollegesAppliedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
<sb:LookupDialog ID="CollegesAppliedControlDialogButton" runat="server" BuddyControl="CollegesAppliedLabelControl" ItemSource="CollegeDAL" Title="College Selection"/>
<br />

<label for="CollegesAcceptedLabelControl">List of Colleges I’m accepted at:</label>
<asp:TextBox ID="CollegesAcceptedLabelControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
<sb:LookupDialog ID="CollegesAcceptedControlDialogButton" runat="server" BuddyControl="CollegesAcceptedLabelControl" ItemSource="CollegeDAL" Title="College Selection"/>
<br />