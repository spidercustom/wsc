﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class Default : SBBasePage, IWizardStepsContainer<Domain.Seeker>
	{
		#region properties

		public ISeekerService SeekerService { get; set; }
		public IUserContext UserContext { get; set; }

		#endregion

		#region Page Lifecycle Events
		protected void Page_Load(object sender, EventArgs e)
        {
			Steps.ForEach(step => step.Container = this);

			if (!IsPostBack)
			{
				CheckForDataChanges = true;

				BypassPromptIds.AddRange(
					new[]	{
								"PreviousButton",                            
								"NextButton",                            
								"SaveAndExitButton"
							});

				ActiveStepIndex = WizardStepName.Basics.GetNumericValue();
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			PreviousButton.Enabled = ActiveStepIndex != 0;
			NextButton.Enabled = ActiveStepIndex != Steps.Length - 1;
		}

		#endregion

		#region Control event handlers

		protected void BuildSeekerProfileWizard_ActiveStepChanged(object sender, EventArgs e)
    	{
			WizardStepContainerCommon.NotifyStepActivated(this);
		}

    	protected void PreviousButton_Click(object sender, EventArgs e)
    	{
    		GoPrior();
		}

    	protected void NextButton_Click(object sender, EventArgs e)
    	{
			Save();
    		GoNext();
    	}

    	protected void SaveAndExitButton_Click(object sender, EventArgs e)
    	{
			if (Page.IsValid)
			{
				Save();
				Response.Redirect("../");
			}
		}

		protected void CancelButton_Click(object sender, EventArgs e)
    	{
			Response.Redirect("../");
		}

		#endregion

		#region Private Methods

		private bool ValidateStep()
		{
			return WizardStepContainerCommon.ValidateStep(this);
		}

		private void Save()
		{
			if (Page.IsValid && ValidateStep())
			{
				WizardStepContainerCommon.SaveActiveStep(this);
				Dirty = false;
			}
		}

		#endregion

		#region IWizardStepsContainer<Scholarship> Members

		public IWizardStepControl<Domain.Seeker>[] Steps
		{
			get
			{
				IWizardStepControl<Domain.Seeker>[] stepControls = BuildSeekerProfileWizard.Views.FindWizardStepControls<Domain.Seeker>();
				return stepControls;
			}
		}

		Domain.Seeker seekerInContext;

		public Domain.Seeker GetDomainObject()
		{
			if (seekerInContext == null)
			{
				seekerInContext = GetCurrentSeeker() ?? new Domain.Seeker() {User = UserContext.CurrentUser};
			}
			return seekerInContext;
		}

		private Domain.Seeker GetCurrentSeeker()
		{
			return SeekerService.FindByUser(UserContext.CurrentUser);
		}

		public void GoPrior()
		{
			WizardStepContainerCommon.GoPrior(this);
		}

		public void GoNext()
		{
			WizardStepContainerCommon.GoNext(this);
		}

		public void Goto(int index)
		{
			WizardStepContainerCommon.Goto(this, index);
		}

		public int ActiveStepIndex
		{
			get { return BuildSeekerProfileWizard.ActiveViewIndex; }
			set { BuildSeekerProfileWizard.ActiveViewIndex = value; }
		}

		#endregion
	}
}