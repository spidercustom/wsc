﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;
using ScholarBridge.Web.Wizards;

namespace ScholarBridge.Web.Seeker.Profile
{
	public partial class AcademicInformation : WizardStepUserControlBase<Domain.Seeker>
    {
        public ISeekerService SeekerService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Seeker seeker;
        Domain.Seeker SeekerInContext
        {
            get
            {
                if (seeker == null)
                    seeker = Container.GetDomainObject();
                if (seeker == null)
                    throw new InvalidOperationException("There is no seeker in context");
                return seeker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SeekerInContext == null)
                throw new InvalidOperationException("There is no seeker in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
		}

        private void PopulateScreen()
        {
            FirstGenerationControl.Checked = SeekerInContext.FirstGeneration;

            if (null != SeekerInContext.CurrentSchool)
            {
                CurrentStudentGroupControl.SelectedValue = (int) SeekerInContext.CurrentSchool.LastAttended;

                if (null != SeekerInContext.CurrentSchool.College)
                    CollegeControlDialogButton.Keys = SeekerInContext.CurrentSchool.College.Id.ToString();

                if (null != SeekerInContext.CurrentSchool.HighSchool)
                    HighSchoolControlDialogButton.Keys = SeekerInContext.CurrentSchool.HighSchool.Id.ToString();
            }


            if (null != SeekerInContext.SeekerAcademics)
            {
                if (SeekerInContext.SeekerAcademics.GPA.HasValue)
                    GPAControl.Text = SeekerInContext.SeekerAcademics.GPA.Value.ToString("0.#");

                if (SeekerInContext.SeekerAcademics.ClassRank.HasValue)
                    ClassRankControl.Text = SeekerInContext.SeekerAcademics.ClassRank.Value.ToString();

                SchoolTypeControl.SelectedValues = (int)SeekerInContext.SeekerAcademics.SchoolTypes;
                AcademicProgramControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.AcademicPrograms;
                SeekerStatusControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.SeekerStatuses;
                ProgramLengthControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.ProgramLengths;
                
                if (null != SeekerInContext.SeekerAcademics.SATScore)
                {
                    SATReadingControl.Text = SeekerInContext.SeekerAcademics.SATScore.CriticalReading.ToString();
                    SATWritingControl.Text = SeekerInContext.SeekerAcademics.SATScore.Writing.ToString();
                    SATMathControl.Text = SeekerInContext.SeekerAcademics.SATScore.Mathematics.ToString();
                }
                if (null != SeekerInContext.SeekerAcademics.ACTScore)
                {
                    ACTEnglishControl.Text = SeekerInContext.SeekerAcademics.ACTScore.English.ToString();
                    ACTMathControl.Text = SeekerInContext.SeekerAcademics.ACTScore.Mathematics.ToString();
                    ACTReadingControl.Text = SeekerInContext.SeekerAcademics.ACTScore.Reading.ToString();
                    ACTScienceControl.Text = SeekerInContext.SeekerAcademics.ACTScore.Science.ToString();
                }

                APCreditsControl.Text = SeekerInContext.SeekerAcademics.APCredits.ToString();
                IBCreditsControl.Text = SeekerInContext.SeekerAcademics.IBCredits.ToString();
                HonorsControl.Checked = SeekerInContext.SeekerAcademics.Honors;
            }
        }

	    #region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

        public override void Save()
        {
            PopulateObjects();
            SeekerService.Update(SeekerInContext);
        }

        private void PopulateObjects()
        {
            SeekerInContext.FirstGeneration = FirstGenerationControl.Checked;

            if (null == SeekerInContext.SeekerAcademics)
            {
                SeekerInContext.SeekerAcademics = new SeekerAcademics();
            }

            SeekerInContext.SeekerAcademics.Honors = HonorsControl.Checked;
            SeekerInContext.SeekerAcademics.GPA = GetDoubleOrNull(GPAControl);
            SeekerInContext.SeekerAcademics.APCredits = GetIntOrNull(APCreditsControl);
            SeekerInContext.SeekerAcademics.IBCredits = GetIntOrNull(IBCreditsControl);
            SeekerInContext.SeekerAcademics.ClassRank = GetIntOrNull(ClassRankControl);

            if (null == SeekerInContext.SeekerAcademics.SATScore)
            {
                SeekerInContext.SeekerAcademics.SATScore = new SatScore();
            }

            SeekerInContext.SeekerAcademics.SATScore.CriticalReading = GetIntOrNull(SATReadingControl);
            SeekerInContext.SeekerAcademics.SATScore.Writing = GetIntOrNull(SATWritingControl);
            SeekerInContext.SeekerAcademics.SATScore.Mathematics = GetIntOrNull(SATMathControl);

            if (null == SeekerInContext.SeekerAcademics.ACTScore)
            {
                SeekerInContext.SeekerAcademics.ACTScore = new ActScore();
            }

            SeekerInContext.SeekerAcademics.ACTScore.English = GetIntOrNull(ACTEnglishControl);
            SeekerInContext.SeekerAcademics.ACTScore.Mathematics = GetIntOrNull(ACTMathControl);
            SeekerInContext.SeekerAcademics.ACTScore.Reading = GetIntOrNull(ACTReadingControl);
            SeekerInContext.SeekerAcademics.ACTScore.Science = GetIntOrNull(ACTScienceControl);

            if (null == SeekerInContext.CurrentSchool)
            {
                SeekerInContext.CurrentSchool = new CurrentSchool();
            }
            SeekerInContext.CurrentSchool.LastAttended = (StudentGroups) CurrentStudentGroupControl.SelectedValue;

            var colleges = CollegeControlDialogButton.GetSelectedLookupItems();
            if (null != colleges && colleges.Count > 0)
            {
                SeekerInContext.CurrentSchool.College = (College) colleges[0];
            }
            else
            {
                SeekerInContext.CurrentSchool.College = null;
            }

            var highSchool = HighSchoolControlDialogButton.GetSelectedLookupItems();
            if (null != highSchool && highSchool.Count > 0)
            {
                SeekerInContext.CurrentSchool.HighSchool = (HighSchool)highSchool[0];
            }
            else
            {
                SeekerInContext.CurrentSchool.HighSchool = null;
            }

            SeekerInContext.SeekerAcademics.SchoolTypes = (SchoolTypes) SchoolTypeControl.SelectedValues;
            SeekerInContext.SeekerAcademics.AcademicPrograms = (AcademicPrograms) AcademicProgramControl.SelectedValue;
            SeekerInContext.SeekerAcademics.SeekerStatuses = (SeekerStatuses) SeekerStatusControl.SelectedValue;
            SeekerInContext.SeekerAcademics.ProgramLengths = (ProgramLengths) ProgramLengthControl.SelectedValue;

        }

        public double? GetDoubleOrNull(ITextControl tb)
        {
            if (String.IsNullOrEmpty(tb.Text))
            {
                return null;
            }
            return Double.Parse(tb.Text);
        }

        public int? GetIntOrNull(ITextControl tb)
        {
            if (String.IsNullOrEmpty(tb.Text))
            {
                return null;
            }
            return Int32.Parse(tb.Text);
        }

	    public override bool WasSuspendedFrom(Domain.Seeker @object)
        {
            return @object.Stage == SeekerStages.AcademicInfo;
        }

        public override bool CanResume(Domain.Seeker @object)
        {
            return @object.Stage >= SeekerStages.AcademicInfo;
        }

		public override bool IsCompleted
		{
			get { return true; }
			set { }
		}
		#endregion

    }
}