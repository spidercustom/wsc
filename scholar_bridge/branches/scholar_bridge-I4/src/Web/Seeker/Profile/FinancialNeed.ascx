﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinancialNeed.ascx.cs" Inherits="ScholarBridge.Web.Seeker.Profile.FinancialNeed" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<div>
<h4>What is your financial need?</h4>
<br />
    <asp:PlaceHolder ID="NeedContainerControl" runat="server">
      <h5>Need</h5>
      <asp:CheckBox ID="Fafsa" runat="server" Text="FAFSA Definition of Need" TextAlign="Left"/>
      <asp:CheckBox ID="UserDerived" runat="server" Text="User Derived Definition of Need" TextAlign="Left"/>
      <br />
       <br />
      <label for="MinimumSeekerNeed">Minimum Need:</label>
      <br />
      <asp:TextBox ID="MinimumSeekerNeed" runat="server"></asp:TextBox>
      <elv:PropertyProxyValidator ID="MinimumSeekerNeedValidator" runat="server" ControlToValidate="MinimumSeekerNeed" PropertyName="MinimumSeekerNeed"  SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
      <br />
      <label for="MaximumSeekerNeed">Maximum Need:</label>
      <br />
      <asp:TextBox ID="MaximumSeekerNeed" runat="server"></asp:TextBox>
      <elv:PropertyProxyValidator ID="MaximumSeekerNeedValidator" runat="server" ControlToValidate="MaximumSeekerNeed" PropertyName="MaximumSeekerNeed" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.DefinitionOfNeed"/>
      <br />
      <label for="NeedGaps">Need Gap Threshold:</label>
      <br />
      <asp:CheckBoxList ID="NeedGaps" runat="server" TextAlign="Left"/>
      <br />
    </asp:PlaceHolder>
    <br />
    <h5>Types of Support</h5>
    
    <asp:PlaceHolder ID="SupportSituationContainerControl" runat="server">
      
      <asp:CheckBox ID="Emergency" runat="server" Text="Emergency funding" />
      <asp:CheckBox ID="Traditional" runat="server" Text="Traditional support" />
      <br />
      <label for="TypesOfSupport">Types of Support:</label>
      <asp:CheckBoxList ID="TypesOfSupport" runat="server" TextAlign="Left"/>
      <br />
    </asp:PlaceHolder>
    
</div>