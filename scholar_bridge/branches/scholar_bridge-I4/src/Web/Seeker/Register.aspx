﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Register" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>

 <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" 
        DisableCreatedUser="True"
        OnCreatedUser="CreateUserWizard1_CreatedUser"
        OnCreatingUser="CreateUserWizard1_CreatingUser"
        CreateUserButtonText="Register" 
        InvalidPasswordErrorMessage="Password requires 1 capital letter &amp; 1 numeric or special character with an 8 character minimum" 
        oncreateusererror="CreateUserWizard1_CreateUserError" 
        onsendingmail="CreateUserWizard1_SendingMail">
    <WizardSteps>
        <asp:CreateUserWizardStep  ID="CreateUserWizardStep1" runat="server">
            <ContentTemplate>
            
                <h3>User Information</h3>
                <label for="FirstName">First Name:</label>
                <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="FirstNameValidator" runat="server" ControlToValidate="FirstName" PropertyName="FirstName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="MiddleName">Middle Name:</label>
                <asp:TextBox ID="MiddleName" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="MiddleNameValidator" runat="server" ControlToValidate="MiddleName" PropertyName="MiddleName" SourceTypeName="ScholarBridge.Domain.PersonName"  ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="LastName">Last Name:</label>
                <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="LastNameValidator" runat="server" ControlToValidate="LastName" PropertyName="LastName" SourceTypeName="ScholarBridge.Domain.PersonName" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="UserName">Email Address:</label>
                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                <elv:PropertyProxyValidator ID="UserNameValidator" runat="server" ControlToValidate="UserName" PropertyName="Email" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                <asp:TextBox ID="Email" runat="server" Visible="false"></asp:TextBox>
                <br />
                <label for="ConfirmEmail">Confirm Email Address:</label>
                <asp:TextBox ID="ConfirmEmail" runat="server"></asp:TextBox>
                <asp:CompareValidator ID="ConfirmEmailValidator" runat="server" ControlToValidate="ConfirmEmail" 
                    ControlToCompare="UserName" ErrorMessage="Confirm Email Address must match Email Address." />
                <br />
                
                <label for="Password">Password:</label>
                <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                <span class="noteBene">Password needs to be at least 8 characters with one capital letter and one special character</span>
                <elv:PropertyProxyValidator ID="PasswordValidator" runat="server" ControlToValidate="Password" PropertyName="Password" SourceTypeName="ScholarBridge.Domain.Auth.User" ValidationGroup="CreateUserWizard1"/>
                <br />
                <label for="ConfirmPassword">Confirm Password:</label>
                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                    ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required."
                    ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                    ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                    ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                <br />

                <p>Placekeeper for note on junk email filter</p>
            </ContentTemplate>
        </asp:CreateUserWizardStep>
        
        <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
            <ContentTemplate>
                <h2>Thanks for registering with us</h2>
                <p>Now wait for an email to be sent to the email address you specified with instructions
                to enable your account and login.</p>
            </ContentTemplate>
        </asp:CompleteWizardStep>
    </WizardSteps>
</asp:CreateUserWizard>

</asp:Content>
