﻿using System;

namespace ScholarBridge.Domain.Lookup
{
    [Flags]
    public enum Genders
    {
		Unspecified = 0,
        Male = 1,
        Female = 2
    }
}
