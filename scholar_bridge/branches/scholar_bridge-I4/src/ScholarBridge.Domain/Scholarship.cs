﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain
{
    public class Scholarship : ICloneable
    {
        public Scholarship()
        {
            AdditionalRequirements = new List<AdditionalRequirement>();
            AdditionalQuestions = new List<ScholarshipQuestion>();
            FundingProfile = new FundingProfile();
            Donor = new ScholarshipDonor();
            SeekerProfileCriteria = new SeekerProfileCriteria();
            ScheduleForHibernate = new List<ScholarshipScheduleBase>();
            CompletedStages = new List<ScholarshipStages>();
            Attachments = new List<Attachment>();
        }

        public virtual int Id { get; set; }

        [NotNullValidator]
        [StringLengthValidator(1, 50)]
        public virtual string Name { get; set; }

        public virtual AcademicYear AcademicYear { get; set; }

        [StringLengthValidator(0, 2000)]
        public virtual string MissionStatement { get; set; }

        [StringLengthValidator(0, 2000)]
        public virtual string ProgramGuidelines { get; set; }

        [NotNullValidator]
        public virtual ScholarshipScheduleBase Schedule { 
            get
            {
                if (ScheduleForHibernate.Count > 0)
                    return ScheduleForHibernate[0];
                return null;
            }
            set
            {
                ScheduleForHibernate.Clear();
                if (null != value)
                {
                    value.Scholarship = this;
                    ScheduleForHibernate.Add(value);
                }
            }
        }

        // XXX: Total hack to support polymorphic one-to-one relationships
        protected virtual IList<ScholarshipScheduleBase> ScheduleForHibernate { get; set; }

        public virtual ScholarshipDonor Donor { get; set; }

        [NotNullValidator]
        public virtual Provider Provider { get; set; }

        public virtual Intermediary Intermediary { get; set; }

        public virtual ActivityStamp LastUpdate { get; set; }

        public virtual ScholarshipStages Stage { get; set; }

        [NotNullValidator]
        [MinimumValidator(0)]
        public virtual decimal MinimumAmount { get; set; }

        [NotNullValidator]
        [MinimumValidator(1)]
        [PropertyComparisonValidator("MinimumAmount", ComparisonOperator.GreaterThanEqual, MessageTemplate = "Maximum amount should be greater than or equal to minimum amount")]
        public virtual decimal MaximumAmount { get; set; }

        public virtual SeekerProfileCriteria SeekerProfileCriteria { get; set; }

        public virtual FundingProfile FundingProfile { get; set; }

        public virtual IList<Attachment> Attachments { get; protected set; }

		public virtual IList<ScholarshipQuestion> AdditionalQuestions { get; protected set; }

        public virtual IList<ScholarshipStages> CompletedStages { get; set; }
        
        /// <summary>
        /// Remove a Question from the Scholarship.
        /// </summary>
        /// <remarks>
        /// You really need to use this to add Questions because of the oddities of one-to-many 
        /// relationships when the key on the child table is NOT NULL.
        /// </remarks>
        /// <param name="question"></param>
        public virtual void AddAdditionalQuestion(ScholarshipQuestion question)
        {
            question.Scholarship = this;
            AdditionalQuestions.Add(question);
        }

        public virtual void RemoveAdditionalQuestion(int position)
        {
            AdditionalQuestions.RemoveAt(position);
        }

		public virtual IList<AdditionalRequirement> AdditionalRequirements { get; protected set; }

		public virtual void ResetAdditionalRequirements(IList<AdditionalRequirement> additionalReqs)
        {
            AdditionalRequirements.Clear();
            foreach (var ar in additionalReqs)
            {
                AdditionalRequirements.Add(ar);
            }
        }

        public virtual string DisplayName
        {
            get
            {
                if (null != Donor && ! String.IsNullOrEmpty(Donor.Name.ToString().Trim()))
                    return String.Format("{0} ({1})", Name, Donor.Name);
                return Name;
            }
        }

        public virtual bool CanEdit()
        {
            return ! IsInActivationProcess();
        }

        public virtual bool CanSubmitForActivation()
        {
            return Stage != ScholarshipStages.None && ! IsInActivationProcess();
        }

        private bool IsInActivationProcess()
        {
            return Stage == ScholarshipStages.RequestedActivation ||
                   Stage == ScholarshipStages.Activated ||
                   Stage == ScholarshipStages.Awarded;
        }

        public virtual void MarkStageCompleted(ScholarshipStages stage)
        {
            if (!CompletedStages.Contains(stage))
                CompletedStages.Add(stage);
        }

        public virtual void MarkStageIncomplete(ScholarshipStages stage)
        {
            if (CompletedStages.Contains(stage))
                CompletedStages.Remove(stage);
        }

        public virtual void MarkStageCompletion(ScholarshipStages stage, bool isCompleted)
        {
            if (isCompleted)
                MarkStageCompleted(stage);
            else
                MarkStageIncomplete(stage);
        }

        public virtual bool IsStageCompleted(ScholarshipStages stages)
        {
            return CompletedStages.Contains(stages);
        }
        public virtual bool IsBelongToOrganization(Organization organization)
        {
            return Provider.Id.Equals(organization.Id) ||Intermediary.Id.Equals(organization.Id);
        }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            var result = (Scholarship) MemberwiseClone();
            result.Id = 0;
            result.CompletedStages.Clear();
            result.Schedule= (ScholarshipScheduleBase)Schedule.Clone();
            result.Schedule.Scholarship = result;
            result.LastUpdate = null;
            result.Stage = ScholarshipStages.None;
            result.SeekerProfileCriteria = (SeekerProfileCriteria) SeekerProfileCriteria.Clone();
            result.FundingProfile = (FundingProfile)FundingProfile.Clone();
            return result;
        }

        #endregion

    }
}
