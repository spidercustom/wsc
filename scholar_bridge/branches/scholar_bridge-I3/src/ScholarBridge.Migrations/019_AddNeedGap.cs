using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(19)]
    public class AddNeedGap : Migration
    {
        public const string TABLE_NAME = "SB_NeedGap";
        public const string PRIMARY_KEY_COLUMN = "Id";
        public static readonly string[] COLUMNS = { PRIMARY_KEY_COLUMN, 
                                                      "Name", "Description", 
                                                      "MinPercent", "MaxPercent",
                                                      "LastUpdatedBy", "LastUpdateDate" };

        protected static readonly string FK_LAST_UPDATED_BY = string.Format("FK_{0}_LastUpdatedBy", TABLE_NAME);

        public static readonly string[] INSERT_COLUMNS = { "Name", "Description", "MinPercent", "MaxPercent", "LastUpdatedBy" };

        public override void Up()
        {
            Database.AddTable(TABLE_NAME,
                              new Column(COLUMNS[0], DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity), 
                              new Column(COLUMNS[1], DbType.String, 50, ColumnProperty.NotNull),
                              new Column(COLUMNS[2], DbType.String, 250, ColumnProperty.NotNull),
                              new Column(COLUMNS[3], DbType.Double, ColumnProperty.NotNull),
                              new Column(COLUMNS[4], DbType.Double, ColumnProperty.NotNull),
                              new Column(COLUMNS[5], DbType.Int32, ColumnProperty.NotNull),
                              new Column(COLUMNS[6], DbType.DateTime, ColumnProperty.Null, "(getdate())")
                );

            Database.AddForeignKey(FK_LAST_UPDATED_BY, TABLE_NAME, "LastUpdatedBy", AddUsers.TABLE_NAME, AddUsers.PRIMARY_KEY_COLUMN);

            int adminId = DataHelper.GetAdminUserId(Database);
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "<10%", "Last dollar", "0", "0.09", adminId.ToString() });
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "10 to 39%", "", "0.1", "0.39", adminId.ToString() });
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "40 to 79%", "", "0.4", "0.79", adminId.ToString() });
            Database.Insert(TABLE_NAME, INSERT_COLUMNS, new[] { "80 to 100%", "First dollar", "0.8", "1", adminId.ToString() });
        }

        public override void Down()
        {
            Database.ExecuteNonQuery("DELETE FROM " + TABLE_NAME);
            Database.RemoveForeignKey(TABLE_NAME, FK_LAST_UPDATED_BY);
            Database.RemoveTable(TABLE_NAME);
        }
    }
}