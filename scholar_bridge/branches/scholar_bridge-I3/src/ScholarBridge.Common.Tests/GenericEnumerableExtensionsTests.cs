using System.Collections.Generic;
using System.Collections;
using System.Linq;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Common.Tests
{

    [TestFixture]
    public class GenericEnumerableExtensionsTests
    {

        [Test]
        public void can_run_for_each()
        {
            int i = 0;

            IEnumerable<Foo> foos = new List<Foo> {new Foo(), new Foo()};
            foos.ForEach(f => f.Name = "Foo" + i ++);
            Assert.AreEqual("Foo0", foos.ToList()[0].Name);
            Assert.AreEqual("Foo1", foos.ToList()[1].Name);
        }

        [Test]
        public void can_run_for_each_non_generic()
        {
            int i = 0;

            IEnumerable foos = new ArrayList { new Foo(), new Foo() };
            foos.ForEach((Foo f) => f.Name = "Foo" + i++);
            var fooEnum  = foos.GetEnumerator();
            
            Assert.IsTrue(fooEnum.MoveNext());
            Assert.AreEqual("Foo0", ((Foo) fooEnum.Current).Name);
            Assert.IsTrue(fooEnum.MoveNext());
            Assert.AreEqual("Foo1", ((Foo)fooEnum.Current).Name);
        }

        private class Foo
        {
            public string Name { get; set; }
        }
        
    }
}