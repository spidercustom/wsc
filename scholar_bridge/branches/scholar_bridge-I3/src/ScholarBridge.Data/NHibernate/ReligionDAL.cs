﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class ReligionDAL : LookupDAL<Religion>, IReligionDAL
    {
    }
}
