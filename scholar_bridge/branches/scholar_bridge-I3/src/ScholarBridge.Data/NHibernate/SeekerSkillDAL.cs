﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class SeekerSkillDAL : LookupDAL<SeekerSkill>, ISeekerSkillDAL
    {
    }
}
