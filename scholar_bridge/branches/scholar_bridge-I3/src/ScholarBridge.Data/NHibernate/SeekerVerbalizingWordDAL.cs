﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class SeekerVerbalizingWordDAL : LookupDAL<SeekerVerbalizingWord>, ISeekerVerbalizingWordDAL
    {
    }
}
