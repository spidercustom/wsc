﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class WorkTypeDAL : LookupDAL<WorkType>, IWorkTypeDAL
    {
    }
}
