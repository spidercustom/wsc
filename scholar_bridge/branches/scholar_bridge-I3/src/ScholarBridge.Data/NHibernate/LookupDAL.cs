﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Criterion;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data.NHibernate
{
    public class LookupDAL<T> : AbstractDAL<T>, ILookupDAL, IGenericLookupDAL<T> where T : ILookup
    {
        private static readonly SimpleExpression NOT_DEPRECATED = Restrictions.Eq("Deprecated", false);

        public T FindById(int id)
        {
            return UniqueResult("Id", id);
        }

        public IList<T> FindAll()
        {
            return CreateCriteria()
                .Add(NOT_DEPRECATED)
                .AddOrder(Order.Asc("Name"))
                .List<T>();
        }

        public IList<T> FindAll(IList<object> ids)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Id", ids.ToArray()))
                .List<T>();
        }

        IList<ILookup> ILookupDAL.FindAll()
        {
            var list = FindAll("Name");
            var result = new List<ILookup>(list.Cast<ILookup>());
            return result;
        }

        IList<ILookup> ILookupDAL.FindAll(IList<object> ids)
        {
            var list = FindAll(ids);
            var result = new List<ILookup>(list.Cast<ILookup>());
            return result;
        }

        ILookup ILookupDAL.FindById(int id)
        {
            return FindById(id);
        }

        List<KeyValuePair<string, string>> ILookupDAL.GetLookupItems()
        {
            var list = FindAll();
            var result = list.Select(
                o => new KeyValuePair<string, string>(o.Id.ToString(), o.Name)).ToList();
            return result;
        }


    }
}
