﻿using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Core.DAL;
using ScholarBridge.Common.Extensions;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class StateDAL : AbstractDAL<State>, IStateDAL, ILookupDAL
    {
        public State FindByAbbreviation(string abbreviation)
        {
            return UniqueResult("Abbreviation", abbreviation);
        }

        public IList<State> FindAll(IList<string> abbreviations)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Abbreviation", abbreviations.ToArray()))
                .List<State>();
        }

        public IList<State> FindAll()
        {
            return FindAll("Abbreviation");
        }

        #region ILookupDAL Members

        IList<ScholarBridge.Domain.Lookup.ILookup> ILookupDAL.FindAll()
        {
            IList<State> states = FindAll();
            return states.Cast<ILookup>().ToList();
        }

        IList<ScholarBridge.Domain.Lookup.ILookup> ILookupDAL.FindAll(IList<object> ids)
        {
            var list = FindAll(ids.Cast<string>().ToList());
            var result = new List<ILookup>(list.Cast<ILookup>());
            return result;
        }

        ScholarBridge.Domain.Lookup.ILookup ILookupDAL.FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        List<KeyValuePair<string, string>> ILookupDAL.GetLookupItems()
        {
            var list = FindAll();
            var result = list.Select(
                o => new KeyValuePair<string, string>(o.Abbreviation, o.Name)).ToList();
            return result;
        }

        #endregion
   }
}