﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ISeekerHobbyDAL : ILookupDAL<SeekerHobby>
    {
    }
}
