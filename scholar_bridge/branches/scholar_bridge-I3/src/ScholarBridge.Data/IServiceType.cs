﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IServiceTypeDAL : ILookupDAL<ServiceType>
    {
        
    }
}
