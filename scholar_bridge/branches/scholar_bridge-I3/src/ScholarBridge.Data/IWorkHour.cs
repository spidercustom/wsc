﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IWorkHourDAL : ILookupDAL<WorkHour>
    {
        
    }
}
