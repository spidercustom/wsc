﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ICityDAL : ILookupDAL<City>
    {
    }
}
