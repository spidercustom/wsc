﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IServiceHourDAL : ILookupDAL<ServiceHour>
    {
    }
}
