﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class RelationshipServiceTests
    {
        private RelationshipService relationshipService;
        private MockRepository mocks;
        private IRelationshipDAL relationshipDAL;
        private IMessagingService messagingService;
        private ITemplateParametersService templateParametersService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            relationshipDAL = mocks.StrictMock<IRelationshipDAL>();
            messagingService = mocks.StrictMock<IMessagingService>();
            templateParametersService = mocks.StrictMock<ITemplateParametersService>();
            relationshipService = new RelationshipService
                                      {
                                          RelationshipDAL = relationshipDAL,
                                          MessagingService = messagingService,
                                          TemplateParametersService = templateParametersService
                                      };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(relationshipDAL);
            mocks.BackToRecord(messagingService);
            mocks.BackToRecord(templateParametersService);
        }

        [Test]
        public void Inactivate_Relationship_submitter_provider()
        {
            var relationship = new Relationship
                                   {
                                       Id = 1,
                                       Status = RelationshipStatus.Active
                                   };
            var provider = new Provider
                               {
                                   Name = "Provider",
                                   AdminUser = new User {Id = 3}
                               };
            var intermediary = new Intermediary
                                   {
                                       Name = "intermediary",
                                       AdminUser = new User {Id = 1}
                                   };
            relationship.Provider = provider;
            relationship.Intermediary = intermediary;

            Expect.Call(relationshipDAL.Save(relationship)).Return(relationship);
            Expect.Call(
                () =>
                templateParametersService.RelationshipInActivated(Arg<Organization>.Is.Equal(provider),
                                                                  Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(
                () => messagingService.SendMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));

            mocks.ReplayAll();

            relationshipService.InActivate(relationship);
            Assert.IsTrue(relationship.Status == RelationshipStatus.InActive);

            mocks.VerifyAll();
        }

        [Test]
        public void Inactivate_Relationship_submitterIntermediary()
        {
            var provider = new Provider
                               {
                                   Name = "Provider",
                                   AdminUser = new User {Id = 3}
                               };
            var intermediary = new Intermediary
                                   {
                                       Name = "intermediary",
                                       AdminUser = new User {Id = 1}
                                   };
            var relationship = new Relationship
                                   {
                                       Id = 1,
                                       Status = RelationshipStatus.Active,
                                       Provider = provider,
                                       Intermediary = intermediary,
                                       Requester = RelationshipRequester.Intermediary
                                   };

            Expect.Call(relationshipDAL.Save(relationship)).Return(relationship);
            Expect.Call(
                () =>
                templateParametersService.RelationshipInActivated(Arg<Organization>.Is.Equal(intermediary),
                                                                  Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(
                () => messagingService.SendMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));

            mocks.ReplayAll();

            relationshipService.InActivate(relationship);
            Assert.IsTrue(relationship.Status == RelationshipStatus.InActive);

            mocks.VerifyAll();
        }

        [Test]
        public void create_relationship_request_submitter_provider()
        {
            var provider = new Provider
                               {
                                   Name = "Provider",
                                   AdminUser = new User {Id = 3}
                               };
            var intermediary = new Intermediary
                                   {
                                       Name = "intermediary",
                                       AdminUser = new User {Id = 1}
                                   };
            var relationship = new Relationship
                                   {
                                       Id = 1,
                                       Status = RelationshipStatus.Active,
                                       Requester = RelationshipRequester.Provider,
                                       Provider = provider,
                                       Intermediary = intermediary,
                                   };

            Expect.Call(relationshipDAL.Save(relationship)).Return(relationship);
            Expect.Call(
                () =>
                templateParametersService.RelationshipRequestCreated(Arg<Organization>.Is.Equal(provider),
                                                                     Arg<MailTemplateParams>.Is.Anything));
            Expect.Call(
                () => messagingService.SendMessage(Arg<Message>.Is.Anything, Arg<MailTemplateParams>.Is.Anything));

            mocks.ReplayAll();

            relationshipService.CreateRequest(relationship);
            Assert.IsTrue(relationship.Status == RelationshipStatus.Pending);

            mocks.VerifyAll();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void inactivate_relationship_throws_exception_if_null_relationship_passed()
        {
            relationshipService.InActivate(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void createrequest_throws_exception_if_null_relationship_passed()
        {
            relationshipService.CreateRequest(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void GetByProviderThrowsExceptionIfNullPassed()
        {
            relationshipService.GetByProvider(null);
            Assert.Fail();
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void GetByIntermediaryThrowsExceptionIfNullPassed()
        {
            relationshipService.GetByIntermediary(null);
            Assert.Fail();
        }
    }
}