﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ScholarBridge.Common.DayOfYear;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
    [TestFixture]
    public class ScholarshipDALTest : TestBase
    {
        public delegate void ScholarshipSimilarityAssertor(Scholarship @base, Scholarship check);
        public static List<ScholarshipSimilarityAssertor> OtherAssertors = new List<ScholarshipSimilarityAssertor>();


        public ProviderDAL ProviderDAL { get; set; }
        public UserDAL UserDAL { get; set; }
        public StateDAL StateDAL { get; set; }
        public ScholarshipDAL ScholarshipDAL { get; set; }

        private User user;
        private Provider provider;

        protected override void OnSetUpInTransaction()
        {
            user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", user);
        }

        [Test]
        public void Can_create_scholarship()
        {
            Scholarship scholarship = CreateTestObject();
            var newScholarship = InsertScholarship(scholarship);
            Assert.IsNotNull(newScholarship);
            Assert.AreNotEqual(0, newScholarship.Id);
        }

        [Test]
        public void clone_save_test()
        {
            Scholarship firstScholarship = CreateTestObject();
            var firstScholarshipRetrieved = InsertScholarship(firstScholarship);
            var cloneable = (ICloneable) firstScholarshipRetrieved;
            var clonedScholarship = (Scholarship) cloneable.Clone();
            clonedScholarship.LastUpdate = new ActivityStamp(user);
            clonedScholarship.Stage = ScholarshipStages.GeneralInformation;
            var clonedScholarshipRetrieved = InsertScholarship(clonedScholarship);
            AssertScholarshipsAreSmilar(firstScholarshipRetrieved, clonedScholarshipRetrieved);
        }

        public static void AssertScholarshipsAreSmilar(Scholarship expected, Scholarship actual)
        {
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.MissionStatement, actual.MissionStatement);
            Assert.AreEqual(expected.MinimumAmount, actual.MinimumAmount);
            Assert.AreEqual(expected.MaximumAmount, actual.MaximumAmount);
            OtherAssertors.ForEach(o => o(expected, actual));
        }


        [Test]
        public void Can_update_scholarship()
        {
            Scholarship scholarship = CreateTestObject();
            var newScholarship = InsertScholarship(scholarship);
            Assert.IsNotNull(newScholarship);
            Assert.AreNotEqual(0, newScholarship.Id);

            newScholarship.FundingProfile.FundingParameters = new FundingParameters
                                                   {
                                                       AnnualSupportAmount = 100,
                                                       MinimumNumberOfAwards = 1,
                                                       MaximumNumberOfAwards = 5
                                                   };
            ScholarshipDAL.Save(newScholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);

            Assert.AreEqual(100, (double)foundScholarship.FundingProfile.FundingParameters.AnnualSupportAmount, 0.001);
            Assert.AreEqual(1, foundScholarship.FundingProfile.FundingParameters.MinimumNumberOfAwards);
            Assert.AreEqual(5, foundScholarship.FundingProfile.FundingParameters.MaximumNumberOfAwards);
        }


        [Test]
        public void Can_create_scholarship_with_definition_of_need()
        {
            Scholarship scholarship = CreateTestObject();
            scholarship.FundingProfile.Need = new DefinitionOfNeed
                                   {
                                       Fafsa = true, 
                                       UserDerived = true, 
                                       MinimumSeekerNeed = 100, 
                                       MaximumSeekerNeed = 200
                                   };

            InsertScholarship(scholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);
            Assert.IsTrue(foundScholarship.FundingProfile.Need.Fafsa);
            Assert.IsTrue(foundScholarship.FundingProfile.Need.UserDerived);

            Assert.AreEqual(100, (double)foundScholarship.FundingProfile.Need.MinimumSeekerNeed, 0.001);
            Assert.AreEqual(200, (double)foundScholarship.FundingProfile.Need.MaximumSeekerNeed, 0.001);
        }

        [Test]
        public void Can_create_scholarship_with_fundingparameters()
        {
            Scholarship scholarship = CreateTestObject();
            scholarship.FundingProfile.FundingParameters = new FundingParameters
                    {
                        AnnualSupportAmount = 100,
                        MinimumNumberOfAwards = 1,
                        MaximumNumberOfAwards = 5
                    };

            InsertScholarship(scholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);

            Assert.AreEqual(100, (double)foundScholarship.FundingProfile.FundingParameters.AnnualSupportAmount, 0.001);
            Assert.AreEqual(1, foundScholarship.FundingProfile.FundingParameters.MinimumNumberOfAwards);
            Assert.AreEqual(5, foundScholarship.FundingProfile.FundingParameters.MaximumNumberOfAwards);
        }

        [Test]
        public void Can_create_scholarship_with_supportedsituation()
        {
            Scholarship scholarship = CreateTestObject();
            scholarship.FundingProfile.SupportedSituation = new SupportedSituation
                {
                    Emergency = true,
                    Traditional = true
                };

            InsertScholarship(scholarship);

            var foundScholarship = ScholarshipDAL.FindById(scholarship.Id);

            Assert.IsNotNull(foundScholarship);
            Assert.IsTrue(foundScholarship.FundingProfile.SupportedSituation.Emergency);
            Assert.IsTrue(foundScholarship.FundingProfile.SupportedSituation.Traditional);

        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void find_by_provider_throws_exception_if_null_passed()
        {
            ScholarshipDAL.FindByProvider(null);
        }

        [Test]
        public void find_by_name_and_provider()
        {
            Scholarship newScholarship = CreateTestObject();
            InsertScholarship(newScholarship);
            
            Scholarship scholarship = ScholarshipDAL.FindByNameAndProvider(newScholarship.Name, newScholarship.Provider);
            Assert.IsNotNull(scholarship);

            scholarship = ScholarshipDAL.FindByNameAndProvider("not-existing-name-of-scholarship-in-provider", newScholarship.Provider);
            Assert.IsNull(scholarship);
        }
        [Test]
        public void Find_By_Stage_And_Provider()
        {
            Scholarship newScholarship = CreateTestObject();
            newScholarship.Stage = ScholarshipStages.GeneralInformation;
             
            InsertScholarship(newScholarship);

            Scholarship scholarship = ScholarshipDAL.FindByStageAndProvider(newScholarship.Stage,
                                                                            newScholarship.Provider)[0];
            Assert.IsNotNull(scholarship);
            var nonExistingStage = ScholarshipStages.BuildScholarshipMatchLogic;
            var scholarships = ScholarshipDAL.FindByStageAndProvider(nonExistingStage, newScholarship.Provider);
            Assert.IsTrue( scholarships.Count==0);
        }
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Find_By_Stage_And_Provider_Throws_Exception_If_Null_Passed()
        {
            ScholarshipDAL.FindByStageAndProvider(ScholarshipStages.None, null);
        }

        [Test]
        public void Can_find_by_provider()
        {
            Scholarship newScholarship = CreateTestObject();
            InsertScholarship(newScholarship);
            IList<Scholarship> scholarships = ScholarshipDAL.FindByProvider(provider);
            Assert.AreEqual(1, scholarships.Count);
            AssertScholarshipAreSame(newScholarship, scholarships[0]);
        }

        [Test]
        public void Can_find_by_id()
        {
            Scholarship newScholarship = CreateTestObject();
            InsertScholarship(newScholarship);
            Scholarship retrivedScholarship = ScholarshipDAL.FindById(newScholarship.Id);
            AssertScholarshipAreSame(newScholarship, retrivedScholarship);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void save_provider_throws_exception_if_null_passed()
        {
            ScholarshipDAL.Save(null);
        }

        public void AssertScholarshipAreSame(Scholarship expected, Scholarship actual)
        {
            Assert.AreEqual(expected.Id, actual.Id);
        }

        public Scholarship CreateTestObject()
        {
            var stage = ScholarshipStages.GeneralInformation;
            return CreateTestObject(user, provider, stage);
        }

        public static Scholarship CreateTestObject(User user, Provider provider, ScholarshipStages stage)
        {
            var result = new Scholarship
                             {
                                 Name = "Test Scholarship 1",
                                 MissionStatement = @"Long big text to describe mission",
                                 Schedule = CreateScheduleByMonthDay(),
                                 Provider = provider,
                                 LastUpdate = new ActivityStamp(user),
                                 Stage = stage
                             };
            return result;
        }


        public Scholarship InsertScholarship(Scholarship scholarship)
        {
            return ScholarshipDAL.Save(scholarship);
        }

        public static ScholarshipScheduleBase CreateScheduleByMonthDay()
        {
            return new ScholarshipScheduleByMonthDay
            {
                StartFrom = new ByMonthDay
                {
                    Month = MonthOfYear.January,
                    DayOfMonth = DaysOfMonth.First
                },
                DueOn = new ByMonthDay
                {
                    Month = MonthOfYear.January,
                    DayOfMonth = DaysOfMonth.First
                },
                AwardOn = new ByMonthDay
                {
                    Month = MonthOfYear.January,
                    DayOfMonth = DaysOfMonth.First
                },

            };
        }

        public static ScholarshipScheduleBase CreateScheduleByWeekDay()
        {
            return new ScholarshipScheduleByWeekDay
            {
                StartFrom = new ByWeekDay
                {
                    Month = MonthOfYear.January,
                    DayOfWeek = DayOfWeek.Monday,
                    WeekDayOccurrence = WeekDayOccurrencesInMonth.First
                },
                DueOn = new ByWeekDay
                {
                    Month = MonthOfYear.January,
                    DayOfWeek = DayOfWeek.Monday,
                    WeekDayOccurrence = WeekDayOccurrencesInMonth.First
                },
                AwardOn = new ByWeekDay
                {
                    Month = MonthOfYear.January,
                    DayOfWeek = DayOfWeek.Monday,
                    WeekDayOccurrence = WeekDayOccurrencesInMonth.First
                },

            };
        }

    }
}
