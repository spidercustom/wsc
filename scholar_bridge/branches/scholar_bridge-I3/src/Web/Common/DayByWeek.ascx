﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DayByWeek.ascx.cs" Inherits="ScholarBridge.Web.Common.DayByWeek" %>
<asp:DropDownList id="WeekDayOccurrenceControl" runat="server">
  <asp:ListItem Value="0">First</asp:ListItem>
  <asp:ListItem Value="1">Second</asp:ListItem>
  <asp:ListItem Value="2">Third</asp:ListItem>
  <asp:ListItem Value="3">Fourth</asp:ListItem>
  <asp:ListItem Value="4">Last</asp:ListItem>
</asp:DropDownList>
<asp:DropDownList ID="DayOfWeekControl" runat="server" >
  <asp:ListItem Value="0">Sunday</asp:ListItem>
  <asp:ListItem Value="1">Monday</asp:ListItem>
  <asp:ListItem Value="2">Tuesday</asp:ListItem>
  <asp:ListItem Value="3">Wednesday</asp:ListItem>
  <asp:ListItem Value="4">Thursday</asp:ListItem>
  <asp:ListItem Value="5">Friday</asp:ListItem>
  <asp:ListItem Value="6">Saturday</asp:ListItem>
</asp:DropDownList>
<span>&nbsp;of&nbsp;</span>
<asp:DropDownList ID="MonthControl" runat="server">
  <asp:ListItem Value="0">January</asp:ListItem>
  <asp:ListItem Value="1">February</asp:ListItem>
  <asp:ListItem Value="2">March</asp:ListItem>
  <asp:ListItem Value="3">April</asp:ListItem>
  <asp:ListItem Value="4">May</asp:ListItem>
  <asp:ListItem Value="5">June</asp:ListItem>
  <asp:ListItem Value="6">July</asp:ListItem>
  <asp:ListItem Value="7">August</asp:ListItem>
  <asp:ListItem Value="8">September</asp:ListItem>
  <asp:ListItem Value="9">October</asp:ListItem>
  <asp:ListItem Value="10">November</asp:ListItem>
  <asp:ListItem Value="11">December</asp:ListItem>
</asp:DropDownList>