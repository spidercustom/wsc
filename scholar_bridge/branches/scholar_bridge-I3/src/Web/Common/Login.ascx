﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ScholarBridge.Web.Common.Login" %>
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login ID="Login1" runat="server" UserNameLabelText="Email Address:" 
            PasswordRecoveryUrl="~/ForgotPassword.aspx" 
            PasswordRecoveryText="Forgot Your Password?" 
            RememberMeText="Remember me?" 
            TitleTextStyle-CssClass="loginTitleText" onloggedin="Login1_LoggedIn" />
    </AnonymousTemplate>    
</asp:LoginView>