﻿using System;
using System.Web.UI;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class AdditionalCriteriaShow : UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks();

            if (!Page.IsPostBack)
            {
                AdditionalRequirementsRepeater.DataSource = ScholarshipToView.AdditionalRequirements;
                AdditionalRequirementsRepeater.DataBind();
                RequirementsFooterRow.Visible = ScholarshipToView.AdditionalRequirements.Count <= 0;
                AdditionalCriteriaRepeater.DataSource = ScholarshipToView.AdditionalQuestions;
                AdditionalCriteriaRepeater.DataBind();
            }
        }

        private void SetupEditLinks()
        {
            if (String.IsNullOrEmpty(LinkTo))
            {
                linkarea1.Visible = false;
                linkarea2.Visible = false;
            }
            else
            {
                string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                              (int)ScholarshipStages.AdditionalCriteria);
                linkBottom.NavigateUrl = navurl;
                linkTop.NavigateUrl = navurl;
            }
            linkarea1.Visible = ScholarshipToView.CanEdit();
            linkarea2.Visible = ScholarshipToView.CanEdit();
        }
    }
}