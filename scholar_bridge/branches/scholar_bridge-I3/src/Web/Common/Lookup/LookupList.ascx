﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LookupList.ascx.cs" Inherits="ScholarBridge.Web.Common.Lookup.LookupList" %>
<link href="/styles/lookuplist.css" rel="stylesheet" type="text/css" />

<div id="lookuplist_holder">
    <div id="lookuplist_header">
        <p>Select single item using click or multiple items either using ctrl+click or mouse drag/lasso.</p>
    </div>
    <div id="lookuplist_left" >
        <h1>Available</h1>
        <div id="lookuplist_search" >
            <asp:TextBox CssClass="text ui-widget-content ui-corner-all" runat="server" ID="txtSearch"  Height="18px" Width="200px"></asp:TextBox>
            <asp:HyperLink  runat="server" NavigateUrl="#" ID="btnSearch"><img src="/images/lookupdialog/magnifier.png" Alt="Go"  /></asp:HyperLink>
        </div>
        <div id="lookuplist_available_container">
            <asp:Panel  id="lookuplist_available" runat="server"> 
            </asp:Panel>
        </div>
            <asp:Panel  id="pager" runat="server" CssClass="jqpager"></asp:Panel>
    </div>
    
    <div id="lookuplist_right">
        <h1>Selections</h1>
                <asp:Panel  id="lookuplist_selections" runat="server"> 
                </asp:Panel>
     </div>
   
    
    <div id="lookuplist_middle">
        <table>
            <tr><td> <asp:button id="btnAdd" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="Add>" Width="100px" Height="23px" CausesValidation="False" />
            </td></tr>
            <tr><td><asp:button id="btnRemove" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="<Remove" Width="100px" Height="23px" CausesValidation="False"  />
            </td></tr>
            <tr><td><asp:button id="btnRemoveAll" CssClass="ui-button ui-state-default ui-corner-all" runat="server" text="<<Remove All" Width="100px" Height="23px" CausesValidation="False"  />
            </td></tr>
        </table>
    </div>
</div> <%--End of holder--%>

  

