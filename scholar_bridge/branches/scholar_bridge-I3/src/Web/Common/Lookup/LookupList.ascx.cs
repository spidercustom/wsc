﻿using System;
using System.Web.UI;
using JSHelper = ScholarBridge.Web.Common.Lookup.LookupDialogJSHelper;
namespace ScholarBridge.Web.Common.Lookup
{
    public partial class LookupList : UserControl
    {
        public string HiddenIds { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //var selectionId = txtSelection.ClientID;
            var gridId = FindControl("lookuplist_available").ClientID;

            btnAdd.Attributes.Add("onclick", JSHelper.BuildaddToSelectionsJS(HiddenIds));
            btnRemove.Attributes.Add("onclick", JSHelper.BuildremoveFromSelectionsJS(false));
            btnRemoveAll.Attributes.Add("onclick", JSHelper.BuildremoveFromSelectionsJS(true));
        }
    }
}
