﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class SeekerInfoShow : UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks();

            var seeker = ScholarshipToView.SeekerMatchCriteria;

            MapSeekerPersonality(seeker);
            MapSeekerDemographics(seeker);
            MapSeekerInterests(seeker);
            MapSeekerActivities(seeker);
            MapSeekerPerformace(seeker);
        }

        private void MapSeekerPerformace(SeekerMatchCriteria seeker)
        {
            if (seeker.GPA != null)
                lblGPA.Text = string.Format("{0} - {1}", seeker.GPA.Minimum, seeker.GPA.Maximum);

            if (seeker.SATScore != null)
            {
                string satscore = string.Format("Critical Reading : {0} - {1}",
                                                seeker.SATScore.CriticalReading.Minimum,
                                                seeker.SATScore.CriticalReading.Maximum);
                satscore += string.Format("Writing : {0} - {1}", seeker.SATScore.Writing.Minimum,
                                          seeker.SATScore.Writing.Maximum);
                satscore += string.Format("Mathematics : {0} - {1}", seeker.SATScore.Mathematics.Minimum,
                                          seeker.SATScore.Mathematics.Maximum);
                lblSATScore.Text = satscore;
            }

            if (seeker.ACTScore != null)
            {
                string actscore = string.Format("Reading : {0} - {1}",
                                                seeker.ACTScore.Reading.Minimum,
                                                seeker.ACTScore.Reading.Maximum);

                actscore += string.Format("Mathematics : {0} - {1}",
                                          seeker.ACTScore.Mathematics.Minimum,
                                          seeker.ACTScore.Mathematics.Maximum);
                actscore += string.Format("English: {0} - {1}",
                                          seeker.ACTScore.English.Minimum,
                                          seeker.ACTScore.English.Maximum);
                actscore += string.Format("Science: {0} - {1}",
                                          seeker.ACTScore.Science.Minimum,
                                          seeker.ACTScore.Science.Maximum);
                lblACTScore.Text = actscore;
            }

            if (seeker.ClassRank != null)
                lblClassRank.Text = string.Format("{0} - {1}", seeker.ClassRank.Minimum, seeker.ClassRank.Maximum);


            lblAPCredits.Text = seeker.APCreditsEarned.ToString();
            lblIBCredits.Text = seeker.IBCreditsEarned.ToString();
            lblHonors.Text = seeker.Honors.ToString();
        }

        private void MapSeekerActivities(SeekerMatchCriteria seeker)
        {
            MapIf(lblHobbies, seeker.Hobbies);
            MapIf(lblSports, seeker.Sports);
            MapIf(lblClubs, seeker.Clubs);
            MapIf(lblServiceHours, seeker.ServiceHours);
        }

        private void MapSeekerInterests(SeekerMatchCriteria seeker)
        {
            MapIf(lblAcademicAreas, seeker.AcademicAreas);
            MapIf(lblCareers, seeker.Careers);
            MapIf(lblCauses, seeker.CommunityInvolvementCauses);
            MapIf(lblOrganizations, seeker.Organizations);
            MapIf(lblAffiliationTypes, seeker.AffiliationTypes);
        }

        private void MapSeekerDemographics(SeekerMatchCriteria seeker)
        {
            MapIf(lblEthinicity, seeker.Ethnicities);
            MapIf(lblReligion, seeker.Religions);
            MapIf(lblCounty, seeker.Counties);
            MapIf(lblCity, seeker.Cities);
            MapIf(lblSchoolDistrict, seeker.SchoolDistricts);
            MapIf(lblHighSchool, seeker.HighSchools);

            lblGender.Text = seeker.Genders.GetDisplayName();
        }

        private void MapSeekerPersonality(SeekerMatchCriteria seeker)
        {
            MapIf(lblFiveWords, seeker.Words);
            MapIf(lblFiveSkills, seeker.Skills);
            MapIf(lblColleges, seeker.Colleges);

            lblStudentGroups.Text = seeker.StudentGroups.GetDisplayName();
            lblSchoolTypes.Text = seeker.SchoolTypes.GetDisplayName();
            lblAcademicPrograms.Text = seeker.AcademicPrograms.GetDisplayName();
            lblSeekerStatuses.Text = seeker.SeekerStatuses.GetDisplayName();
            lblLengthOfProgram.Text = seeker.ProgramLengths.GetDisplayName();
            lblFirstGeneration.Text = seeker.FirstGeneration.ToString();
        }

        private static void MapIf<T>(ITextControl lbl, IList<T> items) where T : ILookup
        {
            if (null != items && items.Count > 0)
                lbl.Text = items.CommaSeparatedNames();
        }

        private void SetupEditLinks()
        {
            if (String.IsNullOrEmpty(LinkTo))
            {
                linkarea1.Visible = false;
                linkarea2.Visible = false;
            }
            else
            {
                string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                              (int)ScholarshipStages.SeekerProfile);
                linkBottom.NavigateUrl = navurl;
                linkTop.NavigateUrl = navurl;
            }
            linkarea1.Visible = ScholarshipToView.CanEdit();
            linkarea2.Visible = ScholarshipToView.CanEdit();
        }
    }
}