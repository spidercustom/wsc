﻿using System;
using System.Text;
using System.Web.UI;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Common
{
    public partial class GeneralInfoShow : UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //setup links
            SetupEditLinks();

            lblName.Text = ScholarshipToView.DisplayName;
            lblMission.Text = ScholarshipToView.MissionStatement;
            lblAppStart.Text = ScholarshipToView.Schedule.StartFromDisplayString;
            lblAppDue.Text = ScholarshipToView.Schedule.DueOnDisplayString;
            lblAwardOn.Text = ScholarshipToView.Schedule.AwardOnDisplayString;

            //var schedule=Domain.ScholarshipParts.
            lblAwardAmount.Text = string.Format("{0} - {1}", ScholarshipToView.MinimumAmount.ToString("c" ),
                                                ScholarshipToView.MaximumAmount.ToString("c"));
            //view donor info
            lblDonor.Text = GetDonorDisplayString(ScholarshipToView.Donor);
        }

        private static string GetDonorDisplayString(ScholarshipDonor donor)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("{0}, {1}", donor.Name, donor.Address);
            
            if (null != donor.Phone && !string.IsNullOrEmpty(donor.Phone.Number))
                sb.AppendFormat(" Phone : {0}", donor.Phone.Number);

            if (!string.IsNullOrEmpty(donor.Email))
                sb.AppendFormat(" Email : {0}", donor.Email);            
            return sb.ToString();
        }

        private void SetupEditLinks()
        {
            if (String.IsNullOrEmpty(LinkTo))
            {
                linkarea1.Visible = false;
                linkarea2.Visible = false;
            }
            else
            {
                string navurl = string.Format("{0}?sid={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id,
                                              (int)ScholarshipStages.GeneralInformation);
                linkBottom.NavigateUrl = navurl;
                linkTop.NavigateUrl = navurl;
            }
            linkarea1.Visible = ScholarshipToView.CanEdit();
            linkarea2.Visible = ScholarshipToView.CanEdit();
        }
    }
}
