﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerInfoShow" %>

<div id="recordinfo">
<h4>Seeker Profile</h4>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
<table class="viewonlyTable">

      <tbody>
        <tr>
        <td>
            <table>
                <thead><tr><th>Seeker's Personality</th></tr></thead>
                <tbody>
                    <tr>
                        <th>5 Words</th>
                        <td><asp:Label  ID="lblFiveWords" runat="server"  /></td>
                    </tr>
                    <tr>
                        <th  scope="row">5 Skills</th>
                        <td><asp:Label ID="lblFiveSkills"  runat="server"  /></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
       
        <tr>
        <td>
            <table>
                <thead><tr><th>Seeker Student Type</th></tr></thead>
                <tbody>
                    <tr>
                        <th scope="row">Student Groups</th>
                        <td><asp:Label ID="lblStudentGroups"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">School Types</th>
                        <td><asp:Label ID="lblSchoolTypes"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Academic Programs</th>
                        <td><asp:Label ID="lblAcademicPrograms"  runat="server"  /></td>
                    </tr>
                     <tr>
                        <th scope="row">Enrollment Statuses</th>
                        <td><asp:Label ID="lblSeekerStatuses"  runat="server"  /></td>
                    </tr>
                     <tr>
                        <th scope="row">Length of Program</th>
                        <td><asp:Label ID="lblLengthOfProgram"  runat="server"  /></td>
                    </tr>
                     <tr>
                        <th scope="row">Colleges</th>
                        <td><asp:Label ID="lblColleges"  runat="server"  /></td>
                    </tr>
                     <tr>
                        <th scope="row">First Generation</th>
                        <td><asp:Label ID="lblFirstGeneration"  runat="server"  /></td>
                    </tr>
         
                </tbody>
            </table>
            </td>
        </tr>        
        
        <tr>
        <td>
            <table>
                <thead><tr><th>Seeker Demographics</th></tr></thead>
                <tbody>
                     <tr>
                        <th scope="row">Ethinicity</th>
                        <td><asp:Label ID="lblEthinicity"  runat="server"  /></td>
                    </tr>
                     <tr>
                        <th scope="row">Religion</th>
                        <td><asp:Label ID="lblReligion"  runat="server"  /></td>
                    </tr>
                     <tr>
                        <th scope="row">Genders</th>
                        <td><asp:Label ID="lblGender"  runat="server"  /></td>
                    </tr>
                     <tr>
                        <th scope="row">State</th>
                        <td><asp:Label ID="lblState"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">County</th>
                        <td><asp:Label ID="lblCounty"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">City</th>
                        <td><asp:Label ID="lblCity"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">School District</th>
                        <td><asp:Label ID="lblSchoolDistrict"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">High School</th>
                        <td><asp:Label ID="lblHighSchool"  runat="server"  /></td>
                    </tr>                
                </tbody>
            </table>
            </td>
        </tr>        
       
       <tr>
        <td>
            <table>
                <thead><tr><th>Seeker Interests</th></tr></thead>
                <tbody>
                     <tr>  
                        <th scope="row">Academic Areas</th>
                        <td><asp:Label ID="lblAcademicAreas"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Careers</th>
                        <td><asp:Label ID="lblCareers"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Causes/Community Involvement</th>
                        <td><asp:Label ID="lblCauses"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Organizations</th>
                        <td><asp:Label ID="lblOrganizations"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Affiliation types</th>
                        <td><asp:Label ID="lblAffiliationTypes"  runat="server"  /></td>
                    </tr>                
                </tbody>
            </table>
            </td>
        </tr>          

       <tr>
        <td>
            <table>
                <thead><tr><th>Seeker Activities</th></tr> </thead>
                <tbody>
                    <tr>
                        <th scope="row">Hobbies</th>
                        <td><asp:Label ID="lblHobbies"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Sports</th>
                        <td><asp:Label ID="lblSports"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Clubs</th>
                        <td><asp:Label ID="lblClubs"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Work Type</th>
                        <td><asp:Label ID="lblWorkType"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Work Hours</th>
                        <td><asp:Label ID="lblWorkHours"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Service Type</th>
                        <td><asp:Label ID="lblServiceType"  runat="server"  /></td>
                    </tr>
                    <tr>
                        <th scope="row">Service Hours</th>
                        <td><asp:Label ID="lblServiceHours"  runat="server"  /></td>
                    </tr>                
                </tbody>
            </table>
            </td>
        </tr>  
        
        <tr>
        <td>
            <table>
                <thead><tr><th>Seeker Student Performance</th></tr> </thead>
                <tbody>
                    <tr>
                        
                        <td>
                        <table>
                
                                <tbody>
                                    <tr>
                                        <th scope="row">GPA</th>
                                        <td><asp:Label ID="lblGPA"  runat="server"  /></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">SAT Score</th>
                                        <td><asp:Label ID="lblSATScore"  runat="server"  /></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">ACT Score</th>
                                        <td><asp:Label ID="lblACTScore"  runat="server"  /></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Class Rank</th>
                                        <td><asp:Label ID="lblClassRank"  runat="server"  /></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">AP Credits Earned</th>
                                        <td><asp:Label ID="lblAPCredits"  runat="server"  /></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">IB Credits Earned</th>
                                        <td><asp:Label ID="lblIBCredits"  runat="server"  /></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Honors?</th>
                                        <td><asp:Label ID="lblHonors"  runat="server"  /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>          
      </tbody>
</table>
</div>
<div id="linkarea2" runat="server" class="linkarea">
 <p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>
