﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SentMessageList.ascx.cs" Inherits="ScholarBridge.Web.Common.SentMessageList" %>
<asp:Repeater ID="messageList" runat="server" 
    onitemdatabound="messageList_ItemDataBound">
    <HeaderTemplate>
    <table class="sortableTable">
        <thead>
            <tr>
                <th>Subject</th>
                <th>To</th>
                <th>From</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="row">
            <td><asp:HyperLink id="linkToMessage" runat="server"><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:HyperLink></td>
            <td><%# DataBinder.Eval(Container.DataItem, "To")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "From") %></td>
            <td><%# DataBinder.Eval(Container.DataItem, "Date", "{0:MM/dd/yyyy}")%></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="altrow">
            <td><asp:HyperLink id="linkToMessage" runat="server"><%# DataBinder.Eval(Container.DataItem, "Subject")%></asp:HyperLink></td>
            <td><%# DataBinder.Eval(Container.DataItem, "To")%></td>
            <td><%# DataBinder.Eval(Container.DataItem, "From") %></td>
            <td><%# DataBinder.Eval(Container.DataItem, "Date", "{0:MM/dd/yyyy}")%></td>
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
        </tbody>
    </table>
    </FooterTemplate>
</asp:Repeater>