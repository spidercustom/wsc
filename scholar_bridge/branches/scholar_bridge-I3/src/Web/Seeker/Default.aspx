﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
     Inherits="ScholarBridge.Web.Seeker.Default"  Title="Intermediary" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <p>
        This will be the home page for a Seeker. It will contain the dashboard elements
        based on their organization
    </p>
    <sb:Login ID="loginForm" runat="server" />
    
    <asp:HyperLink ID="registerLnk" runat="server" NavigateUrl="~/Seeker/Register.aspx">Register as a Scholarship Seeker</asp:HyperLink>
    <asp:HyperLink ID="seekerTCLnk" runat="server" NavigateUrl="~/Seeker/Terms.aspx">Seeker Terms &amp; Conditions</asp:HyperLink>
</asp:Content>
