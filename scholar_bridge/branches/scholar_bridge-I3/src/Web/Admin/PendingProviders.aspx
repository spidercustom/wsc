﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="PendingProviders.aspx.cs" Inherits="ScholarBridge.Web.Admin.PendingProviders" Title="Admin | Pending Providers" %>

<%@ Register TagPrefix="sb" TagName="PendingOrgs" Src="~/Admin/PendingOrgs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <h3>Scholarship Provider Pending Approval</h3>
    <sb:PendingOrgs id="pendingProviders" runat="server" LinkTo="~/Admin/ApproveProvider.aspx" />

</asp:Content>

