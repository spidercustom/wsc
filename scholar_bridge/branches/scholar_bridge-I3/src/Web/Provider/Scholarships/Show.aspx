﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.Show" %>
<%@ Register src="~/Common/ShowScholarship.ascx" tagname="ShowScholarship" tagprefix="sb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div class="print">&nbsp;</div>

<h4>View Scholarship : <asp:Label ID="lblName" runat="server" Text="Label" /></h4>
<div id="linkarea">
<p>[<asp:HyperLink ID="linkCopy" runat="server">Copy</asp:HyperLink>]</p> 
</div>

<sb:ShowScholarship id="showScholarship" runat="server" LinkTo="~/Provider/BuildScholarship/Default.aspx" />

<asp:Button ID="deleteBtn" runat="server" Text="Delete Scholarship" CausesValidation="false"
        onclick="deleteBtn_Click" />

<asp:Button ID="requestActivationBtn" runat="server" Text="Request Activation" CausesValidation="false"
        onclick="requestActivationBtn_Click" />

</asp:Content>