﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeSelectionControl.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.AttributeSelectionControl" %>
<asp:Repeater ID="AttributesControl" runat="server">
  <HeaderTemplate>
    <table>
      <thead>
        <tr>
          <th>Minimum</th>
          <th>Preference</th>
          <th>Not Used</th>
          <th>Attribute</th>
        </tr>
      </thead>
      <tbody>
  </HeaderTemplate>
  <ItemTemplate>
    <tr class="row">
        <td align="center"><asp:RadioButton ID="Minimum" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="Preference" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="NotUsed" runat="server" GroupName="UsageSelector" /></td>
      <td>
        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Value") %>' runat='server' ID='attributeNameLabel'></asp:Label>
        <input type="hidden" runat="server" id="KeyControl" value='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />
      </td>
    </tr>
  </ItemTemplate>
  <AlternatingItemTemplate>
    <tr class="altrow" style="background-color: #eeeeee">
        <td align="center"><asp:RadioButton ID="Minimum" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="Preference" runat="server" GroupName="UsageSelector" /></td>
        <td align="center"><asp:RadioButton ID="NotUsed" runat="server" GroupName="UsageSelector" /></td>
      <td>
        <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Value") %>' runat='server' ID='attributeNameLabel'></asp:Label>
        <input type="hidden" runat="server" id="KeyControl" value='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />
      </td>

    </tr>
  </AlternatingItemTemplate>
  <FooterTemplate>
      </tbody>
    </table>
  </FooterTemplate>
</asp:Repeater>