﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class AttributeSelectionControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public Repeater RepeaterControl
        {
            get { return AttributesControl; }
        }

        public void ForEachRepeaterItem(Action<RadioButtonList, string> action)
        {
            foreach (RepeaterItem repeaterItem in RepeaterControl.Items)
            {
				var usageTypeControl = (RadioButtonList)repeaterItem.FindControl("AttributeUsageControl");
				var attributeControl = (HtmlInputHidden)repeaterItem.FindControl("KeyControl");
                var attribute = attributeControl.Value;

                action(usageTypeControl, attribute);
            }
        }

		public void ForEachRepeaterItem(Action<RadioButtonList, Int32> action)
		{
			ForEachRepeaterItem((radioControl, stringValue) => action(radioControl, Int32.Parse(stringValue)));
		}

		public void ForEachRepeaterItem(Action<AttributeUsageButtonGroup> action)
		{
			foreach (RepeaterItem repeaterItem in RepeaterControl.Items)
			{
				var usageMinimumRB = (RadioButton)repeaterItem.FindControl("Minimum");
				var usagePreferenceRB = (RadioButton)repeaterItem.FindControl("Preference");
				var usageNotUsedRB = (RadioButton)repeaterItem.FindControl("NotUsed");
				var attributeControl = (HtmlInputHidden)repeaterItem.FindControl("KeyControl");
                var attributeEnumID = int.Parse(attributeControl.Value);
				var buttonGroup = new AttributeUsageButtonGroup(attributeEnumID, usageMinimumRB, usagePreferenceRB, usageNotUsedRB);

				action(buttonGroup);
			}
		}

		public class AttributeUsageButtonGroup
		{
			RadioButton Minimum
			{
				get;
				set;
			}
			RadioButton Preference
			{
				get;
				set;
			}
			RadioButton NotUsed
			{
				get;
				set;
			}
			public int AttributeEnumInt
			{
				get; private set;
			}
			public ScholarshipAttributeUsageType Value
			{
				get
				{
					if (Minimum.Checked)
						return ScholarshipAttributeUsageType.Minimum;
					if (Preference.Checked)
						return ScholarshipAttributeUsageType.Preference;
					return ScholarshipAttributeUsageType.NotUsed;
				}
				set
				{
					Minimum.Checked = Preference.Checked = NotUsed.Checked = false;
					switch (value)
					{
						case ScholarshipAttributeUsageType.Minimum:
							Minimum.Checked = true;
							break;
						case ScholarshipAttributeUsageType.Preference:
							Preference.Checked = true;
							break;
						default:
							NotUsed.Checked = true;
							break;
					}
				}
			}
			public AttributeUsageButtonGroup(int attribute, RadioButton minimum, RadioButton preference, RadioButton notUsed)
			{
				AttributeEnumInt = attribute; 
				Minimum = minimum;
				Preference = preference;
				NotUsed = notUsed;
			}
		}

	}
}