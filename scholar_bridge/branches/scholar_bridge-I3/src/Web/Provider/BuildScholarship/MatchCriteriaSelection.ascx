﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchCriteriaSelection.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.MatchCriteriaSelection" %>
<%@ Register TagPrefix="sb" TagName="AttributeSelectionControl" Src="AttributeSelectionControl.ascx" %>
<h3>Build Scholarship – Match Specifications</h3>
<h4><asp:Label ID="scholarshipNameLbl" runat="server" /></h4>
<sb:AttributeSelectionControl id="MatchSpecifications" runat="server" />

<h3>Build Scholarship – Funding Specifications</h3>
<sb:AttributeSelectionControl id="FundingSpecifications" runat="server" />
