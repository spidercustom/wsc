﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdditionalCriteria.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register TagPrefix="sb" TagName="ByMonthControl" Src="~/Common/DayByMonth.ascx" %>
<%@ Register TagPrefix="sb" TagName="ByWeekControl" Src="~/Common/DayByWeek.ascx" %>

<h3>Build Scholarship – Criteria</h3>
<h4><asp:Label ID="scholarshipNameLbl" runat="server" /></h4>

<br /><br />
<p>
Place keeper for text to be defined by WSC explaining the +Criteria.  Each question can be 200 characters maximum
</p>
<br />
<fieldset>
<h5>Additional Selection Processes Needed</h5>
<label for="AdditionalRequirements">Types of Support:</label>
<asp:CheckBoxList ID="AdditionalRequirements" runat="server" TextAlign="Left"/>
</fieldset>
<br />
<table width="90%">
<tr>
<td>
<asp:GridView ID="questionsGrid" runat="server"  ShowFooter="true" 
    DataSourceID="questionsDataSource" AutoGenerateColumns="False" 
        onrowupdated="questionsGrid_RowUpdated">
        <EmptyDataTemplate>
            <table>
                <tr>
                    <th>
                        Scholarship Specific Questions
                    </th>
                    <th>
                        &nbsp;
                    </th>
                
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td>
                        <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine" ID="questionEmptyAddTextBox" runat="server"></asp:TextBox>
                    
                    </td>
                    <td>
                        <asp:LinkButton ID="addEmptyQuestionButton" runat="server" 
                                Text="Add" onclick="addEmptyQuestionButton_Click" />
                    </td>
                
                </tr>
            </table>

        </EmptyDataTemplate>
    <Columns>
        <asp:TemplateField HeaderText="#">
            <EditItemTemplate>
                <asp:Label ID="displayOrderEditLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>'></asp:Label>
                <asp:HiddenField runat="server" ID="questionOrderHidden" Value='<%# Bind("DisplayOrder") %>' />
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="displayOrderLabel" runat="server" Text='<%# string.Format("{0}.", Eval("DisplayOrder"))%>'></asp:Label><br />&nbsp;
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Scholarship Specific Questions">
            <EditItemTemplate>
                <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine" ID="questionTextBox" runat="server" Text='<%# Bind("QuestionText") %>'></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="questionTextLabel" runat="server" Text='<%# Bind("QuestionText") %>'></asp:Label><br />&nbsp;
            </ItemTemplate>
            <FooterTemplate>
                &nbsp;<br />
                <asp:TextBox Width="450" MaxLength="200" Columns="100" Wrap="true" Rows="2" TextMode="MultiLine" ID="questionAddTextBox" runat="server"></asp:TextBox>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField ShowHeader="False">
            <EditItemTemplate>
                <asp:LinkButton ID="updateQuestionButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="Update"></asp:LinkButton>
                &nbsp;<asp:LinkButton ID="cancelQuestionUpdateButton" runat="server" CausesValidation="False" 
                    CommandName="Cancel" Text="Cancel"></asp:LinkButton>
           </EditItemTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="editQuestionButton" runat="server" CausesValidation="False" 
                    CommandName="Edit" Text="Edit"></asp:LinkButton>&nbsp;
            </ItemTemplate>
            <FooterTemplate>
                &nbsp;<br />
                    <asp:LinkButton ID="addQuestionButton" runat="server" 
            Text="Add" onclick="addQuestionButton_Click" />
            </FooterTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</td>
</tr>
</table>
<br />&nbsp;<br />
<asp:ObjectDataSource ID="questionsDataSource" runat="server" 
        UpdateMethod="UpdateQuestion"
        TypeName="ScholarBridge.Web.Provider.BuildScholarship.AdditionalCriteria+QuestionSource"
        onobjectcreating="questionsDataSource_ObjectCreating" SelectMethod="SelectQuestions">
    
</asp:ObjectDataSource>
