﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Wizards;


namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class MatchCriteriaSelection : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }
        
        Scholarship scholarshipInContext;

        protected void Page_Load(object sender, EventArgs e)
        {
            scholarshipInContext = Container.GetDomainObject();
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
            {
                MatchSpecifications.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(typeof (MatchCriteriaAttribute));
                MatchSpecifications.RepeaterControl.DataBind();

                FundingSpecifications.RepeaterControl.DataSource = EnumExtensions.GetKeyValue(typeof(FundingProfileAttribute));
                FundingSpecifications.RepeaterControl.DataBind();

                PopulateScreen();
            }
        }

        private void PopulateScreen()
        {
            PopulateMatchCriteriaAttributesScreen();
            PopulateFundingAttributesScreen();
        }

        private void PopulateMatchCriteriaAttributesScreen()
        {
			MatchSpecifications.
				ForEachRepeaterItem(usageButtonGroup =>
				{
					var attribute = (MatchCriteriaAttribute)usageButtonGroup.AttributeEnumInt;

					var attributeUsage = scholarshipInContext.SeekerMatchCriteria.AttributesUsage.
						FirstOrDefault(o => o.Attribute == attribute);

					usageButtonGroup.Value =
						null == attributeUsage ?
						ScholarshipAttributeUsageType.NotUsed :
						attributeUsage.UsageType;
				});
		}

        private void PopulateFundingAttributesScreen()
        {
            FundingSpecifications.
				ForEachRepeaterItem(usageButtonGroup =>
				                    	{
											var attribute = (FundingProfileAttribute) usageButtonGroup.AttributeEnumInt;

											var attributeUsage = scholarshipInContext.FundingProfile.AttributesUsage.
												FirstOrDefault(o => o.Attribute == attribute);

											usageButtonGroup.Value = 
												null == attributeUsage ? 
												ScholarshipAttributeUsageType.NotUsed : 
												attributeUsage.UsageType;
				                    	});

        }

        public override bool ValidateStep()
        {
            return true;
        }

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        private void PopulateObjects()
        {
            PopulateMatchCriteriaAttributesObject();
            PopulateFundingAttributesObject();
            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage < ScholarshipStages.MatchCriteriaSelection)
                scholarshipInContext.Stage = ScholarshipStages.MatchCriteriaSelection;
        }

        private void PopulateFundingAttributesObject()
        {
            scholarshipInContext.FundingProfile.AttributesUsage.Clear();
            
            foreach (RepeaterItem repeaterItem in FundingSpecifications.RepeaterControl.Items)
            {
            	AttributeSelectionControl.AttributeUsageButtonGroup buttonGroup = BuildButtonGroup(repeaterItem);
				if (!buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
                {

					var attribute = (FundingProfileAttribute)buttonGroup.AttributeEnumInt;
                    var attributeUsage = new FundingProfileAttributeUsage
                                             {
                                                 Attribute = attribute,
												 UsageType = buttonGroup.Value
                                             };
                    scholarshipInContext.FundingProfile.AttributesUsage.Add(attributeUsage);
                }
            }
        }

        private void PopulateMatchCriteriaAttributesObject()
        {
            scholarshipInContext.SeekerMatchCriteria.AttributesUsage.Clear();
			foreach (RepeaterItem repeaterItem in MatchSpecifications.RepeaterControl.Items)
			{
				AttributeSelectionControl.AttributeUsageButtonGroup buttonGroup = BuildButtonGroup(repeaterItem);
				if (!buttonGroup.Value.Equals(ScholarshipAttributeUsageType.NotUsed))
				{

					var attribute = (MatchCriteriaAttribute)buttonGroup.AttributeEnumInt;
					var attributeUsage = new MatchCriteriaAttributeUsage
					{
						Attribute = attribute,
						UsageType = buttonGroup.Value
					};
					scholarshipInContext.SeekerMatchCriteria.AttributesUsage.Add(attributeUsage);
				}
			}
        }

		private static AttributeSelectionControl.AttributeUsageButtonGroup BuildButtonGroup(Control repeaterItem)
		{
			var usageMinimumRB = (RadioButton)repeaterItem.FindControl("Minimum");
			var usagePreferenceRB = (RadioButton)repeaterItem.FindControl("Preference");
			var usageNotUsedRB = (RadioButton)repeaterItem.FindControl("Minimum");
			var attributeControl = (HtmlInputHidden)repeaterItem.FindControl("KeyControl");
			return new AttributeSelectionControl.AttributeUsageButtonGroup(Int32.Parse(attributeControl.Value), usageMinimumRB, usagePreferenceRB, usageNotUsedRB);
		}

		public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.MatchCriteriaSelection;
        }

        public override int StepIndex
        {
            get { return (int)ScholarshipStages.MatchCriteriaSelection; }
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.MatchCriteriaSelection;
        }
    }
}