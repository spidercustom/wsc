﻿using System;
using System.Collections;
using System.Globalization;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Business;

namespace ScholarBridge.Web.Provider.BuildScholarship
{
    public partial class FundingProfile : WizardStepUserControlBase<Scholarship>
    {
        public IScholarshipService ScholarshipService { get; set; }
        public IUserContext UserContext { get; set; }

        public ISupportDAL SupportDAL { get; set; }
        public INeedGapDAL NeedGapDAL { get; set; }
        public ITermOfSupportDAL TermOfSupportDAL { get; set; }

		private Scholarship _scholarshipInContext;
    	private Scholarship scholarshipInContext
    	{
    		get
    		{
    			if (_scholarshipInContext == null)
    				_scholarshipInContext = Container.GetDomainObject();
    			return _scholarshipInContext;
    		}
    	}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (scholarshipInContext == null)
                throw new InvalidOperationException("There is no scholarship in context");

            if (!IsPostBack)
                PopulateScreen();
        }

        public override void Activated()
        {
            base.Activated();
            SetControlsVisibility();
        }

        private void SetControlsVisibility()
        {
            NeedContainerControl.Visible =
                scholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.Need);
            SupportSituationContainerControl.Visible =
                scholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.SupportedSituation);
            FundingParametersContainerControl.Visible =
                scholarshipInContext.FundingProfile.HasAttributeUsage(FundingProfileAttribute.FundingParameters);


            NoSelectionContainerControl.Visible = !
                                                  (NeedContainerControl.Visible |
                                                   SupportSituationContainerControl.Visible |
                                                   FundingParametersContainerControl.Visible);
        }

        private void PopulateScreen()
        {
            PopulateCheckBoxes(NeedGaps, NeedGapDAL.FindAll());
            PopulateCheckBoxes(TypesOfSupport, SupportDAL.FindAll());
            PopulateCheckBoxes(TermsOfSupport, TermOfSupportDAL.FindAll());

            scholarshipNameLbl.Text = scholarshipInContext.Name;

            if (null != scholarshipInContext.FundingProfile.Need)
            {
                Fafsa.Checked = scholarshipInContext.FundingProfile.Need.Fafsa;
                UserDerived.Checked = scholarshipInContext.FundingProfile.Need.UserDerived;
                MinimumSeekerNeed.Text = scholarshipInContext.FundingProfile.Need.MinimumSeekerNeed.ToString();
                MaximumSeekerNeed.Text = scholarshipInContext.FundingProfile.Need.MaximumSeekerNeed.ToString();
                NeedGaps.Items.SelectItems(scholarshipInContext.FundingProfile.Need.NeedGaps, ng => ng.Id.ToString());
            }

            if (null != scholarshipInContext.FundingProfile.SupportedSituation)
            {
                Emergency.Checked = scholarshipInContext.FundingProfile.SupportedSituation.Emergency;
                Traditional.Checked = scholarshipInContext.FundingProfile.SupportedSituation.Traditional;

                TypesOfSupport.Items.SelectItems(scholarshipInContext.FundingProfile.SupportedSituation.TypesOfSupport,
                                                 ts => ts.Id.ToString());
            }

            if (null != scholarshipInContext.FundingProfile.FundingParameters)
            {
                AnnualSupportAmount.Text = scholarshipInContext.FundingProfile.FundingParameters.AnnualSupportAmount.ToString();
                MinimumNumberOfAwards.Text = scholarshipInContext.FundingProfile.FundingParameters.MinimumNumberOfAwards.ToString();
                MaximumNumberOfAwards.Text = scholarshipInContext.FundingProfile.FundingParameters.MaximumNumberOfAwards.ToString();
                TermsOfSupport.Items.SelectItems(scholarshipInContext.FundingProfile.FundingParameters.TermsOfSupport, ts => ts.Id.ToString());
            }
        }

        private void PopulateObjects()
        {
            // Need
            if (null == scholarshipInContext.FundingProfile.Need)
                scholarshipInContext.FundingProfile.Need = new DefinitionOfNeed();
            scholarshipInContext.FundingProfile.Need.Fafsa = Fafsa.Checked;
            scholarshipInContext.FundingProfile.Need.UserDerived = UserDerived.Checked;
            scholarshipInContext.FundingProfile.Need.MinimumSeekerNeed = GetMoneyValue(MinimumSeekerNeed, MinimumSeekerNeedValidator);
            scholarshipInContext.FundingProfile.Need.MaximumSeekerNeed = GetMoneyValue(MaximumSeekerNeed, MaximumSeekerNeedValidator);

            var selectedNeeds = NeedGaps.Items.SelectedItems(item => (object)item);
            scholarshipInContext.FundingProfile.Need.ResetNeedGaps(NeedGapDAL.FindAll(selectedNeeds));

            // SupportedSituation
            scholarshipInContext.FundingProfile.SupportedSituation.Emergency = Emergency.Checked;
            scholarshipInContext.FundingProfile.SupportedSituation.Traditional = Traditional.Checked;

            var selectedSupport = TypesOfSupport.Items.SelectedItems(item => (object)item);
            scholarshipInContext.FundingProfile.SupportedSituation.ResetTypesOfSupport(SupportDAL.FindAll(selectedSupport));

            // FundingParameters
            var selectedTermsOfSupport = TermsOfSupport.Items.SelectedItems(item => (object)item);
            scholarshipInContext.FundingProfile.FundingParameters.AnnualSupportAmount = GetMoneyValue(AnnualSupportAmount,
                                                                                       AnnualSupportAmountValidator);
            scholarshipInContext.FundingProfile.FundingParameters.MinimumNumberOfAwards = GetIntValue(MinimumNumberOfAwards,
                                                                                       MinimumNumberOfAwardsValidator);
            scholarshipInContext.FundingProfile.FundingParameters.MaximumNumberOfAwards = GetIntValue(MaximumNumberOfAwards,
                                                                                       MaximumNumberOfAwardsValidator);
            scholarshipInContext.FundingProfile.FundingParameters.ResetTermsOfSupport(TermOfSupportDAL.FindAll(selectedTermsOfSupport));

            scholarshipInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
            if (scholarshipInContext.Stage < ScholarshipStages.FundingProfile)
                scholarshipInContext.Stage = ScholarshipStages.FundingProfile;
        }

        protected void BuildMatchCriteriaLinkControl_Click(object sender, EventArgs e)
        {
			Container.GoPrior();
			Container.GoPrior();
		}

        #region IWizardStepControl<Scholarship> Members

        public override void Save()
        {
            PopulateObjects();
            ScholarshipService.Save(scholarshipInContext);
        }

        public override bool WasSuspendedFrom(Scholarship @object)
        {
            return @object.Stage == ScholarshipStages.FundingProfile;
        }

        public override bool CanResume(Scholarship @object)
        {
            return @object.Stage >= ScholarshipStages.FundingProfile;
        }

        public override int StepIndex
        {
			get { return (int)ScholarshipStages.FundingProfile; }
        }

        public override bool ValidateStep()
        {
            return true;
        }

        public decimal GetMoneyValue(TextBox tb, PropertyProxyValidator validator)
        {
            decimal amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (! Decimal.TryParse(tb.Text, NumberStyles.Currency, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public int GetIntValue(TextBox tb, PropertyProxyValidator validator)
        {
            int amounts = 0;
            if (!string.IsNullOrEmpty(tb.Text))
            {
                if (!Int32.TryParse(tb.Text, NumberStyles.Integer | NumberStyles.AllowThousands, CultureInfo.CurrentUICulture, out amounts))
                {
                    validator.IsValid = false;
                    validator.Text = "Invalid currency value";
                }
            }

            return amounts;
        }

        public void PopulateCheckBoxes(CheckBoxList ctl, IEnumerable domainObjects)
        {
            ctl.DataSource = domainObjects;
            ctl.DataTextField = "Name";
            ctl.DataValueField = "Id";
            ctl.DataBind();
        }

        #endregion
    }
}