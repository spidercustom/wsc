﻿using System;
using System.Web;
using Common.Logging;

namespace ScholarBridge.Web
{
    public partial class Error : System.Web.UI.Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Error));

        protected void Page_Load(object sender, EventArgs e)
        {
            var mainEx = HttpContext.Current.Server.GetLastError();
            if (null != mainEx)
            {
                log.Error("Unhandled exception", mainEx);

                var ex = mainEx.GetBaseException();
                errorMessage.InnerText = ex.Message;
            }
        }
    }
}
