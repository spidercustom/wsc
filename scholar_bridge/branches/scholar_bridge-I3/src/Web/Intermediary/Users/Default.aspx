﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Intermediary.Users.Default" Title="Intermediary | Users" %>

<%@ Register TagPrefix="sb" TagName="OrgUserList" Src="~/Common/OrgUserList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<div class="tabs">
    <ul>
        <li><a href="#users-tab"><span>Users</span></a></li>
        <li><a href="#inactive-users-tab"><span>Inactive Users</span></a></li>
    </ul>
    <div id="users-tab">
       <h3>Users</h3>
        <sb:OrgUserList id="orgUserList" runat="server" LinkTo="~/Intermediary/Users/Show.aspx" />
    </div>
    <div id="inactive-users-tab">
        <h3>Inactive Users</h3>
        <sb:OrgUserList id="orgInactiveUserList" runat="server" LinkTo="~/Intermediary/Users/Show.aspx" />
    </div>
</div>
    
<ul class="pageNav">
    <li><asp:HyperLink ID="createUserLnk" runat="server" NavigateUrl="~/Intermediary/Users/Create.aspx">Create User</asp:HyperLink></li>
</ul>
    
</asp:Content>
