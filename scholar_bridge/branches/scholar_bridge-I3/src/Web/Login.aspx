﻿<%@ Page Language="C#" MasterPageFile="~/External.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ScholarBridge.Web.Login" Title="Login" %>

<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

    <sb:Login ID="loginForm" runat="server" />
    
    <asp:HyperLink ID="reconfirmLnk" runat="server" NavigateUrl="~/Reconfirm.aspx">Resend Confirmation Email?</asp:HyperLink>
</asp:Content>
