using System;
using System.Collections.Generic;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Messaging
{
    public class TemplateParametersService : ITemplateParametersService
    {
        private const string RESET = "reset=true";
        public ILinkGenerator LinkGenerator { get; set; }

        public void ConfirmationLink(User user, bool requireResetPassword, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()},
                               {"Link", BuildConfirmationLinkForEmail(user.Email, requireResetPassword)}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void OrganizationApproved(User user, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"Name", (user.Name == null) ? "" : user.Name.ToString()},
                               {"Link", LinkGenerator.GetFullLink("Login.aspx")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void OrganizationRejected(User user, MailTemplateParams templateParams)
        {
            OrganizationApproved(user, templateParams);
        }

        public void RequestOrganizationApproval(User user, Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"FirstName", user.Name.FirstName},
                               {"MiddleName", user.Name.MiddleName},
                               {"LastName", user.Name.LastName},
                               {"Email", user.Email},
                               {"OrganizationName", organization.Name},
                               {"OrganizationTax", organization.TaxId},
                               {"OrganizationWebsite", organization.Website},
                               {"AddressStreet", (organization.Address == null) ? "" : organization.Address.Street},
                               {"AddressStreet2", (organization.Address == null) ? "" : organization.Address.Street2},
                               {"City", (organization.Address == null) ? "" : organization.Address.City},
                               {"State", (organization.Address == null) ? "" : organization.Address.State.Name},
                               {"PostalCode", (organization.Address == null) ? "" : organization.Address.PostalCode},
                               {"Phone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Fax", (organization.Fax == null) ? "" : organization.Fax.FormattedPhoneNumber},
                               {"OtherPhone", (organization.OtherPhone == null) ? "" : organization.OtherPhone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void RequestScholarshipActivation(Scholarship scholarship, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", scholarship.Provider.Name},
                               {"OrganizationTax", scholarship.Provider.TaxId},
                               {"ScholarshipName", scholarship.DisplayName},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void ScholarshipApproved(Scholarship scholarship, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", scholarship.Provider.Name},
                               {"ScholarshipName", scholarship.DisplayName}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void ScholarshipRejected(Scholarship scholarship, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", scholarship.Provider.Name},
                               {"ScholarshipName", scholarship.DisplayName}
                           };
            templateParams.MergeVariablesInto(dict);
        }

        public void RelationshipRequestCreated(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void RelationshipInActivated(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public string BuildConfirmationLinkForEmail(string email, bool requiredToSetPassword)
        {
            var linkData = email;
            if (requiredToSetPassword)
            {
                linkData += "&" + RESET;
            }
            var queryString = String.Format("key={0}", LinkGenerator.UrlEncode(CryptoHelper.Encrypt(linkData)));
            return LinkGenerator.GetFullLink("ConfirmEmail.aspx", queryString);
        }

        public void RelationshipRequestApproved(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                           };

            templateParams.MergeVariablesInto(dict);
        }

        public void RelationshipRequestRejected(Organization organization, MailTemplateParams templateParams)
        {
            var dict = new Dictionary<string, string>
                           {
                               {"OrganizationName", organization.Name},
                               {"AdminFirstName", organization.AdminUser.Name.FirstName},
                               {"AdminMiddleName", organization.AdminUser.Name.MiddleName},
                               {"AdminLastName", organization.AdminUser.Name.LastName},
                               {"AdminEmail", organization.AdminUser.Email},
                               {"OrganizationPhone", (organization.Phone == null) ? "" : organization.Phone.FormattedPhoneNumber},
                               {"Link", LinkGenerator.GetFullLink("Message/Default.aspx")}
                           };

            templateParams.MergeVariablesInto(dict);
        }
    }
}
