using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business.Messaging
{
    public interface ITemplateParametersService
    {
        void ConfirmationLink(User user, bool requireResetPassword, MailTemplateParams templateParams);

        void RequestScholarshipActivation(Scholarship scholarship, MailTemplateParams templateParams);
        void ScholarshipApproved(Scholarship scholarship, MailTemplateParams templateParams);
        void ScholarshipRejected(Scholarship scholarship, MailTemplateParams templateParams);

        void RequestOrganizationApproval(User user, Organization organization, MailTemplateParams templateParams);
        void OrganizationApproved(User user, MailTemplateParams templateParams);
        void OrganizationRejected(User user, MailTemplateParams templateParams);

        void RelationshipRequestCreated(Organization organization, MailTemplateParams templateParams);
        void RelationshipInActivated(Organization organization, MailTemplateParams templateParams);
        void RelationshipRequestApproved(Organization organization, MailTemplateParams templateParams);
        void RelationshipRequestRejected(Organization organization, MailTemplateParams templateParams);
    }
}