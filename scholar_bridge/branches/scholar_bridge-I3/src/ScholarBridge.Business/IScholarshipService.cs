﻿using System.Collections.Generic;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
    public interface IScholarshipService
    {
        Scholarship GetById(int id);
        IList<Scholarship> GetByProvider(Provider provider);
          
        Scholarship Save(Scholarship scholarship);
        void Delete(Scholarship scholarship);

        bool ScholarshipNameExists(string name, Provider provider);
        Scholarship FindByNameAndProvider(string name, Provider provider);

        IList<Scholarship> GetByStageAndProvider(ScholarshipStages scholarshipStage, Provider provider);
        IList<Scholarship> GetNotActivatedScholarshipsByProvider(Provider provider);

        void RequestActivation(Scholarship scholarship);
        void Approve(Scholarship scholarship, User approver);
        void Reject(Scholarship scholarship, User approver);
    }
}
