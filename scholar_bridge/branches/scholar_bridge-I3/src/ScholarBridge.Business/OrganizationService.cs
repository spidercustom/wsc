using System;
using System.Collections.Generic;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public abstract class OrganizationService<T> : IOrganizationService<T> where T : Organization
    {
        public IOrganizationDAL<T> OrganizationDAL { get; set; }
        public IRoleDAL RoleDAL { get; set; }

        public IUserService UserService { get; set; }
        public IMessagingService MessagingService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }

        protected abstract void AddRolesToNewUser(User user, bool isAdmin);

        protected abstract MessageType TemplateForApproval { get; }
        protected abstract MessageType TemplateForRejection { get; }

        public T FindById(int id)
        {
            return OrganizationDAL.FindById(id);
        }

        public void SaveNewWithAdminUser(T organization, User user)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            AddRolesToNewUser(user, true);
            UserService.Update(user);
            organization.AdminUser = user;
            OrganizationDAL.Insert(organization);

            UserService.SendConfirmationEmail(user, false);
        }

        public void SaveNewUser(T organization, User user)
        {
            SaveNewUser(organization, user, false);
        }

        public void SaveNewUser(T organization, User user, bool requireResetPassword)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            AddRolesToNewUser(user, false);
            user.IsApproved = true;
            organization.Users.Add(user);
            OrganizationDAL.Update(organization);

            UserService.SendConfirmationEmail(user, requireResetPassword);
        }

        public void DeleteUser(T organization, User user)
        {
            UserService.Delete(user);
        }

        public void ReactivateUser(T organization, User user)
        {
            user.IsDeleted = false;
            UserService.Update(user);
        }

        public void Update(T organization)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }

            OrganizationDAL.Update(organization);
        }

        public IList<T> FindPendingOrganizations()
        {
            return OrganizationDAL.FindAllPending();
        }

        public void Approve(T organization)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }
            organization.ApprovalStatus = ApprovalStatus.Approved;

            if (null != organization.AdminUser)
            {
                organization.AdminUser.IsApproved = true;
            }
            OrganizationDAL.Update(organization);

            // Send message to admin user that this organization has been approved
            var templateParams = new MailTemplateParams();
            TemplateParametersService.OrganizationApproved(organization.AdminUser, templateParams);
            var msg = new OrganizationMessage
                          {
                              MessageTemplate = TemplateForApproval,
                              To = new MessageAddress {Organization = organization},
                              LastUpdate = new ActivityStamp(organization.AdminUser)
                          };
            MessagingService.SendMessageFromAdmin(msg, templateParams);
        }

        public void Reject(T organization)
        {
            if (null == organization)
            {
                throw new ArgumentNullException("organization", "Provider can not be null");
            }
            organization.ApprovalStatus = ApprovalStatus.Rejected;
            OrganizationDAL.Update(organization);

            // Send message to admin user that this organization has been rejected
            var templateParams = new MailTemplateParams();
            TemplateParametersService.OrganizationRejected(organization.AdminUser, templateParams);
            var msg = new OrganizationMessage
                          {
                              MessageTemplate = TemplateForRejection,
                              To = new MessageAddress {Organization = organization},
                              LastUpdate = new ActivityStamp(organization.AdminUser)
                          };
            MessagingService.SendMessageFromAdmin(msg, templateParams);
        }

        public User FindUserInOrg(T organization, int userId)
        {
            return OrganizationDAL.FindUserInOrg(organization, userId);
        }
    }
}