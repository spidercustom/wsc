﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public enum MatchCriteriaAttributeUsageType
    {
        NotUsed,
        Preference,
        Minimum
    }
}
