﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class FundingProfile : ICloneable
    {
        public FundingProfile()
        {
            Need = new DefinitionOfNeed();
            SupportedSituation = new SupportedSituation();
            FundingParameters = new FundingParameters();
            AttributesUsage = new List<FundingProfileAttributeUsage>();
        }

        public virtual IList<FundingProfileAttributeUsage> AttributesUsage { get; set; }
        public virtual bool HasAttributeUsage(FundingProfileAttribute attribute)
        {
            return AttributesUsage.Any(o => o.Attribute == attribute);
        }

        public virtual DefinitionOfNeed Need { get; set; }
        public virtual FundingParameters FundingParameters { get; set; }
        public virtual SupportedSituation SupportedSituation { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
