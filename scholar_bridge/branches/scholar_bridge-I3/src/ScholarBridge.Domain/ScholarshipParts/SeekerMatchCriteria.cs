﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Location;
using ScholarBridge.Domain.Lookup;
using Spider.Common.Validation.Attributes;

namespace ScholarBridge.Domain.ScholarshipParts
{
    public class SeekerMatchCriteria : ICloneable
    {
        public SeekerMatchCriteria()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            AttributesUsage = new List<MatchCriteriaAttributeUsage>();
            Words = new List<SeekerVerbalizingWord>();
            Skills = new List<SeekerSkill>();
            Colleges = new List<College>();
            Ethnicities = new List<Ethnicity>();
            Religions = new List<Religion>();
            States = new List<State>();
            Counties = new List<County>();
            Cities = new List<City>();
            SchoolDistricts = new List<SchoolDistrict>();
            HighSchools = new List<HighSchool>();
            AcademicAreas = new List<AcademicArea>();
            Careers = new List<Career>();
            Hobbies = new List<SeekerHobby>();
            Organizations = new List<SeekerMatchOrganization>();
            CommunityInvolvementCauses = new List<CommunityInvolvementCause>();
            AffiliationTypes = new List<AffiliationType>();
            Sports = new List<Sport>();
            Clubs = new List<Club>();
            WorkTypes = new List<WorkType>();
            WorkHours = new List<WorkHour>();
            ServiceTypes = new List<ServiceType>();
            ServiceHours = new List<ServiceHour>();
			//We don't want to set GPA, ClassRank, SAT Score and ACTScore here
			//As they can be null if user hasn't set criteria on it.
			//Basically, don't uncomment following lines
			//{
            //	GPA = new RangeCondition<int>();
            //	ClassRank = new RangeCondition<int>();
            //	SATScore = new SatScore();
            //	ACTScore = new ActScore();
			//}
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        public virtual IList<MatchCriteriaAttributeUsage> AttributesUsage { get; set; }
        public bool HasAttributeUsage(MatchCriteriaAttribute attribute)
        {
            return AttributesUsage.Any(o => o.Attribute == attribute);
        }

        public virtual IList<SeekerVerbalizingWord> Words { get; set; }
        public virtual IList<SeekerSkill> Skills { get; set; }
        public virtual StudentGroups StudentGroups { get; set; }
        public virtual SchoolTypes SchoolTypes { get; set; }
        public virtual AcademicPrograms AcademicPrograms { get; set; }
        public virtual SeekerStatuses SeekerStatuses { get; set; }
        public virtual ProgramLengths ProgramLengths { get; set; }
        public virtual IList<College> Colleges { get; set; }
        public virtual bool FirstGeneration { get; set; }
        public virtual IList<Ethnicity> Ethnicities { get; set; }
        public virtual IList<Religion> Religions { get; set; }
        public virtual Genders Genders { get; set; }
        public virtual IList<State> States { get; set; }
        public virtual IList<County> Counties { get; set; }
        public virtual IList<City> Cities { get; set; }
        public virtual IList<SchoolDistrict> SchoolDistricts { get; set; }
        public virtual IList<HighSchool> HighSchools { get; set; }
        public virtual IList<AcademicArea> AcademicAreas { get; set; }
        public virtual IList<Career> Careers { get; set; }
        public virtual IList<SeekerHobby> Hobbies { get; set; }
        public virtual IList<SeekerMatchOrganization> Organizations { get; set; }
        public virtual IList<CommunityInvolvementCause> CommunityInvolvementCauses { get; set; }
        public virtual IList<AffiliationType> AffiliationTypes { get; set; }
        public virtual IList<Sport> Sports { get; set; }
        public virtual IList<Club> Clubs { get; set; }
        public virtual IList<WorkType> WorkTypes { get; set; }
        public virtual IList<WorkHour> WorkHours { get; set; }
        public virtual IList<ServiceType> ServiceTypes { get; set; }
        public virtual IList<ServiceHour> ServiceHours { get; set; }
        public virtual RangeCondition<int> GPA { get; set; }
        public virtual RangeCondition<int> ClassRank { get; set; }
        public virtual SatScore SATScore { get; set; }
        public virtual ActScore ACTScore { get; set; }
        public virtual bool Honors { get; set; }
        [MinimumValidator(0)]
        public virtual int? APCreditsEarned { get; set; }
        public virtual int? IBCreditsEarned { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
