﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public enum ScholarshipAttributeUsageType
    {
        NotUsed,
        Preference,
        Minimum
    }
}
