﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public class ScholarshipQuestion
    {
        public virtual int Id { get; set; }
        public virtual string QuestionText { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual Scholarship Scholarship { get; set; }
        public virtual ActivityStamp LastUpdate { get; set; }
    }
}