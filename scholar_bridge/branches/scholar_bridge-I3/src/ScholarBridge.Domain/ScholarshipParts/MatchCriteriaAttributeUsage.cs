﻿namespace ScholarBridge.Domain.ScholarshipParts
{
    public class MatchCriteriaAttributeUsage
    {
        public virtual MatchCriteriaAttribute Attribute { get; set; }
        public virtual ScholarshipAttributeUsageType UsageType { get; set; }
    }
}
