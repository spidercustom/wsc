﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ScholarBridge.Common.Extensions
{
    public static class EnumExtensions
    {
        //Oh! How do I make static extension method
        public static string[] GetDisplayNames(Type @enumType)
        {
            var fieldInfos = enumType.GetFields();

            var displayNames = new List<string>();
            Array.ForEach(fieldInfos,
                          fieldInfo =>
                              {
                                  string displayName = GetDisplayNameFromFieldInfo(fieldInfo);
                                  if (!string.IsNullOrEmpty(displayName) && !"value__".Equals(displayName))
                                      displayNames.Add(displayName);
                              });

            return displayNames.ToArray();
        }

        //Oh! How do I make static extension method
        public static Dictionary<Int32, string> GetKeyValue(Type @enumType)
        {
            var result = new Dictionary<int, string>();
            var fieldInfos = enumType.GetFields();

            Array.ForEach(fieldInfos,
                          fieldInfo =>
                              {
                                  string displayName = GetDisplayNameFromFieldInfo(fieldInfo);
                                  if (!string.IsNullOrEmpty(displayName) && !"value__".Equals(displayName))
                                      result.Add((int)fieldInfo.GetRawConstantValue(), displayName);
                              });

            return result;
        }

        private static string GetDisplayNameFromFieldInfo(FieldInfo info)
        {
            if (info == null) throw new ArgumentNullException("info");

            var attributes = info.GetCustomAttributes(typeof(DisplayNameAttribute), false);
            string result = null;
            Array.ForEach(attributes,
                          attribute =>
                              {
                                  var displayNameAttribute =
                                      attribute as DisplayNameAttribute;
                                  if (displayNameAttribute != null)
                                      result =  displayNameAttribute.DisplayName;
                              });
            if (result == null)
                result = info.Name;
            return result;
        }

        public static string NumericValueString(this Enum @enum)
        {
            return (Convert.ToInt32(@enum)).ToString();
        }

        public static string GetDisplayName(this Enum @enum)
        {
            if (@enum == null) throw new ArgumentNullException("enum");
           
            Type enumType = @enum.GetType();
            FieldInfo enumElementFieldInfo = enumType.GetField(@enum.ToString());
            
            return enumElementFieldInfo == null ? "" : GetDisplayNameFromFieldInfo(enumElementFieldInfo);
        }

        public static bool IsIn(this Enum find, params object[] @in)
        {
            return Array.IndexOf(@in, find) > -1;
        }        
    }
}