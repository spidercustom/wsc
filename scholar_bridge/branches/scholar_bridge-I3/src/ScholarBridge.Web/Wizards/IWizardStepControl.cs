﻿
namespace ScholarBridge.Web.Wizards
{
    /// <summary>
    /// Wizard interface is used for communicating with wizard step
    /// by wizard step manager
    /// </summary>
    public interface IWizardStepControl<T>
    {
        bool ValidateStep();
        /// <summary>
        /// Saves changes to retrival storage.
        /// </summary>
        void Save();
        bool ChangeNextStepIndex();
        int GetChangedNextStepIndex();
        bool WasSuspendedFrom(T @object);
        bool CanResume(T @object);
        int StepIndex { get; }
        IWizardStepsContainer<T> Container { get; set; }
        void Activated();
    }
}
