﻿using System.Web.UI;

namespace ScholarBridge.Web.Wizards
{
    public abstract class WizardStepUserControlBase<T> : UserControl, IWizardStepControl<T>
    {

        #region IWizardStepControl<T> Members
        public abstract bool ValidateStep();
        public abstract void Save();
        public abstract int StepIndex { get; }
        public abstract bool WasSuspendedFrom(T @object);
        public abstract bool CanResume(T @object);
        public bool ChangeNextStepIndex()
        {
            return false;
        }

        public int GetChangedNextStepIndex()
        {
            return -1;
        }

        public virtual IWizardStepsContainer<T> Container { get; set; }
        public virtual void Activated()
        {
        }

        #endregion
    }
}
