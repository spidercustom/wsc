﻿using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Net.Configuration;
using ScholarBridge.Business.Messaging;

namespace ScholarBridge.Web.Config
{
    /// <summary>
    /// Contains common methods for reading sections from Web.Config
    /// </summary>
    public static class ConfigHelper
    {
        private static LinkGenerator linkGenerator = new LinkGenerator();

        /// <summary>
        /// Gets the HECB admin email from config file.
        /// </summary>
        /// <returns></returns>
        public static string GetHECBAdminEmail()
        {
            return GetAppSettingValue("HECBAdminEmail");
        }

        /// <summary>
        /// Gets the HECB admin email from config file.
        /// </summary>
        /// <returns></returns>
        public static string GetEmailTemplatesDirectory()
        {
            return GetAppSettingValue("EmailsDirectory");
        }

        /// <summary>
        /// Gets the SMTP section from web.config file.
        /// </summary>
        /// <returns></returns>
        public static SmtpSection GetSmtpSection()
        {
            var config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            var section = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
            return section.Smtp;
        }

        /// <summary>
        /// Gets the app setting value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string GetAppSettingValue(string key)
        {
            var reader = new AppSettingsReader();
            return reader.GetValue(key, typeof(string)).ToString();
        }

        public static MailTemplateParams GetMailParams(string to)
        {
            return new MailTemplateParams
                       {
                           From = GetSmtpSection().From,
                           To = to
                       };
        }

        /// <summary>
		/// Generates a correct url for inclusion in emails or other non-web documents (to a specific web page)
		/// </summary>
		/// <param name="relativePathToPage">relative path to a specific web page</param>
		/// <returns></returns>
		public static string GetPageURLViaProxy(string relativePathToPage)
		{
            return linkGenerator.GetFullLink(relativePathToPage);
		}

		/// <summary>
		/// Generates a correct url for inclusion in emails or other non-web documents (to a specific web page)
		/// </summary>
		/// <param name="relativePathToPage">relative path to a specific web page</param>
		/// <param name="queryString">query string values</param>
		/// <returns></returns>
		public static string GetPageURLViaProxy(string relativePathToPage, string queryString)
		{
            return linkGenerator.GetFullLink(relativePathToPage, queryString);
		}
	}
}
