using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web
{
    public interface IUserContext
    {
        User CurrentUser { get; }
        Organization CurrentOrganiztion { get; }
        Provider CurrentProvider { get; }
        Intermediary CurrentIntermediary { get; }

        void EnsureUserIsInContext();
        void EnsureProviderIsInContext();
        void EnsureIntermediaryIsInContext();
    }
}