using ScholarBridge.Domain;

namespace ScholarBridge.Business
{
    public interface IIntermediaryService : IOrganizationService<Intermediary>
    {
    }
}