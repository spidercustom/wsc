using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web
{
    public interface IUserContext
    {
        User CurrentUser { get; }
        Organization CurrentOrganization { get; }
        Provider CurrentProvider { get; }
        Intermediary CurrentIntermediary { get; }
        Seeker CurrentSeeker { get; }

        void EnsureUserIsInContext();
        void EnsureProviderIsInContext();
        void EnsureIntermediaryIsInContext();
        void EnsureSeekerIsInContext();
    }
}