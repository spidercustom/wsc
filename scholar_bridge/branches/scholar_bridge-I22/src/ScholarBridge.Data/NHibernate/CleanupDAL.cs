using System;
using ScholarBridge.Domain.Auth;
using Spring.Data.NHibernate.Support;

namespace ScholarBridge.Data.NHibernate
{
    public class CleanupDAL : HibernateDaoSupport, ICleanupDAL
    {
        public IUserDAL UserDAL { get; set; }

        public void RemoveNonConfirmedUsersFromDatabase(int daysOld)
        {
            var users = UserDAL.FindNonConfirmedUsers(daysOld);
            foreach (var user in users)
            {
                RemoveUserRowFromDatabase(user);
            }
        }

        /// <summary>
        /// This is not the same as Delete. Delete does a soft delete. This
        /// removes the row and is really just for cleanup. Unless you KNOW you want
        /// to use this, you do not want to use this.
        /// It generally won't work for a user who has been active for any period of time
        /// because that user will be associated to LastUpdatedBy of data. 
        /// So if it fails don't be too surprised.
        /// </summary>
        /// <param name="user">The <c>User</c> to remove all traces of.</param>
        public void RemoveUserRowFromDatabase(User user)
        {
            if (null == user)
                throw new ArgumentNullException("user", "User can not be null");

            // Have to resort to SQL since the soft-delete would prevent a real delete from
            // happening (which is generally a good thing) but that causes some complexity here.
            Session
                .CreateSQLQuery("delete from SBUserRolesRT where SBUserId=:id")
                .SetInt32("id", user.Id)
                .ExecuteUpdate();
            Session
                .CreateSQLQuery("delete from SBUserOrganizationRT where SBUserId=:id")
                .SetInt32("id", user.Id)
                .ExecuteUpdate();
			Session
				.CreateSQLQuery("delete from SBOrganization where LastUpdateBy=:id")
				.SetInt32("id", user.Id)
				.ExecuteUpdate();
			Session
				.CreateSQLQuery("delete from SBSeeker where UserId=:id")
				.SetInt32("id", user.Id)
				.ExecuteUpdate();
			Session
				.CreateSQLQuery("delete from SBUser where SBUserId=:id")
				.SetInt32("id", user.Id)
				.ExecuteUpdate();
		}
    }
}