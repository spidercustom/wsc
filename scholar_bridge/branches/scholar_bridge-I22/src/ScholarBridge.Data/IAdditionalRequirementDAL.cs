﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IAdditionalRequirementDAL : IGenericLookupDAL<AdditionalRequirement>
    {
    }
}