using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Domain
{
    public abstract class Organization
    {
        private User adminUser;

        protected Organization()
        {
        	InitializeMembers();
        }

    	private void InitializeMembers()
    	{
    		ApprovalStatus = ApprovalStatus.PendingApproval;
    		ApprovalStatusDate = DateTime.Now;
    		Users = new List<User>();
    		MessagesReceived = new List<Message>();
    	}

    	public virtual int Id { get; set; }


        public virtual IList<User> Users { get; protected set; }

        // This is a trick to allow Hibernate to see it as one-big-list,
        // but to split it from a domain perspective. The assumption is that
        // there are not a lot of deleted users so the overhead is low.
        public virtual IList<User> ActiveUsers { get { return Users.Where(u => ! u.IsDeleted).ToList().AsReadOnly(); } }
        public virtual IList<User> DeletedUsers { get { return Users.Where(u => u.IsDeleted).ToList().AsReadOnly(); } }

        public virtual User AdminUser {
            get { return adminUser; }
            set
            {
                // If the user is added as an Admin user also add them to the UserList if they don't exist
                if (null != value && !Users.Any(u => u.Id == value.Id))
                {
                    Users.Add(value);
                }
                adminUser = value;
            } 
        }

        [NotNullValidator]
        [StringLengthValidator(1, 50)]
        public virtual string Name { get; set; }

        public virtual Address Address { get; set; }
        public virtual PhoneNumber Phone { get; set; }
        public virtual PhoneNumber Fax { get; set; }
        public virtual PhoneNumber OtherPhone { get; set; }

        [StringLengthValidator(0, 128)]
        public virtual string Website { get; set; }

        [NotNullValidator]
        [StringLengthValidator(9, 10)]
        public virtual string TaxId { get; set; }

		public virtual ApprovalStatus ApprovalStatus { get; set; }
		public virtual DateTime ApprovalStatusDate { get; set; }
		public virtual AdminNotes AdminNotes { get; protected set; }

        public virtual ActivityStamp LastUpdate { get; set; }

		public virtual IList<Message> MessagesReceived { get; set; }

        /// <summary>
        /// Append notes to the end of the AdminNotes.
        /// The user passed in will be atrributed.
        /// </summary>
        /// <param name="userAppendingNotes">The user to attribute the notes to.</param>
        /// <param name="notes">The test to add.</param>
        public virtual void AppendAdminNotes(User userAppendingNotes, string notes)
        {
			if (AdminNotes == null)
				AdminNotes = new AdminNotes();
        	AdminNotes.AppendAdminNote(userAppendingNotes, notes);
        }
    }
}