using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.SeekerParts
{
    public class CurrentSchool
    {
        public virtual HighSchool HighSchool { get; set; }
        public virtual string HighSchoolOther { get; set; }

        public virtual College College { get; set; }
        public virtual string CollegeOther { get; set; }

        public virtual SchoolDistrict SchoolDistrict { get; set; }
        public virtual string SchoolDistrictOther { get; set; }

        [DomainValidator((StudentGroups)0, MessageTemplate = "Type of student must be selected", Negated = true, Ruleset = Seeker.PROFILE_ACTIVATION_RULESET)]
        public virtual StudentGroups LastAttended { get; set; }
        public virtual int? YearsAttended { get; set; }

        public ValidationResults ValidateActivation()
        {
            var validator = ValidationFactory.CreateValidator<CurrentSchool>(Seeker.PROFILE_ACTIVATION_RULESET);
            return validator.Validate(this);
        }
    }
}