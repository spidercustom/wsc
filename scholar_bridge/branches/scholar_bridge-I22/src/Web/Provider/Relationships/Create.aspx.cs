﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Provider.Relationships
{
    public partial class Create : Page
    {
        public IUserContext UserContext { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        public IIntermediaryService IntermediaryService { get; set; }
        private const string DEFAULT_PAGEURL = "~/Provider/Admin/#relationship-tab";

        protected void Page_Init(object sender, EventArgs e)
        {
            UserContext.EnsureProviderIsInContext();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Form.DefaultButton = cancelBtn.UniqueID;
                Page.Form.DefaultFocus = cancelBtn.UniqueID;
                var intermediaries = RelationshipService.FindAvailableIntermediaries(UserContext.CurrentProvider);
                if (0 == intermediaries.Count)
                {
                    ClientSideDialogs.ShowAlertNative("No Intermediaries are available with which to request a relationship.","Relationship Request");
                    PopupHelper.CloseSelf(false   );
                }
                orgList.DataSource = intermediaries;
                orgList.DataTextField = "Name";
                orgList.DataValueField = "Id";
                orgList.DataBind();
            }
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            PopupHelper.CloseSelf(false );
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            var provider = UserContext.CurrentProvider;
            int selectedID = int.Parse(orgList.SelectedValue);
            var intermediary = IntermediaryService.FindById(selectedID);
            var relationship = new Relationship
                                  {
                                      Provider = provider,
                                      Intermediary = intermediary,
                                      Requester = RelationshipRequester.Provider,
                                      LastUpdate = new ActivityStamp(UserContext.CurrentUser),
                                  };
            
            try
            {
                RelationshipService.CreateRequest(relationship);

                SuccessMessageLabel.SetMessage("Request Created Successfully.");
                PopupHelper.CloseSelf(false );
                PopupHelper.RefreshParent(ResolveUrl(DEFAULT_PAGEURL));
                
            }
            catch (DuplicateRelationshipException)
            {
                validateorganization.IsValid = false;
                validateorganization.Text = "You already have a relationship with that organization. Please choose a different one.";
            }
        }
    }
}
