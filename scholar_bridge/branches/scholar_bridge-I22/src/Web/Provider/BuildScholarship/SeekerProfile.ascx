﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerProfile.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.SeekerProfile" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
<%@ Register src="~/Common/Lookup/LookupDialog.ascx" tagname="LookupDialog" tagprefix="sb" %>
<%@ Register src="~/Common/FlagEnumCheckBoxList.ascx" tagname="FlagEnumCheckBoxList" tagprefix="sb" %>
<%@ Register src="~/Common/LookupItemCheckboxList.ascx" tagname="LookupItemCheckboxList" tagprefix="sb" %>
<%@ Register src="~/Common/NumberRangeConditionControl.ascx" tagname="NumberRangeCondition" tagprefix="sb" %>
<%@ Register Assembly="Web"
             Namespace="ScholarBridge.Web.Common"
             TagPrefix="sbCommon" %>
<%@ Register src="LocationSelector.ascx" tagname="LocationSelector" tagprefix="sb" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="sandTrap" %>

<h2>Build Scholarship – Applicant's Profile</h2>

<div class="legends">
    <ul>
        <li><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" /> <span>indicates a required criteria for this scholarship</span></li>
        <li><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" /> <span>indicates a preferred, but not required criteria, for this scholarship</span></li>
    </ul>
</div>

<div class="form-iceland-container">
    <div class="form-iceland">
        <asp:PlaceHolder ID="SeekerTypeContainerControl" runat="server">
          <p class="form-section-title">Applicant Student Type</p>
          <asp:PlaceHolder ID="StudentGroupContainerControl" runat="server">
            <label id="StudentGroupLabelControl" for="StudentGroupControl">Student groups:</label>
            <asp:Image ID="StudentGroupUsageTypeIconControl" runat="server" class="field-indicator" />
            <sb:FlagEnumCheckBoxList id="StudentGroupControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.StudentGroups, ScholarBridge.Domain"></sb:FlagEnumCheckBoxList>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="SchoolTypeContainerControl" runat="server">
            <label id="SchoolTypeLabelControl" for="SchoolTypeControl">School types:</label>
            <asp:Image ID="SchoolTypeUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:FlagEnumCheckBoxList id="SchoolTypeControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SchoolTypes, ScholarBridge.Domain" />
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="AcademicProgramContainerControl" runat="server">
            <label id="AcademicProgramLabelControl" for="AcademicProgramControl">Academic programs:</label>
            <asp:Image ID="AcademicProgramUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:FlagEnumCheckBoxList id="AcademicProgramControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.AcademicPrograms, ScholarBridge.Domain" />
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="SeekerStatusContainerControl" runat="server">
              <label id="SeekerStatusLabelControl" for="SeekerStatusControl">Enrollment statuses:</label>
              <asp:Image ID="SeekerStatusUsageTypeIconControl" runat="server" class="field-indicator"/>
              <sb:FlagEnumCheckBoxList id="SeekerStatusControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.SeekerStatuses, ScholarBridge.Domain" />
              <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="ProgramLengthContainerControl" runat="server">
            <label id="ProgramLengthLabelControl" for="ProgramLengthControl">Length of program:</label>
            <asp:Image ID="ProgramLengthUsageTypeIconControl"  runat="server" class="field-indicator"/>
            <sb:FlagEnumCheckBoxList id="ProgramLengthControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.ProgramLengths, ScholarBridge.Domain" />
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="CollegesContainerControl" runat="server">
            <label id="CollegesLabelControl" for="CollegesControl">Colleges:</label>
            <asp:Image ID="CollegesUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupDialog ID="CollegesControlDialogButton" runat="server" BuddyControl="CollegesControl" ItemSource="CollegeDAL" Title="College Selection"/>
            <asp:TextBox ID="CollegesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="CollegesRequiredValidator" ControlToValidate="CollegesControl" runat="server" ErrorMessage="Select at least one college"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>

        </asp:PlaceHolder>
        <hr />
        <asp:PlaceHolder ID="SeekerDemographicsPersonalContainerControl" runat="server">
          <p class="form-section-title">Applicant Demographics - Personal</p>

          <asp:PlaceHolder ID="FirstGenerationContainerControl" runat="server">
            <label for="FirstGenerationControl">First Generation:</label>
            <asp:Image ID="FirstGenerUsageTypeIconControl" runat="server" class="field-indicator" />
            <asp:CheckBox ID="FirstGenerationControl" Text="" runat="server" />
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="EthnicityContainerControl" runat="server">
            <label id="EthnicityLabelControl" for="EthnicityControl">Ethnicity:</label>
            <asp:Image ID="EthnicityUsageTypeIconControl" runat="server" class="field-indicator" />
            <sb:LookupItemCheckboxList id="EthnicityControl" runat="server" LookupServiceSpringContainerKey="EthnicityDAL"/>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="ReligionContainerControl" runat="server">
            <label id="ReligionLabelControl" for="ReligionControl">Religion:</label>
            <asp:Image ID="ReligionUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupItemCheckboxList id="ReligionControl" runat="server" LookupServiceSpringContainerKey="ReligionDAL"/>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="GendersContainerControl" runat="server">
            <label id="GendersLabelControl" for="GendersControl">Genders:</label>
            <asp:Image ID="GendersUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:FlagEnumCheckBoxList id="GendersControl" runat="server" DatasourceEnumTypeName="ScholarBridge.Domain.Lookup.Genders, ScholarBridge.Domain" />
            <br />
          </asp:PlaceHolder>
        </asp:PlaceHolder>
        <hr />
        <asp:PlaceHolder ID="SeekerDemographicsGeographicContainerControl" runat="server">
          <p class="form-section-title">Applicant Demographics - Geographic <asp:Image ID="SeekerDemographicsGeographicUsageTypeIconControl" runat="server" /></p>
          
          <asp:PlaceHolder ID="LocationSelectorContainer" runat="server">
              <sb:LocationSelector id="LocationSelectorControl" runat="server" />
              <br />
          </asp:PlaceHolder>

        </asp:PlaceHolder>
        <hr />
        <asp:PlaceHolder ID="SeekerInterestsControlContainer" runat="server">
          <p class="form-section-title">Applicant Interests</p>

          <asp:PlaceHolder ID="AcademicAreasContainerControl" runat="server">
            <label id="AcademicAreasLabelControl" for="AcademicAreasControl">Academic areas:</label>
            <asp:Image ID="AcademicAreasUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupDialog ID="AcademicAreasControlDialogButton" runat="server" BuddyControl="AcademicAreasControl" ItemSource="AcademicAreaDAL" Title="Academic Area Selection"/>
            <asp:TextBox ID="AcademicAreasControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="AcademicAreasControlRequiredValidator" ControlToValidate="AcademicAreasControl" runat="server" ErrorMessage="Select at least one academic area"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="CareersContainerControl" runat="server">
            <label id="CareersLabelControl" for="CareersControl">Careers:</label>
            <asp:Image ID="CareersUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupDialog ID="CareersControlDialogButton" runat="server" BuddyControl="CareersControl" ItemSource="CareerDAL" Title="Career Selection"/>
            <asp:TextBox ID="CareersControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="CareersControlRequiredValidator" ControlToValidate="CareersControl" runat="server" ErrorMessage="Select at least one career"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="CommunityServicesContainerControl" runat="server">
            <label id="CommunityServicesLabelControl" for="CommunityServicesControl">Community Service/Involvement:</label>
            <asp:Image ID="CommunityServicesUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupDialog ID="CommunityServicesControlDialogButton" runat="server" BuddyControl="CommunityServicesControl" ItemSource="CommunityServiceDAL" Title="Community Service/Involvement Selection"/>
            <asp:TextBox ID="CommunityServicesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>

            <asp:RequiredFieldValidator ID="CommunityServicesRequiredValidator" ControlToValidate="CommunityServicesControl" runat="server" ErrorMessage="Select at least one community service"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="OrganizationsContainerControl" runat="server">
            <label id="OrganizationsLabelControl" for="OrganizationsControl">Organizations:</label>
            <asp:Image ID="OrganizationsUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupDialog ID="OrganizationsControlDialogButton" runat="server" BuddyControl="OrganizationsControl" ItemSource="SeekerMatchOrganizationDAL" Title="Organization Affiliation Selection"/>
            <asp:TextBox ID="OrganizationsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>

            <asp:RequiredFieldValidator ID="OrganizationsControlRequiredValidator" ControlToValidate="OrganizationsControl" runat="server" ErrorMessage="Select at least one organization"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="AffiliationTypesContainerControl" runat="server">
            <label id="AffiliationTypesLabelControl" for="AffiliationTypesControl">Affiliation types:</label>
            <asp:Image ID="AffiliationTypesUsageTypeIconControl" runat="server" class="field-indicator"/>
           <sb:LookupDialog ID="AffiliationTypesControlDialogButton" runat="server" BuddyControl="AffiliationTypesControl" ItemSource="AffiliationTypeDAL" Title="Affiliation Type Selection"/>
            <asp:TextBox ID="AffiliationTypesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="AffiliationTypesControlRequiredValidator" ControlToValidate="AffiliationTypesControl" runat="server" ErrorMessage="Select at least one affiliation type"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
        </asp:PlaceHolder>
        <hr />
        <asp:PlaceHolder ID="SeekerActivitiesControlContainer" runat="server">
          <p class="form-section-title">Applicant Activities</p>

          <asp:PlaceHolder ID="HobbiesContainerControl" runat="server">
            <label id="SeekerHobbiesLabelControl" for="SeekerHobbiesControl">Hobbies:</label>
            <asp:Image ID="SeekerHobbiesUsageTypeIconControl" runat="server" class="field-indicator" />
            <sb:LookupDialog ID="SeekerHobbiesControlDialogButton" runat="server" BuddyControl="SeekerHobbiesControl" ItemSource="SeekerHobbyDAL" Title="Hobby Selection"/>
            <asp:TextBox ID="SeekerHobbiesControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="SeekerHobbiesControlRequiredValidator" ControlToValidate="SeekerHobbiesControl" runat="server" ErrorMessage="Select at least one hobby"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="SportsContainerControl" runat="server">
            <label id="SportsLabelControl" for="SportsControl">Sports:</label>
            <asp:Image ID="SportsUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupDialog ID="SportsControlDialogButton" runat="server" BuddyControl="SportsControl" ItemSource="SportDAL" Title="Sport Participation Selection"/>
            <asp:TextBox ID="SportsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="SportsControlRequiredValidator" ControlToValidate="SportsControl" runat="server" ErrorMessage="Select at least one sport"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="ClubsContainerControl" runat="server">
            <label id="ClubsLabelControl" for="ClubsControl">Clubs:</label>
            <asp:Image ID="ClubsUsageTypeIconControl" runat="server" class="field-indicator"/>
           <sb:LookupDialog ID="ClubsControlDialogButton" runat="server" BuddyControl="ClubsControl" ItemSource="ClubDAL" Title="Club Participation Selection"/>
            <asp:TextBox ID="ClubsControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="ClubsControlRequiredValidator" ControlToValidate="ClubsControl" runat="server" ErrorMessage="Select at least one club"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="WorkTypeContainerControl" runat="server">
            <label id="WorkTypeLabelControl" for="WorkTypeControl">Work Type:</label>
            <asp:Image ID="WorkTypeUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupDialog ID="WorkTypeControlDialogButton" runat="server" BuddyControl="WorkTypeControl" ItemSource="WorkTypeDAL" Title="Work Type Selection"/>
            <asp:TextBox ID="WorkTypeControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="WorkTypeControlRequiredValidator" ControlToValidate="WorkTypeControl" runat="server" ErrorMessage="Select at least one work type"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="WorkHoursContainerControl" runat="server">
            <label id="WorkHoursLabelControl" for="WorkHoursControl">Work Hours:</label>
            <asp:Image ID="WorkHoursUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupItemCheckboxList id="WorkHoursControl" runat="server" LookupServiceSpringContainerKey="WorkHourDAL"/>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="ServiceTypeContainerControl" runat="server">
            <label id="ServiceTypeLabelControl" for="ServiceTypeControl">Service Type:</label>
            <asp:Image ID="ServiceTypeUsageTypeIconControl" runat="server" class="field-indicator"/>
           <sb:LookupDialog ID="ServiceTypeControlDialogButton" runat="server" BuddyControl="ServiceTypeControl" ItemSource="ServiceTypeDAL" Title="Service Type Selection"/>
            <asp:TextBox ID="ServiceTypeControl" ReadOnly="true" TextMode="MultiLine" runat="server"></asp:TextBox>
            
            <asp:RequiredFieldValidator ID="ServiceTypeControlRequiredValidator" ControlToValidate="ServiceTypeControl" runat="server" ErrorMessage="Select at least one work hour"></asp:RequiredFieldValidator>
            <br />
          </asp:PlaceHolder>
          
          <asp:PlaceHolder ID="ServiceHoursContainerControl" runat="server">
            <label id="ServiceHoursLabelControl" for="ServiceHoursControl">Service hours:</label>
            <asp:Image ID="ServiceHoursUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sb:LookupItemCheckboxList id="ServiceHoursControl" runat="server" LookupServiceSpringContainerKey="ServiceHourDAL"/>
            <br />
          </asp:PlaceHolder>
          
        </asp:PlaceHolder>
        <hr />
        <asp:PlaceHolder ID="SeekerPerformanceControlContainer" runat="server">
          <p class="form-section-title">Applicant Student Performance</p>

          <asp:PlaceHolder ID="GPAControlContainerControl" runat="server">
                <!-- See code comment WSC-352 -->
                <label id="GPALabelControl" for="GPAControl">GPA (4.0=A) <asp:Image ID="GPAUsageTypeIconControl" runat="server" /></label>
                <br />
                
                <div class="subsection control-set">
                    <label for="GPAGreaterThanControl">Greater than:</label>
                    <sandTrap:NumberBox ID="GPAGreaterThanControl" runat="server" Precision="3" MinAmount="0" MaxAmount="5"/>
                    

                    <label for="GPALessThanControl">Less than:</label>
                    <asp:TextBox ID="GPALessThanControl" runat="server" style="text-align:right" Precision="3" CanNull="true"
                        minamount="0" maxamount="5" onfocus="FormatNumberAsDecimal(this)" onblur="FormatDecimalAsNumber(this)"/>
                </div>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="SATContainerControl"  runat="server">
            <h5>SAT Score:</h5>
            <div class="subsection control-set">
                <div id="satWritingDiv" runat="Server">
                    <label for="SATWritingControl">Writing</label>  <asp:Image ID="satWritingScoreUsageTypeIconControl" runat="server" />
                    <sb:NumberRangeCondition id="SATWritingControl" runat="server" 
                      MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
                    <br />
                </div>
                <div id="satCriticalReadingDiv" runat="Server">
                    <label for="SATCriticalReadingControl">Critical Reading</label> <asp:Image ID="satCriticalReadingScoreUsageTypeIconControl" runat="server" />
                    <sb:NumberRangeCondition id="SATCriticalReadingControl" runat="server"
                      MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
                    <br />
                </div>
                <div id="satMathematicsDiv" runat="Server">
                    <label for="SATMathematicsControl">Mathematics</label> <asp:Image ID="satMathematicsScoreUsageTypeIconControl" runat="server" />
                    <sb:NumberRangeCondition id="SATMathematicsControl" runat="server"
                      MaximumAllowedValue="800" MinimumAllowedValue="200" DefaultMaximumValue="800" DefaultMinimumValue="200"/>
                </div>
            </div>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="ACTContainerControl"  runat="server">
            <h5>ACT Score: </h5>
            <div class="subsection control-set">
                <div id="actEnglishDiv" runat="Server">
                    <label for="ACTEnglishControl">English:</label> <asp:Image ID="actEnglishScoreUsageTypeIconControl" runat="server" />
                    <sb:NumberRangeCondition id="ACTEnglishControl" runat="server"
                      MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                    <br />    
                </div>
                <div id="actMathematicsDiv" runat="Server">
                    <label for="ACTMathematicsControl">Mathematics:</label> <asp:Image ID="actMathematicsScoreUsageTypeIconControl" runat="server" />
                    <sb:NumberRangeCondition id="ACTMathematicsControl" runat="server"
                      MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                    <br />
                </div>
                <div id="actReadingDiv" runat="Server">
                    <label for="ACTReadingControl">Reading:</label> <asp:Image ID="actReadingScoreUsageTypeIconControl" runat="server" />
                    <sb:NumberRangeCondition id="ACTReadingControl" runat="server"
                      MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                    <br />
                    </div>
                <div id="actScienceDiv" runat="Server">
                    <label for="ACTScienceControl">Science:</label> <asp:Image ID="actScienceScoreUsageTypeIconControl" runat="server" />
                    <sb:NumberRangeCondition id="ACTScienceControl" runat="server"
                      MaximumAllowedValue="36" MinimumAllowedValue="0" DefaultMaximumValue="36" DefaultMinimumValue="0"/>
                </div>
            </div>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="ClassRankContainerControl"  runat="server">
            <h5 id="ClassRankLabelControl" runat="server">Class rank: <asp:Image ID="ClassRankUsageTypeIconControl" runat="server" /></h5>
            <div class="subsection control-set">
                <label for="ACTEnglishControl">Class Rank (percentage):</label>
                <sb:NumberRangeCondition id="ClassRankControl" runat="server" GreaterThanControlLabelText="greater than"
                   MaximumAllowedValue="100" MinimumAllowedValue="0" DefaultMaximumValue="100" DefaultMinimumValue="0"/>
                <br />
            </div>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="HonorsContainerControl" runat="server">
            <label for="HonorsLabelControl">Honors:</label>
            <asp:Image ID="HonorsUsageTypeIconControl" runat="server" class="field-indicator" />
            <asp:CheckBox ID="HonorsControl" Text="" runat="server" />
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="APCreditContainerControl" runat="server">
            <label for="APCreditControl">AP Credits Earned:</label>
            <asp:Image ID="APCUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sandTrap:NumberBox ID="APCreditControl" runat="server" Precision="0"  MaxAmount="10000" MinAmount="0"/>
            <elv:PropertyProxyValidator ID="APCreditValidator" runat="server" ControlToValidate="APCreditControl" PropertyName="APCreditsEarned" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.SeekerProfileCriteria"/>
            <br />
          </asp:PlaceHolder>

          <asp:PlaceHolder ID="IBCreditContainerControl" runat="server">
            <label for="IBCreditControl">IB Credits Earned:</label>
            <asp:Image ID="IBCUsageTypeIconControl" runat="server" class="field-indicator"/>
            <sandTrap:NumberBox ID="IBCreditControl" runat="server" Precision="0"  MaxAmount="10000" MinAmount="0"/>
            <elv:PropertyProxyValidator ID="IBCreditValidator" runat="server" ControlToValidate="IBCreditControl" PropertyName="IBCreditsEarned" SourceTypeName="ScholarBridge.Domain.ScholarshipParts.SeekerProfileCriteria"/>
            <br />
          </asp:PlaceHolder>
        </asp:PlaceHolder>

        <asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
          <p>No Scholarship Match Criteria Selected. Please go to the <asp:LinkButton ID="BuildMatchCriteriaLinkControl" runat="server" OnClick="BuildMatchCriteriaLinkControl_Click">Selection Criteria</asp:LinkButton> tab to choose fields to be used for Scholarship selection</p>
        </asp:PlaceHolder>
    </div>
</div>

