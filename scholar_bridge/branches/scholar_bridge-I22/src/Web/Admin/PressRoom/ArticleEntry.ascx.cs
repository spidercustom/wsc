﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Admin.PressRoom 
{

    public partial class ArticleEntry : UserControl
    {
        public delegate void ArticleSaved(Article article);
        public delegate void ArticleCanceled();

        public event ArticleSaved FormSaved;
        public event ArticleCanceled FormCanceled;

        public IArticleService ArticleService { get; set; }
        public Article ArticleInContext { get; set; }
        public IUserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (null == ArticleInContext)
            {
                errorMessage.Visible = true;
                return;
            }

            PostedDateControl.SelectedDate = ArticleInContext.PostedDate;
            TitleControl.Text = ArticleInContext.Title;
            BodyControl.Text = ArticleInContext.Body;
        }

		

        protected void saveButton_Click(object sender, EventArgs e)
        {   Page.Validate();
            if (!Page.IsValid) return;
            ArticleInContext.PostedDate = PostedDateControl.SelectedDate.Value;
            ArticleInContext.Title = TitleControl.Text;
            ArticleInContext.Body = BodyControl.Text;
            ArticleInContext.LastUpdate=new ActivityStamp(UserContext.CurrentUser);
            ArticleService.Save(ArticleInContext);

            if (null != FormSaved)
            {
                FormSaved(ArticleInContext);
            }
        }

		 
        protected void cancelButton_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}