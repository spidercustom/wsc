﻿<%@ Import Namespace="ScholarBridge.Common.Extensions"%>
<%@ Import Namespace="ScholarBridge.Domain"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipList.ascx.cs" Inherits="ScholarBridge.Web.Common.ScholarshipList" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>
<asp:ListView ID="lstScholarships" runat="server" 
    OnItemDataBound="lstScholarships_ItemDataBound" 
    onpagepropertieschanging="lstScholarships_PagePropertiesChanging" >
    <LayoutTemplate>
    <table class="sortableTable" cellpadding="0" cellspacing="0" width="90%">
            <thead>
            <tr>
                <th style="width:60px">Status</th>
                <th>Name</th>
                <th>Academic Year</th>
                <th>Application Start Date</th>
                <th>Application Due Date</th>
                <th>Number of Applicants</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
        <td><%# ((ScholarshipStages) DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName() %></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
        <td><asp:Label id="applicantCountControl" runat="server" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
        <td><%# ((ScholarshipStages) DataBinder.Eval(Container.DataItem, "Stage")).GetDisplayName() %></td>
        <td><asp:HyperLink id="linktoScholarship" runat="server"><%# DataBinder.Eval(Container.DataItem, "Name") %></asp:HyperLink></td>
        <td><%# DataBinder.Eval(Container.DataItem, "AcademicYear") %></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationStartDate", "{0:d}")%></td>
        <td><%# DataBinder.Eval(Container.DataItem, "ApplicationDueDate", "{0:d}")%></td>
        <td><asp:Label id="applicantCountControl" runat="server" /></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships</p>
    </EmptyDataTemplate>
</asp:ListView>

<div class="pager">
    <asp:DataPager runat="server" ID="pager" PagedControlID="lstScholarships" PageSize="20"
        OnPreRender="pager_PreRender">
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
</div>
