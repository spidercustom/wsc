﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderRelationshipList.ascx.cs" Inherits="ScholarBridge.Web.Common.ProviderRelationshipList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<div id="yesnodialogdiv" title="Confirm Inactivate & Notify." style="display:none;">
    <p>Inactivating a relationship will apply to future scholarships. Continue?</p>
</div>

<asp:ListView ID="lstRelationships" runat="server" 
    OnItemDataBound="lstRelationships_ItemDataBound" 
    onitemcommand="lstRelationships_ItemCommand" >
    <LayoutTemplate>
    <table class="sortableTable" cellpadding="0" cellspacing="0" width="95%">
        <thead>
            <tr>
                <th>Organization Name</th>
                <th>Org. Admin Name</th>
                <th>Org. Admin Email</th>
                <th>Phone Number</th>
                <th>Status</th>
                <th>Requested Date</th>
                <th>Activated Date</th>
                <th>InActivated Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    </LayoutTemplate>
          
    <ItemTemplate>
      <tr class="row">
        <td><asp:Literal ID="lblName" runat="server" /></td>
        <td><asp:Literal ID="lblAdminName" runat="server" /></td>
        <td><asp:Literal ID="lblAdminEmail" runat="server" /></td>
        <td><asp:Literal ID="lblPhone" runat="server" /></td>
        <td><asp:Literal ID="lblStatus" runat="server" /></td>
        <td><%#DataBinder.Eval(Container.DataItem, "RequestedOn","{0:d}")%></td>
        <td><%#DataBinder.Eval(Container.DataItem, "ActivatedOn", "{0:d}")%></td>
        <td><%#DataBinder.Eval(Container.DataItem, "InActivatedOn", "{0:d}")%></td>
        <td><asp:HyperLink id="inactivateLink" Text="Inactivate & Notify"   runat="server"/></td>
    </tr>
    </ItemTemplate>
        
    <AlternatingItemTemplate>
     <tr class="altrow">
        <td><asp:Literal ID="lblName" runat="server" /></td>
        <td><asp:Literal ID="lblAdminName" runat="server" /></td>
        <td><asp:Literal ID="lblAdminEmail" runat="server" /></td>
        <td><asp:Literal ID="lblPhone" runat="server" /></td>
        <td><asp:Literal ID="lblStatus" runat="server" /></td>
        <td><%#DataBinder.Eval(Container.DataItem, "RequestedOn","{0:d}")%></td>
        <td><%#DataBinder.Eval(Container.DataItem, "ActivatedOn", "{0:d}")%></td>
        <td><%#DataBinder.Eval(Container.DataItem, "InActivatedOn", "{0:d}")%></td>
        <td><asp:HyperLink id="inactivateLink" Text="Inactivate & Notify"   runat="server"/></td>  
    </tr>
    </AlternatingItemTemplate>   
    <EmptyDataTemplate>
    <p>There are no Relationships, </p>
    </EmptyDataTemplate>
</asp:ListView>
     

