﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.PressRoom.Default" Title="Admin" %>
<%@ Register src="~/PressRoom/ArticleList.ascx" tagname="ArticleList" tagprefix="sb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ImageContent" ContentPlaceHolderID="pictureAreaPlaceHolder" runat="server">
    <div><asp:Image ID="Image1" ImageUrl="~/images/PicTopProviderProfile.gif" Width="918px" Height="15px" runat="server" /></div>
    <div><asp:Image ID="Image2" ImageUrl="~/images/PicBottomProviderProfile.gif" Width="918px" Height="165px" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<p>Press Room</p>
<br />
<br />
<p>Place keeper for WSC provided content. Content could include: Basic facts about the organization, Financial Information, Location of Headquarters, Leadership profiles, Downloadable images, Overview of the organization’s commitment to social responsibility, Media contact information</p>
<br />
<br />

<sb:ArticleList ID="ArticleList1" runat="server" LinkTo="~/PressRoom/Show.aspx" />



</asp:Content>
