using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using Spider.Common.Core.DAL;

namespace ScholarBridge.Data
{
    public interface ISeekerDAL : IDAL<Seeker>
    {
        Seeker FindByUser(User user);
    	int CountRegisteredSeekers();
    	int CountSeekersWithActiveProfiles();
    }
}