using NHibernate.Type;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.NHibernate
{
    /// <summary>
    /// NHibernate Wrapper for the <c>OrganizationContactDisplay</c> enum to allow it to be
    /// mapped by name into the database.
    /// </summary>
    public class ScholarshipOrganizationContactDisplay : EnumStringType
    {
        public ScholarshipOrganizationContactDisplay()
            : base(typeof(OrganizationContactDisplay), 50)
        {
        }
    }
}