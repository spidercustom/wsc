﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(87)]
    public class AddRelationship : AddTableBase
    {
        public const string TABLE_NAME = "SBRelationship";
        public const string PRIMARY_KEY_COLUMN = "SBRelationshipID";

        public override string TableName
        {
            get { return TABLE_NAME; }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return new ForeignKey[]
            {
                CreateForeignKey("LastUpdatedBy", "SBUser", "SBUserID"),
                CreateForeignKey("ProviderOrganizationID","SBOrganization", "SBOrganizationID"),
                CreateForeignKey( "IntermediaryOrganizationID","SBOrganization", "SBOrganizationID")
            };
        }

        public override Column[] CreateColumns()
        {
            return new Column[]
            {
                new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                new Column("ProviderOrganizationID", DbType.Int32, ColumnProperty.NotNull),
                new Column("IntermediaryOrganizationID", DbType.Int32, ColumnProperty.NotNull),
                new Column("ApprovalStatus", DbType.String, 30, ColumnProperty.NotNull), 
                new Column("Stage", DbType.String, 30, ColumnProperty.NotNull), 
                new Column("LastUpdatedBy", DbType.Int32, ColumnProperty.NotNull),
                new Column("LastUpdatedOn", DbType.DateTime, ColumnProperty.NotNull)
            };
        }
    }
}
