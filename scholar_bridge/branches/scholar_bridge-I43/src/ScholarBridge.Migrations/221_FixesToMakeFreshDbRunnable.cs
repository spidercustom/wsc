﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	/// <summary>
	/// responding to JIRA items WSC-550 to 552
	/// </summary>
	[Migration(221)]
	public class FixesToMakeFreshDbRunnable : Migration
	{
		public override void Up()
		{
			Database.ExecuteNonQuery(@"update sbuser 
										set username = 'wscadmin@scholarbridge.com',
											email = 'wscadmin@scholarbridge.com', 
											FirstName = 'WashBoard',
											LastName='Administrator' 
										where username = 'wscadmin'");
			Database.ExecuteNonQuery(@"update sbuser 
										set username = 'admin@scholarbridge.com',
											email = 'admin@scholarbridge.com', 
											FirstName = 'System',
											LastName='Administrator' 
										where username = 'admin'");
		}

		public override void Down()
		{
		}
	}
}