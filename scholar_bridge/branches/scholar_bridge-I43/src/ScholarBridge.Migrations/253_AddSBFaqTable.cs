﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
	[Migration(253)]
	public class AddFaqTable : AddTableBase
	{
		public const string TABLE_NAME = "SBFaq";
		public const string PRIMARY_KEY_COLUMN = "SBFaqId";

		public override string TableName
		{
			get { return TABLE_NAME; }
		}

		public override string PrimaryKeyColumn
		{
			get { return PRIMARY_KEY_COLUMN; }
		}

		public override Column[] CreateColumns()
		{
			return new[]
                       {
                           new Column(PRIMARY_KEY_COLUMN, DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("FaqQuestion", DbType.String,250, ColumnProperty.NotNull),
                           new Column("FaqAnswer", DbType.String,4000, ColumnProperty.NotNull),
                           new Column("FaqType", DbType.String,50, ColumnProperty.NotNull),
                           new Column("LastUpdateBy", DbType.Int32, ColumnProperty.NotNull),
                           new Column("LastUpdateDate", DbType.DateTime, ColumnProperty.NotNull, "(getdate())"),
                       };
		}
		public override ForeignKey[] CreateForeignKeys()
		{
			return new[]
            {
                CreateForeignKey("LastUpdateBy", "SBUser", "SBUserID"),
            };
		}

	}
}