﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class RelationshipService : IRelationshipService
    {
        public IRelationshipDAL RelationshipDAL { get; set; }
        public IMessagingService MessagingService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }

        public void Save(Relationship relationship)
        {
            RelationshipDAL.Save(relationship);
        }

        public Relationship GetById(int id)
        {
            return RelationshipDAL.FindById(id);
        }

        public IList<Relationship> GetByProvider(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            return RelationshipDAL.FindByProvider(provider);
        }

        public IList<Intermediary> FindAvailableIntermediaries(Provider provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            return RelationshipDAL.FindNotRelatedByProvider(provider);
        }

        public IList<Provider> FindAvailableProviders(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return RelationshipDAL.FindNotRelatedByIntermediary(intermediary);
        }

        public IList<Relationship> GetByIntermediary(Intermediary intermediary)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return RelationshipDAL.FindByIntermediary(intermediary);
        }

        public IList<Organization> GetActiveByOrganization(Organization organization)
        {
            if (organization == null)
                throw new ArgumentNullException("organization");

            if (organization is Intermediary)
            {
                // FIXME: We should filter Active in the database for efficiency, can order by there too
                var relationships = GetByIntermediary((Intermediary)organization);
                return (from r in relationships
                        where r.Status == RelationshipStatus.Active
                        orderby r.Provider.Name
                        select r.Provider as Organization).ToList();
            }
            else
            {
                // FIXME: We should filter Active in the database for efficiency, can order by there too
                var relationships = GetByProvider((Provider)organization);
                return (from r in relationships
                        where r.Status == RelationshipStatus.Active
                        orderby r.Provider.Name
                        select r.Intermediary as Organization).ToList();
            }
        }

        public void InActivate(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            if (!(relationship.Status == RelationshipStatus.InActive))
            {
                relationship.Status = RelationshipStatus.InActive;
                relationship.InActivatedOn = DateTime.Now;
                Save(relationship);


                var templateParams = new MailTemplateParams();
                TemplateParametersService.RelationshipInActivated(relationship.FromOrg, templateParams);
                var msg = new RelationshipMessage
                {
                    MessageTemplate = MessageType.RelationshipInactivated,
                    From = new MessageAddress { Organization = relationship.FromOrg },
                    To = new MessageAddress { User = relationship.ToOrg.AdminUser, Organization = relationship.ToOrg },
                    LastUpdate = new ActivityStamp(relationship.FromOrg.AdminUser)
                };

                MessagingService.SendMessage(msg, templateParams,true);
            }
        }

        public void CreateRequest(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            if (HasRelationship(relationship.FromOrg, relationship.ToOrg))
                throw new DuplicateRelationshipException("Relationship already exists.");

            relationship.Status = RelationshipStatus.Pending;
            relationship.RequestedOn = DateTime.Today;
        	relationship.ActivatedOn = null;
        	relationship.InActivatedOn = null;
            Save(relationship);

            var templateParams = new MailTemplateParams();
            TemplateParametersService.RelationshipRequestCreated(relationship.FromOrg, templateParams);
            var msg = new RelationshipMessage
                    {
                        MessageTemplate = MessageType.RelationshipCreateRequest,
                        From = new MessageAddress { Organization = relationship.FromOrg },
                        To = new MessageAddress { User = relationship.ToOrg.AdminUser, Organization = relationship.ToOrg },
                        LastUpdate = new ActivityStamp(relationship.FromOrg.AdminUser)

                    };
            MessagingService.SendMessage(msg, templateParams,true );
        }

        public void Approve(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            relationship.Status = RelationshipStatus.Active;
        	relationship.ActivatedOn = DateTime.Now;
            Save(relationship);

            var templateParams = new MailTemplateParams();
            TemplateParametersService.RelationshipRequestApproved(relationship.ToOrg, templateParams);

            // This is a Response, so From and To are swapped
            var msg = new RelationshipMessage
                {
                    MessageTemplate = MessageType.RelationshipRequestApproved,
                    From = new MessageAddress { Organization = relationship.ToOrg },
                    To = new MessageAddress { User = relationship.FromOrg.AdminUser, Organization = relationship.FromOrg },
                    LastUpdate = new ActivityStamp(relationship.ToOrg.AdminUser)
                };
            MessagingService.SendMessage(msg, templateParams, true);
        }

        public void Reject(Relationship relationship)
        {
            if (null == relationship)
                throw new ArgumentNullException("relationship", "relationship can not be null");

            RelationshipDAL.Delete(relationship);

            var templateParams = new MailTemplateParams();
            TemplateParametersService.RelationshipRequestRejected(relationship.FromOrg, templateParams);

            // This is a Response, so From and To are swapped
            var msg = new RelationshipMessage
                {
                    MessageTemplate = MessageType.RelationshipRequestRejected,
                    From = new MessageAddress { Organization = relationship.ToOrg },
                    To = new MessageAddress { User = relationship.FromOrg.AdminUser, Organization = relationship.FromOrg },
                    LastUpdate = new ActivityStamp(relationship.ToOrg.AdminUser)
                };
            MessagingService.SendMessage(msg, templateParams, true);
        }


        public Relationship GetByOrganizations(Organization orgA, Organization orgB)
        {
            if (orgA == null)
                throw new ArgumentNullException("OrgA");
            if (orgB == null)
                throw new ArgumentNullException("OrgB");

            Provider provider = null;
            Intermediary intermediary = null;

            if (orgA is Provider)
                provider = orgA as Provider;
            else if (orgA is Intermediary)
                intermediary = orgA as Intermediary;

            if (orgB is Provider)
                provider = orgB as Provider;
            else if (orgB is Intermediary)
                intermediary = orgB as Intermediary;

            if (provider == null)
                throw new ArgumentNullException("provider");
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return RelationshipDAL.FindByProviderandIntermediary(provider, intermediary);
        }

        public bool HasRelationship(Organization orgA, Organization orgB)
        {
            return null != GetByOrganizations(orgA, orgB);
        }
    }
}
