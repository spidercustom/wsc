﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Business
{
	public interface ILookupTableService
	{
		IEnumerable GetLookupTableData(string qualifiedClassName);
		List<Type> GetLookupTableList();
		void UpdateLookupTableData(string tableName, User user, string Name, string Description, bool Deprecated, int Id);
		void DeleteLookupTableData(string tableName, int Id);
		void InsertLookupTableData(string tableName, User user, string Name, string Description);
	}
}
