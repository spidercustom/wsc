﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintApplication.ascx.cs" Inherits="ScholarBridge.Web.Common.PrintApplication" %>
<%@ Register src="~/Common/FileList.ascx" tagname="FileList" tagprefix="sb" %>

<h1><asp:Literal  ID="scholarshipName"  runat="server" /></h1>

<h2>Contact Information</h2>
<div class="attribute" id="NameRow" runat="server"><div>Name:</div></div>
<div class="attribute" id="EmailRow" runat="server"><div>Email:</div></div>
<div class="attribute" id="AddressRow" runat="server"><div>Address:</div></div>
<div class="attribute" id="CountyRow" runat="server"><div>County:</div></div>
<div class="attribute" id="PhoneRow" runat="server"><div>Phone:</div></div>
<div class="attribute" id="MobileRow" runat="server"><div>Mobile:</div></div>

<h2>Personal Information</h2>
<div class="attribute" id="BirthdayRow" runat="server"><div>Birthday:</div></div>
<div class="attribute" id="GenderRow" runat="server"><div>Gender:</div></div>
<div class="attribute" id="PersonalStatementRow" runat="server"><div>Personal Statement:</div></div>
<div class="attribute" id="ReligionRow" runat="server"><div>My Religion/Faith:</div></div>
<div class="attribute" id="HeritageRow" runat="server"><div>Heritage:</div></div>
<div class="attribute" ID="FirstGenerationRow" runat="server"><div>First Generation:</div></div>

<h2>Detailed Profile</h2>
<div class="attribute" ID="WorkingRow" runat="server"><div>Working:</div></div>
<div class="attribute" ID="VolunteeringRow" runat="server"><div>Volunteering:</div></div>
<div class="attribute" ID="MyChallengeRow" runat="server"><div>My Challenge:</div></div>
<div class="attribute" ID="MyGiftRow" runat="server"><div>My Gift:</div></div>
<div class="attribute" ID="FiveWordsRow" runat="server"><div>5 Words That Describe Me:</div></div>
<div class="attribute" ID="FiveSkillsRow" runat="server"><div>5 Skills I Have:</div></div>
<div class="attribute" ID="OrganizationsRow" runat="server"><div>Organization Affiliations:</div></div>
<div class="attribute" ID="CompaniesRow" runat="server"><div>Company Affiliations:</div></div>

<h2>Academic Information</h2>
<div class="attribute" ID="TypeofStudent" runat="server"><div>Type of Student:</div></div>
<div class="attribute" ID="CurrentHighSchool" runat="server"><div>Current High School:</div></div>
<div class="attribute" ID="CurrentSchoolDistrict" runat="server"><div>Current School District:</div></div>
<div class="attribute" ID="ConsideringSchoolTypesRow" runat="server"><div>Considering School Types:</div></div>
<div class="attribute" ID="LengthofProgramsRow" runat="server"><div>Length of Programs:</div></div>
<div class="attribute" ID="AcademicProgramsRow" runat="server"><div>Academic Program:</div></div>
<div class="attribute" ID="EnrollmentStatusRow" runat="server"><div>Enrollment Status:</div></div>
<div class="attribute" ID="TypeofCollegeStudentRow" runat="server"><div>Type of College Student:</div></div>
<div class="attribute" ID="CurrentCollegeRow" runat="server"><div>Current College:</div></div>
<div class="attribute" ID="CollegeLocationPreferenceRow" runat="server"><div>College Location Preference:</div></div>
<div class="attribute" ID="CollegesRow" runat="server"><div>Colleges:</div></div>

<h2>Academic Performance</h2>
<div class="attribute" ID="CurrentGPA" runat="server"><div>Current GPA:</div></div>
<div class="attribute" ID="CurrentClassRank" runat="server"><div>Class Rank:</div></div>
<div class="attribute" ID="HonorsRow" runat="server"><div>Honors:</div></div>
<div class="attribute" ID="APCreditsRow" runat="server"><div>AP Credits:</div></div>
<div class="attribute" ID="IBCreditsRow" runat="server"><div>IB Credits:</div></div>

<div id="SATScores" runat="server">
    <h3>SAT Scores</h3>
    <div class="attribute" ID="SATCriticalReading" runat="server"><div>Critical Reading:</div></div>
    <div class="attribute" ID="SATWriting" runat="server"><div>Writing:</div></div>
    <div class="attribute" ID="SATMathematics" runat="server"><div>Mathematics:</div></div>
    <div class="attribute" ID="SATCumulative" runat="server"><div>Cumulative:</div></div>
</div>

<div id="ACTScores" runat="server">
<h3>ACT Scores</h3>
    <div class="attribute" ID="ACTReading" runat="server"><div>Reading:</div></div>
    <div class="attribute" ID="ACTEnglish" runat="server"><div>English:</div></div>
    <div class="attribute" ID="ACTMathematics" runat="server"><div>Mathematics:</div></div>
    <div class="attribute" ID="ACTScience" runat="server"><div>Science:</div></div>
    <div class="attribute" ID="ACTCumulative" runat="server"><div>Cumulative:</div></div>
</div>

<h2>Interests</h2>
<div class="attribute" ID="FieldsOfStudyRow" runat="server"><div>Fields of Study:</div></div>
<div class="attribute" ID="CareersRow" runat="server"><div>Careers:</div></div>
<div class="attribute" ID="HobbiesRow" runat="server"><div>Hobbies:</div></div>
<div class="attribute" ID="ClubsRow" runat="server"><div>Clubs:</div></div>
<div class="attribute" ID="SportsRow" runat="server"><div>Sports:</div></div>


<h2>Financial Information</h2>
<div class="attribute" ID="SupportTypesRow" runat="server"><div>Support Types:</div></div>
<div class="attribute" ID="FinancialChallangeRow" runat="server"><div>Financial Challange:</div></div>
<div class="attribute" ID="FAFSACompletedRow" runat="server"><div>FAFSA Completed:</div></div>
<div class="attribute" ID="FAFSAEFCRow" runat="server"><div>FAFSA EFC Completed:</div></div>

<h2>Additional Requirements</h2>
<table class="viewonlyTable" runat="server" id="AddtionalRequirementsTable">
    <tr>
        <td><label class="captionMedium"></label></td>
        <td></td>
    </tr>
</table>
      
<table class="viewonlyTable" runat="server" id="QuestionsTable">
    <tr>
        <td><label class="captionMedium">Please answer following questions:</label></td>
        <td></td>
    </tr>                             
</table>  

<table class="viewonlyTable" runat="server" id="FormsTable">
    <tr>
    <td><label class="captionMedium">Application Attachments:</label></td>
    <td></td>
    </tr>      
    <tr>
        <td><form id="Form1" runat="server"><sb:FileList id="applicationAttachments" runat="server" /></form></td>
        
    </tr> 
</table>

<div id="Footer">
 <p>TheWashBoard.org is a free web-based tool for Washington students seeking college scholarships. We make scholarship searching simple.</p> 
 <p>Create a profile and review your scholarship matches. Register Today at <a class="GreenLink" href=http://www.theWashBoard.org>theWashBoard.org</a></p>
 <br />
 <p class="GrayText">Copyright © 2009 Washington Scholarship Coalition. All rights reserved.</p>
</div>