﻿using System;
using System.Web.Security;
using System.Web.UI;
using Common.Logging;
using ScholarBridge.Business;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Contact;

namespace ScholarBridge.Web.Common
{
    public delegate void UserSaved(User user);
    public delegate void FormCanceled();

    public partial class UserForm : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserForm));

        public IUserService UserService { get; set; }

        public event UserSaved UserSaved;
        public event FormCanceled FormCanceled;

        protected void Page_Load(object sender, EventArgs e)
        {
 
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                try
                {
                    // email for both username and email
                    Membership.CreateUser(UserName.Text, Membership.GeneratePassword(8, 3), UserName.Text);

                    var currentUser = UserService.FindByEmail(UserName.Text);
                    currentUser.Username = UserName.Text;
                    currentUser.Name.FirstName = FirstName.Text;
                    currentUser.Name.MiddleName = MiddleName.Text;
                    currentUser.Name.LastName = LastName.Text;
                	currentUser.Phone = GetPhone(PhoneNumber.Text);
					currentUser.Fax = GetPhone(FaxNumber.Text);
					currentUser.OtherPhone = GetPhone(OtherPhoneNumber.Text);

                    if (null != UserSaved)
                    {
                        UserSaved(currentUser);
                    }
                } 
                catch (MembershipCreateUserException mcex)
                {
                    log.Warn("Failed to create user.", mcex);
                    SetError(mcex.Message, mcex.StatusCode);
                }
            }
        }

		private PhoneNumber GetPhone(string p)
		{
			return String.IsNullOrEmpty(p) ? null : new PhoneNumber(PhoneNumber.Text);
		}

        private void SetError(string message, MembershipCreateStatus code)
        {
            errorMessage.Visible = true;
            if (code == MembershipCreateStatus.DuplicateEmail 
                || code == MembershipCreateStatus.DuplicateUserName)
            {
                errorMessage.Text = "Specified email address is already in use.";
            }
            else
            {
                errorMessage.Text = "Unable not create the user. " + message; 
            }
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            if (null != FormCanceled)
            {
                FormCanceled();
            }
        }
    }
}