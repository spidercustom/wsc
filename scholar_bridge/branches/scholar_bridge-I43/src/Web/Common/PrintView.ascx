﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintView.ascx.cs" Inherits="ScholarBridge.Web.Common.PrintView" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<asp:PlaceHolder ID="ScreenViewContainer" runat="server">
    <div id="PrintPreview"><a id="printViewLink" runat="server" target="_blank" class="button"><span>Print Preview</span></a></div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="PrintViewContainer" runat="server">
 <div id="PreviewOptions" class="non-printable">
    <sbCommon:AnchorButton ID="sendToFriendBtn" NavigateUrl="mailto:"  runat="server" Text="Send to a friend" />
     <a class="button" onclick="window.print();"><span>Print This Page</span></a>
 </div>
</asp:PlaceHolder>