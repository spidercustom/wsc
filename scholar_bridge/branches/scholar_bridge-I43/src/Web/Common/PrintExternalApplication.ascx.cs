﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Lookup;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.ApplicationParts;

namespace ScholarBridge.Web.Common
{
    public partial class PrintExternalApplication : UserControl
    {
        public Application  ApplicationToView{ get; set; }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Url.Text = ApplicationToView.Scholarship.OnlineApplicationUrl;

            PersonalInfoName.Text = ApplicationToView.Seeker.Name.NameFirstLast;
            PersonalInfoEmail.Text = ApplicationToView.Seeker.User.Email;
             if (null != ApplicationToView.Seeker.Address)
                 PersonalInfoAddress.Text = ApplicationToView.Seeker.Address.ToString().Replace("\r\n", "<br/>\r\n");
        }

        
    }
}