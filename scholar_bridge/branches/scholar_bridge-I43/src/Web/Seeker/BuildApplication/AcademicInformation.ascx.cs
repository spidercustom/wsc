﻿using System;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Extensions;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using System.Web;
using Spring.Web.UI;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class AcademicInformation : WizardStepUserControlBase<Domain.Application>
    {
        public IApplicationService ApplicationService { get; set; }
        public IUserContext UserContext { get; set; }

        Domain.Application application;
        Domain.Application ApplicationInContext
        {
            get
            {
                if (application == null)
                    application = Container.GetDomainObject();
                if (application == null)
                    throw new InvalidOperationException("There is no application in context");
                return application;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ApplicationInContext == null)
                throw new InvalidOperationException("There is no application in context"); 

            if (!IsPostBack)
            {
                PopulateScreen();
            }
            else
            {
                SetHonorsControlState();
                SetIBCreditsControlState();
                SetAPCreditsControlState();
                SetSATandACTScoreEvent();
            }
		}

        private void PopulateScreen()
        {
            FirstGenerationControl.Checked = ApplicationInContext.FirstGeneration;

            if (null != ApplicationInContext.CurrentSchool)
            {
                CurrentStudentGroupControl.StudentGroup = ApplicationInContext.CurrentSchool.LastAttended;
                CurrentStudentGroupControl.Years = ApplicationInContext.CurrentSchool.YearsAttended;

                if (null != ApplicationInContext.CurrentSchool.College)
                    CollegeControlDialogButton.Keys = ApplicationInContext.CurrentSchool.College.Id.ToString();
                CollegeOtherControl.Text = ApplicationInContext.CurrentSchool.CollegeOther;

                if (null != ApplicationInContext.CurrentSchool.HighSchool)
                    HighSchoolControlDialogButton.Keys = ApplicationInContext.CurrentSchool.HighSchool.Id.ToString();
                HighSchoolOtherControl.Text = ApplicationInContext.CurrentSchool.HighSchoolOther;

                if (null != ApplicationInContext.CurrentSchool.SchoolDistrict)
                    SchoolDistrictControlLookupDialogButton.Keys = ApplicationInContext.CurrentSchool.SchoolDistrict.Id.ToString();
                SchoolDistrictOtherControl.Text = ApplicationInContext.CurrentSchool.SchoolDistrictOther;

                if (null != ApplicationInContext.CurrentSchool.TypeOfCollegeStudent)
                    SeekerStudentCollegeTypeControl.StudentGroup = ApplicationInContext.CurrentSchool.TypeOfCollegeStudent;
            }

            if (null != ApplicationInContext.SeekerAcademics)
            {
                CollegesAppliedControlDialogButton.Keys = ApplicationInContext.SeekerAcademics.CollegesApplied.CommaSeparatedIds();
                CollegesAppliedOtherControl.Text = ApplicationInContext.SeekerAcademics.CollegesAppliedOther;
                InWashingtonCheckbox.Checked = ApplicationInContext.SeekerAcademics.IsCollegeAppliedInWashington;
                OutOfStateCheckbox.Checked = ApplicationInContext.SeekerAcademics.IsCollegeAppliedOutOfState;

                //CollegesAcceptedControlDialogButton.Keys = ApplicationInContext.SeekerAcademics.CollegesAccepted.CommaSeparatedIds();
                //CollegesAcceptedOtherControl.Text = ApplicationInContext.SeekerAcademics.CollegesAcceptedOther;

				if (ApplicationInContext.SeekerAcademics.GPA.HasValue)
					GPAControl.Amount = (decimal)ApplicationInContext.SeekerAcademics.GPA.Value;
				if (ApplicationInContext.SeekerAcademics.ClassRank != null)
					ClassRankControl.SelectedValue = ApplicationInContext.SeekerAcademics.ClassRank;

				SchoolTypeControl.SelectedValues = (int)ApplicationInContext.SeekerAcademics.SchoolTypes;
				AcademicProgramControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.AcademicPrograms;
				SeekerStatusControl.SelectedValue = (int)ApplicationInContext.SeekerAcademics.SeekerStatuses;
				//ProgramLengthControl.SelectedValue = (int)SeekerInContext.SeekerAcademics.ProgramLengths;

				if (null != ApplicationInContext.SeekerAcademics.SATScore)
                {
                    SATReadingControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.CriticalReading.Value;
                    SATWritingControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Writing.Value;
                    SATMathControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Mathematics.Value;
                    var SATSum = ApplicationInContext.SeekerAcademics.SATScore.CriticalReading.Value
                                                     + ApplicationInContext.SeekerAcademics.SATScore.Writing.Value + ApplicationInContext.SeekerAcademics.SATScore.Mathematics.Value;

                    if (ApplicationInContext.SeekerAcademics.SATScore.Commulative.HasValue)
                    {
                        if (ApplicationInContext.SeekerAcademics.SATScore.Commulative.Value > 0)
                            ACTScoreCommulativeControl.Amount = ApplicationInContext.SeekerAcademics.SATScore.Commulative.Value;
                        else
                            SATScoreCommulativeControl.Amount = SATSum;

                    }
                    else
                    {
                        SATScoreCommulativeControl.Amount = SATSum;
                    }


                    hiddenSATCommulative.Value = SATScoreCommulativeControl.Text;
                }
                if (null != ApplicationInContext.SeekerAcademics.ACTScore)
                {
                    ACTEnglishControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.English.Value;
                    ACTMathControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Mathematics.Value;
                    ACTReadingControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Reading.Value;
                    ACTScienceControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Science.Value;
                    var ACTSum = ApplicationInContext.SeekerAcademics.ACTScore.English.Value +
                        ApplicationInContext.SeekerAcademics.ACTScore.Mathematics.Value + ApplicationInContext.SeekerAcademics.ACTScore.Reading.Value
                        + ApplicationInContext.SeekerAcademics.ACTScore.Science.Value;

                    if (ApplicationInContext.SeekerAcademics.ACTScore.Commulative.HasValue)
                    {
                        if (ApplicationInContext.SeekerAcademics.ACTScore.Commulative.Value > 0)
                            ACTScoreCommulativeControl.Amount = ApplicationInContext.SeekerAcademics.ACTScore.Commulative.Value;
                        else
                            ACTScoreCommulativeControl.Amount = ACTSum;
                    }
                    else
                    { ACTScoreCommulativeControl.Amount = ACTSum; }

                    hiddenACTCommulative.Value = ACTScoreCommulativeControl.Text;
                }


                APCreditsTextControl.Text = ApplicationInContext.SeekerAcademics.APCreditsDetail;
                IBCreditsTextBoxControl.Text = ApplicationInContext.SeekerAcademics.IBCreditsDetail;
                HonorsTextControl.Text = ApplicationInContext.SeekerAcademics.HonorsDetail;

                HonorsCheckboxControl.Checked = ApplicationInContext.SeekerAcademics.Honors;
                APCreditsCheckBoxControl.Checked = ApplicationInContext.SeekerAcademics.APCredits;
                IBCreditsCheckBoxControl.Checked = ApplicationInContext.SeekerAcademics.IBCredits;


                HonorsCheckboxControl.Attributes.Add("onclick", HonorsTextControl.ClientID + ".disabled = ! " + HonorsCheckboxControl.ClientID + ".checked;");
                APCreditsCheckBoxControl.Attributes.Add("onclick", APCreditsTextControl.ClientID + ".disabled = ! " + APCreditsCheckBoxControl.ClientID + ".checked;");
                IBCreditsCheckBoxControl.Attributes.Add("onclick", IBCreditsTextBoxControl.ClientID + ".disabled = ! " + IBCreditsCheckBoxControl.ClientID + ".checked;");

                SetHonorsControlState();
                SetIBCreditsControlState();
                SetAPCreditsControlState();
                SetSATandACTScoreEvent();
            }
        }

        private void SetSATandACTScoreEvent()
        {
            var scriptnameTemplate = "SetUpdateEvent_{0}_script";
            var scriptTemplate = "$('#CONTROLID').blur(function(e) { return UpdateSATComulative('" + SATWritingControl.ClientID + "', '" + SATReadingControl.ClientID + "','" + SATMathControl.ClientID + "','" + hiddenSATCommulative.ClientID + "','" + SATScoreCommulativeControl.ClientID + "'); });";
            var page = HttpContext.Current.CurrentHandler as System.Web.UI.Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(SATWritingControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(SATWritingControl.ClientID), scriptTemplate.Replace("CONTROLID", SATWritingControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(SATReadingControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(SATReadingControl.ClientID), scriptTemplate.Replace("CONTROLID", SATReadingControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(SATMathControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(SATMathControl.ClientID), scriptTemplate.Replace("CONTROLID", SATMathControl.ClientID));

            SATWritingControl.Attributes.Add("CanNull", "true");
            SATReadingControl.Attributes.Add("CanNull", "true");
            SATMathControl.Attributes.Add("CanNull", "true");
            SATScoreCommulativeControl.Attributes.Add("CanNull", "true");
            SetACTScoreEvent();
        }
        private void SetACTScoreEvent()
        {
            var scriptnameTemplate = "SetUpdateEvent_{0}_script";
            var scriptTemplate = "$('#CONTROLID').blur(function(e) { return UpdateACTComulative('" + ACTEnglishControl.ClientID + "', '" + ACTReadingControl.ClientID + "','" + ACTMathControl.ClientID + "','" + ACTScienceControl.ClientID + "','" + hiddenACTCommulative.ClientID + "','" + ACTScoreCommulativeControl.ClientID + "'); });";
            var page = HttpContext.Current.CurrentHandler as System.Web.UI.Page;

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTEnglishControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTEnglishControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTEnglishControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTReadingControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTReadingControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTReadingControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTMathControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTMathControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTMathControl.ClientID));

            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered(page.GetType(), scriptnameTemplate.Build(ACTScienceControl.ClientID)))
                page.ClientScript.RegisterOnDocumentReadyBlock(page.GetType(), scriptnameTemplate.Build(ACTScienceControl.ClientID), scriptTemplate.Replace("CONTROLID", ACTScienceControl.ClientID));

        }
        public void SetHonorsControlState()
        {

            HonorsTextControl.Enabled = HonorsCheckboxControl.Checked;
        }
        public void SetAPCreditsControlState()
        {

            APCreditsTextControl.Enabled = APCreditsCheckBoxControl.Checked;
        }
        public void SetIBCreditsControlState()
        {

            IBCreditsTextBoxControl.Enabled = IBCreditsCheckBoxControl.Checked;
        }
        protected void SATWritingCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(SATWritingControl.Text);
            if (num != 0)
                ValidateSATScore(num, (CustomValidator)source, args);
        }

        protected void SATReadingCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(SATReadingControl.Text);
            if (num != 0)
                ValidateSATScore(num, (CustomValidator)source, args);
        }
        protected void SATMathCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(SATMathControl.Text);
            if (num != 0)
                ValidateSATScore(num, (CustomValidator)source, args);
        }

        protected void SATCommulativeCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {



            double num = 0;
            double.TryParse(hiddenSATCommulative.Value, out num);
            if (num != 0)
            {
                if (num > 2400 || num < 600)
                {
                    SATCommulativeCustomValidator.IsValid = false;
                    args.IsValid = false;
                }
            }
        }




        private void ValidateSATScore(double num, CustomValidator validator, ServerValidateEventArgs args)
        {

            if (num > 800 || num < 200)
            {
                validator.IsValid = false;
                args.IsValid = false;
            }
        }

        protected void ACTEnglishCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTEnglishControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTReadingCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTReadingControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTMathCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTMathControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTScienceCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            var num = double.Parse(ACTScienceControl.Text);

            ValidateACTScore(num, (CustomValidator)source, args);
        }

        protected void ACTCommulativeCustomValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {


            double num = 0;
            double.TryParse(hiddenACTCommulative.Value, out num);
            if (num != 0)
            {
                if (num > 144 || num < 0)
                {
                    ACTCommulativeCustomValidator.IsValid = false;
                    args.IsValid = false;
                }
            }
        }
        private void ValidateACTScore(double num, CustomValidator validator, ServerValidateEventArgs args)
        {

            if (num > 36 || num < 0)
            {
                validator.IsValid = false;
                args.IsValid = false;
            }
        }
	    #region WizardStepUserControlBase methods

		public override bool ValidateStep()
		{
			return true;
		}

        public override void Save()
        {
            PopulateObjects();
            ApplicationService.Update(ApplicationInContext);
        }

        public override void PopulateObjects()
        {
            ApplicationInContext.FirstGeneration = FirstGenerationControl.Checked;

            if (null == ApplicationInContext.SeekerAcademics)
            {
                ApplicationInContext.SeekerAcademics = new SeekerAcademics();
            }

            CollegesAppliedControlDialogButton.PopulateListFromSelection(ApplicationInContext.SeekerAcademics.CollegesApplied);
            ApplicationInContext.SeekerAcademics.CollegesAppliedOther = CollegesAppliedOtherControl.Text;

            ApplicationInContext.SeekerAcademics.IsCollegeAppliedInWashington = InWashingtonCheckbox.Checked;
            ApplicationInContext.SeekerAcademics.IsCollegeAppliedOutOfState = OutOfStateCheckbox.Checked;

            //CollegesAcceptedControlDialogButton.PopulateListFromSelection(ApplicationInContext.SeekerAcademics.CollegesAccepted);
            //ApplicationInContext.SeekerAcademics.CollegesAcceptedOther = CollegesAcceptedOtherControl.Text;

            ApplicationInContext.SeekerAcademics.Honors = HonorsCheckboxControl.Checked;
            ApplicationInContext.SeekerAcademics.APCredits = APCreditsCheckBoxControl.Checked;
            ApplicationInContext.SeekerAcademics.IBCredits = IBCreditsCheckBoxControl.Checked;


            ApplicationInContext.SeekerAcademics.GPA = (double)GPAControl.Amount;

            ApplicationInContext.SeekerAcademics.APCreditsDetail = APCreditsCheckBoxControl.Checked ? APCreditsTextControl.Text : "";
            ApplicationInContext.SeekerAcademics.IBCreditsDetail = IBCreditsCheckBoxControl.Checked ? IBCreditsTextBoxControl.Text : "";

            ApplicationInContext.SeekerAcademics.HonorsDetail = HonorsCheckboxControl.Checked ? HonorsTextControl.Text : "";

            if (ClassRankControl.SelectedValue != null)
                ApplicationInContext.SeekerAcademics.ClassRank = (ClassRank)ClassRankControl.SelectedValue;

            if (null == ApplicationInContext.SeekerAcademics.SATScore)
            {
                ApplicationInContext.SeekerAcademics.SATScore = new SatScore();
            }

            ApplicationInContext.SeekerAcademics.SATScore.CriticalReading = (int)SATReadingControl.Amount;
            ApplicationInContext.SeekerAcademics.SATScore.Writing = (int)SATWritingControl.Amount;
            ApplicationInContext.SeekerAcademics.SATScore.Mathematics = (int)SATMathControl.Amount;
            if ((ApplicationInContext.SeekerAcademics.SATScore.CriticalReading +
                ApplicationInContext.SeekerAcademics.SATScore.Writing + ApplicationInContext.SeekerAcademics.SATScore.Mathematics

                ) > 0)
            {

                ApplicationInContext.SeekerAcademics.SATScore.Commulative = 0;

            }
            else
            {
                ApplicationInContext.SeekerAcademics.SATScore.Commulative = (int)SATScoreCommulativeControl.Amount;
            }


            if (null == ApplicationInContext.SeekerAcademics.ACTScore)
            {
                ApplicationInContext.SeekerAcademics.ACTScore = new ActScore();
            }

            ApplicationInContext.SeekerAcademics.ACTScore.English = (int)ACTEnglishControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Mathematics = (int)ACTMathControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Reading = (int)ACTReadingControl.Amount;
            ApplicationInContext.SeekerAcademics.ACTScore.Science = (int)ACTScienceControl.Amount;

            if ((ApplicationInContext.SeekerAcademics.ACTScore.English +
               ApplicationInContext.SeekerAcademics.ACTScore.Mathematics + ApplicationInContext.SeekerAcademics.ACTScore.Reading
               + ApplicationInContext.SeekerAcademics.ACTScore.Science

               ) > 0)
            {

                ApplicationInContext.SeekerAcademics.ACTScore.Commulative = 0;

            }
            else
            {
                ApplicationInContext.SeekerAcademics.ACTScore.Commulative = (int)ACTScoreCommulativeControl.Amount;
            }
           
           
            if (null == ApplicationInContext.CurrentSchool)
            {
                ApplicationInContext.CurrentSchool = new CurrentSchool();
            }
            ApplicationInContext.CurrentSchool.LastAttended = CurrentStudentGroupControl.StudentGroup;

            ApplicationInContext.CurrentSchool.TypeOfCollegeStudent = SeekerStudentCollegeTypeControl.StudentGroup;

            ApplicationInContext.CurrentSchool.YearsAttended = CurrentStudentGroupControl.Years;

            ApplicationInContext.CurrentSchool.College = (College)CollegeControlDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.CollegeOther = CollegeOtherControl.Text;

            ApplicationInContext.CurrentSchool.HighSchool = (HighSchool)HighSchoolControlDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.HighSchoolOther = HighSchoolOtherControl.Text;

            ApplicationInContext.CurrentSchool.SchoolDistrict = (SchoolDistrict)SchoolDistrictControlLookupDialogButton.GetSelectedLookupItem();
            ApplicationInContext.CurrentSchool.SchoolDistrictOther = SchoolDistrictOtherControl.Text;

            ApplicationInContext.SeekerAcademics.SchoolTypes = (SchoolTypes)SchoolTypeControl.SelectedValues;
            ApplicationInContext.SeekerAcademics.AcademicPrograms = (AcademicPrograms)AcademicProgramControl.SelectedValue;
            ApplicationInContext.SeekerAcademics.SeekerStatuses = (SeekerStatuses)SeekerStatusControl.SelectedValue;
       }
		#endregion

    }
}