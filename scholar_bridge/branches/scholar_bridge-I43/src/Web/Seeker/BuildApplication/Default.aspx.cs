﻿using System;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.ApplicationParts;
using ScholarBridge.Web.Common;
using ScholarBridge.Web.Extensions;
using ScholarBridge.Web.Wizards;
using ScholarBridge.Web.Config;

namespace ScholarBridge.Web.Seeker.BuildApplication
{
	public partial class Default : SBBasePage, IWizardStepsContainer<Application>
	{
		#region consts
		private const string SUBMISSION_VALIDATION_ERRORS_DIV_ID = "SubmissionValidationErrors";

		private const string SUBMISSION_PAGE = "/Seeker/Applications/Submit.aspx?aid={0}";
		private const string DELETE_PAGE = "/Seeker/Applications/Delete.aspx?aid={0}";
		private const string DEFAULT_PAGE = "/Seeker/Applications/Show.aspx?aid={0}";
		private const string APPLICATION_ID = "aid";
		private const string SCHOLARSHIP_ID = "sid";
        private const string MY_APPLICATIONS_URL = "~/Seeker/Scholarships/";
        
	    private LinkGenerator linkGenerator;
	    private LinkGenerator LinkGenerator
	    {
	        get
	        {
	            if (linkGenerator == null)
	                linkGenerator = new LinkGenerator();
	            return linkGenerator;
	        }
	    }

	    #endregion

		#region properties

		public IApplicationService ApplicationService { get; set; }
		public IUserContext UserContext { get; set; }
		public IScholarshipService ScholarshipService { get; set; }

		private int CurrentApplicationId
		{
			get
			{
				int applicationId = this.GetIntegerPageParameterValue(ViewState, APPLICATION_ID, -1);
				return applicationId;
			}
			set
			{
				ViewState[APPLICATION_ID] = value;
			}
		}

	    private string ExitToUrl
	    {
            get
            {
                var result = (string) ViewState["EXIT-TO"];
                if (null == result)
                    return MY_APPLICATIONS_URL;
                return result;
            }
            set { ViewState["EXIT-TO"] = value; }
	    }

		#endregion

		#region Page Lifecycle Events
		protected void Page_Load(object sender, EventArgs e)
		{
			buildApplicationWizardTabIntegrator.TabChanging += (buildApplicationWizardTabIntegrator_TabChanging);
			Steps.ForEach(step => step.Container = this);

			if (!IsPostBack)
			{
				RemoveSubmitButtonIfAlreadySubmitted();

				CheckForDataChanges = true;
				var applicationToEdit = GetCurrentApplication();
                scholarshipName.Text = applicationInContext.Scholarship.Name;
                scholarshipProvider.Text = applicationInContext.Scholarship.Provider.Name;
                current_app_id.Text = CurrentApplicationId.ToString();

                BypassPromptIds.AddRange(
					new[]	{
														
								PreviousButton.ID,                            
								NextButton.ID,                            
								SaveAndExitButton.ID,
                                SubmitButton.ID,
                                DeleteButton.ID,
                                buildApplicationWizardTabIntegrator.ID
							});
				SkipDataCheckIds.Add(ScholarshipSearchBox.SEARCH_BOX_ID);

				if (applicationToEdit != null)
				{
					if (applicationToEdit.Seeker != UserContext.CurrentSeeker)
						throw new InvalidOperationException("Application does not belong to seeker in context");

					if (applicationToEdit.Stage == ApplicationStages.Submitted)
					{
						SuccessMessageLabel.SetMessage("Dear User, The application has already been submitted hence it can't be edited");
                        Response.Redirect(LinkGenerator.GetFullLink(DEFAULT_PAGE.Build(applicationToEdit.Id)));
					}


					ActiveStepIndex = WizardStepName.Basics.GetNumericValue();
				}
			}
		}

		/// <summary>
		/// Save the current tab data whenever we're in the process of navigating away
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buildApplicationWizardTabIntegrator_TabChanging(object sender, EventArgs e)
		{
			Save();
		}

		public Application GetCurrentApplication()
		{
			return CurrentApplicationId > 0 ? ApplicationService.GetById(CurrentApplicationId) : null;
		}

		private void RemoveSubmitButtonIfAlreadySubmitted()
		{
			var application = GetDomainObject();
			if (null != application && application.Stage == ApplicationStages.Submitted)
			{
				SubmitButton.Visible = false;
				//const string msg =
				//    "You have submitted your application information. Once saved, the changes will replace the previous information in your application. Do you want to update your application with these changes?";
				//SaveButton.Attributes.Add("onclick", String.Format("return confirmSaveDialog('{0}', 'Save Changes?')", msg));
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			PreviousButton.Enabled = ActiveStepIndex != 0;
			NextButton.Enabled = ActiveStepIndex != Steps.Length - 1;
		}

		#endregion

		#region Control event handlers

		protected void BuildApplicationWizard_ActiveStepChanged(object sender, EventArgs e)
		{
			WizardStepContainerCommon.NotifyStepActivated(this);
		}

		protected void PreviousButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
		{
			if (Save())
				GoPrior();
		}

		protected void NextButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
		{
			if (Save())
				GoNext();
		}

		protected void SubmitButton_Click(object sender, SaveConfirmButtonClickEventArgs e)
		{
            Page.Validate();
            if (!Page.IsValid)
                return;
			WizardStepContainerCommon.PopulateObjectsFromActiveStep(this);
			if (ValidateSubmitted())
			{
				Save();
                //check for attachements
                if (applicationInContext.Scholarship.Attachments.Count >0 || applicationInContext.Scholarship.AdditionalRequirements.Count >0)
                {
                    //Inform user to see if they have attached something or not.
                    if (applicationInContext.Attachments.Count ==0)
                    {
                        ClientSideDialogs.ShowDivAsYesNoDialog("warnAttachments",
                    LinkGenerator.GetFullLink(SUBMISSION_PAGE.Build(applicationInContext.Id)));
                        return;
                    }
                }

			    ClientSideDialogs.ShowDivAsYesNoDialog("confirmSubmit",
                    LinkGenerator.GetFullLink(SUBMISSION_PAGE.Build(applicationInContext.Id)));
			}
		}

        protected void SaveAndExit_Click(object sender, SaveConfirmButtonClickEventArgs e)
		{
            if (Save())
                Response.Redirect(ExitToUrl);
		}

		protected void SaveButton_Click(object sender, EventArgs e)
		{
			Save();
		}

		protected void DeleteButton_Click(object sender, EventArgs e)
		{
			ClientSideDialogs.ShowDivAsYesNoDialog("confirmDelete", LinkGenerator.GetFullLink(DELETE_PAGE.Build(applicationInContext.Id)));
		}
		#endregion

		#region Private Methods

		Application applicationInContext;
		public Application GetDomainObject()
		{
			if (applicationInContext == null)
			{

				applicationInContext = GetCurrentApplication();
				if (applicationInContext == null)
				{
					var currentScholarship = GetScholarshipInContext();
					if (currentScholarship == null)
						throw new InvalidOperationException("No Scholarship in context.");

					applicationInContext = ApplicationService.FindBySeekerandScholarship(UserContext.CurrentSeeker,
																						 currentScholarship);

					//Create New one
					if (applicationInContext == null)
					{
                        applicationInContext = new Application(ApplicationType.Internal)
						{
							//intialize default values here
							Seeker = UserContext.CurrentSeeker,
							Scholarship = currentScholarship,

						};
                        applicationInContext.CopyFromSeekerandScholarship();
						applicationInContext.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
						ApplicationService.SaveNew(applicationInContext);
						CurrentApplicationId = applicationInContext.Id;

					}
					else { CurrentApplicationId = applicationInContext.Id; }

				}
			}

			return applicationInContext;
		}

		public Scholarship GetScholarshipInContext()
		{
			int scholarshipId = this.GetIntegerPageParameterValue(ViewState, SCHOLARSHIP_ID, -1);
			return scholarshipId > 0 ? ScholarshipService.GetById(scholarshipId) : null;
		}

		private bool ValidateStep()
		{
			return WizardStepContainerCommon.ValidateStep(this);
		}

		private bool Save()
		{
			var application = GetDomainObject();
			if (!Page.IsValid || !ValidateStep())
				return false;
			if (application.Stage == ApplicationStages.Submitted && !ValidateSubmitted())
				return false;
			application.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
			WizardStepContainerCommon.SaveActiveStep(this);
			Dirty = false;
			return true;
		}

		private bool ValidateSubmitted()
		{
			var application = GetDomainObject();

			var results = application.ValidateSubmission();
			if (results.IsValid)
				return true;

			IssueListControl.DataSource = results;
			IssueListControl.DataBind();
			ClientSideDialogs.ShowDivAsDialog(SUBMISSION_VALIDATION_ERRORS_DIV_ID);
			return false;
		}

		#endregion

		#region IWizardStepsContainer<Application> Members

		public IWizardStepControl<Application>[] Steps
		{
			get
			{
				IWizardStepControl<Application>[] stepControls = BuildApplicationWizard.Views.FindWizardStepControls<Application>();
				return stepControls;
			}
		}

		public void GoPrior()
		{
			WizardStepContainerCommon.GoPrior(this);
		}

		public void GoNext()
		{
			WizardStepContainerCommon.GoNext(this);
		}

		public void Goto(int index)
		{
			WizardStepContainerCommon.Goto(this, index);
		}

		public int ActiveStepIndex
		{
			get { return BuildApplicationWizard.ActiveViewIndex; }
			set { BuildApplicationWizard.ActiveViewIndex = value; }
		}

		#endregion

	}
}
