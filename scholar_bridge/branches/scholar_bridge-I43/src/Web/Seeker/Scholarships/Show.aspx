﻿<%@ Page Language="C#" MasterPageFile="~/PrintView.master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Scholarships.Show" Title="Seeker | Scholarships | Show" %>
<%@ Register src="~/Common/PrintScholarship.ascx" tagname="PrintScholarship" tagprefix="sb" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PrintViewHead" runat="server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PrintViewPageHeader"  runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
    <form id="Form1" runat="server"><!-- Why is the form needed?! (To do webpostback, but why is that needed!?) -->
        <div id="PreviewOptions" class="non-printable">
            <a class="button" onclick="window.print();"><span>Print This Page</span></a>
            <sbCommon:AnchorButton ID="AddtoMyScholarshipBtn" runat="server"  Text="Star This" OnClick="AddtoMyScholarshipBt_Click"/>
            <sbCommon:AnchorButton ID="sendToFriendBtn" NavigateUrl="mailto:"  runat="server" Text="Send to a friend" />
        </div>
        <sb:ScholarshipTitleStripe id="scholarshipTitleStripeControl" runat="server" HideInPrintView="true" />
        <sb:PrintScholarship id="showScholarship" runat="server" />
    </form>
</asp:Content>
