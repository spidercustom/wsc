﻿<%@ Page Language="C#" MasterPageFile="~/PrintView.master" AutoEventWireup="true" CodeBehind="PrintScholarship.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.PrintScholarship" Title="Provider| Scholarships | Show" %>

<%@ Register src="~/Common/PrintView.ascx" tagname="PrintView" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register src="~/Common/EntityTitleStripe.ascx" tagname="ScholarshipTitleStripe" tagprefix="sb" %>
<%@ Register src="~/Common/PrintScholarship.ascx" tagname="PrintScholarship" tagprefix="sb" %>

<asp:Content ContentPlaceHolderID="PrintViewHead" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="PrintViewPageHeader" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">
<form runat="server">
    <div id="PreviewOptions" class="non-printable">
        <a class="button" onclick="window.print();"><span>Print This Page</span></a>
        <sbCommon:AnchorButton ID="EmailtoOthers" NavigateUrl="mailto:"  runat="server" Text="Email to Others" />
    </div>
    <sb:ScholarshipTitleStripe id="scholarshipTitleStripeControl" runat="server" HideInPrintView="true" />
    <div><sb:PrintScholarship id="showScholarship" runat="server" /></div>
</form>    
</asp:Content>
