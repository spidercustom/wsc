﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ScholarBridge.Common.Extensions
{
    public static class EnumExtensions
    {
        public const string COMMA_SEPEARATOR = ", ";

        //Oh! How do I make static extension method
        public static Dictionary<Int32, string> GetKeyValue(this Type @enumType)
        {
            var list = new List<SortableEnum>();
            var fieldInfos = enumType.GetFields();

            Array.ForEach(fieldInfos,
                          fieldInfo =>
                          {
                              string displayName = GetDisplayNameFromFieldInfo(fieldInfo);
                              int displayOrder = GetDisplayOrderFromFieldInfo(fieldInfo);
                              if (!string.IsNullOrEmpty(displayName) && !"value__".Equals(displayName))
                              {
                                  var item = new SortableEnum();
                                  item.Key = (int)fieldInfo.GetRawConstantValue();
                                  item.DisplayName = displayName;
                                  item.DisplayOrder = displayOrder;
                                  list.Add(item);
                              }


                          });
            var result = (from r in list orderby r.DisplayOrder select r).ToDictionary(n => n.Key, n => n.DisplayName);

            return result;
        }



        public static Dictionary<Int32, string> GetKeyValue(this Type @enumType,int[] ExcludeKeys)
        {

            var result = GetKeyValue(@enumType);


            foreach (int key in ExcludeKeys )
            {
                result.Remove(key);
            }
             
            return result;
        }

        private static string GetDisplayNameFromFieldInfo(this FieldInfo info)
        {
            var attributes = info.GetCustomAttributes(typeof(DisplayNameAttribute), false);
            string result = null;
            Array.ForEach(attributes,
                          attribute =>
                              {
                                  var displayNameAttribute =
                                      attribute as DisplayNameAttribute;
                                  if (displayNameAttribute != null)
                                      result =  displayNameAttribute.DisplayName;
                              });
            if (result == null)
                result = info.Name;
            return result;
        }

        private static int GetDisplayOrderFromFieldInfo(this FieldInfo info)
        {
            var attributes = info.GetCustomAttributes(typeof(DisplayOrderAttribute), false);
            int result = 0;
            Array.ForEach(attributes,
                          attribute =>
                          {
                              var displayOrderAttribute =
                                  attribute as DisplayOrderAttribute;
                              if (displayOrderAttribute != null)
                                  result = displayOrderAttribute.DisplayOrder;
                          });
             
            return result;
        }

        public static int GetNumericValue(this Enum @enum)
        {
            return (Convert.ToInt32(@enum));
        }

        public static string NumericValueString(this Enum @enum)
        {
            return (Convert.ToInt32(@enum)).ToString();
        }

        public static string GetDisplayName(this Enum @enum)
        {
            Type enumType = @enum.GetType();
            FieldInfo enumElementFieldInfo = enumType.GetField(@enum.ToString());
            
            return enumElementFieldInfo == null ? string.Empty : GetDisplayNameFromFieldInfo(enumElementFieldInfo);
        }
        
       

        public static string[] GetDisplayNames(this Enum @enum)
        {
            Type enumType = @enum.GetType();

            var displayNames = new List<string>();

            var enumValue = @enum.GetNumericValue();
            var values = Enum.GetValues(enumType);
            foreach (var val in values)
            {
                var intVal = (int) val;
                if ((enumValue & intVal) == intVal)
                {
                    string name = Enum.GetName(enumType, intVal);
                    FieldInfo enumElementFieldInfo = enumType.GetField(name);
                    displayNames.Add(GetDisplayNameFromFieldInfo(enumElementFieldInfo));
                }
            }


            return displayNames.ToArray();
        }

        public static string GetDisplayNames(this Enum @enum, string separator)
        {
            if (separator == null)
                separator = string.Empty;

            return String.Join(separator, @enum.GetDisplayNames());
        }

        public static bool IsIn(this Enum find, params object[] @in)
        {
            return Array.IndexOf(@in, find) > -1;
        }

        public static Dictionary<int, string> GetKeyValue(params Enum[] enumElements)
        {
            if (enumElements == null) throw new ArgumentNullException("enumElements");
            
            var result = new Dictionary<int, string>();
            enumElements.ForEach(o => {
                var constValue = o.GetNumericValue();
                var displayName = o.GetDisplayName();

                result.Add(constValue, displayName);
            });

            return result;
        }

        
        public static int SelectAll(Type flagEnumType) // cannot set expectation as enum, like where T : Enum
        {
            if (!flagEnumType.IsEnum) 
                throw new ArgumentException("Specified type is not enum");
            if (flagEnumType.GetCustomAttributes(typeof(FlagsAttribute),false).Length == 0)
                throw new ArgumentException("Specified enum is not of bit flag type");

            var fieldInfos = flagEnumType.GetFields();
            int result = 0;
            Array.ForEach(fieldInfos,
                          fieldInfo =>
                          {
                              string displayName = GetDisplayNameFromFieldInfo(fieldInfo);
                              if (!string.IsNullOrEmpty(displayName) && !"value__".Equals(displayName))
                                  result |= (int)fieldInfo.GetRawConstantValue();
                          });
            return result;
        }

       
    }

    public class SortableEnum
    {
        public int Key{ get; set;}
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
    }
}