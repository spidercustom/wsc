﻿

namespace ScholarBridge.Domain.ApplicationParts
{
    //handle if application is internal using wsc site or external using provider's online application url
    public enum ApplicationType
    {
        Internal,External
    }
}
