﻿
namespace ScholarBridge.Domain
{
    public enum SeekerStage
    {
        None,
        NotActivated,
        Published
    }
}
