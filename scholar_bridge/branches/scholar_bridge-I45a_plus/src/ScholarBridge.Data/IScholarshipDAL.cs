﻿using System;
using System.Collections.Generic;
using ScholarBridge.Domain;

namespace ScholarBridge.Data
{
    public interface IScholarshipDAL : IDAL<Scholarship>
    {
        Scholarship FindById(int id);
        Scholarship FindByBusinessKey(Provider provider, string name, int year);
        Scholarship Save(Scholarship scholarship);
        IList<Scholarship> FindByProvider(Provider provider, ScholarshipStage[] stages);
        IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStage[] stages);
        IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStage[] stages);
        IList<Scholarship> FindAllClosedSince(DateTime date);
        IList<Scholarship> FindByStage(ScholarshipStage stage);
		IList<Scholarship> FindAllDueOn(DateTime day);
		IList<Scholarship> GetAllScholarships();

        void Flush();

		string GetScholarshipListXml();

		int CountAllActivatedScholarships();
		int CountAllNotActivatedScholarships();
		int CountAllClosedScholarships();
    	int CountAllRejectedScholarships();
    }
}    