﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface IGenericLookupDAL<T> : IDAL<T> where T : ILookup 
    {
        IList<T> FindAll();
        T FindById(object id);
        IList<T> FindAll(IList<object> ids);

        List<KeyValuePair<string, string>> GetLookupItems(string userData);
    }
}
