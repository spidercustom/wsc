﻿using System;
using System.Collections.Generic;
using NHibernate;
using ScholarBridge.Domain;
using NHibernate.Criterion;

namespace ScholarBridge.Data.NHibernate
{
    public class ScholarshipDAL : AbstractDAL<Scholarship>, IScholarshipDAL
    {
       
        #region IScholarshipDAL Members

        public Scholarship FindByBusinessKey(Provider provider, string name, int year)
        {
            return CreateCriteria()
               .Add(Restrictions.Eq("Name", name))
               .Add(Restrictions.Eq("Provider", provider))
               .Add(Restrictions.Eq("AcademicYear.Year", year))
               .UniqueResult<Scholarship>();
        }
        
        public IList<Scholarship> FindByProvider(Provider provider, ScholarshipStage[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Provider", provider))
                .AddOrder(Order.Desc("Stage"))
                .AddOrder(Order.Asc("Name"))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByOrganizations(Provider provider, Intermediary intermediary, ScholarshipStage[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Provider", provider))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindByIntermediary(Intermediary intermediary, ScholarshipStage[] stages)
        {
            return CreateCriteria()
                .Add(Restrictions.In("Stage", stages))
                .Add(Restrictions.Eq("Intermediary", intermediary))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindAllClosedSince(DateTime date)
        {
            return CreateCriteria()
                .Add(Restrictions.IsNotNull("AwardPeriodClosed"))
                .Add(Restrictions.Between("AwardPeriodClosed", date, DateTime.Now))
                .List<Scholarship>();
        }

        public IList<Scholarship> FindAllDueOn(DateTime date)
        {
            return CreateCriteria()
                .Add(Restrictions.IsNotNull("ApplicationDueDate"))
                .Add(Restrictions.Between("ApplicationDueDate", date, date.AddDays(1).AddSeconds(-1)))
                .List<Scholarship>();
        }

    	public IList<Scholarship> GetAllScholarships()
    	{
    		return CreateCriteria().List<Scholarship>();
    	}

    	public IList<Scholarship> FindByStage(ScholarshipStage stage)
        {
            return CreateCriteria()
                 .Add(Restrictions.Eq( "Stage", stage))
                 .List<Scholarship>();
        }

        #endregion
        public static ScholarshipStage[] GetNonActivatedStages()
        {
            return new[]
                       {
                           ScholarshipStage.None,
                           ScholarshipStage.NotActivated,
                           ScholarshipStage.Rejected
                       };
        }

        public void Flush()
        {
            var cur = Session.FlushMode;
            try
            {
                Session.FlushMode = FlushMode.Never;
                Session.Flush();
            }
            finally
            {
                Session.FlushMode = cur;   
            }
        }

    	public string GetScholarshipListXml()
    	{
    		const string sql = @"
select
(
	select 
			'/' + dbo.ScholarshipUrlEncode(p.Name) + '/' + CAST(s.AcademicYear - 1 as CHAR(4)) + '-' + cast(s.AcademicYear as CHAR(4)) + '/' + dbo.ScholarshipUrlEncode(s.Name) as UrlKey,	
			s.Name, cast(s.AcademicYear - 1 as CHAR(4)) + '-' + CAST(s.AcademicYear as CHAR(4)) as AcademicYear, 
			s.ApplicationStartDate, 
			s.ApplicationDueDate, 
			s.MaximumAmount,
			p.Name as ProviderName, 
			isnull(s.DonorName, '') as DonorName,
			ISNULL(s.MaximumNumberOfAwards, '0') as NumberOfAwards,
			s.MissionStatement,
			s.SBScholarshipID
	from SBScholarship s
	inner join SBOrganization p on p.SBOrganizationID = s.SBProviderID
	left join SBOrganization i on i.SBOrganizationID = s.SBIntermediaryID
	where Stage = 'Activated' and AwardDate >= GETDATE()
	FOR XML PATH('scholarship'), ROOT('doc')
) as ScholarshipList 
";
			ISQLQuery query = Session.CreateSQLQuery(sql).AddScalar("ScholarshipList", NHibernateUtil.String);
    	    return  (string) query.UniqueResult();
    	}

    	public int CountAllActivatedScholarships()
    	{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ScholarshipStage.Activated))
				.Add(Restrictions.Gt("ApplicationDueDate", DateTime.Now))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

    	public int CountAllNotActivatedScholarships()
    	{
			return CreateCriteria()
				.Add(Restrictions.In("Stage", GetNonActivatedStages()))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

    	public int CountAllClosedScholarships()
    	{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ScholarshipStage.Activated))
				.Add(Restrictions.Lt("ApplicationDueDate", DateTime.Now))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
    	}

		#region IScholarshipDAL Members


		public int CountAllRejectedScholarships()
		{
			return CreateCriteria()
				.Add(Restrictions.Eq("Stage", ScholarshipStage.Rejected))
				.SetProjection(Projections.CountDistinct("Id"))
				.UniqueResult<int>();
		}

		#endregion
	}
}
