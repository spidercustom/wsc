﻿using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data.NHibernate
{
    public class RoleDAL : AbstractDAL<Role>, IRoleDAL
    {
        public Role FindByName(string name)
        {
            return UniqueResult("Name", name);
        }
    }
}