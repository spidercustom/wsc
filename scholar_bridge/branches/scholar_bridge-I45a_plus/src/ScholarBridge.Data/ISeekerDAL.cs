using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Data
{
    public interface ISeekerDAL : IDAL<Seeker>
    {
        Seeker FindByUser(User user);
    	int CountRegisteredSeekers();
    	int CountSeekersWithActiveProfiles();
    }
}