﻿using System.Collections.Generic;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data
{
    public interface ISchoolDistrictDAL : IGenericLookupDAL<SchoolDistrict>
    {
        IList<SchoolDistrict> FindByState(string stateAbbreviation);
    }
}
