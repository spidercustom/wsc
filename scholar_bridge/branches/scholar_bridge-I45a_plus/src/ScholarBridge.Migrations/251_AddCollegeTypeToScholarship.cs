﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(251)]
    public class AddCollegeTypeToScholarship : Migration
    {
        private const string TABLE_NAME = "SBScholarship";

        public readonly string[]  COLUMNS
            = { "MatchCriteriaCollegeType" };

        public override void Up()
        {

            Database.AddColumn(TABLE_NAME, COLUMNS[0], DbType.Int32, 0,ColumnProperty.NotNull,  1);
         }

        public override void Down()
        {
           Database.RemoveColumn(TABLE_NAME, COLUMNS[0]);
        }
    }
}