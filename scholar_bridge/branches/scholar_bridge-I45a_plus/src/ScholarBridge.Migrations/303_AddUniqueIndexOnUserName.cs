﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(303)]
	public class AddUniqueIndexOnUserName : Migration
	{
        public override void Up()
        {
			AddIndex();
		}

    	public override void Down()
    	{
			DropIndex();
		}

		private void AddIndex()
		{
			DropIndex();
			Database.ExecuteNonQuery(
				@"
/****** Object:  Index [IX_SBUser_UserName]    Script Date: 01/13/2010 16:31:12 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_SBUser_UserName] ON [dbo].[SBUser] 
(
	[Username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
"

				);
		}

		private void DropIndex()
    	{
			Database.ExecuteNonQuery(
				@"
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SBUser]') AND name = N'IX_SBUser_UserName')
	DROP INDEX [IX_SBUser_UserName] ON [dbo].[SBUser] WITH ( ONLINE = OFF )
				");
    	}
    }
}