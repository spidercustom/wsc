﻿using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(212)]
    public class RemoveScholarshipWordsSkills : Migration
    {
        public override void Up()
        {
            Database.RemoveTable("SBScholarshipSeekerVerbalizingWordRT");
            Database.RemoveTable("SBScholarshipSeekerSkillRT");

            Database.RemoveTable("SBSeekerSeekerVerbalizingWordRT");
            Database.RemoveTable("SBSeekerSeekerSkillRT");

            Database.RemoveTable("SBApplicationSeekerVerbalizingWordRT");
            Database.RemoveTable("SBApplicationSeekerSkillRT");

            Database.RemoveTable("SBSeekerVerbalizingWordLUT");
            Database.RemoveTable("SBSeekerSkillLUT");

            Database.AddColumn("SBSeeker", new Column("VerbalizingWords", DbType.String, 100));
            Database.AddColumn("SBSeeker", new Column("Skills", DbType.String, 100));

            Database.AddColumn("SBApplication", new Column("VerbalizingWords", DbType.String, 100));
            Database.AddColumn("SBApplication", new Column("Skills", DbType.String, 100));
        }

        public override void Down()
        {
            Database.RemoveColumn("SBSeeker", "VerbalizingWords");
            Database.RemoveColumn("SBSeeker", "Skills");

            Database.RemoveColumn("SBApplication", "VerbalizingWords");
            Database.RemoveColumn("SBApplication", "Skills");

            new SeekerVerbalizingWord().Up();
            new SeekerSkill().Up();

            new AddScholarshipSeekerVerbalizingWordRT().Up();
            new AddScholarshipSeekerSkillRT().Up();

            new AddSeekerSeekerVerbalizingWordRT().Up();
            new AddSeekerSeekerSkillRT().Up();

            new AddApplicationSeekerVerbalizingWordRT().Up();
            new AddApplicationSeekerSkillRT().Up();
        }
    }
}
