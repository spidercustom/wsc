using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(268)]
    public class AddSeekerCompanyRT : AddRelationTableBase
    {
        public override string TableName
        {
            get { return "SBSeekerCompanyRT"; }
        }
        protected override string FirstTableName
        {
            get { return "SBSeeker"; }
        }
        protected override string SecondTableName
        {
            get { return "SBCompanyLUT"; }
        }
    }
}