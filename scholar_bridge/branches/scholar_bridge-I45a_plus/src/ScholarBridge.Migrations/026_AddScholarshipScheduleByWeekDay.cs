﻿using System.Data;
using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(26)]
    public class AddScholarshipScheduleByWeekDay : AddTableBase
    {

        public const string TABLE_NAME = "SB_ScholarshipScheduleByWeekDay";
        public const string PRIMARY_KEY_COLUMN = "Id";

        public override string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }

        public override string PrimaryKeyColumn
        {
            get { return PRIMARY_KEY_COLUMN; }
        }

        public override Column[] CreateColumns()
        {
            return new[]
                       {  
                           new Column("Id", DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                           new Column("StartFromWeekDayOccurrence", DbType.Int16),
                           new Column("StartFromDayOfWeek", DbType.Int16),
                           new Column("StartFromMonth", DbType.Int16),
                           new Column("DueOnWeekDayOccurrence", DbType.Int16),
                           new Column("DueOnDayOfWeek", DbType.Int16),
                           new Column("DueOnMonth", DbType.Int16),
                           new Column("AwardOnWeekDayOccurrence", DbType.Int16),
                           new Column("AwardOnDayOfWeek", DbType.Int16),
                           new Column("AwardOnMonth", DbType.Int16)
                       };
        }

        public override ForeignKey[] CreateForeignKeys()
        {
            return null;
        }
    }
}
