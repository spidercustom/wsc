using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using ScholarBridge.Domain.Location;
using System.Linq;
using ScholarBridge.Domain.Lookup;
using ScholarBridge.Domain.SeekerParts;

namespace ScholarBridge.Domain.Tests
{
    [TestFixture]
    public class SeekerAddressTests
    {
		[Test]
		public void NullInstateAndOutofstateCityReturnsNullCity()
		{
			var a = new SeekerAddress
			{
			};
			Assert.IsNull(a.GetCity);
		}

		[Test]
		public void CityIsReturnedEvenIfStateIsNull()
		{
			var a = new SeekerAddress
			{
				CityInState = new City() { Name = "Seattle" }
			};
			Assert.AreEqual("Seattle", a.GetCity);

			a.CityInState = null;
			Assert.IsNull(a.GetCity);
			a.CityOutOfState = "Milwaukee";
			Assert.AreEqual("Milwaukee", a.GetCity);
		}


		[Test]
		public void CountyIsReturnedEvenIfStateIsNull()
		{
			var a = new SeekerAddress
			{
				CountyInState = new County() { Name = "Pierce" }
			};
			Assert.AreEqual("Pierce", a.GetCounty);

			a.CountyInState = null;
			Assert.IsNull(a.GetCounty);
			a.CountyOutOfState = "Brewington";
			Assert.AreEqual("Brewington", a.GetCounty);
		}

		[Test]
		public void ToStringProperlyFormatsSeekerInStateAddressWithStreet1Only()
		{
			var a = new SeekerAddress
			{
				Street = "123 Foo St",
				CityInState = new City() {Name="Algona"},
				CityOutOfState = null,
				State = new State { Abbreviation = "WA" },
				PostalCode = "53212"
			};

			var expected = @"123 Foo St
Algona, WA 53212";

			Assert.AreEqual(expected, a.ToString());
		}

		[Test]
		public void ToStringProperlyFormatsSeekerOutOfStateAddressWithStreet1Only()
		{
			var a = new SeekerAddress
			{
				Street = "123 Foo St",
				CityInState = null,
				CityOutOfState = "Milwaukee",
				State = new State { Abbreviation = "WI" },
				PostalCode = "53212"
			};

			var expected = @"123 Foo St
Milwaukee, WI 53212";

			Assert.AreEqual(expected, a.ToString());
		}

		[Test]
		public void ToStringProperlyFormatsSeekerInStateAddressWithBothStreets()
		{
			var a = new SeekerAddress
			{
				Street = "123 Foo St",
				Street2 = "Apt 2",
				CityInState = new City() { Name = "Algona" },
				CityOutOfState = null,
				State = new State { Abbreviation = "WA" },
				PostalCode = "53212"
			};

			var expected = @"123 Foo St
Apt 2
Algona, WA 53212";

			Assert.AreEqual(expected, a.ToString());
		}

		[Test]
		public void ToStringProperlyFormatsSeekerOutOfStateAddressWithBothStreets()
		{
			var a = new SeekerAddress
			{
				Street = "123 Foo St",
				Street2 = "Apt 2",
				CityOutOfState = "Milwaukee",
				State = new State { Abbreviation = "WI" },
				PostalCode = "53212"
			};

			var expected = @"123 Foo St
Apt 2
Milwaukee, WI 53212";

			Assert.AreEqual(expected, a.ToString());
		}

        [Test]
        public void validate_unfilled_Seekeraddress_required_ruleset()
        {
            var address = new SeekerAddress();
            var results = address.ValidateAsRequired();
            Assert.That(results.Count, Is.EqualTo(5));
            Assert.That(results.Any(result => result.Key.Equals("Street") && result.Validator is StringLengthValidator));
			Assert.That(results.Any(result => result.Key.Equals("GetCity") && result.Validator is NotNullValidator));
			Assert.That(results.Any(result => result.Key.Equals("GetCounty") && result.Validator is NotNullValidator));
			Assert.That(results.Any(result => result.Key.Equals("PostalCode") && result.Validator is StringLengthValidator));
            Assert.That(results.Any(result => result.Key.Equals("State") && result.Validator is NotNullValidator));
        }

		[Test]
		public void validate_fill_outofstate_address_with_required_ruleset()
		{
			var address = new SeekerAddress { Street = "Line-1", CityOutOfState = "Milwaukee", CountyOutOfState = "Brewer County", PostalCode = "12345", State = new State() { Abbreviation = "WI" } };
			var results = address.ValidateAsRequired();
			Assert.That(results.Count, Is.EqualTo(0));
			Assert.That(results.IsValid, Is.True);
		}

		[Test]
		public void validate_fill_instate_address_with_required_ruleset()
		{
			var address = new SeekerAddress { Street = "Line-1", CityInState = new City(){Name="Seattle"}, CountyInState = new County(){Name="King"}, PostalCode = "12345", State = new State() { Abbreviation = "WA" } };
			var results = address.ValidateAsRequired();
			Assert.That(results.Count, Is.EqualTo(0));
			Assert.That(results.IsValid, Is.True);
		}
	}
}