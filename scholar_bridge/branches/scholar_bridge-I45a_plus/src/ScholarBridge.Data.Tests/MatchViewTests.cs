﻿
using NUnit.Framework;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.ScholarshipParts;

namespace ScholarBridge.Data.Tests
{
	[TestFixture]
	public class MatchViewTests : TestBase
	{
		public SeekerDAL SeekerDAL { get; set; }
		public ScholarshipDAL ScholarshipDAL { get; set; }
		public ProviderDAL ProviderDAL { get; set; }
		public StateDAL StateDAL { get; set; }
		public UserDAL UserDAL { get; set; }

		private Seeker m_seeker;
		private Scholarship m_scholarship;

		private User m_user;
		private Provider m_provider;

        protected override void OnSetUpInTransaction()
        {
            m_user = UserDALTest.InsertUser(UserDAL, "foo@bar.com");
            m_provider = ProviderDALTest.InsertProvider(ProviderDAL, StateDAL, "TestProvider", m_user);
		}

		#region Match Tests

		//[Test]
		//public void seeker_matches_with_honors()
		//{
		//    const string honorCriterionName = "Honors";

		//    // step 1 - create a seeker and scholarship to match
		//    SetupScholarshipAndSeeker();

		//    // step 2 - turn off the specific match criterion (Honors in this case)
		//    m_scholarship.SeekerProfileCriteria.Honors = false;
		//    SetScholarshipAttributeUsage(SeekerProfileAttribute.Honor, ScholarshipAttributeUsageType.NotUsed);
		//    UpdateScholarship();

		//    m_seeker.SeekerAcademics.Honors = false;
		//    UpdateSeeker();

		//    // step 2a - check for match - s/b false
		//    var honorsMatch = GetMatchOnCriterion(honorCriterionName);
		//    Assert.AreEqual(0, honorsMatch.Count());

		//    //step 3 - turn on the criterion in the scholarship but not the seeker (
		//    m_scholarship.SeekerProfileCriteria.Honors = true;
		//    SetScholarshipAttributeUsage(SeekerProfileAttribute.Honor, ScholarshipAttributeUsageType.Minimum);
		//    UpdateScholarship();

		//    // step 3a - check for match - s/b false
		//    honorsMatch = GetMatchOnCriterion(honorCriterionName);
		//    Assert.AreEqual(0, honorsMatch.Count());

		//    // step 4 - set the seeker match criterion on
		//    m_seeker.SeekerAcademics.Honors = true;
		//    UpdateSeeker();

		//    honorsMatch = GetMatchOnCriterion(honorCriterionName);
		//    Assert.AreEqual(1, honorsMatch.Count());

		//}

		#endregion

		#region helper routines
		private void UpdateSeeker()
		{
			SeekerDAL.Update(m_seeker);
		}

		private void UpdateScholarship()
		{
			ScholarshipDAL.Save(m_scholarship);
		}

		private void SetScholarshipAttributeUsage(SeekerProfileAttribute attribute, ScholarshipAttributeUsageType usageType )
		{
			if (!usageType.Equals(ScholarshipAttributeUsageType.NotUsed))
			{
				var attributeUsage = new SeekerProfileAttributeUsage
				{
					Attribute = attribute,
					UsageType = usageType
				};
				m_scholarship.SeekerProfileCriteria.Attributes.Add(attributeUsage);
			}
			else
			{
				m_scholarship.SeekerProfileCriteria.Attributes.Remove(attribute);
			}
		}

		private void SetupScholarshipAndSeeker()
		{
			m_seeker = SeekerDALTest.InsertSeeker(SeekerDAL, m_user, StateDAL);

			m_scholarship = ScholarshipDALTest.CreateTestObject(m_user, m_provider, ScholarshipStage.Activated);
			ScholarshipDAL.Insert(m_scholarship);

		}
		#endregion
	}
}
