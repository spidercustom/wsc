﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Message
{
    public partial class Show : Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (null == Request["sent"])
            {
                messageView.Visible = true;
                sentMessageView.Visible = false;
            }
            else
            {
                messageView.Visible = false;
                sentMessageView.Visible = true;
            }
            
        }

        protected void CloseBtn_Click(object sender, EventArgs e)
        {
            PopupHelper.CloseSelf(false);
        }
    }
}
