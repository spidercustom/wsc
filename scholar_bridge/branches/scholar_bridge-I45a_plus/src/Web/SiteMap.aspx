﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="SiteMap.aspx.cs" Inherits="ScholarBridge.Web.SiteMap" Title="Site Map" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Site Map" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Site Map</h2>
    <asp:TreeView ID="TreeView1" runat="server" DataSourceID="siteMapDataSource" />
    <asp:SiteMapDataSource ID="siteMapDataSource"  runat="server" />
</asp:Content>
