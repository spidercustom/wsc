﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Web.Extensions;

namespace ScholarBridge.Web.Common
{
    public partial class OrgUserList : UserControl
    {

        public IList<User> Users { get; set; }
        public string LinkTo { get; set; }

        private LinkGenerator linker;
        private LinkGenerator Linker
        {
            get
            {
                if (linker == null)
                    linker = new LinkGenerator();
                return linker;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            orgUsers.DataSource = Users;
            orgUsers.DataBind();
        }

        protected void orgUsers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.IsRow())
            {
                var user=(User)e.Item.DataItem;
                var link = (LinkButton)e.Item.FindControl("linkeditUser");
                var url = Linker.GetFullLink(LinkTo, "popup=true&id=" + user.Id);
                link.Attributes.Add("onclick", String.Format("showinpopup('{0}'); return false;", url));

                var status = (Label)e.Item.FindControl("lblStatus");
                status.Text = user.IsDeleted ? "InActive" : "Active";
            }
        }
    }
}