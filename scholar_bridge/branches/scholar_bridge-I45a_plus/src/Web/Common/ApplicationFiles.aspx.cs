﻿using System;
using System.Web.UI;
using ScholarBridge.Business;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class ApplicationFiles : Page
    {
        public IApplicationService ApplicationService { get; set; }

        public int ApplicationId { get { return Int32.Parse(Request["id"]); } }

        private Application TheApplication {get; set;}

        protected void Page_Load(object sender, EventArgs e)
        {
            TheApplication = ApplicationService.GetById(ApplicationId);
            if (TheApplication != null)
            {
                applicationAttachments.Attachments = TheApplication.Attachments;
            }
        }

        protected void downloadAllBtn_OnClick(object sender, EventArgs e)
        {
            string directory = TheApplication.Seeker.Name.NameFirstLast;
            var attachments = TheApplication.Attachments;

            FileHelper.CreateAndSendZipFile(Response, attachments, directory);
        }
    }
}
