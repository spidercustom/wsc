﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Domain;

namespace ScholarBridge.Web.Common
{
    public partial class IntermediaryRelationshipList : UserControl
    {
        public IList<Relationship> Relationships { get; set; }
        public IRelationshipService RelationshipService { get; set; }
        private const string DEFAULT_PAGEURL = "~/Intermediary/Admin/#relationship-tab";
        public IUserContext UserContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Bind();
        }

        protected void Bind()
        {
            lstRelationships.DataSource = (from r in Relationships orderby r.Provider.Name select r).ToList();
            lstRelationships.DataBind();
        }

        protected void InactivateBtn_Click(object sender, ConfirmButtonClickEventArgs e)
        {
            if (!e.DialogResult) return;
            var btn = (ConfirmButton)sender;
            var id = int.Parse(btn.Attributes["value"]);
            var relationship = RelationshipService.GetById(id);
            if (!(relationship == null))
            {
                relationship.LastUpdate = new ActivityStamp(UserContext.CurrentUser);
                RelationshipService.InActivate(relationship);
                SuccessMessageLabel.SetMessage("Relationship Inactivated.");
                Response.Redirect(DEFAULT_PAGEURL);
            }
        }
        protected void lstRelationships_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) 
                return;

            var relationship = ((Relationship) ((ListViewDataItem) e.Item).DataItem);
            var btnInactivate = (ConfirmButton)e.Item.FindControl("InactivateBtn");
            if (relationship.Status == RelationshipStatus.Active)
            {
                btnInactivate.Attributes.Add("value", relationship.Id.ToString());
                btnInactivate.Visible = true;
            }
            else
            {
                btnInactivate.Visible = false;
            }

            SetLabel(e, "lblName", relationship.Provider.Name == null ? "" : relationship.Provider.Name.ToString());
            SetLabel(e, "lblStatus", relationship.Status.GetDisplayName());
            SetLabel(e, "lblPhone", relationship.Provider.Phone == null ? "" : relationship.Provider.Phone.Number);

            var admin = relationship.Provider.AdminUser;
            SetLabel(e, "lblAdminName", admin.Name == null ? "" : admin.Name.ToString());
            SetLabel(e, "lblAdminEmail", admin.Email);
        }

        private static void SetLabel(ListViewItemEventArgs e, string controlName, string value)
        {
            var lbl = (Literal)e.Item.FindControl(controlName);
            lbl.Text = value;
        }

        protected void lstRelationships_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
        }
    }
}