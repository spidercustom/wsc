using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;

namespace ScholarBridge.Web.Common
{
    public abstract class BaseScholarshipShow : UserControl
    {
        public Scholarship ScholarshipToView { get; set; }
        public string LinkTo { get; set; }

        public abstract int ResumeFrom { get; }

        protected virtual void SetupEditLinks(HtmlGenericControl linkArea1, HtmlGenericControl linkArea2)
        {
            linkArea1.Visible = CanEdit();
            linkArea2.Visible = CanEdit();
            
            string navurl = string.Format("{0}?id={1}&resumefrom={2}", LinkTo, ScholarshipToView.Id, ResumeFrom);
            ((HyperLink) linkArea1.FindControl("linkTop")).NavigateUrl = navurl;
            ((HyperLink) linkArea2.FindControl("linkBottom")).NavigateUrl = navurl;
        }

        protected bool CanEdit()
        {
            return !String.IsNullOrEmpty(LinkTo) && ScholarshipToView.CanEdit() &&
                (Roles.IsUserInRole(Role.PROVIDER_ROLE) || Roles.IsUserInRole(Role.INTERMEDIARY_ROLE));
        }
    }
}