﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.Seeker.Default" Title="Seeker" %>
<%@ Register Src="~/Common/SeekerProfileProgress.ascx" TagName="SeekerProfileProgress" TagPrefix="sb" %>
<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Seeker Home" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img alt="Seek Opportunity" height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <div style="float:right; width:300px; margin-left:20px;">
        <sb:SeekerProfileProgress ID="ProfileProgess" runat="server" ShowGotoProfile="false" />
    </div>
    <h2><img alt="Smarter Scholarship Matches" src="<%= ResolveUrl("~/images/titles/smarter_scholarship_matches.png") %>" width="332px" height="22px"/></h2>
    <p class="hookline">Now that you're logged in we can begin helping you find the scholarships best suited for you. Simply complete your profile and let us do the rest, or if you have matched scholarships, review them in My Matches.</p>

    <a href="<%= ResolveUrl("~/Seeker/Profile") %>">
        <img alt="Go To My Profile" src="<%= ResolveUrl("~/images/Btn_GotoMyProfile.gif") %>" width="144px" height="32px"/>
    </a>
    
    <div class="clear"></div>
    <hr style="margin:20px 0px;" />
    
    <div class="three-column">
        <div>
            <a href="<%= ResolveUrl("~/Seeker/Profile") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox01_DontForget.gif") %>" width="262px" height="51px"/>
                <img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox01.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">In order for us to give you the best possible scholarship matches, please complete your profile.</p>
            <a class="circle-icon-link" href="<%= ResolveUrl("~/Seeker/Profile") %>">Go to My Profile</a>
        </div>
        <div>
            <a href="<%= ResolveUrl("~/Seeker/Matches") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox02_WhatsNew.gif") %>" width="262px" height="51px"/>
                <img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox02.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">Your scholarship matches are ranked based on the number of eligibility criteria that match your profile. Start reviewing scholarships you are most likely to qualify for.</p>
            <a class="circle-icon-link" href="<%= ResolveUrl("~/Seeker/Matches") %>">Go to My Matches</a>
        </div>
        <div>
             <a href="<%= ResolveUrl("~/Seeker/Scholarships") %>">
                <img src="<%= ResolveUrl("~/images/BottomBox03_ApplyToday.gif") %>" width="262px" height="51px"/>
                <img src="<%= ResolveUrl("~/images/SeekerLoggedInBottomBox03.gif") %>" width="237px" height="86px"/>
            </a>
            <p style="min-height:56px;">Save scholarships to your scholarships of interest and we will send you reminders of upcoming application due dates!</p>
            <a class="circle-icon-link" href="<%= ResolveUrl("~/Seeker/Scholarships") %>">Go to My Applications</a>
        </div>
   </div>
</asp:Content>
