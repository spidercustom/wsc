<%@ Page Language="C#" MasterPageFile="~/Simple.Master" AutoEventWireup="true" CodeBehind="SubmitListChangeRequest.aspx.cs" Inherits="ScholarBridge.Web.Provider.Scholarships.SubmitListChangeRequest" Title="Submit List Change" %>
<%@ Register Assembly="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             Namespace="Microsoft.Practices.EnterpriseLibrary.Validation.Integration.AspNet"
             TagPrefix="elv" %>
             
<%@ Register src="~/Common/ListChangeRequest.ascx" tagname="ListChangeRequest" tagprefix="sb" %>
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>            
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            // hides the footer
            $('#Footer').hide();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" runat="server">

<sb:ListChangeRequest ID="ListChangeRequest1" runat="server" />
 
<sbCommon:AnchorButton ID="saveBtn" runat="server" text="Submit" causesvalidation="True"  
        onclick="saveBtn_Click" /> 
<sbCommon:AnchorButton ID="cancelBtn" runat="server" text="Cancel" causesvalidation="false" 
        onclick="cancelBtn_Click" />           
</asp:Content>
