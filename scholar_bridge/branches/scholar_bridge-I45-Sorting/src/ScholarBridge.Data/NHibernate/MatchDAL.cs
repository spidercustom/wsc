using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using ScholarBridge.Domain;

namespace ScholarBridge.Data.NHibernate
{
    public class MatchDAL : AbstractDAL<Match>, IMatchDAL
    {
        private static readonly SimpleExpression NOT_DELETED = Restrictions.Eq("IsDeleted", false);

        public IList<Match> FindAll(Application application)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Application", application))

                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus status)
        {
            return FindAll(seeker, new[] {status});
        }

        public IList<Match> FindAll(Seeker seeker, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.In("MatchStatus", status))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }

    	public IList<Match> FindAll(Scholarship scholarship, MatchStatus[] status)
        {
            return CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Scholarship", scholarship))
                .Add(Restrictions.In("MatchStatus", status))
                .AddOrder(Order.Desc("Rank"))
                .List<Match>();
        }
        public int CountAllCurrentMatches(Seeker seeker, bool excludeMatchesWithApplications)
        {
            var query = CreateCriteria()
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("AddedViaMatch", true));
            if (excludeMatchesWithApplications)
                query.Add(Restrictions.IsNull("Application"));
            query.SetProjection(Projections.CountDistinct("Id"));
			return	query.UniqueResult<int>();
              
        }
         

        public IList<Match> FindAllCurrentMatches( int startIndex, int rowCount,string sortExpression,Seeker seeker, bool excludeMatchesWithApplications )
        {
            var query = CreateCriteria().SetFirstResult(startIndex).SetMaxResults(rowCount)
                .Add(NOT_DELETED)
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("AddedViaMatch", true));
            if (excludeMatchesWithApplications)
                query.Add(Restrictions.IsNull("Application"));

            switch (sortExpression)
            {
                case "ScholarshipName":
                case "ScholarshipName ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("Name", true));
                    break;

                case "ScholarshipName DESC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("Name", false));

                    break;

                case "Rank":
                case "Rank ASC":
                    query.AddOrder(new Order("Rank", true));
                    break;

                case "Rank DESC":
                    query.AddOrder(new Order("Rank", false));
                    break;

                case "EligibilityCriteria":
                case "EligibilityCriteria ASC":
                    query.AddOrder(new Order("MinimumCriteriaPercent", true));
                    query.AddOrder(new Order("ScholarshipMinimumCriteriaCount", true));

                    break;

                case "EligibilityCriteria DESC":
                    query.AddOrder(new Order("MinimumCriteriaPercent", false));
                    query.AddOrder(new Order("ScholarshipMinimumCriteriaCount", false));
                    break;

                case "PreferredCriteria":
                case "PreferredCriteria ASC":

                    query.AddOrder(new Order("PreferredCriteriaPercent", true));
                    query.AddOrder(new Order("ScholarshipPreferredCriteriaCount", true));


                    break;

                case "PreferredCriteria DESC":
                    query.AddOrder(new Order("PreferredCriteriaPercent", false));
                    query.AddOrder(new Order("ScholarshipPreferredCriteriaCount", false));
                    break;
                case "ApplicationDueDate":
                case "ApplicationDueDate ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("ApplicationDueDate", true));
                    break;

                case "ApplicationDueDate DESC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("ApplicationDueDate", false));
                    break;

                case "NumberOfAwards":
                case "NumberOfAwards ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", true));
                    break;

                case "NumberOfAwards DESC":
                    query = query.CreateCriteria("Scholarship");

                    query.AddOrder(new Order("FundingParameters.MaximumNumberOfAwards", false));
                    break;

                case "ScholarshipAmount":
                case "ScholarshipAmount ASC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("MaximumAmount", true));
                    break;

                case "ScholarshipAmount DESC":
                    query = query.CreateCriteria("Scholarship");
                    query.AddOrder(new Order("MaximumAmount", false));
                    break;

                default:
                    throw new NotSupportedException(string.Format("SortExpression {0} not supported", sortExpression));
            }

            

            return query.List<Match>();
        }

        public Match Find(Seeker seeker, int scholarshipId)
        {
            return CreateCriteria()
                .Add(Restrictions.Eq("Seeker", seeker))
                .Add(Restrictions.Eq("Scholarship.Id", scholarshipId))
                .UniqueResult<Match>();
        }

		public void UpdateMatches(Seeker seeker)
		{
			IQuery query = Session.CreateSQLQuery("exec dbo.BuildSeekerMatches @SeekerId=:seekerId,@UpdateUserId=:userId");
			query.SetInt32("seekerId", seeker.Id);
			query.SetInt32("userId", seeker.User.Id);
			query.ExecuteUpdate();
		}

		public void UpdateMatches(Scholarship scholarship)
		{
			IQuery query = Session.CreateSQLQuery("exec dbo.BuildScholarshipMatches @ScholarshipId=:scholarshipId,@UpdateUserId=:userId");
			query.SetInt32("scholarshipId", scholarship.Id);
			query.SetInt32("userId", scholarship.LastUpdate.By.Id);
			query.ExecuteUpdate();
		}

		#region Method Overrides
		public new Match Insert(Match match)
		{
			match.ComputeRank();
			return base.Insert(match);
		}

		public new Match Update(Match match)
		{
			match.ComputeRank();
			return base.Update(match);
		}
		#endregion

    }
}
