﻿using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Data.NHibernate
{
    public class AdditionalRequirementDAL : LookupDAL<AdditionalRequirement>, IAdditionalRequirementDAL
    {
    }
}