﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScholarBridge.Web
{
    public class ControlHelper
    {
        public static void SetControlStateRecursive(Control c, bool state)
        {
            foreach (Control control in c.Controls)
            {
                if (control is WebControl)
                    (control as WebControl).Enabled = state;
                if (control.Controls.Count > 0)
                    SetControlStateRecursive(control, state);
            }
        }

        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }

        public static List<Control> GetControlsByType(
            Control ctl,
            Type type)
        {
            var controls = new List<Control>();

            foreach (Control childCtl in ctl.Controls)
            {
                if (childCtl.GetType() == type)
                    controls.Add(childCtl);

                if (childCtl.Controls != null)
                {
                    var childControls = GetControlsByType(childCtl, type);
                    foreach (Control childControl in childControls)
                    {
                        controls.Add(childControl);
                    }
                }
            }

            return controls;
        }

        public static Control GetParentByType(
            Control ctl,
            Type type)
        {
            var parent = ctl.Parent;

            while (parent != null)
            {
                if (parent.GetType() == type)
                    break;

                if (parent.Parent == null)
                {
                    parent = null;
                    break;
                }
                else
                {
                    parent = parent.Parent;
                }
            }

            return parent;
        }
    }
}
