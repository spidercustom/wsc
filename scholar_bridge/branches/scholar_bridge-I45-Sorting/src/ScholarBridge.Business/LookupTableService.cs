﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ScholarBridge.Data.NHibernate;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Lookup;
using Spring.Context.Support;

namespace ScholarBridge.Business
{
    public class LookupTableService : ILookupTableService
    {
        #region Public ILookupTableService Methods

        public IEnumerable GetLookupTableData(string qualifiedClassName)
        {
            if (string.IsNullOrEmpty(qualifiedClassName))
                return null;

            return BuildTableRowList(qualifiedClassName);
        }

        public List<Type> GetLookupTableList()
        {
            return GetClasses(typeof (LookupBase));
        }

        public void UpdateLookupTableData(string tableName, User user, string Name, string Description, bool Deprecated, int Id)
        {
            object dal;
            Type dalType = GetDALType(tableName, out dal);
            // first read in the table row
            ILookup tableRowObject = GetTableRowObject(dalType, dal, Id);
            // now update the row
            MethodInfo updateMethod = dalType.GetMethod("Update", new[] {Type.GetType(tableName)});
            tableRowObject.Name = Name;
            tableRowObject.Description = Description;
            tableRowObject.Deprecated = Deprecated;
            tableRowObject.LastUpdate = new ActivityStamp(user);
            updateMethod.Invoke(dal, new object[] {tableRowObject});
        }

        public void DeleteLookupTableData(string tableName, int Id)
        {
            object dal;
            Type dalType = GetDALType(tableName, out dal);
            // first read in the table row
            ILookup tableRowObject = GetTableRowObject(dalType, dal, Id);
            // now delete the row
            MethodInfo deleteMethod = dalType.GetMethod("Delete", new[] {Type.GetType(tableName)});
            deleteMethod.Invoke(dal, new object[] {tableRowObject});
        }

        public void InsertLookupTableData(string tableName, User user, string Name, string Description)
        {
            Type tableType = Type.GetType(tableName);
            ILookup lookupRow = (ILookup) Activator.CreateInstance(tableType);

            lookupRow.Name = Name;
            lookupRow.Description = Description;
            lookupRow.Deprecated = false;
            lookupRow.LastUpdate = new ActivityStamp(user);

            object dal;
            Type dalType = GetDALType(tableName, out dal);

            MethodInfo insertMethod = dalType.GetMethod("Insert", new[] {Type.GetType(tableName)});
            insertMethod.Invoke(dal, new object[] {lookupRow});
        }

        #endregion

        #region Private Methods

        private static ILookup GetTableRowObject(Type dalType, object dal, int Id)
        {
            MethodInfo findByIdMethod = dalType.GetMethod("FindById", new[] {typeof (int)});
            return (ILookup) findByIdMethod.Invoke(dal, new object[] {Id});
        }

        private static IEnumerable BuildTableRowList(string className)
        {
            object dal;
            Type dalType = GetDALType(className, out dal);

            MethodInfo findAllMethod = dalType.GetMethod("FindAll", new[] {typeof (bool)});

            return (IEnumerable) findAllMethod.Invoke(dal, new object[] {true});
        }

        private static Type GetDALType(string className, out object dal)
        {
            Type tableType = Type.GetType(className);

            Type lookupDalType = typeof (LookupDAL<>);
            var dalType = lookupDalType.MakeGenericType(tableType);

            var foundObjects = ContextRegistry.GetContext().GetObjectsOfType(Type.GetType(dalType.AssemblyQualifiedName));
            if (null == foundObjects || 0.Equals(foundObjects.Count))
                throw new ArgumentException(string.Format("Cannot find object of type {0} in spring container",
                                                          dalType.AssemblyQualifiedName));

            // What those days were, lines of code to retrive value from collection, 
            // We can probably live without putting ifs
            IEnumerator objectsEnumerable = foundObjects.Values.GetEnumerator();
            if (null == objectsEnumerable)
                throw new InvalidOperationException("Cannot retrive enumerator");
            if (!objectsEnumerable.MoveNext())
                throw new InvalidOperationException("Unexpectedly cannot move to next");
            dal = objectsEnumerable.Current;
            return dal.GetType();
        }

        #endregion

        #region Static Methods

        public static List<Type> GetClasses(Type baseType)
        {
            Assembly assem = Assembly.Load(baseType.Assembly.FullName);
            return (from a in assem.GetTypes() where a.IsSubclassOf(baseType) orderby a.Name select a).ToList();
        }

        #endregion
    }
}