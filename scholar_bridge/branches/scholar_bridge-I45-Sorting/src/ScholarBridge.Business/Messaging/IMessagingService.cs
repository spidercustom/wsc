using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Messaging
{
    public interface IMessagingService
    {

        void SendMessageToAdmin(Message message, MailTemplateParams templateParams, bool sendEmail);
        void SendEmailToAdmin(MessageType type, MailTemplateParams templateParams);

        void SendMessageFromAdmin(Message message, MailTemplateParams templateParams, bool sendEmail);

        void SendMessage(Message message, MailTemplateParams templateParams, bool sendEmail);

        void SendEmail(User toUser, MessageType type, MailTemplateParams templateParams, bool sendEmail);
        void SendEmail(string toEmail, MessageType type, MailTemplateParams templateParams, bool sendEmail);

        void DeleteRelatedMessages(Scholarship scholarship);
        void SaveNewSentMessage(Message message, MailTemplateParams templateParams);
    }
}