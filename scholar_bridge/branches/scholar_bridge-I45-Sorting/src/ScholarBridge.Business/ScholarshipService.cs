﻿using System;
using System.Linq;
using System.Collections.Generic;
using ScholarBridge.Business.Exceptions;
using ScholarBridge.Business.Messaging;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Data;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business
{
    public class ScholarshipService : IScholarshipService
    {
        private const int MAXIMUM_COPY_NAME_ALLOWED = 10;
        private const string COPY_NAME_TEMPLATE = "Copy ({0}) of {1}";

        public IScholarshipDAL ScholarshipDAL { get; set; }
        public IMatchService MatchService { get; set; }
        public ITemplateParametersService TemplateParametersService { get; set; }
        public IMessagingService MessagingService { get; set; }
        public IRoleDAL RoleService { get; set; }
        public string AttachmentsDirectory { get; set; }

        public Scholarship GetById(int id)
        {
            return ScholarshipDAL.FindById(id);
        }

        public Scholarship Save(Scholarship scholarship)
        {
            if (scholarship == null)
                throw new ArgumentNullException("scholarship");
            if (scholarship.Provider == null)
                throw new ArgumentNullException("scholarship.Provider");
            if (scholarship.Provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();

            return ScholarshipDAL.Save(scholarship);
        }

        public void Delete(Scholarship scholarship)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship");
            if (! scholarship.CanEdit())
                throw new ArgumentException("You can only delete an editable scholarship");

            // Rarely should there be messages sent to a deletable Scholarship, but just in case
            MessagingService.DeleteRelatedMessages(scholarship);
            foreach (var attachment in scholarship.Attachments)
            {
                attachment.RemoveFile(AttachmentsDirectory);
            }
            ScholarshipDAL.Delete(scholarship);
        }

        public Scholarship ScholarshipExists(Provider provider, string name, int year)
        {
            if (null == provider)
                throw new ArgumentNullException("provider");
            if (null == name)
                throw new ArgumentNullException("name");

            return ScholarshipDAL.FindByBusinessKey(provider, name, year);
        }
        public IList<Scholarship> GetBySearchCriteria(string criteria)
        {
            criteria = criteria.ToLower();
            // FIXME: Need to do the query in the database, this loads all active scholarships which could be really big
            var query = ScholarshipDAL.FindByStage(ScholarshipStage.Activated);
            return (from s in query
                    where s.Name.ToLower().Contains(criteria)
                          || (s.Provider == null ? "" : s.Provider.Name.ToLower()).Contains(criteria)
                          || (s.Intermediary == null ? "" : s.Intermediary.Name.ToLower()).Contains(criteria)
                          || (s.Donor == null ? "" : s.Donor.Name.ToLower()).Contains(criteria)
                    select s).ToList();
        }
        public IList<Scholarship> GetByProvider(int startIndex, int rowCount, string sortExpression, Provider provider, ScholarshipStage[] stages)
        {
            return ScholarshipDAL.FindByProvider(startIndex, rowCount, sortExpression, provider, stages);
        }

        public IList<Scholarship> GetByIntermediary(int startIndex, int rowCount, string sortExpression, Intermediary intermediary, ScholarshipStage[] stages)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindByIntermediary(startIndex, rowCount,sortExpression, intermediary, stages);
        }

        public IList<Scholarship> GetByOrganizations(int startIndex, int rowCount, string sortExpression, Provider provider, Intermediary intermediary, ScholarshipStage[] stages)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.FindByOrganizations(startIndex, rowCount, sortExpression, provider, intermediary, stages);
        }

        public int CountByProvider( Provider provider, ScholarshipStage[] stages)
        {
            return ScholarshipDAL.CountByProvider(  provider, stages);
        }

        public int CountByIntermediary(  Intermediary intermediary, ScholarshipStage[] stages)
        {
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.CountByIntermediary(  intermediary, stages);
        }

        public int CountByOrganizations(  Provider provider, Intermediary intermediary, ScholarshipStage[] stages)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (provider.ApprovalStatus != ApprovalStatus.Approved)
                throw new ProviderNotApprovedException();
            if (intermediary == null)
                throw new ArgumentNullException("intermediary");

            return ScholarshipDAL.CountByOrganizations(  provider, intermediary, stages);
        }

        public void ApproveOnlineApplicationUrl(Scholarship scholarship, User approver, User organizationUser)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }

            if (scholarship.IsOnlineApplicationApproved)
                return;

            scholarship.IsOnlineApplicationApproved = true;
            ScholarshipDAL.Update(scholarship);

            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipOnlineApplicationUrlApproved(scholarship, organizationUser, templateParams);

            var msg = new Message
                          {
                              MessageTemplate = MessageType.ScholarshipOnlineApplicationUrlApproved,
                              To = new MessageAddress {User = organizationUser},
                              From = new MessageAddress {User = approver},
                              LastUpdate = new ActivityStamp(approver)
                          };

            MessagingService.SendMessage(msg, templateParams, organizationUser.IsRecieveEmails);
        }

        public void RejectOnlineApplicationUrl(Scholarship scholarship, User rejector, User organizationUser)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }

            scholarship.IsOnlineApplicationApproved = false;
            ScholarshipDAL.Update(scholarship);


            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipOnlineApplicationUrlRejected(scholarship, organizationUser,
                                                                              templateParams);

            var msg = new Message
                          {
                              MessageTemplate = MessageType.ScholarshipOnlineApplicationUrlRejected,
                              To = new MessageAddress {User = organizationUser},
                              From = new MessageAddress {User = rejector},
                              LastUpdate = new ActivityStamp(rejector)
                          };

            MessagingService.SendMessage(msg, templateParams, organizationUser.IsRecieveEmails);
        }

        /// <summary>
        /// Was Approve but now is Activate Scholarship because there is no longer a
        /// 'Request Activation' process.
        /// </summary>
        /// <param name="scholarship"></param>
        /// <param name="approver"></param>
        public void Activate(Scholarship scholarship, User approver)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }

            if (scholarship.IsActivated())
                return;

            scholarship.Stage = ScholarshipStage.Activated;
            scholarship.ActivatedOn = DateTime.Now;
            ScholarshipDAL.Update(scholarship);
            ScholarshipDAL.Flush();

            MatchService.UpdateMatches(scholarship);

            // Send message to scholarship provider, intermediary (if present) and HECB admin that the scholarship has been approved.
            // first the provider
            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipApproved(scholarship, approver, templateParams);


            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.ScholarshipApproved,
                              RelatedScholarship = scholarship,
                              To = new MessageAddress {Organization = scholarship.Provider},
                              From = new MessageAddress {User = approver},
                              LastUpdate = new ActivityStamp(approver)
                          };
            MessagingService.SendMessage(msg, templateParams, true);

            // next, HECB admin
            msg = new ScholarshipMessage
                      {
                          MessageTemplate = MessageType.ScholarshipApproved,
                          RelatedScholarship = scholarship,
                          To = new MessageAddress {Role = RoleService.FindByName(Role.WSC_ADMIN_ROLE)},
                          From = new MessageAddress {User = approver},
                          LastUpdate = new ActivityStamp(approver)
                      };
            MessagingService.SendMessage(msg, templateParams, true);

            // finally, the intermediary if present
            if (scholarship.Intermediary != null)
            {
                msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.ScholarshipApproved,
                              RelatedScholarship = scholarship,
                              To = new MessageAddress {Organization = scholarship.Intermediary},
                              From = new MessageAddress {User = approver},
                              LastUpdate = new ActivityStamp(approver)
                          };
                MessagingService.SendMessage(msg, templateParams, true);
            }
        }

        public Scholarship CopyScholarship(Scholarship copyFrom)
        {
            var copiedScholarship = (Scholarship) copyFrom.Clone(AttachmentsDirectory);
            for (int copyIndex = 1; copyIndex < MAXIMUM_COPY_NAME_ALLOWED; copyIndex ++)
            {
                var newName = COPY_NAME_TEMPLATE.Build(copyIndex, copiedScholarship.Name);
                newName = newName.Length > 100 ? newName.Substring(0, 100) : newName;
                var foundScholarship = ScholarshipExists(copiedScholarship.Provider, newName,
                                                         copiedScholarship.AcademicYear.Year);
                bool copyNameExists = null != foundScholarship;
                if (!copyNameExists)
                {
                    copiedScholarship.Name = newName;
                    return copiedScholarship;
                }
            }

            throw new TooManyScholarshipCopyExistsException();
        }

        public void CloseAwardPeriod(Scholarship scholarship, DateTime time)
        {
            if (null == scholarship)
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");

            scholarship.AwardPeriodClosed = time;
            ScholarshipDAL.Update(scholarship);
        }

        public void NotifySeekersClosedSince(DateTime date)
        {
            var scholarships = ScholarshipDAL.FindAllClosedSince(date);
            foreach (var s in scholarships)
            {
                NotifySeekersOfClose(s);
            }
        }

        public void RequestOnlineApplicationUrlApproval(Scholarship scholarship, Organization organization, User user)
        {
            var templateParams = new MailTemplateParams();
            TemplateParametersService.RequestOnlineApplicationApproval(scholarship, organization, templateParams);
            templateParams.From = user.Email;
            var msg = new ScholarshipMessage
                          {
                              MessageTemplate = MessageType.RequestOnlineApplicationApproval,
                              RelatedScholarship = scholarship,
                              From = new MessageAddress {Organization = organization, User = user},
                              LastUpdate = new ActivityStamp(scholarship.LastUpdate.By)
                          };


            MessagingService.SendMessageToAdmin(msg, templateParams, true);
        }

        private void NotifySeekersOfClose(Scholarship scholarship)
        {
            // RULE: Send to all users with scholarships saved in My Scholarships
            var matches = MatchService.GetSavedMatches(scholarship);
            if (null != matches && matches.Count > 0)
            {
                var seekersToNotify = matches.Select(m => m.Seeker);

                var templateParams = new MailTemplateParams();
                TemplateParametersService.ScholarshipClosed(scholarship, templateParams);
                foreach (var seeker in seekersToNotify)
                {
                    var msg = new Message
                                  {
                                      MessageTemplate = MessageType.ScholarshipClosed,
                                      To = new MessageAddress {User = seeker.User},
                                      From = new MessageAddress {Organization = scholarship.Provider},
                                      LastUpdate = new ActivityStamp(scholarship.LastUpdate.By)
                                  };

                    MessagingService.SendMessage(msg, templateParams, seeker.User.IsRecieveEmails);
                }
            }
        }

        public void NotifySeekersScholarshipDue(int inDays)
        {
            var day = DateTime.Today.AddDays(inDays);
            var scholarships = ScholarshipDAL.FindAllDueOn(day);
            foreach (var s in scholarships)
            {
                NotifySeekersDue(s, inDays);
            }
        }

        private void NotifySeekersDue(Scholarship scholarship, int inDays)
        {
            // RULE: Send to all users with scholarships saved in My Scholarships that have not submitted an application
            var matches = MatchService.GetSavedMatches(scholarship);
            if (null != matches && matches.Count > 0)
            {
                var seekersToNotify =
                    matches.Where(m => null == m.Application || m.Application.Stage != ApplicationStage.Submitted).
                        Select(m => m.Seeker);

                var templateParams = new MailTemplateParams();
                TemplateParametersService.ApplicationDue(scholarship, inDays, templateParams);
                foreach (var seeker in seekersToNotify)
                {
                    var msg = new Message
                                  {
                                      MessageTemplate = MessageType.ApplicationDue,
                                      To = new MessageAddress {User = seeker.User},
                                      From = new MessageAddress {Organization = scholarship.Provider},
                                      LastUpdate = new ActivityStamp(scholarship.LastUpdate.By)
                                  };

                    MessagingService.SendMessage(msg, templateParams, seeker.User.IsRecieveEmails);
                }
            }
        }

        public void SendToFriend(Scholarship scholarship, string userComments, string toEmail, User sender, string domainName)
        {
            if (null == scholarship)
            {
                throw new ArgumentNullException("scholarship", "Scholarship can not be null");
            }
            if (null == scholarship)
            {
                throw new ArgumentNullException("sender", "Sender can not be null");
            }
            if (string.IsNullOrEmpty(toEmail))
            {
                throw new ArgumentNullException("toEmail", "Recipient can not be null");
            }

            var templateParams = new MailTemplateParams();
            TemplateParametersService.ScholarshipSendToFriend(scholarship, userComments, templateParams, domainName,
                                                              sender.Email);
            templateParams.From = "{0} ({1})<{2}>".Build(sender.Name.NameFirstLast, domainName, sender.Email);
            templateParams.To = toEmail;
            MessagingService.SendEmail(toEmail, MessageType.ScholarshipSendToFriend, templateParams, true);
        }

        public int CountAllActivated()
        {
            return ScholarshipDAL.CountAllActivatedScholarships();
        }

        public int CountAllNotActivated()
        {
            return ScholarshipDAL.CountAllNotActivatedScholarships();
        }

        public int CountAllClosed()
        {
            return ScholarshipDAL.CountAllClosedScholarships();
        }

        public int CountAllRejected()
        {
            return ScholarshipDAL.CountAllRejectedScholarships();
        }

        public IList<Scholarship> GetAllScholarships()
        {
            return ScholarshipDAL.GetAllScholarships();
        }
    }
}