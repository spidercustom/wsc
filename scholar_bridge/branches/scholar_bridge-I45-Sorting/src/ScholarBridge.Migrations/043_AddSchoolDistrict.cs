﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(43)]
    public class AddSchoolDistrict : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "SchoolDistrict";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
