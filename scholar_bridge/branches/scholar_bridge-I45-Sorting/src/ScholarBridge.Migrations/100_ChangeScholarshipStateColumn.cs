﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(100)]
    public class ChangeScholarshipStateColumn : Migration
    {
        public const string TABLE_NAME = "SBScholarshipStateRT";


        public override void Up()
        {
            Database.RenameColumn(TABLE_NAME, "ScholarshipID", "SBScholarshipID");
        }

        public override void Down()
        {
            Database.RenameColumn(TABLE_NAME, "SBScholarshipID", "ScholarshipID");
        }
    }
}
