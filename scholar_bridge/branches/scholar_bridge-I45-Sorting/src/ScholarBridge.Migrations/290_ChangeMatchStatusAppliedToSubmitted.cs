using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(290)]
	public class ChangeMatchStatusAppliedToSubmitted : Migration
    {
		public override void Up()
		{
			Database.ExecuteNonQuery(@"
update SBMatch
set MatchStatus = 'Submitted'
where MatchStatus='Applied'
			");
        }

		public override void Down()
		{
			Database.ExecuteNonQuery(@"
update SBMatch
set MatchStatus = 'Applied'
where MatchStatus='Submitted'
			");

		}

    }
}