using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(315)]
    public class ChangeApplicationView : Migration
    {
        public override void Up()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBApplicationView]");
            Database.ExecuteNonQuery(SBApplicationView);
        }

        public override void Down()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBApplicationView]");
            
        }

        public const string SBApplicationView = @"
CREATE View [dbo].[SBApplicationView]
as
Select a.*,
	
	 case when SBMatch.ScholarshipMinimumCriteriaCount = 0 then 100 else ((SBMatch.SeekerMinimumCriteriaCount * 100) / SBMatch.ScholarshipMinimumCriteriaCount)  end 
	AS MatchMinimumCriteriaPercent,
	 
	  case when SBMatch.ScholarshipPreferredCriteriaCount = 0 then 100 else ((SBMatch.SeekerPreferredCriteriaCount * 100) / SBMatch.ScholarshipPreferredCriteriaCount)  end 
	AS MatchPreferredCriteriaPercent,
	
	
	(SBUser.LastName +', '+SBUser.FirstName +' '+ SBUser.MiddleName) AS ApplicantName
from SBApplication a 

INNER JOIN SBMatch ON SBMatch.SBSeekerId=a.SeekerId And SBMatch.SBScholarshipId=a.ScholarshipId
INNER JOIN SBSeeker ON SBSeeker.SBSeekerId=a.SeekerId 
INNER JOIN SBUser ON SBUser.SBUserId=SBSeeker.UserId
";


        

    }
}