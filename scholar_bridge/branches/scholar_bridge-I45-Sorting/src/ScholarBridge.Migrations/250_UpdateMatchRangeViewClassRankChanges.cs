﻿using System;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
	[Migration(250)]
    public class UpdateMatchRangeViewClassRankChanges : Migration
	{
		public override void Up()
		{
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchRanges]");
			Database.ExecuteNonQuery(SBMatchRange);
			Database.ExecuteNonQuery(ChangeSATACTAttributesUp);
			Database.ExecuteNonQuery("Update SBSeeker set ProfileLastUpdateDate = '" + DateTime.Now + "'"); // force a re-match
		}

		public override void Down()
		{
			Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMatchRanges]");
			Database.ExecuteNonQuery(SbMatchRangeListOrig);
			Database.ExecuteNonQuery(ChangeSATACTAttributesDown);
		}

		private const string ChangeSATACTAttributesUp =
			@"

with SATACT (SBScholarshipID, AttributeIndex, UsageIndex) 
as 
(
	select SBScholarshipID, SBMatchCriteriaAttributeIndex, SBUsageTypeIndex
	from SBScholarshipMatchCriteriaAttributeUsageRT
	where SBMatchCriteriaAttributeIndex = 31
)
insert into SBScholarshipMatchCriteriaAttributeUsageRT
(SBScholarshipID, SBMatchCriteriaAttributeIndex, SBUsageTypeIndex	)
select SBScholarshipID, 36, UsageIndex from satact
where not exists (select * from SBScholarshipMatchCriteriaAttributeUsageRT mca
					where mca.SBScholarshipID = satact.SBScholarshipID and
							mca.SBMatchCriteriaAttributeIndex = 36)
union all
select SBScholarshipID, 37, UsageIndex from satact
where not exists (select * from SBScholarshipMatchCriteriaAttributeUsageRT mca
					where mca.SBScholarshipID = satact.SBScholarshipID and
							mca.SBMatchCriteriaAttributeIndex = 37)
union all
select SBScholarshipID, 38, UsageIndex from satact
where not exists (select * from SBScholarshipMatchCriteriaAttributeUsageRT mca
					where mca.SBScholarshipID = satact.SBScholarshipID and
							mca.SBMatchCriteriaAttributeIndex = 38);

with SATACT (SBScholarshipID, AttributeIndex, UsageIndex) 
as 
(
	select SBScholarshipID, SBMatchCriteriaAttributeIndex, SBUsageTypeIndex
	from SBScholarshipMatchCriteriaAttributeUsageRT
	where SBMatchCriteriaAttributeIndex = 32
)
insert into SBScholarshipMatchCriteriaAttributeUsageRT
(SBScholarshipID, SBMatchCriteriaAttributeIndex, SBUsageTypeIndex	)
select SBScholarshipID, 39, UsageIndex from satact
where not exists (select * from SBScholarshipMatchCriteriaAttributeUsageRT mca
					where mca.SBScholarshipID = satact.SBScholarshipID and
							mca.SBMatchCriteriaAttributeIndex = 39)
union all
select SBScholarshipID, 40, UsageIndex from satact
where not exists (select * from SBScholarshipMatchCriteriaAttributeUsageRT mca
					where mca.SBScholarshipID = satact.SBScholarshipID and
							mca.SBMatchCriteriaAttributeIndex = 40)
union all
select SBScholarshipID, 41, UsageIndex from satact
where not exists (select * from SBScholarshipMatchCriteriaAttributeUsageRT mca
					where mca.SBScholarshipID = satact.SBScholarshipID and
							mca.SBMatchCriteriaAttributeIndex = 41)
union all
select SBScholarshipID, 42, UsageIndex from satact
where not exists (select * from SBScholarshipMatchCriteriaAttributeUsageRT mca
					where mca.SBScholarshipID = satact.SBScholarshipID and
							mca.SBMatchCriteriaAttributeIndex = 42)

delete from [SBScholarshipMatchCriteriaAttributeUsageRT]
where SBMatchCriteriaAttributeIndex in (31, 32)

";

		private const string ChangeSATACTAttributesDown =
			@"
update  [SBScholarshipMatchCriteriaAttributeUsageRT]
set [SBMatchCriteriaAttributeIndex] = 31
where [SBMatchCriteriaAttributeIndex] = 36
and not exists (select * 
					from [SBScholarshipMatchCriteriaAttributeUsageRT] mc 
					where mc.[SBMatchCriteriaAttributeIndex] = 31 and 
					[SBScholarshipMatchCriteriaAttributeUsageRT].SBScholarshipID = mc.SBScholarshipID)
					
update  [SBScholarshipMatchCriteriaAttributeUsageRT]
set [SBMatchCriteriaAttributeIndex] = 32
where [SBMatchCriteriaAttributeIndex] = 39
and not exists (select * 
					from [SBScholarshipMatchCriteriaAttributeUsageRT] mc 
					where mc.[SBMatchCriteriaAttributeIndex] = 32 and 
					[SBScholarshipMatchCriteriaAttributeUsageRT].SBScholarshipID = mc.SBScholarshipID)

delete from [SBScholarshipMatchCriteriaAttributeUsageRT]
where SBMatchCriteriaAttributeIndex in (36, 37, 38, 39, 40, 41, 42)

";


		public const string SBMatchRange =
	@"
CREATE VIEW [dbo].[SBMatchRanges]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'GPA' as criterion, 
	s.MatchCriteriaGPAGreaterThan as minimum, s.MatchCriteriaGPALessThan as maximum,
	seeker.SBSeekerId, seeker.GPA as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.GPA between s.MatchCriteriaGPAGreaterThan and  s.MatchCriteriaGPALessThan
where match.SBMatchCriteriaAttributeIndex=29
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'APCredits', 
	s.MatchCriteriaAPCreditsEarned, 10000,
	seeker.SBSeekerId, seeker.APCredits
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.APCredits between s.MatchCriteriaAPCreditsEarned and 10000
where match.SBMatchCriteriaAttributeIndex=34
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'IBCredits', 
	s.MatchCriteriaIBCreditsEarned, 10000,
	seeker.SBSeekerId, seeker.IBCredits
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.IBCredits between s.MatchCriteriaIBCreditsEarned and  10000
where match.SBMatchCriteriaAttributeIndex=35
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATWriting', 
	s.MatchCriteriaSATWritingGreaterThan, s.MatchCriteriaSATWritingLessThan,
	seeker.SBSeekerId, seeker.SATWriting
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATWriting between s.MatchCriteriaSATWritingGreaterThan and  s.MatchCriteriaSATWritingLessThan
where match.SBMatchCriteriaAttributeIndex=36
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATCriticalReading', 
	s.MatchCriteriaSATCriticalReadingGreaterThan, s.MatchCriteriaSATCriticalReadingLessThan ,
	seeker.SBSeekerId, seeker.SATCriticalReading
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATCriticalReading between s.MatchCriteriaSATCriticalReadingGreaterThan and  s.MatchCriteriaSATCriticalReadingLessThan
where match.SBMatchCriteriaAttributeIndex=37
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATMathematics', 
	s.MatchCriteriaSATMathematicsGreaterThan, s.MatchCriteriaSATMathematicsLessThan,
	seeker.SBSeekerId, seeker.SATMathematics
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATMathematics between s.MatchCriteriaSATMathematicsGreaterThan and  s.MatchCriteriaSATMathematicsLessThan
where match.SBMatchCriteriaAttributeIndex=38
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTEnglish', 
	s.MatchCriteriaACTScoreEnglishGreaterThan, s.MatchCriteriaACTScoreEnglishLessThan ,
	seeker.SBSeekerId, seeker.ACTEnglish
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTEnglish between s.MatchCriteriaACTScoreEnglishGreaterThan and  s.MatchCriteriaACTScoreEnglishLessThan
where match.SBMatchCriteriaAttributeIndex=39
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTMathematics', 
	s.MatchCriteriaACTScoreMathematicsGreaterThan, s.MatchCriteriaACTScoreMathematicsLessThan,
	seeker.SBSeekerId, seeker.ACTMathematics
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTMathematics between s.MatchCriteriaACTScoreMathematicsGreaterThan and  s.MatchCriteriaACTScoreMathematicsLessThan
where match.SBMatchCriteriaAttributeIndex=40
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTReading', 
	s.MatchCriteriaACTScoreReadingGreaterThan, s.MatchCriteriaACTScoreReadingLessThan,
	seeker.SBSeekerId, seeker.ACTReading
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTReading between s.MatchCriteriaACTScoreReadingGreaterThan and  s.MatchCriteriaACTScoreReadingLessThan
where match.SBMatchCriteriaAttributeIndex=41
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTScience', 
	s.MatchCriteriaACTScoreScienceGreaterThan, s.MatchCriteriaACTScoreScienceLessThan ,
	seeker.SBSeekerId, seeker.ACTScience
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTScience between s.MatchCriteriaACTScoreScienceGreaterThan and  s.MatchCriteriaACTScoreScienceLessThan
where match.SBMatchCriteriaAttributeIndex=42
";

		private const string SbMatchRangeListOrig =
	@"
CREATE VIEW [dbo].[SBMatchRanges]
AS
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'GPA' as criterion, 
	s.MatchCriteriaGPAGreaterThan as minimum, s.MatchCriteriaGPALessThan as maximum,
	seeker.SBSeekerId, seeker.GPA as seekerValue
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.GPA between s.MatchCriteriaGPAGreaterThan and  s.MatchCriteriaGPALessThan
where match.SBMatchCriteriaAttributeIndex=29
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex,  'ClassRank', 
	s.MatchCriteriaClassRankGreaterThan, s.MatchCriteriaClassRankLessThan,
	seeker.SBSeekerId, seeker.ClassRank
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ClassRank between s.MatchCriteriaClassRankGreaterThan and  s.MatchCriteriaClassRankLessThan
where match.SBMatchCriteriaAttributeIndex=30
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'APCredits', 
	s.MatchCriteriaAPCreditsEarned, 10000,
	seeker.SBSeekerId, seeker.APCredits
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.APCredits between s.MatchCriteriaAPCreditsEarned and 10000
where match.SBMatchCriteriaAttributeIndex=34
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'IBCredits', 
	s.MatchCriteriaIBCreditsEarned, 10000,
	seeker.SBSeekerId, seeker.IBCredits
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.IBCredits between s.MatchCriteriaIBCreditsEarned and  10000
where match.SBMatchCriteriaAttributeIndex=35
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATWriting', 
	s.MatchCriteriaSATWritingGreaterThan, s.MatchCriteriaSATWritingLessThan,
	seeker.SBSeekerId, seeker.SATWriting
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATWriting between s.MatchCriteriaSATWritingGreaterThan and  s.MatchCriteriaSATWritingLessThan
where match.SBMatchCriteriaAttributeIndex=31
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATCriticalReading', 
	s.MatchCriteriaSATCriticalReadingGreaterThan, s.MatchCriteriaSATCriticalReadingLessThan ,
	seeker.SBSeekerId, seeker.SATCriticalReading
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATCriticalReading between s.MatchCriteriaSATCriticalReadingGreaterThan and  s.MatchCriteriaSATCriticalReadingLessThan
where match.SBMatchCriteriaAttributeIndex=31
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'SATMathematics', 
	s.MatchCriteriaSATMathematicsGreaterThan, s.MatchCriteriaSATMathematicsLessThan,
	seeker.SBSeekerId, seeker.SATMathematics
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.SATMathematics between s.MatchCriteriaSATMathematicsGreaterThan and  s.MatchCriteriaSATMathematicsLessThan
where match.SBMatchCriteriaAttributeIndex=31
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTEnglish', 
	s.MatchCriteriaACTScoreEnglishGreaterThan, s.MatchCriteriaACTScoreEnglishLessThan ,
	seeker.SBSeekerId, seeker.ACTEnglish
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTEnglish between s.MatchCriteriaACTScoreEnglishGreaterThan and  s.MatchCriteriaACTScoreEnglishLessThan
where match.SBMatchCriteriaAttributeIndex=32
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTReading', 
	s.MatchCriteriaACTScoreReadingGreaterThan, s.MatchCriteriaACTScoreReadingLessThan,
	seeker.SBSeekerId, seeker.ACTReading
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTReading between s.MatchCriteriaACTScoreReadingGreaterThan and  s.MatchCriteriaACTScoreReadingLessThan
where match.SBMatchCriteriaAttributeIndex=32
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTMathematics', 
	s.MatchCriteriaACTScoreMathematicsGreaterThan, s.MatchCriteriaACTScoreMathematicsLessThan,
	seeker.SBSeekerId, seeker.ACTMathematics
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTMathematics between s.MatchCriteriaACTScoreMathematicsGreaterThan and  s.MatchCriteriaACTScoreMathematicsLessThan
where match.SBMatchCriteriaAttributeIndex=32
union all
select s.SBScholarshipId, s.AcademicYear, match.SBUsageTypeIndex, 'ACTScience', 
	s.MatchCriteriaACTScoreScienceGreaterThan, s.MatchCriteriaACTScoreScienceLessThan ,
	seeker.SBSeekerId, seeker.ACTScience
from SBScholarshipMatchCriteriaAttributeUsageRT match
inner join SBScholarship s on s.SBScholarshipId=match.SBScholarshipId and s.Stage='Activated'
left join SBSeeker seeker on seeker.ACTScience between s.MatchCriteriaACTScoreScienceGreaterThan and  s.MatchCriteriaACTScoreScienceLessThan
where match.SBMatchCriteriaAttributeIndex=32
";
	}
}