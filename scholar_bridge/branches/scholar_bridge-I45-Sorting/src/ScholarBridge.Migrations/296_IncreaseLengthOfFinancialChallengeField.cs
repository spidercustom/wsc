using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(296)]
	public class IncreaseLengthOfFinancialChallengeField : Migration
    {
		private const string SEEKER_TABLE = "SBSeeker";
		private const string APPLICATION_TABLE = "SBApplication";
		private const string MY_CHALLENGE = "MyChallenge";

        public override void Up()
        {
			IncreaseFieldLengths(SEEKER_TABLE);
			IncreaseFieldLengths(APPLICATION_TABLE);
		}

    	public override void Down()
    	{
			DecreaseFieldLengths(SEEKER_TABLE);
			DecreaseFieldLengths(APPLICATION_TABLE);
		}

		private void IncreaseFieldLengths(string tableName)
		{
			Database.ChangeColumn(tableName, new Column(MY_CHALLENGE, DbType.String, 1500, ColumnProperty.Null));
		}

		private void DecreaseFieldLengths(string tableName)
    	{
			TruncateData(tableName);
    		Database.ChangeColumn(tableName, new Column(MY_CHALLENGE, DbType.String, 150, ColumnProperty.Null));
    	}

    	private void TruncateData(string tableName)
		{
			Database.ExecuteNonQuery("Update " + tableName + 
@"  
set MyChallenge = SUBSTRING(APCreditsDetail, 1, 150)
");
		}
    }
}