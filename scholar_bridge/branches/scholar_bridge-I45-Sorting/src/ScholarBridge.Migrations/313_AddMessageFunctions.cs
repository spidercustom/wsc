﻿using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(313)]
    public class AddMessageFunctions : Migration
    {
        public override void Up()
        {
        	DropFunction();
			Database.ExecuteNonQuery(FINDUSERNAMEBYMMESSAGEADDRESS);
            Database.ExecuteNonQuery(FINDORGANIZATIONFUNCTION );
		}

		public override void Down()
		{
		 
			DropFunction();
		}

		private void DropFunction()
		{
            Database.ExecuteNonQuery("if exists (select * from sys.objects where name = 'FindOrganizationNameByMessageAddress') drop function dbo.FindOrganizationNameByMessageAddress");
            Database.ExecuteNonQuery("if exists (select * from sys.objects where name = 'FindUserNameByMessageAddress') drop function dbo.FindUserNameByMessageAddress");
 
        }



        #region FindUserNameByMessageAddress Function

        public const string FINDUSERNAMEBYMMESSAGEADDRESS =
            @"
-- =============================================
-- Author:		Bhupindra Singh
-- Create date: 2/16/2010
-- Description:	compute an application status
-- =============================================
create FUNCTION [dbo].[FindUserNameByMessageAddress]
(
	@UserId int,
	@RoleId int,
	@OrganizationId int
	
	
)
RETURNS varchar(200)
AS
BEGIN

	declare 
	@Result varchar(200) ,
	@OrgAdminUserId int, 
	@RoleName varchar(200) 
	
	SET @Result=''
	
	
	IF  (@UserId >0 )
	BEGIN
		SELECT @Result = FirstName+' '+MiddleName +' '+ LastName 
		FROM [dbo].SBUser
		WHERE
			  SBUserId=@UserId
		return @Result ;
	END
	
	IF  (@RoleId > 0 )
	BEGIN
	
		SELECT @RoleName =SBRole
		FROM [dbo].SBRoleLUT
		WHERE
		SBRoleIndex=@RoleId
		
		IF (@RoleName = 'WSCAdmin')
		  set @Result='Washboard Administrator'
	END

	IF  (@OrganizationId > 0 )
	BEGIN
		SELECT @OrgAdminUserId = AdminUserID
		FROM [dbo].SBOrganization
		WHERE
		SBOrganizationId=@OrganizationId
		
		IF (@OrgAdminUserId > 0)
		BEGIN
			SELECT @Result = FirstName+' '+MiddleName +' '+ LastName 
			FROM [dbo].SBUser
			WHERE
				  SBUserId=@OrgAdminUserId
			
		END
		return @Result  ;
	END 
	
	return @Result ;

END";
		#endregion


        #region FindOrganizationNameByMessageAddress Function

        public const string FINDORGANIZATIONFUNCTION =
            @"
-- =============================================
-- Author:		Bhupindra Singh
-- Create date: 2/16/2010
-- Description:	compute an application status
-- =============================================
create FUNCTION [dbo].[FindOrganizationNameByMessageAddress]
(
	@UserId int,
	@RoleId int,
	@OrganizationId int
	
	
)
RETURNS varchar(50)
AS
BEGIN

	declare 
	@Result varchar(50) ,
	@RoleName varchar(50)  
	 
	SET @Result=''
	
	IF  (@UserId >0 )
	BEGIN
		SELECT  @Result=SBOrganization.Name
		FROM [dbo].SBOrganization
		INNER JOIN [dbo].SBUserOrganizationRT
		ON SBOrganization.SBOrganizationId=SBUSEROrganizationRT.SBOrganizationId
		WHERE
		SBUSEROrganizationRT.SbUserId=@UserId
		return @Result ;
	END
	
	IF  (@OrganizationId > 0 )
	BEGIN
		SELECT @Result = Name
		FROM [dbo].SBOrganization
		WHERE
		SBOrganizationId=@OrganizationId
		return @Result  ;
	END 
	
	IF  (@RoleId > 0 )
	BEGIN
	
		SELECT @RoleName =SBRole
		FROM [dbo].SBRoleLUT
		WHERE
		SBRoleIndex=@RoleId
		
		IF (@RoleName = 'WSCAdmin')
		  set @Result='Washboard Administrator'
	END

	return @Result ;

END";
        #endregion
 

	}
}