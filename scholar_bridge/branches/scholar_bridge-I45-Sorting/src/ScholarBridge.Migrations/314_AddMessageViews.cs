using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(314)]
    public class AddMessageViews : Migration
    {
        public override void Up()
        {
            Database.ExecuteNonQuery(SBApplicationView);
            Database.ExecuteNonQuery(SBMessageView  );
            Database.ExecuteNonQuery(SBSentMessageView);
             
        }

        public override void Down()
        {
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBApplicationView]");
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBMessageView]");
            Database.ExecuteNonQuery("DROP VIEW [dbo].[SBSentMessageView]");
             
        }

        public const string SBApplicationView = @"
Create View [dbo].[SBApplicationView]
as
Select a.*,
	
	 case when SBMatch.ScholarshipMinimumCriteriaCount = 0 then 1 else (SBMatch.SeekerMinimumCriteriaCount / SBMatch.ScholarshipMinimumCriteriaCount)  end 
	AS MatchMinimumCriteriaPercent,
	 
	  case when SBMatch.ScholarshipPreferredCriteriaCount = 0 then 1 else (SBMatch.SeekerPreferredCriteriaCount / SBMatch.ScholarshipPreferredCriteriaCount)  end 
	AS MatchPreferredCriteriaPercent,
	
	
	(SBUser.LastName +', '+SBUser.FirstName +' '+ SBUser.MiddleName) AS ApplicantName
from SBApplication a 

INNER JOIN SBMatch ON SBMatch.SBSeekerId=a.SeekerId And SBMatch.SBScholarshipId=a.ScholarshipId
INNER JOIN SBSeeker ON SBSeeker.SBSeekerId=a.SeekerId 
INNER JOIN SBUser ON SBUser.SBUserId=SBSeeker.UserId";

public const string SBMessageView = @"
Create View [dbo].[SBMessageView]
as
Select m.*,
 [dbo].FindUserNameByMessageAddress(FromUserId,FromRoleId,FromOrganizationId) AS MessageSenderName,
 [dbo].FindOrganizationNameByMessageAddress(FromUserId,FromRoleId,FromOrganizationId) AS MessageOrganizationName
	
	 
from SBMessage m
";

private const string SBSentMessageView =
            @"Create View [dbo].[SBSentMessageView]
as
Select m.*,
 [dbo].FindUserNameByMessageAddress(m.ToUserId,m.ToRoleId,m.ToOrganizationId) AS MessageRecipientName,
 [dbo].FindOrganizationNameByMessageAddress(ToUserId,ToRoleId,ToOrganizationId) AS MessageOrganizationName
	
	 
from SBSentMessage m
";

        

    }
}