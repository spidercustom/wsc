﻿using Migrator.Framework;
using ScholarBridge.Migrations.Common;

namespace ScholarBridge.Migrations
{
    [Migration(38)]
    public class AddCommunityInvolvementCause : AddLookupTableBase
    {
        public const string INDICATIVE_NAME = "CommunityInvolvementCause";

        protected override string IndicativeName
        {
            get { return INDICATIVE_NAME; }
        }
    }
}
