using System;
using System.Data;
using Migrator.Framework;

namespace ScholarBridge.Migrations
{
    [Migration(297)]
	public class AddIndexesToApplicationAndMatch : Migration
    {

        public override void Up()
        {
			AddIndexes();
		}

    	public override void Down()
    	{
			DropIndexes();
		}

		private void AddIndexes()
		{
			Database.ExecuteNonQuery(
				@"
CREATE NONCLUSTERED INDEX [IX_SBApplication_SeekerId] ON [dbo].[SBApplication] 
(
	[SeekerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_SBApplication_ScholarshipId] ON [dbo].[SBApplication] 
(
	[ScholarshipId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_SBMatch_SeekerId] ON [dbo].[SBMatch] 
(
	[SBSeekerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_SBMatch_ScholarshipID] ON [dbo].[SBMatch] 
(
	[SBScholarshipId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_SBMatch_Rank] ON [dbo].[SBMatch] 
(
	[Rank] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
"

				);
		}

		private void DropIndexes()
    	{
			Database.ExecuteNonQuery(
				@"
DROP INDEX SBApplication.IX_SBApplication_SeekerId
DROP INDEX SBApplication.IX_SBApplication_ScholarshipId
DROP INDEX SBMatch.IX_SBMatch_SeekerId
DROP INDEX SBMatch.IX_SBMatch_ScholarshipId
DROP INDEX SBMatch.IX_SBMatch_Rank
				");
    	}
    }
}