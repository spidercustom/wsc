﻿using System;
using NUnit.Framework;
using ScholarBridge.Common.Extensions;

namespace ScholarBridge.Common.Tests
{
    [TestFixture]
    public class DateTimeExtensionsTests
    {
        [Test]
        public void between_date_test_cases()
        {
            Assert.That(DateTime.Today.IsBetween(DateTime.MinValue, DateTime.MaxValue));
            Assert.That(!DateTime.Today.IsBetween(DateTime.Today.AddDays(1), DateTime.MaxValue));
            Assert.That(!DateTime.Today.IsBetween(DateTime.MinValue, DateTime.Today.AddDays(-1)));
        }

        [Test]
        public void between_nullable_date_test_cases()
        {
            Assert.That(DateTime.Today.IsBetween(null, null));
            Assert.That(DateTime.Today.IsBetween(DateTime.MinValue, null));
            Assert.That(DateTime.Today.IsBetween(null, DateTime.MaxValue));
            Assert.That(DateTime.Today.IsBetween(DateTime.MinValue, DateTime.MaxValue));
        }

        [Test]
        public void test_ordinal_suffix()
        {
            Assert.AreEqual("th", DateTimeExtensions.GetOrdinalSuffix(0));
            Assert.AreEqual("st", DateTimeExtensions.GetOrdinalSuffix(1));
            Assert.AreEqual("nd", DateTimeExtensions.GetOrdinalSuffix(2));
            Assert.AreEqual("rd", DateTimeExtensions.GetOrdinalSuffix(3));
            Assert.AreEqual("th", DateTimeExtensions.GetOrdinalSuffix(4));


            Assert.AreEqual("th", DateTimeExtensions.GetOrdinalSuffix(100));
            Assert.AreEqual("st", DateTimeExtensions.GetOrdinalSuffix(101));
            Assert.AreEqual("nd", DateTimeExtensions.GetOrdinalSuffix(102));
            Assert.AreEqual("rd", DateTimeExtensions.GetOrdinalSuffix(103));
            Assert.AreEqual("th", DateTimeExtensions.GetOrdinalSuffix(104));
        
        }

        [Test]
        public void test_day_ordinal_string_null_for_other_part()
        {
            var firstJanuary2009 = new DateTime(2009, 1, 1);
            Assert.AreEqual("1st", firstJanuary2009.ToOrdinalDayString(null));
        }

        [Test]
        public void test_day_ordinal_string_empty_string_for_other_part()
        {
            var firstJanuary2009 = new DateTime(2009, 1, 1);
            Assert.AreEqual("1st", firstJanuary2009.ToOrdinalDayString(string.Empty));
        }

        [Test]
        public void test_day_ordinal_string_format_specified_for_other_part()
        {
            var firstJanuary2009 = new DateTime(2009, 1, 1);
            Assert.AreEqual("1st Jan", firstJanuary2009.ToOrdinalDayString("MMM"));
        }

        [Test]
        public void test_day_ordinal_string_for_various_days()
        {
            var firstJanuary2009 = new DateTime(2009, 1, 1);
            var secondJanuary2009 = new DateTime(2009, 1, 2);
            var thirdJanuary2009 = new DateTime(2009, 1, 3);
            var fourthJanuary2009 = new DateTime(2009, 1, 4);
            var twentethJanuary2009 = new DateTime(2009, 1, 20);
            var twentyFirstJanuary2009 = new DateTime(2009, 1, 21);
            var twentySecondJanuary2009 = new DateTime(2009, 1, 22);
            var twentyThirdJanuary2009 = new DateTime(2009, 1, 23);
            var twentyFourthJanuary2009 = new DateTime(2009, 1, 24);
            var thirtyFirstJanuary2009 = new DateTime(2009, 1, 31);

            Assert.AreEqual("1st", firstJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("2nd", secondJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("3rd", thirdJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("4th", fourthJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("20th", twentethJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("21st", twentyFirstJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("22nd", twentySecondJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("23rd", twentyThirdJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("24th", twentyFourthJanuary2009.ToOrdinalDayString(null));
            Assert.AreEqual("31st", thirtyFirstJanuary2009.ToOrdinalDayString(null));
        }
    }
}
