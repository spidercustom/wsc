﻿
using ScholarBridge.Common;

namespace ScholarBridge.Domain
{
    public enum AwardStatus
    {
        None,
        Offered,
        Awarded,
        [DisplayName("Not Awarded")]
        NotAwarded
    }
}
