namespace ScholarBridge.Domain
{
    public abstract class CriteriaCountBase
    {
        public virtual int SeekerPreferredCriteriaCount { get; set; }
        public virtual int SeekerMinimumCriteriaCount { get; set; }
        public virtual int MinimumCriteriaPercent { get; set; }
        public virtual int PreferredCriteriaPercent { get; set; }
        public virtual int ScholarshipPreferredCriteriaCount { get; set; }
        public virtual int ScholarshipMinimumCriteriaCount { get; set; }

        public virtual string MinumumCriteriaString
        {
            get { return string.Format("{0} of {1}", SeekerMinimumCriteriaCount, ScholarshipMinimumCriteriaCount); }
        }

        public virtual double ComputeMinimumCriteriaPercent()
        {
            if (0 == ScholarshipMinimumCriteriaCount)
                return 1;
            return SeekerMinimumCriteriaCount / (double)ScholarshipMinimumCriteriaCount;
        }

        public virtual string PreferredCriteriaString
        {
            get { return string.Format("{0} of {1}", SeekerPreferredCriteriaCount, ScholarshipPreferredCriteriaCount); }
        }

        public virtual double ComputePreferredCriteriaPercent()
        {
            if (0 == ScholarshipPreferredCriteriaCount)
                return 1;
            return SeekerPreferredCriteriaCount / (double)ScholarshipPreferredCriteriaCount;
        }
    }
}