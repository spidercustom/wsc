﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using ScholarBridge.Domain.Contact;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ScholarBridge.Domain.Lookup;

namespace ScholarBridge.Domain.SeekerParts
{
	/// <summary>
	/// todo: currently SeekerAddress only represents WA State addresses - this needs to be fixed - see WSC-1224
	/// </summary>
    public class SeekerAddress : AddressBase
    {
        [NotNullValidator(MessageTemplate = "City must be specified", Ruleset = ADDRESS_REQUIRED)]
        public virtual City City { get; set; }

        public override string GetCity
        {
            get { return City == null ? "" : City.Name; }
        }
         
        public override ValidationResults ValidateAsRequired()
        {
            var validator = ValidationFactory.CreateValidator<SeekerAddress>(ADDRESS_REQUIRED);
            return validator.Validate(this);
        }
    }
}