using System;
using NUnit.Framework;
using Rhino.Mocks;
using ScholarBridge.Business.Actions;
using ScholarBridge.Domain;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;

namespace ScholarBridge.Business.Tests
{
    [TestFixture]
    public class ActivateIntermediaryActionTests
    {
        private MockRepository mocks;
        private ActivateIntermediaryAction activateIntermediary;
        private IIntermediaryService intermediaryService;
        private IMessageService messageService;

        [SetUp]
        public void Setup()
        {
            mocks = new MockRepository();
            intermediaryService = mocks.StrictMock<IIntermediaryService>();
            messageService = mocks.StrictMock<IMessageService>();
            activateIntermediary = new ActivateIntermediaryAction
                                      {
                                          IntermediaryService = intermediaryService,
                                          MessageService = messageService
                                      };
        }

        [TearDown]
        public void Teardown()
        {
            mocks.BackToRecord(intermediaryService);
            mocks.BackToRecord(messageService);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void approve_with_wrong_message_type_throws_exception()
        {
            activateIntermediary.Approve(new Message(), null, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void reject_with_wrong_message_type_throws_exception()
        {
            activateIntermediary.Reject(new Message(), null, null);
        }

        [Test]
        public void approve_with_intermediary()
        {
            var i = new Intermediary { AdminUser = new User() };
            var m = new OrganizationMessage { RelatedOrg = i };

            Expect.Call(() => intermediaryService.Approve(i));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateIntermediary.Approve(m, null, i.AdminUser);

            mocks.VerifyAll();
        }

        [Test]
        public void reject_with_intermediary()
        {
            var i = new Intermediary { AdminUser = new User() };
            var m = new OrganizationMessage { RelatedOrg = i };

            Expect.Call(() => intermediaryService.Reject(i));
            Expect.Call(() => messageService.ArchiveMessage(m));
            mocks.ReplayAll();

            activateIntermediary.Reject(m, null, i.AdminUser);

            mocks.VerifyAll();
        }
    }
}