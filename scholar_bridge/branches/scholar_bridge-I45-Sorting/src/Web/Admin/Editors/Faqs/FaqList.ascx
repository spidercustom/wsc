﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FaqList.ascx.cs" Inherits="ScholarBridge.Web.Admin.Editors.Faqs.FaqList" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register src="~/Admin/Editors/Faqs/FaqEntry.ascx" tagname="FaqEntry" tagprefix="sb" %>

<h3>Faq Editor</h3>

<asp:panel ID='faqListPanel' runat="server">

    <div>
        <asp:ListView ID="lstFaqs" runat="server" 
            OnItemDataBound="lstFaqs_ItemDataBound" 
            onpagepropertieschanging="lstFaqs_PagePropertiesChanging" >
            <LayoutTemplate>
             <br />
                 <sbCommon:AnchorButton CausesValidation="false" ID="createFaqLnkTop" runat="server" OnClick="createFaqLnk_Click" Text="Add Faq" /> 
     
            <br />
            <table class="sortableTable">
                    <thead>
                    <tr>
                        <th>Select</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Group</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder id="itemPlaceholder" runat="server" />
                </tbody>
            </table>
            
            </LayoutTemplate>
                  
            <ItemTemplate>
            <tr class="row">
                <td><asp:CheckBox ID="chkFaq" runat="server" /></td>
                <td><sbCommon:AnchorButton ID="SingleEditBtn" CausesValidation="false" Text="Edit" runat="server" onclick="SingleEditBtn_Click" /></td>
                <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" CausesValidation="false" ConfirmMessageDivID ="deleteConfirmDiv" 
                    Text="Delete"  runat="server" OnClickConfirm="SingleDeleteBtn_Click"   Width="60px" /></td>
                <td>
                        <%# DataBinder.Eval(Container.DataItem, "Question") %>
                </td>
                <td>
                    <%# Eval("Answer")%>
                </td>
                <td>
                    <%# Eval("FaqType") %>
                </td>
            </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
            <tr class="altrow">
                <td><asp:CheckBox ID="chkFaq" runat="server" /></td>
                <td><sbCommon:AnchorButton ID="SingleEditBtn" CausesValidation="false"  Text="Edit" runat="server" onclick="SingleEditBtn_Click" /></td>
                <td><sbCommon:ConfirmButton ID="SingleDeleteBtn" CausesValidation="false"  ConfirmMessageDivID ="deleteConfirmDiv" 
                    Text="Delete"  runat="server" OnClickConfirm="SingleDeleteBtn_Click" Width="60px"  /></td>
                <td>
                        <%# DataBinder.Eval(Container.DataItem, "Question") %>
                </td>
                <td>
                    <%# Eval("Answer")%>
                </td>
                <td>
                    <%# Eval("FaqType") %>
                </td>
            </tr>
            </AlternatingItemTemplate>
            <EmptyDataTemplate>
            <p>There are no Faqs.</p>
            
                <sbCommon:AnchorButton ID="createFaqLnk" runat="server" OnClick="createFaqLnk_Click" Text="Add Faq" /> 
     
            </EmptyDataTemplate>
        </asp:ListView>

        <div class="pager">
            <asp:DataPager runat="server" ID="pager" 
                PagedControlID="lstFaqs" 
                PageSize="50" 
                onprerender="pager_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                        ShowNextPageButton="false" ShowLastPageButton="false" />
                    <sbCommon:CustomNumericPagerField
                        CurrentPageLabelCssClass="pagerlabel"
                        NextPreviousButtonCssClass="pagerlink"
                        PagingPageLabelCssClass="pagingPageLabel"
                        NumericButtonCssClass="pagerlink"/>
                    <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                        ShowNextPageButton="true" ShowLastPageButton="false" />
                </Fields>
            </asp:DataPager>
            <br />
        </div> 
        <div>
            <sbCommon:ConfirmButton ID="DeleteBtn" ConfirmMessageDivID ="deleteConfirmDiv" 
                Text="Delete Selected"  runat="server" OnClickConfirm="DeleteBtn_Click" Width="130px" />
        </div>
        <div id="deleteConfirmDiv" title="Confirm Delete" style="display:none">
                Are you sure want to delete?
        </div> 
     
     </div>
 
</asp:panel>

<asp:panel ID="addUpdatePanel" runat="server" Visible="false">

    <sb:FaqEntry ID="faqEntry" 
                    runat="server"  
                    OnFormSaved="FaqEntry1_OnFormSaved" 
                    OnFormCanceled ="FaqEntry1_OnFormCanceled" />
    
</asp:panel>
