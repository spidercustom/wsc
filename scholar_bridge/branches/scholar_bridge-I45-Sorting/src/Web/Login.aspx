﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ScholarBridge.Web.Login" Title="Login" %>
<%@ Import Namespace="ScholarBridge.Web"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="sb" TagName="Login" Src="~/Common/Login.ascx" %>
<%@ Register Src="~/Common/GlobalFooter.ascx" TagName="GlobalFooter" TagPrefix="sb" %>
<%@ Register Src="~/Common/ScholarshipSearchBox.ascx" TagName="ScholarshipSearchBox" TagPrefix="sb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>theWashBoard.org</title>
    <meta name="FORMAT" content="text/html"/>
    <meta name="CHARSET" content="ISO-8859-1"/>
    <meta name="DOCUMENTLANGUAGECODE" content="en"/>
    <meta name="DOCUMENTCOUNTRYCODE" content="us"/>
    <meta name="DC.LANGUAGE" scheme="rfc1766" content="en-us"/>
    <meta name="COPYRIGHT" content="Copyright 2010 by Washington Scholarship Coalition"/>
    <meta name="SECURITY" content="Public"/>
    <meta name="ROBOTS" content="index,follow"/>
    <meta name="GOOGLEBOT" content="index,follow"/>
    <meta name="Description" content="Log in to theWashBoard.org"/>
    <meta name="Keywords" content="washington, scholarship, matches, students, college, high school, school"/>
    <meta name="Author" content="theWashBoard.org"/>
    <!-- Base keywords here-->

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
        google.load("jquery", "1.3");
    </script>

    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery-ui-1.7.1.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tablesorter.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.pager.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.tinysort.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.maskedinput.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.cookie.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/jquery.watermark-2.0.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/superfish.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/js/site.js") %>"></script>

    <%--<link rel="stylesheet" href="<%= ResolveUrl("~/styles/main.css") %>" type="text/css" />--%>
    <link href="<%= ResolveUrl("~/styles/WSCStyles.CSS") %>" rel="stylesheet" type="text/css" media="All" />
    <link href="<%= ResolveUrl("~/styles/global.css") %>" rel="stylesheet" type="text/css" media="All"/>         
    
</head>
<body>
    <form id="aspnetForm" runat="server">
    <!--Page wrapper starts here-->
    <div id="EntirePageWrapper">
        <div id="HeaderWrapper">
            <img alt="" src="<%= ResolveUrl("~/images/LogoMainHomepage.gif") %>" width="919px" height="151px"
                border="0" usemap="#Map"/><map id="map" name="Map"><area shape="rect" coords="37,9,236,133"
                    href="<%= ResolveUrl("~/") %>" alt="theWashBoard.org"/></map><img alt="" src="<%= ResolveUrl("~/images/PicBottomOverAllHomepage.gif") %>"
                        width="919px" height="497px"/>
          <!--Global nav links start here-->
        <div id="GlobaNavLinksContainerSeeker">
            <div class="GlobalNavLinks">
                <sb:ScholarshipSearchBox runat="server" />
            </div>
        </div>
        </div>
        <div id="MasterPageLogin" class="LoginContainer">
            <div class="Horizontal">
                <sb:Login ID="loginForm" runat="server" ShowRegisterToday="true"  ShowFailureDialog="true" />
            </div>
        </div>
        <!--Login Text ends here-->
        <div style='clear: both;'>
        </div>
        <!--This is the outer most wrapper 01 starts here-->
        <div id="ContentWrapper01">
            <div id="LeftPageShadow">
                <img alt="" src="<%= ResolveUrl("~/images/LeftContentShadow.gif") %>"/></div>
            <div id="RightPageShadow">
                <img alt="" src="<%= ResolveUrl("~/images/RightContentShadow.gif") %>"/></div>
            <!--This is content wrapper 02 starts here-->
            <div id="ContentWrapper02">
                <div style='clear: both;'>
                </div>

                <div style='clear: both;'>
                </div>
                <!--The Three column starts here-->
                <div id="BoxWrapper">
                    <div id="LeftBottomBox">
                        <a href="<%= ResolveUrl("~/Seeker/Anonymous.aspx") %>">
                            <img alt="" src="<%= ResolveUrl("~/images/HomepageBottomBox01_ForSeekers.gif") %>" width="262px"
                                height="41px"/>
                            <img alt="" src="<%= ResolveUrl("~/images/OverallHomepageBottomBox01.gif") %>" width="237px"
                                height="86px"/>
                        </a>
                        <p style="width: 237px">
                            Create a profile and let us do the rest. We will match you with scholarships you
                            are most likely to qualify for and applying online is easy.
                            <br/>
                            <br/>
                        </p>
                        <p class="IcoBoxArrow">
                            <a href="<%= LinkGenerator.GetFullLinkStatic("/Seeker/Register.aspx") %>" style="font-size:14px;" >
                                <img alt="Seeker Registeration" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;Seeker Registration</a></p>
                    </div>
                    <div id="CenterBottomBox" style="font-size:14px;">
                        <a href='<%= LinkGenerator.GetFullLinkStatic("/PressRoom/") %>'><img alt="News and Announcements" src="<%= ResolveUrl("~/images/news_announcements.png") %>"/></a>
                        <p style="width:237px; margin-top:.5em;"><b>We're making improvements</b> this month, in response to early feedback, to ensure the most accurate scholarship matches.</p>
                        <p style="width:237px; margin-top:1.25em;">
                            <b>New scholarships</b> are being added to theWashBoard.org daily! Check back often.</p>
                        <p style="width:237px; margin-top:1.25em;"><a href="http://www.fafsa.gov"><b>FAFSA</b></a> for 2010-2011 starts January 1, 2010.</p>
                    </div>
                    <div id="RightBottomBox">
                        <a href="<%= ResolveUrl("~/Provider/Anonymous.aspx") %>">
                            <img alt="" src="<%= ResolveUrl("~/images/HomepageBottomBox03_ForProviders.gif") %>" width="262px"
                                height="41px"/>
                            <img alt="" src="<%= ResolveUrl("~/images/OverallHomepageBottomBox03.gif") %>" width="237px"
                                height="86px"/>
                        </a>
                        <p style="width: 237px">
                            Post your scholarship to reach a larger group of students. We make reviewing and
                            evaluating applications easy. Find the next recipients of your scholarship.</p>
                        <p class="IcoBoxArrow">
                            <a href="<%= LinkGenerator.GetFullLinkStatic("/Provider/RegisterProvider.aspx") %>" style="font-size:14px;" >
                                <img alt="" src="<%= ResolveUrl("~/images/Ico_GreenArrow.gif") %>" width="14px" height="14px"/>&nbsp;Provider Registration</a></p>
                    </div>
                </div>
                <br/>
                <div style='clear: both;'></div>
                <sb:GlobalFooter ID="GlobalFooter1" runat="server" />
            </div>
            <!--This is content wrapper 02 ends here-->
        </div>
        <!--This is the outer most wrapper 01 ends here-->
    </div>
    <!--Page wrapper ends here-->
    </form>
</body>
</html>
