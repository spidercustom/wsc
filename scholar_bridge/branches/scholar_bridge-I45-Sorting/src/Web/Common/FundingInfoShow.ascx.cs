﻿using System;
using ScholarBridge.Common.Extensions;
using ScholarBridge.Web.Provider.BuildScholarship;

namespace ScholarBridge.Web.Common
{
    public partial class FundingInfoShow : BaseScholarshipShow
    {
        public string LinkToEditFinancialNeed { get; set; }

        public override int ResumeFrom
        {
            get { return WizardStepName.FundingProfile.GetNumericValue(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupEditLinks(linkarea1, linkarea2);
            editLink.NavigateUrl = ResolveUrl(LinkToEditFinancialNeed + "?id=" + ScholarshipToView.Id);

            bool noSelection = false;
            lblDemonstrateFinancialNeed.Text = "No";
            if (ScholarshipToView.IsApplicantDemonstrateFinancialNeedRequired.HasValue)
            {
                if (ScholarshipToView.IsApplicantDemonstrateFinancialNeedRequired.Value == true)
                    lblDemonstrateFinancialNeed.Text = "Yes";
                else
                    noSelection = true;
            }

            lblFafsaRequired.Text = "No";
            if (ScholarshipToView.IsFAFSARequired.HasValue)
            {
                if (ScholarshipToView.IsFAFSARequired.Value == true)
                    lblFafsaRequired.Text = "Yes";
                else
                    noSelection = true;
            }

            lblEFCRequired.Text = "No";
            if (ScholarshipToView.IsFAFSAEFCRequired.HasValue)
            {
                if (ScholarshipToView.IsFAFSAEFCRequired.Value == true)
                    lblEFCRequired.Text = "Yes";
                else
                    noSelection = true;
            }

            NoSelectionContainerControl.Visible = noSelection;
        }
    }
}