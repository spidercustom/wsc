﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.ascx.cs" Inherits="ScholarBridge.Web.Common.ContactUs" %> 
<%@ Register Assembly="Web" Namespace="ScholarBridge.Web.Common" TagPrefix="sbCommon" %>             

        <p class="hookline"><asp:label ID="titleLabel" runat="server" Text="Send a message to theWashBoard.org"></asp:label></p>
        <br />
            <label for="toLabel">
                To:</label><b><asp:label ID="toLabel" runat="server" Columns="40" Text="theWashBoard Administrator" MaxLength="40"></asp:label></b><br />
        <br/>
            <label for="fromBox">
                From:</label>
            <asp:TextBox ID="fromBox" runat="server" Columns="40" MaxLength="80"></asp:TextBox>
            <asp:RequiredFieldValidator Display="Dynamic" ID="fromBoxValidator" runat="server"
                ControlToValidate="fromBox" 
                ErrorMessage="Please enter your email so that we can respond to your message." 
                ToolTip="Please enter your email address.  Thanks.">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="emailValidator" 
                                                runat="server" 
                                                ControlToValidate="fromBox" 
                                                ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([,])*)*" 
                                                ErrorMessage="Please enter a valid email address"/>
            <b><asp:Label ID="fromLabel" runat="server"></asp:Label></b>
        <br />
        <br/>
            <label for="subject">
                Subject:</label>
            <b><asp:Label ID="subject" runat="server"></asp:Label></b>
        <br />
        <br />
        <br/>
            Your comments
        (1000 character limit)<br />
            <asp:TextBox ID="comments" Rows="5" runat="server" TextMode="MultiLine" Columns="100" MaxLength="1000 "></asp:TextBox>
            <asp:RequiredFieldValidator Display="Dynamic" ID="commentsRequired" runat="server"
                ControlToValidate="comments" ErrorMessage="Did you forget to enter your comments?" ToolTip="We need your comments.  Thanks."></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CommentsCustomValidator" runat="server" ErrorMessage="Comments cannot exceed 1000 characters." ControlToValidate="comments"  OnServerValidate="Comments_OnServerValidate"/>
       
        <br /><br />
        <sbCommon:AnchorButton ID="sendButton" OnClick="sendButton_Click" CausesValidation="true" runat="server"/>
        <sbCommon:AnchorButton ID="cancelButton" OnClick="cancelButton_Click" CausesValidation="true" runat="server"/>



