﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain.Auth;
using ScholarBridge.Domain.Messaging;
using ScholarBridge.Web.Extensions;
using Spring.Context.Support;

namespace ScholarBridge.Web.Common
{
    public partial class MessageList : UserControl
    {
        private const string DEFAULT_SORTEXPRESSION = "Date DESC";
        private const string ARCHIVED_KEY = "ArchivedKey";
        private const string SCHOLARSHIPAPPROVAL_KEY = "ScholarshipApprovalsKey";
        private const string MESSAGEACTION_KEY = "MessageActionKey";
        public bool Archived
        {
            get
            {
                if (ViewState[ARCHIVED_KEY] == null)
                    return false;

                return Convert.ToBoolean(ViewState[ARCHIVED_KEY]);
            }
            set
            {
                ViewState[ARCHIVED_KEY] = Convert.ToString(value);
            }
        }
         
        public bool ScholarshipApprovals { get
            {
                if (ViewState[SCHOLARSHIPAPPROVAL_KEY] == null)
                {
                    ViewState[SCHOLARSHIPAPPROVAL_KEY] = false;
                }
                return  Convert.ToBoolean(ViewState[SCHOLARSHIPAPPROVAL_KEY]);
            }
            set
            {
                ViewState[SCHOLARSHIPAPPROVAL_KEY] = value;
            }
        }
        public MessageAction MessageAction
        {
            get
            {
                if (ViewState[MESSAGEACTION_KEY] == null)
                {
                    return MessageAction.None;
                }
                return (MessageAction)ViewState[MESSAGEACTION_KEY];
            }
            set
            {
                ViewState[MESSAGEACTION_KEY] = value;
            }
        }
         

        public string LinkTo { get; set; }
        public IMessageService MessageService
        {
            get { return (IMessageService)ContextRegistry.GetContext().GetObject("MessageService"); }
        }
        public IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }
        public IUserService UserService
        {
            get { return (IUserService)ContextRegistry.GetContext().GetObject("UserService"); }
        }
         
        public IList<Domain.Messaging.Message> GetMessages(string sortExpression, int startRowIndex, int maximumRows,bool isArchived,bool isScholarshipApprovals,MessageAction  messageAction)
        {
            if (string.IsNullOrEmpty(sortExpression))
                sortExpression = DEFAULT_SORTEXPRESSION;
            if (isArchived)
                return MessageService.FindAllArchived(startRowIndex,maximumRows,sortExpression,  UserContext.CurrentUser, UserContext.CurrentOrganization);

            if (isScholarshipApprovals)
                return MessageService.FindAllApprovedScholarships(startRowIndex, maximumRows, sortExpression, UserContext.CurrentUser, UserContext.CurrentOrganization);

            return (MessageAction)messageAction  != MessageAction.None ? MessageService.FindAll(startRowIndex, maximumRows, sortExpression, MessageAction, UserContext.CurrentUser, UserContext.CurrentOrganization)
                : MessageService.FindAllForInbox(startRowIndex, maximumRows, sortExpression, UserContext.CurrentUser, UserContext.CurrentOrganization);
              
        }

        public int CountMessages(bool isArchived, bool isScholarshipApprovals, MessageAction messageAction)
        {
            if (isArchived)
                return MessageService.CountAllArchived(  UserContext.CurrentUser, UserContext.CurrentOrganization);

            if (isScholarshipApprovals)
                return MessageService.CountAllApprovedScholarships(  UserContext.CurrentUser, UserContext.CurrentOrganization);

            return (MessageAction)messageAction != MessageAction.None ? MessageService.CountAll(MessageAction, UserContext.CurrentUser, UserContext.CurrentOrganization)
                : MessageService.CountAllForInbox(  UserContext.CurrentUser, UserContext.CurrentOrganization);
        }

        protected void pager_PreRender(object sender, EventArgs e)
        {
            var pager = (DataPager)sender;
            pager.Visible = pager.TotalRowCount > pager.PageSize;

        }
        protected void MessageDataSource_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["isArchived"] = Archived;
            e.InputParameters["isScholarshipApprovals"] = ScholarshipApprovals;
            e.InputParameters["messageAction"] = MessageAction;
        }

        protected void messageList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                 
                var date = (Label)e.Item.FindControl("lblDate");
                 
                var message = ((Domain.Messaging.Message)((ListViewDataItem)e.Item).DataItem);
                
                date.Text = message.Date.ToLocalTime().ToShortDateString();
              
                var link = (LinkButton)e.Item.FindControl("linkToMessage");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) { Path = ResolveUrl(LinkTo) }.ToString()
                + "?popup=true&id=" + +(message).Id;
                link.Attributes.Add("onclick", String.Format("messageView('{0}'); return false;", url));
                link.Attributes.Add("class", message.IsRead ? "GreenLink" : "BlueLink");
            }
        }
    }
}