﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="ScholarBridge.Web.Common.Login" %>
<asp:LoginView ID="loginView" runat="server">
    <AnonymousTemplate>
    <asp:Login ID="Login1" runat="server" onloggedin="Login1_LoggedIn" onloginerror="Login1_LoginError">
     <LayoutTemplate>
        <div id="ValidationErrorPopup" title="" style="display: none">
            <asp:Image ID="ValidationErrorPopupimage" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/cooltipicon_errorbig.png"/> 
            <asp:Literal ID="FailureDialogText" runat="server"> Invalid email or password. Please try again.</asp:Literal>
        </div>
        <asp:panel ID="loginPanel" DefaultButton="Login" runat="server">
            <h2>Sign In <span id="or-create-account" style="font-size:12px;">or <asp:HyperLink id="registerlink" runat="server" Text="Create an Account" /></span></h2>
            <asp:Textbox id="UserName" text="Email Address" runat="server" Cssclass="DashboardUnlogged" />
            <asp:TextBox TextMode="Password"  id="Password" text="password" runat="server" Cssclass="DashboardUnlogged" />
            <asp:Button Cssclass="Button button" ID="Login" CommandName="Login" runat="server" />
            <div style="text-align:right;">
                <span class="ForgotPassword"> <asp:HyperLink id="forgotPassword" runat="server" Text="Forgot your password?" NavigateUrl="#"  /> </span>
                <span id="registerTodayContainer" runat="server" class="register-today"><a href="<%= ScholarBridge.Web.LinkGenerator.GetFullLinkStatic("/Seeker/Register.aspx") %>">Register Today!</a></span>
                <br />
                <span class="ErrorMsg"><asp:Literal ID="FailureText"  runat="Server"  Visible="false" /></span>
           </div>
        </asp:panel>
     </LayoutTemplate>
    </asp:Login>
    </AnonymousTemplate>    
</asp:LoginView>