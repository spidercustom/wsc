﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchList.ascx.cs" Inherits="ScholarBridge.Web.Common.MatchList" %>

<%@ Import Namespace="ScholarBridge.Domain.Auth"%>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>
<%@ Register TagPrefix="sb" TagName="CoolTipInfo" Src="~/Common/CoolTipInfo.ascx" %>  
<asp:ObjectDataSource ID="MatchDataSource" runat="server" 
    SelectMethod="GetMatchesForSeekerWithoutApplications" TypeName="ScholarBridge.Web.Common.MatchList"    
      SortParameterName="sortExpression"  
      SelectCountMethod="CountMatchesForSeekerWithoutApplications"  EnablePaging="true"  MaximumRowsParameterName="maximumRows" 
      
         >
</asp:ObjectDataSource>
<sbCommon:SortableListView ID="matchList" runat="server" 
    onitemdatabound="matchList_ItemDataBound" 
    DataKeyNames="Id" DataSourceID="MatchDataSource" SortExpressionDefault="Rank"
			SortDirectionDefault="Descending"   >
    <LayoutTemplate>
     
    <table class="sortableListView" >
            <thead>
            <tr>
             
                <th></th>
                
                <th  style="width:250px;"> <sbCommon:SortableListViewColumnHeader Key="ScholarshipName" Text="Scholarship Name"  ID="SortableListViewColumnHeader1" runat="server"  /></th>
                <th  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Rank', 'Matches are ranked based on your profile and the number of minimum eligibility and preferred criteria you meet. Minimum requirements make up 2/3 of the rank, while preferences count for 1/3.', '')">
                    <sbCommon:SortableListViewColumnHeader Key="Rank" Text="Rank"  ID="SortableListViewColumnHeader2" runat="server"  />
                </th>
                <th  onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Eligibility Criteria', 'The number of minimum eligibility requirements that match your profile and are defined in the Scholarship.', '')">
                    <sbCommon:SortableListViewColumnHeader Key="EligibilityCriteria" Text="Eligibility Criteria"  ID="SortableListViewColumnHeader3" runat="server"  />
                </th>
                <th onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Preferred Criteria', 'The number of preferred criteria that match your profile and are defined in the scholarship.', '')">
                  <sbCommon:SortableListViewColumnHeader Key="PreferredCriteria" Text="Preferred Criteria"  ID="SortableListViewColumnHeader4" runat="server"  />
               </th>
                <th onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Application Due Date', 'The date the scholarship application is due.', '')">
                    <sbCommon:SortableListViewColumnHeader Key="ApplicationDueDate" Text="Application Due Date"  ID="SortableListViewColumnHeader5" runat="server"  />
                </th>
                <th onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Number of Awards', 'The anticipated number of scholarship awards available.', '')">
                    <sbCommon:SortableListViewColumnHeader Key="NumberOfAwards" Text="# of Awards"  ID="SortableListViewColumnHeader6" runat="server"  />
                </th>
                <th onmouseout="$('#COOLTIP').hide();" onmouseover="CoolTip_Show(this.id, 'Scholarship Amount', 'The anticipated amount of the scholarship award.', '')">
                    <sbCommon:SortableListViewColumnHeader Key="ScholarshipAmount" Text="Scholarship Amount"  ID="SortableListViewColumnHeader7" runat="server"  />
                </th>
                <th ></th>
            </tr>
        </thead>
        <tbody>
            <asp:PlaceHolder id="itemPlaceholder" runat="server" />
        </tbody>
    </table>
    <div class="pager">
    <asp:DataPager runat="server" ID="pager"   OnPreRender="pager_PreRender"  PageSize="20"   >
        <Fields>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                ShowNextPageButton="false" ShowLastPageButton="false" />
            <sbCommon:CustomNumericPagerField  VisiblePageNumberCount="25"
                CurrentPageLabelCssClass="pagerlabel"
                NextPreviousButtonCssClass="pagerlink"
                PagingPageLabelCssClass="pagingPageLabel"
                NumericButtonCssClass="pagerlink"/>
            <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                ShowNextPageButton="true" ShowLastPageButton="false" />
        </Fields>
    </asp:DataPager>
    </div> 
    </LayoutTemplate>
          
    <ItemTemplate>
    <tr class="row">
     
        <td><asp:ImageButton id="IconButton" runat="server" CommandName="iconClicked"  ImageUrl="~/images/tag.png"/></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:##}%")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="ListButton" Width="70px" /></td>
    </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <tr class="altrow">
     
        <td><asp:ImageButton  id="IconButton" runat="server" CommandName="iconClicked" ImageUrl="~/images/tag.png" /></td>
        <td><asp:LinkButton id="linktoScholarship" runat="server" ><%# Eval("ScholarshipName")%></asp:LinkButton></td>
        <td><%# Eval("Rank", "{0:##}%")%></td>
        <td><%# Eval("MinumumCriteriaString")%></td>
        <td><%# Eval("PreferredCriteriaString")%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.ApplicationDueDate.GetValueOrDefault().ToShortDateString()%></td>
        <td><%# ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.FundingParameters.MaximumNumberOfAwards%></td>
        <td><%# String.Format("{0:c}", ((ScholarBridge.Domain.Match)Container.DataItem).Scholarship.AmountRange)%></td>
        <td style="text-align:center;"><%# (((ScholarBridge.Domain.Match)Container.DataItem).MatchApplicationStatusString == "Closed")?"Closed":"" %><asp:Button Enabled='<%# UserContext.CurrentUser.IsInRole(Role.SEEKER_ROLE) %>' ID="matchBtn" runat="server" CssClass="ListButton" Width="70px" /></td>
    </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    <p>There are no scholarships saved to <b>My Scholarships of Interest.</b> Click <asp:Image ID="Image4" ImageUrl="~/images/star_empty.png" runat="server"/> to save.</p>
    </EmptyDataTemplate>
</sbCommon:SortableListView>

