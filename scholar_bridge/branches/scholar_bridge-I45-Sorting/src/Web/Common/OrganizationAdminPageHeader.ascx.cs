﻿using System;
using System.Web.UI;

namespace ScholarBridge.Web.Common
{
    public partial class OrganizationAdminPageHeader : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var linkGenerator = new LinkGenerator();
            supportlink.HRef = linkGenerator.GetFullLink("/contactus.aspx");
        }
    }
}