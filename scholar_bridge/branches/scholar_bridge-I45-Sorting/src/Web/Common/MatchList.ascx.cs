﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScholarBridge.Business;
using ScholarBridge.Common;
using ScholarBridge.Domain;
using System.Collections.Generic;
using Spring.Context.Support;
 

namespace ScholarBridge.Web.Common
{
    public delegate void MatchIconHandler(Match match, ImageButton iconButton);

    public delegate void MatchActionButtonHandler(Match match, Button button);
    
    public partial class MatchList : UserControl
    {


        private const string DEFAULT_SORTEXPRESSION = "Rank DESC";
        public string LinkTo { get; set; }
       

        public IMatchService MatchService
        {
            get { return (IMatchService)ContextRegistry.GetContext().GetObject("MatchService"); }
        }

        protected virtual IUserContext UserContext
        {
            get { return (IUserContext)ContextRegistry.GetContext().GetObject("UserContext"); }
        }
        public ListView List
        {
            get { return matchList; }
        }

        public MatchIconHandler IconConfigurator { get; set; }
        public MatchActionButtonHandler ActionButtonConfigurator { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            matchList.ItemCommand += matchList_ItemCommand;
            
        }
 
        private void matchList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            Console.WriteLine(e.CommandArgument);
        }
        
        public IList<Match> GetMatchesForSeekerWithoutApplications(string sortExpression , int startRowIndex, int maximumRows)
        {
            if (string.IsNullOrEmpty(sortExpression))
                sortExpression = DEFAULT_SORTEXPRESSION;

            return MatchService.GetMatchesForSeekerWithoutApplications(startRowIndex, maximumRows, sortExpression, UserContext.CurrentSeeker);
             
        }

        public int CountMatchesForSeekerWithoutApplications()
        {
            var count =MatchService.CountMatchesForSeekerWithoutApplications(UserContext.CurrentSeeker);
            return count;
        }
         

        protected void matchList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var match = ((Match) ((ListViewDataItem) e.Item).DataItem);
                
                var link = (LinkButton) e.Item.FindControl("linktoScholarship");
                var url = new UrlBuilder(Request.Url.AbsoluteUri) {Path = ResolveUrl(LinkTo)}.ToString()
                          + "?id=" + match.Scholarship.Id + "&print=true";
                link.Attributes.Add("onclick", String.Format("scholarshipPrintView('{0}'); return false;", url));
            }
        }
        
        protected void pager_PreRender(object sender, EventArgs e)
        {
            var pager = (DataPager) sender;
            pager.Visible = pager.TotalRowCount > pager.PageSize;
             
        }

         
    }
}