﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeekerInfoShow.ascx.cs" Inherits="ScholarBridge.Web.Common.SeekerInfoShow" %>

<p class="form-section-title section-level-1">Applicant's Profile</p>
<div id="linkarea1" runat="server" class="linkarea">
<p>[<asp:HyperLink ID="linkTop" runat="server">Edit</asp:HyperLink>]</p> 
</div>     
<div class="legends">
    <ul class="vertical-indent-level-1">
        <li><asp:Image ID="MinimumCriteriaIconControl" AlternateText="Minimum" ImageUrl="~/images/criteria_filter.png" runat="server" /> indicates a required criteria for this scholarship</li>
        <li><asp:Image ID="PreferenceIconCcontrol" AlternateText="Preference" ImageUrl="~/images/criteria_preference.png" runat="server" /> indicates a preferred, but not required criteria, for this scholarship</li>
    </ul>
</div>

<br />
<asp:PlaceHolder runat="server" ID="NoSelectionContainerControl">
    <p class="vertical-indent-level-1">
        No Scholarship Match Criteria Selected. Please go to the "Selection Criteria" tab while editing scholarship, 
        to choose fields to be used for Scholarship selection
    </p>
</asp:PlaceHolder>

<p class="form-section-title section-level-2" id="ApplicantStudentTypeHeader" runat="server">Applicant Student Type</p>
<table class="viewonlyTable" id="ApplicantStudentTypeTable" runat="server">
    <tr id="StudentGroupRow" runat="server">
        <th scope="row">Student Groups: <asp:Image ID="StudentGroupUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblStudentGroups"  runat="server"  /></td>
    </tr>
    <tr id="SchoolTypesRow" runat="server">
        <th scope="row">School Types: <asp:Image ID="SchoolTypeUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblSchoolTypes"  runat="server"  /></td>
    </tr>
    <tr id="AcademicProgramsRow" runat="server">
        <th scope="row">Academic Programs: <asp:Image ID="AcademicProgramUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblAcademicPrograms"  runat="server"  /></td>
    </tr>
     <tr id="EnrollmentStatusesRow" runat="server">
        <th scope="row">Enrollment Statuses: <asp:Image ID="SeekerStatusUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblSeekerStatuses"  runat="server"  /></td>
    </tr>
     <tr id="LengthOfProgrammRow" runat="server">
        <th scope="row">Length of Program: <asp:Image ID="ProgramLengthUsageTypeIconControl"  runat="server" /></th>
        <td><asp:Literal ID="lblLengthOfProgram"  runat="server"  /></td>
    </tr>
     <tr id="CollegesRow" runat="server">
        <th scope="row">Colleges: <asp:Image ID="CollegesUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblColleges"  runat="server"  /></td>
    </tr>
</table>

<p class="form-section-title section-level-2" id="ApplicantDemographicsPeronalHeader" runat="server">Applicant Demographics - Personal</p>
<table class="viewonlyTable" id="ApplicantDemographicsPeronalTable" runat="server">
    <tr id="FirstGenerationRow" runat="server">
        <th scope="row">First Generation: <asp:Image ID="FirstGenerUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblFirstGeneration"  runat="server"  /></td>
    </tr>
    <tr id="EthnicityRow" runat="server">
        <th scope="row">Ethinicity: <asp:Image ID="EthnicityUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblEthinicity"  runat="server"  /></td>
    </tr>
    <tr id="ReligionRow" runat="server">
        <th scope="row">Religion: <asp:Image ID="ReligionUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblReligion"  runat="server"  /></td>
    </tr>
    <tr id="GendersRow" runat="server">
        <th scope="row">Genders: <asp:Image ID="GendersUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblGender"  runat="server"  /></td>
    </tr>
</table>

<p class="form-section-title section-level-2" id="ApplicantDemographicsGeographicHeader" runat="server">Applicant Demographics - Geographic</p>
<table class="viewonlyTable" id="ApplicantDemographicsGeographicTable" runat="server">
    <tr id="CountiesRow" runat="server">
        <th scope="row">County:</th>
        <td><asp:Literal ID="lblCounty"  runat="server"  /></td>
    </tr>
    <tr id="CitiesRow" runat="server">
        <th scope="row">City:</th>
        <td><asp:Literal ID="lblCity"  runat="server"  /></td>
    </tr>
    <tr id="StatesRow" runat="server"> 
        <th scope="row">State: </th>
        <td><asp:Literal ID="lblState"  runat="server"  /></td>
    </tr>
    <tr id="SchoolDistrictsRow" runat="server">
        <th scope="row">School District:</th>
        <td><asp:Literal ID="lblSchoolDistrict"  runat="server"  /></td>
    </tr>
    <tr id="HighSchoolsRow" runat="server">
        <th scope="row">High School:</th>
        <td><asp:Literal ID="lblHighSchool"  runat="server"  /></td>
    </tr>                
</table>

<p class="form-section-title section-level-2" id="ApplicantInterestsHeader" runat="server">Applicant Interests</p>
<table class="viewonlyTable" id="ApplicantInterestsTable" runat="server">
    <tr id="AcademicAreasRow" runat="server">
        <th scope="row">Academic Areas: <asp:Image ID="AcademicAreasUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblAcademicAreas"  runat="server"  /></td>
    </tr>
    <tr id="CareersRow" runat="server">
        <th scope="row">Careers: <asp:Image ID="CareersUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblCareers"  runat="server"  /></td>
    </tr>
    <tr  id="OrganizationsRow" runat="server">
        <th scope="row">Organizations: <asp:Image ID="OrganizationsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblOrganizations"  runat="server"  /></td>
    </tr>
    <tr id="CompanyRow" runat="server">
        <th scope="row">Companies: <asp:Image ID="CompaniesUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblCompanies"  runat="server"  /></td>
    </tr>                
</table>

<p class="form-section-title section-level-2" id="ApplicantActivitiesHeader" runat="server">Applicant Activities</p>
<table class="viewonlyTable" id="ApplicantActivitiesTable" runat="server">
    <tr id="HobbiesRow" runat="server">
        <th scope="row">Hobbies: <asp:Image ID="SeekerHobbiesUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblHobbies"  runat="server"  /></td>
    </tr>
    <tr id="SportsRow" runat="server">
        <th scope="row">Sports: <asp:Image ID="SportsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblSports"  runat="server"  /></td>
    </tr>
    <tr id="ClubsRow" runat="server">
        <th scope="row">Clubs: <asp:Image ID="ClubsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblClubs"  runat="server"  /></td>
    </tr>
    <tr id="WorkTypesRow" runat="server">
        <th scope="row">Work Type: <asp:Image ID="WorkTypeUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblWorkType"  runat="server"  /></td>
    </tr>
    <tr id="WorkHoursRow" runat="server">
        <th scope="row">Work Hours: <asp:Image ID="WorkHoursUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblWorkHours"  runat="server"  /></td>
    </tr>
    <tr id="ServiceTypesRow" runat="server">
        <th scope="row">Service Type: <asp:Image ID="ServiceTypeUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblServiceType"  runat="server"  /></td>
    </tr>
    <tr id="ServiceHoursRow" runat="server">
        <th scope="row">Service Hours: <asp:Image ID="ServiceHoursUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblServiceHours"  runat="server"  /></td>
    </tr>                
</table>

<p class="form-section-title section-level-2" id="ApplicantStudentPerformanceHeader" runat="server">Applicant Student Performance</p>
<table class="viewonlyTable" id="ApplicantStudentPerformanceTable" runat="server">
    <tr id="GPARow" runat="server">
        <th scope="row">GPA: <asp:Image ID="GPAUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblGPA"  runat="server"  /></td>
    </tr>
    <tr id="SATWritingScoreRow" runat="server">
        <th scope="row">SAT Writing Score: <asp:Image ID="SATWritingScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblSATWritingScore"  runat="server"  /></td>
    </tr>
    <tr id="SATCriticalReadingScoreRow" runat="server">
        <th scope="row">SAT Writing Score: <asp:Image ID="SATCriticalReadingScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblSATCriticalReadingScore"  runat="server"  /></td>
    </tr>
    <tr id="SATMathematicsScoreRow" runat="server">
        <th scope="row">SAT Writing Score: <asp:Image ID="SATMathematicsScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblSATMathematicsScore"  runat="server"  /></td>
    </tr>
    <tr id="ACTEnglishScoreRow" runat="server">
        <th scope="row">ACT Score: <asp:Image ID="ACTEnglishScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblACTEnglishScore"  runat="server"  /></td>
    </tr>
    <tr id="ACTMathematicsScoreRow" runat="server">
        <th scope="row">ACT Score: <asp:Image ID="ACTMathematicsScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblACTMathematicsScore"  runat="server"  /></td>
    </tr>
    <tr id="ACTReadingScoreRow" runat="server">
        <th scope="row">ACT Score: <asp:Image ID="ACTReadingScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblACTReadingScore"  runat="server"  /></td>
    </tr>
    <tr id="ACTScienceScoreRow" runat="server">
        <th scope="row">ACT Score: <asp:Image ID="ACTScienceScoreUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblACTScienceScore"  runat="server"  /></td>
    </tr>
    <tr id="ClassRankRow" runat="server">
        <th scope="row">Class Rank: <asp:Image ID="ClassRankUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblClassRank"  runat="server"  /></td>
    </tr>
    <tr id="APCreditsEarnedRow" runat="server">
        <th scope="row">AP Credits Earned: <asp:Image ID="APCUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblAPCredits"  runat="server"  /></td>
    </tr>
    <tr id="IBCreditsEarnedRow" runat="server">
        <th scope="row">IB Credits Earned: <asp:Image ID="IBCUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblIBCredits"  runat="server"  /></td>
    </tr>
    <tr id="HonorsRow" runat="server">
        <th scope="row">Honors: <asp:Image ID="HonorsUsageTypeIconControl" runat="server" /></th>
        <td><asp:Literal ID="lblHonors"  runat="server"  /></td>
    </tr>
</table>


<div id="linkarea2" runat="server" class="linkarea">
 <p>[<asp:HyperLink ID="linkBottom" runat="server">Edit</asp:HyperLink>]</p> 
</div>
