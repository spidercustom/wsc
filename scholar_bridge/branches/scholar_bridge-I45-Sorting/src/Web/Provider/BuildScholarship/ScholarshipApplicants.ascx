﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScholarshipApplicants.ascx.cs" Inherits="ScholarBridge.Web.Provider.BuildScholarship.ScholarshipApplicants" %>
<%@ Import Namespace="ScholarBridge.Web"%>

<%@ Register src="~/Common/CalendarControl.ascx" tagname="CalendarControl" tagprefix="sb" %>
<%@ Register assembly="Web" namespace="ScholarBridge.Web.Common" tagprefix="sbCommon" %>

<div class="form-iceland-container">
    <div class="search exclude-in-print form-iceland" runat="server" visible="<%# !ScholarBridge.Web.Extensions.PageExtensions.IsInPrintView(this.Page) %>">
        <p class="form-section-title">Search</p>
        
        <label for="<%= searchApplicantsTxt.ClientID %>">Search by Applicant Last Name</label>
        <asp:TextBox ID="searchApplicantsTxt" runat="server" title="Last Name"/> 
        <sbCommon:AnchorButton ID="searchApplicants" runat="server" Text="Search" 
            onclick="searchApplicants_Click" /> <sbCommon:AnchorButton ID="clearSearchBtn" 
            runat="server" Text="Clear Search" onclick="clearSearchBtn_Click" />
        <br />
        <hr />
        <asp:panel ID="hideMe1" Visible="false" runat="server">
            <sbCommon:AnchorButton ID="printWindow" runat="server" Text="Print" />
            <br />
            <hr />
        </asp:panel>
    </div>


    <br />
    <asp:HiddenField ID="hiddenfinalist" runat="server" />
    <asp:HiddenField ID="hiddenawardstatus" runat="server" />
    

    <div>
        <p class="form-section-title">Award Status</p>
        <p>
            Scholarship applications submitted. Sort by any field. Check finalists as you evaluate applicants. When ready, update the award status:
        </p>
        <dl>
            <dt>Offered</dt>
            <dd>Indicates Scholarship offered to the applicant – generates a message to the applicants in box.</dd>
            <dt>Awarded</dt>
            <dd>Indicates Scholarship accepted by applicant (off line) – generates a message to applicants in box.</dd>
            <dt>Not Awarded</dt>
            <dd>Indicates Scholarship was offered to an applicant but not awarded to that applicant.</dd> 
        </dl>
    </div>

    <br />

    <div id="closePeriod" title="Confirm delete" style="display:none">
        No applicants have been updated to an award status of Awarded. <br />
        Are you sure you want to indicate the award period is closed?
    </div>   
    <br /><hr />

    <div class="form-iceland">
        <p class="form-section-title">Award Period Close Date</p>
        <p>Update this date when all awards for the current award period have been awarded. This will close the scholarship and send a message to applicants that the award period is closed.</p>
        <br />
        <label for="<%= awardPeriodCloseDate.ClientID %>">Award Period Close Date</label>
        <sb:CalendarControl ID="awardPeriodCloseDate" runat="server" /> 
        <br />
        
        <sbCommon:ConfirmButton ID="saveAwardPeriodDate" runat="server" Text="Save" OnClickConfirm="saveAwardPeriodDate_Click" Width="60px"   ConfirmMessageDivID="closePeriod"/>
        <br /><hr />
    </div>

    <br />
<div class="form-iceland">
<asp:ObjectDataSource ID="ApplicantsDataSource" runat="server"
    SelectMethod="GetApplicants" TypeName="ScholarBridge.Web.Provider.BuildScholarship.ScholarshipApplicants"    
      SortParameterName="sortExpression"   OnSelecting="ApplicantsDataSource_OnSelecting"
      SelectCountMethod="CountApplicants"  EnablePaging="true"  MaximumRowsParameterName="maximumRows" >  
       <SelectParameters>
       <asp:Parameter Name="search" Type="String" />
       <asp:Parameter Name="scholarshipId" Type="Int32"   />
       </SelectParameters>
</asp:ObjectDataSource>
<sbCommon:SortableListView ID="lstApplicants" runat="server" 
    onitemdatabound="lstApplicants_ItemDataBound"  DataKeyNames="Id" DataSourceID="ApplicantsDataSource" SortExpressionDefault="DateSubmitted"
			SortDirectionDefault="Descending"  >
    
        <LayoutTemplate>
        <table class="sortableListView" >
                <thead>
                <tr>
                    <th>
                    <sbCommon:SortableListViewColumnHeader Key="Finalist" Text="Finalist"  ID="ColumnHeaderFinalist" runat="server"  />
                     </th>
                    <th>
                     <sbCommon:SortableListViewColumnHeader Key="AwardStatus" Text="Award Status"  ID="ColumnHeaderAwardStatus" runat="server"  />
                    </th>
                    <th>        
                    <sbCommon:SortableListViewColumnHeader Key="ApplicantName" Text="Applicant Name"  ID="ColumnHeaderApplicantName" runat="server"  />
                    </th>
                    <th>        
                    <sbCommon:SortableListViewColumnHeader Key="DateSubmitted" Text="Date Submitted"  ID="ColumnHeaderDateSubmitted" runat="server"  />
                    </th>
                    <th>        
                    <sbCommon:SortableListViewColumnHeader Key="EligibilityCriteria" Text="Required Criteria"  ID="ColumnHeaderEligibilityCriteria" runat="server"  />
                    </th>
                    <th>        
                    <sbCommon:SortableListViewColumnHeader Key="PreferredCriteria" Text="Preferred Criteria"  ID="ColumnHeaderPreferredCriteria" runat="server"  />
                    </th>
                    <th>Attachments</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder id="itemPlaceholder" runat="server" />
            </tbody>
        </table>
        <div class="pager">
        <asp:DataPager runat="server" ID="pager"  PagedControlID="lstApplicants" PageSize="25" onprerender="pager_PreRender">
            <Fields>
                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="true"
                    ShowNextPageButton="false" ShowLastPageButton="false" />
                <sbCommon:CustomNumericPagerField
                    CurrentPageLabelCssClass="pagerlabel"
                    NextPreviousButtonCssClass="pagerlink"
                    PagingPageLabelCssClass="pagingPageLabel"
                    NumericButtonCssClass="pagerlink"/>
                <asp:NextPreviousPagerField ShowFirstPageButton="false" ShowPreviousPageButton="false"
                    ShowNextPageButton="true" ShowLastPageButton="false" />
            </Fields>
        </asp:DataPager>
    </div> 
        </LayoutTemplate>
              
        <ItemTemplate>
        <tr class="row">
            <td><asp:CheckBox ID="appFinalistCheckBox" runat="server" AutoPostBack="true"/></td>
            <td><asp:DropDownList ID="awardStatusDdl" runat="server" AutoPostBack="true" /></td>
            <td>
                <a id="application" target="_blank" href='<%# LinkGenerator.GetFullLinkStatic(string.Format("/Provider/Applications/Show.aspx?aid={0}&print=true", Eval("Id"))) %>'><%# Eval("Seeker.Name.NameLastFirst")%></a>
            </td>
            <td><%# Eval("SubmittedDate", "{0:MM/dd/yyyy}")%></td>
            <td><asp:Label ID="lblMin" runat="server" /></td>
            <td><asp:Label ID="lblPref" runat="server" /></td>
            <td><asp:Button ID="attachmentsBtn" runat="server"   CssClass="ListButton" Width="70px"/></td>
        </tr>
        </ItemTemplate>
        
        <EmptyDataTemplate>
            <p><asp:label ID="emptySearchLabel" runat="server"><%= EmptyMessage %></asp:label></p>
        </EmptyDataTemplate>
    </sbCommon:SortableListView>

    <asp:panel ID="hideMe2"  runat="server">
        <sbCommon:AnchorButton ID="downloadAllBtn" runat="server" Text="Download All Attachments" OnClick="downloadAllBtn_OnClick"  />
        <sbCommon:AnchorButton ID="downloadAllFinalistBtn" runat="server" Text="Download All Finalist Attachments" OnClick="downloadAllFinalistBtn_OnClick"  />
    </asp:panel>

    
</div>
</div>