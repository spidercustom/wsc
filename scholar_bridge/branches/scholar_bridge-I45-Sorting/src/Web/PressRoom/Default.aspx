﻿<%@ Page Language="C#" MasterPageFile="~/Global.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScholarBridge.Web.PressRoom.Default" Title="Press Room" %>
<%@ Register src="~/PressRoom/ArticleList.ascx" tagname="ArticleList" tagprefix="sb" %>

<asp:Content ID="Content4" ContentPlaceHolderID="Head" Runat="Server">
    <meta name="Description" content="theWashBoard.org Press Room" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PageImage" Runat="Server">
    <img height="270" width="873" src="<%= ResolveUrl("~/images/header/seek_opportunity.jpg") %>" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="Body" Runat="Server">
    <h2>Press Room</h2>
    <p class="hookline">News Releases and Announcements &mdash; What's new on <b>theWashBoard.org</b></p>

    <sb:ArticleList ID="ArticleList1" runat="server" LinkTo="~/PressRoom/Show.aspx" />
</asp:Content>
